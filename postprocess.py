"""
################ Functionality ################
    Apply postprocessing to a given source directory
    Remove lines with expressions exclusive to html content
    optional: sanity check
"""

import os
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from nltk.corpus import words as w
from tqdm import tqdm


STOPLIST = set(stopwords.words('english'))
STOPLIST.add('whenever')
ST = SnowballStemmer('english', ignore_stopwords=True)
DICTIONARY = set(w.words())


def sanity_check(file, txt):
    """
    Checks how many words within the file are not contained in dictionary
    (method is rather a stub, was an idea)

    Arguments:
        file {string} -- name of the checked file
        txt {string} -- content of file
    """
    nonwords = [i for j in range(len(txt)) for i in txt[j].split(
    ) if i not in DICTIONARY and ST.stem(i) not in DICTIONARY]

    with open('Report_firstPass', "a", encoding="utf-8") as report:
        report.write("{} has {} fails:\n".format(file, len(nonwords)))
        report.write(' '.join(nonwords))
        report.write('\n')


def process_dir(sourcedir, check=False):
    """
    Perform semantical processing on a directory containing preprocessed text documents

    Arguments:
        sourcedir {string} -- complete path to source dir

    Keyword Arguments:
        check {bool} -- perform additional sanity check (default: {False})
    """
    # get the index of the folder
    filelist = os.listdir(sourcedir)
    print("Postprocessing {} files in folder".format(len(filelist)))

    for i in tqdm(range(len(filelist))):
        doc = filelist[i]
        txt = ''
        with open('{}/{}'.format(sourcedir, doc), "r", encoding="utf-8") as readfile:
            txt = readfile.read().split('\n')

            if check:
                sanity_check(doc, txt)

        with open('{}/{}'.format(sourcedir, doc), "w", encoding="utf-8") as writefile:
            for j in enumerate(txt):
                tmp = j[1]
                if not tmp or 'font face' in tmp or 'html' in tmp or 'opentyp' in tmp:
                    continue
                if 'mjxc' in tmp or 'ajax' in tmp or 'cdnjs' in tmp or 'mjx' in tmp:
                    continue
                # #wikipedia
                if 'svg' in tmp or ' slabel' in tmp or 'wikiproject' in tmp or ' mobi ' in tmp:
                    continue
                # #conservapedia
                if 'pagetalk' in tmp or ' edt' in tmp or ' ebook' in tmp or ' isbn ' in tmp:
                    continue
                # anarchism
                if 'http' in tmp or 'ak press' in tmp or 'kilobyt' in tmp or 'transcrib' in tmp:
                    continue
                # Metapedia
                if 'navig ' in tmp or ' tool ' in tmp:
                    continue
                if 'see extern ' in tmp:
                    continue
                writefile.write(tmp.replace(
                    'co op ', 'coop').replace('op cit', 'opus_citatem'))
                writefile.write('\n')
