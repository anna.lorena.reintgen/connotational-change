"""
############# Functionality ############

crawl the website conservapedia.org 
save all content pages to CDocs_raw
ignore Talk, Template, Category pages (and some other)
can be called several times without crawling duplicates
internally stores list of already processed pages
and the list of pages that remain to be processed
"""


import os
import requests
from bs4 import BeautifulSoup
from tqdm import tqdm

# arbitrary chosen limit to the number of crawled pages
LIM = 1500


def crawler(art, max_art=LIM):
    """
    Main crawling function
    links from visited pages are added to the pipeline

    Arguments:
        art {string} -- page name that is the starting point

    Keyword Arguments:
        max_art {int} -- max number of articles (default: {LIM})
    """
    # retrieve list of pages already crawled
    crawled = read_crawled()
    _dir = 'CDocs_raw'
    if not os.path.exists(_dir):
        os.makedirs(_dir)

    # add starting point to pipe
    crawlpipe = [art]
    # read links from previous runs
    crawlpipe.extend(get_todo())
    print("{} links in the crawlpipe".format(len(crawlpipe)))

    # break condition with update bar
    for i in tqdm(range(max_art)):
        if not crawlpipe:
            break
        # retrieve the first article from the pipe
        current_article = crawlpipe.pop(0)

        # get the links from the article page
        links = get_links(current_article)
        crawlpipe.extend(links)  # add the links to the crawler pipe

        # write the article text to file
        article_text = get_text(current_article)
        if article_text:
            with open("{}/{}".format(_dir, current_article.replace("/", "-").replace("(", "_").replace(")", "_").replace("\"", "").replace("\"", "")), "w", encoding="utf-8") as f:
                for item in enumerate(article_text):
                    index = item[0]
                    f.write(article_text[index].text)
                    f.write(' ')

        crawled.append(current_article)

#         remove already crawled articles
        while len(crawlpipe) > 0 and crawlpipe[0] in crawled:
            crawlpipe.pop(0)

#             what did we crawl?
    with open("CDone", "a", encoding="utf-8") as f:
        for line in crawled:
            f.write(line)
            f.write("\n")

#       whats left?
    with open("CTodo", "w", encoding="utf-8") as f:
        i = 0
        while crawlpipe:
            i += 1
            f.write(crawlpipe.pop())
            f.write("\n")
        print("Wrote {} non-parsed links to file".format(i))


def get_links(article):
    """
    retrieves links from a content page
    filters non-content links
    returns as list of strings 

    Arguments:
        article {string} -- Name of the article page

    Returns:
        list -- list of strings
    """
    url = 'https://www.conservapedia.com/{}'.format(article)
    try:  # catch possibility of wrong link
        result = requests.get(url)
    except requests.exceptions.Timeout:
        return []
    soup = BeautifulSoup(result.content, features='lxml')
    links = soup("a")
    list_link = []

    for i in range(len(links)):
        try:
            link = links[i]['href']
            if(link[0] == '/' and links[i].text != '' and not "#" in link and not "index" in link and not "Special" in link and not ".jpg" in link and not ".jpeg" in link and not "File" in link and not "Category" in link and not "Template" in link and not "Talk" in link and not "User" in link and not "Portal" in link and not r"Debate\:" in link and not "Conservapedia" in link and not "Wikiproject" in link and not "MediaWiki" in link and not "Help" in link and not "Homework" in link):
                list_link.append(links[i]['href'][1:])

        except KeyError:
            continue
    return list_link


def get_text(article):
    try:
        page = requests.get('https://www.conservapedia.com/{}'.format(article))
    except requests.exceptions.Timeout:
        return ''
    soup = BeautifulSoup(page.content, features="lxml")
    text = soup.findAll('p')
    text.extend(soup.findAll('li'))  # also scrape content of lists
    text.extend(soup.findAll('blockquote'))  # ..and quotes
    text.extend(soup.findAll('b'))  # and boldface text
    text.extend(soup.findAll('dd'))  # something else
    text.extend(soup.findAll('dl'))  # yet another thing
    text.extend(soup.findAll('td'))  # some weird quote
    text.extend(soup.findAll('h1'))  # headline
    text.extend(soup.findAll('h2'))  # headline
    text.extend(soup.findAll('h3'))  # headline
    text.extend(soup.findAll('h4'))  # headline
    text.extend(soup.findAll('h5'))  # headline
    text.extend(soup.findAll('h6'))  # headline
    text.extend(soup.findAll('pre'))  # formatted text
    text.extend(soup.findAll('dt'))  # list item

    return text


def read_crawled():
    """ Read the list of previously crawled pages from CDone

    Returns:
        list -- list of page names 
    """
    crawled = []
    with open("CDone", "r", encoding="utf-8") as f:
        for line in f:
            crawled.append(line.replace('\n', ''))
    print("Found {} already crawled articles".format(len(crawled)))
    return crawled


def get_todo():
    """Read list of pages that are to be crawled from CTodo

    Returns:
        list -- list of page names
    """
    articles = []
    with open("CTodo", "r", encoding="utf-8") as f:
        for line in f:
            if(line != '\n'):
                articles.append(line.replace('\n', ''))
    articles_filter = list(set(articles))
    return articles_filter


# call the crawler from some starting page
crawler("Liberal")
