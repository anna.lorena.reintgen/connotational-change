"""
############# Functionality ############

Crawl the pages of the LessWrong blog
(can be modified to crawl the Rationalwiki)

First collect all page links:
    start with the start page of the reader page 
    recursively follow on all the pages and crawl the list of posts
    write the pages links directly to a file
Process list of collected links 
    write contents into LWDocs_raw folder

"""

import os
import requests
from bs4 import BeautifulSoup
from tqdm import tqdm

# utility function used for terminal output
def clear():
    os.system('cls')

# arbitrarily chosen limit to the number of crawled pages
LIM = 1000
_dir = 'LWDocs_raw'



def collect_posts(_offset):
    """as in ancrawler, collect all links to pages from listing
    iterate recursively until empty listing page ist reached
    pages are saved as (title, url) pairs

    Arguments:
        _offset {int} -- offset of the current listing index
    """
    _url = 'https://lw2.issarice.com/?view=new&offset={}'.format(_offset)
    page = requests.get(_url)
    soup = BeautifulSoup(page.content, features='lxml')

 #     first check if we reached an empty page
    divers = soup.findAll('div')  # get all items with tag 'div'
    for item in divers:
        if(item['id'] == 'content'):  # if its the content part, thats what we want
            target = item
            break
    t = target.findAll(href=False)  # scrap the links
    text = ''
    for item in t:
        if(not 'Reader' in item.text):  # scrap the title
            text += item.text

    if(not text):
        return  # if theres nothing on the page, break the recursion

 #     otherwise continue, and get the links on the page
    # dont follow the links from the recent comment section
    links = target("a")
    posts = []  # things we write into LWTodo
    for i in range(len(links)):
        try:
            link = links[i]['href']
            if(link[0:7] == '/posts/' and not '#comments' in link and not '#answers' in link):
                target = (link[1:], links[i].text.replace(
                    ' ', '_').replace('\n', ''))
                posts.append(target)

        except KeyError:
            print("Key Error while appending links")
            continue

    i = len(posts)
    with open("LWTodo", "a", encoding="utf-8") as f:
     #         write the collected posts to the todo file
        while posts:
            art_id, title = posts.pop()
            f.write('{} {}'.format(art_id, title))
            f.write("\n")

    _offset += 50
 #     clear_output(wait=True)
    clear()
    print("Offset is now {}".format(_offset))
    collect_posts(_offset)

    return


def crawler(max_art=LIM):
    """main crawling function
    crawl a maximium of LIM pages from the lesswrong site

    Keyword Arguments:
        max_art {int} -- max number of pages to crawl (default: {LIM})
    """
    # read list of pages already crawled
    crawled = read_crawled()

    if not os.path.exists(_dir):
        os.makedirs(_dir)
#    _init = 'Gender-equality_paradox'
    crawlpipe = []
    fails = []
    crawlpipe.extend(get_todo())
    print("{} links in the crawlpipe".format(len(crawlpipe)))

    # break condition with update bar
    for i in tqdm(range(max_art)):
        if not crawlpipe:
            break
            # retrieve the first article from the pipe
        current_title, current_link = crawlpipe.pop(0)

# Uncomment following two lines for Rationalwiki
        # links = get_links(current_title) # get the links from the article page
        # crawlpipe.extend(links)  # add the links to the crawler pipe

        # write the article text to file
        article_text = get_text(current_link)
        try:
            with open("{}/{}".format(_dir, current_title.replace("/", "-").replace("(", "_").replace(")", "_")), "w", encoding="utf-8") as f:
                for i in range(len(article_text)):
                    f.write(article_text[i].text)
                    f.write(' ')
        except:
            fails.append((current_title,current_link))
        crawled.append(current_title)

#         remove already crawled articles
        while len(crawlpipe) > 0 and crawlpipe[0][0] in crawled:
            crawlpipe.pop(0)

#             what did we crawl?
    with open("LWDone", "a", encoding="utf-8") as f:
        for line in crawled:
            f.write(line)
            f.write("\n")

#       whats left?
    with open("LWTodo", "w", encoding="utf-8") as f:
        i = len(crawlpipe)
        j = len(fails)
        while crawlpipe:
            title, url= crawlpipe.pop()
            f.write('{} {}'.format(title,url))
            f.write("\n")
        while fails:
            title, url = fails.pop()
            f.write('{} {}'.format(title,url))
            f.write("\n")
        print("Wrote {} non-parsed links and {} fails to file".format(i, j))


def get_links(_page):
    """If Rationalwiki is crawled, this function collects links


    Arguments:
        _page {string} -- page name currently processed

    Returns:
        list -- list of page names 
    """
    try:
        page = requests.get('https://rationalwiki.org/wiki/{}'.format(_page))
        soup = BeautifulSoup(page.content, features="lxml")
        links = soup("a")
    except requests.exceptions.Timeout:
        return []
    list_link = []

#     do some filtering of the links
    for i in range(len(links)):
        try:
            link = links[i]['href']
            if(link[:6] == '/wiki/' and links[i].text != '' and not "#" in links[i]['href'] and not "index" in links[i]['href'] and not "Special" in links[i]['href']and not ".jpg" in links[i]['href'] and not ".jpeg" in links[i]['href'] and not "File" in links[i]['href'] and not "Category" in links[i]['href']):
                list_link.append(links[i]['href'][6:])

        except KeyError:
            continue
    return list_link


def get_text(_page):
    """Get content for a page name
    Can be modified for Rationalwiki pages

    Arguments:
        _page {string} -- url suffix for the page name

    Returns:
        list -- list of strings forming the content
    """
    try:
        page = requests.get('https://lesswrong.com/{}'.format(_page))
       # page = requests.get('https://rationalwiki.org/wiki/{}'.format( _page))
    except requests.exceptions.Timeout:
        print('failed page for page {}'.format(_page))
        return ''
    soup = BeautifulSoup(page.content, features="lxml")
    text = soup.findAll('p')
    text.extend(soup.findAll('li'))  # also scrape content of lists
    text.extend(soup.findAll('blockquote'))  # ..and quotes
    text.extend(soup.findAll('b'))  # and boldface text
    text.extend(soup.findAll('dd'))  # something else
    text.extend(soup.findAll('dl'))  # yet another thingf
    text.extend(soup.findAll('td'))  # some weird quote
    text.extend(soup.findAll('big'))  # big text is probably important..

    return text


def read_crawled():
    """Read the list of crawled pages from LWDone

    Returns:
        list -- list of page names
    """
    crawled = []
    with open("LWDone", "r", encoding="utf-8") as f:
        for line in f:
            crawled.append(line.replace('\n', ''))
   
    crawled = list(set(crawled))

    print("Found {} already crawled articles".format(len(crawled)))
    return crawled


def get_todo():
     """Read the list of pages to be crawled from LWTodo

    Returns:
        list -- list of page names
    """
    articles = []
    with open("LWTodo", "r", encoding="utf-8") as f:
        for line in f:
            if(line != '\n'):
                #                url, title = line.split()
                title = line.replace('\n', '')
               articles.append((url,title))
# For Rationalwiki: uncomment next line
                # articles.append(title)
    articles_filter = list(set(articles))
    return articles_filter


crawler( 1000)
