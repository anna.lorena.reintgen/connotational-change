"""
############# Functionality ############

crawl the english part of metapedia.org 
save all content pages to MDocs_raw
Requires graphic interface for the selenium web driver to work
ignore Talk, Template, Category pages (and some other)
can be called several times without crawling duplicates
internally stores list of already processed pages
and the list of pages that remain to be processed
"""

import os
import time
from tqdm import tqdm
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
binary = FirefoxBinary(r'/usr/bin/firefox')

LIM = 7000


def crawler(art, _dir = 'MDocs_raw', max_art=LIM):
    """
    Main crawling function
    start at some article
    continue with links found on page

    Arguments:
        art {string} -- page name where to start
        _dir {string} -- directory where to store files
        max_art {int} -- maximum number of crawled articles 
    """
    driver.get('https://en.metapedia.org/')
    time.sleep(5)  # circumvent ddos protection
    # list of pages already crawled
    crawled = read_crawled()

    if not os.path.exists(_dir):
        os.makedirs(_dir)

    # add the starting point to pipe
    crawlpipe = [art]
    crawlpipe.extend(get_todo())
    print("{} links in the crawlpipe".format(len(crawlpipe)))

    # break condition with update bar
    for i in tqdm(range(max_art)):
        if not crawlpipe:
            print("Crawlpipe empty")
            break
            # retrieve the first article from the pipe
        current_article = crawlpipe.pop(0)
        driver.get('https://en.metapedia.org/wiki/{}'.format(current_article))
        if i % 500 == 0:
            time.sleep(5)
            print("Zzzzz")
        source = driver.page_source

#         get the links from the article page
        links = get_links(source)
        crawlpipe.extend(links)  # add the links to the crawler pipe

        # write the article text to file
        article_text = get_text(source)

        if article_text:
            with open("{}/{}".format(_dir, current_article.replace("/", "-").replace("\"", "").replace("(", "").replace(")", "")), "w", encoding="utf-8") as f:
                for i in range(len(article_text)):
                    f.write(article_text[i].text)
                    f.write(' ')

        crawled.append(current_article)

#         remove already crawled articles
        while len(crawlpipe) > 0 and crawlpipe[0] in crawled:
            crawlpipe.pop(0)

#             what did we crawl? Note: the 'a' means append... important!
    with open("MDone", "a", encoding="utf-8") as f:
        for line in crawled:
            f.write(line)
            f.write("\n")

#       whats left?
    with open("MTodo", "w", encoding="utf-8") as f:
        i = len(crawlpipe)
        while crawlpipe:
            f.write(crawlpipe.pop())
            f.write("\n")
        print("Wrote {} non-parsed links to file".format(i))


def get_links(html_source):
    soup = BeautifulSoup(html_source, features="lxml")
    links = soup("a")
    list_link = []

#     do some filtering of the links
    for i in range(len(links)):
        try:
            link = links[i]['href']
            if(link[0] == '/' and links[i].text != '' and not "#" in link and not "index" in link and not "Special" in link and not ".jpg" in link and not ".jpeg" in link and not "File" in link and not "Category" in link and not "Template" in link and not "Talk" in link and not "User" in link and not "Portal" in link and not r"Debate\:" in link and not "Metapedia" in link and not "Wikiproject" in link and not "MediaWiki" in link and not "Help" in link and not "Homework" in link):
                list_link.append(links[i]['href'][6:])

        except KeyError:
            continue
    return list_link


def get_text(html_source):
    soup = BeautifulSoup(html_source, features="lxml")
    text = soup.findAll('p')
    text.extend(soup.findAll('li'))  # also scrape content of lists
    text.extend(soup.findAll('blockquote'))  # ..and quotes
    text.extend(soup.findAll('b'))  # and boldface text
    text.extend(soup.findAll('dd'))  # something else
    text.extend(soup.findAll('dl'))  # yet another thingf
    text.extend(soup.findAll('td'))  # some weird quote
    text.extend(soup.findAll('h1'))  # headline
    text.extend(soup.findAll('h2'))  # headline
    text.extend(soup.findAll('h3'))  # headline
    text.extend(soup.findAll('h4'))  # headline
    text.extend(soup.findAll('h5'))  # headline
    text.extend(soup.findAll('h6'))  # headline
    text.extend(soup.findAll('pre'))  # formatted text
    text.extend(soup.findAll('dt'))  # list item

    return text


def read_crawled():
    crawled = []
    with open("MDone", "r", encoding="utf-8") as f:
        for line in f:
            crawled.append(line.replace('\n', ''))
    print("Found {} already crawled articles".format(len(crawled)))
    return list(set(crawled))


def get_todo():
    #     either read the links from a todo list or from the index page from metapedia
    articles = []
    try:
        with open("archive/MTodo", "r", encoding="utf-8") as f:
            for line in f:
                articles.append(line.replace('\n', '').replace('MDocs/', ''))
        print("read succeeded")
    except:
        driver.get(
            "https://en.metapedia.org/m/index.php?title=Special:LonelyPages&limit=500&offset=1000")
        source = driver.page_source
        articles.extend(get_links(source))
    return articles


driver = webdriver.Firefox()

crawler('List_of_editions_of_Protocols_of_the_Elders_of_Zion')
