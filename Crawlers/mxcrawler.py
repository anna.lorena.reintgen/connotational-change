"""
############# Functionality ############

Crawl the pages of marxists.org

Stores files in MxDocs_raw
can be called several times without crawling duplicates
internally stores list of already processed pages
and the list of pages that remain to be processed
"""
import os
import requests
from bs4 import BeautifulSoup
from tqdm import tqdm

LIM = 1500
DIR = 'MxDocs_raw'

def crawler(_dir= DIR, max_art=LIM):
    """Main crawling function

    Keyword Arguments:
        _dir {string} -- folder name for document storage (default: {DIR})
        max_art {int} -- maximum number of articles to be crawled (default: {LIM})
    """
    # list of pages already crawled
    crawled = read_crawled()

    if not os.path.exists(_dir):
        os.makedirs(_dir)
    _init = 'archive/index.htm' # starting page
    crawlpipe = [_init]
    fails = []
    crawlpipe.extend(get_todo())
    print("{} links in the crawlpipe".format(len(crawlpipe)))

    # break condition with update bar
    for i in tqdm(range(max_art)):
        if not crawlpipe:
            break
        # retrieve the first article from the pipe
        current_title = crawlpipe.pop(0)

        # get the links from the article page
        links = get_links(current_title)
        crawlpipe.extend(links)  # add the links to the crawler pipe

        # write the article text to file
        article_text = get_text(current_title)
        try:
            with open("{}/{}".format(_dir, current_title.replace("/", "-").replace("(", "_").replace(")", "_").replace(".htm", "")), "w", encoding="utf-8") as f:
                for i in range(len(article_text)):
                    f.write(article_text[i].text)
        except:
            fails.append(current_title)
        crawled.append(current_title)

#         remove already crawled articles
        while len(crawlpipe) > 0 and crawlpipe[0] in crawled:
            crawlpipe.pop(0)

#             what did we crawl?
    with open("MxDone", "w", encoding="utf-8") as f:
        for line in crawled:
            f.write(line)
            f.write("\n")

#       whats left?
    with open("MxTodo", "w", encoding="utf-8") as f:
        i = len(crawlpipe)
        j = len(fails)
        while crawlpipe:
            title = crawlpipe.pop()
            f.write(title)
            f.write("\n")
        while fails:
            f.write(fails.pop())
            f.write("\n")
        print("Wrote {} non-parsed links and {} fails to file".format(i, j))


def get_links(_page):
    """Retrieve links from a given article page

    Arguments:
        _page {string} -- address to article page 

    Returns:
        list -- list of page links relative to root address
    """
    try:
        page = requests.get('https://www.marxists.org/{}'.format(_page))
        soup = BeautifulSoup(page.content, features="lxml")
        links = soup("a")
    except:
        return []
    list_link = []
    #     do some filtering of the links
    # links are given relative to current page directory
    # before returning links are modified to absolute reference
    for i in range(len(links)):
        try:
            link = links[i]['href']
            if(links[i].text != ''and not 'http' in link and not 'www' in link and not 'mailto' in link and not "#" in link and not ".pdf" in link and not ".jpeg" in link and not "File" in link):
                #                 reconstruct the full path
                # append link to current page address
                ref = _page.split('/')[:-1]
                ref.extend(link.split('/'))
                full_ref = ''
                for j in range(len(ref)):
                    if(ref[j] == '..'):
                        del ref[j-1]
                        del ref[j]
                for j in range(len(ref)):
                    full_ref += ref[j]
                    full_ref += '/'
                list_link.append(full_ref[:-1])

        except:
            continue

    return list_link


def get_text(_page):
    try:
        page = requests.get('https://www.marxists.org/{}'.format(_page))
    except:
        print('failed page')
        return ''
    soup = BeautifulSoup(page.content, features="lxml")
    text = soup.findAll('p')
    text.extend(soup.findAll('li'))  # also scrape content of lists
    text.extend(soup.findAll('blockquote'))  # ..and quotes
    text.extend(soup.findAll('b'))  # and boldface text
    text.extend(soup.findAll('dd'))  # something else
    text.extend(soup.findAll('dl'))  # yet another thingf
    text.extend(soup.findAll('td'))  # some weird quote

    return text


def read_crawled():
    crawled = []
    with open("MxDone", "r", encoding="utf-8") as f:
        for line in f:
            crawled.append(line.replace('\n', ''))

    print("Found {} already crawled articles".format(len(crawled)))
    return crawled


def get_todo():
    articles = []
    with open("MxTodo", "r", encoding="utf-8") as f:
        for line in f:
            if(line != '\n'):
                articles.append(line.replace('\n', ''))
    articles_filter = list(set(articles))
    return articles_filter


crawler()
