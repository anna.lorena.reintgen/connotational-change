"""
############# Functionality ############

crawl the website theanarchistlibrary.org 
save all content pages to AnDocs_raw
"""

import os
import requests
from bs4 import BeautifulSoup
from tqdm import tqdm

# visual inspection of the site gave 261 pages listing all articles 
PAGE_LIM = 262

def clear():
    os.system('cls')


def collect_posts():
    """
    start with the start page and collect the list of posts
    write the pages directly to the todo-file
    """
    retry = [] # fails that are retried at later point
    posts = [] # things we write into AnTodo
    # iterate over all pages listing links to articles
    for index in range(1, PAGE_LIM):  # inspection: 261 pages.and 15 links per page
        _url = 'http://theanarchistlibrary.org/latest/{}'.format(index)
        try:
            page = requests.get(_url)
        except requests.exceptions.Timeout: # catch Connection Timout
            retry.append(index) # retry the failed page index at the end
            continue
        if(page.status_code != 200): # 200 = html success code
            retry.append(index)
            continue
        soup = BeautifulSoup(page.content, features='lxml')

        links = soup("a")
        for link in links:
            try:
                ref = link['href']
                if('org/library' in ref): #only append links to articles
                    posts.append(ref)
            except KeyError:
                print("{} was not appended because there was no link".format(link))
                continue

    i = len(posts)
    with open("AnTodo", "a", encoding="utf-8") as f:
        # write the collected posts to the todo file
        while posts:
            url = posts.pop()
            f.write('{}'.format(url))
            f.write("\n")

    print("Wrote {} posts to file and have {} remaining pages that didnt work".format(
        i, len(retry)))
    print(retry)



def crawler(max_art):
    """
    Main crawling function.
    takes todo lists and collects all pages
    
    Arguments:
        max_art {int} -- limit of pages to be crawled
    """
    # list of pages already crawled
    crawled = read_crawled()

    _dir = 'AnDocs_raw'
    if not os.path.exists(_dir):
        os.makedirs(_dir)
    fails = []
    crawlpipe = get_todo() #read list of links from file
    print("{} links in the crawlpipe".format(len(crawlpipe)))

    # break condition with update bar
    for i in tqdm(range(max_art)):
        if not crawlpipe:
            break
        # retrieve the first article from the pipe
        current_title = crawlpipe.pop(0)

        # write the article text to file
        article_text = get_text(current_title)
        # use only page title, not entire url
        name = current_title.replace(
            'http://theanarchistlibrary.org/library/', '')
        try:
            # write page content into new file
            with open("{}/{}".format(_dir, name.replace("/", "-").replace("(", "_").replace(")", "_")), "w", encoding="utf-8") as f:
                for index, txt in enumerate(article_text):
                    f.write(txt.text)
                    f.write('\n')
        except:
            fails.append(current_title)
        crawled.append(current_title)

#   remove already crawled articles
        while len(crawlpipe) > 0 and crawlpipe[0] in crawled:
            crawlpipe.pop(0)

#   what did we crawl?
    with open("AnDone", "a", encoding="utf-8") as f:
        for line in crawled:
            f.write(line)
            f.write("\n")

#   whats left?
    with open("AnTodo", "w", encoding="utf-8") as f:
        i = len(crawlpipe)
        j = len(fails)
        while crawlpipe:
            title = crawlpipe.pop()
            f.write(title)
            f.write("\n")
        while fails:
            f.write(fails.pop())
            f.write("\n")
        print("Wrote {} non-parsed links and {} fails to file".format(i, j))


def get_text(_page):
    try:
        page = requests.get(_page)
    except requests.exceptions.Timeout:
        print('failed page for page {}'.format(_page))
        return ''
    soup = BeautifulSoup(page.content, features="lxml")
    text = soup.findAll('p')
    text.extend(soup.findAll('li'))  # also scrape content of lists
    text.extend(soup.findAll('blockquote'))  # ..and quotes
    text.extend(soup.findAll('b'))  # and boldface text
    text.extend(soup.findAll('dd'))  # something else
    text.extend(soup.findAll('dl'))  # yet another thing
    text.extend(soup.findAll('td'))  # some weird quote
    text.extend(soup.findAll('h1'))  # headline
    text.extend(soup.findAll('h2'))  # headline
    text.extend(soup.findAll('h3'))  # headline
    text.extend(soup.findAll('h4'))  # headline
    text.extend(soup.findAll('h5'))  # headline
    text.extend(soup.findAll('h6'))  # headline
    text.extend(soup.findAll('pre'))  # formatted text
    text.extend(soup.findAll('dt'))  # list item

    return text


def read_crawled():
    """ read the list of already crawled articles from file

    Returns:
        list -- list of strings containing the links of pages already crawled
    """
    crawled = []
    with open("AnDone", "r", encoding="utf-8") as f:
        for line in f:
            crawled.append(line.replace('\n', ''))

    crawled = list(set(crawled)) # remove duplicates

    print("Found {} already crawled articles".format(len(crawled)))
    return crawled


def get_todo():
    """Read the list of articles to be crawled from AnTodo
    If expected file does not exist, collect_posts is called

    Returns:
        list -- list of links 
    """
    articles = []
    try:
        with open("AnTodo", "r", encoding="utf-8") as f:
            todos = f.read()

    except FileNotFoundError:
        print("No Todo file found, collecting posts first")
        collect_posts()
        # file has been created now
        with open("AnTodo", "r", encoding="utf-8") as f:
            todos = f.read()
    
    for line in todos:
        if(line != '\n'):
            title = line.replace('\n', '')
            articles.append(title)
    
    articles_filter = list(set(articles))
    return articles_filter
