"""
##### Functionality ##########
Compute linear map between reference embedding and target
Apply semaxis projection to result set (optional)

Called from within main function
"""

import numpy as np
import gensim.models as mod
from scipy.spatial.distance import euclidean
import sem_ax_proj as spr

#########################################
ROOT_DIR = "" # complete path to root directory containing target folders
TARGETS = 'AnDocs CDocs MDocs MxDocs noTalkLW'.split()
#########################################

def find_deviation(_word, _source, _target):
    """
    called if semaxis is true
    project word into semantic space
    finds the five dimensions that have the highest difference
    uses absolute difference

    Arguments:
        _word {string} -- word that is analysed
        _source {mod.KeyedVectors} -- source embedding
        _target {mod.KeyedVectors} -- target embedding

    Returns:
        dict -- sorted dict of dim - diff pairs
    """
    # project the word into the semantic space for source/target
    proj1 = spr.project_word(_word, _target)
    proj2 = spr.project_word(_word, _source)
    if proj1.size == 0 or proj2.size == 0: print(_word)
    diff = proj1 - proj2
    diff_dic = {}
    for i in enumerate(diff):
        diff_dic[i[0]] = diff[i[0]]
    sort = sorted(diff_dic.items(), key=lambda kv: np.absolute(kv[1]))
    return sort[-5:]  # arbitrarily choose five


def compute_projection(_source, _targets, semaxis=False, _vec_size=300):
    """
    compute linear map and find deviating words
    iterate computation of the linear map and ignore top 50
    write results to file
    optionally analyse result set with semaxis projection

    Arguments:
        _source {string} -- string of the source embedding
        _targets {[string]} -- list of targets to be analyzed

    Keyword Arguments:
        semaxis {bool} -- apply semaxis projection (default: {False})
        _vec_size {int} -- vector size of embedding (default: {300})
    """
    source = mod.KeyedVectors.load(
        "{}Embeddings/{}{}.kv".format(ROOT_DIR, _source, _vec_size), mmap='r')
    blacklist = []
    for trgt in _targets:
        print("Now computing the projection for target {}".format(trgt))
        target = mod.KeyedVectors.load(
            "{}Embeddings/{}{}.kv".format(ROOT_DIR, trgt, _vec_size), mmap='r')

        # maximal number of iterations is 10
        for i in range(10):
            tmps = []
            tmpt = []
            # ensure that the order of the items is the same in both arrays
            for item in source.vocab:
                if item in target and not item in blacklist:
                    tmps.append(source[item])
                    tmpt.append(target[item])

            source_vecs = np.array(tmps)
            target_vecs = np.array(tmpt)
            transform = np.linalg.pinv(source_vecs).dot(target_vecs).T
            dist = {}

            for item in source.vocab:
                if item in target.vocab:
                    d_euc = euclidean(target[item], transform@source[item])
                    dist[item] = d_euc

            sorted_d = sorted(dist.items(), key=lambda kv: kv[1])
            tmp = len(blacklist)
            # add the top 50 deviating words to the blacklist,
            # they are not used to compute the next projection
            for item in sorted_d[-50:]:
                blacklist.append(item[0])
                blacklist = list(set(blacklist))  # remove duplicates
            if len(blacklist) == tmp:
                break
        result = []

        # filter words with identical neighborhood
        for word, dist in sorted_d[-25:]:
            n_source = [w for w, d in source.most_similar(word)]
            n_target = [w for w, d in target.most_similar(word)]
            if len({i for i in n_source if i in n_target}) < 3:  # arbitrarily set limit to 3
                result.append((word, dist, n_source, n_target))
        if not semaxis:
            with open('{}result'.format(ROOT_DIR), 'a', encoding='utf-8') as res:
                res.write(
                    'Result for the embedding {} and vector size {}:\n'.format(trgt, _vec_size))
                res.write("Number of iterations was {}\n".format(i))
                for word, dist, n_s, n_t in result:
                    res.write('%s - %.2f\n' % (word, dist))
                    res.write('most similar source: {}\n'.format(n_s))
                    res.write('most similar target: {}\n'.format(n_t))
        else:
            with open('{}result'.format(ROOT_DIR), 'a', encoding='utf-8') as res:
                res.write(
                    'Result for the embedding {} and vector size {}:\n'.format(trgt, _vec_size))
                res.write("Number of iterations was {}\n".format(i))
                for word, dist, n_s, n_t in result:
                    res.write('%s - %.2f\n' % (word, dist))
                    res.write('most similar source: {}\n'.format(n_s))
                    res.write('most similar target: {}\n'.format(n_t))
                    for index, value in find_deviation(word, source, target):
                        res.write('axis: %s, delta: %.2f \n' % (
                            spr.AXES[index], value))


# compute_projection(TARGETS)
