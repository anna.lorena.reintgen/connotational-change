"""
################ Functionality ################
    Construct a synthetic dataset from the Wikipedia Featured corpus
    perform a random attack on the dataset
    select 5 word pairs that are exchanged in the synthetic dataset.
    both words must have the same count in the dataset and disjoint neighborhoods
"""

import random as rnd
import itertools
import os
from tqdm import tqdm


def compute_candidates(_embedding):
    """
    Compute a set of five word pairs (five is chosen arbitrarily)
    Each pair appears equally often in the corpus
    Their neighborhood is disjoint
    The frequencies are drawn at random

    Arguments:
        _embedding {Word2Vec.KeyedVectors} -- instance of word2vec embedding

    Returns:
        candidates -- list of five word pairs
    """
    # store the counts as key and the list of words with this count as value in dictionary
    count = {}
    for word in _embedding.vocab:
        num = _embedding.vocab[word].count
        if num in count.keys():
            count[num].append(word)
        else:
            count[num] = [word]

    candidates = []

    while len(candidates) < 5:
        # minimal frequency for candidate is 100
        rand = rnd.randint(100, max(count.keys()))
        if rand not in count.keys():  # do words exist with this freq?
            continue
        elif len(count[rand]) == 1:  # do enough words exist?
            continue
        else:
            for cand_a, cand_b in itertools.product(count[rand], repeat=2):
                n_a = [w for w, d in _embedding.most_similar(cand_a)]
                n_b = [w for w, d in _embedding.most_similar(cand_b)]
                # check if word pair has disjoint neighborhood
                if len({i for i in n_a if i in n_b}) == len(n_b):
                    continue  # check other combinations
                else:
                    candidates.append((cand_a, cand_b))
                    break  # draw next random frequency
    return candidates


def replace_candidates(_candidates, _path):
    """
    Iterate through the corpus and replace all candidates
    is called from main function

    Arguments:
            _candidates {list} - list of candidate pairs as string
            _path {string} - complete path to original corpus
    """

    # Make sure target folder exists
    targetdir = _path+'_synthetic'
    if not os.path.exists(targetdir):
        os.makedirs(targetdir)
        print("Created target folder {}".format(targetdir))
    # get the index of the folder
    filelist = os.listdir(_path)
    print("Replacing candidates in {} files in source folder".format(len(filelist)))
    # Write the candidates to the README file (for later validation)
    # dont put the Readme in the corpus folder -> training
    with open('{}/../README'.format(targetdir), "w", encoding="utf-8") as writefile:
        for cand1, cand2 in _candidates:
            writefile.write('{} {}\n'.format(cand1, cand2))

    # Iterate through files and swap the candidates
    for i in tqdm(range(len(filelist))):
        flist = filelist[i]
        with open('{}/{}'.format(_path, flist), "r", encoding="utf-8") as readfile:
            text = readfile.read()
            dummy = ' asdfghjklqwerty '
            for cand1, cand2 in _candidates:
                # use a temporary string (probably) not appearing anywhere else
                cand1 = ' '+cand1+' ' #ensure we replace only whole words 
                cand2 = ' '+cand2+' '
                text = text.replace(cand1, dummy)
                text = text.replace(cand2, cand1)
                text = text.replace(dummy, cand2)
            with open('{}/{}'.format(targetdir, flist), "w", encoding="utf-8") as writefile:
                writefile.write(text)
    print('Created synthetic dataset at {}. Swapped word pairs in README file'.format(targetdir))
    print('Now running evaluation on synthetic-original data pair.')
