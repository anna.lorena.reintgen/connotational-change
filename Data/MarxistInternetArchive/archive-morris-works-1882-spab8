
      I think we all owe our hearty thanks to the Chairman for the extremely sympathetic
      and most eloquent speech which he made, as well as for his kindness in presiding
      here to-day. It encourages us much to know that a man of his education and culture,
      and general sympathy with all human progress, is so entirely with us in our work. I
      am also glad to see him here as a representative of the University of Oxford, which
      we have just been quarrelling with in our Report, and not without reason. I know
      very well there are many who, like him, are sorely grieved at the destruction which
      has gone on there, though they have not been able to prevent it in the teeth of the
      general Philistinism of the place. I think that some of our members who spoke on
      the subject of the possibility at some future day of a sort of control by the State
      of our public buildings, went somewhat off the question. I believe what we are
      thinking of is that State interference should be used rather when it was a question
      of pulling a building down altogether, or of preserving it in some form. There are
      cases when a building would not be pulled down if it could be used. That has been
      one of our great difficulties in endeavouring to persuade public bodies to allow
      threatened buildings to stand. They say, Such and such a building will not suit the
      purpose we wish to use it for, and we can find no other use for it. Such a
      difficulty it appears to me the State might solve by finding a use for the building
      and so preserving it. At present, public bodies feel that they must first of all
      consider the pecuniary interests entrusted to them, and that they have no authority
      to release them from this obligation. This in certain cases the State could do, to
      the great advantage of the arts. Once more I call upon you to give the heartiest
      thanks to the Chairman.
    
Speech Seconding a Vote of Thanks to the Hon. James Bryce MP. (1882).
    
      1. 9 June 1882: Before the Annual Meeting of SPAB held in the lecture hall
      of the Society of Arts, John Street, Adelphi, London. The Hon. J. Bryce, M.P., was
      chairman.
    
      1. [Untitled] in Society for the Proptection of Ancient Buildings. The Fifth
      Annual Meeting of the Society, (London 1882), pp. 46-47.
    
The reference to this piece of work in the Chronology

The William Morris Internet Archive : Works
