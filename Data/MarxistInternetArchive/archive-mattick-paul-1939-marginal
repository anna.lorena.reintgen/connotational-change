Paul Mattick 1939
Source: Living Marxism, April 1939;
Transcribed: by Adam Buick; 
Proofed: and corrected by Geoff Traugh, August 2005.Recently the editors of Common Sense [1] have once more dealt with the “unscientific
character” of Marxism by pointing out that:“Ricardo’s labor theory of value, taken over by
Marx and embellished with the theory of surplus value, was abandoned long ago
by all but Marxist economists, and a whole branch of ‘marginal
utility’ economics developed, of which Marx could know nothing ... that
even in the Soviet Union (so far as Five Year Plans go, if not at the
Marx-Engels Institute) marginal utility economics have displaced the useless
and misleading Marxian economics.”However, what is brought forward here as an argument against Marxism is in
reality only another confirmation of it. Certainly, the Russian
state-capitalism, in which class relations are continued, cannot employ the
Marxian science, for this science consists of nothing but the critique of those
selfsame capitalistic conditions, which characterize Russia and every other
capitalistic country. For the purpose of justifying the exploitation of the
workers, the inequalities of income, and the accumulation of capital that
exists there, the Marxian economic theories are certainly useless. What Marx
had said [2] of the science of
bourgeois economy — namely, that it reached its limits with Ricardo
because,“He consciously made the antagonism of class
interests, of wages and profits, of profits and rents, the starting point of
his investigation,”holds equally true for Russian economic “science.” The continued
class society forces Russian economic theory to embrace those ideological
weapons of bourgeois society which appears as economic theory, and to attempt
to destroy even that kernel of truth contained in Classical economy, which
served with Marxists as a basis of attack upon the whole capitalistic
society.The development of marginal utility economics is closely connected with the
difficulty of the proponents of the classical theory to confute Marxist
theories, as both the Classicists and the Marxists based their argument on the
same objective value concept. The marginal utility school arose in defense of
capitalism, and its apology consisted in the construction of a value concept
which justified the prevailing class and income differentiations. The existing
inequalities based on the exploitation of labor were explained as an
undefeatable natural law of diminishing utility. This theory,
as was so well stated by C. E. Ayres [3],“Only undertakes to demonstrate under any given
conditions of income distribution the automatic achievement of the maximum
total of human satisfaction: the greatest good of all. Even so, this
poor-little-rich-girl notion which proposes to balance the surfeit of the rich
against the precarious existence of the poor is so extravagantly complacent
that most economists have hesitated to give it clear and unequivocal
expression.”Though single concepts of this theory were adopted by economists of other
schools, nevertheless, as a general theory, it was slowly abandoned. The
Neo-Classicists, for instance, did not bother themselves any longer with
questions as to the desirability or the justification of the prevailing
economic system: they simply took for granted that it was the best possible
one, and merely tried to find means of making it more efficient, a condition
which forced them to restrict themselves, as far as market phenomena were
concerned, to mere price considerations. The value concept was displaced by a
cost-of-production theory, which the Neo-Classicists thought sufficient to
explain the existing division of wealth.However, the question of utility was raised anew in relation to the problem
of the allocation of resources in a socialist economy [4] and it was pointed out that even with an acceptance of
the labour theory of value, the question of demand must be dealt with.
It is clear that no society can prevail which entirely disregards the real
needs of its people; that production is impossible unless men are able to eat
and work.“Every child knows, too, that the mass of products
corresponding to the different needs require different and quantitatively
determined masses of the total labor of society. That this necessity of
distributing social labor in definite proportions cannot be done away with by
the particular form of social production, but can only change the
form it assumes, is self-evident.” [5]However, the question of the allocation of resources to meet demand and in
the interest of economy as it is raised in modern economic theory has no
connection with the simple and direct statement of Marx just quoted, but is
determined by class considerations based on a particular form in which the
union of labor and the means of production is accomplished.In Russia, as elsewhere, the means of production are not controlled by the
workers but are the monopoly of a special group in society. In the relations of
the workers to the means of production, no difference exists between a private
property society and a state-capitalist system. The position of the Russian
bureaucracy to its workers is exactly the same as that of the individual
entrepreneur to his. The first need of that bureaucracy is to safeguard its own
position in order to develop industry and agriculture. Whatever else this
bureaucracy may do, it has first of all to “plan” its own security,
and then to proceed to “plan” life for the rest of the population.
This is recognized not only by the present and supposedly
“degenerated” Russian bureaucracy, but was clear also to the
“founders” of the Russian state-capitalist system.“As a general rule,” Trotsky has said [6], “man strives to avoid labor.
The problem before the social organization is just to bring
‘laziness’ within a definite framework, to discipline it, and to
pull mankind together ... The only way to attract the labor power necessary for
our economic problems is to introduce compulsory labor service ... We can have
no way to Socialism except by the authoritative regulation of the economic
forces and resources of the country, and the centralized distribution of labor
power in harmony with the general State plan. The Labor State considers itself
empowered to send every worker to the place where his work is necessary. And
not one serious Socialist will begin to deny to the Labor State the right to
lay its hand upon the worker who refuses to execute his labor duty.”After the question of production is thus settled, the question of
distribution is easily solved.“We still retain, and for a long time will retain, the
system of wages,” Trotsky pointed out [7]. However, “Wages, in the form both of money and
of goods, must be brought into the closest possible touch with the productivity
of individual labor ... Those workers who do more for the general interest than
others receive the right to a greater quantity of the social product than the
lazy, the careless, and the disorganizers. Finally, when it rewards some, the
Labor State cannot but punish others — those who are clearly infringing
labor solidarity, undermining the common work, and seriously impairing the
Socialist renaissance of the country. Repression for the attainment of economic
ends is a necessary weapon of the Socialist dictatorship.”The control of production by a particular group in society carries with it
their control of distribution. The division of society into rulers and ruled as
deemed necessary by Trotsky and as exists in Russia requires, besides a
sufficient number of bayonets, an ideology which convinces those who are ruled
that their status is natural, unavoidable, and beneficial. Income
differentiations and, with this, the formation of additional group interests,
becomes an increasing necessity, and is accentuated still more by the political
need to preclude a unity of misery against the privileged in society. Because
Marxism could be employed only in opposition to such a state of affairs, it had
to be ignored, or emasculated in favor of evaluations supposedly based on
scarcity, utility, or demands; for behind such terms, not
only real but also assumed utility, scarcity, and demand can be hidden and
justified. The “utility” of the one or other social function or
labor is first of all the “utility” it has for the safeguarding of
existing class relations and its corresponding mode of production. Not social
needs will determine “utility,” but groups interests. The class
structure of society comes to light precisely in its need for such evaluations.
Just as little as the privileges of the capitalists results from their
“utility” but from the fact that they control the means of
production and are thus able to exploit the workers, so little does
“utility” explain the privileges of the Russian bureaucracy. Those
privileges are also based on the conditions of the control of the means of
production by the bureaucracy. A theory justifying class rule and exploitation
is necessary in Russia, and its acceptance of the defense theories of
capitalism does not, as the editors of Common Sense believe,
indicate the faulty character of Marxism, but its continued usefulness in the
class struggle of the Russian workers against their present masters.1. ‘Marx over
Europe’, Common Sense, September 1938, p. 4.2. K. Marx, Capital.
Vol I, p. 18.3. The Problems of Economic
Order, New York, 1938, p. 43.4. Oscar Lange, On the
Economic Theory of Socialism, Minneapolis 1938.5. The Correspondence of
Marx and Engels, New York, 1934, p. 246.6. L. Trotsky, Dictatorship
vs. Democracy, New York, 1922, pp. 133-142.7. Ibid., p.
149. 
Paul Mattick Archive
