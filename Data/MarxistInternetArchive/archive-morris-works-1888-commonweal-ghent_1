William Morris. Commonweal 1888
Title:
    “The Revolt of Ghent”,
    Part 1
Author:
William Morris
Source:
Commonweal,
        Volume 4,
        Number 130, 
        pp. 210
7 July 1888 
        (The first of seven parts.)
Transcribed by:
Ted Crawford
Proofing and HTML:Graham Seaman
The events of which an account is here given took place towards the close of the fourteenth century amongst a people of kindred blood to ourselves, dwelling not many hours journey (as we travel now) from the place where we dwell; and yet to us are wonderful enough, if we think of them.
Few epochs of history, indeed, are more interesting than this defeated
struggle to be free of the craftsmen of Flanders whether we look upon the
story as a mere story, a true tale, of the Middle Ages at their fullest
development, rife with all the peculiarities of the period, exemplifying their
manners and customs, the forms that their industry, their religion, their
heroism took at the time; or whether we look upon it, as we Socialists cannot
help doing, as a link in the great chain of the evolution of society, an
incident, full of instruction, in the class struggle which we have now
recognised as the one living fact in the world, since civilisation began, and
which will only end when civilisation has been transformed into something
else. Whether we look upon the Revolt of Ghent as a story of the past or as a
    part of our own lives and the battle which is not wasting, but, using
    them, it is one of the great tales of the world.One piece of good fortune also it has, is that, as Horace says, it has not
lacked a sacred poet. As the tale is here told, its incidents, often the very
words of them, are taken from the writings of one of those men who make past
times live before our eyes for ever. John Froissart, canon of Chimay in
Hainault, was indeed but a hanger-on of the aristocracy : he was in such a
position as would have prevented him on principle from admitting any good
qualities whatever in those people whom he was helping to oppress; but
class-lying was not the fine art which it has since become; and the simpler
habits of thought of Froissart’s days gave people intense delight in the
stories of deeds done, and developed in them what has been called epic
impartiality: added to which one domain for the cultivation of historical lies
was not available in the Middle Ages, since owing to the form feudal society
had then taken what we now call patriotism—i.e., national envy
and rancour—did not exist. Englishmen, Scotchmen, Fleming, Spaniard,
Frenchmen, Gascon, Breton, are treated by John Froissart as men capable of
valiancy, their deeds to told of and listened to with little comment of blame
or discrimination : and I think you will say before you have done with him that
he could even see the good side of the revolutionary characters of his time,
so long as they were not slack in noble deeds. The result of a low standard of
morals, you will say. Maybe; and indeed I have noticed that a would-be high
standard of morality is sometimes pretty fertile of lying, because it is so
anxious that every event should square itself to an a priori theory.
However that may be, there is the general epic impartiality of the mediæval
chronicler amidst all his mistakes and misconceptions.Now a word or two as to the political and social condition of Flanders in
the fourteenth century, and then without more to do I will get to my story and
introduce you to John Froissart, who has given me at least as much
pleasure as he did to any one of the lords, ladies, knights, squires and
sergeants who first heard him read.First very briefly as to the political position of the country. Lying as it
did between the growing monarchy or rather suzerainty of France and the
disjointed members of the “Holy Roman” empire, it was with the
former power that it had to deal. The rise of the great cities of Flanders and
Hainault, and the power they could not fail to acquire, made the feudal lord
of the country but a weak potentate, and he always had a tendency to lean on
France for support. The French king, on his part was ambitious of making the
Earl of Flanders his vassal, and the help he gave him against his rebellious
subjects had to be paid for by homage to the French Suzerainty, or at least by
promises of homage. France therefore was distinctly the enemy of the Flemish
people, though it was, when occasion served, the friend of the Flemish feudal
lord. France could also strike a blow at the prosperity of the country without
even putting an army in the field, by forbidding the export of wool, the great
necessity to the woollen-weaving which was the main industry of Flanders, and
this was done on several occasions.Therefore it was natural for the leaders of the Flemish people to turn
towards England as a support, both because there was a standing quarrel
between the feudal lords of England and France, and because England was the
wool-producing country of Europe. On the other hand, to an English king with a
quarrel on hand with a French one, the advantage of the Flemish alliance was
obvious enough and accordingly at the beginning of the great feudal war
between England and France we find our King Edward III. in firm alliance with
James van Artevelde, the leader of the Flemish people, or rather bourgeoisie,
treaties made between them as to the free passage of wool, and Queen Philippa,
godmother to the enfant child of the great Bourgeois, while the Earl of
Flanders was hanging about the French Court a disinherited lord.Now, as to the social condition of the Flemings. Manufacturing by
handicraft pure and simple, without division of labour, was carried by them
about as far as it could go ; and the gild-system was fully developed there,
accompanied by a complete municipal system, democratic and social as far as
matters within the association were concerned, though exclusive as regarded
outsiders. The great towns of northern Europe, it must be remembered, were not
originally “cities,” sovereign bodies with a definite polity
like those of the ancient classical world. The origin of them was the
agricultural district, the land that gave subsistence to the clan, all the
free men of which took part in the affairs of the community ; the first towns
were not as in Greece and Rome, the sacred spots of the tribal ancestor, but
pallisaded places where convenience had made the population thicker than in
other parts of the district. These as they grew kept their territory and
developed at last within themselves an aristocratic and oligarchic
government.But as these towns changed from being mere centres of an agricultural
population, into being places of resort for handicraftsmen and merchants, and
as the associations for the organisation of industry, that is the gilds, grew
up amongst the former, a new democratic feeling rose up which opposed itself
to the remains of the old tribal freemen, now become a mere exclusive
oligarchy, who considered the practice of handicraft a disgrace.The new democracy triumphed at last, and by the end of the thirteenth
century the gilds, the actual workmen, were the masters of the great towns;
under the feudal lords, however, to whom they owned fealty.Within the gilds themselves there could be no capitalists or great men,
because the rules of the gilds were framed to prevent the accumulation of
wealth in the hands of a few, the masters were master workers, and were kept
so by the rules aforesaid.I suspect, however, that there were remains of the old municipal,
aristocracy (the lineages, as they were called in Flanders) still in existence
in the towns, otherwise it would not be easy to account for the masterful
position of James van Artevelde, and others whom we shall meet with later on
in our story, who were certainly both wealthy and of importance, apart from
any office they might happen to hold.In Ghent also and elsewhere, notably at Bruges its rival, an aristocracy of
the crafts was forming, as is apparent in the fact of the jealousy between,
the greater and the lesser crafts,1 so that if the development of commerce joined with the
rise of bureaucratic monarchy had not supervened and swept away the power and
freedom of the towns altogether, the struggle between the municipal
aristocracy and the craftsmen would have been repeated in the fifteenth
century in another form.Meantime one thing to be noted which is specially interesting to us, and
that is the visible existence of strong Communistic feeling along with the
development of gild democracy.In the popular literature of the epoch one comes across passages whose
mediæval quaintness gives a pleasant sense of surprise and freshness to
aspirations and denunciations which are familiar enough to us Socialists
today, and, so to say, at once make us free of the brotherhood of the old
gildsmen. The two following centuries obliterated this feeling, or rather drew
a dark veil of misery and degradation over all the feelings of the
working-classes; but we now in our hope of better days soon to came can look
back cheerfully to the times when the craftsman citizen of the great towns had his hope also, which he hands over to us across the lapse of the drearier days.
( To be continued.)1.
The lesser crafts were the weavers and fullers, that is to say, the workmen of
the staple industry of the country. 
The William Morris Internet Archive : Journalism |  Works
