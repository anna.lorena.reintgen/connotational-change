
Published: Bulletin of the Provisional Bureau in Amsterdam of the Communist International, vol. 1, no. ?. February 1920.
Transcription/Markup: Micah Muer, 2017.
These differences can partly be traced back to the days of the rise of
Communism. The opposition in Germany during the war, against the government and
social democracy, had its origin in various centres and in various ways. K.
Liebknecht and Rosa Luxemburg (with Fr. Mehring) maintained an unflinching and
inexorable opposition against the war-policy, by means often of illegal
writings, and were therefore kept in prison most of the time. The "Spartacus"
group which they created constituted the extreme left wing of the "Unabhängige
Sozialistische Partei" (the U.S.P., which, under Haase and Kautsky carried on a
timid opposition in the spirit of radical socialism) and strove to bring about
revolutionary movements. In Bremen and Hamburg the "Internationale" sprang up
(organ "Arbeiterpolitik", editor: Knieff). The activities of this group tended
above all towards an education of the masses on revolutionary lines, so that it
waged a fierce contest against the wavering policy of the independents. In
Hamburg, Laufenberg and Wolffheim adopted the attitude of circumspect criticism
with regard to the world-problems of imperialism.After the German revolution, on the first conference in December 1918, these
various tendencies united ("Spartacus" had by that time seceded from the
"Unabhängigen") into the "Kommunistische Partei Deutschlands". Soon after this
the workers' action was violently suppressed by the Noske-troops, Liebknecht and
Rosa Luxemburg were murdered, the communists persecuted, and the party forcibly
reduced to the conducting of a secret, illegal propaganda. Gradually the
communistic papers were suppressed, only in Bremen the "Kommunistische
Arbeiterzeitung" (Laufenberg) managed to maintain itself. The policy propagated
by this paper came into conflict with that of the "Zentrale" (the executive of
the party), especially with regard to their attitude towards parliament and the
unions. At a secret conference in the end of October 1919, the "Zentrale"
proposed theses which were to define the tactics of Communism; whosoever voted
against them was to cease to be a member of the party. The opposition protested,
claiming that the conference had been taken unawares by the proposal of these
theses, so that the members were not prepared to pronounce on them. The minority
of the conference rejected them and were excluded from the party. To this
opposition belong the strongest sections: Hamburg, Berlin, Bremen, etc., it
comprises about half the number of members of the K.P.D. According to the
opposition these dealings of the "Zentrale" were a cunning trick to split up the
opposition, which, in the question of parliamentarism constituted the majority
of the party, and might have dismissed the "Zentrale". The "Zentrale" maintains
that this cleansing was needed, as the Hamburg and Berlin members had strayed
away from communism, and drifted into syndicalism, so that it was necessary to
draw a sharp line of demarcation. The opposition does not admit its expulsion,
nor has it formed a separate party yet, only a loose link. Many other sections
do not consider the decrees of the conference as lawful, and demand a new
conference. The "Zentrale" endeavours to separately bring round each of the
various parts of the opposition, and so far it seems likely to succeed. Since
the raising of the state of siege it has continued the issue of its daily paper
"Die Rote Fahne"; it disposes moreover of the funds of the party.The differences between these two tendencies are in part essential: they are
rooted on the one hand in the different origin of the groups from which the
party sprang, and on the other in the question as to the tactics which in the
present difficult state of affairs in Germany (economic ruin, power of the
Noske-guards, impotence of the workers), will lead to the revolution.In the present state of Germany the opposition is against participation in
parliamentary action. The "Zentrale" wishes to take part in the next
Reichstag-elections. The opposition wages a sharp contest against the
independent Social democrats of the U.S.P. The "Zentrale" wishes to join forces
with the independents (whom it considers almost as communists), after they shall
have shelved their conservative leaders. A main factor in the antithesis is the
attitude towards the big unions (the leaders of which, Legien, Schlicke, Bauer,
are the most powerful supporters of the Ebert-government, and some of them
ministers). The independents wish to dismiss these social-patriotic leaders, and
to substitute to them members of their own party. The opposition wishes to smash
the burocratic organization of the union, and to substitute to it a new form of
union on the basis of the industrial union instead of the craft-union. The
unions thus formed, however, were a success in few places only (in the
coal-basin of the Ruhr the miners' union affiliated itself to the feeble
syndicalist groups), but for the greater part they remained weak. At first the
"Zentrale" likewise recommended withdrawal from the big unions, not individually
however, but in groups, and after a previous vigorous opposition only; now it
recommends the formation of revolutionary groups of opposition, within the
unions. The opposition considers the industrial organizations including all
workers, irrespective of party-membership, as the firm basis for the political
power of the working class, and as the organs for the dictatorship of the
proletariat. The functions of the "Communist Party" will consist in discerning
the aims, in directing the masses, and in educating them on class lines but not
in governing.The "Zentrale" criticizes this attitude as implying a belief that by means of
a new form of organization it will be possible to avoid a revolution as the
forcible means of conquering the power; and furthermore as debasing the
"Communist Party" to the level of a harmless educational union.The "Zentrale" considers it to be the task of the "Communist Party" to call
the masses to the battle when the time has come; the dictatorship of the
proletariat is embodied in the dictatorship of the "Communist Party". The
opposition condemns this as a policy of artificially organized outbreaks, for it
is from the ranks of the masses that action must break out, and it is only then
that communism can come forward and take the lead. A revolutionary minority
cannot carry through the dictatorship of the proletariat: its victory would mean
no more than the dictatorship of a few leaders.The "Zentrale" says: the opposition is syndicalistic, because it rejects
parliamentarism; it considers the "Communist Party" as superfluous, and it
substitutes the federalism of industrial organizations for the rigidly
centralized revolutionary party. The opposition says: the principle of the
conquest of the political power by the proletariat, as well as the recognition
of the need for centralization of the proletarian state, divides us from
syndicalism whilst our conception of the functions of the industrial
organizations exactly coincides with that of the Russian soviet-system.Some confusion was occasioned in these antitheses, when the Hamburg leaders
of the opposition, Laufenberg and Wolffheim declared, in a manifesto to the
German people, that the general misery necessitated a speedy revolution in
Germany and that this revolution necessitated the tearing up of the Versailles
peace-treaty, which meant the renewal of war by the Entente against Germany; and
that on account of this, the Proletariat, whilst establishing a strict
class-dictature, should endeavour to conclude a new truce with the bourgeoisie
against the Entente. This dangerous tendency towards a "national bolshevism" was
soon vigorously opposed by comrades belonging to either wing of the party; the
groups constituting the opposition have not adopted the manifesto. 
Left Communism Subject Archive |
Pannekoek Archive
