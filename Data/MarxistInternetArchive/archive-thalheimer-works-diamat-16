 I would like to continue
with a survey of the main currents in the contemporary bourgeois philosophy of
Europe and America, but through limitations of space I must confine myself to a
brief characterization of one of these trends, namely, pragmatism. It is the
dominant school in America and has also had a powerful influence in England
and, to a lesser degree, in other European countries. I select this particular
school because it is the best known of the foreign world-views or philosophies
and because it has a particularly progressive, democratic, and unprejudiced
quality. It is therefore not so easy for the uninitiated to recognize that the
true character of this philosophy is reactionary and idealistic.The bourgeois philosophy of
Europe which followed classical philosophy (in Germany this would mean
philosophy subsequent to Feuerbach) still maintains
an extensive superficial existence. There is an immense amount of philosophical
literature in Germany and in other countries. Every university has one or more
professors of philosophy. But Feuerbach marks the beginning of the end of
bourgeois philosophy and of philosophy in general in the historical sense of
the word. What has appeared since then as bourgeois philosophy of one kind or
another must be termed philosophical poems, poems supported by concepts. These
have been more or less interesting historically, but they evince no scientific
progress. In fact, it will be discovered that all the various schools and sects
of bourgeois philosophy after Feuerbach revolve about just one problem, namely,
how bourgeois society and the capitalist order can best be defended against the
socialist revolution, how in some universal and fundamental way it can be
vindicated and supported, how the most powerful ideological enemy of the
existing order, dialectical materialism, can be most successfully staved off.
Besides fending off its enemy, dialectical materialism, the bourgeoisie must
strengthen its belief in its own order. These are actually the problems about
which the various schools of modern bourgeois philosophy revolve. They cope
with it through deception and by the demand for "unprejudiced
science." As a rule the perpetrators of this kind of philosophy are
completely unconscious of its objective. The capitalist order and everything that
belongs to it is the unexpressed and usually unconscious assumption and purpose
behind research - it is the natural matrix. This doesn't help the situation,
however; it only makes it more dangerous. To be sure, there are still many  individual accomplishments of a
scientific nature even in bourgeois philosophy. In this category belongs the
accumulation of material on the history of philosophy, the elaboration of
certain problems in logic, the mathematical development of logic, etc. But
these are only the last buds of a dying stalk. Bourgeois philosophy in its
various schools represents just such a dying stalk. One must not be deceived by
its extensive superficial existence. The philosophy of the Middle Ages, the 
scholastic, likewise had a wide influence, and possessed
schools, teachers, and literature galore. It too
made certain positive contributions. As a whole,  however, it was fruitless; it was tied down, the "hand-maiden" of the church, the
defender of the church's established dogmas. Modern bourgeois philosophy is no
less closely bound to capitalism than this scholasticism was bound to the
church and its dogmas. But what was open and understood in scholasticism is
carefully concealed and hidden by our modern scholasticism.Now a few more comments on the general nature of
post-war philosophy. Naturally, I refer to both Europe and America. The war
and, related to it, the beginning of the world revolution, very profoundly
disturbed bourgeois society. We therefore have a universal quest for spiritual
supports for bourgeois society, stronger and more potent supports than those
offered by earlier philosophy. We have a revival of the various forms of
metaphysics. The way was already prepared for this before the war, but after
the war it received a much sharper impetus. We have the creation of a
transcendental world of fantasy, a much greater concentration upon religious
ideas which is in part a regression to the crudest superstition - to
spiritualism, for example. There is also an intrusion of such transcendental
concepts and ideas into the natural sciences, especially those natural sciences
which deal with the phenomena of life. There is the doctrine of vitalism, for
example, the doctrine of life-force.From the point of view of the proletarian revolution or
a national revolution one can hardly regret that the bourgeoisie of the
principal countries abandon themselves in the name of philosophy to the crudest
superstition, to the most absurd religious fantasies and to the greatest
spiritual confusion. It is not our misfortune if the modern capitalistic
bourgeoisie snatch up the ideas of the Australian jungle. Only we must see to
it that these ideas are not carried over to the masses of people. We must help
the working masses free themselves from the various forms of bourgeois as well
as pre-bourgeois world-views, from both crude 
and
refined forms of religious fantasy. In this connection, a question might be
raised: how does it happen that there are still so  many  schools of philosophy in modern
times when the same problem, the same impulse, underlies them all? To this I
believe the answer must be: first, the various historical stages of bourgeois
society as a whole are involved; then there are the differences in class
relations between countries. Take England, Germany, America - the class
relations of these countries have their local peculiarities. In the third
place, in every country and at any given point in time we must take into
consideration the various groupings of the great and petty bourgeoisie, groupings
which find expression in this or that philosophical conception. And finally,
the ideological traditions of particular countries and the personal whims of
philosophizing individuals play an appreciable, if not decisive, role. But in
spite of great temporal and local differences between particular schools, the
universal counter-revolutionary and reactionary class character of the modern
American and European bourgeoisie expresses itself in a host of characteristics
which are common to all schools of bourgeois philosophy. Foremost is their
aversion and opposition to materialism, especially dialectical materialism, and
hence their fundamentally idealistic point of view, sometimes clear, sometimes
obscure, but always present. Another common and extremely characteristic
feature is the effort to restrict the scope and significance of reason and to
extend the province of free will, anarchy, and the unconscious, the province of
"irrationality." Since the light of reason reveals to the bourgeoisie
only the road to destruction, they prefer to shut or half shut their eyes and
give themselves up to more pleasant fantasies - for fantasies they truly are
when viewed with the clear eye of reason.I now
come to the school of pragmatism. This school or trend originated in America.
It then spread to England and Italy and, in lesser degree, to France and
Germany. It reflects the characteristic spirit of the American bourgeoisie.
Hence the democratic and pseudo-radical touch, as well as the distortion of
cause and effect, and the tendency towards commerce. Pragmatism is literally
the philosophy of commerce. The first impulse towards this philosophy came from
the American philosopher Peirce. In 1868 he wrote a short paper which can be
looked upon as the germ of pragmatism. But the well-known American psychologist,
William James, must be looked upon as the founder and leader of this school.
For a long period William James was a professor at Harvard University. His
father had been a theologian of a school which 
was
partial to spiritualism - a Swedenborgian. William
James
first taught as a natural scientist. His philosophy is a cross between
theological and natural-scientific concepts and methods in which theology has
become dominant over natural
science. It was the French philosopher Renouvier who gave to James the decisive
stimulus which led to the philosophy of pragmatism. In England the chief
exponent of pragmatism was a certain Schiller who for many years had been
professor at Oxford. In America the best known representative of the school is
now John Dewey, formerly of Chicago and latterly of New York. In 1919,
immediately after the war, he visited first Japan and then China, where he
propagandized for his doctrines, engaging in a higher sort of missionary
expedition in the interest of America and Americanism.We are now to investigate the relation of pragmatism to
the fundamental trends of philosophy. Pragmatism, as a philosophic trend, is
apparently very radical. Indeed it is sometimes called radical empiricism or
the radical theory of experience, and it claims to be superior to both idealism
and materialism. But this is a false pretension. Upon closer examination one
sees that what the pragmatists call experience, what they consider to be the
ultimate and the primary, is nothing but the idealists' ultimate, namely, the
mental; the pragmatists merely talk about it in terms of sensation and
emotion, that is, in terms of the simplest, most primary psychical functions,
whereas other idealists take higher psychical functions as their primary. They
maintain that in sensation and emotion the psychical and the physical compose
an inseparable unity and that the corporeal cannot be found except in union
with the mental. They accordingly deny the existence of an external world independent
of human sensations, ideas, or feelings. They carry this marvellous jugglery to
the point where they say: the relation of the mental to the corporeal is only a
sham problem, not a real one. Naturally, if the existence of a material world
independent of human consciousness
is reasoned away, there can no longer be
a problem of the relationship of such a world to human
thought. This utterly simple and staggering "solution" is merely a
sleight-of-hand by which the problem itself is made to disappear. In its
fundamental conception pragmatism is therefore idealism. The battle
which it wages against the idealism of other schools is actually only a sham battle. The true and sincere opponent of pragmatism - the openly avowed opponent - is materialism, and dialectical materialism in
ticular. The fundamental
conception of pragmatism shows an extremely close affinity with the conception
of Ernst Mach, the Austrian philosopher and naturalist, and with Avenarius:
the so-called school of empirio-criticism. That we have not done pragmatism an
injustice by calling it idealistic, is supported by the testimony of the
Encyclopedia Britannica, that great dictionary of the Anglo-Saxon world, which
says, in the article on William James, that he defends the idealistic position
from the empirical point of view. And the French historian of pragmatism, F. Leroux, characterizes
pragmatism as an empirical or experiential idealism.I should like to cite one more fundamental concept of
pragmatism. This is the fundamental concept of a "pluralistic universe.
It assumes that the
world consists of component worlds which have no connection with each other. I
need not labor the point that this concept is a nonsensical self-contradiction.
To be sure, it is not self-contradictory to postulate a world which is at the
same time a unity and a plurality, but to affirm a world, a universe, which is
a plurality without unity is plainly a meaningless contradiction. If one asks
oneself how a school of philosophy can achieve such palpable nonsense, one does
not have to seek far for the answer: the prototype of the world which consists
of parts having nothing to do with each other is the world of the high priests
of all schools, a world composed of the earthly vale of tears and the heavenly
hereafter which are utterly and absolutely separate and different from each
other. The "pluralistic universe" is merely a new "higher" label for this
ancient and insipid clerical nonsense. A further characteristic of pragmatism
is its concept of truth. For pragmatism there is no objective measure of truth.
Since it recognizes no reality external to the human mind, it can have no
touchstone for truth. According to pragmatism truth is what "works," what is useful. The
measure is thus subjective. The undefined subject who is the measure of truth
is not man in general but the bourgeois in particular and his particular ends.
The bourgeois mind governed by bourgeois interests is made the supreme judge
of truth. That this is very convenient for the bourgeoisie certainly cannot be
disputed.The purpose of all these maneuvers of pragmatism is the 
"scientific" salvation and vindication of the old religious nonsense. William James himself wrote a
sizeable book on The Will to Believe and another on religion experience in
which he tries to prove that every form of belief, no matter how insane,
contains some element of truth as long as it gives man a certain amount of
power and effectiveness. For William James the Christian religion in which he
was reared, is such an "effective" truth. For the African Negro it
may be a wooden idol studded with nails. The whole trick lies in calling
something "experience" that used to be known as belief or fantasy. William
James, for example, says that the visible world is a part of a more spiritual
universe from which it derives its meaning - a statement that immediately
reminds one of belief in ghosts. What William James passes off as religious
truth or experience is a conglomeration of the creeds of the hundred or more
Christian and non-Christian sects existing in America. It is the laboratory in
which the fantastic products of various religions and
sects are standardized into a normal or average bourgeois faith. If some sect
began to believe that the moon was green cheese and if this belief gave them
strength, then pragmatism would mix this ingredient into the general religious
brew.So much
for this American. I should now like to give you a sample of the English
pragmatist, Schiller. It may suffice to give you his own statements of the
contents of certain sections and paragraphs of his book Riddles of the Sphinx. Thus, one section is called "Man and his cause-God. . . . (a) As the
first cause, but only of the phenomenal 
world....
(c) As personal, (d) as finite, because only a finite God can be
inferred." In paragraph 24 of the same chapter he says: "God not = 'Nature,'
and hence 'Nature' can contain an element which resists God."
The "element" that "resists God" used to be called the devil
or Satan. Can one ask more of pragmatism than that it prove, in addition to the
existence of God, the existence of the Devil? Chapter 
12, Paragraph 2: Here we find the kingdom of heaven pragmatically described
as seems appropriate and plausible to the average mind of the English
philistine. In this paragraph we have "The ultimate aim of the process
(i.e., the development of the world) is the perfectioning of a society of
harmonious individuals." Paragraph 3: "If so, its starting point must
have been a minimum of harmony. This implies a pre-cosmic state when no
interaction, and hence no world, existed. It preceded Time and Change and does
not admit of further inquiries." Which is quite understandable, for the
things which happened when there was no time, no things and even no happenings
are of the same nature as the dragons of Chinese fables. Just as good is what
this pragmatist and teacher from that most famous English university, pious
Oxford, has to say about the end of the world. "The end of the world-process
is the attainment of perfect harmony or adaptation - the perfection and aim
of all the activities of life."How does this ultimate state appear?This
state is "distinguished by its metaphysical character from the becoming of
the time-process, a changeless and eternal state of perfect being." A wonderful
"state," where there is no change, no transformation, no time, and
yet everything is wonderfully perfect. One is forced to admit that in
comparison the Christian or Mohammedan paradise is backward, for in these
something still happens. There are music, dancing, and other
"happenings." The pragmatist paradise is prolonged into eternity and
filled with eternal boredom - an English Sunday whose bliss, as is well known,
consists in absolute boredom. "This includes a solution of all
difficulties, evil, time, divergence of thought and feeling, etc." All
people obviously think the same in this perfect state. How will they entertain
themselves there? This same Mr. Schiller wrote an address for the Pan-Anglican
Church Congress (1908) in which he says: "If all religions work, all are
true." He recommended pragmatism to the gathered clergy of England as an
especially good preservative for religion, as a better protection than
idealistic philosophy - the latest American patent, so to speak, for protecting
religion.With
this we leave the pragmatist Sunday preachers. I will simply add that the
American, John Dewey, is a bit more artful than these other pragmatists, but
that there is no fundamental difference between him and the others.In
conclusion, I should like to recommend some literature. Friedrich Engels'
little essay on Feuerbach offers the best beginning. This little book contains
a very clear and concise exposition of dialectical materialism, its
development and its relations to bourgeois
philosophy. Then another book by Engels: Anti-Dühring 
(Herr Eugen Dühring's Revolution in Science). So far this is the most comprehensive and forceful
presentation of dialectical materialism and its widest application to various fields. I cannot specify
any particular book by Marx; all his books are
written according to the method of dialectical materialism. Further, I might
mention the philosophical writings of Plekhanov. Then there is Lenin's book, Materialism
and Empirio-Criticism. A popular text on historical materialism has been written by Bukharin.
Further, I should like to call your attention to the writings of A. Labriola,
the deceased Italian Marxist, on historical materialism, as well as the
writings of Franz Mehring, who was the most important materialist historian.Naturally
I take for granted that you will not merely study dialectical materialism from
books, but that you have already turned to practical activity. This is
thoroughly consistent with the nature of dialectical materialism. Dialectical
materialism is born of revolutionary activity; it strives to afford general
guidance for revolutionary activity. Karl Marx once said: "The task of
philosophy [and by philosophy he meant materialism] is not to explain the world
anew, but to change it." No one who lives in a great revolutionary period
can remain merely a theorist.
Contents  | 
Chapter 15 - Ancient Chinese Philosophy II

Thalheimer Archive
