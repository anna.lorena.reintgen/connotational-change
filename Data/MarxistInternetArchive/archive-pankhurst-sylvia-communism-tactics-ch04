Version One, Communism and its Tactics by Sylvia Pankhurst
We have seen that the Soviets are destined both to provide the organisational
machinery of Communist society and to act as the instrument of the proletarian dictatorship during
the transitional period in which, whilst capitalism has been overthrown, the dispossessed owners
have not yet settled down to accept the new order. The Soviets may also conduct the fight for the
actual overthrow of Capitalism, though in Russia the power was actually seized by the Bolshevik
Party; then handed to the Soviets. 
Let us consider the essential structure of the Soviet, its particular
characteristic, wherein lies its special fitness to function as the administrative machinery of the
Communist community. 
The Soviet is constructed along the lines of production and distribution; it
replaces not merely Parliament and the present local governing bodies, but also the capitalists,
managerial staffs and employees of today with all their ramifications. The functional units of the
Soviets are the groups of workers of all grades, including those engaged in management in the
factory, the dockyard, the mine, the farm, the warehouse, the office, the distributive store, the
school, the hospital, the printing shop, the laundry, the restaurant, and the domestic workers in
the communal household, the street or block of dwellings. 
The generally accepted theoretical structure of the Soviet community is as follows: 
The Workshop Committee: comprising all the workers in the shop. 
The Factory Committee: comprising delegates from the Workshop Committees. 
The District Committee: comprising delegates from the factory or sub-district
committees of the workers in the industry, and from district committees of distributive workers
engaged in distributing the products of the industry. 
The National Committee: composed of delegates from district committees. 
District and Sub-District Committees: Delegates from district or sub district
committees of industries (including factories, docks, farms, laundries, restaurants, centres of
distribution, schools, domestic workers, parks, theatres, etc., workers in all branches of social
activity being represented). 
National Committee: comprising delegates of district committees of all industries
and works of social activity. 
Thus there is a dual machinery: 1. For the organisation and co-ordination of each
industry and social activity; 2. for the linking together of all industries and social activities. 
The network of committees of delegates which makes up the framework of the Soviets
and links the many productive groups, and also individual producers should not be regarded as a
rigid, cast-iron machinery, but as a convenient means of transacting necessary business, a
practical method of inter-organisation which gives everyone the opportunity of a voice in social
management. The members of a community are dependent upon each other. The cotton spinning mill is
operated by a number of groups of workers practising various crafts. The workers in the spinning
mill are dependent for the execution of their work on the cotton growers, the railwaymen, the
mariners, and the dockers, who provide them with the raw material of their trade. They are
dependent on machine makers, miners, electricians and others for the machinery of spinning and the
power to run it, and on the weaver, the bleacher, the dyer, the printer, the garment worker and
upholsterer to complete the work they have begun. In order that the spinners may do their work they
are also dependent on builders, decorators, furniture makers, food producers, garment makers, and
innumerable others whose labours are necessary to maintain them in health and efficiency. 
At present it is the employer who directs, the merchant who co-ordinates and
distributes social production. When capitalism is destroyed another medium of direction,
co-ordination and distribution must be discovered, the productive processes must not fall into
chaos. The Soviets will supply the necessary medium of co-ordination and direction; but they must
become a medium of convenience, not of compulsion; otherwise there can be no genuine Communism. 
In Russia the Soviet constitution has only been very partially applied, and has
not been theoretically regular in structure, and is still constantly subject to large
modifications. 
The Russian Soviets had not been created in advance in preparation for the
revolution of March, 1917: they sprang into life in the time of crisis. They had arisen in the
revolution of 1905, but had died away at its fall. The March, 1917, revolution only created Soviets
in a few centres, and though their number grew and was added to by the November Bolshevik
revolution, even yet the network of Soviets is incomplete. Kameneff, reporting on this question to
the seventh all-Russian Congress of Soviets in 1920, stated that even where Soviets existed their
general assemblies were often rare, and when held frequently only listened to a few speeches and
dispersed without transacting any real business. 
Nevertheless, the Soviet government had claimed that the number of Soviets
actually functioning has grown continuously; yet it freely admits that the Soviets have taken
neither so active nor so responsible a part as they should in the creation and management of the
new community. Russia’s “new economic policy” of reversion to capitalism strikes at the
root of the Soviet idea and destroys the functional status of the Soviets. 
Russia’s special difficulties in applying the Soviet system were inherent in the
backward state of the country which had only partially progressed from feudalism into capitalism.
In industry the small home producer still accounted for 60 per cent of Russia’s industrial
production. In agriculture the peasants had not yet been divorced from the land as is the case in
England, where we have long had a completely landless class of rural workers. In Russia the ideal
of the land worker was to produce for himself on his own holding and to sell his products, not to
work in co-operation with others. The Russian peasants, vastly outnumbering the rest of the
population, were all but unanimous in their demands. Those who had no land were determined to get a
piece for themselves, and those who had a little piece of land wanted more. Though their
individualism was tempered by the old custom of periodically re-dividing the land and other village
traditions, the peasants were an influence against Communism. Nevertheless, their ancient village
council, the Mir, a survival from the period of primitive Communism, had somewhat prepared them for
the Soviets. 
In the scattered village communities the occupational character of the Soviet is
apparently somewhat submerged in the territorial; yet all the subsidiary crafts of the villages are
attendant on the great industry of agriculture. Ties of common interest and mutual dependence,
which are the life-blood of the Soviet, are clearly apparent between the land workers and the
various craftsmen of the village. The blurring of the occupational character of the village Soviet
does not detract from its function of an administrative unit in harmony with the actual conditions
of the country. On the other hand, the fact that the town Soviets could not supply it with the
industrial products it needed, by weakening the link of mutual usefulness, making the usefulness
merely one-sided, removed the natural impetus of the Soviets of the villages to link themselves for
utilitarian reasons with the Soviets of the towns. Production by individual producers who are
competing with each other creates sources of conflict which are antagonistic to the Soviet. The
strongest and most useful Soviet must always be that which is formed of those who are working
together and who realise at every turn that they are dependent on each other. The necessity for the
Soviet becomes more pronounced and its work more varied the more that work is carried on in common
and the more closely the lives of the people are related to each other. Mankind is gregarious; the
degree of gregariousness in human beings is partly dependant on material conditions, partly on
inclination (which is doubtless largely, if not wholly, the slow product of long environment). As
humanity secures a completer mastery over matter, individual choice as to how life shall be spent,
becomes broader and more free; science will more and more enable desire to determine the degree of
industrial concentration. Our civilisation has perhaps nearly reached the limit of the tendency to
gather together ever greater and greater numbers of workers, performing some tiny mechanical
operation as attendants to machinery. Perhaps the future has in store for us an entirely opposite
development. That would not effect the fact that the Soviet must find its most congenial soil in a
society based on mutual aid and mutual dependence. 
In the industrial centres where it might have been expected that the occupational
basis of the Soviet would have been adhered to, the structure of the Russian Soviets was irregular
from the theoretical standpoint. The Soviets, instead of being formed purely of workers in the
various industries and activities of the community, were composed also of delegates of political
parties, political groups formed by foreigners in Russia, Trades Councils, Trade Unions and
co-operative societies. Pravda of April 18th, 1918, published the following regulations for the
Moscow Soviet elections:— 
“Regulations for Representation.
“Establishments employing 200 to 500 workers, one representative; those employing over
500, send one representative for every 500 men. Establishments employing less than 200 workers,
combine for purpose of representation with other small establishments.
 “Ward Soviets send two deputies, elected at a plenary session.
 “Trade Unions with a membership not exceeding 2,000, send one deputy; not exceeding
5,000, two deputies; above 5,000, one for every 5,000 workers, but not more than ten deputies for
any one union.
 “The Moscow Trades’ Council sends five deputies.
 “Political parties send 30 deputies to the Soviet: the seats are allotted to the parties
in proportion to their membership, providing the parties include four representatives of industrial
establishments and organised workers.
 “Representatives of the following National non-Russian Socialist parties, one
representative per party, are allotted seats:—
 (a) “Bund” (Jewish).
 (b) Polish Socialist Party (Left).
 (c) Polish and Lithuanian Social Democratic Parties.
 (d) Lettish Social Democratic Party.
 (e) Jewish Social Democratic Party.” 
The intention in giving representation to these various interests was, of course,
to disarm their antagonism to the Soviet power and to secure their co-operation instead; but the
essential administrative character of the Soviets was thereby sacrificed. Constituted thus they
must inevitably discuss political antagonisms rather than the production and distribution of social
utilities and amenities. 
The industrial unions, economic councils and co-operative societies which have
been a feature of Soviet Russia (the two former having representation in the Soviets) have no
place, because they have no reason for existence, under an efficient Soviet system, in which they
would be absorbed into the occupational Soviets and indistinguishably fused with them. 
Industrial unions can have no reason for existence if the Soviets are fulfilling
efficiently their proper function as the administrative machinery of the Communist community, for
the Soviets should cover the same constituencies as the industrial unions. The industrial unions
will only exist so long as there is either a conflict between the workers and the Soviets (which
are theoretically the organs of the workers), or in case the Soviets are failing to administer
industry or administer it efficiently. The very existence of the Industrial Union, unless it be
merely a social club, denotes an antagonism between the members of the union and those who are
administering industry; unless, on the other hand, the Soviets are failing to administer industry
and the unions are formed for that purpose. In Russia, as a matter of fact, the continued existence
of the industrial unions is due to the fact that there it antagonism between the workers and those
who are administering industry. In a theoretically correct Soviet community the workers, through
their Soviets, which are indistinguishable from them, should administer. This has not been achieved
in Russia. 
Co-operatives have no place in a genuine Soviet community. If they are
distributive organisations purely, they should be the distributive branches of the industrial
Soviets. If they are organs of buying and selling, they are survivals of capitalism and must
disappear under Communism. If they are associations of producers they can only differ from
industrial Soviets in so far as they exact payment in cash or kind for their produce instead of
distributing it freely. In so far as they exact payment or practice barter, they have no place in a
Soviet community. 
The curious overlapping patchwork which has hitherto made up the Russian Soviet
system should by no means be slavishly copied. The Russians themselves have emphasised that.
Nevertheless, the recent tactics which they have induced the Third International to adopt do not
indicate that they have a clear perception that a highly organised industrial community may build
the new Communist order on the theoretically correct foundation of the occupational Soviets.  
Contents |
Sylvia Pankurst Archive