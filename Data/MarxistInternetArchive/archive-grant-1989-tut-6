Previous chapters have dealt with post-war
developments in the advanced capitalist countries as a whole. The
material in Chapter Two illuminated the role played by the
leaderships of the social democratic and Stalinist parties in
Western Europe, ensuring that capitalism was able to survive the
critical early years after the war. This provided the political
foundation for the long post-war upswing, dealt with in Chapter
Five.In the debates about likely international
developments - whether the post-war economy was heading for slump
or boom, how long the post-war boom would last, whether or not
capitalism had learnt to 'overcome' cyclical crises, and so on -
Ted Grant often drew on the immediate experience and the
statistical data of political and economic developments in
Britain, for obvious reasons. In these cases, the intention was,
nevertheless, to illustrate those processes that affected the
advanced capitalist countries as a whole.But in addressing activists in the British
labour movement it is also essential to point to those features
and characteristics which, among the advanced countries, are
peculiar to British capitalism. Within the general crisis
of world capitalism, in other words, consideration has to be
given to the special crisis of British capitalism.In the years after the first world war, the
writings of Leon Trotsky, and especially his masterpiece, Where
is Britain Going?, had already laid bare the contradictions
of British development. From having been the first and easily the
most powerful of the industrial nations, by 1914, Britain's
primacy was threatened by American and German imperialism. The
first world war effectively ended Britain's pre-eminent position,
placing her in hock to the fast-expanding imperialism across the
Atlantic. This relative decline continued unabated during the
inter-war years.After 1945, although once again emerging from
war as a nominal 'victor', the British economy continued its
slide compared to America, later to Japan and West Germany, and
later still to almost every other major capitalist economy. It
was only the strength of the post-war boom - affecting all the
capitalist countries - that increased the British economy (and
living standards) in absolute terms, and thus for a whole
period disguised a catastrophic relative decline. This
collapse of British industrial competitiveness, on the world and
even the home market, will mean titanic political convulsions in
a period of world economic stagnation. If such events have been
delayed, it has only been because of the very temporary boost
given to the economy by North Sea Oil.The extracts in this chapter are only a tiny
sample of Ted Grant's writings that deal specifically with
political and economic developments in Britain. They are an
indispensible study-guide to any Labour Party or trade union
members seriously examining the course of events since the war,
and especially during the Labour governments of 1945-51, 1964-70
and 1974-79. The first item is an extract from a resolution
presented on behalf of the leadership to the RCP conference in
1946. Like the other documents dealing with Western Europe as a
whole, it corrected the previous perspective of the RCP, of an
immediate economic crisis after the war. 'We anticipated that
British imperialism would be faced with a crisis as soon as the
war ended. However, the concatenation of circumstances has served
to screen the disastrous results of the war for Britain'The resolution went on to outline a perspective
for British capitalism of 'relative stability', which, at a later
stage, would give way to 'a catastrophe greater than she has
experienced in the whole of her history.' As we have already
pointed out, the RCP leadership around Ted Grant were involved in
an intense debate just to demonstrate to alleged Marxist
'theoreticians' that there was a boom of any kind in the
immediate post-war years. But what was not apparent at that time
was that the recovery would extend to become a prolonged upswing,
beginning in the late 1940s and lasting two and a half decades.
It is for that reason that the resolution draws a clear
distinction between the short-term perspective of growth and the
long-term perspective of decline and crisis, due to the chronic
weakness of British capitalism.The analysis of the 1945 Labour government is
also very penetrating, showing the pressure of the labour
movement, in forcing through reforms and nationalisations, but
also showing the limitations of a government still essentially
based on the framework of capitalism'no fundamental
measures against capitalism are being taken by the Labour
government. We are in a classic period reformist illusions - a
reformist government coming to power at a period of economic
boom.'Pointing to the massive compensation, the
bureaucratic control and the fact that only run-down industries
were being taken over, it describes the nationalisations as:"a compromise with the bourgeoisie as a
wholeas the best method of bringing about the necessary
measures of rationalisation and placing the burdens on the
shoulders of the masses. By means of state rationalisation they
hope to gain efficient and cheap coal, electricity, steel, fuel
and transport, in order more effectively to compete on the world
market."The RCP perspective of a collapse of the
British economy, after the initial post-war recovery, was also
linked to the question of the revival of fascism.This was particularly true because there were
already signs, by 1948, of a certain degree of disillusionment
with the Labour government, especially among sections of the
middle class. In by-elections throughout that year, the
percentage vote for the Labour Party was down and the Tories'
vote was increased, largely due to abstentions by former Labour
supporters.In a pamphlet entitled, The Menace of
Fascism, What it is and How to Fight it, written by Ted
Grant, the RCP explained the social basis of fascism, as a mass
movement based on the middle-class and set in motion by the
capitalist class to smash the labour movement. Faced with the
danger of social revolution and the loss of power, the British
capitalists, no less than their European counterparts, would be
prepared to mobilise and finance fascist gangs to atomise the
workers organisations.The pamphlet describes how the British
capitalists were sympathetic to Hitler and Mussolini before the
war, and how they supported the nascent fascist movement in
Britain around Oswald Mosley. Mosley himself, with the recent
history of the war and his association with nazism, may not have
corresponded to the specific needs of the ruling class in a
crisis. But the form is secondary to the content. Even if Mosley
was not to be the model then some other vehicle, more in keeping
with 'British' tradition would be found to develop a movement of
reaction.The warnings of the Marxists in 1948 about the
dangers of fascism contrasted to the lullabies sung by the
workers' leaders and the 'liberality' of the Labour ministers who
allowed Mosley's new 'Union Movement' to meet and organise. The
attitude of the right wing then is not much different to their
ostrich-like behaviour forty years on: faced with virulent
fascist thuggery, a common response is, 'ignore them and they'll
go away'.The extracts of the RCP pamphlet published here
are those that concentrate on the role of British capitalism in
giving political support to German nazism and Italian fascism
before the war, and on the possible development of reaction in
Britain. Large sections of the original, dealing with the rise of
these counter-revolutionary movements in detail and drawing
extensively on Trotsky's writings, are unfortunately left out for
reasons of space.The third item is a discussion paper on Perspectives
for Britain, written in 1977. There are literally scores of
articles, speeches, documents and notes written by Ted Grant on
political and economic developments in Britain throughout this
period, but the 1977 document adequately sums up the general
position of British capitalism in its long and debilitating
decline. With a mass of statistical evidence in relation to
productivity, investment, manufacturing exports, expenditure of
health, education, welfare, etc etc, it catalogues Britain's
decline relative to the other major capitalist powers.After the election of the 1974 Labour
government, the Labour leader, Harold Wilson, (followed later by
James Callaghan) persuaded the trade union leaders to accept a
form of wage restraint, the so-called Social Contract. In the
interests of 'improving the economy', the rank and file of the
trade unions accepted this, if reluctantly, even though it would
result in a 'temporary' decrease in living standards. But after
three years of wage erosion, in addition to cuts in public
expenditure, there was no significant improvement in the economy
and workers were not prepared to tighten their belts any further.
The document anticipated the explosion of anger building up
within the ranks of the trades unions:"It will be impossible for any length of
time for trade union leaders to hold back their membersif
the union leaders will not lead official national strikes, over a
period there will be a rash of unofficial strikes in many
areaswhat happened after similar periods of 'restraint'
will be repeated on a higher scale"The disillusionment in the Labour government
and the frustrations of organised workers did in fact burst into
the open, as predicted, in the 'winter of discontent' in 1978-79,
prior to Labour's defeat in the general election.All the main features of the Tories' government
since 1979 were forecast in this discussion document: the assault
on the trade unions, the drive to slash public expenditure, the
emphasis given to finance capital at the expense of industrial
capital, and even the growing splits within the Tory Party. As a
consequence of the Thatcher policies, there have been titanic
clashes with the unions in mining, steel, printing, and so on.
These have been the most bitter struggles since the war, the
miners' strike, for example evoking what the capitalist press
described as an 'insurrectionary' mood in parts of Yorkshire.The furious reaction of the working class to
the onslaught of the Tories has not led, as was originally
expected, to an early collapse of their government. Other,
unpredictable factors have intervened, like the Falklands war in
1982, the longevity of the world boom since 1982 (upon which the
British economy has been able to ride) and the abject weakness of
the trade union leaders in the face of attacks on workers'
rights. These temporary postponements notwithstanding, the
fundamental theme outlined in the document, that Britain is
entering the stormiest period of her history, will be well
demonstrated in the coming years.It should also be noted that the 1977 document
modifies some of the fundamental perspectives worked out at an
earlier period. In the 1946 resolution already mentioned, the
Communist Party was still considered to be a serious force in
British politics. 'In spite of set-backs for the CP', it argued,
'at a time of crisis for the Labour government the swing of large
sections of workers to the Communist Party, as a temporary phase,
is inevitable.'But as the later document makes clear, this
original prognosis has had to be considerably modified because of
the complete degeneracy of the CP, to become little more than a
midle-class sect. Even the dwindling support for the CP among
trade union activists, worth a mention in the mid-70s, is hardly
a factor a decade later.Similarly, the perspective for the development
of fascism has been qualified. At the time the RCP produced its
pamphlet on the question, it was thought that there was the
possibility of a fascist movement being sponsored and supported
by the ruling class as a counter-weight to the labour movement.But the post-war experience of Italy, Greece
and Chile shows that the social basis for fascism has been
historically weakened by the decreased social weight of the
middle classes, not least in the advanced capitalist states.
Parallel to this, the power, strength and cohesion of the working
class have been enhanced, and a mass movement of fascism could
only develop after a whole series of major defeats for the
workers' organisations.In addition to this, the ruling class itself is
less inclined than before to hand power over to unpredictable and
uncontrolled movements of the 'enraged petit-bourgeois', after
the experience of Hitler and Mussolini. Where there has been a
turn to reaction in the modern epoch, there has been a tendency
for the capitalist class to lean on the tops of the armed forces
- for them a more reliable prop - to effect a military coup
against the labour movement. In these circumstances, the noisy
but largely impotent fascist movements are cast in the 'chorus',
rather than in the leading role. Even then, the capitalist class
would only turn to military-police regimes as a last resort. Contents Ted Grant Archive