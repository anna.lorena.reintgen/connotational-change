Jenny Marx-Longuet 1871
Written: in English in September 1871;
First Published: in Woodhull & Claflin's Weekly, 21 October 1871.
Marx sent a cover letter to New York's Woodhull & Claflin's Weekly,  an early feminist periodical, – which is presented below.Mesdames:

I have the honor to send you, for insertion in your Weekly
– if you judge the contribution sufficiently interesting for your readers
– a short relation of my daughter Jenny on the persecutions she and her
sisters, during their stay at Bagneres de Luchon (Pyrennes), had to undergo
at the hands of the French Government. This tragico-comical episode seems
to me characteristic of the Republic-Thiers....

I have the honor, Mesdames, to remain,

Yours most sincerely,


Karl Marx
September 23, 1871Madame:

The following private letter (originally written to a friend)
may serve the public interest, if by means of it some light is thrown upon
the arbitrary proceedings of the present French Government, who, with supreme
contempt for personal security and liberty, do not scruple to arrest foreigners,
as well as natives, on altogether false pretenses:

Monsieur Lafargue, my brother-in-law, his wife and children, my
youngest sister, and myself, had spent the months of June and July at Bagneres
de Luchon, where we intended remaining until the end of September. I hoped,
by a prolonged stay in the Pyrennes, and by a daily use of the mineral
waters for which Luchon is famous, to recover from the effects of a severe
attack of pleurisy. Mais dans la Republique-Thiers l'homme propose et
la police dispose. On the first or second day in August, M. Lafargue was informed
by a friend that he might daily expect a domiciliary visit of the police,
when, if found, he would surely be arrested, on the pretext that he had
paid a short visit to Paris during the time of the Commune, had acted as
emissary of the International in the Pyrennes, and last, but not least,
because he is the husband of his wife, consequently the son-in-law of Karl
Marx. Knowing that under the present government of lawyers the law is a
dead letter, that persons are continually locked up, no reason whatever
being assigned for their arrest, M. Lafargue follows that advice given
him, crosses the frontier, and settles down at Bosost, a small Spanish
town. Several days after his departure, on the 6th of August, Madame Lafargue
[Laura], her sister Eleanor, and I visit M. Lafargue at
Bosost. Madame Lafargue, finding her little boy is not well enough to leave
Bosost on the same day (she was very anxious on the child's account, having
lost his brother a few days before), resolved to remain with her husband
for a day or two. My sister Eleanor and I therefore returned alone to Luchon.

Without accident we succeeded in getting along the rugged Spanish roads,
and safely reached Fos. There the French customhouse officials ask us the
usual questions and look into our carriage to see whether there are any
contraband goods. As we have nothing but our cloaks with us, I tell the
coachman to drive on, when an individual – no other than the Procureur
de la Republique, M. le Baron Desgarre – steps forward, saying: “In the
name of the republic, follow me.” We leave our carriage and enter a small
room, where we find a forbidding-looking creature – a most unwomanly woman
– waiting to search us. Not wishing to let this coarse-looking person
touch us, we offer to take off our dresses ourselves. Of this the woman
will not hear. She rushes out of the room, whither she soon returns, followed
by the Procureur de la Republique, who in the most ungentlemanly manner
thus apostrophizes my sister: “If you still will not allow this woman to
search you, I shall do so.” My sister replies: “You have no right
to come near a British subject. I have an English passport."

Seeing, however, that an English passport does not inspire M.
le Baron Desgarre with much respect, for he looks as though he were in
good earnest, ready to suit his actions to his words, we allow the woman
to have her way. She unpicks the very seams of our dresses, makes us take
off even our stockings. I fancy I can still feel her spiderlike fingers
running through my hair. Having only found a newspaper on me and a torn
letter on my sister, she runs with these to her friend and ally, M. le
Baron Desgarre. We are reconducted to our carriage – our own coachman,
who had acted as our “guide” during our whole stay in the Pyrennes, and
had grown much attached to us, is forced away, replaced by another coachman,
two officers are installed in the carriage opposite us, and thus we are
driven off, a cartful of customhouse officers and police agents following
us. After a time, finding, no doubt, that after all we are not such very
dangerous characters, that we do not make any attempts to murder our sentinels,
our escort is left behind and we remain in the charge of the two officers
in the carriage. Thus guarded, we are driven through village after village,
through St. Beat, the inhabitants of which comparatively large town collect
in crowds, evidently taking us to be thieves, or at least smugglers.

At 8 o'clock, thoroughly tired out, we arrive at Luchon. cross the Quinconces,
where hundreds of people are assembled to listen to that band, it being
Sunday and the height of the season. Our carriage stops before the hotel
of the Prefect, M. le Comte de Keratry. That personage not being at home,
still guarded, we are kept waiting before his door for at least half an
hour. At length orders are given for us to be taken back to our house,
which we find surrounded by gendarmes. We at once go upstairs, wishing
to refresh ourselves by washing our faces (we had been out since 5 o'clock
in the morning), but a gendarme and an agent in plain clothes follow us
even into our bedroom, we return to the drawing room, unrefreshed, to await
the arrival of the Prefect.

The clock strikes 9, 10; M. de Keratry has not come – he is listening
to the band on the Quinconces, and, we hear, is determined to stay
until the last chord of the music has died away. Meanwhile, quantities
of mouchards drop in, they walk into the rooms as
if it were their own and make themselves quite at home, settling down on
our chairs and sofa. Soon we are surrounded by a motley crowd of police
agents, which devoted servants of the Republic, it is easy to see, have
served their term of apprenticeship under the Empire – they are masters
of their honorable calling.

They have no recourse to impossible tricks and dodges to inveigle us
into a conversation, but, finding all their efforts to do so are vain,
they stare at us as only “professionals” can stare, until, at half-past
ten, the Prefect puts in an appearance, flanked by the Procureur General
M. Delpech, the Juge d'Instruction, Juge de Paix, the Commissaries of Toulousse
and Luchon, etc. My sister is told to step into an adjoining room, the
Commissaire of Toulousse and a gendarme accompany her. My interrogatory
commences. I refuse to give any information concerning my brother-in-law
and other relatives and friends. With regard to myself I declare I am under
medical treatment, and have come to Luchon to take the waters. For more
than two hours M. de Keratry by turns exhorts, persuades, and at length
threatens me, that if I choose to persist in my refusal to act as a witness,
I shall be looked upon as an accomplice.

“Tomorrow,” he says “the law will compel you to give your deposition
on oath; for, let me tell you, M. Lafargue and his wife have been arrested."
At this I felt alarmed, because of my sister's sick child.

At length, my sister Eleanor's turn comes. I am ordered to turn my back
while she speaks. An officer is placed in front of me lest I should attempt
to make some sign. To my annoyance I hear my sister is being led by degrees
to say yes or no to the numberless questions put to her. Afterward I found
out by what means she had been made to speak. Pointing to my written declaration,
M. de Keratry (I could not see his gestures, my back being turned) affirmed
the contrary of what I had really said. Therefore, anxious not to contradict
me, my sister had not refuted the statements said to have been made by
me. It was half-past two before her examination was ended. A young girl
of sixteen, who had been up since 5 a.m., have travelled nine hours on
an intensely hot day in August, and only taken food quite early at Bosost,
cross-examined until half-past two in the morning!

For the rest of the night the Commissaire of Toulousse and several
gendarmes remained in our house. We went to bed, but not to sleep, for
we puzzled our heads how to get a messenger to go to Bosost to warn M.
Lafargue, in case he had not yet been arrested. We looked out of the window.
Gendarmes were walking about in the garden. It was impossible to get out
of the house. We were close prisoners – not even allowed to see our maid
and landlady. On the following day, landlady and servants were examined
on oath. I was again questioned for more than an hour by the Procureur
General, M. Delpech, and the Procureur de la republique. That tongue-valiant
hero, M. le Baron Desgarre, read long extracts to me, pointing out the
penalties I am liable to incur by persisting in my refusal to act as witness.
The eloquence of these gentlemen was, however, lost on me. I quietly but
firmly declared my resolution not to take the oath, and remained unshaken.

My sister's examination only lasted a few minutes this time. She also
resolutely refused to take the oath.

Before the Procureur General left us, we asked for permission to write
a few line to our mother, fearing the news of our arrest might get into
the papers and alarm our parents. We offered to write the letter in French,
under the very eyes of M. Delpech. It was only to consist of a few sentences,
such as we are well, etc. The Procureur refused our request, on the pretext
that we might have a language of our own; that the words – we are well
– might convey some hidden meaning.

These magistrates outdid Dogberry and Verges. The following is another
instance of their utter imbecility. having found, as our maid told us,
a quantity of commercial letters belonging to M. Lafargue, in which reference
was made to the exportation of sheep and oxen, they exclaimed, “Oxen, sheep,
intrigues, intrigues; sheep - Communists, oxen – Internationalists."

For the remainder of that day and night we were again committed
to the care of several gendarmes, one of whom even sat opposite us while
we were dining.

On the following day, the 8th, we had a visit from the Prefect and a
person whom we supposed to be his secretary. Of this interview a most inaccurate
and fantastical account appeared in France, and was from thence
transferred into a great number of other papers. But to return to the Prefect.

M. de Keratry, after making a very lengthy preface, informed us most
blandly that the authorities had been mistaken; that it had been found
that there was no foundation for the charge made against M. Lafargue, who
was innocent, and therefore at liberty to return to France. “As for your
sister and yourself,” said M. de Keratry, thinking, I suppose, that a bird
in the hand is worth two in the bush, “there is much more against you than
against M. Lafargue” (thus we were being suddenly transformed from witnesses
into accused), “and in all likelihood you will be expelled from France.
However, an order from the government for your liberation will come in
the course of the day.” Then, assuming a paternal tone, he said, “Anyhow,
let me advise you to moderate your zeal in the future – pas trop de
zele!” Upon which the supposed secretary said abruptly, “And the International
– is the association powerful in England?” “Yes,” I answered, “most powerful,
and so it is in all countries.” “Ah,” exclaimed M. de Keratry, “the International
is a religion!” Before he made his exit, M. de Keratry once more assured
us, on his word of honor, that Paul Lafargue was free, and asked us at
once to write to Bosost to tell him so, and to invite him to return to
France. Now I fancied I could see the red ribbon of the Legion d'Honneur
adorning the buttonhole of de Keratry, and as I have a notion that the
honor of the the Knights of the Legion d'Honneur must be something very
different to the honor of common mortals, I thought it best to be prudent,
and so instead of advising M. Lafargue to return to Luchon, I intended
to do the contrary, and begged of a friend to send him the mens wherewith
to travel further into Spain.

Followed about by our shadows, the gendarmes, we waited in vain for
the promised order of our release. At 11 o'clock at night, the Procureur
de la republique walked into our room; but instead of bringing us the order
of our liberation, M. Desgarre asked us to get ready a trunk and to follow
him to “une maison particuliere.”  I knew this proceeding
was illegal – but what could we do? There were only a few women in the
house with us, whereas the Procureur was accompanied by several gendarmes.
Therefore, not wishing to afford the cowardly bully M. Desgarre the satisfaction
of using brute force, we give orders to our weeping maid to get ready our
dresses, etc., and having attempted to console the daughter of our landlady
by telling her we should soon return, we got into a carriage occupied by
two gendarmes, in the dead of night, in a strange country, to be taken
whither we knew not.

The gendarmerie barracks proved to be our destination; a bedroom having
been shown us, our door having been duly barricaded outside, we were left
alone. In this place we remained the following day until past 5 o'clock,
when, determined to know what all this meant, I desired to have an interview
with the Prefect, M. de Keratry came. I asked him how it was we had been
taken to the gendarmerie after he had promised us our liberty.

 “Thanks to my intercession,” answered he, “you have been allowed to
spend the night at the gendarmerie. The government” (M. Thiers) “would
have sent you to the prison of St. Godins, near Toulouse.” Then M. de Keratry
handed me a letter containing 3,000 francs, which had been sent to M. Lafargue
by his banker at Bordeaux, and which he, M. de Keratry, had hitherto detained;
declared we were free, were not to be expelled from France, but, like M.
Lafargue, at liberty to remain in the country. This time we were imprudent
enough to inform Madame Lafargue of what M. de Keratry had said with regard
to her husband.

On the 10th we received a laissez-passer to go over to Spain,
but our English passport was not returned to us. During ten days we applied
for it in vain. M. de Keratry wrote he had sent it to Paris, and could
not get it back, though he had repeatedly written for it.

We now saw we had only been turned out of the small gendarmerie of Luchon
to be locked up in the great gendarmerie, the Republic-Thiers. We were
still prisoners. Without a passport there was no getting out of France,
in which country we were evidently to be kept, until some event or other
should afford a pretext for again arresting us.

The police organs of Toulouse were daily accusing us of acting as emissaries
of the International on the French and Spanish frontiers. “But,” they added,
“the Prefect is taking energetic measures in order to reassure the inhabitants
of the Haute Garonne.” Now, it is true, a laissez-passer to go over
into Spain had been given us, but the experience of Madame Lafargue in
that country was not of a nature to encourage us to seek a refuge in the
land of El Cid.

The facts we learned from Madame Lafargue carry us back to the 6th of
August.

I mentioned above that our coachman had been compelled to leave us at
Fos. Whereupon M. Desgarre, the Procureur de la Republique, and several;
“gentlemen” of the police, attempted to persuade him, in the most plausible
manner, to return to Bosost, and on false pretenses to get M. Lafargue
to go to Fos. Fortunately an honest man is more than a match for a half
dozen police agents. The shrewd young fellow guessed there was some trick
at the bottom of all this glib talk, and flatly refused to fetch M. Lafargue;
consequently gendarmes and douaniers, with the Procureur
at their head, set out on an expedition to Bosost. M. le Baron Desgarre,
whose discretion is the better part of his valor, had previously declared
he would not go to Fos to capture M. Lafargue without a sufficient escort;
that he could do nothing with one or two gendarmes against a man like M.
Lafargue, most likely given to the use of firearms. M. Desgarre was mistaken
– not a bullet, but kicks and cuffs were reserved for hiM. On his return
from Bosost he attempted to interfere with peasants celebrating their village
feast. The brave mountaineers, who love their freedom as much as their
own mountain air, gave the noble baron a sound thrashing, and sent him
about his business, a sadder if not wiser man! But I am anticipating.

I was saying that M. Desgarre and his followers started for Bosost.
They soon reached that town, and soon found the hotel at which the Lafargues
were staying, for the inhabitants of Bosost only possess two hotels, or
rather inns. They are not yet sufficiently civilized to have the orthodox
number of public houses.

Now, while M. Desgarre is standing before the front door of the Hotel
Masse, M. Lafargue, aided by his good friends,the peasants, gets out of
the house by the back gate, climbs the mountains, and escapes along paths
known only to guides, goats, and English tourists – all the regular roads
being guarded by Spanish carabiniers. The Spanish police had enthusiastically
taken up the cause of their French brethren. Madame Lafargue is made to
feel all the blessings arising from the International Association of the
police. At 3 o'clock in the morning, her bedroom is suddenly broken into,
and in rush four Spanish officers, with their carbines pointed to the bed
in which she and her child are sleeping. The poor sick baby, suddenly awakened,
frightened, begins to scream; but that doesn't prevent the Spanish officers
from looking in every hole and cranny of the room for M. Lafargue. Finally,
convinced that their prey has escaped them, they declare they will carry
off Madame Lafargue. At this, the master of the hotel – a most worthy
man – interferes, saying he is sure the Spanish government will not accord
the extradition of a lady. He was right. Madame Lafargue was allowed to
remain at Bosost, but was ever after subjected to the annoyance of being
followed about by police agents. At the hotel, a troop of spies established
their headquarters. One Sunday, even the Prefect and the Procureur de la
Republique took the trouble to travel all the way from Luchon to Bosost
for the purpose of seeing Madame Lafargue. As, however, they did not succeed
in satisfying their curiosity, they consoled themselves by playing at rouge
et noir, which, together with baccarat, forms the only serious occupation
of the petits gras from Versailles, now staying
at the Pyrennes.

But I must not forget to explain how it was that M. de Keratry had not
succeeded in seeing Madame Lafargue. The fact is that a French peasant
from Luchon had informed some Spanish friends of his at Bosost of M. de
Keratry's intended visit, and they, of course, at once warned Madame Lafargue.

The French and Spanish population of the Pyrenees form a league, offensive
and defensive, against their respective governments. In our case, they
acted as spies upon the official spies of the Prefect – though repeatedly
stopped at the French frontiers, they were untiring in their attempts to
bring us news. Although M. de Keratry gave orders to the effect that no
one, not even guides, should be allowed to cross over to Bosost,
unless provided with a proper pass. This measure, of course, did not prevent
our having messages brought us as heretofore; it only served to embitter
still more the peasants of the Pyrenees, already so hostile to the Rurals
of Versailles.

In other parts of France I have since heard that the peasants
are quite as much opposed to their so-called representatives, the
governing Rurals. M. Thiers fulfills a great revolutionary mission! By
means of his prefects, priests, garde champetres, and gendarmes
he will before long provoke a general rising of the peasantry!

Of M. Lafargue's escape Madame Lafargue had informed us a few
days after our release from the gendarmerie. Later on, we heard
from a native of Bosost that M. Lafargue had been arrested at Huesca, and
that the Spaniards had made the offer of his extradition to the French
Government. On the very day we received this news, our English passport
was returned to us by the Juge de Paix. So in order to put an end to the
state of anxiety in which we knew Madame Lafargue must be placed, tied
down as she was to Bosost by her sick child, not knowing what had become
of her husband, we at once made up our minds to travel to Huesca, in order
to beg the governor of that district to let us know the real intentions
of the Spanish government with respect to M. Lafargue. On reaching San
Sebastian, we heard to our joy that M. Lafargue had been set at liberty.
So we immediately returned to England.

I cannot conclude this letter without giving a short sketch of the treatment
to which Madame C––, our landlady, and the servant were subjected on
the 6th of August, during our absence; for, compared with them, we had
always been treated with great courtesy. At 11 o'clock in the morning,
the Prefect, Procureur General, Procureur de la Republique, etc., made
a raid upon our house. Enraged at not being able to lay hands on M. Lafargue,
they vented their wrath on Madame C––, an invalid, suffering from heart
disease in an advanced stage, and upon our maid. That poor girl was treated
most roughly, because she would not tell where her master had gone.

This the Prefect, however, succeeded in learning from a boy, employed
by Madame C–– as gardener, and whom he straightway sent up to Fos, there
to lie in wait for us behind a hedge, in order to give warning of our arrival
to the Procureur de la Republique & Co.

If, during the campaign against the Prussians, M. de Keratry had employed
the same art of protecting his flanks and rear from surprise, of surprising
detachments of the enemy by establishing videttes and sending out scouts,
things would have gone better in Brittany – that is to say, if one may
judge from the success of de Keratry's tactics at Fos!

Our landlady was not allowed to light a fire in her own kitchen,
was ordered, instead of sleeping in her bed, to lie down on the floor.
With the latter order she, however, refused to comply. Catching hold of
her son, a child not three years of age, the Prefect said he must be the
son of M. Lafargue. Madame C–– repeatedly declared he was mistaken –
but in vain; at length, really anxious to prove her child's identity (she
feared he might be carried off), she exclaimed: “Why, the boy only speaks
the patois of the district.” For a moment or two, the Prefect looked as
if even that argument had failed to convince hiM. Perhaps M. de Keratry,
believing as he does that the “International is a religion", was pondering
on the miracle of the cloven tongues descending on the apostles.

One of the reasons why Madame C–– was so much ill used was because
she had never in her life heard of the International, and therefore could
not give an account of the doings of that mysterious society at Luchon,
which, y the way, would have been an impossible task for the best initiated
member – at least previous to the period at which M. de Keratry commenced
at Luchon his active propaganda for the International Association. Then
Madame C–– had been guilty of speaking of her tenant, M. Lafargue, in
very high terms. But the head and front of her offending was in her inability
to point out hidden bombs and petroleum.

Yes! It is a fact, bombs and petroleum were searched for in our
house.

Taking up a small night lamp, used for warming the baby's milk,
the assembled magistrates examined it; handling it with great caution,
as if it were some diabolical machine by means of which petroleum might
have been discharged into the streets of Paris. From Luchon to Paris. Even
Munchausen never indulged in such a stretch of imagination. The French
government are capable de tout. They really
believe in the truth of the wild petroleum fables – the coinage of their
own distempered brains. They do think the women of Paris are “neither brute
nor human, neither man nor woman” – but “petroleum” – a species of salamander,
delighting in their native element – fire.

They almost come up to Henri de Pene, of the Paris-Journal, their
prophet and teacher, who, as I am told, now actually fancies that the famous
letters manufactured by himself in my father's name have not been written
by Henri de Pene but Karl Marx.

One could afford to treat with silent contempt a government run mad,
and to laugh at the farces in which the pottering pantaloons employed by
that government play in their muddling and meddling parts, did not these
farces turn out to be tragedies for thousands of men, women, and children.
Think only of the “petroleuses” before the court-martial of Versailles,
and of the women who, for the last three months, are being slowly done
to death on the pontoons.

Jenny Marx

Jenny Marx Biographical Archive
