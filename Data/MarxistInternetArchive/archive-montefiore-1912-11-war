Dora B. Montefiore 1912Source: Daily Herald, November 2, 1912, p. 2;
Transcription: Ted Crawford.
HTML Markup: Brian Reid.
Public Domain: Marxists Internet Archive (2007). You may freely copy, distribute, display and perform this work; as well as make derivative and commercial works. Please credit “Marxists Internet Archive” as your source.John Ruskin once wrote: “There is not a war in the world but you women are responsible for. Not that you made it, but, in that you did not hinder it.”

Those words flash through my mind every day as I read of the horrors going on at present in the Balkan struggle for freedom.

I can sympathise with the Balkan women for urging on their men to drive the Turk out of their country; for those women have looked on scenes of terror and of anguish of which we in settled countries have not the faintest conception.

They have seen under Turkish rule their fathers, brothers and sons murdered and mutilated; they have seen their fellow women and even little girls, sacrificed to the lust of Turkish and Kurd soldiers; they have seen their babes torn from their arms and hacked to pieces in their sight. And now that Turkish power has been checked by Italy, the Balkan women, burning with the heaped up memories of insult and of cruelty, urge on their men to revenge.

But whose fault is it that the Turks have so long been allowed to misgovern and torture the people of the Balkan states?

It is the fault of the great Powers of Europe, who, because they fear each other have been too cowardly to turn out the Turks, as they should have been turned out years ago and allow the Balkan States to form a self-governing republic of their own. England, France, Italy, Russia, Austria and Germany, all knew in their secret hearts that they were doing a wrong to humanity in allowing Turkey to rule the destinies of these smaller and weaker nations, but they found Turkey a convenient policeman to cheek the filching propensities of the Governments of each of these countries.


For the people, in thinking out these questions of foreign policy, must always remember to differentiate between the Government of a country and the People of a country. The Governments, being capitalist Governments, exist to exploit the people of their own and of every other country.

The people of one country have no quarrel with the people of another country, and do not desire to take their land from them; besides, if it were taken away, it would still belong to the band of international capitalists. The lot of the people in every capitalist country is only to be exploited and to suffer.

In the countries of each of these great Powers there are women, millions of women; and what have they done to hinder the iniquities perpetrated by the Turks for many past years on the men, women and children of the Balkans?

“In that ye have not hindered.”

Think of it you women, you mothers, when you read of the shambles of Kirk Kilissa; of the “human ramparts” of the dead, behind which hand to hand fighting took place, of the 6,000 corpses soaked in paraffin and burned, because there was no time to bury them; of the “rivers of blood” and of the nameless mutilations of the wounded—every one of these poor corpses, every one of, these tortured, mutilated bodies, whether men of the Balkans or of Turkey, had a mother who watched over their first uncertain steps, and who with her own life, would have protected them from injury.

The Socialist women organise everywhere with their men to voice the Socialist protest; and to hold out the hand of comradeship to men and women Socialists in every land; but besides this, Socialist women who feel their responsibilities because they have a more intelligent outlook on questions of the day than have most other women, must see to it that this subject is given a prominent place in the teaching of the Socialist Sunday schools; and that during the coming Christmas season our Socialist women’s share of the “Goodwill” which is supposed to especially characterise that season, shall take the form of refusing to buy anything in the shape of a military toy for a child.

The Women’s Committee of the B.S.P. are holding at Chandos Hall, in the evening of November 2, at 7.30, their first annual conference, when several resolutions; including one of solidarity with the revolutionary Socialist women of other countries will be submitted to the conference. I have asked that to that present resolution a protest from the Socialist women against the present war, and against the possibility of its extension may be appended and I beg Socialist women everywhere to come together in their branches and pass similar resolutions pledging themselves to join in a general strike if war between the great Powers is threatened.
 
 Dora Montefiore Archive