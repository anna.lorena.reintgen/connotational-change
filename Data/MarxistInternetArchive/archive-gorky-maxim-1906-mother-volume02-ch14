
Maxim Gorky
 
The mother and Nikolay, walking up to the window, watched the girl
pass through the yard and disappear beyond the gate.  Nikolay
whistled quietly, sat down at the table and began to write.

"She'll occupy herself with this affair, and it'll be easier for
her," the mother reflected.

"Yes, of course!" responded Nikolay, and turning around to the
mother with a kind smile on his face, asked:  "And how about you,
Nilovna--did this cup of bitterness escape you?  Did you never know
the pangs for a beloved person?"

"Well!" exclaimed the mother with a wave of her hand.  "What sort
of a pang?  The fear they had whether they won't marry me off to
this man or that man?"

"And you liked no one?"

She thought a little, and answered:

"I don't recall, my dear!  How can it be that I didn't like anybody?
I suppose there was somebody I was fond of, but I don't remember."

She looked at him, and concluded simply, with sad composure:  "My
husband beat me a lot; and everything that was before him was
effaced from my soul."

Nikolay turned back to the table; the mother walked out of the room
for a minute.  On her return Nikolay looked at her kindly and began to
speak softly and lovingly.  His reminiscences stroked her like a caress.

"And I, you see, was like Sashenka.  I loved a girl:  a marvelous
being, a wonder, a--guiding star; she was gentle and bright for me.
I met her about twenty years ago, and from that time on I loved her.
And I love her now, too, to speak the truth.  I love her all so--
with my whole soul--gratefully--forever!"

Standing by his side the mother saw his eyes lighted from within by
a clear, warm light.  His hands folded over the back of the chair,
and his head leaning on them, he looked into the distance; his whole
body, lean and slender, but powerful, seemed to strive upward, like
the stalk of a plant toward the sun.

"Why didn't you marry?  You should have!"

"Oh, she's been married five years!"

"And before that--what was the matter?  Didn't she love you?"

He thought a while, and answered:

"Yes, apparently she loved me; I'm certain she did.  But, you see,
it was always this way:  I was in prison, she was free; I was free,
she was in prison or in exile.  That's very much like Sasha's position,
really.  Finally they exiled her to Siberia for ten years.  I wanted
to follow her, but I was ashamed and she was ashamed, and I remained
here.  Then she met another man--a comrade of mine, a very good
fellow, and they escaped together.  Now they live abroad.  Yes----"

Nikolay took off his glasses, wiped them, held them up to the light
and began to wipe them again.

"Ah, you, my dear!" the mother exclaimed lovingly, shaking her head.
She was sorry for him; at the same time something compelled her to
smile a warm, motherly smile.  He changed his pose, took the pen in
his hand, and said, punctuating the rhythm of his speed with waves
of his hand:

"Family life always diminishes the energy of a revolutionist.
Children must be maintained in security, and there's the need to
work a great deal for one's bread.  The revolutionist ought without
cease to develop every iota of his energy; he must deepen and
broaden it; but this demands time.  He must always be at the head,
because we--the workingmen--are called by the logic of history to
destroy the old world, to create the new life; and if we stop, if we
yield to exhaustion, or are attracted by the possibility of a little
immediate conquest, it's bad--it's almost treachery to the cause.
No revolutionist can adhere closely to an individual--walk through
life side by side with another individual--without distorting his
faith; and we must never forget that our aim is not little
conquests, but only complete victory!"

His voice became firm, his face paled, and his eyes kindled with the
force that characterized him.  The bell sounded again.  It was
Liudmila.  She wore an overcoat too light for the season, her cheeks
were purple with the cold.  Removing her torn overshoes, she said in
a vexed voice:

"The date of the trial is appointed--in a week!"

"Really?" shouted Nikolay from the room.

The mother quickly walked up to him, not understanding whether
fright or joy agitated her.  Liudmila, keeping step with her, said,
with irony in her low voice:

"Yes, really!  The assistant prosecuting attorney, Shostak, just
now brought the incriminating acts.  In the court they say, quite
openly, that the sentence has already been fixed.  What does it
mean?  Do the authorities fear that the judges will deal too
mercifully with the enemies of the government?  Having so long and
so assiduously kept corrupting their servants, is the government
still unassured of their readiness to be scoundrels?"

Liudmila sat on the sofa, rubbing her lean cheeks with her palms;
her dull eyes burned contemptuous scorn, and her voice filled with
growing wrath.

"You waste your powder for nothing, Liudmila!"  Nikolay tried to
soothe her.  "They don't hear you."

"Some day I'll compel them to hear me!"

The black circles under her eyes trembled and threw an ominous
shadow on her face.  She bit her lips.

"You go against me--that's your right; I'm your enemy.  But in
defending your power don't corrupt people; don't compel me to have
instinctive contempt for them; don't dare to poison my soul with
your cynicism!"

Nikolay looked at her through his glasses, and screwing up his eyes,
shook his head sadly.  But she continued to speak as if those whom
she detested stood before her.  The mother listened with strained
attention, understanding nothing, and instinctively repeating to
herself one and the same words, "The trial--the trial will come off
in a week!"

She could not picture to herself what it would be like; how the
judges would behave toward Pavel.  Her thoughts muddled her brain,
covered her eyes with a gray mist, and plunged her into something
sticky, viscid, chilling and paining her body.  The feeling grew,
entered her blood, took possession of her heart, and weighed it
down heavily, poisoning in it all that was alive and bold.

Thus, in a cloud of perplexity and despondency under the load of
painful expectations, she lived through one day, and a second day;
but on the third day Sasha appeared and said to Nikolay:

"Everything is ready--to-day, in an hour!"

"Everything ready?  So soon?"  He was astonished.

"Why shouldn't everything be ready?  The only thing I had to do was
to get a hiding place and clothes for Rybin.  All the rest Godun
took on himself.  Rybin will have to go through only one ward of the
city.  Vyesovshchikov will meet him on the street, all disguised, of
course.  He'll throw an overcoat over him, give him a hat, and show
him the way.  I'll wait for him, change his clothes and lead him off."

"Not bad!  And who's this Godun?"

"You've seen him!  You gave talks to the locksmiths in his place."

"Oh, I remember!  A droll old man."

"He's a soldier who served his time--a roofer, a man of little
education, but with an inexhaustible fund of hatred for every kind
of violence and for all men of violence.  A bit of a philosopher!"

The mother listened in silence to her, and something indistinct
slowly dawned upon her.

"Godun wants to free his nephew--you remember him?  You liked
Yevchenko, a blacksmith, quite a dude."  Nikolay nodded his head.
"Godun has arranged everything all right.  But I'm beginning to
doubt his success.  The passages in the prison are used by all the
inmates, and I think when the prisoners see the ladder many will
want to run--"  She closed her eyes and was silent for a while.
The mother moved nearer to her.  "They'll hinder one another."

They all three stood before the window, the mother behind Nikolay
and Sasha.  Their rapid conversation roused in her a still stronger
sense of uneasiness and anxiety.

"I'm going there," the mother said suddenly.

"Why?" asked Sasha.

"Don't go, darling!  Maybe you'll get caught.  You mustn't!"
Nikolay advised.

The mother looked at them and softly, but persistently, repeated:
"No; I'm going!  I'm going!"

They quickly exchanged glances, and Sasha, shrugging her shoulders, said:

"Of course--hope is tenacious!"

Turning to the mother she took her by the hand, leaned her head
on her shoulder, and said in a new, simple voice, near to the
heart of the mother:

"But I'll tell you after all, mamma, you're waiting in vain--he
won't try to escape!"

"My dear darling!" exclaimed the mother, pressing Sasha to her
tremulously.  "Take me; I won't interfere with you; I don't believe
it is possible--to escape!"

"She'll go," said the girl simply to Nikolay.

"That's your affair!" he answered, bowing his head.

"We mustn't be together, mamma.  You go to the garden in the lot.
From there you can see the wall of the prison.  But suppose they
ask you what you are doing there?"

Rejoiced, the mother answered confidently:

"I'll think of what to say."

"Don't forget that the overseers of the prison know you," said
Sasha; "and if they see you there----"

"They won't see me!" the mother laughed softly.

An hour later she was in the lot by the prison.  A sharp wind blew
about her, pulled her dress, and beat against the frozen earth,
rocked the old fence of the garden past which the woman walked, and
rattled against the low wall of the prison; it flung up somebody's
shouts from the court, scattered them in the air, and carried them
up to the sky.  There the clouds were racing quickly, little rifts
opening in the blue height.

Behind the mother lay the city; in front the cemetery; to the right,
about seventy feet from her, the prison.  Near the cemetery a soldier
was leading a horse by a rein, and another soldier tramped noisily
alongside him, shouted, whistled, and laughed.  There was no one
else near the prison.  On the impulse of the moment the mother
walked straight up to them.  As she came near she shouted:

"Soldiers! didn't you see a goat anywhere around here?"

One of them answered:

"No."

She walked slowly past them, toward the fence of the cemetery,
looking slantwise to the right and the back.  Suddenly she felt her
feet tremble and grow heavy, as if frozen to the ground.  From the
corner of the prison a man came along, walking quickly, like a
lamplighter.  He was a stooping man, with a little ladder on his
shoulder.  The mother, blinking in fright, quickly glanced at the
soldiers; they were stamping their feet on one spot, and the horse
was running around them.  She looked at the ladder--he had already
placed it against the wall and was climbing up without haste.  He
waved his hand in the courtyard, quickly let himself down, and
disappeared around the corner.  That very second the black head of
Mikhail appeared on the wall, followed by his entire body.  Another
head, with a shaggy hat, emerged alongside of his.  Two black lumps
rolled to the ground; one disappeared around the corner; Mikhail
straightened himself up and looked about.

"Run, run!" whispered the mother, treading impatiently.  Her ears
were humming.  Loud shouts were wafted to her.  There on the wall
appeared a third head.  She clasped her hands in faintness.  A
light-haired head, without a beard, shook as if it wanted to tear
itself away, but it suddenly disappeared behind the wall.  The
shouts came louder and louder, more and more boisterous.  The wind
scattered the thin trills of the whistles through the air.  Mikhail
walked along the wall--there! he was already beyond it, and
traversed the open space between the prison and the houses of the
city.  It seemed to her as if he were walking very, very slowly,
that he raised his head to no purpose.  "Everyone who sees his face
will remember it forever," and she whispered, "Faster! faster!"
Behind the wall of the prison something slammed, the thin sound of
broken glass was heard.  One of the soldiers, planting his feet
firmly on the ground, drew the horse to him, and the horse jumped.
The other one, his fist at his mouth, shouted something in the
direction of the prison, and as he shouted he turned his head
sidewise, with his ear cocked.

All attention, the mother turned her head in all directions, her
eyes seeing everything, believing nothing.  This thing which she
had pictured as terrible and intricate was accomplished with extreme
simplicity and rapidity, and the simpleness of the happenings
stupefied her.  Rybin was no longer to be seen--a tall man in a thin
overcoat was walking there--a girl was running along.  Three wardens
jumped out from a corner of the prison; they ran side by side,
stretching out their right hands.  One of the soldiers rushed in
front of them; the other ran around the horse, unsuccessfully trying
to vault on the refractory animal, which kept jumping about.  The
whistles incessantly cut the air, their alarming, desperate shrieks
aroused a consciousness of danger in the woman.  Trembling, she
walked along the fence of the cemetery, following the wardens; but
they and the soldiers ran around the other corner of the prison and
disappeared.  They were followed at a run by the assistant overseer
of the prison, whom she knew; his coat was unbuttoned.  From
somewhere policemen appeared, and people came running.

The wind whistled, leaped about as if rejoicing, and carried the
broken, confused shouts to the mother's ears.

"It stands here all the time."

"The ladder?"

"What's the matter with you then?  The devil take you!"

"Arrest the soldiers!"

"Policeman!"

Whistles again.  This hubbub delighted her and she strode on more
boldly, thinking, "So, it's possible--HE could have done it!"

But now pain for her son no longer entered her heart without pride
in him also.  And only fear for him weighed and oppressed her to
stupefaction as before.

From the corner of the fence opposite her a constable with a black,
curly beard, and two policemen emerged.

"Stop!" shouted the constable, breathing heavily.  "Did you see--
a man--with a beard--didn't he run by here?"

She pointed to the garden and answered calmly:

"He went that way!"

"Yegorov, run!  Whistle!  Is it long ago?"

"Yes--I should say--about a minute!"

But the whistle drowned her voice.  The constable, without waiting
for an answer, precipitated himself in a gallop along the hillocky
ground, waving his hands in the direction of the garden.  After
him, with bent head, and whistling, the policemen darted off.

The mother nodded her head after them, and, satisfied with herself,
went home.  When she walked out of the field into the street a cab
crossed her way.  Raising her head she saw in the vehicle a young
man with light mustache and a pale, worn face.  He, too, regarded
her.  He sat slantwise.  It must have been due to his position that
his right shoulder was higher than his left.

At home Nikolay met her joyously.

"Alive?  How did it go?"

"It seems everything's been successful!"

And slowly trying to reinstate all the details in her memory, she
began to tell of the escape.  Nikolay, too, was amazed at the success.

"You see, we're lucky!" said Nikolay, rubbing his hands.  "But how
frightened I was on your account only God knows.  You know what,
Nilovna, take my friendly advice:  don't be afraid of the trial.
The sooner it's over and done with the sooner Pavel will be free.
Believe me.  I've already written to my sister to try to think what
can be done for Pavel.  Maybe he'll even escape on the road.  And
the trial is approximately like this."  He began to describe to her
the session of the court.  She listened, and understood that he was
afraid of something--that he wanted to inspirit her.

"Maybe you think I'll say something to the judges?" she suddenly
inquired.  "That I'll beg them for something?"

He jumped up, waved his hands at her, and said in an offended tone:

"What are you talking about?  You're insulting me!"

"Excuse me, please; excuse me!  I really AM afraid--of what I don't know."

She was silent, letting her eyes wander about the room.

"Sometimes it seems to me that they'll insult Pasha--scoff at him.
'Ah, you peasant!' they'll say.  'You son of a peasant!  What's this
mess you've cooked up?'  And Pasha, proud as he is, he'll answer
them so----!  Or Andrey will laugh at them--and all the comrades
there are hot-headed and honest.  So I can't help thinking that
something will suddenly happen.  One of them will lose his patience,
the others will support him, and the sentence will be so severe--
you'll never see them again."

Nikolay was silent, pulling his beard glumly as the mother continued:

"It's impossible to drive this thought from my head.  The trial is
terrible to me.  When they'll begin to take everything apart and
weigh it--it's awful!  It's not the sentence that's terrible, but
the trial--I can't express it."  She felt that Nikolay didn't
understand her fear; and his inability to comprehend kept her from
further analysis of her timidities, which, however, only increased
and broadened during the three following days.  Finally, on the day
of the trial, she carried into the hall of the session a heavy dark
load that bent her back and neck.

In the street, acquaintances from the suburbs had greeted her.  She
had bowed in silence, rapidly making her way through the dense,
crowd in the corridor of the courthouse.  In the hall she was met by
relatives of the defendants, who also spoke to her in undertones.
All the words seemed needless; she didn't understand them.  Yet all
the people were sullen, filled with the same mournful feeling which
infected the mother and weighed her down.

"Let's sit next to each other," suggested Sizov, going to a bench.

She sat down obediently, settled her dress, and looked around.
Green and crimson specks, with thin yellow threads between, slowly
swam before her eyes.

"Your son has ruined our Vasya," a woman sitting beside her said quietly.

"You keep still, Natalya!" Sizov chided her angrily.

Nilovna looked at the woman; it was the mother of Samoylov.  Farther
along sat her husband--bald-headed, bony-faced, dapper, with a
large, bushy, reddish beard which trembled as he sat looking in
front of himself, his eyes screwed up.

A dull, immobile light entered through the high windows of the hall,
outside of which snow glided and fell lingeringly on the ground.
Between the windows hung a large portrait of the Czar in a massive
frame of glaring gilt.  Straight, austere folds of the heavy crimson
window drapery dropped over either side of it.  Before the portrait,
across almost the entire breadth of the hall, stretched the table
covered with green cloth.  To the right of the wall, behind the
grill, stood two wooden benches; to the left two rows of crimson
armchairs.  Attendants with green collars and yellow buttons on
their abdomens ran noiselessly about the hall.  A soft whisper
hummed in the turbid atmosphere, and the odor was a composite of
many odors as in a drug shop.  All this--the colors, the glitter,
the sounds and odors--pressed on the eyes and invaded the breast
with each inhalation.  It forced out live sensations, and filled
the desolate heart with motionless, dismal awe.

Suddenly one of the people said something aloud.  The mother
trembled.  All arose; she, too, rose, seizing Sizov's hand.

In the left corner of the hall a high door opened and an old man
emerged, swinging to and fro.  On his gray little face shook white,
sparse whiskers; he wore eyeglasses; the upper lip, which was
shaven, sank into his mouth as by suction; his sharp jawbones and
his chin were supported by the high collar of his uniform; apparently
there was no neck under the collar.  He was supported under the arm
from behind by a tall young man with a porcelain face, red and round.  
Following him three more men in uniforms embroidered in gold, and
three garbed in civilian wear, moved in slowly.  They stirred about
the table for a long time and finally took seats in the armchairs.
When they had sat down, one of them in unbuttoned uniform, with a
sleepy, clean-shaven face, began to say something to the little old
man, moving his puffy lips heavily and soundlessly.  The old man
listened, sitting strangely erect and immobile.  Behind the glasses
of his pince-nez the mother saw two little colorless specks.

At the end of the table, at the desk, stood a tall, bald man, who
coughed and shoved papers about.

The little old man swung forward and began to speak.  He pronounced
clearly the first words, but what followed seemed to creep without
sound from his thin, gray lips.

"I open----"

"See!" whispered Sizov, nudging the mother softly and arising.

In the wall behind the grill the door opened, a soldier came out
with a bared saber on his shoulder; behind him appeared Pavel,
Andrey, Fedya Mazin, the two Gusevs, Samoylov, Bukin, Somov, and
five more young men whose names were unknown to the mother.  Pavel
smiled kindly; Andrey also, showing his teeth as he nodded to her.
The hall, as it were, became lighter and simpler from their smile;
the strained, unnatural silence was enlivened by their faces and
movements.  The greasy glitter of gold on the uniforms dimmed and
softened.  A waft of bold assurance, the breath of living power,
reached the mother's heart and roused it.  On the benches behind
her, where up to that time the people had been waiting in crushed
silence, a responsive, subdued hum was audible.

"They're not trembling!" she heard Sizov whisper; and at her right
side Samoylov's mother burst into soft sobs.

"Silence!" came a stern shout.

"I warn you beforehand," said the old man, "I shall have to----"

Next: CHAPTER XV

Table of Contents: Mother (Part II)
