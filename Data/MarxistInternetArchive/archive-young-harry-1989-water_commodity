Harry Young
Source: Socialist Standard, November 1989.
Transcription: Socialist Party of Great Britain.
HTML Markup: Adam Buick
Copyleft: Creative Commons (Attribute & No Derivatives) 2007 conference "Be it resolved that all material created and published by the Party shall be licensed under the Creative Commons Attribution-NoDerivs copyright licence".
"It's outrageous", Sara Parkin, the Green Party spokeswoman was quoted as saying, "that water should become a capitalist commodity".

Commodity production is a hall-mark of capitalism and if Sara Parkin could be persuaded to dip into Volume I of Marx's Capital the first words to meet her eye would be:The wealth of societies in which the capitalist mode of production prevails presents itself as an immense accumulation of commodities; our investigation therefore must begin with the analysis of a commodity.And what does this investigation show? That what makes a good (a use value) into a commodity (an exchange value) is its production for sale, with a view to profit. However, how can it be that water through our taps costs us £s a week whereas falling in a storm it is free? The answer lies in the analysis of a commodity.

This brings us back to capitalist society, where water is a commodity possessing value (exchange value). All commodities must have two kinds of value: use value and exchange value. To be a commodity a good must have use value, otherwise it wouldn't sell and so have no exchange value. Water down our necks in a storm is useless but we need water to do the washing or make a cup of tea.

So what is it that converts water from a useless nuisance into a valuable commodity? There is no shortage of water in the world. It is only unequally distributed in nature. The water is available but has to be brought to where it is needed, as to meet the demands of modern conurbations. As was well said by a World Health Organisation expert, "there is no shortage of water, only pipes", and therein lies our answer.

Reservoirs, tanks, pumps, filtration plants and drains can only be produced by human labour, and it is only human labour which, in capitalist society, imparts exchange value. If value depended upon usefulness, water would be the second most expensive item (after air) on Earth. Rain irrigating crops is indispensable but contains no human labour and so is valueless (free), like the air we breathe. Installations to store, purify and supply water necessitate human labour and water thereby becomes a value-bearing commodity depending on the amount of socially necessary labour required.

Therefore, Sara Parkin's idea that water could be somehow exempted from capitalist commodity production is a non-starter. Like so much of the Green Party outlook, it is the same old story of capitalism without commodity production, which Pierre Proudhon was advocating 140 years ago.

Under capitalism every useful thing containing human labour produced as a non-use value to its owner becomes a commodity, with a price. This is so whether the means for producing it are nationalised or privatised. Trying to except the water supply (or electricity or gas or transport) from commodity production under capitalism is like trying to run the Boat Race outside the river.

It is capitalism (commodity production) not privatisation which is the cause of water being a commodity. Only its complete abolition will end the situation which so outrages Sara Parkin.
Harry Young | Socialist Party of Great Britain
