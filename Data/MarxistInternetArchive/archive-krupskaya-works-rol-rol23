
Arrangements were made for Ilyich to spend that night at the
Sulimovs', in the Petrograd District. The safest place for Ilyich
to hide in was the Vyborg District. It was decided that he would
live with Kayurov, a worker. I called for Ilyich at the Sulimovs',
and we went together to the Vyborg District. The Moskovsky
Regiment was passing down a boulevard. Kayurov was sitting in the
boulevard, waiting for us. When he saw us he got up and walked
ahead. Ilyich followed him, and I turned off to one side. The
military cadets wrecked the editorial office of Pravda. A meeting
of the Petrograd Committee was held during the day in the
caretaker's lodge of the Renault Plant, at which Ilyich was
present. The question of a general strike was discussed. A
decision was made not to call it. From there Ilyich went to the
apartment of Fofanova, in Lesnoi Prospekt, where he had an
appointment with several members of the Central Committee. That
day the workers' movement was suppressed. Alexinsky,
Vperyod-ist and former deputy of the Petrograd workers in
the Second Duma, who had once been our close associate, and
Pankratov, member of the S.-R. Party and an old Schlusselburger,
spread a slanderous rumour to the effect that Lenin, according to
information in their possession, was a German spy. They aimed at
paralyzing Lenin's influence. On July 6 the Provisional Government
issued an order for the arrest of Lenin, Zinoviev and Kamenev. The
Krzesinska Mansion was occupied by government troops. Ilyich moved
from Kayurov's place to Alliluyev's, where Zinoviev was in
hiding. Kayurov's son was an Anarchist, and the young people
messed about with bombs; his house, therefore, was not quite a
suitable place for hiding in.
On the 7th Maria Ilyinichna and I went to see Ilyich at the
Alliluyevs' place. It happened to be a moment of vacillation with
Ilyich. He argued the necessity of making his appearance in
court. Maria Ilyinichna warmly protested against it. "Grigory and
I have decided to appear – go and tell Kamenev," Ilyich said to
me. Kamenev was staying at another flat not far away. I got up
hastily. "Let's say good-bye,"Ilyich checked me. "We may not see
each other again." We embraced. I went to Kamenev and gave him
Ilyich's message. In the evening Stalin and others persuaded
Ilyich not to appear in court, and by so doing, saved his
life. That evening our place in Shirokaya Street was raided. Only
our room was searched. The raid was conducted by a colonel and
another military man in a greatcoat with a white lining. They took
some notes and documents of mine off the table. They asked me if I
knew where Lenin was, and I gathered from that question that he
had not given himself up. In the morning I went to Smilga, who
lived in the same street. Stalin and Molotov were there. There I
learned that Ilyich and Zinoviev had decided to go into
hiding.Two days later, on the 9th, a gang of cadets came charging in
and ransacked the whole flat. They took Mark Yelizarov, Anna
Ilyinichna's husband, for Lenin. They questioned me closely about
it. The Yelizarovs had a servant living with them, a country girl
named Annushka. She was from some remote village and had no idea
what was going on in the world. She was very keen on learning to
read and write, and would snatch up her ABC book whenever she had
a moment to spare, but learning did nut come easy to her. "I'm a
village dunce," she would cry ruefully. I tried to help her learn
to read, and to explain what parties there were, what the war was
all about, etc. She had no idea who Lenin was. I was not at home
on the 8th, but the Yelizarovs afterwards told me what happened. A
motor-car drove up to the house and a hostile demonstration was
made. All of a sudden Annushka came running in, yelling: "Olenins
or somebody have arrived!" During the search the cadets questioned her, and pointing to
Mark, asked what his name was. She did not know. They decided that
she did not want to tell them. Then they searched the kitchen, and
looked under her bed. This got Annushka's goat. "Why don't you
look in the stove, maybe somebody's hiding in there!" she
remarked. The three of us – Mark Yelizarov, Annushka and I, were
taken to the General Staff Headquarters. There we were seated at a
distance from one another, and each was guarded by a soldier with
a rifle. After a while a bunch of infuriated officers burst into
the room, ready to throw themselves at us. But a colonel came
in – the same colonel who had been in charge of the first raid – and
he looked at us and said: "These are not the people we want." Had
Ilyich been there, they would have torn him to pieces. We were
dismissed. Mark Yelizarov insisted on our being given a motor-car
to go home in. The colonel promised and went away. Of course, no
one gave us any car. We took a cab. The bridges were raised, and
we did not get home until morning. We knocked at the door for a
long time and were beginning to fear something had happened. At
last the door was opened.
The Yelizarovs' place was searched a third time. I was at the
District Council at the time. I came home to find the entrance to
the building occupied by soldiers and the street full of people. I
stood there awhile, then went back to the District Council. I
could do nothing to help just the same. It was late by the time I
got back to the council office, and there was no one there except
the caretaker. Presently Slutsky came – this comrade had recently
arrived from America with Volodarsky, Melnichansky and others. He
was afterwards killed on the Southern Front. He had just escaped
arrest and urged me not to go home, but to send someone down in
the morning to find out what had happened. We went out to look for
a place to sleep in, but we did not have any addresses of
comrades. We wandered about the district for a long time until we
got to Fofanova's, who put us up for the night. In the morning we
]earned that none of our people had been arrested and that this
time the searchers had not been so rough as before.
Ilyich and Zinoviev were in hiding at Razliv, not far from
Sestroretsk, in the house of Yemelyanov, an old underground Party
worker employed at the Sestroretsk factory. Ilyich retained a warm
feeling towards Yemelyanov and his family till the very end.
I spent all my time in the Vyborg District. The difference
between the temper of the man in the street and that of the
workers during the July days was very striking. The former could
be heard muttering angrily in the trams and on every street
corner, but as soon as one crossed the wooden bridge leading to
the Vyborg District, one seemed to step into another world. I was
up to my ears in work. Through Zof and others connected with
Yemelyanov, I received Ilyich's notes giving various
instructions. The reaction was rampant. On July 9 a joint meeting
of the All-Russian Central Executive Committee and the Executive
Committee of the Soviet of Workers and Peasants' Deputies declared
the Provisional Government to be "the government of salvation of
the revolution." On the same day the "salvation" began. That day
Kamenev was arrested; on July 12 an order was issued introducing
the death penalty at the front; on July 15 Pravda and
Okopnaya Pravda were suppressed, and an order was issued
banning meetings at the front; arrests were made among the
Bolsheviks in Helsingfors, and the Bolshevik paper there,
Volna (Wave), was suppressed. On July 18 the Finnish Diet
was dismissed, and General Kornilov appointed Commander-in-Chief;
on July 22 Trotsky and Lunacharsky were arrested.
Shortly after the July days Kerensky hit on a scheme that was
calculated to improve discipline among the troops; he decided to
make an example of the machinegun regiment which had started the
demonstration in the July days by having it marched out, disarmed,
into a square and there publicly degraded. I saw the disarmed
regiment going out to the square. The soldiers were leading the
horses by the bridles, and there was such smouldering hatred in
their eyes, such resentment in their slow deliberate tread, that
it was clear that no more stupid method could have been
devised. As a matter of fact, the machinegun regiment sided
wholeheartedly with the Bolsheviks in October, and guarded Ilyich
at Smolny.
The Bolshevik Party went over to a state of semi-legality,
but it grew in strength and numbers. By the time of the opening of
the Sixth Party Congress on July 26 it numbered 177,000
members – twice as much as at the All-Russian April Conference
three months previously. The growth of Bolshevik influence,
especially among the troops, was obvious. The Sixth Congress
welded the forces of the Bolsheviks still closer. The appeal
issued in the
name of the Sixth Party Congress spoke about the counter-revolutionary position taken by the Provisional Government, and about the impending world revolution and the battle of classes. "Our Party," the appeal stated, "is entering this battle with its banner unfurled. It has firmly held this banner in its grasp. It has not lowered it before the oppressors and slanderers, before traitors to the revolution and flunkeys of capital. It will hold the banner aloft in the struggle for socialism, for the brotherhood of nations, for it knows that a new movement is rising and that the death hour of the old world is approaching."
On August 25 Kornilov began his advance on Petrograd. The
workers, those of the Vyborg District first and foremost, rushed
to the defence of Petrograd. Our agitators were sent out to the
units of Kornilov's so-called "Savage Division."" Kornilov's
troops quickly became demoralized, and the advance petered
out. Corps Commander General Krymov shot himself. I recall the
figure of one of our Vyborg workers, a young man, who worked on
the organization of literacy classes. He had been one of the first
to go to the front. I remember him returning from the front and
rushing straight off to the District Council with his rifle still
on his shoulder. The literacy school was short of chalk. In came
this young man, his face still wearing the flush of battle, put
his rifle in a corner, and began talking excitedly about chalk and
blackboards. In the Vyborg District I had an opportunity of daily
observing how closely the workers linked the revolutionary
struggle with the struggle for mastering knowledge and
culture.
With the approach of autumn, it was no longer possible for
Ilyich to live in the shanty at Razliv, where he was in hiding. He
decided to cross over into Finland, where he wanted to write his
book The State and Revolution, for which he had collected
a mass of notes, and which he had thought out in every detail. In
Finland it was also more convenient to follow the newspapers.
N. A. Yemelyanov procured for him a passport in the name of a
fictitious Sestroretsk worker, and Ilyich was given a wig to put
on and made up to look like a workman. Dmitry Leshchenko, an old
Party comrade of 1905-1907 days and former secretary of our
Bolshevik newspapers at whose place Vladimir Ilyich had often
slept in those days (Leshchenko was now my associate in
educational work in the Vyborg District), went to Razliv to
photograph Ilyich for the passport. Jalava, a Finnish comrade who
worked as an engine-driver on the Finnish Railway (he was well
known to Shotman and Rahja), undertook to get Ilyich across under
the guise of a fireman. And that is what he did. Jalava also
served as a medium for communication with Ilyich, and I often went
to see him to get letters from Ilyich – he lived in the Vyborg
District, too. When Ilyich was settled in Helsingfors, he sent me
a letter in invisible ink inviting me to join him; he gave his
address and even sketched a plan by which I could find his place
without having to ask anybody. The trouble was I had burnt the
edges of the plan while heating the letter up over a lamp. The
Yemelyanovs got a passport for me, too – that of an old Sestroretsk
woman worker. I put a shawl on my head and went to the Yemelyanovs
in Razliv, and they saw me across the frontier (no special permit
beyond a passport was required for local inhabitants in crossing
the border). An officer just glanced at my passport. I had to walk
five versts through a wood to Ollila, a small station, where I was
to catch a soldiers' train. Everything went off splendidly. The
burnt edges of the plan gave me some trouble, though. I wandered
about the streets for a long time before I found the one I
wanted. Ilyich was ever so glad to see me. Obviously, he had been
feeling desperately lonely, living here underground at a time when
it was so important for him to be in the centre of preparations
for the struggle. I told him all the news, and stayed in
Helsingfors for two days. When I left Ilyich insisted on seeing me
off, at least as far as the last turning before the railway
station. We arranged that I would come again.
I visited Ilyich again about a fortnight later. I was a bit
late and decided to go to Ollila by myself, without dropping
in on the Yemelyanovs. It had begun to grow dark in the woods – it
was late autumn – and the moon rose. My feet sank into the sand. I
was afraid that I had lost my way, and I hurried along. When I
reached Ollila I found the train had not arrived yet. I had to
wait half an hour for it. The carriage was packed with soldiers
and sailors, and I had to stand all the way. The soldiers spoke
openly of an uprising. They only talked politics. The
carriage was like a meeting room, tingling with
excitement. No outsiders came in. One civilian did come in at
first, but after hearing a soldier telling how they had thrown
officers into the river at Vyborg, he slipped out at the next
stop. No one took any notice of me. When I told Ilyich about this
talk among the soldiers, his face became thoughtful, and no matter
what he talked about afterwards it remained thoughtful all the
time. Obviously, he was saying one thing and thinking of
another – thinking of the uprising and how best to organize it.
On September 13-14 Vladimir Ilyich wrote his letter
Marxism and Insurrection to the Central Committee, and at
the end of September he moved to Vyborg from Helsingfors in order
to be nearer to Petrograd. From Vyborg he wrote to Smilga in
Helsingfors (Smilga, at the time, was the chairman of the Regional
Committee of the Army, Navy and Workers of Finland) to the effect
that all attention should be given to military
preparation of the Finnish army and navy for the forthcoming
overthrow of Kerensky. His mind was wholly occupied at the time
with the problem of remodelling the entire machinery of
government, reorganizing the masses along new lines, weaving anew
the whole social fabric, as he expressed it. He wrote about this
in his article "Can the Bolsheviks Retain State Power?", he wrote
about this in his appeal to the peasants and soldiers, in a letter
to the Petrograd City Conference to be read at a closed meeting,
in which he now proposed concrete measures for seizing power; he
wrote about this to the members of the Central Committee, the
Moscow Committee and the Petrograd Committee of the Party, and the
Bolshevik members of the Petrograd and Moscow Soviets.
Read next section |
Krupskaya Internet Archive |
Marxists Internet Archive
