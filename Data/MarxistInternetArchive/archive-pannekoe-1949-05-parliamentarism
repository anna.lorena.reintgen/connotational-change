
Published: Left, no. 149. May 1950. Pages 49-53.
Transcription/Markup: Micah Muer, 2017.
Editorial Note. — The Editorial Board of "Left" is deeply honoured at
being permitted to publish this article by Dr. Pannekoek, the veteran Dutch
Libertarian Socialist and, beyond question, one of the greatest living Socialist
thinkers and scholars. — Editor, "Left."In the second half of the 19th century the opinion spread widely among the
workers that socialism can and must be won by a parliamentary conquest of
political power; and it seemed well on the way. In the 20th century
disappointment brought scepticism; reformist, so-called socialist, parties rose
to power but deteriorated into agencies of capitalism. Many of the most sincere
militant groups lost confidence in parliamentary action, and the masses stand
aloof, indifferent. What can be the reason?Socialist propaganda can clearly demonstrate the advantages of socialist
order above capitalist disorder and exploitation. The working masses constitute
the majority of the population, and they have the ballot. As soon as they see
the necessity of socialism they can by their vote establish a majority in
Parliament that instals a socialist Government. Acts of Parliament and measures
of the Government can then bring about the necessary changes in social
structure. Thus the working class will have won mastery over society, will have
gained socialism without anything of a violent revolution. It is quite easy.That is just the difficulty. It is too easy. If in attacking a strong
fortress you find the gate open, inviting you to enter, you suspect traps, or at
least you know that the real fight has to come elsewhere. Everybody knows that
the conquest of power by the working class, its conquest of mastery over
society, the annihilation of capitalism, can only be the result of a long and
heavy class-war, of the utmost exertion, of stubborn fight and great sacrifice.
There must be a flaw in the argument.The working class stands over against a foe more powerful than ever was the
ruling class. The bourgeoisie is not simply master because it has the majority
in Parliament. The entire fabric of political institutions, of administration
and bureaucracy, of higher and lower officials, is in its hands. The practice of
the present Government is highly instructive in showing how these powers are
able to follow their own old ways in policy independent of Parliament. Though
State power is its mightiest instrument to keep the masses down, the power of
the bourgeoisie has deeper roots in society, material and spiritual roots. Its
material power consists in its in its ownership of all the riches of the earth,
of the entire production apparatus on which the working people is dependent for
life. Its spiritual power consists in that its mode of thinking, the
middle-class world view, dominates the mind of the masses, who by tradition, by
education, by the press, by literature and art, by film and broadcasting, in a
continual effective propaganda, are kept in spiritual dependence.To vanquish this all-embracing power the working class has to develop a
superior power. Through the growth of capitalism itself it ever more constitutes
the majority of the people. Since the decline of small trade it holds the most
important function in society; on its productive work, its handling of the
productive apparatus, depends the life of society. By the very practice of work
and life it learns class-consciousness; the knowledge of the real character of
capitalist exploitation, thus acquired, undermines the middle-class ideas and
teachings. Through the practice of its unceasing enforced fight to improve or
uphold its working and living conditions it acquires the strong unity, the
solidarity, the devotion to common class interests, that makes the separate
individuals into a solid unbreakable block able to withstand and at last to
break the power of the foe.The belief that the working class can win mastery by parliamentary majority
means that it is sufficient if only one of its factors of power is used, namely
the large number, possessing only a first trace of class-consciousness. But more
than number counts social importance. A majority class without an important
function in social economy cannot win a ruling place, cannot even keep it when
it had it before; thus the proletarian class in ancient Rome. The social
importance of the modern proletarian class is the chief warrant that it will be
able to win; and this is entirely left out of play in the election of a
parliament. In its most direct coercive force it comes forward in a political
strike, such as in 1893 in Belgium, by which universal suffrage was conquered
from an unwilling-ruling class. Moreover, in the voting for a socialist
candidate class-consciousness is needed in its most primitive form only, not in
its necessary, more developed character of broad knowledge among the workers
about social structure, involving their capability of managing their own work,
social production. What forms their special class-character, what constitutes
the chief strength of the working class, their strong unity- and
community-feeling, the most essential condition for defeating the power of
bourgeoisie and State, remains entirely unused.The conquest of mastery and freedom by the working class will be a hard and
difficult fight. It is by means of the exigencies of this fight, through its
sacrifices, its hardships, its dangers, in defeat and victory, that the working
class must acquire those qualities that make it strong and capable for
self-rule, for ruling society. Can simply putting secretly a name into a
ballot-box be called a fight at all? What sacrifices, what hardships, what
dangers does it impose? Parliamentary elections may afford some propaganda
increasing social knowledge, and may awaken confidence; that is all. German
Social Democracy in its great time succeeded in combining this with a broad
organisation and instruction of the masses; but the real force to withstand
capitalist and State power was lacking; so it had to submit when called up for
war, and after that the decline went on. According to the abstract doctrine of
parliamentarism a socialist majority in Parliament could be elected by masses
submissive, ignorant, and selfish, as they were in the first rise of capitalism.
Practically, to be sure, this does not happen; on the reverse it might be
surmised that just an instinctive feeling of the workers that in this way
freedom and mastery cannot be won, deters them from voting for socialist
revolution and directs their vote to immediate reforms of capitalism.Thus our first remark can be summarised: in parliamentary action the least
essential only of the power factors of the working class are used.Socialism or communism, in their original sense, means that the workers take
their lot entirely in their own hands. In the conception of parliamentary
conquest of power they put their lot into the hands of a parliament and a
government; these have to transform society and abolish capitalism by means of
parliamentary acts and governmental regulations. Parliament and Government do
the essential work; the workers after having voted play a mostly passive role.
That is the reason why no qualities were needed in the working class. The
conquest of power could look so easy because it is no conquest at all: new
better rulers have replaced the old bad rulers. The workers are no masters of
the means of production; production is organised and regulated by the State, the
community as it is called, i.e., in reality by the State organs, the officials;
the workers can exert their influence only indirectly. Those who do the
essential work, needs command the work. So the outcome of a parliamentary
conquest cannot be other than State-socialism, based on public ownership of the
production apparatus. State organs and officials practically having the
direction, the disposal over production, over the product, over its distribution
between workers, officials, reparation funds, etc., by necessity develop into a
new ruling and exploiting class.The conception of parliamentary conquest of socialism was natural, and came
up by necessity, in the 19th century when the working class was a powerless mass
of sufferers. The only way to liberate them from exploitation and to annihilate
capitalism under such conditions was the legislative action of State power in
the hands of clear-sighted socialists. This liberation was to be the glorious
task of Social democracy, as an array of leaders, intellectuals, politicians,
and revolutionists, backed by the host of followers and adherents. The decline,
afterwards, of social democracy, visible in socialist and communist parties
standing or governing for one or another form of capitalism, indicates that this
conception now is obsolete. The working class is entering into another period of
its fight. It is another class now than it was a century ago; it is growing into
a social power by its mere presence determining all politics; it begins to feel
confident that it will be able to win social ascendancy. Now the belief that
they have only to vote and that others will liberate them, can only have a
paralysing effect on the exertion of their own forces. They have to face the
hard but promising truth that they have to do it all themselves, alone.State power, Government, when organising and directing production, does it by
command, from above. The working class organising and directing production,
i.e., their own work, the contents of their life, does it from below, by means
of mutual understanding, based on community feeling. For this purpose they have
to build up a social organisation of production expressing and ascertaining the
self-action of mankind in its productive work, different, hence, from political
organs embodying foreign command. Such an organisation of self-action completely
under control of the workers, is usually denoted by the name of council
organisation. In the future fight for social dominance this form of organisation
is likely to replace parliament — the genuine instrument of the middle
class — as an instrument of the revolutionary working class.Even when in a country with so mighty parliamentary traditions as England, in
the turmoil of heavy social contests parliaments are elected with genuine
socialist majorities, the essential work of new organisation must be done in the
shops by the workers themselves. The transformation of society cannot be
accomplished by decrees from above; it consists in the establishment of new
working conditions, of new mutual relations in the enterprises, the units of
practical life. In the French Revolution the farmers had already taken the land
and burned the feudal titles, the urban citizens had already taken
administration and jurisdiction in then own hands before legislation formulated
and enacted the new conditions.Our second remark may be summarised: parliamentarism cannot bring freedom and
mastery to the working class, but only new masters instead of the old ones.Whilst on the parliamentary field in all countries the parties are fighting
which of them shall, in its special way, rule and direct the working masses,
these themselves are involved in a heavy fight against the masters, ever again
exploding in strikes. It is a continual struggle for life itself, the only
actual class struggle. Declining, devastated and impoverished capitalism can
keep itself up only by pressing down the life and working conditions of the
working class to the utmost. It is not that the capitalists have grown more
greedy than before; it is the simple fact that the production apparatus has
greatly been destroyed by world war. To rebuild it capitalism has to spend more
of the total social labour on the means of production, less on the articles of
consumption; this means, since the former are the property of the capitalists,
that their share in the total product must increase, the share of the working
class has to diminish. This is the meaning of "reconstruction." Thus, all powers
of capitalism are put to work to intensify the exploitation, to press down the
standard of living, to hurry up the tempo of the work, to squeeze labour power
to the utmost. Since the single capitalists are not able to do that sufficiently
their common organ, the State, supplies its physical and moral power towards
this noble aim of saving the capitalist world. So the workers have to resist in
an embittered struggle; they are fighting for their very life, against all the
powers dominating them, the capitalists, the State, the Unions, the political
Parties. They break forth in spontaneous wild strikes, ever again, because every
just won increase of nominal wage lags behind the rising costs of living. In
these strikes they stand single-handed, over against an entire hostile world.
Here they can only have success by developing those moral and spiritual
qualities: boldness, solidarity, devotion, endurance, that once will enable them
to win freedom.Whereas in all its other actions the working class acts in the role of
followers directed and led by others, in the wild strikes we see them resume
their own liberty of action, direct their action themselves. That means a
fundamental jump forward. But these actions as yet are too limited to bring
important results. They are spontaneous outbursts against unbearable conditions,
but they are lacking in consciousness of wider scopes. As separate little squads
the striking groups are defeated one by one. Once this consciousness arises and
the strikes grow into a mass character they are by necessity directed against
the State organs as their most powerful and direct foe. The constraining power
of the State trying to crush them has to be attacked and defeated in
mass-actions by the superior firmly welded power of the united working
class.This is the significance of the strike movements flaring up again and again
in different countries. They should be attentively followed, studied and
supported by every socialist. We cannot determine in advance what forms of
action will be serviceable in future; the creative power of a fighting class
will devise them in future as it did in the past. But the essential thing is
that in the present struggles those capabilities and powers are engendered that
will form the basic conditions of the workers' revolution.Thus our third remark is: the most heavy and genuine class fight of the
workers against the capitalist class going on at present and preparing them for
revolution, stands outside the realm of parliamentarism. 
Left Communism Subject Archive |
Pannekoek Archive
