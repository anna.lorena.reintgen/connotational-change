
Trying days set in for us when we got back to Geneva after the
congress. First of all, Russian emigrants came pouring in from
other Russian emigrant colonies abroad. League members came,
asking: "What happened at the congress? What was the trouble
about? Over what was the split?"
Plekhanov was fed up with these questions and one day he
related: "X arrived. Kept asking me questions and repeating: 'I am
like Buridan's ass.' I asked him: 'What has Buridan got to do with
it?' "
People began to arrive from Russia too. Among them was Yerema
from St. Petersburg, in whose name Vladimir Ilyich had addressed
his letter to the St. Petersburg organization the year before. He
promptly sided with the Mensheviks, and called on us. He put on an
air of deep tragedy when he came in and turned to Vladimir Ilyich,
exclaiming: "I am Yerema!" Then he began to talk about the
Mensheviks being right. I remember another man, a member of the
Kiev Committee, who was anxious to know what changes in technique
had led to the split at the Congress. I just stared at
him – baffled by such a primitive understanding of the correlation
between "basis" and superstructure." I never thought it could
exist.
People who had been assisting the cause with money or by
offering their apartments for secret rendezvous and so forth,
withdrew this help under the influence of Menshevik agitation. I
remember an old acquaintance of mine coming with her mother to
Geneva, where she had a sister. When we were children we had
played together at such thrilling games of travellers and savages
living up in the trees that I was overjoyed to hear she had
come. She was a not-so-young girl now and quite a stranger. The
subject of the assistance that their family had always rendered
the Social-Democrats was mentioned. "We cannot give you our
apartment now for secret rendezvous," she said. "We highly
disapprove of this split between the Bolsheviks and the
Mensheviks. These personal squabbles are very bad for the cause."
Ilyich and I had some strong things to say about these
"sympathizers" who belonged to no organization and imagined that
their accommodation and paltry donations could influence the
course of events in our proletarian Party!
Vladimir Ilyich wrote at once to Clair and Kurz in Russia,
telling them what had happened. Beyond expressing their
astonishment, they were unable to give any helpful advice, and
seriously suggested recalling Martov to Russia and hiding him away
in some remote corner to write popular pamphlets. It was decided
to bring Kurz over from Russia.
After the congress Vladimir Ilyich did not object when Glebov
suggested co-opting the old editorial board – better to rough it
the old way than to have a split. But the Mensheviks refused. In
Geneva Vladimir Ilyich tried to make it up with Martov, and wrote
to Potresov, reassuring him that they had nothing to quarrel
about. He also wrote to Kalmykova (Auntie) about the split, and
told her how matters stood. He could not believe that there was no
way out. Obstructing the decisions of the congress, staking the
work in Russia and the efficacy of the newly formed Party struck
him as sheer madness, something unbelievable. At times he saw
clearly that a rupture was unavoidable. He started a letter to
Clair once, saying that the latter simply could not imagine the
present situation, that one had to realize that the old relations
had radically changed, that the old friendship with Martov was at
an end; old friendships were to be forgotten, and the fight was
starting. Vladimir Ilyich did not finish that letter or post
it. It was very hard for him to have to break with Martov. Their
work together in St. Petersburg and on the old Iskra had
drawn them close together. Extremely sensitive, Martov in those
days had been very quick at grasping Ilyich's thoughts and
developing them in a talented manner. Afterwards Vladimir Ilyich
had fiercely fought the Mensheviks, but whenever Martov's line
showed a tendency to right itself, his old attitude towards him
revived. Such was the case, for example, in Paris in 1910, when
Vladimir Ilyich and Martov worked together on the editorial board
of Sotsial-Demokrat (Social-Democrat). Coming home from
the office, Vladimir Ilyich often used to tell me in a pleased
tone that Martov was taking a correct line and even coming out
against Dan. Afterward, in Russia, Vladimir Ilyich was very
pleased with Martov's stand during the July days, not because it
was any good to the Bolsheviks, but because Martov bore himself as
behooves a revolutionary.
Vladimir Ilyich was already seriously ill when he said to me
once sadly: "They say Martov is dying too." Most of the congress
delegates (Bolsheviks) went back to Russia to work. Some of the
Mensheviks remained, though, and were even joined by Dan. Their
supporters abroad grew in number.
The Bolsheviks who remained in Geneva met
periodically. Plekhanov took a very firm stand at these
meetings. He cracked jokes and cheered people up.
At last Central Committee member Kurz, alias Vasilyev
(Lengnik) arrived. The squabbles which he found raging in Geneva
had a very depressing effect upon him. He was kept pretty busy
settling disputes, sending people to Russia, and so forth.
The Mensheviks made a hit with people abroad and decided to
challenge the Bolsheviks by calling a congress of the League of
Russian Social-Democrats Abroad at which the League's delegate to
the Second Congress – Lenin – was to report back. The management
board of the League at the time consisted of Deutsch, Litvinov and
myself. Deutsch pressed for the congress, but Litvinov and I were
against it. We knew only too well that under the prevailing
conditions the congress would degenerate into a downright
brawl. Deutsch thereupon reminded himself that two other members
of the management board were Vecheslov, who lived in Berlin, and
Leiteisen, who lived in Paris. Although they had lately taken
no part in the board's activities, they had not officially
resigned from it. They were called upon to vote, and plumped for
the congress.
Just before the League congress, Vladimir Ilyich, letting his
thoughts wander, ran into a tramcar while out cycling and very
nearly had his eye knocked out. He came to the congress pale
and bandaged. The Mensheviks attacked him with bitter hatred. I
remember one shocking scene when Dan, Krokhmal and others with
furious faces leapt to their feet and banged the tops of their
desks.
The Mensheviks were numerically stronger than the Bolsheviks
at the League congress. Besides, the Mensheviks had more
"generals" on their side. They adopted League Rules which turned
the League into a Menshevik stronghold, gave them publishing
facilities, and made the League independent of the Central
Committee. Kurz (Vasilyev) on behalf of the C.C. then demanded
that the Rules should be modified, and as the League resisted
this, he declared it dissolved.
The uproar raised by the Mensheviks was too much for
Plekhanov's nerves. He declared: "I can't shoot at my own
side."
At the meeting of the Bolsheviks Plekhanov said we ought to
compromise. "There are moments," he said, "when even the
autocracy is obliged to make concessions." "That's when we say it
vacillates," Liza Knuniants threw in. Plekhanov glared at her.
In order, as he said, to preserve peace in the Party
Plekhanov decided to co-opt the old Iskra editorial
board. Vladimir Ilyich resigned from the board, saying that he
would no longer collaborate and did not insist even on his
resignation being reported. Plekhanov could try and make peace if
he wanted; he, Lenin, would not stand in the way of peace within
the Party. Not long before this he had written to Kalmykova,
saying: "Quitting the job is a dead end." In resigning from the
editorial board, that was what he was letting himself in for, and
he realized it. The opposition further demanded that their
representatives should be co-opted on the C.C., that two
seats should be given them on the Council, and that the decisions
of the League congress should be recognized as valid. The Central
Committee agreed to co-opt two members of the opposition, to give
them one seat on the Council, and to gradually reorganize the
League. But peace there was none. Plekhanov's concession had
encouraged the opposition. Plekhanov insisted on the second
representative at the C.C. Rou (alias Horse, whose real name was
Galperin) standing down from the Council in favour of a
Menshevik. Vladimir Ilyich hesitated long before agreeing to this
new concession. I remember the three of us – Vladimir Ilyich, Rou
and I – standing on the shore of Geneva Lake, which was in a
turbulent mood that evening. Rou urged Vladimir Ilyich to agree to
his resignation. At last Vladimir Ilyich gave in, and went to
Plekhanov to tell him that Rou would stand down.
Martov put out a pamphlet State of Siege, full of
the wildest accusations. Trotsky also wrote a pamphlet entitled
Report of the Siberian Delegation, in which events were
depicted quite in the Martov strain, Plekhanov being represented
as a pawn in the hands of Lenin, and so forth. Vladimir Ilyich sat
down to write his reply to Martov – his pamphlet One Step
Forward, Two Steps Back, in which he gave a detailed analysis
of what took place at the congress.
Meanwhile, a struggle was going on in Russia too. The
Bolshevik delegates reported back on the congress. The programme
and most of the resolutions adopted at the congress were hailed
with great satisfaction by the local organizations. All the more
puzzling to them was the position of the Mensheviks. Resolutions
were passed demanding submission to the congress decisions. One of
the most energetic of our delegates at the time was Uncle (Lydia
Knipovich), who, being an old revolutionary, could simply not
understand how the congress decisions could be flouted in such a
way. She and other comrades wrote encouraging letters from
Russia. The local committees sided with the Bolsheviks one after
another.
Clair arrived. He had no idea what a barrier had arisen
between the Bolsheviks and the Mensheviks and thought it was still
possible to reconcile them. He went to see Plekhanov only to
convince himself that a reconciliation was absolutely
impossible. He went back in a depressed mood. Vladimir Ilyich was
gloomier than ever.
Early in 1904 Celia Zelikson, Baron (Essen), a
representative of the St. Petersburg organization, and the worker
Makar, arrived in Geneva. All were Bolshevik supporters. Vladimir
Ilyich saw them often. They talked about the work in Russia and
the quarrel with the Mensheviks. Baron, who was quite a young man
at the time, was enthusiastic about the St. Petersburg work. "Our
organization is being run on collective lines now," he said. "We
have separate bodies working – a propagandists' group, an
agitators' group, and an organizers' group." Vladimir Ilyich heard
him out, then asked: "How many people have you in the
propagandists' group?" Baron was a bit put out, and answered: "I'm
the only one so far." "Not many," observed Ilyich. "And how many
have you in the agitators' group?" Baron reddened to the roots of
his hair and said: "I'm the only one so far." Ilyich held his
sides with laughter. Baron laughed too. Ilyich always had
the knack, by means of one or two probing questions, of putting
his finger on the weakest spot, and sifting the real facts from
the husk of fine schemes and spectacular reports.
Afterwards Olminsky (M. S. Alexandrov), who joined the
Bolsheviks, and Beast, who had escaped from remote exile,
arrived.
After her escape front exile Beast was full of cheerful
energy, which communicated itself to all around her. Not a shadow
of doubt or indecision weighed on her mind. She made fun of
everyone who had the blues and moped over the split. All these
emigrant squabbles did not seem to affect her. At that time we had
started holding weekly "at homes" in Séchéron to
bring the Bolsheviks closer together. We never got down to any
"real" talk at these gatherings, but at least they helped to
dispel the gloom cast upon us by all these squabbles with the
Mensheviks. It was excellent fun to hear Beast strike up a
rollicking Song about "Vanka," and bald-headed Yegor, a worker,
join in the chorus. Yegor had gone to have a heart-to-heart talk
with Plekhanov, and had even put on a starched collar for the
occasion. But he had come away disappointed and saddened. "Cheer
up, Yegor. We'll win the day. Come on, let's get on with 'Vanka'!"
Beast said. Ilyich would
cheer up too – this boisterous gaiety dispelled gloomy thoughts.
Bogdanov appeared upon the scene. Vladimir Ilyich was not very
familiar with his philosophical works at the time, and did not
know him at all personally. Plainly, though, he was a man of
calibre as far as the Party was concerned. He was on a temporary
visit, and had extensive connections in Russia. The period of
distressing squabbles was coming to an end.
Hardest of all was it for Vladimir Ilyich to break with Plekhanov.
In the spring Ilyich made the acquaintance of the old
Narodopravets revolutionary Natanson and his wife. Natanson was a
splendid organizer of the old type. He knew lots of people, was
very good at sizing up a man, and could tell what he was capable
of and what job he was best suited for. What struck Vladimir
Ilyich about him was the fact that he was perfectly familiar with
the personnel of both his own and our Social-Democratic
organizations, which he knew better than many of our own Central
Committee members. Natanson had lived in Baku, and knew Krasin,
Postolovsky and others. Vladimir Ilyich thought that Natanson
could be persuaded into becoming a Social-Democrat. He was very
close to the Social-Democratic stand-point. We heard afterwards
that that old revolutionary had sobbed when, for the first time in
his life, he had seen an imposing demonstration in Baku. On one
point Vladimir Ilyich and he could not see eye to eye: Natanson
disagreed with the Social-Democrats' approach to the
peasantry. The wooing of Natanson lasted a fort-night. Natanson
was on familiar and even intimate terms with Plekhanov. Vladimir
Ilyich fell into conversation with him about our Party affairs and
the split with the Mensheviks. Natanson offered to talk things
over with Plekhanov. He came away from Plekhanov in a state of
perplexity. We would have to compromise, he said.
The romance with Natanson was broken off. Vladimir Ilyich was
annoyed with himself for having discussed Social-Democratic
affairs with a man of another party, who had acted as a sort of
go-between. He was annoyed with Natanson as well as with
himself.
Meanwhile, in Russia, the Central Committee was pursuing a
double-faced conciliatory policy, while the local committees
backed the Bolsheviks. It was necessary to convene a new congress
based on Russia.
In protest against the July declaration of the Central
Committee, which prevented him from defending his point of view
and communicating with Russia, Vladimir Ilyich resigned from the
C.C., and the Bolshevik group, numbering twenty-two, passed a
resolution calling for a Third Congress.
Vladimir Ilyich and I took our rucksacks and went out hiking
in the mountains for a month. Beast joined us, but soon gave it
up, saying: "You people like to go to places where there isn't
even a living cat, but I must have human society." Indeed, we
always chose the loneliest trails that led into the wilds, away
from any people. We tramped about for a month, not knowing today
where we would be tomorrow. After a weary day we would throw
ourselves on our beds dead-tired and fall asleep
instantaneously.
We had very little money, and lived mostly on cold food such
as eggs and cheese, washed down with wine or water from a spring;
we rarely had a proper dinner. At one little inn patronized by
Social-Democrats a worker gave us a good tip. "Don't dine with the
tourists, but with the coachmen, chauffeurs and labourers – it's
twice as cheap and more filling." We took his advice. The civil
servants and shopkeepers who ape the bourgeoisie would sooner stop
going out altogether than sit down at the same table with a
servant. This middle-class snobbery is very widespread in
Europe. They talk a lot about democracy there, but to sit down at
the same table with the servants – not at home, but in a smart
hotel – is more than any snob trying to make his way in the world
can stomach. It gave Vladimir Ilyich special pleasure, therefore,
to sit down in the common room to have his meal. He ate there with
a keener relish and was full of praise for the cheap but
satisfying food. After that we would sling on our rucksacks and
continue on our way. The rucksacks were pretty heavy. Vladimir
Ilyich had a fat French dictionary in his, while I had in mine a
no less heavy French book which I had just received for
translation. Neither the dictionary nor the book, however, had
once been opened during our trip. It was not at dictionaries we
looked, but at the snow-capped everlasting mountains, at blue
lakes and turbulent waterfalls.
A month of this restored Vladimir Ilyich's nerves to
normal. It was as if he had bathed in a mountain stream and washed
off all the cobwebs of sordid intrigue. We spent August with
Bogdanov, Olminsky and Pervukhin in a remote village by the shore
of Lac de Bré. The plan of work was arranged with Bogdanov,
who proposed enlisting the cooperation of Lunacharsky, Stepanov
and Bazarov. Plans were made to publish our own organ abroad and
develop agitation for a congress in Russia.
Ilyich became his cheerful old self again. His return from a
visit to the Bogdanovs in the evening was always announced by a
furious barking from the chained dog outside, whom he teased in
passing.
We went back to Geneva in the autumn and moved from the
suburbs nearer to the centre. Vladimir Ilyich joined the
Société de lecture, where there was a vast library
and excellent facilities for work. They received lots of
newspapers and magazines there in French, German and English. It
was a very convenient place to study in. The members of the
society – for the most part old professors – seldom visited the
library, and Ilyich had the room to himself there, where he
could write, pace up and down, think over his articles, and take
down any book he wanted from the shelves. He could rest assured
that no Russian comrade would come there and start telling him
about the Mensheviks having said this and that or played a dirty
trick in such-and-such a place. He could think there without
having his thoughts diverted. And there was plenty to think
about.
Russia has started the Japanese War, which glaringly revealed
all the rottenness of the tsarist monarchy. Not only the
Bolsheviks, but the Mensheviks and even the liberals, too, were
defeatists in this war. A storm of popular protest was rising. The
working-class movement entered a new phase. News of mass public
meetings held in defiance of the police, and of direct clashes
between the workers and the police, reached us ever more
often.In face of the growing mass revolutionary movement petty
factional squabbles did not worry us as much as they recently
had. These squabbles, though, sometimes assumed ugly forms. The
Bolshevik Vasilyev, for instance, arrived from the Caucasus and
wanted to make a report on the situation in Russia. At the opening
of the meeting, however, the Mensheviks demanded the election of a
pre-siding committee, although it was just an ordinary report
which any Party member could come and hear, and not an
organizational meeting. The Mensheviks tried to turn every report
or lecture into a kind of electoral fight, hoping in this way to
stop the mouth of the Bolsheviks "by democratic means." Things
very nearly came to a hand to hand scuffle, a fight, over the
insurance fund. During the uproar Bogdanov's wife Natalia had her
mantle torn, and someone got knocked down. But now it did not
affect us half as much as it used to.
All our thoughts were now in Russia. One felt a tremendous
responsibility in face of the workers movement that was growing
out there – in St. Petersburg, Moscow, Odessa and other places.
All parties – liberals and Socialist-Revolutionaries began
to show themselves in their true colours. The Mensheviks, too,
showed their real face. It became clear now what divided the
Bolsheviks and the Mensheviks.
Vladimir Ilyich had implicit faith in the proletariat's class
instinct, its creative powers, and historic mission. This faith
had not come suddenly to Vladimir Ilyich, but had been hammered
out during the years when he had studied and pondered Marx's
theory of the class struggle, when he had studied Russian
realities, and learnt, in fighting the ideas of the old
revolutionaries, to offset the heroism of the solitary fighter by
the strength and heroism of the class struggle. It was not just
blind faith in an unknown force, but a deep-rooted belief in the
strength of the proletariat and its tremendous role in the cause
of working-class emancipation, a belief founded on a profound
knowledge and thorough study of the facts of life. His work among
the St. Petersburg proletariat had helped to identify this faith
in the power of the working class with real live people.
At the end of December the Bolshevik newspaper Vperyod
(Forward) began to appear. The editorial board, in addition
to Ilyich, had Olminsky and Orlovsky on it. Shortly afterwards
Lunacharsky arrived to lend a hand.
His impassioned articles and speeches were consonant with the Bolsheviks' mood at the time.
The revolutionary movement in Russia was growing, and with it
grew our correspondence with Russia. It soon reached a volume of
three hundred letters a month, which was an enormous figure for
those days. And what rich material it provided Ilyich with! He
knew how to read workers' letters. I remember one from quarry
workers in Odessa. It was a collective letter written in several
uncultivated hands without subjects and predicates, stops and
commas, but full of inexhaustible energy, a readiness to fight to
the victorious end. That letter was colourful in every word,
naive, but unshakable in its conviction. I do not remember what it
was about but I remember how it looked – the paper and the watery
ink. Ilyich read that letter over and over again, and paced up and
down deep in thought. Not for nothing had the quarry workers of
Odessa taken such pains when writing to Ilyich: they had written
to the right man, one who could best understand them.
A few days after this letter, we received one from Tanya, a
young Odessa propagandist, who gave a faithful and detailed report
of a meeting of Odessa artisans. Ilyich read that letter, too, and
sat down at once to answer Tanya. "Thank you for your
letter. Write more often. We are tremendously interested in
letters describing ordinary workaday activities. We get
so few of them, worse luck." Almost in every letter Ilyich asked the comrades in Russia to
give us more contacts. "The strength of a revolutionary
organization is in the number of its contacts," he wrote to Gusev,
and asked him to put the Bolshevik centre abroad in touch with the
youth. "Some of us have a kind of idiotic, philistine,
Oblomov-like fear of the youth," he wrote. Ilyich wrote to Alexei
Preobrazhensky, an old Samara friend, who was then living in the
country, asking him to put him in touch with the peasants. He
asked the St. Petersburg comrades to forward original workers'
letters to the Centre abroad and not just extracts or
resumes. These letters told Ilyich more clearly than anything
else that the revolution was drawing near, was rising. Nineteen
'Five was on the threshold.
  
Read next section |
Krupskaya Internet Archive |
Marxists Internet Archive
