MIA  >  Archive  >  Cliff  >  Problem of M.E. <p. 110>The economic development of the different Arab countries is determined by different combinations of fundamentally the same factors. While in Egypt a space of time separated the destruction of local handicrafts and manufacture (by the competition of European industrial products) from the emergence of a modern industry, and while in Palestine handicrafts were too backward for their ruin to be a factor of any importance, in Syria their destruction by the competition of foreign goods took place at the same time as a modern industry arose there; and their destruction has great importance owing to their advanced development. (Syria was the most important centre of industrial supply in the whole Ottoman Empire.) Seeing that foreign capital was attracted less to Syria than to Egypt (which has a greater natural richness, is situated next to the Suez and so on) and seeing that the layer of commercial bourgeoisie and rich feudal lords who can pass over to industrial activity, is much smaller and weaker in Syria than in Egypt (for the same reasons as above) the penetration of imperialism was revealed not in its constructive, exploitative aspect, but almost only in its negative, destructive aspect: modern industry absorbs only a tiny minority of the artisans and workers who lose their livelihood from the old handicrafts, manufacture and home industry.During the middle ages Syria was considered in the forefront of civilized countries. Her handicrafts achieved a world-wide reputation, and her industrial development surpassed any in Europe. Her status declined under Turkish rule, and declined every so much more during the nineteenth century, when she had to compete with mechanized European industry.Even so, until the First World War, Syrian handicrafts and manufactures were well-developed and diverse.The report of the French Government to the League of Nations in 1931 estimated the number of people employed in 1913–14 in the domestic weaving industry alone at 100,000. A more recent investigation on the state of industries and handicrafts, undertaken by the government at the end of 1937, which also gives material on the pre-war state, showed that in 1913 309,255 people in the whole country worked in industry and handicrafts. Actually this number includes various branches which do not belong to industry or handicrafts, such as picking and packing citrus fruits, work on the railways etc. But even if we deduct those employed in these branches from the total amount, we still have a number of 250–280 thousand people working in industry and handicrafts.The total number of inhabitants in Syria at that time was 2.5 millions. As the number of earners in the East constitutes generally less than one-third of the total population, we may assume that the number of earners in Syria did not go above 850,000 which means that industry employed 25–35 per cent of all earners. This percentage is not at all a low one, and compare well with a number of European countries. Some with approximately the same percentage are: Italy – 30.4 per cent in 1931, Sweden – 31 per cent in 1920, France – 33 per cent in 1926, Hungary – 24 per cent in 1930.This industry, however, was in the main domestic and small workshop industry, each enterprise employing only a few workers. Thus of the 309,000 handicraft and industrial workers in 1913, about 208,000 were domestic workers. The number of piece workers among the total handicraft workers was even greater, their number reaching 247 thousand. Large manufacturing works employing some scores of workers were exceptional. In Syria and Palestine combined (at that time both had common administrative boundaries) there were less than 100 works employing more than 50 workers each, and about 12 employing more than a hundred workers. The only larger-scale industry was silk spinning. In 1912 there were 169 spinning works with 9,547 spindles, an average of 56 spindles per factory. There were 89 works with more than 50 spindles, and 20 with 100 and more. Each spindle employed one worker. Although these industries were small and primitive, the quality of their <p. 111> products was not poor at all. Syrian cloths and metal goods from Damascus workshops had a high repute in the markets of the Middle East and even outside them. Lebanese silk spinning and weaving held a leading position, and metal, soap and leatherworks also held their own easily.Before the First World War Syria was the centre of the industry and handicrafts of the whole Ottoman Empire. Her development far exceeded that of Turkey on the one hand, and that of the rest of the Arab regions on the other. And even though under pressure of the Western Powers, Turkey was forced not to raise the customs tariff above 8 per cent, and although this was an inclusive tariff covering all imported goods, both raw materials and fabricated goods, and although internal tariff walls were set up between the different provinces of the Empire – the old Syrian industry managed to hold its own and flourish.The improvement which the ‘Young Turks’ introduced after their revolution in 1908 – raising the customs tariff to 12 per cent, abolishing the internal tariff walls between the provinces of the Empire – encouraged Syrian industry.With the end of the First World War a decisive change in the position of the old Syrian industry took place. Tariff walls were set up dividing the centres of handicrafts in Syria from their former markets – Turkey, Iraq and Egypt. Only with Palestine was an agreement concluded for ‘free’ trade, but this was Syrian industry’s poorest foreign market. At the time under the influence of contact with the west, the mode of life, clothing, buildings, etc. changed, and the demand for the products of Syrian industry, whose value was high, but whose price likewise, dwindled progressively. The pushing aside of silk, which occupied such an important place in the traditional industry of the Lebanon, in favour of artificial silk, was characteristic of this process of the change in demand.The policy of the French Government had the same influence on Syrian industry. It was interested in developing only those branches which could satisfy French industry’s need for raw materials (raw silk, silk threads, raw cotton) and to preserve the Syrian market as much as possible for French goods. The agents of the French in Syria – the feudal lords and merchants – who constituted the administration were not interested in the development of local industry, but in the cheapness of the industrial products they bought. France’s customs policy thus straitened the position of Syria’s old industries. The world crisis of 1929–33 hastened their decline and the dumping of goods, especially by Japan later on further affected them. The French currency policy which tied the Syrian-Lebanese pound to the franc also harmed Syrian industry’s competitive ability during the great world crisis, when the value of the yen, sterling and the dollar declined, while the value of the franc did not change. The agrarian crisis which affected the purchasing power of the agriculturist who was the main consumer of Syrian industry, and the shrinkage of tourist traffic, also gravely harmed the position of traditional Syrian industry.The decline of Syrian industry is characteristically revealed in the following figures of the state of the weaving industry:–Number of Looms to Number of Workers
[No. Ls/No. Ws]YearAleppo
No. Ls/No. WsHoms
No. Ls/No. WsDamascus
No. Ls/No. WsHama
No. Ls/No. Ws1891  5,884 /      ?      4,000 / 28,000  3,000 / 21,000   700 / 4,900191318,801 / 67,750— / —— / —— / —1921     ?     / 22,500— / —— / —— / —1929  3,000 /10,000  2,000 /     ?        700 /      ?       800 /     ?   1931  1,500–2,000 /   9,000             1,500 /   3,000     550 /   3,500   500 /    500The decline in the silk spinning, leather and soap industries was of similar dimensions.The total number of industrial workers dropped from 250–280 thousand in 1913 to 50–60 thousand in 1932.After the great economic crisis of 1929–33 came a change in Syrian industry. In 1932 an economic paper in Lebanon wrote:‘As far as industry is concerned, it is indeed a fine principle to defend it, but it would be much more explicable if the times were easy. In our difficult economic position we must think of preserving what exists and not creating something from nothing. And by the way, we have not, nor ever will have – fortunately – a great industry after the pattern of the countries of the West.’ (Commerce <p. 112> du Levant, December 31, 1932).Yet it was just in these years so pessimistically spoken about that a turn in the direction of modern industrialisation did take place. The serious economic crisis which overtook Syrian industry and agriculture flooded the towns with unemployed. According to the official report of the French government to the League of Nations, in 1931 there were 30,000 unemployed in the four towns Damascus, Aleppo, Homs and Hama alone. This number is, however, greatly minimised. According to the report of the British consul in Beirut, the number of unemployed in Damascus alone was 30,000, while the government report puts it at 12,000 there, with another 18,000 in the other three towns mentioned above. Thousands of unemployed also roamed the streets of Beirut, Tripoli, Zahleh and other towns.With the cutting of wages to a very low level and the decline in prices of agricultural products, the social and national tension was increased, and the French authorities were compelled to decrease their pressure by encouraging local industry, which could absorb at least some of the unemployed. The pauperisation of the masses of people during the crisis also caused them to buy the cheapest industrial products, even though their quality was poor, and Japanese goods began to oust French products from the markets of Syria. In 1931 Japan already accounted for 42 per cent, in 1932 – 58 per cent and in 1933 – 70 per cent. Confronted with this line-up of contradictions, imperialism, their father and source, was forced to take measures to weaken them. This led to the policy which the French authorities began to follow in the wake of the economic crisis, of defensive custom tariffs. In 1931 the customs duties on machines, machine parts, woollen threads, coal and various chemical materials were exempted from customs duties, and the duties on packing-paper, soda etc. reduced. In 1932 the customs duties on cotton goods were raised to 40 per cent and those on silk good to 50 per cent of what the goods were worth. The duty on stockings, shoes, matches and cement was also considerably raised.Besides the customs policy, another factor also encouraging local industry was the cheapness of labour power and raw materials. Through the pauperisation of the workers and artisans of the old industry and the impoverishment of the agriculturists, modern industry in Syria grew.Furthermore, the reduction of trade due to the economic crisis caused much merchant capital to be superfluous and to seek investment in industry, and at the same time, because of the crisis itself, many Syrian and Lebanese emigrants returned to their motherland from America with capital they had accumulated there. According to one estimate, 60 per cent of Syria’s industrial capital, besides what belonged to the French, was in the hands of these emigrants who had returned to Syria. Thus an important condition for the development of a modern capitalist industry was created.But thus network of factors has its contradictions, and it cannot be the basis of a large-scale development of industry, so that even if a modern industry has grown up, its scale has remained relatively limited, and even though the old industries have begun to revive as a consequence of those factors, nevertheless this revival has been pallid and meagre.The following figures will give some indication of the growth of modern Syrian industry. In 1935 modern Syrian industry employed 5,800 workers. By 1937 the number nearly trebled, amounting to 18,670, which is relatively a very great increase for four years. Some scores of enterprises employ more than a hundred workers. The cement factory in Shaqqa employed about 800 workers in 1937–8; the wool weaving factory La Lainière Nationale employed 520 workers. <p. 113> During the course of six years cement production increased 5½ times, biscuit production 12 times, chocolate production 9½ times, cotton thread production four times, and socks and stockings production 9 times.However modern industry has absorbed only a very small portion of the workers and artisans released from the traditional industries, and because of this, the process of development of modern industry in Syria and Lebanon is accompanied by a great increase in the exploitation of the workers, and a great decline in the real wages, as the following table makes clear:–Changes in Real Wages 1913–1937Monthly expenditure of a worker’s family of five persons and daily earnings
of the male family head occupied in industry, Dec. 1913 and Dec. 1937 Monthly Expenditure
(francs)Daily Earnings
(francs) 1913193719131937Aleppo29350212.210.5Beirut35366316.214.0Damascus32350614.810.7Average32356014.411.7Thus living expenses rose by 73 per cent while the nominal wage decreased by 20 per cent. The number of working days necessary to finance a family therefore nearly doubled:–No of Daily Earnings Necessary
n order to Cover Monthly Expenditure 19131937Aleppo23.947.9Beirut21.847.4Damascus21.747.3In 1937 other members of the family besides the father, were forced to supplement the income. Seeing that the pay of a woman is less than half that of a man, and the pay of a child about a quarter, the maintenance of a family which before the First World War demanded the labour of the father alone, today demands also that of his wife and two children. Obviously such an abundance of work is not to be found, and the worker is forced to lower his standard of living.
 Of course in the conditions of Syria, which for French imperialism is a strategically rather than an economically important colony producing scarcely any agricultural product which France really needs, the extent and speed of development of industry decides the extent and speed of the development of agriculture. Agriculture is subordinated to industry. This subordination is also testimony to the predominance of capitalist relations, of commercial connections, of capital, the predominance of which, however, does not stand in contradiction to the conservation of feudal relations, or even to their regeneration of the basis of the backward forces of production, the existence of a surplus population etc., which flow, first and foremost, from the slowness of the development of modern industry, the decline of the old industry, and the existence of feudal property relations in the village.The fact that imperialism does not develop new markets for Syrian agricultural products and harms the old ones (by harming the old industries which bought agricultural raw materials and food) and also that because of this the field of absorption of working hands coming from the country is restricted, means that the villager has very limited markets for his agricultural products and labour power. This increases the subjugation of the land cultivator to the landowner, usurer and merchant. Thus the outworn forms of economy are conserved and regenerated.Seeing that Syria was until the First World War an important productive <p. 114> centre of handicrafts and manufacturers for the Ottoman Empire, commerce between town and country and agriculture based on commercial relations, had developed to an advanced extent. Syrian agriculture was incomparably less based on natural foundations than that of Palestine which was merely the backward Southern part of Syria. While Palestine produced almost only products of extensive cereal culture – wheat, barley, millet and maize for the consumption of the fellah and to a small extent for the consumption of the urban population – in Syria the products of intensive agriculture – tobacco, mulberry trees for silk worms, cotton, different fruit trees – occupied a very important place. Thus agriculture in Syria rose to a high standard in comparison with that of Palestine. Natural factors – mainly the number of rivers – also contributed to this. The irrigated area in 1920 made up 10 per cent of the total cultivated area in Syria, while in Palestine it made up at most 1 per cent. The wheat yield in Syria is 80–100 kgs per dunam, while in the Arab village in Palestine it is 50 kgs in a good year. The barley yield is 100–110 kgs per dunam as compared with 60–70 kgs in Palestine, grapes 400–450 kgs per dunam as compared with 250–300 in Palestine and olives 80–100 kgs per dunam as compared with 60–80 kgs in Palestine. The Beirut and Damascus cows are of a much superior breed to the Palestine cow. The latter gives 500–600 litres of milk per year, while the Beirut cow gives 1,200 to 1,500 and the Damascus cow 2,500. But nevertheless Syrian agriculture is very backward. The wheat and barley yields are only a third of those of the developed European countries. The yield of a Beirut cow is only a third of that of a Holland cow. Unfortunately statistical material for Syrian agriculture is extremely poor, much more so even than that for Egypt and Palestine. We can therefore not compare the quantity of labour invested in Syria in the production of an agricultural unit with that of other countries. Such a comparison, if it could be made, would even more clearly show up the backwardness of Syrian agriculture as compared with the European.Because of the decline of the old industries and the lack of wide new markets for Syrian agricultural products, the extension of agriculture is very slow. This is shown clearly in a comparison of the cultivated areas in 1938 with those in 1914:–Area in 1000 dunams 19141938% increase
or decreaseWheat5,0005,400    8   Barley3,2003,400    6.3Cotton       8   380475   Grapes  850   560  65.9Total cultivated area12–13,00014,500–15,00017–19At the same time the population of Syria rose from 2 million to 3.5 million, i.e. an increase of 75 per cent.Because of the commercial character of Syrian agriculture on the one hand and the many obstacles to its development on the other, two contradictory yet complementary processes took place: the mode of production remained almost unchanged from what it had been 30 years earlier, and the crops remained nearly on the same standard, while at the same time toilers (agricultural workers, tenants and small peasants portion of the production fell, and the portion of parasitic groups (landowners, usurers and merchants) rose.It is superfluous to dwell at length on the means of production of the Syrian fellah as there is scarcely any difference between this and the Palestinian fellah’s means of production which have been fully described.Regarding the increasing pressure of the yoke of the parasitical elements on the shoulders of the fellah: it is clear that to the extent that the natural agricultural economy is destroyed, the natural boundaries of the landowners’ appetite for the surplus product are destroyed. The encroachment of the landowners in Syria upon the common lands was therefore on a much larger scale even before the First World War than in Palestine. Thus in the Damascus Wilayet, according to Ruppin in Syrien als Wirtschaftsgebiet (Berlin 1920, p. 84) 60 per cent of the land was in the hands of a small number of landowners, 15 per cent in the hands of rich peasants and only 25 per cent in the hands of the masses of fellaheen, while in Palestine about 50 per cent of the land belongs to the fellaheen.<p. 115> The rent that the tenant pays is higher than in Palestine. The director of the agricultural experimental station is Latakia describes the conditions of a tenant who gets from the landlord, besides land, working animal and seeds:‘The distribution of the harvest is as follows: from the total harvest first the tithe for the government is deducted, second a similar portion for the landlord sometimes combined with the expenses of threshing and winnowing, the remaining 75 per cent is divided into three parts two of which are given to the landlord and one to the tenant. In addition, the tenant pays the loans which the landlord supplied him with at a very high interest rate, a certain portion the tenant gives to the priest and the sheikh… In short, the tenant has left at his disposal (excluding payment of debts and interest, and payment to the priest and sheikh – T.C.) only 20 per cent of the gross product. Very often after payment of the loans the tenant is left empty-handed and he is compelled to seek a new loan under most severe conditions until the next harvest’ (Bulletin de l’Union Economique de Syrie, Paris, September 1928)According to one estimate, of all the harvest of the Syrian fellah, 30 per cent goes to the payment of interest on the loans. The currency policy of French imperialism which causes drastic fluctuations in the value of the £LS further weighs down the debt burden of the fellah. Seeing that the usurer evaluates and writes down the loans he gives to the fellah in gold pounds while giving them in £LS, every decline in the £LS causes added suffering to the fellah. These conditions are even more severe than those of the Palestinian fellah, and therefore in spite of the higher level of agricultural production in Syria than in Palestine, the standard of living of the toiler is not higher.According to an investigation undertaken in a number of villages in 1938 by the Society for the Relief of Lebanese Artisans, the average income per capita for the year was £LS 15 or 4 LS piastres per day, i.e. a little over 1½d. (Fuad Saade, L’Agriculture, richesse nationale, Beyrouth 1942, p. 27). The wage of a rural worker in Syria was about 15 Syrian piastres i.e. about 6d. The number of villagers who wandered from their villages and districts to other villages and districts to seek work already in 1924, according to a reliable calculation, reached more than 92 thousand people, and this besides those who worked as wage labourers in their own villages or in other places in their district. The total agricultural population in Syria in the 1920s was about 1.5 millions. Thus 20-25 per cent of them were without work in their district and had to wander about to seek work in other districts. These facts prove that because of the double pressure of imperialism and feudalism in the special conditions of Syria the class differentiation becomes much deeper in the Syrian countryside than the Palestinian. The agrarian relations in Syria are therefore full of revolutionary dynamite. [1]1. On many aspects of the agrarian question in Syria, which very closely resemble aspects of the question in Palestine, (such as the musha’a, the waqf, the agricultural means of production etc.) I have found it unnecessary to dwell. 
Top of the pageLast updated on 28.5.2011
Number of Looms to Number of Workers
[No. Ls/No. Ws]

Year

Aleppo
No. Ls/No. Ws

Homs
No. Ls/No. Ws

Damascus
No. Ls/No. Ws

Hama
No. Ls/No. Ws

1891

  5,884 /      ?    

  4,000 / 28,000

  3,000 / 21,000

   700 / 4,900

1913

18,801 / 67,750

— / —

— / —

— / —

1921

     ?     / 22,500

— / —

— / —

— / —

1929

  3,000 /10,000

  2,000 /     ?   

     700 /      ?    

   800 /     ?   

1931

  1,500–2,000 /   9,000           

  1,500 /   3,000

     550 /   3,500

   500 /    500

Monthly expenditure of a worker’s family of five persons and daily earnings
of the male family head occupied in industry, Dec. 1913 and Dec. 1937

 

Monthly Expenditure
(francs)

Daily Earnings
(francs)

 

1913

1937

1913

1937

Aleppo

293

502

12.2

10.5

Beirut

353

663

16.2

14.0

Damascus

323

506

14.8

10.7

Average

323

560

14.4

11.7

No of Daily Earnings Necessary
n order to Cover Monthly Expenditure

 

1913

1937

Aleppo

23.9

47.9

Beirut

21.8

47.4

Damascus

21.7

47.3

 

1914

1938

% increase
or decrease

Wheat

5,000

5,400

    8   

Barley

3,200

3,400

    6.3

Cotton

       8

   380

475   

Grapes

  850

   560

  65.9

Total cultivated area

12–13,000

14,500–15,000

17–19
