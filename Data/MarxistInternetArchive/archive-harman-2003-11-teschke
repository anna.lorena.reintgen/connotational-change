MIA  >  Archive  >  Harman  From Socialist Review, No.279, November 2003.Copyright
© Socialist Review.Copied with thanks from the
Socialist Review Website.
Marked up by Einde O’Callaghan for the Marxists’ Internet Archive.The Myth of 1648Benno TeschkeVerso £25This book sets out to attack the conventional view in the academic
discipline of International Relations which sees there being an
unchanging form of interaction between states from 1648 to the
present day. Teschke quite rightly insists the whole approach is
untenable, since the relations between states change with changes in
the social relations of production within each, and he provides
useful accounts of the relations during the medieval period, the
period of absolutism and that of modern capitalism.However, two things stymie his argument.First, he succumbs to the arcane language of the discipline he is
criticising, making many passages nearly impenetrable to those of us
who have not studied it. At points I could not help feeling that
rather than Marxism infiltrating the academy, the academy was
infiltrating and subverting Marxism.Second, he bases himself on the self baptised ‘political
Marxism’ propounded by Robert Brenner and Ellen Meiksins Wood.
This stresses the centrality of class struggle in bringing about a
change from one mode of production to another – and in
particular from feudalism to capitalism – and so takes issue
with those like Gunder Frank and Wallerstein who only point to
systems of trade and markets. But it ignores something absolutely
central to Marx’s own account – the development of the
ways of making a livelihood (‘the forces of production’).There is nothing intrinsically bad about disagreeing with Marx.
But he pinpointed the role of the forces of production for a very
important and inescapable reason. There is a tension between the
tendency of humans to improve their capacity to make a livelihood and
the fixity of existing social relations between them, and this
tension creates the pressure for societies to change. Shifts in the
forces of production are the material underpinning of the ups and
downs of class struggles. They extend the range of possibilities open
for human existence and, in doing so, enable people belonging to
different classes to envisage new ways of living.Once you take your eyes off such changes in production, then
shifts from one mode of production to another appear almost
accidental, a product of this or that great leader, or of the
accidental balance of social forces at some point in history.So it is that Teschke, following Brenner, sees a transition from
feudalism towards capitalism taking place in Britain because peasants
only half-won the battles they fought with the lords in the revolts
of the late 14th century. In France, he sees no transition as
possible because, he claims, the peasants won completely.In a somewhat similar way, what is sometimes called the ‘feudal
revolution’ or the ‘revolution of the year 1000’ –
the final consolidation of serf-based exploitation right across
Europe – is merely a result of the interplay of rivalry between
various feudal lords.What is missing in both cases is recognition of cumulative changes
in production right through the medieval and early modern period.
Teschke (again following Brenner and Meiksins Wood) dismisses these
as insignificant, even writing of ‘non-developing’
societies. In fact, as the French historian Duby has stressed, there
were massive increases in agricultural productivity (more than
doubling the output per unit of land) from the 9th to the 14th
centuries, associated with the adoption of new techniques, while Lynn
White and Jean Gimpel have shown how the production, processing and
movement of goods were transformed by the spread of innovations like
water and wind mills, the compound crank, the spinning wheel,
improved looms, the compass, cast iron, and finally the printing
press.These changes increased the weight within feudal society of
elements which were at odds with the existing social structure. They
led to the growth of trade and of towns, which were able, to varying
degrees, to break from the control of feudal lords. With this went
further growth of the market, increasing the pressure for personal
relations between people engaged in production to be replaced by cash
relations. As this happened, it was possible for kings to manoeuvre
with the urban classes to lift themselves above the rest of the
feudal lords, so creating a state, ‘absolutism’, that
looked both back to feudalism and forward to capitalism.Because he does not see this, Teschke does not see absolutism as a
stage in the transition from feudalism to capitalism, but as a social
formation standing apart from either. His is the completely
undialectical approach of saying something must be either completely
capitalist or completely non-capitalist. Nowhere is there any sense
of a connection between economic transformation in the town and
countryside with the cultural/ideological battles of the Renaissance,
Reformation and Enlightenment, and of both the economic and
ideological with the political battles in Holland, England, France
and finally right across Europe.These absurdities follow from a method which cuts history into
self contained slices and then finds only accidental connections
linking each to the slice that preceded it. The connection can only
lie in the slow advance of the forces of production – which
created the conditions in which the first embryos of capitalism could
arise, not just in Europe but in parts of Asia and Africa as well (an
issue which Teschke simply dismisses out of hand). The new embryos
could only develop further when the social forces associated with
them were capable of struggling against the resistance encountered
from the old society. That is where class struggle came in –
but it had to be thoroughgoing class struggle over political power,
directed towards the centres of political power in the towns, not
simply over economic issues restricted to the countryside. Force, as
Marx put it, is the midwife of the new society. But it cannot be
effective without a prior process of conception in which changes in
production play a role.What passes for ‘political Marxism’ ends up ignoring
both the productive base out of which classes grow and the heights of
the ideological and political superstructure where they wage their
world-historical struggles. As a result Teschke’s book makes
often perceptive observations, but fails to bring them together into
a viable historical account. 
Top of the pageLast updated on 27 December 2009Socialist ReviewSocialist ReviewThe Myth of 1648