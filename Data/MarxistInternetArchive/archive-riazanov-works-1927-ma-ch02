WE shall now pass on to the history of Germany after 1815. The Napoleonic
wars came to an end. These wars were conducted not only by England, which
was the soul of the coalition, but also by Russia, Germany and Austria.
Russia took such an important part that Tsar Alexander I, "the Blessed,"
played the chief role at the infamous Vienna Congress (1814-15), where
the destinies of many nations were determined. The course that events had
taken, following the peace concluded at Vienna, was not a whit better than
the chaos which had followed the Versailles arrangements at the end of
the last imperialist war. The territorial conquests of the revolutionary
period were wrenched from France. England grabbed all the French colonies,
and Germany, which expected unification as a result of the War of Liberation,
was split definitely into two parts. Germany in the north and Austria in
the south.

 Shortly after 1815, a movement was started among the intellectuals
and students of Germany, the cardinal purpose of which was the establishment
of a United Germany. The arch enemy was Russia, which immediately after
the Vienna Congress, had concluded the Holy Alliance with Prussia and Austria
against all revolutionary movements. Alexander I and the Austrian Emperor
were regarded as its founders. In reality it was not the Austrian Emperor,
but the main engineer of Austrian politics, Metternich, who was the brains
of the Alliance. But it was Russia that was considered the mainstay of
reactionary tendencies; and when the liberal movement of intellectuals
and students started with the avowed purpose of advancing culture and enlightenment
among the German people as a preparation for unification, the whole-hearted
hatred of this group was reserved for Russia, the mighty prop of conservatism
and reaction. In 1819 a student, Karl Sand, killed the German writer August
Kotzebue, who was suspected, not without reason, of being a Russian spy.
This terrorist act created a stir in Russia, too, where Karl Sand was looked
up to as an ideal by many of the future Decembrists, and it served as a
pretext for Metternich and the German government to swoop down upon the
German intelligentsia. The student societies, however, proved insuppressible;
they grew even more aggressive, and the revolutionary organisations in
the early twenties sprung up from their midst.

 We have mentioned the Russian Decembrist movement which led to
an attempt at armed insurrection, and which was frustrated on December
14, 1825. We must add that this was not an isolated, exclusively Russian
phenomenon. This movement was developing under the influence of the revolutionary
perturbations among the intelligentsia of Poland, Austria, France, and
even Spain. This movement of the intelligentsia had its counterpart in
literature, its chief representative being Ludwig Borne, a Jew, a famous
German publicist during the period of 1818-1830 and the first political
writer in Germany. He had a profound influence upon the evolution of German
political thought. He was a thoroughgoing political democrat, who took
little interest in social questions, believing that everything could be
set right by granting the people political freedom.

 This went on until 1830. In that year the July Revolution shook
France, and its reverberations set Germany aquiver. Rebellions and uprisings
occurred in several localities, but were brought to an end by some constitutional
concessions. The government made short shrift of this movement which was
not very deeply rooted in the masses.

 A second wave of agitation rolled over Germany, when the unsuccessful
Polish rebellion of 1831, which also was a direct consequence of the July
Revolution, caused a great number of Polish revolutionists, fleeing from
persecution, to seek refuge in Germany. Hence a further strengthening of
the old tendency among the German intelligentsia -- a hatred for Russia
and sympathy for Poland, then under Russian domination.

 After 1831, as a result of the two events mentioned above, and
despite the frustration of the July Revolution, we witness a series of
revolutionary movements which we shall now cursorily review. We shall emphasise
the events which in one way or another might have influenced the young
Engels and Marx. In 1832 this movement was concentrated in southern Germany,
not in the Rhine province, but in the Palatinate. Just like the Rhine province,
the Palatinate was for a long time in the hands of France, for it was returned
to Germany only after 1815. The Rhine province was handed over to Prussia,
the Palatinate to Bavaria where reaction reigned not less than in Prussia.
It can be readily understood why the inhabitants of the Rhine province
and the Palatinate, who had been accustomed to the greater freedom of France,
strongly resented German repression. Every revolutionary upheaval in France
was bound to enhance opposition to the government. In 1831 this opposition
assumed threatening proportions among the liberal intelligentsia, the lawyers
and the writers of the Palatinate. In 1832, the lawyers Wirth and Ziebenpfeifer
arranged a grand festival in Hambach. Many orators appeared on the rostrum.
Borne too was present. They proclaimed the necessity of a free, united
Germany. There was among them a very young man, Johann Philip Becker (1809-1886),
brushmaker, who was about twenty-three years old. His name will be mentioned
more than once in the course of this narrative. Becker tried to persuade
the intelligentsia that they must not confine themselves to agitation,
but that they must prepare for an armed insurrection. He was the typical
revolutionist of the old school. An able man, he later became a writer,
though he never became an outstanding theoretician. He was more the type
of the practical revolutionist.

 After the Hambach festivities, Becker remained in Germany for
several years, his occupations resembling those of the Russian revolutionists
of the seventies. He directed propaganda and agitation, arranged escapes
and armed attacks to liberate comrades from prison. In this manner he aided
quite a few revolutionists. In 1833 a group, with which Becker was closely
connected (he himself was then in prison), made an attempt at an armed
attack on the Frankfort guard-house, expecting to get hold of the arms.
At that time the Diet was in session at Frankfort, and the students and
workers were confident that having arranged a successful armed uprising
they would create a furore throughout Germany. But they were summarily
done away with. One of the most daring participants in this uprising was
the previously mentioned Karl Schapper. He was fortunate in his escape
back to France. It must be remembered that this entire movement was centred
in localities which had for a long time been under French domination.

 We must also note the revolutionary movement in the principality
of Hesse. Here the leader was Weidig, a minister, a religious soul, but
a fervent partisan of political freedom, and a fanatical worker for the
cause of a United Germany. He established a secret printing press, issued
revolutionary literature and endeavoured to attract the intelligentsia.
One such intellectual who took a distinguished part in this movement was
Georg Buchner (1813-1837), the author of the drama, The Death of Danton.
He differed from Weidig in that in his political agitation he pointed out
the necessity of enlisting the sympathy of the Hessian peasantry. He published
a special propaganda paper for the peasants -- the first experiment of
its kind -- printed on Weidig's press. Weidig was soon arrested and Buchner
escaped by a hair's breadth. He fled to Switzerland where he died soon
after. Weidig was incarcerated, and subjected to corporal punishment. It
might be mentioned that Weidig was Wilhelm Liebknecht's uncle, and that
the latter was brought up under the influence of these profound impressions.

 Some of the revolutionists freed from prison by Becker, among
whom were Schapper and Theodor Schuster, moved to Paris and founded there
a secret organisation called The Society of the Exiles. Owing to the appearance
of Schuster and other German workers who at that time settled in Paris
in great numbers, the Society took on a distinct socialist character. This
led to a split. One faction under the guidance of Schuster formed the League
of the Just, which existed in Paris for three years. Its members took part
in the Blanqui uprising, shared the fate of the Blanquists and landed in
prison. When they were released, Schapper and his comrades went to London.
There they organised the Workers' Educational Society, which was later
transformed into a communist organisation.

 In the thirties there were quite a few other writers alongside
of Borne who dominated the minds of the German intelligentsia. The most
illustrious of them was Heinrich Heine, the poet, who was also a publicist,
and whose Paris correspondence like the correspondence of Ludwig Borne,
was of great educational importance to the youth old Germany.

 Borne and Heine were Jews. Borne came from the Palatinate, Heine
from the Rhine province where Marx and Engels were born and grew up. Marx
was also a Jew. One of the questions that invariably presents itself is
the extent to which Marx's subsequent fate was affected by the circumstances
of his being a Jew.

 The fact is that in the history of the German intelligentsia,
in the history of German thought, four Jews played a monumental part. They
were: Marx, Lassalle, Heine and Borne. More names could be enumerated,
but these were the most notable. It must be stated that the fact that Marx
as well as Heine were Jews had a good deal to do with the direction of
their political development. If the university intelligentsia protested
against the socio-political regime weighing upon Germany, then the Jewish
intelligentsia felt this yoke even more keenly; one must read Borne to
realise the rigours of the German censorship, one must read his articles
in which he lashed philistine Germany and the police spirit that hovered
over the land, to feel how a person, the least bit enlightened, could not
help protesting against these abominations. The conditions were then particularly
onerous for the Jew. Borne spent his entire youth in the Jewish district
in Frankfort, under conditions very similar to those under which the Jews
lived in the dark middle ages. Not less burdensome were these conditions
to Heine.

 Marx found himself in somewhat different circumstances. These,
however, do not warrant the disposition of some biographers to deny this
Jewish influence almost entirely.

 Karl Marx was the son of Heinrich Marx, a lawyer, a highly educated,
cultured and freethinking man. We know of Marx's father that he was a great
admirer of the eighteenth-century literature of the French Enlightenment,
and that altogether the French spirit seems to have pervaded the home of
the Marxes. Marx's father liked to read, and interested his son in the
writings of the English philosopher Locke, as well as the French writers
Diderot and Voltaire.

 Locke, one of the ideologists of the second so-called glorious
English Revolution, was, in philosophy, the opponent of the principle of
innate ideas. He instituted an inquiry into the origin of knowledge. Experience,
he maintained, is the source of all we know; ideas are the result of experience;
knowledge is wholly empirical; there are no innate ideas. The French materialists
adopted the same position. They held that everything in the human mind
reacted in one way or other through the sensory organs. The degree to which
the atmosphere about Marx was permeated with the ideas of the French materialists
can be judged from the following illustration.

 Marx's father, who had long since severed all connections with
religion, continued ostensibly to be bound up with Judaism. He adopted
Christianity in 1824, when his son was already six years old. Franz Mehring
(1846-1919) in his biography of Marx tried to prove that this conversion
had been motivated by the elder Marx's determination to gain the right
to enter the more cultured Gentile society. This is only partly true. The
desire to avoid the new persecutions which fell upon the Jews since 1815,
when the Rhine province was returned to Germany, must have had its influence.
We should note that Marx himself, though spiritually not in the least attached
to Judaism, took a great interest in the Jewish question during his early
years. He retained some contact with the Jewish community at Treves. In
endless petitions the Jews had been importuning the government that one
or another form of oppression be removed. In one case we know that Marx's
close relatives and the rest of the Jewish community turned to him and
asked him to write a petition for them. This happened when he was twenty-four
gears old.

 All this indicates that Marx did not altogether shun his old kin,
that he took an interest in the Jewish question and also a part in the
struggle for the emancipation of the Jew.

 This did not prevent him from drawing a sharp line of demarcation
between poor Jewry with which he felt a certain propinquity and the opulent
representatives of financial Jewry.

 Treves, the city where Marx was born and where several of his
ancestors were rabbis, was in the Rhine province. This was one of the Prussian
provinces where industry and politics were in a high state of effervescence.
Even now it is one of the most industrialised regions in Germany. There
are Solingen and Remscheid, two cities famous for their steel products.
There is the centre of the German textile industry -- Barmen-Elberfeld.
In Marx's home town, Treves, the leather and weaving industries were developed.
It was an old medieval city, which had played a big part in the tenth century.
It was a second Rome, for it was the See of the Catholic bishop. It was
also an industrial city, and during the French Revolution, it too was in
the grip of a strong revolutionary paroxysm. The manufacturing industry,
however, was here much less active than in the northern parts of the province,
where the centres of the metallurgical and cotton industries were located.
It lies on the banks of the Moselle, a tributary of the Rhine, in the centre
of the wine manufacturing district, a place where remnants of communal
ownership of land were still to be found, where the peasantry constituted
a Glass of small landowners not yet imbued with the spirit of the tight-fisted,
financially aggressive peasant-usurer, where they made wine and knew how
to be happy. In this sense Treves preserved the traditions of the middle
ages. From several sources we gather that at this time Marx was interested
in the condition of the peasant. He would make excursions to the surrounding
villages and thoroughly familiarise himself with the life of the peasant.
A few years later he exhibited this knowledge of the details of peasant
life and industry in his writings.

 In high school Marx stood out as one of the most capable students,
a fact of which the teachers took cognisance. We have a casual document
in which a teacher made some very flattering comments on one of [Earl's
compositions. Marx was given an assignment to write a composition on "How
Young Men Choose a Profession." He viewed this subject from a unique aspect.
He proceeded to prove that there could be no free choice of a profession,
that man was born into circumstances which predetermined his choice, for
they moulded his weltanschauung. Here one may discern the germ of
the Materialist Conception of History. After what was said of his father,
however, it is obvious that in the above we have evidence of the degree
to which Marx, influenced by his father, absorbed the basic ideas of the
French materialists. It was the form in which the thought was embodied
that was markedly original.

 At the age of sixteen, Marx completed his high school course,
and in 1835 he entered the University of Bonn. By this time revolutionary
disturbances had well-nigh ceased. University life relapsed into its normal
routine.

 At the university, Marx plunged passionately into his studies.
We are in possession of a very curious document, a letter of the nineteen-year-old
Marx to his father.

 The father appreciated and understood his son perfectly. It is
sufficient to read his reply to Marx to be convinced of the high degree
of culture the man possessed. Rarely do we find in the history of revolutionists
a case where a son meets with the full approval and understanding of his
father, where a son turns to his father as to a very intimate friend. In
accord with the spirit of the times, Marx was in search of a philosophy
-- a teaching which would enable him to give a theoretical foundation to
the implacable hatred he felt for the then prevailing political and social
system. Marx became a follower of the Hegelian philosophy, in the form
which it had assumed with the Young Hegelians who had broken away most
radically from old prejudices, and who through Hegel's philosophy had arrived
at most extreme deductions in the realms of politics, civil and religious
relations. In 1841 Marx obtained his doctorate from the University of Jena.

 At that time Engels too fell in with the set of the Young Hegelians.
We do not know but that it was precisely in these circles that Engels first
met Marx.

 Engels was born in Barmen, in the northern section of the Rhine
province. This was the centre of the cotton and wool industries, not far
from the future important metallurgical centre. Engels was of German extraction
and belonged to a well-to-do family.

 In the books containing genealogies of the merchants and the manufacturers
of the Rhine province, the Engels family occupies a respectable place.
Here one may find the family coat of arms of the Engelses. These merchants,
not unlike the nobility, were sufficiently pedigreed to have their own
coat of arms. Engels' ancestors bore on their shield an angel carrying
an olive branch, the emblem of peace, signalising as it were, the pacific
life and aspirations of one of the illustrious scions of their race. It
is with this coat of arms that Engels entered life. This shield was most
likely chosen because of the name, Engels, suggesting Angel in German.
The prominence of this family can be judged by the fact that its origin
can be traced back to the sixteenth century. As to Marx we can hardly ascertain
who his grandfather was; all that is known is that his was a family of
rabbis.: But so little interest had been taken in this family that records
do not take us further back than two generations. Engels on the contrary
has even two variants of his genealogy. According to certain data, Engels
was a remote descendant of a Frenchman L'Ange, a Protestant, a Huguenot,
who found refuge in Germany. Engels' more immediate relatives deny this
French origin, insisting on his purely German antecedents. At any rate,
in the seventeenth century the Engels family was an old, firmly rooted
family of cloth manufacturers, who later became cotton manufacturers. It
was a wealthy family with extensive international dealings. The older Engels,
together with his friend Erman, erected textile factories not only in his
native land but also in Manchester. He became an Anglo-German textile manufacturer.

 Engels' father belonged to the Protestant creed. An evangelist,
he was curiously reminiscent of the old Calvinists, in his profound religious
faith, and no less profound conviction, that the business of man on this
earth is the acquisition and hoarding of wealth through industry and commerce.
In life he was fanatically religious. Every moment away from business or
other mundane activities he consecrated to pious reflections. On this ground
the relations between the Engelses, father and son, were quite different
from those we have observed in the Marx family. Very soon the ideas of
father and son clashed; the father was resolved to make of his son a merchant,
and he accordingly brought him up in the business spirit. At the age of
seventeen the boy was sent to Bremen, one of the biggest commercial cities
in Germany. There he was forced to serve in a business office for three
years. By his letters to some school chums we learn how, having entered
this atmosphere, Engels tried to free himself of its effects. He went there
a godly youth, but soon fell under the sway of Heine and Borne. At the
age of nineteen he became a writer and sallied forth as an apostle of a
freedom-loving, democratic Germany. His first articles, which attracted
attention and which appeared under the pseudonym of Oswald, mercilessly
scored the environment in which the author had spent his childhood. These
letters from Wupperthal created a strong impression. One could sense that
they were written by a man who was brought up in that locality and who
had a good knowledge of its people. While in Bremen he emancipated himself
completely of all religious prepossessions and developed into an old French
Jacobin.

 About 1841, at the age of twenty, Engels entered the Artillery
Guards of Berlin as a volunteer. There he fell in with the same circle
of the Young Hegelians to which Marx belonged. He became the adherent of
the extreme left wing of the Hegelian philosophy. While Marx, in 1842,
was still engrossed in his studies and was preparing himself for a University
career, Engels, who had begun to write in 1839, attained a conspicuous
place in literature under his old pseudonym, and was taking a most active
part in the ideological struggles which were carried on by the disciples
of the old and the new philosophical systems.

 In the years 1841 and 1842 there lived in Berlin a great number
of Russians -- Bakunin, Ogarev, Frolov and others. They too were fascinated
by the same philosophy which fascinated Marx and Engels. To what extent
this is true can be shown by the following episode. In 1842 Engels wrote
a trenchant criticism of the philosophy of Hegel's adversary, Friedrich
Schelling. The latter then received an invitation from the Prussian government
to come to Berlin and to pit his philosophy, which endeavoured to reconcile
the Bible with science, against the Hegelian system. The views expressed
by Engels at that period were so suggestive of the views of the Russian
critic Bielinsky of that period, and of the articles of Bakunin, that,
up to very recently, Engels' pamphlet in which he had attacked Schelling's
Philosophy of Revelation, was ascribed to Bakunin. Now we know that
it was an error, that the pamphlet was not written by Bakunin. The forms
of expression of both writers, the subjects they chose, the proofs they
presented while attempting to establish the perfections of the Hegelian
philosophy, were so remarkably similar that it is little wonder that many
Russians considered and still consider Bakunin the author of this booklet.

 Thus at the age of twenty-two, Engels was an accomplished democratic
writer, with ultra-radical tendencies. In one of his humorous poems he
depicted himself a fiery Jacobin. In this respect he reminds one of those
few Germans who had become very much attached to the French Revolution.
According to himself, all he sang was the Marseillaise, all he clamoured
for was the guillotine. Such was Engels in the year 1842. Marx was in about
the same mental state. In 1842 they finally met in one common cause.

 Marx was graduated from the university and received his doctor's
degree in April, 1841. He had proposed at first to devote himself to philosophy
and science, but he gave up this idea when his teacher and friend, Bruno
Bauer, who was one of the leaders of the Young Hegelians lost his right
to teach at the university because of his severe criticism of the official
theology.

 It was a case of good fortune for Marx to be invited at this time
to edit a newspaper. Representatives of the more radical commercial-industrial
bourgeoisie of the Rhine province had made up their minds to found their
own political organ. The most important newspaper in the Rhine province
was the Kolnische Zeitung, and Cologne was then the greatest industrial
centre of the Rhine district. The Kolnische Zeitung cringed before
the government. The Rhine radical bourgeoisie wanted their own organ to
oppose the Kolnische Zeitung and to defend their economic interests
against the feudal lords. Money was collected, but there was a dearth of
literary forces. Journals founded by capitalists fell into the hands of
a group of radical writers. Above them all towered Moses Hess (1812-1875).
Moses Hess was older than either Engels or Marx. Like Marx he was a Jew,
but he very early broke away from his rich father. He soon joined the movement
for liberation, and even as far back as the thirties, advocated the formation
of a league of the cultured nations in order to insure the winning of political
and cultural freedom. In 1812, influenced by the French communist movement,
Moses Hess became a communist. It was he and his friends who were among
the prominent editors of the Rheinische Zeitung.

 Marx lived then in Bonn. For a long time he was only a contributor,
though he had already begun to wield considerable influence. Gradually
Marx rose to a position of first magnitude. Thus, though the newspaper
was published at the expense of the Rhine industrial middle class, in reality
it became the organ of the Berlin group of the youngest and most radical
writers.

 In the autumn of 1842 Marx moved to Cologne and immediately gave
the journal an entirely new trend. In contradistinction to his Berlin comrades,
as well as Engels, he insisted on a less noisy yet more radical struggle
against the existing political and social conditions. Unlike Engels, Marx,
as a child, had never felt the goading yoke of religious and intellectual
oppression -- a reason why he was rather indifferent to the religious struggle,
why he did not deem it necessary to spend all his strength on a bitter
criticism of religion. In this respect he preferred polemics about essentials
to polemics about mere externals. Such a policy was indispensable, he thought,
to preserve the paper as a radical organ. Engels was much nearer to the
group that demanded relentless open war against religion. A similar difference
of opinion existed among the Russian revolutionists towards the end of
1917 and the beginning of 1918. Some demanded an immediate and sweeping
attack upon the Church. Others maintained that this was not essential,
that there were more serious problems to tackle. The disagreement between
Marx, Engels and other young publicists was of the same nature. Their controversy
found expression in the epistles which Marx as editor sent to his old comrades
in Berlin. Marx stoutly defended his tactics. He emphasised the question
of the wretched conditions of the labouring masses. He subjected to the
most scathing criticism the laws which prohibited the free cutting of timber.
He pointed out that the spirit of these laws was the spirit of the propertied
and landowning class who used all their ingenuity to exploit the peasants,
and who purposely devised ordinances that would render the peasants criminals.
In his correspondence he took up the cudgels for his old acquaintances,
the Moselle peasants. These articles provoked a caustic controversy with
the governor of the Rhine province.

 The local authorities brought pressure to bear at Berlin. A double
censorship was imposed upon the paper. Since the authorities felt that
Marx was the soul of the paper, they insisted on his dismissal. The new
censor had great respect for this intelligent and brilliant publicist,
who so dexterously evaded the censorship obstacles, but he nevertheless
continued to inform against Marx not only to the editorial management,
but also to the group of stockholders who were behind the paper. Among
the latter, the feeling began to grow that greater caution and the avoidance
of all kinds of embarrassing questions would be the proper policy to pursue.
Marx refused to acquiesce. He asserted that any further attempt at moderation
would prove futile, that at any rate the government would not be so easily
pacified. Finally he resigned his editorship and left the paper. This did
not save the paper, for it soon was forced to discontinue.

 Marx left the paper a completely transformed man. He had entered
the newspaper not at all a communist. He had simply been a radical democrat,
interested in the social and economic conditions of the peasantry. But
he gradually became more and more absorbed in the study of the basic economic
problems relating to the peasant question. From philosophy and jurisprudence
Marx was drawn into a detailed and specialised study of economic relations.

 In addition, a new polemic between Marx and a conservative journal
burst out in connection with an article written by Hess who, in 1842, converted
Engels to communism. Marx vehemently denied the paper's right to attack
communism. "I do not know communism," he said, "but a social philosophy
that has as its aim the defence of the oppressed cannot be condemned so
lightly. One must acquaint himself thoroughly with this trend of thought
ere he dares dismiss it." When Marx left the Rheinische Zeitung he
was not yet a communist, but he was already interested in communism as
a particular tendency representing a particular point of view. Finally,
he and his friend, Arnold Ruge (1802-1880), came to the conclusion that
there was no possibility for conducting political and social propaganda
in Germany. They decided to go to Paris (1843) and there publish a journal
Deutsch-Französischen Jahrbücher (Franco-German Year Books). By
this name they wanted, in contradistinction to the French and German nationalists,
to emphasise that one of the conditions of a successful struggle against
reaction was a close political alliance between Germany and France. In
the Jahrbücher Marx formulated for the first time the basic principles
of his future philosophy, in which evolution of a radical democrat into
a communist is discerned.

 
THE EARLY REVOLUTIONARY MOVEMENT IN GERMANY.THE RHINE PROVINCE.THE YOUTH OF MARX AND ENGELS.THE EARLY WRITINGS OF ENGELS.MARX AS EDITOR OF THE Rheinische Zeitung.

David Riazanov's KARL MARX and FREDERICK ENGELS An
Introduction to Their Lives and Work
On
to chapter 1 On
to chapter 3  
Marxist writers'
Archives