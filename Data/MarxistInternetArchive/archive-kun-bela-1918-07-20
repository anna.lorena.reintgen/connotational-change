Bela Kun
First Published: Pravda July 20, 1918
		
Source: International Socialist Library No. 15, Revolutionary Essays by Bela Kun, B.S.P., London.
		
Transcription/Markup: Brian Reid
                
Proofreader: Chris Clayton
                
Public Domain:  Marxists Internet Archive  
(2005).  You may freely copy, distribute, display and perform this  
work; as well as make derivative and commercial works. Please credit  
“Marxists Internet Archive” as your source. During the great French Revolution, the guardian of the principle of legitimity, of the principle of monarchy, was the Holy Roman Empire, as it was then — the Austrian Empire, as it is now. At the present time that part is being played by Germany.

All the present German Chancellors, whatever their name, strive to act up to the réle of Metternich, the Austrian Chancellor of that time.

But there is a fundamental difference between Germany now and the Holy Roman Empire then. The German Empire does not intend to reinstate the old dynasties; it founds new dynasties, setting on the throne its own representatives. The first such attempt has taken place in the Ukraine. In the person of Skoropadsky there is, in effect, at the the head of the State a Viceroy, with all the characteristics not of a constitutional but of an autocratic monarch. The question is whether Germany intends to set on the Ukrainian throne one of the “unemployed” still remaining Romanovs, or a German prince. The Romanovs would possibly find some adherents in the ranks of the Black Hundred; but the revolutionary movement in the Ukraine displays the necessity for a “completely reliable” German monarch, who would not under any circumstances show hesitation in crushing opposition.

We have seen the same picture in Finland. The former Grand Duke Cyril Vladimirovitch, a scion of the Romanov house, was amongst the spectators when, in the Parliamentary arena, the Duke of Mecklenburg-Schwerin was proclaimed King. In Finland, just as in the Ukraine, the restoration of the monarchy represents not merely the rehabilitation of the general principle of Monarchism, but the restoration of the bourgeois State as a whole, in contradistinction to the proletarian State.

In such a case the restoration means the withdrawal of power from a class which can take part in the work of government only when it is in a position to become the sole master of that power, i.e., when it holds the dictatorship. A return from the dictatorship of the proletariat to the monarchy can only be a symptom of a form of reaction which, in the end, will, notwithstanding, shorten the path to Socialism.

In future, Skoropadsky and the Duke of Mecklenburg-Schwerin can no longer be displaced by the Rodziankos, the Kerenskys, the Martovs: they can be displaced — and soon will — only by the dictatorship of the proletariat.
