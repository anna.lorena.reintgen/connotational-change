
Translated: by Igor Shevchenko under the pseudonym John Chernyatynskyy
Published: in March 1947
Transcribed: by 
Jonas Holmgren
The Ukrainian translation of Animal Farm was 
intended for Ukrainians living in the camps for Displaced Persons in Germany 
under British and American administration after World War II. They were simple people, peasants and 
workers, some half-educated, but all of whom read eagerly. For these people he 
asked Orwell to write a special introduction. The English original has been lost 
and the version reproduced here is a recasting back into English of the 
Ukrainian version. Orwell insisted that he receive no royalties for this 
edition, nor for other translations intended for those too poor to buy them 
(e.g., editions in Persian and Telugu). Orwell himself paid the production costs 
of a Russian-language edition printed on thin paper, which was intended for 
soldiers and others behind the Iron Curtain.I have been asked to write a preface to the Ukrainian translation 
of Animal Farm. I am aware that I write for readers about whom I know 
nothing, but also that they too have probably never had the slightest 
opportunity to know anything about me.In this preface they will most likely expect me to say something 
of how Animal Farm originated but first I would like to say something 
about myself and the experiences by which I arrived at my political position.I was born in India in 1903. My father was an official in the 
English administration there, and my family was one of those ordinary 
middle-class families of soldiers, clergymen, government officials, teachers, 
lawyers, doctors, etc. I was educated at Eton, the most costly and snobbish of 
the English Public Schools[*]. But I had only got in there by means of a 
scholarship; otherwise my father could not have afforded to send me to a school 
of this type.Shortly after I left school (I wasn't quite twenty years old 
then) I went to Burma and joined the Indian Imperial Police. This was an armed 
police, a sort of gendarmerie very similar to the Spanish Guardia 
Civil or the Garde Mobile in France. I stayed five years in the 
service. It did not suit me and made me hate imperialism, although at that time 
nationalist feelings in Burma were not very marked, and relations between the 
English and the Burmese were not particularly unfriendly. When on leave in 
England in 1927, I resigned from the service and decided to become a writer: at 
first without any especial success. In 1928-9 I lived in Paris and wrote short 
stories and novels that nobody would print (I have since destroyed them all). In 
the following years I lived mostly from hand to mouth, and went hungry on 
several occasions. It was only from 1934 onwards that I was able to live on what 
I earned from my writing. In the meantime I sometimes lived for months on end 
amongst the poor and half-criminal elements who inhabit the worst parts of the 
poorer quarters, or take to the streets, begging and stealing. At that time I 
associated with them through lack of money, but later their way of life 
interested me very much for its own sake. I spent many months (more 
systematically this time) studying the conditions of the miners in the north of 
England. Up to 1930 I did not on the whole look upon myself as a Socialist. In 
fact I had as yet no clearly defined political views. I became pro-Socialist 
more out of disgust with the way the poorer section of the industrial workers 
were oppressed and neglected than out of any theoretical admiration for a 
planned society.In 1936 I got married. In almost the same week the civil war 
broke out in Spain. My wife and I both wanted to go to Spain and fight for the 
Spanish Government. We were ready in six months, as soon as I had finished the 
book I was writing. In Spain I spent almost six months on the Aragon front 
until, at Huesca, a Fascist sniper shot me through the throat.In the early stages of the war foreigners were on the whole 
unaware of the inner struggles between the various political parties supporting 
the Government. Through a series of accidents I joined not the International 
Brigade like the majority of foreigners, but the POUM militia  –  i.e. the Spanish Trotskyists.So in the middle of 1937, when the Communists gained control (or 
partial control) of the Spanish Government and began to hunt down the 
Trotskyists, we both found ourselves amongst the victims. We were very lucky to 
get out of Spain alive, and not even to have been arrested once. Many of our 
friends were shot, and others spent a long time in prison or simply disappeared.These man-hunts in Spain went on at the same time as the great 
purges in the USSR and were a sort of supplement to them. In Spain as well as in 
Russia the nature of the accusations (namely, conspiracy with the Fascists) was 
the same and as far as Spain was concerned I had every reason to believe that 
the accusations were false. To experience all this was a valuable object lesson: 
it taught me how easily totalitarian propaganda can control the opinion of 
enlightened people in democratic countries.My wife and I both saw innocent people being thrown into prison 
merely because they were suspected of unorthodoxy. Yet on our return to England 
we found numerous sensible and well-informed observers believing the most 
fantastic accounts of conspiracy, treachery and sabotage which the press 
reported from the Moscow trials.And so I understood, more clearly than ever, the negative 
influence of the Soviet myth upon the western Socialist movement.And here I must pause to describe my attitude to the Soviet 
régime.I have never visited Russia and my knowledge of it consists only 
of what can be learned by reading books and newspapers. Even if I had the power, 
I would not wish to interfere in Soviet domestic affairs: I would not condemn 
Stalin and his associates merely for their barbaric and undemocratic methods. It 
is quite possible that, even with the best intentions, they could not have acted 
otherwise under the conditions prevailing there.But on the other hand it was of the utmost importance to me that 
people in western Europe should see the Soviet régime for what it really was. 
Since 1930 I had seen little evidence that the USSR was progressing towards 
anything that one could truly call Socialism. On the contrary, I was struck by 
clear signs of its transformation into a hierarchical society, in which the 
rulers have no more reason to give up their power than any other ruling class. 
Moreover, the workers and intelligentsia in a country like England cannot 
understand mat the USSR of today is altogether different from what it was in 
1917. It is partly that they do not want to understand (i.e. they want to 
believe that, somewhere, a really Socialist country does actually exist), and 
partly that, being accustomed to comparative freedom and moderation in public 
life, totalitarianism is completely incomprehensible to them.Yet one must remember that England is not completely democratic. 
It is also a capitalist country with great class privileges and (even now, after 
a war that has tended to equalise everybody) with great differences in wealth. 
But nevertheless it is a country in which people have lived together for several 
hundred years without major conflict, in which the laws are relatively just and 
official news and statistics can almost invariably be believed, and, last but 
not least, in which to hold and to voice minority views does not involve any 
mortal danger. In such an atmosphere the man in the street has no real 
understanding of things like concentration camps, mass deportations, arrests 
without trial, press censorship, etc. Everything he reads about a country like 
the USSR is automatically translated into English terms, and he quite innocently 
accepts the lies of totalitarian propaganda. Up to 1939, and even later, the 
majority of English people were incapable of assessing the true nature of the 
Nazi régime in Germany, and now, with the Soviet régime, they are still to a 
large extent under the same sort of illusion.This has caused great harm to the Socialist movement in England, 
and had serious consequences for English foreign policy. Indeed, in my opinion, 
nothing has contributed so much to the corruption of the original idea of 
Socialism as the belief that Russia is a Socialist country and that every act of 
its rulers must be excused, if not imitated.And so for the past ten years I have been convinced that the 
destruction of the Soviet myth was essential if we wanted a revival of the 
Socialist movement.On my return from Spain I thought of exposing the Soviet myth in 
a story that could be easily understood by almost anyone and which could be 
easily translated into other languages. However, the actual details of the story 
did not come to me for some time until one day (I was then living in a small 
village) I saw a little boy, perhaps ten years old, driving a huge cart-horse 
along a narrow path, whipping it whenever it tried to turn. It struck me that if 
only such animals became aware of their strength we should have no power over 
them, and that men exploit animals in much the same way as the rich exploit the 
proletariat.I proceeded to analyse Marx's theory from the animals’ point of 
view. To them it was dear that the concept of a class struggle between humans 
was pure illusion, since whenever it was necessary to exploit animals, all 
humans united against them: the true struggle is between animals and humans. 
From this point of departure, it was not difficult to elaborate the story. I did 
not write it out till 1943, for I was always engaged on other work which gave me 
no time; and in the end I included some events, for example the Teheran 
Conference, which were taking place while I was writing. Thus the main outlines 
of the story were in my mind over a period of six years before it was actually 
written.I do not wish to comment on the work; if it does not speak for 
itself, it is a failure. But I should like to emphasise two points: first, that 
although the various episodes are taken from the actual history of the Russian 
Revolution, they are dealt with schematically and their chronological order is 
changed; this was necessary for the symmetry of the story. The second point has 
been missed by most critics, possibly because I did not emphasise it 
sufficiently. A number of readers may finish the book with the impression that 
it ends in the complete reconciliation of the pigs and the humans. That was not 
my intention; on the contrary I meant it to end on a loud note of discord, for I 
wrote it immediately after the Teheran Conference which everybody thought had 
established the best possible relations between the USSR and the West. I 
personally did not believe that such good relations would last long; and, as 
events have shown, I wasn't far wrong.I don't know what more I need add. If anyone is interested in 
personal details, I should add that I am a widower with a son almost three years 
old, that by profession I am a writer, and that since the beginning of the war I 
have worked mainly as a journalist.The periodical to which I contribute most regularly is Tribune, 
a socio-political weekly which represents, generally speaking, the left wing of 
the Labour Party. The following of my books might most interest the ordinary 
reader (should any reader of this translation find copies of them): Burmese 
Days (a story about Burma), Homage to Catalonia (arising from my 
experiences in the Spanish Civil War), and Critical Essays (essays mainly 
about contemporary popular English literature and instructive more from the 
sociological than from the literary point of view). [*] These are not public ‘national schools’, but something quite 
the opposite: exclusive and expensive residential secondary schools, scattered 
far apart. Until recently they admitted almost no one but the sons of rich 
aristocratic families. It was the dream of nouveau riche bankers of the 
nineteenth century to push their sons into a Public School. At such schools the 
greatest stress is laid on sport, which forms, so to speak, a lordly, tough and 
gentlemanly outlook. Among these schools, Eton is particularly famous. 
Wellington is reported to have said that the victory of Waterloo was decided on 
the playing fields of Eton. It is not so very long ago that an overwhelming 
majority of the people who in one way or another ruled England came from the 
Public Schools. [Orwell's Note] 
George Orwell Archive |
Marxist Library
