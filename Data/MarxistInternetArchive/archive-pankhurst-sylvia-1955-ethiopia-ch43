E. Sylvia PankhurstMODERN Ethiopia dates from the reign of the
Emperor Menelik II (1889-1913). He was a great
builder and organizer, as well as a famous statesman
and warrior in defence of his country. His reign was one of
intense activity in every field.

Menelik was a son of Haile Melikot, King of Shoa, and a
grandson of the famous King Sahie Selassie, also of Shoa, whose
forebears cherished the Solomonian dynasty during its period of
exile from the Imperial Throne at the time of the Zagwe inter-regnum (a period of about 333 years). He was related by descent
to the ancient Solomonian line.

Menelik II was to follow two intrusive Emperors, Theodore
and Johannes IV, both of whom had secured the Throne by
force of arms, having no blood relationship with previous
sovereigns. Johannes had won the Throne by means of a gift of
modern artillery made to him by Sir Robert Napier in return
for the assistance he had given to the British forces in their
march to vanquish Theodore.

While still King of Shoa, Menelik in 1877 recovered for
Ethiopia the province of Harar, which the Egyptians had
invaded and occupied. He was assisted in this effort by his
cousin, Ras Makonnen, son of a daughter of King Sahle
Sellassie of Shoa, and father of the Emperor Haile Sellassie I.
Ras Makonnen, on being appointed Governor of Harar, restored to the peasants of the province the lands of which they had
been robbed by the Egyptian Emirs, thereby gaining their last-
ing affection and gratitude.

Two years later, at the death of Emperor Johannes IV in
1889, Menelik II succeeded to the Imperial Throne.

In 1879 he had laid the foundations of the town of Entotto
in Shoa, on the mountain of that name overlooking the plateau
where Addis Ababa was subsequently built. Entotto proving
too lofty and inclement for the seat of Government, Metirlik
moved the situation of his capital to Addis Ababa (the New

Flower). The site was chosen and named by his consort, the
future Empress Taitu, who built there the first stone house to
initiate the town. The hot sulphur springs of Finfinni are situated on the outskirts of the city. It is narrated that King Sahle
Sellassie had predicted that his children's children would make
their capital there. In 1887 Menelik laid the foundations of his
residence and seat of administration, now known as the Old 
Palace, an exceedingly spacious edifice, still a pleasant home
for Ethiopia's large hospitalities which have been maintained
from early times.

The architecture produced in the time of Menelik is of
local stone, solid and dignified with something of eastern style. The round or polygonal church was continued throughout his reign. Among the more notable buildings of his period are the
Church of St. Raguel of Entotto, an octagonal church; the
Church of Debra Guennet in Jerusalem erected in 1891;
St George's Cathedral, Addis Ababa, built in 1896 to commemorate the great victory of Adowa in that year, a large and imposing edifice; the Palace at Holleta, the Church of the Saviour of the World at Harar, built to commemorate the recovery of the city and province from the Egyptians; the building which now houses the Ethiopian Treasury; the Hospital of
Menelik II and the modern School he founded in Addis
Ababa, the house of the governor at Aksum.

The Emperor Menelik II endeavoured to assure the frontiers
of Ethiopia by a number of Treaties with European Powers, and
by establishing diplomatic relations with Britain, France,
Germany, Russia, Italy and U.S.A., as well as by his two greatest
victories at Harar and at Adowa.

He established the basis of a modern government.

Innovations introduced in the reign of Menclik which constitute the first stage in breaking down the long isolation off Ethiopia include: telegraph and telephone services; participation in the International Postal Union and the introduction of 
postage stamps; the railway from Jibuti to Akaki, with the important commercial centre and railway town, Dire Dawa, where the
workshops of the railway are situated; a modern currecny.

Owing to the intense interest of Menelik in all modern
developments motor-cars in small numbers, and also electric
light, mechanical stone-crushers for road-making for other
inventions appeared in Ethiopia during his reign, though at








that time still rarities even in Europe. Tobacco growing and
manufacture was initiated as a State Monopoly, as in France,
and has continued profitable to this day.

The renaissance of the ancient nation commenced by
Menelik II has been immensely accelerated by the Emperor
Haile Sellassie I, who in 1917 became Regent and Heir to the
Throne during the reign of Menelik's daughter, the Empress
Zauditu. At that period he bore the designation Dedjasmatch
(later Ras and still later Negus), Tafari Makonnen. In 1930 he
ascended the Imperial Throne as Haile Sellassie I. The achievements of his reign include:

Admission of Ethiopia to the League of Nations and later to
the United Nations, thereby giving practical expression to the
desire for collective security which the sovereigns of Ethiopia
had been endeavouring to establish since the fifteenth century.

Greatly extended diplomatic relations and a tour of visits to
friendly nations by the Emperor himself, treaties of friendship
with Britain, U.S.A. and other nations.

Expansion and modernization of the administration which
now comprises twelve ministries: The Interior, Pen, Defence,
Foreign Affairs, Finance, Justice, Commerce and Industry,
Agriculture, Communications and Public Works, Posts, Telegraphs and Telephones, Education and Fine Arts.1

A Parliament and Parliamentary Constitution.

Democratization of Local Government.

Abolition of the legal status of domestic servitude, an ancient
institution of long duration throughout Africa.

Taxation of Land Values; a single tax, graduated in respect
of good, fair and poor land, being substituted for many ancient
dues and charges which have been abolished. Land survey and
registration. These measures form a veritable charter of liberation and prosperity for the peasants, who form the great majority of the Ethiopian people and produce its principal wealth.

A modern currency in notes and coins.

A State Bank, a Development Bank to assist agricultural and
industrial enterprise.

Improved transport and communications, many new roads

and bridges, the extension of the railway to Addis Ababa and
erection of the Addis Ababa railway station.

Wireless, telegraphy and radio-broadcasting; extension of
post, telephone and telegraphic services, including a post
office at every station of Ethiopian airlines.

Ethiopian Airlines with regular services to all the Ethiopian,
provinces and principal towns (upwards of 20 stations); and
also to Cairo, Jiddah, Khartum, Jibuti, Aden, Nairobi and
Athens.

An Air Force.

A National Police Force, replacing the ancient local guards.

A National Army, replacing the former feudal levies.

The federation of Eritrea to Ethiopia and access of Ethiopia
to the sea.

A Naval College to provide trained personnel for the Ethiopian Navy.

Instruction in port administration and merchant marine.

Improved agriculture and stock-raising; crossbreeding and
selective breeding; Government experimental agricultural and
breeding stations, an Agricultural College, Agricultural
Schools; extensive veterinary services, and animal disease
control.

The introduction of new agricultural products and industries;
sugar growing and refining; machine manufacture of cotton and
of "musa ensete" for bags, ropes and other goods; vegetable oil
extraction; improved apiculture; improved tobacco manufacture; cement manufacture and manufacture of cement products,
tiles, pipes and blocks; brick making; machine corn-milling
coffee decorticating; cleaning of pulses and oil seeds; canning of
meat and tomatoes; making and bottling of carbonated and
mineral beverages; soap making; dairy products; growing of
grapes, oranges, bananas and other fruit.

The development of education in all branches as already described, which is to culminate in the Haile Sellassie I University,
the gift of the Ethiopian people to commemorate the 21st annversary of the Emperor's accession to the Throne.

There has been immense activity in building in conformity
with growing needs. The churches erected during the time of
the present Emperor are mainly basilican in form. The Church
of the Holy Trinity is the most notable. The Mausoleums of the
Emperor Menelik II and of Ras Makonnen are also extremely



fine. The Imperial Palace, the residence and administrative
office of the Emperor Haile Selassie I, around which a number
of administrative offices have arisen, was erected in 1934.  The building was accomplished by the Ethiopian Ministry of'
Works in the short space of eight months. The architect was
Kamietz of the Ministry of Works. The first visitor invited to
the Palace after it has been furnished by Messrs. Waring and
Gillow of London, was H.R.H. Crown Prince Gustav Adolphe
of Sweden, subsequently King of Sweden, whose visit to the
Palace was in January, 1935, only nine months before the
Italian aggression.2

The bridge over the Abai (the blue Nile) at the point where
the provinces of Shoa and Gojjam meet is a magnificent feat
of engineering. It has the widest span of any bridge yet erected
in Africa. Numerous large blocks of shops and flats built of
steel reinforced concrete, well planned on modern lines, have
been erected in Addis Ababa. Sir Patrick Abercrombie was
invited to prepare a plan for the city and its development has
been steadily proceeding during the present decade. The city
has in fact already been remarkably transformed. It has now a
population of 400,000 inhabitants, and those who knew it at the
beginning of the present reign would scarcely recognize it
to-day.

The same must be said of many aspects of Ethiopia as a
whole. Great extensions have been made in water supplies and
the lighting of villages and towns; many new towns have been
built. Hospitals and schools have been established in a large
number of districts. The arts are encouraged. A national
library and museum and other cultural facilities begin to bear
fruit.

To record even a hundredth part of the stupendous achievement of the remarkable personality who to-day leads the
Ethiopian nation, breasting the tides of difficulty, as he has
ever done, must form the subject of another volume, Ethiopia
To-day.1. Public Health and Labour questions are dealt with by special departments of the Ministry of the Interior; special departments of Aviation and the Air Force are
likewise administered by departments of other Ministries, but these with the Navy, Merchant Navy and Port Administration, are doubtless destined in future to be
expanded to the status of Ministries.2.The canard that the Palace was built by the Italian invaders has received
wide circulation.
 
Table of Contents  |  Sylvia Pankurst Archive