Gilbert McClatchie
Source: Socialist Standard, January 1975.
Transcription: Socialist Party of Great Britain.
HTML Markup: Adam Buick
Public Domain: Marxists Internet Archive (2016). You may freely copy, distribute, display and perform this work; as well as make derivative and commercial works. Please credit "Marxists Internet Archive" as your source.
Just as a seed contains the varied hues in a flower, so capitalism at its birth contained all that has flowered into various expressions of revolt. The misery it brought has fostered ideas of short cuts to salvation that continually reappear with different signposts.

Thus, the main arguments of Syndicalism were repeated by the Bolsheviks and their servile followers in Britain in the ’twenties and ’thirties. The Claims that Syndicalism was based on the principles of Marx; that the mass of workers were ignorant and inert, and required leading (and forcing) into the promised land; that the days of theory had passed and the days of action come — these and a host of other ideas demonstrated not only a similarity in outlook between Syndicalism and Bolshevism, but the common source of both movements.

The backbone of Syndicalism was the General Strike. As a proposed means to achieve the workers’ emancipation it has an old history, and received considerable support before the capture of power by parliamentary action had come within the workers’ reach. Like Anarchism, it attracted artists and professional people by its insistence upon leadership, its violent attacks on established institutions, its spurning of theoretical knowledge, and its promise of rapid victory.

After they were expelled from the International the anarchist movement fell to pieces, unable to make progress because of their own opposition to all organization; and the acts of violence to which groups of anarchists resorted led to vigorous police action against them. In their extremity they sought a new lease of life by infiltrating the developing trade union movement. The propaganda of the General Strike fitted in with their anti-State and general-destruction policies, as well as the idea of the capture of power by a militant minority. Sorel describes the position as follows:
In England and America the influence was brief (though for a short time it captured the imaginations of some who should have known better). The main support was in countries where small industries flourished as France, Italy, Spain and Russia.



It was in France that the General Strike was first exhaustively discussed and propagated. The growth of the trade-union movement led to a struggle between its advocates and those of political action. At the Trade Union Congress in 1879 the Guesdist movement, which stood for the conquest of political power, scored a temporary victory, but political action subsequently lost its hold. To the impatient, industrial action appeared as a royal and easy road without having to wait. In 1888 the Congress of the French National Federation of Trade Unions voted in favour of the General Strike and the idea then spread rapidly, even receiving some support from political parties. Among its champions wasAristide Briand who, later as a capitalist cabinet minister, was its vigorous opponent and oppressor.



The arguments for the General Strike at that time may be summed up as follows. To strike was legal, so no matter how widespread it might be the government was unable to prosecute the strikers. A general stoppage of work would reduce the ruling class to famine; it would need to be operated only a short time to compel the government to capitulate and bring the workers into control of power. Its spontaneity meant that it could begin at any time and, hastened by propaganda and organization, bring about the revolution rapidly. The workers must have an organization to take over production afterwards, and the trade unions were admirably suited for this purpose.



Although a minority in the trade-union movement, the anarchists soon occupied key positions and by the middle of the ’nineties were the dominating section. Apart from their fanatical activity, they were assisted by the method of selecting delegates by group and not by number. Being divided in many small groups while their opponents were in fewer larger ones, the anarchists were able to capture the controlling influence; the few highly industrialized parts of France were not able to send as many delegates to conferences as the scattered backward parts where the anarchists predominated. Consequently each congress was “packed” by anarchists — a form of “tactics” with which we are still familiar.



Under their influence, General Strike propaganda went beyond just sitting down and folding arms. Sabotage was adopted, involving the destruction of means of production and the forcing of the support of the unwilling. The practical policy that grew out of the French Revolution and was adopted by Blanqui, Bakunin, De Leon and Lenin was based on the idea that an active minority can carry with it an inert and ignorant mass. It is a policy that depends upon leadership and always places power in the hands of one or two outstanding persons, finally degenerating into personal quarrels between these leaders. Behind it, though rarely recognized, is the conflict between idealism and materialism: the power of the idea alone against the power of material circumstances, which include ideas.




In 1895 a new federation of workers was formed, the General Confederation of Labour (known as the CGT — Confederation General de Travail). It included the General Strike its programme. A conglomeration of unions and federations, it comprised all shades but was ruled by the anarchist minority until 1909, when the crushing of a large-scale postal strike made clear the weakness of industrial action against the power of the state. The opposition to the General Strike policy, particularly by the Guesdists, also showed its failings. For example, the syndicalists were compelled to include anti-militarist propaganda, as an admission that governmental power would not collapse in a general cessation of work.



Probably the most complete expression of the syndicalist outlook is contained in Arnold ’s The Social General Strike (published by the Debating Club No. I, Chicago, 1905). Sliding over difficulties and inconsistencies, the writer distinguishes between strikes for higher wages or political privileges and the strike. According to him the General Strike begins in a small way, spreads in sympathetic strikes, and develops into the overthrowing of the system; it is the culmination of hundreds of earlier strikes in which the workers have gained experience and an ever-stronger sense of solidarity.



Following that argument, one would imagine that the majority of workers had gradually acquired sufficient class-consciousness to understand their social position and desire a change in the basis of society. But this is not the syndicalist view. According to them, the mass of workers are backward and inert until the General Strike commences. Then, in the course of the movement, they become more active and enthusiastic until they convert the strike from the attainment of some minor object into the social revolution. And this change is supposed to occur in a few days or weeks, as production and distribution are brought to a standstill: knowledge grows like a mushroom out of the quagmires of disaster!



Roller is not disturbed by the fact that all workers will not come out on strike. He says: “This ideal of propaganda will, however, in spite of its beauty always be a dream.” He claims that it is only necessary to interrupt production in the whole country for long enough to totally disorganize capitalist society; this, he claims, can be done by sabotage in the principal industries. (But at another time he insists that a great advantage of the General Strike, that will gain for it many adherents who lack courage, is that it will be perfectly legal.) He makes the following very significant statement:The sabotage to assist in this wrecking was to include cutting telephone and telegraph wires; tearing up railway lines; causing accidents; loosening screws in machinery; destroying supports and applying fire and dynamite in mines; sinking ships; and many other methods of destruction — a choice assortment is also described by Pataud and Pouget in their phantasy Syndicalism and the Co-operative Commonwealth, 1913. Thus, as in the policy of Bakunin, society is to be completely wrecked.



In such circumstances, what would be the position of the workers? The inert mass that has suddenly developed “revolutionary fervour” is supposed to trust local leaders, who will pacify them by reporting the progress of the revolution (how, with the means of communication smashed, is not explained). The feeding of the population in the midst of this wreckage is also glossed over with childlike simplicity, with the answer that hungry people will attack stores of food. Leaving out of consideration the action of the government, what city has stores of food to support its inhabitants for any length of time? And what about water and sanitation? It does not require a great deal of imagination to foresee disease and devastation sweeping through cities like London, Paris, Berlin and New York. If the syndicalists were as successful as they hoped, it would not be a question of bringing the ruling class to their knees but of submerging everyone in a common holocaust of disaster.



But before matters had reached such a pitch the government would have stepped in with the forces at its disposal. Roller has an inkling of this. He claims first that the destruction would be so widespread that there would be insufficient military forces to cope with it, and then that it is essential to get the military on the side of the workers: “For the revolutionising of the present order of society, anti-militarism and its propaganda is an absolute necessary supplement to the General Strike.”



Time after time the power of governments to smash big strikes has been demonstrated. Sometimes naked power has been used, sometimes concessions made, and sometimes the workers have been starved into subjection. The condition the syndicalists take for granted, lack of knowledge on the part of the majority, is a condition that is bound to defeat them by playing into the government’s hands. The experience of large strikes has been the very opposite of that anticipated by the syndicalists. The government smashed the French postal workers in 1909 by simply calling the postal workers to the colours and then sending them back to their old jobs as soldiers. Soldiers are average members of the working class made more amenable by the discipline to which they are subjected and, like the mass of workers up to the present, accept the suppression of "disorder" as a proper course of action.
Part 2 of this article will deal with the Syndicalist movement in the United States of America. 
Gilbert McClatchie Archive| Socialist Party of Great Britain

Many anarchists, tired at last of continually reading the same grandiloquent maledictions hurled at the capitalist system, set themselves to find a way which would lead them to acts which were really revolutionary. They became members of syndicates which, thanks to violent strikes, realised to a certain extent, the social war they had often heard spoken of. Historians will one day see in this entry of the anarchists into the syndicates one of the greatest events that has been produced in our times, and then the name of my poor friend Fernand Pelloutier will be as well known as it deserves to be. (Reflections on Violence)

Modern industry, with its extremely specialised labour division and complications, is but poorly adapted to oppose a general strike caused by a minority, for the strike will completely wreck the whole system necessary for the management of production, and vital to the life of modem society.