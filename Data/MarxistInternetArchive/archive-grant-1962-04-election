Source: Socialist Fight, vol. 4 no. 3 (April 1962)
Transcription: Francesco 2009
Proofread: Fred 2009
Markup: Manuel 2009
The
government has suffered its worst electoral blow since 1951 in the bye-election
results. The Tory vote has been falling everywhere. In Orpington it dropped
from 24,303 to 14,991. The Liberals rose from 9,092 to 22,846, and Labour’s
poll fell from 9,543 to 5,350. In the other bye-elections Labour held its
seats, but with a lower poll, while the government poll dropped as well.

The
significance of the election is the swing by the lower middle and middle class
against the vicious measures of the government, which have hit this section of
the population hard. Only big business has benefited from the economic measures
of the government.

But the
outstanding significance of the elections has been the failure of the Gaitskell
policy to attract the middle class or even to keep the enthusiasm of the Labour
voters. Big sections of the white collar workers have been abandoning their
traditional attitudes. The revolt of the teachers and civil service unions was
an indication of this process. But they have not been attracted to the policy
of the Labour Party which has painted itself as “respectable” as the Tories.
They have voted for the “radicalism” of the Liberals. Many industrial workers
too have been attracted to the banner of the Liberals. In the industrial
strongholds of Labour the Liberals have succeeded in hardly making a dent in
the Labour ranks. But nevertheless the Labour poll has dropped.

After 11
years of Tory rule and systematic Tory legislation in the interests of the rich
and privileged, the Labour Party has failed to make an impact on the workers
and middle class disillusioned with the Tories, and not even a crusading spirit
among its own supporters.

The theory
of Gaitskell and the right wing that a watering down of the socialist programme
of the Labour Party would succeed in winning over the middle class can be seen
to be falsified by events.

The way to
win the middle class is first to win the working class! With a vigorous
uninhibited programme of socialism the organised industrial workers could be
aroused and mobilised for a fighting campaign. A programme which exposes the
exploitation of the banks, insurance companies and great industrial combines.
The demonstration of the results of so-called “free enterprise” have been
admitted by the Tories in their lip-service to the idea of “planning”.

Surely all
this should have provided a splendid opportunity to carry the fight against the
capitalist enemy. It is the way to mobilise the youth, the trade union workers
and of course the workers in the constituency parties. With an aroused labour
movement it would be possible to mount a campaign for the resignation of the
government and for new elections.

The middle
class in times of difficulty such as today tends to swing from one side to the
other. They are looking for a solution to their problems: high rents, high
taxes, high mortgage payments, the freezing of their wages and so on. Labour
should have a programme to cater for their needs and interests. But above all
the middle class can be won to a programme of socialism by explaining that only
thus can their problems be solved.

The middle
class and white collar workers would follow the lead of the industrial workers
if they were actively campaigning on the issues sketched above.

The policy
of the right wing has led from one retreat to another, from one disaster to the
next. At a time when the ranks of Labour should be growing by leaps and bounds
there is a feeling of apathy within the ranks. Give the ranks a fighting appeal
and they would carry the message of socialism from Lands End to John O’Groats!

To support
Liberalism, it should be explained, is to move from the Tweedledum of
capitalism to its Tweedledee. Despite the radical demands put forward by the
Liberals, in reality they are as strong supporters of capitalism as the Tories.
The bottom dropped out of the Liberal Party when Labour put forward a working
class alternative to capitalism. The revival of this discredited party is an
indictment of the policies pursued by the leadership for the last 10 years.

What is
worse, the failure to develop a militant campaign now can lead to a Tory
revival once the economic situation improves. Workers and middle class people
not attracted to Labour’s banner now will certainly not be attracted then. If
the labour movement is not crusading now, how [can it] invoke the spirit of
sacrifice and work among the rank and file then?

Thus
present tendencies and future perspectives imperatively demand that the policy
and programme worked out by the last two conferences on the question of
nationalisation and planning must be implemented and not allowed to remain
paper resolutions. A serious attempt to carry this programme to the people
would arouse such a wave of enthusiasm and struggle as to drive the Tories from
power.
 Ted Grant Archive