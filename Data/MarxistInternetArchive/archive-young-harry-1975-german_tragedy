Harry Young
Source: Socialist Standard, February 1975.
Transcription: Socialist Party of Great Britain.
HTML Markup: Adam Buick
Copyleft: Creative Commons (Attribute & No Derivatives) 2007 conference "Be it resolved that all material created and published by the Party shall be licensed under the Creative Commons Attribution-NoDerivs copyright licence".
Leviné by Rosa Leviné-Mayer. (Saxon House, £2.50)This is yet another biography by the wife of a well-known revolutionary. Mrs. Kusinen has written Before and After Stalin; Heinz Neumann's wife has recounted by her harrowing experiences in Soviet labour camps; and now Mrs. Leviné has delved into the details of her comrade's revolutionary career.

Eugen Leviné, a prominent member of the Spartacus Group of Liebknecht and Luxembourg, was sent to Munich to organize the Bavarian Soviet Republic. An extraordinary situation prevailed in Germany at the end of the first World War. On 9th November 1918 the Kaiser abdicated. Power was assumed by the leader of the Social Democratic Party, Friedrich Ebert. 

The army transferred its loyalty to the new régime and General Gruber, speaking for the army, telephoned Ebert to say the armed forces were at his disposal “if he would fight Bolshevism and maintain army discipline”. 

Workers' and Soldiers' Councils had sprung up all over Germany and elected a Central Council dominated by the Social Democratic Party. Included as an Appendix to the book is Leviné's own report on the first All-German Soviet Congress. His lengthy speech can be summarized in one extract. Recounting the failure of the “left” to get any motion passed, he said of the first point:to declare Germany a United Socialist Republic is one to which the SPD is particularly opposed. They do not want a Socialist Republic. (p. 194)The Spartacus Revolt in Berlin and Hamburg was crushed but a group of intellectuals, poets, artists and anarchists managed to proclaim a Soviet Republic in Munich, which hung on for two months, headed by Ernst Toller the dramatist. Sensing collapse and defeat Toller's group packed it in and Leviné, instructed by the Spartacus Group and assisted by the Russians, took over. 

His régime was soon crushed by the army under Social Democratic orders. In its proclamation the SPD asserted:The troops of Hoffman's [the SPD Prime Minister] come not as enemies of the working class, not as White Guards, but as protectors of public safety and order. Comrade Hoffman is no reactionary but a radical champion of the socialist movement. (p. 194)The Bavarian Soviet was ruthlessly suppressed by working men, Social Democrats. Leviné was sentenced to death and faced the firing squad on the first day of May. His last speech to his murderers in the court showed him to be a man of considerable perception and boundless party loyalty: he was aware that the policy of the Spartacists was wrong, but was unable to betray it.
Harry Young | Socialist Party of Great Britain
