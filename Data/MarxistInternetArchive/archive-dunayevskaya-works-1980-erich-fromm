Raya Dunayevskaya 1980
Editor's Note: March, 2000 marked the 100th anniversary of the birth
of the Frankfurt School's Socialist Humanist Erich Fromm. We print Raya
Dunayevskaya's "In Memoriam" to Fromm. It appeared in the April 1980 NEWS
& LETTERS and can be found in THE RAYA DUNAYEVSKAYA COLLECTION, 7059.
The In Memoriam here is taken from it, and from a longer manuscript not
yet in the collection. We also publish here excerpts from two of her early
letters to Fromm. More of the correspondence between Dunayevskaya and Fromm
from 1959 to 1978 can be found in THE RAYA DUNAYEVSKAYA COLLECTION, 9976-10061.
The many articles that poured forth in 1980 when Erich Fromm died on
March 18 all praised him only as a "famous psychoanalyst." The press, by
no accident at all, failed to mention that he was a Socialist Humanist.
Moreover, in writing MARX'S CONCEPT OF MAN (which succeeded in introducing
Marx's Humanist Essays to a wide American public), and in editing the first
international symposium on SOCIALIST HUMANISM, he did so, not as an academician,
but as an activist. In inviting me to participate in that dialogue between
East and West as well as North and South, he stressed that "it took quite
a bit of courage on their (East Europeans) part to write something for
this volume, for no matter how diplomatic the language, they were open
attacks on the Soviet Union." 
Erich Fromm was an original. In attempting to fuse Marx and Freud, it
wasn't so much the audaciousness of such a move in the 1920s that needs
to be stressed, but the fact that even when he was a most orthodox Freudian,
it was social psychology that interested him; his use of psychoanalytic
mechanisms were as a sort of mediating concept between the individual and
the social. In any case, as he moved away from orthodox Freudianism to
elaborate his own version, it was clear that he was breaking not only with
Freud but with the famous Frankfurt School and its "Critical Theory," and
that, not because he was moving away from Marxism, but coming closer to
it. Here is how he put it in his intellectual autobiography:
"I consider Marx, the thinker, as being of much greater depth and scope
than Freud....But even when all of this is said, it would be naive to ignore
Freud's importance....his discovery of unconscious processes and of the
dynamic nature of character traits is a unique contribution to the science
of man which has altered the picture of man for all time to come" (BEYOND
THE CHAINS OF ILLUSION: MY ENCOUNTER WITH MARX AND FREUD).
On Fromm's initiative (and to my great surprise since I kept far away
from any psychoanalysts even when they laid claim to Marxism), I received
a congratulatory letter from him on the publication in 1958 of my MARXISM
AND FREEDOM. The period of the 1950s was a most difficult one for Marxists,
what with McCarthyism as well as nuclear bomb development permeating the
land. Dr. Fromm had helped organize the National Committee for a Sane Nuclear
Policy in 1957, but that was not what he wrote to me about. Rather, the
subject that then aroused his passionate interest was the restoration of
Marxism in its original form of "a new humanism," cleansed of the perversion
of Russian and Chinese Communism. So magnificently an objective human being
was he that he refused to be deterred either by the fact that I let my
hostility to psychoanalysts show by telling him that workers in Detroit
shops referred to them as "head shrinks," or even by the fact that I criticized
his own essay on "Marx's Concept of Man" as abstract. Here is what he answered
me:
 "As to your criticism of my essay that it is too abstract and does not
discuss the humanism of Marxism concretely, I cannot offer any argument
....As to the substance of the points you make about the concrete nature
of Marx's humanism, I naturally entirely agree with you. Also about what
you write of the role of the plant psychoanalyst and Daniel Bell's position." [1]
Our correspondence continued for two decades. It also gave me rare glimpses
into the whole subject of the famous Frankfurt School, of which he was,
after all, one of its most famous personages, the one who influenced them
all on the "integration" of psychoanalysis into Marxism. The lengthy unabating,
sharp debate with Herbert Marcuse in the pages of DISSENT over 1955 and
1956 was not the main issue. He retained too much regard for Herbert Marcuse's
REASON AND REVOLUTION as the seminal work it was. No, what did arouse his
ire most was the duality of Adorno's and Horkheimer's departure from Marxism
on the one hand, and the attraction that that held for the "New Left."
Here is how he summed it up in a letter to me dated Nov. 25, 1976:
 "I get quite a few questions from various people who study the history
of the Frankfurt School. It's really a funny story: Horkheimer is now quoted
as the creator of the critical theory and people write about the critical
theory as if it were a new concept discovered by Horkheimer. As far as
I know, the whole thing is a hoax, because Horkheimer was frightened...of
speaking about Marx's theory. He used general Aesopian language and spoke
of critical theory in order not to say Marx's theory. I believe that that
is all behind this discovery of critical theory by Horkheimer and Adorno." 

As against the movement away from Marx that he sensed in the Frankfurt
School, he himself tried in every possible way to disseminate Marxist-Humanism
into all fields, including his own-psychoanalysis. Consider his attempt
to convince me – who was in no way involved in psychology – to write a piece
for a psychoanalytical journal. That suggestion came after I had told him
the story about Susan E. Blow – a Hegelian and one of the very first women
educators – who was a patient of Dr. James Jackson Putnam, an American pioneer
of Freudian psychoanalysis. She aroused Putnam's interest in Hegelian philosophy
to such a great extent that he, in turn, tried to interest Freud. Freud,
on the other hand, was so opposed to introducing philosophy into psychoanalysis
that he criticized any attempt to place psychoanalysis, as he put it, "in
the service of a particular philosophical outlook on the world." [2]
Here is what Dr. Fromm wrote me:
 "What you wrote about Dr. Putnam who became interested in Hegelian dialectics
through his patient I did not know, and find it of considerable historical
interest, and Freud's reaction to Putnam's philosophical remarks [are]
also an interesting historical footnote to Freud and the history of the
psychoanalytic movement. Why don't you write a note on this and publish
it somewhere? I have no connections with psychoanalytic journals except
CONTEMPORARY PSYCHOANALYSIS, which is published in New York. I am sure
they would be glad to publish a note on this historical datum, and it should
at the same time be published in the Spanish psychoanalytic journal, Revista,
of which I am still formally the director. If you would be inclined to
do this, I would be happy to send it myself to the New York and the Spanish
journals. I shall also look up Freud's letters to find the remark in which
he comments on Putnam's letter, or do you know to whom Freud wrote this
remark about Putnam?" 
Fromm's eyes always were on the future and a new class-less society
on truly human foundations. Least known of his multi-dimensional concerns
was the relationship of Man/Woman and by no means on just a psychological
scale. Rather it was the need for totally new human relations in the Marxian
sense: a global vision of the future meant also a look back into the past.
Thus, he found Bachofen's studies into matriarchal society very congenial,
not because he believed in the existence of matriarchal society, but because
it, at least, allowed one a vision of an alternative society to this patriarchal,
class, alienating society in which we live. In relating patriarchy to class
domination, he had invented the magnificent phrase for it: "patricentric-acquisitive." 
Far from remembrance of things past being a question merely of memory,
it brings into view the unity of Man/Woman; the human being as a totality,
being not just a quantitive measure but something dialectical, showing
movement, a movement forward. It was what Fromm stressed when, in creating
an international forum for his SOCIALIST HUMANISM, he emphasized that Humanism
was not just an idea, but a movement against what is, a glimpse into the
future. Listen to what he wrote me when he heard I was relating ROSA LUXEMBURG,
WOMEN'S LIBERATION, AND MARX'S PHILOSOPHY OF REVOLUTION:
 "I feel that the male Social Democrats never could understand Rosa Luxemburg,
nor could she acquire the influence for which she had the potential because
she was a woman; and the men could not become full revolutionaries because
they did not emancipate themselves from their male, patriarchal, and hence
dominating, character structure. After all, the original exploitation is
that of women by men and there is no social liberation so long as there
is no revolution in the sex war ending in full equality....Unfortunately
I have known nobody who still knows her personally. What a bad break between
the generations." 
That letter was written on Oct. 26, 1977. It is now March 19, 1980,
and Fromm is dead. And I say, dear Youth, let's not let another "bad break
between generations" occur. To prepare for the future one must know the
revolutionary past. Getting to know Fromm as a Socialist Humanist is a
good way to begin.
Oct. 11, 1961
What matters is...the need to discuss the Humanism of Marxism concretely.
I do not mean to reduce philosophy to what Trotsky used to call "the small
coin of concrete questions." I mean the discussion must be in terms of
what Marx called the "abolition" of philosophy through its "realization,"
that is to say, by putting an end to the division between life and philosophy,
work and life, and the different intellectual disciplines and work as the
activity of man, the whole of man, the man with heart, brain and physical
power, including the sensitivity and the genius of the arts. It is this
which Marx literally pounds at in the [1844 Humanist] Essays both when
he deals with the five senses and when he deals with the limits of psychology
which excludes "industry," or the workshop where a worker wastes most of
his time but also gains from it the spirit and cohesiveness of revolt.

July 21, 1964
While he [Herbert Marcuse] attacks the status quo, he himself has very
nearly given in to technology by attributing to it truly phenomenal powers.
Feeling that this may be true, he tries for a way out, to find "absolute
negativity," but since he has turned his back on the proletariat as the
revolutionary force, he looks elsewhere; very nearly on the last page [of
Marcuse's ONE-DIMENSIONAL MAN] he finds the third underdeveloped world
to modify his overwhelming pessimism.
Now, in his previous discussion on Hegel's Absolute Idea, which he rejected,
he stated that it was no more than the proof of the separation of mental
and manual labor in the PRE-technological stage of history.[3] If this
is so, if Hegel, after all his valiant striving to extricate philosophy
from theology, retreated from concrete history to abstract absolutes not
because he was, as a person, an opportunist; or, as a visionary, lacked
the belief that the human embodiment of that keystone of his dialectic – "absolute
negativity"  – could possibly be that "one-dimensional man" working a single
operation in a factory but that Hegel's historic barrier was the pre-technological
state of society, then how can HM maintain that this is our fate? If the
pre-technology and the FORCIBLE leisure needed for intellectual thought
sends you back to abstractions, then how could it also have achieved the
highest stage of human thought[,] for HM does believe that Hegelian dialectics
and Marxian revolutionary philosophy are the very modes of thought we now
lack, and were achieved at a less than advanced industrial pace?
My contention had been that, irrespective of what retreat Hegel CONSCIOUSLY
hankered for, when confronted with the contradictions in his society making
havoc of his beloved field of philosophy and PHILOSOPHIC CHAIRS, the OBJECTIVE
COMPULSION to thought came from the French Revolution, not from pre-technology
or post-technology, and the logic of this, JUST THIS, revealed the pull
of the future, the new society which Hegel named "Absolute Idea" but which
we first can understand in its material and most profound implications
and therefore our age must work out THAT absolute. 
1. In another letter Fromm wrote: "My relations with COMMENTARY are
not good. Years ago Mr. Podhoretz rejected something I had written because
it contradicted the majority opinion of American Jews. I wrote him a sharp
letter about his concept of freedom...." 
2. Nathan G. Hale, editor, JAMES JACKSON PUTNAM AND PSYCHOANALYSIS (Cambridge,
Mass.: Harvard University Press, 1971), p. 43. 
3. This refers to Marcuse's letter to Dunayevskaya, Dec. 22, 1960; see
THE RAYA DUNAYEVSKAYA COLLECTION, 13822. 
Dunayevskaya Internet Archive