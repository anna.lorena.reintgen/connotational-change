Vere Gordon Childe, 1930
First Published: by Cambridge University Press, 1930
Mark-up: Steve PainterThe Middle Bronze Age is much more than a mere continuation of the previous
		period. It witnessed the rise of schools of metallurgy in regions where
		Early Bronze Age types are rare and among peoples who had spent the preceding
		period in a belated Stone Age. The new communities of metalworkers made
		an original contribution to the common European stock of types. Thus many of
		the bronzes illustrate a new spirit instead of being just improvements on
		the older types. Conversely, in several centres of early metallurgy,
		particularly South-eastern Spain and Great Britain and to some extent also
		Central Bohemia, Middle Bronze Age types are either totally lacking or represented only
		by stray objects and a few hoards. The principal new provinces are
		Scandinavia, the South-west German uplands and Hungary, to which may be added
		the peculiar developments in Upper Italy and the Rhone valley. It will
		be seen from a glance at the map that these centres lie along and on either
		side of the great central amber route. The regions remote therefrom failed
		to participate in the new developments.While the earlier Aunjetitz culture had been flourishing in Bohemia, and
		plentiful metal objects were being buried with beakers and early food
		vessels in Great Britain, the peoples of Scandinavia and North Germany still
		used stone tools, supplemented by a very few bronzes imported from England
		or Central Europe. To that epoch should be assigned the latest megalithic kists and the separate graves high up in the barrows of the Battleaxe folk.
		The latter had obtained complete dominance before the secrets of
		metalworking had been mastered locally. But smiths were eventually attracted
		to Denmark, which became the centre of a new metalworking province,
		termed Germanic or Teutonic. Besides Denmark it embraced the Norwegian coasts,
		Southern Sweden, North-west Germany and Central Germany north of Magdeburg.The distribution and grouping of the barrows for very few settlements are known
		produces the impression of a semi-nomadic people, living in little
		groups with a limited regular range. It must be remembered that the dry
		sub-boreal conditions had converted the North European plain into an open
		parkland, verging on steppe in some districts. The Bronze Age population
		buried their dead, like the Neolithic Battleaxe folk, under barrows,
		normally in the extended position and very often enclosed in coffins formed out
		of hollowed oak trunks.Besides flint tools sickles, scrapers, knives, and even celts — flanged celts,
		palstaves or even socketed celts, button sickles and knives — were
		manufactured locally in bronze. In men's graves weapons are abundant. From such
		come the splendid swords with inlaid pommels, great socketed spearheads sometimes 35cm long, and, more rarely, heavy battleaxes with a
		shaft-hole. The arrows were still tipped with flint points, and even
		flint daggers remained current, though inferior in workmanship to the amazing
		products of the last Neolithic period.Unusually favourable circumstances have preserved to us substantial vestiges of
		the actual clothing then worn. Men wore a close-fitting woollen cap; a
		sort of blanket was girt round the body under the arms, while the shoulders were
		covered with a plaid fastened by a brooch at the throat. Women were clad in a
		short-sleeved jacket, like a pullover, and a skirt formed by girding a blanket
		round the waist. Their long hair was held in place by a net. Both sexes
		were shod with leather boots. The simple woollen dress was set off by a wealth
		of gold or bronze ornaments. For fastening the cloak two-piece fibulae
		(Fig. 112) were used, but neat studs were also manufactured. The leather
		girdles were fastened with the clasps already described in Chapter III (Buttons, clasps, studs and tutuli) and
		decked with tutuli. These are circular. Those worn by women have a central
		spike while the disk may attain a diameter of 28 cm. (Fig. 133). Men's
		were of more modest size with a hollow boss or umbo in the centre. Males wore
		bracelets on the left arm only, females on both. The most distinctive and
		beautiful terminate in spirals or pairs of spirals. Finger-rings and bracelets
		of double gold wire were favoured by both sexes. Finally, women wore the
		broad gorgets, like Fig. 132. Necklaces of amber or glass beads are less
		common.Towards the close of the period toilet articles in the form of tweezers,
		single-edged razors (Fig. 81) and bronze combs begin to appear in the
		graves.Pottery is rare and exceedingly rough. Finer vessels were made of wood. Several
		neat cups of this material have survived. They appear to have been turned on a pole-lathe, are provided with a band handle and sometimes are
		adorned with little tin nails forming a star pattern on the base.The Teutons of the Middle Bronze Age displayed high artistic capacity. Their
		aesthetic taste is best exemplified in the shapely weapons and graceful
		ornaments and their decoration. Axes, sword-hilts (Fig. 64), collars and
		tutuli are covered with running spiral patterns engraved with astonishing
		accuracy. In the later phases of the Bronze Age the first delicacy is lost, but
		we shall see a fine revival of curvilinear ornament in the latest
		period that corresponds to the southern Hallstatt age.Probably to our period belong also some rock-engravings, found principally in
		Bohuslan, Southern Sweden. As artistic productions they are far inferior
		to the delicate geometrical art of the bronze-worker or to the older
		naturalistic engravings of the Arctic Stone Age hunters[1]
		but they are nonetheless full of human interest. They depict in fact scenes
		of daily life — men at the plough, combats between warriors protected by
		round shields, very like those we shall meet in the Late Bronze Age, and naval
		battles between great rowing galleys. Different in style from the foregoing are the engravings on a Middle Bronze Age grave kist unearthed at Kivik,
		Schonen. One slab depicts a prince in a chariot, directing the slaughter
		of three naked captives quite in the spirit of certain early Sumerian scenes.
		Another slab (Fig. 155) represents some rather puzzling ritual
		ceremonies: its upper register shows a band of musicians blowing long curved
		trumpets or playing other less easily recognisable instruments; below in
		the middle we see eight women (looking very like seals!) grouped symmetrically
		about a large cauldron. The bottom register is taken up with another
		group of captives being slaughtered.The significance of these uncouth carvings cannot be overestimated. They
		afford the oldest positive proof of the use of wheeled vehicles north of the
		Alps and probably also of the domestication of the horse. The use of the
		musical instruments, well known in the succeeding period, is here dated back well into the second millennium B.. The cult
		scene is even more important; for it anticipates a ceremony described by
		Strabo as observed among the Cimbri who hailed from Denmark, and more clearly
		depicted on a famous bronze cauldron of later date discovered at
		Gundestrup in Jutland. The Greek author describes how among the Teutonic tribe
		a priestess used to cut the throats of prisoners of war so that their
		blood gushed into a great cauldron. Omens were obtained in this manner. The
		Kivik monument implies a similar gruesome rite among the ancestral Teutons
		about 1400BC, unrolling in salutary wise a blood-stained page which we
		should gladly forget.The free use of the spiral, and especially of interlacing spiral figures
		exactly as at Mycenae, has been thought to betoken Aegean influence, the Irish
		and Scottish carvings being sometimes invoked as links. But in point of fact
		the Teutonic Bronze Age was singularly original and independent. Hungary,
		indeed, supplied models for a number of types, but imported foreign commodities
		are rare. Of course the metals, copper, tin and gold, had to be
		imported from the South or West, but they arrived raw and even unalloyed. Of
		foreign manufactures we find from the East Mediterranean glass beads, from
		Italy a sword with lead solder on the hilt and from South-west Germany
		wheel-head pins, but that is all. Conversely Teutonic bronzes were never
		exported at this date. Save for a couple of two-piece fibulae from the Tyrol
		and North Italy, the unmistakable bronzes we have described only found
		their way very sporadically just across the border of the Teutonic province
		into Holland and Thuringia. The imported metals must have been paid for
		entirely in amber or slaves. In the Late Bronze Age we shall find affairs changed and Teutonic manufactures reaching Hungary and Switzerland.The heaths and upland country of Holland, Western Germany, Bavaria, Upper
		Austria and South-western Bohemia are dotted over with groups of barrows
		whose furniture marks each as just a specialised manifestation of a single
		culture. In the enormous area local differences are only to be expected, and
		in fact extend to tools and weapons as well as vases and ornaments. Still it is
		convenient and justifiable to treat all the local groups together as
		the Tumulus Bronze culture.The tumulus-builders are thought by many authorities to have been Kelts, but
		this, as we shall see, is dubious. Physically they were distinctly
		mixed, including both long-heads and round-heads as well as mesaticephals.
		But they were the direct descendants of the peoples who had occupied the
		South-west German uplands and the Alpine slopes towards the close of the Stone
		Age. Among these the Battleaxe folk, as in the North, would have been
		the most prominent. Indeed in Upper Bavarian,[2] Alsace,[3] and elsewhere
		in the area barrows with Corded Ware have been found in or near the
		Middle Bronze Age cemeteries, forming as it were their nuclei. These
		highlanders and heathmen learned metalworking late, like their Scandinavian
		relatives, and learnt it from the Danubian school, as the earliest bronzes even
		in Alsace and the French Jura prove.Economically the barrow-builders must have been largely pastoral and
		semi-nomadic. There is no doubt that they cultivated grain like our own
		ancestors, but they did not settle in the fertile valleys like the Aunjetitz folk. Their favourite haunts were poor and hilly regions that are today
		heavily timbered unless artificially cleared, but that under the dry
		sub-boreal conditions were heath or parklands, as surviving xerophilous plants
		indicate. As characteristic regions we might mention the swampy tract
		of alluvial sands, covered today with oak woods, north of Haguenau, near
		Strasburg, and the lovely slopes of boulder clay above the little glacial
		lakes behind Munich.Owing to their mode of life and perhaps under stress of periods of real
		drought, the tumulus-builders spread far. From centres in Upper Bavaria or
		Wurrtemberg the slopes of the Hercynian forest in Bohemia were early colonised,
		and by the Late Bronze Age we find allied groups as far away as Bosnia.
		So, too, from the terraces above the Upper Rhine and the Jura the greater part
		of Eastern and Central France was overrun as far as Charente.Settlements are practically unknown, but the graves are distinctive. The burial
		place is always marked by a tumulus of earth or stones generally
		covering one or two interments only, but sometimes serving as a collective
		sepulchre. The remains were laid, not in a trench, but just on the surface
		of the earth, protected by stones. The normal rite was inhumation in the
		extended position. But cases of cremation occur among even the earliest Bronze
		Age interments as under Neolithic barrows in the same area. This rite became
		increasingly common as time advanced. But the ashes were generally just
		deposited on the ground; only where the influence of the Urn-field folk,
		described in the next chapter, was strong in the Late Bronze Age, were the
		ashes enshrined in cinerary urns.The warrior was armed with an axe, a dagger, and a rapier or a spear with socketed head. The axeheads never possessed
		shaft-holes, but consisted of flanged celts (everywhere), very slender winged
		celts (Fig. 21) (Wurtemberg and Upper Bavaria), palstaves (Fig. 14) (South-west Germany and Holland) or Bohemian palstaves (Fig. 23) (in
		the Palatinate and Bohemia). The daggers were very seldom mounted in bronze
		hilts, but at least in Bavaria bronze-hilted rapiers are common. As a
		defence, the warrior carried a round targe of wood or leather, studded with
		hollow bronze knobs, which alone have survived. The bow was used in hunting,
		and bronze arrowheads have been found even in women's tombs though they are
		far from common. Sickles too of the button type (Fig. 92) were
		sometimes buried in the graves, but, with the possible exception of a small
		group in Franconia and Bavaria, single-edged knives appeared first in the
		Late Bronze Age.The dress was probably similar to that worn in Denmark. Men fastened the cloak
		at the throat with a single pin; women always used two crossed on the
		breast. In Wurrtemberg the type with an eyelet in the swollen neck was at first
		the standard, to give place in the Late Bronze Age to giant forms with
		ribbed necks (Fig. 108). In Bavaria and Bohemia mushroom-headed
		varieties were popular, while in Alsace the nail-headed type was once general,
		though later superseded by wheel-headed and ribbed types. Bracelets were worn
		by both sexes, exactly as in Denmark. The most general type was a simple
		rod, tapering at both ends and bent into an open ring, but forms like Figs. 118-119, andon the Rhine, cylinders were quite popular.
		Finger-rings with ribbon bodies like Fig. 125, were displayed upon the fingers, and the legs were sometimes burdened with anklets like Fig. 121. Such were an Alsatian speciality. From the
		Rhine they spread westward into East-central France, helping to mark the
		expansion of the Tumulus culture. The girdle might be fastened with a
		spiral-ended hook and was studded with hollow bronze buttons, small spiked
		tutuli and, later, wheel- pendants. Strings of amber and glass beads, bronze wire
		coils and pendants including the sacral ivy leaf, were hung round the neck, though ingot torques were sometimes worn.The pottery, often very graceful, varies materially from region to region. But
		the best proof of the fundamental homogeneity of the whole culture is
		the fact that any given local ware is represented by stray specimens in almost
		all the other regions. In the Rhine valley, Wurrtemberg and Upper
		Bavaria the commonest shapes are hemispherical cups, jugs with globular bodies
		and wide funnel-like necks, and big urns with short necks and handles on
		the shoulders. In Bohemia and the Palatinate the bowls may have pedestals and a
		handle, the jugs bear four warts on the belly, and the urns are squat
		with conical necks. The ornamentation is also different. Nonetheless
		pedestalled bowls quite comparable to the Bohemian are found also in Alsace.The vases from Bohemia and the Palatinate are decorated either by simply
		roughening the surface or with incised hatched triangles or chevrons of cross-
		hatched ribbons. Roughening was also used decoratively in Wurrtemberg and the
		other groups, but conical warts sitting on the shoulders are a common
		decorative device (Fig. 156). The most distinctive of all, however, is
		the so-called fretwork ornamentation (Kerbschnitt). The effect at first
		was similar to the “false relief” on British food vessels, but in this case
		the little triangles and lozenges were actually cut out of (excised from) the soft clay (Fig. 157), Later, stamps of various shapes including circles came into use. In
		either case the fretwork patterns are arranged, as on beakers, in zones or
		radiating from the bases of vessels. Fretwork pottery is particularly common
		in Wurrtemberg[4] and on the Upper Rhine but is represented also even in Bohemia
		and Upper Austria, all down the Rhine and right across France to the
		Departments of Gard, Puy-de-Dome and Charente. Similarly, jugs or urns with
		conical warts of Swabian style are found in Bohemia and in Western Lorraine
		(Dept Meurthe et Moselle).[5]The third new group of the Middle Bronze Age had its seat in Upper Italy, south
		of the Po. It is distinguished by a curious sort of settlement termed a
		terramara, the “black earth”, full of organic refuse, having been used as
		fertiliser by the local peasantry. A terramara is a low, oblong mound,
		12-15 feet high, formed by the debris of prolonged occupation. On exploration it
		is found that the settlement had been fortified and laid out on a
		regular plan, common to most sites. The occupied area, which may cover nearly
		200,000 square metres (50 acres), is always trapezoid in shape and is
		surrounded by a moat, 15-25 yards wide and about 12 feet deep. The moat was
		traversed by a single bridge and could be flooded by a canal joining it at
		the acute angle of the trapezoid. Some 20 yards inside the moat rises a broad
		rampart of earth, sloping on the outside but supported within by a wooden
		construction, resembling a series of small log-cabins and termed in Italian the
		contraforte. The area thus enclosed reveals on excavation a regular
		forest of piles. These it is supposed supported the actual huts which would have been “pile-dwellings on dry land”. They appear to
		be grouped along lanes parallel to the long sides or at right angles
		thereto. On the south side there is generally an earthen mound encircled by an
		inner moat.Two cemeteries were normally attached to each terramara; they are miniature
		terremare with moats of their own. The inhabitants of the terremare (termed
		terremaricoli) burned their dead, preserving the ashes in cinerary urns. These
		are found packed close together in the necropoles.The terremaricoli were prosperous farmers. The number of sickles or moulds for
		their manufacture testify to the importance of agriculture. The
		domestication and employment of horses is attested by cheek-pieces from
		bits. But the terremaricoli were also skilled craftsmen and keen traders.
		Metallurgy is illustrated by numerous stone moulds, weaving by whorls, loom-weights and spools of clay. Trade brought them, besides metals, amber from
		the Baltic and glass beads from the Eastern Mediterranean.Polished stone celts and axes, flint knives, scrapers, arrowheads and even
		daggers are not uncommon in a terramara, and tools of bone and horn are
		varied and plentiful. The distinctive bronze tools are flanged and winged
		celts, flat chisels, little awls, and needles, numerous grooved sickles, and
		a few single-edged knives. The warrior carried flat triangular daggers with
		bronze or horn hilts of ogival forms as well as three types of sword the
		short sword with flat blade that is just an elongation of the flat dagger, a
		rapier of continental form, and another with a short tang formed by the
		prolongation of a pronounced midrib, which is derived directly or through Sicily
		from Minoan types. Odd ogival blades with a short flat tang and projecting shoulders may have been
		hafted as daggers or as spearheads. But socketed spearheads were also in
		use.A great variety of pins were worn. Wiry headed varieties, singly or doubly
		looped and blossoming into spirals and double spirals, are the most
		distinctive. Little bone wheels that are common may also have been pin heads.
		Safety-pins of the violin-bow form are late and rare in terremare. But
		double-edged razors were in regular use and cast locally (Fig. 85). Another toilet article was a comb of bronze or bone.Terramara pottery is characterised above all by the extraordinary crescentic or
		horn-like projections that surmount the vase handles (ansa lunata, ansa
		cornuta). Such are just exaggerations of a feature, found earlier on Italian
		pottery, to which there are analogies in Macedonia, Aetolia, Malta, Sicily
		and Sardinia. The shapes include shallow cups, pedestalled vessels, and
		inverted conical or biconical urns, generally without necks. Warts, pinched out
		of the clay and often encircled by incisions, are the principal decorative
		device.An approach to plastic art is seen in rude clay figurines and models of
		animals. The bone combs, disks and hilts are often richly carved with zig-zags,
		triangles, concentric circles, or even running spirals.An important school of Italian prehistorians, founded by the late Professor
		Pigorini, hold that the terremaricoli were the original Italici from whom
		the Umbrians, Latins and Sabines were alike descended. A genuine terramara near
		Taranto and other more ambiguous remains from Central Italy would be
		the monuments of the “Aryanisation” of the peninsula. It is at least certain
		that the terramara industry became dominant throughout its whole length.
		According to Pigorini these Italici would have been invaders from beyond the Alps. But despite
		general analogies in sites like Toszeg on the Tisza, no genuine terremare have
		been found in the Danube basin, and the exact starting-point of the Italici
		remains uncertain.The Middle Bronze Age in Hungary begins with the desertion of several Early
		Bronze Age sites and a break in the ceramic record a layer yielding no pot-sherds in others. Yet by the end of the period we find the whole plain occupied
		by extensive communities, each traceable by their pottery to Early
		Bronze Age groups, though the traditions are now differently blended. At the
		same time a number of bronze types, found stray or in hoards and dated by
		their context abroad, show that Hungary was now the seat of a very vigorous and
		original bronze industry.The most distinctive forms of the period are the shaft-hole axes described in chapter III (Fig. 163); for celts were not manufactured locally to
		serve as axeheads. The forms are probably derived from Copper Age models;
		the distribution suggests that they were manufactured principally in
		North-east Hungary, the copper being derived presumably from the Matra
		Mountains. Thence they were exported as far as Upper Austria, Bavaria,
		Mecklenburg, the Ukraine and Serbia. Besides an axe the Hungarian warrior
		carried a spear with socketed head or very rarely a rapier. Small ogival
		daggers are occasionally found in the late graves. Few tools can safely be
		assigned to the Middle Bronze Age, but the ornaments were varied and
		distinctive, and enjoyed a wide popularity even outside Hungary. Many Early Bronze Age types of pin remained in use throughout the Middle Bronze
		Age. But the most characteristic native type had a mushroom head
		vertically pierced (Fig. 165) or with a lateral eyelet just below it. The
		bracelets tended to be massive and richly engraved. The ends are
		either thickened or coiled into opposing spiral disks (Fig. 118). Hardly
		less distinctive are the cylinders of bronze ribbon ending in wire
		spirals worn on the legs and arms (Fig. 166).A great variety of pendants were sewn on the girdle, strung on necklaces,
		twisted in the locks or hung down over the breasts or the middle of the
		back. Most are of bronze, but gold specimens are known. Besides the hollow
		buttons and spiked tutuli, common also in other regions, many varieties of
		the sacral ivy-leaf pendant were manufactured in Hungary and exported thence as
		far as Alsace. Another important form is the pectiform or comb-shaped
		variety that formed a sort of tassel to ornamental chains hung down the back
		(Figs. 167, 168). The gold spiral lock-rings, current already in the Early
		Bronze Age, continued to be worn.Great aesthetic taste was shown by the Hungarians both in the grace of their
		ornaments and in the magnificent patterns engraved upon their weapons. In North Hungary in particular, scrolls luxuriate over the blades and butts of
		axes as lavishly as the more austere spirals oi contemporary Teutonic
		art (Fig. 163).The remains from the relatively sterile layer at Toszeg suffice to show that
		even in the first half of the Middle Bronze Age the Hungarians had at
		their command the motive power of horses. Apart from these layers and a few
		inhumation burials, connected deposits are rare, as if there had been a
		considerable exodus at the end of the Early Bronze Age. The lacuna may be the
		reflex of the abruptly appearing invaders of Italy described in the last section as the
		terremaricoli. Nevertheless before the period closes the abundant remains
		must betoken a large and settled population, descended from old local stocks.
		We rely for our information chiefly upon cemeteries which may be divided
		by burial rites and pottery into several groups. In the largest group termed
		Pannonian, extending from the Austrian borders south-eastward into Central
		Hungary, as well as in the cemeteries of the Banat and North-east Serbia that
		continue the same line, cremation was the sole rite observed. In Southern
		Hungary and Slavonia inhumations also occur, and in the extreme north-east the
		latter rite was alone practised.The tombs are poorly furnished with bronzes, but these conform to the Middle
		Bronze Age types familiar from the hoards. A wealth of vases
		counterbalances this poverty in metal grave goods. The ashes were enclosed in
		cinerary urns, in the Pannonian group generally great pitchers with
		trumpet-like necks, tall piriform bodies and one or two handles. The South
		Hungarian urns belong to the class of two-storied pots (Fig. 162). The
		accessory vases and some urns are elaborately decorated. In the Pannonian group
		proper wide bands of true fretwork (excised, not stamped) are combined
		with stab-and-drag lines, uniting impressed concentric circles (Fig. 159-160). In the more southerly groups fretwork is no longer used, while the
		incised lines often form running spirals, maeanders or rosettes. In the
		north-east the main decorative device was the conical wart, applied or pinched
		up and often surrounded by deep grooves.Both in Slavonia and Serbia art was also manifested in clay figurines decorated
		in the same style as the vases. They represent a female personage,
		wearing a richly embroidered bodice and a flounced skirt and decked with necklaces and pendants.
		The most famous idol of this class, found at Klievac in North Serbia,
		was unfortunately lost during the war. The same region has yielded model
		thrones, axes and other clay votives.The vase forms in most cases can be traced back to Early Bronze Age groups.
		Pannonian jugs and dishes have forerunners at Gata and Toszeg; the
		hour-glass mug of Perjamos reappears in the Banat cemeteries, Pannonian ware is
		decorated in just the same technique as the earlier Slavonian ware.
		Hence the Hungarian plain cannot have been entirely deserted by the Early
		Bronze Age population, though some of the original groups had shifted their
		territories or amalgamated with neighbours.Stray flanged celts and flat daggers of bronze as well as bone copies of common
		bronze pins have been found in several of the later “Neolithic”
		lake-villages of Switzerland. It would seem that the pile-dwellers lived on in
		a stone age throughout the Early Bronze Age and part of the succeeding
		period. By that time, however, we find in the Rhone valley, but unconnected
		with the lacustrine settlements, graves furnished with a distinctive series
		of bronzes. The tombs are either small megalithic kists containing a number of
		corpses, or individual graves without any barrow over them. The bronze
		industry, here represented, is inspired mainly by Bohemian and Hungarian
		traditions though there are some indications of influence from the Iberian
		Peninsula. The types, however, developed along quite individual lines.
		Distinctive are the spatuliform celts and the triangular daggers, often
		bronze-hilted. Besides pins with rolled or even knot-heads, trefoil and disk forms are characteristic,
		the latter being doubtless a local creation. So, in addition to simple
		rod bangles and ingot torques, broad bronze collars were developed in a
		specialised variant as described in chapter III. No pottery is known from these
		graves.The engraved bronzes illustrate a continued development of that system of
		purely rectilinear decoration that had been almost universal in the Early
		Bronze Age.The extent and age of the Rhone culture is not yet exactly determined. It is
		only mentioned here because, as we shall see, it is a prominent
		constituent of the oldest culture to which the name Keltic can be applied.In the British Isles the Middle Bronze Age is merely a continuation of the
		previous period, lacking the sharp demarcation observed in Italy or Southern
		Germany. Types distinctive of the period on the continent — early palstaves,
		ogival daggers, rapiers, socketed spear-heads and button sickles — are found
		stray or in hoards, particularly in the south; north of the Tay no rapiers have
		been reported and even early palstaves are rare. Yet there is no doubt
		that rapiers were actually manufactured in England, since moulds for their
		production are found there — practically the only known rapier moulds. These
		universal Middle Bronze Age shapes are associated in hoards with specialised
		local forms celts with broad flanges near the butt (Fig. 13), tanged
		chisels, spearheads with loops (Fig. 68), true torques, wide armlets
		with horizontal ridges and a sort of pin with a gigantic ring-head. Even
		socketed sickles or double-edged razor blades are exceptionally associated with
		early palstaves and rapiers.Nevertheless, save for a few ogival daggers, no distinctively Middle Bronze Age
		types are found in graves. Yet some of our barrows must obviously be
		contemporary with our Middle Bronze Age hoards. We therefore assign to this
		phase cremated interments, accompanied by vessels of Early Bronze Age
		antecedents whose descendants admittedly belong to the Late Bronze Age. Yet
		such graves may contain flat triangular daggers, and even stone
		battleaxes, though not flint daggers.The pot form, thus marked out as Middle Bronze Age, is the so-called cinerary
		urn in its earlier versions. It is just an enlargement of the food
		vessel. The commonest type, originating probably in Southern England, is known
		as the overhanging-rim urn. It undergoes a regular typological
		degeneration during the period. The oldest form, which is partly contemporary
		with the latest beakers, had an inverted conical body distinguished by
		a definite shoulder from the well- marked concave neck, which is surmounted by
		a broad overhanging rim or collar (Fig. 169). Before the end of
		the Middle Bronze Age the neck disappears, leaving us with rim and body only
		(Fig. 170). In the Late Bronze Age further decadence produces cordoned and bucket urns in which all that is left is one or two ridges encircling
		the body to represent the overhang of the original rim or this and also
		the shoulder below the former neck (Fig. 171). A contemporary form,
		originating in Northern England or Scotland and unknown south of the
		Thames, is just a magnification of the classical food vessel with grooved
		shoulder. It is therefore termed an enlarged food vessel.
		
		These large pots are made of very coarse clay and lack any slip or polish. They are nonetheless elaborately decorated,
		principally on the wide collar or the bevelled moulding inside the lip. The
		ornamentation is executed with a cord, with a chain-looped braid, or by simple
		incision. The cog-wheel technique and false relief have been abandoned.
		The patterns are simple zig-zags, triangles, chequers, lattices, and
		herring-boning.
		
		Contemporary with these urns, often used like them to contain cremated remains
		and not seldom associated with them in the same grave, go a variety of
		small vases termed “incense cups” or “pigmy vessels”. These are often of much
		finer clay than the big urns, but seldom approach the delicacy or
		technical excellence of beaker ware. The decoration includes incised lines
		combined with dots forming figures such as lozenges. One group of pigmy
		vessels, sometimes termed “grape cups”, are covered with knobby projections
		(Fig. 179). The latter, though quite different from the conical
		warts of Swabian and Hungarian pottery, recall the ornamentation on a class
		of Late Neolithic wares from the Danube basin. Another variety of incense
		cup has triangular slits in the walls, producing a sort of lattice effect (Fig.
		178). Perforations in the walls are indeed a common feature in
		the whole class of pigmy vessels.
		
The funerary pottery and bronzes produce a rather depressing picture of our
		civilisation at this date. That is to some extent offset by the discovery
		in graves of the period of cups of shale, amber and gold[6] that are, for
		their age, unique west of the Aegean. The shale and amber cups are simple
		flat-bottomed vessels with ribbon handles. In shape they recall the wooden cups
		from Denmark and seem also to have been turned on a pole lathe.
		Ornamental horizontal grooves may encircle the body, parallel to the rim, and
		decorate the handle. Five shale and two amber cups are known, all from Southern England, west of
		Brighton. The gold cup, from a cairn at Rillaton in Cornwall, is of similar
		shape, hammered out of a single piece of metal and decorated with horizontal
		corrugations. This group of vessels, unique in North-western Europe, shows
		that Britain had not lost her originality in the Middle Bronze Age. Moreover,
		the islands retained their place in European trade. Amber was still
		imported from Denmark and beads of blue faience came by coastal routes from
		Crete or Egypt.[7] The most notable of the latter imports are the segmented
		beads. The type was current in Crete from the end of Middle Minoan times (1600BC), but the trinkets found in Britain are said to resemble rather		Egyptian specimens dated to the twelfth century. Similar beads or bone copies
		thereof have been found in late graves of El Argar type in South-eastern
		Spain and in megalithic tombs in South-west France and Brittany. Britain's
		principal export at this period would presumably be tin. But British and
		Irish gold torques, looped spearheads and other British types of the Middle
		Bronze Age reached Northern France in considerable numbers. The spread of
		the palstave to Western Spain may also be connected with the Atlantic trade
		from the British Isles.
		
		Dwellings of the period in Britain cannot be distinguished. It is possible,
		however, that hill camps were already being built. A hoard of rapier
		blades was, it is reported, unearthed at the bottom of the trench encircling
		that of Drumcoltram in Dumfriesshire. The fort stands, not on the summit
		of the hill, but on a spur projecting westward from the hill about 675 feet above sea-level. The neck and
		flanks of the spur are defended by a wide annular moat, 9 feet deep and
		30 wide at the brim. The upcast from it has been piled up inside to form a
		rampart 9 feet high. A causeway 8 feet wide leads across the moat to a gap
		in the rampart. The fort is a good example of the simpler type widely
		distributed in the British Isles. The majority at least of the more complex
		forms
		belong to the Iron Age.
		
		For the rest, life in prehistoric Britain had undergone no visible change since
		the Early Bronze Age. Only after the lapse of a considerable interval
		was our rather sleepy development rudely interrupted by the Late Bronze Age
		invasion.
		
		Bronze figure from Sardinia
		
		1. BURKITT. Prehistory. Cambridge, 1924, p2132. NAUE. Die Bronzezeit in Oberbayern. Munich, 18943. SCHAEFFER. Les tertres funeraires dans la Foret de Haguenau. Haguenau, 19264. KRAFT. Die Kultur der Bronzezeit in Suddeutschland. Tubingen, 19255. DECHELETTE. Manuel d'archeologie prehistorique, celtique et gallo-romaine. Vol. n. Paris, 19106. NEWALL. “Shale Cups of the Early Bronze Age”. WAM. LIV7. Some authorities maintain that many of our vitreous beads were manufactured locally from slag. It remains certain that they imitate Aegean or Egyptian models. Chapter VI ContentsVere Gordon Childe Archive