
      Now it is clear to me from reading the catalogue of this exhibition that the
      promoters of it think that the working men, as we call them, of these parts do most
      seriously need some education in the fine arts, that they need to be told something
      about them which they do not know, in order that, when they look at pictures or
      other things professing to be works of art in future, they may be impressed by them
      in a different way from what they have been used to do: in other words, they want
      to educate people to look at pictures so that the pictures themselves may educate
      them afterwards. I agree with the promoters of this exhibition that the working men
      hereabouts do sorely need this double education, nor do I think that the richer
      classes would be the worse for it either for that matter; and the admission that
      this education is needed, that it should be necessary to explain to an ordinary
      intelligent person what the merits and aims of a picture are, just as one has to
      translate a piece of writing from a foreign tongue into our own: I say the
      admission that it is necessary to do this, and that it should be justly thought a
      good work for men to take trouble to do it without any other reward than what their
      own consciences may give them, shows that there is something amiss in the condition
      of the population hereabouts, and to cut the matter short, with the population
      throughout England, and indeed more or less throughout the civilized world.
    
      I don't know if I make my thought clear to you: let me put it in another way: here
      on the one hand is a mass of work, done with the utmost care, patience and skill by
      clever and energetic people, who have devoted their lives to doing it; all of them
      have been men of special gifts and some of them men of the very greatest capacity,
      who would have succeeded in almost any career that they might have taken to. That
      is on the one hand I say, and on the other hand is a mass of laborious and
      intelligent people, who have been brought up in such a way that the mass of work
      aforesaid is of no value to them, who don't know of its existence, and when by
      accident they come across any part of it do not understand what it means, till they
      have been educated into doing so. This is a strange case.
    
      You may say the men who took all this trouble about works of art and gave their
      lives to making them are dead, they belonged to a past state of society, we have
      our minds fixed on different aims now. Well to a certain extent that is true, and
      yet there is another wonder: for there are still men alive who understand what
      these dead artists meant, who chiefly think of them and their works and the times
      which produced them; and some of these are themselves engaged with no less energy
      though perhaps with less success than those dead men in producing works of art
      to-day: they are neither foolish nor ignorant as a rule, and though some of them
      may be accused of being dreamers without sympathy for the life of men in the
      present, that is not a necessary characteristic of them, and we should wrong them
      if we thought it was general with them.
    
      So you see you have on the one side artists and those who sympathize with them, and
      on the other the great mass of the people, who scarcely know or rather certainly do
      not know, what the word art means; these two groups of people so far as their ideas
      go, the thoughts which pass through their minds, might almost be living in two
      different worlds, although they are of the same country, the same race, speaking
      the same tongue.
    
      This separation of the lives of two kinds of people seems to me monstrous and
      strange: and yet in that very fact of this separation lies a stumbling block in my
      making myself understood by many of you; for to most of you, I imagine it will not
      seem either strange or monstrous, no more than one man being a carpenter, and
      another a lawyer's clerk; I can only say that to me it seems much as if the
      carpenter, he and his, and the lawyer's clerk had got so perversely developed that
      the carpenter could see nothing in a tree but so many feet of timber and was blind
      to the leaves and fruit and blossom of it; as if to the lawyer's clerk a sheep was
      parchment in the rough, and a goose so many bundles of pens. In other words again,
      it seems to me that the sense of beauty in the external world, of interest in the
      life of man as a drama, and the desire of communicating this sense of beauty and
      interest to our fellows is or ought to be an essential part of the humanity of man,
      and that any man or set of men lacking that sense are less than men, and lack a
      portion of their birthright just as if they were blind or deaf. This proposition if
      it be true does certainly impose a duty upon us, the duty of guarding jealously
      this birthright, this gift of humanity; for those who know anything of the arts
      know for certain that no other pleasure is so sure and so lasting as that which
      comes of our exercising this gift, sometimes actively as a worker, sometimes as a
      looker-on only. Ah friends, seeing how many pains there are which are imposed on
      men's lives, and are not avoidable, surely we were the worst of fools if we did not
      treasure up those pleasures which are, if we knew it, a natural part of humanity
      also; especially when they lie so ready to our hands; for in good truth it is in
      our daily life and our daily work that the chance lies for us to take hold of the
      pleasure offered to us by art. Again you are astonished at my saying this: but that
      is just because of that division between artists and other men, which to me seems
      monstrous, to you natural: you cannot imagine your daily life, still less your
      daily work, having anything to do with art: somebody else paints a picture which he
      hopes a rich man will buy, but scarcely dares to hope anybody but a few artists
      like himself will understand; you look at it, heed more or less what the newspapers
      say about it, are sometimes perhaps a little amused by it, oftenest not, and go
      away quite forgetting what kind of thing it was, and by no means yearning to see
      another like it. Art, as you understand it, you feel you can do pretty well
      without: well so could I, though I am an artist. And yet, art as it should be you
      cannot do without, as I think the coming years will show you; as many men even now
      are beginning to feel - those who do not understand the why and the wherefore of
      our lack of art with mere despair, those that do understand it with something like
      exultation at the signs of the times and the new birth which they promise.
    
      In you daily life and daily work, I have said, your chance alone lies of taking
      hold of art or the pleasure of life, in your becoming all artists: it is only by
      our all becoming artists that we shall be able to guard that natural birthright.
      Again you see because of that fatal schism between art and daily life you are
      astonished, for you think of an artist as a man working at his picture or image day
      in day out, disconnected with all other life but the carrying through of his piece
      of uselessness, as you would, if you said what you thought, most probably think it.
    
      That is not what I mean by an artist at all, when I say we must all be artists: I
      mean that we should all be able to look with reverence and understanding on the
      aspect of nature and the deeds of man on the earth, that we should take a deep and
      thoughtful interest in life in short, and not be merely drifted helplessly hither
      and thither by the force of circumstances, as we too often are.
    
      And if that were our real feeling, if we were really interested in the life of
      ourselves and our fellowmen, we should take care first, that our daily work was
      such as was fit for human beings to do, and secondly, that the time when we were
      not working included something more than mere animal rest, while it provided all
      due animal pleasure also: that, it seems to me, would be living due human lives, to
      have fruitful work-time, pleasant rest-time: I am sure you must agree with me
      there.
    
      And yet, I must say it, how far we are from that now: how little fruitful work we
      do, how little pleasant rest we enjoy! Many and many a man goes down to the grave
      without having done one stroke of useful work, and as a consequence (taking the
      world all round) without having enjoyed one day of pleasant innocent unanxious
      rest: we waste our working energies in producing nothing or things harmful; we
      spoil our capacity for enjoyment in terrible wearing anxieties about how we are to
      live by working hard in doing nothing or less; and as a sad token of our waste and
      unrest we are speedily making the world, the face of the earth, once so beautiful,
      hideous, grievous to look on, unfit for men to dwell on. Nay more, all this might
      be bearable, if we each and all had taken upon us due share of the torment this
      state of things produces; if we had borne one another's burdens and stood together
      determined to face it out, and mend it if we could, at the worst to live together
      bold and kind if we could not live happy. But this we have not done but rather the
      reverse of it; we have divided the world into rich and poor: we have decreed that,
      as near as could be, there should be two classes of people, one to toil and suffer,
      one to spend and enjoy; the poor are to be ill-fed, coarsely clad, dirty,
      wretchedly housed, over-worked, uncertain of their livelihood from day to day, they
      are to talk coarsely and ungrammatically, to think unconsecutively and illogically,
      to be uneducated, unrefined, bigoted, ignorant and dishonest; the rich on the other
      hand are privileged to feed themselves to repletion, to be clad in delicate
      raiment, to be spotlessly clean at all hours of the day, to live in gorgeous
      houses, to do no work or little work and to be paid the more the less they do, to
      talk daintily a tongue of their own, to be carefully and lengthily educated, to
      think according to the rules of logic, to have a plausible pretence of knowledge at
      least, to be over refined, as we call it, that is finikin, or may be vulgar only,
      to be finally, cynical and corrupt and dishonest: all this is their birthright, and
      hardly can they escape from it however good their dispositions as individuals may
      be, just as the poor however kindly and valiant they may be can hardly escape from
      their curse. If this be the case what wonder that there is this severance between
      artists and non-artists: the artists have been annexed by the rich and are their
      hangers on, their lackeys, their toy-makers: what wonder that they can no longer
      talk a language understanded by the people? that is as I have said a token of the
      severance between the rich and poor; it is proper and right according to the system
      under which we live that the poor should be obliged to do without art and that the
      rich should be able to have at least a semblance of it, because, as the
      word goes, money can buy anything, which fortunately, my friends, is a
      lying proverb. For if, as is probable, you are inclined to say that all my words
      about the rich and the poor is but the ordinary frothy talk of the demagogue, I
      will add this to it, that as far as I can judge - I who belong to the rich classes
      - they are not happy either. How should they be? how should a cynical man or class
      feel at rest? Satisfied they cannot be, so soon as any suspicion creeps in on them
      that they are cynical, corrupt and dishonest: nor will you working-men so soon as
      you feel that you are ill fed, ill clad, ill housed, overworked, dirty, ignorant,
      unrefined because you belong to a class, cease to feel any content in the
      wretched scraps of enjoyment of life which are still allowed you; you also will be
      openly discontented, very openly I hope. No, be sure we are both unhappy, rich and
      poor, and justly so when we, we children of one mother, have been so led astray, so
      sported with by circumstance that we can endure the inequalities which underlie
      those words rich and poor.
    
      And that is why I have been speaking the language of the common demagogue (which is
      true mind you) in order that I might make you feel, both rich and poor among you,
      what a gulf gapes between you, and that by some means you might set about trying to
      bridge it over, to fill it up rather. Indeed both consciously and unconsciously I
      know you try to do so, and your attempts at it, ineffectual as they are and must be
      while the present system lasts, do at least testify to your consciousness of there
      being something wrong: and what I want you to see is that the something wrong is
      the existence of class distinctions of any kind. I want there to be no more masters
      and slaves, no more gentlemen and cads (as we used to say at the university), I
      want us all to be friends, all to be gentlemen, working for the common good,
      sharing duly the common stock of pleasure and refinement. If this is being a
      demagogue you must set me down as one: also don't day it is beside the question of
      an art exhibition; for on this question must depend the answer to the other one
      whether your art exhibition has any educational value.
    
      What is the use of showing works of art to a class of men, and practically saying
      to them through a thin veil of verbiage: My contented friends, here are works of
      art done by famous and clever men, some dead, some living; you, as long as you are
      workmen, cannot rightly understand them, but this and this is what men privileged
      to know all about it say about them, and you had better take it for granted; come
      as often as you can or please to see them, but I don't quite know what good it will
      do except exciting a little vague wonder in you as you look at them, and therefore
      I hope a little pleasure. You are told they are beautiful, from which you
      may deduce the fact that you have nothing to do with beauty elsewhere either in
      your workshops or your homes, for you are workmen, and it is of the nature of
      workmen to be unrefined and careless of beauty: look and wonder and go your ways
      with my blessing.
    
      That, I say, must really be the manner of our invitation to the working classes to
      come to such places as this unless we have in some corner of our minds an intention
      of changing their condition entirely: but if we have that intention, we can give a
      different kind of invitation: My friends, my discontented friends, we may say, here
      you have before you the famous works of men the greater part of them dead by now
      and living under different conditions of life to ours: you and your fathers and
      your fathers' fathers have been so oppressed by the folly rather than the malice of
      man that you cannot fully enter into their spirit, but we who for no merits of our
      own have been privileged to refinement and knowledge can tell you this about them,
      that they are the work of men who were not fine gentlemen but workmen like
      yourselves: the instincts that enables them to produce these works are latent in
      your hands and minds if only you had the opportunity to develop them: the qualities
      of beauty and interest which have made these works the wonder of the world should
      be present in some way or another in your own daily work and have their influence
      upon your home life, making it orderly, beautiful, in a word human. Come here often
      if you please, especially on Sundays when your bodies are rested and your minds
      with them, and let the sight of these wonders stimulate you to trying to win a
      worthy life for yourselves and your fellows, a life of men and not of `operatives.'
    
      That it seems to me is the educational, the hopeful invitation we should give to
      workmen to come to such places as this: keeping well in mind that though public
      libraries and museums and picture exhibitions are good, and very good, yet if you
      are tempted to look upon them as substitutes for decent life in the workshop and
      the home, if they are to be mere tubs thrown to the whale of poverty, they may
      become dangerous snares to well-meaning, middle-class philanthropists: consider if
      this must be so: why, I well know the difference when I have been seeing some
      beautiful and famous place between coming home to a well-arranged and simple and
      beautiful house, and being bundled into some wretched sea-side lodging-house or
      noisy vulgar hotel: how one's heart sinks in the latter case. And if that is so,
      think how it must be to a workman who has in him any artistic feeling, who, after
      spending an hour looking at beautiful works of art in a public place, has to go
      back again to his close wretched house, a den, no better, buried amidst the miles
      and miles of vile filth into which the earth of S. Lancashire has been turned: I
      tell you if he feels anything short of rage at least if not despair, it is because
      he is used to it, that is his manhood has been crushed out of him: he forgets that
      he is a man and thinks he has been born to be a machine.
    
      It is now chiefly to those who agree with me in most of what I have been saying,
      but who can see no way out of it, that I shall address what I have left to say:
      this condition of things which, evil as you know it to be, you suppose to be as
      enduring henceforth as the world and incapable of being cured, capable only of
      being palliated, this condition of things, I say, has grown out of other conditions
      partly better, partly worse, and is the necessary outcome of them: I must not dwell
      upon this at length, because it will form the subject of a discourse that I have
      been kindly afforded an opportunity of delivering here to-morrow, so I must ask you
      to take that for granted at present, and to believe me when I say that the changes
      that have taken place in the relations of labour to the commonweal have been very
      great even in our own history: I must ask you to believe that, gradual as the
      change in England has been since the time of Edward III, it has been complete; that
      in spite of our counties and cities being called by the same names, and although
      not one new parish has been added to the English parishes since the time of the
      heptarchy, though there is a seeming continuity in our history, yet it fares with
      us as with the Irishman's knife, the same knife, but with new blades and a new
      handle: in morals and aspirations, in manners of work and life, the English people
      of to-day are totally different from what they were in the fifteenth century; 300
      years has made them another people with little sympathy for the virtues, and no
      understanding of the vices of their ancestors.
    
      Let those that will make their moan over that, but I as a living Englishman will
      choke down the sad sentiment which I admit it is natural to feel over the death of
      the past, so that I may play my part in the life of the present: that thought about
      the utter change that has befallen our country and race, I put before you not as a
      matter of regret for the past, but as hope for the future: I want to point out to
      you that at no time can you draw a line athwart the course of history and say thus
      far and no farther, and that the change, once more as before, will be complete as
      well as gradual. That tremendous edifice of Commercial rule which has supplanted
      Feudality is, you may be sure, not the last system which the world will see: it
      seems almost a truism to say so; and yet, you know, there are men and learned ones
      too, who seem to think that there are and can be but two systems of life and labour
      in the world: the one which now is, which is natural, essential and for henceforth
      enduring, though it may contain lamentable incidents; the other that which has
      been, which was absurd, artificial, bound to perish. One was wrong, the other is
      right: these learned men can see nothing but past and present; for them the world
      has no future. That I am sure must seem impossible to you when you come to think of
      it: your only question then will be in what direction and how will the change come.
      I must refer you to what I said before that under the present systems of labour,
      our work is wasted and our rest is spoilt; and why? Because it is a system of war,
      and therefore necessarily of waste: the parts of the system dovetail into one
      another, so that no one can escape from the conflict: nation competes against
      nation, class against class, individual against individual; each of these wars
      sustains the other and each has its own peculiar waste; only as it is with other
      war so it is with war commercial, that it is the common soldier that pays for all,
      in the long run. In commercial war it is on the loss of the manual workman that the
      whole system is built up, without him there would be nothing to support the war: he
      is in a peculiar position, for while he has only one commodity to sell, that
      commodity, labour, is the only one which has the power of increasing wealth, but
      that commodity he cannot turn to account without the leave of a special class; and
      the system of war aforesaid compels him by driving him to daily competition with
      his fellows to getting that leave on very poor terms, namely, that he shall work
      day long, and that after he has made just so much as shall support him, that is,
      enable him to go on working and to reproduce his kind, the results of the rest of
      his day's labour shall be his master's, the man who has bought him for the
      occasion; which master for his part is in the position of being the privileged
      monopolist of the hoarded labour of the past in the shape of land, machinery,
      credit and the like, without the use of which the workman cannot even begin to
      work.
    
      This is the system of working for profit, which I must ask you to believe me has
      not always obtained; there have been times and places when people worked not for
      other people's profit, but for their own livelihood, under which circumstances they
      regulated their own labour, produced for the market, which was not an extensive
      one, such goods as were obviously demanded and no more, and at the same time dealt
      in a simple way with the consumer, so that there was not the same waste between
      buying and selling which there is now: this was the case in times rude, coarse and
      ignorant if you will; but rude and coarse and ignorant as they were, they were
      fruitful of one thing of which we are now barren, art to wit; and the reason was
      that in spite in good truth of plenteous violence and oppression of the weak by the
      strong, the workmen did themselves regulate labour, did by means of combination
      among themselves settle most of the points which might otherwise have been
      anarchically fought over, and for the rest were when they were at work absolutely
      masters each man of his own work: subject only to the demands of the public they
      were their own employers; or, if you please to put it in another way, the goods
      they made were made to use, whereas they are now, as you well know, made to sell.
    
      I think it necessary to make these remarks because I assume again that the purpose
      of this exhibition is to a certain extent at least educational, and that from my
      point of view I should be defeating that object by misleading you, if in calling
      attention to the works of past times, I were to allow you to suppose that works of
      the same quality and kind could be produced under the system which is now the
      ruling power of civilization: such works as these with all their spontaneous
      unforced imagination could never have been produced in the copiousness with which
      they were produced, if apart from the great masters who made them there had not
      been a vast public who could appreciate them; nor could that public have existed
      but for the constant unconscious education which was going on in those days by
      means of the ordinary work of ordinary handicraftsmen: now-a-days, you know, people
      talk about, and advertise art pottery, art furniture, art fire-grates, and the
      like, giving us clearly to understand by such words, that it is unusual for
      pottery, furniture and fire-grates to have anything to do with art, that there is,
      as I began by saying, a divorce between art and common life. Every object which was
      made in the days when the pictures from which these engravings were made were
      painted, was a matter of art and could not help being so: in ordinary matters of
      daily life the art in a piece of handicraft was not charged for, you understand;
      why should it be when it was not an additional trouble to the handicraftsman but a
      solace to his trouble? he could live without being paid extra for it: whereas
      now-a-days you must pay a man something for the art, because he can, as a rule, do
      nothing else but the art - he is an extra - like the manners at the village school.
    
      So here you see we are back at the point we began at, that the public does its art,
      as it does its religion - by deputy: pays a certain numbers of what I should call
      art-parsons to perform certain mysteries of civilization which it is pleased to
      call the fine arts, and stands on one side while they go through their mumbo-jumbo
      with grievous results to their morals in most cases, and with still more grievous
      results to the fine arts; which depend on all life being carried on with art; that
      is the very sustenance and well-spring of them, the beauty and manliness of daily
      life. Now I tell you that this kind of thing cannot last: we cannot go on for ever
      drawing all the food of our pictorial representation, nay even of the ornament of
      our houses and furniture, from past times: the fact that we do so now is a sign of
      corruption in the arts; and what must follow corruption save extinction or new
      birth?
    
      With such reverence I look upon the arts, so clear I am that they have been, since
      man was man, a part of his very soul, that I know that their extinction is
      impossible. Where then are we to look for their new birth? To speak plainly I see
      no sign of it in the life of cultivated civilization to-day, and if I saw any
      apparent signs in that quarter of what is called Society, I should doubt their
      reality: it would be a beginning at the wrong end: we must start from the root up
      to be real. It is from the workman himself that the new birth must come: but how?
      To speak plainly again, he is to-day a slave, a slave to the great world-market,
      which holds his master in as tight a grip as it does himself, and which mocks at
      any feeble effort of either master or man to produce art. From that slavery of the
      world-market, the mother of lies and theft, of pestilence, war and famine, the
      worker must free himself if he is ever to take any part again in the enjoyment and
      production of beauty; and if he does not then beauty will die out of the life of
      man. The workman must learn to understand that he must have no master no employer
      save himself - himself collectively, that is to say, the commonweal: then once more
      he will be master of his work and refuse to produce at the command of the
      gambling-market: he will have no more need to send his son or his brother equipped
      with all the newest inventions of science to murder half-armed men in distant
      countries that he may sell another thousand yards of calico: he will think the
      slaughter of three or four thousand Arabs or negroes a wasteful if not a cruel way
      of testing the capabilities of the market, and will soberly try to find out what is
      wanted without resorting to vicarious murder. He will see to it that buying and
      selling shall mean nothing more intricate and exciting than the exchange of the
      results of labour without waste; he will not suffer the caprice of a few rich
      people driving them to crave for useless and harmful luxuries to impose a tax on
      all usefully industrious persons; and he will find amidst his peaceful and useful
      work that every stroke he does will benefit both himself and all his neighbours.
    
      This is the root from which all art must spring in the future, whatever has been
      the case in the past: as it has always when genuine sprung from the lives and
      labour of the many, it has always been in some degree, and always in exact
      proportion to its nobility, the token of the freedom of labour: that word, the
      freedom of labour, is now beginning to mean a something wider and more glorious
      than it has ever had yet; to mean freedom no longer for a group or a class or a
      nation but for all men.
    
      True it is that I have for long got to think that in the early days of that freedom
      art will have a rough time of it; and for long perhaps will have to live a spartan
      life, for-going many delicacies which I the weak child of a poorer and less manly
      time than that which is to come cannot help craving after. But what matter so long
      as art is alive and healthy? from that spare and spartan life she will rise to
      greater glories than ever she has attained to yet.
    
      It is in the assured hope of this that I have ventured to hint at what seem to me
      important truths, most important, even at the risk of offending you by seeming
      irrelevance. Though the day of that Freedom of Labour from the gambling-market may
      be distant, be sure it is on the way; nay signs of its approach are even now
      multiplying around us, and who knows but that even as I speak to you the dawning
      may be at hand?
    
      1884.
    
The reference to this piece of work in the Chronology

The William Morris Internet Archive : Works
