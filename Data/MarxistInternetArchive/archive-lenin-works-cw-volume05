Lenin Collected Works: Volume
      5  Volume 5
	      Transcription/Mark-up:
   

	    Tim Delaney & Robert Cymbala,  2000.

	          • R. Cymbala, 2003.

	          • 
	    (Converted to "tx2html", Oct. 2004.)
	     IIIIIIIVVVIIIIIIIIVVVIVIIVIIIIX12IIIIIIIVDogmatism and “Freedom of Criticism”The Spontaneity of the Masses and the Consciousness of the Social-DemocratsTrade-Unionist Politics and Social-Democratic PoliticsThe Primitiveness of the Economists and the Organisation of the RevolutionariesThe “Plan”
		      for an All-Russian
		      Political NewspaperConclusion  
Lenin Collected Works: 

	    Volume 5
1901--1902


Preface




1901



Where to Begin?
15k


Another Massacre

19k


The Persecutors of the Zemstvo and the Hannibals of Liberalism
    

		¶ I
   

		¶ II
   

		¶ III
   

		¶ IV
   

		¶ V
   

		¶ VI

164k


A Valuable Admission

28k


The Lessons of the Crisis

16k


The Serf-Owners at Work

17k


A Zemstvo Congress

8k


The Agrarian Question and the “Critics of Marx”
    
		¶ I.
		The “Law” of Diminishing Returns

		¶ II.
		The Theory of Rent

		¶ III.
		Machinery in Agriculture

		¶ IV.
		The Abolition of
		the Antithesis Between Town and Country. Particular Questions
		Raised by the “Critics”

		¶ V.
		"The Prosperity of
		Advanced, Modern Small Farms” The Baden Example

		

		¶ VI.
		The Productivity
		of a Small and a Big Farm. An Example from East Prussia

		¶ VII.
		The Inquiry into
		Peasant Farming in Baden

		¶ VIII.
		General
		Statistics of German Agriculture for 1882 and 1895. The
		Question of the Medium Farms

		¶ IX.
		Dairy Farming and
		Agricultural Co-operative Societies in Germany. The
		Agricultural Population in Germany Divided According to Its
		Position in the Economy

	      
394k


The “Unity” Conference of R.S.D.L.P. Organisations Abroad
    
September 21-22 (October 4-5), 1901

		    
		¶ 1.
		Speech Delivered On
		September 21
		(October 4). (Note From The Minutes)

		¶ 2.
		Questions Submitted
		to the Union of Russian Social-Democrats Abroad at the "Unity"
		Conference, September 21 (October 4), 1901
	      
27k


Fighting the Famine-Stricken

23k


A Reply to the St. Petersburg Committee

10k


Party Affairs Abroad

8k


Penal Servitude Regulations and Penal Servitude Sentences

20k


Review of Home Affairs
    
		¶ I.
		Famine.

		¶ II.
		Attitude Towards
		the Crisis and the Famine.

		¶ III.
		The Third Element.

		¶ IV.
		Two Speeches by
		Marshals of the Nobility.

	      
160k


Preface to the Pamphlet Documents of the “Unity” Conference

12k


The Protest of the Finnish People

16k


The Journal Svoboda

6k


A Talk With Defenders of Economism

26k


On the Twenty-Fifth Anniversary of the Revolutionary Activity of G. V. Plekhanov

4k


Demonstrations Have Begun

13k



On a Letter from “Southern Workers”

4k


Anarchism and Socialism (1901)
7k


1902


Concerning the State Budget

18k


Political Agitation and “The Class Point of View”

22k


Reply to “A Reader”

7k



What Is To Be Done?
    
Burning Questions of Our Movement.

     ¶ 
		  Preface
     ¶ I
		  Dogmatism and “Freedom of Criticism”

		  (A). What
		  Does “Freedom of Criticism” Mean?

		  (B). The New
		  Advocates of “Freedom of Criticism”

		  (C). Criticism
		  in Russia

		  (D). Engels
		  on the Importance of the Theoretical
		  Struggle

		       ¶ II
		  The Spontaneity of the Masses and the Consciousness of the Social-Democrats

		  (A). The
		  Beginning of the Spontaneous Upsurge

		  (B). Bowing
		  to Spontaneity. Rabochaya Mysl

		  (C). The
		  Self-Emancipation Group and Rabocheye Dyelo
     ¶ III
		  Trade-Unionist Politics and Social-Democratic Politics

		  (A). Political
		  Agitation and Its Restriction by the Economists

		  (B). How
		  Martynov Rendered Plekhanov More Profound

		  (C). Political
		  Exposures and “Training in Revolutionary
		  Activity”

		  (D). What
		  Is
		  There in Common Between Economism
		  and Terrorism?

		  (E). The
		  Working Class as Vanguard Fighter for Democracy

		  (F). Once
		  More “Slanderers”, Once More
		  “Mystifiers”

		       ¶ IV
		  The Primitiveness of the Economists and the Organisation of the Revolutionaries


		  (A). What Is
		  Primitiveness?

		  (B). Primitiveness
		  and Economism

		  (C). Organisation
		  of Workers and Organisation of Revolutionaries

		  (D). The
		  Scope of Organisational Work

		  (E). “Conspiratorial”
		  Organisation and
		  “Democratism”

		  (F). Local
		  and All-Russian Work

		       ¶ V
		  The “Plan”
		      for an All-Russian
		      Political Newspaper

		  (A). Who Was
		  Offended by the Article “Where To
		  Begin”

		(B). Can a
		Newspaper Be a Collective Organiser?

		(C). What
		Type of Organisation Do We Require?

		     ¶ 
		Conclusion
     ¶ Appendix.
		The Attempt to Unite
		Iskra with Rabocheye
		  Dyelo
     ¶ Correction to
		  What Is To Be Done?

512k


 

Volume 5
	      Transcription/Mark-up:
   

	    Tim Delaney & Robert Cymbala,  2000.

	          • R. Cymbala, 2003.

	          • 
	    (Converted to "tx2html", Oct. 2004.)
	  







 
Previous

 | 
 Next 

 
Lenin
		      Collected Works 
 | 
 Lenin
		      Internet Archive 

 



Preface

Where to Begin?15kAnother Massacre
19kThe Persecutors of the Zemstvo and the Hannibals of Liberalism
    

		¶ I
   

		¶ II
   

		¶ III
   

		¶ IV
   

		¶ V
   

		¶ VI
164kA Valuable Admission
28kThe Lessons of the Crisis
16kThe Serf-Owners at Work
17kA Zemstvo Congress
8kThe Agrarian Question and the “Critics of Marx”
    
		¶ I.
		The “Law” of Diminishing Returns

		¶ II.
		The Theory of Rent

		¶ III.
		Machinery in Agriculture

		¶ IV.
		The Abolition of
		the Antithesis Between Town and Country. Particular Questions
		Raised by the “Critics”

		¶ V.
		"The Prosperity of
		Advanced, Modern Small Farms” The Baden Example

		

		¶ VI.
		The Productivity
		of a Small and a Big Farm. An Example from East Prussia

		¶ VII.
		The Inquiry into
		Peasant Farming in Baden

		¶ VIII.
		General
		Statistics of German Agriculture for 1882 and 1895. The
		Question of the Medium Farms

		¶ IX.
		Dairy Farming and
		Agricultural Co-operative Societies in Germany. The
		Agricultural Population in Germany Divided According to Its
		Position in the Economy

	      394kThe “Unity” Conference of R.S.D.L.P. Organisations Abroad
    
September 21-22 (October 4-5), 1901

		    
		¶ 1.
		Speech Delivered On
		September 21
		(October 4). (Note From The Minutes)

		¶ 2.
		Questions Submitted
		to the Union of Russian Social-Democrats Abroad at the "Unity"
		Conference, September 21 (October 4), 1901
	      27kFighting the Famine-Stricken
23kA Reply to the St. Petersburg Committee
10kParty Affairs Abroad
8kPenal Servitude Regulations and Penal Servitude Sentences
20kReview of Home Affairs
    
		¶ I.
		Famine.

		¶ II.
		Attitude Towards
		the Crisis and the Famine.

		¶ III.
		The Third Element.

		¶ IV.
		Two Speeches by
		Marshals of the Nobility.

	      160kPreface to the Pamphlet Documents of the “Unity” Conference
12kThe Protest of the Finnish People
16kThe Journal Svoboda
6kA Talk With Defenders of Economism
26kOn the Twenty-Fifth Anniversary of the Revolutionary Activity of G. V. Plekhanov
4kDemonstrations Have Begun
13k
On a Letter from “Southern Workers”
4kAnarchism and Socialism (1901)7kConcerning the State Budget
18kPolitical Agitation and “The Class Point of View”
22kReply to “A Reader”
7k
What Is To Be Done?
    
Burning Questions of Our Movement.

     ¶ 
		  Preface
     ¶ I
		  Dogmatism and “Freedom of Criticism”

		  (A). What
		  Does “Freedom of Criticism” Mean?

		  (B). The New
		  Advocates of “Freedom of Criticism”

		  (C). Criticism
		  in Russia

		  (D). Engels
		  on the Importance of the Theoretical
		  Struggle

		       ¶ II
		  The Spontaneity of the Masses and the Consciousness of the Social-Democrats

		  (A). The
		  Beginning of the Spontaneous Upsurge

		  (B). Bowing
		  to Spontaneity. Rabochaya Mysl

		  (C). The
		  Self-Emancipation Group and Rabocheye Dyelo
     ¶ III
		  Trade-Unionist Politics and Social-Democratic Politics

		  (A). Political
		  Agitation and Its Restriction by the Economists

		  (B). How
		  Martynov Rendered Plekhanov More Profound

		  (C). Political
		  Exposures and “Training in Revolutionary
		  Activity”

		  (D). What
		  Is
		  There in Common Between Economism
		  and Terrorism?

		  (E). The
		  Working Class as Vanguard Fighter for Democracy

		  (F). Once
		  More “Slanderers”, Once More
		  “Mystifiers”

		       ¶ IV
		  The Primitiveness of the Economists and the Organisation of the Revolutionaries


		  (A). What Is
		  Primitiveness?

		  (B). Primitiveness
		  and Economism

		  (C). Organisation
		  of Workers and Organisation of Revolutionaries

		  (D). The
		  Scope of Organisational Work

		  (E). “Conspiratorial”
		  Organisation and
		  “Democratism”

		  (F). Local
		  and All-Russian Work

		       ¶ V
		  The “Plan”
		      for an All-Russian
		      Political Newspaper

		  (A). Who Was
		  Offended by the Article “Where To
		  Begin”

		(B). Can a
		Newspaper Be a Collective Organiser?

		(C). What
		Type of Organisation Do We Require?

		     ¶ 
		Conclusion
     ¶ Appendix.
		The Attempt to Unite
		Iskra with Rabocheye
		  Dyelo
     ¶ Correction to
		  What Is To Be Done?
512k
Previous
 |  Next 
Lenin
		      Collected Works  |  Lenin
		      Internet Archive   