Anatol Lunacharsky
Translator: Irving D. W. Talmudge;
Source: Pushkin: Homage by Marxist Critics ed. Irving D. W. Talmudge, Critics Group, New York, 1937;
Transcribed: Sally Ryan for marxists.org, January 2000.
Pushkin was not what one might call a theoretician of the
arts. He had no systematized body of principles underlying his
evaluations. In the long evolution of his aesthetic concepts he
never attempted to express in writing any theoretical ideas
relating to the different stages of this evolution.
Pushkin passionately loved art and especially literature. The significance of literature in social relations is a question which he never pondered. Nor did he ever consider for whom the artist should write. He envisioned before him a hazy collective face--the "reader" and in that alluring, sympathetic countenance Pushkin discerned his friends, members of his own social group; beyond that--dimly-perceived contemporaries, and a posterity who would accept with delight the gifts of his muse.
Actually, the face of Pushkin's reader changed during the span
of his life. At first his reading public consisted mainly of the
genteel salon stratum. As time went on it became more
and more representative of the Russian educated classes; it was
gradually augmented by democratic elements until at the close of
his life Pushkin was read by tens of thousands of persons, the
majority of whom were new readers--in other words, the bourgeois
and petty-bourgeois intelligentsia, including even a thin
section of the peasantry.
To put it more generally, it may be said that at first Pushkin actually wrote for the aristocratic reader and saw before him a genteel audience, while toward the close of his life his reading public consisted chiefly of the "bourgeoisie" (to use the term in its wider connotation);and he realized that he was writing for this social group specifically.
Correspondingly, Pushkin's concepts of the poet in general, and of
himself as a poet in particular, underwent a process of
evolution. At first the word poet implied an assured person, a man
of the world, who, without compulsion, motivated by the loftiest
calling, surrenders himself from time to time to the inspired
mission of poesy, finding in it a luxurious complement to his
human, or, to be more exact, his lordly existence. Even though
subjectively the poet does not feel that riding Pegasus is the
most important and the most satisfying experience in life, it is
at best a fine form of dilettantism.
More and more frequently one finds, in the later works of Pushkin, lines breathing recognition of the fact that what is perhaps most important in him is not the lord, nor the squire, nor the gentleman of the Emperor's bed-chamber, but, specifically, the writer. Pushkin does not attempt to conceal the fact that the principal role in this deduction is played by the economic factor. Writing becomes the occupation which feeds him and his family. He sells the product of his labor. He is a professional--a special type of craftsman.
Who, then, pays for his labor? His labor is paid for by a vague
but numerous public which extends beyond the limits of the
genteel salon.
This in itself should have compelled Pushkin to alter his choice of themes, his treatment of these themes, his evaluation of his own work and the work of other artists--both past and present.
It is not, of course, the case that Pushkin, realizing his
position as a professional writer, altered his tastes and style
of writing. No, the process of this realization went side by
side with his literary evolution, an evolution extremely
interesting and unusually integral and natural.
Standing veritably "at the threshold of two cultures," he responded with a gay echo to French classicism, when the latter, to be sure, was already in its twilight. He succeeded in becoming the most splendid representative of that strange, belated rococo of our genteel Russian literature. At the very time he was imbibing the new wine of romanticism, while still indulging in pranks of mischief ("Ruslan and Lyudmila")--but exhibiting in this new poesy greater freedom and greater proximity to nature--Pushkin simultaneously imbibed a
love of liberty, superficial but sparkling, like bubbles of champagne. To Pushkin this love of liberty meant a protest by the aroused citizen (primarily, the aristocrat) against the oppression of the autocracy and the economic and cultural tenor of the life bound up with that autocracy. Pushkin's poetry assumes a social character; it serves as the expression of this protest; he sings for the first time of the hero-apostate. Romanticism is painted in Byronic colors and this freedom-loving lyricism approaches at times genuine revolutionary notes. 
One must guard against reaching superficial conclusions. It is
quite easy to assume that the crushing of the progressive
aristocracy's protest and the routing of the Decembrist uprising
frightened Pushkin; that having lost all hope and support, and
carried away by the general reaction of his class, he changed
the strings of his lyre and became a sincere political defender
of the genteel aristocracy, save for minor differences.
Supporters of this contention believe that by virtue of this fortunate change Pushkin deepened the flow of his poetry, endowed it with a subtle individual character; that he delved into himself; became absorbed in eternal problems surrounding his personality; that he avidly grew closer to the sources of beauty and devoted himself to its reproduction precisely because, as is the wont of disappointed poetic natures, he found solace only in creative work, removed from civic activity.
All this is true, but only partially true.
In his recent book, Pushkin on the Brink of Disaster, N. K. Piksanov avers that the very year 1833 marked the beginning of Pushkin's unmistakable turn to the right, his attempts to reconcile himself with mundane society and to become a part of it, his striving to assert himself as a conservative and an aristocrat. It it noteworthy, however, that Piksanov himself is obliged to admit that sufficient data exists to prove that the poet was not entirely occupied with these feelings and deliberations.
Concurrent with his apparent tendency to make peace with the
powers that be, and his willingness to heed the voice of the
aristocrat within him, the poet is undergoing a far more
important process of development. With unusual vision, such as
one could expect only of an economist or sociologist, Pushkin
perceives the transformation of Old Moscow, its new mercantile
character, the universal growth of
the influence of the bourgeoisie. What is essential here is not that he notes the growth of a new Russia and endeavors to establish his relationship to it (as well as to its writing fraternity), but that the internal, purely poetic evolution of Pushkin is driving him in the same direction and that the movement of the poet toward new forms of creative work is organically bound up with his desertion of the aristocratic camp for that of the bourgeoisie.
Precisely therein lies the gradual strengthening of Pushkin's
realism. In his poetry of that period one can already discern
this realism in mounting degrees; but he now begins to prefer
artistic prose to poetry. To be sure, it is not an artistic
prose differing from verse only in its lack of rhyme. In this
connection Pushkin points out that:
"What is one to say about our writers who deem it degrading to describe things simply, who attempt to enliven their puerile prose with flabby metaphors?... Accuracy and conciseness, these are the foremost virtues of prose. Thought and thought alone is the prime requisite of prose--without it the most brilliant expressions are worthless."
Artistic prose--the short story, the novel--demands, according
to Pushkin, first of all thought. With a somewhat sad and
sardonic smile he asserts that poetry may at times be slightly
nonsensical, but prose--never! The basic merit of prose is
intelligibility.
It is doubtful whether he would ever have abandoned literature as the chief means of communion with his readers; but it is extremely important to observe that his intense and growing interest in "thought" greatly increased his regard for journalistic prose, magazine prose. The dream of owning a magazine, expounding in thiS magazine his attitude toward literature, and developing real literary criticism, seriously occupied Pushkin; and there is no question that if he had not been killed by hostile elements he would have realized this dream and entered a new phase of development. We can merely speculate about this, but all indications point to the fact that this phase would have marked a further advance from the aristocratic to the bourgeois camp.
This non-artistic prose--a prose which is the medium, so to
speak, of pure thought did not appear to Pushkin to be purely
journalistic. In his day and shortly afterward, journalism
resolved itself chiefly into literary criticism. One would
assume, therefore, that concurrently
with Pushkin's altered attitude toward literature he would also change his views on literary criticism.
We know that Pushkin attached great importance to aesthetic
criticism. In all probability he would never have renounced
throughout his creative career the formula by which he strove to
define the task of the critic:
"Criticism," he wrote, "is the science of discovering beauty and shortcomings in works of art. It is based, first of all, upon a thorough knowledge of the rules by which the artist or writer governed himself in his work; and secondly upon a profound study of examples, as well as active observation of contemporary phenomena."
This formula does not exceed the bounds of aesthetic criticism;
but we know full well how our criticism evolved generally, what
changes it went through in the hands of Belinsky who continued
this process, in the birth of which Pushkin played so prominent
a role.
It is undoubtedly true that this conception of aesthetic
criteria is a very broad one. What does it mean to discover
beauty and shortcomings? Is beauty the ability to organize an
encompassed object by definite determinants? Does this
constitute the force of realism in works of art, i.e. that
peculiar truthfulness which strikes louder notes and penetrates
more deeply into the consciousness than the "raw" truthfulness
of reality? How about the forces of romanticism, i.e. the pathos
of emotion which rings in a given work and stirs the reader--is
it possible to isolate it from aesthetics, is it not the very
essence of beauty? If one is to dismiss these considerations, is
there, indeed, much left of purely aesthetic criticism?
Pushkin, who insisted that prose must be above all intelligent, that it must be above all the vehicle of thought--could he have approved the idea that artistic criticism, which to him was the most important form of non-artistic prose, exclude from consideration such vital aspects; aspects which determine the very value of a work of art?
And as for shortcomings. Is superficiality of theme, or false
organization of objective material, or lack of any semblance of
expediency, or lack, for that matter, of intense or lofty
compassion in the emotions ardused by a given work--are these
not to be considered shortcomings ?
We do know, after all, how that indefatigable art critic Belinsky
could not and would not (despite rigid government censorship) separate his aesthetic criteria from social criteria.
The process which was destined to lead Chernishevsky to
aestheticism was already in the offing, and had already engaged
Pushkin.
At this point one might as well admit that aesthetic criticism can remain aesthetic and at the same time gradually merge with social criticism, and that genuine criticism inevitably includes both of these elements.
Moreover, it is a fallacy to refer to them as two separate
elements. Aesthetic criticism and social criticism, in truth,
represent one and the same thing, or at best two phases of one
and the same thing.
Plekhanov, whose views on literary criticism were in several respects (please note that I say only in several respects) retrogressive in comparison with Chernishevsky, asserted, in speaking of the two tasks of criticism, that it is necessary first of all to study genetically the social roots of a work of art and only subsequent to that express an aesthetic evaluation of it. Clearly Plekhanov was wrong. Genetic explanation, i.e. determining the causes underlying the appearance of a work of art (an explanation according to Plekhanov's pet principle: "Do not weep, do not laugh, but merely understand") is not criticism. It is to put it simply, historico-literary research. It is a sociological determination of the reasons for the appearance of a given work. Criticism presupposes an expression of judgment about a work. According to Plekhanov, it would appear that a "real" scientific critic a Marxist critic must have no opinions or judgments about a work. It is altogether apparent that this constitutes monstrous narrowness; this fallacy crept into Plekhanov's system because Plekhanov, carried away by his polemics, presented a crude "objectivism" to counter the ridiculous theories advanced by sociologists of the subjective school.
No, a critic must express judgment. Research into the social
roots of a given work of art is most important to him; it is
difficult for him to appraise a work without this knowledge. (Of
course this method was unknown to Pushkin; even Belinsky, who
dealt with this problem brilliantly, did so very seldom). After
that, in social criticism, comes the question of the function of
a given work--what role it should have played according to the
author, what role it actually did play during the author's life
and in the epochs which followed.
Now we come to the question of criticism, which, for our creative
epoch--the epoch of critical assimilation of an entire cultural heritage and the revaluation of contemporary art from the standpoint of our great cause--is of prime importance: what does a given work of art mean to us, to what extent will it be helpful or harmful to us? To answer these questions is the chief task of criticism.
We have just learned from Pushkin his conception of aesthetic
criticism. To repeat, aesthetic criticism to him meant the science of discovering beauty and shortcomings in works of art. But what endows a work of art with beauty, what removes its shortcomings?
When we speak of the beauty of a work of art, we always mean the
strength of its influence, the captivating nature of the work,
its ability to hold our interest, to make us happy, to
illuminate our consciousness.
All "beauty" has specifically this intent. In uttering the word "beauty" one strives to indicate the objective quality in nature or in a work of art which he believes to be the reason for his elated mood, his emotional uplift.
From a narrow point of view, beauty reduces itself to those
elements in nature or in art which are pleasant to the senses,
or into correct, i.e. easily-grasped, combination
(design, melody, harmony, rhythm, etc.), or into pleasing
presentations of physical perfection, vital strength, health,
intellectual brilliance, moral fascination, etc.
On the other hand, we know well enough that art does not reduce itself merely to this type of beauty. Art can include elements which from this narrow viewpoint are unbeautiful or downright ugly. Art, as Aristotle pointed out in his day, deals also with ashes, with pain, with unusually accurate reproductions of repelling phases of life (Flaubert). And all this the artist can nevertheless surmount. Whether they depict the stripped carcass of a bull or a brutal scuffle of warriors, Rembrandt or Leonardo da Vinci attain heights of beauty and compel us to exclaim--"excellent !" A powerful sensation, which overcomes the reader, compels him to see the world in a new light, makes him think differently of the world and thus moulds his world-outlook, this essentially is beauty. The more subtle it is, the more novel it is, the further it departs from elementary prettiness--the more we delight in it, for its influence operates in the most difficult realms.
Now for shortcomings. A shortcoming is that which repels us, that
which reveals the artist's weakness, betrays his inability to cope with his task, shows that due to his impotence, or in order to be clever, he falsifies reality, or tells us things which are extraneous to us and therefore bore us.
Pushkin justly points out that genuine criticism requires a
knowledge of the rules by which the artist governed himself, as
well as a knowledge of artistic methods. This means that the
critic does not take merely the finished result--the work of
art--but considers also the methods employed to attain the given
result.
What are the methods of artistic creative work? Every method, no matter of what epoch or of what artist, in the final analysis amounts to the selection of a living object, the mastering of this object, taking from it all that is significant and eliminating from it all that is extraneous, and presenting this refined object most powerfully, that is, expressing it with the maximum possible strength.
Readers' demands vary from epoch to epoch, from class to class;
the course adopted by writers likewise alters; but art can never
depart from the three basic elements of creative work--the
selection of material, its treatment and its external expression
(and, of course, these three elements are merged into one
process and can be clearly differentiated only in the
abstract).
In other words, artistic methods are those by which the most powerful impression is attained. Taking into consideration the class structure of society (which in our country is coming to an end), it must be admitted that the methods of artistic expression are the means by which a writer hopes to make his creative work exert the greatest possible influence upon his class, and upon those classes which his own class wishes to dominate.
Thus, aesthetic criticism and social criticism--in the higher
stages of the development of criticism generally--coincide and
complement each other.
In the case of Pushkin, social evaluations remained in the background; he would have become more cognizant of them had his evolution continued. His journalistic evaluations would have acquired more social character.
But perhaps Pushkin was a pure aesthete, since he disregarded
what Plekhanov regarded as the chief obligation of a critic,
namely the determination of the social genesis of the work of
art? Perhaps Pushkin, too, strove to appraise art merely by the
heights it reached
on the ladder of purely aesthetic values? The superficiality and falsity of this opinion about Pushkin is unmistakably obvious. Anyone who attentively reads his works cannot help noticing the poet's continual injection of social elements into his writing.
But let us assume that purely aesthetic considerations did
predominate in Pushkin. In that event we have in him a most
unique master of aesthetic criticism. Aesthetic criticism
requires a most refined taste, that is, a vast experience in
thoughtful evaluation of works of art, a somewhat instinctive
but sure approach to art, a minimum of errors both in
understanding the "rules by which the artist governed himself,"
and in comprehending the immense wealth of impressions inherent
in a given work of art. A man who is himself a great artist and
at the same time a profound thinker. One capable of analyzing
critically his own work and his perceptions. One thoroughly
conversant with world literature. A man who refuses to remain
confined within the rigid walls of one class. Such a man, who
stood "at the threshold of two worlds" and effected a most
striking evolution in his creative work, cannot help being the
center of great interest.
Let us recall what Belinsky said of Pushkin the critic. "In Pushkin," he asserted, "one does not see a critic who bases his opinions on established principles, but a genius whose correct and profound feeling, or sure instinct, rather, uncovers truth everywhere, no matter what he surveys."
What does this mean? It means that even though Pushkin did not
formulate any theoretical theses, and despite the fact that we
fail to discern in him any complex logico-scientific process
which guided him in establishing values--nonetheless the results
of his thinking, which he obviously attains with great facility
and almost by instinct, are remarkably accurate.
Pushkin's genius, like all genius, is distinguished not only by its inherent strength but also by the great volume of work created. Pushkin was not a theoretician; nevertheless when he did engage in criticism his judgments were sound so far as they pertained to the aesthetic evaluation of works of art.
Let us now consider our contemporary criticism. It is weakest
specifically in aesthetic evaluation. It performs splendidly
Plekhanov's "first task." It can accurately trace the roots of a
given work of art; it can also determine fairly well the class
aims pursued consciously or semi-consciously by a given author
and the class results he attained. To this it can also add
deductions explaining the relation of the given work to our
cause and our struggle. But when it comes aesthetic evaluation
there is dismal confusion.
Admitting that we have no "geniuses" among our critics but merely, as is usually the case, simple, capable people who base their opinions on "established principles"--still, do we have these established principles, have we sufficiently developed our own aesthetics, can we borrow aesthetics from the classes which preceded us?
The aesthetics of the past we can accept only critically, that
is, we cannot eschew that aestheticism, we cannot merely ignore
it, but neither can we accept it without reservations; it must
serve only as nourishment for the growth of our own aesthetics,
which to date exists only in the form of a few fundamental
premises. And these fundamental premises do not as yet
constitute the "principles," that is, the harmonious and
multi-phased system to which Pushkin referred.
Every critic should make efforts to formulate these
"principles," as, let us say, Belinsky formulated his. To be
sure, each critic will accomplish this in his own
way. Undoubtedly we are still novices when it comes to such
questions as construction in a work of art of a given genre, the
composition of its integral elements, unity of style and the
different merits of style (diversity, brilliance, etc.), phrase
construction, vocabulary building, the role of language--direct,
metaphoric, ironic, etc. Indeed, in all this we are very weak,
perhaps even weaker than the critic-epigones of the epoch
preceding ours. We have not paid sufficient attention to these
problems, having been involved in matters truly more
importarlt. It is more essential for us to determine whether a
given author is a friend or an enemy, to what extent he is an
enemy, to what extent he is a friend; what elements in his work
are hostile, what elements are friendly. These considerations
are of greater concern to us than the determination of the
extent to which an author is armed with purely artistic weapons,
whether the power of his blow is commensurate with his intent,
whether he has discovered for himself an adequate form and
whether this adequate form is precisely the one that will assure
for him not merely self-gratification but also genuine
success--the capturing of an audience. One must bear in mind,
however, that this success may be hollow, that it may be the
result of a temporary enthusiasm, while as far as we are
concerned, soundness and durability of a work of art is far more
valuable. These are considerations which more and more occupy
our writers.
Although Pushkin cannot teach us theoretical "laws," he can nevertheless serve as a brilliant mentor in that field of keen evaluation mentioned by Belinsky.
Each of Pushkin's critical judgments poses for us a series of most interesting problems.
We ask ourselves, to what extent do his judgments reveal Pushkin's class nature, the virtues and defects of a cultured squire, of a declassed person who stood "at the threshold of two worlds," wherein do his judgments reveal the strength of a specialist, expert in the subject he treats, and, therefore, to what degree are his judgments objective~ If they are objective, we must assume the task of analyzing Pushkin's views, we must from his general judgments discern their hidden theoretical elements.
To what extent are Pushkin's opinions of Shakespeare or
Molière correct (or incorrect)? His opinions of Derzhavin,
Zhukovsky? His views on ancient and contemporary letters?
This is how we can learn from Pushkin and there is much to be learned from him!
The literary critic who mortifies belles-lettres, dissects it as
if it were a cadaver in an anatomical theatre, and delivers over
it a dry sermon, may prove to be a valuable member of a
collegium of artistic scientists, but he is not a literary
critic.
Why did Pushkin interest himself in the science of discovering beauty and shortcomings? In order to serve as a guide to his coevals. He uncovers beauty where a less discriminating person sees none, he discerns shortcomings where a less experienced eye fails to note them or even regards them as merits.
If a critic remains, on the one hand, on the same level as the
writer, and on the other hand, on the same level as the reader,
then why in the devil's name do we need him, and what good is
his writing? He is of value only when he can disclose to the
writer in question, or to other writers, the beauty and the
shortcomings of the works criticized. He is of value to the
extent that he can aid tens and hundreds of thousands of
immature readers to form a correct opinion not only about the
class intent of a writer but also about the artistic
effectiveness of his work.
If this is true, then a critic who becomes a scientific guide in a museum of beauty, a cicerone who in addition to pointing out works of art--that is, works which above all claim to rouse aesthetically the emotions of the spectator, works possessing emotional content--and also expatiates in tedious tones on beauty and shortcomings, will only
hamper the effect of the work of art, and in all probability the majority of his "tourists" will demand his withdrawal so that they may establish direct contact with the artists concerned. Moreover, if this cicerone is an "eloquent babbler," and substitutes for sober judgment inflated metaphors, such as Pushkin fought so vehemently to exclude from prose, the effect will be much worse.
No, a real critic must be an artist himself. He is a special
type of artist; he is a receptive artist; he is, so to speak,
the coryphaus of the public, or a member of a choir who is also
the ideal representative of the public. He is, in
short, the public itself, as it should be in its ideal stage. He
is the desired and understanding reader. He is the discerning
reader--friendly or hostile but always a good judge. And in
order not to become isolated and find himself in a detached
vanguard, he must be able to transmit that tremor in his nerves,
that trepidation of his consciousness, which he derives from a
work of art, to present that other aspect of a work of art which
reveals its social origin, its social function, and explain
wherefore this particular work of art fascinates the
reader.
He must be able to convey all this to the widest sections of the public--or, to be more precise, to his own class which he serves as critic. For this reason he must be capable of transforming himself from a unique receptive artist into a unique creative artist. His critical essays, his critical lectures, must be singular artistic works--artistic because they have the power to exert the widest and deepest influence upon the masses.
Lenin was fond of repeating Basarov's phrase: "Arcady, my
friend, do not speak too beautifully."
There are matters of which one must not speak too floridly. They are in themselves, in their bareness, beautiful. To clothe them in multi-colored vestments and adorn them with decorations is in bad taste. We know only too well how juridical eloquence, tricks of sophistry and oratorical art frequently serve to obscure truth. All this is unquestionably true, but woe to him who, condemning flowery phraseology, concludes that we must discard in criticism all keenness, clarity, passion and emotion.
Read the collection of Pushkin's critical essays. Do you find
there Arcady's beautiful phrases? Can you say that Pushkin at
any place speaks too prettily? Do you receive the impression
that he is trying to sell you something dishonestly, or that he
is a cheap lawyer who, in order to defeat his opponent, resorts
to artifice, which only discredits him in your eyes? This
impression you will never receive from Pushkin's
writings.
What will impress you is the unusual clarity of thought, a clarity which is the result of the selection of an adequate form of expression, a clarity which is the result of richness of vocabulary and elasticity of phrase. You will be impressed by the animated, passionate writing, alluring to the highest degree, but not deprived thereby of a certain ornamentation.
Lenin, for example, spoke simply, without ornamentation; he
seldom employed metaphors but the art of oratory was natural to
him. His speech was virtually bare, but the bareness revealed
extraordinary strength and symmetry, which were a joy to
observe. While his speech gave pleasure it performed its
fundamental task--to convince.
The critic may utilize this "Spartan" method of expression. It is the highest ideal, the most difficult to attain. But he may also utilize the "Athenian" method, employed by Pushkin. Criticism may be a natural flow of thought, crystallizing into symmetric and brilliant gems, which sparkle with multicolored fires, and at the same time be thoroughly natural, neither false nor falsifying, without forcing a point, free of belabored metaphorics.
The artist-critic, the artist of criticism--is a splendid
phenomenon. Such, of course, was Belinsky. Such also, in their
best works, were profoundly sober Chernishevsky and
Dobrolyubov. So was Herzen, to a large extent, when he dealt
with literary criticism. This is true of all great writers who
engage in criticism. It is eminently true of Pushkin and therein
lies his memorable lesson.Further reading:
Marxism and Art |
Literary Criticism Section
Lunacharsky Internet Archive 