
        Deville - The People's Marx (1893)
        The preservation of value and the creation of value by labor.—Value simply
        preserved, and value reproduced and increased.
    
        The various factors of the labor-process play different parts in the formation of
        the value of the products.
    
        The laborer adds a new value to the subject of labor by putting additional labor
        upon it, no matter what the character or utility of that labor may be. On the other
        hand, we find re-appearing in the value of the product, the value of the means of
        production consumed, for instance, the value of the cotton and the spindles in the
        value of the yarn. The value of the means of production is then preserved and transferred
        to the product through the medium of labor. But how?
    
        The laborer does not work once to add a new value to the cotton, and a second time
        to preserve the old value, or, what comes to the same thing, to transfer to the
        yarn the value of the spindles that he uses and that of the cotton on which he works.
        It is by the simple addition of a new value that he preserves the old value. But
        as to add a new value to the subject of labor and to preserve an old value in the
        product, are two wholly different results that the laborer obtains simultaneously,
        this two-fold effect can obviously only result from the two-fold character of his
        labor. This labor must at the same time create value by virtue of
        
        one of its properties, and, by virtue of another, preserve or transfer value.
    
        The, spinner adds new value by labor only by spinning, the weaver by weaving, the
        smith by forging, etc. In other words it is the special productive form in which
        the labor is expended that causes the means of production, such as cotton and spindles,
        the yarn and loom, the iron and the anvil, to give birth to a new product. Now,
        we have seen that the labor-time necessary to make the means of production consumed,
        counts as a factor in the value of the new product. The laborer then preserves the
        value of the means of production consumed and transfers it to the product, as a
        constituent portion of its value, by the special useful form of the labor he adds
        to it.
    
        If the special productive labor of the workingman was not spinning, for example,
        he would not make yarn, and, not making yarn, he would not transfer to his product
        the value of the spindles and the cotton employed in spinning. But, by a day's labor,
        our spinner, if he were to change his trade and become a carpenter, would, as before,
        add value to his materials. Therefore, he adds this new value by his labor considered,
        not as the labor of a spinner or a carpenter, but as labor in general, as the expenditure
        of human power; and he adds a definite quantity of value, not because his labor
        has this or that particular useful form, but because it has lasted a definite time.
        Thus, by a quantity of new labor, a new value is added, and, by the quality or kind
        of the labor added, the original values of the means of production are preserved
        in the product.
    
        This two-fold effect of the same labor is clearly apparent in many phenomena.
    
        Let us suppose that some invention enables the spinner to spin in six hours as much
        cotton as he spun before in eighteen. As productive activity the efficiency of his
        labor has been tripled, and his product is three times greater—thirty pounds
        of yarn instead of ten. The quantity of value added by the six hours' spinning to
        the cotton remains the same; but this same quantity, which was absorbed by ten pounds,
        is now distributed over thirty, and, therefore, each pound absorbs only one-third
        as much as before. On the other hand, as thirty pounds of cotton, instead of five,
        are now employed in six hours' spinning, the product of six hours contains three
        times as much value transferred from the cotton. Thus, in six hours' spinning three
        times as much of the value of the raw material is preserved and transferred to the
        product, although the value added to each pound of the same material is only one-third
        as much as before. This shows the essential difference between the property by virtue
        of which labor preserves value and the property by virtue of which, during the same
        process, it creates value.
    
        The means of production transfer to the product only the value that they lose by
        losing their original utility; but, in this respect, the material elements of labor
        act differently.
    
        Raw materials and auxiliary substances lose their characteristic appearance during
        the labor-process. It is quite otherwise with what are properly called the instruments
        of labor, which last a longer or shorter
        
        time, and function in a greater or smaller number of operations. As experience tells
        how long on the average an instrument of labor lasts, we can calculate its daily
        wear and tear or the amount it transfers daily from its own value to the product;
        but the instrument of labor, a machine, for instance, though transferring thus every
        day a portion of its value to its daily product, always performs its part in the
        labor-process as an integral whole.
    
        Consequently, although an instrument of labor enters as a whole into the production
        of a useful object, a use-value, it enters only bit by bit into the formation of
        value. Inversely, a means of production may enter as a whole into the formation
        of value, although it enters into the production of use-value only bit by bit. Suppose
        that in spinning 115 pounds of cotton the waste amounts to 15. If this loss of 15
        pounds is usual, inevitable on the average in manufacture, the value of the 15 pounds
        of cotton that are not transformed into yarn enter just as much into the value of
        the yarn as does the value of the 100 pounds that form its substance. If this waste
        is a necessary condition of production, the cotton wasted or lost transfers its
        value to the yarn.
    
        The means of production transfer to the new product only the value that they lose
        in their original form, and so they can add to it only the value that they themselves
        have. Their value is determined, not by the labor into which they enter as means
        of production, but by the labor from which they issued as products.
    
        Labor-power in action, living labor, has then the property of preserving value while
        adding new value. If this property costs the laborer nothing, it is very advantageous
        to the capitalist, who owes to it the preservation of the existing value of his
        capital. He becomes aware of this in industrial crises, when labor is interrupted,
        and he suffers loss by the deterioration of the means of production that compose
        his capital, raw materials, machinery, etc.
    
        We say that the value of the means of production is preserved, and not reproduced,
        because the objects in which it originally exists disappear only to put on a new
        useful form and the value persists beneath the changes of form. The thing produced
        is a new useful object in which the original value continues to appear.
    
        While labor is preserving and transferring the value of the means of production
        to the product, every instant it creates new value. Suppose production halts when
        the laborer has just created the equivalent of the daily value of his own labor-power,
        when he has, for instance, added by six hours' work, a value of 80 cents. This value
        replaces the money advanced by the capitalist in the purchase of the labor-power
        and then spent by the laborer for articles of subsistence. But this value, contrary
        to what we have shown in the case of the value of the means of production, is actually
        produced. If one value is here replaced by another, it is by means of a new creation.
    
        We already know, however, that labor continues beyond the point where the equivalent
        of the value of the labor-power would be reproduced and added to the subject of
        labor. Instead of the six hours that would suffice, I assume, for that, the process
        lasts twelve hours or more. Labor-power in action, then, not only reproduces its
        own value, but it also produces value over and above it. This surplus-value forms
        the excess of the value of the product over the value of its constituent elements,
        the means of production and labor-power.
    
        Then, in production, that portion of the capital which in converted into means of
        production, i. c., into new materials, auxiliary substances and instruments of labor
        does not, in the process of production change the magnitude of its value. This is
        why we call it the constant portion of capital, or, more briefly, constant capital.
    
        On the contrary, that portion of the capital converted into labor-power changes
        its value in the productive process. It first reproduces its own value, and then
        produces, besides, an excess, a larger or smaller surplus-value. As this portion
        of capital changes in magnitude, we call it the variable portion of capital, or,
        more briefly, variable capital.
    
         
Table of Contents | Gabriel
            Deville Archive | M.I.A. Library
