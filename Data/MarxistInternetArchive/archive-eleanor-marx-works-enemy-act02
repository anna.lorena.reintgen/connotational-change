
The same. The door of the dining-room is closed Morning. MRS. STOCKMANN enters from dining-room with a sealed letter in her hand, and goes to the room right first entrance, and peeps in.
Mrs. Stockmann. Are you there, Thomas?
 Dr. Stockmann [within]. Yes, I've just got back. [Enters.] What is it?
Mrs. Stockmann. A letter from your brother.
[Hands him letter.] 
Dr. Stockmann. Ah! let's see. [Opens envelope and reads.] "The enclosed MS. remitted herewith– [Reads on muttering.] Hm–!
Mrs. Stockmann. Well, what does he say?
 Dr. Stockmann [putting paper in his pocket]. Nothing; he only writes that he'll come up himself about midday.
Mrs. Stockmann. Then you must for once remember to stay at home.
Dr. Stockmann. Oh! I can do that well enough, for I've finished my morning's work.
Mrs. Stockmann. I am very curious to know how he takes it.
Dr. Stockmann. You'll see he won't be over-pleased that I, and not he himself, have made the discovery. 
Mrs. Stockmann. Yes, aren't you afraid of that, too?
Dr. Stockmann. No; at bottom you may be sure he'll be glad. But still–Peter is so d––bly afraid that others besides himself should do anything for the good of the town.
 Mrs. Stockmann. Do you know, Thomas, you ought to be kind, and share the honours with him. Couldn't you say it was he that put you on the track–
 Dr. Stockmann. Yes, gladly, for aught I care. If only I can set matters straighter, I–
[Old MORTEN KIIL peeps in through the further door, looks round inquiringly, and speaks slyly.]
Morten Kiil. Is it–is it true?
 Mrs. Stockmann [going towards him]. Father, is that you?
 Dr. Stockmann. Hallo! Father-in-law; good morning.
good morning.
Mrs. Stockmann. But do come in.
Morten Kiil. Yes, if it's true; if not, I'm off again.
 Dr. Stockmann. If what is true?
 Morten Kiil. That ridiculous story about the water-works. Now, is it true?
Dr. Stockmann. Why, of course it is. But how did you come to hear of that?
Morten Kiil. [coming in]. Petra flew in on her way to school–
Dr. Stockmann. No; did she though?
Morten Kiil. Ay, ay–and she told me–I thought she was only trying to make game of me; but that is not like Petra either.
Dr. Stockmann. No, indeed; how could you think that?
Morten Kiil. Ah! one should never trust anybody. You can be made a fool of before you know it. So it is true, after all?
 Dr. Stockmann. Most certainly it is. Now just sit down, father-in-law. [Forces him down on to the sofa.] And isn't it a real blessing for the town?
Morten Kiil. [suppressing his laughter]. Blessing for the town?
 Dr. Stockmann. Yes, that I made the discovery at such a favourable time–
 Morten Kiil [as before]. Yes, yes, yes; but I never would have believed you could have played your very own brother such a trick.
Dr. Stockmann. Such a trick!
Mrs. Stockmann. But really, dear father–
 Morten Kiil [resting his hands and chin on the top of his stick and winking slyly at the doctor]. Now, what is it all about? Isn't it this way, that some animal has got into the water-pipes?
 Dr. Stockmann. Yes; infusorial animals.
Morten Kiil. And a good many of them have got in, Petra says; quite an enormous number.
Dr. Stockmann. Certainly. There may be hundreds of thousands.
Morten Kiil. But no one can see them. Isn't that so?
 Dr. Stockmann. True; no one can see them.
Morten Kiil. [with a quiet, chuckling laugh]. d–d if that isn't the best thing I've heard from you. 
Dr. Stockmann. What do you mean?
Morten Kiil. But you'll never be able to make the Burgomaster believe anything of the sort.
Dr. Stockmann. Well, that remains to be seen. 
Morten Kiil. Do you really think he'll be so foolish? 
Dr. Stockmann. I hope the whole town will be so foolish.
Morten Kiil The whole town? Well, that may be. But serves them right; much good may it do them. They wanted to be so much cleverer than we old fellows. They chivvied me out of the chairmanship of the Board. Yes, tell I you they chivvied me out like a dog, that they did.
But now it's their turn. Only you keep the game up with
them, Stockmann.
 Dr. Stockmann. Yes; but, father-in-law–
 Morten Kiil. Keep it up, I say. [Rising.] If you can make the Burgomaster and his friends pay through the nose, I'll give a hundred crowns straight away for the poor. 
Dr. Stockmann. Now, that would be good of you.
Morten Kiil. Yes. I've not got much to throw away, as you know; but if you do that, I'll give the poor fifty crowns at Christmas.
[Enter HOVSTAD from ante-room]
Hovstad. Good morning! [Pausing] Oh!
I beg your pardon–
Dr. Stockmann. Not at all. Come in, come in.
Morten Kiil [chuckling again]. He! Is he in it to?
Hovstad. What do you mean?
 Dr. Stockmann. Yes, of course, he's in it.
Morten Kiil. I might have known it!
It must be put into the papers. Ah! you're the right sort, Stockmann. Let them have it. Now I'm off.
Dr. Stockmann. Oh no! Stop a little longer, father-in-law.
Morten Kiil. No, I'm off now. Play them as many tricks as you can; I'll see you don't lose by it.
[Exit. Mrs. Stockmann goes off with him.]
 Dr. Stockmann [laughing]. Only think! That old fellow won't believe a word about that affair of the water-works.
Hovstad. Was that what he–?
Dr. Stockmann. Yes; that was what we were talking about. And maybe you've come to do the same.
Hovstad. Yes. Have you a moment to spare, doctor?
Dr. Stockmann. As many as you like, old man.
Hovstad. Have you heard anything from the Burgomaster?
Dr Stockmann. Not yet. He'll be here presently.
 Hovstad. I've been thinking over the matter since last evening.
Dr. Stockmann. Well–?
 Hovstad. To you, as a doctor and a man of science, this business of the water-works is an isolated affair. I fancy it hasn't occurred to you that a good many other things are connected with it.
 Dr. Stockmann. Yes–how? Let's sit down, old fellow. No–there, on the sofa.
[HOVSTAD sits on sofa; the doctor on an easy chair on the other side of the table.] 
Dr. Stockmann. Well, so you think–?
 Hovstad. You said yesterday that the bad water is caused by impurities in the soil–
Dr. Stockmann. Yes, undoubtedly, it is caused by that poisonous swamp up in the mill dale.
Hovstad. Excuse me, doctor, but I think it is caused by quite another swamp.
Dr. Stockmann. What sort of a swamp may that be?
Hovstad. The swamp our whole municipal life stands and rots in.
Dr. Stockmann. Mr. Hovstad, whatever have you got hold of now?
 Hovstad. All the affairs of the town have little by little me into the hands of a set of bureaucrats.
Dr. Stockmann. Come, now, they're not all bureaucrats.
Hovstad. No; but those who are not are their friends and adherents. They are all wealthy men, the bearers of distinguished names in the town; it is they who control and govern us.
Dr. Stockmann. But they are men of ability and
shrewdness.
Hovstad. Did they show their ability and shrewdness when they laid down the water-pipes where they are?
Dr. Stockmann. No; that was, of course, very stupid of them. But that'll be set right now.
Hovstad. Do you think it will be done so smoothly?
Dr. Stockmann. Well, smoothly or not smoothly, it'll have to be done.
Hovstad. Yes, if the press takes it up.
 Dr. Stockmann. Not at all necessary, my dear fellow; I'm sure my brother–
Hovstad. Excuse me, doctor, but I want you to know that I think or taking up the matter. 
Dr. Stockmann. In the paper?
 Hovstad. Yes. When I took over the Peoples Messenger, I determined that I would break up this ring of obstinate old blockheads who hold everything in their hands.
 Dr. Stockmann. But you yourself told me what it all ended in. You nearly ruined the paper.
Hovstad. Yes, we had to draw in our horns then, that's true enough. For there was the danger that the Baths wouldn't be started if these men were thrown out. But now matters are different, and now we can do without these gentry.
 Dr. Stockmann. Do without them, yes; but still we owe them much.
 Hovstad. Which shall be paid to the full. But a journalist of such democratic opinions as mine can't let such an opportunity as this slip through his fingers. He must explode the fable of the infallibility of our rulers. Such stuff as this must be got rid of, like every other superstition.
Dr. Stockmann.I agree with you there, Mr. Hovstad with all my heart. If it is a superstition, away with it.
 Hovstad. Now, I should be sorry to deal too harshly with the Burgomaster, as he is your brother. But I know you think with me–the truth before all other considerations. 
Dr. Stockmann. Why, of course. But–but–
Hovstad. You mustn't think ill of me. I am neither more obstinate nor more ambitious than most men. 
Dr. Stockmann. But, my dear fellow, who says you are?
 Hovstad. I come from humble folk, as you know, and I have had occasion to see what is wanted by the lower classes of society. And this is, that they should have a share in the direction of public affairs, doctor. This develops power and knowledge and self-respect–
Dr. Stockmann. I understand that perfectly.
Hovstad. Yes, and I think a journalist assumes an immense responsibility when he neglects an opportunity of aiding the masses, the poor, the oppressed. I know well enough that the upper classes will call this stirring up the people, and so forth, but they can do as they please, if only my conscience is clear, I–
 Dr. Stockmann. Just so, just so, dear Mr. Hovstad. But still–deuce take it–[a knock at the door]. Come in!
[Enter ASLAKSEN, the printer, at the door of the ante-room. He is humbly but neatly dressed in black, wearing a white, slightly crumpled neckerchief; and carrying gloves and a felt hat.]
 Aslaksen [bowing]. I beg your pardon, doctor, for making so bold–
 Dr. Stockmann [rising].  Hallo! if it isn't Printer Aslaksen!
Aslaksen. Yes it is, doctor.
Hovstad. [getting up]. Do you want me, Aslaksen?
Aslaksen. No, I don't. I didn't know I should meet you here. No, it was for the doctor himself–
Dr. Stockmann. Well, what can I do for you?
Aslaksen. Is what I've heard from Mr. Billing true–that the doctor is thinking of getting us better water-works?
Dr. Stockmann. Yes, for the Baths.
Aslaksen. Oh! yes, I know that. So I came to say that I'll back up the affair with all my might.
Hovstad [to the doctor]. You see!
Dr. Stockmann. I'm sure I thank you heartily, but–
Aslaksen. For it might do you no harm to have us middle-class men at your back. We now form a compact majority in the town–when we really make up our minds to. And it's always as well, doctor, to have the majority with you.
 Dr. Stockmann. That is undoubtedly true, but I can't conceive that any special preparation will be necessary. I think that in so clear and straightforward a matter–
Aslaksen. Yes. But all the same, it can do no harm; for I know the local authorities so well. The people in power are not very much inclined to adopt suggestions coming from others. And so I think it wouldn't be amiss if we made some sort of a demonstration. 
Hovstad. I think so too.
Dr. Stockmann. Demonstrate, say you? But what do you want to demonstrate about?
Aslaksen. Of course with great moderation, doctor. I am always in favour of moderation; for moderation is a citizen's first virtue–at least those are my sentiments.
Dr. Stockmann. We all know that about you, Aslaksen.
Aslaksen. Yes I think I may claim that much. And this affair of the water-works is so very important for us small middle-class men. And it is through the
Baths that the whole lot of us are going to get our living, especially we householders. And so we shall gladly support the Baths all we can. So, as I am Chairman of the Householders' Association–
Dr. Stockmann. Well?
 Aslaksen. And as I am agent for the Moderation Society –of course you know, doctor, that I work on behalf of moderation?
Dr. Stockmann. To be sure, to be sure.
Aslaksen. So I naturally meet a great many people. And as I am known to be a temperate and law-abiding citizen, as the doctor himself well knows, I have a certain amount of influence in the town, a position of some authority–though say it that shouldn't.
Dr. Stockmann. I know that very well, Mr. Aslaksen.
Aslaksen. Well, so you see it would be easy for me to get I an address, if it came to a pinch.
Dr. Stockmann. An address?
Aslaksen. Yes, a kind of vote of thanks to you, from the citizens of the town, for bringing to light a matter of such importance to the whole community. It goes without saying that it will have to be drawn up with befitting moderation, so that the authorities and persons of position may not be set against it. And if only we are careful about that, no one can take offence, I think.
Hovstad. Well, even if they didn't like it particularly–
Aslaksen. No, no, no; nothing to offend those in authority, Mr. Hovstad. No opposition to people who stand in such close relation to us; I've never gone in for that in my life; no good ever comes of it either. But no one can object to the thoughtful, free expression of a citizen's opinion.
Dr. Stockmann. [shaking his hand]. I can't tell you, dear Mr. Aslaksen, how heartily it delights me to find so much support among my fellow-citizens. I am so happy–so happy! Look here! Won't you take a drop of sherry? Eh?
Aslaksen. No, thank you; I never take any kind of spiritious drink.
Dr. Stockmann. Well, then, a glass of beer–what do you say to that?
Aslaksen. Thanks; not that either, doctor. I never take anything so early in the day. But now I'll be off to town, and talk with the householders, and prepare public opinion.
Dr. Stockmann. Now, that is extremely good of you, Mr. Aslaksen ; but I can't really get into my head that all these preparations are necessary; I think the matter will go of itself.
Aslaksen. Officials are always very slow, doctor–God forbid I should say this by way of accusation–
 Hovstad. To-morrow we'll stir them up in the paper, Aslaksen. 
Aslaksen. But no violence, Mr. Hovstad. Proceed with moderation, or you'll do nothing with them. You take my advice, for I have gained experience in the school of life. And now I'll say good-morning to the doctor. You know now, that we small middle-class men, anyhow, stand behind you like a rock. You have the compact majority on your side, doctor.
Dr. Stockmann. Many thanks, my dear Mr. Aslaksen. [Holds out his hand.] Good-bye, good-bye.
Aslaksen. Are you coming to the printing office, Mr. Hovstad?
 Hovstad. I'll come on presently. I've got something to see to first.
Aslaksen. All right.
[Bows, and goes. Dr Stockmann accompanies him into the
ante-room]
 Hovstad [as the doctor re-enters]. Well, what do you say to that, doctor? Don't you think it is high time we weeded out and got rid of all this apathy and vacillation and cowardice?
Dr. Stockmann. Are you speaking of Aslaksen?
Hovstad. Yes, I am. He is one of those who are in the swamp, though he's a good enough fellow in other things. And so are most of the people here; they're for ever see-sawing and oscillating from one side to the other, and what with scruples and doubts, they never dare to advance a step.
 Dr. Stockmann. Yes, but Aslasken seems to me so thoroughly well-intentioned.
 Hovstad. There is one thing I value more highly; that is to stand your ground as a trusty, self-reliant man. 
Dr. Stockmann. There I am quite with you.
 Hovstad. That's why I am going to seize this opportunity now to see if I can't stir up the well-intentioned among them for once. The worship of authority must be rooted up in this town. This immense, inexcusable blunder of the water-works should be enough to open the eyes of every voter.
Dr. Stockmann. Very well! If you think it is for the good of the community, so let it be; but not till I've spoken my brother.
Hovstad. Anyhow, I'll be getting ready a leader in the meanwhile. And if the Burgomaster won't go in for it–
Dr. Stockmann. But how can you imagine such a thing?
Hovstad. It can be imagined well enough. And then–
Dr. Stockmann. Well then, I promise you; look here–then you may print my paper–put it in just as it is.
Hovstad. May I really? Is that a promise? 
Dr. Stockmann [handing him MS.]. There it is; take it with you. It can do no harm for you to read it; and then tell me what you think of it.
Hovstad. Thanks, thanks; I shall do so willingly. And now, good-bye, doctor.
 Dr. Stockmann. Good-bye, good-bye. Yes, you'll see it will all go smoothly, Mr. Hovstad, so smoothly.
Hovstad. Hm! We shall see.
Dr. Stockmann [going to dining-room door and looking in]. Katrine! Hallo! you back, Petra?
Petra [entering]. Yes, I've just got back from school. 
Mrs. Stockmann [entering]. Hasn't he been here yet?
Dr. Stockmann. Peter? No; but I've been having a long talk with Hovstad. He is quite overwhelmed at my discovery. For, you see, it is much further reaching than I thought at first. And so he has placed his paper at my disposal if occasion requires.
Mrs. Stockmann. But do you think you will need it?
 Dr. Stockmann. Not I? But all the same, one is proud to think that the free, independent press is on one's side. Just think! I've also had a visit from the director of the Householders' Association.
Mrs. Stockmann. Really! And what did he want?
Dr. Stockmann. To offer me support too. Everyone of them will stand by me if there should be any unpleasantness. Katrine, do you know what I have behind me?
 Mrs. Stockmann. Behind you? No. What have you behind you?
Dr. Stockmann. The compact majority!
Mrs. Stockmann. Oh! Is that good for you, Thomas?
 Dr. Stockmann. Yes, indeed; I should think it was good! [Rubbing his hands as he walks up and down.] Ah! by Jove! what a delight it is to be in such fraternal union with one's fellow-citizens!
 Petra. And to do so much good, and be so helpful, father.
Dr. Stockmann. And to do it, into the bargain, for one's native town!
Mrs. Stockmann. There's the bell.
Dr. Stockmann. That must be he. [Knock at the door.] Come in!
[Enter BURGOMASTER STOCKMANN from the ante-room.]
Burgomaster. Good morning.
 Dr. Stockmann. I'm glad to see you, Peter.
Mrs. Stockmann. Good-morning, brother-in-law. How are you?
Burgomaster. Oh,thanks, so, so. [To the doctor.] Yesterday evening, after office hours, I received a dissertation from you concerning the condition of the water connected with the Baths.
Dr. Stockmann. Yes. Have you read it?
Burgomaster. I have.
Dr. Stockmann. And what do you think of the affair?
Burgomaster. Hmm–[Glancing at the women.]
 Mrs. Stockmann. Come, Petra.
 [She and PETRA go into the room, left.]
 Burgomaster [after a pause]. Was it really necessary to make all those investigations behind my back?
 Dr. Stockmann. Yes, till I was absolutely certain I–
Burgomaster. And so you are certain now?
 Dr. Stockmann. Yes, and I suppose it has convinced you too.
Burgomaster. Is it your intention to submit this statement to the Board of Directors as an official document?
 Dr Stockmann. Of course. Why, something must be done in the matter, and that promptly.
Burgomaster. After your wont, brother, you use very strong expressions in your statement. Why, you actually say that what we offer our visitors is a persistent poison!
 Dr. Stockmann. But, Peter, can it be called anything else? Only think–poisonous water both internally and externally! And that for poor sick folk who come to us in good faith, and who pay us heavily to heal them.
Burgomaster. And from this you come to the conclusion that we must build a sewer which will carry off all the supposed impurities from the Miller's Dale, and relay all the water-pipes.
 Dr. Stockmann. Yes. Can you suggest any other alternative?–I know of none.
 Burgomaster. I looked in at the town engineer's this morning, and so–half in jest–I brought up the subject of these alterations as of a matter we might possibly have to take into consideration at some future time. 
Dr. Stockmann. Possibly at some future time!
 Burgomaster. He smiled at my apparent extravagance–naturally. Have you taken the trouble to reflect upon what these proposed alterations would cost? From the information I have received, these expenses would most likely run up to several hundred thousand crowns!
 Dr. Stockmann. So much as that?
 Burgomaster. Yes. But the worst is to come. The work would take at least two years.
 Dr. Stockmann. Two years; do you mean to say two whole years?
 Burgomaster. At least. And what are we to do in the meanwhile with the Baths? Are we to close them? For that is what it would come to. Besides, do you believe anyone would come here if the rumour got abroad that the water is injurious to health?
 Dr. Stockmann. But, Peter, you know it is injurious.
 Burgomaster. And all this now, just now, when the Baths are beginning to do well. Neighbouring towns, too, have some idea of establishing Baths. Don't you see that they would at once set to work to divert the full stream of visitors to themselves? It's beyond a doubt! And we should be left stranded! We should probably have to give up the whole costly undertaking; and so you would have ruined your native town.
 Dr. Stockmann. I–ruined!
 Burgomaster. It is only through the Baths that the town has any future worth speaking of. You surely know that as well as I do.
Dr. Stockmann. But what do you think should be done?
 Burgomaster. Your statement has not succeeded in convincing me that the condition of the water at the Baths is as serious as you represent.
 Dr. Stockmann. I tell you it is, if anything, worse–or will be in the summer, when the hot weather sets in.
 Burgomaster. The existing supply of water for the Baths is once for all a fact, and must naturally be treated as such. But probably the directors, at some future time, will not be indisposed to take into their consideration whether, by making certain pecuniary sacrifices, it may not be possible to introduce some improvements.
 Dr. Stockmann. And do you imagine I could agree for a moment to such a deception?
 Burgomaster. Deception?
 Dr. Stockmann. Yes, it would be a deception–a fraud, a lie; an absolute crime against the public, against all society.
 Burgomaster. I have not, as I have already remarked, been able to attain the conviction that there is really any such imminent danger.
 Dr. Stockmann. You have–you must have. My demonstration was so plainly true and right. Of that I am sure! And you know that perfectly, Peter, only you don't admit it. It was you who insisted that both the Paths and the water-works should be laid out where they now are; and it is that, it is that d–d blunder which you won't confess. Pshaw! Do you think I don't see through you?
 Burgomaster. And even if that were so? It, perhaps, I do watch over my reputation with some anxiety, I do it for the good of the town. Without moral authority I cannot guide and direct affairs in such a manner as I deem necessary for the welfare of the whole community, therefore–and on various other grounds–it is of great moment to me that your statement should not be submitted to the Board of Directors. It must be kept back for the good of all. Later on I will bring up the matter for discussion, and we will do the best we can quietly; but nothing whatever, not a single word, of this unfortunate business must be made public.
Dr. Stockmann. But it can't be prevented now, my dear Peter.
 Burgomaster. It must and shall be prevented.
Dr. Stockmann. It can't be, I tell you; far too many people know about it already.
Burgomaster. Know about it! Who? Surely not those fellows on the People's Messenger, who–
 Dr. Stockmann. Oh, yes! They know, too. The liberal, independent press will take good care you do your duty.
 Burgomaster [after a short pause.] You are an extremely reckless man, Thomas. Haven't you reflected what the consequences of this may be to yourself?
Dr. Stockmann. Consequences?–Consequences to me?
Burgomaster. Yes–to you and yours.
 Dr. Stockmann. What the devil do you mean?
Burgomaster. I believe I have at all times conducted myself towards you as a useful and helpful brother.
 Dr. Stockmann. Yes, you have, and I thank you for it.
Burgomaster. I ask for nothing. To some extent I had to do this–for my own sake. I always hoped I should be able to keep you within certain bounds if I helped to improve your pecuniary position.
Dr. Stockmann. What! So it was only for your own sake–!
Burgomaster. To some extent,I say. It is painful for a man in an official position when his nearest relative goes and compromises himself time after time.
Dr. Stockmann. And you think I do that?
Burgomaster. Yes, unfortunately, you do, without yourself knowing it. Yours is a turbulent, pugnacious, rebellious spirit. And then you have an unhappy propensity for rushing into print upon every possible and impossible matter. You no sooner hit upon an idea than you must write at once some newspaper article or a whole pamphlet about it.
Dr. Stockmann. Yes, but isn't it a citizen's duty, whenever he has a new idea, to communicate it to the public.
Burgomaster. Pshaw! The public doesn't need new ideas. The public is best served by the good old recognised ideas that they have already.
Dr. Stockmann. And you say that thus bluntly–?
Burgomaster. Yes, I must speak to you frankly for once. Until now I have tried to avoid it, as I know how irritable you are; but now I am bound to speak certain truths to you, Thomas. You have no conception how much you injure yourself by your rashness. You complain of the authorities, ay, of the government itself–you even revile them and maintain you've been slighted, persecuted. But what else can you expect, firebrand that you are.
 Dr. Stockmann. What next! So I'm a firebrand, too, am I?
Burgomaster.Yes, Thomas, you are an extremely difficult man to work with. I know it from experience. You set yourself above all considerations; you seem quite to forget that it is I whom you have to thank for your position here as medical officer of the Baths.
 Dr. Stockmann. I had a right to it! I, and no one else! I was the first to discover that the town might become a flourishing watering-place. I was the only one who saw it then. For years I stood alone struggling for this idea of mine, and I wrote and wrote–
Burgomaster. No doubt. But then the right time hadn't come. Of course, in that out-of-the-world hole of 
yours, you were not in a position to judge that. As soon the propitious moment came I–and others–took the matter in hand–
Dr. Stockmann. Yes, and you bungled the whole of my splendid plan. Oh! we see now what shining lights you were.
 Burgomaster. In my opinion we are now seeing that you again need some outlet for your pugnacity. You want to fly in the face of your superiors–and that's an old habit of yours. You can't endure any authority over you; you look jealously upon anyone who has a higher official post than yourself; you regard him as a personal enemy, and then it's all one to you what kind of weapon you use against him; one is as good as another. But now I have called your attention to this, to the great interests at stake for the
town, and consequently for me also. And therefore I tell you, Thomas, that I am inexorable in the demand I am about to make of you!
Dr. Stockmann. And what is this demand?
Burgomaster. As you have been so garrulous in talking about this unpleasant business to outsiders, although it should have been kept an official secret, of course it can't be hushed up. All sorts of rumours will be spread everywhere, and the evil-disposed among us will swell these rumours with all sorts of additions. It will, therefore, be necessary for you to meet these rumours.
Dr. Stockmann. I? How? I don't understand you.
Burgomaster. We venture to expect that after further investigation you will come to the conclusion that the affair is not nearly so dangerous or serious as you had, at the first moment, imagined.
Dr. Stockmann. Ah, ha! So you expect that!
 Burgomaster. Furthermore, we shall expect you to have confidence in the Board of Directors, and to express your belief that they will thoroughly and conscientiously carry out all measures for the removal of every shortcoming.
 Dr. Stockmann. Yes; but you'll never be able to do that as.long as you go on tinkering and patching. I tell you that, Peter, and it is my deepest, most sincere conviction.
 Burgomaster. As an official, you've no right to have any individual conviction.
Dr. Stockmann [starting]. No right to any–
 Burgomaster. As official, I say. In your private capacity, good gracious, that's another matter. But as a subordinate servant of the Baths, you've no right to express any conviction at issue with that of your superiors.
Dr. Stockmann. That is going too far! I, a doctor, a man of science, have no right to–
 Burgomaster. The matter in question is not a purely scientific one; it is a complex affair; it is both a technical and an economic matter.
 Dr. Stockmann. Pshaw! Whats that to me? What the devil do I care! I will be free to speak out upon any subject on earth.
Burgomaster. As you please. But not a word about the Baths–we forbid that.
Dr. Stockmann. [shouting]. You forbid! you!–such fellows–
Burgomaster. I forbid you that–I, your chief; and when I forbid you anything, you'll have to obey.
 Dr. Stockmann [controlling himself].
Peter, really, if you weren't my brother–
[PETRA throws open the door.]
Petra. Father, you shall not submit to this!
[MRS. STOCKMANN following her.]
Mrs. Stockmann. Petra, Petra!
Burgomaster. Ah! so we've been listening!
 Mrs. Stockmann. You spoke so loud; we couldn't help–
Petra. Yes, I did stand there and listen.
Burgomaster. Well, on the whole, I'm glad–
Dr. Stockmann [coming nearer to him]. You spoke to me of forbidding and obeying–
 Burgomaster. You forced me to speak to you in that tone.
 Dr. Stockmann. And have I, in a public declaration, to give myself the lie?
Burgomaster. We consider it absolutely necessary that you should issue a statement in the terms I have requested.
Dr. Stockmann. And if I don't–obey?
Burgomaster. Then we shall ourselves put forth a statement to reassure the public.
 Dr. Stockmann. Well and good. Then I'll write against you. I hold to my opinion. I shall prove that I am right, and you wrong. And what will you say to that?
 Burgomaster. I shall then be unable to prevent your dismissal.
Dr. Stockmann. What–!
 Petra. Father!
Dismissal!
Mrs. Stockmann. Dismissal!
Burgomaster. Your dismissal from the Baths. I shall be obliged to urge that notice be given you at once, in order to dissociate you from everything concerning the Baths. 
Dr. Stockmann. And you would dare to do that! 
Burgomaster. It is you yourself who play the daring game.
Petra. Uncle, such treatment of a man like father is shameful.
Mrs. Stockmann. Do be quiet, Petra.
 Burgomaster [looking at Petra]. Ah, ah! We already allow ourselves to express an opinion. Of course! [To Mrs. Stockmann.] Sister-in-law, apparently you're the most sensible person in the house. Use all your influence with your husband; try to make him realise all this will bring with it, both for his family–
Dr. Stockmann. My family concerns only myself.
Burgomaster. –Both for his family, I say, and the town in which he lives.
 Dr.Stockmann. It is I who have the real good of the town at heart. I want to lay bare the evils that, sooner or later, must come to light. Ah! You shall yet see that I love my native town.
Burgomaster. You, who, in your blind obstinacy, Rant to cut off the town's chief source of prosperity.
 Dr Stockmann. The source is poisoned, man! Are you mad? We live by trafficking in filth and garbage. The whole of our developing social life is rooted in a lie!
Burgomaster. Idle fancies–or something worse. The man who makes such offensive insinuations against his own native place must be an enemy of society.
 Dr. Stockmann [going towards him]. And you dare to–
Mrs. Stockmann [throwing herself between them]. Thomas!
Petra [seizing her father's arm.] Oh! hush, father.
 Burgomaster. I will not expose myself to physical violence. You are warned now. Reflect upon what is due to yourself and to your family. Good-bye.
[Exit.] 
Dr. Stockmann. [walking up and down.] And I must bear such treatment! In my own house. Katrine! What do you think of it?
 Mrs. Stockmann. Indeed, it is a shame and an insult, Thomas–
 Petra. If only I could give it to uncle–!
 Dr. Stockmann. It is my own fault. I ought to have rebelled against them long ago–have shown my teeth–and I made them feel them ! And so he called me an enemy of society. Me! I will not bear this; by Heaven, I will not!
Mrs. Stockmann. But, dear Thomas, after all, your brother has the power–
Dr. Stockmann. Yes, but I have the right!
 Mrs. Stockmann. Ah, yes, right, right! What is the good of being right when you haven't any might? 
Petra. Oh mother! how can you talk so?
Dr. Stockmann. What! No good in a free society to have right on your side? You are absurd, Katrine. And besides, haven't I the free and independent press with me? The compact majority behind me? That's might enough, I should think!
Mrs. Stockmann. But, good Heavens! Thomas, you're surely not thinking of–
Dr. Stockmann. What am I not thinking of?
Mrs. Stockmann. Of setting yourself up against your brother, I mean.
Dr. Stockmann. What the devil would you have me do if I didn't stick to what is right and true?
Petra. Yes, I too would like to know that?
Mrs. Stockmann. But that will be of no earthly use. If they won't they won't.
Dr. Stockmann. Ho, ho! Katrine, just wait awhile and you'll see I shall yet get the best of the battle.
Mrs. Stockmann. Yes, you'll fight them–but you'll get your dismissal; that's what will happen.
Dr. Stockmann. Well, then, I shall at any rate have done my duty towards the public, towards society. I to be called an enemy of society!
Mrs. Stockmann. But towards your family, Thomas? To us here at home? Don't you think your duty is to those for whom you should provide?
Petra. Ah! mother, do not always think first and foremost of us.
 Mrs. Stockmann. Yes, it's all very well for you to talk; if need be you can stand alone. But think of the boys Thomas, and think a little of yourself too, and of me–
Dr. Stockmann. But, really, you're quite mad, Katrine. Should I be such a miserable coward as to humble myself to Peter and his damned crew. Should I ever again in all my life have another happy hour?
Mrs. Stockmann. That I cannot say; but God preserve us from the happiness we shall all of us have if you remain obstinate. Then you would again be without a livelihood, without any regular income I think we had enough of that in the old days. Remember them, Thomas; think of what it all means.
 Dr. Stockmann [struggling with himself and clenching his hands]. And such threats this officemonger dares utter to a free and honest man! Isn't it horrible, Katrine?
 Mrs. Stockmann. Yes; that he is behaving badly to you is certainly true. But, good God! there is so much injustice to which we must submit here on earth! Here are the boys. Look at them! What is to become of them? Oh! no, no, you cannot find it in your heart–
[EJLIF and MORTEN with school-books have entered meanwhile.]
Dr. Stockmann. The boys! [Suddenly stands still, firmly and decidedly.] Never, though the whole earth should crumble, will I bend my neck beneath the yoke.
[Goes towards his room.]
 Mrs. Stockmann [following him]. Thomas, what are you going to do?
 Dr. Stockmann [at the door]. I want to have the right to look into my boys' eyes when they are grown men.
[Exit into room.]
Mrs. Stockmann [bursts into tears]. Ah! God help and comfort us all!
Petra. Father is brave! He will not give in!
[The boys ask wonderingly what it all means; PETRA signs to them to be quiet.] 
Read Act III |
Eleanor Marx Internet Archive 