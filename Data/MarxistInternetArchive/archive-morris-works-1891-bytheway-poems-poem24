
When the boughs of the garden hang heavy with rain  
And the blackbird reneweth his song,  
And the thunder departing yet rolleth again,  
I remember the ending of wrong.  

When the day that was dusk while his death was aloof  
Is ending wide-gleaming and strange  
For the clearness of all things beneath the world's roof,  
I call back the wild chance and the change.  

For once we twain sat through the hot afternoon  
While the rain held aloof for a while,  
Till she, the soft-clad, for the glory of June  
Changed all with the change of her smile.  

For her smile was of longing, no longer of glee,  
And her fingers, entwined with mine own,  
With caresses unquiet sought kindness of me  
For the gift that I never had known.  

Then down rushed the rain, and the voice of the thunder  
Smote dumb all the sound of the street,  
And I to myself was grown nought but a wonder,  
As she leaned down my kisses to meet.  

That she craved for my lips that had craved her so often,  
And the hand that had trembled to touch,  
That the tears filled her eyes I had hoped not to soften  
In this world was a marvel too much.  

It was dusk 'mid the thunder, dusk e'en as the night,  
When first brake out our love like the storm,  
But no night-hour was it, and back came the light  
While our hands with each other were warm.  

And her smile killed with kisses, came back as at first  
As she rose up and led me along,  
And out to the garden, where nought was athirst,  
And the blackbird renewing his song.  

Earth's fragrance went with her, as in the wet grass,  
Her feet little hidden were set;  
She bent down her head, 'neath the roses to pass,  
And her arm with the lily was wet.  

In the garden we wandered while day waned apace  
And the thunder was dying aloof;  
Till the moon o'er the minster-wall lifted his face,  
And grey gleamed out the lead of the roof.  

Then we turned from the blossoms, and cold were they grown:  
In the trees the wind westering moved;  
Till over the threshold back fluttered her gown,  
And in the dark house was I loved.  
Poems by the Way: Next PoemPoems by the Way: Previous PoemPoems by the Way: IndexThe William Morris Internet Archive : Works