
Karl Liebknecht
The Future Belongs to the People

VICE-PRESIDENT PAASCHE in the chair.

On April 7, 1916, Liebknecht declared – in the Reichstag during the discussion of the military estimates – that he had documents showing an agreement between Herr Zimmerman, the Under Foreign Secretary, and Sir Roger Casement, by which British prisoners were to be drilled to fight against England. After some further remarks about Mohammedan prisoners of war being pressed into service for Germany, Liebknecht was prevented from speaking amid shouts of "Traitor!" from all parts of the Chamber.

Liebknecht was able to speak later about the resignation of Von Tirpitz, but was prevented from discussing the submarine campaign. Here is what he said about the resignation of Von Tirpitz:

"After the War had begun with the cry 'Against Czarism' the aim was soon shifted westward." (Vice-President Paasche: "To say that the war began with one or the other object is to insult the Government. I call you to order and ask you not to dwell at any length on our war policy.)

DR. LIEBKNECHT: "After the war aims had been shifted westward – (the Vice-President: "I repeat my request"). I must touch on this question if I am to discuss the opposing currents in the Government which brought about the change in the Admiralty. The manner in which the conflict was taken up in the Prussian Diet, the way in which the sharpening of the war against England was demanded in the Reichstag on account of the Baralong affair, and the scenes in the Prussian Diet before the change of office; throw an interesting light on the differences within the Government and in capitalist circles. A memorandum was to be published on the subject of armed British merchantmen. It was kept back for some length of time. In this one saw an acknowledgment by the Government of the demand for a sharper submarine warfare. The attack in the Prussian Diet was made premeditatedly, in order to show the strong opposition to certain members of the Government (the Vice-President interrupted the speaker) on pressure from the Prussian Diet. (The Vice-President again requested the speaker to keep to the point.) You must not suppress a most important political question." (General commotion. The Vice-President again requested the speaker to keep to the point.)

"I did keep to the point. I shall now discuss the memorandum on the question of armed merchantmen, for which the Admiralty is responsible. It is so composed that those who do not read it carefully with all the supplements must be misled. The memorandum attempts to prove that British merchantmen are armed in order to attack German submarines. (The Vice-President again forbade a discussion of the submarine question, and called Dr. Liebknecht to order.) With such a ruling I am unable – (The Vice-President: "I ask the member not to criticise me.") So I am obliged to say nothing on what politically is most material!"

A few days after this scene in the Reichstag Herr Däumig, the editor of the Socialist organ Vorwärts, sent a Hungarian journalist with a letter of introduction to Dr. Liebknecht for an interview. The censor condensed the interview, and it only reached Budapest by messenger. The following extracts are from the suppressed portion printed in a Budapest (paper) pamphlet:

Dr. Liebknecht was greatly surprised at the visit, as he had been "quite neglected by reporters nowadays because what I say is generally considered `dead copy' by the censor."

The correspondent explains that it is a mistake to suppose that Herr Liebknecht is as unpopular in Germany as he appears to be inside the Reichstag. He showed him correspondence from parts of Germany, a pile received in two days amounting to hundreds and hundreds of letters, ninety per cent of which are of an encouraging and congratulatory character. The remaining ten per cent are scurrilous anonymous attacks, and these he puts in a separate bundle, which he compares with great pride and satisfaction with the heap of more flattering epistles.

He is overjoyed at the idea that he is, after all, not alone, as he appears to be, and that although he is persecuted by his fellow-members of the Reichstag, he is recompensed by the hearty congratulations of the people. What he wanted to say in the Reichstag when he was muzzled and expelled was said by two members, and he is quite satisfied on that point.

"Herr Davidson," said Liebknecht, "referred to the two cases I wanted to mention, and he drew just as vivid a picture of the spirit prevailing in the army and of the illegal persecutions as I should have done if I had been allowed.

"I wanted to call attention to the case of Dr. Nicolai, the world-famous professor at the University of Berlin, who attended the Empress before the war, and who was persecuted some time ago by the military authorities for what were termed indiscreet utterances. He was appointed to the directorship of two military hospitals at the beginning of the war at Graudentz, but some one reported him to the military authorities and he was discharged. On March 1st he was again sent away from Berlin, this time to Danzig, and was ordered to be sworn in as a soldier. He refused to obey, and as a consequence the world-famous professor was degraded to the status of a private. Orders were given that he was not to be allowed to provide his own food, and he was ordered to submit all his scientific literary work to the military authorities for approval.

"The same thing happened to another scientist, who wrote in a letter: `I am sorry for and disapprove of the cruelties committed in Belgium, and, as a good Christian, I regret and disapprove of the terrors of this war."

"I know for a fact that the higher command uses German soldiers to spy on other German soldiers, a system which brands soldiers and commanders alike."

Table of Contents | Next Section
