MIA  >  Archive  >  Cliff  >  Stalinist Russia  The material heritage of the Tsarist period

The rule of the working class where the material conditions for the abolition of capitalist relations of production do not exist

Socialist relations of production

Capitalist function In the introduction to The Critique of Political Economy Marx formulated concisely the main conclusion of historical materialism. He writes:No social order disappears before all the productive forces, for which there is room in it, have been developed; and new, higher relations of production never appear before the material conditions of their existence have matured in the womb of the old society.The Mensheviks quoted this sentence in order to prove that capitalism in Russia was not yet ripe for the socialist revolution, and that it was assured a long future until it would reach such a stage. This simple conclusion, however, neglects a whole series of factors which determine, limit or extend the possibilities of development of the productive forces.What determined the development in Tsarist Russia was, on the one hand, the relation of forces between classes within Russia itself, and, on the other, Russia’s dependence vis-à-vis world capitalism. These two factors are dialectically knit together. If not for the unity of the world, the uneven, combined development of the different countries could not be explained: why the class struggle should take the deepest and most extreme form in such a backward country as Russia, how it was that the Russian proletariat under Tsarism was more concentrated in gigantic enterprises even than the proletariat of the USA. These phenomena are evidence of the high level of social production which the world economy had reached, and the maturity of the world for the substitution of socialist relations of production for capitalist ones. The First World War which accelerated the downfall of Tsarism was no proof of the high level of productive forces in each of the belligerent countries, but it did show that the material conditions were ripe for socialist revolution on a world scale. The series of military defeats, in which the Russian army suffered disastrous losses, showed clearly the industrial and military backwardness of Russia within the advanced world. The fact that Marxism – the fruit of the synthesis of French socialism, English economic theory and German philosophy – was imported to Russia when the workers’ movement was still in its cradle, is evidence of the spiritual unity of the world. On the other hand, the fact that opportunism and revisionism struck much weaker roots in the Russian labour movement than in the countries of the West reveals the backwardness of Russia in a world ripe for socialism: the low standard of living of the workers, kept low by the stream of peasant migration into the towns; the fact that the Russian bourgeoisie had no overseas investments and could this not use part of the resulting superprofits to bribe a layer of workers and improve temporarily the conditions of the masses as a whole for a period of time, as was done in the West; the concentration of the workers in gigantic enterprises; the fact that the country was perched precariously on the powder barrel of the agrarian revolution.The development of the productive forces within a framework of national and international social relations, and not, as Mensheviks would have it, in a vacuum, entirely invalidated their dream of the tremendous possibilities of development before Russian capitalism. On the contrary, in the concrete national and international relations in which Russian capitalism found itself, its continued existence would have conserved the burden of feudalism; it would have involved the country in wars which might well have resulted in transforming backward Russia into a colony or semi-colony of the Western Powers; it would have meant that the development of the national minorities, which made up about half the population of Russia, would have continued to be suppressed. The quotation from The Critique of Political Economy applies not to a country in isolation, but to the world system,. The very fact that the first proletarian revolution broke out in a backward country affirms this – it is the best evidence of the ripeness of the world for the socialist revolution.The Russian revolution is explicable by the law of uneven development, which is one facet of the unity of world development. But this law allows two possibilities of development: firstly, that the Russian revolution, being evidence of the maturity of the world for socialism, would be the prelude to a series of new revolutions which would break out immediately or after a certain time; secondly, as a reformulation of the first possibility, that this “certain interval” lengthens into years, and leaves the Russian revolution isolated in a hostile world capitalism. It was impossible before October 1917 to determine which path humanity would follow by basing oneself simply on the law of universality of world history; one must take account of the contradiction this law contains, the contradiction being the law of uneven development. Human practice alone can decide which way history will go. Now, looking back, we may say that human practice – the main factor being the support the social democratic parties gave capitalism in Western and Central Europe – brought it about that no victorious revolution took place in other countries in the wake of the October revolution.The social order that existed under the Tsar had to disappear for the productive forces to be able to develop. But what social order was to come in its place? Seeing that the destruction of the social order of Tsarist Russia was the result of the maturity of the world for socialism, there is no doubt that, had the revolution spread, the social order that would have come in its place would have been the first stage of communist society. But as the October revolution did not spread, what social order could come in Russia?In seeking an answer to this question the first step is to analyse the material heritage handed down from the social order that existed before October.Men do not build themselves a new world with “earthly goods” as vulgar superstition believes, but with the historical achievements of the old world which is about to go under. In the course of evolution they must begin entirely by themselves to produce the material conditions for a new society, and no effort of the human mind or will can release them from this fate. [1] Chapter 3 Index In 1913 80 per cent of the population of Russia earned their livelihood from agriculture, and only 10 per cent from industry, mining and transport. These figures alone are sufficient to show up the backwardness of Russia. Of all the countries of Europe only Yugoslavia, Turkey, Rumania and Bulgaria show a similar occupational distribution of the population.CountryYearIndustry,
mining and
transportAgriculture,
fishing and
forestryAll other
occupationsYugoslavia19311379  8Turkey19351082  8Romania1930  97813Bulgaria1934  88012The countries of Western and Central Europe and the US as far back as the middle of the nineteenth century showed a much higher percentage of their population occupied in industry, mining and transport and a much lower percentage occuped in agriculture than Russia in 1913. Thus in Britain in 1841 the percentage of population occupied in agriculture, fishing and forestry was 22.7, that occupied in manufacture, building, mining and transport 47.3. France, which lagged a long way behind Britain, in 1837 had 63 per cent occupied in agriculture; in 1866 it had 43 per cent occupied in agriculture and 38 per cent in industry. Germany in 1800 had nearly two-thirds of the population occupied in agriculture; in 1852 it had 44.4 percent occupied in agriculture and 40.9 per cent occupied in industry and handicrafts. The USA, which was at the beginning mainly a country of agricultural settlement, in 1820 had 72.3 per cent occupied in agriculture, forestry and fishing, and 12.3 per cent occupied in manufacture, building and mining; in 1850 it had 64.8 per cent and 17.8 percent respectively.How poor was the material heritage the Bolsheviks acquired on taking power not only in comparison with contemporary developed capitalist countries, but even in comparison with those same countries in the infancy of their capitalist development will become clear from an anaysis of the national income.The most all-embracing calculation of the national income of different countries at different periods was undertaken by Colin Clark in his book The Conditions of Economic Progress. [2] Although we must accept a certain margin of error in a calculation of such magnitude and complexity, this on no account invalidates Clark’s excellent calculations. In order to make an international comparison it is necessary to have a common measure, which Clark dcenotes in “International Unit” (IU). He defines the IU as “the amount of goods and services which one dollar would purchase in USA over the average of the period 1925-34”.Clark estimates the real income per occupied person in Russia in 1913 to be 306 IUs.As against this the real income per occupied person in some developed countries was [3]:Great BritainFranceGermanyUSAYearI.U.YearI.U.YearI.U.YearI.U.1688   3781850-59 [3]38218504201850   7871860-69 [3]   6381860-69 [3]469187763218801,0321904-10 [3]   9991911786191388119001,38819371,275 19291,636 19371,407Thus the average income per occupied person in Russia in 1913 was only 80.9 per cent of the corresponding figure for Britain in 1688 – nearly a hundred years before the Industrial Revolution.Similar figures to these for Russia are given for only these European countries (up to 25 percent above Russia, 1925-34 annual avenge): Finland 380 IUs; Hungary 359; Poland 352; Latvia 345; Italy 343; Estonia 341; Yugoslavia 330. Below Russia were: Bulgaria 259; Romania 243; Lithuania 207.As regards Japan, Colin Clark calculates that its national income per occupied person for 1925-34 was 353 IUs. For Egypt in the same years he estimates that it was 300 to 350, for India 200, for China 100 to 120. [4] Chapter 3 Index Marx and Engels more than once dealt with the question of what would happen if the working class took power before the historical prerequisites were present for the substitution of capitalist relations by socialist relations of production. They concluded that in such an event the working class would lose power which would fall into the hands of the bourgeoisie. The taking of power by the working class would be only temporary and transitory and would encourage the development of capitalism. Thus, for instance, Marx wrote in 1847:If it is true that the bourgeoisie politically, that is, through state power, “maintains the injustice of property relations” [Heinzen’s expression], then it is not less true that it does not create them. The “injustice of property relations” is conditioned by the modern division of labour, the modern form of exchange, competition, concentration, etc., and does not owe its origin in any way to the political domination of the bourgeois class; ... the political domination of the bourgeois class flows from ... the existing relations of production. Therefore, if the proletariat overthrows the political domination of the bourgeoisie its victory will only be temporary, a point in the process of the bourgeois revolution itself, and will serve its cause as it did in 1794. so long as the ‘movement’ of history has not created the material conditions which make it necessary to abolish the bourgeois mode of production and therewith definitely overthrow the political domination of the bourgeoisie. The “Reign of Terror” in France therefore had to accomplish the cleansing of the surface of France from feudal ruins by its terrible hammer blows. The timid, cautious bourgeoisie would not have manage to complete this task in decades. The bloody acts of the people hence merely serves to level the path for the bourgeoisie. [5]What Marx says about a revolution which brings the proletariat to power before the historical premises for the transition from capitalism to communism exist, does not exactly fit the October Revolution. This is so not only because on an international scale the material historical premises were present, but also because of the national conditions. Even under conditions where the revolution did not spread, reality was more complicated than the above Marxian formulation. The Russian bourgeoisie was not only overthrown politically, but it was also expropriated economically a few months after October. The rural bourgeoisie that remained did not succeed in overthrowing the proletariat, and its social weight, especially from the time of the Five-Year Plan, was almost negligible. The isolation of October did not make it “a point in the process” of the development of the Russian bourgeoisie, because the Russian bourgeoisie was annihilated. If so, what relations of production could come after October? Chapter 3 Index The establishment of socialist relations of production demands a much higher level of productive forces than was the heritage of Tsarism. The way Engels explains why in the past society ws divided into classes, exploiters and exploited, entirely fitted Russia’s conditions even after October:The division of society into an exploiting and an exploited class, a ruling and an oppressed class, was the necessary outcome of the low development of production hitherto. So long as the sum of social labour yielded a product which only slightly exceeded what was necessary for the bare existence of all; so long, therefore, as all or almost all the time of the great majority of the members of society was absorbed in labour, so long was society necessarily divided into classes. Alongside of this great majority exclusively absorbed in labour there developed a class, freed from direct productive labour, which managed the general business of society: the direction of labour, affairs of state, justice, science, art and so forth. It is therefore the law of the division of labour which lies at the root of the division into classes. But this does not mean that this division into classes was not established by violence and robbery, by deception and fraud, or that the ruling class, once in the saddle, has ever failed to strengthen its domination at the cost of the working class and to convert its social management into the exploitation of the masses. [6]In post-October, just as in pre-October russia, the material conditions for teh abolition of classes did not exist. Chapter 3 Index The historical mission of the bourgeoisie is the socialisation of labour and the concentration of the means of production. On a world scale this task had already been fulfilled. In Russia the revolution got rid of the impediments to the development of the productive forces, put an end to the remnants of feudalism, built up a monopoly of foreign trade which defends the development of the productive forces of the country from the devastating pressure of world capitalism, and also gave a tremendous lever to the development of the productive forces in the form of state ownership of the means of production. Under such conditions, all the impediments to the historical mission of capitalism – the socialisation of labour and concentration of the means of production which are necessary prerequisites for the establishment of socialism and which the bourgeoisie was not able to fulfil – are abolished. Post-October Russia stood before the fulfilment of the historical mission of the bourgeoisie.Even in an advanced country there will be certain bourgeois tasks which a victorious proletarian revolution will have to accomplish. For instance, in certain parts of the USA (mainly in the South) and in a certain sector of the economy (mainly agriculture) the development of the productive forces is impeded under the capitalist system, so that social production and the concentration of the means of production is not yet realised. But because the productive forces of the USA as a whole are very well developed, these bourgeois tasks will be only accessories, subordinate to the work of building a communist society. Thus, for instance, the establishment of social production and the concentration of the means of production where they do not yet exist, will not be achieved by the creation of a proletariat on the one hand and capital on the other; the labourers from the beginning will not be divorced from the means of production. As against this, in post-October Russia with its low level of national income, the fulfilment of the bourgeois tasks is the central problem. In the United States the addition of new means of production necessary for the socialisation of labour can be accompanied by a rise in the standard of living of the masses, by a strengthening of the element of conviction in production discipline, by the fortification of workers’ control, by the progressive dwindling of the differences in income between manual and mental workers, etc. But can this be achieved in a backward country under conditions of siege? Can labour discipline based mainly on conviction prevail when the level of production is very low? Can a quick tempo of accumulation, necessitated by the backwardness of the country and the pressure of world capitalism, be accomplished without the separation of society into the managers of the general business of society and the managed, the directors of labour and the directed? Could such a separation be put a stop to before those who directed production also directed distribution in their own interests? Can a workers’ revolution in a backward country isolated by triumphant international capitalism be anything but ‘a point in the process’ of the development of capitalism, even if the capitalist class is abolished? Next Chapter:
The common and different features of state capitalism and a workers’ state

Chapter 3 Index 1. K. Marx, Die Moralisierende Kritik und die Kritische Moral. Beitrag zur deutschen Kulturgeschichte. Gegen Karl Heinzen. Aus dem literarischen Nachlass von Marx Engels und Lassalle, Stuttgart 1902, Vol.2, p.456.2. C. Clark, The Conditions of Economic Progress, London 1940, pp.79, 83, 91, 98.3. Annual average.4. There is a close connection between the high percentage of agriculturists in the Russian population and the smallness of the national income per occupied person. This is primarily because Russian agriculture lags behind the agriculture of the developed countries, even more than its industry. Thus the income produced per occupied person in Russian agriculture lags behind that produced in the agriculture of the developed countries much more than the income produced in Russian industry per person occupied in it lags behind that produced in the industry of the developed countries. The smallness of the income produced in Russian agriculture per occupied person will be clear from the following figures:Net Agricultural Production per Person
Occupied in Agriculture
(1925-34 Annual Average)New Zealand2,444Australia1,524Argentina1,233Uruguay1,000United States   661Denmark   642Canada   618Holland   579Germany   490Great Britain   475Switzerland   433France   415Belgium   394Sweden   352Czechoslovakia   287Estonia   268Poland   195Japan   120USSR     885. K. Marx, Die Moralisierende Kritik und die Kritische Moral, op. cit. My emphasis.6. F. Engels, Socialism Utopian and Scientific in Marx-Engels, Selected Works, Vol.I, p.183. Top of the pageLast updated on 16.6.2004
The material heritage of the Tsarist period

The rule of the working class where the material conditions for the abolition of capitalist relations of production do not exist

Socialist relations of production

Capitalist function

Yugoslavia

1931

13

79

  8

Turkey

1935

10

82

  8

Romania

1930

  9

78

13

Bulgaria

1934

  8

80

12

1688

   378

1850-59 [3]

382

1850

420

1850

   787

1860-69 [3]

   638

1860-69 [3]

469

1877

632

1880

1,032

1904-10 [3]

   999

1911

786

1913

881

1900

1,388

1937

1,275

 

1929

1,636

 

1937

1,407

2. The rule of the working class where the material conditions for the abolition of capitalist relations of production do not exist

New Zealand

2,444

Australia

1,524

Argentina

1,233

Uruguay

1,000

United States

   661

Denmark

   642

Canada

   618

Holland

   579

Germany

   490

Great Britain

   475

Switzerland

   433

France

   415

Belgium

   394

Sweden

   352

Czechoslovakia

   287

Estonia

   268

Poland

   195

Japan

   120

USSR

     88
