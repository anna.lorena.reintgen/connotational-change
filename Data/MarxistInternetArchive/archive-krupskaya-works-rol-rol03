
Although Vladimir Ilyich, Martov and Potresov went abroad with
legal passports, they decided in Munich to live under false
passports, and keep away from the Russian colony in order not to
compromise our associates arriving from Russia and the better to
be able to send illegal literature to Russia in suitcases,
letters, and so on.
When I came to Munich Vladimir Ilyich was living unregistered
with this Rittmeyer under the name of Meyer. Although Rittmeyer
kept a beer-house, he was a Social-Democrat and sheltered Vladimir
Ilyich in his flat. Vladimir Ilyich had a poor room, and lived in
bachelor style, having his meals at a German woman's, who kept him
on a Mehlspeise diet. In the morning and the evening he drank tea
out of a tin cup, which he carefully washed himself and hung up on
a nail by the sink.
He looked worried. Things were going slower than he
wanted. Besides Vladimir Ilyich, there lived in Munich at the time
Martov, Potresov and Vera Zasulich. Plekhanov and Axelrod wanted
the paper to be published somewhere in Switzerland under their
direct control. They – and at first Zasulich too – did not attach
great significance to Iskra, and failed completely to
appreciate the organizing role which it could and eventually did
play. They were much more interested in Zarya.
"That Iskra of yours is silly," Vera Zasulich said
at the beginning. Spoken in jest, it nevertheless betrayed a
certain underestimation of the whole enterprise. Vladimir Ilyich
thought Iskra ought to be kept apart from the political emigrant
centre, and run on secret lines. This was vitally important as a
means of facilitating contact with Russia, correspondence and the
arrival of agents. The Old Men were inclined to construe this
as unwillingness to have the paper transferred to Switzerland,
unwillingness to accept their leadership, a desire to pursue an
independent course of action, and so they were in no particular
hurry to help. Vladimir Ilyich sensed this and was worried about
it. He had a soft spot for the "Emancipation of Labour" group, a
great affection for both Axelrod and Vera Zasulich, not to mention
Plekhanov. "Wait till you see Zasulich," he told me the first
evening I arrived in Munich. "She is true to the core." And he was
right.
Vera Zasulich was the only one of the "Emancipation of Labour"
group to identify herself closely with Iskra. She lived
with us in Munich and London, and Iskra and its
editorial board were all she had in the world. Their joys and
sorrows were hers, and tidings from Russia were the air she
breathed.
"Iskra is coming along, you know," she said as the
influence of the paper grew and extended. Vera Zasulich often
spoke about the long bleak years she had lived in emigration.We never experienced the kind of life in emigration that the
"Emancipation of Labour" group had known. We were constantly and
closely in touch with Russia and always had people from there
coming to see us. We were better informed than if we had lived in
some provincial town in Russia itself. We had no life outside the
interests of our Russian work. Things in Russia were on the
upgrade, the working-class movement was rising. The "Emancipation
of Labour" group had been cut off from Russia, living abroad
during the worst period of reaction, when a student arriving from
Russia had been an event. Travellers had been afraid to call on
them. When Klasson and 
Korobko visited them at the beginning of the nineties, they were summoned to the police as soon as they returned and asked why they had gone to see Plekhanov. Police detection was well organized.
Of all the "Emancipation of Labour" group Vera Zasulich lived
the loneliest life. Plekhanov and Axelrod both had families. Vera
Zasulich often spoke about how lonely she felt. "I have no one,"
she would say, then hasten to cover up her feelings with a joke:
"You love me, I know, but when I die the most you'll do will be to
drink one cup of tea less perhaps."
Her yearning for a home and family was all the more poignant
for her having been brought up herself in a strange home as a
ward. How lovingly she dandled Dimka's baby boy (Dimka was
P. G. Smidovich's sister). She even displayed unsuspected gifts
for housewifery and did the shopping when it was her turn to cook
dinner for the "commune" (Vera, Martov and Alexeyev ran a communal
household in London). Few people would have suspected such
domestic inclinations in her, however. She always lived in
nihilist style – dressed carelessly and smoked without a stop; her
room was shockingly untidy, and she never allowed anyone to do
it. Her eating, too, was rather fantastic. I remember her stewing
some meat on an oil-stove and snipping pieces off it with a
scissors and putting them into her mouth.
"When I lived in England," she told me, "the English ladies
tried to be sociable, and asked: 'How long do you stew your meat?'
'All depends,' I said.'II you're hungry ten minutes will do, if
not – three hours or so.' That stopped them."
When Vera had any writing to do, she would shut herself up in
her room and subsist on strong black coffee.
She was terribly homesick. In 1899, I believe, she went to
Russia illegally – not to do any work, but just like that, "to have
a look at the muzhik and see what kind of nose he has." And when
Iskra began to appear, she felt that this was a piece of
real Russian work, and clung to it desperately. For her to leave
Iskra would have meant cutting herself off from Russia
again, sinking back into the slough of emigrant life abroad.That is why, when the question of Iskra editorship was
brought up at the Second Congress, she was filled with
indignation. For her it was not a question of ambition, but a
matter of life and death.In 1905 she went to Russia and stayed there.
Vera Zasulich, for the first time in her life, opposed
Plekhanov at the Second Congress. She had been associated with him
by years of joint struggle, she saw what a tremendous role he
played in having the revolutionary movement guided into the proper
channel, and appreciated him as the founder of Russian
Social-Democracy, appreciated his intellect, his brilliant
talent. The slightest disagreement with Plekhanov distressed her
terribly. Yet in this case she went against him.
Plekhanov's was a tragic fate. In the theoretical field his
services to the workers' movement are almost inestimable. Long
years of life as a political emigrant, however, told on him – -they
isolated him from Russian realities. The broad mass movement of
the workers started after he had gone abroad. He saw the
representatives of different parties, writers, students, even
individual workers, but he had not seen the Russian working-class
mass, had not worked with it, nor felt it. Sometimes, when letters
came from Russia that lifted the veil over new forms of the
movement and revealed new vistas, Vladimir Ilyich, Martov and even
Vera Zasulich would read them over and over again. Vladimir
Ilyich would then pace the room for a long time and not be
able to fall asleep afterwards. I tried to show those letters to
Plekhanov when we moved to Geneva and was surprised at the way he
reacted. He seemed to be staggered, then looked incredulous, and
never spoke about them again.
His attitude towards those letters from Russia became more
sceptical than ever after the Second Congress.
I felt hurt at this at first, and then I thought I began to
see the reason. He had been away from Russia for such a long time
that he had lost that capacity, developed by experience, which
enables one to gauge the value of each letter and read between the
lines.
Workers from Russia often came to Iskra, and all of
them, of course, wanted to see Plekhanov. Seeing him was much more
difficult than seeing us or Martov, and even when a worker did get
to see him, he would come away feeling baffled. Plekhanov's
brilliant intellect, knowledge, and wit would impress the worker,
but all that the latter felt on leaving him would be the vast
gulf between him self and that brilliant theoretician. The things
that had lain uppermost in his mind, the things he had been so
eager to talk to him about and ask his advice on, had remained
unuttered.
And if a worker differed with Plekhanov and tried to express
his own opinion, Plekhanov would get angry and say: "Your daddies
and mummies were knee-high when I...."
I daresay he was not like that at the beginning of his
emigration, but by the turn of the century he no longer had the
live feel of Russia. He did not go to Russia in 1905.
Axelrod was much more of an organizer than either Plekhanov
or Zasulich. He saw much more of the new arrivals, who spent most
of their time with him, and had their meals at his lodgings. He
questioned them closely about everything.
He carried on a correspondence with comrades in Russia and
was well up in secrecy techniques. One can well imagine how a
Russian revolutionary organizer must have felt, living for years
in Switzerland as a political emigrant! Axelrod worked at only a
quarter of his former capacity; he did not sleep for nights at a
stretch, and writing was a tremendous strain on him – it took him
months to finish an article he had started, and his handwriting
was almost illegible owing to the nervous way he wrote.
His handwriting always upset Vladimir Ilyich. "It's terrible
to think of one reaching such a state as Axelrod," he would often
say. He often spoke about Axelrod's handwriting to Dr. Kramer,
who attended Ilyich during his last illness. When Vladimir
Ilyich first went abroad in 1895 he had discussed organizational
questions mostly with Axelrod. He told me a lot about him when I
arrived in Munich. He asked me what Axelrod was now doing by
pointing to his name in the newspaper when he himself could no
longer write or even speak a word.
Axelrod reacted rather painfully to the fact that Iskra
was not being published in Switzerland and that the flow of
communications with Russia did not pass through him. That accounts
for his bitter attitude on the question of an editorial trio at
the Second Congress. Iskra to be the organizing centre,
while he was removed from the editorial hoard! And this at a time
when the breath of Russia made itself felt more strongly than ever
at the Second Congress.
When I arrived in Munich the only member of the "Emancipation
of Labour" group living there was Vera Zasulich. She had a
Bulgarian passport and lived under the name of Velika
Dmitriyevna.
All the others had Bulgarian passports too. Until my arrival
Vladimir Ilyich had been living without any passport at all. When
I came we took a passport in the name of Dr. Yordanov, a
Bulgarian, with his wife Marica, and rented a room we saw
advertised in a working-class home. The secretary of Iskra
before me had been Inna Smidovich-Leman. She, too, had a
Bulgarian passport, and her Party sobriquet was Dimka. Vladimir
Ilyich told me when I arrived that he had arranged for me to be
the secretary of Iskra on my arrival. This, of course,
meant that all intercourse with Russia would be closely
controlled by Vladimir Ilyich. Martov and Potresov had had nothing
against this at the time, and the "Emancipation of Labour" group
had put up no candidate of their own, as they had not attached
any particular importance to Iskra at the time. Vladimir
Ilyich told me he had felt very awkward about doing this, but had
thought it necessary I in the interests of the cause. I had my
hands full at once. Things were organized in this way: letters
from Russia were addressed to German comrades in various towns in
Germany, and they readdressed them to Dr. Leman, who forwarded
them on to us.
Shortly before this there had been quite a scare. Our
comrades in Russia had succeeded at last in setting up a printing
plant in Kishinev. The manager Akim (brother of Lieber – Leon
Goldman) sent to Leman's address by post a cushion with copies of
pamphlets published in Russia sewn up in it. Leman refused
delivery of the parcel, thinking it a mistake, but when our people
got to know about it and raised an alarm, he took the cushion from
the post office and said that he would henceforth accept delivery
of everything that was addressed to him, even if it was a
trainload.
We had no transport facilities yet for smuggling Iskra
into Russia. It was sent in mainly in double-bottom suit
cases through various travellers, who delivered them at secret
addresses in Russia.
One such secret rendezvous was the Lepeshinskys' in
Pskov. Another was in Kiev and some other town. The comrades in
Russia took the literature out of the suitcases and handed it over
to the organization. Shipments had only just begun to be arranged
through the Letts Rolau and Skubik.
All this took up a lot of our time. A good deal of time was
also wasted on all kinds of negotiations, which led to
nothing.
I remember wasting a week negotiating with a fellow who
planned to get in touch with smugglers by travelling along the
frontier with a camera, which he wanted us to buy for him.
We corresponded with Iskra agents in Berlin, Paris,
Switzerland and Belgium. They tried to help as best they could by
raising money and finding willing travellers, connections,
addresses, and so on.
An organization called the League of Russian Revolutionary
Social-Democrats Abroad was formed out of the sympathizing groups
in October 1901.
Connections with Russia grew apace. One of the most active
correspondents of Iskra  was the St. Petersburg worker
Babushkin. Vladimir Ilyich had seen him before leaving Russia and
made arrangements with him to send in correspondence. He sent in a
mass of reports from Orekhovo-Zuvevo Vladimir, Gus-Khrustalny,
Ivanovo-Voznesensk, Kokhma and Kineshma. He made a regular
round of these towns and strengthened contacts with them. Letters
also came from St. Petersburg, Moscow, the Urals and the South. We
corresponded with the Northern Union. Noskov, a representative of
the Union, arrived from Ivanovo-Voznesensk. A more Russian
type it is difficult to imagine. Fair-skinned and
blue-eyed, with a slight stoop, he spoke with a broad
country accent, and had arrived abroad with a small bundle to make
all the necessary arrangements. His uncle, the owner of a small
mill in Ivanovo-Voznesensk, had given him the money for the trip
in order to get rid of his troublesome nephew, who was for ever
being run in and having his room searched by the police. Boris
Nikolayevich Noskov (Babushkin's alias, his real name and
patronymic being Vladimir Alexandrovich) was an experienced
practical worker. I had met him in Ufa where he had stopped over
on his way to Ekaterinburg. He came abroad for contacts. Making
contacts was his profession. I remember him sitting on the stove
in our Munich kitchen, telling us with shining eyes about
the work of the Northern Union. He was terribly enthusiastic, and
Vladimir Ilyich's questions only added fuel to the flames. Boris
kept a note-book while he lived abroad, in which he meticulously
wrote down all contacts: where this or that one lived, what he
did, and how he could be useful. He left us that note-hook
afterwards. His work as an organizer had a poetic sort of
quality. He overidealized his work and people, however, and lacked
the ability to face up to reality. After the Second Congress he
became a conciliator, and later disappeared from the political
scene. He died during the years of reaction.
Other people came to Munich too. Struve had been there before
my arrival. Things were already heading for a break with him. He
was passing over from the Social-Democratic to the liberal
camp. On the occasion of his last visit there had been a serious
clash. Vera Zasulich had nicknamed him "the book-fed calf." Both
Vladimir Ilyich and Plekhanov had given him up, but Vera Zasulich
still thought there was hope for him. We jokingly called her and
Potresov the "Struve-freundliche Partei." Struve visited
Munich again when I was there. Vladimir Ilyich refused to see
him. I went to see Struve at Vera's rooms. The interview was a
very painful one. Struve felt terribly hurt. There was a
Dostoyevsky sort of touch about it all. He spoke about his being
regarded as a renegade and other things in a similar strain, and
acted the self-tormentor. I do not remember everything he said,
but I do remember the heavy feeling with which I came away from
that meeting. Plainly, he was a stranger, a man hostile to our
Party. Vladimir Ilyich had been right. Afterwards Struve's wife,
Nina Alexandrovna, sent us her regards and a box of sweets through
somebody – I don't remember who now. She was powerless, and I doubt
whether she realized where her husband was heading. He knew,
though.
After my arrival we lived in rooms at a German working-class
home. They were a family of six, and all lived in the kitchen and
a tiny room, but everything was spotlessly clean. The children,
too, were tidy and polite. I decided to put Vladimir Ilyich on
home-cooked food and tackled the pots and pans. I did the cooking
in the landlady's kitchen, but prepared everything in our own
room. I tried to make as little noise as possible, because
Vladimir Ilyich had then begun to write What Is To Be Done?
When writing, he would usually pace swiftly up and down the
room, whispering what he was going to write. I had already adapted
myself to his mode of working, and when he was writing I never
spoke to him or asked him any questions. Afterwards, when we went
out for a walk, he would tell me what he had written and what he
was thinking about. This became as much a necessity to him as
whispering his article over to himself before putting it down in
writing. We went for long rambles on the outskirts of Munich,
choosing the loneliest spots where there were fewer people
about.
A month later we moved into a flat of our own in Schwabing, a
suburb of Munich, in one of the numerous newly erected buildings,
and got ourselves some furniture (we sold it all for twelve marks
when we left). We now Settled down to real home life.
After lunch – which was at twelve – Martov and others came to
attend the so-called editorial meeting. Martov Spoke without a
stop, jumping from one subject to anOther. He read a lot and was
always chock-full of news. He knew everything and
everybody. "Martov is a typical journalist," Vladimir Ilyich often
said about him. "He is remarkably talented, quick at grasping
things, terribly impressionable and easy-going." Martov was an
indispensable man for Iskra. Those five-to-six-hour
talks every day were very tiring for Vladimir Ilyich. He used to
feel quite ill after them and was unfit for work. He asked me once
to go and see Martov and tell him not to come to us. We arranged
that I would call on him myself, and tell him what letters we had
received and arrange everything with him. But nothing came of
it. Two days later we were back again where we were. Martov could
not live without these talks. From us he would go to a cafe with
Vera Zasulich, Dimka and Blumenfeld and sit there talking for
hours.
Afterwards Dan arrived with his wife and children, and Martov spent most of his time with them.
We went to Zurich in October to amalgamate with Rabocheye
Delo. Nothing came of it, though. Akimov, Krichevsky and
others talked themselves silly. Martov worked himself up to such a
pitch in his attack on the Rabocheye Delo adherents that
he even tore his tie off. I had never seen him like that
before. Plekhanov scintillated. A resolution was drawn up to
the effect that amalgamation was impossible. Dan read it out at
the conference in a wooden voice. "Papal nuncio," his opponents
shouted at him.
This split was a painless one. Martov and Lenin had not
collaborated with Rabocheye Delo, and strictly speaking
no break had occurred since there had never been any
cooperation. On the other hand, Plekhanov was in high
feather. The opponent he had been grappling with for so long was
at last worsted. Plekhanov was cheerful and chatty.
We lived in the same hotel, and had our meals together, and
everything seemed to be going well. Only occasionally did a very
slight difference ill the approach to certain questions make
itself felt.
One conversation sticks in my memory. We were sitting in a
cafe, and in the room next to ours there was a gymnasium where
fencing was in progress. Workers armed with shields and cardboard
swords were engaged there in a sham battle. Plekhanov laughed,
saying: "That's how we shall fight under the new order." Going
home – I walked with Axelrod – he developed the theme touched on by
Plekhanov. "Under the new order everything will be a deadly bore,"
he said. "There will be no struggle." I was still painfully shy then and said nothing, but I remember
being surprised at such an argument.
After we returned from Zurich Vladimir Ilyich sat down to
finish his What Is To Be Done? Later the Mensheviks
vehemently attacked that pamphlet, but at that time it gripped
everybody, especially those who were more closely associated
with Russian work. The pamphlet was an ardent appeal for
organization. It outlined a broad plan of organization in which
everyone would find a place for himself, become a cog in the
revolutionary machine, a cog, which, no matter how small, was
vital to the working of the machine. The pamphlet urged the
necessity of intensive and tireless efforts to build the
foundation that had to be built if the Party was to exist in deeds
and not in words under the conditions then prevailing in Russia. A
Social-Democrat should not be afraid of long, hard work. He must
work and work unremittingly, and be ever ready "for everything,
from upholding the honour, the prestige and continuity of the
Party in periods of acute 'evolutionary 'depression,' to preparing
for, fixing the time for and carrying out the nation-wide
armed insurrection," Vladimir Ilyich wrote in What Is To
Be Done? 
Twenty seven years have passed since that pamphlet was
written, and what years! The conditions of work for the Party have
changed completely and entirely new tasks confront the workers'
movement, yet the revolutionary passion of this pamphlet is
irresistible even today, and it should be studied by everyone who
wants to be a Leninist in deeds and not in words.
Whereas The "Friends of the People" was of tremendous
significance in defining the path which the revolutionary movement
had to take, What Is To Be Done? can be said to have
defined a plan for extensive revolutionary activities. It pointed
out a definite task.
It was clear that a Party congress was still premature, that
the conditions capable of preventing it from coming to nothing as
the First Congress had done were lacking, and that long
preparatory work was necessary. The attempt by the Bund,
therefore, to convene a congress in Belostok was not taken
seriously by anybody. Dan went there from Iskra, taking
with him a suitcase whose false lining was crammed with copies
of What Is To Be Done? The Belostok Congress turned into
a conference.
Vladimir Ilyich was particularly interested in the attitude
of the workers to that pamphlet. He wrote to I. I. Radchenko on
July 16, 1902: "I was ever so glad to read your report about the
talk with the workers. We receive such letters much too
rarely. They are really tremendously cheering. Be sure and convey
this to your workers with our request that they should write to us
themselves, not just for the press, but to exchange ideas, so that
we do not lose touch with one another and for mutual
understanding. Personally I am particularly interested to know
what the workers think of What Is To Be Done? So far I
have received no comments from the workers." 
Iskra was going strong. Its influence was
increasing. The Party programme was being prepared for the
congress. Plekhanov and Axelrod came to Munich to discuss
it. Plekhanov attacked parts of the draft programme which Lenin
had drawn up. Vera Zasulich did not agree with Lenin on all
points, but neither did she agree entirely with Plekhanov. Axelrod
also agreed with Lenin on some points. The meeting was a painful
one. Vera Zasulich wanted to argue with Plekhanov, but he looked
so forbidding, staring at her with his arms folded on his chest,
that she was thrown off her balance. The discussion had reached
the voting stage. Before the voting took place, Axelrod, who
agreed with Lenin on this point, said he had a headache and wanted
to go for a walk.
Vladimir Ilyich was terribly upset. To work like that was
impossible. The discussion was so unbusiness-like.Organizing the work on a business-like footing without
introducing any personal element into it, and thus ensuring that
caprice or personal relations associated with the past would not
influence decisions, had now become an obvious need.
All differences with Plekhanov distressed Vladimir Ilyich
greatly. He fretted and did not sleep at night. Plekhanov on the
other hand was sulky and resentful.
After reading through Vladimir Ilyich's article for the
fourth number of Zarya, Plekhanov returned it to Vera
Zasulich with marginal notes in which he gave full vent to his
annoyance. When Vladimir Ilyich saw them he was greatly upset.
By this time it became known that Iskra could no
longer be printed in Munich. The owner of the print-shop did not
want to run the risk. We had to move. But where? Plekhanov and
Axelrod were for Switzerland, the rest – after that whiff of the
atmosphere that had prevailed during the discussion of the
programme – voted for London.We looked back on this Munich period afterwards as a bright
memory. Our later years of life in emigration were a much more
distressing experience. During the Munich days the rift in the
personal relations between Vladimir Ilyich, Martov, Potresov and
Zasulich had not been so deep. All energies had been
concentrated upon a single object – the building up of an
all-Russian newspaper. There had been an intensive rallying of
forces around Iskra. All had had the feel of the
organization's growth, a sense that the path for creating the
Party had been rightly chosen. That explains the genuine spirit of
jollification with which we had enjoyed the carnivals, the
universal good humour that had prevailed during our trip to
Zurich, and so on.Local life held no great attraction for us. We observed it
merely as bystanders. We went to meetings sometimes, but on the
whole they were of little interest. I remember the May Day
celebrations. For the first time that year the German
Social-Democrats had been permitted to organize a procession, on
condition that the celebrations were held outside the town and no
crowds collected within the town.
Fairly large columns of German Social-Democrats with their
wives and children, their pockets stuffed with horseradishes,
marched swiftly through the town in silence to drink beer in a
suburban beer garden. There were no flags, no placards. That
Maifeier bore very little resemblance to a demonstration
of working-class triumph throughout the world.
We did not follow the procession to the suburban beer garden,
but dropped behind and roamed the streets of Munich as was our
habit, in order to let the feeling of disappointment that had
crept into our hearts wear off. We wanted to take part in a real
militant demonstration, and not a procession sanctioned by the
police.
As we were working in strict secrecy, we never met any of the
German comrades except Parvus, who lived near us in Schwabing with
his wife and little son. Rosa Luxemburg came to see him once, and
Vladimir Ilyich went there to meet her. Parvus was then an extreme
Left-winger. He contributed to Iskra and was interested
in Russian affairs.
We travelled to London via Liege. Nikolai Meshcheryakov was
living in Liege at the time with his wife – both old Sunday School
friends of mine. I had known him as a Narodovolets, but
he had been the first to initiate me into illegal work, the first
to teach me secrecy technique and help me to become a
Social-Democrat by keeping me well supplied with the foreign
publications of the "Emancipation of Labour" group.
Now he was a Social-Democrat. He had been living in Belgium
for a long time and was familiar with the local movement. We
decided to call on him en route.
There was tremendous excitement in Liege at that time. A few
days previously the troops had fired on the strikers. The ferment
in the working-class districts could be read in the faces of the
workers and the people, who stood about in knots. We went to see
the People's House. It was very inconveniently situated, and any
crowd standing in front of the building could easily be cooped up
and trapped. The workers were flocking to it. To avoid a crowd
gathering there, the Party leaders had arranged meetings in all
the working-class districts. This gave rise to a vague mistrust of
the Belgian Social-Democratic leaders. It was very much like a
division of labour, some shooting at the crowd, others seeking an
excuse to pacify it....
Read next section |
Krupskaya Internet Archive |
Marxists Internet Archive
