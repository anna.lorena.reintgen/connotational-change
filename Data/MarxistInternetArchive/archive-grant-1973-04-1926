Written: April
1973
Source: Militant (April 27, 1973)
Transcription: Maarten Vanheuverswyn, August
2006
Markup/Proofread:: Maarten Vanheuverswyn, August
2006The one day May Day general strike marks a peak in the post-war
struggle of the British working class. It may be succeeded by a
lull for a short time, in the sense that there may be no national
strikes organised by the unions in defence of wages and conditions,
and against the incomes policy.This is a policy of limiting wages, while prices, especially of
food, are going up twice as fast as they were before it was
introduced. Thus it is a policy to limit the income of the working
class.As prices continue to mount, the exasperation and anger of the
working class will grow. The trickery and confidence tricks of the
employers and the government in the prices and incomes policy will
become even clearer than at the present time.The Trade Union leaders are endeavouring to arrive at some sort
of "compromise" and "agreement" with the employers and the
government on a viable incomes policy. Even such leaders as Scanlon
and Jones have suggested agreements of this sort. But the
endeavours to arrive at a compromise will break down when it comes
clear to the workers what this involves.Some capitalist Cambridge economists have pointed out that it is
necessary to maintain consumption at the same level as it is at the
present time for the next five years. That is at the level where
rickets have appeared in all the main cities because of the poverty
standards of the lower paid workers, where the consumption of meat
per head of population is now 20% below the last year of rationing
which means an even lower level for the working class, and where
malnutrition has appeared among building workers (not the worst
paid section of the working class).Thus, inevitably the attempt to maintain this position will meet
with further resistance on the part of the working class. During
the last three years there has been an unprecedented level of
struggle, a far higher level than the stormy years of 1918 to 1921.
White collar workers as well as industrial workers, skilled and
unskilled, have been involved in battle with the employers and the
government. For the first time in history, all the different layers
of the working class have been involved in militant struggle.The mood of the workers is such that the exposure of the
confidence trick of the government will make new battles
inevitable. Despite the wish for compromise by the Trade Union
leaders, Left and Right, under the pressure of the working class,
new national strikes will take place. This in its turn, because of
the explosive mood in a period of sharp changes and abrupt turns in
which we live, could possibly even involve the outbreak of an
all-out general strike.At the moment the prospect of this has receded, and it seems as
if partially at any rate the workers will turn to the political
field as a means of solving their problems. But the possibility
still exists, therefore it is imperative that the active
layers of the working class, the most conscious politically and
industrially, should be armed with the understanding of what is
really involved in a general strike, and the history of the great
events of 1926.The causes of the 1926 general strike lay in a similar situation
to the position which is developing at the present time. In 1925
the ruling class had a policy of deflation, instead of, as at
present, inflation. But the results for the working class are the
same. It is the choice of death by fire or death by a thousand
cuts.In 1926, the position of the ruling class was that of a direct
assault on the wages of the working class. The Tory Prime Minister
Stanley Baldwin, has declared at a meeting with the Miners' Union
on July 30th, 1925: "All the workers in this country have got to
take reductions in wages to help put industry on its feet".The miners were to accept a reduction of 13% in starvation wages
and to work an extra hour. This attack on the miners and on all
sections of the working class was because of the decline of British
Capitalism. With an antiquated industry, she could only succeed in
maintaining a hold on world markets in the face of more modern and
up to date rivals, at the expense of the working class.It is a similar situation which the working class in Britain is
facing at the present time. The British capitalist class wishes to
put the burden of its own inefficiency and incapacity onto the
shoulders of the working class.The working class in solidarity with the miners and in defence
of its own living standards, were prepared to resist. The transport
unions and other sections issued instructions to black the handling
and transporting, of coal if the coal owners carried out
their threat to lock out the miners. The shadow of conflict
loomed.Despite the complaint of the backwoodsmen of the Tory Party in
Parliament, 300 of whom moved a resolution of protest, Baldwin and
the government prepared a temporary retreat in order to thoroughly
prepare for a showdown with the miners and with the whole of the
working class. A 23 million subsidy was given for nine months to
allow the government time to complete its preparations.At the same time the Samuel Commission was appointed to go into
the question of the mining dispute. At the end of nine months they
recommended increased hours, lower wages and district agreements, a
policy which the miners and the trade union movement generally had
already rejected!The Tory government prepared an "Organisation for the
Maintenance of Supplies" (OMS), a civil guard, which the British
Fascists joined, Special Constables, and an elaborate network for
each county in Britain to confront the TUC and the working
classThe working class in Britain had been swinging to the left after
the coming to power of the Baldwin government in 1924, on the basis
of the forged and notorious, "Zinoviev Letter". The expression of
this move to the left was the organisation of the Minority Movement
in the trade unions, with a left wing programme, which succeeded in
organising in its ranks 1,250,000 members, or a quarter of the
organised Trade Union Movement.The leaders of the left wing in the TUC made far more radical
speeches and gestures than the Left Wing at the present time. An
"Anglo-Russian Committee" of Russian and British Trade Union
leaders, to cement an alliance allegedly to fight war, was
organised, on which the Left wing leaders of the General Council
were prominent.The Left leaders were immensely popular throughout the working
class, and gave very fiery speeches, some even coming out for the
"Socialist Revolution". They reflected the pressure of the masses,
thoroughly aroused and alarmed at the threatened attacks on an
already low standard of living. A.J. Cook went around saying that
he was "proud to be a follower of Lenin".He coined the phrase "Not a penny off the pay, not a second on
the day". This was a fine and decisive slogan to rally the miners
and the working class. He argued that a "strike of the miners would
mean the end of capitalism". And that "97% of the new recruits to
the police and armed services have come from the working class, and
thousands of them are miners, who will not shoot against their kith
and kin when the order comes."But even Cook, the best and most honest of these leaders, had no
idea of what was involved or how to organise it. For him it was
just radical phraseology.Because of the enormous indignation of the workers, the General
Council, in reply to the refusal of the government to make
concessions, threatened to call a General Strike. The decision to
strike was taken by 3,653,527 votes to 49,911 votes. This historic
decision was taken on May 1st. But behind the scenes, desperately,
the General Council of the TUC appointed a Committee to try to
negotiate with the Conservative Government.They were prepared even to accept a cut in wages for the miners
as the price of a "negotiated settlement". In the Cabinet, the
extreme right wing, the Monday clubbers of that day, were exerting
pressure for a showdown. Under this pressure, Baldwin used the
pretext of the Daily Mail printers' refusal to print a vicious
attack on the Unions and the miners, to break off negotiations.After the TUC, with great difficulty, had succeeded in getting
the printers reluctantly to print the editorial, they returned to
the negotiations with the Prime Minister at Downing Street in the
early hours of Sunday morning, where they were told "Mr. Baldwin
has gone to bed and cannot be disturbed."Thus the ruling class deliberately provoked and precipitated the
general strike as a means of defeating the workers and forcing them
to accept a lower standard of living.They got more than they bargained for! Hoping against hope for
some sort of agreement, the TUC tremblingly had made no
preparations for the strike whatsoever. But the magnificent
capacity of the Trade Union and Labour Movement to improvise and
organise came as a surprise to the government and even to the Union
leaders themselves. The first sections to be called out were the
miners, dockers, seamen and workers in transport, heavy chemicals,
building (except housing) and production of electric light and gas
for industrial purposes.The leaders of the Seamen's union refused to join the strike and
organised blacklegging. But in spite of this, the strike was
absolutely solid, and the rank and file of the seamen supported the
strike.There was initiative and improvisation from below. The Trades
Councils in every area formed Councils of Action, and strike
committees. In some areas the Co-ops and Labour Parties were
involved, and in many less, also the Communist Party. Prominent
leaders and individuals were co-opted to the Strike Committee.The organisation of the strike in the North East, for example,
was to form Committees in each area with an overall Committee for
Durham and Northumberland. Transport, food, a workers defence
force, publicity and permits were organised. In a sense these were
the elements of an alternative government appearing to confront the
so-called "legal" government.Many of the police were sympathetic and troops were held in
reserve. Two submarines were used in the Thames for the purpose of
providing light on the docks. But a warship which appeared in the
Tyne was withdrawn after threats of the Council of Action to
withdraw the safety and emergency men. So powerful was the Council
of Action on Tyneside that Kingsley Wood, the representative of the
government in the area was compelled to negotiate with the Tyneside
Regional Council of Action for permits for the transport of
food.Non-Unionists joined in the struggle and joined the Unions in
droves; in some cases non-unionists even preceded the union workers
in coming out on strike! There were militant demonstrations and
processions in all the main towns and baton charges were used by
the police. There were about three thousand arrests.The brunt of the repression was felt by the then revolutionary
Communist Party. In preparation for the strike, already twelve of
their main leaders had been sentenced to imprisonment, and were
safely out of the way on charges of "sedition".The Workers' Weekly was raided and the press was immobilised by
taking away key parts of the machinery. Up and down the country
Communists were also being arrested, along with thousands of
workers, who were charged with incitement and jailed for terms of
six weeks to two months.The ranks of the workers were solid, and each day more were
coming out. But the very success of the strike provoked more fear
in the General Council than in the government! They were terrified
of the movement they had called into being on May 3rd.On the eighth day there was a call-out of the engineers and
other sections, although in many cases these were already coming
out before the call to action. Thus on the eighth and ninth days
the strike was extending.But behind the scenes the General Council were "negotiating"
with Sir Herbert Samuel, the chairman of the Samuel Commission who
had recommended drastic cuts in wages, an added hour on the miners'
day and district agreements.Without consulting the miners, the General Council informed
Samuel that the miners would accept a cut in wages. Samuel had
"obligingly" come back from Italy, but had no "official standing"
to negotiate for the government.Yet, with no guarantees that the terms of the miserable
agreement with Samuel — already a capitulation — would
be carried out, the "Lefts" as well as the Rights on the General
Council agreed precipitately and asked to see the Prime Minister,
who accepted "surrender" terms.Why was the General Council prepared to capitulate to the
government when the strike was actually developing and the ranks of
the working class were becoming more solid every day? Every day
that passed there was a hardening of the attitude of the rank and
file, as well as the lower leaders in all the districts in
Britain.J.H. Thomas, the then "leader" of the railmen put it in the
crudest terms: "God help Britain in any challenge to the
Constitution unless the government won."For Thomas, McDonald and the Right wing leaders of the TUC, a
victory of the government and thus of the employers was preferable
to a victory of their own class. The "Lefts" had no alternative to
offer when it was no longer a question of woolly phrases, but of
the concrete reality.The problem of power had been clearly posed. In addition to
that, the organisation of the strike was entirely in the hands of
the lower ranks throughout the country. The General Council in
effect was a passive recipient of the accomplished actions of the
Councils of Action and Strike Committees, whatever they were called
in the localities. Each day that passed saw a strengthening of the
power, initiative and resource of the committees in the
localities.The spectre that haunted the General Council and gave them
sleepless nights was the possibility of their replacement in the
struggle by the lower ranks who would threaten to by-pass them. In
his book on the General Strike, Julian Symons, not at all a
revolutionary, nevertheless was compelled to remark:"They (the General Council) were not rash but
feebly timid; they hoped for the collaboration of their opponents
and never really trusted the mass of their supporters. They
feared the consequences of complete victory more than those of a
negotiated defeat" (p.143) (My emphasis)."The General Council was torn by conflicting
desires. First, it wanted to make the strike effective; second, it
wanted to make certain that control of it did not pass into the
hands of revolutionary agitators." (My emphasis).He quotes Thomas:"What I dreaded about the strike, more than
anything else, was this; if by any chance it should have got out of
the hands of those who would be able to exercise some control,
every sane man knows what would have happened ... That danger, that
fear, was always in our minds, because we wanted, at least, even in
this struggle to direct a disciplined army." (p.52).In their panic to call off the strike, at a time when it was
expanding and growing, the Union leaders did not even put forward
the elementary demand in every strike that there should be no
victimisation and that every worker must be taken back. In his
speech on the radio, Baldwin implacably declared that there were no
"conditions" and that it was unconditional surrender in the strike.
The scabs taken on must have first claims on the jobs. This was a
signal for the employers to try and wreck, weaken or destroy Union
organisation.The rank and file had greeted the decision to call off the
strike with indignation and resentment:"We've been twisted" — Longeaton Trades
Council. "The men were like a rock; the first man to bring the news
was the sub-editor of the County Express and after giving us the
news he asked us what we were going to do about it. The answer was
double the pickets." — Stourbridge. "The strike was
100% strong and could not get any stronger in that respect. But in
organisation it was getting stronger and more efficient every day"
— Blaina. "We were not at our maximum when the strike was
called off — another 12 hours would have a position the like
of which in our wildest moments we have never imagined." —
Coventry. "The ranks of the strikers strengthened daily" —
Goodwick. "Non-Unionists joined up at the end of each (engineering)
shift" — Lincoln. "At high tide promptly and remained so"
— Longeaton. "Everything was going perfectly when the
bombshell fell" — Ramsgate. "The fight was just about to
begin" — Wellingborough. "Every prospect of it growing
stronger .. Bitter feeling among the rank and file at the calling
off" — Woking.They felt themselves betrayed by the leadership. And it was the
local leadership and this rank and file which was to save the
situation from developing into a rout. When they heard of the
conditions being offered by the employers, the railmen, dockers,
engineers and other sections renewed the strike. In fact, two days
after the General Strike had been officially called off there were
100,000 more on strike!The leaders of the different unions then issued instructions to
their members to come out on strike — which they were already
doing — and not to accept any terms in relation to wages and
conditions worse than before the strike. There would be no going
back unless there was no victimisation by the employers and the
government.Faced with the veritable fury of the working class and the
possibility of large-scale clashes in the localities, Baldwin then
came forward in his hypocritical role of a "conciliator". He
broadcast that the employers must take back the workers on the old
terms, and that he would not countenance any attempt to break up
the Unions. The employers consented.The railway unions negotiated an agreement with the railway
leaders that there would be no victimisation, but they refused to
take back any worker who was guilty of "intimidation or violence".
The printers agreed not to hold any more meetings in work time, and
on this concession the printers went back.Thus what had begun as a tremendous movement ended up in defeat,
and was only saved from rout by the solidarity and militancy of the
local leaders and of the rank and file who stood shoulder to
shoulder against the attempts of the employers to weaken the
organisations of the working class.The working class was caught completely by surprise by the
betrayals of the "Left" as well as the Right Wing leaders. This was
especially so with regard to the Communist Party, where at that
time not only the rank and file but also the leadership were
endeavouring to create a revolutionary Party.At that time the leadership were interested in developing a
revolutionary party and preparing for the socialist revolution. Why
then were they disarmed and unprepared by these events? They were
caught by surprise because of the policy of Stalin and the Russian
leaders who dictated the policy of the then Communist
International.The Anglo-Russian Committee, they had been taught, was a
mobilisation of the British and Russian Trade Unions to fight war.
They took at face value the speeches of the "Left" leaders. At
least they were instructed by Moscow to do so and accepted the
policy.In the strike, the rank and file of the Party had naturally been
among the most active sections of the workers. After the
immobilisation of their press, they reacted by bringing out
duplicated papers with a total circulation of over 100,000.But an examination of these papers indicate that they gave no
perspective, either in their speeches or in writing, during the
strike. There was no guidance, no perspective for the struggle
beyond support for the strike and support for the General
Council.From the first day the General Council had issued a statement to
the ranks to "trust the leadership". During the course of the
strike, there was not a single word of warning in all the agitation
and propaganda of the Communist Party. They were caught completely
unprepared and on one foot.Had they been, even in those days, they would have undoubtedly
won over hundreds of thousands of the best workers.People learn far quicker under fire and in the heat of events,
especially the active layers of the working class. But the howl of
the CP of "betrayal" which was entirely unforeseen and unprepared
for came too late to have any effect except to demoralise their own
ranks.They had been taught not to offer any real criticism of the
"Left" leaders, or even of the General Council, whom they saw as
leaders of the struggle. They did not pose a single idea beyond the
winning of the strike until it was too late. Moreover, they had not
prepared in any way for this inevitable turn of events, given the
situation, and the fact that without Marxist perspective and
Marxist understanding the "Left" leaders had no other course to
take, except to join in the betrayal with their Right Wing
colleagues.The working class being caught completely unprepared by the
betrayal from the top, and the CP leadership having failed utterly
to warn against this before and during those great events, it was
the Communist Party and revolutionary methods which were
discredited.Instead of enormous gains which should be inevitable in a period
like that, on the basis of correct policies, strategy and tactics,
the back of the Communist Party was broken. The Minority Movement
disappeared.By 1927 the Trade Union leaders, "Lefts" and Rights, having
contemptuously cast aside the Anglo-Russian Committee, by breaking
off relations, had turned to ‘collaboration’ with the
employers in the Mond-Turner discussions. They could do this
because of the mood of apathy and indifference which pervaded the
Trade Union Movement.Of course we are not suggesting that militants should embark on
the crude and hysterical mud-slinging of the ultra-left sects which
infest the movement. This inevitably merely recoils on the people
who use it.What was necessary in 1926 and is necessary today is a friendly
but implacable criticism of the left leaders in the unions —
and in the Labour Party. A skilful criticism of the woolliness, the
vagueness and inconsistency of the Lefts and their failure to
present the issues in sharp and clear class terms; not to wobble
over the issue of the "nation" or "collaboration with the
management", or even with the Tory government as suggested by
Scanlon and Jones in recent weeks, but to pose the issue clearly of
the "two nations" in Britain — workers and capitalists.In this new epoch of inflation — it is either/or —
either a struggle for the transformation of society, or an
inevitable capitulation to the interests of Big Business. In 1925
to 1926 it was the struggle against deflation.In 1973, the Communist Party leadership is playing an entirely
disastrous role. By not explaining the crisis of British Capitalism
at the present time — and not putting forward a complete
Socialist alternative — they play into the hands of "Left"
and then Right reformism. How could it be different today to 1926
when they themselves have become a caricature variant of Left
Reformism? In 1926 at least they were trying to find the
revolutionary road. The attempted compromise of the union leaders
with the government on the Prices and Incomes Policy is bound to
break down. The cooing of Heath and the glad invitations to the
unions, conceals the knife he is trying to put in their backs. The
Left leaders, because they are not Marxists, do not prepare the
workers for the overthrow of Capitalism but on the contrary try to
act as mediators with the capitalists within the system.That can only be because they do not see that the onslaught on
wages and conditions is not determined by the subjective wishes of
Heath and the government but by the objective position of British
Capitalism on national and world markets.This is caused by the economic crisis of British Capitalism
— low investment and the falling rate of profit which has
been explained in many articles in Militant; this is the
background to the policies of the Left Union leaders. It makes
inevitable the prospect of new storms and battles by the working
class.Even more imperative is the need for uncompromising Marxist
policies and of Marxist analysis for industrial militants.After the collapse of the General Strike of 1926 — too
late — the CP tried to make a change and criticise the role
of the Left union leaders -theoretically and practically.
Palme-Dutt, their leading "theoretician" at the time quoted the
criticism of the left leaders made in 1924! Not a
sentence, not a word could he find in 1925, or 1926, explaining in
theoretical or practical terms the role of the Left in the material
published by the Communist Party.However, it would be instructive to quote the strictures on the
Left leaders when it was too late:In the July 1926 Labour Monthly, Palme-Dutt wrote in his "Notes
of the Month": "The experience of the General Strike has shown that
the question of leadership is a life and death question for the
workers and to neglect it or treat it lightly is fatal ... The
enemy within in fact is most dangerous ... the old reformist myth
that it is only the backwardness of the workers which is the
obstacle to the progressive intentions of the leaders is smashed.
(It is repeated by the CP leaders today — EG). Only a couple
of weeks before the General Strike, Brailsford (ILP leader) in his
answer to Trotsky, was expressing polite incredulity at Trotsky's
statement that the workers in Britain were already in practice far
in advance of the ILP leaders, and holding it up as a glaring
example of Russian "ignorance" of British conditions. After the
General Strike the statement appears as the merest commonplace."
(p.393). On the right wing leaders, he comments (at the 13th hour):
"havingensured its defeat, they come forward to proclaim the final
failure of the .General Strike weapon, and even that they knew its
folly all along. That is the typical role of Social Democracy."On the other hand, it was conspicuously obvious that the
left wing (my emphasis) which had developed as an
opposition tendency in the Trade Unions during the past two years
around the personalities of certain leaders on the General Council
such as Hicks, Bromley, Tillett, Purcell and others, completely
failed to provide any alternative leadership during the crisis and
in practice fell behind the right wing. This is an extremely
significant fact and it is all important."Palme-Dutt =— not sure at that time which way events in
Russia were going to move — even quoted Trotsky, writing
before these great events and warning of the role of the Lefts.
Palthe-Dutt quotes this, alas, when the working class had been
defeated and without honestly criticising the policy of the
Communist Party in the months and years before the General
Strike:"The Trade Union Left Wing was not able to
provide any alternative to the Right wing because the Trade Union
Left wing had not yet in practice reached any basic difference from
the Right. The TU Left wing was the reflection of the important and
growing tendencies and aspirations within the working class, of a
growing opposition to the opportunism and betrayals of the existing
leadership, and the demand for a policy of solidarity and class
struggle."But these tendencies and aspirations were not
translated into any positive programme and proposals, any organised
policy and action, any recognition of the alternative to
opportunism, or any clearly formulated tactics and methods of
struggle. Therefore, the phrases of solidarity and class struggle
inevitably became air without substance; in practice the Left wing
leaders were impotent and therefore became the prisoners of the
Right wing, who at any rate had a positive polish, although a
policy of surrender and co-operation with the bourgeoisie."Palme-Dutt wrote in the Labour Monthly again: "In a recent
article, Trotsky has pointed out that the more revolutionary in
principle a resolution was at Scarborough, the more easily it was
carried; but the closer it came to an even elementary task of
action, the stronger was the opposition. International unity with
the Communist-led Trade Unions of Russia was carried unanimously by
the same delegates who a few weeks later were voting for the
expulsion of Communist trade unionists at home from the Labour
Movement ... But the formation of factory committees, and even that
in principle, was only carried by 2,456,000 to 1,218,000 ... Thus
the move to the left was in practice a show move to the left
reflecting the undoubted movement of working class opinion but
sterilised and neutralised by the skilful opportunist leadership
who allowed no change in policy."In consequence, the sequel of Scarborough by
Liverpool, with its victory of extreme reaction and the conspicuous
collapse of the left wing, was not a contradiction of Scarborough
but its completion." (p.518, August 1926).In an article by Jack Tanner in Labour Monthly, he declared that
the mood amongst Trade Union activists was that "there is a feeling
now that it would have probably been better if they had
‘trussed ’ them! (In reference to the General Council
leaders' appeal to ‘trust us’) ... after the first week
this outlook (merely to fight in defence of the miners) changed
somewhat. They were in the fight and up against all employers and
the state. They began to realise that and were prepared to
continue." (p.420).Too late, after the strike had been called off, the CPGB sent a
telegram to all parts of the country, emphasising the following
points:"1) The General Council, despite previous
promises and of unanimous demands of workers, has ceased the
struggle against lower wages without receiving any kind of
guarantee from government.2) That is treachery, not only in relation to
miners but all workers.3) While the Right wing of the General Council
and Labour Party has exhibited utmost energy, the Left wing has
tolerated defeatist agitation and not protested against this
treacherous decision."Thus, when it was too late, the CP leadership started explaining
the real issues. This is the opposite of the method of Lenin.The changes in the situation since 1926, in objective terms, are
all favourable to the working class. From 5 million then, there are
now 10 million organised in Trade Unions. In the last three years,
millions of workers have been involved in struggle. Skilled,
unskilled, industrial and white collar workers, through their union
organisations, have been on strike against the employers —
and against the government.There is now a temporary lull, while the workers take stock of
the situation. The heavy battalions of the working class, miners,
railmen, dockers and other sections of the industrial workers
understand that conflict with the employers is now also conflict
with the government and the state — serious and heavy
sacrifices would be necessary and the goal must be worthwhile.Consequently, they are preserving a more watchful attitude: thus
the lull in the industrial struggle. But the workers are far
stronger; the capitalists far weaker. Once again rapidly increasing
prices of most necessities and the runaway increase in the price of
food which are gaining momentum, will anger and incense the working
class — consequently in the months ahead there will be
industrial explosions. In that situation there is the possibility
— not at all the certainty — of an all out general
strike — against the Tory government.The government has not got the social reserves they had in 1926.
Students, civil servants and sections of the white collar workers
blacklegged and supported the government. Now they would support
the workers and the unions if it came to a showdown.In the period before the General Strike, only about a
third of the electorate voted Labour. In the post-war period
it has been between 44% and 48%.With entirely new social layers
involved in struggle, the working class is 1,000 times as strong as
in 1926. The magnificent response to the one day strike call is an
indication of the combativity of the working class. The traditions
of 1918 to 1921 have been re-awakened — on a much higher
level.Despite the lukewarmness of some union leaders and the open
hostility of others the movement has gained impetus. The main Civil
Service and Local Government unions have come out in favour,
confirming the new mood of these layers who rarely, and then only
in isolated cases struck in the past. Many will participate in the
May Day marches.Despite the weakness of the Communist Party among the broad
masses — as shown by the Council election results —
they contain within their ranks a sizeable minority of industrial
militants. These desire to work for the Socialist Revolution. But
the leadership, having learned nothing from the experience of Italy
are calling for a succession of one day strikes, leading to a
longer strike and then a general strike.They have not learned anything from the experience of the 1926
General Strike. To their leaders, this is a closed book. Their
programme, industrially and politically is the same as the
programme of the left leaders. Consequently, far from
differentiating, they make strenuous efforts to appear exactly the
same!In none of their literature is the problem of power posed. On a
higher historical level they repeat in a cruder form the mistakes
of the CP in 1925 and 1926. Nowhere have they criticised Scanlon
and Jones for trying to arrive at an agreement with the Heath
government. But this is because they have long ago ceased to be
Marxists, who try to benefit the working class by generalising the
experience of the working class in the past, in order to learn the
lessons and prepare the workers for present and future struggle.
That is the essence of the Marxist method as developed by Marx,
Engels, Lenin and Trotsky.Thus the need above all for industrial militants, is political
clarity. To immerse themselves purely in the industrial struggle is
to prepare terrible defeats.A clear perspective, a clear understanding are necessary if they
are not to move forward blindly to defeat. The elemental movement
of the class will take place in any case. But to guide it to
victory the active minority must clearly understand the problems
posed.In the Militant International Review of January we have
dealt with broad perspectives. We will return to the theme of the
General Strike and the role of leadership in future issues of
Militant and MIR, our theoretical review.Ted Grant
Archive