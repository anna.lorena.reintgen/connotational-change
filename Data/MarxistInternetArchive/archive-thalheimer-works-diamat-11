 So far
we have discussed the most general and most

fundamental law of dialectics, namely, the law of 
the
permeation of opposites, or the law of polar unity.
We
shall now take up the second main proposition of dialectics, the law of the
negation of the negation, or the law of development through opposites. This is
the most general law of the process of thought. I will first state the law
itself and support it with examples, and then I will show on what it is based
and how it is related to the first law of the permeation of
opposites. There is already a presentiment of this law in the
oldest Chinese philosophy, in the 
of Transformations, as well as in
Lao-tse and his disciples - and likewise in the oldest Greek philosophy, especially in Heraclitus. Not until Hegel, however, was this law developed.This law
applies to all motion and changes of things, to real things as well as to their
images in our minds, i.e., concepts. It states first of all that things and concepts
move, change, and develop; all things are processes. All fixity of individual things is only relative, limited; their
motion, change, or development is absolute, unlimited. For the world as a
whole absolute motion and absolute rest coincide. The proof of this part of the
proposition, namely, that all things are in flux, we have already given in our
discussion of Heraclitus.The law
of the negation of the negation has a special sense beyond the mere
proposition that all things are processes and change. It also states something
about the most general form of these changes, motions, or developments. It
states, in the first place, that all motion, development, or change, takes
place through opposites or contradictions, or through the negation of a thing.Conceptually
the actual movement of things appears as a negation. In other words, negation
is the most general way in which motion or change of things is represented in
the mind. This is the first stage of this process. The negation of a thing from
which the change proceeds, however, is in turn subject to the law of the
transformation of things into their opposites. The negation is itself negated.
Thus we speak of the negation of the negation.The
negation of negation logically results in something positive, in thought as
well as in reality. Negation and affirmation are
polar concepts. Negation of the affirmation results in negation; negation of
the negation equals affirmation. If I negate yes, I get no, the first negation.
If I negate no, I get yes, the second negation. The result is something
positive.Thus
even in ordinary speech an affirmation results
from a
double negation. However, and this is the distinctive feature, the old and the
original are not re-established by the double negation in dialectics; it is not
simply a return to the starting point, but something new arises. The thing or
the condition with which the process started is re-established on a higher
plane. Through the process of double negation new qualities and a new form
emerge, a form in which the original qualities are retained and enhanced.If the
expression, law of the negation of the negation, sounds strange to you, you
may also use the expression, law of the creation of the new out of the old,
which is quite simple. This law has also been given a special formulation as a
law of thought. As such it assumes the following form: the starting point is
the positive proposition or the thesis. All thinking starts with some kind of
proposition, some kind of statement. This proposition is negated or transformed
into its opposite. This new proposition, which negates the first, I call the
opposite or antithesis. This is the second stage. Then this second proposition,
the antithesis, is again negated, and, as you know, we then(get the third proposition
or synthesis, the negation of thesis and antithesis into a higher positive
proposition effected by the further negation.To
understand this law correctly one must guard against two misinterpretations or
distortions.Thesis
and antithesis are dialectically united in the final proposition, the
synthesis. The dialectical union must not be mistaken for the
mere summation of those qualities of two opposite
things which remain after mutually exclusive qualities are cancelled.
Dialectical development does not occur this way; this would simply be a
mixture or effacement of opposites, a hindrance to dialectical development. It
is a necessary characteristic of dialectical development that it fulfill itself
through negations. Without negation there is no process, no development, no
emergence of the new. In society this negation is expressed in struggle which
abolishes the old. False dialectics or pseudo-dialectics says that a mutual
understanding, a compromise is attempted between the old and the new, that an
attempt is made to unite the old and the new, without rejecting the old (I do not
wish to imply, of course. that all compromise is a negation of struggle. A
compromise may even be a weapon of struggle.)This
misunderstanding of the dialectics of development is due to the fact that the
role of negation as an essential factor in unification is forgotten. But there
is also an opposite misunderstanding arising from a 
disregard of the fact that the new which emerges from the process of
development not only negates or neutralizes the old, but also retains the old.
If this is ignored, the dialectics of development is distorted, as,
for example, in the case of the French philosopher, |Henri Bergson.
With Bergson development becomes an incomprehensible, mystical process in which
the relations between the old and the new are conceived of only as oppositions
and not, at the same time, as identities.The
fundamental error in Bergson's conception of dialectics is his disregard of the
fact that the new which has developed from the old, stands not only in
opposition to the old, is not only its negation, but has, at the same time,
something in common with the old. If one follows the thought of Bergson, it
becomes evident that it cancels itself. There is only one kind of negation in
which the thing negated has nothing more to do with that from which the
development proceeded. This is complete or unconditioned negation or
destruction. If I completely negate a thing, I destroy it, and development is
completely stopped. If development is forced beyond its limits, as it is with
Bergson, if it is made absolute, it is transformed into its opposite, into
fixity or lack of development. Negation in the dialectical process is not
absolute, unconditioned, or complete; it is relative, conditioned, and
partial. Dialectics concerns itself with definite, concrete negation. The first
distortion of dialectics, the distortion which disregards negation, may be
called the opportunistic distortion. The second in which the retention 
of the old in the new is disregarded may be called the
anarchistic distortion. These two opposed distortions of dialectics, the
opportunistic as well as the anarchistic, are alike in that both put an end to
development - the first because it puts an end to negation as the moving force
of development, and the second because it puts an end to the connection
between opposites.To make
this general abstract law a little clearer, I will give a few examples. Take a
grain of rice. Suppose I am giving you the problem of starting a developmental
process with this grain of rice. How will you do it? You will put the grain of rice into the earth or into water according to
whether you wish to plant wet or dry rice. What will happen? The first negation
of the grain of rice occurs. The grain dissolves and out of it a rice plant
develops. First negation: the grain of rice dissolves and changes into a
plant. The original grain of rice is thereby destroyed. The second act proceeds
of itself. The rice plant grows and finally develops rice grains; and as soon
as it reaches the point where it produces seeds, grains of rice, it perishes.
Second negation: The rice plant is destroyed and the grain is re-established,
not the old but new grain, not one but
many, and in all probability not of the old quality but with new characteristics.
These slight variations connected with reproduction
are, as a rule, inconsiderable and inconstant. However, their accumulation and
consolidation result, according to the Darwinian theory, in the formation of
new species from old. This process is an example of the negation of the
negation. The double negation re-establishes the original state but on a higher
plane and in a different quantity. The Bergsonian distortion of dialectics, it
is obvious, is very closely connected with the present historical position of
the bourgeoisie. The mystical or falsified dialectics of the Bergsonian type
rejects historical regularity and replaces it by miracle, arbitrariness, and
incomprehensibility whereby nothing is impossible.I will
illustrate the two misunderstandings or distortions of dialectics of which I
have spoken by using the same example. The first distortion, the Bergsonian, or
as one could say, the anarchistic distortion, may be illustrated thus: the law of
dialectics demands that I negate the grain of rice. This can be done more thoroughly,
it might be said. Instead of planting it in the earth, I can put it into a
mortar and break it to pieces. As a consequence its negation will be so
thorough that further development becomes impossible. This is the first
distortion. It is apparent from this that for each thing there is a particular
kind of negation which initiates a developmental process, a negation
appropriate to the nature of the thing.The
second or opportunistic distortion of dialectics occurs when negation is
ignored. The person to whom I give the grain of rice may say that it can
develop "of itself." He neither crushes it nor puts it into the
around. He will let it lie on the table. And, of course, it will not develop
into a plant. It will finally perish as an organism. This illustrates,
incidentally, how these two opposed distortions of dialectics have the same
result. No development occurs and the object is destroyed. On the other hand,
if I negate purposively and initiate a developmental process, the thing will
be destroyed and, at the same time, it will develop into something new and
higher. I will give you a second example drawn from the history of social or
economic forms. You know that the very first mode of production of which we
have any knowledge was primitive communism, i.e., the collective ownership of
the decisive means of production by a small community of people. This primitive
communism is the point of departure of all social development; it represents
the thesis, the proposition. Primitive communism was dissolved, negated. In the
place of collective ownership of the means of production and collective
production, there appeared private production, slave economy, feudalistic
production, simple commodity production, and finally capitalistic production.
This is the antithesis. The negation of primitive
communism is private production in its various historical forms. Then comes the
third stage: the negation, in its turn, of private production, the
re-establishment of collective property, of communism on a higher plane.
Through this twofold negation the development returns to its starting point,
but on a higher plane. Socialist or communist production, as it emerges from
capitalist production, is no longer primitive communism, but communism at a
much more developed stage since it retains the technical achievements of
capitalism. Man is now in control of nature, whereas in the stage of primitive
communism it controlled him. And the compass of modern communist society is
vastly greater than primitive communism. At most, primitive communism could
bring just a few communities together into an economic unit, whereas modern
socialism or communism is capable of embracing the entire world economy. I
have just emphasized the extent to which modern communism differs from
primitive communism. Nevertheless, primitive communism is retained in modern
communism. Common ownership of the means of production is re-established.
Capitalism is negated, dissolved into communism. But this negation is not
absolute or abstract; it is relative, concrete, conditioned. Capitalist
technology as well as co-operation in the factory are retained. Finally, I wish
to illustrate the two distortions of dialectics in terms of this same example.
The first distortion, which disregards the necessity of dissolving or negating
capitalism in order to attain socialism, is the well-known reformist or
opportunistic conception The second distortion of dialectics in this field,
the distortion which overlooks the fact that elements of capitalism are taken
over for the construction of socialism, is the conception of anarchists. For
these reasons I have called the first the opportunistic and the second the
anarchistic distortion of dialectics. History shows that these two distortions
alternate and replace each other.We now
ask, where does the law of the negation of the negation come from? What is its
relation to the first main proposition of the permeation of opposites?
Obviously, it is related directly to the law of the permeation of opposites.
It is the permeation of opposites as a process, a process in time, in sequence.
The permeation of opposites as a process results in the law of the negation of
the negation or the law of development through opposites. The first main
proposition, the law of the permeation of opposites, represents the most
general relations of things from the point of view of structure or static
being. The second proposition of the negation of the negation represents the
relation of things as a process, i.e., dynamically. These two propositions are
so related that they hold true for every process, for everything at the same
time and to the same extent. The two propositions permeate each other; they
form a coherent whole. The first gives a
cross-section of the world, the second a longitudinal section.We now
come to the third main proposition of dialectics, the proposition of the
transformation of quality into quantity and of quantity into quality. The
proposition states that the mere augmentation of a thing or things produces a
change of quality, of characteristics, and, conversely, that a qualitative
change produces a quantitative one.I should
like to illustrate this with a few examples. Let us take the first one from
physics - water. Water has a definite temperature and if you raise the temperature
to a certain point you will not get hotter and hotter water, but at a certain
point you will get steam. And, likewise, if you lower the temperature, the
water does not become colder indefinitely; at a certain point it becomes ice.
It freezes because of the decreased quantity of molecular motion. The
temperature is merely an expression of the motion of the smallest particles,
the molecules. If you change the quantity of the molecular motion or the speed
with which the molecules move about, the characteristics will change at certain
points, from gas to liquid, liquid to solid. Conversely, ice can only be
changed to water or water to steam if the quantity of molecular motion is
changed. The finest example of the law of transformation of quantity into
quality is now being given by atomic research. The various qualities of the
atoms of chemical elements are correlated with the simple numerical relations
of their components of the next lowest order, the electrons.An
additional example from zoology and botany: you know that all plants and
animals are composed, in the last analysis, of small elementary units, of
cells. Every living being develops from one or several small cells. All
differences of living creatures derive from different quantities of cells. If I
increase the cells, other organisms emerge with different characteristics and
forms.Then
there is the reverse process: it is possible to take a certain number of cells
away from an organism without harming it. It will remain the same. But as soon
as this is continued beyond a certain point, the organism is harmed. If a man's
hair is cut off, he does not suffer, but if his arm or leg is cut off, he will
undergo a qualitative change. In fact, he will probably die. You may draw a
certain amount of blood from a person, but beyond a certain point, death will
result - a qualitative change.A last
example from political economy: you have learned in political economy that a sum
of money can only function as capital after it has reached a certain minimum
amount. One dollar, for example, is not capital, and neither are ten dollars;
10,000 dollars, however, may function under certain conditions as
capital. A mere change in quantity changes a sum of money into capital; it
takes on different characteristics, produces a different effect - a qualitative
change occurs. If capital is allowed to grow through concentration and
centralization a new qualitative change takes place, namely, a change to monopoly capital.
You know from your political economy that monopoly capital has characterized
an entire period of capitalist development, i.e., the imperialist period. And,
on the other hand, as soon as you have monopolistic capitalism, this new
quality is in turn transformed into quantitative relations and characteristics.
Monopoly capital realizes a higher rate of profit than non-monopoly capital.
Monopoly prices are generally higher than prices under free competition, etc.We ask,
finally, what relation obtains between this third proposition of dialectics and
the first two. And the answer is clearly that the law of the transformation of
quantity into quality and vice versa merely represents a special application of
the first proposition, the law of the permeation of opposites. Quality and
quantity are polar opposites. Quality is quantity analyzed; quantity is quality
analyzed. An apple, a pear, and a plum all have different qualities. They can
only be counted together if their different qualities are abstracted from them
or negated. I cannot add an apple, a pear, and a plum; I can only say: three
pieces of fruit. In other words, negated quality is quantity; negated quantity
is quality. These opposites are contained in each thing. Each thing has a
definite size, quantity, or degree, and at the same time definite characteristics.
All things have, at the same time, quality and quantity. As opposites they
permeate each other and are transformed into each other.This
brings me to the end of dialectics. Of course, 
it 
must not be assumed that knowledge of these few propositions makes one
completely familiar with dialectics. A long list of other propositions, which
w cannot discuss here, derive from these few main propositions. Nor must it be
assumed that the mere memorization of formulas is the key to dialectics. The
important thing is to be conscious of the dialectical nature of things and of
thought. Dialectical thinking is not magic. Neither is it part of everyone's
natural equipment. It is an art which must be learned and practised. The most
general characteristic of dialectical
thought is the study of things in their interrelations, in both
one-beside-the-other relations and one-after-the-other
relations, - that is, in their changes.
Contents  | 
Chapter 10 - Dialectics I  | 
Chapter 12 - Theory of History and Dialectical Materialism I

Thalheimer Archive
