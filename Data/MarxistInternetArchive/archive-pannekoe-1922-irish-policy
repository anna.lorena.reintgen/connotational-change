Anton Pannekoek 1922
Source: Letter to Sylvia Pankhurst in Workers’ Dreadnought, 30 September 1922.
Transcribed: by Adam Buick.Dear ComradeI have read with much satisfaction your article on the programme
of the Irish Communist Party, and I think you are perfectly right in calling it
a non-Communist programme. Indeed, the essence of Communist thought is that the
great transformation of society from Capitalism to Communism can only be
accomplished by the common efforts of the workers themselves, all of them acting
where they stand in the process of production.The belief that some foreign power, the State, may accomplish it for the
workers by decrees and laws is a social-democratic belief — nay, only the
most narrow-minded social democrats believe it; most social democrats in former
times knew quite well that the chief force of transformation must come from
below.The state is not a supernatural being; it is the organised host of
politicians, leaders and officials backed by armed force. The belief that the
State may establish Communism by legislative means is the belief that this small
host of officials and leaders, by their wisdom, may save the mass of the workers
from slavery — these workers having nothing to do but vote for them. Now
the experience of Germany has proved that placing Labour leaders at the head of
the State is simply a change of rulers, which cannot bring any real
revolution.On the other hand, Russia in the first years of the Revolution showed that
after the workers had already seized the power in the workshops, in the Army,
and on the land, by their committees, the revolution could be accomplished by
seizing the State power — i.e., all this activity was centralised, united,
and organised by central organs, and made a strong united body against attacks
from the Capitalist side.The programme of the Communist Party of Ireland is not only non-Communist
because it appeals to the State for everything, but also because it asks from
this State only reforms. It would have been, though not Communist in its means
and ways, nevertheless Communist in its aims, if it had constituted measures for
abolishing Capitalist exploitation and introducing Communist ownership. But even
this it doesn’t do. It supposes a State Power ruled by the workers —
for awaiting these measures from a State ruled by Capitalists would be pure
nonsense — while private enterprise still dominates the economic field;
but it does not make use of this State Power to attack and destroy private
enterprise, but only to reform it to somewhat less intolerable conditions for
the workers. The model of this programme probably must be sought for in the
Russian conditions, where the Communist Party tries to keep its political
domination at the same time that it must allow Capitalistic enterprises to come
on. But also in our own West European conditions we may find the roots for it.
It tries to combine the interests of the working class for reforms with the
interests of the petty bourgeoisie; by the State ownership of banks, railways,
and big industries, it promises to free petty enterprises from the crushing
domination of big finance and heavy industry. That is the reason why it does not
proclaim the abolition of private property: it desires to eat from two cakes; at
the same time, it does not attempt to win solely the workers by the great ideal
of Communism and revolution, to which at this moment the great mass is
indifferent, which thus exact great pains and long efforts. It also attempts to
win the petty bourgeois class and also the middle-class minded mass of the
workers. It attempts to win both these classes within a short time, not raising
their mind to the higher standing of the great Communist prospects, by
vanquishing their bourgeois narrow-mindedness, but baiting them with the
programme of a reformed petty capitalistic world, wholly in line with their
inherited thoughts.It is nothing else than the “New Zealand Socialism” of twenty
years ago, invented by bourgeois reformers wanting the aid of the small working
class against foreign finance, and resulting in strangling the class struggle
and the freedom of movement of the workers.In Ireland it has its roots in the economic backwardness of the country, with
its small proletariat, its great mass of petty bourgeoisie, its great mass of
petty bourgeoisie, its great mass of small land holders and labourers who hope
to become petty-owners. It tries to give them a common programme, which, of
course, cannot be Communist.Perhaps it may be said that, as Communism is not yet possible in such a
country, this programme of a reformed society of petty enterprise controlled by
the working class is to be preferred to everything else, and the best possible
way out. But the idea of a stable society on this basis of peaceful co-operation
of classes is an illusion. You have already shown it in your article with regard
to workers’ control.The same impossibility may be seen regarding unemployment. “Full
maintenance for the unemployed at full trade union rates” is asked for.
Where would the State get the funds necessary under this programme? The funds
must in some way come from production; either from the profit on State
industries, or from taxes paid by small enterprise. Of course these capitalists
would not be content to pay to the unemployed such rates; they would try to
lower them, in order to restore the pressure of unemployment on the wages. Here
arises the natural and fundamental enmity of the classes, the chief opposition
of their interests, the impossibility of peacefully combining their efforts. As
long as private enterprise exists, it must try to hold itself against
competition by lowering the cost of production, or else be ruined. It cannot be
content to secure a fixed living to the workers.In 1848, in Paris, this payment of unemployed was the chief cause of the
shopkeepers and other petty bourgeois becoming furious against the “do
nothings” and crushing the proletarian revolt in the June massacre. But
also from the Communist point of view this leaving the workers unemployed and
paying them a life rate is not right. Communism means production of an abundance
of goods, leaving people idle who are desirous to work is spoiling the resources
of the community. A Communist society will not leave them unemployed, but will
let them produce goods for the community, thus for themselves and others to
increase the general wealth.Thus the so-called Communist programme is not the programme of Communists
desirous to show to the workers the difficult but only real way to freedom; it
is the programme of politicians desirous to win the great mass of adherents from
various poor classes, by a programme of reforms that means coalition of workers,
small farmers and petty bourgeois.What you say about the results of the coalition in the States of Eastern and
Middle Europe shows that this coalition uses the force of the proletariat to
promote the formation of a numerous class of small land owners, extremely
hostile to any Communism, thus it throws obstacles in the way to Communism. It
does still more so by filling the minds of the workers with illusions, and by
diverting their eyes from the only way to freedom; the way of class struggle,
clear class-consciousness and confidence in their own power.Yours very truly,
Anton Pannekoek 
Pannekoek Archive
