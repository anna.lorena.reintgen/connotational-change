
Karl Marx
Wage Labour and Capital
 
If several workmen were to be asked: "How much wages
do you get?", one would reply, "I get two shillings a day", and so on.
According to the different branches of industry in which they are employed,
they would mention different sums of money that they receive from their
respective employers for the completion of a certain task; for example,
for weaving a yard of linen, or for setting a page of type. Despite the
variety of their statements, they would all agree upon one point: that
wages are the amount of money which the capitalist pays for a certain period
of work or for a certain amount of work.

Consequently, it appears that the capitalist buys their labour
with money, and that for money they sell him their labour. But this is merely
an illusion. What they actually sell to the capitalist for money is their
labour-power. This labour-power the capitalist buys for a day, a week, a
month, etc. And after he has bought it, he uses it up by letting the worker
labour during the stipulated time. With the same amount of money with which
the capitalist has bought their labour-power (for example, with two shillings)
he could have bought a certain amount of sugar or of any other commodity.
The two shillings with which he bought 20 pounds of sugar is the price
of the 20 pounds of sugar. The two shillings with which he bought 12 hours'
use of labour-power, is the price of 12 hours' labour. Labour-power, then,
is a commodity, no more, no less so than is the sugar. The first is measured
by the clock, the other by the scales.

Their commodity, labour-power, the workers exchange for the commodity
of the capitalist, for money, and, moreover, this exchange takes place
at a certain ratio. So much money for so long a use of labour-power. For
12 hours' weaving, two shillings. And these two shillings, do they not
represent all the other commodities which I can buy for two shillings?
Therefore, actually, the worker has exchanged his commodity, labour-power,
for commodities of all kinds, and, moreover, at a certain ratio. By giving
him two shillings, the capitalist has given him so much meat, so much clothing,
so much wood, light, etc., in exchange for his day's work. The two shillings
therefore express the relation in which labour-power is exchanged for other
commodities, the exchange-value of labour-power.

The exchange value of a commodity estimated in money is called
its price. Wages therefore are only a special name for the price of labour-power,
and are usually called the price of labour; it is the special name for the
price of this peculiar commodity, which has no other repository than human
flesh and blood.

Let us take any worker; for example, a weaver. The capitalist
supplies him with the loom and yarn. The weaver applies himself to work,
and the yarn is turned into cloth. The capitalist takes possession of the
cloth and sells it for 20 shillings, for example. Now are the wages of
the weaver a share of the cloth, of the 20 shillings, of the product of
the work? By no means. Long before the cloth is sold, perhaps long before
it is fully woven, the weaver has received his wages. The capitalist, then,
does not pay his wages out of the money which he will obtain from the cloth,
but out of money already on hand. Just as little as loom and yarn are the
product of the weaver to whom they are supplied by the employer, just so
little are the commodities which he receives in exchange for his commodity
– labour-power – his product. It is possible that the employer found no
purchasers at all for the cloth. It is possible that he did not get even
the amount of the wages by its sale. It is possible that he sells it very
profitably in proportion to the weaver's wages. But all that does not concern
the weaver. With a part of his existing wealth, of his capital, the capitalist
buys the labour-power of the weaver in exactly the same manner as, with
another part of his wealth, he has bought the raw material – the yarn
– and the instrument of labour – the loom. After he has made these purchases,
and among them belongs the labour-power necessary to the production of the
cloth he produces only with raw materials and instruments of labour belonging
to him. For our good weaver, too, is one of the instruments of labour, and
being in this respect on a par with the loom, he has no more share in the
product (the cloth), or in the price of the product, than the loom itself
has.

Wages, therefore, are not a share of the worker in the commodities
produced by himself. Wages are that part of already existing commodities
with which the capitalist buys a certain amount of productive labour-power.

Consequently, labour-power is a commodity which its possessor,
the wage-worker, sells to the capitalist. Why does he sell it? It is in
order to live.

But the putting of labour-power into action – i.e., the work –
is the active expression of the labourer's own life. And this life activity
he sells to another person in order to secure the necessary means of life.
His life-activity, therefore, is but a means of securing his own existence.
He works that he may keep alive. He does not count the labour itself as
a part of his life; it is rather a sacrifice of his life. It is a commodity
that he has auctioned off to another. The product of his activity, therefore,
is not the aim of his activity. What he produces for himself is not the
silk that he weaves, not the gold that he draws up the mining shaft, not
the palace that he builds. What he produces for himself is wages; and
the silk, the gold, and the palace are resolved for him into a certain
quantity of necessaries of life, perhaps into a cotton jacket, into copper
coins, and into a basement dwelling. And the labourer who for 12 hours long,
weaves, spins, bores, turns, builds, shovels, breaks stone, carries hods,
and so on – is this 12 hours' weaving, spinning, boring, turning, building,
shovelling, stone-breaking, regarded by him as a manifestation of life,
as life? Quite the contrary. Life for him begins where this activity ceases,
at the table, at the tavern, in bed. The 12 hours' work, on the other hand,
has no meaning for him as weaving, spinning, boring, and so on, but only
as earnings, which enable him to sit down at a table, to take his seat
in the tavern, and to lie down in a bed. If the silk-worm's object in spinning
were to prolong its existence as caterpillar, it would be a perfect example
of a wage-worker.

Labour-power was not always a commodity (merchandise). Labour was
not always wage-labour, i.e., free labour. The slave did not sell his labour-power
to the slave-owner, any more than the ox sells his labour to the farmer.
The slave, together with his labour-power, was sold to his owner once for
all. He is a commodity that can pass from the hand of one owner to that
of another. He himself is a commodity, but his labour-power is not his commodity.
The serf sells only a portion of his labour-power.
It is not he who receives wages from the owner of the land; it is rather
the owner of the land who receives a tribute from him. The serf belongs
to the soil, and to the lord of the soil he brings its fruit. The free
labourer, on the other hand, sells his very self, and that by fractions.
He auctions off eight, 10, 12, 15 hours of his life, one day like the next,
to the highest bidder, to the owner of raw materials, tools, and the means
of life – i.e., to the capitalist. The labourer belongs neither to an owner
nor to the soil, but eight, 10, 12, 15 hours of his daily life belong to
whomsoever buys them. The worker leaves the capitalist, to whom he has
sold himself, as often as he chooses, and the capitalist discharges him
as often as he sees fit, as soon as he no longer gets any use, or not the
required use, out of him. But the worker, whose only source of income is
the sale of his labour-power, cannot leave the whole class of buyers, i.e.,
the capitalist class, unless he gives up his own existence. He does not
belong to this or that capitalist, but to the capitalist class; and it
is for him to find his man – i.e., to find a buyer in this capitalist
class.

Before entering more closely upon the relation of capital to wage-labour,
we shall present briefly the most general conditions which come into consideration
in the determination of wages.

Wages, as we have seen, are the price of a certain commodity,
labour-power. Wages, therefore, are determined by the same laws that determine
the price of every other commodity. The question then is, How is the price
of a commodity determined?

By what is the price of a commodity determined?

Wage Labour and Capital Index
