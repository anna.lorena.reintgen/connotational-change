Baruch Hirson 1990
Source: Revolutionary History.Frank Glass and W H (Bill) Andrews were close friends, both socially
and politically. It is not certain when they first became intimate, but they certainly met
at the founding conference of the Communist Party of South Africa (CPSA) in 1921, where
Glass was the youngest delegate. They occupied leading positions in the party and were in
constant contact with one another. In 1923/4 Andrews was party secretary, Glass the
treasurer and full-time organizer. Both worked in the white trade unions and were the
protagonists inside the CPSA for affiliation to the South African Labour Party (SALP). In
1923 the CPSA, reduced in size after the suppression of the general strike of 1922,
decided at its annual conference to apply for affiliation to the SALP, only to have its
overture rejected.The isolation of the CPSA worsened and, seeking a new initiative,
members of the party's Youth League favoured a radical change in policy. Grouped around
Eddie Roux and Willie Kalk they urged party members to seek recruits among black youth and
black workers. Some members opposed this policy, others accepted it enthusiastically.In mid-1924 conditions changed in South Africa. General Smuts was
defeated in a general election by an alliance of the National and the Labour Parties. This
had CPSA support, but the party condemned the entry of Labour into the government.Nonetheless, Glass and Andrews moved that the party apply for
affiliation to the SALP, for the second time. Their approach arose, at least partly, from
a letter written from a sanatorium in Yalta by David Ivon Jones, in mid-1924. Jones was
one of the acknowledged founders of the CPSA, and this was the last letter he sent before
his untimely death. The letter was sent to Andrews but was obviously meant for the leading
members of the CPSA. Glass had a clear memory in 1986 of Andrews having read the letter to
him soon after it was received.Jones had said that the CPSA had been reduced to a sect and should be
dissolved temporarily. Communists should regain their position among workers
through the trade unions and their one hope organizationally was to seek affiliation with
the SALP. Yet, despite the segregatory policy of the SALP, it would be wrong to read into
this a racist approach on the part of Jones. Jones had worked through the problems of
ethnicity and class and, like many others, had made mistakes in his evaluation of the
white workers as a revolutionary force. However, when he was tried for publishing and
distributing a leaflet entitled 'The Bolsheviks are Coming' in Pietermaritzburg in 1919,
he had maintained in court that the future Lenin of South Africa would be an African.
Later, in 1921, when he spoke at the congress of the Communist International (or
Comintern), he had been the first to call for the convening of a Congress of Negro
Toilers. Furthermore, in the above mentioned letter in which he urged the CPSA to seek
affiliation with the SALP he already perceived in Roux the kind of young party member who
could best represent the interests of communism in South Africa.Andrews did not intervene actively in the debate at the party
conference in 1924 and it was left to Glass to lead the debate. He spoke about winning
white workers but noted that the party had to find its way to the black and the white
masses. He said that through the SALP there would be access to Coloured and African voters
(in the Cape) - and there is no indication that he cast any aspersions at the black worker
as stated subsequently by communist historians. In fact, an unknown informer at the
conference, who reported to the Department of Justice, ascribed remarks with a possible
racist slant to W H Andrews!This time the resolution on affiliation was opposed by S P Bunting,
Roux and Kalk, and was narrowly defeated. Andrews and Glass resigned from the executive
committee of the party and withdrew from the Central Executive in February 1925. However,
according to Roux, Glass left the party immediately after the conference, and then made a
statement during an interview to the Star that Africans could not appreciate the
noble ideas of communism. I searched through the files of the Star but could find no such
interview. However, there was a letter signed by Roux, as General Secretary of the CPSA,
on 4 March 1925, written in response to press reports. He said that neither Andrews nor
Glass had left the party, but Glass had resigned as treasurer because of pressure of trade
union work.In fact, after the December conference many members of the CPSA drifted
away and were not heard of again. On 9 May Frank Glass wrote to the secretary of the CPSA.
He claimed that membership of the party had dwindled and that 'the antagonism of the white
worker has increased'. He continued:
Today the Communist Party in South Africa is a sect - nothing more, and is regarded by
  the average European worker as an anti-white party with some justification. For the
  tactics (or antics) of the more prominent Party members display a distinct bias against
  the whites in favour of the blacks, and all the propaganda of the Party appears to be
  directed towards 'getting the backs up' of the former.This, he said, was harmful to the general progress of the Labour
Movement in South Africa'. He resigned from the CPSA and joined the SALPNonetheless, it was a mistake to have adopted a policy of entryism in
the SALP in South Africa. Jones, Bunting, Andrews and others in the CPSA had been members
of the executive of the SALP before the First World War but resigned in 1915 because they
opposed the party's pro-war stance. They were fully aware of the vigorous segregation
policy of the SALP and had been among the first to reject colour or ethnicity as a
criterion for membership of any movement. Now that the SALP was part of government, there
could be no place for communists in its ranks.Andrews stayed in the CPSA after the 1924 conference but, despite
having been a member of the Executive Committee of the Communist International (ECCI) in
1923, editor of the party paper, the International, and a leading party figure in the
strike of 1922, played no further active role in the CPSA until the late 1930s.Glass was secretary of the Witwatersrand Tailors' Association and
together with Andrews was prominent in the founding conference of the South African
Association of Employees' Organizations (SAAEO), the newly launched trade union
federation. Andrews was elected secretary and Glass treasurer, amidst claims by the press
that the communists had captured control of the trade union movement. Present at the
founding conference of the in 1925 was Fanny Klenerman, organizer with the assistance of
Eva Green, of the women sweet workers and waitresses. Fanny was militant and, if she had
not met Glass before would have been noticed by him now. Green was a teacher and under the
terms of her employment could not work openly in a political party, or in a trade union.Glass and Klenerman, together with Andrews and Green, were socially
inseparable. In this they crossed party affiliation lines, and the CPSA took no action
against Andrews. The narrow sectarianism of the Comintern, which helped to ruin so many
promising political cadres in Europe had not yet crippled the South African CP. Andrews
and Glass were brought even closer by their work in the trade unions. It was probably in
this capacity that they both attended meetings of the black general workers union, the
Industrial and Commercial Workers Union of Africa (ICU), and spoke from its platform in
1925 and 1927. They did this as trade unionists, and yet their appearance at ICU meetings
after 1926 flew in the face of ICU stated policy.The ICU, which seemed to be growing into a mighty movement by 1925 was
actually in poor shape. After its initial involvement in a dock strike in 1919 the
movement was not very successful as a trade union. Its main branches were in small towns
where there was little manufacture and its leaders stayed away from industrial action.
Many of its officers were corrupt and the money collected from workers went into private
pockets. Alcoholism was rife, and at least one top official became a police
informer.Although the ICU claimed to be a general worker's union, it was a
community organization. Men were organized in the townships and the 'union' could do
little more than represent its members legally when they encountered problems. This was a
useful but costly service that could not be sustained. The decline of the ICU was probably
inevitable but was speeded up in 1926 when members of the ICU, who were also members of
the Communist Party of South Africa (CPSA), were confronted by an ultimatum to
resign from the CPSA or leave the ICU.It seems that the ultimatum was precipitated by the report prepared in
1925 by the assistant general-secretary of the ICU and member of the CPSA, James La Guma.
In this he accused the top leadership of corruption. The response from that leadership was
ill-considered but inevitable. All communists had to go.It therefore came as a surprise when Andrews and Glass spoke from ICU
platforms. Andrews was still in the party and Glass's views were even more radical than
that of his friend. Glass was first asked by Clemens Kadalie, the ICU leader, to be the
movement's book-keeper and produce accounts in accordance with new government regulations.
Kadalie also stated that he wanted Glass to be appointed treasurer of the ICU but this was
blocked by the ICU council.Both Glass and Andrews addressed ICU meetings including one in 1925 on
the subject of the British seamen's strike, 5 and one on 28 March 1927 in Johannesburg,
when a meeting was called to protest against the passing of the first reading of the
Native Administration Bill. This legislation was designed to move control of African
affairs to the Native Affairs Department, and contained measures that could cripple all
black organization at the behest of the Minister of Native Affairs.About 2,000 Africans and a small group of whites, Indians, and Chinese
attended. Both Andrews and Glass spoke and both speeches were reported in the morning and
evening press. It was the address given by Glass that received the main headlines and the
main strictures, although both speakers were condemned in the press and in Parliament.Andrews spoke in his personal capacity, and his words were those of a
radical trade unionist. He advised African workers to organize, taking no account of
colour, religion or the politics of members. He continued:
I say to you, and all the workers of South Africa, whether European,
  Native or any other nationality, that they have got to organize along the lines of
  industry, irrespective of creed, colour or politics, and if you do that and the European
  workers as well - you will not only be able to stop this bill, but you will be able to
  raise yourselves indefinitely higher than you have ever been before. Build up your
  organisation, irrespective of prejudices, so as to take possession of this country - I am
  now speaking to all workers, white, black and coloured - as the Russians have of their
  country and as the Chinese are endeavouring to do - and for the first lime in history you
  will be able to enjoy the fruits of your labour.Andrews' talk drew applause, but it was Glass who got the audience to
their feet with his call for revolutionary action.
If you will do what the Russian workers have done and what the Chinese workers are
  doing now you - all the workers of this country, black and white - will be able to secure
  freedom. We don't know at the moment how far the Government is going in its attempt to
  restrict the freedom of the Native workers; but this we do know, that all capitalist
  governments in their dealings with the workers act precisely alike. Therefore we have got
  to be prepared, not merely with demonstrations, but also - if it proves to be necessary -
  with far more drastic action.Glass was stopped at this point by the police who took his name and
claimed that his address was potentially illegal. There was a stormy response when the
matter was raised the next day in the South African parliament with demands that Glass be
prosecuted and that his activities be curtailed. The matter was not acted upon and there
was no prosecution. Andrews was coupled with Glass and also condemned by General Hertzog,
the Prime Minister. The speech was a turning point for Glass, but there was one more
incident before he quitted the white trade union movement and the South African Labour
Party.In 1926 Glass acted with Andrews when he sued Matthews of the
Amalgamated Engineers Union for defamation after it had been asserted that he had
manipulated the balance sheet of the SAAEO to ensure the payment of Andrew's salary. The
court found for Glass, and Matthews issuing a retraction, paid half Glass's costs. It was
a minor event but must have taxed Frank's slender resources. In 1927 Glass resigned from
his trade union and from the SAAEO - and his retirement was noted with regret by the
executive of that body.In view of what was to happen later it must be stressed again that
Glass and Andrews were then, and later, close friends. That is until 1928 when Glass
responded to the journal, the Militant, published by former members of the
Communist Party of America who had resigned and joined the Left Opposition (or followers
of Leon Trotsky). The letter was written to provide a background to the situation in South
Africa and stated his position against the Black Republic slogan. He also sent his copy of
the journal to Manuel Lopes in Cape Town, the one person who had encouraged Glass to write
for the left press (at that time the Bolshevik) in Cape Town in the early 1920s.This was the end of Glass's collaboration with Andrews and henceforth
everything he did politically in Johannesburg was in opposition to the CPSA. But this was
not the end of Glass's association with the leaders of the ICU. It seems most likely that
the remarkable introduction to the ICU Economic and Political Program for 1928 was written
by Glass. The opening passage was not only militant, but also asserted the centrality of
the black workers in the struggle in South Africa:
Opponents of the ICU have frequently asserted that the Organization is not a trade
  union in the sense that the term is generally understood in South Africa, but that it is a
  kind of pseudo-political body ... The new constitution ... definitely establishes the ICU
  as a trade union, albeit one of native workers ... at the same time it must be clearly
  understood that we have no intention of copying the stupid and futile `non-political'
  attitude of our white contemporaries. As Karl Marx said, every economic question is, in
  the last analysis, a political question also, and we must recognise that in neglecting to
  concern ourselves with current politics, in leaving the political machine to the
  unchallenged control of our class enemies, we are rendering a disservice to those tens of
  thousands of our members who are groaning under oppressive laws ... At the present stage
  of our development it is inevitable that our activities should be almost of an agitational
  character, for we are not recognised as citizens in our own country, being almost entirely
  disfranchised and debarred from exercising a say in state affairs closely affecting our
  lives and welfare.Despite the sentiments expressed, the ICU was not organized as a trade
union and its days were already numbered when the constitution was drafted. The corruption
that La Guma had uncovered by 1926 had already destroyed any possibility of effective
functioning and Glass was to play no further role in its activities. But its importance
for this essay is the position Glass had taken on the role of the black worker.There the matter would have rested if it had not been for the
discussion of this period by R K Cope in his biography of Bill Andrews.When Cope's book appeared in 1944 it was a landmark in working class
publication in South Africa. This was the first book that purported to tell the story of
the communists of South Africa. It concentrated on the life of one man but, in the absence
of any other published history, it provided new material about events that were otherwise
unknown to most readers. This was the first published account of the early years of the
Labour Party and then, in successive steps, an account of the events leading to the
formation of the Communist Party. Those of us outside the CPSA who read the book in 1944,
rejected the slavish adulation of the USSR and the Comintern, but were pleased to have
some history of the left in South Africa. We also wanted to know more about Andrews, who
had been expelled from the CPSA in 1931, and who had been reinstated and was chairman of
the party - a party that had achieved respectability by virtue of its support for the war
effort.We were not altogether convinced. There were strange jumps in the book
for which there were no explanations. It was stated that Andrews was the secretary of the
CPSA in 1924. Then, without mention of the party conference, Cope stated on page 296 that
Andrews went back to his trade as a fitter in Johannesburg in 1925. There were other
problems that were fudged by Cope, but we ignored them. This was the kind of history
published by members or sympathisers of the CPSA and we expected no better of Cope.It was only when I started a study of Frank Glass in 1989 that I
noticed, for the first time, a strange omission in Cope's book. In the short
section dealing with the period 1924-30 Glass, Fanny Klenerman and Eva Green are never
mentioned. The campaign to affiliate to the SALP, the joint work in the trade unions, and
so on are expunged. Even in referring to the expulsions from the CPSA of 1931,
which included Andrews, S P Bunting; and others, Cope does not mention Fanny (Klenerman)
Glass. Andrews' speech at the ICU meeting in 1927 is quoted but Glass's address is not
mentioned and the suing of Matthews is also missing.It was only in 1992 that I found a statement, written by Andrews that
seemed to provide explanations of the inconsistencies. This old stalwart of the party, one
time member of the ECCI, was accused in 1931 of breaching party discipline by attending a
May Day rally organized by the Johannesburg United May Day Committee. In his defence,
Andrews stated that he had always maintained his 'revolutionary' position and, to this
end, he reproduced the text of the speech he had delivered in 1927 at the ICU meeting.
Glass's speech was excluded. Cope obviously quoted extensively from the document, not
stating his source, and providing only the evidence that Andrews chose to relate. Andrews
also stated that he had sued Matthews, a statement that I have not been able to confirm,
but again, there is no mention of Glass's role.None of this helped Andrews at the time. He was ignominiously expelled
from the party he had helped to form. He was not an oppositionist and could be expected to
accept every new party line. He could also be expected to turn his back on his closest
friends if the party demanded it of him. What is important for an understanding of the way
members of the CPSA acted is Andrews' selective recording of events to exclude all
reference to Glass, his former close comrade and friend, and to even adopt his friend's
actions as his own. Glass as a Trotskyist could be obliterated from the record, Fanny
Klenerman, Glass's wife and Eva Green, Andrews' one time lover, could be junked.In Cope's partial defence it must be said that he interviewed Andrews extensively and
received many of the documents he needed for the writing of the book from Andrews. He
should have checked against other sources but did not do so. But there is little to be
said for Andrews. He used the methods he learnt in the Communist International, lying when
necessary in an attempt to save his own position. Compared to the record of the men
arraigned in the Moscow Trials of 1937, this was a small lie. In Moscow there was a
systematic use of falsehood, leading to the condemnation of those who stood accused in the
dock and their inevitable execution.In terms of South African history the lies used by 'Comrade Bill
introduced a procedure that has marked much of communist writing. Lies, small and large,
are apparently permissible to boost the record of that party. If in the process others are
maligned or written out of history texts that does not concern such scribes.In early 1931 Glass sailed for China. The reasons for his going will be
discussed in my study of his life - but he was not in the country and had Andrews'
statement been disclosed he was too far away to intervene, or even care about what was
said. Glass knew only too well how men had been corrupted by their work for the Comintern.A history that deliberately excludes people and events is no better than a history that
lies. It should have no place in the annals of the socialist movement. Now it must be
said: it is time to set the record sought.
1. Letter from Suzy Weissman to the author. Suzy spoke to Glass, at a
Los Angeles sanatorium in 1986, and asked him questions on my behalf, about his South
African years. Unfortunately Glass died shortly after this conversation.
2. Department of Justice files, on microfilm at the School of Oriental and African
Studies, London.
3. See the article on the 1922 strike in this issue of Searchlight South Africa.
4. Tom Mbeki, originally a member of the Young Communist League, and
then a leading member of the ICU, was mentioned in police files as an informer. It is not
certain when he assumed this role, but it seems most likely that his craving for alcohol
led to his accepting money from the police.
5. See B Hirson and L Vivian, Strike Across the Empire: The Seamen's Strike of 1925
In Britain, South Africa and Australia, Clio Publications, 1992.
6. Report in the Rand Daily Mail, 28 March 1927. This incident
and the text of the speech was first noted by Peter Wickens, who mentioned it in his
history of the ICU, p 131. Wickens did not seem to have known of Cope's book (see above),
which was probably banned and unavailable in South Africa when he wrote his book-
7. A fuller account of the activities of Cecil Frank Glass will be found in my book,
tentatively entitled Revolutionary in Three Continents, forthcoming.
8. The court documents are available in the State archives, Pretoria
9. See the letter from Lopes to Andrews (below) when he was invited to
assist in the formation of the Friends of the Soviet Union.
10. See bibliography for details of publisher, etc
11. Roux does offer some account of the 1924 conference in his biography of S P
Bunting, but the account is skimpy and provides no details of the position taken by Glass.
R K Cope (c1944) Comrade Bill: The Life and Times of W H Andrews,
Worker's Leader, Stewart Printers, Cape Town.
B Hirson, Revolutionary In Three Continents, forthcoming.
E Roux (1944), S P Bunting; A Political Biography, African Bookman, Cape
Town.
P Wickens (1978), The Industrial and Commercial Workers' Union of
Africa, CUP, Cape Town, 1978.Manuel Lopes was a pioneer members of the Industrial Socialist League in Cape Town,
formed in 1916. Manuel became editor of The Bolshevik, and one of the first to
recognize the talent of Frank Glass.Lopes was a member of the CPSA from its inception, secretary of the
Cape Town section and a keen supporter of the revolutionary agenda of the new Russian
state. However, Lopes was also one of the first in South Africa to recognize that
socialism could not be built in an isolated backward state.Although disillusioned by what he perceived to have gone wrong in Russia, Manuel stayed
in the CPSA until expelled for opposing the `Black Republic slogan. But when he found no
alternative and could find no place in which to build a socialist movement, he veered to
the right and joined the Afrikaner based National Party. There is no excuse for people,
steeped in the ideas of the left, who move over to the far right. But so deep was their
disillusionment in what they saw coming out of Russia, and those falsehoods repeated by
men like Andrews that they abandoned all hope of a socialist agenda. In this they were not
unlike intellectuals like Koestler and Silone who rejected communisrn and contributed
essays in the volume, 7he God that Failed. The tragedy is that the process is being
repeated today by men and women who watch in despair as the countries they once believed
to be socialist are shown to have been primitive and backwards, unable to compete
economically with, never mind outstrip, the west.In seeing through the fraud represented by those who led organizations
like Friends of the Soviet Union in 1931, Lopes still argued in the language of his time.
This was borrowed partly from the writings of Leon Trotsky whose criticism of the Stalin
regime was still bounded by the belief that the achievements of the revolution of 1917
could be rescued, if only there was a working class movement to rally support for the
regime. Lopes might have been correct, but we will never know: the working class movement
he called for was never established.Andrews wrote to Lopes shortly before his own expulsion, suggesting that a branch of
the Friends of the Soviet Union be opened in Cape Town in a demonstration of his loyalty
to the CPSA and the Comintern. The original has not been found, but Lopes' reply, printed
here, was found in the files of the Trades and Labour Council.Dear Com Andrews,Many thanks for the reply received from you re Diamond's case which I regret to see is
moving towards an unhappy climax. If I can be of any further use, please let me know.
Whi1st writing may I state that my brother [also a former member of the CPSA] and I are
always keen to be of service to any section of the worker's movement and that at any time
we can be of service to you or to the organizations you represent, please let us know. I
have read with interest the manifesto of the proposed 'Friends of Soviet Russia' and have
considered your invitation to establish a branch here.A branch [of the FSU] has been established here already, and we are
invited to participate but probably could not give it our active support. For the life of
me, I cannot see the necessity of militants giving their time and energies to such an
organization whilst the political field is left open to opportunists masquerading in the
name of Labour and Socialism. You cannot hope to advance the case of militant Labour in
South Africa by such a procedure. The defence of the achievements of the Russian workers
and peasants can be, and must be, in the programme of a real worker's party
of which it forms an important part but yet, only a part.First things first: a real worker's party needs all our time and
attention and such a party would undoubtedly support the defence of the USSR, but a
separate organization based on such an isolated appeal is unnecessary and at present
unjustified.I may state that the above point of view is that held by a large
section of the Left, more especially by the followers of Leon Trotsky.As I am writing to you personally, may I add that the manifesto
reiterates many of the exaggerations broadcast by the ruling regime in Russia. 'Socialism
by leaps and bounds' is simply non est in Russia today and the slogan 'overtaking
and outstripping' capitalist countries is all bunkum and as misleading as the principle of
Socialism in one country. Socialism cannot exist in one country any more than capitalism
can exist in one country, and the question of Socialism in Russia is one that will find an
answer only in the arena of the world revolution. From this view again we see the relative
weakness of such bodies as 'Friends of the USSR' with their boasting of Stalinist
propaganda and the prime necessity of developing the class consciousness of the workers to
the end of the creation of a real worker's revolutionary party which today in South Africa
does not exist.I am sending under separate cover a journal which I am distributing
locally and which is devoted to the propagation of the above point of view.With best wishes, I remain,
Yours faithfully,
Manuel Lopes.
1. This letter, found in the files of the Trades and Labour Council was sent to us by
Kevin French.
2. Diamond, a member of the CPSA appeared in court on a charge of 'inciting to
violence' after leading a procession of white and black workers on the May Day in 1931 for
which Andrews was expelled from the party. Several party members received prison sentences
and Diamond received a twelve months prison sentence. 
Baruch Hirson Archive |
Marxism and Anti-Imperialism in Africa
, 