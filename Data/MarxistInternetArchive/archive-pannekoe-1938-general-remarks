
Published: Living Marxism, vol. 4, no. 5. November 1938.
Transcribed David Walters/Greg Adargo, December, 2001.
Organisation is the chief principle in the working class fight for
emancipation. Hence the forms of this organisation constitute the most important
problem in the practice of the working class movement. It is clear that these
forms depend on the conditions of society and the aims of the fight. They cannot
be the invention of theory, but have to be built up spontaneously by the working
class itself, guided by its immediate necessities.With expanding capitalism the workers first built their trade unions. The
isolated worker was powerless against the capitalist; so he had to unite with
his fellows in bargaining and fighting over the price of his labour-power and
the hours of labour. Capitalists and workers have opposite interests in
capitalistic production; their class struggle is over the division of the total
product between them. In normal capitalism, the workers' share is the value of
their labour power, i.e., what is necessary to sustain and restore continually
their capacities to work. The remaining part of the product is the surplus
value, the share of the capitalist class. The capitalists, in order to increase
their profit, try to lower wages and increase the hours of labour. Where the
workers were powerless, wages were depressed below the existence minimum; the
hours of labour were lengthened until the bodily and mental health of the
working class deteriorated so as to endanger the future of society. The
formation of unions and of laws regulating working conditions—features
rising out of the bitter fight of workers for their very lives—were
necessary to restore normal conditions of work in capitalism. The capitalist
class itself recognised that trade unions are necessary to direct the revolt of
the workers into regular channels to prevent them from breaking out in sudden
explosions.Similarly, political organisations have grown up, though not everywhere in
exactly the same way, because the political conditions are different in
different countries. In America, where a population of farmers, artisans and
merchants free from feudal bonds could expand over a continent with endless
possibilities, conquering the natural resources, the workers did not feel
themselves a separate class. They were imbued, as were the whole of the people,
with the bourgeois spirit of individual and collective fight for personal
welfare, and the conditions made it possible to succeed to a certain extent.
Except at rare moments or among recent immigrant groups, no need was seen for a
separate working class party. In the European countries, on the other hand, the
workers were dragged into the political struggle by the fight of the rising
bourgeoisie against feudalism. They soon had to form working class parties and,
together with part of the bourgeoisie, had to fight for political rights: for
the right to form unions, for free press and speech, for universal suffrage, for
democratic institutions. A political party needs general principles for its
propaganda; for its fight with other parties it wants a theory having definite
views about the future of society. The European working class, in which
communistic ideas had already developed, found its theory in the scientific work
of Marx and Engels, explaining the development of society through capitalism
toward communism by means of the class struggle. This theory was accepted in the
programs of the Social Democratic Parties of most European countries; in
England, the Labour Party formed by the trade unions, professed analogous but
vaguer ideas about a kind of socialist commonwealth as the aim of the
workers.In their program and propaganda, the proletarian revolution was the final
result of the class struggle; the victory of the working class over its
oppressors was to be the beginning of a communistic or socialist system of
production. But so long as capitalism lasted, the practical fight had to centre
on immediate needs and the preservation of standards in capitalism. Under
parliamentary government parliament is the battlefield where the interests of
the different classes of society meet; big and small capitalists, land owners,
farmers, artisans, merchants, industrialists, workers, all have their special
interests that are defended by their spokesmen in parliament, all participate in
the struggle for power and for their part in the total product. The workers have
to take part in this struggle. Socialist or labour parties have the special task
of fighting by political means for the immediate needs and interests of the
workers within capitalism. In this way they get the votes of the workers and
grow in political influence.With the modern development of capitalism, conditions have changed. The small
workshops have been superseded by large factories and plants with thousands and
tens of thousands of workers. With this growth of capitalism and of the working
class, its organisations also had to expand. From local groups the trade unions
grew to national federations with hundreds of thousands of members. They had to
collect large funds for support in big strikes, and still larger ones for social
insurance. A large staff of managers, administrators, presidents, secretaries,
editors of their papers, an entire bureaucracy of organisation leaders
developed. They had to haggle and bargain with the bosses; they became the
specialists acquainted with methods and circumstances. Eventually they became
the real leaders, the masters of the organisations, masters of the money as well
as of the press, while the members themselves lost much of their power. This
development of the organisations of the workers into instruments of power over
them has many examples in history; when organisations grow too large, the masses
lose control of them.The same change takes place in the political organisations, when from small
propaganda groups they grow into big political parties. The parliamentary
representatives are the leading politicians of the party. They have to do the
real fighting in the representative bodies; they are the specialists in that
field; they make up the editorial, propaganda, and executive personnel: their
influence determines the politics and tactical line of the party. The members
may send delegates to debate at party congresses, but their power is nominal and
illusory. The character of the organisation resembles that of the other
political parties—organisations of politicians who try to win votes for
their slogans and power for themselves. Once a socialist party has a large
number of delegates in parliament it allies with others against reactionary
parties to form a working majority. Soon socialists become ministers, state
officials, mayors and aldermen. Of course, in this position they cannot act as
delegates of the working class, governing for the workers against the capitalist
class. The real political power and even the parliamentary majority remain in
the hands of the capitalist class. Socialist ministers have to represent the
interests of the present capitalist society, i.e., of the capitalist class. They
can attempt to initiate measures for the immediate interests of the workers and
try to induce the capitalist parties to acquiesce. They become middlemen,
mediators pleading with the capitalist class to consent to small reforms in the
interests of the workers, and then try to convince the workers that these are
important reforms that they should accept. And then the Socialist Party, as an
instrument in the hands of these leaders, has to support them and also, instead
of calling upon the workers to fight for their interests, seeks to pacify them,
deflect them from the class struggle.Indeed, fighting conditions have grown worse for the workers. The power of
the capitalist class has increased enormously with its capital. The
concentration of capital in the hands of a few captains of finance and industry,
the coalition of the bosses themselves, confronts the trade unions with a much
stronger and often nearly unassailable power. The fierce competition of the
capitalists of all countries over markets, raw materials and world power, the
necessity of using increasing parts of the surplus value for this competition,
for armaments and welfare, the falling rate of profit, compel the capitalists to
increase the rate of exploitation, i.e., to lower the working conditions for the
workers. Thus the trade unions meet increasing resistance, the old methods of
struggle grow useless. In their bargaining with the bosses the leaders of the
organisation have less success; because they know the power of the capitalists,
and because they themselves do not want to fight—since in such fights the
funds and the whole existence of the organisation might be lost—they must
accept what the bosses offer. So their chief task is to assuage the workers'
discontent and to defend the proposals of the bosses as important gains. Here
also the leaders of the workers' organisations become mediators between the
opposing classes. And when the workers do not accept the conditions and strike,
the leaders either must oppose them or allow a sham fight, to be broken off as
soon as possible.The fight itself, however, cannot be stopped or minimised; the class
antagonism and the depressing forces of capitalism are increasing, so that the
class struggle must go on, the workers must fight. Time and again they break
loose spontaneously without asking the union and often against their decisions.
Sometimes the union leaders succeed in regaining control of these actions. This
means that the fight will be gradually smothered in some new arrangement between
the capitalists and labour leaders. This does not mean that without this
interference such wildcat strikes would be won. They are too restricted. Only
indirectly does the fear of such explosions tend to foster caution by the
capitalists. But these strikes prove that the class fight between capital and
labour cannot cease, and that when the old forms are not practicable any more,
the workers spontaneously try out and develop new forms of action. In these
actions revolt against capital is also revolt against the old organisational
forms.The aim and task of the working class is the abolition of capitalism.
Capitalism in its highest development, with its ever deeper economic crises, its
imperialism, its armaments, its world wars, threatens the workers with misery
and destruction. The proletarian class fight, the resistance and revolt against
these conditions, must go on until capitalist domination is overthrown and
capitalism is destroyed.Capitalism means that the productive apparatus is in the hands of the
capitalists. Because they are the masters of the means of production, and hence
of the products, they can seize the surplus value and exploit the working class.
Only when the working class itself is master of the means of production does
exploitation cease. Then the workers control entirely their conditions of life.
The production of everything necessary for life is the common task of the
community of workers, which is then the community of mankind. This production is
a collective process. First each factory, each large plant, is a collective of
workers, combining their efforts in an organised way. Moreover, the totality of
world production is a collective process; all the separate factories have to be
combined into a totality of production. Hence, when the working class takes
possession of the means of production, it has at the same time to create an
organisation of production.There are many who think of the proletarian revolution in terms of the former
revolutions of the middle class, as a series of consecutive phases: first,
conquest of government and instalment of a new government, then expropriation of
the capitalist class by law, and then a new organisation of the process of
production. But such events could lead only to some kind of state capitalism. As
the proletariat rises to dominance it develops simultaneously its own
organisation and the forms of the new economic order. These two developments are
inseparable and form the process of social revolution. Working class
organisation into a strong body capable of united mass actions already means
revolution, because capitalism can rule only unorganised individuals. When these
organised masses stand up in mass fights and revolutionary actions, and the
existing powers are paralysed and disintegrated, then simultaneously the leading
and regulating functions of former governments fall to the workers'
organisations. And the immediate task is to carry on production, to continue the
basic process of social life. Since the revolutionary class fight against the
bourgeoisie and its organs is inseparable from the seizure of the productive
apparatus by the workers and its application to production, the same
organisation that unites the class for its fight also acts as the organisation
of the new productive process.It is clear that the organisational forms of trade union and political party,
inherited from the period of expanding capitalism, are useless here. They
developed into instruments in the hands of leaders unable and unwilling to
engage in revolutionary fight. Leaders cannot make revolutions: labour leaders
abhor a proletarian revolution. For the revolutionary fights the workers need
new forms of organisation in which they keep the powers of action in their own
hands. It is pointless to try to construct or to imagine these new forms; they
can originate only in the practical fight of the workers themselves. They have
already originated there; we have only to look into practice to find its
beginnings everywhere that the workers are rebelling against the old powers.In a wildcat strike, the workers decide all matters themselves through
regular meetings. They choose strike committees as central bodies, but the
members of these committees can be recalled and replaced at any moment. If the
strike extends over a large number of shops, they achieve unity of action by
larger committees consisting of delegates of all the separate shops. Such
committees are not bodies to make decisions according to their own opinion, and
over the workers; they are simply messengers, communicating the opinions and
wishes of the groups they represent, and conversely, bringing to the shop
meetings, for discussion and decision, the opinion and arguments of the other
groups. They cannot play the roles of leaders, because they can be momentarily
replaced by others. The workers themselves must choose their way, decide their
actions; they keep the entire action, with all its difficulties, its risks, its
responsibilities, in their own hands. And when the strike is over, the
committees disappear.The only examples of a modern industrial working class as the moving force of
a political revolution were the Russian Revolutions of 1905 and 1917. Here the
workers of each factory chose delegates, and the delegates of all the factories
together formed the 'soviet,' the council where the political situation and
necessary actions were discussed. Here the opinions of the factories were
collected, their desires harmonised, their decisions formulated. But the
councils, though a strong directing influence for revolutionary education
through action, were not commanding bodies. Sometimes a whole council was
arrested and reorganised with new delegates; at times, when the authorities were
paralysed by a general strike, the soviets acted as a local government, and
delegates of free professions joined them to represent their field of work. Here
we have the organisation of the workers in revolutionary action, though of
course only imperfectly, groping and trying for new methods. This is possible
only when all the workers with all their forces participate in the action, when
their very existence is at stake, when they actually take part in the decisions
and are entirely devoted to the revolutionary fight.After the revolution this council organisation disappeared. The proletarian
centres of big industry were small islands in an ocean of primitive agricultural
society where capitalist development had not yet begun. The task of initiating
capitalism fell to the Communist Party. Simultaneously, political power centred
in its hands and the soviets were reduced to subordinate organs with only
nominal powers.The old forms of organisation, the trade union and political party and the
new form of councils (soviets), belong to different phases in the development of
society and have different functions. The first has to secure the position of
the working class among the other classes within capitalism and belongs to the
period of expanding capitalism. The latter has to secure complete dominance for
the workers, to destroy capitalism and its class divisions, and belongs to the
period of declining capitalism. In a rising and prosperous capitalism, council
organisation is impossible because the workers are entirely occupied in
ameliorating their conditions, which is possible at that time through trade
unions and political action. In a decaying crisis-ridden capitalism, these
efforts are useless and faith in them can only hamper the increase of
self-action by the masses. In such times of heavy tension and growing revolt
against misery, when strike movements spread over whole countries and hit at the
roots of capitalist power, or when, following wars or political catastrophes,
the government authority crumbles and the masses act, the old organisational
forms fail against the new forms of self-activity of the masses.Spokesmen for socialist or communist parties often admit that, in revolution,
organs of self-action by the masses are useful in destroying the old domination;
but then they say these have to yield to parliamentary democracy to organise the
new society. Let us compare the basic principles of both forms of political
organisation of society.Original democracy in small towns and districts was exercised by the assembly
of all the citizens. With the big population of modern towns and countries this
is impossible. The people can express their will only by choosing delegates to
some central body that represents them all. The delegates for parliamentary
bodies are free to act, to decide, to vote, to govern after their own opinion by
'honour and conscience,' as it is often called in solemn terms.The council delegates, however, are bound by mandate; they are sent simply to
express the opinions of the workers' groups who sent them. They may be called
back and replaced at any moment. Thus the workers who gave them the mandate keep
the power in their own hands.On the other hand, members of parliament are chosen for a fixed number of
years; only at the polls are the citizens masters—on this one day when
they choose their delegates. Once this day has passed, their power has gone and
the delegates are independent, free to act for a term of years according to
their own 'conscience,' restricted only by the knowledge that after this period
they have to face the voters anew; but then they count on catching their votes
in a noisy election campaign, bombing the confused voters with slogans and
demagogic phrases. Thus not the voters but the parliamentarians are the real
masters who decide politics. And the voters do not even send persons of their
own choice as delegates; they are presented to them by the political parties.
And then, if we suppose that people could select and send persons of their own
choice, these persons would not form the government; in parliamentary democracy
the legislative and the executive powers are separated. The real government
dominating the people is formed by a bureaucracy of officials so far removed
from the people's vote as to be practically independent. That is how it is
possible that capitalistic dominance is maintained through general suffrage and
parliamentary democracy. This is why in capitalistic countries, where the
majority of the people belongs to the working class, this democracy cannot lead
to a conquest of political power. For the working class, parliamentary democracy
is a sham democracy, whereas council representation is real democracy: the
direct rule of the workers over their own affairs.Parliamentary democracy is the political form in which the different
important interests in a capitalist society exert their influence upon
government. The delegates represent certain classes: farmers, merchants,
industrialists, workers; but they do not represent the common will of their
voters. Indeed, the voters of a district have no common will; they are an
assembly of individuals, capitalists, workers, shopkeepers, by chance living at
the same place, having partly opposing interests.Council delegates, on the other hand, are sent out by a homogeneous group to
express its common will. Councils are not only made up of workers, having common
class interests; they are a natural group, working together as the personnel of
one factory or section of a large plant, and are in close daily contact with
each other, having the same adversary, having to decide their common actions as
fellow workers in which they have to act in united fashion; not only on the
questions of strike and fight, but also in the new organisation of production.
Council representation is not founded upon the meaningless grouping of adjacent
villages or districts, but upon the natural groupings of workers in the process
of production, the real basis of society.However, councils must not be confused with the so-called corporative
representation propagated in fascist countries. This is a representation of the
different professions or trades (masters and workers combined), considered as
fixed constituents of society. This form belongs to a medieval society with
fixed classes and guilds, and in its tendency to petrify interest groups it is
even worse than parliamentarism, where new groups and new interests rising up in
the development of capitalism soon find their expression in parliament and
government.Council representation is entirely different because it is the representation
of a class engaged in revolutionary struggle. It represents working class
interests only, and prevents capitalist delegates and capitalist interests from
participation. It denies the right of existence to the capitalist class in
society and tries to eliminate capitalists by taking the means of production
away from them. When in the progress of revolution the workers must take up the
functions of organising society, the same council organisation is their
instrument. This means that the workers' councils then are the organs of the
dictatorship of the proletariat. This dictatorship of the proletariat is not a
shrewdly devised voting system artificially excluding capitalists and the
bourgeoisie from the polls. It is the exercise of power in society by the
natural organs of the workers, building up the productive apparatus as the basis
of society. In these organs of the workers, consisting of delegates of their
various branches in the process of production, there is no place for robbers or
exploiters standing outside productive work. Thus the dictatorship of the
working class is at the same time the most perfect democracy, the real workers'
democracy, excluding the vanishing class of exploiters.The adherents of the old forms of organisation exalt democracy as the only
right and just political form, as against dictatorship, an unjust form. Marxism
knows nothing of abstract right or justice; it explains the political forms in
which mankind expresses its feelings of political right, as consequences of the
economic structure of society. In Marxian theory we can find also the basis of
the difference between parliamentary democracy and council organisation. As
bourgeois democracy and proletarian democracy respectively they reflect the
different character of these two classes and their economic systems.Bourgeois democracy is founded upon a society consisting of a large number of
independent small producers. They want a government to take care of their common
interests: public security and order, protection of commerce, uniform systems of
weight and money, administering of law and justice. All these things are
necessary in order that everybody can do his business in his own way. Private
business takes the whole attention, forms the life interests of everybody, and
those political factors are, though necessary, only secondary and demand only a
small part of their attention. The chief content of social life, the basis of
existence of society, the production of all the goods necessary for life, is
divided up into private business of the separate citizens, hence it is natural
that it takes nearly all their time, and that politics, their collective affair,
is a subordinate matter, providing only for auxiliary conditions. Only in
bourgeois revolutionary movements do people take to the streets. But in ordinary
times politics are left to a small group of specialists, politicians, whose work
consists just of taking care of these general, political conditions of bourgeois
business.The same holds true for the workers, as long as they think only of their
direct interests. In capitalism they work long hours, all their energy is
exhausted in the process of exploitation, and little mental power and fresh
thought is left them. Earning their wage is the most immediate necessity of
life; their political interests, their common interest in safeguarding their
interests as wage earners may be important, but are still secondary. So they
leave this part of their interests also to specialists, to their party
politicians and their trade union leaders. By voting as citizens or members the
workers may give some general directions, just as middle-class voters may
influence their politicians, but only partially, because their chief attention
must remain concentrated upon their work.Proletarian democracy under communism depends upon just the opposite economic
conditions. It is founded not on private but on collective production.
Production of the necessities of life is no longer a personal business, but a
collective affair. The collective affairs, formerly called political affairs,
are no longer secondary, but the chief object of thought and action for
everybody. What was called politics in the former society—a domain for
specialists—has become the vital interest of every worker. It is not the
securing of some necessary conditions of production, it is the process and the
regulation of production itself. The separation of private and collective
affairs and interests has ceased. A separate group or class of specialists
taking care of the collective affairs is no longer necessary. Through their
council delegates, which link them together, the producers themselves are
managing their own productive work.The two forms of organisation are not distinguished in that the one is
founded upon a traditional and ideological basis, and the other on the material
productive basis of society. Both are founded upon the material basis of the
system of production, one on the declining system of the past, the other on the
growing system of the future. Right now we are in the period of transition, the
time of big capitalism and the beginnings of the proletarian revolution. In big
capitalism the old system of production has already been destroyed in its
foundations; the large class of independent producers has disappeared. The main
part of production is collective work of large groups of workers; but the
control and ownership have remained in a few private hands. This contradictory
state is maintained by the strong power factors of the capitalists, especially
the state power exerted by the governments. The task of the proletarian
revolution is to destroy this state power; its real content is the seizure of
the means of production by the workers. The process of revolution is an
alternation of actions and defeats that builds up the organisation of the
proletarian dictatorship, which at the same time is the dissolution, step by
step, of the capitalist state power. Hence it is the process of the replacement
of the organisation system of the past by the organisation system of the
future.We are only in the beginnings of this revolution. The century of class
struggle behind us cannot be considered a beginning as such, but only a
preamble. It developed invaluable theoretical knowledge, it found gallant
revolutionary words in defiance of the capitalist claim of being a final social
system; it awakened the workers from the hopelessness of misery. But its actual
fight remained bound within the confines of capitalism, it was action through
the medium of leaders and sought only to set easy masters in the place of hard
ones. Only a sudden flickering of revolt, such as political or mass strikes
breaking out against the will of the politicians, now and then announced the
future of self-determined mass action. Every wildcat strike, not taking its
leaders and catchwords from the offices of parties and unions, is an indication
of this development, and at the same time a small step in its direction. All the
existing powers in the proletarian movement, the socialist and communist
parties, the trade unions, all the leaders whose activity is bound to the
bourgeois democracy of the past, denounce these mass actions as anarchistic
disturbances. Because their field of vision is limited to their old forms of
organisation, they cannot see that the spontaneous actions of the workers bear
in them the germs of higher forms of organisation. In fascist countries, where
bourgeois democracy has been destroyed, such spontaneous mass actions will be
the only form of future proletarian revolt. Their tendency will not be a
restoration of the former middle class democracy but an advance in the direction
of the proletarian democracy, i.e., the dictatorship of the working class. 
Left Communism Subject Archive |
Pannekoek Archive
