
The first of the nights is this, and I cannot go to bed;  
I long for the dawning sorely, although when the night shall be dead,  
Scarce to me shall the day be alive.  Twice twenty-eight nights more,  
Twice twenty-eight long days till the evil dream be o'er!  
And he, does he count the hours as he lies in his prison-cell?  
Does he nurse and cherish his pain?  Nay, I know his strong heart well,  
Swift shall his soul fare forth; he is here, and bears me away,  
Till hand in hand we depart toward the hope of the earlier day.  
Yea, here or there he sees it:  in the street, in the cell, he sees  
The vision he made me behold mid the stems of the blossoming trees,  
When spring lay light on the earth, and first and at last I knew  
How sweet was his clinging hand, how fair were the deeds he would do.  

Nay, how wilt thou weep and be soft and cherish a pleasure in pain,  
When the days and their task are before thee and awhile thou must work  
for twain?  
O face, thou shalt lose yet more of thy fairness, be thinner no doubt,  
And be waxen white and worn by the day that he cometh out!  
Hand, how pale thou shalt be! how changed from the sunburnt hand  
That he kissed as it handled the rake in the noon of the summer land!  

Let me think then it is but a trifle:  the neighbours have told me so;  
"Two months! why that is nothing and the time will speedily go."  
'Tis nothing--O empty bed, let me work then for his sake!  
I will copy out the paper which he thought the News might take,  
If my eyes may see the letters; 'tis a picture of our life  
And the little deeds of our days ere we thought of prison and strife.  

Yes, neighbour, yes I am early--and I was late last night;  
Bedless I wore through the hours and made a shift to write.  
It was kind of you to come, nor will it grieve me at all  
To tell you why he's in prison and how the thing did befal;  
For I know you are with us at heart, and belike will join us soon.  
It was thus:  we went to a meeting on Saturday afternoon,  
At a new place down in the West, a wretched quarter enough,  
Where the rich men's houses are elbowed by ragged streets and rough,  
Which are worse than they seem to be.  (Poor thing! you know too well  
How pass the days and the nights within that bricken hell!)  
There, then, on a bit of waste we stood 'twixt the rich and the poor;  
And Jack was the first to speak; that was he that you met at the door  
Last week.  It was quiet at first; and dull they most of them stood  
As though they heeded nothing, nor thought of bad or of good,  
Not even that they were poor, and haggard and dirty and dull:  
Nay, some were so rich indeed that they with liquor were full,  
And dull wrath rose in their souls as the hot words went by their ears,  
For they deemed they were mocked and rated by men that were more than their peers.  
But for some, they seemed to think that a prelude was all this  
To the preachment of saving of souls, and hell, and endless bliss;  
While some (O the hearts of slaves!) although they might understand,  
When they heard their masters and feeders called thieves of wealth and of land,  
Were as angry as though THEY were cursed.  Withal there were some that heard,  
And stood and pondered it all, and garnered a hope and a word.  
Ah! heavy my heart was grown as I gazed on the terrible throng.  
Lo! these that should have been the glad and the deft and the strong,  
How were they dull and abased as the very filth of the road!  
And who should waken their souls or clear their hearts of the load?  

The crowd was growing and growing, and therewith the jeering grew;  
And now that the time was come for an ugly brawl I knew,  
When I saw how midst of the workmen some well-dressed men there came,  
Of the scum of the well-to-do, brutes void of pity or shame;  
The thief is a saint beside them.  These raised a jeering noise,  
And our speaker quailed before it, and the hubbub drowned his voice.  
Then Richard put him aside and rose at once in his place,  
And over the rags and the squalor beamed out his beautiful face,  
And his sweet voice rang through the tumult, and I think the crowd would have hushed  
And hearkened his manly words; but a well-dressed reptile pushed  
Right into the ring about us and screeched out infamies  
That sickened the soul to hearken; till he caught my angry eyes  
And my voice that cried out at him, and straight on me he turned,  
A foul word smote my heart and his cane on my shoulders burned.  
But e'en as a kestrel stoops down Richard leapt from his stool  
And drave his strong right hand amidst the mouth of the fool.  
Then all was mingled together, and away from him was I torn,  
And, hustled hither and thither, on the surging crowd was borne;  
But at last I felt my feet, for the crowd began to thin,  
And I looked about for Richard that away from thence we might win;  
When lo, the police amidst us, and Richard hustled along  
Betwixt a pair of blue-coats as the doer of all the wrong!  

Little longer, friend, is the story; I scarce have seen him again;  
I could not get him bail despite my trouble and pain;  
And this morning he stood in the dock:  for all that that might avail,  
They might just as well have dragged him at once to the destined jail.  
The police had got their man and they meant to keep him there,  
And whatever tale was needful they had no trouble to swear.  

Well, the white-haired fool on the bench was busy it seems that day,  
And so with the words "Two months," he swept the case away;  
Yet he lectured my man ere he went, but not for the riot indeed  
For which he was sent to prison, but for holding a dangerous creed.  
"What have you got to do to preach such perilous stuff?  
To take some care of yourself should find you work enough.  
If you needs must preach or lecture, then hire a chapel or hall;  
Though indeed if you take my advice you'll just preach nothing at all,  
But stick to your work:  you seem clever; who knows but you might rise,  
And become a little builder should you condescend to be wise?  
For in spite of your silly sedition, the land that we live in is free,  
And opens a pathway to merit for you as well as for me."  

Ah, friend, am I grown light-headed with the lonely grief of the night,  
That I babble of this babble?  Woe's me, how little and light  
Is this beginning of trouble to all that yet shall be borne -  
At worst but as the shower that lays but a yard of the corn  
Before the hailstorm cometh and flattens the field to the earth.  

O for a word from my love of the hope of the second birth!  
Could he clear my vision to see the sword creeping out of the sheath  
Inch by inch as we writhe in the toils of our living death!  
Could he but strengthen my heart to know that we cannot fail;  
For alas, I am lonely here--helpless and feeble and frail;  
I am e'en as the poor of the earth, e'en they that are now alive;  
And where is their might and their cunning with the mighty of men to strive?  
Though they that come after be strong to win the day and the crown,  
Ah, ever must we the deedless to the deedless dark go down,  
Still crying, "To-morrow, to-morrow, to-morrow yet shall be  
The new-born sun's arising o'er happy earth and sea" -  
And we not there to greet it--for to-day and its life we yearn,  
And where is the end of toiling and whitherward now shall we turn  
But to patience, ever patience, and yet and yet to bear;  
And yet, forlorn, unanswered as oft before to hear,  
Through the tales of the ancient fathers and the dreams that mock our wrong,  
That cry to the naked heavens, "How long, O Lord! how long?"  
The Pilgrims of Hope: Next PoemThe Pilgrims of Hope: Previous PoemThe Pilgrims of Hope: IndexThe William Morris Internet Archive : Works