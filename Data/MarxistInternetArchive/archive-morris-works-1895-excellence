An Objections to Socialism founded on the
difficulty of getting necessary work done when people will be free to choose
their own work are common in the mouths of antisocialists; and also it has
been and still is not uncommon to hear persons saying that no great works of
art or no productions of a high intellect will be
possible under a condition of things in which a reward is not given for such
work out of all proportion to the average of work, the hewing of wood and
drawing of water. Even Socialists themselves are sometimes hazy on these
subjects; and sometimes they seem ready to accept the view that when people
are free they will no longer care for anything more than what are now called
the necessities of life. Let us look into this matter a little. And first we
shall find that what lies at the root of these misconceptions is that reading
of the present into the future, which is so often a stumbling-block in the way
of a frank acceptance of the new Society.For as things now are, though a certain amount of utilities are of
necessity produced, yet it is at the expense of a waste of human labour mental
and bodily which is absolutely appalling. In spite of all the marvellous
inventions of modern times, and above all of the invention of the organization
of labour for production of market wares, the bulk of the population of
this country is not better, but worse off, than in the days when a great
part of the country was wood, waste, and marsh, when there was no machinery to
take the place of mere drudgery in production; when there were no appliances
to resist the accidents of the seasons and the rigour of the climate.The mere statement of this fact which cannot be seriously contravened,
shows how desperately wrong we have gone in some way or other. The truth is
that our system of Society is essentially a system of waste. We are,
all of us, engaged in making our livelihood, or accumulating our riches, not
by means of collaboration, but at each others expense; the result of this is
that inevitably we do not, and as a rule cannot think of the things
we make as pieces of utility, but rather as weapons for the defeat
of others; so that not hundreds or thousands, but millions of skilled and
intelligent men are engaged in producing things which people can be forced to
buy, but which they don't want at all. Space fails me to give examples of this
kind of waste, but a walk down a street of ‘flash’, Regent Street or
Bond Street e.g. will illustrate it sufficiently. How many of the articles
exhibited in this dreary show would any man in his senses carry home if he
were not compelled to buy them! The compulsion of the market is on
all of us, and not only forces us to pay for vulgarities and
shabby-gentilities, but worse still, forces a vast number of workmen to waste
their lives in producing them.Now in a Communistic Society all this would be altered; the demand for
goods wares would be real and not factitious: people would
ask for what they really wanted, and not for futilities and makeshifts. Labour
would be expended on things worth doing: and it is a fact past discussion that
so soon as things worth doing ????? are made, the intellect,
the skill, the artistic feeling of the makers are called out by their
production; in a word they exercise mens pleasurable energies, and therefore
make them happy.Such wares as this are works of art, each according to the necessities of
its own use, and I have not the slightest doubt that when the opportunity
was is offered then vast numbers of workmen
would will take it, and
wouldwill become artists, working well but
pleasantly, and also leisurely, because they would not have to expend their
energy defeating other workmen, but in developing their own best
faculties.In truth it was in this way that those great works of are which are still
left us from the past were produced: in those times whatever inequalities   
existed otherwise, amongst the workhandicraftsmen there was a
much nearer approach to equality than most people imagine. E.g. the architects
of an ancient building were not ‘gentlemen’ sitting in offices,
surrounded by an army of clerks and draftsmen, ‘ghosting’ their work
for them, but workmen abiding by their work, helping the masons and carpenters
certainly, directing them no doubt, but paid little more than they were. The
carvers again, who, mind you, were free to design their ornaments,
were paid no more than the ordinary masons: and so it was through all the
crafts. And did they do their work the worse for this approach to equality;
did thye neglect it because they were not bribed into excellence? There stands
their work to-day, unapproachable in excellence to answer the question. Go to
Westminster Abbey, and ask who raised that mass of loveliness. No one knows
their names have perished. You can have the names of
almost every fool who has but damaged
itthe building, since the epoch of the
artist-workman has passed away: theythese persons
were bribed to do their conceited trash by money and position, but those who
made its beauty needed no bribing to do their best, because their work was a
pleasure to them from day to day. On the one hand they worked for livelihood
and on the other for the works sake itself. They were men of the people, doubt
it not; and if their names have died, their work in more ways than one has
lived.And when we win equality in its full measure we shall do what we want in
the like spirit. Work without unceasing anxiety, without waste, without
contention is bound to be happy work, and from happy work comes beauty and
pleasure and self-respect.Even amidst the present turmoil of commercialism there are men who, working
in a comparatively humble sphere, can resist it, and who work for the works
sake. I will give one example instance of such men: a man I
know was a book-binder, to say truth the only man I have known who could be
trusted to repair a fine old book-binding: nothing would make him spoil his
work or hurry it; he would give the utmost care and attention to it, and
produce results quite wonderful doing the work with his own hands. Now he did
not need to be bribed; in fact he refused it, always working for ordinary
book-binders wages. If he had employed a number of men and done the work a
little worse he would have made a good income: but as it was he lived poor,
and died pooran artist, but a wage earner. That was a shame to all
of us. Yet I cannot pity him, for all his work was a pleasure to him; and his
friends also, which I am sure he had a good right to.But you see, he could not now be an example to other workmen.
As things go, I am glad there are not many like him, or we shoud not get on
toward our goal. In our condition of inequality it is better that we should
feel our oppression, even at the expense of good work, and beauty. We
are not fit for such things now, nor shall we be till we are working as equals
and friends, all of us. But when we are thus equal in some such way
shall we work; and there will be no fear then of our doing nothing but dry
utilitarian work. Have we not our wonderful machines to do that for us, to
save us from drudgery? What are the said machines about now, that the mass of
the people should toil & toil without pleasure? They are making profits
for their owners, and have no time to save usthat
people from drudgery. When the people are their owners — then we
shall see.
As to bribing Excellence
As to bribing Excellence is taken from the manuscript of
            the article. The manuscript is available on the website of the
            International Institute for Social History.
            In the MIA version, Morris's additions and
            corrections to the text are shown using superscript text and
            strike-throughs. In the anarchist journal Freedom, May 1895
The William Morris Internet Archive : Works
