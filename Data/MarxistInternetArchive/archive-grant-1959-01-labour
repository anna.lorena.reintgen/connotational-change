Source: Socialist Fight, vol. 2 no. 1 (January 1959)
Transcription: Francesco 2008
Proofread: Fred 2008
Markup: Manuel 2008
In a glossy
sixpenny pamphlet, the Labour Party puts forward its programme for the next
General Election. It is a programme which promises everything that the people
want: homes, jobs, education, health, pensions, public ownership, peace, a
reduction in taxes and the cost of living, and so on and so forth.

If this
programme could be carried out, it would mean a happy future for the people and
an impregnable position for the Labour Movement. It would be impossible for the
Tories to dislodge any Socialist Government which could guarantee all these
good things.

But there
is one flaw in this programme. That it is intended to be carried out within the
basic capitalist framework. This makes it an illusion and a snare.

The plan is based on the premise
of expanding production. The means are certainly there to carry out this
programme, and even a greatly extended one. But the trouble is to be able to use the resources that exist in the
country. Under capitalism this is not possible, for production under capitalism
is for profit and not for use. In order to carry out the programme, it would be
necessary to take over all industry with minimum compensation and operate it on
the basis of a plan. But this is not the perspective put forward.

In the
present period, the market is beginning to contract, and not to expand. Under
such conditions, far from increasing the standard of living and carrying out
the other promises, it would be difficult even to maintain the present
standards.

In the
section on Public Ownership, the document states:

“If—after full and careful enquiry—other industries
are found to be failing the nation, we shall not hesitate to use whatever
remedies, including further public ownership, are shown to be most effective…Today, 
fewer than 600 giant privately-owned firms dominate the production,
investment, finance and trade of the private sector of Britain’s economy. The
men who make the crucial decisions in these firms are the directors. Their
shareholders have long since ceased to exercise effective control over them.
The Board of Directors are, in fact, responsible to no-one but themselves.
Labour believes the time has come when public control must be extended so as to
ensure that the decisions of these Boards, which vitally affect our economy,
are in line with the nation’s interests.”

Unless
these 600, and the other major industries, are taken over and production
rationally organised according to a democratic plan, with the full participation
of the workers and technicians themselves, the programme of reforms is
unrealistic. Controls by themselves can settle nothing. In the last years of
the Labour Government, Harold Wilson made a “bonfire” of controls.

Unless they
are making profits, the capitalists will not produce. They will begin a
campaign of sabotage and of pressure, as in the days of 1929 to 1931. Under
such conditions, the programme of the Labour Government would collapse. The
capitalist press and all the media of public opinion would be waging a campaign
of lies and slander against the Labour Government. Disillusionment and
demoralisation of the workers would begin, and the path be opened for
conspiracies and plots for the return to power of an even more vicious Toryism.

In this
connection, the role of the so-called Lefts is particularly pernicious. Tribune’s prize-winning essay on “My
Fighting Policy for Labour,” does not differ in any essentials from Labour’s
plan. In fact, it does not even mention nationalisation, which receives an
apologetic reference in Labour’s Plan. There is nothing to indicate that only a
real socialist programme can serve the needs of the people.

Equally inadequate is the Plan’s
approach to international affairs. The world has reached an impasse in the
struggle between Anglo-American imperialism and the Soviet bloc. Crisis follows
crisis, in unending succession. Berlin is the latest.

The
document bases itself on the idea of the United Nations. But the United Nations
can only be a forum for the conflicting interests of all the powers. It cannot
solve the problems of our time.

A Socialist
Plan in Britain, introducing real socialist democracy, would be the greatest
blow for peace that has been struck since the Russian revolution of 1917. A
Socialist Britain could appeal to the peoples of the world with clean hands and
with the interests of all the peoples as its basic policy. It would undermine
the positions of the Russian Bureaucracy and of American Imperialism at one and
the same time.

The
publication of this programme, and the approach of the General Election, makes
it even more urgent for the Marxist forces within the Labour Movement to
propagate a real socialist policy for a Labour Government. It is an interesting
fact that nearly every union is pledged to fight for the nationalisation of its
own industry. Let us generalise these aspirations of the workers and fight in
the unions and the Labour Movement for the carrying out of a bold Socialist
programme, without which the Movement is doomed to defeat and sterility.
 Ted Grant Archive