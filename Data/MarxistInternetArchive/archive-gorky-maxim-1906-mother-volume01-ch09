
Maxim Gorky
 
When the police had led Pavel away, the mother sat down on the
bench, and closing her eyes began to weep quietly.  She leaned her
back against the wall, as her husband used to do, her head thrown
backward.  Bound up in her grief and the injured sense of her
impotence, she cried long, gently, and monotonously, pouring out all
the pain of her wounded heart in her sobs.  And before her, like an
irremovable stain, hung that yellow face with the scant mustache, and
the squinting eyes staring at her with malicious pleasure.  Resentment
and bitterness were winding themselves about her breast like black
threads on a spool; resentment and bitterness toward those who tear
a son away from his mother because he is seeking truth.

It was cold; the rain pattered against the window panes; something
seemed to be creeping along the walls.  She thought she heard,
walking watchfully around the house, gray, heavy figures, with
broad, red faces, without eyes, and with long arms.  It seemed to
her that she almost heard the jingling of their spurs.

"I wish they had taken me, too!" she thought.

The whistle blew, calling the people to work.  This time its sounds
were low, indistinct, uncertain.  The door opened and Rybin entered.
He stood before her, wiping the raindrops from his beard.

"They snatched him away, did they?" he asked.

"Yes, they did, the dogs!" she replied, sighing.

"That's how it is," said Rybin, with a smile; "they searched me,
too; went all through me--yes!  Abused me to their heart's content,
but did me no harm beyond that.  So they carried off Pavel, did
they?  The manager tipped the wink, the gendarme said 'Amen!' and
lo! a man has disappeared.  They certainly are thick together.  One
goes through the people's pockets while the other holds the gun."

"You ought to stand up for Pavel!" cried the mother, rising to her
feet.  "It's for you all that he's gone!"

"Who ought to stand up for him?" asked Rybin.

"All of you!"

"You want too much!  We'll do nothing of the kind!  Our masters
have been gathering strength for thousands of years; they have
driven our hearts full of nails.  We cannot unite at once.  We
must first extract from ourselves, each from the other, the iron
spikes that prevent us from standing close to one another."

And thus he departed, with his heavy gait, leaving the mother to
her grief, aggravated by the stern hopelessness of his words.

The day passed in a thick mist of empty, senseless longing.  She 
made no fire, cooked no dinner, drank no tea, and only late in the
evening ate a piece of bread.  When she went to bed it occurred to
her that her life had never yet been so humiliating, so lonely and
void.  During the last years she had become accustomed to live
constantly in the expectation of something momentous, something
good.  Young people were circling around her, noisy, vigorous, full
of life.  Her son's thoughtful and earnest face was always before
her, and he seemed to be the master and creator of this thrilling
and noble life.  Now he was gone, everything was gone.  In the whole
day, no one except the disagreeable Rybin had called.

Beyond the window, the dense, cold rain was sighing and knocking
at the panes.  The rain and the drippings from the roof filled the
air with a doleful, wailing melody.  The whole house appeared to be
rocking gently to and fro, and everything around her seemed aimless
and unnecessary.

A gentle rap was heard at the door.  It came once, and then a second
time.  She had grown accustomed to these noises; they no longer
frightened her.  A soft, joyous sensation thrilled her heart, and a
vague hope quickly brought her to her feet.  Throwing a shawl over
her shoulders, she hurried to the door and opened it.

Samoylov walked in, followed by another man with his face hidden
behind the collar of his overcoat and under a hat thrust over his
eyebrows.

"Did we wake you?" asked Samoylov, without greeting the mother, his
face gloomy and thoughtful, contrary to his wont.

"I was not asleep," she said, looking at them with expectant eyes.

Samoylov's companion took off his hat, and breathing heavily and
hoarsely said in a friendly basso, like an old acquaintance, giving
her his broad, short-fingered hand:

"Good evening, granny!  You don't recognize me?"

"Is it you?" exclaimed Nilovna, with a sudden access of delight.
"Yegor Ivanovich?"

"The very same identical one!" replied he, bowing his large head
with its long hair.  There was a good-natured smile on his face, and
a clear, caressing look in his small gray eyes.  He was like a
samovar--rotund, short, with thick neck and short arms.  His face
was shiny and glossy, with high cheek bones.  He breathed noisily,
and his chest kept up a continuous low wheeze.

"Step into the room.  I'll be dressed in a minute," the mother said.

"We have come to you on business," said Samoylov thoughtfully,
looking at her out of the corner of his eyes.

Yegor Ivanovich passed into the room, and from there said:

"Nikolay got out of jail this morning, granny.  You know him?"

"How long was he there?" she asked.

"Five months and eleven days.  He saw the Little Russian there, who
sends you his regards, and Pavel, who also sends you his regards and
begs you not to be alarmed.  As a man travels on his way, he says,
the jails constitute his resting places, established and maintained
by the solicitous authorities!  Now, granny, let us get to the point.
Do you know how many people were arrested yesterday?"

"I do not.  Why, were there any others arrested besides Pavel?"
she exclaimed.

"He was the forty-ninth!" calmly interjected Yegor Ivanovich.  "And
we may expect about ten more to be taken!  This gentleman here,
for example."

"Yes; me, too!" said Samoylov with a frown.

Nilovna somehow felt relieved.

"He isn't there alone," she thought.

When she had dressed herself, she entered the room and, smiling
bravely, said:

"I guess they won't detain them long, if they arrested so many."

"You are right," assented Yegor Ivanovich; "and if we can manage
to spoil this mess for them, we can make them look altogether like
fools.  This is the way it is, granny.  If we were now to cease
smuggling our literature into the factory, the gendarmes would take
advantage of such a regrettable circumstance, and would use it
against Pavel and his comrades in jail."

"How is that?  Why should they?" the mother cried in alarm.

"It's very plain, granny," said Yegor Ivanovich softly.  "Sometimes
even gendarmes reason correctly.  Just think!  Pavel was, and there
were books and there were papers; Pavel is not, and no books and no
papers!  Ergo, it was Pavel who distributed these books!  Aha!  Then
they'll begin to eat them all alive.  Those gendarmes dearly love so
to unman a man that what remains of him is only a shred of himself,
and a touching memory."

"I see, I see," said the mother dejectedly.  "O God!  What's to be
done, then?"

"They have trapped them all, the devil take them!" came Samoylov's
voice from the kitchen.  "Now we must continue our work the same as
before, and not only for the cause itself, but also to save our comrades!"

"And there is no one to do the work," added Yegor, smiling.  "We
have first-rate literature.  I saw to that myself.  But how to get
it into the factory, that's the question!"

"They search everybody at the gates now," said Samoylov.

The mother divined that something was expected of her.  She understood
that she could be useful to her son, and she hastened to ask:

"Well, now?  What are we to do?"

Samoylov stood in the doorway to answer.

"Pelagueya Nilovna, you know Marya Korsunova, the peddler."

"I do.  Well?"

"Speak to her; see if you can't get her to smuggle in our wares."

"We could pay her, you know," interjected Yegor.

The mother waved her hands in negation.

"Oh, no!  The woman is a chatterbox.  No!  If they find out it comes
from me, from this house--oh, no!"

Then, inspired by a sudden idea, she began gladly and in a low voice:

"Give it to me, give it to me.  I'll manage it myself.  I'll find a
way.  I will ask Marya to make me her assistant.  I have to earn my
living, I have to work.  Don't I?  Well, then, I'll carry dinners to
the factory.  Yes, I'll manage it!"

Pressing her hands to her bosom, she gave hurried assurances that
she would carry out her mission well and escape detection.  Finally
she exclaimed in triumph:  "They'll find out--Pavel Vlasov is away,
but his arm reaches out even from jail.  They'll find out!"

All three became animated.  Briskly rubbing his hands, Yegor smiled
and said:

"It's wonderful, stupendous!  I say, granny, it's superb--simply
magnificent!"

"I'll sit in jail as in an armchair, if this succeeds," said 
Samoylov, laughing and rubbing his hands.

"You are fine, granny!" Yegor hoarsely cried.

The mother smiled.  It was evident to her that if the leaflets
should continue to appear in the factory, the authorities would be
forced to recognize that it was not her son who distributed them.
And feeling assured of success, she began to quiver all over with joy.

"When you go to see Pavel," said Yegor, "tell him he has a good mother."

"I'll see him very soon, I assure you," said Samoylov, smiling.

The mother grasped his hand and said earnestly:

"Tell him that I'll do everything, everything necessary.  I want
him to know it."

"And suppose they don't put him in prison?" asked Yegor, pointing
at Samoylov.

The mother sighed and said sadly:

"Well, then, it can't be helped!"

Both of them burst out laughing.  And when she realized her ridiculous
blunder, she also began to laugh in embarrassment, and lowering
her eyes said somewhat slyly:

"Bothering about your own folk keeps you from seeing other people
straight."

"That's natural!" exclaimed Yegor.  "And as to Pavel, you need not
worry about him.  He'll come out of prison a still better man.  The
prison is our place of rest and study--things we have no time for
when we are at large.  I was in prison three times, and each time,
although I got scant pleasure, I certainly derived benefit for my
heart and mind."

"You breathe with difficulty," she said, looking affectionately at
his open face.

"There are special reasons for that," he replied, raising his finger.
"So the matter's settled, granny?  Yes?  To-morrow we'll deliver the
matter to you--and the wheels that grind the centuried darkness to
destruction will again start a-rolling.  Long live free speech!  And
long live a mother's heart!  And in the meantime, good-by."

"Good-by," said Samoylov, giving her a vigorous handshake.  "To my
mother, I don't dare even hint about such matters.  Oh, no!"

"Everybody will understand in time," said Nilovna, wishing to please
him.  "Everybody will understand."

When they left, she locked the door, and kneeling in the middle of
the room began to pray, to the accompaniment of the patter of the
rain.  It was a prayer without words, one great thought of men, of
all those people whom Pavel introduced into her life.  It was as if
they passed between her, and the ikons upon which she held her eyes
riveted.  And they all looked so simple, so strangely near to one
another, yet so lone in life.

Early next morning the mother went to Marya Korsunova.  The peddler,
noisy and greasy as usual, greeted her with friendly sympathy.

"You are grieving?" Marya asked, patting the mother on the back.
"Now, don't.  They just took him, carried him off.  Where is the
calamity?  There is no harm in it.  It used to be that men were
thrown into dungeons for stealing, now they are there for telling
the truth.  Pavel may have said something wrong, but he stood up for
all, and they all know it.  Don't worry!  They don't all say so, but
they all know a good man when they see, him.  I was going to call on
you right along, but had no time.  I am always cooking and selling,
but will end my days a beggar, I guess, all the same.  My needs get
the best of me, confound them!  They keep nibbling and nibbling like
mice at a piece of cheese.  No sooner do I manage to scrape together
ten rubles or so, when along comes some heathen, and makes away with
all my money.  Yes.  It's hard to be a woman!  It's a wretched
business!  To live alone is hard, to live with anyone, still harder!"

"And I came to ask you to take me as your assistant," Vlasova broke
in, interrupting her prattle.

"How is that?" asked Marya.  And after hearing her friend's
explanation, she nodded her head assentingly.

"That's possible!  You remember how you used to hide me from my
husband?  Well, now I am going to hide you from want.  Everyone
ought to help you, for your son is perishing for the public cause.
He is a fine chap, your son is!  They all say so, every blessed
soul of them.  And they all pity him.  I'll tell you something.  No
good is going to come to the authorities from these arrests, mark my
word!  Look what's going on in the factory!  Hear them talk!  They
are in an ugly mood, my dear!  The officials imagine that when
they've bitten at a man's heel, he won't be able to go far.  But it
turns out that when ten men are hit, a hundred men get angry.  A
workman must be handled with care!  He may go on patiently enduring
and suffering everything that's heaped upon him for a long, long
time, but then he can also explode all of a sudden!"

Next: CHAPTER X

Table of Contents: Mother (Part I)
