Henryk Grossman 1934Written:
Unknown;
First Published: 1934;
Source: Edwin R. A.
Seligman (ed) Encyclopaedia of the Social Sciences, Volume Fourteen,
(Macmillan,  New York, 1934), pp 69-71. 
Transcription:
Steve
Palmer with thanks to Rick Kuhn for help.
Copyleft: This article
is published under the fair use provision of relevant copyright
legislation.
SISMONDI,
JEAN CHARLES LEONARD SIMONDE DE (1773-1842), Swiss
economist,
historian, historian of literature and publicist. Sismondi, a Genevan
descended from an old French family, emigrated to England in 1793 and
to Tuscany the following year but in 1800 settled in his native city,
where from 1809 on he lectured at the Academy of Geneva on ancient and
literary history and economics. His development, particularly as
historian and historian of literature, was profoundly influenced by his
friendship with Madame de Staël and by the prominent personalities whom
he met in her cosmopolitan salon at Coppet.Through
his Histoire des républiques italiennes au moyen âge
Sismondi was the
first to awaken general interest in the mediaeval history of Italy. But
while he was a pioneer in the reclamation of the Middle Ages from the
contempt of the eighteenth century rationalists, he shared the tendency
of the latter to use history for didactic purposes: he wrote his
history of the Italian towns as an anti-Bonapartist republican who
wished to remind an enslaved nation of its glorious past. He was one of
the first to understand that the liberation of the mediaeval Italian
cities enabled them to take precedence in the development of a
bourgeois society. His descriptions of currency, trade, manufactures,
agriculture and the role of productive capital often reveal technical
precision, although he had little inkling of the interdependence
between politics and economic drives. His subsequent Histoire
des
Français without transcending the limitations of the earlier
work won
recognition as the earliest comprehensive survey of the subject based
on original research and as a suggestive synthesis of varied currents
particularly in the mediaeval period.As
author of De la littérature du midi de l'Europe,
a revision of his
lectures on the evolution of Arabian, Provencal, Italian, Spanish and
Portuguese literature, Sismondi became cofounder with Madame de Staël
of the "philosophy of literature," which paved the way for the
sociological approach to the subject. Just as his investigations of the
mediaeval history of Italy had convinced him that national character
was determined by social and political institutions rather than by
racial or climatic peculiarities, so he now presented literature as the
natural if sometimes belated reflection of the same forces. His use of
the comparative method enabled him to show the universal parallelism in
the stages of intellectual development of different nations and his
defense of national art smoothed the path of romanticism especially in
France.Sismondi's
general political outlook, as represented, for instance, by Études
sur
les constitutions des peuples libres (Paris 1836), was
liberal but
anti-Rousseauistic and antidemocratic. Believing that both the
proletariat and the lower middle classes were not yet ready for
democracy, he offered an apologia for the privileges of minorities,
particularly of the urban bourgeoisie and the intellectuals, whom he
regarded not only as the progressive elements but as the
representatives of national tradition.Although
in his earliest economic writings, Tableau de l'agriculture
toscane
(Geneva 1801) and De la richesse commerciale (2
vols., Geneva 1803), he
had defended Smithian liberalism and popularized it before J. B. Say,
his Nouveaux principes d'économie politique (2
vols., Paris 1819; 2nd
ed. 1827), which reflects his observation of the economic crisis during
his second trip to England (1818-19), constitutes a devastating
criticism of the basic axioms upon which the classical economists built
their idealized and static conception of capitalism. He showed clearly
the untenability of the doctrine that competition always tends to
establish an equilibrium between production and consumption. More
specifically he emphasized that general crises, or gluts, are not only
possible - a fact denied by the classical economists, although they
admitted the possibility of partial gluts arising from an occasional
disproportion between individual branches of industry - but an
inevitable periodic concomitant of the prevailing economic structure.
The direct cause of such crises he found in underconsumption on the
part of the working classes, whose purchasing power is insufficient to
absorb the annual national output. The second inherent characteristic
of capitalism overlooked by the classical economists was the necessity
of continuous expansion and conquest of new outlets, resulting from the
restriction of the internal market. While Sismondi was incorrect in
limiting demand to consumers' demand and in other aspects of his
argument, the essential validity of his conclusions makes him the
scientific discoverer of capitalistic dynamics.The
deeper cause of underconsumption, that is, of the simultaneous decline
in income and increase in the volume of production, Sismondi attributed
to the fact that in a capitalistic society the extent and direction of
economic activity are determined by exchange value, which by reason of
the unevenness of technological advance within each branch of
production is in a constant state of flux. The only possible basis for
a harmonious and stable adjustment is the social need measured in terms
of the quantity of useful commodities. Since the latter has no
determining effect in a capitalistic system, Sismondi denied
categorically the possibility of regulating economic activity in such a
system. His various reform proposals, including his demand for public
protection of workers, were consciously advanced merely as palliatives,
which he realized could not correct the evils inherent in the existing
order.Among
his contemporaries Sismondi was recognized chiefly as historian and
historian of literature and subsequently fell into oblivion, except in
Italy, where his work as a national historian influenced the spiritual
revival during the Risorgimento. After 1850 the protagonists of social
reform, frequently exaggerating Sismondi's really limited faith in
reform measures, rediscovered him and hailed him as a precursor. Today
he is famous especially as a theorist of crises, whose ideas were taken
over not only by his contemporaries Malthus and Rodbertus but under the
disguise of Marxist terminology by such socialists as Heinrich Cunow,
L. Boudin, Karl Kautsky and Rosa Luxemburg. Since the outbreak of the
present world crisis this under-consumption theory, which Lenin justly
attacked as non-Marxist, has become the official doctrine of numerous
socialist parties and trade unions in Europe and America.HENRYK
GROSSMANWorks: Histoire des
républiques italiennes au moyen âge, 16 vols. (Zurich and
Paris 1807-18; new ed., 10 vols., Paris 1840-41; abridged ed. as Histoire
de la renaissance de la liberté en Italie, 2 vols., Paris
1832), tr. as A History of the Italian Republics
(London 1832;
reprinted in Everyman's Library, London 1907); Histoire des
Français,
31 vols. (Paris 1821-44, abridged ed. as Precis de
l'histoire des
Francais, 2 vols., Paris 1838, and a third volume by E.
Robinet, 1844); De la littérature du midi de l'Europe,
4 vols. (Paris 1813; 4th ed., 2
vols., Brussels 1837), tr. by Thomas Roscoe as Historical
View of the
Literature of the South of Europe, 2 vols. (4th ed. London
1853); Etudes sur les sciences sociales, 3 vols.
(Paris 1836-38).Consult:
Sails, Jean R. de, Sismondi, 1773-1842; Bibliotheque
de la Revue de
Litterature Comparée, vol. lxxvii, 2 vols. (Paris 1932);
Aftalion,
Albert, L'oeuvre économique de Simonde de Sismondi
(Paris 1899);
Grossman, Henryk, Simonde de Sismondi et ses théories
économiques,
Warsaw, Bibliotheca Universitatis Liberae Polonae, no. 11 (Warsaw
1924); Tuan, Mao-Lan, Simonde de Sismondi as an Economist,
Columbia
University, Studies in History, Economics and Public Law,
no. 298 (New
York 1927); Festy, O., "Sismondi et la condition des ouvriers français
de son temps" in Revue d'économie politique, vol.
xxxii (1918) 46-72,
119-36; Lenin, V. I., "K kharakteristike ekonomicheskogo romantizma"
(Characteristics of economic romanticism) in his Sochineniya,
vol. ii
(2nd rev. ed. Moscow 1926) p. 9-115; Jeandeau, René, Sismondi,
précurseur de la législation sociale contemporaine (Bordeaux
1913);
Pellegrini, Carlo, II Sismondi e la storia delle letterature
dell'
Europa meridionale, Biblioteca dell' Archivum romanicum, 1st
ser., vol.
vii (Geneva 1926); Gooch, G. P., History and Historians in
the
Nineteenth Century (London 1913) p. 165-68. Henryk
Grossman Internet Archive | Political
Economy Archive

Henryk Grossman 1934
Sismondi, Jean Charles Leonard Simonde de
(1773-1842)

Written:
Unknown;
First Published: 1934;
Source: Edwin R. A.
Seligman (ed) Encyclopaedia of the Social Sciences, Volume Fourteen,
(Macmillan,  New York, 1934), pp 69-71. 
Transcription:
Steve
Palmer with thanks to Rick Kuhn for help.
Copyleft: This article
is published under the fair use provision of relevant copyright
legislation.

 
SISMONDI,
JEAN CHARLES LEONARD SIMONDE DE (1773-1842), Swiss
economist,
historian, historian of literature and publicist. Sismondi, a Genevan
descended from an old French family, emigrated to England in 1793 and
to Tuscany the following year but in 1800 settled in his native city,
where from 1809 on he lectured at the Academy of Geneva on ancient and
literary history and economics. His development, particularly as
historian and historian of literature, was profoundly influenced by his
friendship with Madame de Staël and by the prominent personalities whom
he met in her cosmopolitan salon at Coppet.
Through
his Histoire des républiques italiennes au moyen âge
Sismondi was the
first to awaken general interest in the mediaeval history of Italy. But
while he was a pioneer in the reclamation of the Middle Ages from the
contempt of the eighteenth century rationalists, he shared the tendency
of the latter to use history for didactic purposes: he wrote his
history of the Italian towns as an anti-Bonapartist republican who
wished to remind an enslaved nation of its glorious past. He was one of
the first to understand that the liberation of the mediaeval Italian
cities enabled them to take precedence in the development of a
bourgeois society. His descriptions of currency, trade, manufactures,
agriculture and the role of productive capital often reveal technical
precision, although he had little inkling of the interdependence
between politics and economic drives. His subsequent Histoire
des
Français without transcending the limitations of the earlier
work won
recognition as the earliest comprehensive survey of the subject based
on original research and as a suggestive synthesis of varied currents
particularly in the mediaeval period.
As
author of De la littérature du midi de l'Europe,
a revision of his
lectures on the evolution of Arabian, Provencal, Italian, Spanish and
Portuguese literature, Sismondi became cofounder with Madame de Staël
of the "philosophy of literature," which paved the way for the
sociological approach to the subject. Just as his investigations of the
mediaeval history of Italy had convinced him that national character
was determined by social and political institutions rather than by
racial or climatic peculiarities, so he now presented literature as the
natural if sometimes belated reflection of the same forces. His use of
the comparative method enabled him to show the universal parallelism in
the stages of intellectual development of different nations and his
defense of national art smoothed the path of romanticism especially in
France.
Sismondi's
general political outlook, as represented, for instance, by Études
sur
les constitutions des peuples libres (Paris 1836), was
liberal but
anti-Rousseauistic and antidemocratic. Believing that both the
proletariat and the lower middle classes were not yet ready for
democracy, he offered an apologia for the privileges of minorities,
particularly of the urban bourgeoisie and the intellectuals, whom he
regarded not only as the progressive elements but as the
representatives of national tradition.
Although
in his earliest economic writings, Tableau de l'agriculture
toscane
(Geneva 1801) and De la richesse commerciale (2
vols., Geneva 1803), he
had defended Smithian liberalism and popularized it before J. B. Say,
his Nouveaux principes d'économie politique (2
vols., Paris 1819; 2nd
ed. 1827), which reflects his observation of the economic crisis during
his second trip to England (1818-19), constitutes a devastating
criticism of the basic axioms upon which the classical economists built
their idealized and static conception of capitalism. He showed clearly
the untenability of the doctrine that competition always tends to
establish an equilibrium between production and consumption. More
specifically he emphasized that general crises, or gluts, are not only
possible - a fact denied by the classical economists, although they
admitted the possibility of partial gluts arising from an occasional
disproportion between individual branches of industry - but an
inevitable periodic concomitant of the prevailing economic structure.
The direct cause of such crises he found in underconsumption on the
part of the working classes, whose purchasing power is insufficient to
absorb the annual national output. The second inherent characteristic
of capitalism overlooked by the classical economists was the necessity
of continuous expansion and conquest of new outlets, resulting from the
restriction of the internal market. While Sismondi was incorrect in
limiting demand to consumers' demand and in other aspects of his
argument, the essential validity of his conclusions makes him the
scientific discoverer of capitalistic dynamics.
The
deeper cause of underconsumption, that is, of the simultaneous decline
in income and increase in the volume of production, Sismondi attributed
to the fact that in a capitalistic society the extent and direction of
economic activity are determined by exchange value, which by reason of
the unevenness of technological advance within each branch of
production is in a constant state of flux. The only possible basis for
a harmonious and stable adjustment is the social need measured in terms
of the quantity of useful commodities. Since the latter has no
determining effect in a capitalistic system, Sismondi denied
categorically the possibility of regulating economic activity in such a
system. His various reform proposals, including his demand for public
protection of workers, were consciously advanced merely as palliatives,
which he realized could not correct the evils inherent in the existing
order.
Among
his contemporaries Sismondi was recognized chiefly as historian and
historian of literature and subsequently fell into oblivion, except in
Italy, where his work as a national historian influenced the spiritual
revival during the Risorgimento. After 1850 the protagonists of social
reform, frequently exaggerating Sismondi's really limited faith in
reform measures, rediscovered him and hailed him as a precursor. Today
he is famous especially as a theorist of crises, whose ideas were taken
over not only by his contemporaries Malthus and Rodbertus but under the
disguise of Marxist terminology by such socialists as Heinrich Cunow,
L. Boudin, Karl Kautsky and Rosa Luxemburg. Since the outbreak of the
present world crisis this under-consumption theory, which Lenin justly
attacked as non-Marxist, has become the official doctrine of numerous
socialist parties and trade unions in Europe and America.
HENRYK
GROSSMAN
Works: Histoire des
républiques italiennes au moyen âge, 16 vols. (Zurich and
Paris 1807-18; new ed., 10 vols., Paris 1840-41; abridged ed. as Histoire
de la renaissance de la liberté en Italie, 2 vols., Paris
1832), tr. as A History of the Italian Republics
(London 1832;
reprinted in Everyman's Library, London 1907); Histoire des
Français,
31 vols. (Paris 1821-44, abridged ed. as Precis de
l'histoire des
Francais, 2 vols., Paris 1838, and a third volume by E.
Robinet, 1844); De la littérature du midi de l'Europe,
4 vols. (Paris 1813; 4th ed., 2
vols., Brussels 1837), tr. by Thomas Roscoe as Historical
View of the
Literature of the South of Europe, 2 vols. (4th ed. London
1853); Etudes sur les sciences sociales, 3 vols.
(Paris 1836-38).
Consult:
Sails, Jean R. de, Sismondi, 1773-1842; Bibliotheque
de la Revue de
Litterature Comparée, vol. lxxvii, 2 vols. (Paris 1932);
Aftalion,
Albert, L'oeuvre économique de Simonde de Sismondi
(Paris 1899);
Grossman, Henryk, Simonde de Sismondi et ses théories
économiques,
Warsaw, Bibliotheca Universitatis Liberae Polonae, no. 11 (Warsaw
1924); Tuan, Mao-Lan, Simonde de Sismondi as an Economist,
Columbia
University, Studies in History, Economics and Public Law,
no. 298 (New
York 1927); Festy, O., "Sismondi et la condition des ouvriers français
de son temps" in Revue d'économie politique, vol.
xxxii (1918) 46-72,
119-36; Lenin, V. I., "K kharakteristike ekonomicheskogo romantizma"
(Characteristics of economic romanticism) in his Sochineniya,
vol. ii
(2nd rev. ed. Moscow 1926) p. 9-115; Jeandeau, René, Sismondi,
précurseur de la législation sociale contemporaine (Bordeaux
1913);
Pellegrini, Carlo, II Sismondi e la storia delle letterature
dell'
Europa meridionale, Biblioteca dell' Archivum romanicum, 1st
ser., vol.
vii (Geneva 1926); Gooch, G. P., History and Historians in
the
Nineteenth Century (London 1913) p. 165-68.


 

Henryk
Grossman Internet Archive | Political
Economy Archive
