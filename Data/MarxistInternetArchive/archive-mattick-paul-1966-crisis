
Published: in Contemporary Issues (New York) No 52, Spring 1966.
Transcription: Adam Buick
HTML-markup: Jonas HolmgrenThe Orwellian principles of newspeak, which include the
interchangeability of words to enable them to express the opposite of what they
appear to be, dominate today's political vocabulary. Just as peace stands for
war and wars are fought to maintain peace, so many other terms have come to
designate their opposites. It is then not surprising to encounter a book which
declares that the opposite of crisis, namely prosperity, is itself in crisis.
[Prosperity in Crisis by Joseph M. Gillman. Marzani & Munsel.
New York, 1965. $5.00].Gillman's new book develops the thesis "that the capitalist
economies experience booms and depressions because they operate within a
framework in which more profits, or social surplus, is produced than can be
absorbed in the expansion of their productive assets on a continuing basis, and
that this condition has worsened under the impact of the new technologies and
concentrated business organization." In recent times, Gillman says, there has
been no major depression because uninvestible capital has been absorbed in
government-induced unproductive and wasteful expenditures. Unless new
substitutes for private investments are found, these expenditures must
continuously be increased to avoid another depression. In this, he says, "lies
the crisis of America's postwar prosperity. It is a new form of economic
crisis."This "new form of economic crisis" does not seem to be a crisis
at all. If a depression can be prevented by way of government expenditures, and
if there is a large enough surplus to allow for such expenditures, why should
not prosperity continue unabated? Although Gillman professes to being a
Marxist, his theory is the exact opposite of Marx's theory of accumulation. To
put it briefly—for Marx, the capitalist system experiences periods of
crisis and finally comes to its end because of a lack of profit, or
surplus-value, relative to the accumulated capital. For Gillman—to put it
just as briefly—capitalism is in crisis because "the economy's
profit-producing potentialities are greater than its profit-consuming
potentialities." For Marx, the rate of profit falls in the course of capital
formation; for Gillman there is too much "excess social surplus," which
condemns the system to increasingly larger unproductive expenditures. If that
were all that bothers the system it really has no problem, for nothing could be
easier than to increase unproductive expenditures.Of course, Marx could be wrong and Gillman right and the "law of
the falling rate of profit" could very well have lost its validity. Indeed it
has, says Gillman, because "present-day industrial technology is to an
ever-increasing degree capital-saving as well as labor-saving." In Marx's view,
we recall, capital invested in means of production grows more rapidly than that
invested in wages—expressing by this means the growing productivity of
labor. Because profits are surplus-labor, the relative reduction of labor
depresses the rate of profit since the latter, though exclusively derived from
the capital invested in wages, is measured on total capital investments. In
Gillman's view, the growing discrepancy between the capital invested in means
of production and that invested in labor-power is halted by technological
changes which allow for a cheapening of the productive apparatus, in short, for
capital-savings, which arrest the tendential fall of the rate of profit.Capital development, however, has always been both a
labor-saving and a capital-saving process and has, in this manner, maintained
the profitability of capital despite the tendential fall of the rate of profit
inherent in capital production. While labor-saving devices fostered the more
rapid increase of capital investments relative to wages, capital-saving devices
diminished to some extent the widening gap between the money invested in labor
and that invested in capital. This could not be otherwise because the
increasing productivity of labor also affects the production of the means of
production. Although recognized by Marx as a "countertendency" to the falling
rate of profit, he considered its effectiveness as temporary; in the long run
the tendential fall of the rate of profit would lead to the downfall of
capitalism.In Gillman's view, the capital-saving effects of the new
technologies go beyond anything Marx envisioned himself, and by affecting what
Marx called the "organic composition of capital," i.e., the proportional
relationship between capital and labor in both technical and value terms,
arrested not only the decline of the rate of profit but increased the
profitability of capital to an extent that it now exceeds investment
opportunities. Automation in particular, according to Gillman, "is potentially
the most drastic capital-saving as well as labor-saving device yet invented by
man." Although this is undoubtedly true with respect to labor-savings, it is
far from obvious with regard to capital-savings. A more extensive automation
would require an enormous amount of new capital and an even larger destruction
of old capital. In any case, as Gillman says, "the capital-saving aspect of
automation has not been adequately documented." It is quite immaterial,
moreover, whether or not automation is capital-saving because its great
labor-saving capacity is reason enough to prevent its large-scale application
to capitalist production. Where there is no labor there can be no
surplus-labor. One has only to assume complete automation to recognize that it
would mean the end of capitalism. Increasing automation can only mean the
continuous reduction of labor and therefore of surplus-labor, or profits.Capital-saving and labor-saving innovations are actually one and
the same process. It means that relative to the quantities of commodities
produced, less and less labor is employed in all branches of production and
thus also in those manufacturing capital goods. Such goods are themselves
nothing but congealed surplus-labor. If there is less and less congealed
surplus-labor, capital finds itself in a state of disinvestment. To accumulate
capital, the mass of capital must increase despite and because of the
cheapening of the means of production, or of the constant capital, to use
Marxian terminology. Capital is not only a production relation but also a value
relation. No matter how much the constant capital is cheapened by
capital-saving innovations, to speak of capital formation, the mass of capital
in any one cycle of production must be larger than it was in a previous cycle.
It is the same with labor, with variable capital, in Marx's terms; no matter
how many workers may be displaced relative to the increase of capital, their
absolute number increases with the accumulation process.The source of Gillman's reversal of Marx's theory of capital
formation may be found in his inability clearly to distinguish between
production and capitalist—or value—production. He says, for
instance, that "automation minimizes the rate of investment and hence retards
the rate of economic growth in physical terms." Yet automation necessitates new
investments and is resorted to in order to increase the profitability of
capital. Even though it can only lead into a cul-de-sac, it is nonetheless a
way to continue the accumulation process. It is the need to increase profits
which induces automation, even though it will, in turn, reduce the
profitability of capital still further. Capitalists are not interested in
either the enhancement or the retardation of the rate of economic growth in
physical terms; their sole concern is with the value of their capital, its
preservation and expansion, and it is this concern which determines the rate of
growth and the physical character of the production process.While for Gillman capital-saving and automation minimize the
rate of investments, they also in his view produce an abundance of profit which
cannot find fresh capitalization because of the declining growth rate. On this
point Gillman thinks that he is in disagreement with Keynes (who traced the
decline of investments to a superfluity of capital) by saying that, contrary to
Keynes, "the rate of investment declines not so much because capital becomes
'less scarce', but because it becomes more efficient,"—without noticing
that if it becomes more efficient it also becomes 'less scarce'. Despite
Gillman's criticisms of Keynes, which take up a considerable part of his book,
it is more from Keynes than from Marx that his own position derives. In fact,
Keynes is far closer to Marx than is Gillman, if only because the former at
least bases his theory on the diminishing marginal efficiency, or
profitability, of capital, whereas in Gillman's view capital suffocates in its
own efficiency and profitability.Except with respect to some practical policy proposals and the
terminology employed, there is not much difference between Keynes' and
Gillman's position. Both decry the declining rate of investments, which leaves
a mass of idle capital because profits are not reinvested. They are not
reinvested, according to Gillman, because the efficiency of capital is too
great to allow for its further expansion. It should be obvious that this could
only mean that additional investments would not yield additional profits; i.e.,
that a lack of profitability leads from expansion to stagnation. But the
obvious is not for Gillman; for him the system is already too productive and
too profitable to be able to afford still more affluency under the given
conditions of capitalistic distribution and the principle of capital
concentration.Capital not reinvested, or invested capital not used, ceases to
be productive, that is, profit-yielding capital, and becomes a hoard. To the
extent that the hoard grows, capital itself declines, as may be gathered from
any depression. The hoard itself, in so far as it appears in tangible form,
depreciates, and in its money form it may be lost partly through inflation and
partly by being consumed. Far from indicating a superfluity of profits, idle
capital is itself an expression of the declining profitability of capital. In
Gillman's view, however, the hoard represents a "social surplus" to be
"siphoned off" by government to supplement private spending by
government-induced production in order to secure a necessary level of
employment. "Deficit spending", he writes, "has a special virtue in that it
involves borrowing from the actual or potential pool of the community's savings
which may not be planned to materialize as private investment. Deficit spending
thereby becomes a withdrawal from the community of actual or potential social
surplus."However, there is not such an "actual or potential social
surplus" in the form of the "community's savings." There are only idle
productive resources which cannot be profitably employed. The money which the
government "borrows" to set them to work, the government itself creates with
the aid of the banking mechanism and through the extension of the credit
system. All capitalist production is based on credit, but credit dries up with
the decreasing profitability of capital. Where private credit expansion comes
to a halt, government credit expansion steps up. The '"prosperity" thus brought
about is not immediately different from that created by private credit
expansion, only that the "profits" made in such a "prosperity" take on the form
of the national debt. These "profits" are not realized on the market, for there
is no market far government-induced production, but are "realized" in form of
claims on the national debt. Whether these claims are eventually repudiated or
actually honored, in either case they reduce the profitability of the total
private capital and to that extent hamper its further accumulation. The
apparent prosperity is thus itself an additional sign of the decreasing
profitability of capital.In view of capitalism's real development, Gillman's reversal of
Marx's theory of accumulation is entirely out of place. And this the more so
because it rests upon a mistaken identification of productive capacity with the
capacity to produce profits. It should be pointed out, however, that in spite
of his affinity with Keynesian economics, Gillman does not share the Keynesian
illusion of the reformability of capitalism. For him, capitalism is in crisis
because it cannot use its productive powers to alleviate the capital needs of
underdeveloped countries and to improve the living standards in the developed
nations; instead it is condemned to dissipate its great wealth in war and
waste-production. In order to utilize the new productive powers for the
alleviation of human misery, Gillman sees the necessity for a radical
alteration of society's political and economic structure. He would, however,
have found a better substantiation for this conviction if he had tried to learn
less from
Keynes and more from Marx. Last updated on: 6.22.2018
Paul Mattick Archive | MIA
