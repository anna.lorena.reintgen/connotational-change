Dora B. Montefiore, New Age September 1903Source: New Age, p. 571, 3 September 1903;
Transcribed: by Ted Crawford.A very useful publication for women, seeking employment is The Central Bureau’s Employment List, issued on the first and third Fridays in the month. All the issues contain lists of vacant situations, and advertisements in poster form; while in the issue published on the first of the month there are some very sensible articles on women’s work. Technical teachers, librarians, and laundry matrons seem to be in demand, while lady-cooks can earn salaries from £25 to £4o. No one seems needing women gardeners, though seven trained women seek situations in that capacity. Several women, living in the country, have told me what a pleasant and profitable arrangement a woman gardener has proved. Many a healthful summer hour has been spent, working and chatting with the woman gardener; and many a winter evening, which would otherwise have been lonely, has been cheered by the bright companionship of a healthy, cultured girl. Amongst the new professions for women is that of “courier-maid”; and though none are asked for in this month’s employment list, I happen to know that girls well-equipped for such positions are in demand by American and other travellers. A good conversational knowledge of modern languages, and some artistic sympathies are, of course, among the desiderata of those applying for such posts; but they must be also thoroughly good women of business, and able to protect their own interests as well as those of their employers.Such is the title of “The Letters of Callicrates,” by Mr. W.T. Stead, first published in the Review of Reviews Annual for 1903, are now re-published in book form by Grant Richards at half a crown. The letters, written by her Prime Minister to Queen Dione, describe the impression made on the former by a study of our civilisation; and more especially of the position of women in the modern world; and they form an excellent propaganda pamphlet which, may be, many who will not tackle a serious article or listen to a lecture will read. I must own its present title, “The Despised Sex,” does not appeal to me by its fitness, as many of our sex get too much homage and deference, are in fact the spoilt children of society, whilst others have to bear all the burden, which is the outcome of systematic exploitation rather than of contempt. Neither would I have suggested the quotation from the laws of Manu which is to be found on the title-page; for in those same laws we also read that woman is during every stage of her life to be under the domination of man; as a girl she is to be under her father, as a wife under her husband, and a a widow under her son. Such an arrangements can scarcely make for the “honouring “ of women, or the consequent “rejoicing” of the Gods.I have had sent me by a correspondent a most encouraging account of a visit to a Co-educational Grammar School in the Midlands; which Grammar School (an old foundation, dating from 1698, and used entirely for boy scholars was but recently in an unsatisfactory and moribund state. It was restored rather over a year ago to life and renewed vigorous work by the adoption of the co-educational principle; as it appears there was at that time a real need in the neighbourhood for a higher school for girls. The Governors had heard of the co-educational experiment at Keswick and elsewhere, and the Chairman, Mr. Thurston, made careful enquiry with the result that after carrying out certain structural alterations in the school buildings the school was thrown open to girls as well as boys. “We have more than doubled our number of scholars in one year,” said the head master; “and there has not been a hitch from first to last. It has been the pleasantest year I have spent as a schoolmaster, and there has been added a distinct stimulus to the work of the boys by the presence of the girls, and the knowledge that they had now to compete on even terms with a set of conscientious little workers.” Canon Rawsley, who was called upon to give an address, as he has had experience, not only with American schools, but also with the Keswick Grammar School, said, amongst other things: “That co-education was home and family life brought into school, which brought with it greater consideration for one another; and greater gentleness of manners and speech. The boys educated the girls, and the girls the boys, without knowing it. The sexes policed one another. The boy ceased to be a kind of hero on a pedestal, and the girl ceased to be a mystery or only a toy.” It seems that there are twenty or thirty old grammar schools in Cheshire only, that need setting on their feet by the adoption of some more up-to-date curriculum. They might do worse than follow the excellent example of the Lymm Grammar School, and give the girls in their neighbourhood equality of opportunity, as far as studies are concerned, with the boys.Dora B. Montefiore. 
 Dora B Montefiore Archive
