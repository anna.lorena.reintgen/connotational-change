 

THE sleepy porter of the Hotel Angleterre I  fumbled his keys and
      finally got the door open.  My two soldiers rode away waving
      their hands cheerfully; I never saw them again. The porter took
      my passport and put it in the safe without looking at it and
      shuffled along upstairs ahead of me until we reached a large
      vault-like suite on the third floor.

  It was four o'clock and not for many hours would it be
  light--Petrograd is very far north to a New Yorker. By December when
  things had reached such a desperate state that we seldom had
  artificial light at all, because there was no coal to run the power
  plants, we seemed to live in perpetual darkness.  I have often
  purchased, in the deserted churches, holy candles which were
  designated to be burned before the shrines of saints but which were
  carried home surreptitiously in order to see to write.  But in
  October the lights were still running.  When the porter pressed the
  button I blinked painfully under the dazzling blaze of sparkling
  old-fashioned crystal candelabra.

  I looked around at the great unfriendly room in which I found
  myself.  It was all gold and mahogany with old blue draperies; most
  of the furniture was still wearing its summer garments.  I had a
  feeling that no one had lived in this room for years--it had a
  musty, unused smell.  Lost in a remote corner of the room adjoining
  was my bed and beyond that an enormous bath-tub, cut out of solid
  granite, coldly reflected the light.

  For all this elegance? "Thirty rubles," the porter murmured, still
  half awake.

  There was a large sign above my bed forbidding me to speak
  German--the penalty being fifteen hundred rubles. I had no desire to
  break the law. It seemed a lot to pay for so small amount of
  enjoyment, I thought as I slid bravely down between the icy sheets
  and fell into a dead slumber.

  I was awakened by loud knocks on my door. A burly Russian entered
  and began to bellow about my baggage. I rubbed my eyes and tried to
  make out what language he was speaking and suddenly I realised--he
  was speaking German!  I pointed to the sign and he shook with
  laughter.

  I found out afterwards that no one pays any attention to signs in
  Russia.  They read the signs and then use their own judgment. Take
  language, for instance.  Few foreigners ever learn to speak Russian;
  on the other hand, they are very apt to have at least a smattering
  of French or German. Solution: speak the language you understand. If

you tell them German is an enemy language they will tell you that they
      are not at war with the language. Furthermore, they have found
      their use of it very valuable in getting over propaganda into
      Austria and Germany.

  Just across from my window, St. Isaac's Cathedral loomed blackly and
  I watched the bellringers in the ponderous cupolas, bell-ropes tied
  to elbows, knees, feet and hands, making the maddest music with
  great and little bells.  The people passing looked up also and
  occasionally one crossed himself.

  Out on the streets I wandered aimlessly noting the contents of the
  little shops now pitifully empty. It is curious the things that
  remain in a starving and besieged city. There was only food enough
  to last three days, there were no warm clothes at all and I passed
  window after window full of flowers, corsets, dog-collars and false
  hair!

  This absurd combination can be accounted for without much scientific
  investigation. The corsets were of the most expensive, out-of-date,
  wasp-waist variety and the women who wear them have largely
  disappeared from the capital.

  The reason for the false hair and dog collars was equally plain.
  About a third of the women of the towns wear their hair short and
  there is no market for the tons of beautiful hair in the shops,
  marked down to a few rubles. An enterprising dealer in such goods
  could make a fortune by exporting the gold, brown and auburn tresses
  of the shorn and emancipated female population of Russia and selling
  them in America, France or some other backward country where women
  still cling to hairpins.

  As for the dog collars, just imagine any one being a dog-fancier or
  even a fondler of dogs to the extent of purchasing a gold-rimmed or
  a diamond-studded collar while a Revolutionary Tribunal is sitting
  just around the corner.  Whatever class lines there were among dogs
  fell with the Tsar.

 And the masses of flowers. Horticulture had reached a high state of
 development before the revolution.  This  was  especially true of
 exotic flowers because of the extravagant tastes of the upper
 classes. With the change in government the demand for these luxuries
 abruptly ceased; but there were still the hot-houses, there were
 still the old gardeners.  It is impossible to break off old
 established things  in  the  twinkling of an eye. Habits of trade are
 as hard to break as any other habits of life.  So the shops continued
 to be filled with flowers. On the Morskaya where so much bitter
 street fighting occurred were three flower-shops --in them were
 displayed always the rarest varieties of orchids.   And in those
 turbulent January days suddenly appeared--white lilacs!

  These strange left-overs of another time cropped up everywhere
  making sharp contrasts. There were the men, for instance, who stood
  outside of the palaces and the big hotels, peacock feathers in their
  round Chinese-looking caps and wearing green, gold or scarlet
  sashes. Their duty had been to assist people who alighted from
  carriages, but now grand personages never arrived, and still they
  stood there, their sashes bedraggled and faded, their feathers
  ragged and forlorn. As helpless they were as the old negroes of the
  South who clung to their slavery after the emancipation.

  And in contrast were the waiters bustling about in the restaurants
  inside of the very buildings where the svetzars stood
  before the doors like courtiers without a court.  They ran their
  restaurants co-operatively and at every table was a curt little
  notice.

  "Just because a man must make his living by being a waiter do not
  insult him by offering him a

tip."

  Petrograd is impressive, vast and solid.  New York's high buildings
  have a sort of tall flimsiness about them that is not sinister;
  Petrograd looks

as if it were built by a giant who had no regard for human life. The
  rugged strength of Peter the Great is in all the broad streets, the
  mighty open spaces, the great canals curving through the city, the
  rows and rows of palaces and the immense facades of government
  buildings.  Even such exquisite bits of architecture as the graceful
  gold spires of the old Admiralty building and the round blue-green
  domes of the Turquoise Mosque, cannot break that
  heaviness.... 

  Built by the cruel wilfulness of an autocrat, over the bodies of
  thousands of slaves, against the unanimous will of all grades of
  society, this huge artificial city, by a peculiar irony has become
  the heart of world revolution; has become Red
  Petrograd!

  There were wonderful tales about the defeat of Korniloff and what
  they described as a "new kind of fighting."  Every one was anxious
  to tell his version of how the scouts went out and met the army of
  the counter-revolutionists and fraternised with them and overcame
  them "with talk" so that they refused to fight and turned against
  their leaders.  There was little variation, in short, the story was
  this:

  The scouts came upon the hostile army encamped for the night and
  went among them saying: "Why have you come to destroy the
  revolution?"  The hostile army indignantly denied the charge,
  claiming that they had been sent to "save" the revolution.  So the
  scouts continued to argue. "Do not believe the lies your leaders
  tell you. We are both fighting for the same thing.  Come to
  Petrograd with us and sit in our councils, learn the truth, and you
  will abandon this Korniloff who is attempting to betray you."

  Accordingly delegates were sent to Petrograd. When they reported to
  their regiments the two armies joined as brothers.


  While all this fraternising was going on and no one was sure of its
  results, revolutionists in Petrograd worked feverishly.  In one
  place they told me that they had manufactured a whole cannon in
  thirty hours and the trenches that encircled the city were dug over
  night.

  Ugly tales went round about the fall of Riga. Most Russians, with
  fairly good reason, believe that it was sold out.  It fell just
  after General Korniloff said in public:  "Must we pay with Riga the
  price of bringing the country to its senses?"

  No one ever explained the reason for the vague order given to the
  retreating Russian army: "Go north and turn to the left!"
  Bewildered soldiers retreated in confusion for days without officers
  or further instructions, finally entrenching themselves, forming
  Soldiers' Committees, beginning to fight again....

  Officers returning a week or two afterwards told an amazing
  story. It was printed in the conservative paper Yetcherneie
  Vremya and I heard it twice myself from men who were captured,
  and I believe it to be true.  When Riga fell many prisoners were
  taken.  It was towards the end of the week.  On Sunday there were
  services at which the Raiser appeared and made a speech to the
  Russian soldiers.  He called them "dogs" and berated them for
  killing their officers whom he claimed were brave and admirable
  gentlemen, commanding his respect.  Consistent with Prussian
  military ideals, he made a practical demonstration by allowing the
  officers full freedom and issuing orders that the common soldiers
  should have little food and hard work and in certain cases a
  flogging. The hundreds of thousands of tubercular Russian prisoners
  now returning to Russia are evidence of how well the instructions
  were carried out. In his speech to the soldiers in the church the
  Raiser said: "Pray for the government of Alexander III., not for
  your present disgraceful government."

  That evening he dined the officers and they came back into Russia
  and explained that we did not "understand" the Kaiser....

  In Petrograd one of the things that strike coldness to one's heart
  are the long lines of scantily clad people standing in the bitter
  cold waiting to buy bread, milk, sugar or tobacco. From four o'clock
  in the morning they begin to stand there, while it is still black
  night.   Often after standing in line for hours the supplies run
  out.  Most of the time only one-fourth pound of bread for two days
  was allowed; and the soggy black peasant's bread is the staff of
  life in Russia--it is not a "trimming" like our American
  bread. Cabbage is also a staple diet.

  On my second night in Petrograd I met a Russian from New York.  We
  strolled up and down the Nevsky Prospect.  All Russia promenades the
  Nevsky; it is one of the great streets of the world. My friend
  wanted to be hospitable as all Russians are, but he was very
  poor. We passed a little booth and spied a few bars of American
  chocolate--5c bars. He enquired the price--seven rubles!
  With true Russian recklessness he paid out his last kopeck and said:
  "Come, let us walk up and down once again, it is only a
  mile. .. ."

  Petrograd with food for three days was not tragic or sad.  Russians
  accept hardships uncomplainingly.  When I first went there I was
  inclined to put it down to servility, but now I believe it to be
  because they have unconquerable spirit.    Weeks  at a stretch the
  street cars would not run.  People walked great distances without a
  murmur and the life of the city went on as usual. It would have
  upset New York completely, especially if it happened as it did in
  Petrograd that while the street cars were stopped, lights and water
  also were turned off and it was almost impossible to get fuel to
  keep warm.

  The most remarkable thing about Russians is this  wonderful
  persistence.   Theatres  somehow managed to run two or three times a
  week. The Nevsky after midnight was as amusing and interesting as
  Fifth Avenue in the afternoon.  The cafes had nothing to serve but
  weak tea and sandwiches but they were always full.  A wide range of
  costumes made the picture infinitely more interesting.  There is
  practically no "fashion" in Russia.  Men and women wear what they
  please. At one table would be sitting a soldier with his fur hat
  pulled over one ear, across from him a Red Guard in rag-tags, next a
  Cossack in a gold and black uniform, earrings in his ears, silver
  chains around his neck, or a man from the Wild Division, recruited
  from one of the most savage tribes of the Caucasus, wearing his
  sombre, flowing cape....

  And the girls that frequented these places were by no means all
  prostitutes, although they talked to everybody.  Prostitution as an
  institution has not been recognised since the first revolution. The
  degrading "Yellow Tickets" were destroyed and many of the women
  became nurses and went to the front or sought other legal
  employment. Russian women are peculiar in regard to dress. If they
  are interested in revolution, they almost invariably refuse to think
  of dress at all and go about looking noticeably shabby--if they are
  not interested they care exceedingly for clothes and manage to array
  themselves in the most fantastic "inspirations." I shall
  always remember Karsavina, the most beautiful dancer in the world,
  in those meagre days, dancing to a packed house.  It was a
  marvellous audience; an audience in rags; an audience that had gone
  without bread to buy the cheap little tickets. I think Karsavina
  must have wondered what it would be like to dance before that tired,
  undernourished crowd instead of her once glittering and exclusive
  little band of nobles.

 When she came on it was as hushed as death. And how she danced and
 how they followed her! Russians know dancing as the Italians know
 their

operas; every little beautiful trick they appreciate to the
      utmost. "Bravo! Bravo!" roared ten-thousand throats. And when
      she had finished they could not let her go--again and again and
      again she had to come back until she was wilted like a tired
      butterfly.  Twenty, thirty times she returned, bowing, smiling,
      pirouetting, until we lost count. ... Then the people filed out
      into the damp winter night, pulling their thin cloaks about
      them.

  In Petrograd were flags--all red.  Even the statue of Catherine the
  Greet in the little square before the Alexandrinsky Theatre did not
  escape. There stood Catherine with all her favourite courtiers
  sitting at her feet and on Catherine's sceptre waved a red flag!
  These little visible signs of the revolution  were  everywhere.
  Great  blotches marked the places where imperial insignia had been
  torn from the buildings.  Mild mannered guards patrolled the
  principal corners, trying not to offend anybody. And over it all
  stalked King Hunger while a chill autumn rain soaked into the
  half-fed shivering throngs that hurried along, lifting their faces
  and beholding a vision of world democracy. ...


Six Red Months in Russia contents

Next section

The Marxist writers' Archives
