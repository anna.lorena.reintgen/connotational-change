
Source: Fraser, C. (1998). "Time of the Whirlwind." In Revolution, She Wrote (pp. 351 - 354). Seattle, WA: Red Letter Press.
Transcription/Markup: Philip Davis and Glenn Kirkindall
Copyleft: Internet Archive (marxists.org) 
2014. Permission is granted to copy and/or distribute this document 
under the terms of the GNU Free Documentation License.Our house hosted a Soviet guest, a sculptor, during the
Goodwill Games in August, and what a juicy tidbit
that proved to be for dinner table conversation and talk show chatter.Seattle was agog over the thousands of locals who opened
their homes to the vast influx of Soviet athletes, artists and
intellectuals, officials, experts, trade negotiators, and just plain
workers whom Aeroflot deposited on our freshly scrubbed
doorsteps.We didnt need to go to the USSR—it came to us.
Moscow-on-Puget Sound was a moveable cultural feast: the
Bolshoi Ballets Ivan the Terrible; Prokofievs opera War and
Peace; Chekhovs The Three Sisters; Eugenia Ginzburgs Into
the Whirlwind, performed by Moscows Sovremenik theatre
and depicting the plight of women prisoners and their male
jailers in the darkness-at-noon decades of Stalins purges;
exhibits of centuries of Russian art climaxed by the
Constructivists and their Art Into Life creations.All art flourished in the welcoming climate encouraged
by Lenin and Trotsky after the revolution, before Lenin died.Before the invading armies of U.S. and European
capitalists drained the land.Before the civil war provoked by imperialism exhausted the people and drowned a huge sector of young leaders in
blood.Before the defeat of the German revolution, which the
Bolsheviks utterly depended on for economic and political
help.Before the crystallizing of a caste of bureaucrats, grouped
around Stalin, who exploited the ravaged country as they
scrambled for power positions. (You always need functionaries
to run the food banks, employment agencies, housing
departments, and all the rest.)Before Trotskys Left Opposition to Stalin was dirty tricked
by officials intoxicated with their vodka-soaked
privileges.And before Trotskys protest against Stalins abandonment
of revolution abroad, and democratic, commonsensical
measures at home, was silenced, by exile and later by the
assassins ax.Not only Trotsky died. The counterrevolutionary
hatchet men of Stalinism executed the entire remaining
leadership of the October Revolution and killed or imprisoned
millions who refused to surrender principles and humaneness.
The prime products of the bureaucracy were corpses, the
death of art (suffocated in the one-size-fits-all shroud of
socialist realism), and the cynical perversion of Marx and
Lenins ideas.
So how did my discussions go with the artists and actors?
With extreme difficulty. Language differences are an
impenetrable, nationalistic barrier to an exchange of thinking,
and what the Goodwill Games gurus neglected—or refused?
or were forced into failing to provide?—were translators. Our
lives centered on competitive hunts for Russian-English
translators.
You may be sure that visitors who came to talk about
trade and capital investment were accompanied by a decent
contingent of translators, as were the pampered athletes. It
was the artists and journalists and just-folks who suffered. Of
course.Happily we discovered that pantomime, sound effects
and pictures are incredible lubricants to communication.
The Soviets were delighted and amazed to find socialists
and lower-case c communists in this bucolic corner of
Yankeeville.They hated Stalin and knew little about Trotsky; what
they did know about Trotsky was wrong and untrue, hardly
surprising when Gorbachev, the new Bonaparte, stalls over
rehabilitating him.
They were puzzled and divided over feminism and
lesbian/gay rights, though the 1917 Soviet Constitution was
the first in history to legitimize these paramount social and
human interests.Comfortable with all races and my Jewish heritage, they
were as yet unscathed by recent eruptions of Soviet anti-
Semitism.They love their country, feel free to criticize it, hope for
the best, fear the future, were thrilled to be here and partake
of our bounty. But they deplored the seamy slums of paradise
that we showed them. These people are not going to trade off
their own and their childrens birthrights—insured jobs,
housing, education, medical care, abortion—for the terrible
risks inherent in an unbridled profit system.
The Soviet Union can never go whole-hog capitalist
without a century of convulsions and turmoil.
And we of the West, which fattened off the plunder and
cheap labor of the world, will never find tranquility either
until we merge our wealth and know-how with the socialist
concern for all people. Then, together, we will replace the free

Clara Fraser Archive
