William Morris. Commonweal 1890
“Development of Modern Society”,
    Part 1
Author:
William Morris
Source:
Commonweal,
        Volume 6, Number 236, pp.225-6
19 July 1890 
        (the first of five parts.)
Transcribed by:
Ted Crawford
Proofing and HTML:Graham Seaman
ALL the progressive races of man have gone through a stage of development
during which society has been very different to what it is now. At present
there is a very definite line of distinction drawn between the personal life
of a man and his life as a member of society. As a rule, the only direction in
which this social life is felt is in that of his nearest kindred—his wife,
children, parents, brothers and sisters. This is so much the case that we
to-day have given to the word relations (which should mean all those 
with whom
a man has serious and continuous dealings) a fresh meaning, and made it
signify only those near members of kinship aforesaid. For the rest most
civilised men acknowledge no responsibility. Though the word State is in
everybody's mouth, most people have but the vaguest idea as to what it means;
it is even generally considered as a synonym to the Government, which also
indicates either the heads of one of the political parties, or the vague
entity called by Carlyle the parish constable—in other words, the executive
power of the ruling classes in our society. So little do we feel any
responsibilities to this hardly conceivable thing, the State, that while few
indeed feel any loyalty towards it, most men do not realise it sufficiently
even to feel any enmity against it—except, perhaps, when the tax-gatherer’s
hand is on the knocker.Now all this is so far the result of a long series of history, which I must
just hint at before one comes to the condition of the workman during its
different stages,—a series of events which tended to give to the word
property the meaning which it now has; a series of events which tended more
and more to consider things as the important matter of consideration rather
than persons; which I may illustrate by the fact that nowadays the law looks
upon the estate as of more importance than the user of it, as for instance in
the case of the estate of a lunatic, which it will defend to the utmost
against all attacks, and treat as if it had a genuine life and soul capable of
feeling all injuries and pains, while all the time the lunatic is under
restraint.I will now contrast this entire ignoring of the community (for that will be
a better word than State to use at present) with the conditions under which
men lived in earlier ages of the world, and through which, as I have said, all
the progressive races have passed, some of them so early that when we first
meet them in history they are already passing out of it into the next
development. In this early period the individual is so far from feeling no
responsibility to the community, that all his responsibilities have relation
to the community. Indeed, this sense of responsibility, as we shall see later
on, has only been completely extinguished since the introduction of the
present economical and political system—since the death of feudality, in
short: but in the period I am thinking about it was a quite unquestioned
habit. The unit of society, the first, and in the beginning the only bond, was
the narrowest form of clan, called the gens. This was an association of
persons who were traceably of one blood or kinship. Intermarriage between its
members was forbidden, or rather was not even dreamed of: a man of the Eagle
gens could have no sexual intercourse with an Eagle woman, nor thought of it.
All property was in common within the gens, and descent was traced, not
through the father, but through the mother, who was the obvious parent of the
child. Whatever competition (war, you may call it, for competition was simple
in those days), was outside the group of blood relations, each of which felt
no responsibility for other groups of their members. But the fact that
intermarriage was impossible within these groups brought about a larger
association. Since an Eagle could not marry an Eagle, the Eagles must either
get their wives by violent robbery in a haphazard fashion from outsiders, or
have some other society at hand into which they could marry, and who could
marry into their society. It used to be thought that the violent robbery was
the method, but I believe the second method was the one used. There were
groups of neighbours at hand who were recognised as belonging to the same
stock, but who were not too near in blood to make marriage impossible. Between
these groups there was affinity, therefore; the Eagles could intermarry with
the Owls, the Sparrows, the Cats, or what not, according to a somewhat
intricate system, and this quite without violence. And also between the clans
or gentes who composed these tribes there would be no war, and the use of
whatever land they fed their stock upon or cultivated (for in some places or
ages this gentile-tribal system lasted well into the agricultural period) was
arranged peaceably in a communal method.Now the tribe in which a common ancestor (worshipped as a god) was always
assumed, and was generally a fact, tended to federate with other tribes who
still felt that they belonged to a common stock, who thus formed an
association called by our ancestors the thiod, or people; an association much
looser, of course, than that of the gens or tribe, but like those, founded on
an idea of common kindred; founded on the personal kinship of all its members
to the god-ancestor, and not on locality or the holding of certain property or
position. The offices of the body, under whatever names they went, were
appointed by the tribesmen for their personal qualities to perform definite
duties. There was no central executive body; every freeman had certain
necessary duties to perform, a shadow of which still exists in our jury, who
were originally the neighbours called together to utter their finding (without
direction from a judge) as to how such a one had come by his death, what was
to do between two neighbours who could not agree, and so forth. If a man was
injured, it was the duty of the members of his gens or clan to take up the
injury as an injury to the community. This is the meaning of the blood-feud of
which we hear so much in the early literature of the North, and of the Celtic
clans, and a survival of which still exists among out-of-the-way folks. The
practice of the vendetta in Corsica, e.g., does not indicate that the
Corsicans are a specially vindictive people; it is a survival of the tribal
customary law: its sentimentalising by novelists and poets is a matter of
ignorance—naturally enough, I admit. “Government” or administration, or
whatever else you may call it, was in this condition of society as direct as
it ever can be; nor had government by majority been invented—e.g., if the
clans could not agree to unite in war, the war could not go on, unless any
clan chose to go to war by itself.I am conscious of not explaining fully the difference between such a state
of society and ours; but it is indeed difficult to do so now, when all our
ideas and the language which expresses them have been for so many ages moulded
by such a totally different society. But I must, at least, try to make you
understand that the whole of the duties of a freeman in this society had
reference to the community of which he formed a part, and that he had no
interests but the interest of the community; the assertion of any such private
interests would have been looked upon as a crime, or rather a monstrosity,
hardly possible to understand. This feudal union of the tribes is the last
state of society under barbarism; but before I go on to the next stage, I must
connect it with our special subject, the condition of productive labour.With the development of the clans into federated tribes came a condition of
organised aggressive war, since all were recognised as enemies outside of the
tribe or federation; and with this came the question what was to be done with
the prisoners taken in battle, and, furthermore, what was to be done with the
tribe conquered so entirely as not to be able to defend its possessions, the
land, which it used. Chattel slavery was the answer to the one question,
serfdom to the second. You see this question was bound to come up in some
form, as soon as the productive powers of man had grown to a certain point. In
the very early stages of society slaves are of no use, because your slave will
die unless you allow him to consume all that he produces; it is only when by
means of tools and the organisation of labour that he can produce more than is
absolutely necessary for his livelihood, that you can take anything from him.
Robbery only begins when property begins; so that slavery doesn't begin till
tribes are past the mere hunter period. When they go to war they only save
their prisoners to have some fun out of them by torturing them, as the
redskins did, unless, perhaps, as sometimes happened, they adopt them into the
tribe, which also the redskins did at times. But in the pastoral stage slaves
become possible, and when you come to the agricultural stage (to say nothing
of further developments) they become necessary till the time when privilege is
destroyed and all men are equal. There are, then, three conditions of mankind,
mere gregarious organised savagery, slavery, and social equality. When you
once have come to that conclusion you must also come to this deduction from
it, that if you shrink from any sacrifice to the Cause of Socialism it must be
because we are either weak or criminal, either cowards or tyrants—perhaps
both.Well, this last stage of barbarism, that of the federated tribes, gave way
in ancient history, the history of the Greeks and Romans, into the first stage
of civilisation. The life of the city[A], and in mediaeval history into
feudalism; it is under the latter that the development of the treatment of the
conquered tribe as serfs is the most obvious; serfdom being the essence of
mediaeval society proper, and its decay beginning with the decline of serfdom.
But, undoubtedly, there were serfs in the classical period; that is to say an
inferior class to the freemen, who were allowed to get their own livelihood on
the condition of their performing certain services for them, and with a
certain status, though a low one, which raised them above the condition of the
chattel-slave, whose position was not recognised at all more than that of his
fellow labourer, the horse or the ass. The Helots, for example, were the serfs
rather than the slaves of the Spartans, and there were other instances both
among the Greeks and the Romans of labourers in a similar position.However, chattel slavery as opposed to serfdom is the characteristic form
of servitude in the ancient city life. In that life you must understand the
idea of the merging of the individual into the community was still strong,
although property had come into existence, and had created a political
condition of society under which things were growing to be of more moment than
persons. But the community had got to be an abstraction, and it was to that
abstraction, and not to the real visible body of persons that individual
interests were to be sacrificed. This is more obvious among the Romans than
the Greeks, whose mental individuality was so strong and so various, that no
system could restrain it; so that when that system began to press heavily upon
them they could not bear it, and in their attempts to escape from its
consequences fell into the mere corruption of competitive tyranny at an early
period. The Romans, on the other hand, without art or literature, a hard and
narrow-minded race, cultivated this worship of the city into an over-mastering
passion, so fierce and so irrational that their history before their period of
corruption reads more like that of a set of logical demons bent on torturing
themselves and everybody else, than a history of human beings. They must be
credited with the preservation of the art and literature of Greece (though
with its corruptions and stultification as well), and for the rest I think the
world owes them little but its curse, unless indeed we must accept them as a
terrible example of over-organisation. Of their state one may say what one of
their poets said of their individual citizens, when they were sunk in their
well-earned degradation, that for the sake of life they cast away the reasons
for living.( To be continued.) 
The William Morris Internet Archive : Journalism |  Works
