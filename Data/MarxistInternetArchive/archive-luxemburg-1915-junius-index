MIA: Marxist Writers: LuxemburgWritten: February–April 1915 (while in prison).
First Published: In Zurich, February 1916, and illegally distributed in Germany.
Source: Politische Schriften, pp.229-43, pp.357-72.
Translated: (from the German) by Dave Hollis.
Transcription/Markup: Dave Hollis, Brian Baggins, Einde O’Callaghan.
Copyleft: Luxemburg Internet Archive (marxists.org) 1996, 1999, 2003. Permission is granted to copy and/or distribute this document under the terms of the GNU Free Documentation License.Chapter 1
Chapter 2
Chapter 3
Chapter 4
Chapter 5
Chapter 6
Chapter 7
Chapter 8The voting of war credits in August 1914 was a shattering moment in the life of individual socialists and of the socialist movement in Europe. Those who had worked for and wholly believed in the ability of organized labor to stand against war now saw the major social democratic parties of Germany, France, and England rush to the defense of their fatherlands. Worker solidarity had proved an impotent myth. Rosa Luxemburg (1871-1919) had for years warned against the stultifying effects of the overly bureaucratized German Social Democratic Party and the anti-revolutionary tendencies of the trade unions that played such a large role in the party’s policy decisions. The abdication of 1914 had proved her right but had also dashed the revolutionary yearnings of a lifetime. While she was able to construct new hope from the revolutionary opportunities presented by the war, Luxemburg could not shake the knowledge that, whatever the outcome, the European working class would pay the greatest price in blood and suffering. Thrice handicapped – a woman, a Pole, and a Jew – Luxemburg was the most eloquent voice of the left wing of German Social Democracy, among the most lucid Marxists of her era, and a constant advocate of radical action. She spent much of the war in jail, where she wrote and then smuggled out the pamphlet excerpted below. Published under the name ‥Junius,” a pseudonym used by an influential English pamphleteer in the 18th century, but perhaps also a reference to Lucius Junius Brutus, a legendary republican hero of ancient Rome, the pamphlet became the guiding statement for the International Group, which became the Spartacus League and ultimately the Communist Party of Germany (January 1, 1919). Luxemburg was instrumental in these developments and, along with Karl Liebknecht (1871-1919), led the Spartacists until their assasination by the German government on January 15, 1919.Note that Chapter 4 of this book is typically not published and is difficult to find. In 2003, MIA added Chapter 4 from the Junius Pamphlet printed by The Merlin Press, London 1967. There were no copyright notices on this pamphlet. 
Luxemburg ArchiveLast updated on: 16.12.2008


Rosa Luxemburg
The Junius Pamphlet
The Crisis of German Social Democracy
(1915)

Written: February–April 1915 (while in prison).
First Published: In Zurich, February 1916, and illegally distributed in Germany.
Source: Politische Schriften, pp.229-43, pp.357-72.
Translated: (from the German) by Dave Hollis.
Transcription/Markup: Dave Hollis, Brian Baggins, Einde O’Callaghan.
Copyleft: Luxemburg Internet Archive (marxists.org) 1996, 1999, 2003. Permission is granted to copy and/or distribute this document under the terms of the GNU Free Documentation License.



Chapter 1
Chapter 2
Chapter 3
Chapter 4
Chapter 5
Chapter 6
Chapter 7
Chapter 8



The voting of war credits in August 1914 was a shattering moment in the life of individual socialists and of the socialist movement in Europe. Those who had worked for and wholly believed in the ability of organized labor to stand against war now saw the major social democratic parties of Germany, France, and England rush to the defense of their fatherlands. Worker solidarity had proved an impotent myth. Rosa Luxemburg (1871-1919) had for years warned against the stultifying effects of the overly bureaucratized German Social Democratic Party and the anti-revolutionary tendencies of the trade unions that played such a large role in the party’s policy decisions. The abdication of 1914 had proved her right but had also dashed the revolutionary yearnings of a lifetime. While she was able to construct new hope from the revolutionary opportunities presented by the war, Luxemburg could not shake the knowledge that, whatever the outcome, the European working class would pay the greatest price in blood and suffering. Thrice handicapped – a woman, a Pole, and a Jew – Luxemburg was the most eloquent voice of the left wing of German Social Democracy, among the most lucid Marxists of her era, and a constant advocate of radical action. She spent much of the war in jail, where she wrote and then smuggled out the pamphlet excerpted below. Published under the name ‥Junius,” a pseudonym used by an influential English pamphleteer in the 18th century, but perhaps also a reference to Lucius Junius Brutus, a legendary republican hero of ancient Rome, the pamphlet became the guiding statement for the International Group, which became the Spartacus League and ultimately the Communist Party of Germany (January 1, 1919). Luxemburg was instrumental in these developments and, along with Karl Liebknecht (1871-1919), led the Spartacists until their assasination by the German government on January 15, 1919.
* * *
Note that Chapter 4 of this book is typically not published and is difficult to find. In 2003, MIA added Chapter 4 from the Junius Pamphlet printed by The Merlin Press, London 1967. There were no copyright notices on this pamphlet. 


Luxemburg Archive



Chapter 1
Chapter 2
Chapter 3
Chapter 4
Chapter 5
Chapter 6
Chapter 7
Chapter 8


Chapter 1
Chapter 2
Chapter 3
Chapter 4
Chapter 5
Chapter 6
Chapter 7
Chapter 8
