
Karl Marx. Capital Volume One We have seen that the rate of surplus-value is represented
by the following formulae:The two first of these formulae represent, as a ratio of values, that which,
in the third, is represented as a ratio of the times during which those
values are produced. These formulae, supplementary the one to the other,
are rigorously definite and correct. We therefore find them substantially,
but not consciously, worked out in classical Political Economy. There we
meet with the following derivative formulae.One and the same ratio is here expressed as a ratio of labour-times, of
the values in which those labour-times are embodied, and of the products
in which those values exist. It is of course understood that, by “Value
of the Product,” is meant only the value newly created in a working-day,
the constant part of the value of the product being excluded.
In all of these formulae (II.), the actual degree of exploitation of
labour, or the rate of surplus-value, is falsely expressed. Let the working-day
be 12 hours. Then, making the same assumptions as in former instances,
the real degree of exploitation of labour will be represented in the following
proportions.


From formulae II. we get very differently,These derivative formulae express, in reality, only the proportion in which
the working-day, or the value produced by it, is divided between capitalist
and labourer. If they are to be treated as direct expressions of the degree
of self-expansion of capital, the following erroneous law would hold good:
Surplus-labour or surplus-value can never reach 100%. [1] Since the surplus-labour is only an aliquot part of the working-day, or
since surplus-value is only an aliquot part of the value created, the surplus-labour
must necessarily be always less than the working-day, or the surplus-value
always less than the total value created. In order, however, to attain
the ratio of 100:100 they must be equal. In order that the surplus-labour
may absorb the whole day (i.e., an average day of any week or year), the
necessary labour must sink to zero. But if the necessary labour vanish, so
too does the surplus-labour, since it is only a function of the former.
The ratio
can therefore never reach the limit 100/100, still less rise to 100 + x/100.
But not so the rate of surplus-value, the real degree of exploitation of
labour. Take, e.g., the estimate of L. de Lavergne, according to which the
English agricultural labourer gets only 1/4, the capitalist (farmer) on
the other hand 3/4 of the product [2] or its value, apart from the question of how the booty is subsequently divided between the
capitalist, the landlord, and others. According to this, this surplus-labour
of the English agricultural labourer is to his necessary labour as 3:1, which
gives a rate of exploitation of 300%.
The favorite method of treating the working-day as constant in magnitude
became, through the use of formulae II., a fixed usage, because in them
surplus-labour is always compared with a working-day of given length. The
same holds good when the repartition of the value produced is exclusively
kept in sight. The working-day that has already been realized in given value,
must necessarily be a day of given length.

The habit of representing surplus-value and value of labour-power as
fractions of the value created — a habit that originates in the capitalist
mode of production itself, and whose import will hereafter be disclosed
— conceals the very transaction that characterizes capital, namely the
exchange of variable capital for living labour-power, and the consequent
exclusion of the labourer from the product. Instead of the real fact, we
have false semblance of an association, in which labourer and capitalist
divide the product in proportion to the different elements which they respectively
contribute towards its formation. [3]
Moreover, the formulae II. can at any time be reconverted into
formulae I. If, for instance, we have
then the necessary labour-time being 12 hours less the surplus-labour of
6 hours, we get the following result,
There is a third formula which I have occasionally already anticipated;
it isAfter the investigations we have given above, it is no longer possible
to be misled, by the formula
into concluding, that the capitalist pays for labour and not for labour-power.
This formula is only a popular expression for
The capitalist pays the value, so far as price coincides with value, of
the labour-power, and receives in exchange the disposal of the living labour-power
itself. His usufruct is spread over two periods. During one the labourer
produces a value that is only equal to the value of his labour-power; he
produces its equivalent. This the capitalist receives in return for his
advance of the price of the labour-power, a product ready made in the market.
During the other period, the period of surplus-labour, the usufruct of the
labour-power creates a value for the capitalist, that costs him no equivalent.
[4] This expenditure of labour-power comes to him gratis. In this sense it is that surplus-labour can be called unpaid labour.
Capital, therefore, is not only, as Adam Smith says, the command over
labour. It is essentially the command over unpaid labour. All surplus-value,
whatever particular form (profit, interest, or rent), it may subsequently
crystallize into, is in substance the materialization of unpaid labour.
The secret of the self-expansion of capital resolves itself into having
the disposal of a definite quantity of other people’s unpaid labour.
 1. Thus, e.g., in “Dritter Brief an v. Kirchmann von Rodbertus. Widerlegung der Ricardo’schen Lehre von der Grundrente und
Begrundung einer neuen Rententheorie". Berlin, 1851. I shall return to
this letter later on; in spite of its erroneous theory of rent, it sees
through the nature of capitalist production.
NOTE ADDED IN THE 3RD GERMAN EDITION: It may be seen from this how favorably
Marx judged his predecessors, whenever he found in them real progress,
or new and sound ideas. The subsequent publications of Robertus’ letters
to Rud. Meyer has shown that the above acknowledgement by Marx wants restricting
to some extent. In those letters this passage occurs:

 “Capital must be rescued not only from labour, but from itself, and
that will be best effected, by treating the acts of the industrial capitalist
as economic and political functions, that have been delegated to him with
his capital, and by treating his profit as a form of salary, because we
still know no other social organization. But salaries may be regulated,
and may also be reduced if they take too much from wages. The irruption
of Marx into Society, as I may call his book, must be warded off.... Altogether,
Marx’s book is not so much an investigation into capital, as a polemic
against the present form of capital, a form which he confounds with the
concept itself of capital.” 
("Briefe, &c., von Dr. Robertus-Jagetzow, herausgg. von Dr. Rud. Meyer,” 
Berlin, 1881, I, Bd. P.111, 46. Brief von Rodbertus.) To such ideological
commonplaces did the bold attack by Robertus in his “social letters” finally
dwindle down. — F.E.
2. That part of the product which merely replaces the constant capital advanced is of course left out in this calculation.
Mr. L. de Lavergne, a blind admirer of England, is inclined to estimate
the share of the capitalist too low, rather than too high.

3. All well-developed forms of capitalist production being forms of cooperation, nothing is, of course, easier, than to make
abstraction from their antagonistic character, and to transform them by
a word into some form of free association, as is done by A. de labourde
in “De l’Esprit d’Association dans tous les intérêts de la communauté".
Paris 1818. H. Carey, the Yankee, occasionally performs this conjuring
trick with like success, even with the relations resulting from slavery.

4. Although the Physiocrats could not penetrate the mystery of surplus-value, yet this much was clear to them, viz., that
it is “une richesse indépendante et disponible qu’il (the possessor) n’a
point achetée et qu’il vend.” [a wealth which is independent and disposable, which he ... has not bought and which he sells] (Turgot: “Réflexions sur la Formation et la Distribution des Richesses,” p.11.)
 
Transcribed by Zodiac
Html Markup by Stephen Baird (1999)

Next: Chapter Nineteen: The Transformation of the Value (and Respective Price) of Labour-Power into Wages

Capital Volume One- Index
I.II.III.I.Surplus-value(s)=Surplus-value=Surplus-labourVariable CapitalvValue of labour-powerNecessary labourII.Surplus-labour   =Surplus-value=Surplus-productWorking-day Value of the ProductTotal Product6 hours surplus-labour=Surplus-value of 3 sh.= 100%6 hours necessary labourVariable Capital of 3 sh.6 hours surplus-labour=Surplus-value of 3 sh.= 50%Working-day of 12 hoursValue created of 6 sh.Surplus-labourorSurplus-valueWorking-dayValue createdSurplus-labour of 6 hoursWorking-day of 12 hoursSurplus-labour of 6 hours=100Necessary labour of 6 hours100III.Surplus-value=Surplus-labour=Unpaid labourValue of labour-powerNecessary labourPaid labourUnpaid labour,Paid labourSurplus-labour,Necessary labour