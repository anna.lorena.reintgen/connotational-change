One of the most important contributions made by
Trotsky to the theoretical storehouse of Marxism was his analysis
of the rise and development of Stalinism. He explained that the
fundamental social gains of the October revolution remained
intact, in the form of the state-ownership of the economy and the
plan of production, but that the working class had been politically
expropriated by a new ruling caste. Against those who saw
this bureaucracy as a new ruling class, Trotsky argued that it
was a parasitic growth resting on the economic base of a workers'
state, and not a class.But Trotsky also believed that the Second World
War would decide the fate of the bureaucracy one way or the
other. The end of hostilities, he suggested, would result in one
of two possibilities: firstly, the overthrow of the gains of
October, by a military defeat by nazi Germany or another
imperialism, or secondly, an internal political revolution to
establish workers' democracy. In either case, it would seal the
fate of Stalinism.Yet even the greatest political genius could
not predict the exact outcome of the war, given the enormous
multiplicity of factors that would come to bear. In 1943, the
Workers International League, forerunner of the Revolutionary
Communist Party, still reflected the pre-war thinking of Trotsky,
arguing that Stalinism would not be likely to survive the war:
'The fate of the Soviet Union rests directly on the fate of the
new wave of revolutions. Further defeats and a new epoch of
reaction would inevitably usher in the bourgeois
counter-revolution in Russia.' (Workers International News,
September 1943).But by 1945, it was becoming clear that rather
than being weakened, the Soviet Union and Stalinism would emerge
from the war stronger than before. Of all the Trotskyist
organisations, it was only the British RCP which was able to come
to terms and explain the new developments and the new balance of
forces.The Fourth International leadership, rather
than use the method of Trotsky, hung on to the letter of his
predictions. Reflecting this, for example, a document of the
American Socialist Workers Party, of September 1944, stated that
'far from having increased its independent strength, under Stalin
the Soviet Union has been debilitated and today is weaker than
ever in relation to the capitalist world.'Not to be outdone by concrete facts, such
'theoreticians' even went to the point of denying the world war
had ended. Thus, SWP leader, James Cannon, speaking in November
1945: 'Trotsky predicted that the fate of the Soviet Union
would be decided in the war. That remains our firm conviction.
Only we disagree with some people who carelessly think that the
war is over'This failure to face up to the new reality was
characteristic of all the so-called theoreticians of Trotskyism:
Ernest Mandel, Michael Pablo, Pierre Frank, James Cannon, Gerry
Healy (then in a minority in the RCP) and others. Moreover, as
Lenin often remarked, a mistake, if repeated and not corrected in
time, becomes a tendency. The fundamental theoretical
incapacities of the leadership of the Fourth International were
to become a factor in the collapse of the RCP in Britain and the
main cause of the degeneration of their international
organisation to become a myriad of unimportant middle-class
sects.The RCP position, in contrast, was clear:
'by far the greatest event of world significance is the
emergence of Russia, for the first time in history, as the
greatest military power in Europe and Asia.' (see The Changed
Relationship of Forces in the previous chapter).A more complicated theoretical problem arose,
however, over those countries occupied by the Red Army after
1945. The RCP at first put a tentative position, raising the
possibility that Moscow could change the social relations in
these states, albeit on a bureaucratic basis. Within three years,
this tentative position was made firm and rounded out by the
unfolding events. While the International Secretariat of the
Fourth International once again clung to out-dated formulations -
that Russia was a degenerated workers' state, but all those
states occupied by the Red Army were still capitalist states - it
was Ted Grant, as the leading theoretician of the RCP, who worked
out a correct position.The Marxist analysis of the Eastern European
states was not arrived at lightly. Ted Grant has described how it
was necessary to go back to basics, to the works of Marx, Engels,
Lenin and Trotsky and for a whole period of months to read and
re-read the relevant works, as a guide. The living experience of
Eastern Europe was carefully examined in the light cast by the
classics of Marxism and eventually, as Ted himself put it, 'we
came to the conclusion that what we had here was a form of
proletarian Bonapartism.'The first article reproduced in this section,
from the Socialist Appeal of June 1948, describes and
explains the 'February events' in Czechoslovakia, the so-called
'Prague coup'. Here, the Stalinist-dominated government, leaning
on the working class through 'action committees', overcame the
resistance of the capitalist class and carried through the
nationalisation of industry and the major part of the economy.
The end result, as the article explained, provided 'the economic
basis for a workers' state', but without the democratic control
of the state by the workers, 'all the rights which the workers
still possess will be strangled and an uncontrolled bureaucracy
will ride roughshod over the masses, as in Russia.'A more general and thoroughly developed
treatment of Eastern Europe is contained in the second and major
part of this chapter. In June 1948, Tony Cliff, an RCP member,
published a lengthy document entitled The Nature of Stalinist
Russia. This work has been extended over the years, and the
arguments partly modified, but its essence has always been the
idea that Russia, under Stalin, became 'state capitalist'. It
followed from this that the other states of the Eastern bloc were
also 'state capitalist'.The reply by Ted Grant was published in two
parts. The first, Against the Theory of State Capitalism: In
Answer to Cliff, deals more particularly with economic
arguments, thoroughly demolishing Cliff's confused and
contradictory theories. It draws upon a wealth of material from
the great teachers of Marxism and condenses from this a
worked-out description of the character of the 'transitional'
state between capitalism and socialism, when the working class
holds the levers of political and economic power, but many of the
vestiges of class society remain. The document explains how many
of the 'capitalist' features of Russia, gleefully enumerated by
Cliff, would exist in any workers' state, whether it was a
'healthy' state based on workers' democracy or a degenerated
workers' state, as in Russia.The second part, entitled The Marxist Theory
of the State, As Applied to the Stalinist States, describes
in more general terms the means by which the Moscow bureaucracy
was able to extend its social and political system to the rest of
Easte rn Europe. It again throws an important light on the
difficult theoretical questions of Bonapartism and the role of
the state. While the alleged 'leadership' of the Fourth
International found themselves in an increasingly untenable and
unreal position, the analysis elaborated by Ted Grant has stood
unchallenged for over forty years.Taken as a whole, the reply is itself a modern
'classic', a major contribution to the theoretical arsenal of
Marxism. It is to this day the most definitive defence, and a
deepening, of the original arguments of Leon Trotsky, that Russia
is a degenerated workers' state, and in that light, deserves not
only to be read, but carefully and diligently studied.The final piece in this chapter is a document
giving a wide-ranging view of the nature of Stalinism after the
war. The copy used for this collection was a poorly duplicated
pamphlet,(1) dated 1951, but it is clear from the text that it
was written earlier, some time in 1949. This is confirmed by
other sources which quote large extracts from the same document
and cite it as an internal discussion document of the RCP, of
1949.(2) It seems likely, therefore, that the original document,
written at the time the RCP was disintegrating, lay for two years
before it was published privately by the author.This pamphlet, Stalinism in the Post-War
World, once again describes the strengthening of Stalinism in
Europe as a result of the war. Despite the temporary political
stabilisation in Western Europe, it was not possible at that time
to anticipate a prolonged economic upswing and for that reason,
it was believed that the incipient social discontent in Spain,
for example, would lead to a new outbreak of revolutionary
struggle. It is only with the benefit of hindsight that it can be
seen that the post-war boom unexpectedly underpinned the Franco
regime for a whole period, although it was decidedly shaky in the
first years after the war.With the perspective for political upheaval in
Western Europe, and given the mass support of the Communist
Parties at that time, the document put forward the prognosis of
growth in these parties, followed by left splits at a later
stage. As it has turned out, the long post-war boom has led to
such a degeneration of the Western European Communist Parties, in
a liberal-reformist direction, that many of them have split into
several different parts. A number of them are now unlikely to
develop at all, especially alongside tendencies of genuine
Marxism.Another theme once again developed in this
document is the method by which the Russian bureaucracy was able
to transform social relations in Eastern Europe, establishing
regimes of proletarian Bonapartism in the image of Moscow. But in
a new departure, a significant part of it also provides a bridge
to the issues dealt with in more detail in the next chapter: the
Stalin-Tito split and the Chinese revolution.In June 1948, the rivalry between the Moscow
bureaucracy and the Yugoslavian state bureaucracy erupted into
open conflict. The Yugoslav leader, Tito, was denounced by the
Cominform and he in turn denounced the Kremlin. Yet again, the
'leadership' of the Fourth International were at sixes and
sevens. Up to this point, they still held the view that Russia
was a deformed workers' state, but Eastern Europe - Yugoslavia
included - was still capitalist. Now, without any explanation,
they ditched the view that Yugoslavia was capitalist and suddenly
discovered instead that not only was it a workers' state, but a
relatively healthy one at that! Because of his split with Stalin,
the IS took the completely impressionistic view that Tito was
some kind of 'unconscious Trotskyist' and gave their wholehearted
support to Belgrade in its struggle with Moscow. 'Long live the
Yugoslav Socialist Revolution', crowed the IS, as they appealed
for fraternal links between the Fourth International and the
Yugoslav 'communist movement.'Once again it was left to Ted Grant, alone
among all the international theoreticians of Trotskyism, to
explain these events. Using the analysis of Eastern Europe
already worked out, it was now possible to describe in consistent
Marxian terms, the nature and the origin of the split between
Stalin and Tito. The document correctly described Tito as a
'Yugoslav Stalin' who was not prepared to be subjugated by
Moscow, and, having a relatively independent base in the partisan
movement which had brought him to power, was able to free himself
from the political control of the Russian bureaucracy.Furthermore, not only was Grant's analysis of
the Eastern European states able to explain the Tito-Stalin
split, it could also anticipate - and this is the test of the
correctness of theory, in politics as in science - other splits,
along national lines, within the Eastern European monolith. 'To
this day the national question remains a key question in the
struggle against the bureaucracyStalin's tendency to
convert Eastern Europe into a fief for the benefit of the Russian
bureaucracywas bound to awaken opposition among the masses,
which had to arouse an echo even in the dominant Stalinist
parties.'More prophetically still, the document not only
anticipated in advance the establishment of a Stalinist state in
China after the revolution, but it predicted the inevitability of
a split between the Chinese and the Russian bureaucracy, on the
same basis, although on a far larger scale, as in the case of
Yugoslavia. This issue is considered more fully in the next
chapter. Contents(1) The only available copy of the
document is in a poor physical condition and is incomplete in one
or two places. At these points a few words, indicated by squared
brackets, have been added to maintain continuity.(2) War and the International, A
History of the Trotskyist Movement in Britain 1937-49 (Sam
Bornstein and Al Richardson) gives quotes from a document called The
World Situation and the Crisis of Stalinism, which correspond
exactly to passages in Stalinism in the Post War World. The
former is dated by the authors as 1949, but a copy of it has not
been traceable. Ted Grant Archive