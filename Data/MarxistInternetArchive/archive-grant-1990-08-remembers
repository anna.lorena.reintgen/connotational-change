Written: August 17,
1990
Source: Militant (August 17,
1990)
Transcription/Markup: Patrik Olofsson,
2006
Proofread: Patrik Olofsson, 2006[Ted Grant remembers the day
the news of Trotsky’s murder came over the radio as he lay in
bed in hospital.]One can’t imagine the fury, anger and frustration felt by
all the comrades throughout the world. It was the 19th time they
had tried to assassinate him; this time they succeeded.The events of the war at that time were so stupendous that
Trotsky’s death didn’t have the impact it would have
had in peacetime. That is why Stalin calculated on murdering
Trotsky at that time. He thought it wouldn’t be noticed in
the general turmoil of the war. But it made an enormous impact on
us and made us even more determined to carry on Trotsky’s
work, as we have done in Britain and internationally in the
succeeding years.We were a bit critical at the time of the American Socialist
Workers’ Party for not having guarded him better but of
course we didn’t say anything publicly. What was the use
after the assassination had already taken place?Our immediate response was to launch a campaign to expose the
Stalinists’ crimes. The news of the assassination was
presented by JR Campbell in the Communist Party’s Daily
Worker of those days in an absolutely scandalous way, as if
one of Trotsky’s followers had murdered him and Stalin and
the bureaucracy had nothing to do with it. We exposed this lie.At the time I was involved in the Workers’ International
League, building up the struggle against the imperialist war. We
were campaigning for a Labour government to come to power and then,
if necessary, wage a revolutionary war against Hitler, after having
nationalised the economy.Without Trotsky we would have been blind. He was in many ways
even greater than Lenin, in the theoretical work he did between
1923 and 1940 to deepen and develop the ideas of Marxism and
Leninism. His analysis of the events in Spain, particularly the
1931-37 revolution, France, Britain at the time of the general
strike, China in 1925-27 and Germany in the struggle against
Hitler, armed his followers for the struggles that lay ahead.Above all; without Trotsky we would not have had an analysis of
Stalinism. His criticisms of the Communist International were shown
over and over again to be correct in the light of events.We felt a burning regret, because perhaps Trotsky’s best
work was done in the last year of his life. His book, In
Defence of Marxism, remains correct to this day in its
analysis of the Soviet Union and the processes of Stalinism.He showed that the bureaucracy was not a new class or new
formation but an aberration from socialism which could not bring
socialism. His marvelous book on Stalin was also written at that
time, though it was only published in 1945. Stalin exerted pressure
on the governments of the United States, Britain and other
countries to prevent the publication of Trotsky’s work until
after the end of the war. These works gave a sound theoretical
foundation to the movement. If he had lived for another five
years, turning out material of that sort, it would have been an
enormous plus for the development of the workers’
movement.If Trotsky had lived on into the post war period it would not
have made a fundamental difference to the course of events - the
victory of Stalinism on the one hand and the revival of capitalism
on the other. This was due to the policies of Stalinism and
reformism which prevented the carrying through of the revolution,
which Trotsky had looked forward to.In France, Britain, Germany and other countries if the so-called
Communist Parties had been revolutionary parties they could have
carried through the revolution on the European continent and solved
the problems of the working class nationally and internationally
once and for all.But had Trotsky lived on he would have armed the workers better.
There would not have been the absolute disasters perpetrated by
those who claimed to be supporters of Trotsky but in reality
completely distorted his ideas. And there would not have been so
many losses from the movement at that time and later if Trotsky had
been there to direct and guide it.He would have supported our analysis of these events - that
there would be a consolidation of capitalism, in the industrialised
countries at least, and a consolidation of Stalinism in Eastern
Europe and the Soviet Union for a temporary period, a period which
has now passed.These ideas have been completely vindicated and they will be
even more vindicated in the period of struggle that opens up in the
course of the 90s. In particular the news from the Soviet Union -
the openly counter-revolutionary movement of Gorbachev to turn back
to capitalism - shows how correct was Trotsky’s analysis of
Stalinism and the processes taking place in the world.The bureaucracies are being compelled to try and maintain their
privileges by turning themselves into capitalists and going over
openly to the side of the counter-revolution, as Trotsky
predicted.They are trying to seek assistance and support from people like
Bush, Thatcher and Kohl. It’s absolutely nauseating to see
the way that Gorbachev cringes in front of the imperialists. In the
period that opens up, capitalism will go into decline, disease and
disintegration, much as Stalinism has already disintegrated in
Eastern Europe and the Soviet Union.In the last 50 years, we have been building, defending and
deepening Trotsky’s ideas and have built a movement that will
be victorious in transforming the Labour Party, the trade unions
and society in Britain and internationally. We will prepare the way
for the complete vindication of Trotsky’s ideas.Even now, when one thinks back to the assassination of Trotsky
it brings tears to the eyes.What enormous sacrifices he made. His family was murdered, his
comrades, the old Bolsheviks, were entirely annihilated. There was
a victory for Stalinist and reformist reaction. Trotsky went right
through all that but it did not stop him from doing the necessary
work to prepare the cadres for the development of the movement. It
is on the basis of his ideas, extending and deepening them however,
that we will go forward to victory. Ted Grant
Archive