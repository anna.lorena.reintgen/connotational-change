Paul Mattick 1978
Source: Anti-Bolshevik Communism by Paul Mattick, published by Merlin Press, 1978;
Transcribed: by Andy Blunden, for marxists.org 2003.The reprinting of this collection of essays and reviews, which
were written during the last 40 years, may find its justification in the
current ferment of ideas by which a new left-wing within the socialist movement
attempts to derive a theory and practice more adequate to the present situation
and the needs of social change. As yet merely of a theoretical nature, this
trend has led to a growing interest in the comprehension of past revolutionary
movements. However, although its proponents try to differentiate themselves
from the old and discredited labour movement, they have not as yet been able to
evolve a theory and practice of their own which could be considered superior to
those of the past. In fact, the ‘lessons of history’ seem to be
largely wasted on the new generation, which often merely repeats in a more
insolent fashion and with less sophistication the proven mistakes of the past.
Instead of finding their orientation in the actual social conditions and their
possibilities, the new leftists base their concerns mainly on a set of
ideologies that have no relevance to the requirements of social change in
capitalist nations. They find their inspiration not in the developmental
processes of their own society but in the heroes of popular revolution in
faraway countries, thereby revealing that their enthusiasm is not as yet a real
concern for decisive social change.Of course, there stands a theory behind this strange aberration, namely, the
assumption that anti-imperialist struggles of the ‘Third World’
will abet the social revolution in capitalist nations, thus leading to
world-wide social transformation. Although this theory may only indicate the
revolutionaries’ present frustration in the given non-revolutionary
situation, it was at one time the accepted doctrine of a revolutionary movement
which briefly tried, but failed, to extend the Russian into a world revolution.
In this respect, the ideas of the new revolutionaries still relate to the old
Leninism, which Stalin described as the “Marxism of the period of
imperialism.”In Lenin’s view, it was not the strongest but the weakest link in the
chain of imperialist nations that would, through its own revolution, release a
world-revolutionary process. Moreover, as imperialism had become an absolute
necessity for capitalism, the anti-imperialist struggle was at once a struggle
against world capitalism. He envisioned the world revolution as a kind of
repetition of the Russian Revolution on a global scale. Just as the Russian
Revolution had been a ‘peoples’ revolution’, embracing
workers, peasants and the liberal bourgeoisie, without therefore, in
Lenin’s mind, losing its socialist character, so the world revolution
could be seen as a unitary fight of national-revolutionary movements and
working-class struggles in the imperialist nations. And just as, according to
Lenin, the existence of the Bolshevik Party in Russia guaranteed the
transformation of the ‘peoples’ revolution’ into a communist
revolution, so, on the world scale, the Bolshevik International was to
transform the national-revolutionary struggles into struggles for international
socialism.More than half a century has passed since this theory was celebrated as a
necessary extension of the theories of Marx, who did not emphasise the
imperialistic difficulties of capitalism but based his hopes for a socialist
revolution on the inherent contradictions of the capitalist system of
production. In Marx’s view, a fully developed capitalism was a
precondition for a socialist revolution, even though he thought it possible
that such a revolution may receive its impetus from the outside, that is, from
revolutionary occurrences in less developed nations. What Marx had specifically
in mind was a revolution in Russia which could conceivably lead to a European
revolution. Should the latter succeed, it was reasonable to assume that the
character of the international revolution as a whole would be determined by the
capitalistically-advanced nations. However, the Russian Revolution did not
spread to the West and in its isolation could not realise a socialist society
but merely a form of state-capitalism under the authoritarian rule of the
Bolshevik Party.It is of course true that bourgeois revolutions in the traditional sense are
no longer possible. The monopolistic control of the world economy by the great
capitalist powers and their productive preponderance excludes an independent
national capitalistic development in underdeveloped nations. To aspire to this
goal nonetheless requires their political liberation from imperialist rule, as
well as from their native ruling classes, allied as they are with the foreign
oppressors. Because the struggle for liberation has to base itself on the broad
masses, it cannot use traditional capitalist ideologies, but must be carried on
with anti-imperialist and therefore anti-capitalist ideologies.These national-revolutionary movements are not signs of an impending
world-wide socialist revolution, but just so many attempts at an independent
capitalistic development — albeit in a state-capitalist form. To the
degree to which the liberated nations succeed in freeing themselves from
foreign control, they do increase the difficulties of capitalism and further
its dissolution. To that extent, they may also aid the class struggle in the
dominating capitalist countries. But this does not alter the fact that the
goals of the proletarian revolution in the capitalist nations are necessarily
different from those that can be realised in backward countries.It would be ideal, no doubt, to combine the anti-capitalist and
anti-imperialist struggles into one great movement against all forms of
exploitation and oppression. Unfortunately, this is only an imaginary
possibility; unrealisable because of the actual material and social differences
between the various differently developed nations. The history of Russia since
1917, as the prototype of ‘socialist revolutions’ in backward
countries, illuminates the objective limitations to their transformation.
Today, we even experience the sorry spectacle of so-called socialist countries,
all of them adhering to the Leninist ideology, facing one another in deadly
enmity and preparing to destroy one another. It is quite obvious that the
national interests of state-capitalist systems — like all national
interests — contain in themselves their own imperialistic tendencies. It
is thus no longer possible to speak of common needs of the
national-revolutionary and the international-socialist movement.The international socialist movement must of course be an anti-imperialist
movement. But it has to actualise its anti-imperialism through the destruction
of the capitalist system in the advanced countries. Were this accomplished,
anti-imperialism would become meaningless and the social struggles in the
underdeveloped part of the world would focus on internal class differences. To
be sure, the weakness of anti-capitalist movements in the developed countries
is one more reason for the existence of national-revolutionary movements. For
the latter cannot wait for the proletarian revolution in the dominating
capitalist countries; yet, where they succeed, they can reach at best only
partial release from foreign exploitation but not the conditions of socialism.
On the other hand, successful proletarian revolutions in the capitalistically
developed nations could lead to the internationalisation of all social
struggles and progressively hasten the integration of underdeveloped nations
into a socialist world system.That there are national-revolutionary movements in the backward nations but
not as yet socialist movements in the imperialist countries is due to the
greater and more pressing misery in the former. It is also due to the
dissolution of the colonial structure resulting from the second world war and
the re-organisation and modification of imperialist rule in the post-war world.
Force of circumstances interconnects the national movements with the currently
waged imperialist power struggles and ‘liberation’ from one type of
imperialism leads to subordination to another. Under present conditions, in
brief, national revolutions remain illusory, with respect both to real national
independence and to its apparent socialist ideology. They may, however, be
preconditions for future struggles for more realistic goals. But this, too,
depends on the course of events in the capitalistically-advanced nations.The preoccupation with national-revolutionary movements that still
characterises left-wing radicalism has led, on an international scale, to a
re-dedication to Leninist principles in either a Russian or Chinese garb and
dissipates the energies thereby released into meaningless and often grotesque
activities. By trying to actualise the Leninist ideas of revolution and its
organisation in capitalistically-advanced nations, would-be radicals
necessarily hinder the development of a revolutionary consciousness adequate to
the tasks of the socialist revolution. Because new revolutionary socialist
movements may arise in response to capitalism’s increasing social and
economic difficulties, it is essential to pay renewed attention to the
aspirations and accomplishments of former similar movements and here, in
particular, to Bolshevism and its Leninist creed.In this connection, it is particularly apt to recall another movement which
emerged out of the shambles of the Second International and the expectations
based on the Russian Revolution. Most of the items in this anthology concern
themselves with the problems of the international labour movement at the turn
of the century — that is, with the reasons for, and the consequences of,
the growth of a labour movement that ceased to be revolutionary because of the
resilience of capitalism and its ability to improve the living conditions of
the labouring population. Still, the immanent contradictions of capitalism led
to the first world war and while leading to the partial collapse of the old
labour movement, also gave rise to a new radicalism culminating in the Russian
and Central European revolutions.These revolutions involved the organised as well as unorganised masses of
workers, which created their own and new form of organisation for action and
control in the spontaneously-arising workers’ and soldiers’
councils. But in both Russia and Central Europe the actual content of the
revolution was not equal to its new revolutionary form. Whereas in Russia it
was mainly the objective unreadiness for a socialist transformation, in Central
Europe, and here particularly in Germany, it was the subjective unwillingness
to institute socialism by revolutionary means, which largely accounts for the
self-limitation and finally the abdication of the council movement in favour of
bourgeois democracy. The ideology of Social Democracy had left its mark; the
great mass of workers mistook the political for a social revolution; the
socialisation of production was seen as a governmental concern, not as that of
the workers themselves. In Russia, it is true, the Bolshevik Party advanced the
slogan ‘All power to the Soviets,’ but only for opportunistic
reasons, in order to reach its true goal in the authoritarian rule of the
Bolshevik Party.By itself, the workers’ self-initiative and self-organisation offers
no guarantee for their emancipation. It has to be realised and maintained
through the abolition of the capital-labour relationship in production, through
a council system, which destroys the social class divisions and prevents the
rise of new ones based on the control of production and distribution by the
national state. However difficult this may prove to be, the history of the
existing state-capitalist systems leaves no doubt that this is the only way to
a socialist society. This had already been recognised by small minorities in
the radical movement prior to, during, and after the Russian Revolution and was
brought into the open within the communist movement as an opposition to
Bolshevism and the theory and practice of the Third International. It is this
movement and the ideas it brought forth, which this volume recalls, not,
however, to describe a particular part and phase of labour history, but as a
warning, which may also serve as a guide for future actions.The revolutions which succeeded, first of all, in Russia and China, were not
proletarian revolutions in the Marxist sense, leading to the ‘association
of free and equal producers’, but state-capitalist revolutions, which
were objectively unable to issue into socialism. Marxism served here as a mere
ideology to justify the rise of modified capitalist systems, which were no
longer determined by market competition but controlled by way of the
authoritarian state. Based on the peasantry, but designed with accelerated
industrialisation to create an industrial proletariat, they were ready to
abolish the traditional bourgeoisie but not capital as a social relationship.
This type of capitalism had not been foreseen by Marx and the early Marxists,
even though they advocated the capture of state-power to overthrow the
bourgeoisie — but only in order to abolish the state itself.Although designated as socialism, state-control of the economy and over
social life generally, exercised by a privileged social layer as a newly
emerging ruling class, has perpetuated for the industrial as well as
agricultural labouring classes the conditions of exploitation and oppression
which had been their lot under the semi-feudal social relations of
capitalistically-underdeveloped nations. That this new social system could also
be applied in capitalistically more advanced nations was demonstrated after the
second world war, through the extension of state-capitalist system into the
West by way of imperial conquest. In either case, ‘socialism’
became quite generally identified with the prevailing state-capitalist systems.
Movements exist everywhere whose proclaimed goals are precisely the
establishment of similar regimes in additional countries, even though, for
opportunistic reasons, these goals may be toned down at times, or even totally
disclaimed. There exists then the danger that possible new revolutionary
outbreaks may once again be side-tracked into state-capitalist
transformations.This possibility finds its support in the centralising tendencies inherent
in capitalism itself. The concentration of capital, its monopolisation, and the
rise of corporations in which ownership is separated from direct control, and,
finally, the reluctant integration of state and capital in the mixed economy,
with its fiscal and monetary manipulations, seem to spell a tendency in the
direction of a fullfledged state-capitalism. What once constituted a vague hope
on the part of social reformers and what in backward countries became a reality
through revolution, appears now as an unavoidable requirement for securing the
social relations of capital production.Although the so-called mixed economy will not automatically transform itself
into state-capitalism, new social upheavals may well lead to it in the name of
socialism. It is true that ‘Marxism-Leninism’ presents itself today
as a purely reformist movement, which, like the Social Democracy of old,
prefers the democratic processes of social change to the revolutionary
overthrow of capitalism. In some countries, France and Italy, for instance,
relatively strong communist parties offer their services to capitalism to help
it overcome its crisis conditions. But should everything fail, and an
intensified class struggle pose the question of social revolution, there can be
no doubt that these parties will opt for state-capitalism, which in their
views, is the only possible form of socialism. Thus, the revolution would be at
once a counter-revolution. The end of capitalism demands therefore, first of
all, the end of Bolshevik ideology and the rise of an anti-Bolshevik
revolutionary movement, such as has been attempted at the earlier revolutionary
situation to which this book tries to draw attention.P.M. 
Paul Mattick Archive
