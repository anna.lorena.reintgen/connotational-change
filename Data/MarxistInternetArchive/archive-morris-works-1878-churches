
      The question asked by Lord Houghton in the House of Lords on Thursday elicited from
      the Bishop of London an acknowledgment that the scheme proposed some few years back
      for the wholesale removal of the City churches is continuing its destructive course
      unimpeded. Four more churches are to be sacrificed to the Mammon-worship and want
      of taste of this great city. Last year witnessed the destruction of the fine church
      of St Michael's, Queenhithe, and All Hallows, Bread Street, which bore upon its
      walls the inscription stating that Milton had been baptized there. St Dionis
      Backchurch, a remarkable building by Wren, is now in course of destruction, while
      within the last ten years the beautiful church of St Antholin, with its charming
      spire, and the skilfully designed little church of St Mildred, in the Poultry, All
      Hallows, Staining, (except its tower), St James's, Duke Place, St Benet,
      Gracechurch, with its picturesque steeple, the tower and vestibule of All
      Hallows-the-Great,Thames Street, have all disappeared. Those for the removal of
      which a Commission has been now issued are as follows: St Margaret Pattens, Rood
      Lane; St George, Botolph Lane; St Matthew, Friday Street; and St Mildred, Bread
      Street, all works of Wren, and two of them, St Mildred, Bread Street, and St
      Margaret Pattens, possessing spires of singularly original and beautiful design. It
      must not be supposed that these are the only churches which are in danger, but
      their proposed destruction serves to show the fate which sooner or later is in
      store for the whole of Wren's churches in this city, unless Englishmen can be
      awakened, and by strong and earnest protest show the ecclesiastical authorities
      that they will not tamely submit to this outrageous and monstrous barbarity.
    
      From an art point of view the loss of these buildings will be irreparable, for
      Wren's churches form a distinct link in the history of the ecclesiastical art of
      this country.
    
      Many persons suppose that by preserving St Paul's Cathedral, that architect's great
      masterpiece, enough will be left to illustrate his views upon ecclesiastical
      architecture, but this is far from being the case. For, grand as St Paul's
      undoubtedly is, it is only one of a class of buildings common enough on the
      Continent - imitations of St Peter's, Rome. In fact, St Paul's can scarcely be
      looked upon as an English design, but, rather, as an English rendering of the great
      Italian original, whereas the City churches are examples of purely English
      renaissance architecture as applied to ecclesiastical purposes, and illustrate a
      style of architecture peculiar not only to this country but even to this city, and
      when they are destroyed the peculiar phase of architecture which they exhibit will
      have ceased to exist, and nothing will be left to record it. The Continent
      possesses nothing in the least resembling our City churches, and the fact that they
      are all found in suchclose proximity to one another only serves to make them the
      more valuable for purposes of study. One great merit which they possess is shown by
      the fact that, although they are diminutive in point of size, scarcely any one of
      them being above 80 ft. long, they possess a dignity of proportion, a masterly
      treatment as to scale, which renders them far more imposing than many buildings
      double and treble their dimensions; the relation which they bear to each other and
      to the great Cathedral which they surround, enhancing by their thin taper spires
      the importance of the majestic dome, and relieving the dulness and monotony of the
      general sky line of the City, all serve as unanswerable arguments for their
      preservation. Surely an opulent city, the capital of the commercial world, can
      afford some small sacrifice to spare these beautiful buildings the little plots of
      ground upon which they stand. Is it absolutely necessary that every scrap of space
      in the City should be devoted to money-making, and are religion, sacred memories,
      recollections of the great dead, memorials of the past, works of England's greatest
      architect, to be banished from this wealthy City? If so, alas for our pretended
      love of art; alas for the English feeling of reverence of which we hear so much;
      alas for those who are to come after us, whom we shall have robbed of works of art
      which it was our duty to hand down to them uninjured and unimpaired; alas for
      ourselves, who will be looked upon by foreign nations and by our own posterity as
      the only people who have ever lived, who, possessing no architecture of their own,
      have made themselves remarkable for the destruction of the buildings of their
      forefathers.
    
      Letter to the Times, 17 April 1878.  
    
The reference to this piece of work in the Chronology

The William Morris Internet Archive : Works
