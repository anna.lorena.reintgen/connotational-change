MIA  >  Archive  >  Kidron From Socialist Review, [11th Year No. 10,] November 1961, p. 2.
Transcribed by Ian Birchall, Nina Kidron & Richard Kuper.
Marked up by Einde O’Callaghan for the Marxists’ Internet Archive.[THE STRIKES II was a separate contribution by John Phillips]WORKERS at British Light Steel Pressings, Acton, the Rootes Motors
subsidiary, and at the Steel Company of Wales, Port Talbot have
nailed the charge of apathy and I’m-all-right-Jackism levelled
against them. Solidarity at both of these major strike fronts was
tremendous; and rank-and-file militancy at Acton was great enough to
have both nullified Carron’s strike-breaking moves and shoved aside
the Communist Party ban on street demonstrations supported by
neighbouring factories. If this is going to be the reaction to the
‘pay pause’, the Tories had better watch it. There is hardly a
section of workers in the country that hasn’t got a pay claim in;
and the mood is hardening.But Acton and Port Talbot are not only examples of working class
militancy in action. They can tell us something about the intentions
of the bosses. The Rootes group has been doing worse than any other
member of the Big Five this last year and the motor industry as a
whole has fallen on comparatively hard times: at forty seven thousand
odd, sales this September were one-fifth below the figure for
September 1960.The motor industry is the single largest customer for the steel
industry so that its decline (and the switch to smaller cars) is a
major factor in the build up of surplus capacity in the mills. What
could be more convenient for them than to force a showdown now, when
stocks are piling up and orders slack? There is nothing new in this.
Last summer, the Bulletin of the Oxford Institute of Statistics
featured an article by H.A. Turner and J. Bestoby on the pattern of
strikes in the motor industry since the war.They showed that the hardening of attitudes on the part of the
bosses in slack times, and the greater willingness of unions to lend
official support to strikers at times when they can both please the
workers and do little harm to the bosses, combined to bump up the
number of workdays lost, at precisely those periods when production
was down. This time, the union leadership are less willing and the
bosses, faced with a greater than normal slow-down in the economy and
with the growing threat of Common Market competition are proving more
than willing to force the pace. The result – lockout. Or in the
words of The Economist (21 October), ‘What people do
realise, at last, is that the employers mean business when they talk
of economies. Perhaps this is hot such a bad way to face up to the
foreign challenge after all.’So we are back to the slogan of the 1920s – ‘all wages must
come down!’ And this after a decade in which rent, interest and
dividends have risen by over £1,000 million before tax; in which
dividends doubled and capital values have soared without being taxed;
in which rents for dwellings have gone up nearly two-and-a-half
times, and other rents doubled; in which tax revenue from death
duties, surtax and profits tax, at £639 million in 1959 (less than
the yield on tobacco) was only £59 million more than in 1951 while
the yield on national insurance contributions, the tax which hits the
lowest earners hardest, doubled between the two dates to £898
million in 1959; and in which, despite these and other ‘incentives’
to capital, the real investment on plant and machinery was no higher
in 1959 than in 1951. (These facts are to be found, together with
other useful information in Michael Barrat Brown and John Hughes’
New Left pamphlet, Britain’s Crisis and the Common Market,
3s., from 7 Carlisle Street, London W1).The record is clear: however great the concessions made to the
bosses, they do not result in greater efficiency on their part. They
have had the opportunity and failed miserably. It is not the ‘foreign
challenge’ we have to face up to, but the bosses’ challenge. BSLP
and SCOW workers have shown what can be done. Let’s have more of
the same, so that we may drag the unions officially into battle and
put paid to the pay pause. 
Top of the pageLast updated on 18 February 2017