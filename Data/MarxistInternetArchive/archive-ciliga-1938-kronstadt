Published in La Révolution Prolétarienne, No. 278; September 10, 1938.The correspondence between Trotsky and Wendelin Thomas (one of 
the leaders of the revolt in the German Navy in 1918, and a member of the 
American Committee of Enquiry into the Moscow Trials) regarding the historical 
significance of the events in Kronstadt in 1921, has given rise to widespread 
international discussion. That in itself indicates the importance of the 
problem. On the other hand, it is no accident that special interest should be 
shown in the Kronstadt revolt today; that there is an analogy, a direct link 
even between what happened at Kronstadt 17 years ago, and the recent trials at 
Moscow, is only too apparent. Today we witness the murder of the leaders of the 
Russian revolution; in 1921 it was the masses who formed the basis of the 
revolution who were massacred. Would it be possible today to disgrace and 
suppress the leaders of October without the slightest protest from the people, 
if these leaders had not already by armed force silenced the Kronstadt sailors 
and the workers all over Russia?Trotsky's reply to Wendelin Thomas shows that unfortunately 
Trotsky—who is, together with Stalin, the only one of the leaders of the 
October revolution concerned in the suppression of Kronstadt who remains 
alive—still refuses to look at the past objectively. Furthermore, in his 
article Too Much Noise About Kronstadt, he increases the gulf which he created at 
that time between the working masses and himself; he does not hesitate, after 
having ordered their bombardment in 1921 to describe these men today as 
"completely demoralised elements, men who wore elegant wide trousers and did 
their hair like pimps". No! It is not with accusations of this kind, which reek 
of bureaucratic arrogance, that a useful contribution can be made to the lessons 
of the great Russian revolution.In order to assess the influence that Kronstadt has had on the 
outcome of the revolution, it is necessary to avoid all personal issues, and 
direct attention to three fundamental questions:1. In what general circumstances the Kronstadt revolt arose?2. What were the aims of the movement?3. By what means did the insurgents attempt to achieve these 
aims? Everyone now agrees that during the winter of 1920 to 1921 the 
Russian revolution was passing through an extremely critical phase. The 
offensive against Poland had ended in defeat at Warsaw, the social revolution 
had not broken out in the West, the Russian revolution had become isolated, 
famine and disorganisation had seized the entire country. The peril of bourgeois 
restoration knocked at the door of the revolution. At this moment of crisis the 
different classes and parties which existed within the revolutionary camp each 
presented their solution for its resolution.The Soviet Government and the higher circles in the Communist 
Party applied their own solution of increasing the power of the bureaucracy. The 
attribution of powers to the Executive Committees which had hitherto been 
vested in the soviets, the replacement of the dictatorship of the class by the 
dictatorship of the party, the shift of authority even within the party from its 
members to its cadres, the replacement of the double power of the bureaucracy 
and the workers in the factory by the sole power of the former—to do all this 
was to "save the Revolution!" It was at this moment that Bukharin put forward 
his plea for a "proletarian Bonapartism". By placing restrictions on itself the 
proletariat would, according to him, facilitate the struggle against the 
bourgeois counter-revolution. Here was manifested already the enormous 
quasi-messianic self-importance of the Communist Bureaucracy.The Ninth and Tenth Congresses of the Communist Party, as well as 
the intervening year passed beneath the auspices of this new policy. Lenin 
rigidly carried it through, Trotsky sang its praises. The Bureaucracy prevented 
the bourgeois restoration ... by eliminating the proletarian character of the 
revolution. The formation of the Workers' Opposition within the party, which was 
supported not only by the proletarian faction in the party itself but also by 
the great mass of unorganised workers, the general strike of the Petrograd 
workers a short time before the Kronstadt revolt and finally the insurrection 
itself, all expressed the aspirations of the masses who felt, more or less 
clearly, that a 'third party' was about to destroy their Conquests. The movement 
of poor peasants led by Makhno in the Ukraine was the outcome of similar 
resistance in similar circumstances. If the struggles of 1920-1921 are examined 
in the light of the historical material now available, one is struck by the way 
that these scattered masses, starved and enfeebled by economic disorganisation, 
nevertheless had the strength to formulate for themselves with such precision 
their social and political position, and at the same time to defend themselves 
against the bureaucracy and against the bourgeoisie. We shall not content ourselves, like Trotsky, with simple 
declarations, so we submit to readers the resolution which served as a programme 
for the Kronstadt movement. We reproduce it in full, because of its immense 
historical importance. It was adopted on February 28th by the sailors of the 
battleship Petropavlovsk, and was subsequently accepted by all the sailors, 
soldiers and workers of Kronstadt.After having heard the representatives delegated by the general 
meeting of ships' crew to report on the situation in Petrograd this assembly 
takes the following decisions:1. Seeing that the present soviets do not express the wishes of 
the workers and peasants, to organise immediately re-elections to the Soviets 
with Secret vote, and with care to organise free electoral propaganda for all 
workers and peasants.2. To grant liberty of speech and of press to the workers and 
peasants, to the anarchists and the left socialist parties.3. To secure freedom of assembly for labour unions and peasant 
organisations.4. To call a non-partisan Conference of the workers, Red Army 
Soldiers and sailors of Petrograd, Kronstadt, and of Petrograd province, no 
later than March 10th, 1921.5. To liberate all political prisoners of Socialist parties as 
well as all workers, peasants, soldiers and sailors imprisoned in connection 
with the labour and peasant movements.6. To elect a Commission to review the cases of those held in 
prisons and concentration camps.7. To abolish all politodeli 
[official propaganda] because no party should be given 
special privileges in the propagation of its ideas or receive financial support 
from the government for such purposes. Instead there should be established 
educational and cultural commissions, locally elected and financed by the 
government.8. To abolish immediately all zagryaditelniye otryadi
[armed units that requisitioned grain from the peasants].9. To equalize all the rations of all who work with the exception 
of those employed in trades detrimental to health.10. To abolish the communist fighting detachments in all branches 
of the army, as well as the communist guards kept on duty in mills and 
factories. Should such guards or military detachments be found necessary they 
are to be appointed in the army from the ranks, and in the factories according 
to the judgement of the workers.11. To give the peasants full freedom of action in regard to 
their land and also the right to keep cattle on condition that the peasants 
manage with their own means; that is, without employing hired labour.12. To request all branches of the Army, as well as our 
comrades the military kursanti [cadets] to concur in our resolutions.13. To demand that the press give the fullest publicity to our 
resolutions.14. To appoint a travelling commission of control.15. To permit free artisan production which does not employ hired 
labour.These are primitive formulations, insufficient no doubt, but all 
of them impregnated with the spirit of October; and no calumny in the world can 
cast a doubt on the intimate connection existing between this resolution and the 
sentiments which guided the expropriations of 1917.The depth of principle which animates this resolution is shown by 
the fact that it is still to a great extent applicable. One can, in fact, oppose 
it as well to the Stalin regime of 1938, as to that of Lenin in 1921. More even 
than that: the accusations of Trotsky himself against Stalin's regime are only 
reproductions, timid ones, it is true, of the Kronstadt claims. Besides, what 
other programme which is at all socialist could be set up against the 
bureaucratic oligarchy except that of Kronstadt and the Workers' Opposition?The appearance of this resolution demonstrates the close 
connections which existed between the movements of Petrograd and Kronstadt. 
Trotsky's attempt to set the workers of Petrograd against those of Kronstadt in 
order to confirm the legend of the counter-revolutionary nature of the Kronstadt 
movement, comes back on Trotsky himself: in 1921, Trotsky pleaded the necessity 
under which Lenin was situated in justification of the suppression of democracy 
in the Soviets and in the party, and accused the masses inside and outside the 
party of sympathising with Kronstadt He admitted therefore that at that time the 
Petrograd workers and the opposition although they had not resisted by force of 
arms, none the less extended their sympathy to Kronstadt.Trotsky's subsequent assertion that "the insurrection was 
inspired by the desire to obtain a privileged ration" is still more wild. 
Thus, it is one of these privileged people of the Kremlin, the rations for whom 
were very much better than those of others, who dares to hurl a similar 
reproach, and that at the very men who in paragraph 9 of their resolution, 
explicitly demanded equalisation of rations! This detail shows the desperate 
extent of Trotsky's bureaucratic blindness.Trotsky's articles do not depart in the slightest degree from the 
legend created long ago by the Central Committee of the Party. Trotsky certainly 
deserves credit from the international working class for having refused since 
1928 to continue to participate in the bureaucratic degeneration and in the new 
'purges' which were destined to deprive the Revolution of all its left-wing 
elements. He deserves still more to be defended against Stalin's calumny and 
assassins. But all this does not give Trotsky the right to insult the working 
masses of 1921. On the contrary! More than anyone else, Trotsky should furnish a 
new appreciation of the initiative taken at Kronstadt. An initiative of great 
historic value, an initiative taken by rank-and-file militants in the struggle 
against the first bloodstained 'purge' undertaken by the bureaucracy.The attitude of the Russian workers during the tragic winter of 
1920-1921 shows a profound social instinct; and a noble heroism inspired the 
working classes of Russia nor only at the height of the Revolution but also at 
the crisis which placed it in mortal danger.Neither the Kronstadt fighters, nor the Petrograd workers, nor 
the ranks of the Communists could summon, it is true, in that winter the same 
revolutionary energy as in 1917 to 1919, but what there was of socialism and 
revolutionary feeling in the Russia of 1921 was possessed by the rank-and-file. 
In their opposition to this, Lenin and Trotsky, in line with Stalin, with 
Zinoviev, Kaganovitch, and others responded to the wishes and served the 
interests of the bureaucratic cadres. The workers struggled for the socialism 
which the bureaucracy were already in the process of liquidating. That is the 
fundamental point of the whole problem. People often believe that Kronstadt forced the introduction of 
the New Economic Policy (NEP)—a profound error. The Kronstadt resolution 
pronounced in favour of the defence of the workers, not only against the 
bureaucratic capitalism of the State, but also against the restoration of 
private capitalism. This restoration was demanded—in opposition to 
Kronstadt—by the social democrats, who combined it with a regime of political 
democracy. And it was Lenin and Trotsky who to a great extent realised it (but without 
political democracy) in the form of the NEP. The Kronstadt resolution declared 
for the opposite since it declared itself against the employment of wage labour 
in agriculture and small industry. This resolution, and the movement underlying, 
sought for a revolutionary alliance of the proletarian and peasant workers with 
the poorest sections of the country labourers, in order that the revolution 
might develop towards socialism. The NEP, on the other hand, was a union of 
bureaucrats with the upper layers of the village against the proletariat; it was 
the alliance of State capitalism and private capitalism against socialism. The 
NEP is as much opposed to the Kronstadt demands as, for example, the 
revolutionary socialist programme of the vanguard of the European workers for 
the abolition of the Versailles system, is opposed to the abrogation of the 
Treaty of Versailles achieved by Hitler.Let us consider, finally, one last accusation which is commonly 
circulated: that action such as that at Kronstadt could have indirectly let 
loose the forces of the counter-revolution. It is possible indeed that even by 
placing itself on a footing of workers' democracy the revolution might have been 
overthrown; but what is certain is that it has perished, and that it has 
perished on account of the policy of its leaders. The repression of Kronstadt, 
the suppression of the democracy of workers and soviets by the Russian Communist 
party, the elimination of the proletariat from the management of industry, and 
the introduction of the NEP, already signified the death of the Revolution.It was precisely the end of the civil war which produced the 
splitting of the post-revolutionary society into two fundamental groupings: the 
working masses and the bureaucracy. As far as its socialist and internationalist 
aspirations were concerned, the Russian Revolution was stifled: in its 
nationalist, bureaucratic, and state capitalist tendencies, it developed and 
consolidated itself.It was from this point onwards, and on this basis, each year more 
and more clearly, that the Bolshevik repudiation of morality, so frequently 
evoked, took on a development which had to lead to the Moscow Trials. The 
implacable logic of things has manifested itself. While the revolutionaries, 
remaining such only in words, accomplished in fact the task of the reaction and 
counter-revolution, they were compelled, inevitably, to have recourse to lies, 
to calumny and falsification. This system of generalised lying is the result, 
not the cause, of the separation of the Bolshevik party from socialism and from 
the proletariat. In order to corroborate this statement, I shall quote the 
testimony regarding Kronstadt of men I have met in Soviet Russia."The men of Kronstadt! They were absolutely right; they 
intervened in order to defend the Petrograd workers: it was a tragic 
misunderstanding on the part of Lenin and Trotsky, that instead of agreeing with 
them, they gave them battle," said Dch. to me in 1932. He was a non-party worker 
in Petrograd in 1921, whom I knew in the political isolator at Verkhne-Uralsk as 
a Trotskyist."It is a myth that, from the social point of view, 
Kronstadt of 1921 had a wholly different population from that of 1917," 
another man from Petrograd, Dv., said to me in prison. In 1921 he was a member 
of the Communist youth, and was imprisoned in 1932 as a 'decist' (a member of 
Sapronov's group of Democratic Centralists).I also had the opportunity of knowing one of the most effective 
participants in the Kronstadt rebellion. He was an old marine engineer, a 
communist since 1917, who had, during the civil war, taken an active part, 
directing at one time a Tcheka in a province somewhere on the Volga, and found 
himself in 1921 at Kronstadt as a political commissar on the warship Marat (ex 
Petropavlovsk). When I saw him, in 1930, in the Leningrad prison, he had just 
spent the previous eight years in the Solovietski islands. The Kronstadt workers pursued revolutionary aims in struggling 
against the reactionary tendencies of the bureaucracy, and they used clean and 
honest methods. In contrast, the bureaucracy slandered their movement odiously, 
pretending that it was led by General Kozlovski. Actually, the men of Kronstadt 
honestly desired, as comrades, to discuss the questions at issue with the 
representatives of the government. Their action, had at first, a defensive 
character—that is the reason why they did not occupy Oranienbaum in time, 
situated on the coast opposite Kronstadt.Right from the start, the Petrograd bureaucrats made use of the 
system of hostages by arresting the families of the sailors, Red Army soldiers 
and workers of Kronstadt who were living in Petrograd because several commissars 
in Kronstadt—not one of whom was shot—had been arrested. The news of the 
seizing of hostages was brought to the knowledge of Kronstadt by means of 
leaflets dropped from aeroplanes. In their reply by radio, Kronstadt declared on 
March 7th "that they did not wish to imitate Petrograd as they considered 
that such an act, even when carried out in an excess of desperation and 
hate, is most shameful and most cowardly from every point of view. 
History has not yet known a similar procedure". The new governing clique 
understood much better than the Kronstadt 'rebels' the significance of the 
social struggle which was beginning, the depth of the class-antagonism which 
separated it from the workers. It is in this that lies the tragedy of 
revolutions in the period of their decline.But while military conflict was forced upon Kronstadt, they still 
found the strength to formulate the programme for the 'third revolution', which 
remains since then the programme of the Russian socialism of the future. There are reasons for thinking that granted the relation between 
the forces of the proletariat and the bourgeoisie, of socialism and capitalism, 
which existed in Russia and Europe at the beginning of 1921, the struggle for the 
socialist development of the Russian Revolution was doomed to defeat. In those 
conditions the socialist programme of the masses could not conquer: it had to 
depend on the triumph of the counter-revolution whether openly declared or 
camouflaged under an aspect of degeneracy (as has been produced in fact).But such a conception of the progress of the Russian Revolution 
does not diminish in the slightest, in the realms of principle, the historic 
importance of the programme and the efforts of the working masses. On the 
contrary, this programme constitutes the point of departure from which a new 
cycle in the revolutionary socialist development will begin. In fact, each new 
revolution begins not on the basis from which the preceding one started, but 
from the point at which the revolution before it had undergone a moral set-back.The experience of the degeneration of the Russian Revolution 
places anew before the conscience of international socialism an extremely 
important sociological problem. In the Russian revolution, as in two other great 
revolutions, those of England and of France, why is it that it is from the 
inside that the counter-revolution has triumphed, at the moment when the 
revolutionary forces were exhausted, and by means of the revolutionary party 
itself ('purged', it is true of its left-wing elements)? Marxism believes that 
the socialist revolution, once begun, would either be assured of a gradual and 
continued development towards integral socialism, or would be defeated through 
the agency of bourgeois restoration.Altogether, the Russian Revolution poses in an entirely new way 
the problem of the mechanism of the socialist revolution. This question must 
become paramount in international discussion. In such discussion the problem of 
Kronstadt can and must have a position worthy of it. Last updated on: 8.21.2010
Ante Ciliga Archive |
MIA
TrotskyThe Masses and the Bureaucracy in 1920-21The Kronstadt ProgrammeKronstadt and the NEPThe Methods of StruggleBalance Sheet