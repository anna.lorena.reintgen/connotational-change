Mihailo Marković 1968
Written: 1968;
Source: The Autodidact 
Project;
First Published: Marx and Contemporary Scientific Thought. Marx et la Pensée Scientifique 
Contemporaine (The Hague: Mouton, 1970), pp. 155-67. Papers from the 
Symposium on the Role of Karl Marx in the Development of Contemporary Scientific 
Thought, 1968, in Paris;
Transcribed: Ralph Dumain.Marx developed a theory which is both scientific 
and critical. However, in most interpretations and further developments of his 
thought either one or the other of these two essential characteristics has 
invariably been overlooked. Among those who speak in the name of Marx or 
consider themselves his intellectual followers some accept only his radical 
criticism of the society of his time, some lay emphasis only on his contribution 
to positive scientific knowledge about contemporary social structures and 
processes.To the former group belong, on the one hand, 
various apologists of post‑capitalist society who develop Marxism as an 
ideology, and, on the other hand, those romantic humanists who consider positive 
knowledge a form of intellectual subordination to the given social framework, 
and who are ready to accept only the anthropological ideas of the young 
Marx.To the latter group belong all those scientists 
who appreciate Marx’s enormous contribution to modern social science, but who 
fail to realize that what fundamentally distinguishes Marx’s views from those of 
Comte, Mill, Ricardo and other classical social scientists, as well as from 
those of modern positivists, is his constant radical criticism of both existing 
theory and existing forms of social reality.The failure of most contemporary interpreters of 
Marx to grasp one of the basic novelties of his doctrine has very deep roots in 
the intellectual climate of our time and can be explained only by taking into 
account some of the fundamental divisions and polarizations in contemporary 
theoretical thinking.I. – The development of science and philosophy in 
the twentieth century has been decisively influenced by the following three 
factors: (1) the accelerated growth of scientific knowledge, which gave rise to 
a new technological revolution characterized by automation, use of huge new 
sources of energy and new exact methods of management; (2) the discovery of the 
dark irrational side of human nature through psychoanalysis, anthropological 
investigations of primitive cultures, surrealism and other trends of modern 
arts, and, above all, through unheard of mass eruptions of brutality from the 
beginning of World War I up to the present day; (3) the beginning of a process 
whereby existing forms of class society are destructuralized, and the rapidly 
increasing role of ideology and politics.(1) As the result of a rapid technological 
development and of an increasing division of work in modern industrial society, 
the rationality of science has gradually been reduced to the narrow 
technological rationality of experts, interested only in promoting and conveying 
highly specialized positive knowledge. In an effort to free itself from the 
domination of theology and mythology, modern science has always tended to 
dismiss unverifiable theoretical generalizations and value‑judgments. As a 
consequence, a spiritual vacuum was created, which, under the given historical 
conditions, could be filled only by faith in power, faith in success of all 
kinds. This philosophy of success, this obsession with the efficiency of means, 
followed by an almost total lack of interest in the problem of rationality and 
humanity of goals, are the essential characteristics of the spiritual climate of 
contemporary industrial society.By now it has become quite clear that, while 
increasing power over nature, material wealth, and control over some blind 
forces of history, while creating new historical opportunities for human 
emancipation, the material form of positive science, industry has neglected many 
essential human needs, and has multiplied the possibilities of human 
manipulation. The universal penetration of technology into all forms of social 
life has been followed by the penetration of a routine and uniform life‑style. 
Growth of material wealth did not make men happier; data on suicide, alcoholism, 
mental illness, juvenile delinquency, etc., even indicate a positive correlation 
between the degree of technological development and social pathological 
phenomena.Obviously, positive science and technology set 
off unpredicted and uncontrollable social processes. The scientist who does not 
care about the broader social context of his inquiry loses all control over the 
product of his work. The history of the creation and use of nuclear weapons is a 
drastic example. Another one is the abuse of science for ideological purposes. 
The most effective and, therefore, most dangerous propaganda is not that which 
is based on obvious untruths, but that which, in order to rationalize the 
interests of privileged social groups, uses partial truths established by 
science.Science would be helpless against such abuses if it were 
atomized, unintegrated, uninterested in the problems of wholes, and neutral with 
regard to such general human values as emancipation, solidarity, development, 
production according to the “laws of beauty,” disalienation, etc.However, the most influential philosophy in 
contemporary science is positivism, according to which the sole function of 
science is to describe and explain what there is and, if at least some 
laws are known, to extrapolate what there will probably be. All evaluation in 
terms of needs, feelings, ideals, in terms of ethical, aesthetic and other 
standards, are considered basically irrational and, from the scientific point of 
view, pointless. The only function of science, then, is to investigate the most 
adequate means for the ends which have been determined by others. In this way, 
science loses its power to supersede the existing forms of historical reality 
and to project new, essentially different and more humane historical 
possibilities. By its indifference towards goals it only leads to an abstract 
growth of power, and to a better adjustment within a given framework of social 
life. The framework itself remains unchallenged. Thus, behind this apparent 
neutrality and absence of any value orientation one discovers an implicit 
conservative orientation. Even a passive resistance to the reduction of science 
to a mere servant of ideology and politics is acceptable to the ruling elite, 
because pure, positive, unintegrated knowledge can always be interpreted and 
used in some profitable way: ultimately society would become devoid of its 
critical self‑consciousness.(2) Nowadays, positivism and other variants of 
philosophical intellectualism, conformism and utilitarianism are facing strong 
opposition from all those philosophers, writers and artists who prefer “the 
logic of heart” to “the logic of reason,” and who rebel against the prospect of 
an impersonal inauthentic life in an affluent mass society of the future. They 
see that power and material wealth in themselves do not help man to overcome his 
anxiety, his loneliness, his perplexity, boredom, uprootedness, his spiritual 
and emotional poverty. They see that new experiences in political life, modern 
art, and science are signs of a general lack of order and stability in the 
world, and of a basic human irrationality. Thus they reinforce the feeling that 
from all the successes of positive sciences and technology has emerged a 
fragile, unreasonable and suicidal society.As a reaction against the spirit of the 
Enlightenment (which has to some extent survived in the form of positivism), a 
powerful anti‑Enlightenment attitude is gaining ground among intellectuals. The 
world does not make sense, there is no rational pattern by which the individual 
can hope to master it, no causal explanation which would allow him to predict 
the future. There is no determination and progress in history; the history of 
civilization is only the history of growing human estrangement and 
self-deception. Human existence is absurd. Man, who is confronted with a 
universe in which there is pure contingency, and who lacks any stable internal 
structure, lives a meaningless life filled with dread, guilt, and despair. There 
are no reasons to believe that man is basically good; evil is a permanent 
possibility in his existence.Such an anti‑positivist and anti‑Enlightenment 
philosophy (which has been most consistently expressed in Lebensphilosophie 
and various forms of existentialism) is clearly critical, and concerned with 
the problems of individual existence. However, this kind of rebellion against 
 “given” and “existing” tends to be as immediate as possible and to avoid 
any mediation by positive knowledge and logic. The basic idea of this obviously 
anti-rationalist form of criticism is the following: to rely on empirical 
science already means to be caught up within the framework of the given present 
reality. On the other hand, as neither the historical process nor the human 
being has any definite structure preceding existence, all general knowledge is 
pointless. Nothing about the present can be inferred from the past, nor can the 
future be determined on the basis of knowledge of the present. All possibilities 
are open. Freedom of projection is unlimited.This kind of romantic rebellious criticism is 
entirely powerless. Postulated absolute freedom is only freedom of thought; as 
Hegel already showed in Phenomenologie des Geistes, it is the imagined 
freedom of a slave. Real criticism must start with the discovery of concrete 
practical forms of slavery, with the examination of human bonds and real, 
practical possibilities of liberation. Without such concrete and practical 
examination (which requires the use of all relevant social knowledge and the 
application of scientific methods), criticism is only an alienated form of 
disalienation.(3) In an historical epoch of fundamental social 
transformation a theory which expresses the needs and acceptable programs of 
action of powerful social forces becomes a decisive historical determinant.The theory of Marx has been playing such a 
revolutionary role throughout the historical epoch of human emancipation from 
alienated labor. It has been and still is the theoretical basis for every 
contemporary form of active and militant humanism.The critical thought of Marx is the fullest and, 
historically, the most developed expression of human rationality. It contains, 
in a dialectically superseded form, the essence of ancient Greek theoria: 
a rational knowledge of the world’s structure, with which man can change the 
world and determine his own life. Hegel’s dialectical reasoning is already a 
creative negation of the Greek notion of ratio and theory, in which the 
contradictions between static, rational thinking and irrational dynamics, 
between positive assertion and abstract negation are superseded 
(aufgehoben). The theory and method of Marx is a decisive step 
further in the process of totalization and concretization of dialectical 
reasoning: it embraces not only change in general but also, in particular, the 
human, historical form of change: praxis. The dialectic of Marx raises 
the question of rationality, and not only the rationality of the individual, but 
also that of society as a whole, not only rationality within a given closed 
system, but also that of the system’s very limits, not only rationality of 
praxis as thinking but also of praxis as material activity, as a mode of real 
life, in space and time. There is dialectical reasoning in history only in the 
extent to which it creates a reasonable reality.This theoretico‑practical conception of man and 
human history has not been further developed by Marx’s followers in its 
totality; rather it has been divided up into its component parts: various 
branches of social science, philosophical anthropology, dialectics, philosophy 
of history, conception of proletarian revolution and socialism as a concrete 
program of practical action, etc.In socialist society, as in capitalist society, 
science that had no dialectic and humanist philosophy incorporated in its 
telos, in all its assumptions, criteria and methods of inquiry, developed 
as partial, positive, expert knowledge, which informs about the given but does 
not seek to discover its essential inner limitations and overcome them. The 
connection with philosophy remained doubly external: first, because this science 
assimilated the principles of Marxism in a fixed, completed form as something 
given, obligatory, imposed by authority, abstract, torn out of context, 
simplified, vulgarized; second, because these principles externally applied do 
not live the life of science, are not subject to the process of normal critical 
testing, reexamining, revising, but become dogmas of a fixed doctrine.That is why Marxist philosophy became 
increasingly abstract, powerless, conservative. That part of it which pretended 
to be a Weltanschauung looked more and more like a boring, old‑fashioned, 
primitive Naturphilosophie, and the other part, which was supposed to 
state the general principles for interpreting social phenomena and revolutionary 
action, assumed increasingly the character of pragmatic apologetics, expected to 
serve as a foundation for ideology, and for the justification of past and 
present policies.This temporary degeneration was the consequence 
of several important circumstances: – Marxist theory became the official ideological 
doctrine of victorious labor movements; – revolutions had unexpected success just in 
those underdeveloped countries of East Europe and Asia where, in addition to 
socialist objectives, the tasks of a previous primitive accumulation, 
industrialization, and urbanization had to be accomplished; – it was necessary, under such conditions, to 
give priority to accelerated technological development, to establish a 
centralized system and to impose an authoritarian structure on all thinking and 
social behavior.Thus a return to and reinterpretation of Marx’s 
thought is needed, in order to restore and to further develop his critical 
method.II. – The essential theoretical and 
methodological novelty of Marx’s conception of science is constituted by the 
following features:1) By moving in the research process from 
unanalyzed concrete phenomena (population, wealth, etc.) to abstract universals 
(commodity, labor, money, capital, surplus‑value, etc.) and from these back to 
analyzed empirico-theoretical concrete phenomena, Marx succeeds in overcoming 
the traditional dualism between the empirical and the rational (speculative) 
approach. There is no doubt that he tries to support each of his contentions by 
as ample evidence as possible; all his major works have been preceded by years 
of studying data and establishing facts. But, in sharp contrast to empiricism, 
Marx’s science neither begins with brute facts nor remains satisfied with simple 
inductive generalizations from them. His real starting position is a 
philosophical vision and a thorough critical study of all preceding relevant 
knowledge. Initial evidence is only a necessary part of the background against 
which he builds up a whole network of abstract scientific concepts, endowed with 
an impressive explanatory power. This elaboration of a new conceptual apparatus 
(new not so much in the sense of introducing new terminology as in the sense of 
giving new meanings to already existing terms) is the most important and most 
creative part of Marx’s scientific work.(2) According to Marx, science should be 
primarily concerned not with the description of details and explanation of 
isolated phenomena, but with the study of whole structures, of social situations 
taken in their totality. That is why Marx’s new science does not know any sharp 
division into branches and disciplines. Das Kapital belongs not 
only to economics but also to sociology, law, political science, history, and 
philosophy. However, although the notion of totality plays an overwhelming role 
in the methodology of Marx, his approach is not purely synthetical. Marx knew 
that any attempt to grasp totalities directly, without analytical mediation, 
leads to myth and ideology. Therefore, a necessary phase of his method is the 
analytic breakdown of initial, directly grasped wholes into their components, 
which in the final stages of inquiry have to be brought back into various 
relations with other components, and conceived only as moments within a complex 
structure.(3) Those variants of contemporary Marxist 
humanism which are mainly interested in the diachronic aspects of social 
formations, and structuralism, which pays attention only to their synchronic 
aspects, are degenerated and one‑sided developments of certain essential moments 
of Marx’s method. In Marx’s new science these moments are inseparable. A 
totality cannot be fully understood without taking into account the place it 
occupies in history. A system is meaningful only as a crystallization of the 
past forms of human practice and with respect to historically possible futures. 
On the other hand, what is historically possible cannot be grasped without 
taking into account determining structural characteristics of the whole given 
situation. Marx discovered self‑destructive forces within the very structure of 
the capitalist system; without establishing the law of decreasing average rate 
of profit and other laws of capitalist economy, he would not have been able to 
point out the historical possibility that capitalist society will disappear. But 
on the other hand, had he not had a profound sense of history, had he approached 
capitalist society in the same ahistorical way as Smith, Ricardo and other 
bourgeois economists – as the permanent, natural structure of human society, he 
would hardly have been able to look for and find all those structural features 
which determine both the relative stability and ultimate transformation of the 
whole system.(4) A true sense of history implies a critical 
attitude, not only towards all rival theories but also towards the examined 
society. Marx’s dialectics is essentially a method of critique and of 
revolutionary practice. He himself had expressed this fundamental characteristic 
of his method by saying that dialectics arouses the anger and horror of the 
bourgeoisie, because it introduces into a positive understanding of existing 
states, the understanding of the negation of the bourgeoisie, of its 
necessary destruction; because it con­ceives every existing form in 
its change, therefore as something in transition; because it does not let 
anything be imposed upon it, and because it is fundamentally critical and 
revolutionary. [1] This thought was expressed much earlier in Theses on 
Feuerbach: the basic weakness of traditional materialism was to 
construe reality only as object, not as praxis. This praxis is critical 
and revolutionary; man is not just the product of social conditions, but the 
being who can change these conditions. He lives in a world full of 
contradictions, but he can resolve and practically remove them. The main 
objective of philosophical criticism should be the “real essence” of man; 
however, this essence is not something ahistorical and unchangea­ble, but 
the totality of social relationships. In short, what really matters is not just 
the explanation of, but also the change of the world.What must follow from such activistic assumptions 
is a new conception of the function of science. According to this conception, 
science does not only provide positive knowledge but also develops critical 
self‑consciousness. It does not only describe and explain the historical 
situation but also evaluates it and shows the way out. It does not only discover 
laws and establish what are the possibilities and probabilities of the future, 
it also indicates which possibilities best correspond to certain basic human 
needs. Thus critical scientific thought is not satisfied by showing how man can 
best adjust to the prevailing trends of a situation and to the whole social 
framework; it expresses a higher level idea of rationality by showing how man 
can change the whole framework and adapt it to himself.Two examples would suffice to illustrate this 
conception of critical science.In his economic writings Marx thoroughly examines 
structural and functional characteristics of capitalistic society. He does that 
in an objective way, in accordance with all requirements of the scientific 
method of his time. But a critical anthropological standpoint is always present: 
man is a “generic being,” a potentially free, creative, rational, social being. 
In relation to what man could already be, how he could 
already live in a highly productive and integrated industrialized society, 
Marx shows how utterly limited and crippled man in fact is in a system in which 
he is reduced to his working power, in which his working power is being 
bought as a thing, and regarded not as creative power, but as a mere quantity of 
energy which can be efficiently objectified and marketed with a good profit. The 
message of Marx’s theory is not that the worker could better adjust to the 
situation by demanding a higher price for his labor power – insofar as his labor 
power is a mere commodity, he already receives the equivalent for it. The 
implication of Marx’s theory is that the worker should reject the status of a 
thing, of a commodity, and change the whole social framework in which his labor 
is so alienated.Another example. In his criticism of Hegelian 
philosophy of law, Marx points out that the general interest of a human 
community could not be constituted by the abstract concept of an ideal, rational 
state. Insofar as in “civil society” there is bellum omnium contra omnes 
and each individual and social group pursues only one or another particular 
interest, the general interest of a truly human community has not yet been 
awakened. The Hegelian state, construed as a moment of objective spirit, exists 
only in abstract thought. What exists in reality is alienated political power 
beside and above all individual and particular interests. The form 
of this alienated political power, which treats society as the simple object of 
its activity, is the state and its bureaucracy. Now, Marx’s explanation of the 
nature of professional politics, the state and bureaucracy does not lead to the 
conclusion that man could be freer if he would simply make the state more 
democratic or increase control over bureaucracy. Without disregarding the 
temporary importance of such modifications, Marx opens up the prospects of a 
radical human emancipation by altogether abolishing the state and political 
bureaucracy as forms of social organization. This, according to Marx, is 
possible if organized labor, the only class whose ultimate interests coincide 
with those of mankind as a whole, practically removes the economic and political 
monopoly of any particular social group. The atomized, disintegrated world of 
the owners of commodities would, in such a way, be superseded by an integrated 
community of producers. The state would be replaced by organs of 
self‑management, i.e. by institutions composed of the true 
representatives of the people, who have been elected by a general free vote, who 
are immediately responsible to and replaceable by their voters, and who do not 
enjoy any privileges for the duties they perform.III. – The nature of the key concepts in Marx’s 
anthropology and philosophy of history best shows the character of his 
theoretical thought. These concepts are not only descriptive and explanatory but 
also value‑laden and critical.Thus Marx’s criticism of the fetishism of 
commodities in Capital can be understood only if we bear in mind his 
assumption of a truly human production, in which man affirms both himself 
and the other:1) by objectifying his individuality and by 
experiencing his personality as an objective, sensate power by an immediate 
awareness that through his activity and through the use of his product, the 
needs of all other human being can be satisfied;2) by mediating between the other and generic 
human being (his activity become part of the other’s, whom it has enriched and 
complemented) so as to allow man to immediately affirm and fulfill his own 
generic being.[2]Alienated labor is labor which lacks these 
qualities.In a similar way the concepts of social man, 
human needs, history, freedom, the state, capital, communism, etc. always 
imply a distinction between actual and possible, between factual and ideal.Social man is not just the individual who 
lives together with other individuals, or who conforms to the given norms of a 
society. Such a person can be very far from reaching the level of a social 
being. On the other hand, a person may be compelled to live in isolation and 
still profoundly need others, and carry in his language, thinking, and feeling 
all the essential characteristics of generic human being.In this sense, Marx distinguishes, for example, 
between mail who regards woman as “prey and the handmaid of communal lust,” “who 
is infinitely degraded in such an existence for himself,” and man whose “natural 
behavior towards woman has become human” and “whose needs have become human 
needs.” This “most natural, immediate and necessary relationship” shows to what 
extent man “is, in his individual existence, at the same time a social being."[3]Furthermore, history is not just a series 
of events in time – it presupposes supersession of “the realm of necessity” and 
full emancipation of man. That is why Marx sometimes labelled history of our 
time as “prehistory."Freedom never meant for Marx only choice 
among several possibilities or “the right to do and perform anything that does 
not harm others.” Freedom in Marx’s sense is the ability to self‑determine and 
to rationally control the blind forces of nature and history. “All emancipation 
is restoration of the human world and the relationships among men themselves.” [4]The state is not just any social 
organization which directs social processes and takes care of the order and 
stability of the society. The typical feature of the state, according to Marx, 
is its coercive character as an instrument of the ruling class. The state is 
institutionalized alienated power. Thus Marx very definitely maintained that the 
labor movement must abolish the state very soon after successful revolution, and 
replace it by the associations of workers.Capital is not only objectified labor, 
stored up in the form of money or any particular commodity. It is the 
objectified labor which at a given level of material production appropriates 
surplus value. The objective form of capital conceals and mystifies a social 
relationship beyond it; the object mediates between those who produce and those 
who rule.There is no doubt that in both the early and the 
mature writings the concept of communism does not only express a possible 
future social state, but also contains an evaluation of that state. In 
Economic and philosophical manuscripts there are even three different 
descriptions and evaluations: 1: “crude communism” in which “the domination of 
material property looms so large that it aims to destroy everything that is 
incapable of being possessed by everyone as private property"; 2: communism “(a) 
still political in nature, (b) with the abolition of the state, yet still 
incomplete and influenced by private property, that is, by the alienation of 
man"; 3: communism “as the positive abolition of private property, and of human 
self‑alienation.” [5] But even when, in The German ideology, Marx denies that 
communism is “an ideal to which reality will have to adjust, he says, we 
call communism the real movement which abolishes the present state of 
affairs.” [6] Here the adjective “real” clearly is a value term.Therefore any attempt to determine the nature of 
Marx’s scientific thought should lead to the conclusion that it is both 
knowledge and a vision of the future. As knowledge it is vastly different from 
the idea of knowledge propounded in any variant of empiricist philosophy, 
because for Marx, our future project determines the sense of everything in the 
present and the past, and this preliminary vision of the future is more an 
expression of revolt than it is a simple extrapolation of the present trends 
determined in an empirical way. And still, no matter how bold and pervaded by 
passion is this vision of the future, it is not merely an arbitrary dream or a 
utopian hope. The future is not a logical inference from the present, it is not 
the result of a prediction made according to the methodological standards of 
empirical science, nor is it divorced from the present and the past. At the 
beginning of inquiry, it is a relatively a priori projection (based more 
on preceding theory than on empirical data). But when, at the end of inquiry, it 
has been shown that the preliminary vision has been confirmed by all available 
evidence about actual trends in the present reality, then a posteriori, 
this vision of the future becomes meaningful knowledge.This dialectic between the future and the 
present, the possible and the actual, philosophy and science, value and fact, 
a priori and a posteriori, criticism and description, is perhaps 
the essential methodological contribution of Marx to contemporary science – one 
which so far has not been sufficiently taken into account, even by the followers 
of Marx themselves.IV. – In order to clarify and further elaborate 
our contention about the critical character of Marx’s scientific thought, we 
should add the following qualifications:1. Criticism is present in all Marx’s works and 
at all stages of his intellectual development, To make a sharp distinction 
between the value‑laden humanist utopia of the young Marx and the value‑free 
scientific structuralism of the mature Marx would be a grave error, indicating a 
superficial study of his work. To be sure, there are some important differences 
in methodology, in richness, and concreteness of the conceptual apparatus used, 
in the extent to which theory is supported by empirical evidence. However, the 
fundamental critical position remains the same. There is often only a change of 
vocabulary, or a substitution of specific terms applicable to capitalistic 
society for general terms applicable to society in general. For example, what 
Marx calls “alienated labor” in his early writings (e.g. in Economic 
and philosophical manuscripts) will, in Capital, be called “the world 
of commodities.” Or, in his criticism of Hegel’s philosophy of the state Marx 
says that “the abolition of bureaucracy will be possible when general interest 
becomes a reality” and “particular interest really becomes general interest"; in 
Capital and in his analysis of the experience of the Paris Commune, Marx 
is much more concrete and explicit: associated producers will do away with the 
state and take the control over exchange with nature into their own hands.2. Marxist criticism is radical although not 
destructive in a nihilistic sense. Without understanding the Hegelian concept of 
aufheben, the nature of this criticism can hardly be grasped.In spite of the differences between Hegel’s and 
Marx’s methods, they both maintain that the idea of dialectical negation 
contains both a moment of discontinuity and a moment of continuity: a moment of 
discontinuity insofar as the given cannot be accepted as it is (as truth in 
Hegel’s logic, as satisfactory human reality in Marx’s interpretation of 
history), a moment of continuity insofar as a component of the given must be 
conserved as the basis for further development – it is only the inner limitation 
which must be overcome.Most Marxists are not quite clear about the 
nature of Marxist criticism, but this is not surprising, considering how few 
have tried to interpret him in the context of the whole intellectual tradition 
to which he belongs. However, a good deal of misunderstanding is of an 
ideological character. Thus, in order to develop a militant optimism or to 
express a natural revolt against market economy tendencies in underdeveloped 
socialist countries, some Marxists tend to underestimate the importance of those 
forms of civilization, of political democracy, of educational and welfare 
institutions which have been developed in Western industrial society. Marx took 
into account the possibility of such a primitive negation of private property 
and called it “crude” and “unreflective” communism, which “negates the 
personality of man in every sphere,” “sets up universal envy and levelling 
down,” “negates in an abstract way the whole world of culture and civilization,” 
and constitutes a regression to the “unnatural simplicity of the poor wantless 
individual who has not only not surpassed private property but has not yet even 
attained it.” [7] Thus, there can hardly be any doubt that, for Marx, a true negation 
of class society and alienated labor is possible only at a high level of 
historical development.Such a negation presupposes an abundance of 
material goods, various civilized patterns of human behavior (which arise as 
scarcity is overcome), and, most important of all, an individual who, among 
other things, has overcome at least the elementary, rudest forms of greed for 
material objects.In this respect, some Marxists are overly‑radical 
critics, who fail to realize that certain features of advanced capitalism are 
necessary conditions for any higher forms of society. But these same Marxists, 
in some other essential respects, give the impression of being reformers; they 
remain quite satisfied with certain initial changes, and all too soon become 
interested in preserving the status quo, rather than in persisting in their 
revolutionary role, and striving for further and deeper structural changes.What present day socialism offers as a practical 
solution to the fundamental problems of alienated labor and political alienation 
is far from constituting truly radical criticism, or from really superseding the 
alienation of capitalist society.As we have said, the main source of exploitation 
and of all other aspects of economic alienation lies in the rule of objectified, 
stored‑up labor over living labor.[8] The social group disposing of stored‑up labor is able to 
appropriate surplus value. The specific historical form of this structure in 
Marx’s time was the disposal of capital on the grounds of private ownership of 
the means of production: however, private property is not the cause but the 
effect of alienated labor. Abolition of the private ownership of the means of 
production is only abolition of one possible specific form of the rule of dead 
labor over living labor. The general structure remains if there is any other 
social group such as, for example, bureaucracy, which retains monopoly on 
decision making concerning the disposal of accumulated and objectified labor. 
Therefore, only such criticism might be considered radical and truly 
revolutionary which puts a definitive end to exploitation and which aims at 
creating conditions in which associate producers themselves will dispose of the 
products of their labor.Another example. If the state, as such, is 
historically a form of alienated political power, the abolition of the 
bourgeois state is only an important step in the process of disalienation 
of politics. This step, according to Marx, (and Lenin in State and 
revolution) must be followed by a transition period of gradually 
withering away of any coercive state apparatus. Unless such an apparatus is 
replaced by an entirely different social organization, all the symptoms of 
political alienation, such as apathy, distrust, lust for power, need for 
charismatic leadership and for ideological rationalization, use of all available 
techniques for manipulating masses, etc. will be reproduced.Insofar as in man there is a profound Faustian 
need to rebel against any permanent, historically‑determined limitations in 
nature, in society and in himself, he will strive to supersede such limitations, 
to develop further his human world and his own nature. Such an activistic 
attitude towards the world will always need a philosophical and scientific 
thought which would constitute a bold radical criticism of existing reality.1. K. Marx, Capital, Afterword to the second German 
edition.2. K. Marx and F. Engels, Gesamtausgabe, I, 
vol. 3, Berlin, Marx‑Engels Verlag, 1929‑1932, p. 546.3. E. Fromm, Marx’s concept of man, with a 
translation from Marx’s economic and philosophical manuscripts, New York, 
Ungar, 1961, pp. 126‑127.4. K. Marx, “On the Jewish question,” in: L. Easton 
(ed.), Writings of the young Marx on philosophy and society, New York, 
Doubleday, 1967.5. Marx, Economic and philosophical manuscripts, 
op. cit., p. 127.6. Marx, “German ideology,” in. Easton (ed.), op. cit., 
p. 426.7. Marx, Economic and philosophical manuscripts, 
op. cit., p. 125.8. K. Marx and F. Engels, Archiv, Moscow, Marx‑Engels 
Institute, 1933, p. 68.  Mihailo Marković Archive
