 
TOWARD the end of the first week of the Bolshevik uprising, Zinoviev and Kaminev lost heart. With Mylutin, Nogin and Rykov, they handed in resignations to the Central Executive Committee of the Communist Party. Lenin read the letters at a great meeting in Smolny and cried: "Shame upon these men of little faith, who hesitate and doubt!"

Lenin has long since conquered their opposition, but he has not changed their souls. Kaminev and Zinoviev are in his cabinet, as satellites, however, not as leaders; they are the weakest members.

Kaminev is President of the Moscow Soviet and a member of the All-Russian Central Executive Committee of Soviets.  He is on the national famine committee and probably makes a very good impression on the American Relief people with whom he comes in contact. They ought to feel quite at home with him; he has the genial manners of an American small-town politician.

He has certainly retained a middle class consciousness in spite of five years of actual revolutionary experience, in spite of what is known in Russia as a steady revolutionary past. I don't think his idea of Communist discipline can be very well defined. He likes to be magnanimous and promises everything to everybody. He is generous, and since it is hard to be generous in a Socialist State where one owns nothing to be generous with, his only recourse is to be generous with the property of the State.

In a glow of hospitality he once gave a sable coat worth thousands of dollars to a lady he admired and was genuinely astonished and grieved, I think, by the bitter criticisms then hurled at him. Had there been enough "trusted and trained" men to go round (which there never are in any government), this particular attack of giving might have cost him his career, though in any other society his actions would have been quite normal.

Kaminev is an excellent subordinate and capable of making a fortune in a capitalist society. He cannot be accused of having no ideals; he can be accused only of having the average politician's moral stamina.

Bela Kun is the only other Soviet official who parallels Kaminev's distrust of the press. I once went to Bela Kun's office to get a statement. After ten promises and ten delays, I was in a small rage myself and asked him why he had not said in the first instance that he had no intention of giving a statement.  He looked up from his desk, ran his chubby fingers through his hair, and said in a bewildered way, "Well, you see, I have just taken aspirin." A young Russian officer sitting nearby smiled at me and said dryly, in English, "No wonder the Hungarian Soviets fell!"

Kaminev will probably remain in office many years for no better or no worse reason than the reason for which many of our Senators are returned to Washington term after term.

Zinoviev's position is much more important and much harder to define. Aside from being in the Cabinet, he is President of the Petrograd Commune and President of the Third International. He really has great power and he is known to be wilful and arbitrary. His party opponents claim that his capriciousness has split the German Communist party into six factions and confused and alienated most of the parties in the other countries.

A Russian professor, who was interested in political movements merely as an observer, explained Zinoviev's curious tenacity in office in this way:  "The Communists," he said, "insist so strongly upon party discipline and party loyalty that they have never been able to face the fact that Zinoviev is not a leader."

Zinoviev's appearance is against him.  He is short, heavily built, flabby. Yet he is not devoid of vanity; he is the most photographed man in Russia. And while he has little imagination, he has real dramatic sense, he has a way of staging everything.

At Baku, at one moment he had the Easterners unsheathing their swords and declaring a holy war; at another moment he had a group of Mohammedan women in the gallery tearing off their veils. All this was distinctly impressive and dramatic, but the real drawback was that the effect of the second act almost ruined the effect of the first.

I often wonder if a man can be a great leader, and be utterly devoid of humor. When Zinoviev says anything clever he is not even aware of it and invariably spoils it by a second act.

During the Kronstadt uprising the Esthonian government, with its wish father to its thought, sent a wire to Petrograd which read: "What sort of government have you in Petrograd?"  And Zinoviev replied: "The same government that has been here since 1917."

If he could only have stopped at that one sentence he might have been able to lead a world revolution, or at least catch the imagination of the workers. Instead he insisted upon being didactic. He wired page after page, delivering an orthodox Marxian lecture! But the International is not without men of talent and daring. There are men under Zinoviev - Italians, Frenchmen, Germans, Bulgarians, Americans, with real fire and ability.

Bucharin, editor of Pravda, is by far the most brilliant of the Russians. No matter how much one may disagree with Bucharin, one must concede his brilliancy. The Letts have an exceptionally able group in the International.  Some of these men were actually in power during the brief time that Latvia was "red." The leader of the group is a stately old man called Stootchka. For four months he was President of Latvia.

Written: Between 1921 - 1923

Source: "Mirrors of Moscow", New York by Thomas Seltzer, 1923

First Published: 1923

Online Version: marxists.org 1999

Transcription/Markup: Alf Pangas

Mirrors of Moscow contents

The Marxist writers' Archives
