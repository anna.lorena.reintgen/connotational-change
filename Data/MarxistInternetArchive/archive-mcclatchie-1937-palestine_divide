Gilbert McClatchie
Source: Socialist Standard, September 1937.
Transcription: Socialist Party of Great Britain.
HTML Markup: Adam Buick
Public Domain: Marxists Internet Archive (2016). You may freely copy, distribute, display and perform this work; as well as make derivative and commercial works. Please credit "Marxists Internet Archive" as your source.
The Royal Commission's proposal to solve the Palestine troubles by partition has met with a mixed reception.

The recommendation is to split Palestine up into three pieces, an Arab State, a Jewish State, and a portion which will remain under the British Mandate.

At the recently concluded Zionist Congress opinion was sharply divided. A two-thirds majority, headed by the Zionist Chairman, Dr. Weizmann, voted in favour of the principle of partition, largely on the grounds that it was the best that could be expected from the British Government under the circumstances. The decision was only come to after long and heated discussion, and the news of it called forth protest from some leading Zionists in America and elsewhere.

Among the professed spokesmen of working-class interests there is also a conflict of views.

Communist writers, who claim that the Jews in Palestine have been building up a flourishing business under the protection of the British Government, back the Arabs. They urge resistance to the partition proposal and the establishment of an independent Arab State in Palestine, of which the Jews are to be members on an equal basis. The Communists put this forward on two grounds: The right of the Arabs to self-determination and the need to curb British Imperialism.

On the other side, opponents of the Communists, like Abramovich and Orenstein, favour the continuance of the mandatory system and oppose the Arab viewpoint. In doing so they overstate the Jewish case, though probably unconsciously, painting a beautiful picture of Palestine under the Jews, where Socialist ideas will flourish.

The state of affairs in Palestine is not clear cut. There is an old social system side by side with a new, and both subject to an over-ruling Imperial power. Within each again there are class interests that cut across racial and national feelings. And, to still further complicate matters, there is the peculiar international position of the Jew.

The Arab lives under a semi-feudal regime with the land-owner despoiling the peasant of nearly everything. The coming of the Jews introduced modern industrial methods which threaten the incomes of land-owners, partly by offering the peasants a way of escape from fleecing, and partly by competition. Hence the influential Arabs are opposed to the continued immigration of Jews and strive to stir racial hatred among the poor by using the religious bogey. They are opposed to partition and want an independent Arab State.

The Jew, hunted out of various occupations in the East and the victim of pogroms, has been drifting into Palestine for decades. The immigration of the Jew has been vastly increased since the War, until at present it has reached from fifty to sixty thousand a year. How the Jews are permeating Palestine may be appreciated from the fact that, in spite of Arab immigration, the Jewish section rose from 17 per cent. of the population in 1930 to 30 per cent. in 1936. This antagonises the Arab, who foresees himself being swamped in the rising flood. On the other hand, the growing Jewish section pays the bulk of the tax revenue but receives the least benefit from it, which is a permanent source of complaint.

To the Jew, interference with immigration would have serious consequences. Palestine is perhaps the only place to which he has free access. A large proportion of the immigrants are representatives of families that have been left behind in other countries in dire straits, penniless and denied almost any opportunity of earning a living. To these the immigrant sends back contributions that enable the relatives to buy the necessaries of life. Thus, as one writer puts it, if the Arabs succeed in stopping Jewish immigration into Palestine it will mean starvation to millions of Jews.

The Jewish migration has brought with it to Palestine the capitalist system with its antagonistic classes. While, on the one hand, it is making the desert flower and bringing into existence flourishing towns, on the other hand it is replacing the feudal method of fleecing by the capitalist exploitation of wage-workers. To quote Mordekai Orenstein: You will find in Palestine a highly organised capitalist class, a vigorous and aggressive Jewish clericalism, and a modern Jewish Fascism with all the usual characteristics from strike-breaking to the base murder of a distinguished working-class leader. (Page 10, Jews, Arabs and British in Palestine.) He also adds that you will find there a strongly organised working class. But, towards the end of the pamphlet, he laments that: Considerable sections of Jewish workers in Palestine have not as yet reached the realisation of the vital urgency of forging this supernational weapon [Jewish-Arab proletarian unity] in the political struggle in Palestine.(Page 21.)It may be added that partition, by restricting the area open to Jews, must have a considerable effect on their immigration, which is some explanation of Zionist opposition.

The attitude of the British Government is based on simple principles: The safeguarding of British capitalists' interests, as represented by such things as the oil pipe between Mosul and Haifa; security of Imperial air routes, communications through the Suez Canal, and so forth. Their policy of divide and rule leads them to favour different sides at different times, and to keep racial animosities alive as long as they do not become too dangerous.

The support of both Jews and Arabs during the War was bought by promises that have not been kept, and cause irritation to both sections.

The Mandate has evidently outlived its uses to British capitalism, and the partition system is to take its place. This will give Arab and Jew (like the North and South of Ireland) something to quarrel over for years to come, to the hindrance of propaganda for working-class solidarity against the international capitalist class.
Gilbert McClatchie Archive| Socialist Party of Great Britain
