
Maxim Gorky
 
In front of you is the main street, with two rows of
miserable-looking huts with shuttered windows and old walls 
pressing on each other and leaning forward.  The roofs of 
these time-worn habitations are full of holes, and have been 
patched here and there with laths; from underneath them 
project mildewed beams, which are shaded by the dusty-leaved 
elder-trees and crooked white willow--pitiable flora of 
those suburbs inhabited by the poor.

The dull green time-stained panes of the windows look upon 
each other with the cowardly glances of cheats.  Through the 
street and toward the adjacent mountain runs the sinuous 
path, winding through the deep ditches filled with 
rain-water.  Here and there are piled heaps of dust and other 
rubbish--either refuse or else put there purposely to keep 
the rain-water from flooding the houses.  On the top of the 
mountain, among green gardens with dense foliage, beautiful 
stone houses lie hidden; the belfries of the churches rise 
proudly toward the sky, and their gilded crosses shine beneath
the rays of the sun.  During the rainy weather the
neighboring town pours its water into this main road, which, 
at other times, is full of its dust, and all these miserable 
houses seem, as it were, thrown by some powerful hand into 
that heap of dust, rubbish, and rainwater.  

They cling to the ground beneath the high mountain, exposed 
to the sun, surrounded by decaying refuse, and their sodden 
appearance impresses one with the same feeling as would the 
half-rotten trunk of an old tree.

At the end of the main street, as if thrown out of the town, 
stood a two-storied house, which had been rented from 
Petunikoff, a merchant and resident of the town.  It was in 
comparatively good order, being farther from the mountain, 
while near it were the open fields, and about half-a-mile 
away the river ran its winding course.

This large old house had the most dismal aspect amid its 
surroundings.  The walls bent outward, and there was hardly 
a pane of glass in any of the windows, except some of the 
fragments, which looked like the water of the marshes--dull 
green.  The spaces of wall between the windows were covered 
with spots, as if time were trying to write there in 
hieroglyphics the history of the old house, and the tottering 
roof added still more to its pitiable condition.  It seemed as 
if the whole building bent toward the ground, to await the 
last stroke of that fate which should transform it into a 
chaos of rotting remains, and finally into dust.

The gates were open, one-half of them displaced and lying on 
the ground at the entrance, while between its bars had grown 
the grass, which also covered the large and empty court-yard. 
In the depths of this yard stood a low, iron-roofed, 
smoke-begrimed building.  The house itself was of course 
unoccupied, but this shed, formerly a blacksmith's forge, 
was now turned into a "dosshouse," kept by a retired captain 
named Aristid Fomich Kuvalda.

In the interior of the dosshouse was a long, wide and grimy 
board, measuring some 28 by 70 feet.  The room was lighted 
on one side by four small square windows, and on the other 
by a wide door.  The unpainted brick walls were black with 
smoke, and the ceiling, which was built of timber, was almost 
black.  In the middle stood a large stove, the furnace of which 
served as its foundation, and around this stove and along the 
walls were also long, wide boards, which served as beds for 
the lodgers.  The walls smelt of smoke, the earthen floor of 
dampness, and the long, wide board of rotting rags.

The place of the proprietor was on the top of the stove, 
while the boards surrounding it were intended for those who 
were on good terms with the owner, and who were honored by 
his friendship.  During the day the captain passed most of his 
time sitting on a kind of bench, made by himself by placing 
bricks against the wall of the court-yard, or else in the 
eating-house of Egor Yavilovitch, which was opposite the 
house, where he took all his meals and where he also drank 
vodki.

Before renting this house, Aristid Kuvalda had kept a registry 
office for servants in the town.  If we look further back into 
his former life, we shall find that he once owned printing 
works, and previous to this, in his own words, he "just lived! 
And lived well too, Devil take it, and like one who knew how!"

He was a tall, broad-shouldered man of fifty, with a
raw-looking face, swollen with drunkenness, and with a
dirty yellowish beard. 

His eyes were large and gray, with an insolent expression of 
happiness.  He spoke in a bass voice and with a sort of 
grumbling sound in his throat, and he almost always held 
between his teeth a German china pipe with a long bowl. When 
he was angry the nostrils of his big, crooked red nose swelled,
and his lips trembled, exposing to view two rows of large and 
wolf-like yellow teeth.  He had long arms, was lame, and always 
dressed in an old officer's uniform, with a dirty, greasy cap 
with a red band, a hat without a brim, and ragged felt boots 
which reached almost to his knees.  In the morning, as a rule, 
he had a heavy drunken headache, and in the evening he caroused.
However much he drank, he was never drunk, and so was always 
merry.

In the evenings he received lodgers, sitting on his brick-made 
bench with his pipe in his mouth.

"Whom have we here?" he would ask the ragged and tattered object 
approaching him, who had probably been chucked out of the town 
for drunkenness, or perhaps for some other reason not quite so 
simple.  And after the man had answered him, he would say, "Let 
me see legal papers in confirmation of your lies."  And if there 
were such papers they were shown. The captain would then put 
them in his bosom, seldom taking any interest in them, and would 
say:  "Everything is in order.  Two kopecks for the night, ten 
kopecks for the week, and thirty kopecks for the month.  Go and 
get a place for yourself, and see that it is not other people's, 
or else they will blow you up.  The people that live here are 
particular."

"Don't you sell tea, bread, or anything to eat?"

"I trade only in walls and roofs, for which I pay to the 
swindling proprietor of this hole--Judas Petunikoff, merchant 
of the second guild--five roubles a month," explained Kuvalda 
in a business-like tone.  "Only those come to me who are not 
accustomed to comfort and luxuries. . .but if you are 
accustomed to eat every day, then there is the eating-house 
opposite.  But it would be better for you if you left off that 
habit. You see you are not a gentleman. What do you eat? You 
eat yourself!"

For such speeches, delivered in a strictly business-like manner,
and always with smiling eyes, and also for the attention he paid 
to his lodgers, the captain was very popular among the poor of 
the town.  It very often happened that a former client of his 
would appear, not in rags, but in something more respectable and 
with a slightly happier face.

"Good-day, your honor, and how do you do?"

"Alive, in good health! Go on."

"Don't you know me?"

"I did not know you."

"Do you remember that I lived with you last winter for nearly a 
month . . . when the fight with the police took place, and 
three were taken away?" 

"My brother, that is so. The police do come even under my 
hospitable roof!"

"My God! You gave a piece of your mind to the police inspector 
of this district!"

"Wouldn't you accept some small hospitality from me? When I 
lived with you, you were. . . ." 

"Gratitude must be encouraged because it is seldom met with. 
You seem to be a good man, and, though I don't remember you, 
still I will go with you into the public-house and drink to 
your success and future prospects with the greatest pleasure."

"You seem always the same . . . Are you always joking?"

"What else can one do, living among you unfortunate men?"

They went.  Sometimes the Captain's former customer, uplifted 
and unsettled by the entertainment, returned to the dosshouse, 
and on the following morning they would again begin treating 
each other till the Captain's companion would wake up to 
realize that he had spent all his money in drink.

"Your honor, do you see that I have again fallen into your 
hands? What shall we do now?"

"The position, no doubt, is not a very good one, but still 
you need not trouble about it," reasoned the Captain.  "You 
must, my friend, treat everything indifferently, without 
spoiling yourself by philosophy, and without asking yourself 
any question.  To philosophize is always foolish; to 
philosophize with a drunken headache, ineffably so.  Drunken 
headaches require vodki, and not the remorse of conscience 
or gnashing of teeth . . . save your teeth, or else you will 
not be able to protect yourself.  Here are twenty kopecks.  Go 
and buy a bottle of vodki for five kopecks, hot tripe or lungs, 
one pound of bread and two cucumbers.  When we have lived off 
our drunken headache we will think of the condition of 
affairs. . . ."

As a rule the consideration of the "condition of affairs" 
lasted some two or three days, and only when the Captain had 
not a farthing left of the three roubles or five roubles given 
him by his grateful customer did he say:  "You came! Do you 
see?  Now that we have drunk everything with you, you fool, 
try again to regain the path of virtue and soberness.  It has 
been truly said that if you do not sin, you will not repent, 
and, if you do not repent, you shall not be saved.  We have done
the first, and to repent is useless. Let us make direct for 
salvation.  Go to the river and work, and if you think you 
cannot control yourself, tell the contractor, your employer, 
to keep your money, or else give it to me.  When you get 
sufficient capital, I will get you a pair of trousers and 
other things necessary to make you seem a respectable and 
hard-working man, persecuted by fate.  With decent-looking 
trousers you can go far.  Now then, be off!"

Then the client would go to the river to work as a porter, 
smiling the while over the Captain's long and wise speeches. 
He did not distinctly understand them, but only saw in front 
of him two merry eyes, felt their encouraging influence, and 
knew that in the loquacious Captain he had an arm that would 
assist him in time of need.

And really it happened very often that, for a month or so, 
some ticket-of-leave client, under the strict surveillance of 
the Captain, had the opportunity of raising himself to a 
condition better than that to which, thanks to the Captain's 
cooperation, he had fallen.

"Now, then, my friend!" said the Captain, glancing critically 
at the restored client, "we have a coat and jacket.  When I had 
respectable trousers I lived in town like a respectable man. 
But when the trousers wore out, I, too, fell off in the opinion 
of my fellow-men and had to come down here from the town.  Men, 
my fine mannikin, judge everything by the outward appearance, 
while, owing to their foolishness, the actual reality of things 
is incomprehensible to them.  Make a note of this on your nose, 
and pay me at least half your debt. Go in peace; seek, and you 
may find."

"How much do I owe you, Aristid Fomich?" asks the client, in 
confusion.

"One rouble and 70 kopecks . . . Now, give me only one rouble, 
or, if you like, 70 kopecks, and as for the rest, I shall wait 
until you have earned more than you have now by stealing or by 
hard work, it does not matter to me."

"I thank you humbly for your kindness!" says the client, touched 
to the heart.  "Truly you are a kind man . . .; Life has 
persecuted you in vain . . . What an eagle you would have been 
in your own place!"

The Captain could not live without eloquent speeches.

"What does 'in my own place' mean?  No one really knows his own 
place in life, and every one of us crawls into his harness.  The 
place of the merchant Judas Petunikoff ought to be in penal 
servitude, but he still walks through the streets in daylight, 
and even intends to build a factory.  The place of our teacher 
ought to be beside a wife and half-a-dozen children, but he is 
loitering in the public-house of Vaviloff. 

"And then, there is yourself.  You are going to seek a situation 
as a hall porter or waiter, but I can see that you ought to be a
soldier in the army, because you are no fool, are patient and 
understand discipline.  Life shuffles us like cards, you see, and 
it is only accidentally, and only for a time, that we fall into 
our own places!"

Such farewell speeches often served as a preface to the
continuation of their acquaintance, which again began with 
drinking and went so far that the client would spend his last 
farthing.  Then the Captain would stand him treat, and they 
would drink all they had.

A repetition of similar doings did not affect in the least 
the good relations of the parties.

The teacher mentioned by the Captain was another of those 
customers who were thus reformed only in order that they 
should sin again.  Thanks to his intellect, he was the nearest 
in rank to the Captain, and this was probably the cause of his 
falling so low as dosshouse life, and of his inability to rise 
again.  It was only with him that Aristid Kuvalda could 
philosophize with the certainty of being understood.  He valued 
this, and when the reformed teacher prepared to leave the 
dosshouse in order to get a corner in town for himself, then 
Aristid Kuvalda accompanied him so sorrowfully and sadly that 
it ended, as a rule, in their both getting drunk and spending 
all their money.  Probably Kuvalda arranged the matter
intentionally so that the teacher could not leave the
dosshouse, though he desired to do so with all his heart.  Was 
it possible for Aristid Kuvalda, a nobleman (as was evident 
from his speeches), one who was accustomed to think, though 
the turn of fate may have changed his position, was it possible 
for him not to desire to have close to him a man like himself? 
We can pity our own faults in others.

This teacher had once taught at an institution in one of the 
towns on the Volga, but in consequence of some story was 
dismissed.  After this he was a clerk in a tannery, but again 
had to leave.  Then he became a librarian in some private 
library, subsequently following other professions.  Finally, 
after passing examinations in law he became a lawyer, but 
drink reduced him to the Captain's dosshouse.  He was tall, 
round-shouldered, with a long, sharp nose and bald head.  In 
his bony and yellow face, on which grew a wedge-shaped beard,
shone large, restless eyes, deeply sunk in their sockets, and 
the corners of his mouth drooped sadly down.  He earned his 
bread, or rather his drink, by reporting for the local papers. 
He sometimes earned as much as fifteen roubles.  These he gave 
to the Captain and said:

"It is enough. I am going back into the bosom of culture. 
Another week's hard work and I shall dress respectably, and 
then Addio, mio caro!"

"Very exemplary!  As I heartily sympathize with your decision, 
Philip, I shall not give you another glass all this week," the 
Captain warned him sternly.

"I shall be thankful! . . . You will not give me one drop?"

The Captain beard in his voice a beseeching note to which he 
turned a deaf ear.

"Even though you roar, I shall not give it you!"

"As you like, then," sighed the teacher, and went away to 
continue his reporting. 

But after a day or two he would return tired and thirsty, and 
would look at the Captain with a beseeching glance out of the 
corners of his eyes, hoping that his friend's heart would 
soften.

The Captain in such cases put on a serious face and began 
speaking with killing irony on the theme of weakness of 
character, of the animal delight of intoxication, and on such
subjects as suited the occasion.  One must do him justice:  he 
was captivated by his role of mentor and moralist, but the 
lodgers dogged him, and, listening sceptically to his 
exhortations to repentance, would whisper aside to each other:

"Cunning, skilful, shifty rogue!  I told you so, but you would 
not listen.  It's your own fault!"

"His honor is really a good soldier.  He goes first and examines 
the road behind him!"

The teacher then hunted here and there till he found his friend 
again in some corner, and grasping his dirty coat, trembling 
and licking his dry lips, looked into his face with a deep, 
tragic glance, without articulate words.

"Can't you?" asked the Captain sullenly.

The teacher answered by bowing his head and letting it fall on 
his breast, his tall, thin body trembling the while.

"Wait another day . . . perhaps you will be all right then," 
proposed Kuvalda.  The teacher sighed, and shook his head 
hopelessly.

The Captain saw that his friend's thin body trembled with the 
thirst for the poison, and took some money from his pocket.

"In the majority of cases it is impossible to fight against 
fate," said he, as if trying to justify himself before someone. 

But if the teacher controlled himself for a whole week, then 
there was a touching farewell scene between the two friends, 
which ended as a rule in the eating-house of Vaviloff.  The 
teacher did not spend all his money, but spent at least half 
on the children of the main street.  The poor are always rich 
in children, and in the dirt and ditches of this street there
were groups of them from morning to night, hungry, naked and 
dirty.  Children are the living flowers of the earth, but 
these had the appearance of flowers that have faded 
prematurely, because they grew in ground where there was no 
healthy nourishment.  Often the teacher would gather them round 
him, would buy them bread, eggs, apples and nuts, and take 
them into the fields by the river side.  There they would sit 
and greedily eat everything he offered them, after which they 
would begin to play, filling the fields for a mile around with
careless noise and laughter.  The tall, thin figure of the
drunkard towered above these small people, who treated him 
familiarly, as if he were one of their own age.  They called 
him "Philip," and did not trouble to prefix "Uncle" to his 
name.  Playing around him, like little wild animals, they 
pushed him, jumped upon his back, beat him upon his bald head, 
and caught hold of his nose.  All this must have pleased him, 
as he did not protest against such liberties.  He spoke very 
little to them, and when he did so he did it cautiously as if
afraid that his words would hurt or contaminate them.  He 
passed many hours thus as their companion and plaything, 
watching their lively faces with his gloomy eyes. 

Then he would thoughtfully and slowly direct his steps to the 
eating-house of Vaviloff, where he would drink silently and 
quickly till all his senses left him.

Next: Section II     

Table of Contents: Creatures That Once Were Men
