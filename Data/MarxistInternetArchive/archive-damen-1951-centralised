Onorato Damen 1951
First published: in the book Bordiga, validita e limiti d'una esperienza nella storia della “sinistra italiana”
Source: Leftcom;
Translated: by J.S. Daborn.We should first address the issue of centralism which the
“Programmists” have never been able to define in an
“organic” way. Linked as it is to the interpretation of a
given historical experience, it simply cannot be reduced to formal and
scholastic abstractions.These muddle-headed “left communists” argue thus: in
Lenin’s International, there were no “pure communist
parties’ so the use of the democratic mechanism was inextricably
linked to what went at in that particular historical time. It is
therefore obvious that an International unlike the Third, which consists
of “pure communist parties” should be identified by a
different internal mechanism and not by democratic centralism, which
ceased to be operative with the death of Lenin. What happened after
that, in the Stalinist era, is not covered in their analysis because it
had nothing to do with the working class and the objectives of the
revolution.But to suppose, as the “Programmists” do, an organisation
in a state of chemical purity, an international of “pure Communist
parties” as opposed to that of Lenin made of “impure
parties,” is playing with a metaphysical paradox. Instead of
formulating the problems of a whole series of historical events through
the lenses of dialectical materialism, they adopt a formal mechanistic
calculation, which tends to get lost in the fog of the most obsolete
idealism.We can tell these comrades in all certainty that there will be no
international of pure communist parties, but only an international that
will reflect within it the good and the evil, the contradictions and
absurdity, of a society divided into classes, themselves torn by various
layers of interest, social conditions, culture, etc. The assumption of
communist parties in a pure state with an equally pure world
organisation, even as a simple aspiration, is not the result of any
serious investigation based on Marxism. It strangely resembles a certain
mysticism which had its heyday in the twenty years of fascism.Lenin’s International certainly had its weaknesses, due to the
immaturity of the historical period that followed the collapse of the
Second International and the crisis then afflicting the capitalist
world. Every proletarian organisation reproduces, though in a more
advanced way, and on an inversely proportional scale, the
characteristics of the historical period in which it was formed. And it
is certain that the negative aspects present in the Third International
will be present, although differently articulated in future
international organisations, as amply proved by the objective conditions
in which the various Left Communist groupings, who today claim the right
to make a contribution to the reconstruction of the international
proletarian party, are operating. Amongst these groups, the one that
suffers most from intolerance and crises is the Bordigist
“Communist Programme” where the dynamics of democratic
centralism work more deeply, as seen in the explosive cycle of its
internal contradictions. Today, for polemical convenience, the
“Programmists” would like to pass off the Third
International as made up of “impure” parties. But
here’s how Bordiga previously judged Lenin’s International,
in clear contradiction with the current positions.The real strength of these Bordigists lies in their
inconsistency!How can this group, with its structure of an aristocratic and
intellectual elite, with a filtered and distilled Marxism, developed in
backrooms rather than in the storm of class struggle, contest the
accuracy of what we are saying? So then, how can we resolve, with
Leninist integrity, the debate over the two faces of centralism?In the phase of imperialist domination and proletarian revolution no
organisation of the revolutionary party can conceivably exist which is
not based on a highly centralised structure. Perhaps this is the feature
that most dramatically distinguishes it from parliamentary parties. If
centralism is therefore an imperative requirement imposed by class
conflict, the attributes of “democratic” and
“organic” define the subjective terms of a polemical
distinction that has never affected the substance of this
centralisation. Who can say with absolute precision how far bodies
involved in this centralisation make use of the tools of democracy
(active participation and active control of the rank and file) and how
far the centres of power are based on an authoritarian regime in the
physical person of a leader, and through him, to the Central
Committee?For the Bordigists of “Programma” the problem is posed in
terms that come from the counterrevolutionary practice of Stalinism.
This is how they tried, finally, to clarify their extraordinary theory
that goes under the name of “organic centralism.” We have
reproduced it above in the same words in which it was formulated.But we need to clarify once and for all the relationship that must
exist between the centre and the base so that the party is structured
and operates according to Leninist principles. An ongoing dialectical
relationship exists between the members and the party centre. It is
obviously on the basis of that relationship, in the context of
theoretical and political platform already agreed that the party
leadership develops its tactical action. Lenin never advocated, either
in theory or in his political actions, any other way in which the
organisation could act. And how can we understand the organisational
formula of a Central Committee or of a leader who relies only on
himself, on his capacity as related to a “set” of
already planned possible moves (our emphasis) in relation to
no less foreseen outcomes whilst the “so-called
membership can usefully be ordered to perform actions indicated by the
leadership?”It simply means the same as the policy of the Central Committee under
Stalin, once all working class elements had been eliminated from the
dictatorship of the proletariat. It means a deep and irreparable rupture
between the members of the party and its directing centre and the
resulting slide into the open reconstruction of capitalism. It also
means that the Central Committee of the Russian Communist Party and
Stalin himself was tied to a “set” of possible moves that
were perfectly planned in advance, that would be carried out with equal
accuracy, in terms, and in a reality, we all know. What we are
denouncing are the disastrous consequences which occur in a supposedly
revolutionary party when its central organ, as a body, operates outside
of the bounds and control of the organisation’s membership.But closer to our experience, we have to denounce precisely those who
postulate, or allow to be postulated, this laughable distinction between
a political membership required only to carry out acts indicated by the
centre and a centre that is entrusted with such powers of foresight and
divination that it does not offer us a very encouraging sight. And here
we are dealing with comrades who in terms of preparation and long
militancy are highly skilled and command the respect and confidence of
the whole party.Was the leadership of the Communist Party of Italy (PCd'I), through
Bordiga’s declarations to the Comintern, perhaps not bound to a
set of possible options that denied the possibility of Fascism’s
rise to power at the very time when it was carrying out the March on
Rome? And was this glaring error of perspective not “in
correspondence with the no less foreseeable outcome” of
jeopardising the party with the tactic of the offensive for the
offensive’s sake?And who prepared a “scientific” analysis of the Russian
economy defining the October Revolution as anti-feudal revolution after
having celebrated it as a socialist? Had Bordiga not affirmed (in
Lenin nel cammino della rivoluzione): “The revolution
will be made in Russia, by and for the working class itself"? And
further: “Soviet power was victorious, the dictatorship of the
proletariat predicted by Marx, made its tremendous entrance onto the
stage of history"?How should we judge someone who was the most prominent exponent of
the party and of “left-wing communism” who refused to become
a “militant” in the Internationalist Communist Party at the
time of its formation, as he considered it a mistake to fight directly
against “the national communist party” (the PCI) [1] with the excuse that the workers were in the
party of Togliatti? Then, when our split occurred, agreed to enter the
PCd'I provided that the rump remained true to him, politically neutered
and reduced to a sect of repeaters of not always digested formulae?What was his contribution to the development of a critical
examination of the nature of the Second World War and the role played by
Russia as a major imperialist player, when he rejected our definition of
state capitalism to speculate about Russia as a spurious form of
“industrial state"?The questions could continue, but we have said enough to show how
ill-founded, precarious and objectively dangerous is his claim to assign
to the Central Committee and this or that person, whatever their esteem,
or skills of divination, the tasks of arbitrarily developing our theory,
and functions of leadership, outside of and above, the party as a
whole.Lenin, at his most personal and most decisive, by which we mean the
Lenin of the “April Theses” had a desperate determination to
“go to the sailors,” beyond the formal organisation of the
Bolshevik Party’s Central Committee whose positions which were
based on misunderstanding and compromise. Lenin was not operating on
organic or even democratic centralism here, but acting as the chief
pillar of the coming revolution, the only one who had understood and
endorsed the demands of the working class and this is because his feet
were firmly on a class terrain, because he thought and worked in class
terms, and for the class, and had a very lively sense of history which
teaches us that revolution loves action and hates cowards who turn up a
day late.In this constant dialectical relationship between the membership and
leadership of the party, in this necessary integration of freedom and
authority, lies the solution of a problem to which professional
objectors have perhaps paid too much attention.Any revolutionary party which is not a mere abstraction has to
address the problems of the class struggle in a historical climate in
which violence and unchallenged authority dominates. In order to
increasingly become a living instrument of combat it can only be
organised around the most iron unity. Its ranks therefore have to be
closed against the general thrust of the counter-revolution. The
revolutionary party does not ape bourgeois parties, but obeys
the need to adapt its organisational structure to the objective
condition of the revolutionary struggle.The elementary tactical principle of the revolutionary party in
action, is that it must take into account the characteristics of the
terrain on which it works and that its members are adequately prepared
for their tasks. We do not believe there needs to be disagreements on
the question of centralism. These only begin when we talk in
“democratic” or “organic” terms. The use, or
worse, the abuse, of the term “organic” can lead to forms of
authoritarian degeneration which break the dialectical relationship that
must exist between the leadership and the members. The experience of
Lenin is still valid, and it is vital to be able to fuse together, in a
single vision, the seeming contradiction between
“democratic” and “organic” centralism.Onorato Damen1. The
Italian Communist Party was formed under the leadership of Togliatti as
a completely Stalinist party after the war. It dropped the old name of
the Communist Party of Italy as a symbol that it no longer had
internationalist pretensions. 
Onorato Damen Archive
“After restoring proletarian theory, the practical
work of the Third International towered over the divisions raised by
opportunists of all countries in banning from the ranks of the
world’s vanguard all reformists, social democrats, and centrists
of all types. This renewal took place in all the old parties and is the
foundation of the new revolutionary party of the proletariat. Lenin
guided with an iron hand the difficult task of dispelling all confusions
and weaknesses.”