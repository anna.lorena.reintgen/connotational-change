Dawn To Dusk, E. H. Lane 1939These reminiscences of an active life closely associated with various phases of the Australian Labour Movement, are not in any sense a history of that movement. I have not had either records, data – or inclination to write such.Even as an autobiography these memoirs are incomplete, lacking many details and incidents which might well be included. But in many respects this record is of interest as indicative of some of the unseen currents and shallows a the movement that have been responsible for much of the disaster and shipwrecks that have marked the track of the workers towards their goal.These memoirs are at least an honest endeavour to give a true replica of the background and foundations of my beliefs – in the supreme rightness and inevitability of Communism as the one and only road whereby the workers – the mass of the people – can free themselves from exploitation. Despite the buffetings of Fate, almost continual betrayal and treacheries, I have never faltered in this faith – have never in the slightest measure doubted its truth and glorious destiny, and it is from this angle alone that I have approached and met all the problems of human existence which we all have to face.Thus as a confirmed Communist, admitting no other solution whereby to solve the evils of capitalist society, I have applied that standard of ethics and economics to those individuals and organisations who presume to act and speak on behalf of the disinherited toilers. If my judgment of men as disclosed in these memoirs appears harsh or intolerant, it is solely the fault of the condemned who have wittingly or unwittingly followed false gods – and grossly betrayed their trust. In the crucible of life which immutably tests men’s souls they have proved dross, lacking that fidelity to principle and ideals without which man is a poor puppet and plaything of fate.To clear away any misunderstanding, I specially desire to emphasise the fact that the Australian Labour Party – or any other party for that matter – is NOT the Labour Movement. The A.L.P. is just a section of the movement, and, as its deplorable later history shows only too plainly, a very mediocre and futile section. But with an effrontery typical of Labour politicians, Labour Party representatives unblushingly designate the A.L.P. as the Labour Movement, with themselves as the impeccable high priests. It would indeed be a hopeless tragedy if the A.L.P. claim was a bona fide one, for then the future would be a blank insofar as the welfare of the workers is concerned.In these discursive memoirs, I have omitted to pay well deserved tribute to many friends and comrades who it has been my good fortune to know and associate with. With faith unshaken they have never faltered, never chosen the line of least resistance which often leads to material comfort and advancement. I hope these gallant marchers in the Army of the Night will forgive me this apparent non-recognition of their worth. To those Labourites who are either in the camp of the enemy or in such close contact as to render it impossible to define their line of demarcation, who I have not honourably (?) mentioned – well – they can consider themselves fortunate that they also have not been pilloried and placed in the rogues gallery with their perhaps more prominent colleagues.In my judgment and comments on those who have falsified their erstwhile pledges and ideals I have “set down naught in malice.” They by their own acts of betrayal and treachery have left themselves naked, not to their enemies, but to the scorn and righteous resentment of all who regard the Labour Movement as something far nobler than a profession and means to secure personal aggrandisement and popular applause.The charge that Utopian idealism is outside the realms of practical politics leaves me unperturbed. It has been truly said “That a map of the world that does not include Utopia is not worth even glancing at, for it leaves out the one country at which Humanity is always landing.” “Where there is no vision – there the people perish.” I have been a dreamer – a visionary – seeing in the future that society wherein all the dreams of’ Labour, of the poor oppressed ones of the earth, will come true. “There has never been a great invention that did not begin in a dream, just as there never has been a great truth that did not begin as a heresy. And if we look back over history we find that the sublime moments, with men and nations, are those in which they break free from the anchorage of the past, and set sail towards the unknown seas on a new and spiritual voyage of discovery.” I believe that the day is not far distant when the dreamers of the world will reap their reward. Then will the blind leaders of the blind who to-day slander and disavow Communism as a danger and disruptive force be utterly discredited. Like the misleaders berated by Omar Khayyam, their words to scorn will be scattered, and their mouths stopped with dust.With Heine I have found that “Life is a comedy to those who think – and a tragedy to those who feel.” And it is because I have felt the woes and sufferings of the common people, contemned, mercilessly exploited under the jungle law of capitalist society, that I have rebelled and ever challenged this debased system and its apologists.It was at the repeated urgings of many active participators in the Labour Movement of today that I resolved to write these reminiscences. I have sought the approval of none, probably have disappointed some who anticipated a more detailed and effective revelation of Labour’s ills. But at least I have been honest and as frank as possible. Unlike many autobiographies, every incident – every word in this passing record of my life are actual facts and truths. (The half century covered in this period has gone beyond recall, and still the show goes on).It is now the great privilege and duty of the younger generation to so shape their lives – their activities – to carry on the fight for human emancipation, for the triumphant establishment of the Communist State, which to me and to countless others has ever been the flaming gaol of our life’s pilgrimage.It should be clearly understood that my Communist faith and ideals are not necessarily those of any party or even individuals, but entirely my own point of view. Neither are these memoirs propaganda only insofar as they record my own opinions and reactions of some of the events of a not uneventful life. They can in short be truly designated – a human document– nothing more, but certainly nothing less.E. H. Lane, “Cosme,” Highgate Hill, Brisbane,
September, 1938. 
Table of Contents
