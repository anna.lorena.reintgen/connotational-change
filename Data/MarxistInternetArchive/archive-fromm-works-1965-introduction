Erich Fromm 1965Written:
1965;
Source: The
Autodidact Project;
First Published: Erich
Fromm (ed.), Socialist
Humanism: An International Symposium,
Garden City, NY: Doubleday, 1965, pp. vii-xiii.
Transcribed:
Ralph Dumain.
One of the most remarkable phenomena of the past decade has
been the renascence of Humanism in various ideological systems.
Humanism—in simplest terms, the belief in the unity of the
human race and man’s potential to perfect himself by his own
efforts—has had a long and varied history stretching back to
the Hebrew prophets and the Greek philosophers. Terentius’
statement, “I believe that nothing human is alien to
me,” was an expression of the Humanist spirit, echoed
centuries later by Goethe’s “Man carries within
himself not only his individuality but all of humanity, with all its
potentialities, although he can realize these potentialities in only a
limited way because of the external limitations of his individual
existence.”Over the ages some Humanists have believed in the innate
goodness of man or the existence of God, while others have not. Some
Humanist thinkers—among them Leibniz, Goethe, Kierkegaard,
and Marx—particularly stressed the need to develop
individuality to the greatest possible extent in order to achieve the
highest harmony and universality. But all Humanists have shared a
belief in the possibility of man’s perfectibility, which,
whether they believed in the need for God’s grace or not,
they saw as dependent upon man’s own efforts (which is why
Luther was not a Humanist). Nonreligious Humanists like Gianbattista
Vico and Karl Marx carried this further to say that man makes his own
history and is his own creator.Because Humanists believe in the unity of humanity and have
faith in the future of man, they have never been fanatics. After the
Reformation they saw the limitations of both the Catholic and the
Protestant positions, because they judged not from the narrow angle of
one particular organization or power group, but from the vantage point
of humanity. Humanism has always emerged as a reaction to a threat to
mankind: in the Renaissance, to the threat of religious fanaticism; in
the Enlightenment, to extreme nationalism and the enslavement of man by
the machine and economic interests. The revival of Humanism today is a
new reaction to this latter threat in a more intensified
form—the fear that man may become the slave of things, the
prisoner of circumstances he himself has created—and the
wholly new threat to mankind’s physical existence posed by
nuclear weapons.This reaction is being felt in all camps—Catholic,
Protestant, Marxist, liberal. This does not mean, however, that
contemporary Humanists are willing to forego their specific
philosophical or religious convictions for the sake of
“better understanding,” but rather that, as
Humanists, they believe they can reach the clearest understanding of
different points of view from the most precise expression of each,
always bearing in mind that what matters most is the human reality
behind the concepts.This volume is an attempt to present the ideas of one branch
of contemporary Humanism. Socialist Humanism differs in an important
respect from other branches. Renaissance and Enlightenment Humanism
believed that the task of transforming man into a fully human being
could be achieved exclusively or largely by education. Although
Renaissance Utopians touched upon the need for social changes, the
socialist Humanism of Karl Marx was the first to declare that theory
cannot be separated from practice, knowledge from action, spiritual
aims from the social system. Marx held that free and independent man
could exist only in a social and economic system that, by its
rationality and abundance, brought to an end the epoch of
“prehistory” and opened the epoch of
“human history,” which would make the full
development of the individual the condition for the full development of
society, and vice versa. Hence he devoted the greater part of his life
to the study of capitalist economics and the organization of the
working class in the hopes of instituting a socialist society that
would be the basis for the development of a new Humanism.Marx believed that the working class would lead in the
transformation of society because it was at once the most dehumanized
and alienated class, and potentially the most powerful, since the
functioning of society depended upon it. He did not foresee the
development of capitalism to the point where the working class would
prosper materially and share in the capitalist spirit while all of
society would become alienated to an extreme degree. He never became
aware of that affluent
alienation which can be as dehumanizing as
impoverished alienation.Stressing the need for a change in the economic organization
and for transferring control of the means of production from private
(or corporate) bands into the bands of organized producers, Marx was
misinterpreted both by those who felt threatened by his program, and by
many socialists. The former accused him of caring only for the
physical, not the spiritual, needs of man. The latter believed that his
goal was exclusively material affluence for all, and that Marxism
differed from capitalism only in its methods, which were economically
more efficient and could be initiated by the working class. In
actuality, Marx's ideal was a man productively related to other men and
to nature, who would respond to the world in an alive manner, and who
would be rich not because he had much but because he was much.Marx was seeking an answer to the meaning of life, but could
not accept the traditional religious answer that this can be found only
through belief in the existence of God. In this he belongs to the same
tradition as the Enlightenment thinkers, from Spinoza to Goethe, who
rejected the old theological concepts and were searching for a new
spiritual frame of orientation. But, unlike such socialists as Jean
Jaurés, Lunacharsky, Gorki, and Rosa Luxemburg, who
permitted themselves to deal more explicitly with the question of the
spiritual, Marx shied away from a direct discussion of the problem
because he wanted to avoid any compromise with religious or idealistic
ideologies, which he considered harmful.Authentic Marxism was perhaps the strongest spiritual movement
of a broad, nontheistic nature in nineteenth-century Europe. But after
1914—or even before—most of this spirit
disappeared. Many different factors were involved, but the most
important were the new affluence and ethics of consumption that began
to dominate capitalist societies in the period between the wars and
immediately following the second and the seesawing pattern of
destructiveness and suffering caused by two world wars. Today, the
questions of the meaning of life and man's goal in living have emerged
again as questions of primary importance.One must realize that, by necessity, the spiritual problem has
been camouflaged to a large extent until our present moment in history.
As long as productive forces were not highly developed, the necessity
to work, and to keep alive, gave sufficient meaning to life. This still
holds true for the vast majority of the human race, even those living
in industrially developed countries where the mixture of work and
leisure, and the dream of ever increasing consumption, keeps man from
realizing his true human potential, of being what he could be. But we
are moving rapidly toward a fully industrialized, automated world in
which the ten or twenty hour work week will be standard, and
where the many material satisfactions provided for everyone will be
taken for granted. In this totally affluent society (which will be a
planned if not a socialist one), man’s spiritual problem will
become much more acute and urgent than it has ever been in the past.This volume has a dual purpose. It seeks to clarify the
problems of Humanist socialism in its various theoretical aspects, and
to demonstrate that socialist Humanism is no longer the concern of a
few dispersed intellectuals, but a movement to be found throughout the
world, developing independently in different countries. In this volume
many Humanist socialists from the East and the West meet for the first
time. Reading the volume, contributors as well as readers may become
fully aware for the first time of the common response of many
socialists to what the history of the past decades and the present
threat to the physical and spiritual survival of mankind has taught
them.With five exceptions, all of the contributions were written
specifically for this volume, but in no case did I suggest the topic of
a specific essay to the author. I preferred to ask each of them to
write on any topic that appeared most important to him within the
general frame of reference of socialist Humanism. I hoped that in this
way the volume would represent the main interests of Humanist
socialists. It did not seem to me a disadvantage if some topics were
dealt with several times by different authors. On the contrary, I
thought it would be an interesting and even impressive phenomenon to
see the fundamental agreement among most authors represented in this
volume and the extent to which a new school of thinking has arisen in
various parts of the world, in particular among the scholars of
Yugoslavia and Czechoslovakia, whose writings have so far been little
known in the English speaking world.Despite the authors’ common bond, there are
important disagreements among them and with the editor. The authors
belong to different political parties. Most of them are socialists, but
some are not. Most of them are Marxists, but some—including
Catholics, independent liberals, and non Marxist labor party
members—are not. No one whose contribution is published here
can be held responsible for the views expressed by any other author or
by the editor.As Humanists, all of the contributors have a common concern
with man and the full unfolding of his potentialities, and a critical
attitude toward political reality, especially toward ideologies. This
latter is of the utmost importance. Today, more than ever, we find
concepts like freedom, socialism, humanism, and God used in an
alienated, purely ideological way, regardless of who uses them. What is
real in them is the word, the sound, not a genuine experience of what
the word is supposed to indicate. The contributors are concerned with
the reality
of human existence, and hence are critical of ideology;
they constantly question whether an idea expresses the reality or hides
it.There is one other factor common to all the contributors:
their conviction that the most urgent task for mankind today is the
establishment of peace. No one represented in this volume in any way
supports the cold war.Inevitably there are omissions, which the editor regrets. Most
of the authors are either European or North American, even though Asia,
Africa, and Australia are represented. There is also a rather one sided
emphasis on the philosophical aspect of socialist Humanism as compared
to the practical and empirical problems of Humanist socialist
organization, which are dealt with only in the last chapter, On the
Practice of Socialist Humanism. Indeed, a great number of
important
problems of socialist organization are not only not represented here,
but have been little discussed in socialist literature in general.
(Such problems are, for instance, the distinction between real human
needs and artificially produced needs, the possibility of a revival of
handicrafts as a luxury industry, new forms of democratic participation
based on small face to face groups, etc.)To sum up: it is perhaps no exaggeration to say that never in
the past hundred years have there been such widespread and intensive
studies of the problem of Humanist socialism as today. To demonstrate
this phenomenon and show some of the results of these studies are the
purposes of this volume. In this concern for man and opposition to
dehumanization we feel a deep sense of solidarity with all Humanists,
many of whom do not share all of our views, but all of whom share our
concern for the full development of man.I wish to thank all those who have helped me in my editorial
task. I have often turned to Thomas B. Bottomore of the London School
of Economics and Gajo Petrović of the University of Zagreb for advice,
and they have always been most generous in their response. I am
grateful to the contributors for responding so cooperatively to my
suggestions about space and organization, and to the translators for
taking on the difficult job of putting complicated manuscripts in
French, German, Italian, Polish, and Serbo Croat into English. Finally
my sincere thanks to Anne Freedgood of Doubleday for her ever present
interest in this book and for her extraordinary effort in preparing the
manuscript.Erich Fromm

 Erich
Fromm Archive