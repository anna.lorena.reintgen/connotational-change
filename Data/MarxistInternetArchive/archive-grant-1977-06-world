
Written: June 1977      
Source: The Unbroken Thread 
Transcription/Markup: Emil 1998 
Proofread: Emil 1998 The last 30 years marked a period where the
imperialist industrialised states built up industry on a scale,
and at a pace probably greater than any time since the inception
of capitalism.They retreated from direct military
domination of the colonies, because of the revolt of the colonial
peoples; but in the former colonial areas where capitalism and
landlordism have been maintained they exercise an even greater
domination, economically. Thus collectively the
ex-colonial powers of the EEC, the USA and Japan exact
super-tribute from the exploitation of the colonial peoples. They
do this through the disparities in the terms of trade, where the
price of industrial goods, consumer and capital goods have
increased in price far more than the food and raw materials
mainly exported by the under-developed countries. This is so even
in the case of oil, where the price has gone up more than four
times. This increase in price followed the oil embargo of 1973
due to the Arab-Israeli war. Despite the screams of anguish by
the imperialists, and further increases in oil prices since that
period, these increases do not completely compensate for the
increase in price of capital and manufactured goods since the
war.The disparity is even more marked for the
non-oil producing under-developed countries, because the oil
costs, plus more, are added to the cost of capital and consumer
manufactured goods.Thus the economic basis was laid in the
colonial world for a period of upheaval; of revolutions,
counter-revolutions and coups unexampled in history. It must be
the most disturbed period for the colonial world in the history
of capitalism in all its main areas: Asia, Africa and Latin
America.This will seem peaceful in comparison with the
titanic conflicts and upheavals of the next two decades.In the countries where capitalism has been
overthrown, especially China and Russia, the last 30 years have
seen a temporary consolidation of the power of the Stalinist
bureaucracies in a totalitarian proletarian Bonapartist system.
In all the three main economic and social areas of the world, a
new epoch is opening up. From the point of view of Marxist theory
this development of the economy was a preparation for the coming
period of social convulsions. This is so in the industrialised
capitalist world, the ex-colonial world and the Stalinist states.
The world economic upswing of capitalism, with its apparently
endless industrial growth, with only incidental interruptions of
production in the form of minor recessions in one country or
another, is now at an end. This period has been explained in a
series of documents only by the Marxist tendency, and there is no
need to repeat that analysis here.But as argued in the material, all the
factors making for an unprecedented boom were the same factors
preparing the way for economic and social catastrophes and
upheavals. The accumulation of fictitious capital resulted in
a world-wide explosion of inflation. The inter-penetration of
trade intensified the world division of labour. The gap increased
between the under-developed and the developed world.The further disparity between the relatively
'weak' countries of Italy, Britain and France and the relatively
'strong' countries of West Germany, Japan and the United States,
increased the instability of the workings of the world-wide
capitalist system.The leap forward in the increase of world trade
by a rate of 12.5 per cent per annum in its turn led to a partial
dismantling of tariffs and other impediments to a free exchange
of goods and this gave impetus to world trade and to the world
division of labour. This has now slowed down. World trade in 1976
and in 1977 (projected according to the calculations of the OECD
(1), UN,
and the capitalist powers) will increase only at the rate of 5
per cent per annum. That is an enormous drop. Together with the
other factors, it marks the end of the epoch of economic upswing.
Now a new period of booms and slumps is ushered in.But because of the growth of productive forces,
together with the growth of the social and economic
contradictions in the intervening period, the rhythm of the booms
and slumps will no longer be that of the classical period of
capitalism of a decade or so ago. They will be of a much shorter
duration, between two and six years. This is already shown in the
first simultaneous slump (1974-5) since the second world
war. This has resulted in the re-appearance of a permanent
army of unemployed in the capitalist countries, which will
further aggravate the hopelessness of the position of the tens
and hundreds of millions of unemployed and under-employed in the
under-developed countries.The recession or small slump of 1974-5 has been
succeeded by the boomlet (in most capitalist countries) of
1975-6. But in all of these countries the 'boom' has not been
anything in the nature of the upswing of the post-war period.
Supplementing the unemployment of labour has been the
underemployment of resources. Only 80 per cent of industrial
capacity has been used in most of the industrialised capitalist
countries. Inflation, even in the economically 'strong' countries
still remains an ever-present threat. In Britain and Italy it is
between 20 per cent and 30 per cent, in France 15 per cent, Japan
and the United States 8 per cent to 10 per cent and even West
Germany 4 per cent to 6 per cent (at the time of writing in June
1977).Investment under these circumstances has been
at a far lower rate than in previous booms. The 'entrepreneurs',
ie the monopolies, are not interested in an extensive increase in
capacity when they cannot see a future market, and when they
cannot make use of already existing capacity. Consequently the
rate of increase of production too is smaller than after previous
recessions or small slumps.With the complete discrediting of the theories
of the 'witch doctor' Keynes, there is a reluctance to increase
state expenditure by 'priming the pump', for fear of inflation
getting out of hand. This explains the 'deflationary' measures of
Britain, France and Italy and also why, despite pleas from the
'weaker powers', the governments of West Germany, Japan and the
USA have refused to entertain the idea of 'reflating'. They
prefer to trample their capitalist rivals and weaker brethren in
the bog, lest they too be sucked into the mire. Thus Fukuda, the
Prime Minister of Japan, observed brutally: 'What would be the
point of making the strong weak, by reducing them to the level of
the weaker powers?'Thus the summit of the seven powers in May
solved not a single one of the burning problems facing the
capitalist world. It brought no cheer to the weaker powers, and
nothing to the under-developed regions of the world. Nor did it
strengthen the 'stronger' powers. They were all looking to their
own resources and flailing round like drowning men who could just
keep afloat. They pushed under their stricken companions
afflicted with cramp, in their frantic efforts to get a lift back
to safety.The large economically dominant powers, West
Germany and Japan, with a substantial surplus in their balance of
payments, and the United States with its crushingly dominant
continental economy, were not prepared to increase state
expenditure and thus budget deficits in order to lessen the
balance of payments deficits of the weaker economies of Italy,
Britain and France. This would mean sucking in imports and thus
lessening their surpluses, but would give a twist to the spiral
of inflation nationally and internationally.At the same time the 'strong' powers were
demanding cuts in state expenditure and deflation, in the weaker
states. This with the reluctant agreement of their weaker rivals.
The latter were fearful of uncontrolled inflation and thus were
slashing the reforms of the past, described as the 'social wage'.
State expenditure was formerly regarded by the bourgeoisie as a
painless way of increasing the market. Now they wish to curb
inflation by cutting down state expenditure and at the same time
hold down the wages of the workers in order to increase the mass
and rate of profit of the capitalist class.In all capitalist countries the tendency of the
rate of profit to fall has manifested itself in a steep decline.
Hence the pre-occupation of the ruling classes to increase the
rate of profit. In West Germany between the 1960s and 1970s the
rate decreased sharply. That decline is continuing according to The
Economist (26 February, 1977): 'In the early 1960s, German
industry earned on average a net return after tax of just over 6
per cent on sales. In 1967-71 the average return came down to 5.3
per cent and in 1972-5 it was only 4.1 per centIn 1970,
German employers had to pay about DM 42 billion for social
security contributions. By 1975, this total has almost doubled.'The decline in the markets, simultaneously with
a decline in the rate of profit led in West Germany, as in the
other main capitalist countries, to a decline in manufacturing
investment. In 1970 gross fixed investment in industry was DM
35.5 billion. This was an increase of 17 per cent on the previous
year. Investment as a percentage of sales was 6.9 per cent, and
investment per employee was DM 4,280. In 1976, which was recovery
year after the recession 1974-5, gross fixed investment had
dropped to DM 26.5 billion, only slightly higher than the
recession year of 1975. Investment in 1976 was 1 per cent higher
than in 1975. Investment as a percentage of sales actually
dropped slightly to 4.6 per cent. Investment per employee was DM
3,685.Similar figures could be quoted for all
industrialised capitalist countries. The contradictions pile up.
Yesterday the capitalists were intent on expanding the market
through measures such as reducing tariff barriers and state
restrictions on a freer flow of foods and trade between the
capitalist countries (through the General Agreement Tariffs and
Trade). Internally they used state expenditure to boost the home
market. Now they are faced with the major problem of limited
markets both at home and abroad, while saddled with surplus
capacity. Industrial investment, which for a period creates its
own market in an economic spiral, has fallen. The whole spur to
increased production has sagged. The production of machinery and
buildings creates a market for materials, steel, building
materials and so on. Extra workers employed mean increased
purchasing power, which means more sales for consumer goods, a
further possible market for new machinery. The extra workers in
steel and building (or increased earnings etc), create a further
market and so on.The measures of the British Labour government,
and those of the French and Italian governments, have restricted
the market and consequently, despite lavish inducements,
investment has not reached the real level of 1970 in Britain,
Italy and France. It is lagging far behind previous figures under
world boom conditions. The economic miracles of Japan and West
Germany have ended with investment little above the recession
figures of 1974-5. The USA is in an even worse predicament,
considering its enormous capacity.The slump of 1974/5 was the first serious
world check to the development of productive forces since the
early post-war period, although the fall in production was
not high in comparison with the inter-war period. Painfully the
bourgeoisie has recovered economically in the boom of 1976-7. But
the boom has nothing of the character of the world rhythm of
production in the post-war period. One of the economic
witch-doctors of capitalism writing in The Economist of 30
April 1977 says:"The best guess is that the world is not
set for slump. If the trade cycle has changed from seesaw to
sluggardliness, by the same token (?) output is unlikely to
plunge into the same kind of sharp downturn that followed real
booms. (!?)"More likely is a steady climb off the
plateau in the second half of 1977. At an annual rate (for OECD
countries) of perhaps 3-4 per cent, sustained into 1978."In other words the boom has been sluggish. The
bourgeoisie has foresworn another dose of economic opium,
increasing state expenditure, because it means inflation under
both boom and slump conditions. They have painfully learned that
to increase state expenditure might give them an economic 'high'
for a year or two (or even possibly only months), only to bring
more inflation on Latin American lines, more unemployment and all
the nightmare reality of 'stagflation', stagnation and inflation,
or even worse 'slumpflation'.But cutting state expenditure and cutting real
wages would lead to the distressing 'withdrawal symptoms' of
deflation, once the opium is withdrawn from the sick economy of
world capitalism. So The Economist hopefully argues that
by missing the boom, they can also miss the inevitable aftermath
of slump, and permanently maintain a slow plateau of growth. The
superstitious representatives and idealogues of the bourgeois
have these utopian fancies. If the production does not increase
then the market will continue to stagnate. Investment will fall
even if the measures to hold down real wages succeed, and because
they succeed!Marx divided production into department 1,
production of means of production (capital goods, machinery,
buildings etc), and department 2, production of
means of consumption (consumer goods). The two sections are
inter-dependent. A fall in one means ultimately a fall in the
other.The whole system of capitalism, where every
factor interacts on every other factor, requires rising
production, investment and an increased market in a spiral. In
the end through credits this would exceed the limits of the
capitalist system and lead to bust. But without a real boom the
limits would be reached even sooner! Greater overproduction of
capital and consumer goods would be the consequence and hence
slump would take place even sooner.Serious commentators of capitalism expect a
world slump, beginning (as could be expected) in the United
States, towards the end of this year or during the course of next
year, 1978. All the economic factors point towards this with all
the political and social consequences. However, at the first
signs of a break in the economy, with more bankruptcies and
rising unemployment, in a panic the capitalist governments would
again reach for the dope syringe as a 'solution'. President
Carter and Denis Healey have both hinted at reflation towards the
end of the year if necessary to restore production. In other
words the very measures that have meant a crisis of inflation
will once again be resorted to by the capitalists. This over a
period would exacerbate the problems of inflation, which by
raising prices causes dissatisfaction among both workers and the
middle class, by cutting the purchasing power of the masses. In
the end it has the same consequences. In addition faith in the
currency is undermined. Inflation delays slump for a relatively
short period only to make it worse when production collapses.Either way there would be resistance, even if
delayed, by the working class at still further cuts in real
standards of living. Even if the slump is delayed for a short
period, that would open up a period of bitter battles between the
classes. Even more bitter will be the reaction to empty factories
side by side with enforced idleness of the workers.The new epoch which began in 1974 of short
booms followed by slumps is an epoch of storm and stress. The
relative development of the productive forces by the
bourgeoisie for a period of a quarter of a century or so, also
gave relative stability to the main capitalist countries in
the period since the post-war years. Marxism has always explained
that a period of social revolution opens up when the ruling class
and their economic system become a drag and a fetter on the
development of the productive powers of society. This also
applies to the development of society under a ruling caste, as in
the Soviet Union, which hampers the development of productive
forces. Then opens up a period of political revolution. Trotsky
had assumed in 1938 that this was exactly the period which the
world bourgeoisie and also the Stalinist bureaucracy had entered.Owing to the peculiar development of events,
and the treacherous role of Stalinism and reformism, the
bourgeoisie gained a breathing space, and for reasons explained
in other material the development of productive forces
(relatively and to a certain extent absolutely) gained an
impetus. Now, generally, the productive forces cannot be
contained within the confines of the national state and private
ownership.Hence during the next decade on a world scale a
new period of upheavals and titanic class clashes is on the order
of the day. A period of social and political convulsions in all
three spheres - the capitalist West, the former colonial world,
and the Stalinist bloc - is inevitable. It was for such a period
in 1917-21 that Lenin and Trotsky tried to prepare the forces of
the Communist International. Trotsky prepared for a similar
period which would follow the outcome of World War II. He
predicted the inevitability of this cataclysm in 1938.These political upsurges did take place, but
for lack of Marxist leadership and Marxist mass organisations,
the bourgeoisie succeeded every time in avoiding complete
overthrow. Capitalism was restabilised albeit on a more shaky
basis. Stalinism, as a governmental system in Russia with
extensions in the Communist parties of other countries, also
recovered from its crises and even expanded its power and
influence.But the old dialectics of world development
have been burrowing beneath the apparent stability of Stalinism
and capitalism. Five to six decades of economic progress in the
industrialised world, in the Stalinist states, and even to a
limited extent in the ex-colonial world, have ploughed the field
for the revolutionary crop which will follow. Marxism sees in the
development of productive forces the key to the development of
society and of history. While productive forces are being
developed it gives a relative stability to any class society. In
the last twenty-five years there has been phenomenal development
of productive forces, especially the most important productive
force, the working class.The new organic crisis which is maturing marks
a new stage in the development of post-war capitalism. It
opens up a new period of struggle between the classes over the
next five, ten or twenty years which poses the problem of the
entire fate of mankind. In its train, the upswing brought an
immense accumulation of fictitious capital in the shape of
so-called Euro-dollars. More than $150 billion of worthless
currency were foisted on Europe by the United States during the
period of her overwhelming economic supremacy in the early
post-war period. Supplying the necessary 'liquidity' in the
period of upswing, the paper money served to fructify industry,
but it was also one of the factors leading to world inflation.In a period of economic storms it acts like
unbattened-down lumber on a ship, which causes holes in the hull
and sides as it moves from one side to another with the waves. So
the Euro-dollars are moved from one country to another in order
for the monopolies to profit from the successes or the
difficulties of the economies of the capitalist industrialised
nations. So also the enormous waste of resources in unproductive
arms expenditure, which also acts as fictitious capital, while
imposing crippling burdens on the peoples of the world. This has
reached the fantastic level now of ï¿½1,000,000,000,000 ie a
million million pounds every six and a half years.There is the stark contrast between poverty,
hunger and starvation in the colonial world, and the profligate
waste of resources in research and production of ever more
devilish means for the extermination of mankind. This ominous
threat suspended over the head of mankind makes ever-more
imperative the need for the masses to take the fate of society
and the world into their own hands.The twilight of capitalism, through its
incapacity to serve the needs of the masses, not only in the
colonial world but now also the developed world, makes this a
period of inevitable disillusionment with capitalism and of class
awakening on the part of the proletariat.(1) The Organisation for
Economic Cooperation and Development (OECD), founded in 1961,
incorporated the main capitalist powers aiming to expand trade
and coordinate aid. 
Ted Grant Archive
