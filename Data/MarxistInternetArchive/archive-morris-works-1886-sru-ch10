
      During the French Revolution, especially during its
      earlier stages there was a corresponding movement in England. It made some noise at
      the time, but was merely an intellectual matter, led by a few aristocrats --
      eg., the Earl of Stanhope -- and had no sympathy with the life of the
      people; it was rather a piece of aristocratic Bohemianism, a tendency to which has
      been seen in various times, even our own. For the rest, there certainly was in
      England a feeling, outside this unreal republicanism -- a feeling of which Priestly
      the Unitarian may be looked on as a representative; this feeling was of the nature
      of that felt by respectable and thoughtful Radicals of later days, and was
      distinctly bourgeois, as the other was aristocratic.
    
      The French Revolution naturally brought about a great reaction, not only in
      absolutist countries, but also in England, the country of Constitutionalism; and
      this reaction was much furthered and confirmed by the fall of Napoleon and the
      restoration of the Bourbons in France. We may take as representative names of this
      reaction the Austrian Prince Mettternich (sic) on the Continent and Lord
      Castlereagh in England. The stupid and ferocious repression of the governments
      acting under this influence, as well as the limitless corruption by which they were
      supported, were met in England by a corresponding progressive agitation, which was
      the beginning of Radicalism. Burdett and Cartwright are representatives of the
      earlier days of this agitation, and later on Hunt, Carlile, Lovett, and others.
      William Cobbett must also be mentioned as belonging to this period -- a man of
      great literary capacity of a kind, and with flashes of insight as to social matters
      far before his time, but clouded by violent irrational prejudices and prodigious
      egotism; withal a peasant rather than a bourgeois -- a powerful disruptive agent,
      but incapable of association with others. This period of Radical agitation was
      marked by a piece of violent repression in the shape of the so-called Peterloo Massacre, where an unarmed crowd at a strictly
      political meeting was charged and cut down by the yeomanry, and eleven people
      killed outright(1).
    
      This agitation, which was partly middle-class and partly popular, was succeeded by
      the Chartist movement, which was almost exclusively supported by the people, though
      some of the leaders -- as Feargus O'Connor and Ernest Jones -- belonged to the
      middle-class. Chartism, on the face of it, was as much a political movement as the
      earlier Radical one; its programme was wholly directed towards parliamentary
      reform; but as we have said, it was a popular movement, and its first motive power
      was the special temporary suffering of the people, due, as we said in our last
      chapter, to the disturbance of labour caused by the growth of the machine industry;
      and the electoral and parliamentary reforms of its programme were put forward
      because it was supposed that if they were carried they would affect the material
      condition of the people directly: at the same time, however, there is no doubt that
      the pressure of hunger and misery gave rise to other hopes besides the
      above-mentioned delusion as to reform, and ideas of Socialism were current among
      the Chartists though they were not openly put forward on their programme.
      Accordingly the class-instinct of the bourgeoisie saw the social danger that lurked
      under the apparently political claims of the charter, and so far from its receiving
      any of the middle-class sympathy which had been accorded to the Radical agitation,
      Chartism was looked upon as the enemy, and the bourgeois progressive movement was
      sedulously held aloof from it. It is worthy of note that Chartism was mainly a
      growth of the midland and northern counties -- that is, of the great manufacturing
      districts -- and that it never really flourished in London. In Birmingham the
      movement had the greatest force, and serious riots took place there while a
      Chartist conference was sitting in the town. The movement gave birth to a good deal
      of popular literature; and it must be remembered that the press was very strictly
      controlled by the Government. No paper was allowed to be issued without a stamp,
      the expense of which prevented the issue of cheap papers; and one of the incidents
      of the struggle was the determined opposition to this law kept up by some
      courageous agitators, who published unstamped papers in the teeth of the certain
      imprisonment that awaited them.
    
      The Chartist movement went on vigorously enough till the insufficiency both of its
      aims and of knowledge as to how to carry them out found out the weak places in it.
      The immediate external cause of its wreck was the unfortunate schism that arose
      between the supporters of moral force and physical force in the body itself. The
      fantastic folly of supposing that there can be any 'moral force' in matters
      political which does not rest on the resolution of a party to attain their end by
      the use of what 'physical force' they may haye, if it should become necessary to
      use it, does not call for much comment here; although some thoughtless persons may
      even at present think that they believe such a 'moral force' exists. On
      the other hand, it is clear to us now that a Chartist revolt had no chance of
      success at that time, and but for self-deception would have been clear to both
      leaders and rank and file of the party then. It may here be mentioned that the
      trump-card which the Chartists were always thinking of playing was the organization
      of an universal strike, under the picturesque title of the Holy Month. In
      considering the enormous difficulties, or rather impossibilities, of this
      enterprise, we should remember that its supporters understood that the beginnings
      of it would be at once repressed forcibly, and that it would lead directly to civil
      war.
    
      The truth is that there were two distinct groups in the party, one of which went
      about as far as our ultra-Radicals of the present day; and another which was at
      heart Socialist, only deficient in knowledge, and consequently without definite
      principles on which to base action; and these two groups pretty much corresponded
      to the division between the supporters of moral and physical force.
    
      From 1842, when the schism came to a head, Chartism began to die out. Its decay,
      however, was far more due to the change that was coming over the economical state
      of affairs than even to its incomplete development of principle and ill-considered
      tactics. Things were settling down from the dislocation caused by the rise of the
      great industries. The workers shared in the added wealth brought about by enormous
      expansion of trade, although in an absurdly small proportion to the share of the
      middle-classes; but those classes tended ever to become more numerous and more
      contented. The trades' unions began to be powerful, and improved the prospects of
      the skilled workmen. So-called co-operation began to flourish: it was really an
      improved form of joint-stockery, which could be engaged in by the workmen, but was
      and is fondly thought by some to be if not a shoeing-horn to Socialism at least a
      substitute for it; indeed Chartism itself at this time became involved in a kind of
      half co-operative half peasant-proprietorship land scheme, which of course proved
      utterly abortive.
    
      As this improvement in the condition of the working-classes weakened that part of
      the life of Chartism which depended on mere hunger desperation, so the growing
      political power in the middle-classes and the weakening of the mere Tory reaction
      swallowed up the political part of its life.
    
      Chartism, therefore, flickered out in the years that followed 1842, but its last
      act was the celebrated abortive threat at revolt which took place in April 1848.
      And it must be said that there was something appropriate in such a last act. For
      this demonstration was distinctly caused by sympathy with the attacks on absolutism
      then taking place on the Continent, and Chartism was always on one side of it a
      part of the movement which was going on all over Europe, and was directed against
      the reaction which followed on the French Revolution, and which was represented by
      the 'Holy Alliance' of the absolutist sovereigns against both bourgeoisie and the
      people.
    
      On the fall of Chartism, the Liberal Party, a nondescript and flaccid creation of
      bourgeois supremacy, a party without principles or definition, but a thoroughly
      adequate expression of English middle-class hypocrisy, cowardice, and
      short-sightedness, engrossed the whole of the political progressive movement in
      England, and dragged the working-classes along with it, blind as they were to their
      own interests and the solidarity of labour. This party has shown little or no
      sympathy for the progressive movement on the Continent, unless when they deemed it
      connected with their anti-Catholic prejudice. It saw no danger in the
      Cæsarism which took the place of the corrupt sham Constitutionalism of Louis
      Philippe as the head of the police and stock-jobbing régime, which dominated
      France in the interests of the bourgeoisie, and hailed Louis Napoleon with delight
      as the champion of law and order.
    
      Any one, even a thoughtful person, might have been excused for thinking in the
      years that followed on 1848 that the party of the people was at last extinguished
      in England, and that the class-struggle had died out and given place to the
      peaceable rule of the middle-classes, scarcely disturbed by occasional bickerings
      carried on in a lawful manner between the two parties to that false free-contract,
      which is the lying foundation on which Commercial Society rests. But, as we shall
      show in a future chapter, under all this, Socialism was making great strides and
      developing a new and scientific phase, which at last resulted in the establishment
      of the International Association, whose aim was to unite the workers of the world
      in an organization which should consciously oppose itself to the domination of
      middle-class capitalism. The International was inaugurated in England in 1864, at a
      meeting held at St Martin's Hall, London, and at which Professor Beesly took the
      chair. It made considerable progress among the Trades' Unions, and made a great
      impression (beyond indeed what its genuine strength warranted) on the arbitrary
      Governments of Europe. It culminated in the Socialistic influence it had, in the
      Commune of Paris, of which we shall treat in a separate chapter. The International
      did not long out-live the Commune, and once more for several years all proletarian
      influence was dormant in England, except for what activity was possible among the
      foreign refugees living there, with whom some few of the English working-men had
      relations. From this connection sprang, however, a new movement, which we must
      barely mention, though it cannot yet be considered a matter of history. In 1881, an
      attempt was made to federate the various Radical Clubs into a body, with a
      programme which, though for the most part merely Radical, had an infusion of
      Socialism in it, and which took the name of the Democratic Federation. The Radical
      Clubs, however, that had joined soon seceded, mostly from disagreement with the
      revolu- tionary attitude taken by the Federation on the Irish question. In 1883,
      the programme became more definitely Socialistic, and the next year the title was
      changed to that of the Social Democratic Federation; but in the last days of 1884
      differences of opinion which had been developing for some time, chiefly centering
      on the questions of Parliamentary Opportunism and Nationalism, ended in a secession
      which founded the Socialist League as a definite Revolutionary Socialist body early
      in 1885.
    
      At the present time the Socialist bodies, though relatively small, tend to attract
      various elements to them; the discontent of the workmen with an outlook of ever
      increasing gloom; that also of the Ultra-Radicals unable to make any real
      impression on the dense mass of mingled Conservatism and Whiggery, which really
      governs the country. The aspirations of thoughtful people who have studied the
      works of the great Socialist thinkers; the permeation of Socialist feeling from its
      centres on the Continent; and lastly and chiefly the steady march of events towards
      a new state of Society, which is making itself felt even amongst those who are
      unconscious of the advance of Socialism, or hostile to it -- all these causes
      combining together, are forcing even England, the stronghold of middle-class
      domination, to pay attention to the subject, and will certainly before long form a
      new and powerful Party of the People, whose outlook will be far more hopeful than
      that of any of those we have told of; since its aim will no longer be partial or
      one-sided, but will be the realization of a new Society with new politics, ethics,
      and economics, in short, the transformation of civilization into Socialism.
    
1 The readers of Commonweal will find an
      article on this subject in the first number (Feb. 1885), by our comrade E. T.
      Craig, who was in Manchester at the time, though not an eye-witness. It is
      interesting to note that the scene of the massacre, St Peter's Fields, is now a
      mass of streets in the very centre of the city of Manchester.back

Commonweal, Volume 2, Number 33, 28 August 1886, PP. 170-171
    
Contents |
      The William Morris Internet Archive : Works
