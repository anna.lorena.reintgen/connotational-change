
August Thalheimer (1884-1948) was a member of the
      German Social Democratic Party before the First World War, and editor of
      one of its papers, the Volksfreund. From 1916 he assisted in the
      production of the Spartakusbriefe, was a member of the USPD
      (Independent Socialists) from 1917, and a founder member of the German
      Communist Party (KPD). He quickly rose to prominence as the partys
      main theoretician, being editor of Rote Fahne as well as of Franz
      Mehrings manuscripts left unpublished at his death.During the 1923 crisis he was Minister of Finance in
      the Württemburg local government, was subsequently blamed along with
      Brandler for the debâcle, and was called to Moscow in 1924, where he
      worked in the Communist International apparat, as well as for the
      Marx-Engels Institute. His lectures delivered at the Sun Yat-Sen
      University in 1927 were published as a textbook in philosophy (which
      appeared in English as Introduction to Dialectical Materialism,
      New York, 1936), and he also worked on the draft programme of the
      Comintern along with Bukharin. Pressure from the KPD, still uneasy with
      the leadership of Thälmann, secured his return to Germany in 1928,
      but a year later he was expelled from the KPD along with Brandler, and
      they went on to form the KPO, or Brandlerites.The Brandlerite organisation restricted most of its
      criticisms to the foreign policy of the Soviet Union, whilst maintaining
      that its domestic operations were basically healthy. Thalheimer insisted
      that: We do not want to draw the conclusion that as the politics of
      the Comintern are wrong, it must follow that the politics of Russia are
      also wrong. (GdST, 4/1931) Thalheimer himself supported
      forced collectivisation and Stakhanovism, and whilst in Barcelona became
      involved in a heated argument with Nin over the POUMs condemnation
      of the first Moscow Trial.In exile in Paris from 1932 onwards, Thalheimer went
      to Spain in 1936, and then back to France again where the KPOs exile
      organisation worked. When six members of the KPO were arrested in
      Barcelona by the Stalinists and charged with the usual crimes in July
      1937, he issued a statement co-signed by Brandler saying that:We take upon ourselves any political and
      personal guarantee for our arrested comrades. They are anti-Fascists and
      revolutionaries, incapable of any action that could be construed as high
      treason to the Spanish Revolution.They were not to stay long in Paris. In 1940 France
      fell to Hitler, and Thalheimer fled to Cuba, where he died in 1948.Thalheimer Archive 