Version Two, Communism and its Tactics by Sylvia Pankhurst
Under Communism all shall satisfy their material needs without stint or measure,
from the common storehouse, according to their desires. Everyone will be able to have what he or
she desires in food, in clothing, books, music, education and travel facilities. The abundant
production now possible, and which invention will constantly facilitate, will remove any need for
rationing or limiting of consumption. 
Every individual, relying on the great common production, will be secure from
material want and anxiety. 
There will be no class distinctions. These arise from differences in material
possessions, education and social status. All such differences will be swept away. 
There will be neither rich nor poor. Money will no longer exist, and none will
desire to hoard commodities not in use, since a fresh supply may be obtained at will. There will be
no selling, because there will be no buyers, since everyone will be able to obtain everything
desired without payment. 
The possession of private property, beyond that which is in actual personal use,
will disappear. 
There will be neither masters nor servants. Because all will be economically equal
— no individual will be able to become the employer of another. 
Children will be educated up to adult age, and adults will be able to make free,
unstinted use of all educational facilities in their abundant leisure. 
Stealing, forgery, burglary, and all economic crimes will disappear, with the vast
and bjectionable apparatus which at present exists for preventing, detecting, and punishing crime.
Prostitution will become extinct; it is a commercial transaction, dependent upon
the economic need of the prostitute and the customer’s power to pay. 
Sexual union will no longer be based upon material conditions, but will be freely
contracted on the basis of affection and mutual attraction. The marriage laws, having become
obselete, will disappear. If people have ceased to be happy together they will part in freedom and
without incurring the stigma of social disapproval. 
The birth of children will cease to be prevented by reason of poverty. 
Material anxiety being removed, and the race for wealth eliminated, other objects
and ambitions will take the place of the individual struggle for existence and material wealth.
Since all will benefit from the labour of all, praise will be given, not to the wealthy, as at
present, but to those who prove skilful and zealous in the common service. 
Emulation in work will take the place of emulation in wealth. 
With the disappearance of the anxious struggle for existence, which saps the
energy and cripples initiative, a new vigour, a new independence will develop. People will have
more courage to desire freedom, greater determination to possess it. They will be more exacting as
to their choice of a vocation. They will wish to work at what they enjoy, to order their lives as
they desire. Work will be generally enjoyed as never before in the history of mankind. 
The desire for freedom will be tempered by the sense of responsibility towards the
commonweal, which will provide security for all. 
Public opinion provides a stronger, more general compulsion than any penal code,
and public opinion will strongly disapprove idleness and waste. 
To secure the abundant production necessary to Communism, and to cope with the
ever-growing complexity of modern life and requirements, large-scale production and co-operative
effort is necessary. The people of today would not be willing to go back to producing everything by
hand in domestic workshops; were they to do so, they could not maintain the population in comfort
and with reasonable leisure. The people of today would be unwilling to abandon all the productive
factories, the trains, the electric generating stations and so on. The retention of such things
necessitates the working-together of large numbers of people. As soon as numbers of people are
working together and supplying with their products numbers of other people, some sort of
organisation of work and of distribution becomes inevitable. The work itself cannot be carried on
without organisation. In each industry, either the workers concerned in the work must form and
control the organisation, or they will be under the dominion of the organisers. The various
industries are interlocked in interest and utility; therefore the industrial organisations must be
interlocked. 
When wages have disappeared, when all are upon a basis of economic equality, when
to be manager, director, organiser, brings no material advantage, the desire to occupy such
positions will be less widespread and less keen, and the danger of oppressive action by the
management will be largely nullified. Nevertheless, management imposed on unwilling subordinates
will not be tolerated; where the organiser has chosen the assistants, the assistants will be free
to leave; where the assistants choose the organiser, they will be free to change him. Co-operation
for the common good is necessary; but freedom, not domination, is the goal. 
Since co-operative work and mutual reliance on mutual aid renders some kind of
organisation necessary, the best possible form of organisation must be chosen: the test of its
worth is its efficiency and the scope for freedom and initiative it allows to each of its units.
The Soviet structure of committees and delegates, built up from the base of the
workshop and village assembly, presents the best form of organisation yet evolved; it arises
naturally when the workers are thrown upon their own resources in the matter of government. The
Soviet structure will undoubtedly be the organisational structure of Communism, at any rate, for
some time to come. We live always, however, in a state of flux, and there is, and happily can be,
no permanence about human institutions; there is always the possibility of something higher, as yet
undiscovered. 
The overthrow of Capitalism precedent to the establishment of Communism will be
resisted by the possessors of wealth. Thus Capitalism will only be overthrown by revolution. 
The revolution can only come when conditions are ripe for it; but opportunities
may be missed: the rising may fail to take place at the opportune moment, or it may fail by
mismanagement of the proletarian forces. A partial success may be achieved, and if Capitalism is
not completely destroyed it may afterwards re-establish itself, as it speedily did in Hungary, as
it is gradually doing in Russia.  
Contents |
Sylvia Pankurst Archive