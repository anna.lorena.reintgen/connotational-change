Alexandra Kollontai's Red LoveAt last Vasya had carried her point at the hemp-binding works. She had
succeeded in obtaining concessions from the management. The shop girls were
jubilant, escorted Vasya to her door. But she knew that matters would never
have turned out so well without the Chairman. She had come to esteem him. He
was inflexible, and anything but indulgent toward the economists.  When
she reached her house Vasya found the entire courtyard full of shipping
clerks. A babel of voices, disputing, shouting. “The highest rates! No
concessions! Or we’ll stop work! Let the managers and office clerks do the
loading!”  Vasya mingled with the crowd, listening, asking questions. 
They recognized her, surrounded her, drowned her voice. Everybody wanted to tell her all about it at once. Their pay was too low, and they got nothing for overtime. The accounts were not drawn up correctly. They crowded about Vasya, uttered threats against the management. Wasn’t she the manager’s wife? Let her explain the whole business to him. There could be no family considerations in a case of this sort.

Vasya listened and asked questions. She knew and understood their grievances well. The managers and office employees were well treated and well fed, but the shipping clerks were slaves. Their children had nothing to wear. Things couldn’t be permitted to go on that way; the union would have to exert pressure on the management. Nothing could be done without organization and a program. The leaders came forward to arrive at an understanding with Vasya. They would state their demands on paper. And if the management were to refuse all concessions, they would appeal directly to the accounting department.

Vasya’s blood was up. Forgetting her position as the manager’s wife she took the shipping clerks’ cause as her own. How could she help supporting “her own people” with word and deed? They were an inexperienced mob, inefficiently led.

She asked the leaders into the house, there to formulate their demands.

They went in, the shipping clerks looking askance at the manager’s house furnishings as they went through the reception rooms into Vasya’s bedroom. Only then it occurred to Vasya that she should not have brought the men into the house. But it was too late to turn back.

They sat down at Vasya’s table and drew up their demands.

There was less noise in the courtyard, no more shouting. The men had separated into groups, were talking and smoking.

Then suddenly the uproar began again. An auto had stopped before the house. The manager. He was entering the courtyard.

“What sort of business is this? You’re holding a meeting here? You’ve come here to make threats? You’re dissatisfied?” Vladimir’s voice rumbled like thunder. “I haven’t the slightest intention of negotiating with you here! This is my private residence. Go to the office. You don’t like the accounting? Go to the union! The management has nothing to do with that. It has other things to worry about. You’re going to strike? That’s your affair. Go ahead and strike, if the union says so. But get out of here this very moment. I won’t listen to you. I’ll see you in the office!”

Vladimir banged the door, and, going through the house, went directly to Vasya, to the bedroom.

When he came in he stopped as though paralyzed. Vasya was sitting at the table with the shipping clerks, “drawing up” demands.

“And what’s this? Who let you in here? How did you dare come in here without permission? Get out of here! Get out!”

“But Vladimir Ivanovitch, we didn’t come in on our own hook.... Your wife...”

“Get out, I say, or...”

Vladimir was white as a sheet; he was raising his arm. The men retreated toward the door.

“Are you crazy, Vladimir? How dare you! I called them in! Stop, Comrades! Where are you going?”

Vasya ran after them, but Vladimir stepped in her way and grasped her arm so tightly that she cried out.

“You invited them? Who gave you permission? Who asked you to meddle in my affairs? You’re not responsible to the syndicate! If you want to start any strikes, go to your hemp-binding works!”

“Oh! So you’re driving me away? Because I’m siding with my brothers. Because I want the truth. Because I don’t consider your managerial interests, because I lower your bonus.”

“You should be ashamed of yourself. You disgusting hypocrite.”

Vasya felt as if he had lashed her with a whip. Disgusting? She, Vasya, was disgusting?

They faced each other furiously, like enemies. But her heart was filled with agonizing, excruciating pain. Was her happiness gone forever?

The shipping clerks had dispersed, and Vladimir had gone to the office. Lying across the bed, her face buried in the quilt, Vasya was letting her tears wet the silk. But her sorrow could not be relieved by tears.

She was heartbroken, not at his having called her disgusting, but at their estrangement, their inability to understand each other. Like enemies, in two hostile camps.

The days that followed were dismal, cheerless. Vladimir spent much time at home. But what good did that do? They were just like strangers, spoke only when it was absolutely necessary. Each lived his own life. Vasya was ill again. Ivan Ivanovitch had gone for the doctor, who had ordered a complete rest for her, and had forbidden all excitement.

Vladimir was very busy with his work. He would sit up half the night in his study with Ivan Ivanovitch and the bookkeeper. They would come out for supper, but their thoughts were wrapped in their business; they were taciturn and in bad humor.

Occasionally Lisa Sorokina would visit Vasya to tell her about the hemp-binding works. The girls were sorry that she was ill.

Yet her illness did not distress Vasya as much as the knowledge that she and Volodya had become estranged. Neither could forget the quarrel about the shipping-clerks. Neither could forgive the other.

Vasya thought of going home to her province. She wanted to be back home. But where could she go? Grusha was living in her attic under the roof; it would be very crowded for two. She could not think of going to her parents to recuperate, for they would weep over her, and would rail against the Bolsheviki. Where, then? Vasya wrote to Grusha, asking her to get a room for her. And she wrote to Stepan Alexeyevitch, asking him to procure some work for her, with the Party, with the masses. She would go as soon as she heard from them. Why should she stay here? No one needed her. Volodya would get along without her. The days dragged, slowly, heavily.

It was midsummer. The cherries in the garden were ripe; the plums were covered with a purplish bloom. The lilies, white and delicate, shone on their tall, dewy stalks. But nothing delighted Vasya now. As she wandered through the garden she would remember how she had lain in the chaise lounge in the spring, how glad she had been that she was alive. And the memory made her heart even heavier.

She felt as if she had been quite another Vasya then, a young, confiding Vasya. Something had gone out of her. What was it? She didn’t know exactly. But this much was certain. It had gone out of her, and would never come back.

Sometimes Vladimir would stand at the window and watch Vasya walking about the garden, indifferent, drooping. He would stand at the window a while; then, turning away abruptly, would return to Ivan Ivanovitch and his work.

Then Vasya would sigh with fresh disappointment. She had expected him to come down to her, to the garden. And he hadn’t come. There it was. It was clear that he had no more feeling for her. To him business was more important than the anguish of a woman’s heart.


Some noise woke Vasya. It was morning. Vladimir was rummaging in his wardrobe, taking out something.

“What are you doing there so early, Volodya?”

“I have to meet a train, there’s a consignment coming in.”

“Must you go yourself?”

“I have to supervise.”

Vladimir was standing before the mirror, putting on his new tie; but he couldn’t quite manage it. As Vasya looked at him she suddenly felt once more that he was so close, so deeply bound to her.

“Come here, Volodya. Let me help you.”

He came obediently, sat down on the bed. Vasya made his tie. They looked at each other, and suddenly, without a word, they were in each other’s arms.

“My little Vasyuk! My darling. It hurts so to live beside you, and yet so terribly far away. Can’t it be different?” he asked plaintively, pressing Vasya’s curly head to his breast.

“Do you think it doesn’t hurt me? I don’t want to live any more.”

“But why do we quarrel, Vasyuk?”

“I don’t know. There’s some barrier between us.”

“No, Vasya. No, nothing can stand between us. My heart is all yours, only yours.”

“And you haven’t stopped loving me?”

“You silly little thing.” He kissed her. “Come, let’s stop quarreling. It’s stupid, and makes both of us suffer. And I can’t afford to lose you, Vasya. can’t live without you. So now we won’t hurt each other any more?”

“You won’t try to play a managerial role any more?”

“And you won’t set the shipping clerks against me?”

They laughed.
“But now you go to sleep. If you don’t sleep you’ll be sick again for the day. I’ll be back in about two hours.”

Covering her, he kissed her eyes and went. Vasya felt happy, light of heart. She fell asleep as if all her joy had come back to her, as if she had lost nothing.

Vladimir didn’t come back from the station, but telephoned that he would have to go to the office. He would be back for dinner. Vasya was feeling better, but she didn’t go to the hemp-binding works. Instead, she busied herself about the house, helping Marya Semyonovna to straighten up the house.

Not long before dinner the telephone rang. Vasya answered.

“Hello.”

“Is Vladimir Ivanovitch at home?”

“No, not yet. Who’s speaking?”

“The administrative office.”

“But why do you call here? He’s still in your office.”

“No, he’s not here; he left the office some time ago. Please forgive me.”

That woman’s voice again. Who was it? Vasya didn't like that voice. During the first few days of
her stay it had called often. Then it had stopped. Vasya once asked Ivan Ivanovitch, quite casually, who it might be that was forever telephoning from the office, and during working hours at that. Ivan Ivanovitch explained that it was the clerks. Queer, that their voices should be so much alike. And again Vasya felt the serpent’s fangs.

Vladimir brought home two members of the administration for dinner.

They discussed the morning’s consignments. Nevertheless he found time to ask how Vasya was feeling whether she had been sunning herself, as the doctor had ordered.

“No, I didn’t lie in the sun."

Dryly Vasya brought the conversation to a close, adding carelessly:

“The young lady who’s forever phoning you from the office called again.”

“What young lady?” Vladimir looked surprised. “From the office, you say? Then it must be the Shelgunov woman – some young lady, that one! A venerable materfamilias. You’ve seen her, Vasya  –  the fat woman with the wart on her face.”

He spoke so simply, so naturally. But Vasya felt uneasy.

No. Something was wrong there.

After dinner the gentlemen of the management went away. Vasya was glad. She wanted to be alone with
Vladimir, to warm her spirit. The morning’s promise of joy would be fulfilled.

But the guests had hardly left when the telephone buzzed in the study. Vladimir went to answer it.

“Yes, it’s I.” Curtly, “Didn’t I ask you not to telephone?”

A short laugh. “Family matters, of course.” Reproachfully, “By no means, I forbid it most decidedly.” Vehemently, “All right, all right.” Relenting, “But not for long. Good-bye.”

Vasya was in the next room, listening.

With whom was he speaking? Whom did he promise: “But not for long?” To whom could he say: “I forbid it.”

Vladimir went from the study straight into the bedroom, passing Vasya as though he didn’t see her. She followed him. He was standing before the mirror, combing his hair.

“To whom were you speaking, Volodya?”

“To Savelyev.”

“To Savelyev? Has he come back?”

“This morning.”

"Did you meet him?”

“Look here, what sort of cross-examination is this? You know I was supervising the unloading of a shipment this morning.” He seemed disturbed.

“And you’re going to him right away? Did you promise?”

“Yes, I’m going there.”

Silence.

Vasya felt her heart hammering, pounding. As if it would burst. If only it would. She could endure this agony no longer. She went quickly over to Vladimir, gently took his hand. “Don’t do that, Volodya. Don’t start that business again....”

“What do you mean?” he asked suspiciously, uneasily.

“Don’t have anything to do with that crooked speculator. I’ve been warned. After all, that’s the principal thing they have against you, your association with objectionable people.”

“Ah. There you go again. Talking like one of your Supervisory Committee. Do you insist on tormenting me? Tyrannizing me? Do you Want to tie me to your apron strings?”

Flushed, he pushed Vasya’s hand away.

“Stop, Vladimir. Stop. What did you say? Did I ever attempt to chain you to me? Try to keep your head. I’m talking about you, not myself. Don't dig a pit for yourself. You have enemies enough. And if you resume your friendship with Savelyev...."

“What has Savelyev to do with this?”

“What do you mean? What has he to do with this? Aren’t you going to him?” Vasya’s eyes were troubled.

“Of course I’m going to him. But what of it? Can’t you understand that I’m going to him on business? It can’t be helped.”

“I don’t believe you,” she cried hotly. “Postpone it for tomorrow, tell him to come to the office.”

“What a child you are, Vasya,” he said, altering his tone. “All right, I’ll tell you the truth. It’s true that Savelyev didn’t call me over to discuss business. That can be attended to in the office. He’s simply having a jolly little crowd at his house. And he asked me to come over for a game of cards. You know yourself, Vasya, that I went nowhere for almost a month. I was at home, and taken up with business, all the time. Let me get a breath of air for a change, Vasya. I’m young. I want to live. I can’t be a hermit.”

“I understand, Volodya,” she said sadly. “Yes, everything’s as you say. And your getting a little diversion is no calamity. But you must understand one thing. You mustn’t start up again with this Savelyev, this speculating scoundrel. You have no respect for him yourself. What do you need him for? People will be saying right away that Vladimir Ivanovitch and Savelyev are hand and glove again. And then the whole business’ll start all over again. Volodya darling. Please don’t go there today. Cancel it.”

“What nonsense!” Volodya was losing his patience. “If the Provincial Committee has nothing to do but take legal action against a fellow because of his acquaintances, then it’s no Provincial Committee but a cesspool. You’re exaggerating, Vasya.”

“But I don’t like to see you go there. I know he can’t stand me. He asks you over only to hurt me. Didn’t I hear you say over the phone that you couldn’t come on account of your family? And then you laughed. Volodya...” She was becoming agitated “It hurts me to see you laughing with a stranger about me, and with Savelyev at that. As if I didn’t let you go.

“Well. You don’t.”

“So that’s how you put it. Very well, then, go! But remember...,” her eyes flashed. “Remember that my patience is at an end. I’ve helped you, suffered for you, stood up for you. That’s enough. Go if you want to. But then I’ll know what I have to do.” Her voice rose to a shrill, hysterical shriek.

“I’m sick of your hysterics! Why do you nag me  –  what do you want of me?”

“Volodya!” There were tears in Vasya’s voice. “I’ve never asked you for anything. But today I beg you to stay. For your sake, and mine.”

“Oh, you women. You’re all the same. Disgusting.” Rushing past her, he hurried through the hall; the front door banged. The motor purred.


“I’ve come to you, Lisa. Take me in. I’ve gone away from him forever.”

Her voice failed her, but her eyes were dry. Her misery was too great for tears.

“You’ve come away from him? You should have done it long ago! We’ve all been wondering that you’ve stood it so long...”

“We’ve become estranged, Lisa. That’s the terrible thing,” wailed Vasya.

“Of course. How in the world can you love him?”

Vasya ignored the question. She could hardly believe what had happened. She could never forgive, never forget this indignity. It had been the first time she had begged him for something. And what had he done? He might just as well have walked over her dead body. And why? Why? To play cards with that thief, that speculator, Savelyev, and a crowd of his filthy fellows! It was all the same to him that Vasya was dying of grief. As long as he was having a good time, as long as he was getting the entertainment he wanted. Was that love? Was that her friend and comrade? Was that a Communist?

Lisa was unable to make head or tail out of Vasya’s incoherent speech. What had happened? What did Savelyev have to do with it?

“What does he have to do with it? Why, it was all on his account, on account of that crooked speculator. Vladimir went to him.”

“You think he went to him?”

“Why, to whom do you think? Don’t you believe it?”

“But what is there to believe? The whole town knows it; only you seem to be blind. Or do you refuse to see it? Do you refuse to realize it?”

“See what, Lisa? Tell me!”

“Why, that your Vladimir has a friend!”

“A friend?”

Vasya did not understand at once, but stared at Lisa. She was neither shocked nor grieved, but only surprised.

“A friend you say. Who is it?”

“Not one of us, not a working girl. One of the office employees.”

“Do you know her?”

“I’ve seen her. The whole town knows her.”

“Why?”

“She’s always so dressed up. That’s why the Comrades are so angry at your Vladimir. Michailo Pavlovitch told you of this friendship, too. How could you help knowing about it? You’re not so stupid otherwise. But in this again you’ve acted like a real goose!”

Vasya, however, was concerned with something quite different.

“Does he love her?”

“How should I know? He must love her; he’s been running around with her for so many months. People thought the affair would stop when you came. But nothing of the sort. He’s forever going to her in his car.”

“Does she have a home of her own?”

“The chances are it's more elaborate than yours.”

So that’s what it was. “He’s keeping up two households.”

Now Vasya understood everything. Everything but one point. Why had Volodya lied to her, tormented her, deceived her?

“What do you expect? Was he to come to you as the contrite sinner? Or was he to beg you for permission to visit his friend? It was your business to see it. If you didn’t, you were a fool, and have only yourself to blame.”

“Why do you insist on talking about my being a fool, Lisa? That’s not important. The question is this: Does he really love her, or does it only look like it?”

“How do you mean? I don’t know what you’re talking about! He must love her. Doesn’t he support her entirely, and give her expensive presents?”

“Do you think so? But, you see, I don’t know....”

“You surely don’t believe that he loves you? Don’t fool yourself, Vasya. It’ll only hurt you all the more. He likes and esteems you, of course. You’re his wife and comrade. But as for loving you. That was over long ago. I know...”

Vasya shook her head. “But, you see, I don’t agree
with you."

Her stupidity annoyed Lisa, who now told her about Volodya’s friend. Beautiful as a picture. And her
clothes. Always dressed in silks and always surrounded by admirers. Savelyev was one of them; knew her well. It was very gay there in the evening. And there were rumors that both Vladimir and Savelyev were keeping her.

For some reason or other this idea was particularly distasteful to Vasya.

Had Vladimir actually changed so? Could he really love a woman like that? Vasya didn’t believe the stories she heard. She didn’t believe them. Something was wrong there.

Lisa, however, was offended. “All right, don’t believe me. It’s your own affair. Ask anybody; everyone’ll tell you the same thing. She was in the office as Savelyev’s secretary, until she let the manager keep her. But it’s possible that others are making use of her, too. They’re talking about Ivan Ivanovitch, too. And some members of the administration visit there occasionally. She’s a real one, only not registered. Her luck that they don’t have to have permits nowadays.”

“But Vladimir would never have fallen in love with a woman like that,” objected Vasya.

“Why do you think that? Men like that kind, especially men like your Vladimir. You can see it in his face: the worse the woman, the better he’ll like her.”

“Be still, Lisa! How dare you! You don’t know him. How can you judge him like this?”

“Why do you stand up for him? Didn’t he make you the laughing stock of the town? But you, you defend him like a fortress!”

“Made me a laughing stock! And how, if you please? What have Vladimir’s actions to do with me? I’m not responsible for him. You don’t understand, Lisa. That isn’t why I’m suffering. That’s not it at all.”

“I know. You’re suffering because he doesn’t love you any more.”

“No, Lisa, that’s not it, either. It hurts, of course. But it’s not the most important thing. I know what I mean, but I can’t find the words. What is it? We were such comrades, so close and intimate and suddenly you say: Vladimir turned away from me, lied to me, was afraid of me. Of me! How could he? Would I have stood in his way? Would I have kept him from his love? He couldn’t, Volodya couldn’t think that! There must be something else. He can’t love that girl so much.”

“Now you’re beating about the bush,” said Lisa, waving her away angrily. “It’s impossible to talk to you. You’re still in love with Volodya: ‘Strike me, wipe your feet on me – it’s all the same to me, I’m still your obedient wife – I’ll lick your boots’ – I’m not like that. I’d have got even with him long ago. I’d have given him something to think about.”

Vasya didn’t deny it. But the more Lisa condemned Vladimir, the more ardently Vasya defended him. She wanted to convince Lisa that he was wrong not in taking a friend, not in loving another woman, but only in his not having told her, Vasya, about it. As if she weren't his friend and comrade, but a stranger. What was more, she meant less to him than a stranger, for he had no faith in her. Did he think that she would fight for her rights like a legitimate wife?

“You must fight for them,” shouted Lisa. “Of course you must fight for them. How dared he make you ridiculous? Then you surely must leave him.”

Vasya objected. It was always like that. In her heart she often condemned Vladimir, disagreed with him entirely. But the moment some one else attacked him she took his side and grew indignant. People didn’t understand him. She alone knew Vladimir, the American. Only when she said “The American” did the tears come. She remembered Vladimir, the American, leading the members of their group, fighting for the Soviet.

Weeping, she fell in Lisa’s arms. She was not thinking of Vladimir, the manager, but she was mourning for the “American,” was suffering for him inconsolably, mortally.

“It’s so hard for me, Lisenyka. I’m exhausted.”

“I know, darling. Only be patient. It’ll pass. I went through the same thing last year. But when we meet nowadays it doesn’t bother me at all.”


Vasya was unable to sleep, although Lisa had given her her own bed, sleeping on a couple of chairs herself. Lisa had worked all day; now she was sleeping soundly. Vasya turned restlessly from side to side, now sitting up, now lying down again. She couldn’t rest. Countless thoughts raced through her head, tortured and broke her heart. It was like that dreadful night when she had found the bandage, when Vladimir had been arrested.

It wasn’t jealousy that was tormenting her. But Volodya’s lack of confidence hurt her. If not for that, she would forgive everything. Man cannot control his heart. But Vasya did not believe that he loved the other girl. She did not believe it. It was only a “liaison.” For months he had lived alone, he of the ardent temperament. (She remembered Styosha.) He had begun an affair with her, and it went on. She probably wouldn’t let him go. Lisa, too, said he was a ladies’ man. And if it was that, it couldn’t be a case of love. Therefore she was looking out for her own advantage. Volodya would have liked to tear himself away, but he could not. Vasya remembered how moody, how variable he had been; now loving, now distant. He had suffered agonies. How could one live with the person one loved while the other was sharpening the knife behind his back? She remembered how frequently Vladimir had tried to confess something, how he had always stopped short. He had been about to say something on the morning of the clash with the shipping clerks. Vasya had felt he had it on the tip of his tongue. She, too, had been frightened, and unfortunately had begun to cough. Then Vladimir said nothing more; did he, therefore, pity her? And if he pitied her, he loved her. But did he love her? It was easy to say he did. But what about the blue material? The same for both of them?

“I have bought you a present, my beautiful sweetheart; and I haven’t forgotten that wearisome wife of mine. Here, take the silk, and say nothing.”

Damn him! Vasya clenched her fists as though she wanted to fight with Vladimir. She thought: so he didn’t go to Savelyev yesterday? And Savelyev had nothing to do with it. He had been only a screen. Had she known that he had a friend who pretended to
love him, she would not have been angry with Vladimir. She would have been unhappy; still, she would have understood. But to humiliate Vasya for that vulgar speculator, Savelyev! She would have understood the affair with his friend; yet, would she have forgiven? As she had forgiven him for the nurse, for Styosha? Would she have been able to like the white poodle, to forget the blue silk? 
Contents |
Alexandre Kollontai Archive