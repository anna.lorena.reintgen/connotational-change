
In the midst of the bridge there we stopped and we wondered  
   In London at last, and the moon going down,  
All sullied and red where the mast-wood was sundered  
   By the void of the night-mist, the breath of the town.  

On each side lay the City, and Thames ran between it  
   Dark, struggling, unheard 'neath the wheels and the feet.  
A strange dream it was that we ever had seen it,  
   And strange was the hope we had wandered to meet.  

Was all nought but confusion?  What man and what master  
   Had each of these people that hastened along?  
Like a flood flowed the faces, and faster and faster  
   Went the drift of the feet of the hurrying throng.  

Till all these seemed but one thing, and we twain another,  
   A thing frail and feeble and young and unknown;  
What sign mid all these to tell foeman from brother?  
   What sign of the hope in our hearts that had grown?  

We went to our lodging afar from the river,  
   And slept and forgot--and remembered in dreams;  
And friends that I knew not I strove to deliver  
   From a crowd that swept o'er us in measureless streams,  

Wending whither I knew not:  till meseemed I was waking  
   To the first night in London, and lay by my love,  
And she worn and changed, and my very heart aching  
   With a terror of soul that forbade me to move.  

Till I woke, in good sooth, and she lay there beside me,  
   Fresh, lovely in sleep; but awhile yet I lay,  
For the fear of the dream-tide yet seemed to abide me  
   In the cold and sad time ere the dawn of the day.  

Then I went to the window, and saw down below me  
   The market-wains wending adown the dim street,  
And the scent of the hay and the herbs seemed to know me,  
   And seek out my heart the dawn's sorrow to meet.  

They passed, and day grew, and with pitiless faces  
   The dull houses stared on the prey they had trapped;  
'Twas as though they had slain all the fair morning places  
   Where in love and in leisure our joyance had happed.  

My heart sank; I murmured, "What's this we are doing  
   In this grim net of London, this prison built stark  
With the greed of the ages, our young lives pursuing  
   A phantom that leads but to death in the dark?"  

Day grew, and no longer was dusk with it striving,  
   And now here and there a few people went by.  
As an image of what was once eager and living  
   Seemed the hope that had led us to live or to die.  

Yet nought else seemed happy; the past and its pleasure  
   Was light, and unworthy, had been and was gone;  
If hope had deceived us, if hid were its treasure,  
   Nought now would be left us of all life had won.  

O love, stand beside me; the sun is uprisen  
   On the first day of London; and shame hath been here.  
For I saw our new life like the bars of a prison,  
   And hope grew a-cold, and I parleyed with fear.  

Ah!  I sadden thy face, and thy grey eyes are chiding!  
   Yea, but life is no longer as stories of yore;  
From us from henceforth no fair words shall be hiding  
   The nights of the wretched, the days of the poor.  

Time was we have grieved, we have feared, we have faltered,  
   For ourselves, for each other, while yet we were twain;  
And no whit of the world by our sorrow was altered,  
   Our faintness grieved nothing, our fear was in vain.  

Now our fear and our faintness, our sorrow, our passion,  
   We shall feel all henceforth as we felt it erewhile;  
But now from all this the due deeds we shall fashion  
   Of the eyes without blindness, the heart without guile.  

Let us grieve then--and help every soul in our sorrow;  
   Let us fear--and press forward where few dare to go;  
Let us falter in hope--and plan deeds for the morrow,  
   The world crowned with freedom, the fall of the foe.  

As the soldier who goes from his homestead a-weeping,  
   And whose mouth yet remembers his sweetheart's embrace,  
While all round about him the bullets are sweeping,  
   But stern and stout-hearted dies there in his place;  

Yea, so let our lives be! e'en such that hereafter,  
   When the battle is won and the story is told,  
Our pain shall be hid, and remembered our laughter,  
   And our names shall be those of the bright and the bold.  

NOTE--This section had the following note in The Commonweal.  It is the intention of the author to follow the fortunes of the lovers who in the "Message of the March Wind" were already touched by sympathy with the cause of the people. The Pilgrims of Hope: Next PoemThe Pilgrims of Hope: Previous PoemThe Pilgrims of Hope: IndexThe William Morris Internet Archive : Works