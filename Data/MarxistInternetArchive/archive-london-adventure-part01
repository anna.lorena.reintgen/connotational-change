He was a very sick white man. He rode pick-a-back on a woolly-headed, 
black-skinned savage, the lobes of whose ears had been pierced and stretched 
until one had torn out, while the other carried a circular block of carved wood 
three inches in diameter. The torn ear had been pierced again, but this 
time not so ambitiously, for the hole accommodated no more than a short clay 
pipe. The man-horse was greasy and dirty, and naked save for an 
exceedingly narrow and dirty loin-cloth; but the white man clung to him closely 
and desperately. At times, from weakness, his head drooped and rested on 
the woolly pate. At other times he lifted his head and stared with 
swimming eyes at the cocoanut palms that reeled and swung in the shimmering 
heat. He was clad in a thin undershirt and a strip of cotton cloth, that 
wrapped about his waist and descended to his knees. On his head was a 
battered Stetson, known to the trade as a Baden-Powell. About his middle 
was strapped a belt, which carried a large-calibred automatic pistol and several 
spare clips, loaded and ready for quick work. The rear was brought up by a black boy of fourteen or fifteen, who carried 
medicine bottles, a pail of hot water, and various other hospital 
appurtenances. They passed out of the compound through a small wicker 
gate, and went on under the blazing sun, winding about among new-planted 
cocoanuts that threw no shade. There was not a breath of wind, and the 
superheated, stagnant air was heavy with pestilence. From the direction 
they were going arose a wild clamour, as of lost souls wailing and of men in 
torment. A long, low shed showed ahead, grass-walled and grass-thatched, 
and it was from here that the noise proceeded. There were shrieks and 
screams, some unmistakably of grief, others unmistakably of unendurable 
pain. As the white man drew closer he could hear a low and continuous 
moaning and groaning. He shuddered at the thought of entering, and for a 
moment was quite certain that he was going to faint. For that most dreaded 
of Solomon Island scourges, dysentery, had struck Berande plantation, and he was 
all alone to cope with it. Also, he was afflicted himself. By stooping close, still on man-back, he managed to pass through the low 
doorway. He took a small bottle from his follower, and sniffed strong 
ammonia to clear his senses for the ordeal. Then he shouted, “Shut up!” 
and the clamour stilled. A raised platform of forest slabs, six feet wide, 
with a slight pitch, extended the full length of the shed. Alongside of it 
was a yard-wide run-way. Stretched on the platform, side by side and 
crowded close, lay a score of blacks. That they were low in the order of 
human life was apparent at a glance. They were man-eaters. Their 
faces were asymmetrical, bestial; their bodies were ugly and ape-like. 
They wore nose-rings of clam-shell and turtle-shell, and from the ends of their 
noses which were also pierced, projected horns of beads strung on stiff 
wire. Their ears were pierced and distended to accommodate wooden plugs 
and sticks, pipes, and all manner of barbaric ornaments. Their faces and 
bodies were tattooed or scarred in hideous designs. In their sickness they 
wore no clothing, not even loin-cloths, though they retained their shell 
armlets, their bead necklaces, and their leather belts, between which and the 
skin were thrust naked knives. The bodies of many were covered with 
horrible sores. Swarms of flies rose and settled, or flew back and forth 
in clouds. The white man went down the line, dosing each man with medicine. To 
some he gave chlorodyne. He was forced to concentrate with all his will in 
order to remember which of them could stand ipecacuanha, and which of them were 
constitutionally unable to retain that powerful drug. One who lay dead he 
ordered to be carried out. He spoke in the sharp, peremptory manner of a 
man who would take no nonsense, and the well men who obeyed his orders scowled 
malignantly. One muttered deep in his chest as he took the corpse by the 
feet. The white man exploded in speech and action. It cost him a 
painful effort, but his arm shot out, landing a back-hand blow on the black’s 
mouth. “What name you, Angara?” he shouted. “What for talk ’long you, 
eh?I knock seven bells out of you, too much, quick!”With the automatic swiftness of a wild animal the black gathered himself to 
spring. The anger of a wild animal was in his eyes; but he saw the white 
man’s hand dropping to the pistol in his belt. The spring was never 
made. The tensed body relaxed, and the black, stooping over the corpse, 
helped carry it out. This time there was no muttering. “Swine!” the white man gritted out through his teeth at the whole breed of 
Solomon Islanders. He was very sick, this white man, as sick as the black men who lay helpless 
about him, and whom he attended. He never knew, each time he entered the 
festering shambles, whether or not he would be able to complete the round. 
But he did know in large degree of certainty that, if he ever fainted there in 
the midst of the blacks, those who were able would be at his throat like 
ravening wolves. Part way down the line a man was dying. He gave orders for his removal 
as soon as he had breathed his last. A black stuck his head inside the 
shed door, saying,—“Four fella sick too much. ”Fresh cases, still able to walk, they clustered about the spokesman. 
The white man singled out the weakest, and put him in the place just vacated by 
the corpse. Also, he indicated the next weakest, telling him to wait for a 
place until the next man died. Then, ordering one of the well men to take 
a squad from the field-force and build a lean-to addition to the hospital, he 
continued along the run-way, administering medicine and cracking jokes in 
