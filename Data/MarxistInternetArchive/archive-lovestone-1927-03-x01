Jay Lovestone
Source: The Communist, Vol. VI, No. 1, March 1927, pp. 7-16.
Publisher: Workers (Communist) Party of America
Transcription/HTML Markup: Brian Reid
Public Domain: Marxists Internet Archive (2009). You may freely copy, distribute, display and perform this work; as well as make derivative and commercial works. Please credit “Marxists Internet Archive” as your source.
BEFORE attempting to analyse the present role of American imperialism it is necessary to characterize briefly the high spots, the main features, of American capitalist industry. Nor is it possible to evaluate adequately the prospects of capitalism in the United States and its international relations without getting at least a cursory glimpse of American imperialist resources.

At the outset we should keep clearly in mind three fundamental features of American capitalism at this moment.

1) American capitalist development is not yet at its zenith. Our bourgeoisie still has plenty of resources untouched and undeveloped. The productive capacities of the industrial machine in the United States can be brought up to a still higher level than prevails today. To make the point more clear. Let us examine the outstanding features of the latest period in America’s export of capital. We will examine the figures for the first nine months of 1926, especially the third quarter of the year. The total amount of capital exported during this period by the American imperialists is $890,000,000. What we are most interested in are the characteristic trends of the export of this capital. These indicate beyond a doubt that American capitalism is still on the upgrade.

We find that the main characteristics of the export of capital from the United States in the first nine months of 1926 were:

a) An unusual proportion of new capital: The sums given over to refunding in this whole period were only 4.3% of the total par value of the capital exported. For the first six months this ratio was 25.8%. No emphasis is needed to characterize the sharp turn to new flotations in the latter part of 1926.

b) We also find an unusual proportion of large blocs of securities, of big individual shipments of capital from the United States. Of a total of 43 issues under-written in the third quarter of 1926 at least 15 shipments of capital were for ten million dollars or over. Again, it is significant that in the first six months of 1926 there were only 16 cases of the export of capital out of a total of 109 which were 10 million dollars or over.

c) About two-thirds of the capital exported went to private corporations. Only one-third was taken by governmental agencies. Obviously American imperialism feels that European capitalism is considerably stabilized. In previous years the largest proportion of the capital exported went to governments—to help these governments maintain and solidify their position. Europe was the largest borrower during this period. At the same time Germany was the largest single borrower in the world.

2) American colonial resources are not yet fully exploited. It is only in recent years that the United States Department of Commerce has been giving special attention to develop the resources for certain raw materials in which the United States is at present deficient, such as rubber, sisal, etc. A few months ago a special mission was sent to the Philippines to do prospecting for and report on the possibilities of raising rubber in the Islands. Carmi Thompson, who headed the mission, has reported favorably on the prospects of developing sufficient supplies of rubber in the Philippines to make America independent from the British who today control raw rubber production. This will precipitate America’s tightening her grip on the Philippines.

In speaking of the colonial resources which American capitalism can exploit to a higher degree than has been done to date, I have in mind also the huge oil tracts and other unmeasured resources in the semi-colonial countries of Latin America. Witness the sharpening conflict with Mexico—primarily an oil conflict.

3) We should also keep in mind the fact that America’s military and naval powers have not yet been fully exerted in action. Nor has America mobilized to her highest industrial capacity, her resources in military and naval preparations. Today America’s military and naval budgets are more than two hundred per cent (224%) bigger than they were prior to the war. But if the American capitalists should find it necessary even to treble these budgets in order to make more secure their dominant position in the world market, then they could do so without a dangerously oppressive. strain on their resources.

It is not necessary to go into details to get an idea of the extensive control American capitalism has over some of the basic resources of the world such as iron, coal, steel, petroleum, cotton, wheat, etc.

For instance, in all Europe, inclusive of the Soviet Union, there are to be found 42,800 square miles of coal area. More than half of this total is within the boundaries of the Soviet Union. In the United States we find 340,000 square miles of coal area available for exploitation. Each of such states, as West Virginia or Kentucky, alone has a greater coal area than France, Germany, Belgium and England combined. America produces one-and-a-half times the total steel produced by Great Britain, Germany and France. In the United States there is raised fifty-five out of every hundred bales of cotton produced in the world.

With only six per cent of the world’s population the total wealth of America is approximately $400,000,000,000.

If one were to get a bird’s eye view of American capitalist strength in the world, he would find that Yankee capitalists are either heavily interested or dominate manufacturing industries in twenty-five countries, public works in sixteen lands, railways in thirty countries, the mining resources of twenty-five nations and the sugar and fruit industries of fifteen countries.

The rationalization process in European capitalism is child’s play compared with that in America. There are in the United States six hundred industrial research laboratories working day and night to devise ways and means of improving capitalist production, exchange and distribution. In the last two decades the electric power used in American manufacturing industries increased 236%.

Briefly summarized, the main features of American capitalism are: mass production, extensive use of time-saving devices, highly developed industrial research and technique, increasing elimination of waste and especially developed schemes for class collaboration in industry such as company unions, profit-sharing ventures, workers’ insurance systems, stock ownership by workers and sundry welfare frauds palmed off as genuine industrial democracy.

In speaking of rationalization one must therefore not overlook a fundamental feature of this process as manifested in America where rationalization is most highly developed. In the United States the class collaboration movement is an integral phase of the rationalization process. It is tied up with the very methods of American industry. Rationalization must therefore also be viewed as not only new relations between man and machine, not only improved technique, scientific management and the elimination of waste but also as an attempt at new relations between man and man, between employer and worker.

No doubt the European bourgeoisie in their rationalization drive will also endeavor to institute in and force upon their industries various “industrial peace” schemes now in vogue in the United States, the numerous class peace agencies so often resorted to in America, in order to stabilize industry and intensify exploitation. Here we clearly see that if the workers are to answer effectively the rationalization schemes of the bourgeoisie, the stabilization attempts borrowed so largely from the United States, they must pit the sound proposals of bolshevization against the dangerous attempts of imperialist Americanization.

The outstanding leaders of the world’s exploiters very well realize this. They are aware of the fact that much more than ever before large sections of the international working class are cognizant today that the opposite poles in the present world situation are the United States of America and the Union of Socialist Soviet Republics. We need but call upon Premier Baldwin to illustrate our point. Recently Mr. Baldwin said:

“I will urge employers and trade union leaders to make the point of visiting America quickly and often just at present, to study their methods, which are proving to be of such success in production, and I venture to think that no trade union leader could do better service to the cause he represents than by investigating closely what the methods are that enable American workmen to enjoy a better standard of living than any other working people in the world, to produce more and at the same time to have higher wages. I venture to think that THERE IS MUCH MORE FOR US TO LEARN FROM STUDYING CONDITIONS IN THAT COUNTRY THAN BY SPENDING ANY AMOUNT OF MONEY STUDYING CONDITIONS IN MOSCOW.” (My emphasis.)


The key to the question of America’s immediate role in the international situation is to be found in an analysis of America’s present relations with Europe. Various theories have been advanced regarding the actual relations between the United States and the different European capitalist groups. Numerous predictions have been made as to the course these relations are now taking and will take in the near future. Let me attempt to discuss the viewpoint of European-American relations as developed by Comrade Trotsky in his article appearing in the Pravda on March 4, 1926.

In this article Comrade Trotsky, in his finest belletristic style and skilled satirical method, flays my position on the relations between America and Europe. Some time ago in reviewing Comrade Trotsky’s “Whither England?” in the “Workers Monthly” (official organ of the Workers (Communist) Party of America) I criticised Trotsky’s theory of “America Putting Europe on Rations.” I attacked the theory of Comrade Trotsky that America is pursuing such a course as will subordinate completely European politics to American politics.

Let us now examine critically the conditions on which Comrade Trotsky’s theory of “Europe on Rations” can be based. American rationing of Europe is possible under either of two conditions. First of all, either through a victorious war against Europe’a war in which America conquers Europe. Secondly, American rationing of Europe is possible through a complete and permanent, basic Anglo-American alliance. So long as Great Britain has energy and resources and strength she will not permit America to put Europe on rations. A permanent Anglo-American alliance is a prerequisite to such a rationing of Europe. And a definite, decisive subordination, industrially and financially, of England by America is a positive prerequisite for such an alliance today or in the near future between the United States and Great Britain.

Let us not overlook the fact that England has by no means given up the struggle for the world economic supremacy which has in recent years fallen from her hands into those of the American imperialists. England is on the down-grade but she is still quite far from being down. An alliance between Great Britain and Japan or France, or between Great Britain and a number of European capitalist countries against the United States would prove a dangerous challenge to the prowess of American imperialism.

What does this theory of Comrade Trotsky really mean? In effect it means that New York is today the decisive, the sole, the primary obstacle to the proletarian revolution. This theory of Comrade Trotsky means that no revolution is possible in Europe without there first being a successful revolution in America. If that be the case what will we further find? We find that, according to Comrade Trotsky’s theory, the building of Socialism in the Soviet Union is out of the question. But this is not bad enough. The logical deductions from Comrade Trotsky’s theory, the consequences arising from such a state of affairs are even worse. Here they are: 1. We cannot have a successful development of Socialism in the Soviet Union without there being a proletarian revolution in Europe. 2. We cannot have a successful revolution in Europe unless we have it first in the United States.

This simply means that we really cannot have Socialism developed the Soviet Union without there being a successful revolution in America. 

Of course, even the most sanguine hopefuls and the least objective persons will agree that we are today quite some distance from this revolutions in the United States. If the occurrence of the proletarian revolution in Europe depends on the occurrence of the proletarian revolution in the United States, then there is very slight likelihood of the international Communist movement having such success in Europe for a long time to come. Likewise, if the success of Socialism in the Soviet Union depends under such circumstances on the possibility of successful proletarian revolution in Europe or, on what is even worse but what logically follows from Comrade Trotsky’s theory, on the occurrence of a successful revolution in America, then, the building up of socialism in the Soviet Union has as much chance as a snowball in hell.

This theory of Europe on rations is really an extention of Trotsky’s theory of permanent revolution. Perhaps this accounts for Comrade Trotsky’s recently intensified pessimism regarding the development of socialism in the Soviet Union. Comrade Trotsky is wrong in his theory of America putting Europe on rations. The objective logic, the conclusions of this theory are equally erroneous. Europe is still alive. With increasing effectiveness European capitalism is resisting American imperialist encroachments. Socialism is being consistently and systematically developed in the Soviet Union.

Let us briefly sketch some of the latest basic facts indicating that America has not only NOT put Europe on rations but that Europe is showing multiplying signs of effective resistance to the attempts at domination by American imperialism.

1. a) On the eve of the Locarno Conference Stressemann told the newspaper correspondents that the ultimate purpose of the gathering was: “the re-establishment of Europe and its liberation from dependence on America.”

b) The official mouthpiece of one of America’s biggest banks, the “Index”, published by the New York Trust Company, has several months ago summed up the European attitude towards America in this fashion: “There is a united impression that the United States has so profited by continental misfortunes that Europe must organize to compete successfully with this country; no single nation is strong enough to prevent American dominance in economic affairs.”

2. The wave of mortal hatred of American imperialism by the European capitalists is thus indicated in an editorial appearing in “Il Tevere”, organ of the extreme Fascists in Rome:

“The Americans have their eyes full of figures, their ears are absorbed with the clicking of adding machines. They have a knife up their sleeves, namely, their most powerful dollar which can crush twenty Europeans. They live in a state of superb obliviousness which is astonishing and offensive . . .

“No! Things cannot go on thus. Americans are sowing to the right and left hatred and a desire for vengeance. The right to enslave a whole continent is not to be secured even on the battlefield, with risk of life. They must think it can be acquired behind the teller’s window of a bank, manipulating loans at so much per cent.

“We cannot foresee the future, but we can read in the hearts of the men of this old Europe, written in letters of blood condemnation of that certain slavery which has the dollar as its symbol.” (July 23, 1926.)

3. The attitude of British capitalism towards American imperialism is characterized by the Uncle Shylock compliment recently hurled at America by the London “Daily Mail”, and by such epithets as “the Pound of Flesh Attitude” and “usury”!	

4. America has gained 30% net in her world trade since 1913 as against a world deficit. But the European capitalists are busily at work trying to undermine this position at present maintained by the United States. Last year Europe had a deficit of 912 million dollars in her trade with the United States. That is, this was the favorable balance, of America in her European trade for the year. Because of this condition there are now sundry private and official European missions visiting different sections of the world, particularly the Latin-American countries, in order to take trade away from the American capitalists.

The continental trust movement is only an intensification and an extension of the recent efforts of the European capitalists to win back their pre-war markets. Mr. Felix Deutsch, general manager of the German General Electric Company, has asked for a syndication of all branches of similar industries in any one country; this to be followed by world trusts for the purpose of forming a commercial bloc in Europe against the United States.

Dr. Julius Klein, director of the United States Bureau of Domestic and Foreign Commerce, estimates the progress towards commercial coalition among countries in Europe against the United States in recording the adoption, since the war, of nearly one hundred commercial treaties with most favored nation clauses.

America’s reaction to this movement towards European trustification is not favorable. Mr. Bell, the American Commissioner in the International Chamber of Commerce has refused to give his approval to the Continental trust movement. He see in these trust centers of more effective opposition to the extension of American influence and control on the continent. Likewise, the authoritative American journal of business, the Commercial and Financial Chronicle, for October 16, 1926 (page 1923) thus views this trust movement:

“Generalizations aside, it is evident in the first place, that the organizers of the consortium intend to control, as far as possible, the European steel market, and that the combination is aimed, indirectly if not directly, at the steel producers of the United States.”

This is plain talk at an early date from a source which wields considerable power in the United States.

5. One need not spend much time emphasizing the pivotal role of the debt problem in European-American relations. The debt question is a source of friction which is irritating American domestic, as well as foreign politics. The decisive point in the whole matter is that perhaps more than around any other grievance does European capitalist opposition to America center itself on the debt question. Caillaux expresses European capitalist resentment only moderately when he declares:

“Unfortunately, it all resolves itself into a tribute paid by Europe, to America. It is essential that the debtors should take council together so as to distribute the burden in the least irksome way and to obtain from their creditor, the super-winner of the war, the concessions and conditions which both reason and justice demand.” (Caillaux: “Europe Must Unite or Die”, World’s Work, November, 1926, p. 39).

6. We should not forget that the Dawes’ Plan is far from stable and not accepted as a settled matter by many sections of European capitalism. It still breeds considerable hostility to American imperialist encroachments. Practically all that Germany pays in reparations now comes to America in payments on war debts. There is now going on in French and English financial circles, a serious discussion as to how soon the Dawes’ Plan will be doomed. We must remember that the real test of the Dawes’ Plan will come when Germany begins to make larger payments out of her own funds instead of out of what she has been paying so far, largely out of capital extended on credit to her by American financiers.

Mr. Edward Price Bell, the noted American correspondent, tells us that the British industrialists are more and more viewing the Dawes’ Plan with an attitude bordering on consternation. These capitalist interests see in the possible capacity of Germany to meet all the terms of the Dawes’ Plan. their ultimate undoing. They figure that if in the first normal Dawes year, and for thirty years after, Germany should be able to pay two and one-half billion gold marks annually, then, German industry will be so highly developed as to wipe out British competition completely.

Naturally, the key to the whole question is to be found in the relations between England and the United States. The increasing sources of conflict between these two imperialist powers are too well known to need enumeration or repetition at this point. I do want to emphasise, however, the fact that we must avoid the tendency to exaggerate the tempo of the decline if British imperialism. For the last fiscal year, prior to the general strike and the coal strike, Great Britain regained first place in world trade.

British imperialism knows and hates the fact that for at least three generations England will have to pay to the United States $500,000 daily. This means that seventy-six million days’ labor each year by British working men for the next sixty years are to be put into paying Britain’s debts to Uncle Shylock.

American imperialism looks upon Great Britain as the real enemy, as the primary obstacle to a complete, satisfactory settlement of the debt question with the European countries. The Balfour Declaration of 1922 in which it was stated that England would expect just as much and no more payment from its debtors than it had to pay to the United States is looked upon by the American capitalists as the real beginning of a possible European compact against the United States. The United States is a creditor nation. England is both a debtor and creditor nation. Consequently England can play a double game in this debt question. The American bourgeoisie are well aware of this role, played by the British imperialists. They are reacting to this sharply.

For instance, it is no accident that the American bankers have invested more than one hundred million dollars in developing Italian water power resources. Italy has been a great market for British coal. Here is a way of hitting at Britain very profitably. But England strikes back through Poland. The report for the improvement of Poland’s fiscal condition prepared by the American economist, Professor Kemmerer of Princeton University, is adopted by the Pilsudski cabinet. Professor Kemmerer then leaves, feeling secure that from now on American capitalist interests will have it easier to establish their hegemony in Poland. Then that member of the Pilsudski cabinet who has been most aggressive in his support for the Kemmerer report is dismissed. Uncle Shylock sees the hand of John Bull. There is a tug of war between Wall Street and bombard Street. Control of Polish resources is the stake.

The sharp rebuff given by the American financiers to the Thoiry proposals was, in the last resort, an attempt by Wall Street to blackjack British interests who are held to be responsible—the power behind the scenes—for the failure of France to come to a settlement of its debt with the United States.

America is pursuing a similar policy in the Far East.	There is no doubt that if it had not been for the failure of the powers to secure American cooperation, intervention by the imperialist forces in China would have already taken place. Witness the American report on the Shanghai massacre. In this report the American investigations held the foreign regime responsible for the shootings of the Chinese workers and students. Again, note the fact that at the moment when Wu Pei Fu was hardest pressed for funds with which to pay armies—for months unpaid—the American government sent a note to the Peking government reminding it that it owes Americans forty to fifty million dollars in payment of loans and materials supplied it by American agencies. Therefore, the American government entered strong objections to the proposals then put forward by Wu Pei Fu’s Minister of Finance to secure a new domestic loan by allocating to its service part of the salt and customs surpluses. Wall Street insisted on all surplus available being used to pay debts long due.

This policy of the United States was naturally followed by Great Britain and Japan because they would not have it that the United States alone should collect. This concerted demand, initiated and forced by the American interests, had a disastrous effect on Wu Pei Fu’s plans and likely was a force making for the disintegration of Wu’s armies. It must not be forgotten that Wu Pei-fu has been the British military white-hope in China until the recent English flirtations and arrangements with Chang Tso-lin.

The allied imperialist armada rushed to Chinese waters to stem the tides of Chinese revolutionary nationalism has been placed under the command of the United States admiral C. S. Williams not for formal reasons of his high naval rank, but mainly to draw America into the conflict against tire Chinese masses as quickly, as definitely and aggressively as possible.

In fact there is developing a totally new reaction to Europe in the United States. The American imperialists are feverishly at work at trying to facilitate the advance of militarism in the United States by means of exaggerating the possible dangers to Yankee imperialist supremacy from European resistance, by working up a scare amongst the masses as to all kinds of phantom hostilities being planned against the United States by European powers.

The House of Representatives has just overwhelmingly repudiated the so-called economy pleas of Coolidge and voted increased military appropriations.

There is likewise developing a new literature on American foreign affairs. This literature no longer talks of peace. It speaks of the need for preparing for a new war. The speech delivered by President Coolidge on Armistice Day was a warning to European capitalism not to cast jealous glances at American prosperity. It served notice upon Europe that America is prepared to mobilze all its resources in men and money for defense of its imperialist hegemony. No wonder the Paris “Temps” thus said in despair in commenting on this speech:

“We are forced to wonder whether permanent and active co-operation between Europe and America in the same work of international solidarity is feasible in the present situation of affairs . . . It is a view of the situation devoid of all idealism, which subordinates the whole of American politics to the question of money.”

Some of America’s most recent books on foreign affairs even speak of the mistake the United States made in entering the war. We find typical of these new winds in American foreign policy the following:

“It is time that our people have their minds recalled to the truth as to how we were beguiled into the war, how much our allies appropriated to themselves, how they deceived us, and how they at last have become dangerous to this country which befriended them . . . . 

“It is very plain what was at the bottom of the late war. Peace may remain but PEACE WILL DEPEND UPON OUR POWER. We are as much the enemies of Great Britain in world pre-eminence and in that trade by which she lives as were the four great European powers whom she has successively humbled.” (Author’s emphasis) (Bausman: Facing Europe).

A more bitter note was struck in the American magazine, “The Independent,” of August 7th, 1926, when it said:

“Every blast of the debt sirocco convinces thousands that the war was a ghastly blunder for Europe and a bootless adventure for America. There is a growing feeling that the United States should have held out and forced an early settlement . . . Every year makes American participation seem a greater folly.”

And J. Shatford, chairman of the American Railroad Security Owners’ Association, in the May 22nd, 1926, issue of the Magazine of Wall Street openly declared:


“I look for the greatest economic war in history to develop and rage within the next few years. What the result will be on our business can readily be visualized.”

The whole situation is thus summed up by Paul Scott Mowrer in a recent issue of the Chicago Daily News:


“That if in a few years, Europe is still struggling and staggering tipsily under the burden of excessive debts entailing annual tribute to us of hundreds of millions of dollars a situation so intolerable will arise that only war can result.”
 
This is the viewpoint given him by a prominent American diplomat. From this viewpoint it is clear why the American Ambassador Houghton in his speech at the Pilgrims Dinner, delivered in London, last May, declared that the United States would not lend any money to Europe for expenditures
on armaments. 

Because of the growing European resistance to American imperialist advance some have developed a notion of the rise of a United States of capitalist Europe pitted against the United States of America. This view, developed for example by Comrade Treint of the Communist Party of France, is erroneous. It completely overlooks the conflict of interests among the European powers. It tends to mistake a possible temporary military coalition of a section of European capitalist nations against the United States for a permanent, stable sort of capitalist super-government for all Europe.

We can properly estimate the present trend of European-American relations in this fashion:

1. America is not putting Europe on rations. He who maintains such a viewpoint exaggerates the weakness of European capitalism and overestimates the strength of American imperialism.

2. Europe is increasing its resistance to American encroachment on its resources but there is no reason to conclude that there is being formed or can be formed a United States of Capitalist Europe against America.

3. America is skillfully utilizing the European economic revival, the continental trustification movement, the increasing competition it is meeting in certain markets from European capitalist groups for the purpose of psychologizing the American masses so as to make them more susceptible to war propaganda.

4. At the same time the Washington government is busily at work improving its chemical, aerial, naval and military engines and devices of warfare. On short notice the American bourgeoisie feel that they can meet successfully in a combat any group of capitalist countries. While the United States still uses peaceful words in its international relations, especially in its European relations, it is actually working overtime to be prepared to strike and strike hard as soon as necessary.
 
Jay Lovestone ArchiveThe Communist Index