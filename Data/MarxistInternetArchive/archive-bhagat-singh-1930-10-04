Bhagat SinghDate  :  October 4, 
1930
Transcription/Source  :  www.shahidbhagatsingh.org

HTML Markup  :  Salil Sen for MIA, December, 
2007
Copyright  :  © Shahidbhagatsingh.org. Published 
on MIA with the permission of Shahidbhagatsingh.org and Shahid Bhagat Singh 
Research Committee.  [When the case was in its final stage, Sardar Kishan 
Singh (Bhagat Singh's father) made a written request to the Tribunal, saying 
that there were many facts to prove that his son was innocent and that he had 
nothing to do with Sounder's murder. He also requested that his son be given an 
opportunity to prove his innocence. When Bhagat Singh came to know of it he was 
very angry, and wrote this strong letter to his father, protesting against his 
move.]  Oct. 4, 1930MY DEAR FATHER, I was astounded to learn that you had submitted a petition to the 
members of the Special Tribunal in connection with my defence. This intelligence 
proved to be too severe a blow to be borne with equanimity. It has upset the 
whole equilibrium of my mind. I have not been able to understand how you could 
think it proper to submit such a petition at this stage and in these 
circumstances. Inspite of all the sentiments and feelings of a father, I don't 
think you were at all entitled to make such a move on my behalf without even 
consulting me. You know that in the political field my views have always 
differed with those of yours. I have always been acting independently without 
having cared for your approval or disapproval. I hope you can recall to yourself that since the very beginning you have been 
trying to convince me to fight my case very seriously and to defend myself 
properly. But you also know that I was always opposed to it. I never had any 
desire to defend myself and never did I seriously think about it. Whether it was 
a mere vague ideology or that I had certain arguments to justify my position, is 
a different question and that cannot be discussed here. You know that we have been pursuing a definite policy in this trial. Every 
action of mine ought to have been consistent with that policy, my principle and 
my programme. At present the circumstances are altogether different, but had the 
situation been otherwise, even then I would have been the last man to offer 
defence. I had only one idea before me throughout the trial, i.e. to show 
complete indifference towards the trial inspite of serious nature of the charges 
against us. I have always been of opinion that all the political workers should 
be indifferent and should never bother about the legal fight in the law courts 
and should boldly bear the heaviest possible sentences inflicted upon them. They 
may defend themselves but always from purely political considerations and never 
from a personal point of view. Our policy in this trial has always been 
consistent with this principle; whether we were successful in that or not is not 
for me to judge. We have always been doing our duty quite disinterstedly. In the statement accompanying the text of Lahore Conspiracy Case Ordinance 
the Viceroy had stated that the accused in this case were trying to bring both 
law and justice into contempt. The situation afforded us an opportunity to show 
to the public whether we ere trying to bring law into contempt or whether others 
were doing so. People might disagree with us on this point. You might be one of 
them. But that never meant that such moves should be made on my behalf without 
my consent or even my knowledge. My life is not so precious, at least to me, as 
you may probably think it to be. It is not at all worth buying at the cost of my 
principles. There are other comrades of mine whose case is as serious as that of 
mine. We had adopted a common policy and we shall stand to the last, no matter 
how dearly we have to pay individually for it. Father, I am quite perplexed. I fear I might overlook the ordinary principle 
of etiquette and my language may become a little but harsh while criticizing or 
rather censoring this move on your part. Let me be candid. I feel as though I 
have been stabbed at the back. Had any other person done it, I would have 
considered it to be nothing short o treachery. But in your case, let me say that 
it has been a weakness - a weakness of the worst type. This was the time where everybody's mettle was being tested. Let me say, 
father, you have failed. I know you are as sincere a patriot as one can be. I 
know you are as sincere a patriot as one can be. I know you have devoted your 
life to the cause of Indian independence, but why, at this moment, have you 
displayed such a weakness? I cannot understand. In the end, I would like to inform you and my other friends and all the 
people interested in my case, that I have not approved of your move. I am still 
not at all in favour of offering any defence. Even if the court had accepted 
that petition submitted by some of my co-accused regarding defence, etc., I 
would have not defended myself. My applications submitted to the Tribunal 
regarding my interview during the hunger strike, were misinterpreted and it was 
published in the press that I was going to offer defence, though in reality I 
was never willing to offer any defence. I still hold the same opinion as before. 
My friends in the Borstal Jail will be taking it as a treachery and betrayal on 
my part. I shall not even get an opportunity to clear my position before them.
I want that public should know all the details about this complication, and, 
therefore, I request you to publish this letter. Your loving son 
Bhagat Singh  