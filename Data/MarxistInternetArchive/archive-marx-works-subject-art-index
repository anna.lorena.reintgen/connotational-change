Marx-Engels Subject Archive
Source: Marx Engels On Literature and Art. Progress Publishers. 1976;
Transcribed: by Andy Blunden.PrefaceSocial Being and Social Consciousness1. Preface to the Contribution to the Critique of Political Economy
2. The German IdeologyNatural Conditions and Development of Culture
LandscapesAgainst Vulgarisation of Historical Materialism1. Engels to Joseph Bloch. September 21-22 1890
2. Engels to W. Borgius. January 25 1894
3. Engels to Conrad Schmidt. October 27 1890
4. Engels to Joseph Bloch. September 21-22 1890
5. Engels to Conrad Schmidt. August 5 1890Engels About Mehring’s The Lessing Legend1. Engels to Franz Mehring. April 11 1893
2. Engels to Karl Kautsky. June 1 1893
3.  Engels to Franz Mehring. July 14 1893Class Relations and Class Ideology1. The German Ideology
2. The Communist ManifestoScientific and Vulgar Conceptions of Class IdeologyEngels to Paul ErnstHistorical Continuity and Its Contradictions1. The German Ideology
2. Eighteenth Brumaire of Louis BonaparteUneven Character of Historical Development and Questions of ArtIntroduction to the Economic Manuscripts of 1857-58Ideological Content and Realism1. Engels to Minna Kautsky, 26 November 1885
2. Engels to Margaret Harkness, beginning April 1888
3. Review of A Chenu, Les Conspirateurs and L. de la Hodde, La Renaissance de la République, Feb 1848The Tragic and the Comic in Real History1. Critique of Hegel’s Philosophy of Right. Introduction
2. Eighteenth Brumaire of Louis Bonaparte
3. Leading article in Kölnische Zeitung No. 179
4. Engels to Marx, 4 September 1870
5. Engels to August Bebel, 7 July 1892Problems of Revolutionary TragedyOn Ferdinand Lassalle’s Drama Franz von Sickingen1. Marx to Ferdinand Lassalle, 19 April 1859
2. Engels to Ferdinand Lassalle, 18 May 1859Miscellaneous ItemsLanguage and Literature1. Ideas do not exist separately from language, from Grundrisse
2. Materials on the History of France and Germany, EngelsImprovisation and Poetry
New-York Daily Tribune, 7 March 1853, Marx On Literary Style1. On Proudhon, Letter to J B Schweizer, 24 January 1865
2. Marx To Engels, 31 July 1865
3. Engels to Eduard Bernstein, 12-13 July 1883
4. Engels to Sorge, 29 April 1886On Literary Polemics1. On Brentano’s Polemic Against Marx over Alleged Misquotation
2. Refugee Literature, IV
3. Engels to Marx, 25-26 October 1847
4. Engels to Eduard Bernstein, 12 March 1881
5. Engels to Eduard Bernstein, 29 June 1884On Translation1. Engels To Marx, 23 September 1852
2. Engels To Marx, 29 November 1873
3. Engels To Friedrich Sorge, 29 June 1883
4. Engels To Eduard Bernstein, 5 February 1884
5. How Not to Translate Marx, Engels, The Commonweal, 1885
6. Engels To Laura Lafargue, 16 November 1889
7. Engels To Laura Lafargue, 8 January 1890Additional References On Translation of Marx’s works

8. Engels To Marx, 24 June 1867
9. Engels To Sorge, 20 June 1882
10. Engels To August Bebel, 18 August 1886
11. Engels To Laura Lafargue, 13 September 1886
12. Engels To Laura Lafargue, 28 April 1886
13. Engels To Sorge, 29 June 1888
The Origin of ArtHistorical Development of the Artistic Sense1. Private Property and Communism, 1844
2. The Division of Labour and Human Needs, 1844
3. Private Property and Communism, 1844The Role of Labour in the Origin of Art, from Part Played by Labour
Artistic Creation and Aesthetic Perception, from Critique Political EconomySocial Division of LabourDivision of Labour and Social Consciousness
Estrangement of Labour and Condition of Workers in Capitalist SocietyMoney and World CultureThe Distorting Power of MoneyCapitalism and Spiritual ProductionRelation of Art to Capitalist Mode of Production, Theories of Surplus Value
Bourgeois Taste and Its Evolution, Engels To Laura Lafargue, 14 January 1884The Work of the Artist in Capitalist Society1. Theories of Surplus Value, Chapter 4
2. Productive Labour, Economic writings of 1864
3. Theories of Surplus Value, AddendaFreedom of the Press and of Artistic Creation1. Debates on Freedom of the Press, Marx 1842
2. Debates on Freedom of the Press, Marx 1842
3. Debates on Freedom of the Press, Marx 1842
4. Debates on Freedom of the Press, Marx 1842
5. Stamp Duty on Newspapers, Neue Oder Zeitung, 30 March 1855Asceticism and Enjoyment, from German Ideology
Work and Play, from Capital, Volume I
Bourgeois Civilisation and Crime, from Theories of Surplus Value, AddendumHistorical Mission of the Working ClassThe Proletariat and Wealth, from The Holy FamilyThe Working Class and the Progressive Development of Society1. Speech at Anniversary of The People’s Paper, Marx
2. The Housing Question, EngelsThe Working Class and Culture1. Letters from London, Engels
2. The Condition of England, Engels
3. Letters from London, Engels
4. The Condition of the Working Class in England, Engels
5. Marginal Notes on “The King of Prussia.”, Marx
6. Note on Eccarius’ article on Tailoring in London, MarxProletarian Revolution and the Vandalism of the Bourgeoisie1. The Civil War in France
2. Notes from NewspapersCriticism of Egalitarian Communism1. Private Property and Communism, 1844
2. The Peasant War in Germany, Engels 1850Individuality and Society1. From “Saint Max,” German Ideology
2. From “Saint Max,” German Ideology
3. From “Proletarians and Communism,” German IdeologyThe Kingdom of Freedom and Material Labour1. From The Grundrisse, Marx 1857
2. From Capital Volume IIIAntiquityThe Dawn of Human Culture, from Early German History, Engels
Beginnings of Mythology, Conspectus of Morgan’s Ancient History, Marx
Epic Tradition of the Semites, Engels to Marx. approx. 26 May 1853Ancient Greek Society in Homer’s Poems1. Origins of the Family, Chapter 1, Engels 1884
2. Origins of the Family, Chapter 4, Engels 1884Greek Tragedy1. Origins of the Family, Preface, Engels 1884
2. Conspectus of Morgan’s Ancient History, MarxPosition of Women in Greece, from  Origins of the Family, Chapter 2Ancient Slavery and World Culture1. Anti-Dühring, (Theory of Force) Engels
2. Greetings to Socialists in Sicily, 26 September 1894, EngelsThe Plastic Element in Greek Art1. Marx’s Doctoral Dissertation
2. Notebooks on Epicurean Philosophy, Marx 1839Greek Enlightenment1. Notebooks on Epicurean Philosophy, Marx 1839
2. Marx’s Doctoral DissertationReligion and Culture in the Ancient World, Marx 1842
Decline of the Ancient World, from Notebooks on Epicurean PhilosophyLucretius Carus1. Notebooks on Epicurean Philosophy
2. Notebooks on Epicurean Philosophy
Horace, Engels to Marx, 21 December 1866
Persius’ Satire, from Bruno Bauer and Early Christianity
Lucian, from History of Early Christianity, Engels 1894Middle AgesGermanic Culture, Early German History, Engels
Love in Literature of Antiquity & Middle Ages, from  Origins of the Family
Wagner and Germanic Epos, from  Origins of the Family, Chapter 2
Legend of Siegfried and the German Revolutionary Movement, Engels 1840Ancient Irish Literature1. History of Ireland, Engels 1870
2. History of Ireland, Engels 1870
3. Manuscripts on the History of England and Ireland, Engels 1870
4. Notes for the preface to a collection of Irish Songs, Engels July 1870Ancient Scandinavian Epos, History of Ireland, Engels 1870
Early Medieval Danish Poetry, Engels to Marx, June 20 1860
The Chanson de Roland, On the History of France and Germany, EngelsProvençal Literature1. On the Polish Question, Marx 1848
2. Chronological Extracts, from Schlosser’s World HistoryChivalrous Love Poetry, from Origins of the Family, Chapter 2
Peasant Equalitarian Ideas in England, from Schlosser’s World History
German Volksbücher, Engels 1848RenaissanceEnd of the Ancient World to End of the Middle Ages, Dialectics of NatureItalian Culture from Dante to Garibaldi1. Preface to Capital Volume III
2. Affairs in Prussia, Marx in New-York Tribune, 15 October 1860Dante, Preface to Italian Edition of the Communist Manifesto, Engels 1893
Petrarch, Wanderings in Lombardy, Over the Alps, Engels 1893
Boccaccio, The Peasant War in Germany, Engels 1850
Great Renaissance, from Dialectics of Nature, Engels 1883
Titian, Engels to Marx, 20 May 1857
Grobian Literature of the Reformation Period, from Moralising Criticism and Critical Morality, Marx 1847
Significance of Reformation, On History of France and Germany, EngelsThomas More1. Capital, Volume I, Chapter 27
2. Capital, Volume I, Chapter 28
3. Preparatory materials for Anti-Dühring
4. Foreign Policy of Russian Tsardom, Engels 1890Shakespeare1. War Debate in Parliament, New-York Tribune, 17 March 1854
2. Engels To Marx, 10 December 1873
3. from The German Ideology
4. Marx To Engels, 27 February 1861Calderón1. Revolutionary Spain, Marx 1854
2. Marx To Engels, 3 May 1854
3. Herr Vogt, Marx 1860Cervantes1. The New Martial Law Charter, Marx 1849
2. Capital, Volume I, Chapter 1Three Unities of Classical Drama, Marx to Lassalle, 22 July 1861
La Rochefoucauld, Marx To Engels. 26 June 1869
Historic Significance of the Enlightenment, Anti-DühringThe Materialism of the Encyclopedists1. Socialism: Utopian & Scientific
2. The Holy FamilyThe Enlightenment and Dialectics, Anti-Dühring
Utilitarian Philosophy of the Enlightenment from The German IdeologyVoltaire1. Marx to Ludwig Kugelmann. 12 October 1868
2. What have the Working Classes to do with Poland?, Engels, 1866Diderot1. Ludwig Feuerbach
2. Marx to Engels 15 April 1869
3. Engels To Marx. 16 April 1869Rousseau1. from Anti-Dühring
2. On Proudhon, Letter to J B Schweizer, 24 January 1865Sentimentalism as a Reaction to the Enlightenment, Moralising Criticism, Marx 1847
Crisis of Enlightenment Ideals, Socialism: Utopian & Scientific
From the Enlightenment to Romanticism, Marx To Engels. 25 March 1868
Criticism of Progress from the point of view of the Past, Communist Manifesto
Petty-Bourgeois Criticism of Capitalism, Communist Manifesto
Restoration Writers, On the Polish QuestionFrench LiteratureAbbé Prévost from Dialectics of Nature
Chateaubriand
1. Marx To Engels. 26 October 1854
2. Marx To Engels. 30 November 1873Alexandre Dumas. Marx To Engels. 23 February 1851
Lamartine
1. Marx To Engels. 18 March 1848
2. Neue Rheinische Zeitung. 22 October 1848
3. Democratic Panslavism, Engels 1849Victor Hugo
1. Eighteenth Brumaire of Louis Bonaparte, Preface
2. Engels To Lafargue. 5 December 1892
3. Engels To Marx. 13 September 1870
4. Notes on the War, Pall Mall Gazette, 13 October 1870Eugène Sue, Continental Movements, Engels
From Critical Analysis of Sue’s Mystères de Parts
1. The Holy Family
2. The Holy Family
3. The Holy FamilyBalzac1. Capital, Volume I
2. Capital, Volume III
3. Engels To Laura Lafargue. 13 December 1883
4. Engels To Marx. 4 October 1852
5. Marx To Engels. 25 February 1867Pierre Dupont, Capital, Volume I
Arthur Ranc,  Engels To Marx. 22 October 1869
Renan
1. Marx To Engels. 20 January 1864
2. Engels To Victor Adler. 19 August 1892Zola. Engels To Laura Lafargue. 15 June 1887
Maupassant. Engels To Laura Lafargue. 2 February 1887English and Irish LiteratureDaniel Defoe1. Engels To Karl Kautsky. 20 September 1884
2. Capital, Volume ISwift. Manuscripts on the History of England and Ireland, Engels 1870
Pope, Herr Vogt, Marx 1860
Shelley and Byron, Edward Aveling and Eleanor Marx-Aveling, Neue Zeit, 1888
Walter Scott.  Origins of the Family, Chapter 1, Engels 1884
William Cobett’s Social Writings. Marx, New-York Tribune, 22 July 1853
English Working-Class Poet Mead.  Condition of the Working Class, Engels
Thomas Carlyle1. The Condition of England, Engels 1844
2. Latter-Day Pamphlets edited by Thomas Carlyle, Marx and Engels 
English Realists of the Mid-Nineteenth Century, The English Middle Class, Marx. New-York Tribune, 1 August 1854
Carleton.  Marx To Engels. 14 August 1879
Bernard Shaw. Engels To Kautsky. 4 September 1892
Aveling1. Engels To Paul Lafargue. 17 May 1889
2. Engels To Konrad Schmidt. 9 December 1889William Morris. Engels To Laura Lafargue. 23 November 1884German LiteratureGerman Culture from the Mid-17th to the Early 19th Century1. Notes on Germany, Engels
2. The State of Germany, Engels in Northern Star, 25 October 1845Schiller. Shortcomings of His Poetry, from Ludwig Feuerbach, Chapter 2, 1886
Goethe1. Ludwig Feuerbach, Chapter 1, 1886
2. Anti-Dühring, 1877
3. Ludwig Feuerbach, Chapter 1, 1886
4. The Condition of England, Engels
5. Engels To Marx. 15 January 1847
6. Karl Grün, German Socialism in Verse and Prose, Engels 1847Heine1. Ludwig Feuerbach, Chapter 1, 1886
2. Rapid Progress of Communism in Germany, Engels 1844
3. Capital, Volume III, Supplement
4. Marx Heinrich Heine. 12 January 1845
5. Marx Heinrich Heine. 5 April 1846
6. Engels To Marx. 14 January 1848“Young Germany.” Revolution and Counter-Revolution in Germany, 1852
Alexander Jung.  “Lectures on Modern German Literature”, Engels 1842
German, or “True,” Socialism
1. The Communist Manifesto
2. The German IdeologyKarl Beck, the Poetry of True Socialism1. German Socialism in Verse and Prose
2.  German Socialism in Verse and Prose
3.  German Socialism in Verse and Prose
4.  German Socialism in Verse and ProseGottfried Kinkel1. Heroes of the Exile
2. Heroes of the ExileFreiligrath1. Reappearance of Neue Rheinische Zeitung
2. The True Socialists, Engels 1847
3. Marx To Ferdinand Freiligrath. 27 December 1851
4. Marx To Joseph Weydemeyer. 16 January 1852
5. Engels To Jenny Marx. 22 December 1859
6. Marx To Engels. 17 July 1869
7. Marx To Engels. 22 August 1870
8. Marx To Engels. 2 September 1870
9. Marx To Sigfrid Meyer. 21 January 1871Weerth. “Song of the Apprentices”, Engels 1883Johann Philipp Becker1. Engels to Bebel. 8 October 1886
2. Johann Philipp Becker, Engels 1886
3. Marx To Johann Philipp Becker. 9 April 1860Henryk Ibsen. Engels To Paul ErnstRussian LiteratureThe Russian Language1. Marx To Sigfrid Meyer. 21 January 1871
2. Refugee Literature, Engels 1874
3. Engels To Vera Zasulich. 6 March 1884The Lay of Igor’s Host:  Marx To Engels. 5 March 1856
Lomonosov. Slavic Languages and Philology. Notes on Lomonsov, Engels
Derzhavin. The War on the Danube. Engels in New-York Tribune, 25 July 1854
Pushkin. Engels To Nikolai Danielson. 29-31 October 1891Chernyshevsky and Dobrolyubov1. Refugee Literature, Engels 1874
2. Engels To Eugenie Papritz. 26 June 1884
3. Marx To Nikolai Danielson. 9 November 1871Chernyshevsky as a Scholar and Critic. Capital Afterword to 2nd German Edition
Chernyshevsky and the Russian Village Commune1. Marx to the Editorial Board of Otechestvenniye Zapiski
2. Afterword to On Social Relations in Russia, EngelsChernyshevsky as a Revolutionary1. The Alliance of Socialist Democracy and the International, 1872
2. Marx To Engels. 5 July 1870
3. Marx To Nikolai Danielson. 12 December 1872
4. Marx To Nikolai Danielson. 18 January 1873
5. Engels To Nikolai Danielson. 10 June 1890Shchedrin. Engels To Nikolai Danielson. 19 February 1887
Flerovsky.  Marx To Engels. 10 February 1870Herr Tidmann. Engels To Marx. 27 January 1865
“Herr Tidmann”. Old Danish Folk Song. Engels 1864
Conspectus of Kostomarov’s Revolt of Stekna Razin, Marx
The Vicar of Bray, Engels August 1882
German Revolutionary Songs. Engels To Hermann Schlüter. 15 May 1885
Weavers’ Song.  Marginal Notes on “The King of Prussia.”, MarxSongs of the German Revolution of 18481. The Role of Force in History, Engels 1887
2. Campaign for the Imperial Constitution, Engels 1850Satirical Folk Song Against Bonaparte. Engels To Marx. 7 February 1856Confessions of Marx and Engels
Karl Marx. Confessions
Frederick Engels. ConfessionsFrom Reminiscences of MarxPaul Lafargue1. Literature
2. Novels
3. FamilyEleanor Marx-Aveling. Karl Marx
Franzisca Kugelmann. Small Traits of Marx’s Great Character
Anselmo Lorenzo. Reminiscences of the First InternationalFrom Reminiscences of EngelsN. S. Rusanov. My Acquaintance with Engels
Fanni Kravchinskaya. ReminiscencesCritical Articles by Jenny Marx
1. From London’s Theatre World
2. The London Season
3. Shakespearean Studies in England
4. Shakespeare’s Richard III in London’s Lyceum Theatre
5. From the London Theatre 
Early Literary Experiments of Marx and Engels
Abstracts not included in Collected Works
Marxist Literary Criticism |
Marx/Engels Subject Archive



Marx and Engels On Literature and Art


Source: Marx Engels On Literature and Art. Progress Publishers. 1976;
Transcribed: by Andy Blunden.

Preface
Materialist Conception of the History of Culture
Social Being and Social Consciousness
1. Preface to the Contribution to the Critique of Political Economy
2. The German Ideology
Natural Conditions and Development of Culture
Landscapes
Against Vulgarisation of Historical Materialism
1. Engels to Joseph Bloch. September 21-22 1890
2. Engels to W. Borgius. January 25 1894
3. Engels to Conrad Schmidt. October 27 1890
4. Engels to Joseph Bloch. September 21-22 1890
5. Engels to Conrad Schmidt. August 5 1890
Engels About Mehring’s The Lessing Legend
1. Engels to Franz Mehring. April 11 1893
2. Engels to Karl Kautsky. June 1 1893
3.  Engels to Franz Mehring. July 14 1893
Class Relations and Class Ideology
1. The German Ideology
2. The Communist Manifesto
Scientific and Vulgar Conceptions of Class Ideology
Engels to Paul Ernst
Historical Continuity and Its Contradictions
1. The German Ideology
2. Eighteenth Brumaire of Louis Bonaparte
Uneven Character of Historical Development and Questions of Art
Introduction to the Economic Manuscripts of 1857-58
General Problems of Art
Ideological Content and Realism
1. Engels to Minna Kautsky, 26 November 1885
2. Engels to Margaret Harkness, beginning April 1888
3. Review of A Chenu, Les Conspirateurs and L. de la Hodde, La Renaissance de la République, Feb 1848
The Tragic and the Comic in Real History
1. Critique of Hegel’s Philosophy of Right. Introduction
2. Eighteenth Brumaire of Louis Bonaparte
3. Leading article in Kölnische Zeitung No. 179
4. Engels to Marx, 4 September 1870
5. Engels to August Bebel, 7 July 1892
Problems of Revolutionary Tragedy
On Ferdinand Lassalle’s Drama Franz von Sickingen
1. Marx to Ferdinand Lassalle, 19 April 1859
2. Engels to Ferdinand Lassalle, 18 May 1859
Miscellaneous Items
Language and Literature
1. Ideas do not exist separately from language, from Grundrisse
2. Materials on the History of France and Germany, Engels
Improvisation and Poetry
New-York Daily Tribune, 7 March 1853, Marx 
On Literary Style
1. On Proudhon, Letter to J B Schweizer, 24 January 1865
2. Marx To Engels, 31 July 1865
3. Engels to Eduard Bernstein, 12-13 July 1883
4. Engels to Sorge, 29 April 1886
On Literary Polemics
1. On Brentano’s Polemic Against Marx over Alleged Misquotation
2. Refugee Literature, IV
3. Engels to Marx, 25-26 October 1847
4. Engels to Eduard Bernstein, 12 March 1881
5. Engels to Eduard Bernstein, 29 June 1884
On Translation
1. Engels To Marx, 23 September 1852
2. Engels To Marx, 29 November 1873
3. Engels To Friedrich Sorge, 29 June 1883
4. Engels To Eduard Bernstein, 5 February 1884
5. How Not to Translate Marx, Engels, The Commonweal, 1885
6. Engels To Laura Lafargue, 16 November 1889
7. Engels To Laura Lafargue, 8 January 1890
Additional References On Translation of Marx’s works

8. Engels To Marx, 24 June 1867
9. Engels To Sorge, 20 June 1882
10. Engels To August Bebel, 18 August 1886
11. Engels To Laura Lafargue, 13 September 1886
12. Engels To Laura Lafargue, 28 April 1886
13. Engels To Sorge, 29 June 1888

Art in Class Society
The Origin of Art
Historical Development of the Artistic Sense
1. Private Property and Communism, 1844
2. The Division of Labour and Human Needs, 1844
3. Private Property and Communism, 1844
The Role of Labour in the Origin of Art, from Part Played by Labour
Artistic Creation and Aesthetic Perception, from Critique Political Economy
Social Division of Labour
Division of Labour and Social Consciousness
Estrangement of Labour and Condition of Workers in Capitalist Society
Money and World Culture
The Distorting Power of Money
Capitalism and Spiritual Production
Relation of Art to Capitalist Mode of Production, Theories of Surplus Value
Bourgeois Taste and Its Evolution, Engels To Laura Lafargue, 14 January 1884
The Work of the Artist in Capitalist Society
1. Theories of Surplus Value, Chapter 4
2. Productive Labour, Economic writings of 1864
3. Theories of Surplus Value, Addenda
Freedom of the Press and of Artistic Creation
1. Debates on Freedom of the Press, Marx 1842
2. Debates on Freedom of the Press, Marx 1842
3. Debates on Freedom of the Press, Marx 1842
4. Debates on Freedom of the Press, Marx 1842
5. Stamp Duty on Newspapers, Neue Oder Zeitung, 30 March 1855
Asceticism and Enjoyment, from German Ideology
Work and Play, from Capital, Volume I
Bourgeois Civilisation and Crime, from Theories of Surplus Value, Addendum
Historical Mission of the Working Class
The Proletariat and Wealth, from The Holy Family
The Working Class and the Progressive Development of Society
1. Speech at Anniversary of The People’s Paper, Marx
2. The Housing Question, Engels
The Working Class and Culture
1. Letters from London, Engels
2. The Condition of England, Engels
3. Letters from London, Engels
4. The Condition of the Working Class in England, Engels
5. Marginal Notes on “The King of Prussia.”, Marx
6. Note on Eccarius’ article on Tailoring in London, Marx
Proletarian Revolution and the Vandalism of the Bourgeoisie
1. The Civil War in France
2. Notes from Newspapers
Art and Communism
Criticism of Egalitarian Communism
1. Private Property and Communism, 1844
2. The Peasant War in Germany, Engels 1850
Individuality and Society
1. From “Saint Max,” German Ideology
2. From “Saint Max,” German Ideology
3. From “Proletarians and Communism,” German Ideology
The Kingdom of Freedom and Material Labour
1. From The Grundrisse, Marx 1857
2. From Capital Volume III
History of Social Thought, Literature and Art Antiquity, Middle Ages and Renaissance
Antiquity
The Dawn of Human Culture, from Early German History, Engels
Beginnings of Mythology, Conspectus of Morgan’s Ancient History, Marx
Epic Tradition of the Semites, Engels to Marx. approx. 26 May 1853
Ancient Greek Society in Homer’s Poems
1. Origins of the Family, Chapter 1, Engels 1884
2. Origins of the Family, Chapter 4, Engels 1884
Greek Tragedy
1. Origins of the Family, Preface, Engels 1884
2. Conspectus of Morgan’s Ancient History, Marx
Position of Women in Greece, from  Origins of the Family, Chapter 2
Ancient Slavery and World Culture
1. Anti-Dühring, (Theory of Force) Engels
2. Greetings to Socialists in Sicily, 26 September 1894, Engels
The Plastic Element in Greek Art
1. Marx’s Doctoral Dissertation
2. Notebooks on Epicurean Philosophy, Marx 1839
Greek Enlightenment
1. Notebooks on Epicurean Philosophy, Marx 1839
2. Marx’s Doctoral Dissertation
Religion and Culture in the Ancient World, Marx 1842
Decline of the Ancient World, from Notebooks on Epicurean Philosophy
Lucretius Carus
1. Notebooks on Epicurean Philosophy
2. Notebooks on Epicurean Philosophy
Horace, Engels to Marx, 21 December 1866
Persius’ Satire, from Bruno Bauer and Early Christianity
Lucian, from History of Early Christianity, Engels 1894
Middle Ages
Germanic Culture, Early German History, Engels
Love in Literature of Antiquity & Middle Ages, from  Origins of the Family
Wagner and Germanic Epos, from  Origins of the Family, Chapter 2
Legend of Siegfried and the German Revolutionary Movement, Engels 1840
Ancient Irish Literature
1. History of Ireland, Engels 1870
2. History of Ireland, Engels 1870
3. Manuscripts on the History of England and Ireland, Engels 1870
4. Notes for the preface to a collection of Irish Songs, Engels July 1870
Ancient Scandinavian Epos, History of Ireland, Engels 1870
Early Medieval Danish Poetry, Engels to Marx, June 20 1860
The Chanson de Roland, On the History of France and Germany, Engels
Provençal Literature
1. On the Polish Question, Marx 1848
2. Chronological Extracts, from Schlosser’s World History
Chivalrous Love Poetry, from Origins of the Family, Chapter 2
Peasant Equalitarian Ideas in England, from Schlosser’s World History
German Volksbücher, Engels 1848
Renaissance
End of the Ancient World to End of the Middle Ages, Dialectics of Nature
Italian Culture from Dante to Garibaldi
1. Preface to Capital Volume III
2. Affairs in Prussia, Marx in New-York Tribune, 15 October 1860
Dante, Preface to Italian Edition of the Communist Manifesto, Engels 1893
Petrarch, Wanderings in Lombardy, Over the Alps, Engels 1893
Boccaccio, The Peasant War in Germany, Engels 1850
Great Renaissance, from Dialectics of Nature, Engels 1883
Titian, Engels to Marx, 20 May 1857
Grobian Literature of the Reformation Period, from Moralising Criticism and Critical Morality, Marx 1847
Significance of Reformation, On History of France and Germany, Engels
Thomas More
1. Capital, Volume I, Chapter 27
2. Capital, Volume I, Chapter 28
3. Preparatory materials for Anti-Dühring
4. Foreign Policy of Russian Tsardom, Engels 1890
Shakespeare
1. War Debate in Parliament, New-York Tribune, 17 March 1854
2. Engels To Marx, 10 December 1873
3. from The German Ideology
4. Marx To Engels, 27 February 1861
Calderón
1. Revolutionary Spain, Marx 1854
2. Marx To Engels, 3 May 1854
3. Herr Vogt, Marx 1860
Cervantes
1. The New Martial Law Charter, Marx 1849
2. Capital, Volume I, Chapter 1
History of Social Thought and Literature 
The Modern Period
Three Unities of Classical Drama, Marx to Lassalle, 22 July 1861
La Rochefoucauld, Marx To Engels. 26 June 1869
Historic Significance of the Enlightenment, Anti-Dühring
The Materialism of the Encyclopedists
1. Socialism: Utopian & Scientific
2. The Holy Family
The Enlightenment and Dialectics, Anti-Dühring
Utilitarian Philosophy of the Enlightenment from The German Ideology
Voltaire
1. Marx to Ludwig Kugelmann. 12 October 1868
2. What have the Working Classes to do with Poland?, Engels, 1866
Diderot
1. Ludwig Feuerbach
2. Marx to Engels 15 April 1869
3. Engels To Marx. 16 April 1869
Rousseau
1. from Anti-Dühring
2. On Proudhon, Letter to J B Schweizer, 24 January 1865
Sentimentalism as a Reaction to the Enlightenment, Moralising Criticism, Marx 1847
Crisis of Enlightenment Ideals, Socialism: Utopian & Scientific
From the Enlightenment to Romanticism, Marx To Engels. 25 March 1868
Criticism of Progress from the point of view of the Past, Communist Manifesto
Petty-Bourgeois Criticism of Capitalism, Communist Manifesto
Restoration Writers, On the Polish Question
French Literature
Abbé Prévost from Dialectics of Nature
Chateaubriand

1. Marx To Engels. 26 October 1854
2. Marx To Engels. 30 November 1873
Alexandre Dumas. Marx To Engels. 23 February 1851
Lamartine

1. Marx To Engels. 18 March 1848
2. Neue Rheinische Zeitung. 22 October 1848
3. Democratic Panslavism, Engels 1849
Victor Hugo

1. Eighteenth Brumaire of Louis Bonaparte, Preface
2. Engels To Lafargue. 5 December 1892
3. Engels To Marx. 13 September 1870
4. Notes on the War, Pall Mall Gazette, 13 October 1870
Eugène Sue, Continental Movements, Engels
From Critical Analysis of Sue’s Mystères de Parts

1. The Holy Family
2. The Holy Family
3. The Holy Family
Balzac
1. Capital, Volume I
2. Capital, Volume III
3. Engels To Laura Lafargue. 13 December 1883
4. Engels To Marx. 4 October 1852
5. Marx To Engels. 25 February 1867
Pierre Dupont, Capital, Volume I
Arthur Ranc,  Engels To Marx. 22 October 1869
Renan

1. Marx To Engels. 20 January 1864
2. Engels To Victor Adler. 19 August 1892
Zola. Engels To Laura Lafargue. 15 June 1887
Maupassant. Engels To Laura Lafargue. 2 February 1887
English and Irish Literature
Daniel Defoe
1. Engels To Karl Kautsky. 20 September 1884
2. Capital, Volume I
Swift. Manuscripts on the History of England and Ireland, Engels 1870
Pope, Herr Vogt, Marx 1860
Shelley and Byron, Edward Aveling and Eleanor Marx-Aveling, Neue Zeit, 1888
Walter Scott.  Origins of the Family, Chapter 1, Engels 1884
William Cobett’s Social Writings. Marx, New-York Tribune, 22 July 1853
English Working-Class Poet Mead.  Condition of the Working Class, Engels
Thomas Carlyle
1. The Condition of England, Engels 1844
2. Latter-Day Pamphlets edited by Thomas Carlyle, Marx and Engels 

English Realists of the Mid-Nineteenth Century, The English Middle Class, Marx. New-York Tribune, 1 August 1854
Carleton.  Marx To Engels. 14 August 1879
Bernard Shaw. Engels To Kautsky. 4 September 1892
Aveling
1. Engels To Paul Lafargue. 17 May 1889
2. Engels To Konrad Schmidt. 9 December 1889
William Morris. Engels To Laura Lafargue. 23 November 1884
German Literature
German Culture from the Mid-17th to the Early 19th Century
1. Notes on Germany, Engels
2. The State of Germany, Engels in Northern Star, 25 October 1845
Schiller. Shortcomings of His Poetry, from Ludwig Feuerbach, Chapter 2, 1886
Goethe
1. Ludwig Feuerbach, Chapter 1, 1886
2. Anti-Dühring, 1877
3. Ludwig Feuerbach, Chapter 1, 1886
4. The Condition of England, Engels
5. Engels To Marx. 15 January 1847
6. Karl Grün, German Socialism in Verse and Prose, Engels 1847
Heine
1. Ludwig Feuerbach, Chapter 1, 1886
2. Rapid Progress of Communism in Germany, Engels 1844
3. Capital, Volume III, Supplement
4. Marx Heinrich Heine. 12 January 1845
5. Marx Heinrich Heine. 5 April 1846
6. Engels To Marx. 14 January 1848
“Young Germany.” Revolution and Counter-Revolution in Germany, 1852
Alexander Jung.  “Lectures on Modern German Literature”, Engels 1842
German, or “True,” Socialism
1. The Communist Manifesto
2. The German Ideology
Karl Beck, the Poetry of True Socialism
1. German Socialism in Verse and Prose
2.  German Socialism in Verse and Prose
3.  German Socialism in Verse and Prose
4.  German Socialism in Verse and Prose
Gottfried Kinkel
1. Heroes of the Exile
2. Heroes of the Exile
Freiligrath
1. Reappearance of Neue Rheinische Zeitung
2. The True Socialists, Engels 1847
3. Marx To Ferdinand Freiligrath. 27 December 1851
4. Marx To Joseph Weydemeyer. 16 January 1852
5. Engels To Jenny Marx. 22 December 1859
6. Marx To Engels. 17 July 1869
7. Marx To Engels. 22 August 1870
8. Marx To Engels. 2 September 1870
9. Marx To Sigfrid Meyer. 21 January 1871
Weerth. “Song of the Apprentices”, Engels 1883
Johann Philipp Becker
1. Engels to Bebel. 8 October 1886
2. Johann Philipp Becker, Engels 1886
3. Marx To Johann Philipp Becker. 9 April 1860
Henryk Ibsen. Engels To Paul Ernst
Russian Literature
The Russian Language
1. Marx To Sigfrid Meyer. 21 January 1871
2. Refugee Literature, Engels 1874
3. Engels To Vera Zasulich. 6 March 1884
The Lay of Igor’s Host:  Marx To Engels. 5 March 1856
Lomonosov. Slavic Languages and Philology. Notes on Lomonsov, Engels
Derzhavin. The War on the Danube. Engels in New-York Tribune, 25 July 1854
Pushkin. Engels To Nikolai Danielson. 29-31 October 1891
Chernyshevsky and Dobrolyubov
1. Refugee Literature, Engels 1874
2. Engels To Eugenie Papritz. 26 June 1884
3. Marx To Nikolai Danielson. 9 November 1871
Chernyshevsky as a Scholar and Critic. Capital Afterword to 2nd German Edition
Chernyshevsky and the Russian Village Commune
1. Marx to the Editorial Board of Otechestvenniye Zapiski
2. Afterword to On Social Relations in Russia, Engels
Chernyshevsky as a Revolutionary
1. The Alliance of Socialist Democracy and the International, 1872
2. Marx To Engels. 5 July 1870
3. Marx To Nikolai Danielson. 12 December 1872
4. Marx To Nikolai Danielson. 18 January 1873
5. Engels To Nikolai Danielson. 10 June 1890
Shchedrin. Engels To Nikolai Danielson. 19 February 1887
Flerovsky.  Marx To Engels. 10 February 1870
Revolutionary and Satirical Folk Poetry of the Past
Herr Tidmann. Engels To Marx. 27 January 1865
“Herr Tidmann”. Old Danish Folk Song. Engels 1864
Conspectus of Kostomarov’s Revolt of Stekna Razin, Marx
The Vicar of Bray, Engels August 1882
German Revolutionary Songs. Engels To Hermann Schlüter. 15 May 1885
Weavers’ Song.  Marginal Notes on “The King of Prussia.”, Marx
Songs of the German Revolution of 1848
1. The Role of Force in History, Engels 1887
2. Campaign for the Imperial Constitution, Engels 1850
Satirical Folk Song Against Bonaparte. Engels To Marx. 7 February 1856
Confessions of Marx and Engels
Karl Marx. Confessions
Frederick Engels. Confessions
From Reminiscences of Marx and Engels
From Reminiscences of Marx
Paul Lafargue
1. Literature
2. Novels
3. Family
Eleanor Marx-Aveling. Karl Marx
Franzisca Kugelmann. Small Traits of Marx’s Great Character
Anselmo Lorenzo. Reminiscences of the First International
From Reminiscences of Engels
N. S. Rusanov. My Acquaintance with Engels
Fanni Kravchinskaya. Reminiscences
Critical Articles by Jenny Marx

1. From London’s Theatre World
2. The London Season
3. Shakespearean Studies in England
4. Shakespeare’s Richard III in London’s Lyceum Theatre
5. From the London Theatre
 


Early Literary Experiments of Marx and Engels
Abstracts not included in Collected Works
Marxist Literary Criticism |
Marx/Engels Subject Archive


