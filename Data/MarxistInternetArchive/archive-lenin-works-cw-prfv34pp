Lenin 
      Collected Works: Volume 34: Preface by Progress Publishers
	    The thirty-fourth and thirty-fifth volumes contain Lenin's
	    correspondence with organisations and persons—letters,
	    tel egrams and notes—during the period from 1895 to
	    1922.
	  
		    The documents in these volumes comprise a considerable part
		    of Lenin's correspondence and form a valuable sup plement to
		    his writings published in the preceding volumes of the
		    Collected Works. These documents reflect the
		    immense and varied activity of Lenin in building up the
		    Bolshevik Party, a party of a new type, his irreconcilable
		    struggle against opportunists of all shades, his struggle
		    for the proletarian revolution, for the dictatorship of the
		    proletariat, his leadership of the world's first Soviet
		    socialist state.
		  
		    Volume 34 includes letters of Lenin written in the period
		    from November 1895 to November 1911.
		  
		    The letters of 1895-1901 reflect Lenin's activities in
		    building up the Social-Democratic Labour Party in Russia,
		    his struggle against Narodism, “legal Marxism”
		    and Econo mism. The letters addressed to G. V. Plekhanov,
		    Lydia Knipovich, N. E. Bauman and others show liow Lenin's
		    plan for the creation of the first all-Russia newspaper of
		    the rev olutionary Marxists—Iskra—was carried
		    out; they re veal Lenin's leading role in Iskra,
		    and his struggle within the editorial board of the
		    newspaper.
		  
		    A considerable part of the volume consists of the letters of
		    1901-04. A group of letters of this period, addressed to
		    G. V. Plekhanov, deal with questions concerning the drafting
		    of the revolutionary programme of the proletarian par ty. In
		    his correspondence with local committees—those of
		    Kharkov and Nizhni-Novgorod, the St. Petersburg organi
		    sation (letters to 1. V. Babushkin and others), and the

		    Organising Committee for convening the Second Party Con
		    gress—Lenin calls on the Social-Democratic
		    organisations in Russia to unite on the basis of the
		    programmatic and organisational principles of
		    Iskra, and gives precise direc tives for developing
		    Party work and preparing for the Party Congress. In a number
		    of letters written after the Second Congress Lenin exposes
		    the splitting activities of the Mensheviks, wages a
		    relentless struggle against certain demoralised Bolsheviks
		    (Krasin, Noskov, Galperin) who had gone over to the
		    Mensheviks and helped them gain a majority in the Central
		    Committee. These are his letters to the Central Committee,
		    to the Siberian Committee, to N. Y. Vilonov, A. M. Stopani,
		    Bozalia Zemlyachka and others.
		  
		    The letters to the Caucasian Union Committee reflect Lenin's
		    leadership of the Bolshevik organisations in the Caucasus.
		  
		    The letters of the period of the first Russian revolution
		    (1905-07) reflect Lenin's struggle for the convocation of
		    the Third Party Congress, for the implementation of its
		    decisions, and for the tactical principles of
		    Bolshevism. Included here are letters to the Central
		    Committee, S. I. Gusev, Rozalia Zemlyaclika
		    and others.
		  
		    The letters of the period of Stolypin reaction reveal
		    Lenin's struggle against liquidationism, Trotskyism,
		    otzovism and ultimatumism, conciliation, and distortions of
		    the theoret ical principles of the revolutionary Marxist
		    Party. This volume includes a letter to
		    G. Y. Zinoviev in which Lenin brands Trotsky as a
		    despicable careerist and factionalist. A number of letters
		    published in this volume expose the international
		    revisionists who supported the Russian Men shevik
		    opportunists.
		  
		    An important place in Lenin's correspondence of 1908-li is
		    occupied by his letters to Maxim Gorky.
		  
		    The letters in this volume depict Lenin's struggle to create
		    a Marxist revolutionary party, to rally the Party's forces
		    and make the Bolsheviks an independent party, a party of a
		    new type, a party of Leninism, a Bolshevik party.
		  
		    The following letters, which have previously appeared in
		    various publications, are included in Lenin's
		    Collected

		    Works for the first time: to the Editorial Board of
		    Iskra, February 26, 1904; to M. K. Vladimirov,
		    August 15, 1904; to the Caucasian Union Committee, December
		    20, 1904; to the St. Petersburg organisation of the
		    R.S.D.L.P., Octo ber-December 1904; Letter to a Comrade in
		    Russia, January 6, 1905; five letters to A. V. Lunacharsky,
		    1905, 1907 and 1908; to Maxim Gorky, February 7, 1908; to
		    P. Yushkevich, November 10, 1908; two letters to
		    A. I. Lyubimov, August and September 1909; a letter to
		    G. Y. Zinoviev, August 24, 1909; draft of a letter to the
		    “Trustees”, February-March 1910; to
		    N. G. Poletayev, December 7, 1910; to A. Rykov, February 25,
		    1911.
		  
		    Published for the first time is the letter in this volume to
		    G. D. Leiteisen, July 24, 1902, in which Lenin notes the
		    union of Russian Social-Democratic organisations around
		    Iskra.
		  
		    * *
		    *
		    The letters in volumes 34 and 35 are arranged in chrono
		    logical order; those sent from Russia are dated according to
		    the old style, those sent from abroad are dated according to
		    the new style. Where Lenin's manuscript is undated, the
		    editors have given the date at the end of the letter. Each
		    letter has a serial number and it is stated to whom and
		    where it was sent, the date of writing and the address of
		    the sender.
		  
		    Besides brief notes, each volume of the letters is provided
		    with an index of deciphered pseudonyms, nicknames and
		    initials.
		  
Lenin Collected Works: 
Volume 34
Preface by
	      Progress Publishers

	    The thirty-fourth and thirty-fifth volumes contain Lenin's
	    correspondence with organisations and persons—letters,
	    tel egrams and notes—during the period from 1895 to
	    1922.
	  

		    The documents in these volumes comprise a considerable part
		    of Lenin's correspondence and form a valuable sup plement to
		    his writings published in the preceding volumes of the
		    Collected Works. These documents reflect the
		    immense and varied activity of Lenin in building up the
		    Bolshevik Party, a party of a new type, his irreconcilable
		    struggle against opportunists of all shades, his struggle
		    for the proletarian revolution, for the dictatorship of the
		    proletariat, his leadership of the world's first Soviet
		    socialist state.
		  

		    Volume 34 includes letters of Lenin written in the period
		    from November 1895 to November 1911.
		  

		    The letters of 1895-1901 reflect Lenin's activities in
		    building up the Social-Democratic Labour Party in Russia,
		    his struggle against Narodism, “legal Marxism”
		    and Econo mism. The letters addressed to G. V. Plekhanov,
		    Lydia Knipovich, N. E. Bauman and others show liow Lenin's
		    plan for the creation of the first all-Russia newspaper of
		    the rev olutionary Marxists—Iskra—was carried
		    out; they re veal Lenin's leading role in Iskra,
		    and his struggle within the editorial board of the
		    newspaper.
		  

		    A considerable part of the volume consists of the letters of
		    1901-04. A group of letters of this period, addressed to
		    G. V. Plekhanov, deal with questions concerning the drafting
		    of the revolutionary programme of the proletarian par ty. In
		    his correspondence with local committees—those of
		    Kharkov and Nizhni-Novgorod, the St. Petersburg organi
		    sation (letters to 1. V. Babushkin and others), and the

		    Organising Committee for convening the Second Party Con
		    gress—Lenin calls on the Social-Democratic
		    organisations in Russia to unite on the basis of the
		    programmatic and organisational principles of
		    Iskra, and gives precise direc tives for developing
		    Party work and preparing for the Party Congress. In a number
		    of letters written after the Second Congress Lenin exposes
		    the splitting activities of the Mensheviks, wages a
		    relentless struggle against certain demoralised Bolsheviks
		    (Krasin, Noskov, Galperin) who had gone over to the
		    Mensheviks and helped them gain a majority in the Central
		    Committee. These are his letters to the Central Committee,
		    to the Siberian Committee, to N. Y. Vilonov, A. M. Stopani,
		    Bozalia Zemlyachka and others.
		  

		    The letters to the Caucasian Union Committee reflect Lenin's
		    leadership of the Bolshevik organisations in the Caucasus.
		  

		    The letters of the period of the first Russian revolution
		    (1905-07) reflect Lenin's struggle for the convocation of
		    the Third Party Congress, for the implementation of its
		    decisions, and for the tactical principles of
		    Bolshevism. Included here are letters to the Central
		    Committee, S. I. Gusev, Rozalia Zemlyaclika
		    and others.
		  

		    The letters of the period of Stolypin reaction reveal
		    Lenin's struggle against liquidationism, Trotskyism,
		    otzovism and ultimatumism, conciliation, and distortions of
		    the theoret ical principles of the revolutionary Marxist
		    Party. This volume includes a letter to
		    G. Y. Zinoviev in which Lenin brands Trotsky as a
		    despicable careerist and factionalist. A number of letters
		    published in this volume expose the international
		    revisionists who supported the Russian Men shevik
		    opportunists.
		  

		    An important place in Lenin's correspondence of 1908-li is
		    occupied by his letters to Maxim Gorky.
		  

		    The letters in this volume depict Lenin's struggle to create
		    a Marxist revolutionary party, to rally the Party's forces
		    and make the Bolsheviks an independent party, a party of a
		    new type, a party of Leninism, a Bolshevik party.
		  

		    The following letters, which have previously appeared in
		    various publications, are included in Lenin's
		    Collected

		    Works for the first time: to the Editorial Board of
		    Iskra, February 26, 1904; to M. K. Vladimirov,
		    August 15, 1904; to the Caucasian Union Committee, December
		    20, 1904; to the St. Petersburg organisation of the
		    R.S.D.L.P., Octo ber-December 1904; Letter to a Comrade in
		    Russia, January 6, 1905; five letters to A. V. Lunacharsky,
		    1905, 1907 and 1908; to Maxim Gorky, February 7, 1908; to
		    P. Yushkevich, November 10, 1908; two letters to
		    A. I. Lyubimov, August and September 1909; a letter to
		    G. Y. Zinoviev, August 24, 1909; draft of a letter to the
		    “Trustees”, February-March 1910; to
		    N. G. Poletayev, December 7, 1910; to A. Rykov, February 25,
		    1911.
		  

		    Published for the first time is the letter in this volume to
		    G. D. Leiteisen, July 24, 1902, in which Lenin notes the
		    union of Russian Social-Democratic organisations around
		    Iskra.
		  

		    * *

		    *

		    The letters in volumes 34 and 35 are arranged in chrono
		    logical order; those sent from Russia are dated according to
		    the old style, those sent from abroad are dated according to
		    the new style. Where Lenin's manuscript is undated, the
		    editors have given the date at the end of the letter. Each
		    letter has a serial number and it is stated to whom and
		    where it was sent, the date of writing and the address of
		    the sender.
		  

		    Besides brief notes, each volume of the letters is provided
		    with an index of deciphered pseudonyms, nicknames and
		    initials.
		  







 
Previous

 | 
 Next 

 
Lenin
		      Collected Works 
 | 
 Lenin
		      Internet Archive 

 




Previous
 |  Next 
Lenin
		      Collected Works  |  Lenin
		      Internet Archive   