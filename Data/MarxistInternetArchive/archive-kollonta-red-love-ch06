Alexandra Kollontai's Red Love The train was to arrive in the morning. And Vassilissa was up with the
first pale light of dawn had to collect her things, and dress, to please her
beloved Volodya. They had been hard, those seven months of separation. 
Vassilissa was happy, gay, joyful. She felt the spring in the air.

The Nep-girl was still in bed, lying on her back, and gazing at her reflection in a hand mirror. But Vasya had already washed herself, carefully brushed her curls, and put on the new dress Grusha had made for her. Vassilissa looked into the mirror on the wall. She saw only her eyes. They sparkled so that her entire face looked beautiful.

Everything seemed to be allright. This time Volodya wouldn’t lecture her for running around in “rags. ”

A station. Vassilissa looked out the window. It was early in the morning, the sun was shining. In the North one could hardly see that it was Spring, but here everything was in bloom. The trees, too, were full of flowers. Strange, very odd trees. Leaves like those of the black alder, but more delicate in color; and the boughs covered with white blossoms, resembling lilac blossoms; but they were not lilacs. Their perfume, sweet and very strong, was pouring in through the window.

“What sort of tree is that?” Vasya asked the conductor. “We don’t have them in our country.”

“White acacias.”

“White acacias? They’re pretty.” The conductor broke off a few branches and gave them to Vasya.

How sweet their scent was. And Vasya was so happy that she was almost in tears. Everything about her was so beautiful, so fascinating. But the most important thing: “In another hour, I’ll see Volodya.”

“Will we be there soon?” Again Vasya turned to the conductor. To her it seemed that the train wasn’t moving at all. It had stopped again, at a siding. At last it moved on.

Now the city was in sight. The cathedrals. The barracks. The suburbs. The platform of the station. But where was Volodya? Where?

Vasya looked out of the open window. Volodya, however, had come in through the other end of the car, and took her into his arms.

“There you are, Volodya. How you frightened me.”

They kissed.

“Quick, let me have your things. This is our secretary. Please take the things, Ivan Ivanovitch. We’re going to the auto. I have a couple of horses now, Vasya, a cow and a car. And I am intending to get some little pigs. We have a lot of room; it’s a regular farm. You’ll see for yourself. You’ll live like the lady of the manor. Things are getting along well. Not long ago we opened a branch in Moscow.”

Vladimir talked on and on. He could not tell her quickly enough of the things he was busy with, which filled his thoughts. Sitting in the auto, Vasya listened. And although she was very much interested in what Volodya had to say she would have liked to talk about her own affairs, to find out how he had got along without her. Had he longed for her very much? Had he waited very anxiously for her?

They drew up before the house. A one-family house with a garden. A half-grown youth, an errand-boy in a gallooned cap, was standing guard at the door. He helped them out of the car.

“Now, Vasya, we’ll see how you like our house. Whether you’ll find it better than your cage under the roof.”

Carpet on the stairs. A mirror. A reception hall. Vasya took off her hat, dropped her coat. They went into the living-room. Sofas, rugs. A huge clock in the dining room. Some still life's in gilded frames. Antlers on the wall.

“Well, how do you like it?” Vladimir was radiant with pride.

“I like it,” Vasya replied uncertainly, looking about. She didn’t know herself whether she cared for it, Everything was so strange, so unfamiliar.

“And this is our bedroom.” Vladimir opened the door wide. There were two windows opening on the garden. This pleased Vasya at once.

“Trees,” she cried, delighted. “White acacias.” She hurried to the window.

“Look around the room first; you’ll have plenty of time to run around the garden. Isn’t it nice, what I’ve prepared for you? I picked out and arranged everything myself. I’ve waited for you since the moment I moved in.”

“Thanks, darling.” Vasya stretched to kiss Volodya. But he seemed not to see it, took hold of her shoulders and turned her so that she faced the long mirror in the wardrobe.

“See how convenient it is. When you dress you can see yourself from top to toe in the mirror. There are shelves inside: for your underwear, for your hats, for all sorts of gewgaws....”

“Why, how many hats and gewgaws do you think I have? You’ve hit the nail on the head.” Vasya laughed.

But Volodya went on: “Will you look at the bed? A silk quilt – I had a hard time finding it. It belongs to me; I didn’t get it among my supplies. And here’s a pink lamp to light at night.”

Vladimir showed Vasya around, pointing out every trifle, delighted as a child.

“Didn’t I feather a cozy little nest for my little girl?” Vasya listened, smiling at his happiness, but still not quite at ease. She couldn’t deny it, the rooms were nice, splendid. Rugs, curtains, mirrors! But not familiar. As if she had come into somebody else’s house. The things were not what Vasya needed. There were no tables where she could spread her books and papers. Only one thing really pleased her. That she could see white acacias from the windows facing the garden.

“Get ready, now, and wash up; then we’ll have some breakfast,” said Vladimir, going over to the window to let down the shades.

“What are you doing that for?” objected Vasya. “It’s so nice to look out into the garden.”

“But it won’t do. The shades have to be down by day, or the upholstery will fade.”

Down sank the gray shades; like heavy eyelids they hid the green of the garden shining in through the window. And the room became gray, monotonous, even less familiar. Vasya washed her hands, and combed her curls before the mirror.

“What’s that you have there? Did you have a dress made of the material I sent you?”

“Why, yes...,” expecting a word of praise, Vasya looked up at Volodya inquiringly.

“Let’s have a good look at you.” He turned her about. She could tell from his expression that he wasn’t pleased.

“Where in the world did you get the idea of piling all that stuff on your hips? You’ve a narrow figure, just the thing for the new fashions. Why did you get this monstrosity?”

Vasya was confused, flushed to the roots of her hair. She felt guilty.

“Why a monstrosity? Grusha says that’s stylish now.

“What does your Grusha know about it? She made a mess of the material. You look like the wife of a pope. You’d do better to take off that dress and put on your every-day skirt. You’d look more like yourself. This way you’re neither fish, flesh, nor fowl.”

Volodya did not see the disappointment in Vasya’s face. He went out into the dining room to see about breakfast.

With a heavy heart Vasya pulled off Grusha’s handiwork, and hastened to put on her old skirt and the blouse with the leather belt.

She was not at all happy. Two little tears dropped on the old blouse. They dried quickly. And there was an unpleasant coldness in Vasya’s eyes.


The “manager’s housekeeper” came to pay her respects during breakfast. Marya Semyonovna. A robust woman of middle age and respectable appearance.

Vasya shook hands with her.

“That wasn’t necessary,” said Vladimir after Marya Semyonovna had left the dining room. “If you don’t
act like the lady of the house you’ll have them all on your neck.”

Vasya looked at him in amazement. “That’s something I simply can’t understand.”

Vladimir served Vasya. But she had no appetite,
felt ill at ease.

“Here, look at the tablecloth, Morosov linen. The
napkins have the same design, too. But I didn’t have
them put out, it costs too much to wash them.”

“Where did you get all these things? Did you really
buy everything?” Vasya looked searchingly at
Vladimir.

“Such an idea! Why, do you know what these
furnishings would cost nowadays? Billions! Do you
really think that my manager’s salary enables me to
buy such luxuries? All these things were supplied to
me. I was lucky enough to come at the time when it
was possible, with the assistance of some friends to
obtain such furnishings from the authorities. Now
they’ve put a stop to all this. No one can have his
house furnished like this today. Not unless he pays
cash. Besides, I bought several things on my own
account during the winter; the wardrobe with the
mirror, in the bedroom, the silk quilt, the drawing
room lamp....” Vladimir enumerated everything,
happily, contentedly.

Vasya’s eyes grew colder and colder, shone with wrath. They no longer seemed brown, but green, like a cat’s eyes.

“And how much did all these splendid things cost you?” Vasya’s voice trembled. Vladimir did not notice, but continued eating his chop and drinking his beer.

“Well, if you calculated the total, including what I’ve taken on credit, on the instalment plan, it amounts to....”

Slowly, so as to impress Vasya, Vladimir mentioned a very considerable sum. Raising his laughing eyes to her face, he seemed to say: now do you see what a fine fellow I am?

“Why, Vasya, what’s the matter with you?

She had jumped to her feet, was standing over him with angry green eyes.

“Where did you get the money? Tell me at once – where?”

“What’s the matter, Vasya? Calm yourself. You surely don’t believe that I got it dishonestly? Or don’t you know anything about money values? Compare it with my salary, and you’ll see.” He told her the amount of his monthly drawing account and bonus.

“That’s your salary? Your monthly salary? But how dare you, a Communist, spend it for such trash, for such nonsense? Poverty’s increasing! Misery and famine are round about! And the unemployed? Have you forgotten them? Was there nothing irregular about your becoming the manager?”

The furious green eyes came closer to Vladimir. “Well, Sir Manager, will you be good enough to answer?”

Vladimir did not give up, but wanted to bring Vasya to reason, to convince her good-naturedly. He laughed at her. “You live like a sparrow under the gables, and have no idea of what money is worth. Others are earning even more, and live quite differently. They make a really elegant appearance.”

But Vasya was not the sort to be defeated with words. She had made up her mind to demand an account. Why didn’t he live as a Communist? Why did he throw away his money on foolish trifles while poverty and famine held sway round about him?

Vladimir realized that he couldn’t get at her by this means. He would have to try another way. He would have to attempt a political explanation. That it was all a part of the manager’s task. Instructions from headquarters. The main thing was to do all he could to make the undertaking flourish, to increase the earnings of his company. And this was his strongest point. Vasya must wait until she saw what he had accomplished in a year. He had built up everything in a deserted place, had increased the output, so that now the entire trust was dependent on his supervision. She would see for herself. Though he lived “like a human being” he was none the less concerned with every one of his employees, with the most humble shipping clerk. Let her only get an insight into the matter, then she would think differently. But he had not expected that his friend, Vasya, his wife, his comrade, would come there to join in the chorus of his enemies. It was so hard to work that way. He gave all his energy to the cause, and these were his thanks. Even his wife was against him, wanted to condemn him.

Vladimir was offended and furious. His eyes were those of an angry wolf. They flashed fire at Vasya, as though to burn her. Because of her suspicion and condemnation.

Vasya listened thoughtfully. He might be right. Everything was different now. The most important thing was that his accounts were straight and the work done. The national wealth must be increased. She was not disputing that.

“Because I get some things, establish my own household? Am I to live in community houses forever? And. why are we worse than American laborers? You should see how they live there. They have their own piano, their own Ford, their own motorcycle.”

In the meantime the worthy Marya Semyonovna had looked into the dining room several times. She wanted to serve the fritters; and she saw that these two quarreled the moment they met. That’s how it had been with the “real gentlefolk” whom she had served before the Revolution. They, or the Communists – they were all alike. Only, it was too bad about the fritters, they would spoil with the standing.

Vladimir took Vasya everywhere, showed her the
offices, the warehouses and the homes. He took her
to the bookkeeping department, too. “Just take a look
at our books, you won’t find such a system of accountancy anywhere else. See how wonderfully I’ve arranged things, and then tell me that I’m wasteful.”

He asked the bookkeepers to explain to Vasya the
principle of their system, which was simplified, but
accurate. It had received special commendation from
headquarters.

Vasya listened carefully. Although she couldn’t
understand everything she saw that they were trying
hard and loved their work. Volodya, too, was in it
with all his heart and soul. He took her to the homes
of the employees, purposely asked their wives whether
they were satisfied. He looked at Vasya triumphantly.
Everyone said the same thing: Were they satisfied?
Nowadays it was impossible to be better off “We
owe our lives to your providence, Vladimir Ivanovitch.”

“There! And you say I’m a spendthrift! Believe
me, I took care of our employees first. I got as much
as I could for them. And only then did I think of myself. You see how they live. The workingmen are
just as well off as the office force. I made special
efforts in their behalf. Really, I did everything I possibly could.”

“Very well, you did all that. But what about them?
What did they do for themselves?”

“What peculiar ideas you have, Vasya. Don’t we have the same interests, they and I? Before, of course, the manager stood on one side, and the workers on the other. But not now, not here. You’ve become moss-grown in your little bog.”

He was joking, yet Vasya felt that Vladimir wasn’t pleased, that she had offended him. He spent the entire day taking her about the various buildings of the works. Vasya grew tired. Her temples began to throb; she had a stitch in her side, a backache. If only she could go home, lie down and go to sleep. Her head was still buzzing with the noise of the train-wheels. But Volodya had just told her that there would be guests for dinner. She was to receive them.

They came home, entered the hall. The errand-boy opened the door, and remained standing, as though expecting a command. Looking at him, Vladimir took a notebook from his pocket, scribbled a few words, and gave the note to the boy.

“Now hurry, Vassya, so there’ll be no delay. You’ll bring the answer to me personally. Understand?”

He turned to Vasya again, looked at her with an odd expression on his face, half guilty, half inquiring.

“What’s the matter with you, Vasyuk? Why do you stare at me so?” His voice sounded uncertain.

“Nothing’s the matter. But – the errand-boy’s name is Vassya, too?”

“Yes; don’t you like the idea of there being two Vasya’s in my house? Can you imagine! She’s jealous ! But you needn’t worry. There’s not another Vasya like you in the world.”

Gently he put his arm about her, gazed into her eyes, and kissed her. It was the first time he had caressed her all day. They went into the bedroom arm in
arm.


The dinner-guests arrived: Savelyev and Ivan Ivanovitch, the secretary of the administration. Savelyev was a tall, lean man, in a light gray suit. His thin hair was neatly combed, and he wore a seal-ring on his index-finger. Clever, rather crafty eyes, an unpleasant smile on his smooth-shaven face. As though he were watching everything, and as if everything were the same to him as long as he was well off. That’s how it seemed to Vasya.

When he met Vasya he raised her hand to his lips. She pulled it away.

“I’m not used to that.”

“As you say. But I never object to kissing the hand of a young woman. It’s pleasant, and the husband can’t be jealous. You must be very jealous, Vladimir Ivanovitch? Confess!”

As he spoke he slapped Volodya’s back. Vladimir laughed.

“Vasya is a model wife, there’s no need of being jealous of her.”

“So she doesn’t follow her husband’s example?”

Savelyev winked at Vladimir. And Vladimir’s eyes suddenly grew big and frightened.

“I don’t think I’ve ever done anything to ..

Savelyev interrupted. “Never mind. We know how you are, you married men. I’ve been through it myself. But now I’m leading a bachelor’s life.”

Vasya didn’t like Savelyev. Didn’t like him at all. But Volodya talked with him as with a friend. About business, about politics. Vasya wouldn’t have discussed politics with this “speculator,” wouldn’t have laughed with him at the Chairman of the Executive Committee. She would have to reason with Volodya, persuade him to drop this friendship.

They had wine for dinner. The secretary, Ivan Ivanovitch, had brought it in a basket. They were worried about some large shipments that had failed to arrive, and which they were afraid would come too late for the fair.

Vasya listened, trying to grasp the meaning of it all. But it seemed to her that these things weren’t so very important, as if the main point were not being mentioned. The throbbing and hammering of her temples bothered her, and her eyes hurt. If only the meal were over.

Vladimir ordered the auto right after dinner. He had to attend an important meeting concerning the shipment.
“Are you really going to the meeting today? The day your wife came? You ought to stay with her.
It’s not nice of you, Vladimir Ivanovitch.” Savelyev looked at Vladimir with a crooked smile.

“Impossible,” interrupted Vladimir, carefully lighting a cigarette. He would have been glad to stay – business, you know.

Savelyev could not refrain from saying, “There are two sides to everything.”

And again Vasya thought he was winking at Vladimir, laughing at him. A disgusting speculator.

“If I were in your place I’d drop everything else today, and spend the first evening with your wife. Your business won’t run away.”

Vladimir didn’t answer, but picked up his cap angrily.

“Well, Nikanor Platonovitch, are we ready to go?”

They drove off, Ivan Ivanovitch going with them. Vasya was left alone. Alone, in the great empty house that was so strange to her. She went through the rooms. Dreary, lonely, cold. She stood beside the window. Then she lay down on the bed with the silk quilt, and fell asleep at once.

She awoke with a start. It was dark. Lighting the lamp, she glanced at the clock. A quarter past twelve. Had she really slept so long? Past midnight. Vladimir had not come in.

Getting up, Vasya bathed her face and went into the dining room.

The supper-table was set, the light was burning. The room was empty and still, the rest of the house dark. She went into the kitchen, where Marya Semyonovna was straightening things.

“Hasn’t Vladimir Ivanovitch returned?”

“No. Not yet.”

“Does he always come back so late from his meetings?”

“It depends.”

Marya Semyonovna was sullen, and sparing with her words.

“How about you? Are you waiting up for him? Aren’t you going to bed?”

“Vassya and I take turns. One day he stays up, the next day I do.”

“Will Vladimir have supper when he comes?”

“If he brings any guests I guess he’ll have some. Otherwise he goes straight to his room.”

Vasya stayed a little while longer, silent. She saw that Marya Semyonovna was busy with her own affairs, and paid no attention to her.

Going back to the bedroom, Vasya opened the window. A cool, quiet spring night. The air was filled with the strong perfume of the acacias. The frogs croaked loudly, curiously. At first Vasya thought they were night birds.

The sky was dark, and dotted with many, many twinkling stars. Vasya gazed into the dark garden, looked up at the sky and stars. Her heart became calmer. She forgot the speculator, Savelyev, forgot the pain Vladimir had involuntarily caused her during the day. Now she felt with all her soul that she had come to him, to her beloved, to help and guide him. One who associates with Nep-people cannot help leaving the right road. That was why he had summoned her, his friend and wife.

Remembering how Vladimir had arranged everything, Vasya was proud of him. How energetic he was. Now she saw things in a different light. Everything seemed clearer, more intelligible, more cheerful than during the day.

Vasya was so absorbed with her thoughts that she failed to hear either the car drawing up or Vladimir walking over the rugs and carpets to her. The sound of his voice made her start.

“What were we thinking about so hard, little Vasyuk of mine?”

As Vladimir bent over her, his eyes seemed anxious and loving.

“Have you really come, dear? I’ve been waiting so long.”

She threw her arms about his neck.

Vladimir picked her up as in the first months of their love, and carried her through the room like an adored child.

Vasya felt happy and gay. Volodya loved her, loved her as always! How silly she had been! Why had she felt hurt in the morning?

They drank tea together, had an intimate and affectionate talk. Vasya pronounced her opinion of Savelyev. “It’s better not to be a friend of his.”

Vladimir did not deny it. He admitted that he, too, had no respect for him; but he was useful; the whole business would have been impossible without him. He had many connections from before, and enjoyed the confidence of the merchants; it was possible to come in contact with them through him. Volodya, too, had learned much from him. Frankly speaking, he was not worth much as a man. A genuine burshui; but in business he was indispensable. That was why Volodya had defended him when the highest authorities, the “super-clever fellows,” had arrested Savelyev. And he was highly esteemed in Moscow. The local authorities had been given a good calling-down on his account.

“Yes, but didn’t you write me that his hands aren’t clean?”

“How can I make it clear to you? He’s our representative. Of course he doesn’t neglect himself. But he’s no worse than the others. Besides, the other fellows dawdle about and do nothing, while he works conscientiously. And he knows his work, likes it.”

All this notwithstanding, however, Vladimir promised to see less of him. Business was business, but it didn’t necessitate a friendship.

Having finished their tea, they returned to the bedroom arm in arm. Vladimir pressed Vasya’s head to his breast, kissed her curls, and spoke thoughtfully, tenderly. “Such a dear little head. It’ll always be mine, won’t it? Another friend like you, Vasya, doesn’t exist. I love only you, my Vasya, my little tomboy.”


Vasya woke up late. Vladimir had gone to work long before.

She didn’t feel well. She had shooting pains in her side, felt feverish, and was beginning to cough. Had she caught cold on the trip? Although it was a beautiful sunny day she wrapped a shawl about her. She didn’t want to move, and didn’t want to get up. Marya Semonovna came into the room, stood in the doorway, folded her hands before her, and looked at Vasya as though she were expecting something.

“Good morning, Marya Semyonovna.”

“Good morning,” was the dry response. “What will you order for dinner? When he left, Vladimir Ivanovitch said you’d attend to everything. You're having guests.”

Vasya was at a loss. She had no idea what she should order. At home, in the community house, she had had only such food as the State supplied.

Seeing that Vasya knew nothing whatever about such matters, Marya Semyonovna suggested various dishes. Vasya agreed to everything. But she inquired as to the cost. Wouldn’t it be very dear? Marya Semyonovna’s mouth snapped shut.

“Well, if you want a good dinner, you can’t save on it. You can’t have anything without money. The Communists have done away with the payoks.”

“Do you have any money?”

“There’s a little left from yesterday, but not enough for today. Meat is expensive, and we’ll have to buy butter, too.”

“So Vladimir left you no money?”

“He left me nothing. He only said: ‘Go to Vassilissa Dementyevna and discuss everything with her.’ ”

What should she do now? Marya Semyonovna stood there, waiting for the money, and would not go. Vasya had a little money left, but the household would soon eat it up; and she would be left without a kopek. She didn’t like that idea.

“Why don’t you advance some of your money to me, and then have Vladimir Ivanovitch give it back to you?” suggested Marya Semyonovna.

“Really, that never occurred to me!”

And the matter was settled.

When Marya Semyonovna had gone Vasya went out into the garden. She walked up and down the paths for a long time, until she was tired. She felt so exhausted. Lying down, she took up a book, and fell asleep over it.


Vasya lay stretched on the bed. Her cheeks were
burning, her sleep was disturbed by dismal, tormenting dreams. Waking, she looked about fretfully. Why had she gone to sleep? It would have been better to see the sights of the city. She hadn’t come to Vladimir to be sick. Yet she hadn’t the slightest desire to raise her head. She closed her eyes, and her thoughts immediately became confused. It was no proper sleep,
not even a doze. But she wasn’t fully conscious, either.

“Vassilissa Dementyevna, Vladimir Ivanovitch will come in for dinner any moment; you should get dressed. Then I could make the bed. He hates to see disorder in the house.”

Marya Semyonovna was bending over Vasya as though, being the elder, she wanted to correct her.

“Is it that late?”

“Almost five. And you haven’t even had breakfast. I wanted to wake you before, but you were sound asleep. That’s from the trip. You haven't gotten over it yet.”

“It might be the trip, or I may have caught cold. I feel chilled.”

“You should put on your woollen dress; it’ll by warmer. The little rag you’re wearing isn't any good.”

“My suit turned out badly. My husband didn’t like it at all.”

“Why do you say that? It’s not so bad. There may
be too many pleats on the hips, and the waistline isn't
just where it should be. Nowadays they’re wearing
the waistline... I’ve been a dressmaker, too. I know all about clothes. Just you let me remodel the skirt. We’ll change that dress so Vladimir Ivanovitch won’t recognize it.”

“Will it be ready by dinner-time?”

“That’s asking a little too much. No, we’ll do it slowly; we won’t rush it. Now you put on your black skirt and wear the coat of your suit over it. That’ll look very well.”

Never before had Vasya spent so much time before the mirror. Marya Semyonovna was forever finding something to change. Here she fastened something with pins, there she made some long stitches. She found a lace collar, too. The effect was quite good. Simple, yet elegant. Even Vasya liked it. What would Vladimir say about it?

Almost as soon as she was finished Vladimir came with his guests: an employee of the G. P. U. (what had been the Cheka) and his wife. The ends of his mustache had been waxed to needle points; he was foppishly dressed, with tan boots that reached to his knees. And that called himself a Communist!

Vasya didn’t like him at all. And his wife dressed up like a street-walker! She wore a thin dress, white shoes and a fur scarf across her shoulders; her fingers were glittering with rings. Vladimir kissed her hand, jested with her. What were they talking about? She couldn’t understand it. It was all nonsense. Vladimir was bending over her gallantly, his eyes flirting with hers.

Vasya sat beside the man from the G. P. U. He was a Communist. But she had no idea of what to say to him.

They had wine again. Vladimir touched glasses with the lady; she whispered something to him, and both laughed. It annoyed Vasya. But he paid no attention to her. As if she didn’t belong to him. Queer! She didn’t like it.

Jokingly they mentioned the fasts. The lady said that she was religious and went to confession, even though she did not fast. How could that be? A Comrade of the G. P. U. married to a believer? Vasya scowled. She was out of humor. Because of Vladimir, too. What sort of friends did he have? Toward the end of the meal, Ivan Ivanovitch came in to tell them that Savelyev had taken a box in the theatre, and had invited them.

“We’ll go, won’t we, Vasya?” asked Vladimir.

“With Savelyev?” Vasya tried to catch his eyes; but he pretended not to understand.

“Yes, of course, with Nikanor Platonovitch. With the whole crowd. They’re giving a new operetta. It'll amuse you.”

“No, I won’t go.”

“Why not?”

“I don’t feel quite well. I must have caught cold on the trip.”

Vladimir looked closely at her.

“Really, you don’t look well, Vasya. Your eyes are quite sunken. Let me have your hand. Why, it’s terribly hot. Of course you can’t go. And I won’t go either.”

“But why not? Do go!”

The guests, too, persuaded Vladimir, and he gave in.

In the hall Vladimir embraced Vasya in the presence of the others, and whispered in her ear: “You look unusually pretty today, Vasya.”

He asked Marya Semyonovna to take care of Vassilissa Dementyevna.

“Go to bed right away, Vasya. I’ll be back soon. I won’t stay to the end.”

They drove off.

Vasya wandered about the rooms, quite forlorn.

She didn’t like this life. She couldn’t say just what was wrong with it. But everything was new and unfamiliar. And she was a stranger here; no one had need of her. Vladimir might love her, but he thought of her so little. He had put his arms around her, kissed her and gone away. It was different when he had to go to a meeting, to work. But this time it was the theatre! Why had he gone without her? Hadn’t he seen enough of the theatre during the winter? Something was troubling Vasya, haunting her. She couldn’t express it. She felt ill at ease.

“I’ll stay here a week,” she decided. “I’ll see how things stand with Volodya, and then I’ll go.”

But there was the rub. Where would she go? Back to the community house? Her room there, her attic under the roof was gone. Her friend, Grusha the seamstress, was living in it. Besides, the Fedosseyevs were there; there would be gossip and worry. Once more she would have to fight everybody for the house. And she felt too worn out for that. Besides, she had lost faith in the soundness of the proposition. And that was the most important point.

No, she had no place where she could go.

This thought made her heart even heavier, stabbed it as with a steel blade.

Vasya was cold. Shivering, she drew her hands into her sleeves. She wandered through the dark empty rooms.

She felt as if this strange house were preparing sorrow for her. A lurking disaster.

A premonition?

Could a Communist believe in premonitions? But it must be that. Else, why this melancholy? This infinite, nameless, fruitless melancholy? 
Contents |
Alexandre Kollontai Archive