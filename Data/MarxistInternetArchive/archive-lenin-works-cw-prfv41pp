Lenin 
      Collected Works: Volume 41: Preface by Progress Publishers
	    The additional volumes 41. to 45 of the present edition contain the
	    most important of the new material included in the Fifth Russian
	    edition of the Collected Works of V. I. Lenin.
	  
	    Volume 41 contains works written before the Great October Socialist
	    Revolution, from 1896 to October 1917, which are an essential
	    supplement to the works published in the respective volumes of the
	    present edition.
	  
	    A great part of the volume consists of documents reflecting Lenin's
	    efforts in creating and strengthening the Bolshevik Party and
	    working out the ideological and organisational principles, the
	    programme and the rules of a new type of proletarian party. Among
	    them are: “Outline of Various Points of the Practical Section
	    of the Draft Programme”, “Record of Points One and Two
	    of Plekhanov's First Draft Programme, and Outline of Point One of
	    the Programme's Theoretical Section”, “Initial Variant
	    of the Agrarian Section and the Concluding Section of the Draft
	    Programme”, and Lenin's speeches at the Second Party
	    Congress. They show that Lenin helped the Iskra Editorial
	    Board to draft a truly revolutionary programme.
	  
	    The record of the Second Congress of the League of Russian
	    Revolutionary Social-Democracy Abroad, the January and June (1904)
	    sessions of the R.S.D.L.P. Council, “Draft Resolution of the
	    Majority's Geneva Group”, “Reply to L. Martov”,
	    “Report on the State of Affairs in the Party”, and
	    others show Lenin's struggle against the Mensheviks' splitting and
	    disorganising activity after the Second Congress of the R.S.D.L.P.
	  
	    A large group of documents written by Lenin in connection with the
	    work of the Third, Fourth and Fifth Congresses of the R.S.D.L.P. is
	    of great importance for a study of the Party's strategy and tactics
	    during the first Russian revolution. These documents contain
	    propositions on the hegemony
	    
	    of the proletariat, the alliance of the working class and the
	    peasantry, and the development of the bourgeois-democratic
	    revolution into a socialist revolution.
	  
	    Considerable interest attaches to the works connected with the
	    elaboration of Bolshevik tactics in the Duma (Parliament): the
	    report and summing-up speech on the report on the election campaign
	    for the Second Duma and other material of the Second Conference of
	    the R.S.D.L.P. (the First All-Russia Conference), the articles
	    “Are the Mensheviks Entitled To Conduct a Policy of Supporting
	    the Cadets?", “The Third Duma and Social-Democracy”,
	    “Report to the International Socialist Bureau, 'Elections to
	    the Fourth Duma'", “The Duma Group and the Majority
	    Outside”, etc.
	  
	    A number of works dating from the period of reaction reflect Lenin's
	    struggle against ideological vacillations and deviations from
	    Marxism. Lenin waged an implacable struggle against the avowed
	    opportunists, the Menshevik liquidators, and also against the
	    “Left” opportunists inside the Bolshevik Party—the
	    otzovists, the ultimatumists and the Vperyod splinter
	    group. In addition to the material already published, the volume
	    contains 14 works by Lenin shedding light on the conference of
	    Proletary's enlarged Editorial Board which condemned both
	    liquidationism and otzovism.
	  
	    The volume gives a fuller picture of the meeting held by members of
	    the R.S.D.L.P. Central Committee in Paris in June 1911. In his
	    “Report on the State of Affairs in the Party” and
	    speeches at the meeting, Lenin defined the Party's tasks in the
	    struggle against the anti-Party groups.
	  
	    The Sixth (Prague) All-Russia Conference of the R.S.D.L.P. brought
	    to a close a long period of struggle against Menshevism. By
	    expelling the Menshevik liquidators from the Party, it strengthened
	    the Party as an all-Russia organisation, capable of giving a lead to
	    the masses in afresh revolutionary upsurge. The volume contains a
	    number of documents which are of great interest for the study of the
	    Conference. Among them are: “Report on the Work of the
	    International Socialist Bureau”, setting out important
	    propositions on the new epoch, an epoch of socialist revolutions and
	    “battles against the bourgeoisie”, and on the

	    consequent sharpening of the struggle between the revolutionary
	    Social-Democrats and the reformists inside the European socialist
	    parties, and “Speech on the Organisational Question”,
	    emphasising the need to strengthen the Party's ties with the masses
	    and to combine legal and illegal work.
	  
	    The volume contains Lenin's resolution for the Cracow meeting of the
	    R.S.D.L.P. Central Committee with Party workers, “On the
	    Reorganisation and Work of the Pravda Editorial
	    Board”. This decision shows how the Central Committee, led by
	    Lenin, gave effective and concrete guidance to Pravda, the
	    Party's most important legal organ.
	  
	    In some of his works—"Reply to Liquidators' Article in
	      Leipziger Volkszeitung”, “Letter to the
	      Executive of the German Social-Democratic Party”, “On
	      the Question of the Bureau's Next Steps”, “Russian
	      Workers and the Inter national”, “How the Liquidators
	      Are Cheating the Workers”, “Resolution on the
	      Socialist Bureau's Decision"—Lenin gives a firm rebuff to
	      attempts by the leaders of the German Social-Democrats and the
	      Second International to “reconcile” and unite the
	      Bolsheviks and Mensheviks by liquidating the Bolshevik Party.
	  
	    Lenin's struggle for Party unity is characterised by the documents
	    relating to the Fourth Congress of the Social-Democrats of the
	    Latvian territory: his report and summing-up speech, and the draft
	    resolution on the attitude of the Social-Democrats of the Latvian
	    territory to the R.S.D.L.P.
	  
	    Of the documents supplementing Lenin's elaboration of the national
	    question, the volume includes: “Theses for a Lecture on the
	    National Question”, “German Social-Democracy and the
	    Right of Nations to Self-Determination”, “Note to the
	    Theses 'Socialist Revolution and the Right of Nations to
	    Self—Determination'", “On the Declaration by the Polish
	    Social-Democrats at the Zimmerwald Conference”, plans of an
	    unfinished pamphlet, Statistics and Sociology, and
	    “Speech on the National Question” at the Seventh (April)
	    All-Russia Conference of the R.S.D.L.P.(B.).
	  
	    Lenin urged the need for the workers to struggle against the danger
	    of the world war which was being prepared by the imperialists of all
	    countries, and exposed the opportunists who denied that such a
	    struggle was of any real importance,

	    an attitude which doomed the workers to a passive stand. He believed
	    that it was a major task of the revolutionary Social-Democrats to
	    conduct anti-militarist propaganda and spread the idea of
	    international solidarity among the working people. This question is
	    dealt with in the following articles: “Notes to the Resolution
	    of the Stuttgart Congress on 'Militarism and International
	    Conflicts'", “Notes to Clara Zetkin's Article 'International
	    Socialist Congress in Stuttgart'", “Anti-Militarist Propaganda
	    and Young Socialist Workers' Leagues” and “How the
	    Socialist-Revolutionaries Write History”.
	  
	    A number of documents published in the volume relate to the period
	    of the First World War, namely, “On the Slogan to Transform
	    the Imperialist War into a Civil War”, “Editorial Note
	    to the Article 'The Ukraine and the War'", “Draft Point Three
	    of the Resolution 'The C. O. and the New Paper', Adopted by the
	    Conference of the R.S.D.L.P. Sections Abroad”, “Draft
	    Resolution of the International Socialist Women's Conference”,
	    “Variant of the Draft Resolution of Left-wing Social-Democrats
	    for the First International Socialist Conference”, “Plan
	    for a Lecture on 'Two Internationals'", speeches at the Zimmerwald
	    and Kienthal International Socialist conferences, “Draft
	    Resolution of the R.S.D.L.P. Central Committee To Terminate
	    Publication of the Journal Kommunist”, “Remarks
	    on an Article about Maximalism” and others. These documents
	    show the Bolshevik tactics with regard to war, peace and revolution;
	    they explain the slogan of transforming the imperialist war into a
	    civil war, and characterise Lenin's activity in rallying the
	    Left-wing and revolutionary elements within the international
	    working-class movement round the banner of internationalism, his
	    struggle against social-chauvinism and Kautskyism (Centrism), and
	    against the Left-wing opportunist, sectarian stand and splitting
	    activities of the Bukharin-Pyatakov group.
	  
	    A number of documents written after the bourgeois-democratic
	    revolution in Russia in February 1917 contain Lenin's propositions
	    concerning the Party's attitude to the bourgeois Provisional
	    Government.
	  
	    The volume contains material connected with Lenin~ s return from
	    Switzerland to Russia in April 1917. It will be

	    recalled that the bourgeois and petty-bourgeois press started a
	    campaign of slander and harassment over Lenin and the Bolsheviks'
	    return home across Germany. This is fully exposed in the following:
	    replies to a correspondent of the newspaper Politiken and
	    to F. Ström, a spokesman of the Left-wing Swedish
	    Social-Democrats, the group's communiqué, “Russian
	    Revolutionaries' Trip Across Germany”, speeches at a
	    conference with Left-wing Swedish Social- Democrats on March 31
	    (April 13), at a meeting of the soldiers of an armoured battalion on
	    April 15 (28), and at a meeting of the soldiers' section of the
	    Petrograd Soviet of Workers' and Soldiers' Deputies on April 17
	    (30), “An Unfinished Autobiography”, etc.
	  
	    There is also a newspaper report of Lenin's speech upon his arrival
	    in Petrograd on April 3 (16), 1917, when he addressed workers,
	    soldiers and sailors in the Finland Station Square from the top of
	    an armoured car.
	  
	    Lenin's return, his elaboration of a concrete plan for going over
	    from the bourgeois-democratic revolution to a socialist revolution,
	    and the open exposition of his plan in the press and in speeches at
	    numerous meetings helped to orient the Party towards preparations
	    for a socialist revolution. A tremendous part in this effort was
	    played by the Petrograd City and the Seventh All-Russia Party
	    conferences held in April 1917. Some of Lenin's reports and speeches
	    at these conferences are published both according to the minutes and
	    the newspaper reports, which gives a fuller idea of their
	    content. The volume also contains “Report on the Results of
	    the Seventh (April) All-Russia Conference of the R.S.D.L.P.(B.) at a
	    Meeting of the Petrograd Organisation” on May 8 (21), 1917.
	  
	    A number of documents in the volume deal with the drafting of the
	    Party's second programme, which charted the building of a socialist
	    economy in Russia. Among them are: "Outline of Fifth 'Letter
	    from Afar'”, “Preliminary Draft Alterations in the
	    R.S.D.L.P. Party Programme”, which was the basis for
	    “Proposed Amendments to the Doctrinal, Political and Other
	    Sections of the Programme” (see Vol. 24, pp. 459-63),
	    “Report on the Question of Revising the Party Programme”
	    at the Seventh (April) All-Russia Conference of the R.S.D.L.P.(B.),
	    etc.
	  
	    The Party's policy on the basic aspects of the revolution, such as
	    war, peace and the agrarian question, is explained in the
	    “Speech at a Sitting of the Bolshevik Group of the First
	    All-Russia Congress of Soviets of Workers' and Soldiers'
	    Deputies”, “Report on the Current Situation at the
	    All-Russia Conference of Front and Rear Military Organisations of
	    the R.S.D.L.P.(B.)", the articles “The Attention of
	    Comrades!", “Too Gross a Lie”, “On the Grimm
	    Affair”, “Shame!" and others.
	  
	    The theses “The Political Situation”, which Lenin wrote
	    after the July events, were published as an article in the newspaper
	    Proletarskoye Dyelo, and that was how they appeared in
	    Volume 25. Here they are given in their original form. They defined
	    the Party's new tasks and tactics in the changed political
	    situation. Great interest attaches to the “Letter Over the
	    Publication of 'Leaflet on the Capture of Riga'", which was
	    published for the first time in the Fifth Russian edition. Lenin
	    gives important instructions in the item “On the List of
	    Candidates for the Constituent Assembly” from his
	    “Theses for a Report at the October 8 Conference of the
	    St. Petersburg Organisation, and also for a Resolution and
	    Instructions to Those Elected to the Party Congress”, part of
	    which was published in Volume 26. In a letter to Y. M. Sverdlov,
	    Lenin exposes Kamenev and Zinoviev's strike-breaking behaviour and
	    voices his confidence in the victory of the revolution.
	  
	    A considerable part of the documents consists of preparatory
	    material, such as plans, notes, outlines and theses, which show
	    Lenin's methods and thoroughness in preparing his works. The plans
	    of unfinished or unwritten articles, and plans for speeches and
	    lectures which either have not been recorded, or of which a record
	    no longer exists, are of great importance, because some of them
	    contain vital theoretical propositions and characterise the Party's
	    tasks.
	  
	    This volume contains 47 of Lenin's works which were first published
	    in the Fifth Russian edition.
	  
	    Institute of Marxism-Leninism of the C.P.S.U. Central Committee
	  
Lenin Collected Works: 
Volume 41
Preface by
	      Progress Publishers

	    The additional volumes 41. to 45 of the present edition contain the
	    most important of the new material included in the Fifth Russian
	    edition of the Collected Works of V. I. Lenin.
	  

	    Volume 41 contains works written before the Great October Socialist
	    Revolution, from 1896 to October 1917, which are an essential
	    supplement to the works published in the respective volumes of the
	    present edition.
	  

	    A great part of the volume consists of documents reflecting Lenin's
	    efforts in creating and strengthening the Bolshevik Party and
	    working out the ideological and organisational principles, the
	    programme and the rules of a new type of proletarian party. Among
	    them are: “Outline of Various Points of the Practical Section
	    of the Draft Programme”, “Record of Points One and Two
	    of Plekhanov's First Draft Programme, and Outline of Point One of
	    the Programme's Theoretical Section”, “Initial Variant
	    of the Agrarian Section and the Concluding Section of the Draft
	    Programme”, and Lenin's speeches at the Second Party
	    Congress. They show that Lenin helped the Iskra Editorial
	    Board to draft a truly revolutionary programme.
	  

	    The record of the Second Congress of the League of Russian
	    Revolutionary Social-Democracy Abroad, the January and June (1904)
	    sessions of the R.S.D.L.P. Council, “Draft Resolution of the
	    Majority's Geneva Group”, “Reply to L. Martov”,
	    “Report on the State of Affairs in the Party”, and
	    others show Lenin's struggle against the Mensheviks' splitting and
	    disorganising activity after the Second Congress of the R.S.D.L.P.
	  

	    A large group of documents written by Lenin in connection with the
	    work of the Third, Fourth and Fifth Congresses of the R.S.D.L.P. is
	    of great importance for a study of the Party's strategy and tactics
	    during the first Russian revolution. These documents contain
	    propositions on the hegemony
	    
	    of the proletariat, the alliance of the working class and the
	    peasantry, and the development of the bourgeois-democratic
	    revolution into a socialist revolution.
	  

	    Considerable interest attaches to the works connected with the
	    elaboration of Bolshevik tactics in the Duma (Parliament): the
	    report and summing-up speech on the report on the election campaign
	    for the Second Duma and other material of the Second Conference of
	    the R.S.D.L.P. (the First All-Russia Conference), the articles
	    “Are the Mensheviks Entitled To Conduct a Policy of Supporting
	    the Cadets?", “The Third Duma and Social-Democracy”,
	    “Report to the International Socialist Bureau, 'Elections to
	    the Fourth Duma'", “The Duma Group and the Majority
	    Outside”, etc.
	  

	    A number of works dating from the period of reaction reflect Lenin's
	    struggle against ideological vacillations and deviations from
	    Marxism. Lenin waged an implacable struggle against the avowed
	    opportunists, the Menshevik liquidators, and also against the
	    “Left” opportunists inside the Bolshevik Party—the
	    otzovists, the ultimatumists and the Vperyod splinter
	    group. In addition to the material already published, the volume
	    contains 14 works by Lenin shedding light on the conference of
	    Proletary's enlarged Editorial Board which condemned both
	    liquidationism and otzovism.
	  

	    The volume gives a fuller picture of the meeting held by members of
	    the R.S.D.L.P. Central Committee in Paris in June 1911. In his
	    “Report on the State of Affairs in the Party” and
	    speeches at the meeting, Lenin defined the Party's tasks in the
	    struggle against the anti-Party groups.
	  

	    The Sixth (Prague) All-Russia Conference of the R.S.D.L.P. brought
	    to a close a long period of struggle against Menshevism. By
	    expelling the Menshevik liquidators from the Party, it strengthened
	    the Party as an all-Russia organisation, capable of giving a lead to
	    the masses in afresh revolutionary upsurge. The volume contains a
	    number of documents which are of great interest for the study of the
	    Conference. Among them are: “Report on the Work of the
	    International Socialist Bureau”, setting out important
	    propositions on the new epoch, an epoch of socialist revolutions and
	    “battles against the bourgeoisie”, and on the

	    consequent sharpening of the struggle between the revolutionary
	    Social-Democrats and the reformists inside the European socialist
	    parties, and “Speech on the Organisational Question”,
	    emphasising the need to strengthen the Party's ties with the masses
	    and to combine legal and illegal work.
	  

	    The volume contains Lenin's resolution for the Cracow meeting of the
	    R.S.D.L.P. Central Committee with Party workers, “On the
	    Reorganisation and Work of the Pravda Editorial
	    Board”. This decision shows how the Central Committee, led by
	    Lenin, gave effective and concrete guidance to Pravda, the
	    Party's most important legal organ.
	  

	    In some of his works—"Reply to Liquidators' Article in
	      Leipziger Volkszeitung”, “Letter to the
	      Executive of the German Social-Democratic Party”, “On
	      the Question of the Bureau's Next Steps”, “Russian
	      Workers and the Inter national”, “How the Liquidators
	      Are Cheating the Workers”, “Resolution on the
	      Socialist Bureau's Decision"—Lenin gives a firm rebuff to
	      attempts by the leaders of the German Social-Democrats and the
	      Second International to “reconcile” and unite the
	      Bolsheviks and Mensheviks by liquidating the Bolshevik Party.
	  

	    Lenin's struggle for Party unity is characterised by the documents
	    relating to the Fourth Congress of the Social-Democrats of the
	    Latvian territory: his report and summing-up speech, and the draft
	    resolution on the attitude of the Social-Democrats of the Latvian
	    territory to the R.S.D.L.P.
	  

	    Of the documents supplementing Lenin's elaboration of the national
	    question, the volume includes: “Theses for a Lecture on the
	    National Question”, “German Social-Democracy and the
	    Right of Nations to Self-Determination”, “Note to the
	    Theses 'Socialist Revolution and the Right of Nations to
	    Self—Determination'", “On the Declaration by the Polish
	    Social-Democrats at the Zimmerwald Conference”, plans of an
	    unfinished pamphlet, Statistics and Sociology, and
	    “Speech on the National Question” at the Seventh (April)
	    All-Russia Conference of the R.S.D.L.P.(B.).
	  

	    Lenin urged the need for the workers to struggle against the danger
	    of the world war which was being prepared by the imperialists of all
	    countries, and exposed the opportunists who denied that such a
	    struggle was of any real importance,

	    an attitude which doomed the workers to a passive stand. He believed
	    that it was a major task of the revolutionary Social-Democrats to
	    conduct anti-militarist propaganda and spread the idea of
	    international solidarity among the working people. This question is
	    dealt with in the following articles: “Notes to the Resolution
	    of the Stuttgart Congress on 'Militarism and International
	    Conflicts'", “Notes to Clara Zetkin's Article 'International
	    Socialist Congress in Stuttgart'", “Anti-Militarist Propaganda
	    and Young Socialist Workers' Leagues” and “How the
	    Socialist-Revolutionaries Write History”.
	  

	    A number of documents published in the volume relate to the period
	    of the First World War, namely, “On the Slogan to Transform
	    the Imperialist War into a Civil War”, “Editorial Note
	    to the Article 'The Ukraine and the War'", “Draft Point Three
	    of the Resolution 'The C. O. and the New Paper', Adopted by the
	    Conference of the R.S.D.L.P. Sections Abroad”, “Draft
	    Resolution of the International Socialist Women's Conference”,
	    “Variant of the Draft Resolution of Left-wing Social-Democrats
	    for the First International Socialist Conference”, “Plan
	    for a Lecture on 'Two Internationals'", speeches at the Zimmerwald
	    and Kienthal International Socialist conferences, “Draft
	    Resolution of the R.S.D.L.P. Central Committee To Terminate
	    Publication of the Journal Kommunist”, “Remarks
	    on an Article about Maximalism” and others. These documents
	    show the Bolshevik tactics with regard to war, peace and revolution;
	    they explain the slogan of transforming the imperialist war into a
	    civil war, and characterise Lenin's activity in rallying the
	    Left-wing and revolutionary elements within the international
	    working-class movement round the banner of internationalism, his
	    struggle against social-chauvinism and Kautskyism (Centrism), and
	    against the Left-wing opportunist, sectarian stand and splitting
	    activities of the Bukharin-Pyatakov group.
	  

	    A number of documents written after the bourgeois-democratic
	    revolution in Russia in February 1917 contain Lenin's propositions
	    concerning the Party's attitude to the bourgeois Provisional
	    Government.
	  

	    The volume contains material connected with Lenin~ s return from
	    Switzerland to Russia in April 1917. It will be

	    recalled that the bourgeois and petty-bourgeois press started a
	    campaign of slander and harassment over Lenin and the Bolsheviks'
	    return home across Germany. This is fully exposed in the following:
	    replies to a correspondent of the newspaper Politiken and
	    to F. Ström, a spokesman of the Left-wing Swedish
	    Social-Democrats, the group's communiqué, “Russian
	    Revolutionaries' Trip Across Germany”, speeches at a
	    conference with Left-wing Swedish Social- Democrats on March 31
	    (April 13), at a meeting of the soldiers of an armoured battalion on
	    April 15 (28), and at a meeting of the soldiers' section of the
	    Petrograd Soviet of Workers' and Soldiers' Deputies on April 17
	    (30), “An Unfinished Autobiography”, etc.
	  

	    There is also a newspaper report of Lenin's speech upon his arrival
	    in Petrograd on April 3 (16), 1917, when he addressed workers,
	    soldiers and sailors in the Finland Station Square from the top of
	    an armoured car.
	  

	    Lenin's return, his elaboration of a concrete plan for going over
	    from the bourgeois-democratic revolution to a socialist revolution,
	    and the open exposition of his plan in the press and in speeches at
	    numerous meetings helped to orient the Party towards preparations
	    for a socialist revolution. A tremendous part in this effort was
	    played by the Petrograd City and the Seventh All-Russia Party
	    conferences held in April 1917. Some of Lenin's reports and speeches
	    at these conferences are published both according to the minutes and
	    the newspaper reports, which gives a fuller idea of their
	    content. The volume also contains “Report on the Results of
	    the Seventh (April) All-Russia Conference of the R.S.D.L.P.(B.) at a
	    Meeting of the Petrograd Organisation” on May 8 (21), 1917.
	  

	    A number of documents in the volume deal with the drafting of the
	    Party's second programme, which charted the building of a socialist
	    economy in Russia. Among them are: "Outline of Fifth 'Letter
	    from Afar'”, “Preliminary Draft Alterations in the
	    R.S.D.L.P. Party Programme”, which was the basis for
	    “Proposed Amendments to the Doctrinal, Political and Other
	    Sections of the Programme” (see Vol. 24, pp. 459-63),
	    “Report on the Question of Revising the Party Programme”
	    at the Seventh (April) All-Russia Conference of the R.S.D.L.P.(B.),
	    etc.
	  

	    The Party's policy on the basic aspects of the revolution, such as
	    war, peace and the agrarian question, is explained in the
	    “Speech at a Sitting of the Bolshevik Group of the First
	    All-Russia Congress of Soviets of Workers' and Soldiers'
	    Deputies”, “Report on the Current Situation at the
	    All-Russia Conference of Front and Rear Military Organisations of
	    the R.S.D.L.P.(B.)", the articles “The Attention of
	    Comrades!", “Too Gross a Lie”, “On the Grimm
	    Affair”, “Shame!" and others.
	  

	    The theses “The Political Situation”, which Lenin wrote
	    after the July events, were published as an article in the newspaper
	    Proletarskoye Dyelo, and that was how they appeared in
	    Volume 25. Here they are given in their original form. They defined
	    the Party's new tasks and tactics in the changed political
	    situation. Great interest attaches to the “Letter Over the
	    Publication of 'Leaflet on the Capture of Riga'", which was
	    published for the first time in the Fifth Russian edition. Lenin
	    gives important instructions in the item “On the List of
	    Candidates for the Constituent Assembly” from his
	    “Theses for a Report at the October 8 Conference of the
	    St. Petersburg Organisation, and also for a Resolution and
	    Instructions to Those Elected to the Party Congress”, part of
	    which was published in Volume 26. In a letter to Y. M. Sverdlov,
	    Lenin exposes Kamenev and Zinoviev's strike-breaking behaviour and
	    voices his confidence in the victory of the revolution.
	  

	    A considerable part of the documents consists of preparatory
	    material, such as plans, notes, outlines and theses, which show
	    Lenin's methods and thoroughness in preparing his works. The plans
	    of unfinished or unwritten articles, and plans for speeches and
	    lectures which either have not been recorded, or of which a record
	    no longer exists, are of great importance, because some of them
	    contain vital theoretical propositions and characterise the Party's
	    tasks.
	  

	    This volume contains 47 of Lenin's works which were first published
	    in the Fifth Russian edition.
	  

	    Institute of Marxism-Leninism of the C.P.S.U. Central Committee
	  







 
Previous

 | 
 Next 

 
Lenin
		      Collected Works 
 | 
 Lenin
		      Internet Archive 

 




Previous
 |  Next 
Lenin
		      Collected Works  |  Lenin
		      Internet Archive   