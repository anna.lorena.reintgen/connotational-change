 

Why is it still necessary to explain the
1923 events and the tactics and strategy of the Party at this time? Not
primarily to defend or to justify the then leadership of the Party. That is not
a very noble aim. it is not worth engaging in a long discussion about it. What
Brandler, Thalheimer and so on, who were then among those participating in the
Party leadership, had shown of leadership qualities, is not a question of great
significance. The main purpose of such a discussion can only be to draw out the
real lessons for the proletarian revolution by examining the mistakes made by
the Party in 1923. It is often repeated that we, the then leadership of the Party,
are not willing to admit that serious mistakes were made. That is absolutely
not the case. Such an opinion could only arise because a series of, in part,
detailed declarations, which we made to the CC of the Party in the years 1923,
1924, 1925, 1926 and still later, were not brought to the attention of party
membersand as the latter were not told they must think that we
admit to no mistakes at all. That is not so. But it does concern finding out which
type of mistakes were made and which lessons from the future can
be drawn from them. This is the nub of the question. The events of 1923
have a general significance in so far as the questions of strategy and tactics
involved are not unique questions but general issues of strategy and
tactics of the proletarian revolution which are contained within them.





Hence it follows that we will not get
clarity on questions about the communist movement in Germany until these
questions, which do not just concern the past, have been fully explained. That,
through the hitherto existing discussions, it has been neither formally or
factually explained is best illustrated by the fact that, later on in Germany,
the lessons which were drawn from the events of that year, were in fact the
conclusions of Ruth Fischer and Maslow - and this, the ultra-left line, almost
led to the break-up of the Party. The first open letteraddressed
to the German Party in 1925 by the Executive of the Communist International is
spot-on in examining the failure of Ruth Fischer's and Maslow's ultra-left line
and in liquidating and clearing it up, but it protected the legend fabricated
over 1923. It was stated then that it was not the left that was bankrupt but
some left leaders. And thus it was possible to break some of the worst leaders
from the line, to liquidate some of the crudest practices but then a new
exaggerated relapse into the ultra-left line followed.





This second relapse, which we are still
seeing today, is the empirical evidence that shows that without a genuine
clarification of the 1923 events, that is the direction of the proletarian
revolution in Germany, then a secure, clear and solid deployment of the
Party cannot be attained. One cannot fool history. It is not possible to arrive
at a clear and definite balance through little tricks and legends. The account
must be settled in full.We now ask what the October legend, on
which from 1924 onwards the ultra-left line was built, comprises and which,
today, still underpins this ultra-left line and frightens off Party members
from listening to our arguments since the question of 1923 is brought up
repeatedly. The content of this ultra~left legend can be summed up in a few
words. The core of the case is the claim that 1923 was comparable with 1917
in Russia, that is that objective conditions in Germany were just as ripe
as in 1917 Russia. Ifthe revolution conquered in Russia then it was
thanks to the leadership of the Party through its Central Committee headed by
Lenin. If the victory did not occur in 1923 it was because no such party and no
such leadership existed at the time, that the leadership both made clumsy mistakes,
was not sufficiently alert or "missed" the revolutionary moment - or even
betrayed it. Such was the left legend.





Moreover in addition the then Party
leadership was accused of betrayal, neglect and idiocies which prevented the
otherwise objectively ripe revolution - all arising fromits opportunist
attitude. The tactics and leadership in 1923 were the natural and necessary
result of its opportunist attitude, which it had demonstrated in previous
years. What was stressed as the essence of this "Opportunist attitude" was
first, the United Front tactic, as carried out by the then Party
leadership and Party. It was declared erroneous and opportunist. In part this
was direct since the United Front was absolutely rejected but in part it was
stated that the United Front "from below", - whatever that might mean - was
permissible but that the United Front "from above" was wrong because it was
opportunist. It was also stated that the leadership of the Party then had
imagined that power could be conquered in a government coalition with Social
Democracy. The evidence for this was the coalitions with Social Democracy
in Saxony and Thuringia that we had entered.





According to the left legend the third
opportunist crime was that we thought that the trade unions could be won to
Communist ideas from within. In the spring of 1924 the left comrades went
around the country declaring far and wide that the slogan of winning over
the Trade Unions was  erroneous.
This went so far that Maslow declared at a conference in Moscow in January 1924
that the slogan of winning over the trade unions should be rejected and
replaced with the call for the destruction of the Trade unions! Thepractical
consequences drawn by Maslow and the other ultra-lefts from their evaluation of
the opportunism of the previous leadership are the following, that first that
the United Front tactic had to be abandoned. This was done and entitled the
'United Front from below'. Under the mask of 'United Front from below' the
United Front was totally abandoned because, by this, it was understood that
proletarian organisations, Trade Unions as well as the lower level
organisations of Social Democracy, did not have to be approached. In this
conception of 'United Front from below' attempts were to be made to win Social
Democratic, Christian and other workers directly to joint action with the
Communist Party without regard to their organisational affiliations.





If
that is a United Front, how simple it would be. But the particular
problem of the Communist Party in Germany, and in a number of other countries,
consists precisely in resolving the question that when there exist two
tendencies in the workers' movements, how, in spite of their fundamental
differences, joint working class action can be achieved. How can the workers,
involved in their partial struggles, be brought together? Where only one party
dominates the workers movement, as was the case before the war, this situation
does not arise at all. It is a specific problem of the post-war period that
cannot be disposed of by ignoring it.





Furthermore the conclusion was drawn that
only total solutions must be advanced: the "organisation of the revolution" -
something totally misunderstood too. These ultra-left conclusions reacted quite
disastrously on trade union work. I have already mentioned Maslow's proposals
for the destruction of the trade unions' at a conference in Moscow during
January 1924. It is very characteristic that a man like Tomsky, for many years
the leader of the Russian Trade Unions, at first accepted this slogan.
That showed that he had no real idea of the conditions for struggle of the
Trade Unions in the West. This crude formulation was later corrected, but, even
then, such loopholes remained that it was possible for quite some time to
follow an ultra-left trade union line and to move to the creation of new
separate trade unions. The formula, "organisation of the unorganised," provided
an opportunity for this. The result was that the influence of the Party in the
trade unions, which had reached a high point in 1923, declined continuously and
was almost destroyed.





Yet another conclusion drawn from 1923 was
that, only in 1924, with the appearance of Maslow and Ruth Fischer was
'Bolshevisation' started. Everything done by the Party until then, even during
the stormy revolutionary years of 1918-1923 and the war as well, had been,
according to the left legend, more or less opportunism. The real Bolshevisation
only began in 1924.





Hence the further conclusion was drawn that
the older party cadre had to be expelled and the Party had to be based on the
younger cadre which had not had the unhappy experience of going through the
Spartakusbund. This 'Bolshevisation' led to the establishment of an
authoritarian regime in the Party, to the suffocation of any free discussion
and to the lack of any control over the leadership by the members. This was
considered 'Bolshevist' until the 1925 Open Letter of the Executive of the
Communist International demanded the "normalisation of Party life" and
characterised the party regime introduced by Fischer and Maslow (though with
the aid and toleration of that same Executive) as a caricature of the essence
and requirements of a Communist Party. These are the practical effects
ofthe left legend concerning the 1923 events, the politics of the then
party leadership and the opportunist sins that led to such politics. When such
practical consequences are displayed, it can be implicitly deduced that the
theoretical basis from which these consequences flowed is not a firm one.How did this legend arise? For this I must
quote from some documents, some facts which will throw light on the methods of
the leadership of the Communist international. The facts were either unknown to
most comrades until now or have been in part forgotten. Therefore they must be
quoted again. First then I must mention the fact that until December 1923, that
is well after the October retreat, the then leader of the Communist
International, Zinoviev, had repeatedly declared both in speech and writing
that the tactics agreed by the KPD in 1923 were essentially correct.
Implicitly this was the view put forward in a series of articles in Pravda which
was then published in Germany under the title, Probleme der deutschen
Revolution, which, after October, were distributed throughout Germany. I
want to quote one passage from it. Here, Zinoviev says the following about the
tactics chosen by the German Party leadership in 1923:


"There is not the smallest doubt that the
German Party has, on the whole, used the United Front tactic with great
success. By correctly using these tactics the German Party has won over the
majority of the working class - a success that it could only dream about with
difficulty two or three years ago." (p.68)





During 1923 it did not succeed in winning
over the majority of the working class to struggle for power by means of the
United Front. But this is a partial truth for without the previous success
gained through the United Front tactic, the question of taking power in 1923
could not even have been raised. Ifthe Party only had the influence
in the Metal Workers or other unions that it had under Ruth Fischer or has
today, no-one would have dared to do so. Only because of the colossal progress
that we made using this tactic was it at all possible to raise the question.
Now I want to quote yet another witness, who - after October too - intervened
to agree that the tactics and strategy of the then Party leadership had been
basically correct. This witness is Hermann Remmele. The quotation is taken from
the statement made by Remmele at a meeting of the German comrades with the
Executive Committee (of the CI) on 11th January 1924. What Hermann Remmele said
on that occasion is as follows:


"I want to refer to the time of the fascist
movement when not only in Stuttgart but also in central Germany and the north,
east and west, in fact everywhere in the country, demonstrations took place in
spite of the ban. Then, during July and the August days in central Germany, in
Thuringia, we had the situation where the workers had entirely taken over the
food supply and had confiscated lorries to drive them into the countryside to
fetch food directly from the farmers, so nobody could then be in any doubt that
we stood on the threshold of great events. Undoubtedly, the strike against
Cuno was the high point of the movement but it is my heartfelt opinion that it
was the turning point in the movement as well. As the Social Democratic Party
joined the coalition the Social Democratic workers were once more filled with
illusions.



With the entry of the Social Democrats into
the government in mid-August, there began, as it were, the ebb of the
revolutionary high tide.When we had discussions
with the Social Democrats we found that they placed high hopes on the entry to
the government of Hilferding. Social Democrats who, quite spontaneously, had
stood shoulder to shoulder with us in every struggle, who had joined in the
strike against Cuno, the whole mass of them were filled with new illusions.

Of course the decision that our Saxon
comrades should enter the government occurred because of misleading reports and
information. The decision was taken on the basis of the conviction that the
arming and mobilisation of the masses existed already to such an degree that
such a thing could be risked. We thought the decomposition of the enemy to be
much further advanced than it actually was .... Was it correct to prepare for a
decisive struggle at the stage at which we found ourselves? Could we arm, could
we fix the date and prepare for the decisive battle? We thought not. We said
that because of the particular structure of Germany, its class relations and
class forces, we do not yet stand at the stage at which we could set the
exact date for a decisive struggle. We said that before we reach the
decisive struggle we will have to go through a whole period with a series of
violent armed clashes ...





This passage can be found in the pamphlet Die
Lehren der deutschen Ereigneisse, pp.40-44. It was Hermann Remmele who said
that in January 1924 with the immediate experience of 1923 behind him.





A change first occurred - as can be
established in the documents - in the Executive's evaluation of the tactics of
the German Party in December 1923. Only subsequently have we been able
to find out the precise date and the reasons for this change. How did the
change come about? How was it that Zinoviev and the Executive, which until then
had approved of the tactics in substance, made a 180 degree change of course.
The context was the following: on the 13th December - if I do not err - comrade
Radek gave a speech in a large Party gathering in Moscow where he intervened in
the then growing Trotsky debate when he declared that, if the majority
of the Russian CC turned against Trotsky, then not only he but the
leaderships of the French and German parties, that is the main parties in the
West, would turn against the Russian CC majority. That was on the 13th
December.





A few days later Zinoviev sent a letter to
the leadership of the German Party in which he completely changed course and
opened up a violent attack against it. In this way there began the general
witch-hunt against the leadership. The real reason for the turn was a panic in
the leadership of the Russian party caused by Zinoviev because he took
seriously Radek's assertion that the German Party leadership would back Trotsky
against the Russian majority. That was the cause of the turn. The story had
nothing at all to do with any event in Germany, France or the Comintern as
such. It was simply the result of a manoeuvre in the internal Russian factional
struggle. We only heard of Radek's speech very much later. As we, at the 5th
Congress, first heard about the real reason for the turn in Moscow, the
campaign, the bombardment was in full swing and Maslow and Co had been
unleashed. The peculiar thing about it was that Radek's assertion was a total
invention. Nobody had authorised him to say that we would back Trotsky if he
was attacked. When we first heard about the dispute with Trotsky we said that
we would have to have more facts about the basis of the dispute before we made
up our minds. As soon as something was known about it I wrote an article in Die
Internationale which was opposed to Trotsky's ideas. Therefore it
was not even our genuine view about the internal Russian factional struggle but
an imaginary one.It was the perfidious letter from Zinoviev
mentioned above, which followed Radek's speech, which introduced the turn. The
fact that Clara Zetkin and Wilhelm Pieck were both asked where they stood on
the October 1923 retreat and whether or not it was correct, was even
more typical. Previously the Executive had always claimed that it was correct.
Now Zinoviev starred to oil out of it. Despite repeated requests he refused to
put down anything in writing about it. As a result the ultra-left legend was
given the green light. Since there was no official decision to the contrary,
Maslow could claim that this retreat was wrong, had been a betrayal and had
ruined the revolution. This conclusion was drawn by Maslow and Ruth Fischer in
the crudest and most insane way. At the Rhineland-Westphalia district Congress
on 6th March 1924, in the presence of Ruth Fischer, a resolution was submitted
which said on this point:


'This conference declares that a decisive
revolutionary battle was historically necessary in October of last year.
Neither the avoidance of the battle, nor the substitution of the final struggle
by so-called rearguard actions, partial actions or similar activities was
admissible.'





Reading these things in cold blood one can
only wonder how a party majority, how an Executive, could accept such total
nonsense. In effect it is saying that we were totally unprepared for the
struggle, the decisive battle, but, in spite of that, the showdown had to be
attempted in October - such an assertion is completely contradictory but it is
swallowed whole. In Germany that was the high-point of the legend. It attained
its highest point in Russia a series of speeches by Trotsky. One of them
was made in Tiflis. In this his intention was not to take aim at the
majority of the leadership of the German Party but he wanted to demonstrate the
following - that the revolution was not victorious in Germany in 1923 because
we had at the head of the International not Lenin but the opportunist Zinoviev.
He wanted in other words to strike at Zinoviev, at that time his bitterest
enemy and who, with Kamenev and Stalin, were waging the fight against him. As
part of this dispute Trotsky published a text, Lessons of October, in
which he set down his detailed view, first of the Russian October of 1917 and
then of the German October of 1923. And here can be found the October legend in
its bluntest, and if one can use the term, classical form. The passage to be
considered is as follows:





"Point 2. Germany. the question of the
defeat of the German proletariat in October is even more interesting. In the
second half of last year we saw there a classical demonstration of the fact
that a quite extraordinarily favourable revolutionary situation of world
historical significance can be missed."





That was in fact the lesson. An
'extraordinarily favourable revolutionary situation' had been missed because of
the leadership and, by this, Trotsky meant, primarily the leadership of the
Comintern.But now at this point, both ideas and
public statements in official circles in both Germany and Russia underwent a
change. This involved, in opposition to Trotsky, an attempt to show that both
his conception of the Russian October 1917 as well as the German October 1923
was quite wrong and there was a concern as to whether it was correct that, the objective
conditions for revolution in Germany in 1923 as asserted by Maslow,
tolerated by Zinoviev and sharpened by Trotsky, had been present, as they had
in Russia during October 1917. From now on this idea started to be revised in
official circles - the left legend had to be changed. I want to quote here some
particularly suggestive passages but by no means the only ones. Let me mention
first some statements by comrade Bukharin from an article on the Lessons of
October in a Pravda feuilleton:





"According to comrade Trotsky's conception,
here the mistakes consist in a 'classical' moment being missed. At whatever
cost the decisive struggle had to be taken up and the victory would have been
ours. Here comrade Trotsky makes an analysis resembling October 1917 in
Russia in every respect.The same medicine had to be taken there as
here. Here, under the pressure of a Lenin, action was decided upon and we
conquered. Without Lenin's pressure - no such decision was taken and thus the
suitable moment was missed. But now, influenced by the October Revolution in
Russia, the forces for the decisive struggle were insufficient. That, according
to Trotsky, is the schema of the German events.





"But also here, in the evaluation of 1924,
we had a schematised and sad realm of grey abstraction before us. Comrade
Trotsky describes how history would have been written if the majority of the
members of the Russian CC had been opponents of the uprising; then it could be
said that our forces had been too meagre, that the enemy was frightfully
strong, etc.





"All that is only superficially convincing;
yes one probably should write history in this way but that in no way shows that
the forces of the German Revolution in October were wrongly estimated. It
was above all erroneous that it was a 'classic' moment. Social Democracy proved
itself much stronger thanwe had thought. Here an analogy with the
Russian October is hardly relevant at all. In Germany there were no armed
soldiers on the side of the revolution. We could not put forward the demand for
'peace'. There was no peasant agrarian movement. There was no other party like
ours. Although everyone disregarded it, Social Democracy had not outlived its
time, so these concrete facts had to refuted. At the time of the decisive
events the ECCI declared for the October line (emphasised by Bukharin). Now
as objective conditions and also the right leadership failed as a force, the
failure 'was greater than necessary'. Comrade Trotsky, who supported precisely
this wing of right opportunists who tended to capitulate, and had repeatedly
fought the left, delivers a 'profound' theoretical analysis for their
conceptions and so strikes a blow at the leading circles of the CI.





"But it is wholly inadmissible to persist
in mistakes, such as the ones that comrade Trotsky still clings onto today. One
of the lessons (the real lessons) of the German October is that before any
rising there must a huge participation of the masses. But such work was very
backward. For example during the uprising in Hamburg there were no workers'
councils and our party organisation was unable to draw into struggle the tens
of thousands of strikers. Soviets were absent from the whole of Germany. In
comrade Trotsky's conception it would have been correct if there had been the
'substitution' of the Soviets by factory councils. In reality though the
factory councils could not replace them as they could not unite the whole
class, including the most backward and the most indifferent, as the Soviets do
at the critical moments of the class struggle."





So Bukharin wrote against Trotsky in 1925
and note well that it was the same Bukharin who, together with the majority of
the Politburo and the majority of the Russian CC, had led the struggle against
Trotsky at the time. Then Kuusinen, the present Comintern secretary, followed
by Krupskaya, Lenin's wife and finally Stalin intervened in the debate on the
revision of the October legend. in an article attacking Trotsky, Kuusinen said:
"It must here be added that whether such moments did or did not exist, not only
Trotsky, but also Maslow and other comrades, ignored a possible army of
millions on the side of the revolution."





Attacking Trotsky, Krupskaya said:


"Comrade Trotsky wants October to be
studied. But he wants the role of individuals and opinions in the Central
Committee to be studied. But what should be examined is not these but the
international situation situation during the October days as well as the
relationship of class forces at the time. Trotsky overlooks this
question. So he underestimates the role of the peasantry. Besides the Party at
the time of October must be studied. Trotsky writes a lot about the Party, however
he confuses the party with its General staff.Detached from the
Party the party leadership would not have been able to win.





"Comrade Trotsky does not understand at all
either the role of the Party or the function of unanimity within the Party
also. For him the Party is the same as its general staff. Today too comrade
Trotsky thinks that Bolshevisation consists of the selection of the appropriate
general staff. Such a purely administrative view is incorrect. More correct
is the assessment of the role and significance of the masses as the Bolsheviks
did in October. Trotsky forgets this aspect of the question.





"When assessing the German events comrade
Trotsky underestimates the passivity of the masses."





Here comrade Krupskaya also stresses that
it is essential to examine and establish the objective circumstances and the
relative class forces in October 1923 and not the subjective factors stressed
by Trotsky. Lastly I want to quote some remarks by Stalin which, if not
directly, are at any rate closely linked to this and which he made to comrade
Wilhelm Hertzog who asked him about the conditions for a proletarian revolution
in a country like Germany. What Stalin said then throws a very clear light on
the process of how the question should be correctly posed and on the question
of what the conditions for a revolution in Germany are. Stalin said:





"This circumstance is not the only
favourable condition of the German revolution. For the victory of the
revolution to occur the Communist Party must represent the absolute majority of
the working class and it must be the decisive force within the working class.
Social democracy must be unconditionally smashed and exposed, reduced that
is to an insignificant  minority of the
working class. Without this the dictatorship of the proletariat is unthinkable.
In order to win, the workers must be inspired with determination and the
working class masses must be led by a party which possesses their undisputed
confidence. When two competing parties of equal strength exist within the
working class,then, even with unusually favourable conditions, a
permanent solid victory is impossible. Lenin, especially in the period
before the October Revolution, insisted on this priority as the most essential
pre-condition for the victory of the proletariat."





What Stalin says here is that a lasting solid
victory of the Communist Party and the proletarian revolution is only possible
when the Social Democratic Party has been reduced to an insignificant minority,
and when the great mass of workers uniformly follow the Communist
leadership already. If this question, of whether the Social Democratic Party
had been reduced to an insignificant minority, is asked in respect of 1923,
then it must be totally rejected. This was absolutely not the case.





With the revision of the left October
legend, a process was begun but not finished and therefore we must deal
directly with the questions posed by comrade Krupskaya: the question of
the objective class forces, that of the relative strengths and influence
of the KPD and SDP and that of the real factors influencing events.Contents  | 
      Forward  | 2. The Class Forces in Russia in 1917 compared with Germany in 1923Thalheimer Archive not brought to the attention of party
memberswhich
typemistakeslessons from the future can
be drawn from themnot uniquegeneral issues of strategy and
tactics of the proletarian revolution which are contained within themopen letterthe direction of the proletarian
revolution in Germanythat 1923 was comparable with 1917
in Russiaopportunist
United Front tacticgovernment coalition with Social
DemocracySaxonyThuringiawinning over
the Trade Unionsrejectedthe destruction of the Trade unionsparticularat firstpractical effectsessentially correctthe question of taking power in 1923
could not even have been raised.Undoubtedly, the strike against
Cuno was the high point of the movement but it is my heartfelt opinion that it
was the turning point in the movement as well. As the Social Democratic Party
joined the coalition the Social Democratic workers were once more filled with
illusionsthe whole mass of them were filled with new illusionswe do not yet stand at the stage at which we could set the
exact date for a decisive struggleDecember 1923Trotsky debatebut the
leaderships of the French and German parties, that is the main parties in the
West, would turn against the Russian CC majorityopposed to Trotsky's ideasOctober 1923 retreatRussiaTiflisextraordinarily favourable revolutionary situationobjective
conditions for revolution in Germany in 1923makes an analysis resembling October 1917 in
Russia in every respectthe forces of the German Revolution in October were wrongly estimated. It
was above all erroneous that it was a 'classic' moment. Social Democracy proved
itself much strongerAt the time of the decisive
events the ECCI declared for the October linehowever
he confuses the party with its General staffMore correct
is the assessment of the role and significance of the masses as the Bolsheviks
did in October this aspectWhen assessing the German events comrade
Trotsky underestimatesreduced that
is to an insignificant  minority of the
working class. Without this the dictatorship of the proletariat is unthinkablea party which possesses their undisputed
confidence. When two competing parties of equal strength exist within the
working classLenin, especially in the period
before the October Revolution, insisted on this priority as the most essential
pre-condition for the victory of the proletariatuniformlybegunKrupskayaobjective class forces