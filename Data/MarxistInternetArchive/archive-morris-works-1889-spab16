
      Ladies and Gentlemen, - I must explain to you that I am put, almost at the last
      moment, to make a few disconnected remarks in the place of a gentleman whom we
      should have much liked to see - Mr. Boni, who now holds a position under the
      Italian Government which enables him to be of great service to us in stopping the
      unfortunate flow of restorations in that country. He could have given to us most
      interesting and encouraging information as to what is now going on in Italy.
    
      Now as to what I have to say to you. Our Society, so far as I can make out, has two
      sides to its work, which it does not always receive sufficient credit for,
      especially at first sight. Its first duty is to try to prevent the careless, or
      what I may call the commercial, destruction of valuable monuments of art and
      history. The second duty springs from the first, and that is the prevention of
      ignorant and, consequently, injurious attempts at the preservation of these ancient
      buildings. That appears at first sight to be a paradox, but you will see it is not.
      It is scarcely necessary, perhaps, for me to say much about the value of the
      ancient buildings we wish to preserve. All of us here present are sure that they
      are valuable. Some of us know that we feel a definite and distinct pain when an old
      building is destroyed - and not only pain but also shame, because we who live in a
      period of the world which is sometimes called civilised (I am not very fond of the
      word), certainly might have hoped that people by this time, after such a long
      experience, would have got to be rather wiser than to destroy a thing which need
      not be destroyed and the existence of which was a benefit to society. But under
      that feeling of pain and shame we want sometimes to be able to express our feelings
      to other people with considerable emphasis. I will tell you the kind of thing I
      should say to a total Philistine if I were talking to him. You must remember that
      the greater part of the public do not care a snap of the fingers for these
      buildings. They have no idea of the history of our ancient buildings, and their
      complete ignorance places them in a position which it is very difficult for us to
      deal with. Still a Society like ours is nothing if it is not aggressive, therefore
      we have to try to convince even the most ignorant; and to do that properly, we
      ought to be able to get in the habit of putting ourselves in their position.
      Therefore, I do not excuse myself much for giving you some downright, sturdy,
      common-place reasons why old buildings should not be pulled down. Besides the
      persons who have some definite knowledge of history and art, and therefore do not
      want these buildings pulled down, there are a number of people who, without such
      knowledge, say that they do not want old buildings pulled down because they feel or
      see that an old building is pretty and romantic. When they say that, Podsnap laughs
      - Podsnap political, Podsnap social, Podsnap scientific - and yet these people
      whose ideas are given in such simple terms are after all quite right, while Podsnap
      is simply a loud-voiced ass who does not understand the feeling of a human being.
      For to those who are not deficient in the qualities which make a man: outside the
      satisfaction of generous impulses for the public welfare, which is the very highest
      of all pleasures: outside that and the emotions which deal with the other human
      beings we know best and closest, which is the second great pleasure of life: there
      is a third source of pleasure, and that is the capacity or habit of seeing beauty
      and romance in the daily life of the world generally. I say that is almost the
      greatest of all harmless pleasures at all events, apart from those other pleasures
      were are so sweet in the mouth but bitter in the stomach. So let Podsnap call us
      sentimental, and, for me, I say in return that I am a sentimentalist in
      all the affairs of life, and I am proud of the title. So we will say that if a
      building is pretty and romantic (i.e., if it be beautiful to the eye, and
      recalls to the mind the interest of the life of times past) there must be very
      strong public reasons for its destruction: no private onescan be
      strong enough. As to the public reasons, I must say in the course of my life I have
      never known one serious, real public reason for the destruction of a beautiful
      building.
    
      Of course there are heaps of private reasons, such as wanting to make another
      £500 a year for oneself, or the adding 2s. 6d. to the dividend of a company
      one is interested in; but such reasons as these are not sufficient for the
      destruction of a building which is at once pretty and romantic. The capacity of
      seeing beauty means the capacity for art. If you have a population which cannot
      see beauty, you will have a population which cannot produce
      beauty: a population which, even if a small amount of beauty is produced by the
      élite, will not generally acknowledge that beauty.
    
      In order to make in any country a poet or a literary man, you must have an audience
      which can understand his works though they could not produce them, perhaps; and the
      same thing is true of the arts which depend on the sense of sight. Artists cannot
      live amongst a people careless of the arts; art dies in such an atmosphere; and
      remember that the greatest side of art is that art of daily life which historical
      buildings represent; the enjoyment and, above all, the practice of which can be
      seen by reasonable and thoughtful people to be the one pleasure which could not be
      taken away from them by accident, or sickness, or by sorrow. As for romance, what
      does romance mean? I have heard people mis-called for being romantic, but what
      romance means is the capacity for a true conception of history, a power of making
      the past part of the present. I think that is a very important part of the pleasure
      in the exercise of the intellectual faculties of mankind which makes the most
      undeniable part of happiness. I say, then, that to disregard these two things does
      not indicate progress, is not practicality, but degradation. It is degradation and
      not progress to destroy and lose these powerful aids to the happiness of human life
      for the sake of a whim or the greed of the passing hour.
    
      It has been most truly said at our meetings that these old buildings do not belong
      to us only; that they have belonged to our forefathers, and they will belong to our
      descendants unless we play them false. They are not in any sense our property, to
      do as we like with. We are only trustees for those that come after us. So I say
      nothing but absolute necessity can excuse the destruction of these buildings; and I
      say, further, that such a necessity has never yet existed in our time. For example,
      you have heard about the destruction of houses which is being carried on in Naples
      with the expressed intention of destroying the slums of that city and rebuilding
      it. That opens a question about as wide as the whole life of man at the present
      moment, and which cannot be gone into fully here; but this much one may say, that
      it is not the existence of these buildings, raised by our forefathers, which has
      caused the slums, either in Naples or London, but rather the same fatalistic and
      slothful ignorance which has destroyed the old buildings. It is exactly these two
      things together which, while they allow the careless and wanton destruction of what
      should not be destroyed, prevent the due, proper, and happy change in life which
      ought to be going on around us every day.
    
      I do not think I need dwell further on this matter of the mere wanton destruction
      of works of art and monuments of history; I will only add, Do not be ashamed of
      saying, with John Ruskin, that any old building in a civilised country is, at all
      events, worth the ground it stands upon. Surely, if we in England are not wealthy
      enough to be able to indulge in the pleasure of buying a piece of ground that bears
      upon it a monument of beauty and romance, we are poor indeed.
    
      Now comes the second part of the subject: viz., our dealing with ignorant and
      futile attempts at preserving these old buildings, which lead more or less to their
      destruction. At first sight it may seem a more intricate matter than resisting
      their brutal destruction, and yet it seems to me that the views of the Society
      follow naturally on our wish to preserve these old buildings.
    
      And if you care to preserve the buildings, in the first place, you will certainly
      feel it your duty to sacrifice some whims, and, perhaps, some convenience of the
      passing hour to your love for them. In this world it is seldom you can get an
      absolute and clear gain. In one way or another one must pay for almost every gain
      one has; and I think we may call on the public generally to sacrifice some time or
      convenience if that be necessary for distinct intellectual gain; but the gain must
      be real. In saving an old building, what are we to save to make the sacrifice
      above-said worth while? Surely not guesses about the buildings, not hearsay about
      them, but the buildings themselves - all that they are and mean.
    
      The value of these buildings has more than one side, they have many parts, and we
      are unable to discriminate between the value of one part and another. If we be
      asked which side of them, what part of them, we want to preserve, we must say our
      only possibility of preserving them at all is to preserve them whole as they are.
      That is, to preserve all the beauty and all the romance which is left in them. Less
      than that is putting our passing whims upon the building as the thing we want to
      pass on to our descendants, instead of the piece of art, the monument of history
      itself.
    
      Now, this integral preservation involves questions with regard to the modern use of
      these buildings, and other matters. As to the modern use of them, and their
      connection with us as possessing ordinary utility for the passing day, people may
      say, You talk about history; but are we not part of history? Are we not to link
      ourselves with the history of these old buildings? Most unquestionably we are, or
      else if we cannot, they will not be to us of that personal importance as relics
      which they are; only the history must be real and not false. In this matter a trap
      awaits the unwary. People say, for example, that men like Edington, Wickham, and
      the rest deal with the buildings according to the convenience and needs of the day.
      They restored, developed, and altered them, and why should not we? They certainly
      did so, and though we may sometimes regret that a Norman building was turned into a
      Perpendicular one, we cannot help feeling that these people had a right to do as
      they did. But why? Because we are perfectly conscious that when they took away
      something they left us a living thing in its place.
    
      It seems to me that the life of these buildings lay in the traditions of the human
      race handed down from generation to generation, and the architects of the later
      Middle Ages worked as the traditions of their own times compelled them to work.
      Tradition compelled them to set the mason and the carpenter to work on the
      buildings, according to their traditions. But as for us, the tradition of
      our times forbids us to pretend to do this, and to imitate them mechanically in
      their actions.
    
      We could not do so with a clear conscience; we should be masquerading in the
      past-master's clothes. It seems to me that for us to pretend to be William of
      Wykeham and the like, would not be acknowledging the continuity of history, but
      ignoring it. In their time tradition was not only alive in the world at large, but
      it was alive in a very definite form, in the actual craftsmen themselves, the
      people who had to deal with the old buildings. In these days tradition is
      absolutely dead among them. Therefore we shall better carry on the continuity of
      history by gaining genuine knowledge of the past, by careful study of it, and by
      acting with conscious public spirit in the present than by imagining an unbroken
      mechanical connection between the present and the past, which does not exist. Art
      of the present is as much dissevered from our ordinary ways of early life as the
      position of Queen Victoria is from that of Alfred the Great, and that of
      Nasr-ed-Deen is from that of Nourshivan, or as that of the present City companies
      is from the old craft-guilds whose names the former take. The upshot of the matter
      is that we must look things in the face. We do not want to deceive ourselves. Let
      us have sentiment, but not false sentiment.
    
      The real, the essential purpose, in this day, of our old buildings is to be
      instructiverelics of the past art and past manners of life. If you can do so,
      without altering them or making shams of them, use them for ecclesiastical, civic,
      or domestic purposes, just as we are using this building now. That is the best way
      of preserving them.
    
      But, I repeat, do not imagine that is now their essential, their primary purpose.
      It is not. It is only their secondary purpose. I say they themselves are of
      infinitely more value than any use they can be put to, because as mere bricks and
      mortar you do not need them; you can have as many convenient buildings as you
      choose to pay for. There is no difficulty about that, whereas if you destroy the
      old ones and the continuity of history which they contain, where are you going to
      get all that again? Nowhere and never! That is a thing past the power of all of us
      to do. Therefore, I say that the straining of the ideas of the continuity of
      history - although there is a certain interest in it - is now pedantry; not wholly
      dull and stupid pedantry, but founded on a misconception, and certainly as far as
      our business is concerned - the preservation of ancient buildings - it is apt to
      lead to the destruction of buildings and their falsification. Persons with that
      false idea of the continuity of history are loth to admit the fatal words, `it
      cannot be, it has gone.' They believe that we can do the same sort of work in the
      same spirit as our forefathers, whereas for good and for evil we are completely
      changed, and we cannot do the work they did. All continuity of history means is
      after all perpetual change, and it is not hard to see that we have changed with a
      vengeance, and thereby established our claim to be the continuers of history.
    
      Sometimes I think we do not quite realise how great that change is. I said just now
      that tradition was dead amongst the craftsmen, but that does not really express the
      condition of things about the craftsmen in civilized countries. I should have said
      that so far as those people who work under a master are concerned, craftsmanship is
      absolutely dead itself; whatever there is left of craftsmanship abides at present
      with that part of the professional classes who use their own hands and whole only
      master is the general public: that is, with the painters, etchers, machine
      inventors, experiment makers, or scientific men, and, above all, with the surgeons.
      You cannot get a good house built now-a-days, but if you want your leg cut off - my
      word! how well you can get that done (I am quite in earnest, I think the surgeons
      are a very remarkable body of craftsmen). Now the work of the old craftsmen was as
      resourceful, as ingenious, and delicate as the best of the modern professional
      handicraftsmen. That craftsmanship and that feeling you cannot restore by putting
      men to work, who necessarily, not by their own faults, but by the conditions of
      society in which they live, lack all these qualities, and, above all, men who have
      not behind them, as their forefathers had, the masterful traditions which compelled
      them to do work which was beautiful, delicate, ingenious, and resourceful. It has
      gone. Just consider the immeasurable difference between the surroundings of the
      workmen of the present day and the workmen of the fourteenth century. Consider
      London of the fourteenth century: a smallish town, beautiful from one end to the
      other; streets of low whitewashed houses with a big Gothic church standing in the
      middle of it; a town surrounded by walls, with a forest of church towers and
      spires, besides the cathedral and the abbeys and priories; every one of the houses
      in it, nay, every shed, bearing in it a certain amount of absolute, definite,
      distinct, conscientious art. Think of the difference between that and the London of
      to-day, whose houses either have no attempt at ornament or architecture about them,
      or where they have ornament, make us regret that there is such a thing; and at
      least where art exists, it is paid for by the foot, and only comes in as part of
      the conditions of contract which rules all society among us; whereas in the old
      town, the ornament grew spontaneously out of the method of work. The modern
      conditions of labour were not known in the old time. Yet even this difference
      between the towns in the fourteenth and nineteenth centuries does not express the
      difference which exists between the workmen of the two ages. It is far greater than
      that. Just consider what England was in the fourteenth century. The population,
      rather doubtful, but I suppose you may take it at about four millions. Think then
      of the amount of beautiful and dignified buildings which those four millions built,
      of whom there were, of course, the regular proportion of women, children, and
      idlers. As we go from parish to parish in England we see in each a church, which
      is, or at all events has been, beautiful, and in every town we see important and
      sometimes huge and most elaborate buildings, the very sight of which fills us with
      a kind of awe at the patience and skill which produced them.
    
      But further, we have to consider not only those churches and houses which we see,
      but also those which have been destroyed, and all those other beautiful abbey
      buildings, e.g., of which only a few relics have been left, and of which Cobbett
      truly says that at the time of the Reformation England must have looked like a
      country which had been subject to a cruel invasion. Those buildings also, though
      they contained little upholstery, contained much art: pictures, metal-work,
      carvings, tapestry, and the like, altogether forming a prodigious mass of art,
      produced by a scanty population. Try to imagine that. Why, if we were asked
      (supposing we had the capacity) to reproduce the whole of those buildings with
      their contents, we should have to reply, `The country is not rich enough; every
      capitalist in the country would be ruined before it could be done.' Is not that
      strange? Surely there must be some reason for that. It must be clear to us, when we
      think of it, that these buildings could never have been built but by people who had
      a habit of building in that sort of way, without effort, men who had a habit, I
      say, of working, every one of them, more or less as artists. Where are the
      artist-workmen now to do our ordinary every-day work? What has become of their
      method of work? Such an ordinary thing as a wall, ashlar or rubble, cannot
      at the present day be built in the same way as a medieval wall was. Any architect
      who has tried it who may be present will tell you that what I say is correct. I say
      the unconscious habit of working the stone in a certain way cannot be supplied
      artificially, and in such habits lies the very life of the buildings; it was the
      language in which the story was told when stories were told in buildings. If you
      have destroyed the language, can you restore the style of the story? You can have
      but a diagram of it, a caput mortuum. The language gone, the literature is
      also departed. In short, the art of that time was the outcome of the life of that
      time. Consider then the daily life of to-day, and what a contrast was the life of
      the old workmen to that of the workmen of to-day. Now, they work consciously for a
      livelihood and blindly for a mere abstraction of a world-market which they do not
      know of, but with no thought of the work passing through their hands. Then, they
      worked to produce wares, and to earn their livelihood by means of them, and their
      only market they had close at hand and knew it well. Now, the results of their work
      passes through the hands of half a dozen middlemen. Then, they worked directly for
      their neighbours, understanding their wants, and with no one coming between them.
      Huckstering, which was then illegal, has now become the main business of life, and,
      of course, those who practise it most successfully are better rewarded than anyone
      else in the community.
    
      Now, people work under the direction of an absolute master whose power is
      restrained by a trade's union, in absolute hostility to that master. Then they
      worked under the direction of their own collective wills by means of trade guilds.
      Now the factory hand, the townsman, is a different animal for the countryman. Then,
      every man was interested in agriculture, and lived with the green fields coming
      close to his own doors. In short, the difference between the two may be told very
      much in these words: In those days daily life, as a whole, was pleasant, although
      its accidents might be rough and tragic. Now, daily life is dreary, stupid, and
      wooden, and the only pleasure is in excitement, even if that pleasure should be
      more or less painful or terrible. Surely the vile and disgraceful heaps of misery
      and fatuity that should not be called buildings, which curse our towns and curse
      the historyof the country, are truly and properly the expression of our life, which
      is lived in and round about them, just as those which it is our business to
      preserve, and those which have been destroyed - more's the pity - were in their
      beauty and manliness the expression of life which was lived then. I do not want to
      rail at the present days. They are a part of history, and they have their tragical
      side which interests me immensely; but here is the point, how can you patch that
      `old' with this `new'? It is quite impossible, it is preposterous. It is against
      all proverbial wisdom, from the days of Adam downwards. Is not the gulf clear and
      startling between us of the present day, and those of the past day? If these
      buildings are to be really preserved by us for our use, that can only be done by
      understanding the gulf which lies between us and the past. The only common sense
      way in which to treat them, putting the sentiment altogether aside, is either to
      sweep them away at once and think no more about them, or to treat them as relics of
      the past, to be preserved even at the sacrifice of convenience if that be
      necessary. As to restoration, surely you must be in your own mind convinced that
      the restoration of such buildings is impossible. All the knowledge and mastery over
      nature, which modern civilization has given us, cannot change the executant, whom
      our system forces to be a machine, into being (for the occasion) a free artist
      directed by necessary tradition. You cannot both eat your cake and have it. You
      have made the workman a machine, and you have gained certain advantages thereby. Be
      content with those advantages, and do not try to make your machine do the work of a
      man, because that is impossible. What we have to do is simple enough. On one hand
      to prevent sordid destruction of these ancient buildings at the hands of private
      persons for `jobs' and for gain. We have also to prevent ignorant restoration,
      so-called, of the poor remains of our forefathers' are that is left us. That is our
      work to-day as a Society, and I must say although I am engaged with other
      societies, who might consider themselves more useful, I think the work of this
      Society is thoroughly worth doing. Even if we are more or less mistaken about the
      value of these buildings, even if they are not worth so much as we think, they are
      worth preserving. We can repair a building and watch it when we have repaired it,
      so that it shall meet its death at the hands of the slow perpetual decay of nature,
      and not by the sudden spasmodic folly of man. I think it is worth while doing that.
      So suppose we leave it to the future ages to settle how much worth it is. Would not
      that be worth while? By a little care and patience we may preserve these old
      buildings for another 500 or 600 years. And, meantime, I will promise you that in
      another 100 years at most, the change will be so great in this country that we
      shall hardly recognise it then for what it is now. So let us do what seems to us
      our duty in this matter, and let those that come after us do theirs; that will
      suffice; but my belief is that our descendants will thank us for our share of the
      work.
    
      Mr. Morris then alluded to the work of the Society as contained in the Report, and
      concluded with an appeal for help in the case of Inglesham Church, for the repairs
      of which the Society is engaged in raising funds.
    
Address at the Twelfth Annual Meeting - SPAB (1889).
    
      1. 3 July 1889: Before the Annual Meeting of SPAB held at ?.
    
      1. [Untitled] in SPAB Report, 1889, (London 1889), pp. 62-76.
    
      2. As `Address at the Twelfth Annual Meeting, 3 July, 1889', in William Morris:
      Artist, Writer, Socialist, ed. May Morris, I, pp. 146-147. This version omits
      the first paragraph, the first sentence of the second, and the final paragraph.
    
      1. Eugene LeMire in The Unpublished Lectures of William Morris, (Detroit:
      Wayne State University Press 1969, p. 315, states that `May Morris's title and date
      are not corroborated by the SPAB Report, 1889'. This must be an oversight.
    
The William Morris Internet Archive : Works
