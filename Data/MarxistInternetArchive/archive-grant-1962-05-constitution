Written: May 1962
Source: Socialist Fight, vol. 4 no. 4 (May 1962)
Transcription: Francesco 2008
Markup/Proofread: Manuel 2008The preparations to introduce a new constitution in Russia are 
  an indication of the stirrings taking place beneath the surface calm of the 
  totalitarian system inside the Soviet Union.

  The first constitution of the Soviet Union was a revolutionary document which 
  tried to place full power in the hands of the working class through the Soviets. 
  By this means in a ‘semi-state’ the rule of the working class or 
  in Marxian terms the dictatorship of the proletariat was to be wielded. This 
  was intended to be the most revolutionary and democratic doctrine in history. 
  In the early days of the revolution it was realised at least in part.

  After the Stalinist reaction there came the introduction of the Stalinist Constitution 
  declared by its authors to be the “most democratic in the world.” 
  This constitution, even formally, was a step backward in comparison with the 
  flexible instrument of the past. It established ‘parliamentarianism’ 
  on the capitalist model.

  Of course in words it was very democratic. The only thing prohibited in Russia 
  would have been an attempt to exercise the rights declared in the Constitution 
  and enshrined in its clauses. The right to vote for the person of one’s 
  choice, free speech and press, freedom of the individual and all other freedoms 
  were established on paper. But they went side by side with one of the bloodiest 
  purges, or one-sided civil wars in the history of man. Practically the whole 
  of the Bolshevik leadership which carried out the revolution was murdered. Hundreds 
  of thousands and even millions of workers and peasants were drafted forcibly 
  to Siberia to work as slave labour.

  The most ghastly farce were the totalitarian elections with but a single candidate, 
  ‘a free choice’ where the electors voted for the picked tools of 
  the ruling bureaucracy. It was in these years that there was the abolition of 
  equality, or to be exact, the strict limitation of privileges, including the 
  fact that no official was to have a higher wage than a skilled workman. The 
  idea of equality was declared a capitalist prejudice. The uncontrolled bureaucracy 
  greedily grabbed a bigger and bigger share of the national income for themselves, 
  at the expense of the ordinary workers and peasants. The Stalinist constitution 
  was their constitution. The real crowning of the constitution was the last ‘election’ 
  before the war when ‘Stalin the Great’ revealed himself as a true 
  magician by receiving more than 105 per cent of the votes in his constituency! 
  This is a fitting comment on the farce of the constitution.

  However undoubtedly in the Bonapartist, or dictatorial system in the Soviet 
  Union the Stalinist constitution was originally intended as a check and brake 
  on the worst excesses of the uncontrolled bureaucrats. It was intended to be 
  a whip in the hands of the autocrat Stalin, who was not averse to leaning on 
  the working class on occasions, as all other Bonapartist dictators have done, 
  and occasionally giving blows to the officialdom, the heads of the Party, civil 
  service, armed forces and the general administrative hierarchy, which also included 
  the newly privileged managers and other controllers of industry. They threatened 
  to devour the state with their privileges, greed and corruption. Hence the Stalinist 
  constitution was originally intended to be a system of checks and balances. 
  It was not only intended to be window dressing. However in this purpose it was 
  bound to fail for the specific reason, that turning from one solution of their 
  problems to another, the bureaucracy could not admit that they were becoming 
  a hindrance and an anachronism. With the development of Soviet Society, they 
  dare not allow the slightest suggestion of real workers democracy and control 
  by the workers themselves.

  With the growth of Soviet economy the development of industry, the technical 
  competence of the working class, the education and culture of the working people 
  has enormously increased. Moreover from a society of mainly peasant, and peasant-worker 
  composition, the working class of 60 million has become a decisive potential 
  force in Soviet Society. Its silent pressure permeates Soviet society.

  Moreover an additional reason, perhaps decisive, for the Krushchev reforms and 
  the de-Stalinisation of recent years has been the fact that the Bureaucracy 
  itself has become more and more alarmed as it has realised that it is the main 
  impediment to the development of production at higher levels than at present. 
  By its uncontrolled squandering and speculation; by its bureaucratic inefficiency, 
  it holds up the full and free development of Soviet society which could only 
  be harmonious with the participation and control at all levels of industry and 
  government by the working class themselves.

  Now Krushchev has announced a new constitution which plagiarising the Stalinist 
  constitution is to be “the most democratic in the world.” Krushchev 
  in his speech on the subject said, “To reflect the new stage in the development 
  of Soviet Society and the State; to raise Socialist democracy to an even higher 
  level; to create even firmer guarantees of the democratic rights and freedoms 
  of the working people, guarantees of the strict observance of Socialist legality 
  and to prepare the conditions for the transition to public self-government.” 
  As a gesture to the imperialists abroad, “peaceful co-existence” 
  is to be written into the Constitution.
 The purpose of the Constitution is allegedly to advance the victory of Communism, 
  “Socialism” and the rule of the working class having been left far 
  behind. In reality this Constitution has the same aims as the Constitution originally 
  drafted by Stalin. The Krushchev bureaucracy have become alarmed at the unparalleled 
  scope of corruption and nepotism in high places, the most scandalous of which 
  occasionally seep into the Soviet press, the peculation and graft of hundreds 
  of thousands and even on occasions millions of pounds. Krushchev in the name 
  of advancing to Communism is trying to introduce some checks and balances on 
  the voracious caste which he represents. In vain! It will turn out to be as 
  futile and hopeless as the Stalinist constitution of the past. This is indicated 
  by the draconic laws, including reintroduction of the death penalty for hooliganism, 
  peculation, swindling, theft of State property, etc. The tirades in the official 
  press against the violence of a section of the youth, aping the parrot cries 
  of the capitalists when criticising Youth. They declare, “They don’t 
  want to work”… they are a bunch of “idlers, ne-er do wells 
  and assorted scoundrels.” 
  All the clap trap about advancing to “Communism,”—leaving 
  aside the economic question which cannot be dealt with here—can be easily 
  exposed. The same fundamental flaw exists in the Constitution. It is like the 
  naked emperor and the imaginary clothes he is supposed to wear. The only real 
  right in the Constitution, that of exercising it, will no more exist in reality 
  than in the Stalinist one. Krushchev like Stalin is trying to square the circle.

  The best test to the pretensions of the Constitution can be given by a little 
  theoretical insight. The original Soviet Constitution took as its model the 
  four principles worked out by Marx and Lenin, not for an alleged “transition 
  to Communism,” but simply for the rule of the working class under extremely 
  onerous, backward and generally difficult conditions.

  If the Soviet Union were really advancing to Communism, this would mean the 
  flowering of real democracy on a scale unheard of under capitalism. There would 
  be full freedom for all tendencies to contend. All working class organisations 
  would have the right to express and organise opposition points of view. It would 
  even be possible today under conditions of rapid economic advance to allow a 
  capitalist opposition to develop. So little would be the support they would 
  gather in the country as a whole, no more a threat than feudalism and its Chestertonian 
  glorifiers in capitalist England.

  None of this will take place. And for a good and sufficient reason. The same 
  ruling caste of officialdom controls Russia which glorified the bloodthirsty 
  Stalin, and raised him to power as the personification of their interests, more 
  educated and polished perhaps, but the same ruling caste. It is their interests 
  that are paramount and that the Constitution is intended to safeguard. They 
  cannot introduce real workers democracy, because as the Hungarian revolution 
  showed that would mean the end of their rule. Bureaucracy like the ruling class 
  in other countries “will do anything for the workers and peasants except 
  get off their backs.” That is the real secret of the Constitution.
 Ted Grant ArchiveSoviets with free elections and right of recall.No official to be paid more than a skilled worker.No standing army but an armed people.No permanent bureaucracy. Each in turn to do the tasks of government.