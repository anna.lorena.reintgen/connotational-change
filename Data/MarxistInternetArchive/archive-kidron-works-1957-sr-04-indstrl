MIA  >  Archive  >  Kidron From Socialist Review, Vol.  No. 7, April 1957, p. 1.
Unsigned but claimed by Kidron in the next issue – IB.
Transcribed by Ian Birchall, Nina Kidron & Richard Kuper.
Marked up by Einde O’Callaghan for the Marxists’ Internet Archive.We are going to press four days after the snow-ball engineering
strike started, eleven days after the ship-builders came out, one day
after the ship-building employers were met with a flat “no” to
their offer of a five per cent, increase in wage rates, with strings attached.But even now, before the end is in sight, the Labour Movement has
been able to learn some of the home-truths that have been left on the
shelf for almost a generation of industrial “peace” and
“coexistence.” Younger workers especially are learning some of
the things that their fathers can never forget.Firstly, if we want something the bosses don’t want to give even
if it is only an “offer,” we can get it through direct strike action.Look at the record. The engineering employers said “no.” No
pay rise, no offers, no arbitration, not even discussions with the
unions. They came into the struggle fully determined to force a
showdown. They have been preparing the showdown for more than a year.In January last year, they appealed through their financial press
for three-quarters of a million unemployed – the Banker, the
Economist, the Financial Times all came out with the
magic figure of 750,000. In February, the British Employers’
Confederation lashed out at a number of boards of nationalized
industries, including the National Coal Board and the Transport
Commission, for conceding wage advances to their workers. In May, the
Engineering and Allied Employers’ National Federation set the
pattern for the redundancies that followed the Tories’ credit
squeeze by reminding federated firms “that there was no nationally
agreed redundancy procedure” and that “it was not desirable for
any management to adopt any procedure designed to share the
responsibility for decision on redundancy” (Times, May 24, 1956).Later that month, “encouraged,” said the Times (June 1,
1956), “by the appearance of underemployment in some sections of
the industry,” the Federation announced the rejection of a wage
claim from the Confederation of Shipbuilding and Engineering Workers’
Unions even before it had been presented. They then swung into the
attack at Standards, BMC and, most recently, Briggs and Fords.Yes, they were certainly preparing for a showdown, for a “no
offer” year and to hell with the consequences. But when it came to
the test, and especially when it came to seeing the tremendous
solidarity of the shipbuilding and engineering workers, they knuckled
under. “No discussions” turned into “discussions”; “no
arbitration” became “arbitration”; “no offers” became “five
per cent”; and “no pay rise” is sure to become “five per cent
plus.” That is the power of direct action.The second lesson to be learned from the strikes a lesson that a
great number of Labour MPs still have to learn – is that the
Government is no umpire. As soon as we climb into the ring with the
employers we find it’s a two-to-one battle.What could be better for the bosses than a Government that does
the work of cutting living standards without anyone having to go to
the expenses of a lock-out, or the risks of wage-cuts? A Government
that cuts food subsidies, raises rents, mutilates the Health Service,
raises prices of school meals and children’s milk; one that, having
created unemployment and short-time working, filches unemployment
benefit from workers on short-time; one that uses Admiralty tugs to
blackleg on striking dockers – such a Government is a bosses’
tool, not an impartial arbitrator as many of our Labour MPs seem to believe.What sort of arbitration can we expect from such a government?
Neither the Industrial Court nor the Industrial Disputes Tribunal
publishes the reasons for their decisions. The independent members –
the ones who make the decisions – are independent only from
working-class influence. All five of the Industrial Court are
barristers. Of the seventeen appointed members of the Industrial
Disputes Tribunal, twelve are barristers, three professors and one
Principal and Vice-Chancellor of Glasgow University. The one who
seems to have had the most industrial experience is Professor H.S.
Kirkaldy – he, at least, was Assistant Secretary of the British
Employers’ Federation from 1929 to 1939 and then General Secretary
of the Iron and Steel Trades Employers’ Association. Just the type
of jobs for a Tory industrial arbiter!Faced with such effective and obvious collusion between the
industrial lions and the Tory jackals, the job of the Labour MPs is
not to press for Government arbitration. Bevan did no great service
by suggesting that the Minister of Labour appoint an “independent
person” to hear both sides in the shipbuilding dispute. Trade-union
MPs have rejected the proposal outright – now that the union
leaderships have been forced to weigh in in the fight, they know that
the addition of a government umpire only makes it more of an uneven battle.The job of the Labour MP is to bring all his political craft to
bear in support of the strikers, now or at any time in the future.
Expose the Tory Government; lay bare the collusion between Capital
and Conservatism; show the sham in Tory “objectivity”; and forge
a link between the workers mobilized in the industrial struggle and
the political aim of getting rid of the Tories now!That is the third lesson taught by the strike, even in its first week.And the fourth lesson is this. If the bosses and their Tory
ministers could not harness us to their “opportunity state”
this time, they will meet failure with more extensive preparations
for the future. Our only weapons are mass solidarity and the
knowledge of the union rank and file of our aims. The job of the
leadership is to expand this knowledge, to forge this solidarity. As
yet little has been done in this direction.Why were there so many abstentionists in the BMC strike last
Summer? Why did only 500 out of Sheffield’s 35,000 engineering
workers turn out to hear Confed. leaders outline the case for this
year’s tremendous strike? (reported in the Manchester Guardian,
March 18). Why was the decision to strike met with some apathy on the
part of the ranks who suffer the consequences and on whose enthusiasm
success depends? Why, finally, are branch officials and shop stewards
amazed at the solidarity shown so far and tend to think that it is
only because the strike promises to be a short one?There has been too much complacency at the top. Union and Party
leaderships have taken the mass membership too much for granted. Too
little has been done to bring home the issues at stake through
massive propaganda and education. Until this is rectified and until
the Labour Party really becomes the political wing of the trade union
movement, relying on the union membership for its strength and
constantly giving guidance in the things that effect this membership,
the initiative of the rank and filer will be hampered by bureaucratic
obstruction on the part of the leadership and the initiative of the
leadership will be blunted by the apathy of the rank and file. The
united front of capitalist employers and capitalist politicians must
be fought, but it can only be fought effectively when each arm of the
movement knows what the other is doing and when both are packing the
punches of a militant working class, conscious of its aims and power. 
Top of the pageLast updated on 16 February 2017