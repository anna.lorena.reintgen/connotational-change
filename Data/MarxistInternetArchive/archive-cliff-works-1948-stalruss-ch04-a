MIA  >  Archive  >  Cliff  >  Stalinist Russia  In this chapter we shall describe the transformation of the class character of teh Russian state from a workers’ to a capitalist state. We shall do this by dealing with teh following points: Chapter 4 Index Under capitalism the consumption of the masses is subordinated to accumulation. Sometimes consumption increases at the same time as accumulation, at other times it decreases while accumulation rises; but always, in every situation, the basic relationship remains.If we follow the history of Russia from October, we find that until the advent of the Five-Year Plan this subordination did not exist, but from then on expressed itself in unprecedented brutality. This will become clear from the following table:Division of gross output of industry into means of
production and means of consumption (in percentages) 191319281932193719401942
(planned)Means of production33.332.853.357.861.062.2Means of consumption66.767.246.742.239.037.8These official figures, nakedly as they show the decline in the weight of the production of means of consumption relative to that of means of production, if anything minimise the decline.It is very difficult to check the absolute change in production of different means of consumption. Russian statistics practise a simple trick in order to describe the situation in rosier colours than is true. The production of the small factories, which as regards means of consumption was very important till 1928, is not included. Official statistics can therefore show a tremendous rise in output in the food industry between 1928 and 1932 (from 1,544 billion roubles to 3,485 billion roubles in 1926/7 fixed prices). And this increase – of more than 100 percent – took place at a time when the production of these very agricultural products which are the raw material of the food industry fell sharply! A similar phenomenon can be shown as regards shoe production, where a jump in shoe output (according to statistics) was accompanied by a steep decline in tanning. These statistical marvels are based not only on price falsifications where figures are given in prices, but also on the exclusion of the output of the small enterprises that were very important till 1928. There is one important means of consumption which even before the Five-Year Plan was produced almost only in big enterprises, and whose production can be measured in volume. This is textiles. Let us compare the output of this product over some years with the output of some means of production: 1928
as percentage
of 1913193219371940as percentages of 1928Cotton goods193.187.1   109.7   122.6Woollen goods  86.1124.8   128.5–Oil122.1192.2   262.9   294.9Coal124.9178.1   352.6   456.2Pig iron  78.6187.9   439.4   451.5Machines and manufactures of steel181.8470.01,375.02,420.0It is not enough that the production of means of consumption lags badly behind that of means of production. The Russian government, relying on the “short memory” of the masses, or more correctly on the oppressive machinery which ensures that past memories remain unuttered, adds insult to injury, and while promising a tremendous rise in the production of means of consumption with every Five-Year Plan, at the same time fixes the official target of the plan at a volume of production which does not exceed the target of former plans. This will be clear from the following table:Targets of Production for the End of the Five-Year Plans 1st2nd3rd4thSome means of consumptionCotton goods (billion m)       4.7       5.1    4.9       4.7Woollen goods (million m)   270.0   227.0177.0   159.0Sugar (million tons)       2.6       2.5    3.5       2.4Some means of productionElectric current (million kwh.)     22.0     33.0  75.0     82.0Coal (million tons)     75.0   152.5243.0   250.0Pig iron (million tons)     10.0     17.4  22.0     19.5Steel (million tons)     10.4     17.0  28.0     25.4Oil and gas (million tons)     21.7     46.8  54.0     35.4 Chapter 4 Index Until 1928, notwithstanding the increasing bureaucratisation, the accumulation of wealth in the statified economy was not accompanied by a growth of poverty. This will become clear from the following table shows [1]:Year Capital of large-
scale industry (Million
roubles 1926/7 prices)  YearReal wages***
(1913=100)19217,930*    19227,935*  1922/3  47.319237,969*  1923/4  69.119248,016*  1924/5  85.119258,105*  1925/6  96.719268,552**1926/7108.419279,151*  1927/8111.119289,841*  1928/9115.6Thus even according to the calculations of Professor Prokopovicz, ex-Minister of the Kerensky government, whom no-one would suspect of friendship for the Bolsheviks, the real wages of Russian workers in 1928-29 were 15.6 per cent higher than before the war. At the same time the working hours were cut by 22.3 per cent. If we also take into account the social services which in 1928 made up about 32 percent of wages, the rise will be even more pronounced. Another point that comes to light from this table is that in the last few years before the inauguration of the Five-Year Plan, as the bureaucracy strengthened itself, real wages almost ceased to rise, and the tempo of their rise lagged a little behind the tempo of accumulation.The situation changed radically with the inauguration of the Plan. From then on accumulation leaps ahead with giant strides, while the standard of living of the masses not only lags far behind, but even declines absolutely compared with 1928. The following table gives an indication of the rate of accumulation:Investment of capital
(thousand million current roubles) TotalIn industry1923/4-1927/8  11.1    5.21928/9-1932  52.5  24.81933-37114.7  58.61938-1942 (plan)192.0111.9Even if the value of the rouble declined in these years it is clear that a tremendous accumulation of capital took place in the three Five-Year plans. according to the data of teh Gosplan, the fixed capital of Russian industry in 1933 prices, which in 1928 was 10.5 billion roubles, rose in 1932 to 25.5 billion roubles, in 1937 to 75.1 billion roubles, and was in 1942 expected to exceed 150 billion roubles.Seeing that from 1928 the Russian authorities stopped publishing the index of real wages and the cost of living and from 1931 wholesale or retail prices, it becomes very difficult to calculate the changes in the level of real wages. The following figures, however, will help towards an estimation of teh changes in teh standard of living [2]:YearMonthly wages*Price of ‘food basket’**‘Food baskets’ per
monthly wagesRoublesIndexRoublesIndexNumberIndex191325.4100.06.57100.03.7100.0192870.2276.411.96182.05.6151.41932115.4454.324.07366.44.8129.71935185.3729.596.421,467.61.951.41937241.8952.099.901,520.52.464.919403231,271.6165.222,514.82.054.1This tells the story of the changes in the workers’ conditions as regards food. Clothing and housing are, if anything, more difficult. In order not to expand too much, we shcall suffice with some figures giving the average housing space per capita.YearUrban population
(millions)Housing area in townsTotal
(million m2)Per person
(m2)1927/2826.3160.06.1193235.6185.15.2193750.2211.94.2  Is it necessary to give additionalo proof that the accumulation of wealth on the one hand means the accumulation of poverty on the other? Chapter 4 Index In a workers’ state a rise in the productivity of labour is accompanied by an improvement in the conditions of the workers. As Trotsky said in 1928, real wages “must become the main criterion for measuring the success of socialist evolution”. The “criterion of socialist upswing is constant improvement of labour standards”. Let us see what the relations between the rise in the productivity of labour and the standard of living of the workers was in Russia. The following table gives an indication of this:YearProductivity of labourNumber of “food baskets”
per average monthly wages IndexIndex1913100   100   1928106.0151.41937196.6  64.9Thus till 1928 not only were wages above pre-war, but they rose much more than the productivity of labour. Between 1928 and 1937, while the productivity of labour more than trebled [3], real wages were actually cut by more than 50 per cent.The same point can be arrived at from a different angle, by comparing the level of productivity in Russia with that of other countries on the one hand, and the standard of living of the Russian workers with that of workers in other countries on the other hand.In 1913 the average productivity of labour in Russian industry was about 25 percent of that in the USA, 35 percent of that in Germany, and 40 percent of that in Britain. A committee of the Gosplan specially appointed in 1937 to investigate the productivity of labour in Russian industry, found that it was 40.5 per cent of productivity in US industry, and 97 per cent of that in Germany. There is ground for the assumption that this calculation is exaggerated, and that the productivity of labour in Russian industry in 1937 was about 30 per cent of that in the US, 70 per cent of that in Germany, and about the same percentage of that in Britain. I would take to long to explain how we arrived at this conclusion. But to accept the conclusions of the Gosplan committee will even further strengthen the arguments we put forward. While the Russian worker produces about 70 per cent as much as a British worker, his standard of living is very much lower.In the following table we assume that the Russian worker earns 500 roubles a month, which is the average wage of all state employees (the bureaucracy included), planned for the end of the Fourth Five-Year Plan in 1950. On the other hand, we have taken as the basis of the price calculation prices from Zone 1, where prices are lowest in Russia. [4] For Britain we have taken the workers’ average weekly earnings of £5 3s. 6d. [5] The basis of the price calculation is the official figures published by the Board of Trade.Number of units average weekly wages can buyItemUnitRussiaBritainWheat bread (first grade)lbs.41.7480.7Wheat bread (second grade)lbs.63.3––Rye breadlbs.  91.0––Beeflbs.    9.079-127Butterlbs.    4.1  77.2Milkpints57-81247.2Sugarlbs.  18.5412.0Eggsnumber82-115706.3Tealbs.    1.6  36.4Coffeelbs.    3.4  41.2Beerpints  14.4  88.2Cigarettesnumber464.0618.0Men’s shoespairs    0.42-4.5Women’s shoespairs    0.41-4.0Women’s jackets, semi-woolnumber    0.6  1.1-2.3Stockings, women’s cottonpairs  16.2  25-27.0Crêpe-de-chineyards    1.4  23-25.0Men’s suits. single-breasted, semi-woolnumber    0.3  0.6-1.5Men’s suits, woolnumber    0.1  0.2-0.3Rubber overshoespairs    2.6      9.5  Women’s cotton dressesnumber    0.2  3.5-6.0Women’s woollen dressesnumber    0.6  0.8-2.1Matchesboxes577.0  824.0  Combs, women’s toiletnumber  28.8  103-154Gramophonesnumber    0.12    0.6  Radio receiving sets (5 valve)number    0.20    0.17Wrist watchesnumber    0.120.3-0.5If the productivity of labour of a worker in Russian industry is about four-fifths of that of a worker in Britain, while his standard of living is a quarter or a third of that of the British worker, can we conclude otherwise than that if the British worker is exploited, his Russian brother is much more so? Chapter 4 Index Immediately after the revolution, it was decided that the management of every plant would be in the hands of the trade unions. Thus the programme of the Communist Party of Russia, adopted at the Eighth Party Congress (held 18 to 23 March 1919), declared:The organised apparatus of social production must primarily depend on the trade unions ... They must be transformed into huge production units, enrolling the majority of the workers, and in due time all the workers, in the respective branches of production.Inasmuch as the trade unions are already (as specified in the laws of the Soviet Republic and as realized in practice) participants in all the local and central organs administering industry, they must proceed to the practical concentration in their own hands of the work of administration in the whole economic life of the country, making this their unified economic aim. By thus protecting the indissoluble union between the central State authority, the national economy, and the broad masses of the workers, the trade unions must in the fullest possible measure induce the workers to participate directly in the work of economic administration. The participation of the trade unions in the conduct of economic life, and the involvement by them of the broad masses of the people in this work, would appear at the same time to be our chief aid in the campaign against the bureaucratisation of the economic apparatus of Soviet Power. This will facilitate the establishment of an effective popular control over the results of production. [6]Participating in the running of industry, together with the workers’ plant committees, were the Party cells. Together with these, and under their control, worked the technical manager. These three together formed the Troika.With the strengthening of the bureaucracy in the party and the trade unions, the Troika became more and more a mere label, more and more raised above the mass of workers. Nevertheless, fundamentally the control was still vested in its hands, until the advent of the Five-Year Plan. A. Baykov, who is no supporter of workers’ control, but praises Stalin’s activities, says:De facto, during that period [before the Five-Year Plan] the director was largely dependent on the work’ trade union organ, the “Zavkom” (the factory trade union committee) and on the party cell, the organ of the Communist Party at the enterprise. Representatives of these organisations considered it their duty to supervise the director’s activities and usually interfered with his decisions. [7]=class="fst">In addition, and as a last resort against the actions of the bureaucracy in the party, the state and the unions, the workers could take to strike action, which was legal. Despite the weariness after long years of war and civil war, despite the dilution of the proletariat by the influx from the villages, despite the exodus of a large part of the best workers into the ranks of the bureaucracy and, above all, despite the fact that the state was a workers’ state, strikes were declared which embraced sometimes thousands of participants. In 1925 there were 196 strikes with 37,600 workers participating; in 1926 there were 337 strikes with 43,200 workers participating; in 1927 there were 396 strikes with 25,400 participants. More than 90 percent of the participants in these strikes were workers employed by the state.With the complete victory of Stalin over the Left Opposition and with the inauguration of the Five-Year Plan a general attack began on workers’ control. Thus a resolution of the Central Committee of the party decided that the workers’ committee of the plant:=class="quoteb">... may not intervene directly in the running of the plant, or endeavour in any way to replace plant management. They shall by all means help to secure one-man control, increase production, plant development, and, thereby, improvement of the conditions of the working class. [8]=class="fst">The manager is in full and sole charge of the plant. All his economic orders are unconditionally binding on all the workers. He alone shall select, promote and remove personnel taking into consideration’ the opinions of the party and the trade union organisations’, but is not to be bound by them.The Troika was officially buried in 1937. In the plenum of the Central Committee, Stalin’s second in command, Zhdanov, declared:=class="quoteb">The Troika is something quite impermissible ... The Troika is a sort of administrative board, but our economic administration is constructed along totally different lines. [9]=class="fst">The new management of industry was very clearly defined by the official manual, Economics of Socialist Industry [10]=class="quoteb">Each plant has a leader endowed with full power of decision, hence – fully responsible for everything: the plant manager.=class="fst">Further:One-man control implies strict demarcation between the administration on the one hand and Party and trade-union organisations on the other. This strict demarcation must be applied on all levels of industrial management. Current operations in fulfilment of the Plan are the task of the administration. The chief of a workshop, the manager of the plant, the head of the Glavk, a board of industry or branch of industry, has full powers, each within his field, and the Party and trade-union organisations may not interfere with their orders. Chapter 4 Index In 1919 the Russian Communist Party stated its wages policy in these terms:While aspiring to equality of reward for all kinds of labour and to complete communism, the Soviet government cannot consider as its task the immediate realisation of this equality at the present moment, when only the first steps are being made towards the transition from capitalism to communism.” [11]But as a matter of fact, in teh time of military communism there was an equalisation of rewards. According to data given by the Soviet statistician, Strumilin, the wages of the workers in the highest category, which in 1917 amounted to 232 per cent of those in the lowest category, were in the first part of 1921 only 102 per cent, i.e. they were practically equal. The situation changed with the introduction of the NEP. the Bolsheviks were compelled to retreat from nearly full equality. In 1921-22 a unified scale of wages was introduced in which there were 17 grades ranging, from the apprentice to the top specialist. According to this, the most highly skilled worker received three and a half times the wages of the lowest-paid unskilled worker. Specialists earned a maximum of eight times as much as the unskilled worker.These differences, however, were mitigated until the introduction of the First Five-Year Plan. First of all, no member of the Communist Party was allowed to earn more than a skilled worker. This provision had great importance, as the majority of the directors of enterprises, departments of industry, etc., were Party members. In 1928, 71.4 per cent of the personnel of the managing boards of the trusts were Party members, of the syndicates 84.4 per cent were, and of those of individual enterprises 89.3 per cent were.An additional factor, which made the differences much smaller than they would seem from the unified scale of wages, was that the total number of specialists (a section of whom were Party members who thus did not earn more than skilled workers) was very small. In 1928 they constituted only 2.27 per cent of all those engaged in industry.A general picture of the income differentiation in Russia was given by Statisticheski Spravechnik SSSR 1926. [12] According to it, in 1926-27 the annual average income of manual workers in pre-war roubles was 465. At the same time the maximum allowed to specialists was 1,811. Excluding the bourgeoisie, the NEP men and the kulaks, there were only 114,000 people who earned this maximum. They made up 0.3 per cent of all earners, and their income made up only 1 per cent of the national income. [13]The situation changed radically with the introduction of the Five-Year Plan. The law of maximum income for Party members was abolished. The “general law of wages” promulgated on 17 June 1920, which laid down that anyone exceeding the norm in piece-work was never to receive more than 100 per cent above the normal rate, was cancelled. So was teh other clause to the law laying down that a worker was never to receive less than two-thirds of the established norm. The director’s main income, however, is not his fixed salary. More important than this are the bonuses. These are dependent on the overfulfilment of the economic plan. Thus, for instance, for every decrease of 1 percent of the real cost of production below the planned cost, the manager, his assistant, the chief engineer and his assistant receive a bonus of 15 percent of their salary except in the iron and steel industries, where the percentage is ten. For every percentage of increase of output above the plan, a manager of a mine and his close assistants receive 4 percent of their salaries. [14] If the output of pig iron exceeds the plan by 5 percent, the top administrators receive a bonus of 10 percent of their salary for every percent above the plan; if the output exceeds the plan by 6 to 10 percent, the bonus is 15 percent of the every percent above the plan. Thus, if the output is 10 percent above plan, the top administrators receive a bonus of 125 percent of their salary.The third source of income is the Director’s Fund. Its official aim is to build houses for the workers and employees, clubs, canteens, creches, kindergartens, to give bonuses for outstanding achievements at work, etc. From what sources is this fund drawn? The profits of the plant are divided between the plant itself and the higher state administration of industry. The proportion is determined by the state at intervals. The share of the state cannot be less than 10 percent in any enterprise. In 1937 industry as a whole gave 48 percent of all its profit to the state. The part which remains in the hands of the director of the plant is divided into two. A part goes to develop the plant-the rest remains in the hands of the director and is called the Director’s Fund. According to a decree of 19 April 1936, 4 percent of the planned profit and 50 percent of the profit above the plan are to go to the Director’s Fund. One Russian economist has given the figures of the size of this bind [15]: Realisation
of plan in
percentDirector’s fund
in millions
of roublesDirector’s fund
per workerPetroleum industry104.121.7   344.92Meat industry118.651.9   752.69Spirit industry108.886.01,175.00As has already been mentioned, the average wage of all workers and employees was 250 roubles per month in 1937. The above figures show that by exceeding the plan only a few percent the Director’s Fund per annum in the petroleum industry comes to more than one monthly average income, in the meat industry to three, and in the spirit industry to more than four and a half. Other industries present the same picture. The table shows how huge are the sums concentrated in the hands of directors of industries numbering thousands of workers.We have no statistical data on how the Directors’ Funds are distributed. The only indication we have is from Yvon in L’URSS telle qu’elle est. [16] He writes that in the paper Za Industrializatsiya of 20 AprIl 1937 figures were published concerning the distribution of the Director’s Fund in the enterprise Porchen in Kharkov:Of the 60,000 roubles constituting the Director’s Fund, the director appropriated 22,000 for himself, the secretary of the party committee 10,000, the chief of the production office 8,000, the chief accountant 6,000, the president of the trade union committee 4,000, the head of the workshop 5,000.The distribution of the other 5,000 roubles is not indicated. They doubtless served to compensate the thousands of workers. This example is no doubt more glaring than many another, but the fact that such an excess can occur indicates the degree to which the industrial bureaucracy is independent of the masses; and it indicates that one-man management, together with the Director’s Fund, are certainly excellent conditions for the increasing prosperity of the directors.As is well known, some professional people in Russia receive very high incomes too. The only professional group about which we have detailed, even though not full, statistics of income distribution, is that of authors. According to Alexei Tolstoy, in 1937 the authors were divided according to their incomes thus: 14 authors receiving over 120,000 roubles a year; 11 authors receiving 72,000 to 120,000 a year; 39 authors receiving 24,000 to 36,000 a year; 114 authors receiving 12,000 to 24,000 a year; 137 authors receiving 6,000 to 12,000 a year; and 4,000 authors receiving less than 6,000 a year.As regards the high status of officials, with the abolition of the Law of Maximum which said that no state official could receive more than a skilled worker, their incomes sky-rocketed. According to a decision of the Supreme Soviet of the USSR of 17 January 1938, the salary of the President of the Supreme Soviet of the RSFSR and his deputies was fixed at 150,000 roubles per annum; that of the president and vice-presidents of the Soviet of the Union and Soviet of Nationalities was fixed at 300,000 roubles per annum. Members of the Supreme Soviets were to receive 12,000 roubles a year in addition to 150 roubles per day of session. [17]At the same time the unskilled worker was getting 100 to 150 roubles per month! [18]The difference between income differentiation before the Five-Year Plan and that after its introduction necessarily leaves the realm of quantity alone and becomes a qualitative difference also. If a specialist or factory manager receives four, five, eight times more than the unskilled worker, it does not necessarily mean that there is a relation of exploitation between the two. A skilled worker, specialist or manager produces more values then an unskilled worker in one hour of work.Even if the specialist receives more than the difference in the values that they produce, it still does not prove that he exploits the unskilled worker. This can be simply demonstrated. Let us assume that in a workers’ state an unskilled worker produces his necessities in six hours a day, and that he works eight hours, the other two hours being devoted to the production of social services, to increasing the amount of means of production in the hands of society, etc. As these two hours of work are not labour for someone else, but for himself, it would be wrong to call it surplus labour. But to avoid introducing a new term, and to distinguish them from the first portion of the labour time (the six hours), let us call it “surplus labour”, while the six hours we shall call “necessary labour”. For the sake of simplicity let us say that an hour of unskilled labour produces the value embodied in 1 shilling. The unskilled worker thus produces 8s. and receives 6s. Let us assume that the specialist produces 5 units of value, or 5s., in an hour of his labour. If the specialist earned five times more than the unskilled worker, i.e., 30s., there would clearly be no relation of exploitation between them.Even if he earned six times more than the unskilled worker, while he produced only five times more, there still would not exist a relation of exploitation, as the specialist would be earning 36 shillings a day, while he produces 40s. But if the specialist earned 100s. or 200s., the situation would be fundamentally changed. In such a case a large part of his income would necessarily come from the labour of others.The statistics we have at our disposal conclusively show that although the bureaucracy enjoyed a privileged position in the period preceding the Five-Year Plan, it can on no account be said that it received surplus value from the labour of others. It can just as conclusively be said that with the introduction of the Five-Year Plans the bureaucracy’s income consisted to a large extent of surplus value. Next Section:
The common and different features of state capitalism and a workers’ state (Part 2)

Top of the page 1. * = Socialist Structure of USSR, 1936, p.3; ** = Calculated according to Summary Production – Financial Plan for Industry for 1927-8; *** = According to S.N. Prokopovich, Russlands Volkswirtschaft unter den Sowjets, Zurich, 1944, p302.2. * = This average wage includes every earner from charwoman to chief director; ** = Calculated according to official data by S.N. Prokopovich, op. cit., p.306.3. According to this calculation, which is based on official data, the productivity of labour rose by 96.6 percent between 1928 and 1937. The official Russian statisticians claim a much bigger rise – 172 percent. They exaggerate, however. Of course, to accept their calculation strengthens our point considerably.4. Soviet Weekly Supplement, 18 December 1947.5. Ministry of Labour Survey for British Workers, April 1947.6. Quoted in N. Bukharin and E. Preobrazhensky, The ABC of Communism, London 1927, pp.401-402.7. A. Baykov, The Development of the Soviet Economic System, London 1946, p.115.8. Pravda, 7 September 1929.9. Pravda, 11 March 1937.10. Economic Institute of the Academy of Sciences, Economics of Socialist Industry, Moscow 1940.11. Programme of the Russian Communist Party, 1919.12. Quoted in L. Lawton, Economic History of Soviet Russia, London 1932, vol.II, pp.359-361.13. In addition we must remember that the specialists, bureaucrats, etc. could not bequeath more than 10,000 roubles to their heirs, i.e. the income of a skilled worker over four yearn.14. Orders of the Commissariat for Fuel, 20 June 1939. For ferrous metalurge, Orders of the Commissariat for Fuel, 16 July 1939; Industriya, 21 June 1939 and 21 Juy 1939 respectively.15. G. Poliak, On the Director’s Funds in Industrial Enterprises, Planned Economy 4, 1938, p.61.16. M. Yvon, L’URSS telle qu’elle est, Paris 1938, p.111.17. L.E. Hubbard, Soviet Labour and Industry, London 1942, p.221.18. The official scale of income tax indicates the same differentiation. It coven incomes from below 150 roubles per month to above 25,000 roubles per month. The tax on high incomes, incidentally, is much less than in Britain. Top of the pageLast updated on 20.1.2004Subordination of consumption to accumulation – subordination of the workers to the means of productionThe accumulation of capital on the one hand and of poverty on the otherThe productivity of labour and the workerWho controls production?Changes in the relations of distributionThe denial of any legal freedom to the workerForced labourThe expropriation of the means of production from the peasantryThe transformation of money into money capital – an important symptom of the systemThe introduction of the turnover taxFrom a workers’ state with bureaucratic deformations to the complete liberation of the bureaucracy from popular control.Why the Five-Year Plan signifies the transformation of the bureaucracy into a ruling class
Chapter 4
Russia’s transformation from a workers’ state to a capitalist state

 

1942
(planned)

33.3

32.8

53.3

57.8

61.0

62.2

66.7

67.2

46.7

42.2

39.0

37.8

 

193.1

87.1

   109.7

   122.6

  86.1

124.8

   128.5

–

122.1

192.2

   262.9

   294.9

124.9

178.1

   352.6

   456.2

  78.6

187.9

   439.4

   451.5

181.8

470.0

1,375.0

2,420.0

 

Some means of consumption

Cotton goods (billion m)

       4.7

       5.1

    4.9

       4.7

Woollen goods (million m)

   270.0

   227.0

177.0

   159.0

Sugar (million tons)

       2.6

       2.5

    3.5

       2.4

Some means of production

Electric current (million kwh.)

     22.0

     33.0

  75.0

     82.0

Coal (million tons)

     75.0

   152.5

243.0

   250.0

Pig iron (million tons)

     10.0

     17.4

  22.0

     19.5

Steel (million tons)

     10.4

     17.0

  28.0

     25.4

Oil and gas (million tons)

     21.7

     46.8

  54.0

     35.4

Year

 

Capital of large-
scale industry (Million
roubles 1926/7 prices)

 

 

Year

Real wages***
(1913=100)

1921

7,930*  

 

 

1922

7,935*  

1922/3

  47.3

1923

7,969*  

1923/4

  69.1

1924

8,016*  

1924/5

  85.1

1925

8,105*  

1925/6

  96.7

1926

8,552**

1926/7

108.4

1927

9,151*  

1927/8

111.1

1928

9,841*  

1928/9

115.6

 

1923/4-1927/8

  11.1

    5.2

1928/9-1932

  52.5

  24.8

1933-37

114.7

  58.6

1938-1942 (plan)

192.0

111.9

Monthly wages*

Price of ‘food basket’**

1913

25.4

100.0

6.57

100.0

3.7

100.0

1928

70.2

276.4

11.96

182.0

5.6

151.4

1932

115.4

454.3

24.07

366.4

4.8

129.7

1935

185.3

729.5

96.42

1,467.6

1.9

51.4

1937

241.8

952.0

99.90

1,520.5

2.4

64.9

1940

323

1,271.6

165.22

2,514.8

2.0

54.1

Year

Urban population
(millions)

Housing area in towns

Total
(million m2)

Per person
(m2)

1927/28

26.3

160.0

6.1

1932

35.6

185.1

5.2

1937

50.2

211.9

4.2  

 

Index

Index

100   

100   

106.0

151.4

196.6

  64.9

Item

Unit

Russia

Britain

Wheat bread (first grade)

lbs.

41.7

480.7

Wheat bread (second grade)

lbs.

63.3

––

Rye bread

lbs.

  91.0

––

Beef

lbs.

    9.0

79-127

Butter

lbs.

    4.1

  77.2

Milk

pints

57-81

247.2

Sugar

lbs.

  18.5

412.0

Eggs

number

82-115

706.3

Tea

lbs.

    1.6

  36.4

Coffee

lbs.

    3.4

  41.2

Beer

pints

  14.4

  88.2

Cigarettes

number

464.0

618.0

Men’s shoes

pairs

    0.4

2-4.5

Women’s shoes

pairs

    0.4

1-4.0

Women’s jackets, semi-wool

number

    0.6  

1.1-2.3

Stockings, women’s cotton

pairs

  16.2  

25-27.0

Crêpe-de-chine

yards

    1.4  

23-25.0

Men’s suits. single-breasted, semi-wool

number

    0.3  

0.6-1.5

Men’s suits, wool

number

    0.1  

0.2-0.3

Rubber overshoes

pairs

    2.6  

    9.5  

Women’s cotton dresses

number

    0.2  

3.5-6.0

Women’s woollen dresses

number

    0.6  

0.8-2.1

Matches

boxes

577.0  

824.0  

Combs, women’s toilet

number

  28.8  

103-154

Gramophones

number

    0.12

    0.6  

Radio receiving sets (5 valve)

number

    0.20

    0.17

Wrist watches

number

    0.12

0.3-0.5

 

Petroleum industry

104.1

21.7

   344.92

Meat industry

118.6

51.9

   752.69

Spirit industry

108.8

86.0

1,175.00

Next Section:
The common and different features of state capitalism and a workers’ state (Part 2)

Top of the page
