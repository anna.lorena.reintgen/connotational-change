MIA  >  Archive  >  C.L.R. James From Labor Action, Vol. X No. 4, 28 January 1946, p. 3.
Transcribed & marked up by Einde O’Callaghan for the Marxists’ Internet Archive.We commemorate with this article the anniversaries of three
giants of the revolution who, with their contributions to the
cause of socialism, carved for themselves an everlasting place in
the memory and living thought of the international working class.
Lenin, the great genius of revolution, the man who with Trotsky
led the monumental revolution in 1917, died in January 1924 after
a long illness. In January, 1919, the two titans of the German
Socialism, Rosa Luxemburg and Karl Liebknecht, were murdered by
the German reactionaries who hoped thereby to decapitate the
rising revolutionary movement. –  Ed.“I also recall the words of one of the most
beloved leaders of the American proletariat, Eugene Debs, who
wrote toward the end of 1915, in the article, In Whose War I
Will Fight ... that he, Debs, would rather be shot than vote
loans for the present criminal and reactionary imperialist war;
that he, Debs, knows of only one holy and from the standpoint of
the proletariat legal war, namely: the war against the
capitalists, the war for the liberation of mankind from wage
slavery. I am not at all surprised that Wilson, the head of the
American billionaires and servant of the capitalist sharks, has
thrown Debs into prison. Let the bourgeoisie be brutal to the true
internationalists, the true representatives of the revolutionary
proletariat! The more obduracy and bestiality it displays, the
nearer comes the day of the victorious proletarian revolution.”
– From A Letter to the American Workers, written by
Lenin and dated August 20, 1918.Every generation judges and must judge its ancestors in the light
of its own experience. Every judgment of an historical figure is a
judgment of contemporary society. The youth of today, looking back at
Lenin, Liebknecht and Luxemburg, is in a position to evaluate their
contributions to human society and measure their stature towering
higher and higher over their contemporaries.The youth of today does not have a stable society. It has been
born and bred and has lived in a society of fascism, of war, national
wars, international wars and civil wars. Mass murder, concentration
camps, judicial frame-ups of thousands carried out with all the
apparatus and paraphernalia of government-power, ‘“liquidation”
of millions who obstructed state policy, unemployment of 20 millions
at a time in one country, these are the characteristics of our epoch
in time of peace.In war the youth has been compelled to participate in the
organization of all the vast resources of the world for mutual
self-destruction, has seen great states wiped off the map, brutality,
cruelty, organized lying, organized hunger, organized attempts to
destroy or to move whole populations – the youth has seen these
become the common practice of the rulers of bourgeois society.
Fascists and democratic states alike participate into the descent
into barbarism. Stalin liquidates revolutionists; Hitler liquidates
millions of Jews; the British government organizes the economy of
India for war in such a manner as to liquidate six million Indians in
a few months of famine. This is the degradation of modern society.
 Lenin, Liebknecht and Luxemburg, growing to maturity in a far
different age, were able to see the elements of capitalist
disintegration and devote their lives to the struggle against it.
Liebknecht and Rosa Luxemburg reached the age of 20 in 1891. Lenin
was 20 in 1890. Anyone who is today 20 years of age has seen in any
single year since 1933 more of capitalism’s waste of human and
material resources and capitalism’s misery than Lenin and
Luxemburg and Liebknecht saw in the whole of their first 20 years.“Militarism is not only a means of defense
against the external enemy; it has a second task, which comes more
and more to the fore as class contradictions become more marked
and as proletarian class-consciousness keeps growing. Thus the
outer form of militarism and its inner character takes a more
definite shape: its task is to uphold the prevailing order of
society, to prop up capitalism and all reaction against the
struggle of the working class for freedom. Militarism manifests
itself here as a mere tool in the class struggle, as a tool in the
hands of the ruling class. It is destined to retard the
development of class consciousness ... The task of militarism is,
above all, to secure for a minority, at whatever cost, even
against the enlightened will of the majority of the people,
domination in the state and freedom to exploit.” – 
Karl Liebknecht.Yet at the age of 20 they were all revolutionists, their lives
dedicated to the struggle for socialism, determined never to
compromise with the capitalist monster whose characteristics they saw
so clearly. Today, as the youth looks back at them and then looks at
the world around, it can appreciate the rightness of the instinct and
the soundness of the judgment which, in their comparatively civilized
epoch, led them to revolutionary socialism.Not one of them was proletarian in origin. Karl Liebknecht was a
lawyer, and his father, William Liebknecht, the friend of Marx, and a
leader of the German Social-Democracy, was educated at three German
universities. Lenin’s mastery of the bar examinations in
Russia, passing at the head of the list in an incredibly short space
of time, testifies early, even in the routine of bourgeois society,
to the intellectual power which he commends. Rosa Luxemburg was a
brilliant student at the Polish university which she attended. But
they brought their natural gifts and their education to the working
class, scorning to use them in the service of bourgeois society for
material reward and bourgeois recognition. Those today (far less
gifted than they) who hanker after the fleshpots of bourgeois society
in all its contemporary barbarism and hopeless perspective, can
measure the pettiness of their aspirations against the actions of
Lenin, Liebknecht and Luxemburg when they grew to maturity and made
their choice.But did they, in embracing the proletarian cause, condemn
themselves to narrow lives of pure misery, privation, obscurity and
suffering? How ridiculous! In finding the proletariat and the
struggle for socialism they found themselves. Marxism today stands
higher than ever as the climax of all the intellectual discoveries
and methods of mankind. And in Marxism and the proletarian struggle
of which Marxism is the ideological reflection, they made
imperishable contributions to human development and lived full, rich
rounded lives in which creative thought and vigorous action were both
indivisible, component parts.
 Today the whole world recognizes that a crisis in human society
has been reached. Through all the clamor for a world organization and
for the Four Freedoms, for the “century of the common man,”
for economic democracy, through all the lies, hypocrisy, confusion
and stupidity, the contributions of Lenin stand unshaken.“Capitalist politicians, in whose eyes the
rulers of the people and the ruling class are the nation, cannot
honestly speak of the ‘right of national self-determination’
in connection with such colonial empires. To the socialist, no
nation is free whose national existence is based upon the
enslavement of another people, for to him colonial peoples, too,
are human beings, and, as such, parts of the national state.
International socialism recognized the right of free, independent
nations, with equal rights. But socialism alone can create such
nations, can bring self-determination of their peoples. This
slogan of socialism is like all its others, not an apology for
existing conditions, but a guide-post, a spur for the
revolutionary, regenerative, active policy of the proletariat. So
long as capitalist states exist, i.e., so long as imperialistic
world policies determine and regulate the inner and outer life of
a nation, there can be no ‘national self-determination,’
either, in war or in peace.” – Rosa Luxemburg.Capitalism has reached the stage where the only alternatives it
offers are imperialist war or a revolutionary overturn by the working
class, to institute Socialism. The future of human society rests with
the victory of the working class, the only consistently revolutionary
class in modern society.To lead the revolution this class needs a political party based on
Marxism and composed of the best, the most enlightened, the most
self- sacrificing members of the working class or intellectual
adherents of socialism.And these doctrines Lenin expounded and concretized in a
combination of writing and organization which together form one of
the most comprehensive, and the most inspiring, and most remarkable
contributions to society ever made by any single human being.Luxemburg for nearly thirty years was one of the leading figures
of that gifted body of men and women who led the German Social
Democracy and made it the greatest civilizing force of its time.
Despite errors, “her luminous mind” illuminated all
aspects of European socialism. In Polish organization, the Russian
revolution, the Bolshevik conflict with Menshevism, political
economy, the crises of French reform socialism, in the struggle
against revision of Marxism, she was in the forefront. For years she
fought an unrecognized fight against those who, like Kautsky, wanted
to remove the revolutionary socialist content from Marxism. At the
moment of the uprising of the German workers, she showed in
revolutionary action the same great abilities of heart and mind which
had distinguished her theoretical and organizational contributions.Liebknecht is forever associated with the struggle against
militarism. But it is characteristic of that great generation that,
being opposed to dialectical materialism, i.e., the philosophic
method of Marxism, in the midst of his revolutionary activities, he
prepared a study of that subject for the discussion .and
clarification of the revolutionary movement.Today the Germany of Liebknecht lies in ruins; the Poland of
Luxemburg and the Russia of Lenin groan under a monstrous tyranny.
The socialist society for which they struggled is nowhere realized.
On this theme the Philistines preach their sermons. But the Hitlers
and the Pilsudskis and the Becks who flourished over the defeat of
socialism have disappeared. The house they built has fallen in
irreparable ruin. Stalinism still stands but has shown not the
slightest capacity to do anything but increase the general crisis of
mankind. The ideas and ideals of Lenin, Liebknecht and Luxemburg
today assume a gigantic stature owing to the very depths into which
capitalist society drags mankind. Humanity will not perish. And as it
fights its way to a sane social order, it must and will discover the
principles, aims and methods which were so enriched by these great
figures.The New York Times of January 14, 1945, tells us that in
Germany on January 13, 10,000 Berliners filed through shattered
streets and snowfall to honor the memory of Liebknecht and Luxemburg.
They went to the Friedrichsfelde Cemetery, the site of the monument
to these great revolutionaries. It was destroyed by Hitler. Where is
he today? But Luxemburg and Liebknecht are beginning to regain their
positions in the hearts and lives of the German people. 
Top of the pageLast updated on 7 August 2018“I also recall the words of one of the most
beloved leaders of the American proletariat, Eugene Debs, who
wrote toward the end of 1915, in the article, In Whose War I
Will Fight ... that he, Debs, would rather be shot than vote
loans for the present criminal and reactionary imperialist war;
that he, Debs, knows of only one holy and from the standpoint of
the proletariat legal war, namely: the war against the
capitalists, the war for the liberation of mankind from wage
slavery. I am not at all surprised that Wilson, the head of the
American billionaires and servant of the capitalist sharks, has
thrown Debs into prison. Let the bourgeoisie be brutal to the true
internationalists, the true representatives of the revolutionary
proletariat! The more obduracy and bestiality it displays, the
nearer comes the day of the victorious proletarian revolution.”
– From A Letter to the American Workers, written by
Lenin and dated August 20, 1918.“Militarism is not only a means of defense
against the external enemy; it has a second task, which comes more
and more to the fore as class contradictions become more marked
and as proletarian class-consciousness keeps growing. Thus the
outer form of militarism and its inner character takes a more
definite shape: its task is to uphold the prevailing order of
society, to prop up capitalism and all reaction against the
struggle of the working class for freedom. Militarism manifests
itself here as a mere tool in the class struggle, as a tool in the
hands of the ruling class. It is destined to retard the
development of class consciousness ... The task of militarism is,
above all, to secure for a minority, at whatever cost, even
against the enlightened will of the majority of the people,
domination in the state and freedom to exploit.” – 
Karl Liebknecht.“Capitalist politicians, in whose eyes the
rulers of the people and the ruling class are the nation, cannot
honestly speak of the ‘right of national self-determination’
in connection with such colonial empires. To the socialist, no
nation is free whose national existence is based upon the
enslavement of another people, for to him colonial peoples, too,
are human beings, and, as such, parts of the national state.
International socialism recognized the right of free, independent
nations, with equal rights. But socialism alone can create such
nations, can bring self-determination of their peoples. This
slogan of socialism is like all its others, not an apology for
existing conditions, but a guide-post, a spur for the
revolutionary, regenerative, active policy of the proletariat. So
long as capitalist states exist, i.e., so long as imperialistic
world policies determine and regulate the inner and outer life of
a nation, there can be no ‘national self-determination,’
either, in war or in peace.” – Rosa Luxemburg.New York Times of January 14, 1945, tells us that in
Germany on January 13, 10,000 Berliners filed through shattered
streets and snowfall to honor the memory of Liebknecht and Luxemburg.
They went to the Friedrichsfelde Cemetery, the site of the monument
to these great revolutionaries. It was destroyed by Hitler. Where is
he today? But Luxemburg and Liebknecht are beginning to regain their
positions in the hearts and lives of the German people.
We commemorate with this article the anniversaries of three
giants of the revolution who, with their contributions to the
cause of socialism, carved for themselves an everlasting place in
the memory and living thought of the international working class.
Lenin, the great genius of revolution, the man who with Trotsky
led the monumental revolution in 1917, died in January 1924 after
a long illness. In January, 1919, the two titans of the German
Socialism, Rosa Luxemburg and Karl Liebknecht, were murdered by
the German reactionaries who hoped thereby to decapitate the
rising revolutionary movement. –  Ed.





“I also recall the words of one of the most
beloved leaders of the American proletariat, Eugene Debs, who
wrote toward the end of 1915, in the article, In Whose War I
Will Fight ... that he, Debs, would rather be shot than vote
loans for the present criminal and reactionary imperialist war;
that he, Debs, knows of only one holy and from the standpoint of
the proletariat legal war, namely: the war against the
capitalists, the war for the liberation of mankind from wage
slavery. I am not at all surprised that Wilson, the head of the
American billionaires and servant of the capitalist sharks, has
thrown Debs into prison. Let the bourgeoisie be brutal to the true
internationalists, the true representatives of the revolutionary
proletariat! The more obduracy and bestiality it displays, the
nearer comes the day of the victorious proletarian revolution.”
– From A Letter to the American Workers, written by
Lenin and dated August 20, 1918.




“I also recall the words of one of the most
beloved leaders of the American proletariat, Eugene Debs, who
wrote toward the end of 1915, in the article, In Whose War I
Will Fight ... that he, Debs, would rather be shot than vote
loans for the present criminal and reactionary imperialist war;
that he, Debs, knows of only one holy and from the standpoint of
the proletariat legal war, namely: the war against the
capitalists, the war for the liberation of mankind from wage
slavery. I am not at all surprised that Wilson, the head of the
American billionaires and servant of the capitalist sharks, has
thrown Debs into prison. Let the bourgeoisie be brutal to the true
internationalists, the true representatives of the revolutionary
proletariat! The more obduracy and bestiality it displays, the
nearer comes the day of the victorious proletarian revolution.”
– From A Letter to the American Workers, written by
Lenin and dated August 20, 1918.





“Militarism is not only a means of defense
against the external enemy; it has a second task, which comes more
and more to the fore as class contradictions become more marked
and as proletarian class-consciousness keeps growing. Thus the
outer form of militarism and its inner character takes a more
definite shape: its task is to uphold the prevailing order of
society, to prop up capitalism and all reaction against the
struggle of the working class for freedom. Militarism manifests
itself here as a mere tool in the class struggle, as a tool in the
hands of the ruling class. It is destined to retard the
development of class consciousness ... The task of militarism is,
above all, to secure for a minority, at whatever cost, even
against the enlightened will of the majority of the people,
domination in the state and freedom to exploit.” – 
Karl Liebknecht.




“Militarism is not only a means of defense
against the external enemy; it has a second task, which comes more
and more to the fore as class contradictions become more marked
and as proletarian class-consciousness keeps growing. Thus the
outer form of militarism and its inner character takes a more
definite shape: its task is to uphold the prevailing order of
society, to prop up capitalism and all reaction against the
struggle of the working class for freedom. Militarism manifests
itself here as a mere tool in the class struggle, as a tool in the
hands of the ruling class. It is destined to retard the
development of class consciousness ... The task of militarism is,
above all, to secure for a minority, at whatever cost, even
against the enlightened will of the majority of the people,
domination in the state and freedom to exploit.” – 
Karl Liebknecht.





“Capitalist politicians, in whose eyes the
rulers of the people and the ruling class are the nation, cannot
honestly speak of the ‘right of national self-determination’
in connection with such colonial empires. To the socialist, no
nation is free whose national existence is based upon the
enslavement of another people, for to him colonial peoples, too,
are human beings, and, as such, parts of the national state.
International socialism recognized the right of free, independent
nations, with equal rights. But socialism alone can create such
nations, can bring self-determination of their peoples. This
slogan of socialism is like all its others, not an apology for
existing conditions, but a guide-post, a spur for the
revolutionary, regenerative, active policy of the proletariat. So
long as capitalist states exist, i.e., so long as imperialistic
world policies determine and regulate the inner and outer life of
a nation, there can be no ‘national self-determination,’
either, in war or in peace.” – Rosa Luxemburg.




“Capitalist politicians, in whose eyes the
rulers of the people and the ruling class are the nation, cannot
honestly speak of the ‘right of national self-determination’
in connection with such colonial empires. To the socialist, no
nation is free whose national existence is based upon the
enslavement of another people, for to him colonial peoples, too,
are human beings, and, as such, parts of the national state.
International socialism recognized the right of free, independent
nations, with equal rights. But socialism alone can create such
nations, can bring self-determination of their peoples. This
slogan of socialism is like all its others, not an apology for
existing conditions, but a guide-post, a spur for the
revolutionary, regenerative, active policy of the proletariat. So
long as capitalist states exist, i.e., so long as imperialistic
world policies determine and regulate the inner and outer life of
a nation, there can be no ‘national self-determination,’
either, in war or in peace.” – Rosa Luxemburg.
