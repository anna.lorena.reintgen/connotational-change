
        Deville - The People's Marx (1893)
        Reduction of the necessary labor-time.—Increase of the productiveness of labor
        and of surplus-value.
    
        Up to this point we have considered that portion of the working-day during which
        the laborer only replaces the value paid him by the capitalist, as having a fixed
        constant length. And in fact its duration is constant so long as the conditions
        of production do not vary. Beyond this fixed duration, this necessary time, labor
        may be prolonged a larger or smaller number of hours, and the rate of surplus-value
        and the total length of the day will vary with the length of this prolongation.
        And so, even when the necessary labor-time is constant, the working-day as a whole
        is, on the contrary, variable.
    
        Now, suppose a working-day with a given limited duration, for instance, a day of
        twelve hours. The surplus-labor and the necessary labor taken together cannot exceed
        twelve hours. How, then, can the surplus-labor, the production of surplus-value,
        be increased? There
        
        is only one way to do it, and that is to shorten the necessary labor-time, and in
        the same ratio to lengthen the part of the twelve hours devoted to surplus-labor.
        In this way, a part of the time which the laborer had thus far, as a matter of fact,
        employed for himself, would be converted into labor-time for the capitalist. The
        limits of the day would be unaltered. The only change would be in its division between
        necessary labor and surplus-labor.
    
        On the other hand, if the length of the entire working- day and the daily value
        of labor-power are given, the duration of the surplus-labor is necessarily fixed.
        If this value (of labor-power per day) is 80 cents, a quantity of gold incarnating
        six hours' labor, the laborer must toil six hours to replace the value of his labor-power
        paid each day by the capitalist, or, to produce an equivalent for the value of the
        means of sustenance required for his daily support. The value of these means of
        sustenance determines the per diem value of his labor-power, and the value
        of his labor-power determines the daily duration of his necessary labor.
    
        The necessary labor-time might be, and is, in practice, shortened by reducing wages
        below the value of labor-power. But we assume here that labor-power is bought and
        sold at its just value. On this assumption, the time devoted to reproducing this
        value can diminish only when this value itself diminishes. Now this value depends
        on the value of the sum of the means of subsistence required for its support. Hence,
        the value of these must diminish. Five hours must, for instance, suffice to produce
        the same quantity of the means of
        
        subsistence formerly produced in six, and this production of the same quantity of
        the means of subsistence in a shorter time can result only from an increase in the
        productive power of labor. Such an increase in productivity can only be brought
        about by an alteration in the instruments or in the methods of labor, or in both
        at once. Hence, a revolution must be effected in the conditions of production.
    
        By an increase of the productive power or of the productivity of labor, we mean,
        generally, an alteration in its processes reducing the time necessary, on the average,
        for the production of a commodity, thus enabling less labor to produce more objects
        of utility.
    
        When we considered the surplus-value, arising from the prolongation of the labor-time,
        the mode of production was deemed to be given and unchanging. - But when it is a
        question of forming surplus-value by the transformation of necessary labor into
        surplus-labor, instead of leaving the customary processes of labor untouched, capital
        must alter the technical and social conditions of labor, that is to say, it must
        transform the mode of production. In this way only can it increase the productivity
        of labor, and thus lower the value of labor-power, and. by so doing, shorten the
        time employed in reproducing it.
    
        The surplus-value produced by the simple prolongation of the working-day we call
        absolute surplus-value, and we give the name of relative surplus-value
        to the surplus-value, which is, on the contrary, the result of the
        
        curtailment of the necessary labor-time and of the consequent change in the relative
        length of the two components of the working-day —necessary labor and surplus-labor.
    
        In order to cause a fall in the value of labor-power, the increase of productivity
        must take place in those branches of industry whose products determine the value
        of labor-power, that is to say, which furnish either the commodities necessary for
        the maintenance of the laborer or the means of production used in making those commodities.
        But the lower price of one of these articles decreases the value of labor-power
        only in proportion to the extent to which it enters into the reproduction of labor-power.
        In those branches of industry which furnish neither the means of subsistence nor
        their material elements, an increase in productivity does not alter the value of
        labor-power.
    
        We saw in Chapter I. that the value of commodities, and consequently of labor-power,
        since its value is determined by the value of commodities, falls when there is an
        increase in the productivity of the labor to which they owe their existence. On
        the other hand, an increase in the productiveness of labor increases the time devoted
        to the formation of surplus-value, and relative surplus- value rises when the productivity
        of labor rises.
    
        And so, by lowering the price of commodities, the development of the productive
        power of labor lowers the price of the laborer. This development, under the capitalist
        regime, results in shortening the part of the day in which the laborer works for
        himself, and in lengthening the part in which he works, gratis, for the capitalist.
    
        The same processes that lower the price of commodities, increase the surplus-value
        which those commodities yield. The saving of labor effected by such a development
        never tends, as certain economists would have one believe, to shorten the working-day.
        The fact that, thanks to an increase in productivity, the laborer can produce in
        an hour ten times as much as formerly, has no tendency to prevent his employer from
        continuing to make him work, at the least, as long as before.
    
         
Table of Contents | Gabriel
            Deville Archive | M.I.A. Library
