
Published: Science & Society, vol. 26, no. 3. Summer, 1962. pp. 293-307.
Transcription/Markup: Micah Muer, 2018.
Note: A version of this text appears as chapter 17 of Mattick's Marx and Keynes: The Limits of the Mixed Economy.
The world's trade and payments dilemma dates back to the First World
War and acquired an apparently insoluble character in the wake of the
Second World War. The trade and payments balance of the European nations
which, until recently, were quite consistently unfavorable can be
traced, though not exclusively, back to the loss of these nations of
most of their foreign holdings, to their indebtedness for American
supplies, a change in the terms of trade, and the shrinkage of their
traditional markets. The situation was aggravated by the deterioration
of East-West trade which, in part, resulted from political changes and
in even greater part from the industrialization of countries that
formerly were almost exclusively producers of primary products.
America's world dominance in both industrial and agricultural production
dislodged still further an already precarious international economic
balance. Whereas the last century found Europe a capital-exporting and
America a capital-importing territory with a large and complementary
trade, the present century reversed the picture in terms of economic
power relations but not with regard to the general economic conditions
of nineteenth-century capitalism. America became a creditor nation
unable to collect and Europe a conglomeration of debtor nations unable
to pay.The difficulties that arose from this change of situation could not
be met by the Keynesian instrumentalities devised at Bretton
Woods in order to restore a quasi-multilateral trade and payments
system. The postwar world required national economic policies of a kind
that the Bretton Woods institutions were intended to prevent.
The International Monetary Fund and the International Bank for
Reconstruction and Development were soon recognized as wholly inadequate
and unable to fulfill their proclaimed functions. Trade deliberations
did not even try to eliminate but merely to reduce restraints,
discriminations, tariffs, quotas, and exchange-controls. Various
"waivers" and special concessions turned the settled agreements into so
many changing agreements on trade restrictions. Tariffs prevailed
generally fifteen years after the war and twenty-five of the
thirty-seven nations participating in the General Agreement on Tariffs
and Trade (GATT) still had import restrictions because of
balance-of-payments difficulties.Payments can balance on any level of production and trade. A balance
of payments may be lost by way of commodity exchange, through capital
movements, and through the requirements of war. By itself, a trade and
payments balance means just what it says: a balance of trade which must
be settled at a definite period. Neither its existence nor its absence
tells anything about actual economic conditions, whether there is
prosperity or depression. A persistent imbalance, however, points to a
disruption of the market system and to important structural changes of
world economy. Between 1946 and 1952 the deficit of the "free" nations
with the United States rose to about 34 billion dollars. Some four
billion dollars were taken care of by gold and dollar reserves of the
deficit nations; over thirty billion dollars by way of American aid.America's "favorable balance of trade," which at times amounted to
ten billion dollars a year, was offset by loans and grants. Trade and
payments questions could not be left to the vicissitudes of market
events, as the gap between European and American production was far too
wide. The United States' favorable balance of trade in 1948 was 5.5
billion dollars and her production in the same year exceeded that of
1937 by seventy per cent. The deficit of the Marshall Plan countries was
5.1 billion dollars and their production was still below the prewar
level. As trade was not possible, aid was unavoidable to maintain the
"imperfect" market system despite its obvious obsolescence.But since, for some ideological reasons, trade appears preferable to
aid, it was, and often is, suggested that trade be substituted for aid
by abolishing American import restrictions. By adhering to a high tariff
and by insisting upon import quotas for a number of products, American
economic foreign policy, it is said, favors the interests of particular
industries and pressure groups at the expense of the national interest.
And since, admittedly, a different course would lead to unemployment and
liquidation in affected American industries, it is suggested that these
eventualities should be mitigated through subsidies, compensations, and
by conversions to other lines of business without any further
specifications what these lines could be. Such suggestions, however, are
no more than a request to shift governmental aid from the international
to the national level.The liberalization of trade and the lifting of exchange controls can
not alter existing economic difficulties. It was these difficulties that
led to trade restrictions and controls in the first place. Ending the
tariff system would, at best, allocate international trade somewhat
differently without necessarily enlarging it. It would not remove the
discrepancy in productivity between the American and the foreign
economies. A unilateral ending of the tariff system would only be
another form of American aid to less productive nations. "To many
countries, moreover, "the present inadequacy of reserves, the
difficulties and uncertainties of expanding and maintaining much larger
exports in an increasingly competitive world, the wide oscillations of
relative prices and of the terms of trade, these all appear to present a
convincing argument for the retention of essential controls over
imports. ... And they can argue with some justification that the
ordinary operations of a free market--the rise of certain exchange
rates, for instance, and the fall or maintenance of others--would
produce almost exactly the same effect somewhat more slowly and less
certainly, and would be equally discriminatory in the sense that the
consequences would affect differently the country's trade with other
nations."[1]When both, discrimination and the lack of it, tend to have identical
effects, discriminatory policies may be adjudged a precondition for the
continued existence and precarious "balance" of any particular nation
and for the world at large. There exists then only an apparent desire to
restore, not an actual possibility of restoring, a free multilateral
trade and payments system. Whether to have restrictions and controls is
not seriously discussed; what is debated in all the talk about freedom
of trade and economic integration is the degree of discrimination and
the direction it is to take with regard to various national interests.
It becomes apparent that "too much time has been wasted in elaborating
rules for a world that cannot be recreated, instead of working out what
can be done in a world that exists. What can be attained is ... a state
of affairs in which countries are under an obligation to make their
trade and payments as free as they can be made, but with the recognition
that there must inevitably be grades of freedom."[2]Economic integration can mean different things--a free world market
with its competitive "equilibrium" mechanism, as well as political
unification and planned supranational interventions in the economy. The
latter type of "unification" and economic "integration" was, for
instance, incorporated in the Nazi vision of a Europe under German
control. The political outcome of the war forced the United States to
accept European economic policies which were discriminatory to her own
economic interests in order to induce a rapid European recovery.
European economic "integration" found strong American support, even
though it did not suit all the European nations, least of all Great
Britain, unable as she was to forget her previous special position in
world economy.Although in its conception "integration" was at once economic and
political, at its start it was purely a monetary matter in accordance
with the Keynesian view of things which considers economic activities
mainly from a monetary point of view. Several hundred changes in
exchange rates in concurrence with different degrees of inflation in
various countries had led into an impenetrable jungle of inconvertible
currencies. To restore at least partial convertibility was then regarded
as the starting point for an increase of trade and a consequent rise of
production. The first attempt in this direction was the European
Payments Union (EPU), modeled after Keynes' International Clearing
Union, proposed during the war. It was to make possible a better
transferability of European currencies. This was regarded as a
precondition for the elimination of import restrictions, export
subsidies, and other measures that hampered intra-European trade. It was
also regarded as an in-between station on the road to universal
convertibility in an altogether free-trading world.The EPU--or the European Monetary Agreement within the frame of the
Organization for European Economic Cooperation (OEEC) at a later
date--succeeded only in warding off a possible further deterioration of
the European markets. And this necessitated successive manipulations of
the EPU itself. Its regulatory mechanism ran out within the frame of a
prearranged quota system of allotted dollar reserves, which reflected
the economic power relations of the participating countries at the time
of the EPU's initiation. As the fortunes of the individual nations
changed, the intra-European trade and payments system became once more a
matter of national policy. Debtor nations maintained their right to
postpone liberalization; the liberalization clause itself became the
means of restoring payments balances, which is to say that protectionist
policies which the EPU was supposed to mitigate and eventually to
eliminate, became the very instrument for the desired payments balance.
No longer was the EPU an instrument of trade; trade was to be conducted
in such a manner as to assure the continued existence of the EPU.European trade and payments problems were soon superseded, however,
by the overriding issues of Western "defense" and Germany's
incorporation into the Atlantic Pact. The decision to revive Germany's
economic and military power implied different things for France and
England than for the United States. For the latter, it was first of all
a military decision, but for France it was acceptable only if
accompanied by guarantees that secured her European position. Yet her
actual weakness and inability to oppose American policies induced French
politicians to anticipate the dangerous aspects of this development and
to answer them in advance with the Schuman Plan. The purpose of this
Plan, or the European Coal and Steel Community (EEC), comprising France,
Germany, Holland, Belgium, Italy and Luxemburg, was to create a single
market for coal and steel in all of Western Europe. Adherence by France
and Germany made participation by the smaller nations practically
mandatory. Britain associated herself merely for the exchange of
information, without surrendering control over her own coal and steel
affairs. The new supranational institution was hailed as the beginning
of a new era in intra-European relations, as the harbinger of better
things to come. The drawing into the single market of other products
besides coal and steel and the creating of a European atomic energy
program were to culminate in a Western European Federation, a veritable
United States of Europe.In a more prosaic mood, however, the EEC appeared to be merely an
extension of the European steel cartel of pre-Hitler days. This cartel
was a price-fixing arrangement whose existence indicated the relative
capital stagnation which preceded the Second World War. And in 1950,
when the Schuman Plan was born, there were signs of impending surpluses
of coal and steel, so that the Plan was probably codetermined by the
desire to avoid another period of cutthroat competition. Yet, at the
time of its ratification, the situation had already changed in favor of
a general expansion of coal and steel production. There was now a need
for collaboration not so much to secure the given market as to assure a
larger production. Whatever the future would bring, for the time being
the EEC satisfied all those engaged in its foundation. For America it
increased the war potential of the West; for Germany it offered a chance
for a quicker recovery, and for France it provided the opportunity to
participate in the control of the apparently inevitable development of
Germany's productive power and war-making ability.Existing for a time only in latent form, the EEC, soon after the
Korean war, increased its activities. As in previous periods of
prosperity, the economic upswing after 1958 created a climate of
optimistic readiness to forego some of the stifling measures of
protectionism. A series of tariff reductions in 1959 were coupled to the
proclaimed intention of reaching a single, six-nations, tariff-free
market by 1965 or 1970. Although the first tariff reductions were not of
great significance, they did help initiate a rash of economic changes of
greater importance. A series of industrial agreements led to many
mergers of companies, encompassing joint selling and production, the
pooling of resources, specialization and rationalization. As the removal
of trade barriers increases both competition and protection against
competition, so it fosters capital concentration which, in turn,
strengthens the competitive abilities of the industries within the EEC.
By promising greater profitability, it attracted a great amount of new
American capital. While all this spelled "prosperity" it also increased
international competition. But competition in a generally expanding
economy merely accelerates the upward swing. European production and
exports increased, cutting the United States trade surplus to its
narrowest margin since the end of the war.However, the EEC did not lead to the political and economic
unification of the whole of Western Europe. Some nations, and England in
particular, were not willing, or found it disadvantageous, to subject
even part of their economic activities to supranational control. What
was realized, in this respect, was a kind of supranational
protectionism. With the EEC a reality, England joined six other nations
(Austria, Denmark, Norway, Portugal, Sweden, Switzerland) in a European
Free Trade Association (EFTA) to counteract the possible competitive
advantages of the six-nation trading bloc. Retaining full control over
their national economic policies, including tariffs with countries
outside the EFTA, these seven nations pledged themselves to cut tariffs
within the EFTA (leading to their complete abolishment by 1970) to fair
competition, to the equalization of supply conditions, and to a full
employment policy. As in the EEC, so in the EFTA, agricultural products
were not included in the planned tariff reductions.The common market, by leading to a division of the European economy
into two trading blocs created as many problems as it solved. While it
does increase free trade beyond national boundaries, it may decrease it
on an international scale. The gains implied in the existence of
free-trade areas are largely offset by difficulties arising from the
disruption of earlier trading practices based on previously established
patterns of production. While capital will perhaps flow more freely
within the separate trading blocs, it will most likely flow less freely
from one bloc to another. And while the two trading areas may be
considered as a first, and at this juncture the only possible step,
toward a single market for all the OEEC countries and beyond, it may
also, in time, demonstrate the hopelessness of the task of reintegrating
either the European or the world economy. It all depends upon the
economic weather, whether or not a necessary degree of world prosperity
can be maintained.Whatever the expectations or apprehensions associated with the
establishment of the two separate European market systems, their very
existence--quite aside from their eventual destiny--points, on the one
hand, to the increasing inability to maintain national economic
policies, and, on the other hand, to the improbability of a return to a
"free" world market. This will not prevent futile attempts in either
direction. When necessary, nations will always tend to insulate their
economy against the detrimental effects of international competition.
Yet, they cannot cease hoping for and working toward the restoration of
an internationally integrated economy. Regional groupings constitute, in
a sense, a kind of "compromise" between these extremes, so as to
overcome the limitations of national economy in a world not susceptible
to rational arrangements of international economic affairs. The European
trade blocs initiated a general movement from Africa to Latin America
for custom unions and intranational market arrangements. While the
regional "solution" seems the only one available, it appears as a
"solution" only on the assumption that it will move toward, and not
away, from world-wide integration.Although the existence of the two trading blocs demonstrates the
present impossibility of integrating into a common market, or a free
trade area, all the European countries, this hoped-for eventuality
remains the proclaimed goal of both the EEC and the EFTA. This goal,
however, is less likely to be reached the longer the trading blocs
retain their separate existence. Newly evolving patterns of competition
will tend to harden; and breaking up regional arrangements may prove
even more difficult than overcoming national protective policies. If one
group should gain exceptional advantages by virtue of the regional
arrangement, it will not sacrifice this advantage to the principle of
free trade, which, at any rate, would still be operative only within the
European markets.The "final" solution of the world's trade and payments problem is
then conceived as a merger of all the various trade areas and in "the
economic fusion of the free world's nations."[3] It is recognized, of course, that such a
"fusion," involving the elimination of tariffs and other trade
restrictions, would aggravate the problems for nations competing with
the United States. But this is to be dealt with by a "relatively
unimpeded movement of capital and labor," by agreement on the part of
the "strong" nations to "extend great blocs of credit to weaker nations
to tide them over their balance-of-payments difficulties," and by the
creation of "an international fund to ease the pain of unemployment and
of capital liquidations in segments of any economy hard-hit by the
process of integration."[4]Against the "relatively unimpeded" movement of capital stands,
however, the actual decline of capital exports in recent history.
American capital exports have been less than the inflow of earnings from
American long-term foreign investments. It is often pointed out that the
low proportion of total American production that goes into exports and
her relatively limited capital exports testify to a lack of "economic
imperialism" on the part of the United States, and that American
competition can consequently not be blamed for the economic difficulties
besetting the world. From a consistent capitalist point of view,
however, it would be just this lack of "economic imperialism"--whatever
its cause--which, to a large degree, accounts for the contraction of the
world market. During the period from 1870 to 1913, for example, "Britain
invested abroad two-fifths of her savings, i.e., something like
one-tenth of her income. By 1913 her foreign investments, equal to
nearly four-ninths of her home investments represented one-third of all
European foreign investments and contributed one-tenth of her national
income."[5] The
equivalent of this on the scale of the now dominating American economy
"would be an American foreign investment of about 600 billion dollars
yielding thirty billion dollars a year income and growing somewhat like
fifteen billion dollars a year."[6] Instead, United States foreign private
investments after the Second World War have been, for a long time, at a
rate less than one billion dollars a year, representing less than
one-third of one per cent of her national income, and slowly rising to
three billion dollars in 1957 and to 4.5 billion dollars thereafter.
Total investments abroad were estimated to be about 37.5 billion
dollars. This insufficiency, relative to the existing capital mass and
compared with capital exports at the turn of the century, allowed for
only an insufficient rise of international trade.The recent spurt in American capital exports, though for the time
being decreasing payments difficulties, may, at a later time, come to
have the opposite effect by an outflow of dividends, interest, and
profits which will exceed new investments. The setting-up of American
manufacturing enterprise in Europe and the participation of American
capital in European industries compensate to some extent for the export
decrease of American commodities due to the actual, or potential, dollar
shortage. Yet part of the profits made in this manner must find their
way back to the American base. If not, it ceases being American capital
and functions as European capital in competition with America and the
rest of the world.To be sure, a great flow of capital from "stronger" to "weaker"
nations may improve the balance-of-payments positions of the latter, for
the fact that foreign capital is entering a domestic economy is itself a
sign that the latter is expanding and is in the process of ending, or
narrowing, its balance-of-payments difficulties. It is only under
conditions of an expanding economy that capital movements affect the
payments sufficiently so that the "free" movement of capital appears as
an "equilibrium" force. This does not do away with, but merely covers
up, the concentration and monopolization process of capital accumulation
by which a widening disequilibrium appears at times as an apparent
equilibrium. The course of actual development cannot be derived from
fluctuating balance-of-payments situations, which may be in
"equilibrium" merely as a precondition for a still greater disparity
between creditor and debtor nations after a given phase of expansion
comes to a halt.Although it may be "immaterial" for a national economy whether its
capital investment is of domestic or foreign origin (provided the rate
of capital formation is not affected by the transfer of profits to the
foreign investors) it is not immaterial for the domestic capitalists to
find their own traditional sphere of capital expansion invaded by
foreign capital. While the incentives for capital exports are getting
smaller, the readiness to invite capital from abroad is also
diminishing--at any rate in the European countries. Both the EEC and the
EFTA tend toward a continental allocation of capital resources which
involves the curtailment of American investments in European industry.
In England, for example, where American controlled capital approaches
three billion dollars, opposition against the potential foreign
domination of some British industries becomes increasingly more
vocal.Whereas the "unimpeded" flow of capital will accomplish little toward
a more balanced international economy, the "unimpeded" flow of labor is
altogether an impossibility. It cannot be realized --except on a small
and insignificant scale--even in supranational combinations such as the
EEC and EFTA. The relatively free movement (to varying degrees) of
commodities, capital, and labor in the United States is an advantage not
to be duplicated by either one of the European trade blocs, or by a
Pan-European market. Here the free flow of labor is as
impossible as is the free flow of labor between Europe and America.Since capital movements are governed by profitability and security,
the most profitable economies attract most of the capital and thus
become still more profitable. This diminishes the competitive ability of
less productive, under-capitalized nations and this, in turn, decreases
the general flow of capital through its concentration in
"overcapitalized" nations. The movement of capital from less profitable
and less secure to more profitable and more secure nations cannot have
an "equilibrating" quality, as it is bound to increase the gap between
the "strong" and the "weak" countries. It is the profitability principle
which induces, but also prevents, large-scale capital exports, and which
induces, or hinders, the "flight of capital." To have a capital movement
of an "equilibrating" nature implies the sacrifice of the profitability
principle by even those nations still able to enjoy its results, that
is, it implies not the free movement of capital, but a rational
allocation of capital according to the actual requirements of world
economy as seen from the point of view of human needs. This clearly
transcends the possibilities of the private enterprise economy, and even
minimum requirements in this direction--to assure a necessary degree of
social stability and international intercourse--depend upon the
interventions of governments which "socialize" the "losses" thus
engendered.European proponents of a "fused" Western economy envision its
"actualization" not so much as freedom of trade and the free movement of
capital and labor as the avoidance of economic warfare in which they
could not win. They accept the EEC and EFTA as arrangements that need
not necessarily lead to conflicts within, and between, themselves, or
with the rest of the world if only there is recognition of, and free
consent to, a variety of preferential treatments for nations within the
trading blocs, between these blocs and the large-scale economies. This
would involve the continued support of the European by the American
economy not, to be sure, by way of additional aid, but by making
allowances for the disadvantages of the European economies
vis-à-vis the United States. Despite all the ideological
free-trade verbiage accompanying policy statements, the international,
like the national economy, is no longer thought of as capable of
functioning in an "automatic" way but as requiring conscious
considerations of actual economic relationships --if necessary, in
defiance of the so-called "market laws"--in order to secure as much as
possible of the market economy. Of course, this kind of partial
"planning" of the international economy to secure a necessary "balance"
implies gains and losses and the acceptance of losses by the stronger
capitalist powers.This desired "reversal" of the trend of international capital
accumulation, or the undoing of history, is as unrealistic as the
widespread nostalgia for a "free" world market and is merely a state of
mind camouflaging the actual impasse in which international capitalism
finds itself. Notwithstanding all declarations, and even actual
policies, to the contrary, it cannot be America's objective (or that of
any other nation) to bring about a well-functioning world economy at the
expense of her own dominating position. To come to the aid of
other nations, even if dictated by national interest, cannot be a
limitless process, nor can it be done in perpetuity without assisting in
the destruction of the social fabric of the free enterprise system. Such
a course finds its "justification" in "emergencies" but remains foreign
to the capitalist mode of production. As the "emergency" passes,
competition supplants "philanthropy." It is then that "philanthropy" is
revealed as but one of the necessary parts of competition.There is, at any rate, no indication of an American readiness to
sacrifice her dominant position in world economy to a more balanced
international economy. After all, America's preponderance is the result
not only of her own productive efforts, but is due also to the
occurrence of two World Wars which left the European economies far
behind the American. At least in part, the United States owes her
exceptional growth to exceptional circumstances and some of the
"blessings" of these circumstances will disappear as the recovery of the
European economies narrows the gap between European and American
production. The gap will be even narrower should the European economies
find ways and means for a more rapid capital expansion than is now
possible in the United States. Relative to the growth of European
production, American markets for these products will become smaller and
thus American exports to Europe will also decline. Because European
expansion is by sheer necessity geared to the world market, its
continued profitable expansion depends on a successful penetration of
extra-American markets in competition with the United States. And not
only in competition with America but also with the Eastern economic
bloc, which limits still further the external expansion of both the
European nations and the United States. With the growing competitive
strength of the Eastern economic bloc and increasing European
competition, America's exceptional position during the first half of the
twentieth century is coming to a close.Further international capital expansion points then to an intensified
competition in a shrinking world market, and the arising trade blocs may
also be considered instrumentalities to counteract the monopolistic
pressures of the large-scale economies. So far as America is concerned,
Europe's recovery, however necessary, can only be a dubious affair.
America's acquiescence in discrimination against herself is wearing
thinner under a growing demand that the European nations, now that they
have recovered, must share in greater measure the profitless burden of
Western "defense." Concern over a growing American payments deficit
despite a continued favorable balance of trade shifts the previous
emphasis upon foreign economic policies to national economic interests,
in full recognition of the renewed force of European competition aided,
on the one hand, by the cutting down of intra-European competition and,
on the other, by the threatening possibility of higher common tariff
walls against American competition.Although, in theory, competitive international capital accumulation
in the traditional sense could continue if economic activities were left
to the free play of the world market, the recognizable results of such a
free play preclude the play itself. Even in a world without tariffs,
quotas and other restrictions, trade would still face the discriminatory
situation of unequal levels of productivity, different degrees of
industrial development, and unequal distribution of natural resources,
which cannot be coped with by the sole incident of lower wage-levels in
the competitive but less fortunate nations. Over a period of time, "if
there are divergent rates of growth of productivity, the trade will be
progressively less favorable to the countries less rapidly advancing in
productivity."[7]
Under conditions of free trade, American competition could ruin European
producers and disemploy European labor, not to speak of even less
developed non-European areas. The less productive nations will try to
block the foreign competitor by all available means, including the
utilization of the national state and the formation of trade blocs in
their defense. And economic legislation will come to their aid the more
readily as capital production necessitates a definite degree of social
stability.Structural changes in world economy brought about by previous
accumulation exclude international economic "integration" by way of a
market-determined capital formation. Yet all attempts at "integration"
are still based on this assumed possibility and are simultaneously
contradicted by practices which divide the world market into "spheres of
interests" and "autarchic" zones which prevent the revival of free
competition on a free world market. Nor can this be otherwise, for if
there cannot be a "free" capital accumulation (due to its concentration
effect) neither can there be a "free" market nor market integration of
the international economy. And just as a controlled international
capital expansion is likewise an impossibility, so is a controlled
international market. A mixture of free and controlled capital
expansion, of free and controlled market relations excludes
both an "automatic" and a "controlled" integration of world economy. It
does not exclude economic manipulation, to be sure, but this
manipulation, which could only serve particularistic interests, cannot
serve the needs of world economy.[1]. Willard L.
Thorp, Trade, Aid or What? (Amherst, 1954), p. 53.[2]. The
Economist, London, January 30, 1954.[3]. Raymond
Vernon, Trade Policy in Crisis (Princeton, 1958), p.
21.[4]. Loc.
cit.[5]. Willard L.
Thorp, op. cit.,, p. 183.[6]. Loc.
cit.[7]. John H.
Williams, Economic Stability in a Changing World (New York,
1953), p. 38. 
Left Communism Subject Archive |
Paul Mattick Archive
