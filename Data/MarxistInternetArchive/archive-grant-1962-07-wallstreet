
Source: Socialist Fight, vol. 4 no. 6 (July 1962)
Transcription: Francesco 2009
Proofread: Fred 2009
Markup: Niklas 2009

There was the
spectacular fall in the price of shares on Wall Street, followed by a
corresponding panic on the European stock markets, ended by a rise in
share values. Since then there has been a gradual fall in the value
of shares on the New York stock exchange to an even lower level than
the previous catastrophic drop. Up to June 15th
there has been a drop of 20 percent in stock market prices quoted on
the New York stock exchange compared to prices at the beginning of
this year.

The
brunt of the losses of this fall has been borne by the small
investors, middle class, small business people, the professional
classes and some of the better off sections of the American working
class. As the Daily Herald
City column of May 30th
commented, “It is possible that big business stung by President
Kennedy’s sharp victory over the steel companies is fighting back…
In other words the tycoons are telling the Americans ‘stop this man
Kennedy or lose your money’.”

Thus
the “people’s capitalism” of Kennedy is shown to be as much a
racket as the much-vaunted “American” answer to Marx in the
prosperity boom before the slump of 1929. It was these same classes
of people who were then affected by the Wall Street slump. This must
be a warning to the British labour movement because Gaitskell at the
last party conference referred to the American administration as a
“model” for a future Labour government.

Big
business in America is cracking the whip and demonstrating who
decides policy in America. The
Times of June 19th
puts the issue thus “the fact of the matter is that the hostility
comes from the business side. Their critics of the executive want to
be masters of the nation’s economic fate as well as their own. For
them the function of Government is to reduce taxation and stay out of
business. This is the fundamental dispute…”

In
fact there is more to it than that. However, under pressure,
President Kennedy is demonstrating in whose interests the measures of
the government are intended. He has taken drastic action against
workers who have gone on strike for higher wages and better
conditions. Against the workers who have been on strike for 11 weeks
at the Republic Aviation Co. he has served an injunction under the
Taft-Hartley anti-Labour law ordering the workers back to work for 80
days. He has warned the flight engineers’ union in a dispute with
the principal airways companies that he would serve an injunction
against them if they went out on strike. Thus he demonstrates his
“impartiality” as between the workers and big business.

However,
the causes of the crisis go deeper than mere personal whims and
grudges of the “business community” against the President. The
power of the great monopolies and combines which rules the United
States in reality is a thousand times greater than that of the
President himself. But they in turn are dominated by the uncontrolled
forces of the capitalist market. Their sole criterion is how much
profit can be made. The wealth producing factories, machinery,
transport network, created by the labour of the workers, which has
the potential of a flood of goods which could on its own supply the
needs of the peoples of the entire world, is in their eyes their
“own” personal property, to be used in their interests. They can
play with the livelihoods of the tens of millions of American
workers, like a child with a toy town. They open and shut factories
according to what they can make out of them. They are the “national
interest.”

The
cause of this flurry on Wall Street is in the fact that the American
economy with swings up and down for the last five years has been
virtually stagnant. The fall in prices of stocks has been in reality
caused by the fact that retail trade in May dropped 1 percent over
April for the first time for five months. This, coupled with the fact
that pre-tax corporate profits in the first quarter of this year
declined slightly from the record rate of the previous quarter,
explains the hostility of Big Business. They can tolerate reforms or
concessions to the workers while their profits are getting bigger and
bigger. The moment these begin to decline they show their teeth and
demonstrate who is master.

America,
formerly overwhelmingly dominant economically in the capitalist
world, has seen a change in its position during the last five years.
Fantastic military expenditure and foreign aid hand-outs to keep the
capitalist-landlord system intact, especially in the “undeveloped”
major part of the world, has undermined the favourable American
balance of trade and turned it into a deficit. This is beginning to
have its effect on the standing of the dollar. The gold supply in
America is now $16,500 million (about £5,900 million). The gold
backing of the dollar in accordance with American law takes up $5,000
million, but there are foreign claims on the dollar amounting to
$20,000 million, half in the hands of private business. With the
business uncertainty reflected in the falls on the stock exchange
“foreign confidence” in the dollar could be undermined.

Naturally
the big business tycoons want the burdens and difficulties of their
system to be pushed on to the backs of the working people and the
small business people. Despite an unmatched productive machine, the
American economy has for the last 15 years failed to work to
capacity. It is useless for president Kennedy piously to declaim in a
speech at Harvard University “The national interest lies in high
employment, steady expansion of output, stable prices, and a strong
dollar…”

These
words ring hollow indeed on the background of unemployment of 4
millions, with 2 millions on short time, with rising prices and the
instability of the dollar referred to above. It rings out mockingly
against a background where the steel industry, one of the major
indexes of the economy, is only working at 55 percent of capacity and
threatens to drop even further.

Despite
the hostility of big business, while President Kennedy was making
empty threats, Douglas Dillon, his secretary of the Treasury was
explaining that the tax proposals of the government in effect offer a
considerable reduction to big business. Big business is also
demanding a balancing of the budget as a means of preventing
inflation, a cutting down of social services and other “luxury
expenditure” to achieve this end and Dillon has faithfully
responded. “As I have often stated, we intend to work vigorously
until the deficit is wholly eliminated—a result we hope to achieve
by the end of next year.”

In
reply to criticisms of government “interference, and parasitism in
mopping up a great part of the national income”, President Kennedy
has quoted figures to show that president Eisenhower outspent
President Truman by £64,286 million, thus showing that there was no
difference between Republicans and Democrats in this regard. However,
in relative terms, apart from the increase in expenditure on “defence
and space,” while the amount had increased, government expenditure
had in reality grown less rapidly than the economy.

This
is an object lesson for the “revisionists” in the labour
movement. It is noteworthy that Douglas Jay, Roy Jenkins and
Gaitskell himself have been silent on these events in America. Coming
at a time of “prosperity” in the capitalist world, where the
difficulties of the system in Britain have resulted in the pay pause,
stagnation of production and the measures of the Tories against the
workers and middle class, it should serve as a warning to the
advanced elements in the labour movement. If in America the
stronghold of the system, at a time of prosperity, the “democratic”
politicians cannot “organise” the system for the benefit of the
people, how much less can it be expected of a much weaker capitalist
economy, more dependent on the world market like Britain? If the
American capitalists are hostile even to the liberalism of Kennedy
why should the British monopolists like ICI be favourable to the
liberalism or radicalism of Gaitskell, under similar circumstances?
The shadow of 1931 and the disastrous sell-out of MacDonald, hangs
over the labour movement.

Even
in prosperity capitalism cannot solve its problems. The myth of an
uninterrupted boom has been rudely shaken by the falls on the stock
markets. It is true these will not immediately be followed by the
same consequences as in 1929. But over a period not only small slumps
but steep falls in production and employment are inevitable.
Consequently the only practical policy for the labour movement lies
in putting forward a socialist policy to use all the resources of the
nation to guarantee maximum production for the benefit of all. This
can only be done by nationalising with minimum compensation all big
business and operating it under a democratic plan of production, with
workers’ control and management.

Ted Grant Archive
