Anton Pannekoek 1908
Source: Le Socialisme, November 7, 1908;
Translated: for Marxists.org by Mitch Abidor.The question of the relationship between reform and revolution
has played a preponderant role in all debates these last few years. We saw this
at the congresses of Nuremberg and Toulouse.People seek to oppose reform to revolution. Intransigent comrades, always
preoccupied with revolution, are accused of neglecting reform. Opposed to them
is the concept that says that reforms systematically and methodically realized
in current society lead to socialism without a violent rupture being
necessary.Contempt for reform is more anarchist than socialist. It is just as little
justified as the reformist concept. In fact revolution cannot be opposed to
reform because it is composed, in the final instance, of reforms, but socialist
reforms.Why do we seek to conquer power if it’s not to accomplish decisive
social reforms in a socialist direction? It’s possible that some anarchist
or bourgeois brains have conceived the idea of the destruction of the old
society and the introduction of a new mode of production with the assistance of
a decree. But we socialists know that a new mode of production cannot be
improvised by a magic spell; it can only proceed from the old via a series of
reforms. But our reforms will be of a completely different kind from
those of even the most radical bourgeois. The declaration of these reforms will
make tremble the bourgeois reformists who never stop talking in congresses about
social reforms, complaining of their difficulty. On the other hand, proletarian
hearts will leap for joy. It’s only when we will have conquered power that
we can carry out the complete task. Once master of this power, and no longer
needing to take into account capitalist interests, the proletariat will have to
destroy all of the miseries of our regime up to their roots. Then we will
advance rapidly, while now every step must be painfully conquered and defended,
and sometimes the conquered positions are lost again. That will be the era of
true reform, in comparison with which the greatest bourgeois reforms will be
nothing but poorly done work.After having conquered power the proletariat can have one sole goal: the
suppression of its poverty by the suppression of the causes that give rise to
it. It will suppress the exploitation of the popular masses by socializing
monopolies and trusts. It will put an end to the exploitation of children, and
will consecrate large amounts of resources to the physical and intellectual
education of the children of the people. It will suppress unemployment by
furnishing productive labor to all the unemployed. It will find the resources to
carry out its work of reform in the accumulated colossal riches. It will ensure
and develop finally conquered freedom by the complete realization of democracy
and autonomy.The social revolution is nothing but this social reform. In realizing this
program the proletariat revolutionizes the mode of production, for capitalism
can only subsist on the misery of the proletariat. Once political power has been
conquered by the proletariat and unemployment has been suppressed, it will be
easy for union organizations to considerably raise salaries and gradually
improve working conditions, up to the disappearance of profit. Exploitation will
become so difficult that the capitalists will be forced to renounce it. The
workers will take their place and will organize production by doing without
parasites. The positive work of the revolution will begin. Proletarian social
reform directly leads to the complete realization of socialism.What distinguishes revolution from what is today called social reform? Its
depth. The revolution is a series of profound and decisive reforms. Where does
this decisive character come from? It comes from the class that accomplishes
them. Today it is the bourgeoisie, or even the nobility, that holds power. All
that these classes do they naturally do in their own interests. It’s in
their self-interest that they accord the workers a few ameliorations. As soon as
they see that reforms don’t succeed in putting down the people they begin
to concoct new laws of an oppressive character. In Germany these are laws
against the freedom of assembly, against cooperatives, sick funds, etc. After
the revolution the proletariat will act in its own interest in making the
machine of state work for it. The difference between revolution and social
reform consequently resides in the class holding power.Those who believe that we will manage to gradually realize socialism by
social reform within the current regime misunderstand the class antagonisms that
determine reforms. Current social reform, having as a goal the preservation of
the capitalist system, finds itself in opposition to the proletarian reform of
tomorrow, which will have the contrary goal: the suppression of the system.The organic connection that exists today between reform and revolution is
completely different. In fighting for reform the working class develops and
makes itself strong. It ends by conquering political power. This is the unity of
reform and revolution. It’s only in this special sense that it can said
that from today on we work every day for the revolution. 
Anton Pannekoek Archive
