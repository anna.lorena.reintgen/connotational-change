
      ... the blessings of irresponsible poverty which have been sung and said in verse
      and prose for many a year by rich men and their dependants, who, if the said
      blessings had been showered on their own heads, would have thought them little
      better than a violent death.
    
      ... as you well know, while I speak, "the amelioration of the lot of the working
      classes" is or seems to be, if we are to trust words, the main object of our
      statesmen, clergy and employers of labour. If it were only the main object of the
      working-classes themselves there would be nothing lacking to the equipment
      of modern Society for building its own funeral pyre which shall transform it into a
      society of useful and happy persons unoppressed by any wrongs, and without
      opportunities for wronging others...
    
      Are any remedies for these evils proposed? Are there any commonly received
      suggestions for putting the useful part of society in the way of gaining the due
      heritage of useful men, of which they are at present deprived? There any many such
      suggestions afloat, put forward with more or less sincerity; of which the following
      may serve as types; working-men should have fewer children; they should spend less
      money on such pleasures (poor enough, I admit) as may be within their reach; they
      should join a certain class of joint-stock commercial undertakings technically
      known as co-operative societies; they should emigrate to other countries, where
      (until they largely take this advice) there is more room for them; they should be
      more industrious; they should spend more time in educating themselves; they should
      furnish their houses with taste and refinement - and so on and so forth. Now of
      these things some are good if they could be done; some most workmen do already and
      perforce; and some they by no means ought to do...
    
      But good, bad or indifferent, these kind of proposed remedies have one quality in
      common: they all mean the expenditure of energy, none can be done without expense,
      and the expense of them is to fall upon the working-men themselves; from their
      poverty is this new wealth to be drawn; from their toilsome days is the new leisure
      to be won; from their sordid lives is to spring up the refinement of a new world,
      from their degradation is to come heroism unsurpassed before. Vain dreams! The
      crimes of civilization cannot be atoned for on such easy terms. Or to put it in
      another way all the remedies of this kind assume that the useful classes who are
      oppressed by our system of society shall still go on serving the useless classes
      who oppress them. They shall be thrifty, self-restrained, refined, educated, in a
      word heroic - and slaves - a preposterous contradiction...
    
      A labouring-man must be niggardly, which, with a very significant misuse
      of the word, they call thrifty...
    
      Once more the greater part of the riches consumed in luxury is not used but wasted.
      I ask any man of sense who knows the life of the wealthy middle class and upper
      class in civilization if it is not true that beyond a certain point all their
      expenditure is on things that give them no pleasure at all; that the wealth so
      spent adds nothing to the refinement, to the dignity or the pleasure of life; that
      it is simply got rid of because it must be got rid of somehow. Just as if it were
      burned on a great bonfire. But perhaps some of you may think that this waste,
      though undoubtedly injurious to the wasters, is of some advantage to the producers
      who are "employed" by it. There is just this amount of truth in that view, that
      this waste of the rich is necessary grist to the great profit-grinding
      mill, and under the system of commercial privilege the workers would not be
      employed unless the mill ground. The workers are too poor to employ the workers,
      and the rich must do that or employment will fall very short indeed. Thus we go
      round the vicious circle, first employment for the production of useless things;
      then production of useless things in order that employment may not fall short. Is
      not this of the very essence of waste? Tell me, I pray you, what is more precious,
      what is more necessary, to the progress of the world than the skill and force of
      the workman, the craftsman, which is more truly the heritage handed down to us by
      countless years of tradition? Yet this precious heritage our society of commercial
      privilege wastes light-heartedly as if it were a part of the nature of things to
      make the worst of that which is the best of things, the token and reward of the
      world's progress, the hope of its future.
    
Socialism up-to-date

The William Morris Internet Archive : Works
4th October 1891 at a meeting sponsored by the Ancoats Recreation Committee at the New Islington Hall, Ancoats, ManchesterThe original manuscript (BM Add Ms 45333[1]) exists but is largely topic headingsThe MIA version is taken from William Morris: Artist, Writer, Socialist (Vol II) ed. May Morris