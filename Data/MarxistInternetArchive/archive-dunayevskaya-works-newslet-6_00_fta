

This month we conclude Raya Dunayevskaya's 1961 lecture notes on Hegel's
Smaller LOGIC. The first part, "Introduction and Preliminary Notion,"
appeared in April, and the second part, "Attitudes to Objectivity,"
appeared last month. Publishing the series is part of our continuing effort
to stimulate theoretical discussion on the "dialectic proper."
Dated Feb. 15, 1961, these notes on Hegel's Smaller LOGIC–the first part of
his  ENCYCLOPEDIA OF PHILOSOPHICAL SCIENCES–comment on all sections of the
work. Dunayevskaya's notes contain an especially detailed commentary on the
"Three Attitudes of Thought Toward Objectivity," a section of the Smaller
LOGIC which does not appear in the SCIENCE OF LOGIC and is a theme
overlooked by many writers on Hegel. There Hegel critiques not only
Kantianism and Empiricism, but also romanticism and intuitionism.
The text of the Smaller LOGIC used by Dunayevskaya is THE LOGIC OF HEGEL,
trans. by William Wallace (Oxford: Clarendon Press, 1894), which differs in
some respects from later editions of Wallace's translation. Parenthetical
references are to the paragraph numbers found in all editions and
translations of Hegel's text. All footnotes are by the editors. The
original can be found in  THE RAYA DUNAYEVSKAYA COLLECTION, 2834-2842.I will not go into the separate categories of Quality, Quantity, Measure or
the question of Being, Nothing and Becoming. Instead, all I will do here is
point to the examples from the history of philosophy so that you get a
feeling for yourself about the specificity of [Hegel's] thinking and
realize that his abstractions are not abstractions at all. Two things, for
example, from the section on Quality will speak for themselves:
"In the history of philosophy the different stages of the logical Idea
assume the shape of successive systems, each of which is based on a
particular definition of the Absolute. As the logical Idea is seen to
unfold itself in a process from the abstract to the concrete, so in the
history of philosophy the earliest systems are the most abstract, and thus
at the same time have least in them. The relation too of the earlier to the
later systems of philosophy is much like the relation of the earlier to the
later stages of the logical Idea; in other words, the former are preserved
in the latter, but in a subordinate and functional position. This is the
true meaning of a much misunderstood phenomenon in the history of
philosophy-the refutation of one system by another, of an earlier by a
later (¶86)....Opinion, with its usual want of thought, believes that
specific things are positive throughout, and retains them fast under the
form of Being. Mere Being, however, is not the end of the matter" (¶91).
Remember that the sections in the smaller type are the ones that Hegel
quotes orally and then you will get a view of his response to his audience
when, say, they would look with blank faces when he would speak of
something like "Being-for-self."(1) And now read the following:
"The Atomic philosophy (2) forms a vital stage in the historical growth of
the Idea. The principle of that system may be described as Being-for-self
in the shape of the Many. At present, students of nature who are anxious to
avoid metaphysics, turn a favorable ear to Atomism. But it is not possible
to escape metaphysics and cease to trace nature back to terms of thought,
by throwing ourselves into the arms of Atomism. The atom in fact is itself
a thought; and hence the theory which holds matter to consist of atoms is a
metaphysical theory. Newton gave physics an express warning to beware of
metaphysics, it is true; but to his honor, be it said, he did not by any
means obey his own warning. The only mere physicists are theanimals: they
alone do not think: while man is a thinking being and a born metaphysician."
(Read the rest for yourself–it is too important to miss ¶98.)
Here again I will not go into categories such as Identity, Difference,
Contradiction, etc., all of which I dealt with when summarizing the Larger
LOGIC and which you will find comparatively easy to read here. (3) What
interests me are the so-called examples and once in a while the easy
definitions like "The aim of philosophy is to banish indifference, and to
learn the necessity of things" (¶119). So we go back to the historical
basis which always throws an extra illumination on the generalization that
follows:
"The Sophists came forward at a time when the Greeks had begun to grow
dissatisfied with mere authority and tradition in the matter of morals and
religion, and when they felt how needful it was to see that the sum of
facts was due to the intervention and act of thought.....Sophistry has
nothing to do with what is taught:-that may always be true. Sophistry lies
in the formal circumstance of teaching it by grounds which are as available
for attack as for defense" (¶121).
I want to recommend the studying in full of the final part of this section
called "Actuality." It is not a question only of content or its profound
insistence on the relationship of actuality to thought and vice-versa ("The
idea is rather absolutely active, as well as actual") (¶142). It is a
movement of and to freedom within every science, philosophy, and even class
struggle, though Hegel, of course, never says that; nevertheless [one] must
go through the actuality of necessity and the real world contradictions
that are impossible to summarize in any briefer form than the 24 paragraphs
Hegel does here (¶142-159).
You have heard me quote often the section on Necessity, which ends with:
"So long as a man is otherwise conscious that he is free, his harmony of
soul and peace of mind will not be disturbed by disagreeable events. It is
their view of Necessity, therefore, which is at the root of the content and
discontent of man, and which in that way determines their destiny itself"
(¶147). Now you go to it and study those pages.
This last section of the LOGIC is the philosophic framework which most
applies to our age. From the very start where he says, "The Notion is the
power of Substance in the fruition of its own being, and therefore, what is
free," you know that on the one hand, from now on you are on your own and
must constantly deepen his content through a materialistic, historical
"translation." And, on the other hand, that you cannot do so unless you
stand on his solid foundation: "The Notion, in short, is what contains all
the earlier categories of Thought merged in it. It certainly is a form, but
an infinite and creative form, which includes, but at the same time
releases from itself the plenitude of all that it contains" (¶160).
I would like you to read the letter I wrote to Olga [Domanski] on
Universal, Particular and Individual (4) and then read Hegel on those
categories, and you will see how little of his spirit I was able to
transmit and how changeable are his own definitions. For example, he says,
"Individual and Actual are the same thing....The Universal in its true and
comprehensive meaning is one of those thoughts which demanded thousands of
years before it entered into the consciousness of man" (¶163). Just ponder
on this single phrase "thousands of years."
These categories–Universal, Particular and Individual–are first described
in the [Doctrine of the] Notion as notion, then they enter Judgment, then
Syllogism, and then throughout to the end, and in each case they are not
the same, and you can really break your neck if you try to subsume them
into a definitional form. They just will not be fenced in. Hegel, himself,
has something to say on this fencing in of the syllogism, for example,
which in "common logic" is supposed to conclude so-called elemental theory,
which is then followed by a so-called doctrine of method, which is supposed
to show you how to apply what you learned in Part I:
"It believes Thought to be a mere subjective and formal activity; and the
objective fact which confronts Thought it holds to be permanent and
self-subsistent, but this dualism is a half-truth... It would be truer to
say that it is subjectivity itself, which, as dialectics, breaks through
its own barrier and develops itself to objectivity by means of the
syllogism" (¶192).
(I want to call to your attention that it is the last sentence in ¶212,
which [C.L.R. James] so badly misused in justifying our return to
Trotskyism. Note that the quotation itself speaks of error as a necessary
dynamic, whereas James spoke of it as if it were the dynamic: "Error, or
other-being, WHEN IT IS UPLIFTED AND ABSORBED, is itself a necessary
dynamic element of truth: for truth can only be where it makes itself its
own result." (The phrase underlined was underlined by me in order to stress
that James had left it out.) (5)
The final section on the Absolute Idea is extremely abbreviated and by no
means gives you all that went into the SCIENCE OF LOGIC, but it will serve
if you read it very carefully; to introduce you to its study in the Larger
LOGIC. I will quote only three thoughts from it:
"The Absolute Idea is, in the first place, the unity of the theoretical and
practical idea, and thus at the same time, the unity of life with the idea
of cognition....The defect of life lies in its being only the idea in
itself or naturally: whereas cognition is in an equally one-sided way, the
merely conscious idea or the idea for itself, The Unity... (¶236). It is
certainly possible to indulge in a vast amount of senseless declamation
about the idea absolute, but its true content is only the whole system, of
which we have been hitherto examining the development" (¶237).
I love the expression that to get to philosophic thought one must be strong
enough to ward off the incessant importance of one's own opinion:
"The philosophical method is analytical, as well as synthetic...to that
end, however, there is required an effort to keep off the ever-incessant
impertinence of our own fancies and opinions" (¶238).
The final sentence of the whole book in the Smaller LOGIC is what pleased
Lenin so highly that he wrote as if the SCIENCE OF LOGIC ended [there] by
stating that the "rest of the paragraph" wasn't significant. It is on that
rest of the paragraph in the Larger LOGIC around which the whole reason for
my 1953 Letters on the Absolute Idea rests. (6) The sentence Lenin liked
because it held out a hand to materialism is: "We began with Being,
abstract being: where we now are we also have the idea as Being: but this
idea, which has Being is Nature." This is the oral remark which followed
the written last sentence:
"But the idea is absolutely free; and its freedom means that it does not
merely pass over into life, or as finite cognition allow life to show in
it, but in its own absolute truth resolves to let the element of its
particularity, or of the first characterization and other-being, the
immediate idea, as its reflection, go forth freely itself from itself as
Nature" (¶244).
1. Hegel defines "being-for-self" thusly: "We say that something is for
itself in so far as it cancels its otherness, its relatedness to and
community with Other, rejecting and abstracting from them. In it, Other
only exists as having been transcended, or as its moment...
Self-consciousness is Being-for-Self accomplished and posited; the aspect
of relation to an Other, an external object, has been removed" [SLI, p.
171; SLM, p. 158].
2. "The Atomic philosophy" refers to the doctrine that existence can be
explained in terms of aggregates of atoms, irreducible fixed particles or
units. It reached its classic expression in ancient Greece in the
philosophy of Democritus. Atomism has often been connected to philosophical
materialism.
3. Dunayevskaya's notes on Hegel's SCIENCE OF LOGIC can be found in THE
RAYA DUNAYEVSKAYA COLLECTION, 2815-2833. NEWS & LETTERS reprinted them in
the January-February, March, April and May 1999 issues.
4. This refers to a letter to Olga Domanski, a colleague of Dunayevskaya's,
of Feb. 27, 1961. It can be found in the SUPPLEMENT TO THE RAYA
DUNAYEVSKAYA COLLECTION, 13842-43.
5. In 1947-48 James used the notion that "error is THE dynamic of truth" to
justify the Johnson-Forest Tendency's decision to rejoin the Socialist
Workers Party, despite its "erroneous" politics which the Johnson-Forest
Tendency had long combated. See his NOTES ON DIALECTICS, pp. 92-93.
6. See the "Letters on Hegel's Absolutes of 1953" in THE PHILOSOPHIC MOMENT
OF MARXIST-HUMANISM (Chicago: News and Letters, 1989) and "New Thoughts on
the Dialectics of Organization and Philosophy" in PHILOSOPHY AND
REVOLUTION: FROM HEGEL TO SARTRE AND FROM MARX TO MAO (New York: Columbia
Unviersity Press, 1989), where Dunayevskaya critiques Lenin's
interpretation of the closing sentences of the LOGIC.
