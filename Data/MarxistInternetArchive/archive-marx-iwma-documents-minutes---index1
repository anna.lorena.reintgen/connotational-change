
The First International
Source: The General Council of the First International 1866-1868. Minutes; Progress Publishers, Moscow, for the Centenary of the First International in 1964.
Transcribed: by Andy Blunden. 
Minutes 1864-1866 (Volume 1)
Name Index |
Explanatory Notes |
First International Archive
 

The General Council of the First International
Minutes


Source: The General Council of the First International 1866-1868. Minutes; Progress Publishers, Moscow, for the Centenary of the First International in 1964.
Transcribed: by Andy Blunden.



Contents
Preface
1
The Minute Book of the General Councilof the InternationalWorking Men’s Association
(September 18 1866-September 1 1868)
1866
Meeting of September 1829
Meeting of September 2533
Meeting of October 238
Meeting of October 943
Meeting of October 1647
Meeting of October 2350
Meeting of October 3054
Meeting of November 656
Meeting of November 1359
Meeting of November 2062
Meeting of November 2765
Meeting of December 470
Meeting of December 1174
Meeting of December 1876
1867
Meeting of January 179
Meeting of January 882
Meeting of January 1584
Meeting of January 2992
Meeting of February 594
Meeting of February 1296
Meeting of February 1996
Meeting of February 2696
Meeting of March 598
Meeting of March 12100
Meeting of March 19103
Meeting of March 26105
Meeting of April 2106
Meeting of April 9108
Meeting of April 16110
Meeting of April 23112
Meeting of April 30114
Meeting of May 7118
Meeting of May 14121
Meeting of May 21123
Meeting of May 28124
Meeting of June 4126
Meeting of June 18128
Meeting of June 25130
Meeting of July 2132
Meeting of July 9133
Meeting of July 16139
Meeting of July 23141
Meeting of July 30145
Meeting of August 6147
Meeting of August 13150
Meeting of August 20153
Meeting of August 27156
Meeting of August 29158
Meeting of September 17158
Meeting of September 24160
Meeting of October 1163
Meeting of October 8164
Meeting of October 22167
Meeting of October 29168
Meeting of November 5170
Meeting of November 12173
Meeting of November 19 174
Meeting of November 20179
Meeting of November 26180
Meeting of December 17182
Meeting of December 31183
1868
Meeting of January 21185
Meeting of January 28186
Meeting of February 4190
Meeting of February 11190
Meeting of February 18192
Meeting of February 26193
Meeting of March 3195
Meeting of March 10196
Meeting of March 17197
Meeting of March 24199
Meeting of March 31200
Meeting of April 7202
Meeting of April 14203
Meeting of April 21204
Meeting of April 28206
Meeting of May 5208
Meeting of May 12209
Meeting of May 19210
Meeting of May 26211
Meeting of June 2213
Meeting of June 9215
Meeting of June 16219
Meeting of June 23221
Meeting of June 30223
Meeting of July 7224
Meeting of July 14225
Meeting of July 21227
Meeting of July 28229
Meeting of August 4234
Meeting of August 11240
Meeting of August 18245
Meeting of August 25248
Meeting of September 1249
From the Manuscripts of Karl Marx
Karl Marx, Notes for an Undelivered Speech on Ireland253
Documents of the General Council of the International Working Men’s Association
International Working Men’s Association (Form of Application for Working Men Wishing To Join the International)261
Balance Sheet of the International Working Men’s Association (from March 29th, 1865 to September 1, 1866)263
Rules and Administrative Regulations of the International Working Men’s Association265
The French Government and the International Association of Working Men 271
Poland (Programme of the Tea-Party and Public Meeting) 277
Central Council of the International Working Men’s Association. To the Miners and Iron-Workers of Great Britain280
Balance Sheet of the International Working Men’s Association (from September 1st, 1866 to April 23rd, 1867)283
Address of the General Council of the International Working Men’s Association. To members and Affiliated Socities285
Adresse du Conseil Général de L'Association Internationale des Traveilleurs. Aux Membres et aux Sociéties Affiliées et à tous les Traveilleurs288
Third Annual Report of the International Working Men’s Association292
The Annual Report of the American Secretary of the General Council of the International Working Men’s Association (September 1866 to August 27, 1867)304
Balance Sheet for the Financial Year ending August 31st, 1867311
The Fenian Prisoners at Manchester and the International Working Men’s Association312
To the Secretaries and Members of the International Working Men’s Association (Questions on Labour Statistics)314
To the Members of the International Working Men’s Association (Draft Programme for the Brussels Congress)315
The Geneva Lock-Out. To the Editor of the Star 317
To the Trades’ Unionists of Great Britain and Ireland319
The Fourth Annual Report of the General Council of the International Working Men’s Association324
Editorial Material
Explanatory Notes331
Name Index413
Index of Periodicals438
Index of Addresses and Geographical Names440
Illustrations
First page of the Minute Book for 1866-68 (General Council meeting of September 18, 1866) containing Marx’s remarks on the reaction in the German papers to the English tailors’ strike28-29
Leaflet with the programme of the public meeting held to commemorate the anniversary of the Polish Insurrection of 186376-77
Page of the Minute Book with Minutes recorded by Shaw and Fox88-89
Page of the Minute Book (General Council meeting of July 23, 1867), with the printed text of Marx’s speech on the statistics in a new Blue Book 144-5
House in High Holborn, London, where the General Council held its meetings from June 1868 to February 1872208-9
Page of the Minute Book (General Council meeting of July 28, 1868) with a motion by Marx that a delegate be sent to the Congress of the German Workers’ Unions to be held at Nuremberg232-3
Application from the Operative Bricklayers’ Society, dated February 21, 1865, for affiliation to the International260-1
Page of the Minute Book (General Council meeting of July 9, 1867) with a motion by Marx concerning the agenda of the Lausanne Congress of 1867296-7


 


Minutes 1864-1866 (Volume 1)
Name Index |
Explanatory Notes |
First International Archive

 



Contents
Preface
1
The Minute Book of the General Councilof the InternationalWorking Men’s Association
(September 18 1866-September 1 1868)
1866
Meeting of September 1829
Meeting of September 2533
Meeting of October 238
Meeting of October 943
Meeting of October 1647
Meeting of October 2350
Meeting of October 3054
Meeting of November 656
Meeting of November 1359
Meeting of November 2062
Meeting of November 2765
Meeting of December 470
Meeting of December 1174
Meeting of December 1876
1867
Meeting of January 179
Meeting of January 882
Meeting of January 1584
Meeting of January 2992
Meeting of February 594
Meeting of February 1296
Meeting of February 1996
Meeting of February 2696
Meeting of March 598
Meeting of March 12100
Meeting of March 19103
Meeting of March 26105
Meeting of April 2106
Meeting of April 9108
Meeting of April 16110
Meeting of April 23112
Meeting of April 30114
Meeting of May 7118
Meeting of May 14121
Meeting of May 21123
Meeting of May 28124
Meeting of June 4126
Meeting of June 18128
Meeting of June 25130
Meeting of July 2132
Meeting of July 9133
Meeting of July 16139
Meeting of July 23141
Meeting of July 30145
Meeting of August 6147
Meeting of August 13150
Meeting of August 20153
Meeting of August 27156
Meeting of August 29158
Meeting of September 17158
Meeting of September 24160
Meeting of October 1163
Meeting of October 8164
Meeting of October 22167
Meeting of October 29168
Meeting of November 5170
Meeting of November 12173
Meeting of November 19 174
Meeting of November 20179
Meeting of November 26180
Meeting of December 17182
Meeting of December 31183
1868
Meeting of January 21185
Meeting of January 28186
Meeting of February 4190
Meeting of February 11190
Meeting of February 18192
Meeting of February 26193
Meeting of March 3195
Meeting of March 10196
Meeting of March 17197
Meeting of March 24199
Meeting of March 31200
Meeting of April 7202
Meeting of April 14203
Meeting of April 21204
Meeting of April 28206
Meeting of May 5208
Meeting of May 12209
Meeting of May 19210
Meeting of May 26211
Meeting of June 2213
Meeting of June 9215
Meeting of June 16219
Meeting of June 23221
Meeting of June 30223
Meeting of July 7224
Meeting of July 14225
Meeting of July 21227
Meeting of July 28229
Meeting of August 4234
Meeting of August 11240
Meeting of August 18245
Meeting of August 25248
Meeting of September 1249
From the Manuscripts of Karl Marx
Karl Marx, Notes for an Undelivered Speech on Ireland253
Documents of the General Council of the International Working Men’s Association
International Working Men’s Association (Form of Application for Working Men Wishing To Join the International)261
Balance Sheet of the International Working Men’s Association (from March 29th, 1865 to September 1, 1866)263
Rules and Administrative Regulations of the International Working Men’s Association265
The French Government and the International Association of Working Men 271
Poland (Programme of the Tea-Party and Public Meeting) 277
Central Council of the International Working Men’s Association. To the Miners and Iron-Workers of Great Britain280
Balance Sheet of the International Working Men’s Association (from September 1st, 1866 to April 23rd, 1867)283
Address of the General Council of the International Working Men’s Association. To members and Affiliated Socities285
Adresse du Conseil Général de L'Association Internationale des Traveilleurs. Aux Membres et aux Sociéties Affiliées et à tous les Traveilleurs288
Third Annual Report of the International Working Men’s Association292
The Annual Report of the American Secretary of the General Council of the International Working Men’s Association (September 1866 to August 27, 1867)304
Balance Sheet for the Financial Year ending August 31st, 1867311
The Fenian Prisoners at Manchester and the International Working Men’s Association312
To the Secretaries and Members of the International Working Men’s Association (Questions on Labour Statistics)314
To the Members of the International Working Men’s Association (Draft Programme for the Brussels Congress)315
The Geneva Lock-Out. To the Editor of the Star 317
To the Trades’ Unionists of Great Britain and Ireland319
The Fourth Annual Report of the General Council of the International Working Men’s Association324
Editorial Material
Explanatory Notes331
Name Index413
Index of Periodicals438
Index of Addresses and Geographical Names440
Illustrations
First page of the Minute Book for 1866-68 (General Council meeting of September 18, 1866) containing Marx’s remarks on the reaction in the German papers to the English tailors’ strike28-29
Leaflet with the programme of the public meeting held to commemorate the anniversary of the Polish Insurrection of 186376-77
Page of the Minute Book with Minutes recorded by Shaw and Fox88-89
Page of the Minute Book (General Council meeting of July 23, 1867), with the printed text of Marx’s speech on the statistics in a new Blue Book 144-5
House in High Holborn, London, where the General Council held its meetings from June 1868 to February 1872208-9
Page of the Minute Book (General Council meeting of July 28, 1868) with a motion by Marx that a delegate be sent to the Congress of the German Workers’ Unions to be held at Nuremberg232-3
Application from the Operative Bricklayers’ Society, dated February 21, 1865, for affiliation to the International260-1
Page of the Minute Book (General Council meeting of July 9, 1867) with a motion by Marx concerning the agenda of the Lausanne Congress of 1867296-7

ContentsPreface1The Minute Book of the General Councilof the InternationalWorking Men’s Association
(September 18 1866-September 1 1868)
1866Meeting of September 1829Meeting of September 2533Meeting of October 238Meeting of October 943Meeting of October 1647Meeting of October 2350Meeting of October 3054Meeting of November 656Meeting of November 1359Meeting of November 2062Meeting of November 2765Meeting of December 470Meeting of December 1174Meeting of December 18761867Meeting of January 179Meeting of January 882Meeting of January 1584Meeting of January 2992Meeting of February 594Meeting of February 1296Meeting of February 1996Meeting of February 2696Meeting of March 598Meeting of March 12100Meeting of March 19103Meeting of March 26105Meeting of April 2106Meeting of April 9108Meeting of April 16110Meeting of April 23112Meeting of April 30114Meeting of May 7118Meeting of May 14121Meeting of May 21123Meeting of May 28124Meeting of June 4126Meeting of June 18128Meeting of June 25130Meeting of July 2132Meeting of July 9133Meeting of July 16139Meeting of July 23141Meeting of July 30145Meeting of August 6147Meeting of August 13150Meeting of August 20153Meeting of August 27156Meeting of August 29158Meeting of September 17158Meeting of September 24160Meeting of October 1163Meeting of October 8164Meeting of October 22167Meeting of October 29168Meeting of November 5170Meeting of November 12173Meeting of November 19 174Meeting of November 20179Meeting of November 26180Meeting of December 17182Meeting of December 311831868Meeting of January 21185Meeting of January 28186Meeting of February 4190Meeting of February 11190Meeting of February 18192Meeting of February 26193Meeting of March 3195Meeting of March 10196Meeting of March 17197Meeting of March 24199Meeting of March 31200Meeting of April 7202Meeting of April 14203Meeting of April 21204Meeting of April 28206Meeting of May 5208Meeting of May 12209Meeting of May 19210Meeting of May 26211Meeting of June 2213Meeting of June 9215Meeting of June 16219Meeting of June 23221Meeting of June 30223Meeting of July 7224Meeting of July 14225Meeting of July 21227Meeting of July 28229Meeting of August 4234Meeting of August 11240Meeting of August 18245Meeting of August 25248Meeting of September 1249From the Manuscripts of Karl MarxKarl Marx, Notes for an Undelivered Speech on Ireland253Documents of the General Council of the International Working Men’s AssociationInternational Working Men’s Association (Form of Application for Working Men Wishing To Join the International)261Balance Sheet of the International Working Men’s Association (from March 29th, 1865 to September 1, 1866)263Rules and Administrative Regulations of the International Working Men’s Association265The French Government and the International Association of Working Men 271Poland (Programme of the Tea-Party and Public Meeting) 277Central Council of the International Working Men’s Association. To the Miners and Iron-Workers of Great Britain280Balance Sheet of the International Working Men’s Association (from September 1st, 1866 to April 23rd, 1867)283Address of the General Council of the International Working Men’s Association. To members and Affiliated Socities285Adresse du Conseil Général de L'Association Internationale des Traveilleurs. Aux Membres et aux Sociéties Affiliées et à tous les Traveilleurs288Third Annual Report of the International Working Men’s Association292The Annual Report of the American Secretary of the General Council of the International Working Men’s Association (September 1866 to August 27, 1867)304Balance Sheet for the Financial Year ending August 31st, 1867311The Fenian Prisoners at Manchester and the International Working Men’s Association312To the Secretaries and Members of the International Working Men’s Association (Questions on Labour Statistics)314To the Members of the International Working Men’s Association (Draft Programme for the Brussels Congress)315The Geneva Lock-Out. To the Editor of the Star 317To the Trades’ Unionists of Great Britain and Ireland319The Fourth Annual Report of the General Council of the International Working Men’s Association324Editorial MaterialExplanatory Notes331Name Index413Index of Periodicals438Index of Addresses and Geographical Names440IllustrationsFirst page of the Minute Book for 1866-68 (General Council meeting of September 18, 1866) containing Marx’s remarks on the reaction in the German papers to the English tailors’ strike28-29Leaflet with the programme of the public meeting held to commemorate the anniversary of the Polish Insurrection of 186376-77Page of the Minute Book with Minutes recorded by Shaw and Fox88-89Page of the Minute Book (General Council meeting of July 23, 1867), with the printed text of Marx’s speech on the statistics in a new Blue Book 144-5House in High Holborn, London, where the General Council held its meetings from June 1868 to February 1872208-9Page of the Minute Book (General Council meeting of July 28, 1868) with a motion by Marx that a delegate be sent to the Congress of the German Workers’ Unions to be held at Nuremberg232-3Application from the Operative Bricklayers’ Society, dated February 21, 1865, for affiliation to the International260-1Page of the Minute Book (General Council meeting of July 9, 1867) with a motion by Marx concerning the agenda of the Lausanne Congress of 1867296-7