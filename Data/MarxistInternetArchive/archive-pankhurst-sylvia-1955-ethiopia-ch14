E. Sylvia PankhurstTHE various Ethiopic versions of the story of Alexander
the Great must not be regarded by European readers as
history in the sense of an attempt to present an accurate
record of historical facts, nor were they so regarded in Ethiopia
at any time. Still less must they be considered as Scriptures in
the Biblical or other religious sense. On the contrary, they are
romances making no claim to factual accuracy or historic truth,
but offered to the reader as imaginary stories by which their
authors sought to convey in an attractive form certain ethical
conceptions and advice on conduct, faith and morals.
In short they may be classed as belonging to the category of
such romances as John Bunyan's Pilgrim's Progress and Holy
War. Nevertheless, written at a much earlier period, cast in the
Ethiopian idiom and environment and dealing with the amazing career of Alexander, they differ greatly from the romances
of Bunyan.
They include an original Ethiopic work which is accurately
described as a "Christian Romance", and free translations of
the following works, which have all been infused by Ethiopian
authors with Christian ideals and used by them as occasions for
inculcating moral precepts. Budge, who was Keeper of Egyptian
and Assyrian antiquities at the British Museum and an Ethiopic
scholar, made fine translations of various Ge'ez versions of the
Alexander story then available either in British or French
libraries.1 The cost of the work was borne by Lady Meux.
These Ethiopic versions of the Alexander story are six in
number. Most of the incidents occuring in the works on which
each of these Christian versions is based are woven into the
narrative, but are modified to suit the Christian purpose of the
author, or it may be, in some cases, a succession of authors and

revisers. Alexander's victory in the chariot races described in
the Greek version of the Pseudo-Callisthenes is discarded.
Greek gods are transformed into Old Testament prophets.
Pagan kings and queens express Christian sentiments. The date
of the originals of the Ethiopic versions is not known, but Budge
believed they were written in the thirteenth century. The six
Ethiopic manuscripts translated by him comprise:


I. THE HISTORY OF ALEXANDER THE GREAT
(356-323 B.C.), ATTRIBUTED TO THE PHILOSOPHER
CALLISTHENES.
This is a fabulous story, written in Egypt with the embellishments of magic and sorcery current in the Egypt of the time.
Though it is ostensibly the work of Callisthenes, this writer was
dead before many of the events it records occurred. The author
is therefore termed the Pseudo-Callisthenes.
A copy of the Ethiopic version was among the collection of
manuscripts made by the Emperor Theodore for the library of
the Church of Medhane Alem (the Redeemer of the World),
which he intended to build at Magdala. This manuscript was
brought to England after Theodore's death by the British force
under Sir Robert Napier which fought at Magdala. It was
received at the British Museum in 1877, where it is numbered
Oriental 826.
It is a handsome manuscript written on vellum, containing
296 large pages measuring ii inches by 7 inches, boldly
written in beautiful script, each page having two columns of
from 24 to 26 lines. The title and the first few words of each
section are written in red ink. It is bound in leather, tooled,
with a cross in the centre.
The original of the Pseudo-Callisthenes is believed to have
been written at Alexandria by an Egyptian, probably in
Demotic, one of the Egyptian languages. It is an agglomeration
of the legends and miraculous fabrications which gathered
readily around the extraordinary personality of Alexander and
his meteoric career. The story gained amazing popularity; it
was translated into language after language, the translators
adding new details. To the Egyptians Alexander was the son of
an Egyptian King who had disguised himself as the God
Ammon for which reason Alexander was designated the two-
horned. To the Persians Alexander was a Persian, although a

conqueror of Persia. The Moslems made him speak as a follower of Mohammed the Prophet.
In the Ethiopic version Alexander is represented as a Christian saint, his father, Philip of Macedon, as a Christian martyr.
The date of the Ethiopic version is not known to western
scholarship. The copy of it in the British Museum is of recent
date. Budge, who used it for his translation considered the style
of the writing of this copy was of the nineteenth century.
There are three Greek versions of the Pseudo-Callisthenes,
known as A, B, and C, a Latin version translated by Julius
Valerius, also Armenian, Syriac, Malay, and Siamese versions
and a Coptic remnant discovered at Panopolis and written in
the dialect of upper Egypt.


2. THE HISTORY OF ALEXANDER BY JIRIS IBN-
AL-AMID ABUL-YASIR, KNOWN AS AL MAKIN AND
GEORGE WALDA AMID. The author of this work died at
Damascus about A.D. 1273-4. It was written originally in
Arabic. The father of Al Makin, originally a Christian monk in
Egypt, became Secretary for the Moslem wars in Egypt and
Syria under Salah ad-Din. The Alexander story is one episode
in this history.
A handsome Ethiopic version of Al-Makin's history was in
the Emperor Theodore's collection at Magdala, and is now in
the British Museum (Orient 813). Budge made an English
translation of the section devoted to Alexander; he considered
the handwriting was of the seventeenth century, but no doubt
many earlier copies exist, or formerly existed. Its 376 large
pages each measures 13 5/8 inches by 11 5/8 inches, and each
contains three 32 line columns of beautiful script.


3. THE HISTORY OF ALEXANDER BY ABU SHAKER
PETROS, IBN ABIL-KARAM IBN AL-MUHADDIB,
KNOWN AS IBN AL RAHIB, OR WALDA MANAKOS.
A Ge'ez version of this history written in the reign of lyasu I
(1682-1706) was also brought from the Emperor Theodore's
Magdala collection to England and preserved in the British
Museum, but in 1872, it was returned to Ethiopia at the request
of the Emperor John IV (1868-1889), Budge therefore, used for
his English translation an Ethiopic manuscript (No. 46), which

was in the French Bibliothèque Nationale in Paris, and was
written in the reign of the Emperor lyasu I (A.D. 1682-1706).
Abu Shaker, who wrote in Arabic was a Christian and a
deacon in the Church of the Virgin at Al Muallakah in Cairo,
and lived in the thirteenth century.


4. THE HISTORY OF ALEXANDER BY JOSEPH BEN
GORION. The translation of Budge is an extract from the
Ge'ez version of the Hebrew text. Ben Gorion's entire history of
the Jews is preserved in the British Museum (Oriental 822). He
lived in the ninth or tenth century A.D.; he is also known as the
Pseudo Josephus. The date of the Ethiopic version is unknown;
the handwriting from which Budge made his copy he ascribes
to the seventeenth century.
Alexander's visit to Jerusalem is described in Ben Gorion's
history much as in that of Josephus.


5. AN ANONYMOUS HISTORY OF THE DEATH OF
ALEXANDER. This Ethiopic manuscript by an anonymous
author gives a brief account of Alexander's life and death.
Budge used for his translation a manuscript in the British
Museum (Add. 24,900).


6. THE HISTORY OF THE BLESSED MEN WHO
LIVED IN THE DAYS OF JEREMIAH THE PROPHET,
AND VISION OF ABBA GERASIMUS (PARIS MS. 146).
This refers to the Sages or Brahmins whom Alexander is supposed to have visited in India or in Taprobane (Ceylon), and
who are visited in this romance by St Gerasimus. There is also
an episode of Alexander and a scarlet cloak, an emblem of
sovereignty which he took from the Jews as "King of the
World", but after three days put it off, saying: "Thy will be
done, O God, my God."


A HISTORY OF ALEXANDER INCLUDED IN
THE SUMMARY OF GENERAL HISTORY BY JOHN
MADABBA, KNOWN AS JOHN OF NIKIU IN EGYPT,
TRANSLATED INTO ETHIOPIC FROM ARABIC BY
JOHN OF KALYUB, IN 1602. This version was translated
into French by Zotenburg, Paris 1876, into English by Charles,
1916.
The "Christian Romance" of Alexander is an original
Ethiopian work, not a translation. Budge rendered it in English
from an Ethiopic manuscript in the British Museum (Oriental
827). He was unable to ascertain either the date or the author.
Alexander the Great is here likened to Elijah and Elisha, the
prophets, and to John the Baptist, for virtue, humility, fasting
and prayer. Therefore he was given dominion over a thousand
kingdoms. The book opens with this inscription:


"In the name of God Almighty, the triune God, the God of
olden time who never had a beginning, the God of after time
who shall never come to an end, we begin to write this book
of the exploits of Alexander the King, the beloved one of
God; may his prayers and blessings be with him that loveth
him. Amen."


Exhortations follow to love God and to live virtuously, to
every man to content himself with one wife, to every woman to
be faithful to her husband. These homilies, which are combined
with references to Biblical characters from Adam and Eve onward and many Scriptural quotations, serve as the introduction
to an allegorical and wholly imaginary account of the exploits
of Alexander.
It is here told that his father was Philip of Macedon, "unto
whom had been given the spirit of prophecy and who knew
what would come to pass long before it happened, being wont
to make observations with the astrolabe, an instrument for
taking the altitude of the sun."
Having inherited wealth from his father and a kingdom from
the sovereign of Kelkedon, Alexander served God and overthrew idols, and "went about doing battle with unclean
nations". His position is stated thus:


"Now Alexander first of all shaped aright the actions of his
body, and then God Almighty shaped for him aright the
things which concerned his soul; and Alexander shaped
aright the Kingdom of his Lord unto the ends of the earth,
and then God Almighty became his helper in everything he
asked from Him."


We are told that Alexander journeyed to the land of the
nations whose kings were Gog and Magog, "viper and serpent
nations who were generations of fiends and devils", with faces
of dogs and hands of men, and tails of lions, wings of eagles,
teeth of swine, "which eat the flesh of every animal and devour
whatever they find, trees, bark and roots, rejecting nothing".
There were twelve tribes of people with faces of animals, bulls,
crocodiles, bears, elephants, buffaloes, caterpillars, eagles, who
were devourers of men, who drank water and blood and fed on
clean and unclean meat.
The country of these terrible tribes was precipitous and
rugged, misty and dark, with hail and clouds and frost and
snow. It reached to the land of darkness, and there was a place
of going forth from it, "through which those nations were wont
to go out to war with the children of men".
The sight of that country troubled the soul of Alexander; he
wished neither to hear the sound of the voices of these people,
nor to see their tribes. "So he covered his face and grieved, for
they were a generation of vipers which had no fear in them."
Nevertheless, it is written: "God Almighty in the abundance of
His mercy put into their hearts fear and trembling" when they
heard the sound of Alexander's horn.
As they were gathered together in the narrow pass which led
out of their country Alexander was able to set up a gate of brass,
to preclude their egress, and to seal it "with a threefold seal,
which could never be broken" "until God Almighty should
desire to bring them forth by His decree, to make an end of the
world after the coming of Antichrist."
Now Alexander prayed that "inasmuch as the generations of
the children of men were weary", they might not again be terrified by "the faces of these terrible peoples or by their bitter
words". And it was plain to him that, having "prayed in pureness, power had been given unto him".
He journeyed till he arrived at a citadel built by the children
ofJaphet of adamant, stone, brass, iron and lead, the height of
which was 30 cubits. It was fitted "with every kind of strong
work and contrivance", but because of the wickedness of the
inhabitants they had all died in a single night. It had been their
custom to close the gates of the citadel and to dance all day and
all night to the sound of brass musical instruments, organs,
pipes, flutes, horns and drums which they had made "with

wheels and pulleys and levers and a host of cunning contrivances. Brass figures of men and women continued dancing even
when the people stopped." The roofs of the houses were of gold
and silver decorated with precious ornaments; the utensils and
vessels of great value in the city were innumerable.
Alexander camped for eight days outside the walls of the
citadel; then he ordered his soldiers to surround it and make an
entry. The soldiers could find no means of doing so. No men
answered their summons; the gates were all locked and bolted.
The soldiers returned to Alexander, telling him they had heard
most wonderful music; the people inside the city were singing
songs to the sound of drums, organ, flutes and pipes.
Alexander therefore commanded that lofty trees be felled
and ladders made whereby the soldiers could ascend the walls
and see into the city.
When some of the soldiers climbed to the battlements and
saw the figures dancing to the music, they cried, "Good and
excellent", and straightway fell down on their faces into the
citadel from whence they could not return. When other soldiers
climbed up the ladders, the same thing happened.
Eventually, Alexander himself mounted to the battlements to
discover why his soldiers did not return. He saw the amazing
mechanical contrivances, the brass figure of a man to which the
other brass figures of men and women were dancing, clapping
their hands, shuffling their feet, blowing the horns, beating the
drums and playing all the other musical instruments. He knew
that these contrivances were of brass with no breath of life in
them. "They were only vain things which were moved from
beneath by levers and wheels". He saw on the inner side of the
battlements steps leading down into the city, and measured the
distance thereto with his eye. The soldiers had not been able to
see the mechanical contrivances without throwing themselves
headlong into the city; but Alexander cautiously returned to
the ladder and descended by the way he had climbed.
He brought up to the battlements "three chosen men", who
were blindfolded that they might not see the dancing figures.
He guided them with ropes and directed them until they had
descended by steps leading down to the ground within and had
groped along the walls to the gates and opened them.
Alexander then entered the citadel, a perilous place, girt
about by fire and water, chains and precipitous places. He


found within the walls an inscription foretelling his own arrival
170 years after the former inhabitants had died of a pestilence,
and warning him of the dangers to be encountered therein, "a
place of suffocation", "a gulf" and "an abyss".
Alexander, having braved these dangers and examined "the
places of ambush", gave directions by which "his chosen men"
could follow him safely. "A vast mass of incalculable spoil" was
then brought out of the citadel, "gems of great price which
shone like lamps in the darkness, jewels gotten in the sea, pearls
and chalcedonies and topazes, loads of gold and silver". Every
horse and ass was laden and none took goods of lesser worth
than gold and silver.
Alexander examined the brazen figures and the musical
instruments "and every kind of mechanical contrivance". "He
understood the marvellous thing; he uncovered the ground
below it, and saw all the mechanism, the levers and the wheels
and the cords."
When he had bought out in their proper order all these
wonderful objects "so that the skill and cunning of the contrivance might not be destroyed", he "wrought a sign" by which
serpents were made and set up on the road "to terrify men from
coming to the citadel".
Then Alexander gave thanks to God, and prayed that his
undertakings might prosper, and begged that the children of
men might be delivered.
"And the Spirit of God Almighty came upon him, and told
him of what had been and what should come to pass, and
brought close together the two mountains called Madiken and
Kerafteran, leaving a space of but 23 cubits between.
In that space Alexander set up the images of brass, the organ,
the trumpet, the drum and the cymbals, and all the contrivances for music and dancing, and he made a hollow in the
ground to enclose the works of brass and the fixtures and fittings
and set them up, "even as he had seen them in the citadel of the
children of Japhet". He sealed all this "with three seals and
laid three enchantments upon it", so that no man should be
able to work against it successfully, either by arts and crafts or
mechanical contrivances.
Thus the musical instruments and the brazen images remain
there, "playing and dancing until this day".
When the serpents heard the music they were terrified and

fled to the ends of the earth. When they returned to gnaw away
the mountains and remove the brazen figures, they heard the
sound of the trumpet, and imagined Alexander the King was
there. A boundless fear and trembling seized them; they fled to
the ends of the earth and there they dwell until this present day.

Alexander now turned to complete the great city of Alexandria, in Egypt, which he had previously founded, "and he
caused the river of Ethiopia to flow above the city so that the
people might drink thereof", and that good food might be
brought to the city, and he made sluice gates that the waste and
refuse from the city might be carried by the waves into the
great sea.

"He paid gold wages to every man who worked for him, for
Alexander was a God-fearing man, and to every man that
wrought he gave a golden dinar each day."

Having heard of the Country of the Living, Alexander
prayed that he might go there and immerse himself in the water
of life. To reach that desired land he traversed the Country of
Darkness with his soldiers, in boats drawn by great birds,
much larger than eagles, which drew them on their journey
through the darkness into the light.

Then Alexander and his soldiers saw the City of Saints:


"where the water is sweeter than the grapes of which wine is
made and the stones are of sard, chalcedony, jacinth and
sapphire, where there is neither freezing cold nor parching
heat, neither summer nor winter, neither oppression nor
tears. Instead of snow, manna falleth from heaven. All their
wells and rock cisterns are filled with honey, and every beast
among them is filled with milk."

Alexander and his troops dwelt in great happiness in this
pleasant land; none wished to leave it, "for it was graciously
provided with everything".

Allowing for some measure of poetic fancy, one may imagine
the author of the Romance had in mind the Ethiopian highlands, with their fertile well-watered soil, abundance of honey,
great herds of cattle, and climate of eternal spring, wherein
there is "neither freezing cold, nor parching heat".

From this delightful country Alexander and his soldiers
"journeyed into a desert region where there were no houses".
Here the poet may have thought of the dry and sultry lowlands

of the Dankali and the Ogaden. But as Alexander journeyed
on, we are told, his horse was "seized by the Spirit"; he was
borne aloft to the "high places of the earth", and was brought
"into the Spiritual tabernacle, where Enoch and Elijah dwelt".
On seeing these Prophets, he asked them whether they were
human or spiritual beings; they answered they were men like
himself, and promised to tell him all he desired to know.
Alexander asked them why they thus dwelt alone. "What
solace have ye? What manner of existence is yours?"

Enoch, "the scribe of the decrees of God Almighty", replied
their existence was a happy one, without sickness or pain,
sorrow or Satan; their solace was converse with the angels and
with God, who made their hearts glad. Their drink was the dew
of life which flows from Paradise; they sought no other. Tongue
could not tell the gladness they enjoyed in converse with "the
angels, the righteous, the martyrs, the shining ones, the elect".

The place where they dwelt was the storehouse of Life; there
was no death therein, whether of feathered fowl or fish of the
sea. Every tree bore the fruits of Paradise, and on every herb
manna descended from heaven. It was because Alexander was
pure that he was able to converse with these immortals.

Elijah urged Alexander to lead his companions also in the
path of virtue, and assured him that he would inherit the world
of life with the martyrs and the righteous.

While Elijah still spoke, chariots of fire appeared, and Enoch
and Elijah were carried away, before Alexander had "had his
fill of the graciousness of their countenances".

Then Alexander mounted his horse and came back among his
soldiers. They told him they did not wish to return to their own
country, but to abide in that place which was the "storehouse of
gladness". Alexander replied that whether they would dwell
there did not depend upon his own will or upon theirs, but so
long as God willed they would abide there.

A fisherman approached Alexander, bringing some fish
which he had caught in a milk-white stream. The King's servants endeavoured to cook the fish, but they continued to live
when put on the fire, and even when cut to pieces with a knife
they came together again unhurt. Alexander questioned the
fisherman, who told him he had himself bathed in the water of
life and promised to guide Alexander and his soldiers to it, but
when they had travelled a great distance without reaching the

milk-white stream he ran away. Eventually Alexander's
soldiers caught him, but he refused to lead them further, and
when they struck him in anger they discovered he could not be
harmed.

With considerable eloquence and in a style which doubtless
was appreciated at the time and by the people for whom the
romance was written, the narrative roams through the Bible,
giving what doubtless appealed to its author as clarifications of
Biblical incidents which had seemed to him obscure.

He was probably one of the debteras, well versed in the
Scriptures. He adorns the Biblical narrative at times, either
from his own imagination, or by drawing upon the legends and
traditions concerning the Israelites and their Prophets, Jesus
Christ, the Virgin Mary and other Biblical characters then
current in the Middle East, which was of course, the scene of all
the events described in the Old and New Testaments.

The existing manuscripts of the Alexander stories are relatively modern, though copied from older originals. It is probable the Christian Romance was progressively modified by a
succession of Christian scribes, for it contains many inconsistencies. A higher ethical concept appears at times to have been
imposed upon older, more barbarous ideas. Thus when it was
found that the fisherman who refused to lead the way to the
Water of Life could neither be coerced by violence nor punished by death, he was chained with fetters of iron and cast into
a pit of darkness. Yet a few pages later we read that being frustrated by the recalcitrant fisherman from immersion in the
Water of Life which would have rendered him immortal,
Alexander declared to God: "The Kingdom of Heaven is better
for me than a kingdom on earth, lest Satan should attack me,
for I would come unto Thee pure and undefiled."

Moreover, though in all the Ethiopic versions of the Alexander story he is lauded for his numerous wars and conquests,
which are said to have been waged in the service of God, when
in the Ethiopic version of the Pseudo-Callisthenes, Alexander
visits the Brahmins and asks them: What is the most wicked
thing in all creation? The answer given by these sages is: "Look
at thyself and consider what thou hast heaped up, and how
many men have perished by thy hands."

It is told in the Christian Romance that Alexander, realizing
he had been denied the Water of Life, appealed to God for a

sign by which he would know when his death was near and
might console his mother. The Holy Spirit replied that Alexander must anticipate death when the heavens above him
appeared as brass and the stars as gold and silver.

Anticipating his end, he now threw open his treasure-house,
and called the poor and needy to take from it the gold and
silver and costly apparel, even the precious stones set in the
walls, and the lamps—all the possessions of his palace. Everyone marvelled, for the poor spared nothing, not even the paving
stones, the walls or the roof; they left not even the smallest brass
coin.

Alexander however declared that he had now found treasure
which could never be stolen. Henceforth he lived like the prophets, eating only green herbs, patiently fasting, and praying
throughout the night. "He fed the hungry and clothed the
naked; no wrong was done by him to any man."

Nevertheless, though he is thus portrayed as a Christian
ascetic, we are told, incongruously, that he still waged war,
though it is said "only on the wicked, the soothsayers and the
idolators".

On the instructions of Enoch, as it is said, Alexander wrote
"a book of wisdom", giving advice to posterity. Herein he recalls that Moses told Joshua the son of Nun to give daughters
their share of the inheritance. He advises parents to instruct
their children in the Law, to teach them to read the Scriptures
and to work with their hands. "Let women teachers instruct
the virgins and men teachers the young men, and when the
time of their full age hath arrived let them examine them
according to the law." The need of educating women was
therefore not ignored by the author of the Romance.

Alexander urges kings and governors to remember they are
beings who will pass away and in the day of their death their
riches will be vain. He warns them not to pervert justice, nor to
persecute the poor, not to take the land of the orphans, or to
refuse redress to the widow, but to give of their possessions to the
needy and the stranger, in order that they may find wealth in
"the storehouse of life, where there is no merchandise and no
market." To kings he said: "To take vengeance is an accursed
thing in a king, but to be long suffering and refrain from
evil is blessed. Say not, O King, 'Who shall rebuke me? Who
shall set himself in opposition to me? I am over and above all,'

for when thou sayest these things an evil spirit draweth nigh
unto thee."
At length God appeared to Alexander, and told him he had
ended his fight. "And now come rest thyself in the habitation of
Abraham, Isaac, Jacob, David and Solomon", for though he
was "not of their race", he was "their kinsman" by his works of
righteousness.
On hearing God's words, Alexander "was smitten with
weakness". He called his "chosen ones" to prepare a darkened
chamber. This they did, and "hung it about with spears and
shields of gold and silver. On seeing this Alexander recalled the
warning of the Spirit of God that his death should come when
the heavens above him appeared as brass and the stars as gold
and silver.
He instructed one of his friends to tell his mother, Olympias,
that he had "conquered in the war of the body" and come to his
rest. She should prepare a feast that he might eat and make
merry and might not die; but the water for the feast she must
take from a house wherein no man had ever died.
His mother at once hastened to comply with his desire, but
though she searched throughout the great city of Macedonia,
she could find no such house, for "death had gotten dominion
over all the race of men". In vain she asked, "Is there no man to
be found of whom some kinsman hath not died?" At last she
understood: "My son, in his wisdom, hath sent me this message
that I might comfort myself for his death."
Then Olympias cried out and fell on the earth and wept,
lamenting for her son. But when she became calmer she said:
"I give thanks to God Almighty, for though my son is dead
he shall live in Him, for he kept his body pure and undefiled,
and he wrought the will of his Lord, and his name is honourable.
"His strength was mighty like the lion, and at his roarings
the sea was moved, the abyss was poured out, nations were conquered, kings submitted unto him, and mountains burst into
flame…God Almighty was the light on his path. The country
of darkness terrified him not.
"What man born of woman who hath lived before thee can
be found like unto thee, O my son Alexander? He turned not
away his face from the enemy until he had broken the power of

the foe; there was never a time when a man could say of him,
'He is vanquished.' ... With what words now shall I lament for
my son? Is it a question of his handsome form? There was no
beauty like unto his. His eyes were bright and shining like
lamps in the darkness, and his figures was straight as a palm.
"…Woe is me, O my son, my honour and my ornament.
Woe is me, O my son, my charm and my beauty. Woe is me, O
my son, for my womb is burnt up and my heart scorched,
and I am shrivelled like grass before the fire.
"O father of the orphan, stay of the widow, comforter of the
sorrowful, helper of him that hath no helper, creator of all men,
vivifier of all men…be thou to my son Alexander a place of
rest and a habitation of delight."
On his death-bed Alexander rebuked his friend Komsat, one
of the Macedonian generals, who wept on his pillow, saying,
"Woe is me. Art thou then a man? And must thou also die,
even as other men!"
When the moment of death came, Alexander implored God
to receive his soul in peace, and amid a sound of thunder a voice
was heard saying, "Come in peace into the Kingdom of
Heaven." A spiritual hand appeared which shone with a light
mightier than the light of the sun, and Alexander's soul went
forth and took its rest in glory.1.  Sir E. A. Wallis Budge, Litt.D., F.S.A., The Life and Exploits of Alexander the
Great, a Series of Ethiopic Texts with an English Translation and Notes. Two vole.
Printed for Private Circulation, 1896. 
Table of Contents  |  Sylvia Pankurst Archive