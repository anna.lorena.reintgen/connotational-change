Jack Fitzgerald
Source: Socialist Standard, December 1920.
Transcription: Socialist Party of Great Britain.
HTML Markup: Adam Buick
Public Domain: Marxists Internet Archive (2016). You may freely copy, distribute, display and perform this work; as well as make derivative and commercial works. Please credit "Marxists Internet Archive" as your source.
The Threefold State. The True Aspect of the Social Question. By Dr. Rudolf Steiner. London: G. Allen & Unwin, Ltd. 5s. net.
The Adaptability of Capitalism.
Capitalism is a wonderful system of society. Its powers of production are colossal: its products more amazing than any dream of the Arabian Nights. Huge structures, mighty engines, gigantic ships, tremendous means of transport, mark its march across the earth. But along with the giant powers and products exist problems of such seeming complexity, and so far-reaching in their effects, that all the spokesmen, defenders, apologists, and believers in the system are bewildered at the appalling appearance of these problems, and vainly seek a solution within the limits of the system.

Their failure to find such a solution raises the hopes of those who, with fiery enthusiasm taking the place of knowledge, look to the overthrow of capitalism as a result of the insolubility of these problems, in the immediate future.

Capitalism, however, has shown remarkable powers of recovery from shocks, and it is easy to overestimate the speed at which it may succumb to its own contradictions.

The world war lasting over four years was a terrific strain upon its powers ; yet it not only supported millions of men as combatants, but supplied stupendous quantities of materials of a solely destructive character in addition to maintaining both the combatants and the producers.
The Strain of "Peace."
If ever, it stood the shock of a mighty war tolerably well, it is sustaining the shock of partial peace badly. The inherent contradictions of the system come to the front with greater force because the war pushed forward mechanical and scientific developments in industry as a hothouse developes the growth of a plant. As a result of this development the problems inevitably bound up with capitalism loom larger, show sharper, and press harder than before.
Pannicky "Intellectuals."
Apologists and defenders of capitalism are growing uneasy, and even alarmed, at the menacing aspects of these problems. The bolder of them seek for some reorganisation of the system that, while it leaves the capitalists secure, will palliate the evils existing. In this country perhaps the chief example of this idea is Mr. and Mrs. Sidney Webb's Constitution for the Socialist Commonwealth.

It is claimed that on the Continent the chief work on this subject is Dr. Rudolph Steiner's Threefold State. The collapse of Austria as a result of the war, and the appalling condition of Eastern Europe generally, have shown the evils and dangers of the present situation more clearly there than they appear here. Under such conditions the primary need for food places all else in a secondary position. Hence the professional sections find themselves pushed into the background and in danger of being submerged in the chaos.
Sounding the Alarm.
This book, with its threefold State as a solution for the evils of capitalism, is a vivid expression of the professional's attempt to restore order out of confusion.

The author first draws attention to the attitude of the working class. He warns the master class that the workers have become "class-conscious," are students of Marx, and are developing a scientific mode of thought.

He claimsthat the most powerful driving force in the world of labour is the system of thought," and says that it is to some extent the first of its kind in the world to take its stand solely on a scientific basis. (P. 34.)According to him the great evil of the present system is not that it robs the workers of the material wealth they have produced, though the fact of the robbery is admitted, but —It is because machinery and capitalism could give the working man nothing to fill and satisfy his soul as a human being that the working class movement was driven to seek for its fount of inspiration in the direction of modern science. (P. 12.)What the Workers Want
It is true that the worker is not really conscious of this "soul aspiration" and if asked would deny it, claiming that what is required is freedom from exploitation ; but our author denies that such freedom alone would solve the problem, and on page 34 asks :Suppose we find anywhere signs that there is a life of the soul having its source in the spirit of the times, bearing mankind up and down with it as it goes, and rooted in a spiritual reality, then from this soul life may come the force which shall give the right impulse to the social movement also.and on page 87 he says that —in the domain of mind and spirit there reigns a reality that transcends material external circumstances and bears within itself its own matter and substance.Meaningless jargon of this sort prepares the reader for the author's remedies. According to him human societies should function in three independent but connected channels. He admits that to-day the economic factor pervades — nay overrides — all other activities in society but this he holds is one of the great causes of the growing chaos. Mankind has three sides to its character, and the State should be reformed in harmony with these characteristics.
Here is the New Utopia.
First should come the Economic State, concerned with the production, circulation and consumption of commodities.

Secondly should come the "Equity State" that deals with the common rights, the relation of man and man in social affairs.

Thirdly there is the "Spiritual State." This "must comprise all that concerns the life of the mind and spirit" or "everything that must play a part in the body social by reason of those natural attributes of the individual being, whether those aptitudes be qualities of mind or body." (P. 57.)	
Here we reach a world of shadow and vagueness, in fact, the only thing standing out clear and definite in this threefold State is that private property in the means of life will stillThe Equity State must not prevent the formation and control of private property in capital, so long as the connection between the capital-basis and individual ability remains such that the private control implies a service to the whole body social. (P. 131) Nothing New Under the Sun.
It is to be under control that is on lines curiously similar to those laid down in the British Government's new Agricultural Bill, where, having guaranteed the farmers a price for their goods, it is enacted that if a farmer does not carry on production efficiently the Government will take control of the land.

Dr. Steiner's scheme covers all branches of production, but there the Government does not take control itself, but merely hands the business on to another capitalist or group. The capitalist concerned, however, will have the right to pick out an individual or group to whom the property shall be transferred.

Under this section the worker is not to treated as a commodity, but must have "the conditions of a decent human existence." These conditions being a question of man to man, will be arranged by the "equity State." To prevent the latter becoming contaminated by economic interests it must be rigidly excluded from any management of economic processes and must get rid of those now managed by the State, such as the Post Office, etc.
Forgotten Trifles. 
Who, then, shall decide the conditions of production ? The Spiritual State. This State will lay down the methods and details of the economic system, though the management will be in the hands of the Economic State.

The Spiritual State will lay down rules for guidance in Education— but not State Education. The author's ideal is to see State Education completely abolished, and all education left to private persons. Children are to have the "right to education," and the father of a family will have an income in excess of that of a single man to provide for his children's education.

The critical reader may here ask "How are these States to be formed?" the answer to which will be that of Masefield's vagabond—"Dunno." Whether they are to be elected or selected, nominated or appointed, hereditary or periodical, the author does not say. It is all left in beautiful, vague, one might almost say "spiritual" condition.
Steiner's Ignorance. 
How deeply the author has gone in his examination of present conditions is shown by his statements about the "useful functions" of the capitalists, and his childish belief that the "entrepreneur"—or "initiator" as his translator calls-it—still exists in modern capitalism. He has no idea that the same factor in economic development killed both these personalities at the same time. The joint stock company wound up the "useful functions" of the capitalist and converted the initiator into a salaried servant.
A Funk Hole Book.
The book is an interesting example of the attempts of the "non-productive labourers"—as Loria calls the "intellectuals"—to produce a plan that will show the capitalists a solution to their present difficulties, while still leaving them a ruling class, and which will lead the workers on another wild-goose chase by promising them a decent existence.

How imbecile these attempts are is shown by one simple factor in the system of capitalism.

Under private ownership of the means of life goods are produced to sell. In order to sell there must be buyers—that is, people able to purchase. Mere desire for, or even pressing need for,. The goods is not sufficient. One must be able to produce the cash. The market will thus be determined by the number of buyers multiplied by their purchasing power. Every student of economic development knows that the methods of production have improved and increased by leaps and bounds until the products have gone far beyond both the increase of buyers and their purchasing power. To meet this situation one of two courses must be followed.
Dubious Alternatives.
Either the workers must have their hours of work reduced until, even with increased means of production, only the amount of commodities required by the market is produced, or a sufficient number of workers must be thrown out of work to allow the remainder to stay at production to turn out the goods required.

The capitalists, of course, have not the slightest intention of adopting the first method. The second is in full swing now, though modified by a few doles.

The idiotic paradox of millions of producers being in want because they have produced so lavishly that their products are piled all around them will not be abolished by patching up administration. The evil is rooted in the private ownership of the means of life, and cannot be cured until that ownership is abolished.

Fabian quackeries and Austrian paper lids on volcanoes are equally ineffective. The only remedy is for the workers to capture political power and use it to abolish capitalism root and branch.
Jack Fitzgerald Archive| Socialist Party of Great Britain
