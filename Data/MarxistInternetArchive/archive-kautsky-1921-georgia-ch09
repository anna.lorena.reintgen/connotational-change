MIA  > 
Archive  > 
Kautsky  > 
Georgia The Menshevism of Georgia is the most
important, but not the only cause of the Bolshevist invasion. The world
policy of Moscow forms another reason. As Czardom did formerly, so now
Bolshevism, although from quite different standpoints, regards England
as the greatest and most dangerous enemy of Russia. And this great
Empire seems by its geographical position alone among all the Powers of
the world to control the road on which England can be dealt a blow, and
brought to her knee, without the mastery of the seas, namely, the road
to India.Soviet Russia is now playing with the grandiose idea of
Napoleon the First to attack England in India. Napoleon came to grief
by the first step which he took, as he could not stand up against the
English at sea. Without the victory of the English at sea, his failure
to penetrate into the interior of Asia would have been much more
inglorious, as it would have by mere insufficiency of the means of
transport prepared for him a Moscow at the very beginning, of his
military career.Soviet Russia does not need to repeat the first step of
Napoleon. It can commence with the second. This has lost none of its
difficulties, as a far larger army is necessary for the conquest of
India to-day than was the case at the end of the eighteenth century.
The Russians can hardly get very far without great railway
construction. Such works are out of the question in the present
condition of Russian industry. However, the plan is bold, and in
boldness the Bolshevists equal Danton and the first Napoleon. In this
quality, and not in their positive achievements, rests the great power
of attraction which they exercise over so many persons who live far
from their jurisdiction.One of the stages of the road to India is Persia, into which
the Bolshevists have already penetrated, although unsuccessfully, last
year. At that time, their basis was too narrow. It would be
considerably broadened by the possession of Georgia. Thus Moscow world
policy required this country for further military progress.As chance had it, Rosta at the
same time as it announced its account of the Georgian conflict had the
following dispatch from Moscow. “On February 28th an
agreement was signed in Moscow between Russia and Afghanistan.
“The West of Georgia is a part of Russia’s Eastern
policy directed against England.The likeness to the policy of Napoleon is a close one, and has
already been pointed out. But the resemblance is more than a mere
chance. We are struck more and more with the manner, in which the
course of the great French Revolution has been repeated in that of
Russia, although the international situation and ideology are of quite
a different order to-day than at the end of the eighteenth century.Montesquieu, Voltaire and J.J. Rousseau are scarcely read
to-day; Marx dominates the hour, and present-day Russia is not, like
the France of one hundred years ago, the most highly developed, but the
most backward of the countries of the European continent. But the
principal tasks, agrarian reform and the overcoming of Absolutism,
corresponded in Russia in 1917 so closely with the of France in 1789
that since that date the Revolution here has followed the same stages
as there, only in Russia with younger and simpler social sections in
considerably grosser forms.Here, as there, we find first of all a middle class
revolution. In France, it developed into the Reign of Terror of the
Jacobins, who were supported by the lower classes, especially in the
capital. In Russia the Reign of Terror of the Bolshevists, who proclaim
the Dictatorship of the Proletariat.In order to maintain themselves, the Jacobins found themselves
obliged to substitute for the bureaucracy, the police and the army of
the old regime, which had been abolished by the Revolution, a new
police and army, much stronger and more centralised than the old, and
therefore established that machinery of domination which was to lead to
the Empire of Napoleon.The Bolshevists have found themselves obliged to pursue the
same course. Gradually, they have more and more restricted the
self-government of the working class in the domain both of economics
and politics, created an all-powerful police apparatus, proclaimed the
dictatorship of the factory chiefs, reduced the Soviets to a shadow,
and instead have built up a great, strictly disciplined army, to which
all that remains of Russian industry is subservient.Thus, Soviet Russia has entered upon a phase of the Revolution
which corresponds with the third phase of the French Revolution, viz.,
the phase of Absolutism and the domination of the police and military
forces.We may class this the Bonapartist phase. The victorious
general is, indeed, lacking. Meanwhile, Russia is in the stage of the
Consulate of the two Consuls, Lenin and Trotsky.Like the Moscow Bonapartism, its French predecessor derived
from the Revolution, the allurements of which it retained, whereby so
many enthusiasts have been deceived. It is notorious that the fiery
republican, Beethoven, was in 1804 an enthusiastic worshipper of
Napoleon, immediately before the latter made himself Emperor. Napoleon
passed as the incarnation of the Revolution, only because the
reactionary powers hated him as much as the Revolution itself, although
the Napoleonic Empire already possessed a character which distinguished
it fundamentally from the Revolution.The present-day Moscow regime has as little in common with the
proletarian dominance of the State as the French Empire at the
beginning of the last century had with the Republic.The so-called Soviet Republic of to-day does not rest upon the
power of the proletariat, but on the strength of its army and on the
impotence of the proletariat against this army. As the strength of the
army grows so the power of the rulers of the State increases, but
simultaneously grows their dependence on the only element on which they
are able to support themselves, the military. Accordingly, a new
militarism is arising in Russia, and likewise a new imperialism. For
the latter, the impulse towards constant extension of power and fields
of exploitation is peculiar to militarism as well as to capitalism. The
need for employing his army, and constantly providing fresh booty and
advantages, drove Napoleon to that restless policy of conquest which
finally collapsed at Moscow. The same conditions are to-day creating in
Russia similar efforts on the part of the Moscow Imperialism.To this policy Georgia has now been sacrificed.It is important to make this quite clear. The effect upon us
would be disastrous if it were a genuinely proletarian Republic which
had suddenly invaded another proletarian Republic, a small, friendly
and peaceful community. To invade it without any declaration of war, in
the midst of peace, was indeed an infamy more wicked than the German
invasion of Belgium in 1914. For then Germany was engaged in a war for
its existence and the invasion was an episode of the world-war. The
Bolshevist invasion threatens to paralyse the whole of the Socialist
propaganda against the war and to brand it as humbug.Never before have wars wrought such destruction as to-day of
technical appliances for the needs of production and of communication,
and never before was peace so essential to the prosperity of the
peoples.It brings consolation, encouragement and hope to large
sections of people when we Socialists point out that it is capitalism
alone that renders war inevitable, and that the proletariat is the
force that will bring peace and maintain it.The world rule of the proletariat would be synonymous with
lasting world peace! And now we have two Republics, governed by the
proletariat, existing side by side, and one makes war upon the other
with a treachery that is seldom met with among capitalist governments.Were Russia still a proletarian republic, the events in
Georgia would inflict a serious blow on the whole of our propaganda, in
which we describe the proletariat as the firmest support of peace.Yet, in reality, the Russian proletariat has borne no share in
the invasion of Georgia, because it has ceased to exercise political
power in Russia. We are justified in continuing to assert that the
general rule of the proletariat will secure lasting world peace; that
between two States, equally governed by the proletariat, no occasion
for war will any longer arise; and that the international solidarity of
the workers will be strong enough to settle peacefully any possible
conflicts between two proletarian States.For the Russia which has just made this execrable invasion
into Georgia is no longer a proletarian, but a Bonapartist community.Far from rejoicing over the conquest of Georgia, the
proletariat of Russia vigorously condemned it, as was shown by the
protest issued in Berlin on March 3rd by the Foreign Agency of the
Social-Democratic Labour Party (signed by Abramovitch and Martoff). In
Russia itself, the proletariat is muzzled and cannot express itself
freely, but the Social-Democratic Party, that is, the Menshevists, is
competent to speak in its name. Times have changed since Bolshevism
forced Menshevism into the background and won to its side the mass of
the workers in the large towns. This was the case in the autumn of
1917, when the craving for peace outweighed every other consideration
among the masses, and the Bolshevists gave to it the most powerful and
unequivocal expression.Since that time the domination of Bolshevism has become
synonymous with constant war, with hunger and poverty, and also with
the complete suspension of every kind of liberty of movement for the
proletariat. Peace and freedom are to-day most stoutly championed by
the Menshevists; the mass of the Russian proletariat turns more and
more towards them; and the Bolshevists attempt in vain, by all means of
electoral shuffling, corruption, intimidation, bloody terror, to dam
the rising tide of opposition.The invasion of Georgia has been undertaken, not with the
concurrence, but against the wishes of the Russian proletariat. The
latter is free from the latest Moscow blood guilt.We are entitled to expect that the entire international
proletariat, so far as it does not obey the behests of Moscow, will
unanimously endorse the protest of our Russian comrades.The fear is groundless that such a protest will strengthen
French and English imperialism, which is hostile to Soviet Russia.
Quite the contrary. We blunt the points of our weapons in the struggle
against the imperialism of the capitalist Powers, if we are afraid to
denounce imperialism when it arises out of a proletarian revolution,
and discredits the latter. It is our business to remove the influence
of imperialist ways of thinking from the proletariat. How can we do
this if we tolerate an imperialism which masquerades in the name of the
proletariat?Yet another factor renders it necessary for the
Social-Democratic parties of the world to make a decisive stand against
the Moscow Bonapartism.The close parallel which exists between the course which the
Russian Revolution has hitherto followed and that of the great French
Revolution must not blind us to the differences between the two events.
In the eighteenth century France was the most progressive State of the
European continent. To-day Russia is still the most backward amongst
the great States of Europe. Although the French Bonapartism constituted
a strong reaction from the Republic, its policy of expansion brought
many improvements to the rest of Europe.
The present Moscow Bonapartism is not only reactionary in relation to
the proletarian revolution of Russia, out of which it arose, but even
more so in comparison with the proletarian movements of the rest of
Europe, which it seeks to fetter.A further distinction exists between the old Bonapartism of
Paris and the new one of Moscow.No class-conscious proletariat existed at the time of the
great French Revolution. The proletarian sections formed a tail to the
small middle class, an extremely divided and unreliable class, which
constantly swayed between obstinate resistance and cowardly submission,
between anti-capitalist discontent and capitalist covetousness.At the time of the Revolution this class was without the
slightest political experience. However wild its conduct had been
during the Reign of Terror, it was an easy matter for the Empire to
paralyse this class. The Empire was confronted with no other serious
opponents than the old legitimate foreign dynasties, which could not
forget the revolutionary origin of the new Emperor. For Continental
Europe at that time there were two alternatives, either Bonapartism or
the Holy Alliance.To-day we are far removed from this. The revolutionary
struggle is conducted, not by the small middle-class, but by the
proletariat, a class which, in contrast to the former, is of a
homogeneous character, and pursues a single object. It will not make
terms with capitalism, and much less will it permit any restrictions on
its liberty of movement. The workers are not always conscious of the
Socialist objective of their class struggle, as was shown in the case
of the English workers for more than a generation after the
disappearance of Chartism, but in all countries, and under all
circumstances, they zealously guard their freedom of movement. At times
they may be suppressed and forcibly held down, but this policy becomes
more difficult as they grow in numbers, as their political and
organising experience extends, and as they become pore indispensable in
an economic sense.For decades the proletariat has waged the class struggle in an
open and organised manner.Under these circumstances, the new Russian Bonapartism is
faced with quite a different situation from that of the old French
Bonapartism. The world is no longer confronted with the choice of two
alternatives, submission to the dictation of the new Absolutism, born
of the Revolution and the reaction; that is, between Moscow and the
Entente. A third possibility exists: the overthrow of the Moscow
Bonapartism from within, by means of the strengthening of proletarian
freedom, which is best represented by the growing power of Socialist
opposition.The victory of the Alliance over Napoleon signified the
triumph of reaction, and the defeat of the peoples of Europe for a
generation. But this victory was rendered unavoidable by the excesses
which are necessarily bound up with Bonapartism.The victory of the capitalism of the Entente over Soviet
Russia would likewise signify the victory of reaction and facilitate
the defeat of the European proletariat, even if not for so long as a
generation, nor to the same degree as before. In any event, the
proletarian class struggle would be considerably hampered.Consequently, the workers of all countries, whatever their
opinions of the Bolshevist methods, have resisted the efforts of the
Entente to crush Soviet Russia.But this does not imply that the Russian Bonapartism should be
defended against all criticism, especially against that which proceeds
from the Menshevists. This is called the defence of the Russian
Revolution, but is merely a defence of the exploiters of this
Revolution against the Social-Democratic opposition, which would be
best able to maintain and extend the revolutionary achievements.Not Bolshevism, but this opposition is now the real support of
the Russian Revolution. Its fate depends upon the victory of this
opposition, and its speedy victory.Russia is a peasant State, and will remain so for a long time.
Russia’s political future rests upon this fact, whichever
class or party may succeed in gaining the leadership of the peasants,
who are not fitted to pursue an independent class policy.Hitherto the Russian peasantry has followed the proletarian
leadership. The practice of the Bolshevists tends more and more to
alienate the peasants, and to make them disposed to accept a capitalist
or any other kind of counter-revolutionary leadership.It is not alone the victory of one of the
counter-revolutionary generals which threatens to make Russia once more
the citadel of reaction, but also the transference of the allegiance of
the peasants to the other side. This would be detrimental to the class
struggle, in Europe as well as in Russia.The defection of the peasants, who have hitherto been
revolutionary and led by the proletariat, can only be arrested by the
substitution of the Menshevist methods, so successfully practised in
Georgia, for the existing Bolshevist methods. Thus the most urgent need
of the hour, and the best means of saving the jeopardised Revolution,
is the overcoming of Bolshevism by Menshevism.It is the duty of the Social-Democrats of all countries to
assist Menshevism to the utmost extent of their power. This is the same
thing as working for the triumph of the methods of little Georgia. It
still lies crushed and mishandled by its overwhelming opponent, but
simultaneously the ideas which inspired it and made it capable of great
things are sweeping over the giant empire of its oppressor. Russia will
only be able to prosper when it is animated by the spirit that inspired
Georgia. This will constitute the revenge of the Social-Democratic
Republic of the Caucasus. Top of pageLast updated on
1.3.2017