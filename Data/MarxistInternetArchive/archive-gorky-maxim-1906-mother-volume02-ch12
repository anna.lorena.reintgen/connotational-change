
Maxim Gorky
 
The mother lay motionless, with ears strained in the drowsy
stillness, and before her in the darkness wavered Rybin's face
covered with blood.  In the loft a dry whisper could be heard.

"You see what sort of people go into this work?  Even elderly people
who have drunk the cup of misery to the bottom, who have worked, and
for whom it is time to rest.  And there they are!  But you are young,
sensible! Ah, Stepan!"

The thick, moist voice of the peasant responded:

"Such an affair--you mustn't take it up without thinking over it.
Just wait a little while!"

"I've heard you say so before."  The sounds dropped, and rose again.
The voice of Stepan rang out:

"You must do it this way--at first you must take each peasant aside
and speak to him by himself--for instance, to Makov Alesha, a lively
man--can read and write--was wronged by the police; Shorin Sergey,
also a sensible peasant; Knyazev, an honest, bold man, and that'll
do to begin with.  Then we'll get a group together, we look about
us--yes.  We must learn how to find her; and we ourselves must take
a look at the people about whom she spoke.  I'll shoulder my ax and
go off to the city myself, making out I'm going there to earn money
by splitting wood.  You must proceed carefully in this matter.  She's
right when she says that the price a man has is according to his own
estimate of himself--and this is an affair in which you must set a
high value on yourself when once you take it up.  There's  that
peasant!  See!  You can put him even before God, not to speak of
before a police commissioner.  He won't yield.  He stands for his own
firmly--up to his knees in it.  And Nikita, why his honor was suddenly
pricked--a marvel?  No.  If the people will set out in a friendly
way to do something together, they'll draw everybody after them."

"Friendly!  They beat a man in front of your eyes, and you stand
with your mouths wide open."

"You just wait a little while.  He ought to thank God we didn't beat
him ourselves, that man.  Yes, indeed.  Sometimes the authorities
compel you to beat, and you do beat.  Maybe you weep inside yourself
with pity, but still you beat.  People don't dare to decline from
beastliness--they'll be killed themselves for it.  They command you,
'Be what I want you to be--a wolf, a pig'--but to be a man is
prohibited.  And a bold man they'll get rid of--send to the next
world.  No.  You must contrive for many to get bold at once, and
for all to arise suddenly."

He whispered for a long time, now lowering his voice so that the
mother scarcely could hear, and now bursting forth powerfully.
Then the woman would stop him.  "S-sh, you'll wake her."

The mother fell into a heavy dreamless sleep.

Tatyana awakened her in the early twilight, when the dusk still
peered through the window with blank eyes, and when brazen sounds
of the church bell floated and melted over the village in the gray,
cold stillness.

"I have prepared the samovar.  Take some tea or you'll be cold if
you go out immediately after getting up."

Stepan, combing his tangled beard, asked the mother solicitously
how to find her in the city.  To-day the peasant's face seemed
more finished to her.  While they drank tea he remarked, smiling:

"How wonderfully things happen!"

"What?" asked Tatyana.

"Why, this acquaintance--so simply."

The mother said thoughtfully, but confidently:

"In this affair there's a marvelous simplicity in everything."

The host and hostess restrained themselves from demonstrativeness
in parting with her; they were sparing of words, but lavish in little
attentions for her comfort.

Sitting in the post, the mother reflected that this peasant would
begin to work carefully, noiselessly, like a mole, without cease,
and that at his side the discontented voice of his wife would always
sound, and the dry burning gleam in her green eyes would never die
out of her so long as she cherished the revengeful wolfish anguish
of a mother for lost children.

The mother recalled Rybin--his blood, his face, his burning eyes,
his words.  Her heart was compressed again with a bitter feeling
of impotence; and along the entire road to the city the powerful
figure of black-bearded Mikhail with his torn shirt, his hands
bound behind his back, his disheveled head, clothed in wrath and
faith in his truth, stood out before her on the drab background of
the gray day.  And as she regarded the figure, she thought of the
numberless villages timidly pressed to the ground; of the people,
faint-heartedly and secretly awaiting the coming of truth; and of
the thousands of people who senselessly and silently work their
whole lifetime without awaiting the coming of anything.

Life represented itself to her as an unplowed, hilly field which
mutely awaits the workers and promises a harvest to free and honest
hands:  "Fertilize me with seeds of reason and truth; I will return
them to you a hundredfold."

When from afar she saw the roofs and spires of the city, a warm joy
animated and eased her perturbed, worn heart.  The preoccupied faces
of those people flashed up in her memory who, from day to day,
without cease, in perfect confidence kindle the fire of thought
and scatter the sparks over the whole earth.  Her soul was flooded
by the serene desire to give these people her entire force, and--
doubly the love of a mother, awakened and animated by their thoughts.

At home Nikolay opened the door for the mother.  He was disheveled
and held a book in his hand.

"Already?" he exclaimed joyfully.  "You've returned very quickly.
Well, I'm glad, very glad."

His eyes blinked kindly and briskly behind his glasses.  He quickly
helped her off with her wraps, and said with an affectionate smile:

"And here in my place, as you see, there was a search last night.
And I wondered what the reason for it could possibly be--whether
something hadn't happened to you.  But you were not arrested.  If
they had arrested you they wouldn't have let me go either."

He led her into the dining room, and continued with animation:
"However, they suggested that I should be discharged from my
position.  That doesn't distress me.  I was sick, anyway, of
counting the number of horseless peasants, and ashamed to receive
money for it, too; for the money actually comes from them.  It
would have been awkward for me to leave the position of my own
accord.  I am under obligations to the comrades in regard to work.
And now the matter has found its own solution.  I'm satisfied!"

The mother sat down and looked around.  One would have supposed
that some powerful man in a stupid fit of insolence had knocked
the walls of the house from the outside until everything inside
had been jolted down.  The portraits were scattered on the floor;
the wall paper was torn away and stuck out in tufts; a board was
pulled out of the flooring; a window sill was ripped away; the
floor by the oven was strewn with ashes.  The mother shook her
head at the sight of this familiar picture.

"They wanted to show that they don't get money for nothing,"
remarked Nikolay.

On the table stood a cold samovar, unwashed dishes, sausages, and
cheese on paper, along with plates, crumbs of bread, books, and
coals from the samovar.  The mother smiled.  Nikolay also laughed
in embarrassment, following the look of her eyes.

"It was I who didn't waste time in completing the picture of the
upset.  But never mind, Nilovna, never mind!  I think they're going
to come again.  That's the reason I didn't pick it all up.  Well,
how was your trip?"

The mother started at the question.  Rybin arose before her; she felt
guilty at not having told of him immediately.  Bending over a chair,
she moved up to Nikolay and began her narrative.  She tried to preserve
her calm in order not to omit something as a result of excitement.

"They caught him!"

A quiver shot across Nikolay's face.

"They did?  How?"

The mother stopped his questions with a gesture of her hand, and
continued as if she were sitting before the very face of justice
and bringing in a complaint regarding the torture of a man.  Nikolay
threw himself back in his chair, grew pale, and listened, biting
his lips.  He slowly removed his glasses, put them on the table,
and ran his hand over his face as if wiping away invisible cobwebs.
The mother had never seen him wear so austere an expression.

When she concluded he arose, and for a minute paced the floor in
silence, his fists thrust deep into his pockets.  Conquering his
agitation he looked almost calmly with a hard gleam in his eyes
into the face of the mother, which was covered with silent tears.

"Nilovna, we mustn't waste time!  Let us try, dear comrade, to take
ourselves in hand."  Then he remarked through his teeth:

"He must be a remarkable fellow--such nobility!  It'll be hard for
him in prison.  Men like him feel unhappy there."  Stepping in front
of the mother he exclaimed in a ringing voice:  "Of course, all the
commissioners and sergeants are nothings.  They are sticks in the
hands of a clever villain, a trainer of animals.  But I would kill
an animal for allowing itself to be turned into a brute!"  He
restrained his excitement, which, however, made itself felt to the
mother's perceptions.  Again he strode through the room, and spoke
in wrath:  "See what horror!  A gang of stupid people, protesting
their pernicious power over the people, beat, stifle, oppress
everybody.  Savagery grows apace; cruelty becomes the law of life.
A whole nation is depraved.  Think of it!  One part beats and turns
brute; from immunity to punishment, sickens itself with a voluptuous
greed of torture--that disgusting disease of slaves licensed to
display all the power of slavish feelings and cattle habits.  Others
are poisoned with the desire for vengeance.  Still others, beaten
down to stupidity, become dumb and blind.  They deprave the nation,
the whole nation!"  He stopped, leaning his elbows against the
doorpost.  He clasped his head in both hands, and was silent,
his teeth set.

"You involuntarily turn a beast yourself in this beastly life!"

Smiling sadly, he walked up to her, and bending over her asked,
pressing her hand:  "Where is your valise?"

"In the kitchen."

"A spy is standing at our gate.  We won't be able to get such a big
mass of papers out of the way unnoticed.  There's no place to hide
them in and I think they'll come again to-night.  I don't want you
to be arrested.  So, however sorry we may be for the lost labor,
let's burn the papers."

"What?"

"Everything in the valise!"

She finally understood; and though sad, her pride in her success
brought a complacent smile to her face.

"There's nothing in it--no leaflets."  With gradually increasing
animation she told how she had placed them in the hands of sympathetic
peasants after Rybin's departure.  Nikolay listened, at first with
an uneasy frown, then in surprise, and finally exclaimed, interrupting
her story:

"Say, that's capital!  Nilovna, do you know--"  He stammered,
embarrassed, and pressing her hand, exclaimed quietly:  "You touch
me so by your faith in people, by your faith in the cause of their
emancipation!  You have such a good soul!  I simply love you as I
didn't love my own mother!"

Embracing his neck, she burst into happy sobs, and pressed his head
to her lips.

"Maybe," he muttered, agitated and embarrassed by the newness of
his feeling, "maybe I'm speaking nonsense; but, upon my honest word,
you are a beautiful person, Nilovna--yes!"

"My darling, I love you, too; and I love you all with my whole soul,
every drop of my blood!" she said, choking with a wave of hot joy.

The two voices blended into one throbbing speech, subdued and
pulsating with the great feeling that was seizing the people.

"Such a large, soft power is in you; it draws the heart toward you
imperceptibly.  How brightly you describe people!  How well you see them!"

"I see your life; I understand it, my dear!"

"One loves you.  And it's such a marvelous thing to love a person--
it's so good, you know!"

"It is you, you who raise the people from the dead to life again;
you!" the mother whispered hotly, stroking his head.  "My dear, I
think I see there's much work for you, much patience needed.  Your
power must not be wasted.  It's so necessary for life.  Listen to what
else happened:  there was a woman there, the wife of that man----"

Nikolay sat near her, his happy face bent aside in embarrassment,
and stroked his hair.  But soon he turned around again, and looking
at the mother, listened greedily to her simple and clear story.

"A miracle!  Every possibility of your getting into prison and
suddenly--  Yes, it's evident that the peasants, too, are beginning
to stir.  After all, it's natural.  We ought to get special people
for the villages.  People!  We haven't enough--nowhere.  Life demands
hundreds of hands!"

"Now, if Pasha could be free--and Andriusha," said the mother softly.
Nikolay looked at her and drooped his head.

"You see, Nilovna, it'll be hard for you to hear; but I'll say it,
anyway--I know Pavel well; he won't leave prison.  He wants to be
tried; he wants to rise in all his height.  He won't give up a
trial, and he needn't either.  He will escape from Siberia."

The mother sighed and answered softly:

"Well, he knows what's best for the cause."

Nikolay quickly jumped to his feet, suddenly seized with joy again.

"Thank you, Nilovna!  I've just lived through a magnificent moment--
maybe the best moment of my life.  Thank you!  Now, come, let's give
each other a good, strong kiss!"

They embraced, looking into each other's eyes.  And they gave each
other firm, comradely kisses.

"That's good!" he said softly.

The mother unclasped her hands from about his neck and laughed
quietly and happily.

"Um!" said Nikolay the next minute.  "If your peasant there would
hurry up and come here!  You see, we must be sure to write a leaflet
about Rybin for the village.  It won't hurt him once he's come out
so boldly, and it will help the cause.  I'll surely do it to-day.
Liudmila will print it quickly.  But then arises the question--how
will it get to the village?"

"I'll take it!"

"No, thank you!" Nikolay exclaimed quietly.  "I'm wondering whether
Vyesovshchikov won't do for it.  Shall I speak to him?"

"Yes; suppose you try and instruct him."

"What'll I do then?"

"Don't worry!"

Nikolay sat down to write, while the mother put the table in order,
from time to time casting a look at him.  She saw how his pen
trembled in his hand.  It traveled along the paper in straight
lines.  Sometimes the skin on his neck quivered; he threw back his
head and shut his eyes.  All this moved her.

"Execute them!" she muttered under her breath.  "Don't pity the villains!"

"There!  It's ready!" he said, rising.  "Hide the paper somewhere on your
body.  But know that when the gendarmes come they'll search you, too!"

"The dogs take them!" she answered calmly.

In the evening Dr. Ivan Danilovich came.

"What's gotten into the authorities all of a sudden?" he said,
running about the room.  "There were seven searches last night.
Where's the patient?"

"He left yesterday.  To-day, you see, Saturday, he reads to working
people.  He couldn't bring it over himself to omit the reading."

"That's stupid--to sit at readings with a fractured skull!"

"I tried to prove it to him, but unsuccessfully."

"He wanted to do a bit of boasting before the comrades," observed
the mother.  "Look!  I've already shed my blood!"

The physician looked at her, made a fierce face, and said with set teeth:

"Ugh! ugh! you bloodthirsty person!"

"Well, Ivan, you've nothing to do here, and we're expecting guests.
Go away!  Nilovna, give him the paper."

"Another paper?"

"There, take it and give it to the printer."

"I've taken it; I'll deliver it.  Is that all?"

"That's all.  There's a spy at the gate."

"I noticed.  At my door, too.  Good-by!  Good-by, you fierce woman!
And do you know, friends, a squabble in a cemetery is a fine thing
after all!  The whole city's talking about it.  It stirs the people
up and compels them to think.  Your article on that subject was
excellent, and it came in time.  I always said that a good fight
is better than a bad peace."

"All right.  Go away now!"

"You're polite!  Let's shake hands, Nilovna.  And that fellow--
he certainly behaved stupidly.  Do you know where he lives?"

Nikolay gave him the address.

"I must go to him to-morrow.  He's a fine fellow, eh?"

"Very!"

"We must keep him alive; he has good brains.  It's from just such
fellows that the real proletarian intellectuals ought to grow up--
men to take our places when we leave for the region where evidently
there are no class antagonisms.  But, after all, who knows?"

"You've taken to chattering, Ivan."

"I feel happy, that's why.  Well, I'm going!  So you're expecting
prison?  I hope you get a good rest there!"

"Thank you, I'm not tired!"

The mother listened to their conversation.  Their solicitude in
regard to the workingmen was pleasant to her; and, as always, the
calm activity of these people which did not forsake them even before
the gates of the prison, astonished her.

After the physician left, Nikolay and the mother conversed quietly
while awaiting their evening visitors.  Then Nikolay told her at
length of his comrades living in exile; of those who had already
escaped and continued their work under assumed names.  The bare
walls of the room echoed the low sounds of his voice, as if listening
in incredulous amazement to the stories of modest heroes who
disinterestedly devoted all their powers to the great cause of liberty.

A shadow kindly enveloped the woman, warming her heart with love
for the unseen people, who in her imagination united into one huge
person, full of inexhaustible, manly force.  This giant slowly but
incessantly strides over the earth, cleansing it, laying bare before
the eyes of the people the simple and clear truth of life--the great
truth that raises humanity from the dead, welcomes all equally, and
promises all alike freedom from greed, from wickedness, and
falsehood, the three monsters which enslaved and intimidated the
whole world.  The image evoked in the mother's soul a feeling
similar to that with which she used to stand before an ikon.  After
she had offered her joyful, grateful prayer, the day had then seemed
lighter than the other days of her life.  Now she forgot those days.
But the feeling left by them had broadened, had become brighter and
better, had grown more deeply into her soul.  It was more keenly
alive and burned more luminously.

"But the gendarmes aren't coming!" Nikolay exclaimed suddenly,
interrupting his story.

The mother looked at him, and after a pause answered in vexation:

"Oh, well, let them go to the dogs!"

"Of course!  But it's time for you to go to bed, Nilovna.  You must
be desperately tired.  You're wonderfully strong, I must say.  So much
commotion and disturbance, and you live through it all so lightly.
Only your hair is turning gray very quickly.  Now go and rest."

They pressed each other's hand and parted.

Next: CHAPTER XIII

Table of Contents: Mother (Part II)
