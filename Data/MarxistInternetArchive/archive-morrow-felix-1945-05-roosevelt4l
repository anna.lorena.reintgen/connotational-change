MIA  >  Archive  >  Morrow Source:
The Militant, Vol. IX No. 20, 19 May 1945, p. 4.
Transcription/Editing/HTML Markup: 2018 by Einde O’Callaghan.
Copyleft: Felix Morrow Internet Archive (www.marx.org) 2018. Permission is granted to copy and/or distribute this document under the terms of the Creative Commons Attribution-ShareAlike 2.0.In previous articles we have demonstrated, with facts and figures, that Roosevelt (1) had no solution for economic crises; (2) that monopolies grew during his administration; (3) that the farmer’s permanent crisis remains. Roosevelt admirers can hardly deny these facts, and so they evade these crucial questions.On one question, however, they challenge us. Isn’t it a fact that the workers made gains during Roosevelt’s administration? Didn’t his sponsorship of 7A and the Wagner Labor Act help the unions organize and result in average hourly wage increases?It would be easy enough to show that Section 7A of the NRA was the least that could be conceded to the trade unions in exchange for their acceptance of the NRA, which enabled the monopolies to grow as never before. And far from guaranteeing the right to organize, 7A was interpreted by Roosevelt himself to permit company unions. The auto workers, for instance, should still remember Roosevelt’s sponsorship of the notorious “merit clause” in the NRA Auto Code, enabling employers to hire and fire on an open shop basis; and the vicious Auto Labor Board, set up in March 1934 by Roosevelt, which tried to maintain company unionism.As for the Wagner Labor Act, adopted in 1935, it would be easy to show that it came AFTER the workers, losing their illusions about 7A, embarked on the greatest strike wave of the Twentieth Century. It was Roosevelt himself who, in June 1934, on the same day that the Wagner Labor Bill was to come up for final vote, set up instead the National Steel Labor Relations Board which recognized company unionism. Senator Robinson, Democratic floor leader, stating that he was acting on Roosevelt’s request, on that day prevented the Wagner Bill from coming to a vote. Senator Wagner himself stated that he accepted Roosevelt’s judgment that “further trial be permitted before company unions be rejected as bargaining agents.”
 Only after the great strikes which built the CIO did the Wagner Labor Act become a law, not to help organize the unorganized but to channelize the struggles of the already-organized workers into government boards.It was on the picket-line, as the history of the rise of the CIO shows, that the workers raised the average hourly wage. But even so, one must understand the real limitations of this gain. Until the war, the rise in wages of the employed workers did not mean a rise in the total wages. Total wages were 63 billions in 1929, and only 55 billions in 1938. The working class as a whole had less to live on during the New Deal years than before the depression began.The true story of such gains as were made by the workers during the Roosevelt administration is long and complex. We have told that story over and over in our press during the past twelve years. But there is a short and conclusive way to measure Roosevelt’s attitude toward the workers. What did his administration do for the unemployed ? If it were true that the workers owe their gains to Roosevelt’s humanitarianism, then this should show most clearly in the case of the most needy, the unemployed.The central fact to understand is that it is a myth that Roosevelt steadily supported federal responsibility for unemployment relief. He did so only during the short period of the existence of FERA, from the spring of 1933 to the end of 1935, during which the federal government put up three dollars for relief to every dollar spent by state and local governments. “While it isn’t written in the Constitution,” he declared in 1933, “nevertheless it is the inherent duty of the federal government to keep its citizens from starving.”
 But it was precisely this principle that he abandoned in 1935, when the great body of the unemployed were returned to state and local relief and the federal government limited its relief responsibilities to those on WPA jobs. This meant that about three-fourths of the unemployed were turned back to the states and communities, under Roosevelt’s infamous pronouncement to Congress early in 1935: “The federal government must and shall quit this business of relief.” During most of the New Deal, three-fourths of the unemployed were reduced to the coolie levels of local relief, which even in a rich state like New York averaged about $25 a month per family.In comparison to local relief, the $50 per month average of WPA wages seemed munificent. But how many got it? Only one out of every four or five families on relief. And many unemployed were either unable to get on the relief rolls or refused to undergo the humiliation of applying for relief until they were actually starving.Let us see the actual statistics of how many were on WPA and PWA, and how many were unemployed, during the peacetime New Deal years. The figures for WPA and PWA payrolls are the government’s own. Those for the number of unemployed are the very conservative figures of the AFL monthly estimates (which I have roughly computed in yearly totals). It should be remembered that the only actual count of the unemployed ever attempted, the Federal “unemployment census” taken in the summer of 1937, showed about two million MORE unemployed than the AFL estimate. Here are the figures:Year        WPA        PWA  No. Unemployed19351,092,000325,00010,500,00019363,061,000268,000  9,000,00019372,140,000159,00010,000,00019382,926,000111,00011,000,00019392,436,000233,00010,500,00019401,941,000  61,000  9,500,000These figures show:But, goes the myth, it wasn’t Roosevelt’s fault, but the fault of the Republican and ultra-reactionary Democratic Congressmen who cut down his requests for appropriations. This systematically-propagated lie is refuted by the complete figures of how much Roosevelt asked for WPA each year. These show he asked for little more than the so-called “Republocrates” gave, and far less than the labor movement asked for. Here one example must suffice. On April 27, 1939 Roosevelt wrote in his relief message to Congress:
 “For the fiscal year 1940, I recommend, therefore, that the specific sum of $1,477,000,000 be provided for the Works Progress Administration. This represents a reduction of one-third below the amount provided in the current fiscal year.”In other words, it was Roosevelt himself who, when unemployment stood at nearly eleven million, proposed to throw a million out of the 2,900,000 on WPA off the payrolls.In a word, the story of his admirers, that Roosevelt provided WPA jobs for the bulk of the unemployed, is a deliberate fraud.(This is the fourth of a series of articles on Roosevelt’s role.) Felix Morrow Internet Archive
Marxists’ Writers’ Index   |    Trotskyist Writers’ IndexLast updated on: 7 November 2018PWA proved to be not a drop in the bucket as a means of alleviating unemployment.
 WPA ranged from employing one out of ten of the unemployed to one out of three—the latter, however, only during 1936.
 In the election year of 1936, WPA payrolls were at their highest. Similarly for the years of Congressional elections, and also for 1940, payrolls temporarily rose just preceding elections.
 
        

        

  

1935

1,092,000

325,000

10,500,000

1936

3,061,000

268,000

  9,000,000

1937

2,140,000

159,000

10,000,000

1938

2,926,000

111,000

11,000,000

1939

2,436,000

233,000

10,500,000

1940

1,941,000

  61,000

  9,500,000
