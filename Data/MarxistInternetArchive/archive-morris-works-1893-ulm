
      The invention of printing books, and the use of wood-blocks for book ornament in
      place of hand-painting, though it belongs to the period of the degradation of
      medieval art, gave an opportunity to the Germans to regain the place which they had
      lost in the art of book decoration during the thirteenth and fourteenth centuries.
      This opportunity they took with vigour and success, and by means of it put forth
      works which showed the best and most essential qualities of their race. Unhappily,
      even at the time of their first woodcut book, the beginning of the end was on them;
      about thirty years afterwards they received the Renaissance with singular eagerness
      and rapidity, and became, from the artistic point of view, a nation of rhetorical
      pedants. An exception must be made, however, as to Albert Dürer; for, though
      his method was infected by the Renaissance, his matchless imagination and intellect
      made him thoroughly Gothic in spirit.
    
      Amongst the printing localities of Germany the two neighbouring cities of Ulm and
      Augsburg developed a school of woodcut book ornament second to none as to
      character, and, I think, more numerously represented than any other. I am obliged
      to link the two cities, because the early school at least is common to both; but
      the ornamental works produced by Ulm are but few compared with the prolific birth
      of Augsburg.
    
      It is a matter of course that the names of the artists who designed these
      wood-blocks should not have been recorded, any more than those of the numberless
      illuminators of the lovely written books of the thirteenth and fourteenth
      centuries; the names under which the Ulm and Augsburg picture-books are known are
      all those of their printers. Of these by far the most distinguished are the kinsmen
      (their degree of kinship is not known), Gunther Zainer of Augsburg and John Zainer
      of Ulm. Nearly parallel with these in date are Ludwig Hohenwang and John
      Bämler of Augsburg, together with Pflanzmann of Augsburg, the printer of the
      first illustrated German Bible. Anthony Sorg, a little later than these, was a
      printer somewhat inferior, rather a reprinter in fact, but by dint of
      reusing the old blocks, or getting them recut and in some cases redesigned, not
      always to their disadvantage, produced some very beautiful books. Schonsperger, who
      printed right into the sixteenth century, used blocks which were ruder than the
      earlier ones, through carelessness, and I suppose probably because of the aim at
      cheapness; his books tend towards the chap-book kind.
    
      The earliest of these picture-books with a date is Gunther Zainer's Golden
      Legend, the first part of which was printed in 1471; but, as the most
      important from the artistic point of view, I should name: first, Gunther Zainer's
      Speculum Humanae Salvationis (undated but probably of 1471); second, John
      Zainer's Boccaccio De Claris Mulieribus (dated in a cut, as well as in the
      colophon, 1473); third, the Aesop, printed by both the Zainers, but I do
      not know by which first, as it is undated; fourth, Gunther Zainer's Spiegel des
      menschlichen Lebens (undated but about 1475), with which must be taken his
      German Belial, the cuts of which are undoubtedly designed by the same
      artist, and cut by the same hand, that cut the best in the Spiegel above
      mentioned; fifth, a beautiful little book, the story of Sigismund and Guiscard, by
      Gunther Zainer, undated; sixth, Taberinus, die Geschicht von Symon, which
      is the story of a late German Hugh of Lincoln, printed by G. Zainer about 1475;
      seventh, John Bämler's Das Buch der Natur (1475), with many full-page
      cuts of much interest; eighth, by the sameprinter, Das Buch von den 7
      Todsünden und den 7 Tugenden (1474); ninth, Bämler's Sprenger's 
      Rosencranz-Bruderschaft, with only two cuts, but those most remarkable.
    
      To these may be added as transitional (in date at least), between the earlier and
      the later school next to be mentioned, two really characteristic books printed by
      Sorg: (a) Der Seusse, a book of mystical devotion, 1482, and
      (b) the Council of Constance, printed in 1483; the latter being,
      as far as its cuts are concerned, mainly heraldic.
    
      At Ulm, however, a later school arose after a transitional book, Leonard Hol's
      splendid Ptolemy of 1482; of this school one printer's name, Conrad
      Dinckmut, includes all the most remarkable books: to wit, Der
      Seelen-wurzgarten (1483), Das buch der Weisheit (1485), the 
      Swabian Chronicle (1486), Terence's Eunuchus (in German) (1486).
      Lastly, John Reger's Descriptio Obsidionis Rhodiae (1496) worthily closes
      the series of the Ulm books.
    
      It should here be said that, apart from their pictures, the Ulm and Augsburg books
      are noteworthy for their border and letter decoration. The Ulm printer, John
      Zainer, in especial shone in the production of borders. His De Claris
      Mulieribus excels all other books of the school in this matter; the initial S
      of both the Latin and the German editions being the most elaborate and beautiful
      piece of its kind; and, furthermore, the German edition has a border almost equal
      to the S in beauty, though different in character, having the shield of Scotland
      supported by angels in the corner. A very handsome border (or half-border rather),
      with a zany in the corner, is used frequently in J. Zainer's books,1
e.g. in the 1473 and 1474 editions of the Rationale of Durandus,
      and, associated with an interesting historiated initial O, in Alvarus, De
      planctu Ecclesiae, 1474. There are two or three other fine borders, such as
      those in Steinhowel's Büchlein der Ordnung, and Petrarch's 
      Griseldis (here shown), both of 1473, and in Albertus Magnus, Summa de
      eucharistiae sacramento, 1474.
    
      A curious alphabet of initials made up of leafage, good, but not very showy, is
      used in the De Claris Mulieribus and other books. An alphabet of large
      initials, the most complete example of which is to be found in Leonard Hol's 
      Ptolemy, is often used and is clearly founded on the pen-letters, drawn mostly
      in red and blue, in which the Dutch `rubrishers' excelled.2 This big
      alphabet is very beautiful and seems to have been a good deal copied by other
      German printers, as it well deserved to be.3 John Reger's 
      Caoursin has fine handsome `blooming-letters,' somewhat tending towards the
      French style.
    
      In Augsburg Gunter Zainer has some initial I's of strap-work with foliation: they
      are finely designed, but gain considerably when, as sometimes happens, the spaces
      between the straps are filled in with fine pen-tracery and in yellowish brown; they
      were cut early in Gunther's career, as one occurs in the Speculum Humanae
      Salvationis, c. 1471, and another in the Calendar, printed 1471.
      These, as they always occur in the margin and are long, may be called
      border-pieces. A border occurring in Eyb, ob einem manne izu nemen ein
      weib is drawn very gracefully in outline, and is attached, deftly enough, to a
      very good S of the pen-letter type, though on a separate block; it has three
      shields of arms in it, one of which is the bearing of Augsburg. This piece is
      decidedly illuminators' work as to design
    
      Gunther's Margarita Davidica has a border (attached to a very large P)
      which is much like the Ulm borders in character.
    
      A genealogical tree of the House of Hapsburg prefacing the Spiegel des
      Menschlichen lebens,and occupying a whole page, is comparable for beauty and
      elaboration to the S of John Zainer above mentioned; on the whole, for beauty and
      richness of invention and for neatness of execution, I am inclined to give it the
      first place amongst all the decorative pieces of the German printers.
    
      Gunther Zainer's German Bible of c.1474 has a full set of pictured letters, one to
      every book, of very remarkable merit: the foliated forms, which make the letters
      and enclose the figures being bold, inventive, and very well drawn. I note that
      these excellent designs have received much less attention than they deserve.
    
      In almost all but the earliest of Gunther's books a handsome set of initials are
      used, a good deal like the above-mentioned Ulm initials, but with the foliations
      blunter, and blended with less of geometrical forms: the pen origin of these is
      also very marked.
    
      Ludwig Hohenwang, who printed at Augsburg in the seventies, uses a noteworthy set
      of initials, alluded to above, that would seem to have been drawn by the designer
      with a twelfth-century MS. before him, though, as a matter of course, the fifteenth
      century betrays itself in certain details, chiefly in the sharp foliations at the
      ends of the scrolls, etc. There is a great deal of beautiful design in these
      letters; but the square border round them, while revealing their origin from
      illuminators' work, leaves over-large whites in the backgrounds, which call our for
      the completion that the illuminator's colour would have given them.
    
      Bämler and the later printer Sorg do not use so much ornament as Gunther
      Zainer; their initials are less rich both in line and design than Gunther's, and
      Sorg's especially have a look of having run down from the earlier ones: in his 
      Seusse, however, there are some beautiful figured initials designed on
      somewhat the same plan as those of Gunther Zainer's Bible.
    
      Now it may surprise some of our readers, though I should hope not the greatest part
      of them, to hear that I claim the title of works of art, both for these
      picture-ornamented books as books, and also for the pictures themselves. Their two
      main merits are first their decorative and next their story-telling quality; and it
      seems to me that these two qualities include what is necessary and essential in
      book-pictures. To be sure the principal aim of these unknown German artists was to
      give the essence of the story at any cost, and it may be thought that the
      decorative qualities of their designs were accidental or done unconsciously at any
      rate. I do not altogether dispute that view; but then the accident is that of the
      skilful workman whose skill is largely the result of tradition; it has thereby
      become a habit of the hand to him to work in a decorative manner.
    
      To turn back to the books numbered above as the most important of the school, I
      should call John Zainer's De Claris Mulieribus, and the Aesop,
      and Gunther Zainer's Spiegel des Menschlichen lebens the most
      characteristic. Of these my own choice would be the De Claris Mulieribus,
      partly perhaps because it is a very old friend of mine, and perhaps the first book
      that gave me a clear insight into the essential qualities of the medieval design of
      that period. The subject-matter of the book also makes it one of the most
      interesting, giving it opportunity for setting forth the medieval reverence for the
      classical period, without any of the loss of romance on the one hand, and epical
      sincerity and directness on the other, which the flood-tide of renaissance rhetoric
      presently inflicted on the world. No story-telling could be simpler and more
      straightforward, and less dependent on secondary help, than that of these curious,
      and, as people phrase it, rude cuts. And in spite (if you please it) of
      theirrudeness, they are by no means lacking in definite beauty: the composition is
      good everywhere, the drapery well designed, the lines rich, which shows of course
      that the cutting is good. Though there is no ornament save the beautiful initial S
      and the curious foliated initials above mentioned, the page is beautifully
      proportioned and stately, when, as in the copy before me, it has escaped the fury
      of the bookbinder.
    
      The great initial S I claim to be one of the very best printers' ornaments ever
      made, one which would not disgrace a thirteenth-century MS. Adam and Eve are
      standing on a finely-designed spray of poppy-like leafage, and behind them rise up
      boughs of the tree. Eve reaches down an apple to Adam with her right hand, and with
      her uplifted left takes another from the mouth of the crowned woman's head of the
      serpent, whose coils, after they have performed the duty of making the S, end in a
      foliage scroll, whose branches enclose little medallions of the seven deadly sins.
      All this is done with admirable invention and romantic meaning, and with very great
      beauty of design and a full sense of decorative necessities.
    
      As to faults in this delightful book, it must be said that it is somewhat marred by
      the press-work not being so good as it should have been even when printed by the
      weak presses of the fifteenth century; but this, though a defect, is not, I submit,
      an essential one.
    
      In the Aesop the drawing of the designs is in a way superior to that of
      the last book: the line leaves nothing to be desired; it is thoroughly decorative,
      rather heavy, but so firm and strong, and so obviously in submission to the
      draughtsman's hand, that it is capable of even great delicacy as well as richness.
      The figures both of man and beast are full of expression; the heads clean drawn and
      expressive also, and in many cases refined and delicate. The cuts, with few
      exceptions, are not bounded by a border, but amidst the great richness of line no
      lack of one is felt, and the designs fully sustain their decorative position as a
      part of the noble type of the Ulm and Augsburg printers; this Aesop is, to
      my mind, incomparably the best and most expressive of the many illustrated editions
      of the Fables printed in the fifteenth century. The designs of the other German and
      Flemish ones were all copied from it.
    
      Gunther Zainer's Spiegel des Menschlichen lebens is again one of the most
      amusing of wood-cut books. One may say that the book itself, one of the most
      popular of the Middle Ages, runs through all the conditions and occupations of men
      as then existing, from the Pope and Kaiser down to the field labourer, and, with
      full indulgence in the medieval love of formal antithesis, contrasts the good and
      the evil side of them. The profuse illustrations to all this abound in excellent
      pieces of naive characterisation; the designs are very well put together, and, for
      the most part, the figures well drawn, and draperies good and crisp, and the
      general effect very satisfactory as decoration. The designer in this book, however,
      has not been always so lucky in his cutter as those of the last two, and some of
      the pictures have been considerably injured in the cutting. On the other hand the
      lovely genealogical tree above mentioned crowns this book with abundant honour, and
      the best of the cuts are so good that it is hardly possible to rank it after the
      first two. Gunther Zainer's Speculum Humanae Salvationis and his 
      Golden Legend have cuts decidedly ruder than these three boooks; they are
      simpler also, and less decorative as ornaments to the page, nevertheless they have
      abundant interest, and most often their essential qualities of design shine through
      the rudeness, which by no means excludes even grace of silhouette: one and all they
      are thoroughly expressive of the story they tell. The designs in these two books,
      by the by, do not seem to have been done by the same hand; but I should think that
      the designer of those in the Golden Legend drew the subjects that
      `inhabit' the fine letters of Gunther's GermanBible. Both seem to me to have a kind
      of illuminator's character in them. The cuts to the story of Simon bring us back to
      those of the Spiegel des Menschlichen lebens; they are delicate and
      pretty, and tell the story, half so repulsive, half so touching, of `little Sir
      Hugh,' very well.
    
      I must not pass by without a further word of Sigismund and 
      Guiscard. I cannot help thinking that the cuts therein are by the same hand
      that drew some of those in the Aesop; at any rate they have the same
      qualities of design, and are to my mind singularly beautiful and interesting.
    
      Of the other contemporary, or nearly contemporary, printers, Bämler comes
      first in interest. His book von den 7 Todsünden, etc., has cuts of
      much interest and invention, not unlike in character to those of Gunther Zainer's
      Golden Legend. His Buch der Natur has full-page cuts of animals,
      herbs, and human figures exceedingly quaint, but very well designed for the most
      part. A half-figure of a bishop `in pontificalibus' is particularly bold and happy.
      Rupertus a sancto Remigio's History of the crusade and the Cronich von allen
      Konigen und Kaisern are finely illustrated. His Rosencranz
      Bruderschaft above mentioned has but two cuts, but they are both of them, the
      one as a fine decorative work, the other as a deeply felt illustration of
      devotional sentiment, of the highest merit.
    
      The two really noteworthy works of Sorg (who, as aforesaid, was somewhat a
      plagiaristic publisher) are, first, the Seusse, which is illustrated with
      bold and highly decorative cuts full of meaning and dignity, and next, the 
      Council of Constance, which is the first heraldic woodcut work (it has besides
      the coats of arms, several fine full-page cuts, of which the burning of Huss is
      one). These armorial cuts, which are full of interest as giving a vast number of
      curious and strange bearings, are no less so as showing what admirable decoration
      can be got out of heraldry when it is simply and well drawn.
    
      To Conrad Dinckmut of Ulm, belonging to a somewhat later period than these
      last-named printers, belongs the glory of opposing by his fine works the coming
      degradation of book-ornament in Germany. The Seelen-wurzgarten, ornamented
      with seventeen full-page cuts, is injured by the too free repetition of them; they
      are, however, very good; the best perhaps being the Nativity, which, for simplicity
      and beauty, is worthy of the earlier period of the Middle Ages. The Swabian
      Chronicle has cuts of various degrees of merit, but all interesting and full
      of life and spirit: a fight in the lists with axes being one of the most
      remarkable. Das buch der Weisheit (Bidpay's Fables) has larger cuts which
      certainly show no lack of courage; they are perhaps scarcely so decorative as the
      average of the cuts of the school, and are somewhat coarsely cut; but their frank
      epical character makes them worthy of all attention. But perhaps his most
      remarkable work is his Terence's Eunuchus (in German), ornamented with
      twenty-eight cuts illustrating the scenes. These all have backgrounds showing
      (mostly) the streets of a medieval town, which clearly imply theatrical scenery;
      the figures of the actors are delicately drawn, and the character of the persons
      and their action is well given and carefully sustained throughout. The text of this
      book is printed in a large handsome black-letter, imported, as my friend Mr Proctor
      informs me, from Italy. The book is altogether of singular beauty and character.
    
      The Caoursin (1496), the last book of any account printed at Ulm, has good
      and spirited cuts of the events described, the best of them being the flight of
      Turks in the mountains. One is almost tempted to think that these cuts are designed
      by the author of those of the MainzBreidenbach of 1486, though the cutting
      is much inferior.
    
      All these books, it must be remembered, though they necessarily (being printed
      books) belong to the later Middle Ages, and though some of them are rather
      decidedly late in that epoch, are thoroughly `Gothic' as to their ornament; there
      is no taint of the Renaissance in them. In this respect the art of book-ornament
      was lucky. The neo-classical rhetoric which invaded literature before the end of
      the fourteenth century (for even Chaucer did not quite escape it) was harmless
      against this branch of art at least for more than another hundred years; so that
      even Italian book-pictures are Gothic in spirit, for the most part, right up to the
      beginning of the sixteenth century, long after the New Birth had destroyed the
      building arts for Italy: while Germany, whose Gothic architecture was necessarily
      firmer rooted in the soil, did not so much as feel the first shiver of the coming
      flood till suddenly, and without warning, it was upon her, and the art of the
      Middle Ages fell dead in a space of about five years, and was succeeded by a
      singularly stupid and brutal phase of that rhetorical and academical art, which, in
      all matters of ornament, has held Europe captive ever since.
    
1 By the by, in Gritsch's Quadragesimale, 1475, this zany is
      changed into an ordinary citizen by means of an ingenious piecing of the block.
    
2 Another set of initials founded on twelfth-century work occurs in John
      Zainer's folio books, and has some likeness to those used by Hohenwang of Augsburg
      in the Golden Bibel and elsewhere, and perhaps was suggested by these, as
      they are not very early (c. 1475), but they differ from Hohenwang's in
      being generally more or less shaded, and also in not being enclosed in a square.
    
3 The initials of Knoblotzer of Strassburg and Bernard Richel of Basel
      may be mentioned.
    
      This article was illustrated by prints from the following books:
    
Bibliographica: Papers on Books, their History and Art, 1893.
    
The William Morris Internet Archive : Works

        Gunther Zainer's Ingold, das golden Spiel. Augsburg, 1472.
      
        John Zainer's Griseldis. Ulm, 1473.
      
        John Zainer's Boccaccio, de Claris Mulieribus.
      
        John Zainer's Aesop.
      
        Gunther Zainer's Epistles and Gospels. Augsburg, 1474.
      
        Gunther Zainer's Tuberinus, Geschicht von dem Heiligen Kind Symon.  Augsburg, c.1475.
      
        Conrad Dinckmut, Terence's Eunuchus. Ulm, 1486.
      