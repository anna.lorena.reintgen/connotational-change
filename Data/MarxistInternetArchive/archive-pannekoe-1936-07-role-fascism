
Published: International Council Correspondence, vol. 2, no. 8. July 1936.
Transcription/Markup: Micah Muer, 2017.
The chief characteristic of fascism is that of organizing the petty
capitalist and middle class with their narrow-minded spirit of private business
into a mass organization, strong enough to check and beat the proletarian
organizations. This class, squeezed in between the capitalist and the working
class, unable to fight capitalism, is always ready to turn against the workers'
class struggle. Tho it hates big capital and puts forth anti-capitalistic
slogans, it is a tool in the hands of capitalism, which pays and directs its
political action towards the subduing of the workers.Its ideas and theories are directed chiefly against the class struggle,
against the workers feeling and acting as a separate class. Against this, it
brings forward a strong nationalistic feeling, the idea of the unity of the
nation against foreign nations. In this nation workers have their place, not as
a separate class, but combined with the employers as industrial and agrarian
groups of production. Representatives of these groups form advisory boards for
the government. This is called the Corporative State, founded on direct
representation of the economic grouping of society, on capitalist labor. It is
opposed to the parliamentary system for which fascism has hardly any use and
which it denounces as a power of disruption, a mischievous preaching of internal
dissension.Parliamentarianism is the expression of supremacy of the people, the
citizens, and of the dependence of the government. Fascism puts the State above
the citizens. The State, as organization of the nation, is the superior
objective to which the citizens are subordinate. Not democracy, not the people's
right, but authority, the people's duties stand first. It places the party chief
at the head of the State, as a dictator, to rule with his party companions
without interference from parliamentary delegates.It is clear that this form of government corresponds to the needs of modern
capitalism. In a highly developed capitalism economic power is not rooted, as it
was in the beginning, in a numerous class of independent producers, but in a
small group of big capitalists. Their interests can be served better by
influencing a small body of absolute rulers, and their operations seem more
safely secured if all opposition of the workers and all public criticism is kept
down with an iron fist. Hence a tendency is visible in all countries to increase
the power of the central government and of the chieftains of the State. Tho this
is also sometimes called fascism, it makes some difference whether parliamentary
control is maintained, or an open dictatorial rule is established, founded upon
the terrorism of a mighty party organization.In Germany an analogous development of the national-socialist movement took
place somewhat later. The revolution of 1918 had brought socialism into power
but this power was made use of to protect capitalism. The socialists in the
government let the capitalists operate as they liked. The petty capitalist
classes seeing their antagonists on both sides now united and socialist
officials involved in foul capitalist affairs considered socialist state concern
and capitalist speculation as one common principle of corruption of an
international gang of grafters. It opposed to them the honest small business of
petty capitalists and the conservative old-time farmers. Young intellectuals of
the universities who found their former monopoly of public offices infringed
upon by detested socialist leaders, and former officers jobless thru the
diminuation [sic] of the army, organized the first
groups of national-socialists.They were eager nationalists because they belonged to the capitalist middle
classes and were opposed to the internationalism of the ruling social-democracy.
They called themselves socialist, because their petty-capitalistic feeling was
hostile to big business and big finance. They were strongly anti-Semitic, too.
Firstly, because Jewish capital played an important role in Germany especially
in the large stores, which stores caused the ruin of the small shopkeepers.
Secondly, because numerous Jewish intellectuals flooded the universities and the
learned professions, and by their keener wits often — e.g. as lawyers and
physicians — left their German competitors behind them.Financially these national-socialists were backed by many big capitalist
concerns, especially by the armament industry which felt its interests
endangered by the increasing disarmament conferences. They formed the illegal
fighting groups of capitalism against rising Bolshevism. Then came the world
crisis, aggravating the conditions in Germany exhausted as it was by the peace
treaty indemnities. The revolt of the desperate middle classes raised the
National-Socialist Party to the position of the mightiest party and enabled it
to seize the political power and to make its leader the dictator of Germany.Seemingly this dictatorship of middle class ideas is directed against big
capitalism as well as against the working class movement. It is clear, however,
that a petty capitalist program of a return to former times of small business
cannot be carried out. It soon became evident in Germany that big capitalism and
the land-owning aristocracy are still the real masters behind the ruling
National-Socialist Party. In reality this party acts as an instrument of
capitalism to fight and destroy the workers' organization.So strong was the power of the new slogans that they drew even a large number
of workers with them, who joined the National-Socialist Party. The workers had
learned to follow their leaders, but these leaders having disappointed them,
were beaten by the stronger leaders. The splendour and the spiritual power of
the socialist and communist ideals had waned. National-socialism promised the
workers a better socialism, by class-peace instead of by class-war. If offered
them their appropriate place in the nation as members of the united people not
as a separate class.Due to the victory of Fascism, or its equivalent, in certain countries, the
working classes in these countries have been thrown back in their systematic
upward strife for liberation. Their organizations have been wiped out, or in the
case of the trade unions, put directly under the command of capitalist state
officials. The workers' papers have been suppressed, free speech prohibited,
socialist and communist propaganda forbidden and punished with imprisonment,
concentration camps or long incarceration. In the enforced uniformity of opinion
there is no room for revolutionary teachings. The way of regular progress
towards proletarian power in the development of insight and organization by
means of propaganda and discussions, the way to revolution and freedom, is
blocked by the concrete wall of reaction.So it appears on the surface. But, looking deeper into the problem, it only
means that for the workers the smooth and peaceful way of growing to power is
blocked. We said before that the right of free speech, the right of organizing,
the right of propaganda and of forming political parties, were necessary for
capitalism. It means that they are necessary to ensure a regular working of
capitalist production and capitalist development. It means that, once they are
gone, the class antagonism must at last explode in heavy uprisings and violent
revolutionary movements. The capitalist class has to decide whether it prefers
this way.It has its reasons for taking this way. It strongly feels that the heavy
world crisis of today is shaking the capitalist system in the heart. It knows
that the diminished production is unable to feed the whole working class and at
the same time to leave sufficient profits. It is resolved not to bear the losses
itself. So it realizes that the workers, starved by unemployment, must rise and
will rise in revolts. And it tries to forestall them by fortifying its own
position, by forging the whole capitalist class into one strong unity, by
putting the state power in strong armor, by tying the workers to this state by
means of strong fetters, by robbing them of their old means of defense, their
socialist spokesmen and their organizations. This is the reason why in these
last years fascism became powerful.Capitalism at one time seemed to be on to the best way of fooling the workers
by means of sham-democracy and sham-reforms. Now it is turning the other way, to
heavy oppression. This must drive the workers to resistence [sic] and to determined class fighting. Why does capitalism
do so? Not of its own free will, but compelled by material, economic forces
inherent in its innermost nature; by the heavy crisis which endangers its
profits and arouses its fears for revolution.Triumphant fascism boasts that it has blocked the way to communism forever.
Its claim for this is because it has crushed the workers' movement. What it
really crushed were only the ineffective, primitive forms. It destroyed the
illusions, the old socialist beliefs, the socialist and communist parties
— all obsolete things hampering progress. It destroyed at the same time
the old party divisions which incited workers against workers. It thereby has
restored their natural class unity.Parties are groups of common opinion; organizations are dependent on
membership — both of these are secondary accidentals. Class is the primary
reality founded in the nature of capitalism itself. By tradition the workers
considered political opinion and organization membership as the real
distinctions between workers and capitalists. They were thinking and feeling in
terms of parties and unions — and by tradition may continue to do so for
some time. Now they are constrained to think and feel in terms of class. Without
any walls of partition, they stand one beside the other and they see that they
are all comrades, subject to the same capitalist exploitation. No party
discipline can call them to action; they will have to think out and make their
own action when the burden of Fascist capitalism makes itself too heavily felt.
The mist of opposing party opinions, of political slogans, of union narrowness,
which dimmed the natural class consciousness, has been destroyed. Sharp and
relentless the reality of capitalism confronts them, and to fight it they have
only themselves, their class unity to rely upon.The political parties of the working class — we speak of Germany and
Italy — have disappeared; only the leaders in exile continue to speak as
if they were the parties. This does not mean that they have disappeared forever.
If there should come an uprising of the working class, they will come back and
present themselves again as leaders. They must he vanquished for the second
time, now by the workers, by conscious recognition that they are obsolete.This does not mean that there will be no more parties in the future, that
their role is finished. New parties will arise undoubtedly in revolutionary
periods to express in new situations the unavoidable differences of tactical
opinions within the working class. Parties in this sense are necessary elements
in social development. The working class cannot be given ready-made opinions and
platforms from some Dictator Party which claims to do the thinking work for it,
and forbids independent opinion. The working class has to think out and to find
out the way for itself. Then opinions as to what is and what must be done will
differ because their lives — tho in the main rather alike — were
different in particulars. Groups of common opinion will be formed to discuss and
to propagate their ideas, to fight the scientists of the capitalist class, to
wage the spiritual contest with other groups. This is the way of self-education
for the working class.Parties in this sense may be called the scouting groups in the capitalist
jungle. They have to investigate the ways, to study science and circumstances,
to discuss these in mutual debate, to lay their ideas, their explanations, their
advice before their fellow workers. In this way they are the necessary
instruments to build up the intellectual power of the working class.Their task is not to act instead of the workers, to do the real fighting work
for the workers and to drag the class behind them. They will not have the power
to put themselves in the place of the class. Class unity, class action will be
paramount, party opinion subordinate.There are points of similarity between fascist Italy and Germany, and
bolshevist Russia. They are ruled by dictators, the chiefs of dictator parties
— the Communist Party in Russia, the Fascist Party in Italy, the
National-Socialist Party in Germany. These parties are large, strongly organized
groups which by their zeal and enthusiasm, their devotion to the cause, by their
discipline and energy are able to dominate state and country, and to enforce
upon it the stamp of one hard, big unity.This is a similarity in form; the contents are different. In Russia state
capitalism builds up the productive forces; private capital is not tolerated. In
Italy and Germany, the state and the ruling party are intimately connected with
private large-scale capitalism. But here also a better economic organization is
included in the fascist aims.Big business always means a certain organization of production, transport and
banking in the hands of a small number of directing individuals. And these
comparatively few persons have control and power over the mass of lesser
capitalists. Political rulers were already connected with these big capitalists
before. Now the fascist program proclaims it to be the task of state power to
direct and regulate the economic force. The increase of nationalism in all
countries, and the preparing for world war, as expressed in the slogan of
autarchy, i.e., the complete reliance of each state upon its own resources,
imposes upon the political leaders a close cooperation with the leaders of
industry. If in the old capitalism the state was a necessary instrument of
industry, new industry becomes a necessary instrument of the state, too. Ruling
the state and ruling industry is being merged into one. Imposing regulation upon
private business now means that by the fascist power the bulk of the lesser
capitalists are subjected still more completely to big business.To be sure, in fascist capitalism the ruling class clings to the principle of
private enterprise, if not for others, then at least for themselves. The silent
contest of big capitalists, monopolists, bankers, for supremacy and profit goes
on behind the scenes. If, however, the economic crisis lasts, then the
increasing misery, the rebellions of workers or middle classes will compel the
rulers to more efficient regulations of economic life. Already now, capitalist
economists look to Russia and study its economics as a possible model, and as a
way out. "Planned Economics" is the talk of politicians in many countries. A
development of European and American capitalism in the direction of and into
some form of state capitalism may offer itself as a means to prevent or to
thwart or to turn back a proletarian revolution. This will be called socialism
then. If we compare it to the last program, the "Plan" of the Belgian
Social-Democratic party for regulating capitalism, the difference is not
fundamental. The Belgian plan, indeed, may be called an attempt to compete with
fascism in a salvation-action for capitalism.If now we compare these three parties, the Social-Democratic Party, the
Communist Party, the Fascist Party, we find that they have their chief aim in
common. They want to dominate and rule the working class. Of course in order to
save the worker, to make them happy, to make them free. They all say so.Their means, their platforms are different; they are competitors, and each
abuses the others calling them counter-revolutionaries or criminals.Social-democracy makes an appeal to democracy; the workers shall choose their
masters by vote. The Communist Party resorts to revolution; the workers shall
rise at the call of the C.P., overthrow capitalist rule and put the C.P. into
office. The fascists make an appeal to national feelings and petty-capitalist
instincts. They all aspire to some form of state capitalism or state socialism
where the working class is commanded and exploited by the state, by the
community of leaders, directors, officials, the managers of production.Their common basis is the opinion that the working masses are unable to
conduct their own affairs. The incapable and stupid many, as they believe, must
be led and educated by the capable few.When the working class fights for its real freedom, in order to take the
direction of the production, the rule of society into its own hands, it will
find all these parties opposed to it. 
Left Communism Subject Archive |
Pannekoek Archive
