
Translation: Joseph Fracchia
Transcription: left-dis.nl
HTML-markup: Jonas Holmgren
The interpretation of the historical events that more than fifty 
years ago entered historical chronologies (and were quickly removed from them) 
as 'the Kronstadt Rebellion of 1921' is inseparably linked to the social 
position of each interpreter; or, in other words, each interpretation is stamped 
and conditioned by the author's position vis-à-vis the class struggles occurring 
in the society.[1]Those who interpret the Russian Revolution of 1917 as a socialist 
upheaval, who consider the Bolshevik rule established during the Civil War years 
a proletarian power, must necessarily treat that which took place in that island 
fortress in the Finnish gulf as a counterrevolutionary attempt to overthrow the 
new 'workers state'. Those, on the other hand, who regard precisely the action 
of those in Kronstadt as a revolutionary act will sooner or later arrive at 
diametrically opposed interpretations of the Russian developments and of the 
real situation in Russia.All this appears self-evident. But there is more to it. 
Bolshevism was not simply a form of economy or state whose existence at that 
time - not only in Kronstadt, but also in Petrograd, the Ukraine, and in large 
parts of Southern Russia - was hanging in the balance; Bolshevism was also a 
form of organization that matured in the Russian revolutionary struggles and 
that was tailored for the Russian situation. After the Bolshevik victory in the 
October Revolution this form of organization was, and is still being, forced 
onto the workers of all countries by representatives of the most varied 
political positions.The uprising of the population of Kronstadt against the 
Bolsheviks was not only a rejection of Bolshevik claims to power, but also a 
questioning of the traditional Bolshevik conception of Party and of the Party as 
such. That is why differences of opinion over organizational problems of the 
working class all too often include a discussion of Kronstadt, and why every 
discussion of Kronstadt inevitably discloses differences over the tactics and 
organizational issues of the proletarian class struggle. This means therefore 
that the Kronstadt Rebellion still remains, after more than a half-century, a 
burning issue. However colossal its historical importance, that is overshadowed 
by its practical importance for today's generations of workers.Leon Trotsky was one of those who did not understand this 
significance. In his 1938 essay, 'Hue and Cry over Kronstadt', he groaned: 'One 
would think that the Kronstadt Rebellion occurred not seventeen years ago, but 
yesterday'.[2] Trotsky wrote these words at the same time when he worked day in 
and day out to expose the Stalinist falsification of history and the Stalinist 
legends. That he, in his critique of Stalinism, never went beyond the boundary 
of Leninist revolutionary legends - that is a fact that we can here overlook. The Kronstadt Rebellion destroyed a social myth: the myth that in 
the Bolshevik state, power lay in the hands of the workers. Because this myth 
was inseparably linked to the entire Bolshevik ideology (and still is today), 
because in Kronstadt a modest beginning of a true workers' democracy was made, 
the Kronstadt Rebellion was a deadly danger for the Bolsheviks in their position 
of power. Not only the military strength of Kronstadt - that at the time of the 
rebellion was very much impaired by the frozen gulf - but also the demystifying 
effect of the rebellion threatened Bolshevik rule - a threat that was even 
stronger than any that could have been posed by the intervention armies of 
Denikin, Koltchak, Yudenitch, or Wrangel.For this reason the Bolshevik leaders were from their own 
perspective - or better, as a consequence of their social position (which 
naturally influenced their perspective) - forced to destroy the Kronstadt 
Rebellion without hesitation.[3] While the rebels were - as Trotsky had 
threatened - being 'shot like pheasants', the Bolshevik leadership characterized 
the Rebellion in their own press as a counterrevolution. Since that time this 
swindle has been zealously promoted and stubbornly maintained by Trotskyists and 
Stalinists.The circumstance that Kronstadt gained open sympathy from both 
Menshevik and white-guard circles reinforced the Trotskyist and Stalinist 
versions.[4] A sorrier justification of the official legend is hardly possible. 
Had Trotsky not himself disdainfully and correctly expressed his views in his 
History of the Russian Revolution about the political positions and social 
analyses of Professor Miliukov, the reactionary sympathiser with the Kronstadt 
Rebellion? Just because Miliukov and the entire white-guard press sympathised 
with Kronstadt - was the Kronstadt Rebellion for this reason 
counterrevolutionary? How then, according to this notion, should the New 
Economic Policy, implemented shortly after Kronstadt, be evaluated? The 
bourgeois Ustrialov openly gave his blessing to the new policy! But that did not 
at all cause the Bolsheviks to denounce the NEP as 'counterrevolutionary'. This 
fact is also symptomatic of the entire demagogic manner of fabricating legends. 
We will turn our attention away from this last issue. It is naturally of 
interest, not least because of the social function of legends which, however, 
can only be understood on the basis of the actual course of events, of the 
process of social development, and of the social character of the Russian 
upheaval. The Kronstadt Rebellion of 1921 was the dramatic high-point of a 
revolution whose social content must in shorthand be defined as bourgeois.The Rebellion was the proletarian spin-off of this 
bourgeois revolution, just as, in almost identical circumstances, the May 
events in Catalonia in 1937 represented the proletarian spin-off of the Spanish 
Revolution, or Babeuf's conspiracy of 1796 was the proletarian tendency in the 
great French Revolution.[5] The same causes are responsible for the fact that 
all three ended in defeat. In each case the conditions and prerequisites for a 
proletarian victory were lacking. Czarist Russia participated in the first world 
war as an underdeveloped country. Out of military and political need it had 
begun to industrialize and it took therewith the first step on the capitalist 
path; but the proletariat that emerged in this context was numerically too small 
in relation to the huge mass of Russian peasants.Certainly the political climate of czarist absolutism had 
resulted in a extraordinary increase in the militant spirit of the Russian 
workers. That enabled them to put a certain imprint on the developing 
revolution, but not enough decisively to influence its course. Despite the 
existence of the Putilov Works, the oil facilities in the Caucasus, the coal 
mines in the Donetz region, and the textile factories in Moscow, agriculture was 
the essential economic base of Russian society. Though a kind of emancipation of 
the peasantry occurred in 1861, the remnants of serfdom had by no means 
disappeared. The relations of production were feudal and the political 
superstructure corresponded: nobles and clergy were the ruling classes that - 
with the help of the army, the police, and the bureaucracy - exercised their 
power in the gigantic empire of large landholdings. Consequently, the Russian 
Revolution of the twentieth century confronted the economic task of abolishing 
feudalism and all of its components - serfdom, for example. It needed to 
industrialize agriculture and subject it to the conditions of modern commodity 
production; and it had to break all feudal chains on existing industry.Politically, this revolution had the task of destroying 
absolutism, abolishing the privileges accorded the feudal nobles, and developing 
a form of government and the state machinery that could politically guarantee 
the solution of the revolution's economic goals. It is clear that these economic 
and political tasks corresponded to those which in the West had to be fulfilled 
by the revolutions of the seventeenth, eighteenth, and nineteenth Centuries.[6] 
However, the Russian Revolution - similar to the later Chinese Revolution - had 
a peculiar characteristic. In Western Europe, above all in France, the 
bourgeoisie was the bearer of social progress, the preliminary proponent of the 
upheaval. In the East, and for the above-mentioned reason, the bourgeoisie was 
weak. And for this reason its interests were closely connected to those of 
czarism. That is, the bourgeois revolution in Russia had to be 
accomplished without, and moreover, against, the bourgeoisie. Lenin recognized exactly this peculiarity of the Russian 
Revolution. 'The Marxists', he wrote, 'are thoroughly convinced about the 
bourgeois character of the Russian Revolution. What does that mean? That means 
that those democratic transformations of the political order and those 
socio-economic transformations, that are necessary for Russia alone, do not 
amount to the burial of capitalism, nor the burial of the rule of the 
bourgeoisie; rather they for the first time prepare the ground for a broad and 
rapid development of capitalism ...'[7] In another passage he wrote: 'The victory 
of the bourgeois revolution in Russia is impossible[as] a bourgeois victory. 
That seems paradoxical. But so it is. The majority peasant population, the 
strength and consciousness of the proletariat that is already organized in the 
Socialist Party - all these circumstances lend a unique character to our 
bourgeois revolution. This uniqueness however does not eliminate the bourgeois 
character of the revolution'.[8]One comment however must be added here: the party of which Lenin 
speaks was neither socialist, nor could one claim that the proletariat was 
organized in it. It is of course true that it should be differentiated in 
several ways from the social-democratic parties of the West which played the 
role of the loyal opposition on the bourgeois parliamentary playing field, and 
which tried with all possible means to prevent the transformation of the 
capitalist into a socialist society. But Lenin's party did not differ from its 
Western counterparts in a socialist sense.Lenin's party in Russia strove for the revolutionary 
transformation of social relations; but as Lenin himself admitted, it was a 
matter of a revolution that in a different form had long since been accomplished 
in the West. This fact did not remain without consequences for Russian Social 
Democracy in general and the Bolshevik Party in particular.Lenin and the Bolsheviks were of the opinion that because of the 
class relations in Russia, their own party would inherit the role of Jacobins. 
Not without reason did Lenin define the socialdemocrat as 'a Jacobin in alliance 
with the masses'; not without reason did he create his party as a committee of 
professional revolutionaries; not without reason did he argue in What is to 
be Done? that their main task was the struggle against spontaneity. When 
Rosa Luxemburg criticized this conception at the beginning of the twentieth 
century, she was correct, but also incorrect. She was correct in that Lenin's 
conspiratorial organization had nothing to do with the natural organizational 
forms of the militant workers, that is, those that are predicated on capitalist 
relations and that grow out of class antagonism. What she overlooked, however, 
is that in Russia such a proletarian struggle was present in a very small 
measure, if at all. In Russia where the abolition of capitalist production 
relations and wage labour was not even on the horizon, it was a matter of a 
different struggle.For this struggle the Bolshevik party was perfectly suited. It 
completely fulfilled the needs of the imminent revolution. That the 
organizational form of this party - the so-called democratic centralism - would 
end with the dictatorship of the central committee over the mass of the members 
(as Rosa Luxemburg had predicted) proved to be completely correct; and precisely 
that was required for that 'bourgeois revolution with its unique 
character'. The Bolshevik Party derived its intellectual weapons from Marxism 
which at that time was the only radical theory that it could latch onto. 
Marxism, however, was the theoretical expression of a highly developed class 
struggle of a kind that was foreign to Russia; and it was a theory whose proper 
understanding was lacking in Russia. Thus it happened that the development of 
'Marxism' in Russia had only the name in common with Marxism, and was actually 
much closer to the Jacobin radicalism of, for example, Auguste Blanqui than to 
the ideas of Marx and Engels. Lenin, and Plekhanov too, shared with Blanqui a 
naturalistic conception of materialism [naturwissenschaftlicher 
Materialismus] that on the eve of the revolution in France was the main weapon 
in the struggle against the nobility and religion, and that was very distant 
from dialectical materialism.In Russia the situation was similar to that of pre-revolutionary 
France. Marxism, as Lenin understood it - and as he had to understand it - made 
it possible for him to gain deep insight into the essential problems of the 
Russian Revolution. That same Marxism provided the Bolshevik Party with a 
conceptual apparatus that stood in the most blatant contradiction to its own 
tasks and also to its practice. This meant, as Preobrazhenski publicly 
acknowledged during a regional conference in 1925, that Marxism in Russia had 
become a mere ideology.Naturally the revolutionary praxis of the Russian working class - 
to the extent that there was one - was not in harmony with the praxis of the 
Bolshevik Party that represented the interests of the bourgeois revolution in 
Russia as a whole. When the Russian workers rose up in 1917, they went, in 
accordance with their class nature, far beyond the limits of bourgeois upheaval. 
They attempted to determine their own lot and, with the help of the workers' 
councils, to realize their own self-determined forms of organization as 
producers.The Party that was 'always right' and that was supposed to show 
the working class the proper path - since as the leaders insisted, the 
proletariat could not find it on its own - limped behind. The Party was forced 
to recognize the fact of the workers' councils just as it was forced to 
acknowledge the existence of a massive peasantry. Neither the worker councils 
nor the large peasantry fitted in with its doctrine which reflected all previous 
experiences of revolution where conditions had been underdeveloped. In Russia 
the revolutionary praxis on the part of either the workers or the peasants could 
not be sustained for long. The material conditions for such sustained 
revolutionary praxis did not exist. What happened was the following: capitalism (hardly developed) 
was not toppled. Wage labour remained, which Marx, as it is well known, insisted 
is predicated on capital, as conversely capital is predicated on wage labour.The Russian workers did not obtain control over the means of 
production; that control fell rather to the Party (or the state). The Russian 
workers accordingly remained producers of surplus value. Neither the fact that 
the surplus value was not expropriated by a class of private capitalists, but by 
the state, or by the Party elements in control of the state, nor the fact that 
economic development in Russia - because of the absence of a bourgeois class - 
took another course than that of the West, changed anything for the position of 
the Russian worker as object of exploitation or wage slave. One cannot speak of 
the exercise of power by the working class. The czarist state was indeed broken, 
but the power of the workers' councils did not take its place. The councils that 
were spontaneously formed by Russian workers were stripped of their power as 
quickly as possible by the Bolshevik government, that is, already in the early 
summer of 1918, and they were condemned to complete insignificance. In place of 
the previous serfdom or quasi-feudal servitude, the economic basis of the 
country now assumed the form of economic slavery of the kind about which Trotsky 
wrote in 1917 that it was 'incompatible with the political sovereignty of the 
proletariat'. This thesis was correct; the Bolsheviks, however, - after they had 
wrongly proclaimed that their rule was that of the working class - helped 
themselves to political power, ostensibly in order to overcome the oppression of 
the Russian proletariat.But because of the lack of real worker power, Bolshevik political 
rule developed not into an instrument of emancipation, but into an instrument of 
suppression. In Bolshevik Russia, between the outbreak of the February 
Revolution and the forceful elimination of Kronstadt and the introduction of the 
new economic policy, the situation was similar to that of the February 
Revolution of 1848 in France. Marx commented on this revolution as follows: 'In 
France the petit bourgeois does what normally would have to be done by the 
industrial bourgeoisie, the worker does, what normally would be the duty of the 
petit bourgeois. And the task of the worker, who resolves that? This obligation 
is not discharged in France; it is merely proclaimed in France'. In Russia, this 
obligation continued to be proclaimed. However, with the Kronstadt uprising, the 
revolutionary process - of which October was only a staging ground - had come to 
an end. Kronstadt was the revolutionary moment where the pendulum swings of the 
revolution swung the furthest to the left.In the previous four fateful years a profound schism had been 
revealed between, on the one hand, the Bolshevik party and the Bolshevik 
government, and, on the other, the Russian working class. This became ever more 
apparent the more the opposition between this government and the peasants 
revealed itself. In addition there was the contradiction between workers and 
peasants, which was hushed up under the cover of the so-called Smytschka, 
that is, the class alliance between the two. From our perspective the 
contradiction between peasants and the Bolshevik government can be left aside. 
We only mention it in passing because the manifold contradictions between 
workers, Bolshevik government, and peasants, explains the necessity of party 
dictatorship. In the time-span, then, between the eruption of the revolution 
and the events of 1921, the Russian working class was engaged in a constant 
struggle. In the course of 1917, this struggle progressed much further than the 
Bolsheviks intended. In 1917, between March and the end of September, there had 
been 365 strikes, 38 factory occupations and 111 dismissals of company 
managers.[9] The Bolshevik motto 'control of production by the workers' was, in 
these conditions, condemned to fail. The workers expropriated the means of 
production on their own initiative, until, that is, the decree of workers' 
control that was issued on the 14th of November 1917, only one week after the 
Bolshevik seizure of power (!), put the brakes on these activities. After May 
1918, 'nationalizations' could only be undertaken by the central economic 
council. Shortly before, in April 1918, the individual responsibility of company 
managers had been reintroduced; they no longer had to justify their decisions to 
'their' workers.The factory councils had been liquidated in January 1918. Soon 
afterwards, once the so-called war-communism had been surmounted, the economic 
laws of a commodity producing society made themselves felt.Lenin lamented: 'The steering wheel slips out of the hands ... the 
wagon does not drive properly, and frequently not at all in the way that the one 
who sits at the wheel imagines'. A Russian union newspaper reported that there 
were 477 strikes in 1921 with a total of 184,000 participants. Some other 
numbers: 505 strikes with 154,000 participants in 1922; 267 strikes in 1924, 151 
of which were in state-run factories; 199 strikes in 1925, 99 of which were in 
state factories.[10] The numbers show a slow decline in workers' protests. The 
movement reached its high-point in 1921 with the Kronstadt Rebellion. On 24 
February 1921 the Petrograd workers went out on strike. They demanded: freedom 
for all workers; abolition of the special decrees; free elections for the 
councils. These were the same demands that were raised a few days later in 
Kronstadt. A general discontent gripped the country. At the turn of the year 
1920-21, Bolshevik Russia was the stage of a deep antagonism. This immediately 
gave rise to the 'worker opposition' that was led by two former metal workers. 
This opposition demanded the exclusion of the Bolshevik Party, abolition of the 
Party dictatorship, and its replacement by the self-government of the producing 
masses. In a word, the opposition demanded council democracy and communism!Shortly thereafter, the above-mentioned Kronstadt document 
characterized the general situation in Russia just as briefly as it did 
accurately: 'Through cunning propaganda the sons of working people were pulled 
into the party and subjected to a rigid discipline. When the communists felt 
that they were strong enough, they excluded step by step socialists of other 
stripes, and finally they shoved the workers and peasants themselves away from 
the rudder of the ship of state, yet they continued to rule the country in their 
names'.[11] Strong protests broke out in Petrograd in 1921. Proletarian 
demonstrators marched through the outlying areas of the city. The Red Army 
received the command to break up these demonstrations. The soldiers refused to 
shoot at the workers. The word was: general strike! On February 27, the general 
strike was a fact. On the 28th reliable troops devoted to the government were 
mobilized in Petrograd.The strike leaders were arrested; the workers were driven into 
factories. The resistance was broken. Nevertheless, on the same day the sailors 
of the battleship Petropavlovsk, riding at anchor near Kronstadt, 
demanded free elections for the workers' councils and freedom of press and 
association - for the workers. The crew of the battleship Sebastopol 
joined in those demands. On the next day 16,000 people gathered in the Kronstadt 
harbour to declare their solidarity with the Petrograd strikers. The significance of the Kronstadt Rebellion can hardly be 
overestimated. It is like a beacon light. The rebels wrote in their newspaper: 
'What are we fighting for? The working class had hoped to win its freedom in the 
October Revolution. But the result is a still greater oppression. The Bolshevik 
government has exchanged the famous symbol of the workers' state - the hammer 
and sickle - for the bayonet and prison bars in order to protect a comfortable 
life for the commissars and bureaucrats'. This all means that in Kronstadt the 
moment of truth had arrived for Bolshevik rule, just as in 1848 the June Days of 
the French proletariat was the moment of truth for the radical French republic. 
Here as there the burial site of the proletariat was made into the birthplace of 
capitalism. In France the proletariat had forced the bourgeois republic to show 
its true colours as the state whose acknowledged purpose was the perpetuation of 
the rule of capital. Likewise in Kronstadt the sailors and workers forced the 
Bolshevik Party to show its true colours as an institution that was openly 
hostile to workers and whose single purpose was the establishment of state 
capitalism. With the defeat of the rebellion, the path to that purpose had been 
cleared.In the streets of Paris General Cavaignac drowned proletarian 
hopes in blood. The Kronstadt Rebellion was beaten down by Leon Trotsky. In 
March 1921 Trotsky became the Cavaignac, the Gustav Noske of the Russian 
Revolution. As befitting the irony of history, Trotsky, the most famous and most 
respected representative of the theory of permanent revolution, prevented the 
most serious attempt since October 1917 to make the revolution permanent.This course, however, was unavoidable. The material prerequisites 
for proletarian victory in Kronstadt were lacking. The only thing that could 
have helped them was precisely that permanence of the revolution that we have 
mentioned. The Kronstadt workers themselves knew and understood this. For that 
reason they continually sent telegrams to their comrades on the Russian mainland 
asking for active support.The Kronstadt workers pinned their hopes on 'the third 
revolution', just as thousands of Russian proletarians hoped for that third 
revolution in Kronstadt. But that which was called 'the third revolution' was in 
the agrarian Russia of that time, with its relatively small working class and 
its primitive economy, nothing but an illusion. 'In Kronstadt', Lenin said at a 
time when the construction of the Kronstadt legend had hardly begun, 'they don't 
want the power of the white guards, they don't want our power. But there is no 
other power'.[12]Lenin was right to the degree that at that moment there was no 
other choice, at least not in Russia. But the Kronstadt workers, like the German 
workers, had shown the possibility of another form of power. With their 
commune and with their freely elected councils, the workers, not the 
Bolsheviks, provided the prototype of a proletarian revolution and workers' 
power.One should not be disturbed by the battle cry 'councils without 
communists'. 'Communists' is what those usurpers, those Bolshevik champions of 
state capitalism who suppressed the strike of the Petrograd workers, called 
themselves and what they still - and incorrectly - call themselves. The name 
'communist' was hated by the Kronstadt workers in 1921, by the East German 
workers in 1953 and the Hungarian workers in 1956. The Kronstadt workers, 
however, just like those others, took their class interests to heart. 
Accordingly, their proletarian methods of struggle are still today of utmost 
importance for all of the class comrades who - wherever they may be - carry on 
their own struggle and have learned from experience that their emancipation must 
be their own work. [1] Cajo Brendel's essay was originally given as a speech at the 
Technical University of Berlin in 1971, on the 50th anniversary of Kronstadt. 
See the original text: Cajo Brendel: "Kronstadt: Proletarischer Ausläufer der Russischen Revolution". 
In: Agnoli, Johannes; Brendel, Cajo; Mett, Ida: Die revolutionären Aktionen 
der russischen Arbeiter und Bauern: Die Kommune von Kronstadt, Berlin: Karin 
Kramer Verlag, Berlin 1974. 94 pages.[2] Trotsky's essay appeared in English with the title 'Hue and 
Cry over Kronstadt. A People's Front of Denouncers', in The New International, 
April 1938, p. 104. I retranslated the title from the Dutch Trotskyist press in 
which the essay was republished shortly after its initial publication in 
English.[3] Trotsky also speaks of this need in his biography of Stalin. 
There he says '[t]hat which the Soviet government did against its will in 
Kronstadt was a tragic necessity'. Nevertheless, already in the next sentence, 
and in keeping with the legend, he speaks again of 'a handful of reactionary 
peasants and rebellious soldiers'. (English edition: Stalin: An Appraisal of 
the Man and His Influence, edited and annotated from the Russian by Charles 
Malamuth, London, 1947, p. 337).[4] In certain Menshevik and white-guard circles, that is, not in 
all of them. It has been suggested that these were primarily those who found 
themselves outside of Russia at the time. In a contemporary document it is 
mentioned how the defeated remnants of the white guard who found themselves 
still in Russia recognized with such an unerring instinct the proletarian threat 
emerging in Kronstadt that they unconditionally volunteered their services to 
the Bolshevik leaders to help quell the rebellion. 'Die Wahrheit über 
Kronstadt', 1921. Complete reprinting of this work in German translation in 
Dokumente der Weltrevolution, vol. 2, Arbeiterdemokratie oder 
Parteidiktatur, Olten, 1967, p. 297ff.[5] These examples could be endlessly multiplied. One might 
compare this with the movement of the Levellers in the English Revolution of the 
seventeenth century.[6] Compare the social character of the Russian Revolution in 1917 
in 'Thesen über den Bolschewismus', first published in Rätekorrespondenz, 
no. 3, August 1934; Reprint in Kollektiv-Verlag, Berlin, n.d.[7] V. I. Lenin, 'Zwei Taktike der Sozialdemokratie in der 
demokratischen Revolution', in Ausgewählte Werke, vol. 1, Berlin, Dietz 
Verlag, 1964, p. 558.[8] This is an indirect citation of Lenin from the essay by N. 
Insarov, that was published in September 1926 in the journal Proletarii. 
Insarov used the Russian edition of Lenin's Complete Works that was published by 
the Russian State Publishing House. The passage is to be found there in vol. 11, 
Part I, p. 28.[9] These figures were taken from F. Pollock (Die 
planwirtschaftlichen Versuche in der Sowjetunion 1917-1927, Leipzig, 1929, 
p. 25) and from the work of Y. G. Kotelnikov and V. L. Melier, Die 
Bauernbewegung 1917 (which also contains facts concerning strikes and 
workers' political actions).[10] The statistics about the strikes and strikers are provided by 
the Russian union newspaper Voprosy Truda, 1924, no. 7/8. The editors 
note that the numbers are not at all complete. We cite once again Pollock, op. 
cit. In the (historical) first part of her book, Labour Disputes in Soviet 
Russia, 1957-1965 (Oxford, 1969, p. 15), Mary McAuley also provides 
information about the number of strikes in Russia in the first years after the 
revolution. She bases her information on Revzin in Vestnik Truda, 1924, 
no. 5-6, pp. 154-160. These numbers are in agreement with Pollock's.[11] 'Die Wahrheit über Kronstadt 1921', Dokumente der 
Weltrevolution, op. cit., vol. 2, p. 288.[12] Dokumente der Weltrevolution, op. cit., vol. 2, p. 
288. Last updated on: 6.14.2009
Brendel Archive |
MIA
