Anton Pannekoek 1946
Publication: Politics, Vol. III, No 8, September 1946, p. 270-272;
Transcribed: by David Walters/Greg Adargo, December, 2001;
Source: Kurasje Council Communist Archives.In former issues of Politics the problem has been posed: Why did the
working class fail in its historical task? Why did it not offer resistance
to national socialism in Germany? Why is there no trace of any revolutionary
movement amongst the workers of America? What has happened to the social
vitality of the world working class? Why do the masses all over the globe
no longer seem capable of initiating anything new aimed at their own self-liberation?
Some light may be thrown upon this problem by the following considerations.It is easy to ask: why did not the workers rise against threatening fascism?
To fight you must have a positive aim. Opposed to fascism there were two
alternatives: either to maintain, or to return to the old capitalism, with
its unemployment, its crises, its corruption, its misery--whereas Nationalism
Socialism preserved itself as an anti-capitalist reign of labor, without
unemployment, a reign of national greatness, of community politics that could
lead to a socialist revolution. Thus, indeed, the deeper question is: why
did the German workers not make their revolution?Well, they had experienced a revolution: 1918. But it had taught them
the lesson that neither the Social Democratic Party, nor the trade unions
was the instrument of their liberation; both turned out to be instruments
for restoring capitalism. So what were they to do? The Communist Party did
not show a way either; it propagated the Russian system of state-capitalism,
with its still worse lack of freedom.Could it have been otherwise? The avowed aim of the Socialist Party in
Germany--and then in all countries--was state socialism. According to program
the working class had to conquer political dominance, and then by its power
over the state, had to organize production into a state-directed planned
economic system. Its instrument was to be the Socialist Party, developed
already into a huge body of 300,000 members, with a million trade-union members
and three million voters behind them, led by a big apparatus of politicians,
agitators, editors, eager to take the place of the former rulers. According
to program, then, they should expropriate by law the capitalist class and
organize production in a centrally-directed planned system.It is clear that in such a system the workers, though their daily bread
may seem to be secured, are only imperfectly liberated. The upper echelons
of society have been changed, but the foundations bearing the entire building
remain the old ones: factories with wage-earning workers under the command
of directors and managers. So we find it described by the English socialist
G.D.H. Cole, who after World War 1 strongly influenced the trade unions
by his studies of guild socialism and other reforms of the industrial system.
He says:"The whole people would no more be able than the whole body of shareholders
in a great enterprise to manage an industry....It would be necessary, under
socialism as much as under large scale capitalism, to entrust the actual
management of industrial enterprise to salaried experts, chosen for their
specialized knowledge and ability in particular branches of work....There
is no reason to suppose that the methods of appointing the actual managers
in socialized industries would differ widely from those already in force
in large scale capitalist enterprise....There is no reason to suppose that
the socialization of any industry would mean a great change in its managerial
personnel."Thus the workers will have got new masters instead of the old ones. Good
humane masters instead of the bad, rapacious masters of today. Appointed
by a socialist government or at best chosen by themselves. But, once chosen,
they must be obeyed. The workers are not master over their shops, they are
not master of the means of production. Above them stands the commanding
power of a state bureaucracy of leaders and managers. Such a state of affairs
can attract the workers as long as they feel powerless against the power
of the capitalists: so in their first rise during the 19th century this
was put up as the goal. They were not strong enough to drive the capitalists
out of the command over the production installations; so their way out was
state socialism, a government of socialists expropriating the capitalists.Now that the workers begin to realize that state socialism means new fetters,
they stand before the difficult task of finding and opening new roads. This
is not possible without a deep revolution of ideas, accompanied by much
internal strife. No wonder that the vigor of the fight slackens, that they
hesitate, divided and uncertain, and seem to have lost their energy.Capitalism, indeed, cannot be annihilated by a change in the commanding
persons; but only by the abolition of commanding. The real freedom of the
workers consists in their direct mastery over the means of production. The
essence of the future free world community is not that the working masses
get enough food, but they direct their work themselves, collectively. For
the real content of their life is their productive work; the fundamental
change is not a change in the passive realm of consumption, but in the active
realm of production. Before them now the problem arises of how to unite freedom
and organization; how to combine mastery of the workers over the work with
the binding up of all this work into a well-planned social entirety. How
to organize production, in every shop as well as over the whole of world
economy, in such a way that they themselves as parts of a collaborating community
regulate their work. Mastery over production means that the personnel, the
bodies of workers, technicians and experts that by their collective effort
run the shop and put into action the technical apparatus are at the same
time the managers themselves. The organization into a social entity is then
performed by delegates of the separate plants, by so-called workers councils,
discussing and deciding on the common affairs. The development of such a
council organization will afford the solution of the problem; but this development
is a historical process, taking time and demanding a deep transformation
of outlook and character.This new vision of a free communism is only beginning to take hold of
the minds of the workers. And so now we begin to understand why former promising
workers' movements could not succeed. When the aims are too narrow there
can be no real liberation. When the aim is a semi- or mock-liberation, the
inner forces aroused are insufficient to bring about fundamental results.
So the German socialist movement, unable to provide the workers with arms
powerful enough to fight successfully monopolistic capital, had to succumb.
The working class had to search for new roads. But the difficulty of disentangling
itself from the net of socialist teachings imposed by old parties and old
slogans made it powerless against aggressive capitalism, and brought about
a period of continuous decline, indicating the need for a new orientation.Thus what is called the failure of the working class is the failure of
its narrow socialist aims. The real fight for liberation has yet to begin;
what is known as the workers' movement in the century behind us, seen in
this way, was only a series of skirmishes of advance guards. Intellectuals,
who are wont to reduce the social struggle to the most abstract and simple
formulas, are inclined to underrate the tremendous scope of the social transformation
before us. They think how easy it would be to put the right name into the
ballot box. They forget what deep inner revolution must take place in the
working masses; what amount of clear insight, of solidarity, of perseverance
and courage, of proud fighting spirit is needed to vanquish the immense
physical and spiritual power of capitalism.The workers of the world nowadays have two mighty foes, two hostile and
suppressing powers over against them: the monopolistic capitalism of America
and England, and Russian state capitalism. The former is drifting toward
social dictatorship camouflaged in democratic forms; the latter proclaims
dictatorship openly, formerly with the addition "of the proletariat," although
nobody believes that any more. They both try to keep the workers in a state
of obedient well-drilled followers, acting only at the command of the party
leaders, the former by the aid of the socialist program of socialist parties,
the latter by the slogans and wily tricks of the Communist party. The tradition
of glorious struggle helps keep them spiritually dependent on obsolete ideas.
In the competition for world domination, each tries to keep the workers
in its fold, by shouting against capitalism here, against dictatorship there.In the awakening resistance to both, the workers are beginning to perceive
that they can fight successfully only by adhering to and proclaiming the
exactly opposite principle--the principle of devoted collaboration of free
and equal personalities. Theirs is the task of finding out the way in which
the principle can be carried out in their practical action.The paramount question here is whether there are indications of an existing
or awakening fighting spirit in the working class. So we must leave the
field of political party strife, now chiefly intended to fool the masses,
and turn to the field of economic interests, where the workers intuitively
fight their bitter struggle for living conditions. Here we see that with
the development of small business into big business, the trade unions cease
to be instruments of the workers' struggle. In modern times these organizations
ever more turn into the organs by which monopoly capital dictates its terms
to the working class.When the workers begin to realize that the trade unions cannot direct
their fight against capital they face the task of finding and practicing
new forms of struggle. These new forms are the wildcat strikes. Here they
shake off direction by the old leaders and the old organizations; here they
take the initiative in their own hands; here they have to think out time
and ways, to take the decisions, to do all the work of propaganda, of extension,
of directing their actions themselves. Wildcat strikes are spontaneous outbursts,
the genuine practical expression of class struggle against capitalism, though
without wider aims as yet; but they embody a new character already in the
rebellious masses: self-determination instead of determination by leaders,
self-reliance instead of obedience, fighting spirit instead of accepting
the dictates from above, unbreakable solidarity and unity with the comrades
instead of duty imposed by membership. The unit in action and strike is,
of course, the same as the unit of daily productive work, the personnel of
the shop, the plant, the docks; it is the common work, the common interest
against the common capitalist master that compels them to act as one. In
these discussions and decisions all the individual capabilities, all the
forces of character and mind of all the workers, exalted and strained to
the utmost, are co-operating towards the common goal.In the wildcat strikes we may see the beginnings of a new practical orientation
of the working class, a new tactic, the method of direct action. They represent
the only actual rebellion of man against the deadening suppressing weight
of world-dominating capital. Surely, on small scale such strikes mostly
have to be broken off without success--warning signs only. Their efficiency
depends on their extension over larger masses; only fear for such indefinite
extension can compel capital to make concessions. If the pressure by capitalist
exploitation grows heavier--and we may be sure it will--resistance will
be aroused ever anew and will involve ever larger masses. When the strikes
take on such dimensions as to disturb seriously the social order, when they
assail capitalism in its inner essence, the mastery of the shops, the workers
will have to confront state power with all its resources. Then their strikes
must assume a political character; they have to broaden their social outlook;
their strike committees, embodying their class community, assume wider social
functions, taking the character of workers' councils. Then the social revolution,
the breakdown of capitalism, comes into view.Is there any reason to expect such a revolutionary development in coming
times, through conditions that were lacking until now? It seems that we
can, with some probability, indicate such conditions. In Marx's writings
we find the sentence: a production system does not perish before all its
innate possibilities have developed. In the persistence of capitalism, we
now begin to detect some deeper truth in this sentence than was suspected
before. As long as the capitalist system can keep the masses alive, they
feel no stringent necessity to do away with it. And it is able to do so as
long as it can grow and expand its realm over wider parts of the world. Hence,
so long as half the world's population stands outside capitalism, its task
is not finished. The many hundreds of millions thronged in the fertile plains
of Eastern and Southern Asia are still living in pre-capitalist conditions.
As long as they can afford a market to be provided with rails and locomotives,
with trucks, machines and factories, capitalist enterprise, especially in
America, may prosper and expand. And henceforth it is on the working class
of America that world-revolution depends.This means that the necessity of revolutionary struggle will impose itself
once capitalism engulfs the bulk of mankind, once a further significant
expansion is hampered. The threat of wholesale destruction in this last
phase of capitalism makes this fight a necessity for all the producing classes
of society, the farmers and intellectuals as well as the workers. What is
condensed here in these short sentences is an extremely complicated historical
process filing a period of revolution, prepared and accompanied by spiritual
fights and fundamental changes in basic ideas. These developments should
be carefully studied by all those to whom communism without dictatorship,
social organization on the basis of community-minded freedom, represents
the future of mankind.