MIA: Writers Archive: Mary BeardCharles Beard, Mary Beard, 1921
Source: Project Gutenberg;
First Published: Macmillan, March 1921;
Public Domain: this text is free of copyright;
Transcribed: by Distributed Proofreaders;
HTML Mark-up: by Andy Blunden.
Preface
PART I. THE COLONIAL PERIOD
The Great Migration to America
The Agencies of American Colonization
The Colonial Peoples
The Process of Colonization

II.
Colonial Agriculture, Industry, and Commerce
The Land and the Westward Movement
Industrial and Commercial Development

III.
Social and Political Progress
The Leadership of the Churches
Schools and Colleges
The Colonial Press
The Evolution in Political Institutions

IV.
The Development of Colonial Nationalism
Relations with the Indians and the French
The Effects of Warfare on the Colonies
Colonial Relations with the British Government
Summary of Colonial Period

PART II. CONFLICT AND INDEPENDENCE
The New Course in British Imperial Policy
George III and His System
George III's Ministers and Their Colonial Policies
Colonial Resistance Forces Repeal
Resumption of British Revenue and Commercial Policies
Renewed Resistance in America
Retaliation by the British Government
From Reform to Revolution in America

VI.
The American Revolution
Resistance and Retaliation
American Independence
The Establishment of Government and the New Allegiance
Military Affairs
The Finances of the Revolution
The Diplomacy of the Revolution
Summary of the Revolutionary Period

PART III. FOUNDATIONS OF THE UNION AND NATIONAL POLITICS
The Formation of the Constitution
The Promise and the Difficulties of America
The Calling of a Constitutional Convention
The Framing of the Constitution
The Struggle over Ratification

VIII.
The Clash of Political Parties
The Men and Measures of the New Government
The Rise of Political Parties
Foreign Influences and Domestic Politics

IX.
The Jeffersonian Republicans in Power
Republican Principles and Policies
The Republicans and the Great West
The Republican War for Commercial Independence
The Republicans Nationalized
The National Decisions of Chief Justice Marshall
Summary of Union and National Politics

PART IV. THE WEST AND JACKSONIAN DEMOCRACY
The Farmers beyond the Appalachians
Preparation for Western Settlement
The Western Migration and New States
The Spirit of the Frontier
The West and the East Meet

XI.
Jacksonian Democracy
The Democratic Movement in the East
The New Democracy Enters the Arena
The New Democracy at Washington
The Rise of the Whigs
The Interaction of American and European Opinion

XII.
The Middle Border and the Great West
The Advance of the Middle Border
On to the Pacific – Texas and the Mexican War
The Pacific Coast and Utah
Summary of Western Development and National Politics

PART V. SECTIONAL CONFLICT AND RECONSTRUCTION
The Rise of the Industrial System
The Industrial Revolution
The Industrial Revolution and National Politics

XIV.
The Planting System and National Politics
Slavery – North and South
Slavery in National Politic
The Drift of Events toward the Irrepressible Conflict

XV.
The Civil War and Reconstruction
The Southern Confederacy
The War Measures of the Federal Government
The Results of the Civil War
Reconstruction in the South
Summary of the Sectional Conflict

PART VI. NATIONAL GROWTH AND WORLD POLITICS
The Political and Economic Evolution of the South
The South at the Close of the War
The Restoration of White Supremacy
The Economic Advance of the South

XVII.
Business Enterprise and the Republican Party
Railways and Industry
The Supremacy of the Republican Party (1861-1885)
The Growth of Opposition to Republican Rule

XVIII.
The Development of the Great West
The Railways as Trail Blazers
The Evolution of Grazing and Agriculture
Mining and Manufacturing in the West
The Admission of New States
The Influence of the Far West on National Life

XIX.
Domestic Issues before the Country (1865-1897)
The Currency Question
The Protective Tariff and Taxation
The Railways and Trusts
The Minor Parties and Unrest
The Sound Money Battle of 1896
Republican Measures and Results

XX.
America a World Power (1865-1900)
American Foreign Relations (1865-1898)
Cuba and the Spanish War
American Policies in the Philippines and the Orient
Summary of National Growth and World Politics

PART VII. PROGRESSIVE DEMOCRACY AND THE WORLD WAR
The Evolution of Republican Policies(1901-1913)
Foreign Affairs
Colonial Administration
The Roosevelt Domestic Policies
Legislative and Executive Activities
The Administration of President Taft
Progressive Insurgency and the Election of 1912

XXII.
The Spirit of Reform in America
An Age of Criticism
Political Reforms
Measures of Economic Reform

XXIII.
The New Political Democracy
The Rise of the Woman Movement
The National Struggle for Woman Suffrage

XXIV.
Industrial Democracy
Cooperation between Employers and Employees
The Rise and Growth of Organized Labor
The Wider Relations of Organized Labor
Immigration and Americanization

XXV.
President Wilson and the World War
Domestic Legislation
Colonial and Foreign Policies
The United States and the European War
The United States at War
The Settlement at Paris
Summary of Democracy and the World War
 
Mary Beard Archive |
Women and Marxism |
USA History

To volunteer for the MIA, Email our Admin Committee



Charles Beard, Mary Beard, 1921
History of the United States


Source: Project Gutenberg;
First Published: Macmillan, March 1921;
Public Domain: this text is free of copyright;
Transcribed: by Distributed Proofreaders;
HTML Mark-up: by Andy Blunden.

CONTENTS

Preface

PART I. THE COLONIAL PERIOD

The Great Migration to America
The Agencies of American Colonization
The Colonial Peoples
The Process of Colonization


II.

Colonial Agriculture, Industry, and Commerce
The Land and the Westward Movement
Industrial and Commercial Development


III.

Social and Political Progress
The Leadership of the Churches
Schools and Colleges
The Colonial Press
The Evolution in Political Institutions


IV.

The Development of Colonial Nationalism
Relations with the Indians and the French
The Effects of Warfare on the Colonies
Colonial Relations with the British Government
Summary of Colonial Period


PART II. CONFLICT AND INDEPENDENCE

The New Course in British Imperial Policy
George III and His System
George III's Ministers and Their Colonial Policies
Colonial Resistance Forces Repeal
Resumption of British Revenue and Commercial Policies
Renewed Resistance in America
Retaliation by the British Government
From Reform to Revolution in America


VI.

The American Revolution
Resistance and Retaliation
American Independence
The Establishment of Government and the New Allegiance
Military Affairs
The Finances of the Revolution
The Diplomacy of the Revolution
Summary of the Revolutionary Period


PART III. FOUNDATIONS OF THE UNION AND NATIONAL POLITICS

The Formation of the Constitution
The Promise and the Difficulties of America
The Calling of a Constitutional Convention
The Framing of the Constitution
The Struggle over Ratification


VIII.

The Clash of Political Parties
The Men and Measures of the New Government
The Rise of Political Parties
Foreign Influences and Domestic Politics


IX.

The Jeffersonian Republicans in Power
Republican Principles and Policies
The Republicans and the Great West
The Republican War for Commercial Independence
The Republicans Nationalized
The National Decisions of Chief Justice Marshall
Summary of Union and National Politics


PART IV. THE WEST AND JACKSONIAN DEMOCRACY

The Farmers beyond the Appalachians
Preparation for Western Settlement
The Western Migration and New States
The Spirit of the Frontier
The West and the East Meet


XI.

Jacksonian Democracy
The Democratic Movement in the East
The New Democracy Enters the Arena
The New Democracy at Washington
The Rise of the Whigs
The Interaction of American and European Opinion


XII.

The Middle Border and the Great West
The Advance of the Middle Border
On to the Pacific – Texas and the Mexican War
The Pacific Coast and Utah
Summary of Western Development and National Politics


PART V. SECTIONAL CONFLICT AND RECONSTRUCTION

The Rise of the Industrial System
The Industrial Revolution
The Industrial Revolution and National Politics


XIV.

The Planting System and National Politics
Slavery – North and South
Slavery in National Politic
The Drift of Events toward the Irrepressible Conflict


XV.

The Civil War and Reconstruction
The Southern Confederacy
The War Measures of the Federal Government
The Results of the Civil War
Reconstruction in the South
Summary of the Sectional Conflict


PART VI. NATIONAL GROWTH AND WORLD POLITICS

The Political and Economic Evolution of the South
The South at the Close of the War
The Restoration of White Supremacy
The Economic Advance of the South


XVII.

Business Enterprise and the Republican Party
Railways and Industry
The Supremacy of the Republican Party (1861-1885)
The Growth of Opposition to Republican Rule


XVIII.

The Development of the Great West
The Railways as Trail Blazers
The Evolution of Grazing and Agriculture
Mining and Manufacturing in the West
The Admission of New States
The Influence of the Far West on National Life


XIX.

Domestic Issues before the Country (1865-1897)
The Currency Question
The Protective Tariff and Taxation
The Railways and Trusts
The Minor Parties and Unrest
The Sound Money Battle of 1896
Republican Measures and Results


XX.

America a World Power (1865-1900)
American Foreign Relations (1865-1898)
Cuba and the Spanish War
American Policies in the Philippines and the Orient
Summary of National Growth and World Politics


PART VII. PROGRESSIVE DEMOCRACY AND THE WORLD WAR

The Evolution of Republican Policies(1901-1913)
Foreign Affairs
Colonial Administration
The Roosevelt Domestic Policies
Legislative and Executive Activities
The Administration of President Taft
Progressive Insurgency and the Election of 1912


XXII.

The Spirit of Reform in America
An Age of Criticism
Political Reforms
Measures of Economic Reform


XXIII.

The New Political Democracy
The Rise of the Woman Movement
The National Struggle for Woman Suffrage


XXIV.

Industrial Democracy
Cooperation between Employers and Employees
The Rise and Growth of Organized Labor
The Wider Relations of Organized Labor
Immigration and Americanization


XXV.

President Wilson and the World War
Domestic Legislation
Colonial and Foreign Policies
The United States and the European War
The United States at War
The Settlement at Paris
Summary of Democracy and the World War

 


Mary Beard Archive |
Women and Marxism |
USA History


To volunteer for the MIA, Email our Admin Committee

