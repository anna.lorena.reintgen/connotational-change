MIA  >  Archive  >  Cliff After resolving the crisis in the Bolshevik leadership over coalition with the Mensheviks and the Right Socialist Revolutionaries, the regime had to face a new problem. The Bolsheviks had to decide whether elections to a Constituent Assembly should be allowed. If the result of such elections was to create a body whose composition was radically different from that of the Soviet, what could be done about it?The demand for the convocation of a Constituent Assembly had been one of the main plans of the programme of the Russian Social Democratic Labour Party since its foundation. Since 1905 Lenin had repeatedly referred to this demand as ‘one of the three pillars of Bolshevism’. (The other two were the nationalization of land and the 8-hour day.) This slogan was put forward even more immediately and urgently between the February and October revolutions. The Bolsheviks pressed constantly for a Constituent Assembly to be called and the delay in doing so was one of the many charges they laid at the door of the provisional government. Again and again between April and October Lenin reiterated that the Bolsheviks, and only the Bolsheviks, would ensure its convocation without delay. They were fighting at the time simultaneously for power for the Soviets and the convening of the Constituent Assembly. They asserted that unless the Soviets took power the Constituent Assembly would not be convened.In early April 1917 Lenin set out the Bolshevik attitude to the question of whether the Constituent Assembly should be convened. ‘Yes,’ he said, ‘as soon as possible. But there is only one way to ensure its convocation and success, and that is by increasing the number and strength of the Soviets and organizing and arming the working-class masses. That is the only guarantee.’ [1]On 12–14 (25–27) September he wrote: ‘Our party alone, on taking power, can secure the Constituent Assembly’s convocation; it will then accuse the other parties of procrastination and will be able to substantiate its accusations.’ [2]On 24 September (7 October) the Bolshevik daily Rabochii Put accused the Cadets of ‘secret postponement and sabotage of the Constituent Assembly’. [3]For many months the Bolsheviks had posed the question not of Soviets or Constituent Assembly, but of Soviets and Constituent Assembly. In a fiery speech at the Kerensky-convened State Council on 7 (20) October Trotsky, leading the Bolshevik fraction out of the meeting, said in conclusion: ‘Long live an immediate, honest, democratic peace. All power to the Soviets. All land to the people. Long live the Constituent Assembly.’ [4]On 29 November (12 December) Bukharin, using precedents from English and French history, proposed that once the Constituent Assembly was convoked the Cadets should be expelled from it, and that the Assembly should declare itself a revolutionary convention. Bukharin hoped that in the Assembly the Bolsheviks and Left Socialist Revolutionaries would command an overwhelming majority, which would give the truncated Assembly legitimacy. Trotsky supported Bukharin’s plan of action. Stalin argued that Bukharin’s tactic would not work. No-one suggested the dispersal of the Constituent Assembly.The fact is that the Bolsheviks, who campaigned strongly for the convocation of the Constituent Assembly, were completely unprepared for a conflict between the Assembly and the Soviets. At the same time they were quite clear that the future masters of Russia would be the Soviets, the revolutionary organisations of the proletariat and peasantry. If they did not consider the possibility of conflict between the Constituent Assembly and the Soviet, it was because then it was the provisional government which stood in opposition to both the Soviets and the Constituent Assembly.Immediately after the October insurrection, Lenin was clearly worried about the results of elections to the Constituent Assembly and wanted them to be postponed, the voting age to be lowered to 18 years, the electoral list revised, and the Cadets and Kornilov supporters outlawed. Other Bolshevik leaders said that postponement was unacceptable, especially since the Bolsheviks had often reproached the provisional government with this very crime.‘Nonsense!’ objected Lenin. ‘Deeds are important, not words. In relation to the provisional government the Constituent Assembly represented, or might have represented, process; in relation to the regime of the Soviets, and with the existing electoral lists, it will inevitably mean retrogression. Why is it inconvenient to postpone it? Will it be convenient if the Constituent Assembly turns out to be composed of a Cadet-Menshevik-Socialist Revolutionary alliance? ...Sverdlov, who more than others was connected with the provinces, protested vehemently against the adjournment.Lenin stood alone. He kept on shaking his head, dissatisfied, and went on repeating:‘You are wrong; it’s clearly a mistake which can prove very costly. Let us hope that the revolution will not have to pay for it with its life.’ [5]In the event, the Bolsheviks permitted the election to be held.
 The elections took place over a period of a few weeks. One study gives the following results:The vote by parties for the whole countrySocialist Revolutionaries15,848,004     Ukrainian Socialist Revolutionaries  1,286,157     Mensheviks  1,364,826     Cadets  1,986,601     Bolsheviks  9,844,637     Others11,356,651     Total  41,686,876 [6]For the seats in the Constituent Assembly, the archives of the October Revolution have assembled a list of 707 deputies, divided into the following groupings:Socialist Revolutionaries 370     Left Socialist Revolutionaries  40     Bolsheviks 175     Mensheviks  16     Popular Socialists    2     Cadets   17     National groups   86     Unknown      1 [7]The Socialist Revolutionaries achieved a clear majority, both of the popular vote and of the seats in the Assembly. While the Bolshevik vote was about a quarter of the total, in some key areas they predominated. In the two capitals, the Bolshevik vote was four times larger than that of the Socialist Revolutionaries, and nearly 16 times larger than that of the Mensheviks.What about the troops?If the districts were remote from the metropolitan centers, and specifically from the influence of the Petrograd Soviet and the Bolshevik party organization, the SRs carried the day, and the farther removed the district was, the greater their degree of success; but on the Northern and Western Fronts the old-line agrarian appeal of the Socialist Revolutionary Party had been overbalanced by intensive propaganda in favour of immediate peace and immediate seizure of the estates, so that here the SRs sustained a crushing defeat and Lenin’s party won a great victory. The contrast is seen in the accompanying tabulation: Western FrontRumanian FrontBolsheviks653,430   167,000     Socialist Revolutionaries180,582   679,471     Mensheviks    8,000     33,858     Ukranian Socialist Bloc  85,062   180,576     Cadets  16,750     21,438     Residue  32,176     46,257     Total976,000 1,128,600 [8]If we take the northern and western fronts, the vote polled by the Bolsheviks amounted to over a million, compared with 420,000 votes polled by the Socialist Revolutionaries. However, the strength of Bolshevism waned steadily as the influence of the metropolitan centres receded. Not only the Socialist Revolutionaries, but also the Mensheviks, were helped by distance: thus on the western front Menshevism was already virtually extinct by the time of the election, whereas on the Rumanian front it still retained a following, albeit a modest one. [9] [A]‘The conclusion is inescapable, that only time was needed to make the more remote fronts resemble the Petersburg garrison,’ writes Radkey, historian of the Socialist Revolutionary Party. [10]Summing up the general position in the country, Radkey writes:The Bolsheviks had the center of the country — the big cities, the industrial towns, and the garrisons of the rear; they controlled those sections of the navy most strategically located with reference to Moscow and Petrograd; they even commanded a strong following among the peasants of the central, White Russian, and northwestern regions. The Socialist Revolutionaries had the black-earth zone, the valley of the Volga, and Siberia; in general they were still the peasants’ party, though serious defections had taken place. Particularist or separatist movements had strength in the Ukraine, along the Baltic, between the Volga and the Urals, and in the Transcaucasus; of these movements by all odds the most robust was Ukrainian nationalism. Menshevisrn was a spent force everywhere save in the Transcaucasus, where it was entwined with Georgian nationalism. [11]
 So, contrary to Bolshevik expectations, the Right Socialist Revolutionaries dominated the Constituent Assembly. Lenin used a number of arguments to explain this. First, the elections were held under an obsolete law that gave undue weight to the Rights among the Socialist Revolutionary candidates:as is well known, the party which from May to October had the largest number of followers among the people, and especially among the peasants – the Socialist Revolutionary Party largest number of followers among the people, and especially among the peasants – the Socialist Revolutionary Party – produced united election lists for the Constituent Assembly in the middle of October 1917, but split in November 1917, after the elections and before the Assembly met.For this reason, there is not, nor can there be, even a formal correspondence between the will of the mass of the electors and the composition of the elected Constituent Assembly. [12]Radkey, who was far from being partial to the Bolsheviks, confirms this evaluation of Lenin’s. [13]However, the main reason for the conflict between the Assembly and the Soviets was more fundamental. The catchment area covered by the Constituent Assembly was far wider than that of the Soviet. While the Second Congress of Soviets represented about twenty million people, the number of votes for the Constituent Assembly was more than forty million. The Bolsheviks, together with the Left Socialist Revolutionaries, represented the overwhelming majority of the urban proletariat, the peasantry in the neighbourhood of the industrial centres, and the troops in the north and northwest. These were the more energetic and enlightened elements of the masses, on who active support the revolution depended for survival. The Socialist Revolutionaries who dominated the Constituent Assembly represented the political confusion and indecision of the petty bourgeoisie in the towns and the millions of peasants relatively distant from the capital and industrial centres.To consider the Constituent Assembly in isolation from the class struggle was impossible. The interests of the revolution had to take precedence over the formal rights of the Constituent Assembly. At the Second Congress of Russian Social Democracy, Plekhanov had already answered in the affirmative the question he himself posed: whether the proletariat, on coming to power, would be justified in suppressing democratic rights. [14]The Constituent Assembly met on 5 (18) January 1918. Sverdlov, in the name of VTsIK read a ‘Declaration of the Rights of the Toiling and Exploited People’ written by Lenin. It summed up the main decrees of the Soviet government: all power to the Soviets, the decree on land, the decree on peace, workers’ control over production. Sverdlov’s proposal that the Assembly should endorse the declaration was rejected by 237 votes to 136. This sealed the fate of the Assembly. After one day of existence it was dissolved.Unlike the disagreement among the Bolshevik leadership on the question of coalition government, the decision to dissolve the Constituent Assembly led to little dissension in the party. There were, however, some difficulties.On 13 (26) December, Pravda published Lenin’s Theses on the Constituent Assembly, in which final form was given to the Bolshevik tactics. Starting from the principle that ‘revolutionary Social Democracy has repeatedly emphasized, ever since the beginning of the Revolution of 1917, that a republic of Soviets is a higher form of democracy than the usual bourgeois republic with a Constituent Assembly’, Lenin argued that the election returns did not correspond with the actual will of the people. Since the October revolution the masses had moved further to the left, a change not reflected in the Assembly. The civil war then beginning had ‘finally brought the class struggle to a head, and destroyed every chance of settling in a formally democratic way the very acute problems with which history has confronted the people of Russia’. If therefore, the Constituent Assembly would not declare that ‘it unreservedly recognizes Soviet power, the Soviet revolution, and its policy on the question of peace, the land and workers’ control’, then ‘the crisis in connection with the Constituent Assembly can be settled only in a revolutionary way, by Soviet power adopting the most energetic, speedy, firm and determined revolutionary measures.’ [15]Lenin used two arguments to justify the dispersal of the Constituent Assembly. The basic on was that the Constituent Assembly was a bourgeois parliament and had become the rallying point for the forces of the counter-revolution; the second, that for a number of contingent reasons (the split within the SRs, the timing of the elections, etc.) the composition of the Constituent Assembly did not adequately reflect the actual balance of forces within the country.
 In our times there is not a single issue which can be decided by ballots. In the decisive class battles bullets will prevail. The capitalists count the machine guns, the bayonets, the grenades at their disposal, and so does the proletariat. Lenin expressed this very clearly in his article ‘The Constituent Assembly Elections and the Dictatorship of the Proletariat’. While in terms of voting power the countryside outweighed the towns, in real social, political power, the towns were far superior. ‘The country cannot be equal to the town under the historical conditions of this epoch. The town inevitably leads the country. The country inevitably follows the town.’ Controlling the capitals gave the Bolsheviks a great ‘striking power’:An overwhelming superiority of forces at the decisive point at the decisive moment — this ‘law’ of military success is also the law of political success, especially in that fierce, seething class war which is called revolution. Capitals, or, in general, big commercial and industrial centres (here in Russia the two coincided, but they do not everywhere coincide), to a considerable degree decide the political fate of a nation, provided, of course, the centres are supported by sufficient local, rural forces, even 1 that support does not come immediately: [16]The Bolsheviks had ‘(1) an overwhelming majority among the proletariat; (2) almost half of the armed forces; (3) an overwhelming superiority of forces at the decisive moment at the decisive points, namely: in Petrograd and Moscow and on the war fronts near the centre.’Not only could elections not replace force in achieving the dictatorship of the proletariat, but this dictatorship itself must be ‘an instrument for winning the masses from the bourgeoisie and from the petty bourgeois parties’. [17]Lenin poured ridicule on the reformist leaders who arguedthe proletariat must first win a majority by means of universal suffrage, then obtain state power, by the vote of that majority, and only after that, on the basis of ‘consistent’ (some call it ‘pure’) democracy, organize socialism. But we say on the basis of the teachings of Marx and the experience of the Russian revolution: the proletariat must first overthrow the bourgeoisie and win for itself state power, and then use that state power, that is, the dictatorship of the proletariat, as an instrument of its class for the purpose of winning the sympathy of the majority of the working people. [18]The petty-bourgeois democrats ... are suffering from illusions when they imagine that the working people are capable, under capitalism, of acquiring the high degree of class-consciousness, firmness of character, perception and wide political outlook that will enable them to decide, merely by voting, or at all events, to decide in advance, without long experience of struggle, that they will follow a particular class, or a particular party ...Capitalism would not be capitalism if it did not, on the one hand, condemn the masses to a downtrodden, crushed and terrified state of existence, to disunity (the countryside !) and ignorance, and if it (capitalism) did not, on the other hand, place in the hands of the bourgeoisie a gigantic apparatus of falsehood and deception to hoodwink the masses of workers and peasants, to stultify their minds, and so forth. [19]
 Under the banner of the Constituent Assembly reaction assembled its forces. Long before, Engels had explained the role of ‘pure democracy’ in a letter to Bebel (11 December 1884) on ‘pure democracy’.pure democracy ... when the moment of revolution comes, acquires a temporary importance ... as the final sheet-anchor of the whole bourgeois and even feudal economy ... Thus between March and September 1848 the whole feudal-bureaucratic mass strengthened the liberals in order to hold down the revolutionary masses ... In any case our sole adversary on the day of crisis and on the day after the crisis will be the whole of the reaction which will group around pure democracy, and this, I think, should not be lost sight of. [20]And Marx elaborated: ‘from the first moment of victory, and after it, the distrust of the workers must not be directed any more against the conquered reactionary party, but against the previous ally, the petty bourgeois democrats, who desire to exploit the common victory only for themselves.’ [21]Throughout the years of the civil war in Russia (1918–1920), the slogan of the Constituent Assembly served as a screen for the dictatorship of the landowners and capitalists. Admiral Kolchak’s banner was that of the Constituent Assembly, carried for him by the Socialist Revolutionaries (until he suppressed them). The South-eastern Committee of the Members of the Constituent Assembly, overwhelming Socialist Revolutionary in composition, called for recruits to the Volunteer Army of Generals Denikin and Alekseev. At Archangel, in Siberia, on the Volga, Socialist Revolutionary leaders raised the banner of the Constituent Assembly, under which recruits could be mobilized for the White Armies.A. The same is true of the navy. The Bolsheviks overwhelmed the Socialist Revolutionaries three to one in the Baltic fleet, only to succumb by a margin of two to one in the Black Sea fleet.1. Lenin, Works, Vol. 24, p. 99.2. ibid., Vol. 26, p. 20.3. Browder and Kerensky, op. cit., Vol. 3, p. 169.4. ibid., p. 1729.5. Trotsky, On Lenin, op. cit., pp. 105–6.6. O.H. Radkey, The Elections to the Russian Constituent Assembly of 1917, Cambridge (Mass.) 1950, pp. 16–17.7. ibid., p. 20.8. ibid., p. 36.9. ibid., p. 37.10. Radkey, The Sickle under the Hammer, op. cit., p. 344.11. Radkey, The Elections to the Russian Constituent Assembly of 1917, op. cit., p. 38.12. Lenin, Works, Vol. 26, p. 380.13. Radkey, The Sickle under the Hammer, op. cit., p. 301.14. See T. Cliff, Lenin, London 1975, Vol. 1, p. 116.15. Lenin, Works, Vol. 26, pp. 379–83.16. ibid., Vol. 30, pp. 257–8.17. ibid., p. 263.18. ibid.19. ibid., pp. 266–7.20. K. Marx and F. Engels, Selected Correspondence, London 1942, pp. 433–4.21. K. Marx, Address to the Communist League, 1850, Appendix to F. Engels, Revolution and Counter Revolution in Germany, London 1933. 
Top of the pageLast updated on 19.9.2012
Socialist Revolutionaries

15,848,004     

Ukrainian Socialist Revolutionaries

  1,286,157     

Mensheviks

  1,364,826     

Cadets

  1,986,601     

Bolsheviks

  9,844,637     

Others

11,356,651     

Total

  41,686,876 [6]

Socialist Revolutionaries

 

370     

Left Socialist Revolutionaries

  40     

Bolsheviks

 175     

Mensheviks

  16     

Popular Socialists

    2     

Cadets

   17     

National groups

   86     

Unknown

      1 [7]

 

Western Front

Rumanian Front

Bolsheviks

653,430

   167,000     

Socialist Revolutionaries

180,582

   679,471     

Mensheviks

    8,000

     33,858     

Ukranian Socialist Bloc

  85,062

   180,576     

Cadets

  16,750

     21,438     

Residue

  32,176

     46,257     

Total

976,000

 1,128,600 [8]
