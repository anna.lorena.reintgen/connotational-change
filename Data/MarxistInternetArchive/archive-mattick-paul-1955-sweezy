Paul Mattick 1955
Source: Western Socialist, Boston, USA, May-June 1955;
Transcribed: by Adam Buick.The Present As History. By Paul M. Sweezy. Monthly Review Press (376 pp., $5.00);Mr. Sweezy, an editor of Monthly Review and author of a highly
regarded but quite muddled Theory of Capitalist Development,
presents in this book a collection of book reviews and essays written firing
the last fifteen years. Aside from three short and insignificant papers, all
the reprinted material is still available in its original publication in
various magazines. Its reappearance in book form is difficult to understand,
particularly because the review, the editorial, and even the space-restricted
essay are not the best media for the consideration of comprehensive theories.
To review the reviews of books, of which many are no longer of real interest,
is an awkward affair. It may be said, however, that within the limits of his
media, Sweezy’s comments on works written by Toynbee, Burnham,
Hallgarten, Sternberg, Veblen, Hansen, Pigou, and so forth, are interesting and
justified not only from his own point of view but also from any other realistic
and honest social attitude. Wherever Sweezy applies Marxian criticism to
capitalist theories and ideologies he is quite successful, but where he tries
to square his Marxism with the realities of Russian society he remains
unconvincing. It should be noted, moreover, that what is particularly good in
the book is not so much due to his Marxism as to his technical training as an
economist. His researches in the structure of the American economy, though of
interest to Marxists, are nevertheless more in the spirit of the New
Deal economics. Issues such as the centralization of American capital, the
changing role of the investment banker, interest groups within the economy,
etc., may have emerged as well from any depression-period Congressional
investigation of concentration and monopoly. In fact some of this research was
undertaken on behalf of the New Deal government. But Sweezy’s rather
conciliatory attitude towards the Keynesian brand of economics and his apparent
lack of the specific dogmatism of the party-communist, does not alter the
essentially apologetic nature of his work. 
 Paul Mattick Archive
