MIA  >  Archive  >  Harman  >  Rev. in 21st Cent. Revolution is about more than just overthrowing the ruling class.
It is also about setting in train a new way of cooperating to make
our livelihoods. This is necessary if humanity is to banish the
material hardship that means one billion of the world’s people go
hungry each day, end the economic crises that periodically devastate
the lives of millions more, end the colossal waste, environmental
destruction and spending on weapons, and liberate the mass of the
world’s population from the daily treadmill.Taking control of the means of production is the precondition for
doing this. After all, few of the handful of people who control the
multinational corporations and determine what happens to a huge chunk
of the world’s production have any particular skill when it comes
to producing things. Their wealth enables them to pay other people to
do what is required.
 Those who support the present system tell us it simply is not
possible to run things in any other way. The economist Alec Nove in
his influential book The Economics of Feasible Socialism
(London 983) argued modern production is too complex to be operated
in any other way than through the market mechanisms of capitalism. It
involves the production of too many different products, involving
vast quantities of components. Any attempt to implement democratic
planning would result in a bureaucracy of the sort that arose in the
Soviet Union and be extremely inefficient. He argued: ‘The complex
modern economy is unnameable to centralised direction’ and ‘was
bound to be overwhelmed by these tasks.’But if the technical complexity of production would be a problem
for a democratically planned economy, it must also be problem in a
world economy under the control of a few hundred billionaires. If
these global production empires are to function profitably, they
cannot assume the blind play of the market will provide the hundreds
of thousands of components and other inputs they will need in three
months or two years time. They have to try to plan to ensure they
have these things.This was true even 40 years ago. To produce a light car range the
UK firm Rootes then had:to order, correctly schedule and marshal no less than
16,000 different parts ... to be fed through the production machine
in such a way that thousands of variations can be made on a handful
of basic models ... the company was forced to work on an
approximately five-year pattern on any given model. [G. Turner, The
Car Makers, Harmondsworth 1964]The three supermarket chains that now dominate the sale of
foodstuffs in Britain have to plan for a similar level of complexity.
They want to guarantee the right mix of products in stores month
after month, year after year. They are not prepared simply to hope
that market forces will supply these. On the contrary, they have
established a stranglehold over the food-processing industry, most of
British agriculture and many farmers in countries such as Spain and
Kenya to ensure the output the supermarkets predict they will need.However, capitalist planning is directed towards competition with
rival firms, not the needs of the mass of people. It involves
planning at the behest of those whose wealth gives them control of
production – who have the power to manipulate market relations with
small firms and farmers, and to redirect all know-how to their own purposes.At the most basic level, if those running a multinational
corporation can plan to achieve their ends, there is no intrinsic
reason why democratic organs of workers’ power could not do the
same. Indeed, these would be better placed to do so, since the
planning of each capitalist firm is continually undermined by
attempts to damage the prospects of rivals. Plans are often abandoned
half way through, creating chaos for other firms structured around
supplying inputs. A workers’ government that subjected all food
outlets, for example, to democratically decided targets would not
suffer from this. It would allow co-ordination across an entire
industry instead of competition within it.This does not mean someone trying to calculate in advance the
number of various components to produce – any more than the
individual corporations do this today. But it does mean decisions
about the general direction of the economy being subject to
democratic control. What matters is ensuring investment is directed
to satisfying human need. Such democratic control should be exercised
by elected and recallable representatives of those whose labour
produces the wealth of society as a whole. They would decide whether
to prioritise production of vehicles or of kidney machines, whether
to shorten working time or use extra capacity to raise living standards.The key decisions would have to be made centrally otherwise the
big production units would be competing with each other to sell
products. But once the major decisions are made in any economy, there
would be an enormous leeway about how parts of the economy fit in to
fulfil these. There is no requirement for centralised state control
over every production unit. All that is needed is a basic democratic
willingness by those running each unit – the workers who would take
control of them in a revolutionary confrontation – to accept the
need to find ways of fitting what they do to what has been decided in
a free discussion.This is the opposite of what happened under the so-called planning
implemented by Stalinist, social democratic and third world regimes
in the past. None of these submitted their plans to any organ of
genuine democratic control. Those whose labour created the wealth in
such societies were the last to have any say over what they produced
and for what purpose. The competition between rulers – for example,
between those in the Eastern bloc and those in the West, reflected in
the arms race between the US and USSR – completely distorted their
‘planning’, just as competition between one supermarket chain and
another distorts the planning of both. It was not the complexity of
the economies that created chaos, but the attempt to compete with the
giants of world capitalism. The Soviet economy at its height was less
than half the size of the US economy. The pressure of competition was
correspondingly greater as a result, just as the corner shop has more
difficulty competing with Tesco than does Sainsbury.The revolution of the 21st century can open the way to genuine,
democratic planning, by setting itself a very different goal to that
of Stalin and his successors.
 The revolution of the 21st century can only achieve its goals if
it spreads from initial victories in one country to others. The
history of attempts at ‘socialism in one country’ shows this to
be a blind alley. Capitalism, as an international system, has created
an uneven distribution of resources globally and, along with that, an
international division of labour. No one country contains the
resources necessary to fully satisfy human needs.This applies even more to individual third world countries. After
centuries of pillaging by imperialism, many are too impoverished to
find within their own borders the means to industrialise to the level
of the existing advanced countries. Those third world countries that
have developed have done so on the basis of vicious, dictatorial
repression against the mass of workers and peasants: this was true
not just in Russia and China, but also Taiwan and South Korea. Even
in Cuba, which many people on the left see as a better example, the
attempts at independent development in the 1960s collapsed after the
failure to achieve the target of 10 million tonnes of sugar
production in the 1970s, despite subordinating virtually the whole of
economic life to this goal. This failure left Cuba as dependent on
the Soviet Union as it had once been on the US and the collapse of
the Soviet Union in 1991 left its people facing years of acute
shortages and poverty.What is needed in the 21st century is not development as it was
seen by the middle classes of the third world and their multinational
advisors in the 20th century – the attempt to squeeze out of the
mass of workers and peasants the means to build up industry to a
level comparable to that in the west. Rather, what is needed is a
redirection of the huge resources that currently go to the local rich
and the profiteers of the international system towards improving the
lives of the mass of people. This would be a very different kind of
development to that of the past. Ultimately, to achieve this depends
upon gaining access to the resources not only of the poor parts of
the world, but also to some at least of those controlled by
capitalism in its heartlands.But the mass of people in a single country do not simply have to
sit back and wait for revolution elsewhere. They can make many
immediate steps forward by taking power in their own hands. In
conditions of acute economic crisis, the actual wealth produced in a
country can be far below its potential level. In such circumstances a
revolutionary transformation, involving the redistribution of wealth
from the very rich to the mass of people can produce one-off
improvements in living standards. It is absurd that in a country like
Argentina, millions have gone hungry while vast amounts of food have
been exported to pay interest on foreign debts and fatten the profits
of the country’s agrarian capitalists. But to sustain these
improvements requires the creation of a new international division of
labour, involving more than one country, something only possible by
the spreading of the revolution.There can never be a guarantee that a revolutionary breakthrough
in one country will spread elsewhere. The Russian revolution of 1917
was, as we have seen, left isolated despite the wave of near
revolutions in Germany and elsewhere in Europe. The Cuban revolution
of 1959 created a tidal wave of hope elsewhere in Latin America, but
this was not sufficient to wash away the local regimes which the US
rushed to bolster up.But such an outcome is not inevitable. Economic, social and
political crises that open up the possibility of revolution are
rarely confined to individual countries. The most important
revolutionary upsurges in the last century all occurred on an
international scale: 1917–20, 1934–36, 1943–45, 1956, 1968–75,
1989–91. In each of these, what happened in one country had a
decisive effect elsewhere.There are already signs of similar wave-like process at work in
the first years of the 21st century. The anti-war protests on 15
February 2003 were not confined to individual countries but fed into
each other drawing more of the planet’s people on to the streets
than any single issue ever before in human history. One estimate put
the numbers marching around the world at 20 million. The movements in
Latin America have strengthened each other, so that the continent is
again alive to the hopes of revolution after two decades of defeat
and demoralisation. In Europe, attempts by governments to push
through neoliberal counter-reforms have created resistance across
national frontiers, providing the impetus for the birth of a new left
across the continent.In either continent a successful revolution would have a very real
prospect of spreading to neighbours, drawing into one democratically
planned economic process the resources needed to offer people better
lives in the long term as well as the short term. 
Top of the pageLast updated on 5 October 2016