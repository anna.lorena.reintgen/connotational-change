
      The Chapter which is here put before the reader can be well considered as a
      separate piece of work, although it contains here and there references to what has
      gone before in The Stones of Venice. To my mind, and I believe to some
      others, it is one of the most important things written by the author, and in future
      days will be considered as one of the very few necessary and inevitable utterances
      of the century.
    
      To some of us when we first read it, now many years ago, it seemed to point out a
      new road on which the world should travel. And in spite of all the disappointments
      of forty years, and although some of us, John Ruskin amongst others, have since
      learned what the equipment for that journey must be, and how many things must be
      changed before we are equipped, yet we can still see no other way out of the folly
      and degradation of Civilization.
    
      For the lesson which Ruskin here teaches us is that art is the expression of man's
      pleasure in labour; that it is possible for man to rejoice in his work, for,
      strange as it may seem to us to-day, there have been times when he did rejoice in
      it; and lastly, that unless man's work once again becomes a pleasure to him, the
      token of which change will be that beauty is once again a natural and necessary
      accompaniment of productive labour, all but the worthless must toil in pain, and
      therefore live in pain. So that the result of the thousands of years of man's
      effort on the earth must be general unhappiness and universal degradation;
      unhappiness and degradation, the conscious burden of which will grow in proportion
      to the growth of man's intelligence, knowledge, and power over material nature.
    
      If this be true, as I for one firmly believe, it follows that the hallowing of
      labour by art is the one aim for us at the present day.
    
      If Politics are to be anything else than an empty game, more exciting but less
      innocent than those which are confessedly games of skill or chance, it is toward
      this goal of the happiness of labour that they must make.
    
      Science has in these latter days made such stupendous strides, and is attended by
      such a crowd of votaries, many of whom are doubtless single-hearted, and worship in
      her not the purse of riches and power, but the casket of knowledge, that she seems
      to need no more than a little humility to temper the insolence of her triumph,
      which has taught us everything except how to be happy. Man has gained mechanical
      victory over nature, which in time to come he may be able to enjoy, instead of
      starving amidst of it. In those days science also may be happy; yet not before the
      second birth of Art, accompanied by the happiness of labour, has given her rest
      from the toil of dragging the car of Commerce.
    
      Lastly it may well be that the human race will never cease striving to solve the
      problem of the reason for its own existence; yet it seems to me that it may do this
      in a calmer and more satisfactory mood when it has not to ask the question, Why
      were we born to be so miserable? but rather, Why were we born to be so happy?
    
      At least it may be said that there is time enough for us to deal with this problem,
      and that it need not engross the best energies of mankind, when there is so much to
      do otherwhere.
    
      But for this aim of at last gaining happiness through our daily and necessary
      labour, the time is short enough, the need so urgent, that we may well wonder that
      those who groan under the burden of happiness can think of anything else, and we
      may well admire and love the man who here called the attention of English-speaking
      people to this momentous subject, and that with such directness and clearness of
      insight, that his words could not be disregarded.
    
      I know indeed that Ruskin is not the first man who has put forward the possibility
      and the urgent necessity that men should take pleasure in Labour; for Robert Owen
      showed how by companionship and good will labour might be made at least endurable;
      and in France Charles Fourier dealt with the subject at great length, and the whole
      of his elaborate system for the reconstruction of society is founded on the certain
      hope of gaining pleasure in labour. But in their times neither Owen nor Fourier
      could possible have found the key to the problem with which Ruskin was provided.
      Fourier depends, not on art for the motive power of the realization of pleasure in
      labour, but on incitements, which, though they would not be lacking in any decent
      state of society, are rather incidental than essential parts of pleasurable work;
      and on reasonable arrangements, which would certainly lighten the burden of labour,
      but would not procure for it the element of sensuous pleasure, which is the essence
      of all true art. Nevertheless, it must be said that Fourier and Ruskin were touched
      by the same instinct, and it is instructive and hopeful to note how they arrived at
      the same point by such very different roads.
    
      Some readers will perhaps wonder that in this important Chapter of Ruskin I have
      found it necessary to consider the ethical and political, rather than what would
      ordinarily be thought, the artistic side of it. I must answer, that, delightful as
      is that portion of Ruskin's work which describes, analyses, and criticises art, old
      and new, yet this is not after all the most characteristic side of his writings.
      Indeed from the time at which he wrote this chapter here reprinted, those ethical
      and political considerations have never been absent from his criticism of art; and,
      in my opinion, it is just this part of his work, fairly begun in the `Nature of
      Gothic' and brought to its culmination in the great book Unto this Last,
      which has had the most enduring and beneficent effect on his contemporaries, and
      will have through them on succeeding generations.
    
      John Ruskin the critic of art has not only given the keenest pleasure to thousands
      of readers by his life-like descriptions, and the ingenuity and delicacy of his
      analysis of works of art, but he has let a flood of daylight into the cloud of
      sham-technical twaddle which was once the whole substance of `art-criticism,' and
      is still its staple, and that is much. But it is far more that John Ruskin the
      teacher of morals and politics (I do not use this word in the newspaper sense), has
      done serious and solid work towards the new-birth of Society, without which genuine
      art, the expression of man's pleasure in his handiwork, must inevitably cease
      altogether, and with it the hopes of the happiness of mankind.
    
      1892.
    
The William Morris Internet Archive : Works
