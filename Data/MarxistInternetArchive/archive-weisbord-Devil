By Albert Weisbord
(From the magazine “La Parola del Popolo” June/July 1961)THE word, “devil” comes from “deus” (Latin) or “diva” (Sanscrit) meaning God. When you

speak of the Devil you speak of God.Mankind has long recognized the Devil as the most powerful God to whom the earth and all its

people belong. This is confirmed by the holy teachings that embody the primitive folklore of

peoples, by the events of natural evolution, by the nature of man, by the practices of the churches,

by today’s events. The Devil’s glory is such that he is actually called “Lucifer” or “bearer of

light"!According to the Bible, in the very beginning when God was supposed to have created the earth

and to have peopled it with its creatures there was found among them Satan or the Devil. He

either had been placed on earth by God, or he himself was the God that had created the earth.

That he himself is the all-powerful God is seen from the story of the Old Testament.Following the Old Testament, Adam and Eve, the first people, are in a Garden of Eden, in a

Paradise. They have everything they want, or rather need, for their wants are poorly developed.

One thing, however, is forbidden to them, the apple from the tree of knowledge. Now if God

controlled the earth why would he forbid mankind from knowledge, of all things? Because the

truth would make man free of God? Because knowledge would destroy man’s faith, or would

have exposed the very idea of God as a concept of the Devil himself?At any rate, in the Bible it is apparently the Devil, in the guise of a serpent, who entices man to

taste the apple from the tree of knowledge and who urges man, in effect, not to trust faith but to

taste, try, probe, discover, and grow wiser and learn. It seems, however, that this is only a trick,

for appearing as an evil spirit and not as God, the Devil seeks to prove that the abandonment of

faith carries with it terrible consequences for when man actually does the bidding of the serpent

and tastes the apple of knowledge he is thrown out of the Garden of Eden and is cursed forever

with all the ills of this world. Thus man is taught to believe that this eating of the tree of

knowledge was the original sin, the most terrible thing he could have done, a devilish mistake for

which he must atone by running to God for forgiveness crying: I will have faith; Faith alone

saves; Faith alone is all that matters!With knowledge recognized as a device prompted by the Devil and with faith designated as the

opposite of what is evil the Devil succeeds in getting man to accept his fundamental principles,

namely: Trust in faith; no trust in knowledge based on experiment, discovery, examination,

explanation, science. The Devil hides his real victory by making it appear that submission to faith

is the wish of God—another God which is the figment of the Devil’s imagination placed inside

the head of man. Of course the Devil knows that man, once having tasted the apple, cannot now

untaste the apple. (The Devil knows the second law of thermodynamics also!) In fact man goes

on eating more and more apples and liking them so much that the very word “Eden,” according to

one professor, comes from the word “apple.” There is something in man, apparently, that makes

him go on tasting the apple of knowledge despite the terrible evils that may befall him. To stop

this process force must be added to prevent man from seeking further knowledge. This force

consists of physical, social, and psychological pressures that form a great set of tools for carrying

out the Devil’s will.We have tasted enough of the apple of wisdom to know that mankind did not jump full blown

into the world but took a million years in emerging from the animal stage. The predecessors of

man, as animals, had no idea of either God or the Devil. One might say that the animal is of no

concern to the Devil. It is only when man dimly emerges from the animal world and begins to

think and talk that he can begin to conceive of the idea of a devil. There is then no god, only an

idea of devils. Devils are everywhere around him. They prevent him from multiplying and

prospering. They ambush him to destroy him in a thousand ways. They must be reckoned with at

every moment, on every side. Else how explain death, hysteria, disease, abortion, birth, dreams,

chance, the stars and moon and sun, the tide and waters, the seasons, the loves and hates and

passions that carry him away? To man emerging from the animal world, with little thinking

ability but with great emotional capacity, everything is looked at emotively from the standpoint

of what does it mean to him: good or evil? life or death? Every thing is animated and personified.

His world is full of fetishism of devils. This is the stage before religion, where there are no gods,

only devils. The Devil has not deemed mankind bright enough yet to have to be fooled by means

of the trick of “God.” At this point man does not control nature but is, on the whole, still mostly

animal passively submitting to the forces of nature and trying dimly to understand nature and its

workings.But man does not live individually alone. He lives in a horde, the development of the herd that

forms itself the chief instrument to move mankind forward from the animal world to the world of

homosapiens. In this horde, however, some are smarter than others. It is only to the brightest of

such man-animals, the witch-doctors, that the concept of devil can be understood and used to

increase their power over the others. The witch-doctors undertake to explain the workings of the

devil to the others, if the others will work for them and support them. Their practice is voodoo.

They soon build up a powerful influence over the minds of the other members of the family,

gens, clan, tribe. They find the leisure to think not only about the workings of the devil but about

how to make other men work more for them and theirs.Witchcraft and ruler soon emerge into one. This step arrives when the tribe is not only the victim

of circumstances but the beneficiary. The horde wins better feeding and hunting grounds and

defends them against others. It is victorious and with victory the tribal religious force is

conceived not merely as something to propitiate unfriendly devils but as something that has

placed the devils on the side of the tribe. The winning tribe’s devils are stronger than those of the

losers. Devils are no longer to be feared but to be welcomed and worshiped. It is at this point that

devils become gods and voodooism gives way to true religion. It is also the moment when the

winning tribe begins to understand the value of private property and the hunting and fishing

communistic stages give way to livestock raising and to agriculture. The horde has become an

integrated society with an elaborate subdivision of labor headed by a witchdoctor-ruler. The

others in the society are believers, they have “faith.” The witchdoctor-ruler has not only the sole

right to interpret the wishes of God but now sets himself up as God himself. To the mass of

people this is very fitting because he acts as the very devil. The people become slaves to their

devil lord and master, their God. Private property in cattle now has developed into private

property in humans: slaves.As part of the organization of witchdoctor-ruler a whole set of “wisemen” are supported. They

include astrologers, viscera inspectors and interpreters, necromancers, magicians, soothsayers,

oracles, wizards, dream interpreters, priests, temple guardians, etc. All those who make the faith

are well supported and fed; those who have the faith are the victims and slaves. This continues

until the particular witchdoctor-ruler (the God) loses to another witchdoctor-ruler when it is now

the turn of the masters to become the victims of their “faith.” Religion takes the form of gods

perpetually fighting and becoming devils. To the mass of slaves, all act like devils. There is no

good god, no god of love, but only gods to propitiate, gods of evil.In addition to the regular army of the ruler to enforce the faith, there is also the clerical army of

the faith. One physically compels belief, the other swindles its way into the minds of men to have

them believe from their first days of birth. For belief means obedience and obedience is the first

duty of the slave. (Ruhe is der erste Pflick des Burgentums.)The organization of clergy and their hangers-on find faith very self-satisfying. They feast and live

well. They would soon control all the wealth if they did not, alas, have to share it with the ruler,

but so long as the ruler is considered as God himself, the clergy cannot supplant him. But why

should not one God be separated from the ruler, an “independent” God over all, so to speak? This

myth the clergy carefully proceed to develop.THE CHIEF MERIT for this innovation should go to the Hebrews. Whatever the reasons for

prompting them to take this course they were the ones to promulgate a single deity entirely

independent of the secular ruler and speaking solely through selected priests by means of secret

revelations. So long as there were many gods, man’s faith naturally was restricted and frustrated

as the luck of his gods went up and down with military victory and defeat. Since these gods were

made in the image of demons they all behaved like men with carnal lusts and human passions.

Such a situation could not satisfactorily delay men’s longing for greater knowledge and truth such

as the Devil’s faith was supposed to block.The Hebrew’s God, however, was a fine piece of the devil’s invention. The Hebrew God was

eternally jealous, filled with wrath, bloodthirsty and revengeful. He was what a devil ought to be.

God was revealed only to a priestly caste which took complete control over society making sure

that an enormous share of the social wealth fell into its hands. The trouble was that this priestly

caste could not organize a military State, and without such a State the Hebrews were destined to

be destroyed as a nation and scattered all over the globe.God would have to be removed as the private property of the Hebrew. Furthermore, the problem

could be best resolved by a combined system in which the weak brain of the Pharoah was to be

controlled by the smart brain of the levite, and where the king or emperor who did the fighting

could do so only after being anointed or approved by the clergy. The Devil devised this system

only after considerable trial and error but it was a system that succeeded with marvelous

effectiveness until modern times. Only today, for the first time, is it being seriously challenged by

the atheistic (anti-devil) Communists.The Christian religion, classically enforced by the Catholic Church, is the answer that the Devil

gave to the problem of saving faith and preventing knowledge from eventually overthrowing all

religion, including the Devil who is the center piece of it. According to Christians, God is

independent of the ruler but works his will through the ruler if the ruler obeys God as interpreted

by the clique that runs the church. “Render unto Caesar that which is Caesar’s and render unto the

Lord (the Church) that which is the lord’s (the Church’s).” With such a slogan Caesar can not

destroy the temples but build them. He and his family, when anointed and approved by the imps

of Satan (the clergy) will rule forever over the slaves who have faith provided Caesar himself

will have the faith laid down by Satan’s lieutenants, the serpents in clerical robes.And now it becomes the work of ruler and church to see that the apple of knowledge is never

eaten or is indigestible. No one to read or write except the clergy. The worst frauds and forgeries

to pass unchallenged. A world of witches and devils, of tortures, of eternal damnation, of

ubiquitous sin. A closed circle of ignorance. A repeated and continuous mumbling of prayers.

Caesar and the church ruthlessly stamp on any eating of the apple. They massacre and slay all

who oppose. But to no avail. The apple of knowledge continues to be eaten. The imps of the

Devil fight among themselves and reveal their cloven hooves!Cursed be Adam the atheist who first disobeyed and ate the apple and who later came to love the

apple. He is no longer Adam, the poor victim of the Devil, but God himself!* * * * * Return to the A. Weisbord  Internet Archive
