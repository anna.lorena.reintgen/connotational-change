Bela Kun
First Published: Pravda June 27, 1918
		
Source: International Socialist Library No. 15, Revolutionary Essays by Bela Kun, B.S.P., London.
		
Transcription/Markup: Brian Reid
                
Proofreader: Chris Clayton
                
Public Domain:  Marxists Internet Archive  
(2005).  You may freely copy, distribute, display and perform this  
work; as well as make derivative and commercial works. Please credit  
“Marxists Internet Archive” as your source. The communiques from the internal front of the Austro-Hungarian monarchy daily give us further and further hope. The defeat on the Italian front is not the result of the strength of the Italian Army. On the contrary, it is brought about by the sharpening of the conflict on the internal front. The troops which have fought blindly and senselessly for years, in the cause of imperialism, are now deliberately surrendering. In Austria-Hungary there has at last arisen a yearning for the defeat of one's own imperialism. This denotes already a high level of development of the revolutionary consciousness.

Simultaneously with the news of defeats on the Italian front information has arrived, from the internal front, concerning bloody repression in Hungary. “The factories are idle,” declares the Premier Wekerle; “just at the moment when their intensive activity is required.”

This is a patent symptom that, by the undermining of war industry, the workers are instinctively striking for the defeat and dissolution of the military State institutions of their “own” imperialists, in order to clear the path in this way for the revolution. The refusal to accord the most elementary rights to the proletariat raises these waves still higher. The immediate political cause of the recent explosion was the project of electoral reform proposed by Tisza, which annuls all the solemn promises hitherto given. All the efforts of the official Hungarian Social-Democratic Party were directed only towards the achievement of electoral reform. They were attempting to divert the working-class movement into “legal” channels, and thereby were hindering the development of the revolution. But objective conditions broke up these artificial channels, and the workers have begun to use semi-legal methods of struggle. The last events show us that the Government has to suppress the workers’ revolts “with blood and iron.”

At Budapest, where the movement assumed an extremely threatening character, the Government invoked the assistance of the gendarmes, of whose good offices they had availed themselves hitherto only to maintain order in the villages. In them lie all the hopes of the Government at the present moment, as it is no use counting on the soldiers: they are the worst firebrands of the revolutionary movement.

But the weapon is two-edged, and the repressions of the gendarmes render existing antagonisms still more acute. During the last demonstrations at Budapest four workers were killed, while the wounded are reckoned by scores. This measure will still more rapidly force the workers to forsake the peaceful path of the struggle for the franchise. From day to day the conditions for an armed uprising of the masses become more and more mature.

At Pecs, one of the principal industrial and mining centres of Hungary, the soldiers of the 48th Reserve Infantry Regiment shot their colonel and several officers. On the other side of the Danube, in Western Hungary, the soldiers secretly removed from their barracks arms and ammunition.

Returned internationalist prisoners of war, carrying on revolutionary agitation, are subjected to the most savage persecution.

The Government may possibly improve the economic position of the workers to a certain extent; but politically it is quite incapable of making the slightest concession to them. The composition of the Governmental parties precludes the possibility of any modifications whatsoever in the Tisza-Wekerle project of electoral reform. In those parties are represented not only the semi-feudal aristocrats, but also the rich peasants and manufacturers, compulsorily organised nowadays into manufacturers’ associations.

The new project for the compulsory amalgamation of large industrial enterprises, the indirect tax on corn, and the mill monopoly, as a means for uniting the financial and landed aristocracy — all this reduces the proletariat to a condition from which no electoral reform can rescue it. Thanks to this condition, all sections of the lower middle class, as well as the proletariat, have been brought to a state of desperation.

The country has been handed over, lock, stock, and barrel, to the German militarists. The promises and pacifist declarations of Count Czernin could only for a short time keep the people in a state of deception, even with the efforts to the same end of the official representatives of the working-class movement.

The recent meetings and strikes, however, prove that the masses are about to take over the question of peace into their own hands.

That is a sketch of the general situation in Hungary. The new Minister of the Interior is trying to calm the frightened bourgeoisie by telling them that the soldiers’ mutinies will be suppressed by the most drastic means. But there are no longer any reliable troops available for this purpose. In one small town in Bohemia, lately, the following incident occurred. The 68th Infantry Regiment, which hitherto had been considered trustworthy, and which was specially ear-marked for the work of crushing the Czech revolutionary movement at Prague, suddenly went over to the side of the workmen on strike.

The new alliance with Germany is reviving the movement in the Austrian half of the Dual Monarchy as well. The unsuccessful offensive against Italy is there, too, bearing its revolutionary fruit.

The condition of the Austro-Hungarian monarchy clearly points to the fact that the birth-pangs of the revolution have begun.
