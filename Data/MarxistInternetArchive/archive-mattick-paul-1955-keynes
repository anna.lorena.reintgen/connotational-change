Paul Mattick 1955
Source: Western Socialist, Boston, USA, November-December 1955;
Transcribed: by Adam Buick.Classical economy, whose beginning is usually traced to Adam
Smith, found its best expression and also its end in David Ricardo. Ricardo, as
Marx wrote, “made the antagonism of class-interest, of wages and profits,
of profits and rent, the starting-point of his investigation, naively taking
this antagonism for a social law of nature. But by this start the science of
bourgeois economy had reached the limits beyond which it could not pass,”
for a further critical development could lead only to the recognition of the
contradictions and limitations of the capitalist system of production. By doing
what could no longer be done by bourgeois economists, Marx felt himself to be
the true heir, and the destroyer as well, of bourgeois economy.The further development of economic theory supported Marx’s opinion.
Though bourgeois economy was indeed unable to advance, it was able to change
its appearance. Classical economics had emphasized production and the system as
a whole. Their followers emphasized exchange and individual enterprise.
Although there arose no serious doubt that the capitalist system is natural,
reasonable, and inalterable, yet the early confidence of bourgeois economy was
slowly destroyed by a growing discrepancy between liberal theory and social
reality. The increasing economic difficulties which accompanied the
accumulation of capital developed an interest in the business cycle, in the
factors that make for prosperity, crisis and depression. The neo-classical
school, whose best-known proponent was Alfred Marshall, attempted to transform
economy into a practical science, that is, to find ways and means to influence
market movements and to increase both the profitability of capital and the
general social welfare. But the increasing length and violence of depressions
soon changed a new optimism into even deeper despair, and the sterility of
bourgeois economics led economists once more to embrace the less-embarrassing
security of “pure theory” and the silence of the academies.In the midst of the Great Depression, bourgeois economic theory was suddenly
raised from the dead by the “daring” theories of John Maynard
Keynes. His main work, The General Theory of Employment, Interest and
Money, was hailed as a “revolution” in economic thought and
led to the formation of a school of “Keynesian economics.” While
persistent “orthodox” economists opposed this new school as
“socialistic” or “illusionary,” so-called socialists
attempted to blend Marx with Keynes, or rather, to accept Keynes’
theories as the “Marxism” of our time. Marx’s scepticism with
regard to the future of bourgeois economy was now said to indicate only his
inability or unwillingness to criticize the classicists
constructively. Of Keynes it was said that he made real Alfred
Marshall’s aspirations for a reformed and improved capitalism. These
endeavors, as well as the great popularity of the “Keynesians,”
both generally and in academic circles, and also their insistence upon the
practical applicability of their economic reasoning and their apparent
political influence, make it both advisable and interesting to investigate
their claims and to review the work of their deceased master in the light of
the actual development and the recognizable trend of present-day society. This
invites a comparison of the Keynesian with the Marxian point of view.Until the publication of the General Theory, Keynes was
regarded as an economist of the neo-classical school whose marginal language
was also his own. Economic categories were decked out in psychological terms,
presumably derived from “human nature.” Individual anticipations
and disappointments determine economic life and Keynes even spoke of the
money-making and money-loving instincts of individuals as the main motive force
of the economic machine. He believed that it is a “psychological
law” that individuals tend to consume progressively smaller portions of
their income as their incomes increase. When aggregate real income increases,
consumption increases, too, of course, but not by so much as income. Assuming
that all investment ultimately serves consumption needs and that an increase of
income increases consumption by less than income, savings will increase faster
than investments. With this, aggregate demand declines and the level of
employment falls short of the available labor supply. This happens in a
“mature” society because of the already existing large stock of
capital, which depresses the marginal efficiency or profitability of capital
and therewith expectations with respect to future capital yields. And this, in
turn, creates a psychological attitude on the part of the wealth-owners to hold
their savings in money-form rather than to invest in enterprise promising
little or no reward.Economic stagnation and large-scale unemployment was at the center of
Keynes’ interest. Traditional economic theory was bound to the imaginary
conditions of full employment and to Say’s “law of the
market” — to the belief, that is, that “supply creates its
own demand.” Like Say, Keynes saw in present and future consumption the
goal of all economic activity, but, in distinction to the French economist, he
no longer held that supply brings forth sufficient demand to maintain full
employment. The refutation of “Say’s law” is hailed as the
most important aspect of the Keynesian theory, particularly because it defeats
this “law” on its own ground by showing that just because of the
“fact” that production serves consumption, supply does not create
its own demand.Almost seventy-five years earlier, Marx had already pointed out that only an
accelerated capital expansion allows for an increase of employment, that
consumption and “consumption” under conditions of capital
production are two different things, and that total production can rise only if
it provides a greater share of the total for the owners and controllers of
capital. The “dull and comical ‘prince de la science’, J.B.
Say,” Marx did not find worth overthrowing, even though “his
continental admirers have trumpeted him as the man who had unearthed the
treasure of the metaphysical balance of purchase and sales” [1]. For Marx, Say’s law of the
market was sheer nonsense in view of the growing disequilibrium between the
profit needs of capital expansion and the rationally considered productive
requirements of men, between the “social demand” in capitalism and
the actual social needs; and he pointed out that capital accumulation implies
an industrial reserve army.When Keynes at such late hour, approached Marx’s position,
it was not in order to point to an inherent contradiction of capital production
but to hail the disparity between employment and investment as a great
accomplishment. In his view only “a wealthy community will have to
discover much ampler opportunities for investment if the saving propensities of
its wealthier members are to be compatible with the employment of its poorer
members” [2]. However, short
of closing the gap between income and consumption it follows from Keynes’
theory that “each time we assure today’s equilibrium by increasing
investments we are aggravating the difficulties of securing equilibrium
tomorrow” [3]. For the next
future, however, he thought these difficulties surmountable through government
policies which diminished “liquidity-preference” and increased
“effective demand” by controlled inflation, reduced interest-rates
and lowered real wages, thus promoting inducements to invest. If these are not
sufficient, the government can increase economic activity by public works and
deficit-financing. With full employment the criterion, the effectiveness of
various interventions into the market-economy can be tested experimentally.
Anything that does not lead to full employment is not enough. Because increased
employment by way of “pump-priming” may lead to “secondary
employment” in additional branches of production, it was assumed that it
will lead to such employment. And if all should fail, there is still the
possibility of a direct control of investments by government.It is not necessary to agree with Keynes as to the cause of unemployment to
recognize that the policies he suggested to combat it have been the policies of
all governments in recent history whether they were aware of his theories or
not. They had made their historical debut long before their theoretical
expression. All the monetary and fiscal innovations had already been tried:
public works, inflation and deficit-financing are as old as government rule and
have been employed in many a crisis situation. In modern times, however, they
have been regarded as exceptions to the rule, excusable in times of social
stress but disastrous as a permanent policy.For Marx, the inherent contradictions of capital production are
not of an “economic” character in the bourgeois sense of the term.
He is not concerned with the supply and demand relations of the market but with
the effect of the social forces of production upon the capitalist social
relations of production, that is, with the results of the increasing
productivity of labor upon the production of value and surplus-value.
Celebrated as the product of capital itself, bourgeois theory separates growing
productivity from its social implications. For Marx, it is the independent
variable that determines all the other variables in the system of economic
relationships.The special importance of labor and its increasing productivity in
Marx’s scheme of reasoning led to his discovery of a definite
developmental trend in capital accumulation, which revealed qualitative changes
in the wake of quantitative ones. He could show that the capitalist
“equilibrium mechanism” must itself change in the course of capital
expansion and that it is the latter which determines and modifies the market
forces of supply and demand, since market laws have to assert themselves within
a larger frame of a developing “disequilibrium” between the social
forces of production and the capitalist relations of production.The increase of productivity, of surplus-value and the accumulation of
capital are all one and the same process. It implies a more rapid growth of
capital invested in means of production than that invested in labor power. It
involves what Marx called a “rising organic composition of
capital.” As profits are calculated on the total invested capital, they
must show a tendency to decline as that part of the total which alone yields
surplus value becomes relatively smaller. Of course, the process also implies
an increasing ability to extract more surplus-value, thus nullifying the
“tendency” of profits to decline, and constituting the reason for
the process itself. Leaving aside all the intricacies of Marx’s
exposition, his abstract scheme of capital expansion shows that apart from
competition as the driving force of capital expansion in the market reality,
the production and accumulation of surplus-value already finds in the two-fold
character of capital production — such as exchange and use value —
a limiting element, to be overcome only by the continuous expansion and
extension of the capitalist mode of production. In order to forestall a falling
rate of profit, accumulation must never rest. More and more surplus-value must
be extracted and this involves the steady revolutionizing of production and the
continuous extension of its markets. As long as accumulation is possible, the
capitalist system prospers. If accumulation comes to a halt crisis and
depression set in.Both Marx and Keynes, then, though for different reason, recognize the
capitalist dilemma in a declining rate of capital accumulation. Keynes
diagnoses its cause as a lack of incentive to invest. Marx, looking behind the
lack of incentive, finds the reason for it in the social character of
production as a production of capital. Keynes does not regard crisis and
depression as necessary aspects of capital formation; they are such only under
laissez-faire conditions, and then only in the sense that the economic
equilibrium does not include full employment. For Marx, however, a continuous
capital accumulation presupposes periods of crises and depression, for the
crisis is the only “equilibrium mechanism” which operates in
capitalism with regard to its development. It is in the depression period that
the capital structure undergoes those necessary changes which restore lost
profitability and enable further capital expansion.Marx’s theory of accumulation anticipated Keynes’
criticism of the neo-classical theory through its criticism of classical
theory. In its essentials, then, Keynes’ “revolution”
consists in a partial re-statement of some of Marx’s arguments against
the capitalist economy and its theory. Keynes did not study Marx, and he did
not feel the need for doing so because he identified Marx’s theories with
those of the classicists. By opposing the classical theory Keynes thought he
was opposing Marx as well. In reality, however, he dealt with neither of these
theories but with the neo-classical market theory which had no longer any
significant connection with the ideas of Smith and Ricardo. Marx’s
critique of classical economy, however, resembles Keynes’ criticism of
the neo-classicists, although it cuts deeper than Keynes’ because the
classicists had been profounder thinkers than their apologetic emulators, and
because Marx was not a bourgeois reformer.Although Keynes wished to “knock away the Ricardian foundations of
Marxism,” in order to do so, he had first of all to post himself on these
very foundations. He accepted the theory of value in the Ricardian sense, in
which labor as the sole factor of production includes “the personal
services of the enterpreneur and his assistants.” Like Marx he dealt in
economic aggregates, but while in Marx’s system the analysis in terms of
economic aggregates was to lead to the discovery of the basic trend of capital
accumulation and to no more, in the Keynesian system it was to serve the
formulation of a policy able to support the trend without doing damage to the
capitalist relations of production. Expressed in simplest terms, Keynes’
model represents a closed system divided into two departments of production;
one producing consumption goods and the other producing capital goods. The
total money expenditure in terms of wage-units (based on the working hour) for
both consumption and capital goods constitutes total income. When the aggregate
demand, that is, the demand for consumption and capital goods, equals total
income and implies that total savings equal investments, the system is supposed
to be in equilibrium. A decline of aggregate demand, implying a discrepancy
between savings and investments, reduces total income and produces
unemployment. In order to alter this situation the aggregate demand must be
increased to a point where total income implies full employment.In Marx’s system of economic aggregates constant capital is the
property of the capitalist class, variable capital the social equivalent of
labor-power, and surplus-value the accumulation and income source of the ruling
class. It is here not a question of “social income” and
“social output” and their relation to each other, but a question of
the capitalist exploitation of labor power.Until the second world war, Keynes’ theories enjoyed only
small verification. He had a perfect alibi, however — either his
suggestions were not carried out or they were too meagerly applied. But with
the beginning of war production, Keyness was confident that his theory would be
fully confirmed. Now it would be seen “what level of total output
accompanied by what level of consumption is needed to bring a free, modern
community . . . within the sight of the optimum employment of its
resources” [4]. War-time
policies, however, were quite independent of Keynesian ideology, being neither
different from those employed in the First World War, nor different between
various governments, some of which did not adhere to the Keynesian
“revolution.” All the innovations associated with the commandeered
economy of the second world war, such as a money and credit inflation, price
controls, labor controls, priorities, forced savings, rationing and so forth
had been current in the first debacle despite the then prevailing
“orthodox” approach to economics.If the war economy “proved” the validity of Keynes’
theory, it did so to such a degree that the theory itself had to be put in
reverse. Although unsuccessful in increasing the “propensity to
consume” during the depression, it was a “brilliant success”
in cutting it down during the war. Unable to increase investments up to the
point of full employment, it led to labor shortages through the destruction of
capital. Although suspended during the war, Keynes’ theories would hold
good again with the return to “normalcy.” The war itself only
proved to him that technically any economic system could have full employment
if it so wished; it fid not occur to him that under present conditions war and
preparation for war may be the only way to full employment. It did occur to
others; generally, however, the Keynesian spirit is best represented by such
adherents of the welfare-state as William Beveridge, who, near the end of the
second world war, proposed a full employment program based on the
“socialization of demand without the socialization of production”
[5]. Built on Keynesian principles
and choosing budgetary means for its realization, it was to carry the full
employment of war into the conditions of peace.Fears that large-scale unemployment would return in the wake of the second
world war proved to be exaggerated. A clear distinction between war-production
and peace-time production no longer existed and no need arose to adopt the
Beveridge or any other plan for a fuller utilization of productive resources.
Since the inception of the “Keynesian revolution,” then, no real
opportunity has arisen to test its practical validity. Yet, government
intervention during the depression increased employment to some extent. It may
then be said that the theory proved itself in a very general way wherever it
was employed, and to the degree in which it was applied. In this sense,
however, Keynesianism would be just another name for governmental depression
policies, and would exhaust itself in the suggestion that the government should
take care of the anticipatory aspects of capital formation wherever private
initiative begins to slacken. While production is still production for private
gain, its expansion is the government’s responsibility — a logical
extension of the credit-system by a shift from private to governmental
financial control.Not only from the Keynesian, but from any realistic point of view,
government intervention is now regarded as an inescapable necessity. An
increasing amount of “welfare-economics” is advocated by the
proponents both of the “welfare-state” and of private enterprise.
But even though nobody seems to doubt that government control is here to stay,
the question of its character remains controversial. The Keynesians are
generally for more government intervention, but as the consistent increase of
government regulation and deficit-financing is synonymous with the
transformation of the private into a state-capitalism system, it is often
opposed as a form of “creeping socialism.” Because Keynesianism may
also be regarded as a transitory state towards a completely
government-regulated capitalist economy, it has become the theory of social
reform within the capitalist system. It stands thus in strictest opposition to
Marxism which concerns itself not with social reform but with the abolition of
the capitalist system.1. A Contribution to the Critique of Political economy, Chicago, 1904, p. 123.2. The General Theory, p. 31.3. The General Theory, p. 105.4. J. M. Keynes in The New Republic, July 29, 1940.5. Full Employment in a Free Society, 1945, p. 29. 
 Paul Mattick Archive
