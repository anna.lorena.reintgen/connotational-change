Reform or Revolution. Paul MattickThe workers’ failure to maintain control over their own destiny was due
mainly to Russia’s general objective unreadiness for a socialist development,
but also to the fact that neither the soviets, nor the socialist parties, knew
how to go about organizing a socialist society. There was no historical
precedent and Marxist theory had not seriously concerned itself with the problem
of the socialist reconstruction of society. However, past revolutionary
occurrences had some relevance, particularly as regards Russia, because of her
general backwardness. Following Marx and Engels, Russian Marxists were apt to
point to the Paris Commune as an example of a working-class revolution under
similarly unfavorable conditions. Trotsky wrote, for instance, thatit is not excluded that in a backward country with a lesser
degree of capitalist development, the proletariat should sooner reach
political supremacy than in a highly developed capitalist state. Thus, in
middle-class Paris, the proletariat consciously took into its hands the
administration of public affairs in 1871. True it is that the reign of the
proletariat lasted only for two months; it is remarkable, however, that in the
far more advanced centers of England and the United States, the proletariat
never was in power even for the duration of one day.(1)Lenin, too, found in the Paris Commune a justification for his own attitude
with respect to the Russian Revolution and the Soviet dictatorship. Quoting
Marx, he cited as the great lesson of the Paris Commune that the bourgeois state
cannot simply be taken over by the proletariat but must be destroyed and
replaced by a proletarian state, or semi-state, which would begin to wither away
as soon as majority rule had replaced the minority rule of bourgeois society.
"Overthrow the capitalists,” he wrote, “crush with the iron hand of the armed
workers the resistance of these exploiters, break the bureaucratic machine of
the modern state – and you have before you a mechanism of the highest technical
equipment, freed of ’parasites’, capable of being set in motion by the united
workers themselves who hire their own technicians, managers, bookkeepers, and
pay them all, as, indeed, every ’state’ official, with the usual workers’ wages.
Here is a concrete, practical task, immediately realizable in relation to all
trusts, a task that frees the workers of exploitation and makes use of the
experiences (especially in the realm of the construction of the state) which the
Commune began to reveal in practice.” (2)The practice of the proletarian state as revealed by the Commune was a rather
limited one, however, not so much “consciously” introduced, as Trotsky asserted,
as spontaneously released by the particular conditions of the Franco-Prussian
war, the siege of Paris, and the great patriotism of the Parisian population.
But whatever the circumstances, the incorporation of the workers into the
National Guard, which they came to dominate, gave them the weapons to express
their opposition to the newly established bourgeois government that was trying
to come to terms with the Prussian invaders. Their great suffering during the
siege of Paris had not diminished the proletariat’s patriotic ardor but merely
intensified their hatred for the bourgeoisie, which was willing to accept the
consequences of the defeat in order to secure its own rule through the disarming
of the working class. In view of the increasingly revolutionary situation in
Paris, the bourgeois government established itself in Versailles, preparing for
the reconquest of the capital. The Paris municipal elections of March 26, 1871,
gave the republican left opposition a majority of four to one and led to the
proclamation of the Commune de Paris. The Commune shared the rule of the city
with the Central Committee of the National Guard, responsible for its defense.Although the Communal Revolution saw itself as inaugurating a “new political
era” and as marking the “end of the old governmental and clerical world, of
militarism, of monopolism, of privileges to which the proletariat owes its
servitude, the Nation its miseries and disasters,” (3) the force of
circumstances, as well as the variety of opinions which agitated the Communards,
precluded a far-reaching or consistent socialist program. There were, however,
the decrees that abolished the Army in favor of the National Guard, the
limitation of government salaries to the equivalent of workers’ wages, the
expropriation of Church property, the elimination of fines imposed upon workers
by their employers, the abolition of nightwork in bakeries, the nationalization
of workshops abandoned by their bourgeois owners, and so forth. But these
measures did not as yet point to a radical social transformation. In the
Executive Council of the Commune, moreover, workers were still in a minority. Of
its 90 members, only 21 belonged to the working class, while the rest were
middle-class people such as small tradesmen, clerks, journalists, writers,
painters, and intellectuals. Only a few of the leading members of the Commune
were adherents of the First International. The majority was divided between
Proudhonists, Blanquists, and Jacobins of various descriptions, who were
interested mainly in political liberties and the preservation of small property
owners in a decentralized society. The Commune was thus open to different
interpretations by a variety of interests operating within it.All the shortcomings of the Commune, particularly in the light of Marx’s own
position, could not erase the fact that it was basically an anti-bourgeois
government, one in which some workers actually exercised governmental functions
and expressed their willingness to dominate society. This intrinsic fact weighed
far heavier in Marx’s estimation of the Commune than all its other aspects,
which ran counter to his own concept of socialism.The Commune was not initiated by the International and had no socialist
character in the Marxian sense. That Marx nonetheless identified himself and the
International with the Commune was seen by his political adversaries as an
opportunist attempt to annex the glory of the Commune to Marxism.(4) There is no
need to question Marx’s motivations in making the cause of the Commune his own.
The very passions released by the Paris Commune among the workers as well as the
bourgeoisie indicate that the social class division can come to overrule and
dominate the ideological and even material differentiations within each separate
class. It was not the particular program adopted by the Commune that mattered –
whether it was of a centralist or a federalist nature, whether it actually or
only potentially implied the expropriation of the bourgeoisie-but the fact alone
that segments of the working class had momentarily freed themselves from
bourgeois rule, had arms at their disposal, and occupied the institutions of
government. In the brutal answer of the bourgeoisie to this rather feeble first
attempt at self-government on the part of the Parisian workers, all
class-conscious workers recognized the ferocity and irreconcilability of the
class enemy, not only in Paris but throughout the world. Instinctively as well
as consciously, they stood at the side of the French workers, quite
independently of all the theoretical and practical issues which otherwise
divided the working-class movement. For this reason Marx described the Commune
as “essentially a working-class government” and as “the political form, at last
discovered, under which to achieve the economic emancipation of labor,” for, as
he argued, “the political rule of the producer cannot coexist with the
perpetuation of his social slavery. The Commune was therefore to serve as the
lever for uprooting the economic foundations upon which rests the existence of
classes, and therefore of class rule.” (5)The destruction of the bourgeois state and the capture of political power
made sense only on the assumption that it would be used to eliminate the
capital-labor relation as well. One cannot have a workers’ state in a capitalist
society. Marx seemed convinced that, had the Commune survived, its own
necessities would have forced it to shed its many inadequacies. “The
multiplicity of interpretations to which the Commune has been subjected, and the
multiplicity of interests which construed it in their favor,” he wrote, “show
that it was a thoroughly expansive political form, while all previous forms of
government had been emphatically repressive."(6) The fall of the Commune
precluded further speculation about its expansive quality and the direction it
would take. But Marx saw no need to emphasize his own differences with the
Commune, instead stressing those of its aspects that could serve the future
struggles of the proletariat.For this purpose, Marx simply side-stepped the problem of federalism and
centralism, which, among others, divided the Marxists from the Proudhonists
whose ideas dominated the Commune. He described the latter and its autonomy as
instrumental in breaking the bourgeois state and realizing the producers’
self-government. The Paris Commune, he wrote, was to serve as a model to all the
great industrial centers in France. The communal regime once established in
Paris and the secondary centers, the old centralized government would in the
provinces, too, have to give way to the self-government of the producers. In a
rough sketch of national organization which the Commune had no time to develop
it states clearly that the commune was to be the political form of even the
smallest country hamlet, and that in the rural districts the standing army was
to be replaced by a national militia, with an extremely short term of service.
The rural communes of every district were to administer their common affairs by
an assembly of delegates in the central town, and these district assemblies were
again to send deputies to the National Delegation in Paris, each delegate to be
at any time revocable and bound by the instructions of his constituents. The few
but important functions which still would remain for a central government were
not to be suppressed, as has been intentionally misstated, but were to be
discharged by communal and, therefore, strictly responsible agents. The unity of
the nation was not to be broken, but, on the contrary, to be organized by the
Communal Constitution, and to become a reality by the destruction of the State
power which claimed to be the embodiment of that unity independent of, and
superior to, the nation itself, from which it was but a parasitic excrescence.
(7)By merely relating the theoretically contemplated national federation of the
autonomous communes, Marx gave the impression of general agreement with the plan
and its workability. But the whole of Marx’s work speaks against this
conclusion, for he had never been able to envision the return of political forms
which had already been superseded by more advanced ones. He thus found it
necessary to state that it is generally the fate of completely new historical
creations to be mistaken for the counterpart of older and even defunct forms of
social life, to which they may bear a certain likeness. Thus, this new Commune,
which breaks the modern State power, has been mistaken for a reproduction of the
medieval communes, which first preceded, and afterwards became the substratum
of, that very State power. The communal constitution has been mistaken for an
attempt to break up into a federation of small states, as dreamt of by
Montesquieu and the Girondins, that unity of great nations which, if originally
brought about by political force, has now become a powerful coefficient of
social production. The antagonism of the Commune against the State power has
been mistaken for an exaggerated form of the ancient struggle against
overcentralization. (8)Marx’s opinion, then, the federal character of the Communal Constitution was
not in opposition to a centralized social organization but merely realized the
centralist requirements in ways different from those of the capitalist state, in
ways that assured the self-rule of the producers. In short, as Lenin later
insisted, Marx considered “the possibility of voluntary centralization, of a
voluntary union of the communes into a nation, a voluntary fusion of the
proletarian communes in the process of destroying bourgeois supremacy and the
bourgeois state machinery.” (9)However, the truth of the matter seems to be that on this point Marx did not
strive for great precision in the formulation of his ideas. Written in great
haste and in commemoration of the defeated Commune, his address on the civil war
was not really designed as a lesson on and solution to the problems of the
proletarian revolution and the formation of a socialist society, especially as
before, during, and after the Commune, Marx did not believe in the possibility
of its success, which alone would have lent some reality to the problems posed
in his address ten years after the Commune he described it as an “uprising of a
single city under very special conditions, with a population which neither was
nor could be socialistic.” (10) Though the struggle had been hopeless, it was
still instructive by pointing to the necessity of a proletarian dictatorship to
break the power of the bourgeois state. But this did not make the Commune, as
Lenin claimed, a model for the construction of the communist state. It is not a
communist state, at any rate, that the proletariat has to build, but a communist
society. Its real goal is not another state, whether federalist or centralist,
democratic or dictatorial, but a classless society and abolition of the state.The labor movement is no less prone to mythologize its own history than is
the bourgeoisie. Historic events appear different from what they actually were
and their descriptions are directed more to the emotional receptivity of people
than to their need for accuracy. The class struggle, like any other, precludes
objectivity. Marx and Engels were not above myth-making, even if covered up by a
great amount of sophistry. When Lenin conceived of the Russian revolution as an
emulation of the Paris Commune, he was appealing to a mythological Commune, not
to its actual character. The Commune was of so great an interest to Lenin not
because of what it actually implied, but because of what had been said about it
by Marx and Engels. Representing a wing within the Marxist movement, he felt the
need to justify his own position in terms of Marxian ideology. While hiding in
Finland he wrote his pamphlet State and Revolution on a problem he had pondered
many years before but which now, after the February Revolution, seemed to him no
longer merely of theoretical but also of practical importance. Despite his great
respect for theory, Lenin was preeminently a practical politician. While there
could be no practice without theory, only that theory out of many was acceptable
which suited his particular practice – that is, the capture of political power
under the given conditions. At the same time – as an excuse as well as a
support – the acceptance of a theory must be based on authority; even an Emperor
is there by the grace of God. For Lenin, the unquestioned authorities were Marx
and Engels. In this respect he was fortunate because both were dead and unable
to talk back, and also because during their lives they had commented on a great
number of historical events, and had suggested measures to deal with them, in
accordance with their own time-conditioned apprehension of these events. A
dogmatic acceptance of Marxism will thus allow the faithful Marxist to find
support for his own convictions by merely picking one or another statement out
of the founding fathers’ wide-ranging, though often erroneous, pronouncements on
issues that, due to changed economic and political conditions, have long lost
their meaning. Although Lenin wrote a great deal, he did not contribute, and had
no intention to contribute, to the main body of Marxian doctrine – not because of
a lack of ability to do so, but because, for him, Marx and Engels (and even
Kautsky, up to 1914) had said all that needed to be said for the comprehension
of history, capitalism, and the proletarian revolution.Although there is really nothing positive to be learned from the Paris
Commune except the obvious – that the proletariat can not utilize but must
overthrow the capitalist state – what attracted Lenin to Marx’s comments on the
Commune was the statement that “the political rule of the producers is
incompatible with the eternalization of their social servitude"; that is, that
this political rule, if maintainable, will lead to a socialist society. For
Lenin, this political rule was of course embodied in the new state, emerging out
of the revolution, which would then serve as the vehicle of the socialization
process. Perhaps, carried away by his own revolutionary ardor – and quite in
contrast to his own doctrine, which denied the proletariat the independent
capacity to make a revolution, not to speak of building socialism – Lenin
affirmed in State and Revolution the proletariat’s ability to construct a really
democratic society and to manage its own production under an egalitarian system
of distribution. “Capitalist culture,” he wrote now,has created large-scale production, factories, the postal
services, telephones, etc., and on this basis the great majority of functions
of the “old state power” has become simplified and can be reduced to such
simple operations of registration, filing and checking, that they will be
quite within the reach of every literate person, and it will be possible to
perform them for “workingmen’s wages,” which circumstances can (and must)
strip those functions of every shadow of privilege, of every appearance of
"official grandeur.” All officials, without exception, elected and subject to
recall at any time, their salaries reduced to ’workingmen’s wages” – these
simple and self-evident democratic measures, while completely uniting the
interests of the workers and the majority of the peasants, at the same time
serve as a bridge leading from capitalism to socialism.” (11)But, as we have seen before, in Lenin’s view “workers’ management” finds its
actual realization through the political and economic power of the state. It is
the latter that manages the relations of production and distribution; only this
state is now equated with the working class itself. It is necessary, Lenin
wrote,to organize the whole national economy like the postal system,
in such a way that the technicians, managers, bookkeepers as well as all
officials, should receive no higher wages than “workingmen’s wages"; all under
the control and leadership of the armed proletariat – this is our immediate
aim. This is the kind of state and economic basis we need. All citizens are
transformed into hired employees of the state, which is made up of armed
workers. ... The whole society becomes one office and one factory with equal
pay and equal work.” (12)Of course, Lenin was too well versed in Marxian theory to leave the matter at
this point. He knew that socialism excludes state rule, and he even quoted
Engels’s remark that “the first act in which the state really comes forward as
the representative of society as a whole – the seizure of the means of production
in the name of society – is at the same time its last independent act as a
state.” (13) It should follow that the socialist organization of production is
a function not of the state, but of social institutions that progressively
eliminate the functions of the state, finally to end them altogether. But Lenin
saw the “withering away” of the state in a quite different light. “From the
moment,” he wrote, “when all members of society, or even only the overwhelming
majority, have learned to govern the state themselves, have taken this business
into their own hands, have established control over the insignificant minority
of capitalists, over the gentry with capitalistic leanings, and the workers
thoroughly demoralized by capitalism-from this moment the need for government
begins to disappear.” (14) Instead of dissolving the state, i.e., the
“dictatorship of the proletariat,” within the socialization process, it is the
proletarian state itself, in Lenin’s view, that actualizes the socialization
process. The state has to govern in order for the great majority to learn how to
govern the state.Behind this reasoning, if such it is, hides Lenin’s recognition of the
objective difficulties in the way of the socialist reconstruction of Russian
society. All that could be accomplished was the capture of state power and the
state’s intervention in the economy. Lenin was convinced that Russia’s
“modernization” could be more effectively realized through the agency of the
state than by private-enterprise initiative, and he seems to have convinced
himself of the possibility of imbuing the workers with the same idea, so that
they might identify themselves with the Bolshevik state as the latter identified
itself with the proletariat. However, when Lenin was writing State and
Revolution, the Bolshevik state was only a mere possibility that might or might
not become a reality. The existing Provisional Government had first to be
overthrown, and the workers had to be encouraged to undertake this task, or at
least not to interfere with those who would. They had to be convinced that there
was no need to leave the organization of society to the bourgeoisie, but that
they were quite capable, by themselves, of handling the matter. The very
language of State and Revolution, as well as the rather primitive suggestions on
how to go about building the new society, indicate that this pamphlet was not
conceived as a serious discussion of the relations between the state and
revolution, but as a propaganda instrument to induce Lenin’s followers and the
workers generally to make an end of the existing state. As such it came too late
to affect the seizure of power, though it could still serve as a “Marxist”
justification for the Bolshevik initiative.Everything Lenin wrote prior to State and Revolution, and every step taken
after the seizure of power, turns the apparent radicalism displayed in this
pamphlet into a mere opportunistic move to support the immediate aim of gaining
power for the Bolshevik Party. It is quite possible that Lenin’s identification
with the proletariat was subjectively honest, in that he actually believed that
the latter must come to see in his conception of the revolutionary process their
own true interests and their real convictions.On the other hand, the ambiguities within his revolutionary proposals
indicate that, while trusting his own revolutionary principles, Lenin did not
trust those of the working class, which would first have to be educated to
continue to do for themselves what, meanwhile, would be done for them by the
Bolshevik state. What he allows the workers with his left hand, he takes away
again with his right. It was then not a momentary emotional aberration on the
part of Lenin that induced him to grant so much revolutionary self-determination
to the workers, but a pragmatic move in the manipulation of the revolution in
accordance with his own party concept of the socialist state. 
1. L. Trotsky, Our Revolution (New York, 1918), p. 85. 
2. Lenin, State and Revolution (New York: International, 1932), p. 44. 
3. Quoted by A. Home, The Fall of Paris (New York: Penguin, 1965), P 33). 
4. According to Bakunin, for instance, the impression made by the Commune was
so powerful that “even Marxists, whose ideas were overthrown by the uprising,
saw themselves forced to lift their hats before it. Not only that, in
contradiction to all logic and their own true feelings, they adopted the program
of the Commune as their own. It was a comical but unavoidable travesty, for
otherwise they would have lost all their followers due to the mighty passion the
revolution aroused all over the world.” Quoted by F. Brupbacher, Marx und
Bakunin (Munich: Die Aktion, 1922), pp. 101-102. 
5. Marx, The Civil War in France, in Political Writings, Vol. 3
(Harmondsworth: Penguin, 1974), p. 212. 
6. Ibid. 
7. Ibid., p. 210. 
8. Ibid., p. 211. 
9. Lenin, State and Revolution, p. 46. 
10. Marx to Domela Nieuwenhuis, Marx-Engels Werke, Vol. 35, p. 160. 
11. State and Revolution, pp. 38-40. 
12. Ibid., pp. 44, 83, 84. 
13. Ibid., p. 16. 
14. Ibid., p. 84. 
Table of Contents