Review of A People's History of England, A.L. Morton Source: The Labour
Monthly, Volume 20, Number 07, July 1938, pp.449-452 (1,572 words)
Transcription: Ted Crawford
HTML Markup: Mark HarrisPublic Domain: Marxists
Internet Archive (2010). You may freely copy, distribute, display and perform
this work; as well as make derivative and commercial works. Please credit
“Marxists Internet Archive” as your source.A People’s History of England by A. L. Morton. (Victor
Gollancz. 8s. 6d. Left Book Club Edition, 2s. 6d.)English history is in a bad way. There are innumerable specialist
researchers, grubbing up their bits of detail, who have no conception of the
extent to which their work is undermining traditional views; there are
innumerable popularisers, purveying historical pornography; and there are a
very few people who try to mediate between the specialist and the public, to
bring real historical training and judgment to bear on the task of revaluing
traditional views of history in the light of modern research. Many are coming
to con¬clude that this can only be done satisfactorily if the past is studied
with the apparatus of Marxism. Much of the detail unearthed of late years makes
nonsense of old Whig interpretations, and is forcing Marxist conclusions on
unwilling historians. The future of historical writing is with the Marxist.Yet there are dangers in writing a Marxist history of England just now. So
little conscious thinking has been done along these lines that there is a risk
that the result might contain lots of Marxism but very little history -- might
be a book that the academic historian can sniff at and disregard. The great
triumph of A. L. Morton’s A People’s History of England is that the
academic historian cannot pooh-pooh it. Although it modestly “makes no
pretence of being the result of original research,” the book is in fact
based on wide reading, and is written with extreme care to avoid overstatement,
very revolutionary but unprovable hypotheses. The result is an exceedingly
stimulating work which fulfils a double purpose. It is a very good introduction
for the person who from being badly taught has acquired a distaste for history,
as something irrelevant to urgent contemporary issues; it will be equally
valuable in suggesting new and exciting ideas to students conscious of the
defects, the class and nationalist bias, under which orthodox English history
labours.Morton’s caution is not the result of inadequate knowledge, but of
knowing enough to be aware of the limits of what he knows. There is a
confidence based on real understanding and insight in his judgments.
“Magna Carta has been rightly regarded as a turning point in English
history, but almost always for wrong reasons.” (p.83). “Both the
chaos and the prosperity of the Fifteenth Century were equally real and arose
from a common cause, the transition from feudal and bourgeois society”
(p.133). One needs to be very sure of oneself to get away with remarks like
that. Morton has a gift of clear exposition, an eye for the significant, which
is most illuminating in the middle ages, the period to which Marxists have
given least attention. He brings home as none of his predecessors have done the
social importance, e.g., of the hide (pp.36-7); the influence of the
introduction of. Roman Christianity on property; of the Crusades on municipal
history, on trading relations between England and Italy; the importance of the
monasteries in the latter; the economic basis of “maintenance.” He
continually brings to the fore the connections between economic and social and
religious history, normally slurred over. This is particularly well done in the
transitional centuries in which one section of the feudal landowners still
“looked to war and politics as their natural activity,” whilst
others “were growing content to live on their estates and make the
largest possible income from them,” notably by the wool trade (pp.95-6),
later in the clothing industry. “They had a far greater community of
interests with the merchants who also prospered from this trade than with the
great barons” (p.98). This is emphasized as one of the fundamental
peculiarities in English history, influential alike in the Hundred Years’
War and the Wars of the Roses, leading to the rise of this alliance of merchant
and progressive gentry to influence under the Tudor monarchy, to the taking
over of power in the civil war. Pointing out how these classes benefited from
Henry VIII’s confiscations of monastic estates, Morton stresses the
importance of anti-catholicism in their development¬ -- “The defeat of
the Armada is a turning point in the internal history of England as well as in
foreign affairs. It was the merchants, with their own ships and their own
money, who had won the victory, and they had won it in spite of the
half-heartedness and ineptitude of the Crown and Council.... The war with
Spain, therefore, can best be understood as the first phase in the English
Revolution …. It was a defeat for feudal reaction in Europe ... and the
classes inside England which defeated Philip were exactly those which
afterwards led the opposition to Charles” (p.197). Morton has as good a
brief summary of the causes of the French revolution as is to be found
anywhere. -- “Without colonies the State became top-heavy and was
per¬petually on the verge of bankruptcy. At the same time the French
bourgeoisie benefited, though to a less degree than their English rivals, from
the general expansion of trade that followed the opening of the world to
European exploitation and from the profits that even an unsuccessful war brings
to this class. The result was a rising and ambitious capitalist class face to
face with a discredited and bankrupt autocracy” (p.313).In what will be for many readers the most interesting chapters of the book
the author traces the growing consciousness of working-class struggle, from
organised poaching to organised political and industrial action. He emphasizes
what tends to be obscured by liberal historians -- that much of the
“humanitarian” legislation of the Nineteenth Century was the
product of working-class pressure and divisions in the ruling class. Tory
landlords agitated for factory acts; liberal industrialists for “cheap
corn.” The Education Act of 1870 “was urgently demanded by the
requirements of industry” (p.407). It was because “pestilence could
not be confined to the slums” that public health became a state concern
(p.408). And it is no discredit to the genuine humanitarians that
“profitable as the slave-trade proved during the Eighteenth Century, its
suppression in the Nineteenth Century was even more profitable.” In the
course of hunting down slaves the foundation of British power in West Africa
was laid (p.467). The Factory Acts stimulated invention and concentration of
capital, helped the larger concern to drive the smaller out of the market
(p.370). The repeal of the Corn Laws had a similar effect on agriculture,
increased efficiency capitalisation, profits (p.396). Meanwhile, accompanying
these concessions came “the unobtrusive strengthening of the state
apparatus” -- e.g., by the creation of a police force (p.373). Yet the
author’s attitude to reformism is not merely negative. He correctly
points out the advantages gained by the working class from even a limited
extension of the franchise (pp.406-7) and is very sensible in assessing
Cobbett’s rôle.Morton describes the suicidal change in England’s exports from
consumers’ goods to capital goods, and has some telling things to say on
imperialism. “Ireland was important to the English ruling classes only as
a source of cheap food; for the last hundred years, no matter what form the
exploitation has taken, this has been its essence” (p.441). “The
million and a half people who died [between 1845 and 1850] did not die of
famine, but were killed by rent and profit” (p.442). The continuous
connection between the most conscious elements of the English working class
movement and the Irish revolutionaries is repeatedly shown. The progressive
im¬poverishment of India under British rule is noted (p.449), and the change in
policy towards the native rulers, who have been bought over since they headed
the Mutiny of 1857. Similar tactics are illustrated from South Africa after the
Boer War; “Boers and British together were only a small white minority in
the middle of a negro population, and for this reason, once the supremacy of
British Imperialism had been established, it was necessary to do all that was
possible to conciliate the defeated” (p.472). Morton firmly rebuts the
legend of German war guilt (p.501) and exposes the fact that “the
invasion of Belgium came as a veritable godsend” to the rulers of
Britain. Throughout he devotes serious attention to military technique and its
influence on social history. “The decisive technical advance which had
robbed the knight of his superiority was not ... the invention of gunpowder,
but the longbow” (p.111). “The introduction of the hand gun
coincides with the decline of the yeomanry in England” (p.112).
“Wars were immensely profitable to the English financiers and
contractors, and by adding to their wealth, consolidated the triumph of
Whiggery” (p.282).In fact, this is a book distinguished by realism, historical imagination,
and many-sidedness. Vividly and attractively written, it is just what a
people’s history ought to be. But it is not only that. Its vigour,
independence and sanity make it a work from which the professional has a great
deal to learn. One may wish to argue points of detail -- that is inevitable in
“an essay in historical interpretation” on this scale. But for the
achievement as a whole one can feel nothing but admiration. Its
“interpretations” will have to be reckoned with, and are bound to
have a great influence on all those interested in history whose minds are not
closed by prejudice. That being so it is all the more regrettable that the book
is marred by so many careless misprints. The
Labour Monthly Index
Communist
Party of Great Britain 