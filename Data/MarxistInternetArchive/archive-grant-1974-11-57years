
Written: November 8, 1974      
Markup: Emil 2006 
November 7 is the 57th anniversary of the Russian Revolution. In its results
and effects on Russia and the world, this was undoubtedly the greatest event in
human history. Nearly three generations since the revolution gives time for an
assessment of its results and prospects.What has been achieved? At the time of the revolution Russia was one of the
most backward countries in the world, industrially and in its semi-feudal
autocratic regime. The working class possessed none of the rights which they had
gained in Western Europe, USA and other countries by 1917.From one of the most underdeveloped and barbarous countries, with a backward
industry, Russia has become the second most powerful industrial nation, and a
super-power. This, despite the terrible destruction of the First World War, the
Civil War and the insane destruction of the Nazis in the Second World War in
European Russia, which possessed most of her industry at that time.Reduced to the production of 8 million tons of steel in 1945 (still double
the production under Czarism) as against America's 120 million tons at that
time, thanks to the advantages of state ownership and a plan, and thanks to the
sympathy and support of the world working class, the Soviet Union survived the
war.It was a remarkable achievement. Despite 25 million casualties Russia
defeated Nazi Germany which controlled the resources of practically the whole of
Europe. The war in Europe was in fact, mainly and decisively, a war between Nazi
Germany and Stalinist Russia.Hitler's fundamental mistake was in imagining that he was tackling Stalin,
when what he was really attacking was the Russian revolution!The advantages of state ownership of the land and industry, without private
control of the banks and industry, were demonstrated in the 2nd World War, but
perhaps even more strikingly in the growth of industry and production since
then.Only Japan, for special reasons, shows anything near the increase of
production of Russia. Now she too is effected by the general slowdown which has
afflicted most capitalist countries. Her headlong growth is slowing to the same
pace as that of her major capitalist rivals.A few figures will indicate the staggering progress which has been made. The
Gross National Product of Russia in 1971 was £140,787 million [pounds
sterling], as compared with Britain's £55,998 million. But per head of
population it was £574 compared with Britain's £1007. Since then, of course,
Russia has made further progress in comparison with Britain and the other
capitalist industrial powers. But standards of living still lag behind. However
industrial output, including heavy industry is now higher than any other
capitalist country except America. In machine building Russia is now first in
the world. In 1913 there were 190,000 students in higher and secondary and
specialised education. By 1970 there were 16,841,000 students in these subjects.
If we take Gross Industrial Production in 1913 as 100, in 1971, it was 1283!The production of steel can be regarded as a rough index of industrial
capacity. In 1913 Russia produced over 4 million tons of steel. After the Civil
War, by 1929, Russia recovered to nearly 5 million tons. Britain in 1929
produced 14 million tons of steel. Yet in 1973 Russia's production was over 136
million tons compared with Britain's 26 million.Thus, not only has Russia an overwhelming superiority in absolute production
of steel, but it also has a slight edge in production per head of population.
With the sickness of British and international capitalism, at least European
capitalism risks being outstripped in basic production in the coming years!Comparing at random a few figures with those of Britain, taking into account
that Russia before the revolution was far behind and that Britain's population
is about 55 million as against over 250 million for Russia, we find that Russia
is catching up in production per head of capital goods, but is still quite a way
behind in consumer goods.Leaving aside the consumption of food, in which there is room for argument,
the production and use of certain mass production durable goods is a good
indication of comparative living standards. Thus Britain produced in 1971,
256,980 million kWh's of electric energy (in the public sector, to which must be
added a small amount for the private sector) while Russia produced 858,000
million kWh's.In 1971, the UK produced 1,387,000 radios and 2,390,000 TVs and nearly
2,300,000 vehicles and cars. In 1972 Russia produced for nearly 5 times the
population 8,800,000 radios and 6,000,000 TVs. In 1972 Russia possessed, for a
territory 91 times the size of the UK only 16,100,000 telephones, as compared
with Britain's 17,570,904 in 1973. Britain possessed, in 1972, 17,510,632 TVs
and 17,570,904 radios. Russia possessed, in 1972, 53,200,000 radios and
45,600,000 TVs.If we compare comparative figures with the USA, which has a population of 212
million against Russia's over 250 million, the position is even clearer. In 1972
the USSR possessed 80,600 million books of all kinds as against America's 36,038
million. Whereas America possessed in 1971 125,142,000 telephones, 336 million
radios and 93 million TVs. However in comparative figures, considering the
population of the Soviet Union, and from the point of view of standards of
living, Russia remains far behind America and even her Western European enemies.The rulers of the Soviet Union and the Communist Parties of the World
pretend, or claim, that Socialism has been established in Russia and China and
the countries of Eastern Europe. This is a flagrant distortion of the ideas and
theories and the aspirations of Marx, Engels, Lenin and Trotsky.An examination of the fundamental theories of Marx and Lenin and an
examination of Soviet reality (also the realities of all the countries claiming
to be Socialist, where the same conditions and methods prevail) show that the
claim is intended to deceive the workers of the whole world.The ideals of the leaders of the revolution, with which they won the support
of the Russian workers and peasants and carried the revolution victoriously to
the overthrow of Czarism and capitalism were based on internationalism, workers'
democracy and a movement towards socialism.Lenin, basing himself on the teachings of Marx, laid down certain fundamental
principles with which the revolution was to begin. The "dictatorship of the
proletariat" or in modern terms "workers' democracy" rested on
these principles:These were the principles with which the Bolsheviks began the revolution.
They had confidently expected that the victory of the Russian revolution would
be followed by the victory of the Socialist revolution by the workers of the
more advanced countries of the West.It is beyond the scope of this article to go into the details, but suffice it
to say that because of the policies of the leaders of the Labour Movement, the
socialist revolution in Western Europe and Britain did not succeed.The Russian revolution remained isolated. The country had been ruined by wars
of intervention of world capitalism. The working class was decimated and
exhausted. More and more, even in the days of Lenin and Trotsky, the millions of
officials of the state machine, party, army and managers began to usurp the
power and elbow the workers out.It was in this atmosphere that Stalin (after Lenin's death) put forward the
anti-Marxist, anti-Leninist theory of "Socialism in a single country",
Russia, in place of international Socialism.This reflected the interests of the bureaucracy. The Left Opposition of
Trotsky fought for a return to workers' democracy, international socialism and a
planned building of industry by 5 year plans.Because of the weariness of the working class and because of the failure of
the Socialist revolution internationally, the bureaucracy or the officialdom
emerged victorious in the struggle. The consequence was the creation of a
totalitarian state where every element of democracy within the Soviets, the
Party and in the country was suppressed. Following on this, not a single
condition laid down by Lenin for the beginning of the revolution has survived.After denying it for a generation, following the exposure by Khrushchev, the
Communist Parties of the world admitted "errors". Stalin, under the
"cult of personality" had conducted a reign of terror, murdering tens
and hundreds of thousands of old Bolsheviks and exterminating the whole of the
leadership of Lenin's co-thinkers and comrades in the struggle which had carried
through the revolution.The CP leaders tried to pass this off as the personal whims and peccadilloes
of the bloodthirsty tyrant Stalin. But neither the Russian nor the CP leaders
internationally explained how one man could have such power. Without the support
of the millions of officials, Stalin would have been a cipher. Dictatorship must
reflect the interests of a class or grouping within a class in society.Stalin reflected the interests of the Bureaucracy. Thus not a single one of
the conditions laid down by Lenin for the rule of the workers remained intact
under the terror of Stalin.The Soviets were abolished, though the name was retained. Instead of a
workers' democracy, a "Parliament" was introduced where only a single
candidate was allowed to stand. This was as "democratic" as Hitler's
Reichstag! What worker would tolerate only one candidate in elections for his
union branch, shop stewards committee, or national union?The law that no official was to receive a higher wage than a skilled worker
was abolished, and under Stalin, as today, the difference in wages (leaving
aside privileges which are much greater in Russia) between the Russian Prime
Minister and the ordinary workers is greater than in capitalist countries like
Britain. Differences in wages and conditions between managers, top state
officials, top party bureaucrats and ordinary workers have become greater then
even in capitalist countries!Instead of technical officers and generals, the social division between
soldiers and the new officer caste is now greater than in capitalist countries.
Differences in wages are greater. Officers have special quarters, batmen,
servants, clubs, and barbers, while there is "bull" for the squaddies,
worse than in capitalist countries. There is an irremovable bureaucracy with no
right of criticism or discussion from below. The state machine and state
apparatus have grown to gigantic proportions.It is true that the worst excesses of the Stalinist autocracy have been
eliminated. Millions in slave labour camps have been released. A measure of
reform from the top was intended to prevent the eruption of the workers and
peasants from below. But in fundamentals the counterrevolution carried out by
Stalin remains intact.This is a peculiar form of counter-revolution, in the sense that it is based
on the great conquests of the revolution: state ownership of the land, banks,
factories and the state monopoly of foreign trade. The income and privileges of
the bureaucracy, unlike those of capitalism which stem from private ownership of
the means of production, stem from state ownership. Consequently they are
compelled to defend these last remaining fundamental conquests of the
revolution, state ownership and a plan of production and the monopoly of foreign
trade.However the reasons for the victory of the bureaucracy in the past can only
be explained in Marxist terms and Marxist analysis. This was brilliantly shown
by Trotsky's book "The Revolution Betrayed" which remains as a
textbook for revolutionary workers who wish to understand the processes
unfolding in Russia in the past and manifesting themselves today.Marx explained that socialism requires a material basis in the existence of
the working class and the material elements of production - machinery, buildings
etc. "A development of the productive forces is the absolutely necessary,
practical premise (of Communism) because without it want is generalised and with
want the struggle for necessities begins again, and that means that all the old
crap must revive". (said Marx in his early work; The German Ideology).The backwardness of Russia resulted in the aberration of Stalinism and of the
rule of Brezhnev and of similar dictators in Eastern Europe and China.Engels in Anti-Dühring explains this just as clearly. He finds
the division into classes caused by the division of labour in society,
especially of mental and physical labour. So long, he says, as a privileged
minority control art, science and Government, so long will they use and abuse
their position in their own interests. And this must be so long as the mass of
the working class has to work long hours for the basic necessities of existence.
This must be so long as the material basis of production is weak. The great task
of capitalism was precisely the development of the forces of production.In a sense, this task was taken over by the privileged bureaucracy in Russia.
But they do this at a price and in their own interests. But because of the basis
of the revolution, which is in contradiction to privilege, they can only
maintain this by terrible repression.Even today there are still thousands imprisoned in concentration camps. The
flight of Jews from Russia is an indication of intolerable pressures. Repression
of national rights of the many peoples of the USSR - nightmarish under Stalin
with the banishment of whole peoples to Siberia - still continues under his
successors.Even today the "Byzantine splendour" of Stalin's court is
continued. Brezhnev has a special swimming pool, which is worth tens of
thousands of pounds, in his basement! The top bureaucrats, abandoning the
simplicity of Lenin and Trotsky, live like millionaires.What is worse is that when it was a question of a primitive society,
bureaucratic methods, even at great cost, could succeed. Now, with a developed
Soviet Union - at the cost of enormous sacrifices by the workers and peasants -
the bureaucracy wastes a large part of the surplus produced by the workers.State ownership requires workers' democracy if it is to be run efficiently.
Waste, corruption, mismanagement, nepotism and chaos inevitably flow from
arbitrary rule. Occasionally the bureaucracy is compelled to make an example of
some of the lower officialdom. Sometimes even higher officials are used as
scapegoats for the bureaucratic system as a whole.Thus madam Furtseva, the Minister for Culture was exposed for
"unculturally" using state materials to build a sumptuous mansion for
herself. Heads of trusts have been sentenced to long terms of imprisonment for
embezzlement of fantastic sums and materials.Despite all the sacrifices, and despite the achievements, the idea that
Russia can and has built socialism independently of the fate of the rest of the
world, has been demonstrated as false.The Nazis destroyed decades of labour in a few years. Now, with the
development of the H-Bomb and rockets, Russia could be destroyed totally in a
matter of minutes. (It is no consolation that this would mean also, as a
reprisal, the annihilation of America and the rest of the world). Thus the fate
of the Soviet Union, like every other country, is bound to the fate of the world
working class.At the same time, the entire population groans under the burden of a
dictatorial system. The excuses of the past for this repression no longer have a
basis in fact. The old ruling class is now dead and its descendants who remained
in Russia have been absorbed into the bureaucracy.Thus the dictatorship and complete loss of rights, including Soviets, genuine
trade unions, the right to strike and organise, and the rights of free speech
can only be aimed at the working class which today is the majority of the
population. Thus the imprisonment and exile of the dissidents, including many
thousands of workers, the expulsion of the reactionary Solzhenitsyn from Russia
and the repression of even reactionaries is entirely unnecessary.The Marxist Movement in Russia is putting forward demands for a return to the
principles of Lenin listed previously. Now because of the enormous industrial
progress we can extend these demands to free speech and democracy for all.
Advocates of a return to capitalism would be laughed at. The totalitarian system
is retained purely as a defence for the privileges, power, prestige and income
of the bureaucratic upper crust.The bureaucracy, by the very development of industry has been faced to
abandon "autarchy" and participate on the world market. On a higher
economic level they are offering joint exploitation (under Russian control) with
the capitalists of the big industrial nations, of the still largely unexploited
resources of Russia.Thus they collaborate with the capitalist powers. Détente between capitalist
America and bureaucratic Russia (in Trotsky's words, a deformed workers' state)
flows from their fear of Socialist revolution in the West. The victory of
workers' democracy in any big industrial state would undermine their power
completely. But the bureaucracy, like any other dictatorship will not surrender
their power and privileges until they are forced to do so.Thus the Hungarian revolution of 1956 shows the way events will develop in
Russia in the future. The Hungarian workers demanded an introduction of Lenin's
principles, plus the right freely to put forward the view of all workers'
parties. NO TOTALITARIAN RULE OF A SINGLE PARTY IN RUSSIA! The workers will
demand the right of all parties to put forward their point of view on radio and
TV and in the press according, and in proportion, to their support in the
population.Thus political, rather than social, revolution is inevitable in Russia. The
Russian workers, hand in hand with the workers of the world will then begin the
construction of socialism. 
Ted Grant Archive
 Soviets, or workers' committees, must have the power, with the right of
free election and the right of recall if the majority of the electors so decided
(similar to the position of shop stewards).No official to have a higher wage than a skilled worker.No standing army but an armed people.No permanent bureaucracy. All jobs of government should be reduced to
accounting and control. All jobs at the top should gradually become rotated.
"Every cook should be able to be Prime Minister."No huge state apparatus but a semi-state. As society progressed towards
Socialism, the apparatus of repression would be dissolved into society.