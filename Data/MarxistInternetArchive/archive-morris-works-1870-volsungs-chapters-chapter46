
     Sigurd of yore,  
     Sought the dwelling of Giuki,  
     As he fared, the young Volsung,  
     After fight won;  
     Troth he took    
     From the two brethren;  
     Oath swore they betwixt them,  
     Those bold ones of deed.  

     A may they gave to him  
     And wealth manifold,  
     Gudrun the young,  
     Giuki's daughter:  
     They drank and gave doom  
     Many days together,  
     Sigurd the young,  
     And the sons of Giuki.  

     Until they wended  
     For Brynhild's wooing,  
     Sigurd a-riding  
     Amidst their rout;  
     The wise young Volsung  
     Who knew of all ways --  
     Ah!  He had wed her,  
     Had fate so willed it.  

     Southlander Sigurd  
     A naked sword,  
     Bright, well grinded,  
     Laid betwixt them;  
     No kiss he won  
     From the fair woman,  
     Nor in arms of his  
     Did the Hun King hold her,  
     Since he gat the young maid  
     For the son of Giuki.  

     No lack in her life  
     She wotted of now,  
     And at her death-day  
     No dreadful thing  
     For a shame indeed  
     Or a shame in seeming;  
     But about and betwixt  
     Went baleful fate.  

     Alone, abroad,  
     She sat of an evening,  
     Of full many things  
     She fall a-talking:  
     "O for my Sigurd!  
     I shall have death,  
     Or my fair, my lovely,  
     Laid in mine arms.  

     "For the word once spoken,  
     I sorrow sorely --  
     His queen is Gudrun,  
     I am wed to Gunnar;  
     The dread Norns wrought for us  
     A long while of woe."  

     Oft with heart deep  
     In dreadful thoughts,  
     O'er ice-fields and ice-hills  
     She fared a-night time,  
     When he and Gudrun  
     Were gone to their fair bed,  
     And Sigurd wrapped  
     The bed-gear round her.  

     "Ah!  Now the Hun King  
     His queen in arms holdeth,  
     While love I go lacking,  
     And all things longed for  
     With no delight  
     But in dreadful thought."  

     These dreadful things  
     Thrust her toward murder:  
     -- "Listen, Gunnar,  
     For thou shalt lose  
     My wide lands,  
     Yea, me myself!  
     Never love I my life,  
     With thee for my lord --  

     "I will fare back thither  
     From whence I came,  
     To my nighest kin  
     And those that know me  
     There shall I sit  
     Sleeping my life away,  
     Unless thou slayest  
     Sigurd the Hun King,  
     Making thy might more  
     E'en than his might was!  

     "Yea, let the son fare  
     After the father,  
     And no young wolf  
     A long while nourish!  
     For on earth man lieth  
     Vengeance lighter,  
     And peace shall be surer  
     If the son live not."  

     Adrad was Gunnar,  
     Heavy-hearted was he,  
     And in doubtful mood  
     Day-long he sat.  
     For naught he wotted,  
     Nor might see clearly  
     What was the seemliest  
     Of deeds to set hand to;  
     What of all deeds  
     Was best to be done:  
     For he minded the vows  
     Sworn to the Volsung,  
     And the sore wrong  
     To be wrought against Sigurd.  

     Wavered his mind  
     A weary while,  
     No wont it was  
     Of those days worn by,  
     That queens should flee  
     From the realms of their kings.  

     "Brynhild to me  
     Is better than all,  
     The child of Budli  
     Is the best of women.  
     Yea, and my life  
     Will I lay down,  
     Ere I am twinned  
     From that woman's treasure."  

     He bade call Hogni  
     To the place where he bided;  
     With all the trust that might be,  
     Trowed he in him.  

     "Wilt thou bewray Sigurd  
     For his wealth's sake?  
     Good it is to rule  
     O'er the Rhine's metal;  
     And well content  
     Great wealth to wield,  
     Biding in peace  
     And blissful days."  

     One thing alone Hogni  
     Had for an answer:  
     "Such doings for us  
     Are naught seemly to do;  
     To rend with sword  
     Oaths once sworn,  
     Oaths once sworn,  
     And troth once plighted.  

     "Nor know we on mould,  
     Men of happier days,  
     The while we four  
     Rule over the folk;  
     While the bold in battle,  
     The Hun King, bides living.  

     "And no nobler kin  
     Shall be known afield,  
     If our five sons  
     We long may foster;  
     Yea, a goodly stem  
     Shall surely wax.  
     -- But I clearly see  
     In what wise it standeth,  
     Brynhild's sore urging  
     O'ermuch on thee beareth.  

     "Guttorm shall we  
     Get for the slaying,  
     Our younger brother  
     Bare of wisdom;  
     For he was out of  
     All the oaths sworn,  
     All the oaths sworn,  
     And the plighted troth."  

     Easy to rouse him  
     Who of naught recketh!  
     -- Deep stood the sword  
     In the heart of Sigurd.  

     There, in the hall,  
     Gat the high-hearted vengeance;  
     For he can his sword  
     At the reckless slayer:  
     Out at Guttorm  
     Flew Gram the mighty,  
     The gleaming steel  
     From Sigurd's hand.  

     Down fell the slayer  
     Smitten asunder;  
     The heavy head  
     And the hands fell one way,  
     But the feet and such like  
     Aback where they stood.  

     Gudrun was sleeping  
     Soft in the bed,  
     Empty of sorrow  
     By the side of Sigurd:  
     When she awoke  
     With all pleasure gone,  
     Swimming in blood  
     Of Frey's beloved.  

     So sore her hands  
     She smote together,  
     That the great-hearted  
     Gat raised in bed;  
     -- "O Gudrun, weep not  
     So woefully,  
     Sweet lovely bride,  
     For thy brethren live for thee!  

     "A young child have I  
     For heritor;  
     Too young to win forth  
     From the house of his foes. --  
     Black deeds and ill  
     Have they been a-doing,  
     Evil rede  
     Have they wrought at last.  

     "Late, late, rideth with them  
     Unto the Thing,  
     Such sister's son,  
     Though seven thou bear, --  
     -- But well I wot  
     Which way all goeth;  
     Alone wrought Brynhild  
     This bale against us.  

     "That maiden loved me  
     Far before all men,  
     Yet wrong to Gunnar  
     I never wrought;  
     Brotherhood I heeded  
     And all bounden oaths,  
     That none should deem me  
     His queen's darling."  

     Weary sighed Gudrun,  
     As the king gat ending,  
     And so sore her hands  
     She smote together,  
     That the cups arow  
     Rang out therewith,  
     And the geese cried on high  
     That were in the homefield.  

     Then laughed Brynhild  
     Budli's daughter,  
     Once, once only,  
     From out her heart;  
     When to her bed  
     Was borne the sound  
     Of the sore greeting  
     Of Giuki's daughter.  

     Then, quoth Gunnar,  
     The king, the hawk-bearer,  
     "Whereas, thou laughest,  
     O hateful woman,  
     Glad on thy bed,  
     No good it betokeneth:  
     Why lackest thou else  
     Thy lovely hue?  
     Feeder of foul deeds,  
     Fey do I deem thee,  

     "Well worthy art thou  
     Before all women,  
     That thine eyes should see  
     Atli slain of us;  
     That thy brother's wounds  
     Thou shouldest see a-bleeding,  
     That his bloody hurts  
     Thine hands should bind."  

     "No man blameth thee, Gunnar,  
     Thou hast fulfilled death's measure  
     But naught Atli feareth  
     All thine ill will;  
     Life shall he lay down  
     Later than ye,  
     And still bear more might  
     Aloft than thy might.  

     "I shall tell thee, Gunnar,  
     Though well the tale thou knowest,  
     In what early days  
     Ye dealt abroad your wrong:  
     Young was I then,  
     Worn with no woe,  
     Good wealth I had  
     In the house of my brother!  

     "No mind had I  
     That a man should have me,  
     Or ever ye Giukings,  
     Rode into our garth;  
     There ye sat on your steeds  
     Three kings of the people --  
     -- Ah!  That that faring  
     Had never befallen!  

     "Then spake Atli  
     To me apart,  
     And said that no wealth  
     He would give unto me,  
     Neither gold nor lands  
     If I would not be wedded;  
     Nay, and no part  
     Of the wealth apportioned,  
     Which in my first days  
     He gave me duly;  
     Which in my first days  
     He counted down.  

     "Wavered the mind  
     Within me then,  
     If to fight I should fall  
     And the felling of folk,  
     Bold in Byrny  
     Because of my brother;  
     A deed of fame  
     Had that been to all folk,  
     But to many a man  
     Sorrow of mind.  

     "So I let all sink  
     Into peace at the last:  
     More grew I minded  
     For the mighty treasure,  
     The red-shining rings  
     Of Sigmund's son;  
     For no man's wealth else  
     Would I take unto me.  

     "For myself had I given  
     To that great king  
     Who sat amid gold  
     On the back of Grani;  
     Nought were his eyes  
     Like to your eyen,  
     Nor in any wise  
     Went his visage with yours;  
     Though ye might deem you  
     Due kings of men.  

     "One I loved,  
     One, and none other,  
     The gold-decked may  
     Had no doubtful mind;  
     Thereof shall Atli  
     Wot full surely,  
     When he getteth to know  
     I am gone to the dead.  

     "Far be it from me,  
     Feeble and wavering,  
     Ever to love  
     Another's love --  
     -- Yes shall my woe  
     Be well avenged."  

     Up rose Gunnar,  
     The great men's leader,  
     And cast his arms  
     About the queen's neck;  
     And all went nigh  
     One after other,  
     With their whole hearts  
     Her heart to turn.  

     But then all these  
     From her neck she thrust,  
     Of her long journey  
     No man should let her.  

     Then called he Hogni  
     To have talk with him;  
     "Let all folk go  
     Forth into the hall,  
     Thine with mine --  
     -- O need sore and mighty! --  
     To wot if we yet  
     My wife's parting may stay.  
     Till with time's wearing  
     Some hindrance wax."  

     One answer Hogni  
     Had for all;  
     "Nay, let hard need  
     Have rule thereover,  
     And no man let her  
     Of her long journey!  
     Never born again,  
     May she come back thence!  

     "Luckless she came  
     To the lap of her mother,  
     Born into the world  
     For utter woe,  
     TO many a man  
     For heart-whole mourning."  

     Upraised he turned  
     From the talk and the trouble,  
     To where the gem-field  
     Dealt out goodly treasure;  
     As she looked and beheld  
     All the wealth that she had,  
     And the hungry bondmaids,  
     And maids of the hall.  

     With no good in her heart  
     She donned her gold byrny,  
     Ere she thrust the sword point  
     Through the midst of her body:  
     On the boister's far side  
     Sank she adown,  
     And, smitten with sword,  
     Still bethought her of redes.  

     "Let all come forth  
     Who are fain the red gold,  
     Or things less worthy  
     To win from my hands;  
     To each one I give  
     A necklace gilt over,  
     Wrought hangings and bed=gear,  
     And bright woven weed."  

     All they kept silence,  
     And thought what to speak,  
     Then all at once  
     Answer gave:  
     "Full enow are death-doomed,  
     Fain are we to live yet,  
     Maids of the hall  
     All meet work winning."  

     "From her wise heart at last  
     The linen-clad damsel,  
     The one of few years  
     Gave forth the word:  
     "I will that none driven  
     By hand or by word,  
     For our sake should lose  
     Well-loved life.  

     "Thou on the bones of you  
     Surely shall burn,  
     Less dear treasure  
     At your departing  

     Nor with Menia's Meal 

(1)

     Shall ye come to see me."  

     "Sit thee down, Gunnar,  
     A word must I say to thee  
     Of the life's ruin  
     Of thy lightsome bride --  
     -- Nor shall thy ship  
     Swim soft and sweetly  
     For all that I  
     Lay life adown.  

     "Sooner than ye might deem  
     Shall ye make peace with Gudrun,  
     For the wise woman  
     Shall full in the young wife  
     The hard memory  
     Of her dead husband.  

     "There is a may born  
     Reared by her mother,  
     Whiter and brighter  
     Than is the bright day;  
     She shall be Swanhild,  
     She shall be Sunbeam.  

     "Thou shalt give Gudrun  
     Unto a great one,  
     Noble, well-praised  
     Of the world's folk;  
     Not with her goodwill,  
     Or love shalt thou give her;  
     Yet will Atli  
     Come to win her,  
     My very brother,  
     Born of Budli.  

     -- "Ah!  Many a memory  
     Of how ye dealt with me,  
     How sorely, how evilly  
     Ye ever beguiled me,  
     How all pleasure left me  
     The while my life lasted! --  

     "Fain wilt thou be  
     Oddrun to win,  
     But thy good liking  
     Shall Atli let;  
     But in secret wise  
     Shall ye win together,  
     And she shall love thee  
     As I had loved thee,  
     If in such wise  
     Fare had willed it.  

     "But with all ill  
     Shall Atli sting thee,  
     Into the strait worm-close  
     Shall he cast thee.  

     "But no long space  
     Shall slip away  
     Ere Atli too  
     All life shall lose,  
     Yea, all his weal  
     With the life of his sons,  
     For a dreadful bed  
     Dights Gudrun for him,  
     From a heart sore laden,  
     With the sword's sharp edge.  

     "More seemly for Gudrun,  
     Your very sister,  
     In death to wend after  
     Her love first wed;  
     Had but good rede  
     To her been given,  
     Or if her heart  
     Had been like to my heart.  

     -- "Faint my speech groweth --  
     But for our sake  
     Ne'er shall she lose  
     Her life beloved;  
     The sea shall have her,  
     High billows bear her  
     Forth unto Jonakr's  
     Fair land of his fathers.  

     "There shall she bear sons,  
     Stays of a heritage,  
     Stays of a heritage,  
     Jonakr's sons;  
     And Swanhild shall she  
     Send from the land,  
     That may born of her,  
     The may born of Sigurd.  

     "Her shall bite  
     The rede of Bikki,  
     Whereas for no good  
     Wins Jormunrek life;  
     And so is clean perished  
     All the kin of Sigurd,  
     Yea, and more greeting,  
     And more for Gudrun.  

     "And now one prayer  
     Yet pray I of thee --  
     That last word of mine  
     Here in the world --  
     So broad on the field  
     Be the burg of the dead  
     That fair space may be left  
     For us all to lie down,  
     All those that died  
     At Sigurd's death!  

     "Hang round that burg  
     Fair hangings and shields,  
     Web by Gauls woven,  
     And folk of the Gauls:  
     There burn the Hun King  
     Lying beside me.  

     "But on the other side  
     Burn by the Hun King  
     Those who served me  
     Strewn with treasure;  
     Two at the head,  
     And two at the feet,  
     Two hounds therewith,  
     And two hawks moreover:  
     Then is all dealt  
     With even dealing.  

     "Lay there amidst us  
     The right-dight metal,  
     The sharp-edged steel,  
     That so lay erst;  
     When we both together  
     Into one bed went,  
     And were called by the name  
     Of man and wife.  

     "Never, then, belike  
     Shall clash behind him  
     Valhall's bright door  
     With rings bedight:  
     And if my fellowship  
     Followeth after,  
     In no wretched wise  
     Then shall we wend.  

     "For him shall follow  
     My five bondmaids,  
     My eight bondsmen,  
     No borel folk:  
     Yea, and my fosterer,  
     And my father's dower  
     That Budli of old days  
     Gave to his dear child.  

     "Much have I spoken,  
     More would I speak,  
     If the sword would give me  
     Space for speech;  
     But my words are waning,  
     My wounds are swelling --  
     Naught but truth have I told --  
     -- And now make I ending."  


ENDNOTES: 
(1)  "Menia's Maid"
-- periphrasis for gold. The Story of the Volsungs: Next ChapterThe Story of the Volsungs: Previous ChapterThe Story of the Volsungs: IndexThe William Morris Internet Archive : Works