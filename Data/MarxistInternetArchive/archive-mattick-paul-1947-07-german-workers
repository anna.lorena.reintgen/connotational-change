Paul Mattick 1947Source: Western Socialist, Boston, USA, July 1947;
Transcribed: by Adam Buick.
Hammer or Anvil. The Story of the German Working-Class Movement. By Evelyn Anderson (207pp.; V. Gollancz, London).This short history of the German labor movement from the time of
Bismarck’s anti-socialist laws to its extinction under the Hitler regime
deals with both the political and trade-union aspects of the movement and is
written from the same point of view that prevailed in those organizations.
There is little criticism and what there is is directed only to the late phases
of the movement. Some errors of fact appear here and there with regard to
issues that are of no real importance. For instance, Liebknecht is said to have
been the only member of the Reichstag who voted against war credits when the
fact is that Otto Rühle voted with Liebknecht. At times the author should
have been more specific; instead of employing such terms as “a
handful” of German syndicalists, he should have quoted numbers. Shortly
after the first world war the German syndicalists totalled 200,000.All in all, the author tries his best to be objective. But the German labor
movement is considered only in so far as it expressed itself in political
controversy and programmatic proclamations. The less obvious reasons for the
behavior of the German working class (which must be sought not in German but in
capitalist world politics) are not taken into account. Anderson speaks, for
instance, of “Russian February” followed by “Red
October” and says that the “German Revolution just fizzled
out.” From the vantage-point of the Second World War it should be clear,
however, that Germany’s “Red October” came in with the Nazi
regime, not ideologically, of course, but with regard to the essentials of
socio-economic life. If Bolshevism is to be considered a “labor
movement” so should Nazism. Both followed the steps of the social-reform
period of the traditional labor movement. But the author, still bound by
traditional reformist ideology, is unable to see that carrying out the
“ideals” of the past means fascism or bolshevism or whatever else
these conditions may come to be called in other nations.To be sure, the author does put some responsibility for the rise of Nazism
on world capitalism, but not enough. To make it clear that world capitalism
brought fascism to Germany and elsewhere, and keeps it there, (albeit with
changing colors) is but to state the facts of world-wide economy and world
politics. The effect of the Great Depression on the rise of Nazism is also not
sufficiently emphasized in this book. On the other hand, the German
workers’ opposition to Hitler and his regime is over-emphasized, much of
the German “anti-fascist” opposition was merely a competitive
struggle between different labor manipulators or imperialistic rivalries
dressed up in the language of social controversy. That the anti-fascists of
yesterday show up as fascists of today indicates that a mere
“anti-fascist” struggle does not restore the “honor” of
the working class but is a senseless fight having for its objective the
substitution of terrorists. An anti-fascist struggle is real only if it
develops into a socialist movement that ends all terrorism and
exploitation. Mere “anti-fascists” from Stalin to Bevin are only
continuing and expanding Hitler’s work. 
Paul Mattick Archive
