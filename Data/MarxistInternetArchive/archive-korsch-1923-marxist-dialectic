Karl Korsch 1923First published: in Imprekorr, 1923
Translated by Karl-Heinz Otto
Source: Class Against Class
Transcribed: by Zdravko Saveski, for marxists.org 2009.The immense significance of Marx's theoretical achievement for
the practice of proletarian class struggle is that he concisely fused
together for the first time the total content of those new viewpoints
transgressing bourgeois horizons, and that he also formally
conceptualized them into a solid unity, into the living totality of a
scientific system. These new ideas arose by necessity in the
consciousness of the proletarian class from its social conditions. Karl
Marx did not create the proletarian class movement (as some bourgeois
devil-worshippers imagine in all seriousness). Nor did he create
proletarian class consciousness. Rather, he created the
theoretical-scientific expression adequate to the new content of
consciousness of the proletarian class, and thereby at the same time
elevated this proletarian class consciousness to a higher level of its
being.The transformation of the "natural" class viewpoint of the
proletariat into theoretical concepts and propositions, and the
powerful synthesis of all these theoretical propositions into the
system of "scientific socialism" is not to be regarded as a mere
passive "reflex" of the real historical movement of the proletariat. On
the contrary, this transformation forms an essential component of the
real historical process. The historical movement of the proletariat
could neither become "independent" nor "unified" without the
development of an independent and unified proletarian class
consciousness. Just as the politically and economically mature,
nationally and internationally organized proletarian class movement
distinguishes itself from the, at first, dispersed and unorganized
stirrings and spasms of the proletariat, so too "scientific socialism"
distinguishes itself as the "organized class consciousness" of the
proletariat from those dispersed and formless feelings and views in
which proletarian class consciousness finds its first immature
expression. Therefore, from a practical point of view, the theoretical
evolution of socialism towards a science, as expressed by Karl Marx in
the Communist Manifesto and in Capital, appears as a quite necessary
element within that great historical developmental process in which the
proletarian class movement gradually moved away from the bourgeois
revolutionary movement of the "third estate" and constituted itself as
an independent and unified class. Only by taking the form of a strict
"science" could this complex of proletarian class views, contained in
"modern socialism," radically purify itself from the bourgeois views
with which from its origin it was inextricably connected. And only by
becoming a "science" could socialism actually fulfill the task which
Karl Marx and Frederick Engels had set for it: to be the "theoretical
expression" of revolutionary proletarian class action which is to
ascertain the historical conditions and nature of this revolutionary
proletarian class action, thereby "bringing that class which is called
to action, and is today suppressed, to a consciousness of the
conditions and nature of its own action."While in the foregoing exposition we have characterized the
practical meaning of the scientific
form of modern or Marxian socialism we have at the same
time also described the meaning of the dialectical method
which Karl Marx applied. For as certainly as the content of scientific
Socialism was in existence as an unformed viewpoint (proletarian class
viewpoint) before its scientific formulation, just as certainly is the
scientific form in which this content lies before us in the works of
Marx and Engels. Thus "scientific socialism" properly so-called is
quite essentially the product of the application of that mode of
thought which Marx and Engels designated as their "dialectical method."
And it is not the case, as some contemporary "Marxists" might like to
imagine, that by virtue of historical accident those scientific
propositions which Karl Marx produced by the application of his
"dialectical method" could today be separated at will from that method
and simply reproduced. Nor is it the case that this method is out of
date because of the progress of the sciences. Nor is its replacement by
another method today not only possible but rather even necessary!
Whoever speaks in these terms has not comprehended the most important
aspects of the Marxist dialectic. How could one otherwise come to the
thought that today-as at a time of increased class struggle in all
spheres of social, thus also so-called intellectual, life -that method
could be abandoned "which is intrinsically critical and revolutionary."
Karl Marx and Frederick Engels simultaneously opposed the new method of
proletarian science to the "metaphysical mode of thought" ("that
specific weakness of thought of the last century") and to all earlier
forms of "dialectic" (in particular the idealistic dialectic of
Fichte-Schelling-Hegel).Only those who completely overlook that Marx's "proletarian
dialectic" differs essentially from every other (metaphysical and dialectical)
mode of thought, and represents that specific mode of thought in which
alone the new content of the proletarian class views formed in the
proletarian class struggle can find a theoretical-scientific expression
corresponding to its true being; only those could get the idea that
this dialectical mode of thought, as it represents "only the form" of
scientific socialism, consequently would also be "something peripheral
and indifferent to the matter," so much so that the same material
content of thought could be as well or even better expressed in another
form. It is something quite similar when certain contemporary
"Marxists" put forward the notion that the proletariat could wage its
practical struggle against the bourgeois economic, social and political
order in other "forms" than the barbaric uncivilized form of
revolutionary class struggle. Or when the same people fool themselves
and others by saying that the proletariat could achieve its positive
task, the realization of Communist society, by means other than the
dictatorship of the proletariat, for example, by means of the bourgeois
state and bourgeois democracy. Karl Marx, who already in an early work
had written the proposition, "Form has no value if it is not the form
of its content," himself thought about these things quite differently.
Later Marx always emphasized anew that the real understanding of
historico-social development (i.e., consciously revolutionary
understanding that is at the same time positive and negative) -this
understanding, which constituted the specific essence of "scientific"
socialism, can only be brought about by the conscious application of
the dialectical method. Of course, this new, or "proletarian,"
dialectic on which the scientific form of Marxism is founded differs in
the extreme, not only from the ordinary, narrow-minded metaphysical way
of thinking. For, it is also "quite different" in its fundamental
position from the bourgeois dialectic which found its most
comprehensive form in the German philosopher Hegel, and in a definite
sense it is even its "direct opposite." It is impracticable and
superfluous at this point to enter more deeply into the manifold
consequences of these differences and contrasts.It is sufficient for our purposes that these differences and
contrasts that we have pointed out lead us back without exception to
Marx's "proletarian" dialectic as just that form in which the
revolutionary class movement of the proletariat finds its appropriate
theoretical expression. If one has understood this, or has just the
faintest notion of the connection, one can comprehend immediately a
whole series of phenomena otherwise difficult to grasp. One understands
why the bourgeoisie of today has so completely forgotten the times when
it had to fight as the "third estate" a tough and heroically
ever-increasing class struggle against the feudal economic order and
its political-ideological superstructure (aristocracy and church), and
when its spokesman, the Abbe Sieyes, hurled against the ruling social
order the quite "dialectical" outburst: "What is the third estate?
Everything. What is it in the existing order? Nothing. What does it
demand? To be something." Since the feudal state has fallen and the
bourgeois class has become not only something in the bourgeois state,
but everything, there are only two positions in question on the problem
of dialectics for the bourgeoisie today. Either the dialectic is a
standpoint today completely out-of-date, only historically respectable
as a kind of lofty madness of philosophical thought transcending its
natural barriers, to which a realistic man and good burgher ought under
no circumstances be a party. Or the dialectical movement must even
today, and for all the future, make a halt at that absolute end point
at which the last revolutionary philosopher of the bourgeois class, the
philosopher Hegel, once made it come to halt. It must in its concepts
not cross those borders which bourgeois society likewise cannot cross
without negating itself. Its last word, the great all-embracing
synthesis, in which all opposites are dissolved, or can be dissolved,
is the state. Opposite this bourgeois state, which in its complete
development exemplifies the complete fulfillment of all bourgeois
interests and is therefore also the final goal of the bourgeois class
struggle, there is consequently no other dialectical antithesis to
bourgeois consciousness, no irreconcilable opposite. Whoever may yet
oppose this absolute fulfillment of the bourgeois idea in practice and
theory departs from the hallowed circle of the bourgeois world; he puts
himself outside bourgeois law, outside bourgeois freedom and bourgeois
peace, and therefore also outside of all bourgeois philosophy and
science. One understands why as far as concerns this bourgeois
standpoint, which ordains contemporary bourgeois society as the sole
thinkable and possible form of social life for humanity, the "idealist
dialectic" of Hegel, which finds its ideal conclusion in the idea of
the bourgeois state, must be the only possible and thinkable form of
dialectic. Yet likewise, and understandably so, this "idealist
dialectic" of the bourgeoisie is no longer of value to that other class
within contemporary bourgeois society which is driven directly to
rebellion against this whole bourgeois world and its bourgeois state by
"absolutely compelling need which can no longer be denied or
disguised-the practical expression of necessity." In its whole material
conditions of life, in its whole material being, this class already
truly expresses the formal antithesis, the absolute opposition to this
bourgeois society and its bourgeois state. For this class, created
within bourgeois society through the inner mechanism of development of
private property itself, "through an independent and unconscious
development by the very nature of the matter proceeded against its
will" -for this class, the revolutionary aim and actions are "obviously
and irrevocably indicated by its own conditions of life as well as by
the whole organization of contemporary bourgeois society." The value of
a new revolutionary dialectic that is no longer bourgeois-idealist, but
is rather proletarian-materialist follows therefore with equal
necessity from this social life-situation. Because the "idealist
dialectic" of the bourgeoisie transcends the material opposites of
"wealth" and "poverty" existing in bourgeois society only "in the
idea," namely in the idea of a pure, democratic, bourgeois state, these
"ideally" transcended oppositions continue to exist unresolved in
"material" social reality where they even continually increase in
extent and severity. In contrast thereto stands the essence of the new
"materialist dialectic" of the proletariat which really abolishes the
material opposition between bourgeois wealth ("capital") and
proletarian misery through the supersession of this bourgeois class
society and its bourgeois class state by the material reality of the
classless Communist society. The materialist dialectic therefore forms
the necessary methodological foundation for "scientific socialism" as
the "theoretical expression" of the proletarian class's historical
struggle for liberation.Karl Korsch Archive