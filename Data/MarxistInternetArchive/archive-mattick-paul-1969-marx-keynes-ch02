The Limits of the Mixed Economy. Paul Mattick 1969It is rather difficult to regard the theories of Keynes as a
“revolution” in economic thought. However, the term may be used at
will, and the Keynesian theory is called a revolutionary doctrine “in the
sense that it produces theoretical results entirely different from the body of
economic thought existing at the time of its development.”[1] Yet since that
“body of thought,” was neo-classical equilibrium theory,
Keynes’ “revolt” may better be regarded as a partial return
to classical theory. And this notwithstanding Keynes’ own opposition to
classical theory, which in his strange definition, included the whole body of
economic thought from Ricardo down to his own contemporaries.Although Keynes regarded himself as an anti-Ricardian, his critics saw, of
course, that he tried “to arrive at economic truth in the manner of
Ricardo and his followers”[2] through his analysis in terms of economic
aggregates. His friends concluded that, because of Keynes, “the study of
economic aggregates has taken its place in the centre of economic science, and
can never again be pushed to the periphery where pre-Keynesian economists left
it – one does not undiscovered America.”[3] But Keynes was no Columbus, for the
concept of economic aggregates dates back two hundred years to Quesnay’s
Tableau économique, to Ricardo and to Marx.It was Keynes’ rejection of Say’s “law of the
market” which lent his theory the connotation,
“revolutionary.” Almost seventy-five years earlier, Marx had
pointed out that only an accelerated capital expansion allows for an increase
of employment. The “dull and comical ‘prince à la
science,’ J. B. Say,” Marx did not find worth overthrowing, even
though “his continental admirers have trumpeted him as the man who
unearthed the treasure of the metaphysical balance of purchases and
sales.”[4] For
Marx, Say’s law of the market was sheer nonsense in view of the growing
discrepancy between the profit needs of capital expansion and the
rationally-considered productive requirements of society, between the social
demand in capitalism and the actual social needs; and he pointed out that
capital accumulation implies an industrial reserve army of unemployed.There is a necessary connection between Marx and Keynes. Marx anticipated
Keynes’ criticism of the neo-classical theory through his own criticism
of classical theory; and both men recognized the capitalist dilemma in a
declining rate of capital formation. But while Keynes diagnosed its cause as a
lack of incentive to invest, Marx traced the dilemma to its final base, to the
character of production as production of capital. It is rather
astonishing, then, to find Keynes relegating Marx to the “underworld of
economic thought together with Silvio Gesell and Major Douglas.”[5] Though he was ready to
learn from the “underworld,” as is demonstrated by his affinity
with the ideas of Gesell, Keynes held “that the future will learn more
from the spirit of Gesell than that of Marx.” He thought so, he said,
because, unlike Gesell, Marx based his theories “on an acceptance of the
classical hypothesis, and on an unfettering of competition instead of its
abolition.”[6]Even a superficial study of Capital would have shown Keynes
that Marx’s theories, which he considered “illogical, obsolete,
scientifically erroneous, and without interest or application to the modern
world,”[7] led to
conclusions often quite similar to those that constitute the
“revolutionary” content of his own reasoning. He did not study Marx
seriously because he identified Marx’s theories with those of the
classicists. In a letter to G. B. Shaw, Keynes related that he “made
another shot at old Karl Marx ... reading the Marx-Engels
Correspondence,” but he still failed to discover anything “but
out-of-date controversialzing.” He also told Shaw that he himself is
“writing a book on economic theory which, will largely revolutionize
– not at once but in the course of the next ten years – the way the
world thinks about economic problems. There will be a great change, and, in
particular, the Ricardian foundations of Marxism will be knocked
away.”[8] By
opposing the “classical theory,” Keynes thought he was opposing
Marxism as well. [9] In
reality, however, he dealt with neither of these theories, but struck at the
neo-classical market theory which no longer had any significant connection with
the ideas of Ricardo.Keynes preferred Gesell to Marx because he favored economic policies,
particularly in the monetary and fiscal fields, which he thought capable of
alleviating the economic ills of capitalism without altering its basic social
structure. Marx, though dealing with monetary questions exhaustively,
emphasized the extra-monetary aspects of the economy. In his view, money
questions could be understood only in the light of the capitalist relations of
production, which are relations “based on the class distinction between
buyers and sellers of labor power. It is not money which by its nature creates
this relation; it is rather the existence of this relation which permits of the
transformation of a mere money-function into a capital function.”[10] And only in this
latter sense is it of contemporary interest.According to Marx, money is important not as a measure of value and a medium
of exchange, but because it is the “independent form of the existence of
exchange-value.” In the capitalist circulation process, value assumes at
one time the form of money and at another that of other commodities. In the
form of money it preserves and expands itself. The market economy and capital
accumulation are beset with difficulties which appear as monetary troubles. The
buying and selling process itself, by providing money with two different
functions, contains a crisis element, as the seller is not forced to buy but
may retain his wealth in money form. An existing quantity of money, if not
large enough to serve as additional capital, may necessitate a period of
hoarding, which may also constitute an element of crisis. A relative lack as
well as a relative abundance of capital may lead to economic difficulties which
will appear as a crisis of the money system.The need to amass money by hoarding in order to accumulate it as productive
capital was largely eliminated by the development of the banking and credit
system. The pooling of money resources helped extend industrial and commercial
operations. The increasingly more speculative character of capital production
enhanced the irrational aspects of capital competition by producing
mal-investments and over-investments. Of course, these activities were not
considered “speculative” in a derogatory sense,[11] as it was the
presumed function of financial capital to “anticipate” further
development and to “create” the conditions for an accelerated
capital formation. There can exist, however, a strictly monetary crisis due to
the relatively independent movement of money in the form of finance capital.
Accordingly, Keynes distinguished between “finance” and
“industry,” favoring the latter and defining the former as the
business of the money-market, speculation, stock-exchange activities, and the
financing of production. Although he held that “speculators may do no
harm as bubbles on a steady stream of enterprise,” he found the situation
“serious when enterprise becomes the bubble on a whirlpool of
speculation.”[12]This distinction between “industry” and “finance,”
between “productive” and “parasitical” capital is as
old as capitalism itself and gave rise to a pseudo-struggle against
“interest-slavery” and irresponsible speculators. This strictly
intra-capitalist affair is now largely a thing of the past, for the fusion of
industry and finance is so complete as to exclude a “moral”
distinction between them. But even previously, not only the financiers but all
capitalists saw production “merely as a necessary evil of
money-making.” And though profits arise out of the process of productions
attempts were always made “to make money without the mediation of the
process of production.”[13] Particularly during times of “idle”
capital and a slackening rate of investments, capitalists increase their
efforts to make money at the expense of other money- and title-holders by
financial manipulations and stock-market activities.Speculation may enhance crisis situations by permitting the fictitious
over-evaluation of capital, which then cannot satisfy the profit claims bound
up with it.[14] But
speculative “money-gains” represent so many
“money-losses”; unless speculation serves as an instrument of
capital concentration it represents only a redistribution of the available
exchange-value. The concentration of wealth is economically meaningless unless
it is accompanied by a reorganization of the capital structure which leads to
its further expansion.The division of surplus-value (profits) between “active” and
“inactive” capitalists, of which Keynes made so much, is for Marx
only a part of the general competition for the largest possible share of the
social surplus-value by all capitalists and all those living on the
surplus-product. He did not doubt that under definite conditions a lowering of
interest rates would affect investments’ positively. For if too much of
the realized profits goes into the hands of the money-lenders, entrepreneurs
will be less apt to expand production. But no generalization regarding the
behavior and the importance of the rate of interest can be based on this
possibility. High interest rates are not incompatible with high rates of
profit. When all is well in the sphere of profit production, a relatively high
rate of interest will not hamper capital formation. It may even quicken its
pace, if productivity develops fast enough to satisfy both loan capital and
productive capital. In fact, the rate of interest may rise or fall with a
decline of profits as well as with a rise of profitability, for in either case
the demand for money may exceed the supply or vice versa.Interest is for Marx only a portion of the average profit. It results from
the fact that capital appears in two roles – as loanable capital in the
hands of the lender and as industrial capital in the hands of the
entrepreneurs. As capital, however, it functions only once, and only
once can it produce profits. Aside from rent, this profit is then divided into
profit and interest. The division is often arbitrary and does not affect the
basic problems of capital production. Being generally limited by the rate of
profit, the rate of interest cannot have the significance assigned to it by
monetary theory.With regard to interest rate problems, it was not Keynes’ but
Marx’s point of view which found its verification in the crisis
situation. A decade of falling interest rates after 1929 did not affect
investment decisions seriously. Interest-rate manipulation ceased to be
regarded as a main instrument for the control of business activities, and
“in the academic view it seems that the importance of the rate of
interest was very much exaggerated in traditional theory, and that Marx was
after all not much at fault in neglecting it altogether.”[15] Soon it was quite
widely acknowledged that investment decisions are seldom based on
considerations of the market-rate of interest[16] and that the “flow of savings appears to be
influenced in modern conditions only to a relatively modest extent by the level
of interest rates.”[17]Keynes himself was finally forced to concede the economic limitations of
interest-rate manipulations; and he decided that “the collapse in the
marginal efficiency of capital may be so complete that no practical reduction
in the rate of interest will be enough”[18] to stimulate investments. “With markets
organized and influenced as they are,” he wrote, “the market
estimation of the marginal efficiency of capital may suffer such enormous
fluctuations that it cannot be sufficiently offset by corresponding
fluctuations in the rate of interest.” [19] From this he concluded that it may be necessary
for the government to control and guide investments directly.Prior to Keynes there were only two schools of economics; or, rather, there
was only bourgeois economy and its Marxist critique. To be sure, bourgeois
economy comprised a variety of viewpoints about the difficulties arising within
the system and the means of overcoming them. There were theoretical deviations
from the generally-held position of laissez-faire. Some of them
related to the specific and changing needs of particular capitalist groups
within the capitalist system; some discussed the problems created by the
differences between the capitalist nations within the world economy. All of
them, however, took the given capitalist system of production for granted; they
did not attack profit production, private property or the competitive
accumulation of capital. Against such critics laissez-faire theory
could hold its own, as long as the market relations seemed to produce some kind
of actual economic order.But the great economic and social upheavals of twentieth century capitalism
destroyed confidence in laissez-faire’s validity. Marx’s critique
of bourgeois society and its economy could no longer be ignored. The
overproduction of capital with its declining profitability, lack of
investments, overproduction of commodities and growing unemployment, all
predicted by Marx, was the undeniable reality and the obvious cause of the
political upheavals of the time. To see these events as temporary dislocations
that soon would dissolve themselves in an upward turn of capital production did
not eliminate the urgent need for state interventions to reduce the depth of
the depression and to secure some measure of social stability. Keynes’
theory fitted this situation. It acknowledged Marx’s economic predictions
without acknowledging Marx himself, and represented, in its essentials and in
bourgeois terms, a kind of weaker repetition of the Marxian critique; and, its
purpose was to arrest capitalism’s decline and prevent its possible
collapse.1. L. R. Klein, The Keynesian Revolution, New York, 1947,. VII2. A. F. Burns, Economic Research and the Keynesian Thinking of our Time, New York, 1946, p. 4.3. The Economist, London, January 27, 1951.4. K. Marx, A Contribution to the Critique of Political Economy, Chicago, 1904, p. 123.5. The General Theory, p. 32.6. Ibid., p. 355.7. J. M. Keynes, Laissez-Faire and Communism, p. 48.8. R. F. Harrod, The Life of John Maynard Keynes, p. 4629. Class loyalty itself opposed Keynes to Marx: “When it comes to the class struggle as such,” he wrote, “my local and personal patriotism ... are attached to my own surroundings. I can be influenced by what seems to me to be Justice and good sense; but the class war will find me on the side of the educated bourgeoisie.” – Essays in Persuasion, London, 1931, p. 32410. Capital, Vol. II, p. 39.11. Even when his activities are considered in a derogatory sense, the successful capitalist, speculator, and financier becomes a benefactor of the nation. S. H. Holbrook, for example, writes that almost every one of the great American moguls would under present-day rules face a good hundred years in prison. Yet, he believes “that no matter how these men accumulated their fortunes, their total activities were of the greatest influence in bringing the United States to its present incomparable position in the world of business and industry.” – The Age of the Moguls, New York, 1953, p. X.12. The General Theory, p. 159.13. Capital, Vol. II, p. 159.14. Once it becomes easier for people to make money faster by buying du Pont stock than the du Pont Corporation can make money by producing nylon, dacron, and chemicals, then it is time to watch out. The Senate Banking Committee’s Report on its Stock Market Survey. The New York Times, May“27, 1955.15. J. Robinson, An Essay on Marxian Economics, London, 1942, p. 84.16. The British Committee on the Working of the Monetary System (Radcliffe Report) came to the conclusion that the monetary means affecting the rate of interest are by themselves quite incapable of stimulating the economy and have meaning only in connection with a general economic policy which includes fiscal measures and direct physical controls. Cumd. 827, London, 1959.17. The Statist, London, September 24, 195518. The General Theory, p. 316.19. Ibid., p. 320. 
Table of Contents