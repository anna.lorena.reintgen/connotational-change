Delivered: Pamphlet,
    Autumn 1919
Transcription\HTML Markup: Scottish Republican Socialist
    Movement Archive in 2002 and David Walters
    in 2003
Copyleft: John MacLean Internet Archive
    (www.marx.org)1999, 2003. Permission is granted to copy and/or distribute
    this document under the terms of the GNU Free Documentation LicenseA terrible blow was struck at the working class of Scotland
    on Bloody Friday, 31 January 1919. A mass movement was started on the Clyde
    on the Monday prior to reduce the working week to forty hours. The movement
    rapidly spread over the industrial belt of Scotland. A strike for a
    forty-four-hour week was simultaneously proceeding in Belfast, where the
    stoppage was almost complete. The Tyneside also had its strike on the
    meal-time question…. Had the strike feeling spread the whole country
    might have been involved, and in the temper of the people at the time the
    government feared a revolution such as had swept Germany into the hands of
    the “right” socialists.The movement had to be nipped in the bud, and the opportunity presented
    itself in the move of the workers, who appealed to the Lord Provost of
    Glasgow to force the employers to grant the shorter working week.On the Wednesday the Lord Provost told the strikers’ deputation to call
    at the City Chambers on Bloody Friday. Meantime, acting under instructions
    from Bonar Law, the Lord Provost made preparations to trap the people. The
    tramway services provided the authorities with the required provocation on
    the fatal day. The strikers had calculated that the tramway workers would
    join them, and so disorganise the transport to and from the works.The failure of the tramwaymen to respond to the strike call embittered
    the strikers, who on occasions became very nasty to them. This was known to
    the authorities, who in consequence planned that cars should run through
    George Square as usual on Bloody Friday, although the Square was packed
    with as immense a crowd as had ever gathered there before.The man immediately responsible was the general manager, James
    Dalrymple. Of course, he was acting in conjunction with the police under
    Chief Constable Stevenson; and the latter was carrying out the instructions
    of the capitalist town councillors led by the magistrates and the Lord
    Provost.Had no cars been sent through the Square, packed to excess with strikers
    who were provoked by the treachery of the tramwaymen and [tramway] women,
    no riot would have been started and the Chief Constable’s
    “gallant” men would have got no excuse to crack the skulls of
    defenceless, innocent people….The tramway workers were not entirely to blame for being at work. On
    their side, the major part of the blame rests on the shoulders of their
    then union (Municipal Employees’ Association) organiser, the brave soldier,
    Bailie A. Turner….Of course fine profits were being made out of the trams, profits that
    largely ought to have gone to improve the lot of the employees. Dalrymple’s
    salary went up all right. When the war broke out, Dalrymple used the car
    system and the profits, not only to help recruiting and other war work, but
    to get a knighthood as well. That is why I nicknamed him “Sir”
    James Dalrymple from early on in the war, a form of ridicule that probably
    played some part in turning people against him and preventing him attaining
    the desired goal.He used his spies effectively at the depots, and by that means stampeded
    men into joining the army without any show of force or open threats on his
    part…Naturally amongst those remaining on the car system in January 1919,
    there was a feeling of complete despondency and demoralisation. Turner had
    been away fighting the enemy in France when he should have been fighting
    them in Glasgow. The women, who had been introduced on to the cars for the
    war period, had neither interest in the union as a rule nor in any fight
    for a shorter working week.Under these conditions it would be very wrong to hold the car workers
    absolutely responsible for the scabbing during the forty hours strike. The
    above explanation shows clearly enough to the usual member of the working
    class who has seen the spy system and the victimisation system in
    operation, that the essential blame for the cars being out during the
    strike of January 1919 must be placed on the shoulders of Mr James
    Dalrymple….The tramway system of Glasgow has been boasted of all over the world as
    the triumph of municipal socialism. From the standpoint of profit-making
    efficiency I have no objection to raise. But socialism implies security,
    comfort, and happiness for the people who actually run the cars.Victimisation is the opposite of security, and spying is degrading both
    to the spy and the one spied upon. Spying implies that it
    “pays” to hold your tongue; it spells ruin if you speak your
    mind. Socialism means that your bread and butter are secure no matter what
    you think. Socialism means that you are free and entitled to speak your
    mind.Under socialism the making of profit will give place to the comfort of
    the employees…. From a labour standpoint the “municipal
    socialism” of the Glasgow trams is a ghastly blank…Return to the John MacLean
    Internet Archive
Return to Marxists Internet Archive   