MIA  > 
Archive  > 
Kautsky  > 
Georgia No Government of the day was more firmly
established than the Georgian Government. We have seen how unusually
large was the majority of the Social Democratic Party in the Georgian
Parliament. None of the Opposition parties dreamed of overturning the
Government or altering its policy.In addition to the overwhelming majority in the Parliament,
the Government was supported by the overwhelming majority of the
population.The modern section of the proletariat, which is the
politically decisive class in present-day Georgia, stood fast behind
the
Government, which maintained a close association with it.The Communist Party did indeed exist, and enjoyed the fullest
liberty in all its movements which were not directed to raising an
armed rebellion there was no obstacle in the way of its open propaganda
and legal organised activity and the latter ends were eagerly pursued
by the side of an equally energetic underground movement. The Party
operated with the most lavish resources which emanated from Soviet
Russia, but in spite of all this, it did not succeed in gaining a
following of any importance.In contrast to the rest of Europe, Bolshevism has been
familiar in Georgia from the commencement, and there it deceives
nobody. In spite of boundary divisions, the Georgians are too closely
connected with Russia not to know exactly how things are there, and, in
comparison with the hell which Soviet Russia represents, Georgia
appeared as a paradise. The workers also know exactly the fearsome
oppression which weighs upon the working class of Russia, and the
complete loss of rights and impotence of all sections of the
proletariat which do not cower in servile obedience before the
Dictatorship, they perceive clearly that the Dictatorship, which is
supposed to be a dictatorship of the proletariat, has led and must lead
to the dictatorship over the entire population, including the
proletariat, as dictatorship, by its essence, means that even the ranks
of the nominal ruling class are subjected to the despotism of the
Government. The workers of Georgia regard the Soviet regime with
peculiar bitterness, as its faithlessness towards its small neighbour
becomes more obvious every day.The Communists boasted from time to time that they were
recruiting their ranks from the Georgian proletariat. But whenever an
opportunity arose it revealed their insignificance. Thus they commenced
a great agitation amongst the railwaymen. A brilliant result was to be
achieved. Just as I was departing, a Congress of Railwaymen was held in
Tiflis which was going to demonstrate that the confidence of the
railway workers in the Government had been completely shattered. The
Communists expected to dominate the conference.Behold, when the delegates were counted, one single Communist
was found among them. All the others, and there were over eighty, were
Social-Democrats.I had a similar experience with all the Labour conferences at
which I had the opportunity to be present.In Europe to-day, we frequently meet with an intermediate form
of Socialism, which dazzled by the apparent results of Bolshevism,
declares in theory for the dictatorship of the Soviets, but contrives
to keep on firm ground, so that, in practice, it declares for democracy
and applies the methods of democracy. This method is quite unknown in
Georgia; neither from the Left nor the Right was any considerable
opposition forthcoming to the Social-Democratic Government.One might, at least, have expected some resistance from the
large landowners who have been expropriated. In reality they have
acquiesced in their fate, which overtook them with all the force of a
natural catastrophe; they know they are played out, and that any
attempt to re-establish the old order would raise against them the
entire nation. Not a few of them were giving faithful and intelligent
service to the Republic.The aristocrats of the great French Revolution, likewise those
of the present Russian Revolution, would have accepted their fate as an
inescapable dispensation, had they not found allies abroad to arouse in
them hopes of a restoration. The Georgian princes have not enough
friends abroad who would seek to intervene in their favour. The only
power whose interference in the internal affairs of Georgia was to be
feared was the Russian Soviet Republic, and should they do so, the late
landlords would fall out of the frying-pan into the fire. The
Social-Democratic regime has, indeed, taken the land from them without
compensation, except for the peasant-holding which enables them to live
by the labour of their hands; but it has not mishandled their persons.
It has respected their humanity. It shared the view of Marx that we are
not fighting the capitalists as persons, but are attacking their
functions. So far as the functions of capitalists are indispensable, we
propose they should be changed from private functions to social
functions; so far as their functions are superfluous or harmful, we
shall abolish them in an economic organisation in which them will be no
place for them. The Bolshevist regime has not merely undertaken the
campaign against the functions, but also against the persons of the
capitalists and landowners, even after they have ceased to be
exploiters and have become proletarians in reality, as well as against
those who are not willing or able to offer resistance to the new
regime. And Bolshevism was not satisfied to render these persons merely
harmless, it had also degraded them and trodden them under foot, and
inflicted infinite torture upon them, and has aroused against them the
lowest instincts of the proletariat. In Georgia, the expropriators, as
well as the expropriated, have been spared this mutual degradation. The
expropriated have likewise refrained from any attempt to resist their
fate. The alternative which confronted them – the Bolshevist
regime – was too terrible.Feudal lords belong to the past. The capitalist constitutes
the exploiting power of the present. We have already seen why their
political power in Georgia was negligible. Only a few of them are
engaged in industry, and most of them represent the parasitic forms of
capitalism – as usurers, profiteers, – and
landlords in towns. Among them, Armenians preponderate, and they are
not loved by the Georgian people. The cause of this antagonism is
described differently by each side, but the underlying facts remain the
same. Interpretations of the facts differ. The one side despises the
Armenians as dirty, unscrupulous traders and usurers; the other side
declares the Armenians are thrifty and industrious. The Georgians, on
the other hand, have retained too much feudal carelessness and love of
enjoyment. Therefore, the Armenians maintain over them an economic
advantage.Besides Armenians, there are Germans, Russians, Italians and
Jews (who are considered a special nationality in Georgia) and members
of other foreign nations among the capitalists, but few members of the
Georgian nation.The result of all this is that the frequent great
dissatisfaction of the capitalists with the present regime finds no
echo among the population, which, on the contrary, rejoices when they
are severely treated.In capitalist circles, complaint was made that unreasonable
measures were often adopted, so that not merely were the parasitic
activities of capital restricted, but obstacles were set up to its
functioning in cases where it promotes the development of the
productive forces. So far as I was able to investigate such complaints,
I could not become convinced of their justification.But it is possible, and even probable, that many mistakes of
this order were committed in the excitement of a revolutionary period,
among the difficulties of an economic system which had suffered
destruction and confusion through the war, and in view of the lack of
resources essential to the needs of capitalist production, which it is
not the business of any capitalist interest to provide. In the
difficult task of harmonising the interests of the proletariat with the
development of the productive forces under capitalist conditions,
occasional mistakes on one side or the other could scarcely be avoided.All the complaints and all the discontent in capitalist
circles did not, however, crystallise in any important movement of
political opposition. From this side, the Social-Democratic Government
had not the least to fear. The only chance of democratic opposition was
that the capitalists might succeed in winning over the peasants to
their side. But this is unthinkable.The future of the Socialist regime, on the basis of
democracy, depends upon the peasantry. This is the case not alone in
Georgia, but in all States, where the proletariat does not form the
majority of the population.If democracy should be unfavourable to the proletariat, this
is not due to the capitalists, whose numbers are relatively small, but
to the peasants. If the peasants form the majority of the population,
and are hostile to the proletariat, the latter cannot hope to
establish its rule under democratic conditions. This fact is certainly
unpalatable. It is in no way altered by the Soviet system, which is
also obliged to make terms with the peasants, and grant them
Peasants’ Councils.The division into Workers’ and Peasants’
Councils makes the workers the masters of the towns and the peasants
the masters of the countryside. Both classes may work together, quite
well so long as one does not interfere with the other, and each is
permitted freedom to act in its own sphere. But, unfortunately, in the
long run, neither class may exist for itself alone; the towns have to
rely upon the country, and vice versa.Once a common economic organism has been established, the
mere form of the Soviet Constitution offers to the town proletariat no
greater superiority over the country people than the system of
democracy, assuming that each class is equally represented.The fact that the proletariat in Russia has become the
preponderating force was not due to the Soviet Constitution. The
collapse and dissolution of the Russian Army took place under
conditions which left the Communist Party and its soldiers the only
armed force in the State, and, in addition, the Russian peasants lacked
all political discipline and were without a comprehensive political
organisation.Where the proletariat, or the proletarian party, belonging to
an agrarian State does not control the armed forces, it can only
maintain itself in power with the support of the peasantry. This
support cannot even be dispensed with in cases where the proletariat is
backed by arms. For we know as Marxists that in the last resort State
policy is decided not by machine guns, but by economic necessities.
Upon this we base our belief in the effect of a mass strike. Upon this,
too, is based the power of the peasantry in the kind of State we have
been considering. The industrial proletariat is able to coerce the
peasants as little as the peasants can coerce it. They must both learn
to settle their differences peacefully, which is more likely to be done
in a democracy than in a state of permanent separation of one class
from the other.The problem is not an easy one to solve. The antagonisms are
very great.The proletarians must aim at the common ownership and social
management of the means of production, but the peasant is the most
tenacious and fanatical champion of private property in the means of
production.If we should decide upon the policy of immediate and complete
socialisation, then this antagonism would inevitably develop into an
implacable struggle between the proletariat and the peasants. But even
the Bolshevists have not adopted this attitude, or else they would not
have surrendered the countryside to the peasants.So long as the mode of peasant production remains in
existence, its socialisation is out of the question. Such a measure can
only be adopted on the basis of large-scale management.The socialisation of the great monopolistic undertakings
starting, with the mines and forests, is as much in the interests of
the peasant as of the working-class, when it is carried out in a manner
which will lead to increased productivity.The peasant is certainly sceptical and unsympathetic towards
theoretical Socialism. He is disposed to be friendly towards practical
Socialism; when it requires no sacrifice on his part, and achieves what
we Socialists expect of it.In all events, the antagonism of the peasant as the seller and
the proletariat as the buyer of the means of subsistence will persist.
But it is not a class antagonism in this manner, the peasants are
confronted by the entire population of buyers of the means of
subsistence, not only all the town dwellers, but also many village
hand-workers and land-workers.In this matter, democracy, in contrast to the Soviet system,
would increase the number of those who would combine with the
proletariat against the producers of the means of subsistence.The vital part of any proletarian regime will not be the
relation to the capitalist class, but the connection with the peasants.
The capitalists can soon be disposed of, provided the economic
conditions permit, and the peasants accord their support.If the capitalist class, should know how to secure the
determined and energetic support of the peasantry, the issue would be
doubtful, especially in countries in which the peasants formed the
largest class of the population.In this respect the conditions in Georgia are very favourable.
We have seen that here the Social-Democrats have been the leaders and
executors of the agrarian revolution which liberated the peasants from
all the vestiges of feudalism.It is true a similar relation exists also in Russia between
the Bolshevists and the peasants, and it existed in France in 1789
until the close of the Revolution between the majority of the peasants
and the Paris revolutionaries. In these countries the peasants
everywhere backed the Revolution so long as the reactionary powers
threatened a restoration of feudal conditions. But as soon as the
danger was over, the peasants went over as one man into the ranks of
the counter-revolution. In places they had already rebelled against the
Revolution, as we may recall La Vendée and the latest
peasant revolts in Russia.We have hitherto seen nothing like this in Georgia, nor any
indication that a change will occur within a measurable time. What is
the reason for the difference?One of the causes which, during the French Revolution, led to
peasant revolts lay in the diversity of the peasants’
conditions within the separate provinces of the country. There were
some backward districts in which the feudal lord and the Church
functioned as protectors and advisers, and not as exploiters, of the
peasants. When revolutionary France plunged into war, and required
sacrifices from the peasants, especially in the form of recruits, the
feudal lords had no difficulty in provoking the peasants to rebellion
in many of these backward provinces.What remains of the Russian Empire as Soviet Russia is better
placed in this respect. It was far more of a unity after it lost its
Border States than was the old French monarchy. Had Russia retained its
pre-war territory, Poland would easily have become far more dangerous
than was La Vendée in 1793.On the other hand, another circumstance has influenced the
peasant counter-revolution in Russia to a greater degree than in
France. In my book on Terrorism and Communism
I have already pointed out that the French peasant was relieved by the
Revolution from the necessity of selling corn, as, in addition to
getting rid of the feudal burdens, he was exempted from paying taxes.
This aggravated the task of feeding Paris, especially after the
commencement of the war, when large armies became necessary, requiring
great quantities of food.In their need, the townspeople frequently endeavoured to
assist themselves by imposing forcible requisitions on the peasants,
which the latter answered, where they could, by forcible resistance.
But this state of affairs did not last long, as the revolutionary
armies soon pressed victoriously over the country’s borders,
and were in a position to feed themselves as the expense of the
national enemies.This advantage is lacking in the case of Russia. Its
agriculture is so backward that only a slight surplus is yielded. If
even before the Revolution the peasant sold a great deal of corn, he
did so because he and his family were obliged to go hungry. As in
France, heavy tributes were imposed on him by the State and the
landowner in order to pay these he was compelled to sell a considerable
portion of his harvest to the towns and to foreign countries. Now that
the tributes of the State and the landowner have been abolished, the
peasant no longer needs to sell. He sowed his ground more negligently,
worked less and ate sufficient, which he could never do formerly.But hunger invades the towns.The plight of Soviet Russia is all the worse because it has
lost the most fertile districts, which yielded the most abundant
harvests. Its armies have been numerous enough to lay waste large
tracts of land, but not sufficiently victorious to conquer new
territory beyond the borders from which to provision themselves.The Red Armies can only extend in the direction of the south,
towards the Caspian Sea, Baku, North Persia and Turkestan. They appear
in these Mohammedan countries as allies of Pan-Islamism, as liberators
from the yoke of European Imperialism, and as such are welcomed. But
performance assumes a different shape from the promises. The peasantry
of every district are plundered to the utmost. Latterly, Bolshevist
sympathies have greatly cooled in the Mohammedan world.The peasant in Russia proper cannot be dealt with so
recklessly as in these “liberated” territories. The
requisitions made on him are less heavy, although more than he wants to
give, and often more than he can. The Dictatorship is acquainted with
only one method of solving every problem – brute force. The
tribute required is forcibly collected from the recalcitrant peasant.With the exception of peasant revolts and the devastation of
villages, this method has only achieved one thing – the
complete suspension of all efforts on the part of the peasant to make
their industry yield a surplus. The cultivation of the land
deteriorates. Lack of bread and hunger grow.Once more an appeal was made to force. A demand was being put
forward in Soviet Russia that the peasant should be compelled to raise
more crops. Such compulsory tillage was doomed, just as all previous
forcible measures of Bolshevism have failed which have not been aimed
at the destruction of what is existing but at the construction of a new
economy.The protagonists of compulsory tillage have not properly
considered what a gigantic apparatus is necessary to compel four-fifths
of the population to do work.The present population of the towns is not sufficient to
supply the necessary controlling and police force.But even if the measure could be successful, which is out of
the question, it would be nothing more than an immense revival of the
old serf labour, which, next to slavery, is the least productive kind
of labour. It would completely seal the economic downfall of Soviet
Russia.One permanent result of all these experiments, if they are
continued longer, or perchance multiplied, would be an increasing
bitterness of the peasants towards the town proletariat. They would
immediately transform the peasants into a reactionary anti-Socialist
mass as soon as the Entente abandoned its foolish policy of trying to
establish a new landlord regime in Russia. Once this danger to the
Russian peasants disappeared, the reaction would have full scope. It is
possible that the parallel to odd France might extend to the emerging
of a new peasant emperor from the ranks of the revolutionaries. In the
short period of its existence, the men of the Dictatorship have
undergone so many changes that the last-named role would not be
difficult for many of them to assume.Even this would find enthusiastic support among those who
admire only the success of the moment.Quite different from Russia have been the lines of development
in Georgia.Instead of Dictatorship, that country was ruled by democracy,
and the Government could not simply dictate what it liked, and shoot at
its pleasure those who did not obey its instructions. The menace to the
food supply of the industrial population, caused by the liberation of
the peasants, exists there as well as in Russia the problem is common
to all Eastern States which have passed through an agrarian revolution
as a result of the war.The use of force against the peasants cannot be thought of in
Georgia. How, then, can the peasant be induced to produce a surplus and
supply it to the towns?In considering this question, we should not forget that more
than one hundred years have elapsed since the French Revolution. This
has modified to some extent the problem which arose at that time. Then
the village produced almost everything needed by agriculture
– the village hand-worker supplied the peasants with what the
latter did not produce himself. They had scarcely any need of the towns.To-day the peasant is dependent on large-scale industry, which
manufactures his implements and often his manure, when it is of an
artificial nature. It supplies him with his clothes, as well as
furniture like iron bedsteads. The peasant is anxious to have the
products of industry, and in exchange for them, is prepared to produce
a surplus. The greater the variety of goods that industry can furnish
to him, the more intensively will he work his land, and the more he
will be able to produce.The development of native industries and of foreign trade, to
stimulate the importation of foreign products, is essential if the
peasant is to be induced to yield a surplus for the towns. The problem
is not solved by the mere manufacture of paper money. The peasant
whistles at this money if it does not enable him to buy industrial
products.At bottom, the Bolshevists know this. But their attempt to
apply the policy of immediate socialisation has killed native
industries, and their foreign propaganda in favour of the World
Revolution has not achieved the latter, but brought them the blockade.The extension of native industries and of foreign trade is the
first condition for an augmented voluntary supply of food to the towns.
The second is the raising of the productivity of agriculture itself.
This is particularly necessary in countries where primitive
agricultural methods obtain.The Georgian Government had realised these facts. Alongside of
their endeavours to extend industry and trade, efforts were made to
educate the peasants by means of model agricultural undertakings and
schools, and to improve the means of communication and to construct
drainage works, with which we have already dealt.Of course, such a programme as this cannot be carried out
without the aid of large capital resources, which means heavy taxation,
not only of the capitalists, but of the peasants as well. An Income Tax
had already been introduced, specially applicable to these two classes,
and further taxes were bound to follow.The decisions on this point were likely to be vital for the
Social-Democratic Republic. If the peasants exhibited a willingness to
bear this taxation, it would have been possible to give better
guarantees for the feeding of the towns than before to permanently
stabilise the exchange, and thereby give a rapid impulse to the growth
of industry and trade, and the improvement of agriculture itself.
Georgia would then have surmounted the crisis which followed in the
wake of the war more easily and quickly than most of the European
States and gained a secure economic basis. No great extension of
agriculture is needed for the country to become self-supporting. Before
the war it produced five to six millions cwts. of wheat. In addition to
this, about one million cwts. were imported, but half a million cwts.
of maize were exported. Its deficit in bread stuffs, therefore,
amounted to only half a million cwts. There was hardly a deficit in the
case of other food stuffs, with the exception of sugar. The difference
between now and, formerly lies in the fact that then great abundance
prevailed, and to-day there is scarcity and dearness rather than gross
shortage.The antagonism, between workers and peasants, which would
otherwise be so sharp, is softened by the circumstances just described.
Even the imposition of new taxes need not harden this opposition.A Government emanating from the towns, hostile to the
peasants, and not subject to his control, which demands from him
contributions for purely urban purposes, is quite a different thing
from a Government which is partly elected by the peasant through
universal suffrage, is controlled by deputies elected by himself, and
aims at promoting his own welfare together with that of the town
population.Only under democratic conditions, and not under a
Dictatorship, is it possible to enlist the interest of the peasants in
a State that is ruled by the proletariat.In Georgia, the relation between the proletariat and the
peasants is the best possible. They worked together cordially in the
building up of a new economy. The peasants were inspired by the
greatest confidence in the proletarian leadership, and the latter did
all that is possible to further the interests of the country alongside
with its own class interests. This end is achieved by keeping in the
foreground the necessity for increasing the productive forces of the
country in which both classes are equally interested.The co-operation of the classes was assisted by the fact that
they are often brought together in personal association. Many
industrial workers possess small plots of land, and many peasants are
still obliged to perform temporary wage-labour. The co-operation of the
two classes is not less rendered easier by the consumers’
co-operative, societies, which unite town workers and peasants, than by
the fact that the priests as well as the monks have lost all influence
over the peasantry. The historical moment, the tradition, which plays
such a big part with the conservative peasant, is in Georgia associated
with the Social-Democracy, as it was the latter which, from the
commencement led the struggle for the peasants’ emancipation
from the Russian bureaucracy and Absolutism, and from native serfdom.
Add to this a further motive. As soon as the peasant emerges from his
revolutionary period and becomes the undisputed owner of his land, he
supports the readiest that government which not only respects his
property, but also protects it from devastation through foreign
invasion and civil war. This explains the support given by the French
peasants to the victorious Napoleonic Empire, and their hostility to
the urban revolutionaries as soon as the latter appeared to be the
instigators of civil war.The Social-Democratic Government of Georgia has not only
liberated the peasants from the feudal burdens, but its foreign
policy, as we shall presently see, in spite of stupendous
difficulties, had till February 1920 saved the country from foreign
invasion. Its internal policy of democratic tolerance and liberty,
which did not, however, signify apathy or weakness, but was coupled
with energy and conscious initiative, has averted an internal
catastrophe. Within recent years, when rebellions have broken out
almost everywhere from the Rhine to the Pacific Ocean, Georgia was the
only country, with the exception of German Austria, that has escaped
violence. A few attempts at rebellion in outlying districts in the
south and the north are hardly worth mentioning.This peace and security have commended the Social-Democratic
regime to the peasants.Perhaps in no other country at the present time are the
conditions for friendly relations between the peasantry and the
proletariat, and for the sympathetic neutrality of the former towards
industrial socialisation, so favourable as in Georgia.Thus we find that in this period of revolution the Government which
was most firmly supported at home was the Georgian
Government.It is true the external situation was of quite a different
cast. Next
chapter | Top of pageLast updated on
1.3.2017