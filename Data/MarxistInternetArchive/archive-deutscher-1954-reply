First Published:
Originally written for the French monthly Esprit (March 1954).
Transcribed: Martin Fahlgren
Online Version: Marxist Internet Archive 2013
HTML Markup: Martin Fahlgren, 2013My book Russia After Stalin,
which I wrote and concluded within a few weeks after Stalin's death, is
appearing in a French translation shortly before the first anniversary
of that event. This is a short interval; yet it has been crowded with
startling events, and during it Russia has moved quite a distance from
where she stood on 6 March 1953. It is enough to recall what some of
the best known commentators and experts predicted at the time, to
realize how far indeed Russia is now from that point of departure. Some
of the experts, for instance, argued, not without superficial logic,
that in a Police State the police was the decisive factor of power, and
that consequently Beria, its head, was by definition Stalin's real
successor, sure to oust Malenkov and Molotov. Other reputable analysts
assured us stolidly that there was and could be ”nothing new in
the East”, because Stalin had settled beforehand the issue of the
succession and because his heirs, tied by the strongest bonds of
solidarity, saw eye to eye with one another over all major issues of
policy.The most obtuse Stalinists and the bitterest anti-communists
expressed this view with equal self-confidence. Curiously enough, this
was also the view held even later by so intelligent a writer as Mr.
George Kennan and expressed in his critique of my book. I know of
another very shrewd man, the Moscow Ambassador of a great Western
power, who spent the whole evening of 9 July 1953 arguing that my
analysis of the Russian situation, given in Russia After Stalin,
was utterly wrong because it presupposed a cleavage within the Soviet
ruling group. He, the Ambassador, knew from close observation and long
study that no such cleavage existed : that Malenkov, Beria, Molotov,
and Khrushchev thought and acted in unison, knowing full well that
their chances of survival depended on their absolute unity. Having thus
destroyed my analysis and hypothesis once for all, His Excellency went
to bed only to awaken next morning to the dramatic news about Beria's
downfall... .I know well where my own work might gain from some corrections, and
what revision would be advisable in the light of recent events. But
such corrections and revisions would not yet go beyond retouching a
paragraph here and changing slightly the emphasis of my argument there.
Far from refuting my prognostication, events have confirmed it; and
they have done so in the only way in which they confirm a theoretical
formula, namely by showing a pattern of development which, although it
harmonizes basically with the prediction, is naturally more complicated
and dynamic than any theoretical formula.My prognostication has not been basically refuted by events perhaps
because from the outset I approached my task somewhat more modestly
than many another writer on this subject. I did not pretend to know
what would be the fate of this or that personality in the Soviet ruling
group. I drew no personal horoscope for Malenkov, or Beria, or
Khrushchev. Instead, I concentrated on outlining, summing up, and
projecting into the future the broad social trends at work in
contemporary Russia. This led me to the conclusion that the Soviet
Union was approaching a critical turn of its history at which it would
be compelled to begin to move in a new direction, and that Stalin's
death, far from being the main cause of the change, would merely speed
it up and underline its inevitability.My analysis and conclusions have become the subjects of an animated
controversy on both sides of the Atlantic. It is hardly surprising that
some of my fiercest critics are precisely those luckless soothsayers
who either had already seen Beria in Stalin's place, or had been quite
sure of the ”absolute ideological solidarity” of Stalin's
heirs. I have also drawn the wrath of the professional propagandists of
the cold war, and quite especially of the anticommunist crusaders
fighting under the lofty banners of the ”Congress for Cultural
Freedom”. On the other hand, many serious and able writers have
defended my views with much conviction and effect. This controversy has
already found its echoes, both friendly and hostile, in the French
Press as well; and I propose to deal here especially with M. Raymond
Aron's extensive critique of my views which appeared in the October
issue of Preuves... .Any realistic analysis of the Stalin era and of its conclusion must
draw a balance of the Soviet industrial revolution of the last
twenty-five years, the revolution by force of which Russia has from one
of the industrially most backward nations become the world's second
industrial power. This process was accompanied by vast educational
progress, into which the bulk of Soviet society has been drawn.
Stalinist despotism and terrorism drove the Soviet people to carry
through this industrial revolution, in part despite themselves, at an
unprecedented pace, and in the face of unprecedented difficulties. The
”primitive magic of Stalinism” reflected the cultural
backwardness of Soviet society in the formative years and in the middle
stretches of the Stalin era. From this argument I concluded that with
the progress achieved in the 1950's, the Stalinist terrorism and
primitive magic had outlived their day and were coming into conflict
with the new needs of Soviet society. The higher level of industrial
and general civilization favoured a gradual democratization of Soviet
political life, although a military dictatorship, of the Bonapartist
type, might also arise amid mounting international tensions. Both these
prospects signify an end to Stalinism. An attempt to galvanize the
Stalinist regime and orthodoxy was still possible and even probable;
but it could hardly meet with more than episodic success.The cold war propagandist bases all his arguments and slogans on the
assumption of an unchangeable and irredeemable evil in Stalinism or
communism at large. Remove that evil, and all his ideological thrusts
strike into a vacuum. He therefore stubbornly refuses to see that the
”evil” has been historically determined and that the
profound transformation of the structure and outlook of Soviet society
cannot fail to have far-reaching political consequences.At this point my critics, especially M. Raymond Aron, accuse me of
all the mortal sins of Marxist determinism: I am said to deny the
importance of human will in history; to eliminate the role of the
individual, especially that of the grand homme and leader; and
to ascribe one-sidedly to the economic structure of society that
determining influence on human affairs which it does not and cannot
possess.I have, of course, never denied my Marxist convictions, but I try to
stand on my own feet without leaning on Marx's much abused authority.
As a matter of principle I have always endeavoured to develop my
argument in such a way that its validity should not depend on any
specifically Marxist assumptions. One need not be a Marxist at all to
agree with me on the impact of the Soviet industrial revolution upon
Soviet politics. It has not occurred to a single historian of the XIXth
century, conservative or liberal, to ignore the impact of the English
industrial revolution upon the politics of Victorian England. Not a
single historian can ignore the connection between that revolution and
the gradual broadening of the franchise, that is the gradual
democratization of England. It is a truism that modern forms of
democratic life have developed mainly in industrialized nations and
have, as a rule, failed to develop in nations that have remained on the
pre-industrial, semi-feudal level of civilization. But what is accepted
as a truism in the modern and contemporary history of the non-communist
world is, in the eyes of our critics, totally inapplicable to the
Soviet Union: there it is simply preposterous to expect that massive
industrialization, urbanization, and educational progress may foster
any democratic trends and tendencies.A few of the critics have put forward an argument which I am not
inclined to dismiss out of hand. What about Germany? they ask. Has a
high level of industrialization and mass education prevented Germany
from producing the worst authoritarianism and totalitarianism? Did
Nazism not have its ”primitive magic”? How can one speak
about Russia ”outgrowing” Stalinism when Germany never
really ”outgrew” Nazism, which was destroyed only from the
outside, through war?I ought, perhaps, to remark that I have nowhere said or suggested
that industrialization and educational progress automatically guarantee
a democratic development. All I have said is that industrialization tends
to awaken democratic aspirations in the masses. These aspirations may,
of course, be frustrated or defeated by other factors. But even in
Germany industrialization did foster the democratic trend. The four
decades between Bismarck's Ausnahmegesetz and Hitler's rise saw
a very considerable expansion of the democratic forms of political
life, at first under the Hohenzollern Empire and then under the Weimar
Republic. The German working class was the chief factor of that
democratization — it wrested one democratic concession after
another from its ruling classes. That it was not persistent and that it
abdicated at the decisive moment, in 1933, does not obliterate the
historical connection, evident even in Germany, between
industrialization and democratic politics.What Germany's history proves is this : the democratic trend was
strong while German society was growing and expanding on a capitalist
basis. It withered and gave place to the totalitarian trend in a
decaying society based on a shrinking capitalist economy, such as
Germany's economy was on the eve of Hitler's rise. Unemployment of
millions, an all-pervading sense of social instability, mass fear and
mass hysteria, these were the basic elements that went into the making
of Nazism. In addition there was the envy, the hatred, and the contempt
of the Kleinbürgertum for the labour movement; the
illusion of that Kleinbürgertum that it could assert
itself against both the Grossbürgertum and the
proletariat; the determination of the German industrial and financial
barons to use the lower middle class run amok against the proletariat;
the internal division and impotence of German labour; and — last
but not least — Germany's national pride wounded since the 1918
defeat and her acute craving for revenge. This was the specific and
very complex combination of factors which produced the particular
German brand of a totalitarian regime on the basis of a capitalist
economy.While it is obviously true that a high industrial civilization does
not preclude the growth of totalitarianism, it should be even more
obvious that it is not that civilization per se which is responsible
for that growth. In each case the specific causes of totalitarianism
must be examined. I have tried to expose the specific sources of
Stalinism in the state of Soviet society of the 1920's, and to show
that these sources have been drying up in the 1950's. It is therefore
no answer to say that from very different sources, namely from the
ferments of the German society of the 1920's and 1930's, there came
something that was outwardly, and only outwardly, very similar to
Stalinism. I insist on the analysis of specific causes and
consequences, while my critics reason very much like that old Polish
peasant who argued with his children that it was useless to cure
tuberculosis in the family, because, having cured tuberculosis, they
would die from some epidemics sooner or later. I maintain that
urbanization and modernization are ”curing” the Soviet
Union from Stalinism. ”But think of the epidemic of
Nazism”, some profound thinkers reply, ”to which Germany
succumbed; and in view of it how can one speak about Russia curing
herself of Stalinism?”Certainly, if conditions like those that gave rise to Nazism —
mass unemployment, a shrinking economy, a sense of social insecurity,
national humiliation, fear, and mass hysteria — were to appear in
the Soviet Union, the result would probably be very similar. However,
even my critics do not expect such conditions to arise in the Soviet
Union within the foreseeable future. (Such conditions might appear in
consequence of Russia's defeat in a third world war, and the result
would certainly be not democracy but some form of a fascist
totalitarianism, if these political terms were still to retain any
meaning after an atomic war.)It can never be sufficiently strongly emphasized that Soviet
society, no matter whether one views it with hostile or friendly eyes,
or only openmindedly, cannot be understood at all if one of its basic
characteristics is ignored, namely the fact that it is an expanding
society and that it expands on the basis of a planned economy making it
immune from that extreme economic and moral instability which in
bourgeois society tends to produce fascist mass neuroses. The Soviet
Union is emerging from Stalinism with all the conditions necessary for
continued expansion, expansion not merely during certain phases of the
industrial cycle or during armament booms. Continuous expansion is in
fact inherent in planned economy of the socialist, or even of the
present Soviet type, as the basic form of its movement, just as the ups
and downs of the trade cycle represent the forms of movement peculiar
to ”normal” capitalism. (This is the hard core of truth in
all communist propaganda; and it is all too easy to overlook or rashly
to reject it because it is usually wrapped up in thick layers of crude
fiction.) Stalinist totalitarianism and primitive magic, belonging
essentially to an earlier transitional period, become irrelevant,
anachronistic, and untenable in this expanding society at its present
level of productive forces. How much more irrelevant to the problems of
that society are the phenomena of Nazism or fascism born from social
decay and disintegration.One of my French critics claims that in expounding this determinist
view I am reducing ”le rôle de la volonté humaine”
and the role ”des grands hommes” in history. I may
perhaps be allowed to ask: reducing in relation to — what? To
their actual role in the historical process? or to the critic's grossly
exaggerated idea of that role? I certainly take the view that the human
will is ”free” only to the extent to which it acts as the
promoter of ”necessity”, that is within limits
circumscribed by conditions external to it. The will of the grands
hommes represents only one particular case of the general problem
of the human will: le grand homme ”makes” history
within the limits which his environment and the existing balance of
social forces, national or international, allow him to do so. My French
critic seems flabbergasted at my suggestion that the Bolshevik
revolution of 1917 could perhaps have taken place even without Lenin.
He, on the contrary, sees Lenin as the sovereign maker of that
revolution, and Lenin's personal role as more important than all
”objective trends”, than the ”Spirit of the
time”, and the ”laws of history, and similar
abstractions” (the use of some of which he ascribes to me
altogether fortuitously). My French critic — M. Raymond Aron
— is therefore quite consistent with himself when he writes :
”Peut-être aurait-il suffi que le train plombé
qui transportait Lenin à travers l'Allemagne [in 1917] sautât
ou que Trotsky fût retenu aux Etats-Unis ou en Angleterre, pour
que l'Esprit du temps s'exprimât autrement.”
[”Perhaps it would have been enough had the sealed train which
carried Lenin across Germany smashed up, or that Trotsky had been
detained in the United States or England for the Spirit of the times to
have expressed itself differently.”] Thus my critic takes us back
to the crude belief in the decisive role of the accident in history
— to the old quip that the history of the Roman Empire would have
been quite different if the shape of Cleopatra's nose had not been what
it was — and also back to Carlyle's idea of the ”hero in
history”, an idea perhaps indispensable to fascism, Stalinism,
and ... Gaullism. At this point I plead guilty : in relation to this
view of history I do reduce the role of the volonté
humaine and of le grand homme: I do not worship at their
temples. [1]The extremely subjectivist and voluntarist approach of most of my
critics allows them, of course, to ”reduce the role” of all
objective circumstances, and more specifically to ignore the impact of
economic processes, unprecedented in scope and momentum, upon the
political, cultural, and moral future of the Soviet Union. They see the
whole of the Russian revolution in terms of the bad faith or evil
ambition, or ”Manichean-like” moods of a few Bolshevik
leaders. These evil intentions or ambitions existed, of course, prior
to the five Five Year Plans; and they continue to operate into an
indefinite future. They enable one to ”explain” the whole
development of the Soviet Union and of world communism as a single
sequence of plots and conspiracies. How was it that Stalin first
imposed upon his party, by fire and sword, the doctrine of
”socialism in one country”, that he compelled the whole of
international communism to accept this doctrine, and that then he did
more than anyone else to contribute to the spread of communism to a
dozen countries? Was this perhaps a deep, and in a sense tragic
contradiction of Stalinism, as I have tried to prove?Nothing of the sort, say my critics. Stalin's fanatical preaching of
”socialism in one country” was either an irrelevancy or a
fraud meant to mislead the world, more probably a fraud and a
conspiracy. Like a certain type of anti-Semite who draws his
inspiration from the ”Protocols of the Elders of Zion”, so
the cold war propagandist at heart believes in the existence of some
”Protocols of the Elders of Communism” which one day will
no doubt be unearthed and revealed to the world. And then it will be
proven that all doctrines of Stalinism and the bloody struggles over
them were only so much make-believe designed to cover up the communist
conspiracy against the world.Some of the critics, especially Russian veteran Mensheviks and their
American pupils, dismiss the idea of a democratic evolution in the
Soviet Union or in the Communist Party, because any such idea fails to
take into account how inseparable the totalitarian outlook has been
from the Bolshevik Party: Bolshevik totalitarianism goes back allegedly
to Lenin's fight over the party statutes in 1903, the year when the
Russian socialists split into Bolsheviks and Mensheviks. Lenin then
demanded that only active participants in the party's underground work
should be recognized as party members, whereas the Mensheviks wished to
grant membership to ”sympathizers” as well. It was then, we
are told, that the issue was decided in advance, the issue which looms
behind the great upheavals of this century, behind the sequence of
revolution and counter-revolution, behind the massive reality of
Stalinist totalitarianism, behind the cold war, and behind the dangers
now threatening the world. All these have their origin in that idea
about party organization which Lenin embodied in his first paragraph of
the party statutes over fifty years ago. Thus half a century of Russian
and even world history is seen as springing from Lenin's head, from a
single idea in his brain. Should one really carry one's contempt for
”materialist determinism” as far as that?The cold war propagandist conceals, cleverly and not so cleverly,
his intellectual embarrassment or helplessness with the terms
”totalitarianism” and ”totalitarian”. Whenever
he is unable or mentally too lazy to explain a phenomenon, he resorts
to that label... . Denn eben wo Begriffe fehlen
Da stellt zur rechten Zeit ein Wort sich ein. 
Mit Worten lässt sich trefflich streiten, 
Mit Worten ein System bereiten.I should perhaps explain that I myself have occasionally applied
this term to describe certain aspects of Stalinism — I have been
doing this at least since 1932. But the term should be used carefully
and sparingly. Nothing is more confusing and harmful than the habit of
lumping together diverse regimes and social phenomena under one label.
Stalinists have often lumped together all their opponents as fascists.
The anti-Stalinist lumps together Nazis, fascists, Stalinists,
Leninists, and just Marxists, as totalitarians, and then assures us
that totalitarianism, being a completely new phenomenon, rules out even
the possibility of any change and evolution, let alone quasi-liberal
reform. A totalitarian regime, he claims, can never be reformed or
overthrown from inside; it can be destroyed from the outside only, by
force of arms, as Hitler's regime was.The fact is that nearly all modern revolutions (the Paris Commune,
the Russian revolutions of 1905 and 1917, the Central European
revolutions of 1918, the Chinese revolution of 1948-9) and even most
democratic reforms, have come in the wake of war and military defeat,
not as a result of purely internal developments; and this has been so
even in non-totalitarian regimes. Yet it would be a striking mistake to
treat totalitarianism metaphysically as a state of society's utter
immobility, or of history's absolute freezing, which excludes any
political movement in the form of action from below or reform from
above. is true, of course, that the chances of such action or reform
were negligible under Stalin. But they have grown enormously since the
critical moment, at the end of the Stalin era, when the crisis in
leadership coincided with the accumulation of changes in the depth of
society. In denying this, my critics imperceptibly abandon their
extreme opposition to determinism and themselves adopt an utterly
unrealistic brand of determinism. They, too, argue now that Russia's
political future is predetermined, only that it is not the economic and
cultural data — the fact that the Russian steppes and the wastes
of Siberia are covered by thousands of new factories, that Russia's
urban population has grown by over 40 million souls within a little
more than twenty years, or that proportionately more young people
attend schools in Russia than anywhere else in the world — it is
not these facts that can determine Russia's political future, in the
critics' view. It is the politics of the Stalin era and they alone
— the single party system, the absence of free discussion, the
leader cult, the terror of the political police, and so on — that
are going to decide the shape of things to come. Their
”determinism” amounts to this : politics is determined by
politics alone, it is self-sufficient and independent of other fields
of social life. To be sure, in my view the economic processes are of
primary importance, but they are closely connected with cultural
developments and the moral climate; they are dependent on the political
circumstances and themselves have a powerful impact on those
circumstances. The critics' pseudo-determinism is one-dimensional,
whereas the much abused and ”old-fashioned” Marxist
determinism has at least the advantage that it tries to grasp reality
as it is: multidimensional in all its aspects and dynamic.A certain type of ”left-wing” cold war propagandist, who
has not yet had the time to shed the infantile diseases of
ex-communism, approaches the issue from a ”Marxist”
angle, and turns against my analysis the ”weapon” of
economic determinism. A break with the Stalin era and a democratic
evolution, he argues, are excluded because they would go against the
class or group interest of the privileged and ruling minority of Soviet
society. The argument, be it noted, was first advanced partially by
Trotsky, although Trotsky cannot be held responsible for the
oversimplifications of the Trotskyites.The managerial and bureaucratic class, it is said, has a vested
interest in maintaining the economic and social inequality of the
Stalin era. It must therefore preserve the whole apparatus of coercion
and terror which enforces that inequality.This argument assumes (a) that there exists a high degree of something like class
solidarity in the Soviet bureaucratic and managerial groups; and(b) that the ruling group is guided in its policies by a strong
awareness of, and concern for, the distinct class interest of the
privileged.These assumptions may or may not be correct — in my view the
evidence is still inconclusive. A weighty argument against them is that
we have repeatedly seen the privileged and ruling minority of Soviet
society deeply divided against itself and engaged in a ferocious
struggle ending with the extermination of large sections of the
bureaucracy. The victims of the mass purges of 1936-8 came mainly from
the party cadres, the managerial groups, and the military officers'
corps, and only in the last instance from the non-privileged masses.
Whether these purges accelerated the social integration of the new
privileged minority, or whether, on the contrary, they prevented that
minority from forming itself into a solid social stratum is, I admit,
still an open question to me.In any case, we cannot say beforehand to what degree the privileged
groups may resist any democratic-socialist and egalitarian trend
emerging in Soviet society. It may be that they will defend their
privileges tooth and nail and fight any such trend with stubborn
cruelty. But it is at least quite as possible that the ”class
solidarity” of the privileged minority should prove weak, that
its resistance to the democratic-socialist trend should prove
half-hearted and ineffective, and that the first impulse for
quasi-liberal reforms should come, as it has already come, from the
ranks of the bureaucracy itself. This is not to say that one ought to
expect democratization to be brought about exclusively by reform from
above : a combination of pressure from below and reform from above may
be necessary. Yet at a certain stage of development it is the
quasi-liberal reform from above that may most effectively spur on a
revival of spontaneous political action below or create the conditions
under which such action may become possible after a whole epoch of
totalitarian torpor.But even if we assume, for the sake of the argument, that Soviet
bureaucracy does represent a single social and political interest, it
would still not follow that that interest must lie in the perpetuation
of the extreme inequality and oppression of the Stalin era. That
inequality was the direct outcome of a poverty of available resources
which did not permit not merely an egalitarian distribution but even a
distant approach to egalitarianism. As I have pointed out at greater
length in Russia After Stalin, a strong differentiation of
incomes was the only way in which Russia could develop her resources
sufficiently to overcome that initial poverty. In other words, the
privileges of the managerial and bureaucratic groups coincided with a
broader national interest. Yet, with the growth of productive forces,
which makes possible an alleviation of the still existing poverty in
consumer goods, a reduction of inequality becomes possible, desirable,
and even necessary for the further development of the nation's wealth
and civilization. Such a reduction need not take place primarily or
mainly through the lowering of the standards of living of the
privileged minority, but through the raising of the standards of the
majority. In a stagnant society, living on a national income the size
of which remains stationary over the years, the standard of living of
the broad masses cannot be improved otherwise than at the expense of
the privileged groups, who therefore resist any attempt at such
improvement. But in a society living on a rapidly growing national
income, the privileged groups need not pay, or need not pay heavily,
for the rise in the well-being of the working masses; and so they need
not necessarily oppose the rise.The privileged minority in the U.S.S.R. has no absolute
interest — it may still have a relative and a temporary one
— in perpetuating the economic discrepancies and social
antagonisms that were inevitable at a lower level of economic
development. Nor need they cling to a political regime designed to
suppress and conceal those antagonisms behind a
”monolithic” facade. Stalinism, with its orthodoxy, its
iron curtain, and its elaborate political mythology, kept the Soviet
people more or less in the dark about the scope and depth of its own
social divisions and cleavages. But with the phenomenal growth of
Soviet wealth these divisions tend to become softened; and the
orthodoxy, the iron curtain, and the elaborate mythology of Stalinism tend
to become socially useless. Only inertia may still keep them in being
for a time, but the inertia is bound to spend itself; and the open-eyed
observer of the Soviet scene can hardly fail to see that it is already
beginning to spend itself.More than at any previous time in history the political evolution of
nations depends now on international as much as on internal factors.
Nowhere in the world does the danger and fear of war strengthen
democratic institutions. It would be idle to expect that any democratic
trend in the U.S.S.R., which would, in any case, have to contend
against so much resistance, could be strengthened while a war-like mood
prevailed in and outside the Soviet Union. Any further growth of
international tension would most probably arrest the democratic trend
and stimulate a new form of authoritarianism or totalitarianism. Since
the Stalinist form has outlived its relative historic justification and
since danger of war enhances the already strong position of the armed
forces, that new authoritarianism or totalitarianism is likely to
assume a Bonapartist form. A Soviet version of Bonapartism would in its
turn increase the danger of war or perhaps make war unavoidable.This trend of thought seems to have come as a shock to my critic. M.
Aron whom I have already quoted poses a question: ”Pourquoi
un regime Bonapartiste signifierait-il la guerre? Un
général, qui s'efforcerait de liquider le terrorisme du
parti, serait normalement enclin à un accord avec
l'Occident.” [”Why should a Bonapartist regime mean
war? A general who tried to end the terrorism of the party would
normally incline towards the West.”] I re-read these sentences
and rub my eyes : is it possible that they should have been written by
a Frenchman, and a French ”political philosopher”?
”Pourquoi un regime Bonapartiste signifierait-il la
guerre?” Why indeed did it signify that? And why does the
assumption that a similar regime in Russia would also signify war seem
so far-fetched? Because a general ”liquidating the terrorism of
the party” should in fact be peacefully minded. But — the
question must be asked — was not the domestic terrorism of the
Jacobin party finally liquidated under Napoleon? And did not Napoleon
project in a sense that terrorism on to the international arena?No matter to what historical school we belong, Bonapartist or
anti-Bonapartist, pro- or anti-Jacobin, we cannot deny the seeming
paradox that, for all their domestic terrorism, the Jacobins conducted
their foreign policy much more pacifically than Napoleon did, who in
domestic affairs stood for law and order. Did not the warning against
carrying revolution abroad on the point of bayonets come from Danton
and Robespierre, the revolutionary terrorists? The Jacobins suppressed
by means of the guillotine the domestic tensions which the revolution
had brought into the open or had created, while Napoleon could deal
with those tensions only by finding foreign outlets for them. To be
sure, this was only one aspect of the problem — the other was the
attitude of counter-revolutionary Europe and England — but it was
a most essential aspect.It will now perhaps be seen why a Russian analogy to this is not
altogether unreal. A Russian general or marshal may install himself in
the Kremlin, ”liquidate the terrorism of the party”, and
have the most peaceful intentions towards the outside world. But his
intentions may carry little weight compared with the circumstances in
which he has assumed power. He will have inherited the most severe
strains and stresses from the Stalinist or post-Stalinist regime. There
will be tensions between town and country, between collectivism and
individualism in the countryside, and between Russia proper, the
Ukraine, Georgia, and the other outlying Republics. Stalinism had
almost continually suppressed these tensions by terroristic methods.
This was precisely why it was on the whole pacific in its foreign
policy. Stalin was preoccupied with his domestic problems; and his
manner of dealing with them was such that, never being quite free from
those preoccupations, he had to maintain an essentially defensive
attitude towards the outside world. In 1948-52, when Russia's immediate
military pre-eminence in Europe was undeniable, a Russian Bonaparte
might have issued marching orders to the Soviet army — Stalin,
despite his ”Manichean-like attitude” and ”messianic
fervour”, did not. Whatever the clichés of vulgar history
writing and propaganda may say about this, Stalin's domestic terrorism
and cautious, ”peace-loving” foreign policy were only two
sides of the same medal.If a Soviet marshal were to take power, he would do so under
conditions of domestic disorder and acute international tension= in a
more normal situation he would hardly have a chance. He would either
find the apparatus of Stalinist terrorism smashed or he himself would
have to smash it in order to justify himself. He would thus be deprived
of the old means for controlling and suppressing domestic tensions. The
dangerous international situation would hardly allow him to deal with
those tensions in a patient, slow, reformist manner. Instability and
insecurity at home would impart an explosive character to his foreign
policy — he would be impelled to find foreign outlets for
domestic tensions. Having started out with the establishment of law and
order at home and with the most peaceful intentions towards the outside
world, the Russian Bonaparte, like his French prototype, would be
driven into unpredictable military adventure, in part because he would
not be able to exercise domestic control through intense terrorism. He
would probably prove to be just as much more bellicose than Stalin and
Molotov and Malenkov as Napoleon proved to be more bellicose than
Robespierre and Danton.I admit that I remain a determinist on this point : the ultimate
course upon which a Soviet Bonaparte would embark would not greatly
depend on his assumed personal inclination to come to terms with the
West. He might be inspired by the most pacific intentions; he might
even have his Peace of Amiens (over the meaning of which generations of
historians would later argue); and yet he would in all probability be
driven to war, even ”aggressive” war, by a combination of
international and domestic factors.My critics' approach is more often than not dictated by their
prejudice against Bolshevism in all its phases, pre-Stalinist,
Stalinist, and post-Stalinist. From this prejudice they engage in
ludicrously belated apologetics for Tsardom and argue at length about
the progressive features of the Tsarist regime, which, if only it had
existed till now, would have taken Russia much further ahead on the
road of industrial and cultural progress than the Bolshevik revolution
has done. From the same prejudice they are prepared to hail the advent
of a Soviet Bonaparte. ”Anybody, anybody is preferable to the
Bolsheviks!” seems to be the maxim. Any talk about the
proletarian democratic element in Bolshevism — an element
strongly submerged yet genuine — seems to the critics to defy
reality. Yet the vision of the angel of peace dressed up in the uniform
of the Russian Bonaparte does not at all seem odd to them.The alternative is still between a democratic evolution of communism
and some sort of a military dictatorship. This, it seems to me, is the
basic, the long-term alternative. It has never occurred to me
that the historic choice will be made very soon after Stalin's death.
At any rate, the full ”liberalization” of the regime or the
full resurgence of the proletarian democratic tradition of communism
could not be a matter of a few months or even years. What the events
that followed immediately after Stalin's death could show and have
shown is that the alternative outlined above is real, and that the
impulses that may push the Soviet Union in one direction or the other
are already at work and are already in conflict with one another. The
long-term character of the prognostication frees me from the need to
reply any further to those critics who point to the events of a few
months to conclude that my forecast has been refuted. I can only
express mild surprise at this naïve disregard of the time factor.This is not to say that we can ignore the connection between the
short-term and the long-term developments, or that we have fixed our
eyes so exclusively on the latter that the former have caught us
unawares. My prognostication made allowance for the short-term
prospects as well. In Russia After Stalin I wrote that besides
the basic alternative — military dictatorship versus socialist
democracy — there was still the possibility of ”a relapse
into the Stalinist form of dictatorship”. I added: ”A prolonged
relapse into Stalinism is highly improbable” (p. 159 of the
English edition). The adjective ”prolonged”, italicized in
the original, pointed directly, though perhaps too laconically, to the
probability of a short relapse. Something like it has in the
meantime occurred and is still in progress — but even this
relapse has been only partial and vague and feeble, and it is being
carefully concealed.History has only opened a new chapter on Russia — let us
patiently watch her as she fills the pages.[1] Curiously
enough, a critic in The Times Literary Supplement (28 August
1953) thinks that I have ”tended to exaggerate the personal
elements inherent in Stalinism”.Isaac
Deutscher Archive
 Denn eben wo Begriffe fehlen
Da stellt zur rechten Zeit ein Wort sich ein. 
Mit Worten lässt sich trefflich streiten, 
Mit Worten ein System bereiten.
