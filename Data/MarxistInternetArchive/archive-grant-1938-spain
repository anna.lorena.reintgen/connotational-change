
Written: May-June 1938      
Source: Foreword to Leon Trotsky, The Lesson of Spain – The Last Warning!, original pamphlet 
Transcription/Markup: Emil 1998, Francesco 2013 
Proofread: Emil 1998, Francesco 2013 
Under the transparent disguise of the “peace alliance” agitation, the popular front(1) of Britain now makes its first steps towards entering the political arena. The Liberals cock their ears attentively, the
Labour Party heads strenuously oppose the project and the Communist Party, the
initiator of the agitation, is utilising every resource it possesses to bring
the popular front into being. It now becomes urgently necessary for British
workers to draw conclusions from the events in Spain, to examine the experience
of popular frontism as it appears in practice in the civil war in order to face
up to the problems of tomorrow.Leon Trotsky, in a series of articles and pamphlets on the Spanish
situation, has consistently pointed the road which the Spanish masses must
travel if fascism is to be conquered, [and] has called insistently for the only guide
along that road, the revolutionary workers’ party, to take up its position at
the head of the awakening Spanish masses. Trotsky concludes his pamphlet The
Revolution in Spain, written in 1931, with these words: “For a successful
solution of all these tasks, three conditions are required: a party; once more a
party; again a party.”The conditions for a workers’ victory over reaction, thus epigrammatically
summed up, are still unfulfilled: this is the lesson that must be brought to the
consciousness of the working class in Britain as in Spain.While the Spanish fascists openly prepared, with aid from abroad, to strike
their blow, the popular front government conspicuously failed to make that
counter preparation which would have destroyed the enemy swiftly and easily. The
army was left undisturbed in the hands of the reactionaries; under the noses of
the popular front government they consolidated a powerful basis among the Moors(2)
who, finding the chains of the new government no less galling than those of the
monarchy, fell an easy prey to Franco’s specious promises. On the other hand,
the workers were prevented by their reformist leaders from taking those measures
which would have frustrated the fascist plans – the setting up of workers’
militia and factory committees. When, in spite of the entreaties of their
leaders who begged them not to “provoke” the reaction, not to “antagonise” their
republican-capitalist partners in the popular front, the workers struck and the
peasants seized land, the government answered by arresting strikers, breaking up
workers’ meetings, censoring workers’ papers, shooting down peasants. Such is
the story related by the press dispatches and the official communications in the
months of popular front power leading up to the civil war. In this way the
popular front in the months preceding Franco’s uprising gagged and tied the
masses and drove numbers into the opposite camp to join the Moors in opposing a
“democratic” government that perpetuated their misery and oppression.Neither the popular front nor any other capitalist government could solve the
basic problems of modern Spain. Five million peasant families with insufficient
land, three million of them with no land at all, were squeezed by taxation and
were starving. Only the expropriation of the big landowners and the redivision
of the land among the poor peasants could relieve their famine. But this
solution was impossible under capitalism, because the whole structure of Spanish
banking rests on the land mortgages, so that the ruin of the big landowners
would mean the ruin of the capitalists and bankers. Only a Spanish “October”(3)
could, by dealing a death blow at the capitalist and landowning classes alike,
relieve the hunger of the perishing masses of the countryside.The conditions of the workers in the cities likewise presented a problem
insoluble under capitalism. Spanish industry, born too late to compete with the
cheap goods which a well-developed foreign industry is able to pour into
jealously guarded markets, is unable to find even a home market because of the
impoverished peasant population. Marx and Lenin taught that there is no way out
for the workers from their prison of meagre wages and growing unemployment
except by smashing down the barriers of capitalism and placing the control of
industry into the hands of the working class.In the first months of the civil war the workers of Spain spontaneously
sought this way out as an essential part of their struggle against reaction, for
it is not by military method alone that Franco can be defeated. Measures
necessary to rouse the masses, by giving them something to fight for, were put
into operation: factory, village and shop councils, and workers’ tribunals were
set up; a workers’ police force and militia were initiated. The beginnings of a
workers’ state thus came into being to conduct a revolutionary war against the
fascists, and existed side by side with the popular front, challenging its
authority and wresting away its functions.The Communist and Socialist parties came to the rescue of the capitalist
government thus threatened with extinction. They entered the popular front
government and Caballero(4), hailed as “the Spanish Lenin”,
became the prime minister. Step by step the conquests of the workers were
filched back in the name of the “defence of democracy”. The workers’ militia was
dissolved into the republican army, workers’ courts were eliminated, workers’
police corps disbanded.The same process went on in Catalonia where the POUM entered the coalition
government, proclaiming it the workers’ government. But the POUM also proclaimed
that the civil war was fundamentally a question of socialism versus capitalism,
a truth which undermines the very foundations of the popular front. Republicans
and Stalinists united in a vile campaign of calumny against the POUM accusing it
of being in the pay of Franco, driving it from government, suppressing its
propaganda and journals, arresting and imprisoning its leaders.At the beginning of May 1937, the government launched its provocative attack
on the workers to regain possession of the factories and buildings which were
under workers’ control. The resistance of the workers was overcome and full
control was regained by the bourgeoisie in the economic as in the political and
military fields.The alternatives that confront the Spanish masses today are on the one hand
the victory of Franco initiating a totalitarian regime or on the other hand the
now problematic victory of a “democratic” capitalist regime which in a spent and
devastated Spain can only rule by a scarcely veiled dictatorship. In either case
the chains will be more securely riveted on the limbs of the workers, peasants
and the colonial people, exhausted and cheated.From its very inception, the popular front disavowed in its programme not
only socialist but even semi-socialist measures. It was openly and admittedly
the guardian of capitalist property, dangling grandiose plans for future reforms
before the eyes of the people to distract their attention from present miseries.
The projected popular front in Britain is cut on the same pattern. “Any idea of
real socialism would have to be put aside for the present,” declares Sir
Stafford Cripps(5) in the Tribune (April 14, 1938) in pleading for a “democratic front” government. The Daily Worker supports
the Liberal candidate in a by-election as against the Labour candidate, and
sneered at Labour’s “astonishing ‘discovery’ that Liberals are not
socialist, as if Liberals ever made this claim.” (May 11, 1938).For Britain as for Spain, the struggle against fascism is the struggle for
socialism. The arms plans and the food plans, the spy scares and the air raid
precautions serve to warn the workers that the “peace” period draws rapidly to a
close. The American recession in industry spreads to Britain; in the first three
months of 1938 the decline of new capital issues, 33,000,000 as against 49,505,000
for the corresponding period last year, indicates the dimensions of the coming
industrial slump. The increased employment in the armaments industry and the
increased recruiting for the army serve for the time being to mask the growth of
industrial unemployment, and the shifting centre of gravity in national economy
is not visible in the general statistics of trade and industry because the
artificial stimulus of war preparations helps to conceal the real process of
economic breakdown. The disease that grips the vitals of capitalism in decay
produces as its symptom a feverish activity in certain branches of industrial
activity, accompanied by that false sense of well-being which must be recognised
as pre-war “prosperity”, the delirium before the crisis.As long as the pre-war boom continues and the British masses continue in a
comparatively passive state, the right wing bureaucrats of the trade unions and
the Labour Party oppose the popular front. When the masses start to move, as
they did in Spain and France, towards a militant socialist solution of their
difficulties, the Labour bureaucracy will not scruple to follow the example of
its counterparts in Spain and France, to put a bridle on the mass movement and
lead it into the safe bye-paths of popular frontism. If today they resist the
popular front, it is not because it is the open, treacherous abandonment of even
the pretence of socialism, but because they are quite satisfied with their own
status in capitalist society, because they fear the inevitable exposure to which
the taking of political power will subject them. Today they attack the Liberals
as non-socialists, tomorrow they will justify and defend them, and work hand in
hand with them in the “strike-breaking conspiracy” of the popular front, as
their brother reformists of the Communist Party are already doing.The Communist Party of Great Britain pleads for the popular front and
supports the Liberals on a programme of “arms for Spain”, “defence of democratic
liberties,” “economic and social advancement of the people.” The French popular
front in power supplied no arms for Spain; the French colonial slaves of North
Africa and Indo-China received as their share of “democratic liberties” –
bullets and prison sentences; the French popular front(6) government nibbled at the
concessions wrested from the ruling class by the direct strike action of the
French workers and frustrated their wage gains by currency manipulation. The
Liberals and “progressive” capitalists offer, in place of reforms, grandiloquent
plans for reforms.The past writings of the Communist Party leaders prove that they are well
aware of the treacherous role of the Liberals. Today they are able to exploit
the reputation for militancy which has been won by the work of party members in
the trade union struggle, in order to lead militant workers along the political
path mapped out by their paymasters in the Kremlin. Stalin and company are
prepared to sacrifice the socialist aspirations of the British working class for
the sake of a war alliance with the British bourgeoisie and to this end they
have ordered a popular front in Britain. The Communist Party heads leap to obey;
they flatly and brazenly contradict their arguments of a few months back, they
consciously and deliberately manoeuvre the workers into supporting a coalition
government with the class enemy, they blindfold the worker while the Liberals
prepare the dagger which will be plunged into his back.The Communist Party carries out its traitorous work with loud cries of
“Unity! Unity!” But the British working class constitutes in itself two-thirds
of the population, and would draw behind itself the majority of the lower middle
class if it pressed forward with a bold programme of socialist demands. The
workers have no need for an alliance with any section of the class foe, least of
all with the decayed, long ago bankrupt Liberals. They instinctively know that
unity is an all-powerful weapon in their struggle – unity of the working
class. The popular front is a caricature of unity. The genuine united front on
a class basis, binding together the workers, their organisations, their parties
on a programme of common struggle is the crying need of today, the only means of
defending those rights and privileges which the workers have won in generations
of struggle and sacrifice. The successful defence of concessions already gained
must lead inevitably to the campaign for full workers’ rights, to the struggle
for workers’ power.The experience of Spain is a warning and a lesson to the workers of the
world, above all to the British workers. Yesterday’s drama in Spain is being
rehearsed today in Britain. Tomorrow it will be enacted if the British workers
have failed to realise the nature of the tasks which history has placed before
them. And in preparing to tackle those tasks, the working class has need above
all, of “a party, once more a party; again a party”.
(1) The popular front or people’s front was a
name given to coalitions between workers’ parties and so-called liberal or
radical capitalist parties. The Communist International adopted the people’s
front policy in 1935, after the debacle of Hitler’s rise to power.
(2) The Arab population of North West Africa. They
struggled for years in Morocco for autonomy from Spanish rule. Where the popular
front government did nothing, Franco promised them independence.
(3) The Russian revolution took place in October
1917 on the old Russian calendar.
(4) Largo Caballero, leader of a left tendency in
the Spanish Socialist Party in the 1930s. Prime Minister from September 1936 to May 1937.
(5) Stafford Cripps, Labour MP from 1931, expelled
from the party for a period in 1939 for campaigning for a popular front. As Chancellor of the Exchequer 1947-50, he introduced an austere economic programme. Tribune was the paper of the reformist left in the party which Cripps helped to found in 1937.
(6) The alliance of the French Communist Party (PCF), the Socialist SFIO and the Radical and Socialist Party won the May 1936 legislative elections, leading to the formation of a government headed by SFIO leader Léon Blum. Léon Blum’s government lasted from June 1936 to June 1937 but eventually collapsed from the pressure of the bourgeois right wing within the front. 
Ted Grant Archive


Ralph Lee and Ted Grant
Lessons of Spain
 