L.S. VygotskyThis book has come about as the result of a series of minor and some major studies in
the areas of art and psychology. Three of my literary essays – on Krylov, on
Hamlet, and on the composition of the short story – form the basis of my analyses
here, as do a number of articles and notes published in various periodicals.
The different chapters of the present book will give only brief results,
outlines, and summaries of these studies. It would be impossible, for example,
to present an exhaustive analysis of Hamlet in a single chapter – this task alone
calls for at least one other book. The search for a way out of the precarious
confines of subjectivism has equally characterized Russian art scholarship and
Russian psychology during the years of my studies. This tendency toward
objectivism, toward a precise, materialistic, scientific approach to knowledge
in both fields, gave rise to the present volume. On the one
hand, scholarly studies of art have increasingly felt the need for a
psychological base. On the other hand, psychology, striving to understand and
explain behavior as a whole, is inevitably drawn to the complicated problems of
the aesthetic reaction. If we add to this the shift that is taking place in both
areas of study, the present crisis of objectivism, we shall understand the full
urgency of our subject. In fact, traditional art scholarship, whether
consciously or unconsciously, has always been based on psychological
assumptions. But the old popular psychology has ceased to be adequate for two
reasons. In the first place, it was still capable of nourishing every kind of
subjectivism in aesthetics, but objective trends require objective
presuppositions. Secondly, a new psychology
which is rebuilding the time-honored foundations of all the so-called
“sciences of the soul” is emerging. My purpose has been to reexamine
the traditional psychology of art and to try to delineate a new field of
investigation for objective psychology – to state the problem and merely suggest a
method and some fundamental psychological principles of explanation, and no
more.In calling this book “The Psychology of Art,” I did not wish to imply that it would present a
complete system or represent the whole gamut of problems and causes. My goal
has, been quite different: to offer not a system but a program; it was not the
whole range of problems but the central problem that I kept constantly in mind
and pursued as my goal.I shall
therefore leave to one side the controversies on psychologism in aesthetics and
on the boundaries separating aesthetics from pure art scholarship. I agree with
Lipps that aesthetics can be defined as a discipline of applied psychology, but
I shall nowhere pose this problem as a whole. I propose to remain content with
the methodological and theoretical laws of the psychological examination of art,
along with every other attempt, pointing out the essential importance of
finding a place within the Marxist doctrine of art. Here my guideline has been
the well-known Marxist position that the sociological view of art does not deny
its aesthetic consideration; on the contrary, it opens wide the door to it and
presupposes it, in Plekhanov's words, as its complement. Yet any aesthetic
discussion of art, to the extent that it does not wish to depart from Marxist
sociology, must constantly base itself on social psychology. It is easy to show
that even those art scholars who quite properly separate their own field
completely from aesthetics inevitably work into their treatment of the basic
concepts and problems of art psychological axioms that are uncritical,
arbitrary, and precarious. I share Utitz's view that art goes beyond the limits
of aesthetics and even has features that are fundamentally different from
aesthetic values, but that it begins with the aesthetic element and remains with
it to the end. It is also clear to me that the psychology of art must be related
to aesthetics as well, without losing sight of the line separating one area from
the other.It must be
said that in the new art scholarship, as in objective psychology, the basic
concepts and fundamental principles are in the process of being worked out, the
first steps are still being taken. This is why a work that arises at the
crossing of these two disciplines, and that desires to use the language of
objective psychology in dealing with the objective facts of art, must
necessarily be burdened by the circumstance that it remains ever at the
threshold of the problem, not penetrating deeply or ranging widely within it. I
have merely wished to develop the particular aspects of a psychological view of
art and to note the central idea, the methods of working it out, and the content
of the problem. If an objective psychology of art comes forth at the
intersection of these three lines of thought, the present work may well be the
small seed from which it will sprout.The central idea of the psychology of art, I
believe, is the recognition of the dominance of material over artistic form, or,
what amounts to the same thing, the acknowledgement in art of the social
techniques of emotions. The method of investigating this problem is the
objective-analytical method which proceeds from an analysis of art in order to
arrive at a psychological synthesis – the method of analysis of artistic systems
of stimuli. Along with Hennequin, I look upon a work of art as “a
combination of aesthetic symbols aimed at arousing emotion in people” 
and I attempt, by analyzing these symbols, to re-create the emotions
corresponding to them. But the difference between my method and the
aesthetic-psychological one is that I do not interpret these symbols as
manifestations of the spiritual organization of the author or his readers. I do
not attempt to infer the psychology of an author or his readers from a work of
art, since I know this cannot be done on the basis of an interpretation of
symbols.I shall attempt to study the pure and impersonal psychology of art without
reference to either the author or the reader, looking only at the form and
material of the work of art. Let me explain. From Krylov's fables alone we
should never be able to reconstruct his psychology, and the psychology of his
readers will differ between people of the nineteenth and the twentieth
centuries, and between groups, classes, ages, and individuals. But we can, by
analyzing a fable, discover the psychological law on which it is based, the
mechanism through which it acts; this we may call the psychology of the fable.
Actually, of course, this law and this mechanism have nowhere acted in their
pure form but have been complicated by a whole series of phenomena and processes
upon which they have impinged. We were also quite right in eliminating from the
psychology of the fable its concrete effect, just as the psychologist isolates a
pure reaction, sensory or motor, selection or distinction, and studies it as an
impersonal reaction.Finally,
the essence of the problem, I believe, is this: the theoretical and applied
psychology of art should bring to light all the mechanisms that operate through
art and should also provide the basis for all disciplines concerned with art.The task
of the present work is essentially one of synthesis. Müller-Freienfels has
very rightly said that the psychologist of art resembles the biologist
who can make a complete analysis of living matter and break it down into its
component parts but is unable to recreate the living whole out of these parts or
to discover the laws that govern this whole. A long series of publications has
undertaken such a systematic analysis of the psychology of art, but I know of no
work that has objectively posed and solved the problem of the psychological
synthesis of art. In this sense, it seems to me, the present attempt does
represent a new step and ventures to introduce certain new and hitherto unstated
concepts into the field of scientific discussion. The new ideas which I consider
my own in this book must of course be tested and criticized against other
concepts and the facts. Yet at this time they seem reliable and mature enough to
be presented in a book.The general aim of this work has been the
achievement of a degree of scientific soundness and sobriety in the psychology
of art, the most speculative and mystically unclear area of psychology. My
thought is expressed in those words of Spinoza which appear in the epigraph at
the beginning of the book. Like him, I have strived neither to be astonished,
nor to laugh, nor to weep – but to understand. 
Table of Contents |
Chapter 1: The Psychological Problem of Art