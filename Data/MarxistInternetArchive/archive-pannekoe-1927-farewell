
Source: ?
Published: 1927
Transcriber: Collective Action Notes (CAN)
HTML: Jonas Holmgren
In the person of Hermann Gorter, the revolutionary proletariat
has just lost one of its most faithful friends and one of its most notable
comrades in arms. He figured among the greatest experts in Marxist theory and
was one of the very few who, through conflicts and splits, remained invariably
devoted to revolutionary communism.Gorter was born on November 26, 1864, the son of a well-known
writer; upon completing his studies in the humanities, he was appointed
institute professor of secondary education. While still young he composed Mei
("May"), a work of poetry which had an explosive impact on the world of letters
in Holland and was immediately considered a masterpiece. The decade of the 1880s
was a veritable literary golden age; a whole constellation of writers and poets
arose during that period. Rebelling against the formal tradition which had been
erected into a canon of beauty, truth and the expression of feeling, this school
made the earth shake beneath the feet of Dutch language and letters. In the
1890s, however, the well progressively ran dry: everyone went their own ways.
Gorter, too, had to watch in amazement as the movement of the "eighties" was
struck down by sterility. He immersed himself in the great works of literature:
the Greeks of antiquity, the Italians of the Middle Ages, the English of the
early modern era, in an effort to discover the source of their power. He applied
himself to philosophy, he translated Spinoza, he studied Kant, but this did not
give him any answers or new impulses. He then turned to the writings of Marx,
and found what he was looking for: a clear understanding of social development
as the basis for men's spiritual production. Whenever a new class has erupted in
history, whenever its efforts have borne fruit, one witnesses a new energy, a
new feeling of power, and a new enthusiasm lead to a flowering in letters; and
this was certainly the case with the movement of which Gorter himself was part;
an intellectual buoyancy accompanied the take-off of capitalist development in
Holland. But Marx also showed him the limitations of the bourgeois development
which had taken place, he taught him to understand the class struggle. And from
that point on Gorter dedicated himself body and soul to the cause of the
fighting proletariat. In a series of articles entitled Critique of the Literary
Movement of the 1880s in Holland (1899-1900) he drew up a balance sheet of his
past in order to set forth the self-understanding which he had acquired during
that period. Towards the end of his life he turned once again to these
questions, examining the masterpieces of world literature in the light of social
evolution, but was unfortunately unable to bring his labors to a conclusion.Gorter joined the social democratic workers party of Holland
during the late 1890s. The clear simplicity with which he expounded its
principles soon made him one of the most popular orators of this rapidly growing
movement. He also published some excellent propaganda pamphlets. Later, however,
he entered into open conflict with the party leaders who, with the growth of the
movement, had increasingly gravitated towards reformism. Together with Van der
Goes and Henriette Roland-Holst, he founded the journal De Nieuwe Tijd ("The New
Era"), an organ of Marxist theory and principled critique. In regard to every
one of the crucial questions which were the most important issues of the day—the
agrarian question, education, the rail workers strike, elections—he was in the
front ranks of those combating opportunism. He was nonetheless a member of the
party's leadership for a while, but finally his entire group was reduced to a
minority faction by the reformist politicians and was denounced as a threat to
the party (1906). These confrontations (similar to those that were coming to a
head in every country) led him to focus his attention on forging close contacts
with German social democracy. Although he only rarely contributed articles to
Neue Zeit ("New Era"), the theoretical organ of German social democracy, Gorter
established friendly relations with Kautsky, relations which later cooled when
the two men went their separate ways but were never completely quenched. Nor was
this the only time that, as a result of their open minds and broad outlooks, as
well as because of the rigorous objectivity of their militant activities,
friends gained in the common struggle remained friends later, although the
course of the workers movement had turned them into political adversaries.The conflict within the party reached a point of no return during
the following year when some younger militants, Wijnkoop and Van Ravensteyn,
launched their own attack on the parliamentary practice of the party leaders and
began publishing an opposition weekly, De Tribune. After an extended period of
further debate, they were expelled in 1909 and founded a new party, the Social
Democratic Party, which later became the Communist Party. Gorter joined them and
became the party's most outstanding leader, although he was constrained to leave
to others the job of determining practical policy. He was also physically in a
quite weakened state. Gifted with an iron constitution, he was capable of
considerable efforts and, at the same time that he was teaching several
different classes, he indefatigably dedicated himself to political activity. But
when strife broke out in the ranks of the new party, he burned the candle at
both ends, sometimes working twenty-four hours a day; as a result he suffered
from exhaustion, which served to remind him of the limits of human powers.Gorter was a poet at heart, that is, a being who perceives
directly and with clarity what there is of immensity, of the truly universal in
the world, and knows how to express this in a language of total beauty or, to
put it another way, in a language of total truth. These years of tireless
activity and theoretical studies had the effect of leading him to increasingly
transcribe the new socialist concept of the world in terms of immediate
feelings. First, he brought out Ein klein heldendicht ("A Little Epic Poem"),
which describes the awakening of class consciousness in two workers, a man and a
woman; it was the epic poem of the proletariat, but in a more restricted
framework and in a more peaceful environment. Later, in 1912, Pan appeared in
its first version (it was to be significantly expanded later), which describes
in a symbolic form the emancipation of the human species through the class
struggle. Compared to Mei, which is a limpid, luminous vision of the world which
emerged from the illusions characteristic of carefree youth, Pan appears as the
epic poem, rich in content, with powerfully contrasted nuances, of the finally
mature Weltanschaung (World Concept) of conscious man.Then, after 1914, the black period of his life began; the decline
of the revolutionary workers movement affected his profoundly sensitive spirit.
Not allowing himself to become dejected, Gorter carried on the fight. He was
undoubtedly aware of the fact that the situation could not be otherwise but,
like so many of us, he was nonetheless consumed by sadness. When the war broke
out, bringing in its wake the collapse of social democracy, he published Der
Imperialismus, der Weltkrieg und die Sozialdemokratie ("Imperialism, the World
War and Social Democracy") where he proved that this collapse had its origin in
the reformism of the working class itself. The text was printed in German in
Amsterdam; the state of emergency, however, almost totally prevented its
circulation in Germany. But even during these moments of maximally accentuated
regression he did not lose his faith in the proletariat and its capacity for
engendering a new revolutionary movement. And when the Russian Revolution broke
out and, one year later, a revolutionary wave swept over Europe, he devoted
himself wholeheartedly to the movement. In Switzerland, where he was living for
reasons of health, he was in permanent contact with the Russian embassy; it was
there that he wrote his work Die Weltrevolution ("The World Revolution") in
1918. When the staff of the Russian embassy was expelled from Switzerland in
November 1918, he left with them for Berlin, where he made contact with the
emerging revolutionary movement. From then on he never ceased to cooperate with
the German communist movement; on repeated occasions he clandestinely crossed
the border to go to Berlin to participate in conferences and debates.His presence in Germany was rendered all the more necessary by
the fact that the German communist movement, which he supported with heart and
soul, was the origin of yet another disappointment even more serious than the
one he suffered in the Dutch party, because it was not expected this time, and
also because of the fact that the revolution which had begun was destroyed not
so much by the blows of an external power as by an internal weakness, a
deviation from its own principles. Gorter was one of the first people to discern
the danger of opportunism inherent in the Bolsheviks' tactics for western
Europe, whose erroneous nature he proved in an Open Letter to Comrade Lenin.
After a hazardous journey made all the more risky due to his poor health, he
arrived in Russia where, during the course of personal interviews with Lenin and
meetings with the Executive Committee of the Third International, he tried to
convince them of the errors of their ways. But it did not take long for him to
see and to understand why his efforts were in vain: Russia could not become
anything but a bourgeois State. From that moment, Gorter offered his services to
the KAP. On the occasion of the internal conflicts that tore the KAP apart, he
opted for the Essen tendency, to which he contributed a great deal as its
spokesman; however, he often had to admit that the Berlin tendency acted in an
almost exemplary way in practice and he assisted both fractions. Considering
their differences as of secondary importance and their quarrels as obsolete, he
made active contributions to efforts to achieve their reunification.His health seriously deteriorated during these later years. As a
consequence of repeated ordeals of overexertion, to which was added the terrible
blow of his wife's death in 1916, and due also to the depression he suffered as
a result of the disappointing evolution of the workers movement, he was
afflicted with chronic bronchial asthma, of a nervous origin, which physically
exhausted him. But the power of his spirit raised him to an ever higher state of
lucidity and an increasingly broad and penetrating vision of the world. Gorter
worked tirelessly to give expression to the new beauty which he felt; he plunged
into an in-depth study of Marxism, the great poets of the past, communism and,
in his final days, he said that he felt capable of creating an even more perfect
work than anything he had written before. But his illness suddenly took a turn
for the worse during a visit to Switzerland, and he died during his return to
Brussels on September 15, 1927.Gorter was a force of nature, full of youthful freshness, a being
in total harmony both physically as well as morally. During his youth he
ardently participated in almost every sport; cricket, tennis and sailing held no
secrets from him and, even during his last years, he proved to be an
indefatigable walker. Every page of his poetic work is testimony to the depth of
his love for Nature. He could plod for hours, in fall and winter, across
deserted beaches, absorbed in the infinite beauty of the waves and the strand;
in Switzerland he spent entire days exploring mountains, eager for the solitude
of snow-covered summits. A classicist and man of letters by his natural gifts, a
notable expert in philosophical matters, he was later capable of keeping abreast
of the difficult questions of the natural sciences in order to develop his
concept of the world from every angle. Such a man necessarily was compelled to
subscribe to socialism in order to be in perfect harmony with the world.
Henceforth he devoted himself to the working class and to communism. His poetic
work, the most complete expression of his being, unfortunately can only be read
by workers who understand Dutch. But among the Dutch workers, there are many who
profess a fervent admiration for Gorter's poetry. In this recent period of the
workers movement, Gorter stands out as a luminous figure, an example of the new
humanity in the course of its transformation. [1] A. Pannekoek: "Ein
K#228;mpferleben. Abschied von Hermann Gorter," Kommunistische Arbeiter Zeitung
(Berlin), VIII, September 1927, No. 74 and Kommunistische Arbeiter Zeitung
(Essen), VI, 1927, No. 9. Last updated on: 2019-05-19