
Published: International Socialist Review, vol. 12, no. 9, March 1912.
Translation: William E. Bohn.
Transcription/Markup: Micah Muer, 2017.
The elections to the German Reichstag have resulted in a great victory for
the Social Democratic Party. In 1907 the Socialists entered the national
assembly with 43 members, and by means of victories at by-elections this number
was raised to 53. The great electoral battle of 1912, however, gives us quite a
different story to tell. The first ballot, on January 12, gave the Socialists 67
seats, of which 25 represent newly conquered districts. The party of the
working-class was, moreover, left as contestant in 121 reballotings. The second
elections have netted 43 seats. Thus the Social Democratic group numbers 110
members in the new chamber.Even more important than the number of seats is the number of votes
registered. The popular vote of the Social Democratic Party has increased by a
million since 1907; it has grown from three and a quarter millions to four and a
quarter. At the present moment more than a third of all voters (34.8 per cent)
support this party. If the German electoral law were a really just and
democratic one, giving the same influence to every vote cast, a third of the
representatives would now be Socialists. But this is not the case. The electoral
districts have not been altered since 1871. Since this date the great cities and
the industrial regions, in which Socialism is strongest, have developed
tremendously, while the rural districts have become partly depopulated. A simple
comparison will show the injustice of the system which has resulted: Comrades
Ledebour and Zubeil were elected in Berlin by 142,502 and 162,717 votes,
respectively, while in the rural districts representatives of the Conservative
and Centrist parties were elected by fewer than 10,000 votes.The enormous increase in the number of Socialist votes was universally
expected. The last Reichstag had done all in its power to embitter the masses of
the people. In 1907 Premier Bülow promised a liberal regime and thereby
gathered about his standard the majority of the small bourgeoisie, the
government employes [sic], the middle-class, the
intellectuals, all those who really cared little or nothing about the outcome of
the election. All of these joined themselves with the Conservatives and,
although the number of Socialist votes increased from three to three and a
quarter millions, the Socialist group in the Reichstag was cut down from 81 to
43. There was great rejoicing throughout the bourgeois world. The Kaiser
himself, speaking in poetic phrases from the balcony of his palace, referred to
the Social Democracy as a power which had been "trampled underfoot." The
government and the parliamentary majority thought now that they could be as
arbitrarily reactionary as they pleased. In the beginning Bülow governed
with the support of a majority made up of Conservatives and Liberals. The
Conservatives represented, of course, the landed aristocracy. The Liberals
included the National Liberals, representatives of "big business," and the
Free-Thinkers (Freisinnigen), who represented commerce and the solely financial,
stock-exchange interests, which in the past had opposed militarism and the whole
reactionary policy of the government. The new government was, naturally enough,
anything but liberal. A new law to regulate organizations and public meetings
was passed, but it left intact nearly all the police restrictions for which
Prussia is famous. In fact, it cut off some of the liberties previously enjoyed;
the Poles were forbidden to employ their mother tongue in public meetings and
persons under eighteen years of age were denied the privilege of taking part in
political organizations or gatherings. All this was openly designed to hinder
the labor movement. The Liberals of all shades of opinion supported this
law.Then came the new tax laws. There were constantly increasing demands upon the
national treasury. In 1908 the cost of maintaining the army amounted to
854,000,000 marks, that of maintaining the navy to 339,000,000, while military
pensions reached the figure of 146,000,000. During the two years from 1908 to
1910 the imperial debt increased from 4,000,000,000 marks to 5,000,000,000. The
income of the nation was not keeping pace with the expenditures. If the constant
increase in the imperial debt was to be stopped a greatly increased revenue
would have to be provided. In 1908 the government introduced a new revenue law.
Under its provisions 400,000,000 marks were to be raised annually from a tax on
such articles of consumption as beer, tobacco, whiskey, gas, and electricity,
and 100,000,000 from an income tax. The Liberals were willing to vote for these
provisions despite the fact that, contrary to the Liberal program, the money was
to be raised from the poverty-stricken masses of the people. But the
Conservatives would not agree to the income tax provision. They joined
themselves to the Centrum (the Catholic party), cut loose from the Liberals, and
brought about the downfall of Bülow. The new government bloc dropped the
income tax, added to the law provisions for revenue on matches, gas mantles,
coffee, and tea, and finally forced it through the Reichtstag. Needless to say,
it aroused the greatest discontent. The Liberals now made the most strenuous
opposition to the "Blue-Black" bloc, thinking thereby to win back the favor of
the electorate. When, however, in 1910-11 the revision of the sickness and
accident insurance law was undertaken all the bourgeois found themselves in
accord. The changes were made in the direction of depriving the working-class of
the large share it had previously had in the administration of the law. And the
Liberals as well as the Conservatives and Centrists were in favor of these
changes.So the last Reichstag has produced nothing but increase of burdens and
decrease of rights. Naturally enough the German workers looked forward to the
election as a grand opportunity to settle accounts with their rules. They knew
that a large proportion of the people who had been deceived in 1907 would now
vote the Socialist ticket. And in the bourgeois parties there was everywhere in
evidence the apprehensive fear of the sinner approaching his punishment.This sketch of the political conditions which determined the result of the
election throws little light, however, on the real meaning of what has happened.
Anyone who supposes that the policies of the last Reichstag have been finally
eliminated deceives himself. On the contrary, the bourgeois parties will keep to
their old course. For this course is not the result of passing temporary
impulse, but rather an inevitable result of the development of German capitalism
in the direction of imperialism.Imperialism is the modern form of capitalism. It has appeared during the past
ten or twenty years in all nations. Capital knows no country. The capitalist is
not content to exploit the workers of his own land and merely export products to
foreign shores. He does not find adequate conditions for development in his
country. Therefore capital is exported to foreign parts, especially to
regions still agrarian in character, to build railways, harbors, or irrigation
plants, to lay out plantations, to open mines or factories — in short, to
exploit the natives in any way possible. To this end it is necessary to conquer
the natives or to make the native government dependent on the enterprising
capitalists. This process necessarily involves competition among the various
capitalist governments. Imperialist politics is the politics of force, of
conquest, of colonial war. It results, therefore, in growing danger of war
between civilized nations and the necessity of increased armaments. During the
last ten years all the great nations have increased their armies, and especially
their navies, at a tremendous rate. The budgets devoted to this increase are
being rapidly swelled by hundreds of millions, and constantly growing burdens of
taxation are laid upon the shoulders of the masses of the people. There is no
money left for social reforms. On the other hand, the spirit of violence and
intolerance which is developed toward the people of other countries leads to
high-handed brutality at home. Thus the capitalist mind in time reaches a point
at which it will hear of no concessions to the working-class. Moreover, the fear
of a revolution is diminished. The participation of the entire world in
capitalist production gives new life to industry. Prosperity rules, and the
capitalists fancy their mastery of the situation firmer than ever. Individual
business men combine in trusts and associations which are able to resist any
demands of the workers for increased wages. At the same time prices mount
higher, in part because of the combination of capitalists, in part because of
the gradual introduction of modern industry into lands which had hitherto
furnished raw materials and a market for manufactured products. This is, then,
the sum total of the result of imperialism: colonial wars, increased armaments,
danger of war, taxes, high cost of living, reaction and suppression of the
workers, neglect of social reform.Since 1890 Germany has been on this downward road. Kaiser Wilhelm II, as the
trusted representative of big business, has been the most energetic advocate of
the imperialist policy. In numerous public addresses he has urged the necessity
of a strong navy. But in German bourgeois circles there has long been little
understanding of this policy. There has been dissatisfaction with the burdens,
disgust at the horrors revealed in the colonial administration, and exasperation
at the tyranny and reaction at home. The demands of big business were, of
course, ruthlessly asserted. Members of the Reichstag scolded at the shameless
demands of the government and thought with fear and trembling of the moment when
they would have to face their constituents, but after all did not dare to vote
against the military and naval appropriations. The Kaiser was held up to
ridicule when he visited Jerusalem and called on Abdul Hamid in order to secure
for German capital entrance into Asia and, more especially, to gain for it the
Bagdad railway concession. University professors scolded the government and,
occasionally, even gave a word of praise to the Social Democracy for its proud
and consistent opposition to all this. The theories, ideals, and party programs
of the older, undeveloped, home-keeping capitalism still dominate the thinking
of the German bourgeoisie and the German intellectual classes. In the meantime
the government and the great bank-capitalists have already put in practice the
imperialist policy.This state of affairs could not go on indefinitely. Imperialism is a
necessity for the possessing and ruling class. Without it this class would be
suffocated in its own surplus product and go down in a great crisis. Imperialism
affords the only barrier against the rising tide of Socialism. Colonies promise
the business man new markets in foreign parts. Colonial mines and railways offer
the landholder and money-capitalist new avenues for investment and speculation.
Scholars, for their part, are given the opportunity to explore and study
hitherto unknown regions. People of intelligence and conscience have pointed out
to them the study of bearing "the white man's burden" — that is, of
carrying "civilization," or capitalism, to the barbarians of Asia or Africa. And
to the bourgeoisie as a whole is given a new world ideal, the vision of its own
nation standing dominant among all the peoples of the earth. And this vision, it
is hoped, will do something to inspire those who have found themselves powerless
in the path of the overpowering ideals of humanity and world brotherhood
represented by the Social Democracy.All of this, naturally, was bound to take definite political form. Herein
lies the significance of the election of 1907. An intelligent, experienced
financier, Dernburg by name, was placed at the head of the colonial office. In
the course of a discussion of the appropriations for a colonial war he came into
collision with the Centrists. The liberal bourgeoisie, inspired by its long
standing hatred for the clericals, supported Dernburg, had its interest in
colonization aroused, and suddenly became conscious of the surpassing beauty and
glory of world-politics. As a result, all the wise professors entered the
campaign against the blacks and the reds. A wave of enthusiasm carried all the
philistines along in the imperialist flood, and the advance of Socialism was
checked for the moment. The election of 1907 was a victory of the newly aroused,
youthful spirit of imperialism, a victory of the imperialist illusion.The five years which followed were sufficient to dispel this illusion so far
as the mass of the German people were concerned. The great body of the citizens
came to know what imperialism really is. They have discovered that for them it
has nothing to offer but oppression and heavy burdens. With this knowledge has
come the beginning of a rebellion against it. Not only the working-class, but
also the small business people, the farmers, and government officials have
suffered. Thus it has come about that this new form of capitalism tends much
more strongly than the old to drive these sections of the middle-class into the
Socialist movement, and thereby to undermine capitalism itself. For since the
last shreds of Liberal opposition went down with the Bülow bloc the only
party that consistently opposes the imperialist tendency is the Social
Democracy. The election of 1912 is, therefore, the answer to the election of
1907. The result is the defeat of imperialism unmasked, the beginning of the
revolt of the masses against it, the natural result of the imperialist
disillusionment.Naturally this does not mean a change in the policy of the German government.
The only change in the aspect of affairs lies in the fact that the Social
Democracy has grown so strong that it is no longer possible to have two
capitalist parties, one Conservative-Liberal and the other
Conservative-Clerical. Supporting all governmental action there must now stand
one Conservative-Liberal-Clerical majority. All capitalist parties must
co-operate in order to furnish to the big business interests the cannons,
dreadnoughts, taxes, and laws against the working-class. If any one of these
parties deserts the bloc, the opposition will have a majority and the
government's game will be up. But the imperialist policy is firmer in the saddle
than ever. The Social Democracy is a powerless minority in the Reichstag. It can
do nothing but protest. Capital can safely rely on the capitalist majority to
grant all that is demanded. Appropriations for increases in the army and navy
and limitation of the right to strike were promised by the government before the
election; it is, then, to be taken for granted that those who voted against
Socialism were in favor of these things. With firm step German imperialism goes
on to fasten its hold on Africa, China, and Turkey and to prepare for the great
conflicts of the future, especially for a possible conflict with England. But
the further it pursues this policy the more it will rouse the opposition of the
masses. Stronger resolutions on the part of the ruling capitalist power,
constantly rising rebellion in the working-class — this means a fiercer
class-struggle. Harder battles than have yet been fought are what the immediate
future has in store for the German working-class.Imperialism has not only changed the policies of the ruling capitalist class;
it has also transformed the tactics of the working-class. It intensifies the
parliamentary struggles; but parliamentarianism is inadequate to the gigantic
conflict which has been entered upon by bourgeoisie and proletariat. Twenty
years ago it seemed that parliamentarianism offered a straight and regular road
to revolution. The increasing strength of the Socialist group in the Reichstag
forced the bourgeoisie to grant a number of reforms; and wherever universal male
suffrage prevailed there was a basis for believing that in the course of time
education and organization of the masses would secure a majority in parliament.
But these expectations have been transformed into dreams by the growth of
imperialism. The method of electing the members of the Reichstag has become so
undemocratic that a Socialist majority is unthinkable. If the proletariat is
ever to achieve a political conquest of the state and thus overthrow the
capitalist regime, it must first achieve more political rights by means of
nonparliamentary means, by the action of the masses themselves.Under imperialism the working-class cannot win in parliament any further
reforms, any greater rights, any diminution of oppression or want. On the other
hand, the proletariat has to defend itself against increased burdens and
tyranny. The miseries under which it suffers at present cannot be abolished by
parliament, for they result only in part from the actions of parliament. In
reality the real power of parliament is decreasing at the present time. The
policies of the state are more and more shaped behind closed doors by a small
group of magnates and ministers. The will of this small group cannot be
successfully opposed with parliamentary resolutions; the only force that can
make them sit up and take notice is the demonstrated power of the masses
themselves. The dissatisfaction with the tax measures passed by the Reichstag
was expressed in the election. In fact, the election was chiefly useful as a
demonstration of four and a quarter millions of people against the capitalist
parties. But against the high cost of living nothing can be done with the
ballot. Spontaneous uprisings like those which occurred in France and Austria
must voice the feeling of discontent in cases like this.Above all does the constantly increasing danger of war spur the people on to
action. Wars are not instituted by parliament, but by the government and the
capitalists who stand behind it. The growing opposition of interests of the
various governments constantly increases the international tension; again and
again new reasons for fighting are discovered, as recently in Morocco, and there
is danger of an immediate declaration of war. But a war resulting from such a
cause would be the greatest calamity which could overtake the world, and
especially the working-class. The transportation of all able-bodied men to the
borders to butcher one another by millions, complete demoralization of industry,
crises and starvation everywhere, the destruction of all civilization,
degeneration into barbarism — only a world revolution could put a stop to
the horrors of a world war. For the workers, for all the population beyond a
small number of great capitalists, war would be the most terrible misfortune,
and they stand ready to risk everything to prevent it. But this is impossible by
means of parliamentary methods. The only adequate means lies in the action of
the masses themselves.Thus it comes about that imperialism forces the working-class to rise in its
might, either to force from the ruling class new political rights or to fight
against war. It is no wonder that during the past five years mass actions have
become more and more common in Germany. "Mass action is the legitimate offspring
of imperialism," said recently the Leipziger Zeitung, a paper which formerly led
in the warfare of the principles of capitalism and now leads no less in the
revolutionary fight against imperialism. Mass actions begin with mere meetings
and demonstrations, developing sometimes into huge street demonstrations, like
those which played a part two years ago in the fight for a new electoral law in
Prussia; and as the last, and most powerful, weapon the working-class has at its
disposal the general strike.The German working-class has shown the workers of the world how
parliamentarianism can be made a weapon in the revolutionary struggle.
Parliamentary activity still serves to carry the truth about capitalism and its
tyranny into the smallest villages and to weld the workers into powerful united
body. But parliamentary activity is no longer viewed in Germany as the cure-all
of the Socialist movement. During the past ten years there has come about a
great change in the thinking of the German working-class. This fact has not been
universally observed because it is not evident in the speeches of the Socialist
parliamentarians, who are mostly reformists. It is much more evident in the
press; in numerous Socialist papers of the more radical sort it is being
remarked with increased frequency that the conquest of power is not to be
brought about by the use of the ballot alone, but that the masses themselves
must enter into the conflict. And among the workers of the great cities one can
see developing, slowly but none the less certainly, the readiness to employ new
methods in the great conflict. Naturally enough, this has brought about internal
struggles within the party. The heated discussions at the last party congress
grew out of the fact that the executive committee had not been sufficiently
prompt in calling upon the masses of the people to demonstrate against the
threatened war. Since 1905 the party has recognized in the general strike an
important weapon to be used in warding off attacks on the imperial electoral law
and in winning new political rights. As to its use in other cases, nothing has
as yet been formally decided. The party leaders fear that an official
recognition of it as a weapon against war would expose the party to legal
persecution and turn one of the strongest national prejudices against us. But it
is clear that in time of actual danger of war, when it is a matter of life or
death, of destruction or victory, the action of the masses will be determined,
not by party resolutions, but by the deepest impulses of the people. And among
the people there is dawning, especially since the Morocco affair, the
determination not to be led to the field of slaughter, but rather to resist with
any means which offer. When recently Rosa Luxemburg declared in a mass meeting
in Berlin that in case of a threatened war the workers must employ all means to
prevent it, even the mass strike, her words were greeted by a demonstration
which lasted several minutes. But the central organ of the party, Vorwaerts,
omitted just these words from its report of the meeting. This fact reflects in a
strong light the tendencies of German Socialism.Thus it is evident that the tactics and thinking of the German workers are
adapting themselves to the new forms of capitalism. Here the tremendous power of
international Socialism stands opposed to a capitalism which is inferior only to
that of America in strength, in unity of organization, in degree of development,
and in ruthlessness. Imperialism will continue to control our national policies;
the elections have served to strengthen its grip. We face new and terrible
struggles. But they will not take place exclusively in parliament; the masses
themselves will act directly to oppose the oppressions and dangers of
imperialism until it is finally and completely defeated, until the proletariat
is victorious, until we have the revolution.— Translated by William E. Bohn. 
Left Communism Subject Archive |
Pannekoek Archive
