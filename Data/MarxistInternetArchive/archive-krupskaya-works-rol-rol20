
In January 1916 Vladimir Ilyich started to write his pamphlet
on imperialism for the Parus Publishing House. He attached
tremendous importance to this question, believing that a real,
profound appraisal of the present war was impossible unless the
essence of imperialism, economically as well as politically, was
made perfectly clear. He therefore undertook the job
willingly. In the middle of February Ilyich had work to do in the
libraries of Zurich, and we went there for a week or two, and then
kept putting off our return day by day until, in the end, we
stayed there for good, Zurich being a livelier place than
Berne. There were a large number of revolutionary-minded young
foreigners in Zurich, besides working-class elements; the
Social-Democratic Party there was of a more Leftist tendency, and
the petty-bourgeois spirit seemed to be less in evidence there.
We went to rent a room. At one place the landlady a Frau
Prelog, looked more Viennese than Swiss. This was due to her
having been employed for a long time as a cook in a Vienna
hotel. We arranged to take a room at her house, but the next day
she found out that her old lodger was coming back. He had been
lying in the hospital with a broken head, and had now
recovered. Frau Prelog asked us to find a room elsewhere, but
offered us to come and have our meals with her at a fairly cheap
price. We ate there for about two months. The food was plain but
sufficient. Ilyich liked the simplicity of everything there – the
fact that coffee was served in a cup with a broken handle, that we
ate in the kitchen, that the talk was simple, talk not about the
food, not about how marry potatoes had to be used for this or that
soup, but about matters that were of interest to the
boarders. There were not many of them, though, and they kept
changing. It was not long before we realized that we had landed in
peculiar company, the "lower depths" of Zurich. One of the diners
was a prostitute, who spoke without reserve about her profession,
but who was much more concerned about the health of her mother and
the kind of job her sister would find than about her own
profession. A night nurse bearded there for several days, then
other boarders began to turn up. Prelog's lodger did not have much
to say for himself, but from what he did say it was clear that he
was almost a criminal type. Our presence embarrassed no one, and,
if the truth be told, the conversation of these people was more
"human" and lively than that heard in the decorous dining rooms of
a respectable hotel Patronized by well-to-do clients.
I urged Ilyich to have his meals at home, as the crowd we
dined with was likely to get us into a pretty mess one
day. Certain aspects of Zurich's underworld, however, were not
without interest.
Later, when reading John Reed's Daughter of the
Revolution, I liked the way he described the prostitutes. He
depicted them not from the angle of their profession or of love, but from the angle of their other interests. Usually writers give little attention to social conditions when describing the underworld.
Later, in Russia, when Ilyich and I went to see Gorky's
Lower Depths at the Art Theatre – Ilyich was very keen on
seeing the play – he was repelled by the theatricality of the
production, the absence of those details of everyday life which
gives the touch of authenticity and concreteness.
Afterwards, on meeting Frau Prelog in the street, Vladimir
Ilyich always greeted her in a friendly manner. And we were
always meeting her, as we had taken a room nearby in a
side-street, in the home of a shoemaker by the name of
Kammerer. The room was not exactly a bargain. It was a dingy old
house, built, I think, way back in the sixteenth century, and had
a smelly courtyard. For the same money we could have rented a much
better room
but we liked our hosts. It was a working-class family with a revolutionary outlook, who condemned the imperialist war. The place could be truly called an "international." Our landlord and his family occupied two rooms, one room was occupied by the wife and children of a German baker who was in the army, one by an Italian, another by some Austrian actors who had a wonderful brindled cat, and the last room was occupied by us Russians. There was not a hint of any chauvinism, and once, when a whole women's international had gathered round the gas-stove, Frau Kammerer exclaimed with indignation: "The soldiers ought to turn their weapons against their governments!" After that Ilyich would not hear of moving to another place. Frau Kammerer taught me many useful things-how to cook satisfying meals cheaply and quickly. I learned other things too. One day the newspapers announced that Switzerland was having difficulty in importing meat, and the government therefore appealed to citizens to abstain from meat two days in the week. The butcher-shops still sold meat on "fast" days. I had bought meat for
dinner, as usual and, standing by the gas-stove, I asked Frau Kammerer what check there was as to whether the citizens were obeying the appeal, whether there were any inspectors going around the houses. "There's no need to check up," Frau Kammerer said, surprised at the question. "Once it has been published in the papers that there are difficulties, what working man, other than a bourgeois, maybe. will eat meat on meatless days?" Seeing my embarrassment, she added gently: "But that doesn't apply to foreigners." This intelligent proletarian approach won Vladimir Ilyich completely.
Going through my letters to Shlyapnikov for that period, I
found one dated April 8, 1915. It is characteristic of the mood
of that time. "Dear friend," I wrote, "I have received your
letter of April 3, and was somewhat relieved. It had been
painful to read your angry letters in which you had spoken
about going to America and had been ready to blame everything
and everybody. Correspondence is a beastly thing, it lets
misunderstandings pile up one on top of the other.... In my
missing letter I wrote in full detail why Grigory could not be
dragged either to Russia or to your parts. He took your reproach
about his not having moved to Stockholm very much to heart. We
can't afford to undermine the editorial board of our Central
Organ and our foreign headquarters in general. The C.O., now
more than ever, has succeeded in gaining ground
during the war by dint of tremendous effort. Its editorial board has played no small part in the international. We can say that now outright, setting aside unnecessary modesty. The Communist would not have come out either had it not been for the editorial staff of the Central Organ. And the talk, the cares, and the worries it had cost! Still more so Vorbote (organ of the Zimmerwald Left). If we are going to deplete the editorial staff we shall have no one left to do the work. It isn't going to be so easy to get together a new editorial staff. There was Bukharin, with suggestions for sending him here and there; there was talk of his going to Cracow, then to Berne. Nothing could be done. Two people are not enough, and here you want to take one of them away. If you strip the foreign base there will he nothing to send across. Grigory sometimes gets fed up to the teeth with life abroad and then he doesn't know what to do with himself. And your reproaches only make things worse. Looking at it from the point of view of the usefulness of the work as a whole, Grigory should not be touched. There was a question of the whole editorial board moving over, but this brought up the question of money, of international influence, of police regime considerations. He put the question of money to the 'Japanese' point-blank, and they said they had none. In Stockholm life is much dearer. Here Grigory is I working in a laboratory, we have libraries here, and that means a chance to earn at least something by writing. The problem of making a living will become more acute here, too, in the near future.
"As to Ilyich's supposed enthusiasm for emigrant affairs, the
reproach is unfounded. He does not engage in them at
all. International affairs claim more of his time and attention
than ever, but that is unavoidable. True, his enthusiasm now is
'self-determination of nations. In my opinion, if we want to
make good 'use' of him now, we ought to insist on his writing a
popular pamphlet on the subject. The question is by no means one
of academic interest at the present time. There is great
confusion on this question among international Social-Democracy,
but that is no reason for putting it off. We had a dispute with
Radek here last winter on the subject. Personally, that discussion
was of great benefit to me." And I set forth on several pages the
gist of this discussion and Ilyich's point of view.
Our life in Zurich was, as Ilyich described it in a letter
home, "slow-poky" and somewhat detached from the local colony. We
worked hard and regularly at the libraries. Every afternoon young
Grisha Usievich – he was killed in 1919 during the Civil
War – dropped in for half an hour on his way home from the
emigrants' restaurant. Zemlyachka's nephew, who later went mad
through starvation, used to visit us occasionally in the
mornings. He went about in such ragged muddy clothes that he was
refused admission to the Swiss libraries. He tried to catch Ilyich
before he went to the library saving that he had to discuss
certain questions of principle with him. He got on Ilyich's
nerves.
We started to leave the house earlier to take a stroll along
the shore of the lake and have a chat before going to the library
Ilvich spoke about the book he was writing and the various
thoughts that occupied his mind.Those of the Zurich group we saw most often were Usievich
and Kharitonov. Others I remember were Uncle Vanya (Avdeyev), a
metal-worker, Turkin, a Ural worker, and Boitsov, who later worked
in the Central Political Education Department. I also remember a
Bulgarian worker, whose name I have forgotten. Most of the
comrades of our Zurich group worked in factories, and were very
busy; group meetings were comparatively rare. But then the members
of our group had good connections with the workers of Zurich; they
stood closer to the life of the local workers than was the case in
other Swiss towns (with the exception of Chaux-de-Fonds, where our
group was even closer to the mass of workers).
The Swiss movement in Zurich was headed by Fritz Platten. He
was secretary of the Party, and joined the Zimmerwald Left
group. He was the son of a worker, a simple ardent fellow, who had
great influence over the masses. The editor of
Volksrecht, Nobs, joined the Left Zimmerwaldians too. The
young emigrant workers (of whom there were many in Zurich), headed
by Willi Munzenberg, were very active and supported the Lefts. All
this created intimate ties with the Swiss labour movement. Some
comrades, who had never lived among the political emigrants
abroad, are under the impression that Lenin had expected big
things of the Swiss movement and believed that Switzerland was
capable of becoming almost the centre of the future social
revolution.
This is not so, of course. Switzerland never had a strong
working class; it is mainly a country of health resorts, a small
country living off the crumbs of the powerful capitalist
countries. The workers of Switzerland, by and large, were not very
revolutionary. Democracy and the successful solution of the
national question were in themselves not enough to make
Switzerland a centre of the social revolution.
It does not follow, of course, that no international
propaganda was to be conducted in Switzerland and nothing was to
be done to help revolutionize the Swiss labour movement and the
Party, For it Switzerland were drawn into the war the situation
might have undergone a swift change.
Ilyich read lectures to the Swiss workers and kept in close
touch with Platten, Nobs and Munzenberg. Our Zurich group,
with the cooperation of several Polish comrades (Bronski was
living in Zurich at the time), conceived the idea of joint
meetings with the Swiss organization in Zurich. They got together
in a small cafe called Zum Adler, not far from our
house. Something like forty persons attended the first
meeting. Ilyich spoke on current events, and stated his case in a
sharp controversial manner. Although the people who had gathered
were all internationalists, the Swiss were considerably taken
aback by the sharp way in which the question was presented. I
remember the speech of one of the representatives of the Swiss
youth who said it was impossible to break through a stone wall
with one's forehead. The fact remains that our meetings began to
peter out, and the fourth one was attended only by us Russians and
the Poles, who joked about it, then went home.
During the early months of life in Zurich Vladimir Ilyich
worked chiefly on his pamphlet on imperialism. He was deeply
absorbed in this work, and made very many notes. He was
particularly interested in the colonies, on which he had collected
a mass of material; I remember his putting me to work, too,
translating something about the African colonies from the
English. He told me many interesting things. Afterwards, when
rereading his Imperialism, I thought it much drier than
his stories had been. He had studied the economics of Europe,
America. etc., as the saying goes, to a T. Of course, he was
interested not only in the economic system, but in the political
forms that went with it and their influence on the masses. The
pamphlet was finished by July. The Second Zimmerwald Conference
(known as the Kienthal Conference) was held on April 24-30,
1916. Eight months had passed since the first conference, eight
months of ever-spreading imperialist war, yet the face of the 
Kienthal Conference was not so strikingly different from that of the First Zimmerwald Conference. There had been a slight shift to the Left. The Left Zimmerwaldians had twelve instead of eight delegates, and the resolutions of the conference were a step forward. The conference strongly condemned the International Socialist Bureau, and adopted a resolution on peace, which stated: "It is impossible to establish lasting peace on the basis of capitalist society; the conditions necessary for its fulfilment will be created by socialism. By abolishing capitalist private property and, consequently, the exploitation of the masses of the people by the propertied classes, as well as national oppression, socialism will also do away with the causes of war. Hence, the struggle for lasting peace can only be a struggle for the realization of socialism." For distributing this manifesto in the trenches three officers and thirty-two soldiers were shot in Germany in May. The German Government feared nothing so much as the revolutionization of the masses.
In its proposals to the Kienthal Conference, the Central
Committee of the R.S.D.L.P. drew attention precisely to the
necessity of revolutionizing the masses. They stated: "It is not
enough that the Zimmerwald manifesto hints at revolution, saying
that the workers must make sacrifices for their own cause and not
for another's. The road the masses are to take must be clearly and
definitely pointed out to them. The masses must know whither they
are going and why. That revolutionary mass actions during the war,
if successfully followed up, can lead only to the conversion of
the imperialist war into a civil war for socialism, is an obvious
fact, and to conceal this from the masses is harmful. On the
contrary, this objective should be plainly pointed out, no matter
how difficult its achievement may seem when we are only at the
beginning of the road. It is not enough to say what the Zimmerwald
manifesto says, namely, that 'the capitalists are lying when they
speak about defending the motherland' in the present war, and
that the workers in their revolutionary struggle should act
regardless of their country's military situation; we must state
clearly what is here only hinted at, namely, that not only the
capitalists, but the social-chauvinists and the Kautskyites are
lying when they hold the notion of defending the motherland as
being applicable to the present imperialist war; that
revolutionary action during the war is impossible without the
menace of defeat to 'one's own' government, and that all and every
defeat of the government in a reactionary war facilitates
revolution, which alone is capable of securing a lasting and
democratic peace. And lastly, it is necessary to tell the masses
that unless they set up their own illegal organizations and press
free from military censorship, i.e., an illegal press, no serious
support for the nascent revolutionary struggle, its development,
criticism of its various steps, rectification of its mistakes, and
the systematic broadening and intensification of that struggle, is
conceivable." 
This proposal of the Central Committee affords a striking
illustration of the attitude of the Bolsheviks and Ilyich to the
masses – that of always telling the truth, the whole unvarnished
truth to the masses without fear of it frightening them off. The
Bolsheviks placed all their hopes in the masses, and only
the masses, would achieve socialism.
I wrote to Shlyapnikov on June l: "Grigory is very
enthusiastic over Kienthal. I can only judge by reports, of
course, but there seems to be far too much talk and no inner
unity, of the kind that would serve as a pledge of the thing's
solidity. Obviously the masses have not started 'shoving upwards'
yet, as Badayev puts it, except perhaps, to some extent, the
Germans."
A study of the economics of imperialism, an analysis of all
the component parts of this "gear box," a sweeping review of the
whole world-picture of imperialism – that last stage of
capitalism – which was heading for ruin, enabled Ilyich to shed new
light on a number of political issues and probe more deeply into
the question of what forms the struggle for socialism would take
in general and in Russia in particular. Ilyich was anxious to work
out his ideas and give them time to full mature, and so we decided
to go to the mountains, all the more that thyroid was giving me
trouble again. The only cure for it was the mountains. We went for
six weeks to the Tschudiwiese nursing home, high up in the
mountain wilds quite close to the snowy summits, in the Canton of
St. Gallen, not far from Zurich. It was a cheap place, costing
21/2 francs a day per person. True, they kept us on a "dairy" diet
there. In the morning we had coffee with milk and bread and butter
and cheese, but no sugar; for dinner – milk soup, something made of
curd-cheese, with milk for a third dish; at four o'clock we had
coffee with milk again, and another milk meal in the evening. This
milk cure had us howling at first, but after a while we reinforced
it with raspberries and bilberries that grew all round in
profusion. We had a clean room with electric light, but poorly
furnished and without service. We did our own tidying up and
cleaned our boots ourselves. This last duty was discharged by
Ilyich, who, imitating the Swiss, took my mountain boots and his
every morning and went with them to the shed where the boot
cleaning was done, and there, exchanging banter with the other
boot-blacks, he worked away with such zeal that once he knocked
over a wickerbasket full of empty beer bottles amid general
laughter. The crowd was a democratic one. A home that charged 21/2
francs a day for board and lodging was not patronized by
"respectable" folks. In some ways it reminded me of the French
Bombon, except that the people were simpler, poorer, with the
democratic spirit of the Swiss. In the evenings the proprietor's
son played the accordion while the guests danced for all they were
worth. The stamp of their feet could be heard till eleven
o'clock. Tschudiwiese was about eight kilometres from the station,
acid the only mode of communication was by donkey and narrow
mountain trails. Everybody went on foot. Almost every morning a
bell used to ring at six o'clock summoning people to give the
walkers a send-off, which they did by singing some song about a
cuckoo. Every couplet ended with the words: "Good-bye, cuckoo."
Vladimir Ilyich liked his morning nap, and used to grumble and
pull the blanket over his head. The crowd was anything but
politically minded. Even the war was a subject that was never
touched upon. Among the visitors was a soldier. His lungs being
not very strong, his chiefs had sent him to take a milk cure at
the government's expense. The military authorities in Switzerland
take good care of the soldiers (Switzerland has a militia, not a
regular army). He was quite a decent chap. Vladimir Ilyich walked
round him like a cat round the cream bowl, and started a
conversation with him several times about the predatory character
of the war. The fellow did not argue, but obviously refused to
rise to the bait. He was far more interested in his holiday at
Tschudiwiese than in political questions.
We had no visitors at Tschudiwiese. There were no Russians
living in the vicinity, and we lived a carefree existence,
spending all day rambling about the mountains. Ilyich did no work
at all there. During our walks in the mountains he spoke a lot
about the questions that occupied his mind, about the role of
democracy, and the positive and negative aspects of Swiss
democracy, often expressing the same thought in different
words. Clearly, these questions interested him deeply.
We spent the second part of July and the whole of August in
the mountains. When we left everyone gave us the usual send-off by
singing "Good-bye, cuckoo." As we were descending through the
woods, Vladimir Ilyich suddenly caught sight of some edible
mushrooms, and although it was raining, he began to pick them
eagerly, as if they were so many Left Zimmerwaldians he were
enlisting to our side. We were drenched to the skin, but picked a
bagful of mushrooms. Naturally, we missed the train and had to
wait two hours at the station for the next one.Back in Zurich we rented our old room in Spiegelgasse.
During our stay at Tschudiwiese, Vladimir Ilyich had thought
out his plan of work for the immediate future from every
angle. The first thing, which was particularly important at the
moment, was agreement on theoretical questions, the laying down of
a clear theoretical line. There were differences with Rosa
Luxemburg, Radek, the Dutch, Bukharin, Pyatakov, and to some
extent with Kollontai. The sharpest differences were with Pyatakov
(P. Kievsky), who had written an article in August entitled "The
Proletariat and the Right of Nations to
Self-Determination." After reading the manuscript, Ilyich sat down
at once to write him a reply – a whole pamphlet entitled A
Caricature of Marxism and "Imperialist Economism." The
pamphlet was written in a very sharp tone, because by that time
Ilyich had arrived at a very clear and definite view of the relationship between economics and politics in the epoch of struggle for socialism. Underestimation of the political struggle in that epoch was characterized by him as imperialist economism. "Capitalism has won," wrote Ilyich, "therefore there is no need to think about political questions – argued the old 'Economists' in 1894-1901, who went to the extent of repudiating the political struggle in Russia. Imperialism has won – therefore there is no need to think about questions of political democracy, argue the modern 'imperialist Economists.' "
The role of democracy in the struggle for socialism could not
be ignored. "Socialism is impossible without democracy in two
respects," Vladimir Ilyich wrote in the same pamphlet. "1. The
proletariat cannot catry out a socialist revolution unless it has
prepared for it by a struggle for democracy; 2. Victorious
socialism cannot maintain its victory and bring humanity to the
time when the state will wither away unless democracy is
fully achieved."
These words of Lenin's were soon fully borne out by events in
Russia. The February Revolution and the subsequent struggle for
democracy prepared the way for the October Revolution. The
constant broadening and strengthening of the Soviets, of the
Soviet system, tends to reorganize democracy itself and to
steadily give greater depth of meaning to this concept.
By 1915-1916 Vladimir Ilyich had gone deep into the question
of democracy, which he examined in the light of socialist
construction. As far back as October 1915 Ilyich had written a
reply to an article by Radek (Parabellum) published in Berner
Tagewacht: "According to Parabellum it works out that for
the sake of the socialist revolution he spurns a consistently
revolutionary programme in the field of democracy. That is
wrong. The proletariat can win only through democracy, i.e.,
through putting into effect full democracy and linking up every
step of its progress with democratic demands in their most
emphatic wording. It is absurd to offset the socialist
revolution and the revolutionary struggle against capitalism by
one of the questions of democracy, in this case the
national question. We must combine the revolutionary
struggle against capitalism with a revolutionary programme and
tactics in respect of all democratic demands, including a
republic, a militia, election of government officials by the
people, equal rights for women, self-determination of nations,
etc. So long as capitalism exists all these demands are capable of
realization only as an exception, and in incomplete, distorted
form. Basing ourselves on democracy as already achieved, and
showing up its deficiency under capitalism, we demand the
overthrow of capitalism and expropriation of the bourgeoisie as an
essential basis both for abolishing the poverty of the masses
and for fully and thoroughly implementing all
democratic transformations. Some of those transformations will be
started before the overthrow of the bourgeoisie, others in the
course of this overthrow, and still others after it. The
social revolution is not a single battle but an epoch of a series
of battles on all and every problem of economic and democratic
transformations, whose completion will be effected only with the
expropriation of the bourgeoisie. It is for the sake of this
ultimate goal that we must formulate every one of our
democratic demands in a consistently revolutionary manner. It is
quite conceivable that the workers of a given country may
overthrow the bourgeoisie before any single cardinal
democratic transformation has been fully implemented. But it is
quite inconceivable that the proletariat, as an historical class,
will be able to defeat the bourgeoisie unless it has been prepared
for it by being educated in a spirit of the most consistent and
determined revolutionary democratism." 
I quote these long passages because they so strikingly
express the thoughts which had occupied Ilyich's mind at the end
of 1915 and during 1916, thoughts which tinctured all his
subsequent utterances. Most of his articles dealing with the
questions of the role of democracy in the struggle for socialism
were published later – the article against Parabellum in 1927, the
pamphlet Caricature of Marxism in 1924. They are little
known because they were published in symposiums with a small
circulation. Yet these articles must be read if one is to
understand the vehemence with which Vladimir Ilyich stated the
case for the right of nations to self-determination. Considered in
the light of Ilyich's general appraisal of democracy this
vehemence is understandable. It should be borne in mind that the
stand which one took on the question of self-determination was
regarded by Ilyich as a touchstone of one's ability to correctly
approach all democratic demands in general. All the disputes
along this line with Rosa Luxemburg, Radek, the Dutch and Kievsky
and a number of other comrades, were conducted from just this
angle. In his pamphlet against Kievsky he wrote: "All nations will
arrive at socialism – this is inevitable, but not all will do so in
exactly the same way, each will contribute something of its own in
one or another form of democracy, one or another variety of the
dictatorship of the proletariat, one or another rate at which
socialist transformations will be effected in the various aspects
of social life. There is nothing more primitive from the viewpoint
of theory or more ridiculous from that of practice than to
paint,'in the name of historical materialism,' this aspect of the
future in a monotonous grey. The result will be nothing more than
Suzdal daubing." The building up of socialism is not merely a matter of economic
construction. Economics is only the foundation of socialist
construction, its basis and premise; the crux of socialist
construction lies in reconstructing the whole social fabric anew,
rebuilding it on the basis of socialist revolutionary
democratism.
This, if anything, is what most divided Lenin and Trotsky all
the time. Trotsky failed to grasp the democratic spirit, the
democratic principles of socialist construction, the process of
reorganizing the entire mode of life of the masses. It was at that
time, too – in 1916 – that there already existed in embryo the
differences which later arose between Ilyich and
Bukharin. Bukharin's underestimation of the role of the state and
the dictatorship of the proletariat was revealed in an article
entitled "Nota Bene," written at the end of August in No. 6 of
Jugend-Internationale. Ilyich wrote an article "Youth
International" pointing out this mistake of Bukharin's. The
dictatorship of the proletariat, ensuring the proletariat's
leading role in the reconstruction of the whole social
fabric – this is what interested Vladimir Ilyich most of all in the
latter half of 1916.
Democratic demands were included in the minimum programme, and
so in the first letter which Vladimir Ilyich wrote to Shlyapnikov
after his return from Tschudiwiese, he criticized Bazarov for his
article in Letopisi (Annals) advocating that the minimum
programme should be done away with. Ilyich argued with Bukharin,
who underestimated the role of the state, the role of the
dictatorship of the proletariat, etc. He was angry with Kievsky
for failing to understand the leading role of the proletariat. "Do
not scorn theoretical agreement," he wrote to Shlyapnikov, "I
assure you it is essential for our work during these difficult
times."
Vladimir Ilyich set to work rereading all that Marx and
Engels had written on the state and making notes from their
works. This equipped him with a deeper understanding of the nature
of the coming revolution, and thoroughly prepared him for an
understanding of the concrete tasks of that revolution.
On November 30 the Swiss Lefts held a conference on the
subject of the attitude towards the war. A. Schmidt from
Winterthur urged taking advantage of the democratic facilities in
Switzerland for anti-militarist purposes. Ilyich wrote to
Schmidt the next day suggesting that "a
referendum be taken on the question, formulated as follows: for the expropriation of the big capitalist enterprises in industry and agriculture as the only way towards the complete abolition of militarism, or against expropriation.
"In this case," Ilyich wrote to Schmidt, "we shall, in our
practical policy, say the same thing that we all admit in theory,
namely, that the complete abolition of militarism is conceivable
and practicable only in connection with the abolition of
capitalism." In a letter written in December 1916 and not
published until fifteen years later, Lenin wrote on this question:
"You think, perhaps, that I am so naive as to believe that such
questions as that of the socialist revolution can be solved 'by
persuasion'?
"No. I merely wish to give an illustration, and that
only of one particular point, and that is: what
change would have to take place in the whole of our
Party's propaganda if we intended to take up a really serious
stand on the question of rejecting the defence of the
notherland! This is only an illustration to only a
particular point – I do not claim any more." 
Questions of a dialectical approach to all events also
occupied Ilyich's thoughts at that period. He fairly pounced on
the following sentence in Engels' criticism of the draft of the
Erfurt Programme: "...Ultimately such a policy can only lead one's
own party astray. They put general, abstract political questions
into the foreground, thus concealing the immediate concrete
questions, the questions which at the first great events, the
first political crisis, put themselves on the agenda." After
copying out this passage Ilyich wrote in extra-large letters,
putting the words in double parentheses: "((The abstract in the
foreground, the concrete obscured!!)) Nota bene! Splendid! That
hits it on the head! NB."
"Marxian dialectics demands a concrete analysis of every
particular historical situation," Vladimir Ilyich wrote in his
review of the pamphlet by Junius. He strove at that period to
take things in all their bearings and interrelations. It was from
this standpoint that he approached both the question of democracy
and that of the right of nations to self-determination.
In the autumn of 1916 and the beginning of 1917 Ilyich was
completely absorbed in theoretical work. He tried to use every
minute of the time the library was open, going there punctually at
9 a.m., sitting there till 12, coming home at ten past twelve to
the minute (the library was closed from 12 to 1), going back again
after lunch and staying there until 6 p.m. It was not very
convenient for him to work at home. Our room, although it had
plenty of light, overlooked the yard, from which came a terrible
stink – a sausage factory adjoined our yard: We opened the window
only late at night. On Thursday afternoons when the library was
closed, we climbed the Zurichberg. Coming home from the library
Ilyich would buy two bars of nut chocolate in blue wrappers at 15
centimes, and after lunch we took the chocolate and some books and
went off to the mountain. We had our favourite spot there in the
heart of the woods, where there were no people about, and Ilyich
would lie in the grass deep in his reading.
At that period we had cut down our living expenses to a bare
minimum. Ilyich searched hard for something to do to earn some
money. He wrote to Granat, to Gorky, and to relatives about it,
and once he even proposed to Mark Yelizarov, his sister Anna's
husband, a fantastic scheme for publishing a "Pedagogical
Encyclopaedia" on which I was to work. I was doing a lot of work
then, studying pedagogics and familiarizing myself with the
practical side of the school system in Zurich. Ilyich waxed
so enthusiastic about this fantastic plan of his that he wrote
about care being taken that no one should steal his idea.
The prospect of earning something by writing was not very
bright, and so I decided to look for a job in Zurich. There was an
Emigrants' Benefit Funds Bureau in Zurich directed by Felix
Kohn. I became secretary of the Bureau and helped Felix in his
work.
True, the income from this job was more mythical than real,
but it was needful work. Comrades had to be helped to find work,
all kinds of undertakings had to be arranged, and medical relief
given. Funds were very low at the time, and we had more schemes
for giving benefit than actual opportunities for rendering it. One
scheme, I remember, was for setting up a sanatorium that would pay
its own way. The Swiss had such sanatoriums, where the patients
worked at gardening or making wicker-chairs in the open air for
several hours each day, which helped considerably to reduce the
cost of their upkeep. The number of consumptives among the
political emigrants was very large.
And so we lived in Zurich, a quiet jog-trot life, while the
situation grew steadily more revolutionary. In addition to his
work in the theoretical field, Ilyich considered it extremely
important to work out a correct tactical line. He believed the
time was ripe for a split on an international scale, that it was
time to break with the Second International, with the
International Socialist Bureau, to break for good and all with
Kautsky & Co., to start building up a Third International out of
the Zimmerwald Left group. In Russia it was necessary at once to
break with Chkheidze and Skobelev and the O.C.-ists, with those,
who, like Trotsky, did not understand that any idea of
conciliationism and unity was unthinkable at that moment. It was
necessary to wage a revolutionary struggle for socialism and to
expose in the most ruthless manner the opportunists, who, instead
of suiting the action to the word, were in reality serving the
bourgeoisie and betraying the cause of the proletariat. Never
before had Vladimir Ilyich been in such an uncompromising mood as
he was during the last months of 1916 and the early months of
1917. He was positively certain that the revolution was
impending.
On January 22, 1917, Vladimir Ilyich addressed a youth
meeting in Zurich, at which he spoke about the Revolution of
1905. There was quite a number of revolutionary minded young
people in Zurich at the time from other countries – Germany, Italy,
etc., who refused to fight in the imperialist war, and Vladimir
Ilyich wanted to bring home to them as fully as possible the
experience of the workers' revolutionary struggle and the
significance of the Moscow uprising; he considered the Revolution
of 1905 a prelude to the coming European revolution. "Without a
doubt," he said, "this coming revolution can only be a proletarian
revolution, and in an even more profound sense of the word: a
proletarian, socialist revolution in its content. This coming
revolution will show to an even greater degree, on the one hand,
that only grim battles, only civil wars, can free humanity from
the yoke of capital; on the other hand, that only class-conscious
proletarians can and will give leadership to the vast majority of
the exploited." Ilyich never for a moment doubted that such were
the prospects. But, as to how soon that coming revolution would
take place – that, of course, Ilyich could not know. "We of the
older generation may not live to see the decisive battles of this
coming revolution", he wound up rather wistfully. And yet Ilyich
thought of nothing else, worked for nothing else but this
revolution.
Read next section |
Krupskaya Internet Archive |
Marxists Internet Archive
