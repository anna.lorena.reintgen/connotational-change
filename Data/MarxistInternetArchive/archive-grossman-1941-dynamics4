Marx, Classical Political Economy and the
Problem of Dynamics -
Henryk Grossman 1941Right from its origins Classical economic theory was a
theory of abstract exchange-value: when it did concern itself
with production it dealt merely with the value aspect, and
passed over the labour process .[1] Since the rise of the theory of
marginal utility and the mathematical school, the analysis
of the concrete process of production has been increasingly
excluded as an element of theory, and has been used only to
establish its preconditions and overall framework. Analysis
was directed almost exclusively at the relations between
given market variables. It therefore took on a static
character and was unable to explain dynamic changes in
economic structure. Marx's economic theory marks a
fundamental break in principle from both of these
tendencies. The capitalist mode of production is governed by the
relation, exchange-value -increase in exchange-value, (M-M').
As a faithful expression of the bourgeois economy, the
Classical doctrine was always simply a doctrine of abstract
exchange-value.[2] Although Adam Smith did indeed begin
his work on the 'Wealth of Nations' by stressing the
division of labour as the source of wealth, taken to consist
in an abundant supply of the products of labour -use values
he nevertheless forgets use-values in the further course of
his work ; they are not used in the further development of
economic analysis .[3] Although there are presentations of
material and structural relations they have an exclusively
descriptive character. Smith's theory is one of abstract
exchange-value. The social equilibrium between supply and
demand, which yields the 'natural price', is solely an
equilibrium of value.[4] The same applies to Ricardo. Chapter
20 of his Principles, where he elaborates the distinction
between use-value and value, and the importance of 'wealth'
as use-values, remains largely unrelated to the rest of the
book. Ricardo's entire theoretical gaze is concentrated on
the aspect of value (profit), and the use-value of
commodities plays no role in his analysis. The life of the working class depends on the mass of
use-values which can be bought with a capital. However, the
employer's sole interest is exchange-value, the expansion of
exchange-value, i.e. profit, Ricardo expressed this in the now
famous dictum, that for the employer who makes 2,000 profit on
a capital of 20,000 - 10% -'it is utterly irrelevant whether
his capital sets 100 or 1000 people into motion. . as long as
in all instances profit does not fall below 2,000'. Whether a
given capital employs 100 or 1000 workers depends on the
particular structure of the economy. Marx points out that
Ricardo is indifferent to this structure: he is only concerned
with net revenue (pure profit), the value-surplus of price over
costs, and not gross revenue, i.e. the mass of use-values
necessary for the subsistence of the working population. For
Ricardo these figure only as costs -which are to be pushed down
as low as possible. Marx writes: 'By denying the importance of
gross revenue, i.e. the volume of production and consumption
apart from the value-surplus -and hence denying the importance
of life itself, political economy's abstraction reaches the
peak of infamy'.[5] Ricardo's central interest was the theory of distribution:
'To discover the laws which determine distribution is the main
problem of economic theory .' (Preface to the Principles). In a
letter to Malthus he calls political economy a theory of those
laws which govern the proportional division of a given wealth
among the various social classes. He regarded the determination
of the mathematical relation between the parts of a given
totality as 'the only true object of the sciences'
.[6] This point of departure renders
Ricardo's method a prioristic and deductive: his theories
can be deduced from a very small number of premisses. The
Classical doctrine is more a system of logical deductions
than a researching into and presenting of the real economic
relations of the capitalist mode of production. In
post-Classical economics this tendency to avoid the real
labour-process becomes even more pronounced. In itself the
principle of labour as the sole source of value contains a
revolutionary element. It implies, as the Classical
economists themselves explained, that in the prevailing
social order the workers do not receive the full product of
their labour, and that rent and profit represent deductions.
The egalitarian Ricardians in England merely drew the
conclusion implicit in the Classical labour theory of value
when they declared that a society in which the workers would
receive the full product of their labour would in fact be
the only correct and 'natural' one .[7] The reaction of the right-wing of the Ricardian school to
this theoretical turn exercised by the left was to become even
more conservative. They scented a threat to class harmony in
Ricardo's labour theory of value,[8] and avoided any analysis of the
production and labour-process in order to get round the
ticklish question of the labour theory of value, and the
dangerous consequences it held for distribution and the
prevailing social order. Analysis was restricted to market
phenomena, exchange: 'Exchange' stated Bastiat, 'is the
whole of political economy. '[9] According to Leon Walras, the founder
of the Lausanne school, political economy was 'the theory of
value and the exchange of value: but he (Walras) forbade us
from objectively studying production and distribution'
.[10] Out of fear of ending up in opposition to prevailing
property interests, every effort was made to give economic
theory the most abstract and formal shape possible, divorced
from any qualitative-concrete content .[11] In short, they tried to erect a
theory of distribution based on a theory of markets, in
order to furnish the proof, by means of a theory of economic
coordinates, that all factors of production are rewarded in
proportion to the extent to which they are involved in
producing the product, and that consequently the workers
receive in the wage full recompense for the work
done.[12] A second line of development also began to become apparent
at this time. Out of the same need to flee from reality this
school urged economic theory onto another terrain, that of
psychology. This started with ] .B. Say, who began with the
use-value of commodities, but instead of seeing them as
physical phenomena, conceived of them as psychological
variables - the subjective utility of the object -and
constructed a subjective theory of value on these 'services'.
Beginning with Say, proceeding through Senior (1836) in
England, Dupuits (1844) in France, and H. H. Gossen (1854) in
Germany, the subjective theory of value led to the theory of
marginal utility as a doctrine of general hedonism, in the
course of which political economy's object of analysis shifted
from the realm of things and social relations onto the terrain
of subjective feelings. 'Boehm-Bawerk's subjective analysis of
value contains the most precise and rational calculus of
pleasure and pain that has ever been written',[13] as Boehm's special digression on the
'measurability of feelings' shows. The process of production
is passed over,[14] and analysis is confined to market
phenomena, the explanation of which is sought in human
nature. An even higher level of abstraction is represented by those
attempts to make economics into a mathematically 'exact'
science, and consequently to disregard any qualitative content
in economic phenomena. Market phenomena are one-sidedly
regarded as mere 'economic quantities', and where possible they
are expressed in mathematical equations. This tendency in
modern theory is perhaps formulated most clearly in the works
of Joseph Schumpeter .[15] The process of production, as in
fact all real relations in the economy, are excluded from
analysis. In Schumpeter s view the essence of economic
relations consists of the relation 'between economic
quantities', which is in fact reduced to the relation of
exchange: all other relations between economic variables are
neglected as being immaterial. Summarising this we can say the following: Although
theoretical schools and tendencies have changed a great deal in
the hundred years since Classical political economy, they
possess a common feature which consists of the fact that the
real labour process, and the social relations which are entered
into in the course of it, have been expunged from their
theoretical analysis.[16] The Marxist critique is directed against the abstract-value
mode of study of political economy, as was the critique made by
the historical school. But whereas the latter sought to
overcome the abstract 'absolute' character of theoretical
deduction by means of the external and indiscriminate
introduction of concrete historical or statistical material on
production, consumption, trade, tax, the position of workers or
peasants etc., and thus remained at the level of pure
description, denying, in effect, the possibility of the
knowledge of theoretical laws, Marx set himself the task of
'revealing the economic law of motion of modern society'
.[17] This cannot be done, however, if one
abstracts from the 'real world' and merely clings to its one
aspect as 'economic quantities. Such a procedure is not
political economy, but the 'metaphysics of political
economy', which, the more it detaches itself from real
objects by way of abstraction, 'the more it fancies itself
to be getting nearer the point of penetrating to their core'
.[18] Since reality does not consist
merely of values, but is rather the unity of values and
use-values, Marx's critique begins from the twofold
character of economic phenomena, according to which the
essential character of the bourgeois form of economy is a
product of the specific connection of the valorisation
process with the technical labour-process. Of course,
subjectively, the sole interest of the businessman is with
the value aspect, the valorisation process of his capital,
profit. But he can only realise his desire for profit
through the technical labour process, by manufacturing
products, use-values. And it is the specific character of
this labour-process, as the means of meeting the
requirements of the valorisation process,[19] which gives the capitalist period
its particular stamp. Marx criticises previous economic
theory for only looking at individual, isolated sectors,
instead of grasping the concrete totality of economic
relations. The monetary system of the Mercantilists merely analysed the
circuit of capital within the sphere of circulation in its
money-form. The Physiocrats (Quesnay) understood the problem at
a deeper level: but they regarded the economic process as an
eternal circuit of commodities, as the real production of
commodities was not seen as the work of human beings, but
Nature. Finally, the Classical economists (Smith, Ricardo) did
indeed adopt the production process as the object of their
analysis, but only as a valorisation process: this eventually
placed them on the same path, skirting around production, and
with the same formula, as Mercantilism[20] In contrast to his predecessors Marx
stressed the crucial importance of the production process,
understood not simply as a process of valorisation, but at
the same time as a labour-process ; this does not mean,
however, that the two other forms of the circuit of capital,
as money and commodity, can be ignored. Capitalist reality
is a unity of circuits: the process of circulation (both
money and commodity), and the process of production (as the
unity of the valorisation and labour-process). Only by being
this unity of the labour- and valorisation-process does the
production process, in Marx's view, constitute, 'The basis,
the starting-point for the physiology of the bourgeois
system-for the understanding of its internal organic
coherence and life process.'[21] On the other hand, if the production
process is merely understood as a valorisation process -as
the Classicals understood it - it has all the
characteristics of 'hoarding', becomes lost in abstraction,
and is no longer capable of capturing the real economic
process.[22] Because Ricardo s categories of value are the expression-if
one-sided-of concrete reality, namely the valorisation process,
they are taken over by Marx in their basic principles and
developed further. However, at the same time he modifies them
by complementing their exclusively abstract value character
with the material aspect, and elaborates their dual character.
Marx's critique of Ricardo's categories of value, and the
changes he made, closely resembles Marx's critique and
transformation of Hegel's dialectic .[23] Both exhibit the same basic feature,
being directed against the abstract and final character
shared by Ricardo's categories of value and Hegel's
dialectic, because each of these abstracts from the 'real
characteristic form'. In his critique of Hegel's dialectic
Marx compares, in characteristic fashion, the logic with
which Hegel begins the Encyclopaedia, with money
and value: it is the logic of 'money of the spirit' and the
'conceptual value of people and of nature, because it 'is
utterly indifferent to all real forms' and has become
'abstract thought, abstracting from nature and real people'
.[24] This is similar to the way in which
money represents the 'least real' form of capital, and how
capital has reached the 'pure fetishistic form' in
interest-bearing capital, 'in which all the different forms
. . . are obliterated, and it exists . . . as independent
exchange-value'.[25] This crucial philosophical position is also brought into
play by Marx within political economy: the abstract study of
value obscures the 'real forms', the qualitative content of the
concrete labour process, which express the specific,
differentiating features of the capitalist mode of
production. These can only be grasped by demonstrating the specific
connection of the valorisation process to the technical
labour-process in each particular period of
history.[26] The 'value-form', whose final shape
is the money-form, is completely without
content.[27] The category of exchange-value leads
an 'antedeluvian existence'.[28] One can find exchange-values in
ancient Rome, in the Middle Ages and in capitalism; but
different contents are hidden behind each of these forms of
exchange-value.[29] Marx stresses that 'exchange-value'
detached from the concrete relations under which it has
arisen is an unreal abstraction, as exchange-value 'can
never exist except as an abstract, one-sided relation to an
already given concrete and living whole'. The use of the
expression 'exchangevalue' presupposes, 'a population which
produces under specific conditions'.[30] Of course, 'political economy is not
technology .[31]] The point is not however to study
the valorisation process separated from a particular
labour-process, which makes up its basis and with which it
constitutes a unitary whole. 'The concrete is concrete
because it is the concentration of many determinations,
hence unity of the diverse'. The task of science consists in
the 'reproduction of the concrete via
thought'.[32] just as the archaeologist
reconstructs the entire skeleton, and even the supposed
muscles and movements, of an animal from a few excavated
bones, so Marx reads off the necessary tendencies of capital
which are specific to an epoch from the structure of the
labour-process in a particular period, and the type of tools
used in it. This is possible as 'technology reveals the
active behaviour of people to nature, the immediate
production process of their lives, and hence their social
relations'.[33] 'The hand-mill gives you society
with the feudal lord: the steam-mill society with the
industrial capitalist' .[34] Since social relations are closely
bound up with the forces of production, changes in the
tendencies of capital can be read from changes in these
forces. The best illustration of Marx's theoretical thought is
provided by Chapters 14 and 15 of Volume I of Capital,
the chapters on 'Manufacture' and 'Machinery and Large-Scale
Industry'.[35] These chapters are by no means
historical-descriptive depictions, whereby Marx seeks to
provide a genetic exposition of the development of
large-scale industry out of manufacture. Both chapters have
an eminently theoretical character, which is proven by the
fact that they are merely sub-sections of the part of
Capital dealing with the 'Production of Relative
Surplus-Value. What then characterises manufacture and
'machino-facture', large-scale industry as two different
phases of capitalist production? Both have a capitalist
character, both are based on wage-labour, and both are
governed by the search for profit. However, since the
technical labour-process is completely different in each,
manufacture representing a 'productive mechanism whose
organs are human beings',[36] in contrast to which modern
large-scale industry is based on machines, this difference
serves to distinguish the different phases of capitalism.
The example of the derivation of these objective tendencies
of capital from the analysis of the concrete labour-process
and its instruments -machinery -is intended to illustrate
the key distinction between Marx and other theoretical
tendencies in the study of economic processes: later we
shall analyse the additional consequences which arise from
this method of study for the problem of crises and
(economic) dynamics. Whereas changes in the mode of production during manufacture
begin with labour, in large-scale industry they proceed from
the instruments of labour, machinery.[37] The process is as follows: machinery
makes muscle-power dispensable, and thus facilitates the
introduction of women and children into the production
process on a massive scale. The price of labour-power is
lowered and surplus-value increased, as the wages for the
entire 'parcellised' family, for a greatly increased amount
of work, are no higher now than they were previously for the
individual bread-winner alone. The degree of exploitation of
labour increases enormously.[38] In addition, the tendency towards
the employment of young people and the simultaneous
strengthening of the despotism of capital through the
extensive employment of women and children works to break
down the resistance previously put up by the male workers
.[39] The material consumption of the
machinery, which represents a large capital-value and which
must be depreciated and have interest paid on it, does not
only occur through use, but also through its non-use, as a
result of the destructive effects of the natural elements.
This explains the capitalists' tendency to make work
continue day and night, a tendency reinforced by the fact
that every new invention threatens to devalue the machinery;
hence the capitalists' striving to minimise the danger of
the 'moral' wear and tear of the machinery by reducing the
period in which it produces its total
value.[40] 'Hence too the economic paradox that
the most powerful instrument for reducing labour-time
suffers a dialectical inversion and becomes the most
unfailing means for turning the whole lifetime of the worker
and his family into labour-time at capital's disposal for
its own valorisation.'[41] A further incentive to the prolongation of labour-time is
therefore the possibility of saving on the otherwise normal
expansion of the scale of production through buildings. An
expansion in the scale of production without these additional
outlays implies an increase in the mass of surplus-value, with
a simultaneous reduction in capital expenditure per unit of
commodity produced, which further increases the mass of profit
.[42]  Machinery leads to the tendency to intensify work, and in
particular in those areas where the resistance of the working
class has made the excessive prolongation of the working day
impossible because of legal prohibitions. In the factory, 'the
dependence of the worker on the continuous and uniform movement
of the machine creates the strictest discipline'
.[43] The increased speed of the machinery
forces the worker to a greater degree of attentiveness and
activity .[44] This is the point at which the tendency towards a falling
rate of valorisation and the creation of an industrial reserve
army begin to play a role. At a higher level of capitalist
development, and with the universal application of machinery,
this machinery, the use of which has the task of enlarging
relative surplus-value, and hence the mass of surplus-value,
begins to operate in the opposite direction, i.e. towards a
fall in the rate of valorisation. This is because the mass of
surplus-value which can be obtained is dependent on two
factors: the rate of surplus-value, and the 'number of workers
simultaneously employed' .[45] In his hunt for an increase in
relative surplus-value the capitalist is driven to the
constant development of the productivity of labour through
an increased use of machinery in relation to living labour;
and he can only 'attain this result by diminishing the
number of workers employed by a given amount of
capital'.[46].A portion of the capital which was
previously variable, and produced surplus-value, is
progressively transformed into constant capital, which
produces no surplus-value. The result is shown in the
creation of a superfluous working population, and the
tendency towards a reduction in the mass of surplus-value
attainable in relation to the size of the capital employed.
'Hence there is an immanent contradiction in the application
of machinery to the production of surplus-value, since, of
the two factors of the surplus-value created by a given
amount of capital, one, the rate of surplus-value, cannot be
increased except by diminishing the other, the number of
workers .'[47] Finally, Marx stresses the dynamic
impulses which flow from the use of machinery. Whereas
manufacture traditionally 'sought to retain the form of the
division of labour which it found',[48] and was consequently unable to seize
hold of society to its full extent, and change it in
depth,[49] large-scale industry based on
machinery is forced, by the fall in the rate of profit,
continually to revolutionise the technology of the labour
process, and therefore the structure of society.  [1] 'The crux of any theory of economic
events is composed of theories of value and interest . .
. and four fifths of theoretical economic literature
consists of research into or controversies about these
subjects'. Joseph Schumpeter, Eugen von
Boehm-Bawerk in Neue oesterreichische
Biographie Vienna 1935 Vol. I I p. 67.[2]Marx consequently speaks of the
'accentuation of quantity and exchange value' by the
Classical economist, in striking contrast to the writers
of classical antiquity (Plato, Xenophon), 'who are
exclusively concerned with quality and use-value'.
(Capital I p. 486).[3]Cf. Elster: Smiths Lehre and die
Lehren der sogenannten 'Klassiker der
Volkswirtschaftslehre' in Elster Voerterbuch
der Volkswirtschaft Jena 1933 Vol. III p. 213. See
too: G. H. Bousquet Essai sur l'evolution de la
pensee economique Paris. 1927. p. 199 and Gunnar
Myrdal, Das politische Element in der
.nationaloekonomischen Doktrinbildung Berlin, 1932
p. 95.[4]Elsters Smith Lehre.[5]Marx, MEGA p. 514 et seq.[6]'Political Economy you think is an
inquiry into the nature and causes of wealth -I think it
should be called an inquiry into the laws which
determine the division of the produce of industry
amongst the classes who concur in its formation. No law
can be laid down respecting quantity but a tolerably
correct one can be laid down respecting proportions'.
Letters of David Ricardo to Malthus 1810-1823
ed. Bonar, Oxford, 1887. Letter of 9 October 1820.[7]See the sharp formulation of the
workers' rights to the full product of labour in
Hodgskin's Labour defended against the Claim of
Capital. By a Labourer. London 1825.[8]See for example the work by Charles
Knight, The Rights of Industry, Capital and
Labour 1831, which attacks all opponents of the
prevailing rights of property, including Hodgskin, and
characterises them as 'enemies of the people',
'destroyers' and 'servants of despoliation'. Carey
formulated this view the most clearly a little later:
'Ricardo's system is one of discord . . . . It has a
tendency to create animosity between the classes . . . .
His book is a handbook of demagogues who seek power by
the confiscation of land, war and plunder. Carey The
Past, the Present and the Future Philadelphia,
1848. p. 74-5.[9]Bousquet op. cit. p. 226.[10]ibid. p. 208. Walras's analysis is
in fact confined to the exchange-relation. He deals with
the entire 'production process' with one word. The
production process is replaced by a symbol, the concept
of 'coefficients of production', meant to mean those
amounts of productive goods used in the manufacture of
one unit of output. Each unit of production is then
allotted a corresponding 'production coefficient', and
in this formal manner, disposed of for Walras.[11]August Walras makes this quite clear
in a letter to his son (6 February 1859): 'One thing
which I find especially pleasing in the plan for your
work is the project which you have, and which I approve
of totally, of keeping to the least offensive limits as
far as property is concerned. This is very wise and very
easy to observe. It is necessary to do political economy
as one would do acoustics or mechanics' .[12]J. B. Clark constantly tried to
prove the principle that the formation of prices under
free competition would allocate everyone exactly that
which corresponds to their productive efforts. 'Natural
law so far as it has its way, excludes all spoliation.'
In a polemic against von Thuenen he assures that 'the
natural law of wages gives a result . . . (that is)
morally justifiable'. The Distribution of
Wealth New York, 1931 p. 324.[13]Cf. Myrdal op. cit. p. 152 -See too
Boehm-Bawerk, Positive Theorie des Kapitals,
Jena 1921, Abteilung 11/2 pp. 202-5.[14]One could counter to this that
Boehm-Bawerk deals with production in the Positive
Theorie des Kapitals in the unknown sections on
'Produktionsunwege' (p. 15) and 'the capitalist
production process' (p. 81). However, it would be
deceptive to expect that Boehm really does deal with
production. All that one discovers are general concepts
which do not seem to capture the specific features of
the capitalist period of production, but which are
rather intended to apply, in their abstract
universality, to all periods: thus for example, the
statement that objects of use can be made in two ways:
directly, such as picking wild fruits from a high tree:
or indirectly, by first cutting a branch from another
tree, and then knocking the fruits down. (p. 87) The
creation of such an 'intermediary product' means the
creation of a 'capital', and hence the carrying out of
'capitalist production', which for Boehm is identical
with any form of indirect production. This confusion
rests on a trivial confusion of the technical
labour-process with the valorisation process, such that
for Boehm any tool is 'capital': hence the Red Indian or
Zulu who uses a boat for catching fish is a capitalist
and carries on 'capitalist production'. (p. 86)
According to Boehm's terminology capitalist production
already existed at the most primitive levels of culture
.[15]Joseph Schumpeter, Das Wesen and
der Hauptinhalt der theoretischen Nationaloekonomie
Leipzig 1908 pp. 50 et seq.[16]With the possible exception of the
historical school in Germany led by Schmoller, which,
however, because of its descriptive and eclectic
character, and rejection of theory, can be passed over
here.[17]Capital I p. 92.[18]Poverty of Philosophy, New
York, 1%3 p. 106.[19]'In the capitalist mode of
production the labour-process only appears as a means
for the valorisation process'. Capital II .[20]In Marx's view the deep similarity
between capitalist production and the Mercantilist
system is particularly evident in crises. When all
-values and prices are subject to enormous disturbances,
suddenly there is a hunt for a stable metallic currency
-hoarding of gold -, as the one secure thing in the
midst of general insecurity, as the 'summum bonum"just
as it is understood by the hoarder'. This hoarding of
gold then acts to express that in a mode of production
based on abstract exchange-value, 'the actual
devaluation and worthlessness of all physical wealth' is
the natural consequence, because alongside abstract
exchange-value 'all other commodities -just because they
are use-values -appear to be useless, mere baubles and
toys'. (Contribution to a Critique of Political
Economy p. 146). Although political economy
imagines itself to be above Mercantilism, and assails it
as a 'false theory', as illusion, it shares the same
basic assumption as Mercantilism. As a consequence the
Monetary system does not only remain. 'historically
valid but retains its full validity within certain
spheres of the modern economy'. (ibid, p. 159).[21]Theories of Surplus-Value
II, p166.[22]Accordingly, for Marx, the only
'real' labour is the concrete labour which functions in
the technical labour-process. (Contribution p.
36, 38): whereas abstract labour which produces
exchange-value is simply the 'bourgeois form' of labour.
(ibid. p. 48, 54). ' . . . the labour which posits
exchange-value is a specific form of labour' (ibid. p.
36), and it is this exchange-value-positing labour which
is responsible for all market catastrophes,
devaluations, overproduction, stagnation. (Poverty
of Philosophy p. 68).[23]Capital I p. 103.[24]MEGA p. 154.[25]Theories of Surplus-Value
III p. 464.[26]Hegel had already criticised this
tendency to mathematicisation, which only captures one
side, the relations between quantities, in the concrete
totality of reality, and neglects all the remaining
qualitative aspects. 'Its purpose or principle is
quantity. This is precisely the relationship that is
non-essential, alien to the character of the notion. The
process of knowledge goes on, therefore, on the surface,
does not affect the concrete fact itself, does not touch
its inner nature or notion, and is hence not a
conceptual way of comprehending' (Phenomenology of
Mind, London 1931 p. 102-3). He consequently
emphasises that the task of political economy consists,
not only in representing quantitative relations and
movements, but also the qualitative side of their
element in their 'realisation' (Verwirklichung)
.[27]Capital I p. 94.[28]Grundrisse (Introduction)
p. 101 .[29]Ibid.[30]Ibid. p. 101.[31]Ibid. p. 86.[32]Capital I p. 493-494.[33]Poverty of Philosophy p.
109. In a letter to Kautsky (26 June 1864) Engels
criticises him for not having paid sufficient regard to
the role of the labour-process. 'You should not separate
technology from political economy to the extent that you
have. . . . The tools of natives condition their society
just as much as more recent tools condition capitalist
society'. (Kautsky, Aus der Fruehzeit des Marxismus.
Engels Briefwechsel mit Kautsky Prague 1935 p.
124.[34]It is no accident that such a large
part of the presentation in all the volumes of
Capital is devoted to the technical
labour-process. The chapter on machinery and large-scale
industry on its own encompasses nearly 150 pages ; in
addition a lot of space is given over to the
presentation of the technical labour-process in
connection with the valorisation process.[35]Capital I p. 455 and
492.[36]Ibid. p. 457.[37]Ibid. p. 544.[38]Ibid. p. 518-9. See pp. 489-90 on
the insubordination of the workers in manufacture.[39]Ibid. p. 526.[40]Ibid. p. 528.[41]Ibid. p. 532 .[42]Ibid. p. 548.[43]Ibid. p. 549-51.[44]Ibid. p. 531 .[45]Ibid. p. 531 .[46]Ibid. p. 531 .[47]Ibid. p. 531 .[48]Ibid. p. 530.[49]Ibid. p. 490 Cf. Jean Weller La
Conception classique dun equilibre economique,
Paris 1934. p. 11 and John M. Clark The Relation
between Statics and Dynamics in Economic Essays
in Honour of J.B.Clark New York, 1937 p. 51Table
of Contents