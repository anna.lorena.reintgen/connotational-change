Marion (Alabama) Military Institute
Louisiana State University
LSU Law Center​
 United States Army captain in World War II
 ​
Thomas Eaton Stagg, Jr., known as Tom Stagg (January 19, 1923 – June 23, 2015), was an attorney, businessman, politician, and jurist from Shreveport, Louisiana, who served as a federal judge for the United States District Court for the Western District of Louisiana from 1974, when he was appointed by U.S. President Richard M. Nixon, until his death forty-one years later. For the last twenty-three years on the bench, he held the title of "senior status." .​
​
 Stagg's father, Thomas Stagg, Sr. (1892-1960), was a native of Richmond, Virginia, descended from colonial American ancestors. He relocated to Shreveport in 1919 and entered into the real estate business.[1] At the age of sixteen, Stagg found his mother, the former Beulah Meyer (1891-1939), dead of suicide by strangulation in the family home. She had shot herself in a failed suicide attempt earlier on March 9, 1938, and previously had a nervous breakdown.[2]
 Stagg relocated to Shreveport in 1919 and entered into the real estate business.[3]  At the time of his mother's death, Stagg was about to graduate at the age of sixteen from Clifton Ellis Byrd High School in Shreveport. Stagg had a sister, Betty Jane Stagg (1921-1990).​
 Stagg attended Marion Military Institute in Marion, Alabama, and Louisiana State University in Baton Rouge, from which he completed his Bachelor of Arts degree in 1943.[4] During World War II, Stagg was elevated from 1943 to 1946 from second lieutenant]to captain in the United States Army. He was awarded the Combat Infantryman Badge, the Bronze Star for Valor, a second Bronze Star for meritorious service and the Purple Heart with Oak leaf cluster for wounds received in battle. In the war, Stagg escaped death when a German bullet struck a Bible that he carried in a pocket. Over the years, he often showed colleagues and friends the bullet scars in his Bible.[5]​
 On August 21, 1946, Stagg wed the former Mary Margaret O'Brien, who survives him. They have two daughters, Julie Stagg Harrington (born c. 1949) and husband, Martin, of Denver, Colorado, and Margaret Mary Sour (born December 1951) of Shreveport.[5][6] Margaret Sour was formerly married to Edwin William Sour (born December 1950), a son of the late four-term Louisiana State Representative Arthur William Sour, Jr. (1924-2000), of Shreveport.​
​
 ​
After the war, Stagg briefly attended Cambridge University in Great Britain and then the Louisiana State University Law Center, from  which he received his Juris Doctorate  degree in 1949. He began the practice of law with the firm of Hargrove, Guyton, Van Hook, and Hargrove, in Shreveport. He was a solo practitioner from 1953–1958; thereafter, he became the senior partner with Stagg, Cady, Johnson, and Haygood and the successor firm, Stagg, Cady, and Beard.[5]​
​
While he maintained his law practice, Stagg was vice-president of King Hardware Company from 1955 to 1974. He was also the president of the Abe Meyer Corporation in Shreveport from 1960 to 1974, a firm found by his Jewish maternal grandfather, Abe Meyer (1852-1930), who had also been a vice president of City Savings Bank and Trust Company.[2] Stagg established local tire and rubber franchises in the Shreveport area and was a managing partner of the Pierremont Mall Shopping Center from 1963 to 1974. He was president of Stagg Investments, Inc., from 1964 to 1974. He has been a managing partner of Camellia Trading Company. He divested himself of most of his business dealings when he was sworn in as a judge.[4]​
​
Stagg's most avid hobby was photographing wildflowers. Tom Arceneaux, a former member of the Shreveport City Council who clerked for Judge Stagg for two years, recalled that while Stagg was riding with Arceneaux, he would tell him to stop the car so that Stagg could get out to photograph some special flower that only he had seen.[5]​
 A Republican since 1949, Stagg as the GOP chairman for Louisiana's 4th congressional district, with then included seven parishes,  became involved in 1959 in an intra-party feud with the national committeeman, George Reese, of New Orleans, the party's U. S. Senate nominee in 1960 against Allen J. Ellender, and LeRoy Smallenberger, the Shreveport lawyer, party functionary, and subsequent state chairman from 1960 to 1964. Stagg objected when Reese endorsed, with Smallenberger in agreement, a slate of candidates for party position on both the state and parish committees. Stagg, backed by Charles T. Beaird, the then chairman of the Caddo Parish Republican Executive Committee, described Reese as having attempted to assemble a group of "yes-men" and had hence "earned the enmity of a large number of fair-minded Republicans." Stagg compared such activities to those in the 1930s of former state party chairman John Ellett Jackson, Sr. (1892-1989), a New Orleans lawyer.[7] Reese, however, defended his endorsements, most of whom won their primary races, on the premise that he as a statewide party leader was obligated to recommend suitable candidates to rank-and-file voters.[8]
 Stagg was the Republican National Committeeman from Louisiana from 1964 to 1972, a member of the executive committee of the Republican National Committee from 1964 to 1968, and a five-time delegate to GOP national conventions, from 1956 to 1972. He served on the platform committees in the conventions in 1960, 1964, and 1968. He is a former member of the Caddo Parish Republican Executive Committee and the Louisiana Republican State Central Committee.[9]​
 On February 6, 1968, Stagg ran unsuccessfully for the Louisiana State Senate when he sought one of three at-large seats from Caddo Parish. He polled 16,341 votes in the general election, but he fell 6,536 votes behind the bottom-ranked Democrat in the race, conservative incumbent Jackson Beauregard Davis (1918-2016),  a Shreveport attorney who, like Stagg, had supported Republican Barry Goldwater for president in 1964. Joining Davis in the Caddo Senate delegation were Democrats J. Bennett Johnston, Jr., later a long-term U.S. Senator, and Joseph Carnahan LeSage, Jr. (1928-2015).
 At the 1968 Republican convention meeting in Miami Beach, Florida, Stagg supported Nixon's second bid for the party's presidential nomination. Time magazine quoted national committeeman Stagg:  We've had our shot at a candidate who totally met our qualifications [Goldwater in 1964], and that candidate got six states. We've had our druthers. Now shall we win one?" Stagg described as "not viable" last-minute efforts by some party conservatives, including Louisiana Republican leader David C. Treen, to draft then freshman Governor Ronald W. Reagan of California for the presidential nomination.[10]​
​
In 1969, Stagg continued to challenge political corruption and asked: "Government improprieties are a feature of our daily newspapers. ... Will it require revelation of further scandals, corruption, misgovernment, nepotism, and just plain crookedness to gain reform in government?" His remarks came when the 28,000-member Louisiana GOP was seeking to increase membership to 200,000 in time for the 1972 election cycle.[11]
 In 1972, Stagg was the Republican nominee for state attorney general on a ticket headed by gubernatorial candidate David Treen. Stagg was seeking to fill the seat being vacated by long-term Democratic incumbent Jack Paul; Faustin Gremillion (1914-2001), a protégé of the late Earl Kemp Long. Gremillion had been eliminated in the 1971 party primary because of corrupt practices in office.​
 Stagg faced the Democratic primary winner, one-term State Senator William J. Guste, of New Orleans, but party affiliation worked heavily to Guste's advantage. Guste and Stagg were virtually the same age. Stagg won the endorsement of the since defunct Shreveport Journal:  an editorial hailed him as "a man of considerable force ... considered by his colleagues in the law fraternity to be a man of brilliance." The Shreveport Journal also noted that for years Stagg had "fought for the southern viewpoint in national Republican party conventions."[12]​
 Guste prevailed in the race with 763,276 votes (74.1 percent) to Stagg's 270,038 (25.9 percent). Stagg won only his native Caddo Parish with 54 percent of the ballots cast, and he finished with at least 43 percent in six other north Louisiana parishes. Two other statewide Republican nominees also won in Caddo Parish, Treen for governor and Robert Frye, a native of Webster Parish who was then a professor at Southeastern Louisiana University in Hammond, who challenged the Democrat Louis J. Michot, a businessman from Lafayette for the position of state education superintendent.[13] Guste went on to serve twenty years as attorney general.​
 After this campaign, Stagg vacated the position of Republican national committeeman in favor of David Treen. Stagg also announced that he was considering running for the U.S. Senate in 1972 for the seat held by Democrat Allen J. Ellender, who died during the primary campaign. Stagg said that he would need "money, support, and possibilities of success, rather than just running as an exercise."[14] Stagg never ran for the Senate; the Republican nominee was Ben  C. Toledano, a lawyer and author, who had carried the party's banner in 1970 in a race for mayor of New Orleans. Victory in the Senate race, however, went to Stagg's former rival for the state Senate, J. Bennett Johnston, Jr. Stagg did win one election, a nonpartisan contest in the summer of 1972 for delegate to the Louisiana Constitutional Convention of 1973. He served as chairman of both the Temporary Rules and the Executive Department committees.​
 On February 18, 1974, Stagg was nominated to the judicial seat vacated by Benjamin Cornell Dawkins, Jr. (1911-1984). Stagg was confirmed by the United States Senate on March 7, 1974, and received his commission a day later. He was sworn in as judge on April 26, 1974. He served as chief judge from 1984 to 1991, and assumed senior status on February 29, 1992, but still maintained a full staff and case assignment. In addition to trial court duties, he has served on panels in several federal circuit courts of appeal. His full-time position was filled in 1994 by Tucker L. Melancon, appointee of Democrat President Bill Clinton. As a judge, Stagg handled a plethora of cases in his long career.
 In 2006, Stagg sentenced Chevelle "Big Mook" Hamilton (born 1978) to sixteen years in federal prison without parole for his role in drug trafficking and weapons violations in the Shreveport area. Hamilton had pleaded guilty to the charges. Stagg denounced Hamilton from the bench as "the worst drug dealer that's been in this room this year, if he thinks I'm going to slap him on the hand, he's wrong."[15] Federal indictments had been brought against Hamilton and six others in August 2005, as a result of an intense investigation that spanned some eighteen months and involved several law enforcement agencies. Police said that they had broken up a major drug-trafficking ring that had a propensity toward violence.[16]​
 Federal Judge Maury Hicks called his mentor, Stagg, "the finest trial judge I had ever met. Without ever knowing it, he had served as my silent mentor, a role model. I told him of his role as a teacher and role model after joining him on the bench, and he volunteered to serve as my judicial mentor, always available to discuss an evidence problem, or judicial philosophy, or the world in general. He became a close friend. To have served with Judge Tom Stagg on the federal bench for twelve years is a singular honor."[5]​
 Stagg came under fire from The Washington Post for his attendance at a Law and Economics Center seminar in 1993, when he was in his second year of senior status. The judge defied his critics, dismissed complaints about propriety, and declared that he would be eager for a second stay at the resort, Hilton Head Island, South Carolina. "The food was wonderful; the teachers were wonderful. If somebody doesn't like it, I'm sorry."[17] The Washington Post had revealed something which many citizens did not know: that federal judges, like congressmen, take "junkets", which are often never reported and that are sometimes of questionable value to the taxpayers who underwrite their salaries and benefits.[17]​
 ==Recognition==​
In 1990, Stagg was named to the Byrd High School Hall of Fame. In 2004, he received the LSU Paul M. Hebert Law Center's Distinguished Alumnus award, which came on the 30th anniversary of his judicial service. A fellow Shreveporter, Robert Pugh, who had served with Stagg at the Constitutional Convention, received the honor in 2003. Therefore, a joint recognition service was held at the Shreveport Petroleum Club on October 14, 2004. Stagg expressed surprise upon his selection: "Upon considering the merit of those recipients who have preceded me, I am very proud to be named the distinguished alumnus for 2004."​[18]
 Stagg died of a lengthy illness at his home in Shreveport at the age of ninety-two.[5] His funeral mass was held on June 27 at St. John Berchmans Cathedral, 939 Jordan Street in Shreveport.[19]​
​
His papers, including his constitutional convention activities, are filed in the archives section of Noel Memorial Library at Louisiana State University in Shreveport.[5] The collection includes working papers, committee proposals, resolutions and memoranda, files of newspaper clippings, correspondence, pamphlets, and published notes and studies on developing the Louisiana Constitution of 1974.​
​
 ​
 ​
 ​
 ​
 ​
​​​​​
 1 Background 2 Legal practice and business activities 3 Political life
3.1 Candidacy for Louisiana attorney general, 1972
 3.1 Candidacy for Louisiana attorney general, 1972 4 Federal judicial service
4.1 Defending a 1993 "junket"
 4.1 Defending a 1993 "junket" 5 Legacy 6 References ↑ Funeral Rites Set Today for Thomas Stagg: Prominent Local Real Estate Man Succumbs at Home. The Shreveport Times (June 16, 1960). Retrieved on June 24, 2015.
 ↑ 2.0 2.1 Mrs. T. E. Stagg Is Found Dead at Home Here: Despondency, Ill Health Cause of Death; Funeral Today. The Shreveport Times (May 3, 1939). Retrieved on June 24, 2015.
 ↑ "Rites Set Today for Thomas Stagg: Prominent Local Real Estate Man Succumbs at Home|, The Shreveport Times, June 16, 1960.
 ↑ 4.0 4.1 Thomas E. Stagg, Jr.. Biographical Directory of Federal Judges. Retrieved on June 24, 2015.
 ↑ 5.0 5.1 5.2 5.3 5.4 5.5 5.6 5.7 5.8 5.9 John Andrew Prime. Tom Stagg, top area U.S. judge, dies. The Shreveport Times. Retrieved on June 24, 2015.
 ↑ Margaret Martin (January 23, 2013). Scene & Heard: Happy 90th birthday, Judge Tom Stagg. The Shreveport Times. Retrieved on June 24, 2015.
 ↑ "GOP Faction Fight Erupts Over Primary: 4th District Head Charges Attempt to Pack Committee", The Shreveport Times, December 2, 1959, p. 1.
 ↑ "Endorsements Defended by GOP Leader: Reese answers attack by Stagg as Faction Fight," The Shreveport Times, December 3, 1959, pp. 1, 4.
 ↑ "Tom Stagg to Speak to Local Lions Thursday," Minden Press-Herald, January 5, 1972, p. 1.
 ↑ A Chance to Lead. Time (August 16, 1968). Retrieved on June 24, 2015.
 ↑ "Louisiana Republicans Push for 200,000 State Members," Minden Press-Herald, March 4, 1969, p. 2.
 ↑ Shreveport Journal, January 22, 1972.
 ↑ Shreveport Journal, February 2, 1972.
 ↑ "Treen Named State GOP Committeeman," Minden Press-Herald, March 6, 1972, p. 1.
 ↑ Convicted Drug Dealer "Big Mook" Sentenced to Sixteen Years. KSLA-TV (Shreveport). Retrieved on June 24, 2015.
 ↑ Police Bust Drug Trafficking Ring. dea.gov (July 20, 2005). Retrieved on June 24, 2015.
 ↑ 17.0 17.1 Junkets for Judges Undermine Public Confidence in the Judiciary ... Judge Tom Stagg of Louisiana responded.... commdocs.house.gov. Retrieved on June 24, 2015.
 ↑ Pugh and Stagg Named Distinguished Alumni. law.lsu.edu (Fall 2004). Retrieved on June 24, 2015.
 ↑ Services set for Judge Tom Stagg. The Shreveport Times. Retrieved on June 25, 2015.
 ↑ John Andrew Prime. Tom Stagg top area U.S. judge, dies. The Shreveport Times.
 Who's Who in America, 1978​. Louisiana People Attorneys Business People Photographers Politicians Judges Republicans Episcopalians United States Army World War II Bronze Star Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 17 September 2019, at 08:09. This page has been accessed 179 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Forty years ago, Tom Stagg took me into his circle for what I expected would just be a two-year job. But he shaped me in ways I didn't then understand, and forever altered the path of my life. His nurture and wise counsel are always in the back of my mind and the minds of the two generations of lawyers who have worked in his chambers. Put simply, he is the greatest man I have ever known.[20] one of the finest people I have ever known. He had it all: Intelligence, spirit, patriotism, wisdom and wit! When he taught you a lesson in the law, you never forgot it, and neither did he. He and the life he lived made me a better lawyer, a better judge and, most importantly, a better person. I shall miss his Friday, late afternoon, call just to see who was still in the office.[5] loved his family first, but a close second was his job as a federal judge and his court family. He set the standard for our court by his example: Be on time; be the best prepared person in the room; be respectful of the lawyers; and be unfailingly impartial. Tom held everyone to the same exacting standards as he held himself. But no one who ever worked for him or with him failed to love him: his charm, his wit, his exuberance, his ability to treat you like you were the most important person in the room.[5] A giant has fallen. His death leaves a hole in our judicial family and a hole in my heart. He positively impacted the careers of so many lawyers. This remarkable man left a legacy of love of family, of duty and honor and love of this nation, its judicial system, and the rule of law. Tom Stagg loved being a federal judge. We all miss him.[5] In office In office In office Thomas Eaton Stagg, Jr. Tom Stagg Thomas Eaton "Tom" Stagg, Jr.​
 Senior Judge of the United States District Court for the Western District of Louisiana​, based in Shreveport
 In officeFebruary 29, 1992​ – June 23, 2015​
 Chief Judge of the United States District Court for the Western District of Louisiana​
 In office1984​ – 1991​
  Nauman Scott​
  John Malach Shaw​
 Judge of the United States District Court for the Western District of Louisiana​
 In officeMarch 8, 1974​ – February 29, 1992​
  ​
  Richard M. Nixon
  Benjamin C. Dawkins, Jr.​
  Tucker L. Melancon​
 
  January 19, 1923​Shreveport Louisiana​
  June 23, 2015 (aged 92)​Shreveport, Louisiana​
  ​
  Republican nominee for Louisiana state attorney general in 1972 against William J. Guste​
  Mary Margaret O'Brien Stagg (married 1946-2015,  his death)
  Two children​
  Clifton Ellis Byrd High School
Marion (Alabama) Military Institute
Louisiana State University
LSU Law Center​
  Attorney; Businessman]​
United States Army captain in World War II
  Episcopalian
 Tom Stagg Contents Background Legal practice and business activities Political life Federal judicial service Legacy References Navigation menu Candidacy for Louisiana attorney general, 1972 Defending a 1993 "junket" Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
