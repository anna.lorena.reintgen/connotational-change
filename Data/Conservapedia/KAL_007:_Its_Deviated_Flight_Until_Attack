Delayed one hour because of strong tail winds, KAL 007 departed Anchorage International Airport at 13:00 GMT (4:00 a.m. Alaskan time). It was the practice of Korean Airlines to sometimes delay a flight so that it would not arrive at Kimpo Airport in Seoul, Korea prior to 6:00 a.m., as customs and passenger handling personnel began their operations at that time. Climbing, the jumbo jet turned left, seeking its assigned route J501, which would soon take it onto the northernmost of five 50-mile wide passenger plane air corridors that bridge the Alaskan and Japanese coasts. These five corridors are called the NOPAC (North Pacific) routes. KAL 007’s particular corridor, Romeo 20, passed just 17 1/2 miles from Soviet airspace off the Kamchatka coast. 
 Though the Boeing 747 was capable of being navigated by Long Range Navigation (LORAN)—a less up-to-date system relying on navigational guidance aids external to the aircraft—its principal method of navigation was the Inertial Navigation System (INS). The INS consisted of three independent, self-contained, but electronically linked units guiding the aircraft according to nine “waypoint” coordinates, some of which were “punched” into the units prior to flight. If more than nine were required, number 10 (and subsequent) coordinates were entered during the flight, replacing waypoint entries overflown and vacated. Korean Airlines received its computerized flight plan from an independent supplier company, Continental Air Service. This plan would designate the following nine waypoints for KAL 007’s route from Anchorage, Alaska, to Seoul, South Korea—BET (Bethel), NABIE, NEEVA, NIPPI, NOKKA, NOHO, IFH (Inkfish), MXT (Matsushima), and GTC (Niigata). Each INS unit (two of which were actually used for navigation, the third being a reserve) utilized gyroscopes and accelerometers which minutely and continuously (seven times per second) adjusted (through the automatic pilot) the aircraft in flight in conformity with these coordinates, taking into account changing wind, velocity, weight, and other conditions.
 Each unit is comprised of three sub-units:
 1. An Inertial Navigation Unit which senses both the horizon and the various movements of the aircraft as it performs the necessary computations to guide the plane along its desired track. The INU is housed in the electronics bay of the aircraft.
 2. A Control Display Unit, containing digital readout windows for navigational data as well as pilot data entry options. The CDU is located in the flight deck.
 3. A Mode Selection Unit, used for navigational mode engagement. The MSU is located on the flight deck.
 Ironically, the same space-age technology that produced the INS’s ability to navigate without reference to external aids made it possible for the Soviet salvage ship Mikhail Merchink to stabilize itself dynamically over KAL 007’s wreckage, minutely compensating for wind and water changes.
 KAL 007 never reached its assigned transoceanic route Romeo 20. Seven to ten minutes after takeoff, the jumbo jet began to deviate to the east of its prescribed flight path—a deviation which would gradually increase until, approximately three and a half hours after takeoff, it would enter Russian territory just north of Petropavslovsk on the Kamchatka Peninsula. Home to the Soviet Far East Fleet Inter Continental Ballistic Missile Nuclear Submarine base, as well as several air fields, Petropavslovsk was bristling with weaponry.
 At 28 minutes after takeoff, civilian radar at Kenai, on the eastern shore of Cook Inlet and 53 nautical miles southwest of Anchorage, with a radar coverage of 175 miles west of Anchorage, tracked KAL 007 more than six miles north of where it should have been. Where it should have been was a location “fixed” by the nondirectional radio beacon (NDB) of Cairne Mountain. The NDB navigational aid operates by transmitting a continuous three-letter identification code which is picked up by the airborne receiver, the Automatic Direction Finder (ADF). Cairne Mountain was KAL 007’s first assigned navigational aid out of Anchorage Airport. Something was going wrong.
 That night, Douglas L. Porter was the controller at Air Route Traffic Control Center at Anchorage, assigned to monitor all flights in that section, recording their observed position in relation to the fix provided by the Cairne Mountain nondirectional beacon. Porter later testified that all had seemed normal to him (Testimony of Douglas L. Porter, U.S. District Court for the District of Columbia, October 6, 1984). Yet he apparently failed to record, as required, the position of two flights that night—and only two: KAL 007, carrying Democratic Congressman McDonald and 268 others, and KAL 015, carrying Republican Senators Jesse Helms of North Carolina and Steven Symms of Idaho, Congressman Carroll J. Hubbard Jr. of Kentucky, and others, which followed KAL 007 by several minutes.
 
KAL 007 continued on its night journey, having previously received clearance (13:02:40 GMT) to proceed “direct Bethel” when able. Bethel is a small fishing village on the western tip of Alaska, 350 nautical miles west of Anchorage. It is the last U. S. mainland navigational point (but not the last land point), and the first of a series of required reporting stations (KAL 007 was to do the reporting) that would guide KAL 007 along its way—a sort of obligatory external “back-up” verification system designed to confirm the accuracy of KAL 007’s internally based Inertial Navigational System.
 There were two navigational elements operative at Bethel. The first was the VOR (Very High Frequency Omni-Directional Range) navigational radio station. This apparatus emitted Morse code signals (providing its station identity) at regular intervals in all directions (omni-directional). If KAL 007 had been using Bethel’s VOR station as a course provider, the aircraft had only to ride one of these emitted signal radials “home” in order to be brought to destination. The pilot (or copilot, who also had a receiver before him) had only to ensure that the VOR needle remained centered in order to be certain that he was on course. That is, KAL 007 had only to “ride the radial.” However, KAL 007 was not to use the VOR Bethel station as a course provider—the Inertial Navigation System would do that—but as a reporting point.
 The second navigational apparatus available at Bethel was the DME (Distance Measuring Equipment). “When tuned to a DME equipped ground facility, the airborne DME sends out paired pulses at a specific spacing. This is the interrogation. The ground facility receives the pulses and then transmits back to the interrogating aircraft a second pair of pulses with the same spacing but on a different frequency. The airborne DME measures the elapsed time required for the roundtrip signal exchange and translates that time into nautical miles and time to the station as well as the aircraft’s current ground speed.(Aeronautical Knowledge, Paul E. Illman, McGraw-Hill, New York, 1995, p. 281.)
 But the VOR and the DME operate as one. Having verified by the Morse code that he had the right station, Captain Chun would have dialed in the VOR frequency, and that would have given him both the VOR and the DME. The VOR (and the DME) at Bethel were part of a navigational complex called TACAN for Tactical Air Navigation. Hence, it received the acronym VORTAC.
 Korean Airline’s Bethel procedure required Flight 007 to verify its position through VOR/DME. Apparently, it did not do so, for at 50 minutes after takeoff, military radar at King’s Salmon, Alaska, tracked KAL 007 at a full 12.6 nautical miles north of where it should have been.
 What could the pilots of KAL 007 have known of their course deviation? From Bethel and on, alert pilots could have known much—starting with the Horizonal Situation Indicator (ICAO '93 concluded that the pilots were not alert). The Horizontal Situation Indicator’s needle would have alerted the pilots of their course deviation. This is because the cockpit HSI console needle, capable of showing deviation only up to eight miles, would be “pegged” all the way to the side. The pilots, thus, should have known that they were at least eight miles off course!
 Despite this, strangely enough, at 13:49, the pilots were reporting that they were on course! “007, Bethel at forty niner.” And so, fifty minutes after takeoff, military radar at
King Salmon, Alaska acquired KAL 007 at more than 12.6 miles off course. It had exceeded its permissible leeway of deviation by six times! (Two nautical miles an hour error is the permissible drift from course set by INS.)
 Furthermore, pilot and copilot should also have been aware of the aircraft’s serious deviation because now, much more than 12 miles off course, KAL 007 was too far off course for the pilots to make their required Very High Frequency (VHF) radio reports, and had to relay these reports via KAL Flight 015, just minutes behind it and oncourse (KAL 007,increasingly off course, would have to rely on KAL 015 three times to transmit its reports to Anchorage Air Traffic Control). That should have alerted them.
 At one point in this section of its flight, (14:43 GMT) KAL 007 put a call through a navigational “hookup,” the International Flight Service Station on High Frequency. Flight 007, now too distant to speak directly with Anchorage Controller through Very High Frequency, was transmitting its message indirectly using High Frequency. At waypoint NABIE, KAL 007 was too far north to make radar contact with the Very High Frequency Air Traffic Control relay station on St. Paul’s Island. KAL 015 relayed for KAL 007. The message was a change in the Estimated Time of Arrival (ETA) for the next waypoint called NEEVA—delaying by four minutes the ETA that KAL 015 had previously relayed on behalf of KAL 007. Since a revised ETA could only be calculated by means of readout information presented by KAL 007’s Inertial Navigation Systems Control Display unit, pilot and copilot were once again presented with the opportunity of verifying their position and becoming aware of their enormous deviation.
 Halfway between waypoint NABIE and the next required reporting waypoint, NEEVA, KAL 007 passed through the southern portion of the United States Air Force NORAD (North American Air Defense) buffer zone. This zone, monitored intensively by U. S. Intelligence assets, lies north of Romeo 20, KAL 007’s designated air route, and is off-limits to civilian aircraft. KAL was apparently undetected—or, if detected, unreported.
 And so KAL 007 continued its night journey, ever increasing its deviation—60 nautical miles off course at waypoint NABIE, 100 nautical miles off course at waypoint NUKKS, and 160 nautical miles off course at waypoint NEEVA—until it penetrated Kamchatka’s borders.
 At 15:51 GMT, according to Soviet sources, KAL 007 “bumped” the Soviet buffer zone of Kamchatka Peninsula. The buffer zone was generally considered to extend 200 km. from Kamchatka’s coast and is technically known as a Flight Information Region (FIR). Within that region, aircraft would be queried by Soviet interceptors emitting a signal to the unidentified aircraft. An apparatus called a transponder would squawk back, among other things, the aircraft’s four-digit code, identifying the plane—if it were a Soviet plane. A non-Soviet block plane would not respond, but this in itself registered a negative identification. The pilots of the intruding aircraft would be unaware of the Soviet query. This system is similar to the U. S. military’s Identification Friend or Foe (IFF).
 The 200 km. buffer zone is counterpart to the United States’ Aerospace Defense Identification Zone (ADIZ), but the 100 km. radius of the buffer zone nearest to Soviet territory had the additional designation of Air Defense Zone. Heightened surveillance measures would be taken against any non-Soviet aircraft entering the Air Defense Zone.
 August 31/September 1, 1983 was the worst possible night for KAL 007 to “bump the buffer” for a complexity of reasons—all of them ominous. It was but a few short hours before the time that Marshal Ogarkov, Soviet Chief of General Staff, had set for the test firing of the SS-25, an illegal mobile Intercontinental Ballistic Missile (ICBM). The SS-25 was to be launched from Plesetsk, the launch site in northwest Russia which was used for test firing of solid fuel propellant ICBMs—24 minutes later to land in the Klyuchi target area on the Kamchatka Peninsula.
 Prior to his appointment as Marshal of the Soviet Union and Chief of the General Staff, General Ogarkov had been Chief of the Main Operation Directorate of the General Staff and, as such, had begun and had directed the Strategic Deception Department, or “Maskirovka,” which was charged with hiding Salt 2 violations from United States intelligence. On August 31/September 1, Soviet aerial “jammers” under Maskirovka were sent aloft to prevent United States intelligence eyes and ears from obtaining the illegal SS 25’s telemetry data.
The SS-25 was in violation of the SALT II agreements on three counts:
 1. It was a new kind of ICBM (the first mobile one ever launched).
 2. Its telemetry was encoded and encrypted. When a test ICBM reentry vehicle approaches the target, it emits vital data relating to its velocity, trajectory, throw-weight, and
accuracy by means of coded (symbolized) and encrypted (scrambled) electronic bursts, which are then decoded and decrypted by Soviet on-ground intelligence gathering stations.
 3. The missile as a whole was too large for its reentry vehicle (dummy warhead), raising suspicion that the missile was being developed for new and more advanced warheads
than allowable.
 And indeed, United States intelligence eyes and ears were wide open and unblinking that night—an RC-135 Boeing 707 reconnaissance plane was “lazy eighting” off the Kamchatka peninsula coast electronically “sucking in” emissions. Exactly which emissions the 707 was collecting depended on which of two versions of the RC-135—code-named “Rivet Joint” and “Cobra Ball,” respectively— happened to be deployed that night. Rivet Joint, based at Eielson Air Force Base south of Fairbanks, Alaska, was furnished with cameras, SLAR (side-looking radar) and an array of advanced electronic equipment designed to eavesdrop on in-the-air and on-theground
conversations, locate and decipher radar signals, “spoofing” (i.e. simulating electronically and otherwise near intrusions of the border thus turning on Soviet radar stations), and tripping and recording the enemy’s “order of battle.” Cobra Ball, based on Shemya Island on the tip of the Aleutian Island chain, similarly equipped as the Rivet Joint 707 but with much more apparatus, stayed far from the borders of the Kamchatka peninsula waiting for the precise moment of an Intercontinental Ballistic Missiles reentry in order to capture the missile’s telemetry signals.
 Rivet Joint and Cobra Ball were both under the command of the Air Force’s Strategic Air Command (SAC), but the personnel operating the electronic equipment were signal intelligence specialists of the Electronic Security Command (ESC) under the
authority of the National Security Agency (NSA). The NSA was charged with the responsibility of gathering and deciphering “raw” intelligence data. This raw data was
collected from supersensitive apparatus aboard aerial platforms such as the RC-135, on land collection stations such as that on Wakkanai on the northernmost Japanese Island
of Hokkaido (it was from this Wakkanai station that the Japanese radar track of KAL 007’s descent had been obtained), and the Misawa Air Base on the main Japanese Island
of Honshu. Raw data was even collected from under the sea—from strings of underwater movement and pressure sensors, and from listening devises that are capable not only of “fixing” a ship and its type, but of ascertaining its name, port of departure, destination, and probable mission.
 The raw intelligence data then underwent preliminary analysis at various collection platforms and stations, and then, in the far east, were beamed 23 thousand miles up to a geosynchronous satellite (one whose orbit around the world was correlated with the rotation of the earth around its axis in such a way that it remained continually “motionless” over a designated portion of the earth). From this satellite, the raw data was beamed to the NSA facility at Pine Gap, Australia, and from there relayed to NSA headquarters at Fort Meade, Maryland. At Fort Meade, the data was further analyzed and then distributed to various intelligence services of the United States government. The collection stations and platforms around the world operated in an on-spot evaluation of the critical nature of the raw material they collected and analyzed. An evaluation of highest priority was called a “Critic Report.” A Critic Report had to be at the desks of both the President’s National Security Adviser and the Director of the NSA within ten minutes of evaluation at the collection station. In practice, Critic Reports usually reach their destinations within five minutes. It is rare for there to be more than two Critic Reports a year.
 Most commentators believe that the KAL 007 incident fully warranted a Critic Report. After all or most of the ramifications became apparent, Senator Helms would write Boris Yeltsin, “One of the greatest tragedies of the Cold War was the shoot-down of the Korean Airlines Flight KAL 007 by the armed forces of what was then the Soviet Union on September 1st, 1983... The KAL 007 tragedy was one of the most tense incidents of the entire Cold War.”
 It was possible, then, that United States intelligence agencies, poised that night to receive all that the Soviets emitted, were in position to follow KAL 007’s incursion into the Soviet buffer zone off Kamchatka. The RC-135 Rivet Joint might well have seen Kamchatka’s radar positions “light up” one after another and would have heard the chatter at dozens of command posts. James Bamford, author of The Puzzle Palace and an expert on the operations of the United States National Security Agency explains:
 “The RC-135 is designed for one purpose—it’s designed for eavesdropping...There’s almost no way that the aircraft could not have picked up the indications of Soviet activity: Soviet fighters taking off, Soviet defense stations going into higher states of readiness, higher states of alert.”(As quoted by David Pearson, KAL 007: The Coverup, p. 156) 
 Possibly, Cobra Ball’s radar (as well as Rivet Joint’s) could have acquired KAL 007 in its flight traversing the RC-135’s area of detection. The Soviet Union would contend that not only was there an RC-135 in proximity to KAL 007 as the passenger plane neared the coast of Kamchatka, but that their proximity to each other was premeditated for United States intelligence-gathering purposes.
 There were also powerful land and sea radar arrays that could well have tracked
KAL 007 as it approached and entered Soviet territory. These were Cobra Judy aboard
the U.S.S. Observation Island, then off the coast of Kamchatka; Shemya Island’s Cobra
Dane line of sight radar with maximum range of 28 thousand miles and capability of
tracking an airplane at 30 thousand feet altitude through an area covering 400 miles (the
curvature of the earth being its limiting factor); and Shemya Island’s Cobra Talon, an
over the horizon (OTH) “backscatter” radar array with a range from 575 miles to 2,070
miles. Cobra Talon operated by bouncing its emissions off the ionosphere (deflection) to
the other side of the line of sight horizon, thus acquiring its targets. These radar arrays
had capability for both surveillance and tracking. Whether this capability was
actualized in the case of Flight 007 is currently unknown. The security “blanket” is a
thick one!
 We do know that the United States Air Force radar stations at Cape Newenham and Cape Romanzoff in Alaska had the capability to track all aircraft heading toward the Russian Buffer Zone. If they had done so, they were required to warn the straying aircraft on emergency frequency, and to warn the pertinent Air Traffic Control Centers so that they too could attempt to warn the straying aircraft. Well within range of these radar sights, KAL 007 had veered directly toward Kamchatka.
 But that night KAL 007 plunged into the Russian 200 kilometer buffer zone, then the 100 kilometer Air Defense Zone, and then it was over Russian territory with no one
to stop it.
 There was one last navigational aid to warn the crew. With consoles at the knees
of both pilot and copilot, the plane’s weather radar could have alerted them to the fact
that they were no longer flying over water, as they ought to have been. Weather radar has two modes—land mapping for clear weather, when it would be possible to look down and see water or land masses as well as the contours of the coast lines and the weather surveillance mode for cloudy weather, when it is necessary to “see through” clouds in order to detect dangerous thunderstorms. In land mapping mode, KAL 007 had only to make sure that the land mass of Kamchatka and the Island string of the Kurile chain would remain to the right. That night, however, KAL 007’s weather radar was probably not in land mapping mode, for the weather was inclement. The International Civil Aviation Organization’s meteorological analysis would conclude that, “there was extensive coverage of low, medium, and high level clouds over
southern Kamchatka associated with an active cold front.” ICAO’s analysis of KAL 007’s weather radar functioning would state, “it was concluded that the radar was not functioning properly or that the ground mapping capability was not used.”
 Unsuspectingly, KAL 007 crossed the Kamchatka peninsula, and while over the international waters of the Sea of Okhotsk nearing the coast of Sakhalin, a “welcome” was in frantic preparation 33 thousand feet below—documented by the transcripts of the Russian military ground-to-ground communications submitted by the Russian Federation and appended to the 1993 ICAO report.
 General Kornukov (to Military District Headquarters-Gen. Kamenski): (5:47)
 ...simply destroy [it] even if it is over neutral waters? Are the orders to destroy it over
neutral waters? Oh, well.
 General Kornukov: (6:13)
 Chaika [call sign for Far East Military District (FEMD) Air Force Command Post]
 Titovnin:
 Yes, sir.
He sees [it] on the radar screen, he sees [it] on the screen. He has locked on, he is
locked on, he is locked on.
 Kornukov:
 No answer, Roger. Be ready to fire, the target is 45–50 km from the State border. Officer in charge at the command post, please, for report.
 Titovnin:
 Hello.
 Kornukov:
 Kornukov, please put Kamenski on the line. Kornukov: ... General Kornukov, put
General Kamenski on.
 General Kamenski:
 Kamenski here.
 Kornukov: (6:14)
 Comrade General, Kornukov, good morning. I am reporting the situation. Target 60- 65 is over Terpenie Bay tracking 240, 30 km from the State border, the fighter from Sokol is 6 km away. Locked on, orders were given to arm weapons. The target is not responding, to identify, he cannot identify it visually because it is still dark, but he is still
locked on.
 Kamenski:
 We must find out, maybe it is some civilian craft or God knows who.
 Kornukov:
 What civilian? [It] has flown over Kamchatka! It [came] from the ocean without
identification. I am giving the order to attack if it crosses the State border.
 Kamenski:
 Go ahead now, I order…?
 
And at another location—at Smyrnykh Air Force Base in central Sakhalin…
 Lt. Col. Novoseletski34: (6:12)
 Does he see it on the radar or not?
 Titovnin: (6:13)
 He sees it on the screen, he sees it on the screen. He is locked on.
 Novoseletski:
 He is locked on.
 Titovnin:
 Locked on. Well, Roger.
 Titovnin: (6:14)
 Hello.
 Lt. Col. Maistrenko:
 Maistrenko!
 Titovnin:
 Maistrenko Comrade Colonel, that is, Titovnin.
 Maistrenko: (6:15)
 Yes.
 Titovnin:
 The commander has given orders that if the border is violated—destroy [the target].
 Maistrenko:
 …May [be] a passenger [aircraft]. All necessary steps must be taken to identify it.
 Titovnin:
 Identification measures are being taken, but the pilot cannot see. It’s dark. Even now it’s still dark.
 Maistrenko:
 Well, okay. The task is correct. If there are no lights—it cannot be a passenger [aircraft].
 Titovnin:
 You confirm the task?
 Maistrenko:
 Eh?
 Titovnin:You confirm the task?
 Maistrenko:
 Yes.
 Titovnin:
 Roger.
 
And at yet another location…
 Kornukov: (6:21)
 Gerasimenko!
 Lt. Col. Gerasimenko:
 Gerasimenko here.
 Kornukov:
 Gerasimenko, cut the horseplay at the command post, what is that noise there? I repeat
the combat task: fire missiles, fire on target 60-65 destroy target 60-65.
 Gerasimenko:
 Wilco.
 Kornukov:
 Comply and get Tarasov here.
 Take control of the MiG 23 from Smyrnykh, call sign 163, call sign 163, he is behind the
target at the moment. Destroy the target!
 Gerasimenko:
 Task received. Destroy target 60-65 with missile fire, accept control of fighter from Smyrnykh.
 Kornukov:
 Carry out the task, destroy [it]!
 Gerasimenko:
 …Comrade General… Gone to attack position.
 Kornukov: (6:24)
 Oh, [obscenities], how long [does it take him] to go to attack position, he is already getting
out into neutral waters. Engage afterburner immediately. Bring in the MiG 23 as well...
While you are wasting time, it will fly right out.
 Gerasimenko.
 
Gerasimenko:
 Here.
 Kornukov:
 So, 23 is going behind, his radar sights are engaged, draw yours off to the right
immediately after the attack. Has he fired or not?
 Gerasimenko:
 Not yet, not at all.
 Kornukov:
 Why?
 Gerasimenko:
 He is closing in, going on the attack. 163 is coming in, observing both. 
 [The MiG 23 (163) had reported in at 18:23:49
"Twelve kilometers to the target.  I see both" (the Soviet interceptor piloted by Osipovich and KAL 007)]
 Kornukov:
 Okay, Roger, understood, so bring in 163 in behind Osipovich to guarantee destruction.
 1 KAL 007: Its Deviated flight until Attack
1.1 The Details of KAL 007’s Flight from Anchorage, Alaska, to Kamchatka
 1.1 The Details of KAL 007’s Flight from Anchorage, Alaska, to Kamchatka 2 The "Worst of Nights"
2.1 And on the Ground
 2.1 And on the Ground 3 See also Korean Airlines Flight 007 How KAL 007 was Lost KAL 007: Timeline of Interception and Shootdown KAL 007: Soviet stalk, shoot down, and rescue mission orders transcripts KAL007 Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 27 June 2016, at 16:33. This page has been accessed 5,466 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 General Kornukov General Kornukov Titovnin: Kornukov: Titovnin: Kornukov: General Kamenski Kornukov Kamenski: Kornukov: Kamenski: Lt. Col. Novoseletski34 Titovnin Novoseletski Titovnin: Titovnin: Lt. Col. Maistrenko Titovnin: Maistrenko Titovnin: Maistrenko: Titovnin: Maistrenko: Titovnin: Maistrenko: Titovnin: Maistrenko: Titovnin: Kornukov: Lt. Col. Gerasimenko Kornukov: Gerasimenko Kornukov: Gerasimenko Kornukov: Gerasimenko: Kornukov Gerasimenko: Kornukov: Gerasimenko: Kornukov: Gerasimenko: Kornukov: KAL 007: Its Deviated Flight Until Attack Contents KAL 007: Its Deviated flight until Attack The "Worst of Nights" See also Navigation menu The Details of KAL 007’s Flight from Anchorage, Alaska, to Kamchatka And on the Ground Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
