(2) Martha Jane Rebsamen Cobb (married 1977–1996, his death)
 Stepchildren Robert Ellis and Suzanne Mallory
 William H. Bowen School of Law at theUniversity of Arkansas at Little Rock
 Osro Cobb (May 28, 1904 – January 18, 1996) was a Republican lawyer who worked to establish the two-party system in the U.S. state of  Arkansas. He was the United States attorney for the Eastern District of Arkansas during the Little Rock Crisis of 1957-1958. He also served a year on the Arkansas Supreme Court in 1966 as a temporary appointee of Democratic Governor Orval E. Faubus.
 
 Cobb was born near Hatton in Polk County in western Arkansas, to the lumberman Philander Cobb (born 1869), who in 1916 was a vocal supporter of the Republican nominee, Charles Evans Hughes, who narrowly lost the election to U.S. President Woodrow Wilson. Cobb's mother was the former Ida Sublette, a songwriter, playwright, poet, and the author of four books.[1] In his memoirs, Cobb recalls that his mother "always made me feel that I was destined to do great things and make a meaningful contribution to my state and country of which she would be proud. She instilled in me the belief that by doing my very best, almost any objective I might seek would be within my grasp. She was right."[2]
 Reared mostly in Caddo Gap in Montgomery County, Arkansas, Cobb as a child often accompanied his father to work and hence developed an interest in the family business and later in the law as well.[3]
 
 Because Cobb graduated from high school at the age of sixteen, he enrolled in 1920 at Henderson State University in Arkadelphia, Arkansas, formerly known as Arkadelphia Methodist College or as Henderson Brown College. Cobb refers to  Henderson as "a small Methodist college ideally suited to my needs. It took only a short time for me to establish my identity and to make numberous friends among my fellow students and with faculty members." He lettered in baseball and was a member of the debate team and reports that he was falsely accused of plagiarism by an English professor regarding an article he wrote about moonshiners in Montgomery County.[4]
 At Henderson, Cobb became an advocate of two-party competition in the political arena as a potential solution to Arkansas' lagging national standing both politically and economically. He graduated from Henderson in 1925 with two bachelor's degrees.[5] Cobb reports having read:
 the comparative annual ratings of the various states in education [and] per capita income ... and noted to my dismay that Arkansas and Mississippi always seemed to be the last two states. I also observed that most of the outstanding graduates of our colleges were leaving Arkansas for greater opportunities; Arkansas was at a standstill. It became my obsession to help Arkansas throw off its shackles.
 The major factors contributing to the state's problems were:
 (1) Arkansas manufacturers had to pay three times the rates to ship their goods east as eastern merchants had to pay to send the same goods to Arkansas.
 Cobb left Henderson for a year when the family lumber mill at Caddo Gap burned—a $100,000 loss. In the summer of 1924, he went to Union County in south Arkansas to work in the oil boom in an office at Norphlet near Smackover. He recalls a bizarre murder case there. A barber in Norphlet offered Cobb some cake that the man's wife had mailed to him from Oklahoma. The cake was, however, saturated with poison, and the barber quickly died on eating it. On an errand, Cobb delayed eating the cake and later reflected that he "felt the Lord had intervened to take care of me. In fact, I have been spared on several occasions, and I feel there has been a divine protector looking after me."[7]
 Cobb applied for a Rhodes scholarship, but the 1925 appointment went to J. William Fulbright, a student at the University of Arkansas at Fayetteville, who later became the UA president and a long-term U.S. senator.[8] Cobb organized a Republican club at Henderson, having received encouragement from some of the faculty members. He became acquainted with Harmon L. Remmel of Little Rock, the Republican national committeeman during much of the 1920s and arranged to have his older friend Remmel to deliver the 1925 commencement address at Henderson.[9]
 In 1926, Cobb was elected to the Arkansas House of Representatives from Montgomery County and served as the only Republican member in the chamber for two two-year terms. He began studying at what is now the William H. Bowen School of Law at the University of Arkansas at Little Rock. In 1929, Cobb was admitted to practice law before the Arkansas Supreme Court . Several senior partners in the law firm where he was employed, Campbell, Mallory, and Throgmorton in Little Rock, were Republicans.[5] Through these contacts, Cobb became the assistant U.S. attorney under Wallace Townsend and alongside Drew Bowers. He practiced law privately at the same time, a practice then permitted and not considered a conflict-of-interest.[10]
 As the only Republican in the Arkansas House, Cobb was invited to meet with U.S. President Calvin Coolidge in what turned out to have been a failed effort to persuade Coolidge to sign into law a bill to create a national park in the Ouachita National Forest. The state, however, already had the health resort, Hot Springs National Park. Cobb met with Coolidge for more than an hour and found him not silent at all but talkative; it turned out that the president wished to delay another appointment and used Cobb to fill in missing time. Coolidge also pocket-vetoed the national park bill, which had the support of Democratic U.S. Senator Joseph T. Robinson of Arkansas, his party's 1928 vice-presidential candidate.[11]
 In 1932, Cobb became chairman of the Arkansas Republican Party, a position that he retained for twenty-three years until 1955, when he became U.S. attorney.[5]
 In 1936, Cobb waged an active campaign for governor of Arkansas as a Republican against Democrat Carl Edward Bailey. Cobb stressed that he had been born in Arkansas, whereas the Missouri-born Bailey was a "northern man". Bailey received 156,852 votes (85.4 percent) to Cobb's 26,875 ballots (14.6 percent). Cobb recalled that after the election:
 In the late 1930s, Cobb formed the Cobb, Cazort, and Holt law firm in Little Rock, which was dissolved at the start of World War II. In 1942, Cobb joined the United States Army Air Corps, the forerunner of the Air Force, at the rank of first lieutenant. Cobb was promoted to lieutenant colonel and was a judge advocate at Berry Field in Nashville, Tennessee, and thereafter at Rosecrans Field in St. Joseph, Missouri. At Wright-Patterson Field in Dayton, Ohio, he worked on contracts to procure aircraft parts for the Army Air Corps.[5]
 In 1946, he was discharged from the military and joined John E. Coates in the opening of a law firm in the Pyramid Building in Little Rock. Cobb represented timber companies and worked to organize the interest group, the Arkansas Wood Products Association, forerunner of the Arkansas Forestry Association. He also became involved in the Smackover field. In 1948, with the assistance of former Governor Sid McMath, a Democrat, Cobb helped to pass Initiated Act 3, which guarantees that a member of the minority party be in place at each precinct in Arkansas. The act prevented Democrats from ignoring votes cast by Republicans in heavily Democrat precincts.[5]
 In 1952, Cobb was challenged for the chairmanship at the state convention by Vern Tindall of Stuttgart in Arkansas County but again prevailed.[13] As a delegate to the 1952 Republican National Convention in Chicago, Cobb and the Arkansas delegation voted for U.S. Senator Robert A. Taft of Ohio but quickly joined with the majority to endorse Dwight D. Eisenhower. Invited to meet with Eisenhower after the nomination, Cobb advised him not to concede a single southern state to the Democratic nominee, then Governor Adlai Stevenson, of Illinois. Cobb writes in his memoirs that Eisenhower "did not need to be sold on campaigning in the South because he already was convinced that it would be successful."[14]
 In 1954, Eisenhower appointed Cobb as U.S. attorney for the Eastern District of Arkansas, a highly visible position during the desegregation of Central High School in Little Rock. In the fall of 1957, Cobb was in communication with the Federal Bureau of Investigation, the United States Department of Justice, and the White House regarding late-breaking developments. With United States Attorney General Herbert Brownell, Cobb represented in federal court the U. S. government's case against Governor Faubus, who attempted without success to thwart desegregation. Cobb reached the conclusion that Faubus had exaggerated the likelihood that segregationists would engage in violence in a vain bid to block desegregation.[5]
 In his memoris, Cobb describes his relationship with Herbert Brownell:
 
 Late in 1965, Faubus asked Cobb to fill a vacancy on the Arkansas Supreme Court after Justice Sam Robinson retired. On January 11, 1966, Cobb took the position and served for the remainder of the year.[5] Politically, Cobb's Republican activities paralleled the career of Wallace Townsend, an Iowa native who served as Arkansas's Republican national committeeman from 1928 to 1961. 
 In 1964, Cobb refused to support Republican Winthrop Rockefeller for governor and instead endorsed Faubus who won his sixth and final two-year term. At the time, Cobb said that Rockefeller, a businessman originally from New York, had ignored the traditional Republican base in Arkansas. In his memoirs, Cobb reported that "A stranger passing through Arkansas at this time and seeing Mr. Rockefeller's advertising on billboards would not know whether Mr. Rockefeller belonged to any political party. Certainly the fact that he is the Republican nominee has not been included. The evidence simply is unanswerable that Mr. Rockefeller is working for his own personal interest to the exclusion of all other considerations, which leaves the Republican Party in Arkansas hanging precariously at the whims of one individual."[16]
 However, in 1966, Cobb endorsed Rockefeller as Faubus' successor, and the Republican defeated the Democrat James D. Johnson of Conway, a former member of the Arkansas Supreme Court who had previously been identified with the segregationists in the Little Rock Crisis. Cobb writes  of the changed scenario in his memoirs, "Arkansas Republicans were eager to work with Wintrhop Rockefeller on another race for governor if he could be led to run as a true Republican to help build the party in the state. I liked him personally. He showed me many courtesies, and I still thought [despite feelings in 1964] that he would make a good governor and could be elected on the Republican ticket. ... He had learned a lesson. And he won his next two races for governor.... His service contributed greatly to the enormous benefits of two-party government in Arkansas."[17]
 
 From the late 1960s until his retirement, Cobb focused on his petroleum and timber interests. In 1938, Cobb married the oil heiress Audrey Umsted, whom he had met while golfing. Her father, Sydney Albert Umsted, had in 1924 drilled the Discovery oil well in the Smackover field but was subsequently mortally injured in a train accident in Mississippi and died in a hospital in Memphis, Tennessee.[18] The Cobbs adopted one son, John Cobb. Audrey Cobb died in 1976. The next year he married his neighbor, Martha Jane Rebsamen,[5] the widow of Raymond Rebsamen, a prominent Little Rock industrialist and philanthropist.[19] Shortly before the second marriage, two armed robbers wearing masks broke into Martha Jane's home and locked her, Cobb, and her housekeeper in a closet. The intruders left with Cobb's Cadillac but called police to tell them of the three people trapped in the closet.[20]
 In 1989 and 1990, with the help of the late journalist-editor Carol Griffee, then of Little Rock, Cobb published two editions of his autobiography entitled Osro Cobb of Arkansas: Memoirs of Historical Significance. He died in Little Rock at the age of ninety-one. His papers were deposited in Special Collections of the Torreyson Library of the University of Central Arkansas in Conway.[5]
 1 Background 2 College years 3 Political career 4 U.S. attorney 5 Faubus and Rockefeller 6 Personal life 7 References ↑ Osro Cobb, Osro Cobb of Arkansas:  Memoirs of Historical Significance, Carol Griffee, ed., (Little Rock, Arkansas: Rose Publishing Company, 1989), pp. 3, 6.
 ↑ Cobb, p. 7.
 ↑ Cobb, p. 9.
 ↑ Cobb, pp. 10-12.
 ↑ 5.0 5.1 5.2 5.3 5.4 5.5 5.6 5.7 5.8 Osro Cobb (1904-1996). encyclopediaofarkansas.net. Retrieved on May 29, 2012.
 ↑ Cobb, p. 14.
 ↑ Cobb, pp. 16-17.
 ↑ Cobb, p. 13.
 ↑ Cobb, p. 15.
 ↑ Cobb, p. 29.
 ↑ Cobb, p. 42.
 ↑ Cobb, p. 62.
 ↑ Cobb, p. 106
 ↑ Cobb, p. 122.
 ↑ Cobb, p. 251.
 ↑ Cobb, pp. 141-143.
 ↑ Cobb, pp.143-144.
 ↑ Cobb, pp. 77-78.
 ↑ Cobb, p. 157
 ↑ Cobb, p. 159.
 Arkansas Republicans Lawyers Politicians United Methodists Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 12 August 2019, at 17:15. This page has been accessed 3,067 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 
the comparative annual ratings of the various states in education [and] per capita income ... and noted to my dismay that Arkansas and Mississippi always seemed to be the last two states. I also observed that most of the outstanding graduates of our colleges were leaving Arkansas for greater opportunities; Arkansas was at a standstill. It became my obsession to help Arkansas throw off its shackles.
The major factors contributing to the state's problems were:
(1) Arkansas manufacturers had to pay three times the rates to ship their goods east as eastern merchants had to pay to send the same goods to Arkansas.
(2) In those days, Arkansas was a rock-ribbed, solid, yellow-dog Democrat state. ... political machines were spawned in various counties and they corrupted the elections by any means deemed necessary. .... The Republican Party had to be convinced that it must really work in Arkansas and across the South to help establish a viable, competitive two-party system. There was no chance for Arkansas to move forward until it could become a doubtful state in presidential elections.[6] 
many persons called and visited and complained that they thought a substantial number of votes for me had not been counted. This probably did happen, though to what extent no one can be sure. It also hurt the presidential campaign of my friend, Governor Alf Landon of Kansas. This reinforced my conviction that it was absolutely necessary for the rights of the minority party to be protected in elections through the appointment of precinct judges and clerks.[12] 
...Brownell had stuck by his guns for the hard line on the integration dispute. His advice had been followed. The government was committed with no easy way to extricate itself. Many people on both sides of the controversy were becoming increasingly unhappy. I am inclined to believe that while Mr. Brownell was genuinely pleased with the policy, he was grievously disappointed that it had not achieved better results. The impass with Governor Orval Faubus may have contributed substantially to his decision to retire. ...[15] In office In office In office Osro Cobb Osro Cobb
 Arkansas State Representative for Montgomery County
 In office1927–1930
 State Chairman of theArkansas Republican Party
 In office1932–1955
  Andrew J. Russell
  Ben C. Henley
 Arkansas Republican gubernatorial nominee (1936)
  George Ledbetter
  Charles F. Cole
 Justice of the Arkansas Supreme Court
 In officeJanuary 11, 1966 – December 31, 1966
  Sam Robinson
 
  May 28, 1904Hatton, Polk CountyArkansas, USA
  January 18, 1996Little Rock, Arkansas
  (1) Audrey Umsted Cobb (married 1938–1976, her death)
(2) Martha Jane Rebsamen Cobb (married 1977–1996, his death)
  One adopted son
Stepchildren Robert Ellis and Suzanne Mallory
  Little Rock, Arkansas
  Henderson State University
William H. Bowen School of Law at theUniversity of Arkansas at Little Rock
  Lawyer
  United Methodist
 Osro Cobb Contents Background College years Political career U.S. attorney Faubus and Rockefeller Personal life References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
