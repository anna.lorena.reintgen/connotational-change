A consonant is a sound resulting from the partial or total closure of the passage of air that flows from the lungs through the larynx (voice-box) and the mouth.In Linguistics, consonants are described by the parts of the mouth which come into contact, the state of the air flow, and the state of the larynx. 
 The names used for the parts of the mouth are derived from Latin. They are: the lips (labial), the teeth (dental, which usually refers not to the teeth themselves but a position just behind them), the ridge approximately one half-inch behind the teeth (alveolar), the hard palate or "roof" of the mouth (palatal), and the soft flap hanging down at the back of the mouth (uvular). 
 Contact between two of these parts results in a consonant. For example, a labiodental consonant is formed by bringing the teeth in contact with the lips. In English, this produces V or F. A bilabial consonant is formed by bringing both lips together, such as P or B. 
 Another way to form a consonant involves bringing the tongue into contact with one of the other parts. In these cases, the Latin word for tongue (lingua) is not used in the description: if a consonant is said to be alveolar (T or D), palatal (CH or J), velar (K or hard G), or uvular (not found in English, but is used for the CH in the Scottish loch, or German nacht), it is assumed that it is the tongue that is making contact with the specified point in the mouth. The prefix apico-, however, is used to denote the tip of the tongue, rather than its flat upper surface. So an apico-alveolar consonant (not used in English, but found as the R of the Spanish word caro) uses the tip of the tongue to make contact with the alveolar ridge behind the teeth. 
 Occasionally, the tongue may move towards the hard palate during the pronunciation of the consonant, such as in the -TCH- of kitchen. When this happens, the consonant is said to be palatalized.
 A consonant may be produced by fully closing off the passage of air, known as a stop or plosive. For example, the -PP- in apple. Alternatively, the passage may be partially closed and air forced through it, and the resulting consonant is described as a fricative, such as the V in even. 
 Within the group known as fricatives, some further groups are defined. Consonants such as S or Z are known as spirants. Consonants in which the air flow changes direction are known as continuants or approximants, depending on the nature of the change of air flow.  
 Compound consonants, such as the DG in edge, combine a stop with a fricative. These are known as affricates. 
 One sound, H, is formed by increasing the rate of exhalation, and is therefore called an aspirate. 
 A consonant is said to be "voiced" if the larynx is activated, and "voiceless" if the larynx is not activated. This simple distinction defines the difference between pairs such as B and P, V and F, G and K, J and CH, D and T, etc. 
 You can feel the difference if you place your fingers on your larynx (the Adam's apple) and slowly pronounce the words thin and then. You will notice that the larynx vibrates for the TH in then, but not in thin. Linguists would describe the TH in thin as being a voiceless interdental fricative, and the TH of then as being a voiced interdental fricative. 
 B - Voiced bilabial stop. Example: "bid".
 C - Voiceless velar stop, as in "calm". Or voiceless alveolar fricative, as in "centre".
 D - Voiced alveolar stop. Example: "date".
 F - Voiceless labiodental fricative. Example: "fine".
 G - Voiced velar stop, as in "good". Or Voiced palato-alveolar affricate, as in "Germany".
 H - Voiceless glottal fricative. Example: "have".
 J - Voiced palato-alveolar affricate, as in "James".
 K - Voiceless velar stop. Example: "kiss".
 L - Voiced alveolar lateral approximant. Example: "learn".
 M - Voiced bilabial nasal. Example: "man".
 N - Voiced alveolar nasal. Example: "next".
 P - Voiceless bilabial stop. Example: "paper".
 Q - Usually occurs as a combination of K and W.
 R - Voiced alveolar continuant. Example: "red".
 S - Voiceless alveolar fricative. Example: "song".
 T - Voiceless alveolar stop. Example: "team".
 V - Voiced labiodental fricative. Example: "very".
 W - Known as a semivowel, because of its similarity to the "oo" vowel. 
 X - A combination of K and S.
 Y - Known as a semivowel, because of its similarity to the "ee" vowel.
 Z - Voiced alveolar fricative. Example: "zebra".
 1 Parts of the mouth 2 Air flow 3 Voicing 4 Technical descriptions of English Consonants Linguistics Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 13 July 2016, at 00:49. This page has been accessed 6,072 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 consonant B C D F G H J K L M N P Q K W R S T V W X K S Y Z Consonant Contents Parts of the mouth Air flow Voicing Technical descriptions of English Consonants Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
