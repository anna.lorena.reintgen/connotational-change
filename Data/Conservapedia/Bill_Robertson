Brenda R. Weeks (deceased)​
Kyle Kimmer Robertson​
Four grandchildren​
 Billy Henry Robertson (May 5, 1938 – June 27, 2013), known as Bill Robertson, was the longest-serving mayor of the small city of Minden in Webster Parish in northwestern Louisiana. With 22.5 years, he was first elected on November 6, 1990, and held the position until his death in office.[1][2]​
 On August 9, 2008, Robertson was elected  president of the Louisiana Municipal Association at the annual convention in Lafayette. He succeeded outgoing LMA President Clarence Fields, the long-term mayor of Pineville.
 On October 2, 2010, Robertson, a Democrat, won his sixth and final four-year term. He polled 1,885 votes (53 percent) against the Republican candidate, Alton Monroe "Al" Hortman, Jr., who had also run in 2006, and two "No Party" candidates, Deric Tate and Larry Botzong. Hortman finished with 737 votes (20.8 percent); Tate, 749 (21 percent), and Botzong, 170 (5 percent).[3]
 In the 2006 general election, Robertson polled 2,054 votes (56 percent) and carried nine of the city's fifteen precints. Hortman trailed with 1,596 ballots (44 percent) and led in the six other precincts.[4] Hortman hence received less than half the support in 2010 that he had in 2006.​
 Robertson was born to Homer Floyd Robertson (1910-1981) and the late Marie Robertson, a clerk in a shoe store, in Batesville in Independence County in northern Arkansas.[6] After a divorce, the senior Robertson married Imogene Kimmer Stanfield (1925–2008), who was formerly wed to Robert Stanfield, Sr. The couple had a son, Eddie Robertson (born 1962) of Minden. Robertson also had a brother, Bobby Gerald Robertson (born c. 1945) and wife, Lana, subsequently of Branson, Missouri.
 Mrs. Robertson, the former Barbara Kimmer, is the daughter of Dale Edgar Kimmer (1920-2006) and Virginia Martin Kimmer (1922-2013),[7] also natives of Independence County, Arkansas. Dale Kimmer, a farmer in Batesville, raised cattle and chickens until he and Virginia retired and relocated to Minden in 1995. Robertson's stepmother, Imogene, and his father-in-law, Dale, were siblings. 
 Bill and Barbara Robertson have three children, Beverly Jane Waller and husband, Mike; Brenda R. Weeks (1962-2017), a sales manager and counselor for Centuries and Hill Crest Memorial Funeral Home and Cemeteries in Haughton who was married to Billy Weeks of Minden,​[8] and Kyle Kimmer Robertson.[9]
 In 2016, the Minden community raised $18,000 to assist Brenda Weeks, a lung cancer patient who lost the lower portion of her left leg a year before her death.[10]
 Robertson was working at Talbot's Shoe Store in Magnolia, Arkansas, when he was sent to Minden to operate a Talbot's outlet. Thereafter, he  purchased the store from Ben Talbot and his father, D. O. Talbot, having renamed it "Robertson's Shoes", which he operated until 1979.[1][11]​
 For a time, Bill and Barbara Robertson had another outlet in Minden, along with Robertson's Shoes, called the Purple Hippopotamus, which opened in 1972.[12] The Robertsons had another store in Homer in neighboring Claiborne Parish, which opened in 1973, and a fourth outlet in Shreveport, which was launched in 1976. He opened the Shoe Box in Minden in 1978.[1]​
 As a past president of the Minden Jaycees, Robertson was included in the 1965 edition of the publication Outstanding Young Men of America. He was named "Outstanding Young Man of Minden" in 1969. He was also a former chairman of the Minden Airport Authority.[1]​
 In 1967, Robertson filed to run for one of four positions in the former Ward 4 on the Webster Parish Police Jury,[13] the parish governing body usually known as the county commission in other states. In this race, the incumbent police juror, John T. David, a former Minden mayor, was defeated for reelection but veteran jurors Leland Mims and W. Nick Love retained their seats. The newcomer elected to the jury to succeed David was not Robertson, who ran last among six candidates, but James Tenney "Jim" Branch, Jr.,[14] who subsequently lost a bid for mayor in 1982 to Noel Byars.​
 In September 1974, Robertson was elected to the Minden City Council. He defeated fellow Democrat Patrick Cary Nation (1918–2005), a retired educator, coach, and school principal, for the specific position of sanitation commissioner. Robertson was the last person to serve in that capacity. Nation's father, Abraham Brisco Nation, Sr. (1886–1933), had served as a city councilman from 1932 until he was shot to death on Armistice Day 1933, during a heated political argument, by John L. Fort (1906–1992), a son of then Mayor Connell Fort, with whom the senior Nation had quarreled.[15]​
 Robertson took office on the city council on January 1, 1975, and served until the abolition of the city commission government in 1978, when it was replaced by the current single-member-district mayor-council format.​
 Thereafter, Robertson was elected to the Webster Parish Police Jury in 1979 from the District 6 seat, a position that he held until 1990, when he resigned with a year remaining in his third term in order to become mayor of Minden.​
 In 1981, nearly a thousand constituents attempted to recall Robertson and some of his colleagues from the police jury in a dispute regarding the temporary removal of litter bins throughout Webster Parish because of the lack of recurring funds to sustains such services.[16] When the petition was submitted, Robertson successfully appealed to enough voters to remove their names to avoid a recall election. "I think people are basically fair. If police jurors could sit across the table and explain on a person-to-person basis ... people could see a portion of the problem."[17]​
 When Robertson successfully sought reelection to the police jury in 1983, he listed solid waste disposal and jail/penal farm issues as his top concern.[18] Five new jurors won election in 1983, and Robertson was elected jury president early in 1984.[19]​
 
 Robertson ran unsuccessfully for mayor in the 1989 special election called to fill the remaining months of the term of Democrat, Noel "Gene" Byars, who had been recalled from the position in a "Yes" or "No" vote after a citizens' audit revealed that he had charged personal expenses to his municipal credit card. Robertson and two preceding Democratic mayors, J. E. "Pat" Patterson and Jack Batton, contributed money to the recall. Byars hence left Minden and took a job in educational administration in Beaumont, Texas.​
 City Councilman Robert T. Tobin, a retired African-American educator, filled in temporarily after Byars vacated the mayoral office. Tobin hence became Minden's first black mayor since Reconstruction. Tobin was unseated, however, in the November 7, 1989 special election by the Republican  newcomer Paul A. Brown for the year remaining in Byars' term. Brown had been the executive director of the Minden Chamber of Commerce after relocating to the city to take a position as a counselor to alcoholics. Robertson, still on the police jury, ran unsuccessfully against both Brown and Tobin in the fall of 1989.​
 Though defeated for mayor in his first bid for the office, Robertson in January 1990, was elected to a seventh one-year term as president of the police jury.[20]​
 In the 2010 census, Minden was 51.7 percent African-American.[21] Based on the application of the 2000 census, in which African Americans were 52.1 percent of the Minden population, blacks constitute the majority in three of the five city council districts.[22]
 Robertson entered the race for a full term as mayor in 1990. Robertson and Minden businessman Billy Sherman Cost (born 1948) challenged Brown, who was seeking his first full term in the position. Cost and Thomas L. Hathorn (born 1951), another Minden businessman, had led the citizens' panel advocating the recall of Byars. Brown nearly won in the first round: 2,630 ballots (48 percent) to Robertson's 1,729 (32 percent), and Cost's 1,064 votes (20 percent). Robertson and Brown therefore advanced to the general election.[23]​
 Before the primary, Brown was seriously injured on September 28, 1990, in an accident on the Minden High School football field while he was moving the yardage chains. Having no memory of the accident, Brown remained hospitalized throughout the campaign. Minden physician John Hill declared Brown's condition as "conscious and hopeful ... an answer to prayer".[24] As concern persisted that the still disabled Brown could not discharge his duties in a full term, voters handily elected Robertson, 2,529 votes (59 percent) to 1,758 (41 percent).[25] Brown hence polled 872 fewer votes in the second round of balloting than he had in the first. The 1990 general election  launched Robertson into a long career as his city's chief municipal official.​
 Robertson's first two months in office were plagued by personal health issues. On January 9, 1991, a week after he presided over his first city council meeting, Robertson underwent successful triple bypass surgery in Shreveport.[26] Problems developed, and he underwent a second operation on January 20 to reattach surgical wires in his lower sternum. Tubes were inserted to drain the buildup of post-operative fluids.[27] He was released from the hospital on January 28[28] and returned to his mayoral duties on February 13.[29]​
 In April 1991, Robertson sought federal disaster relief after floods overpowered the drainage system in Minden. Nine inches of rain fell in eight hours, and many homes were under three feet of water.[30]​
 In 1994, Robertson ran for reelection citing the upgrading of utilities and development of the Interstate 20 service road as his campaign tenets.[31] Robertson won his second term over fellow Democrat Douglas "Doug" Frye and the Independent Lydianne Vulliamy Scallorn Hammons (born 1936), the wife of Minden businessman Orville Hammons (1915–2011).[32] Robertson received 2,019 (55 percent) to Frye's 1,285 ballots (35 percent), and 369 votes (10 percent) for Mrs. Hammons,[33] a former city clerk who had certified the recall petition signatures against former Mayor Byars.
 In 1998, to secure his third term in office, Robertson overwhelmed the Minden businessman Benjamin Franklin Wright, Jr. (born 1959), a Claiborne Parish native who ran as an Independent. Robertson polled 2,697 votes (89 percent) to 331 votes (11 percent) for Wright in a low-turnout contest.[34]​
 In 2006, Wright was convicted and sentenced to twenty-five years imprisonment for three counts of video voyeurism. He filmed female customers using the dressing room in his clothing store in Minden. The United States Court of Appeals for the Second Circuit in Shreveport upheld the conviction but vacated the sentence because of error. Wright was also arrested on March 18, 2004, on an allegation of having threatened to murder a child protection agent in connection with a child custody dispute. City police stopped Wright to arrest him for public intimidation and found videotapes inside his vehicle and later pornography on his computer disks. On appeal to the Second Circuit Court, the convictions in the 26th Judicial District Court were affirmed, but the lengthy sentences were tossed out pending resentencing. Wright was represented by the Jonesboro attorney Bobby Culpepper.[35]
 No opponent filed against Robertson in 2002.​
 Four years later, Robertson faced three challengers in his 2006 reelection bid. Photographer John Edward Quade (born 1947), a Democrat, was eliminated in the primary. He is a 1966 graduate of Minden High School, the son of Stanley B. and Elnora Davis Quade and the great-grandson of Webster Parish pioneer William G. Stewart, a former president of the Webster Parish School Board for whom the former William G. Stewart Elementary School in Minden was named. Republican Al Hortman ran sufficiently strong to force Robertson into a second round of balloting.[36] Hortman (born 1941) graduated in 1959 from Minden High School. He resided over the years in Dallas, St. Louis, Missouri, and Atlanta, Georgia but returned to Minden to care for his ailing mother,[37] Katherine Marie Fish Hortman (1909–2003).[38]
 Hortman served in the U.S. Air Force as an intelligence operative. As part of his training, he attended Yale University in New Haven, Connecticut, where he completed an intensive eight-month course in the Chinese language. He thereafter earned a bachelor's degree in mathematics, with a minor in Chines, from Louisiana Tech University in Ruston. He also received a master's degree from Louisiana Tech in education as well during the time of the mayoral campaign.[37]​
 Hortman was the president of a group of citizens who drafted the Minden 2020 Visionaries Master Plan, a 20-year proposal for long-range community progress. The plan was developed over a two-year period prior to 2000. Some 150 Minden citizens worked on the project which Hortman spearheaded.[37]​
 A former peace officer and a Methodist pastor, Hortman said that he could work with all aspects of the Minden community. Hortman formerly taught math and coached soccer at Huntington High School in Shreveport.[37]​
 During Robertson's tenure, some 85 percent of the city streets were overlaid, and major upgrades were completed on the electrical, wastewater, and water systems. Many of the projects were funded through state and federal grants. Robertson has pledged to keep electrical rates—the city operates its own power plant—among the lowest in the state.
 The city partnered with the Minden/South Webster Chamber of Commerce to establish an office to conduct economic development services. A new director was hired to work on the expansion of local businesses and to attract new employers. Robertson said that the city sought to attract new industry and entice such new businesses as a movie theatre, skating rink, bowling alley, and additional restaurants. At one time, Minden had two sit-down theaters and a drive-in theater. The last theater closed in the late 1970s.
 Robertson was elected by his fellow mayors as first vice-president at the 2007 LMA convention in Monroe. A year later, he was elected president of the association. He was also a former second vice-president and a district vice-president of the LMA. As mayor, Robertson was an ex officio board member of the Louisiana Energy and Power Authority. LEPA was established by the Louisiana legislature in 1979 as an action agency for the eighteen Louisiana cities and towns which maintain their own independent municipal power system. He was a member of the Chamber of Commerce,[11] the Masonic lodge, and the Shriners.[39]​
 Robertson had back surgery in April 2013 and underwent rehabilitation. He died some two months later at the age of seventy-five in the Promise Hospital in Bossier City.[1]
 His services were held on June 30, 2013 at the First Baptist Church of Minden, where he was an active member. Officiating were pastors Leland Crawford of First Baptist and  Richard D. Methvin, then of Eastside Missionary Baptist Church in Minden and later of Bistineau Baptist Church.[39]​ At the funeral, city council member Tommy Davis described Robertson as "bigger than life. He was respected across the state by everybody. ... Minden will have another mayor [but] Bill Robertson will be 'the mayor.' That’s just the way it will be."[40]
 Dr. Richard Campbell, a Minden dentist, said that Robertson was able to bridge across political, religious, and racial lines and leaves a legacy of sound economic policy. Robertson-sponsored improvements include renovated roads, parks, low electricity rates, a state-of-the-art airport, festivals, and the city recreation center. "It's quite a legacy he leaves for his city and family," added Campbell.[40]​
 Robertson is interred at Gardens of Memory Cemetery in Minden.[39]​
 The day after Robertson's funeral, a letter written by Robertson earlier in 2013 was released by Wanda Pittman, Robertson's executive assistant. It reveals that Robertson and his wife recommend that the city council choose Marvin Thomas "Tommy" Davis, the District D member, as the next mayor.[41]
 First elected in 2006, Davis was until his election as mayor the only Republican on the current city council.[42] A businessman, Davis is a native of Stephens, Arkansas.
 On July 11, 2013, the Minden City Council ignored Robertson's request and instead named one of the three African-American council members, Mayor Pro Tem Joseph Cornelius, Sr. (born 1942), as the interim mayor. Cornelius was elected in District A in 2010. The vote to appoint Cornelius, a former resident of New York City and Shreveport, by a three-to-two vote along racial lines.[43][44]
 1 Background 2 Early political activities 3 Losing mayoral campaign, 1989 4 Toppling Paul Brown, 1990 5 Reelection in 1998 6 The first campaign against Al Hortman, 2006 7 Robertson as mayor 8 Death in office 9 Recommending Tommy Davis as his successor 10 References ↑ 1.0 1.1 1.2 1.3 1.4 Curtis Heyen (June 27, 2013). "Minden Mayor Bill Robertson dies". The Shreveport Times. Retrieved on June 27, 2013.
 ↑ The second longest-serving mayor was John T. David, who held the office from 1946 to 1955.
 ↑ Louisiana primary election returns, October 2, 2010. Louisiana Secretary of State. Retrieved on October 2, 2010.
 ↑ Louisiana general election returns, November 7, 2006. staticresults.sos.la.gov. Retrieved on June 29, 2013.
 ↑ Webster: Mayor City of Minden, October 19, 2013. lasos.blob.core.windows.net. Retrieved on October 19, 2013.
 ↑ "Robertson New Owner of Talbot's Shoe Center", Minden Press-Herald, September 19, 1966, p. 1.
 ↑ Virginia Martin Kimmer. findagrave.com. Retrieved on February 5, 2013.
 ↑ Brenda Weeks obituary. The Shreveport Times (May 26, 2017). Retrieved on May 26, 2017.
 ↑ Dale Edgar Kimmer. findagrave.com. Retrieved on February 5, 2013.
 ↑ Bruce Franklin. More than $18K raised during fundraiser for Brenda Weeks. Minden Press-Herald. Retrieved on July 11, 2016.
 ↑ 11.0 11.1 Minden Press-Herald, September 19, 1966.
 ↑ "Candidate: Robertson seeks reelection", Minden Press-Herald, August 18, 1983, p. 1.
 ↑ "Bill Robertson Announces Candidacy for Police Jury", Minden Press-Herald, July 12, 1967, p. 1.
 ↑ Minden Press-Herald, November 6, 1967, p. 1
 ↑ "Fort Will Be Held in Caddo Parish On Murder Charges," Webster Signal-Tribune, November 14, 1933, p. 1.
 ↑ "Robertson feels some misinformed", Minden Press-Herald, November 10, 1981, p. 1.
 ↑ "Robertson relieved recall attempt is over", Minden Press-Herald, November 18, 1981, p. 1.
 ↑ "Robertson seeks reelection", Minden Press-Herald, August 18, 1983, p. 1.
 ↑ Minden Press-Herald, January 10, 1984, p. 1
 ↑ "Robertson elected to 7th term as president of police jury", Minden Press-Herald, January 5, 1990, p. 1.
 ↑ Minden (city), Louisiana. quickfacts.census.gov. Retrieved on July 2, 2013.
 ↑ Letter to Mayor Bill Robertson. Justice.gov (July 2, 2002). Retrieved on July 2, 2013.
 ↑ "Bloxom elected police chief, Runoffs in mayor, council, school races", Minden Press-Herald, October 7, 1990, p. 1.
 ↑ "Brown conscious, doctors hopeful", Minden Press-Herald, October 5, 1990, p. 1.
 ↑ Louisiana Secretary of State, ElectionReturns: Webster Parish,  November 6, 1990.
 ↑ "Robertson reportedly doing great: Minden Mayor undergoes successful triple by-pass surgery", Minden Press-Herald, January 10, 1991, p. 1.
 ↑ "Robertson in ICU", Minden Press-Herald, January 24, 1991, p. 1.
 ↑ "Robertson Home", Minden Press-Herald, January 28, 1991, p. 1
 ↑ Minden Press-Herald, February 14, 1991, p. 1.
 ↑ "Mayor will seek disaster relief funds: Robertson, councilmen to inspect storm damage", Minden Press-Herald, April 14, 1991, p. 1.
 ↑ "Minden Mayor to run for reelection", Minden Press-Herald, September 5, 1994, p. 1.
 ↑ Orville Hammons. Minden Press-Herald. Retrieved on June 7, 2011.
 ↑ Louisiana primary election returns, October 1, 1994. sos.louisiana.gov. Retrieved on May 23, 2010.
 ↑ Louisiana primary election returns, October 3, 1998. sos.louisiana.gov. Retrieved on May 23, 2010.
 ↑ State v. Wright. Caselaw.findlaw.com (May 19, 2006). Retrieved on April 8, 2019.
 ↑ Louisiana primary election returns, September 30, 2006. staticresults.sos.la.gov. Retrieved on June 29, 2013.
 ↑ 37.0 37.1 37.2 37.3 Hortman candidacy for mayor of Minden, Minden Press-Herald, July 14, 2006, p. 1.
 ↑ Obituary of Katherine Fish Hortman. genealogybuff.com. Retrieved on October 18, 2012.
 ↑ 39.0 39.1 39.2 Mayor Billy H. "Bill" Robertson. The Shreveport Times. Retrieved on June 28, 2013.
 ↑ 40.0 40.1 Vickie Welborn. "Family, friends remember Minden mayor: Celebration of Mayor Bill Robertson's life took place Sunday in city he loved and led," June 30, 2013. The Shreveport Times. Retrieved on July 2, 2013.
 ↑ Minden mayor picks successor before his death: Bill Robertson wants Tommy Davis as interim. Shreveport Times, July 1, 2013. Retrieved on July 2, 2013.
 ↑ Webster Parish primary election returns, September 30, 2006. staticresults.sos.la.gov. Retrieved on July 2, 2013.
 ↑ KEEL Radio, Shreveport, Louisiana, July 11, 2013.
 ↑ Joe Cornelius in Minden, LA. intelius.com. Retrieved on July 2, 2013.
 Louisiana People Arkansas Business People Politicians Mayors Democrats Baptists Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 16 July 2019, at 17:55. This page has been accessed 688 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 ​
Minden will have another mayor [but] Bill Robertson will be 'the mayor.' That’s just the way it will be." --Tommy Davis, Minden city council member and Robertson's elected successor[5] In office In office In office Billy Henry Robertson Bill Robertson Minden will have another mayor [but] Bill Robertson will be 'the mayor.' That’s just the way it will be." --Tommy Davis, Minden city council member and Robertson's elected successor[5] Billy Henry "Bill" Robertsonl​
 Mayor of Minden, Louisiana, USA​
 In officeJanuary 1, 1991​ – June 27, 2013​
  Paul A. Brown​
  Joe Cornelius, Sr. (interim) ​
 Webster Parish Police Juror​
 In office1980​ – 1990​
 Minden Sanitation Commissioner​
 In office1974​ – 1978​
  Lonnie L. "Red" Cupples​
  Position abolished by new city charter​
 
  May 5, 1938​Batesville, Arkansas​
  June 27, 2013 (aged 75)Bossier City, Louisiana​
  Gardens of Memory Cemetery in Minden​
  American​
  Democrat​
  Barbara Kimmer Robertson (married c. 1958-2013, his death)​
  Beverly R. Waller​
Brenda R. Weeks (deceased)​
Kyle Kimmer Robertson​
Four grandchildren​
  Businessman​
  Southern Baptist​
 Bill Robertson Contents Background Early political activities Losing mayoral campaign, 1989 Toppling Paul Brown, 1990 Reelection in 1998 The first campaign against Al Hortman, 2006 Robertson as mayor Death in office Recommending Tommy Davis as his successor References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
