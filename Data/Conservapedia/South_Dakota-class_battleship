The South Dakota-class consisted of two separate classes of battleship named for the 40th state of the Union; the first scheduled for the Fiscal Year 1918-1919 building programs before being cancelled by the limitations of the Washington Naval Treaty; and a second constructed and completed in time for service in World War II.
 The six battleships of the first South Dakota class were slower but more heavily armed and armored contemporaries of the Lexington-class battlecruiser. All were laid down in 1920 and 1921, only to be suspended under the terms of the Naval Limitations Treaty in February 1922 when they were between 11% and 38% completed. All six remained on the building ways until October–November 1923, when they were sold for scrapping.
 Like the Lexington-class, the South Dakota's were large ships, with a designed displacement nearly a third greater than their immediate predecessors. These were classic U.S. Navy battleships, well-protected against gunfire and torpedoes, heavily armed, but relatively slow, intended to prevail in a big-gun slugging match with an enemy battleline. Their main battery represented a fifty-percent increase in number of guns (twelve versus eight), and these 16" guns were of a somewhat more powerful type than those fitted to previous U.S. battleships. After cancellation of their ships, some of these weapons were employed for seacoast defense. Armor and boilers from the South Dakota-class were also recycled for use in modernizing older battleships.
 The six South Dakota-class battleships were being constructed at five locations:
 The four South Dakota class battleships represented the second group of 35,000-ton capital ships whose construction began shortly before World War II. Built with Fiscal Year 1939 appropriations, they were more compact and better protected than the preceding North Carolina class, but had the same main battery of nine 16"/45 guns in triple turrets. Their innovative hull design featured an internal armor belt, to protect the ships' vitals against 16" shells, and outboard propeller shafts that extended further aft than the inboard ones. They also had improved anti-torpedo side protection and more powerful engines, the latter being necessary to drive their shorter hulls at the designed 27-knot speed. Compared with her three "sisters", South Dakota had extra command facilities and two fewer 5-inch twin gun mounts.
 These ships were all completed in March–August 1942, providing a welcome reinforcement to the Navy's surface battle fleet at a critical stage of World War II. In 1942-43, they stood guard in the Atlantic against possible sorties by German battleships, took part in the invasion of North Africa and in operations around Guadalcanal. During the latter campaign, South Dakota was damaged in a gunnery engagement with a Japanese force that included the old battleship Kirishima. As the U.S. went on the offensive in the Central Pacific, they joined in escorting the fast carrier task forces, a job for which their heavy anti-aircraft gun batteries were well-suited. They also employed their main battery guns in shore bombardment, and were kept ready to form battle line in case their Japanese opposite numbers should appear.
 All four South Dakota class battleships went into reserve after World War II and saw no further active service. When they were disposed of in the early 1960s, Alabama and Massachusetts became a memorials. The other two were sold for scrapping.
 The two remaining ships, as well as North Carolina (BB-55) and two surviving Des Moines class heavy cruisers, were considered for President Reagan's expansion of the Navy in 1981.  However, due to their relative slowness and other issues as compared to the more-capable Iowa class battleship led to reactivation of the latter.
 The lead ship of the class, USS South Dakota was built at Camden, New Jersey. She was commissioned in March 1942 and in August was transferred to the Pacific where she was soon involved in the Guadalcanal Campaign. On 26 October 1942, her anti-aircraft guns played a prominent role in the Battle of the Santa Cruz Islands, during which her forward sixteen-inch gun turret was hit by a Japanese bomb. Shortly thereafter, she collided with USS Mahan (DD-364). Damage from these incidents was repaired locally, and she was heavily engaged, and damaged again, during the 14–15 November battleship night action off Guadalcanal, a battle that effectively ended Japan's plans to retake that strategic island.
 Following repairs in the United States, South Dakota operated in the Atlantic from February into August 1943, including service with the British Home Fleet. She then returned to the Pacific and took part in the Gilberts and Marshalls invasions in November 1943-February 1944. The battleship operated with the fast carriers during raids on Japanese bases during that time and into the Spring of 1944. She next participated in the June 1944 Marianas Campaign, using her heavy guns to shell enemy positions on Saipan and Tinian. In the Battle of the Philippine Sea on 19 June, she was hit by another Japanese bomb.
 Another stateside overhaul prepared South Dakota for further Pacific combat operations. From October 1944 to the end of World War II over ten months later, she screened carrier task forces during strikes in the Western Pacific that ranged from the South China Sea to Japan. The invasions of Leyte, Luzon, Iwo Jima and Okinawa were among these operations. In March and April 1945, South Dakota's guns joined in bombarding Okinawa. She shelled targets in the Japanese Home Islands in July and August, during the final acts of the Pacific War, and was present in Tokyo Bay on 2 September 1945 during the Formal Surrender of Japan. South Dakota returned to the United States soon thereafter and was decommissioned in January 1947. She remained inactive until October 1962, when she was sold for scrapping.
 USS Indiana was built at Newport News, Virginia, was commissioned in April 1942. After shaking down in the Atlantic, she joined the war against Japan, operating in the South Pacific from late November 1942 into October 1943. Indiana then moved to the Central Pacific, where she participated in the invasions of the Gilbert Islands in November 1943 and the Marshalls in January 1944. While engaged in the latter operation on 1 February 1944, she was damaged in a night collision with USS Washington (BB-56).
 In April and May 1944, following repairs, Indiana escorted carriers in raids on Japanese positions in the Carolines. Beginning in June, she took part in the Marianas campaign, including pre-invasion bombardment of Saipan and the Battle of the Philippine Sea. The battleship screened the fast carriers into the Autumn of 1944, assisting in strikes on the Palaus and Philippines. After an overhaul, Indiana returned to the Western Pacific in January 1945, in time to participate in the invasion of Iwo Jima, carrier raids on the Japanese Home Islands and the Ryukyus invasion. She passed through a typhoon in June and used her sixteen-inch guns to shell targets in Japan during the last weeks of the Pacific War. Returning to the U.S. soon after the Japanese surrender, Indiana was placed in reserve status in September 1946 and formally decommissioned a year later. She saw no further active service and was sold for scrapping in October 1963.
 USS Massachusetts was built at Quincy, Massachusetts, and commissioned in May 1942. Following shakedown and training operations, she took part in the Invasion of North Africa, engaging French warships at and off Casablanca, Morocco, on 8 November 1942. Massachusetts transferred to the Pacific in early 1943. Following several months in the South Pacific, she participated in the invasions of the Gilberts and Marshalls in late 1943 and early 1944, raids on Japanese-held islands in February to May and the Hollandia invasion in April 1944.
 Massachusetts was overhauled in mid-1944, then returned to the Pacific war zone to take part in raids on Okinawa and Formosa in October, the Battle of Leyte Gulf later in that month and attacks in the Philippines area during the rest of that year. Her operations with the fast carrier striking forces continued in 1945 with further attacks in support of the invasion of Luzon in January, Iwo Jima in February and Okinawa in March–June. During July and August 1945, Massachusetts twice bombarded the Japanese home islands with her sixteen-inch guns.
 Following a late 1945 overhaul, Massachusetts was stationed along the U.S. Pacific coast until April 1946, when she returned to the Atlantic. The battleship decommissioned in March 1947 and remained in the Reserve Fleet until stricken from the Naval Vessel Register in June 1962. Three years later Massachusetts began a second career as a memorial and museum ship at Fall River, Massachusetts, and remains in that role to the present day.
 USS Alabama was built at the Norfolk Navy Yard, Portsmouth, Virginia. Commissioned in August 1942, she operated along the U.S. east coast in late 1942 and early 1943 and was then stationed in the North Atlantic to guard against the threat of raids by German heavy ships. Alabama was transferred to the Pacific in August 1943. During November of that year, she took part in the operation to capture the Gilbert Islands. In 1944, Alabama participated in taking the Marshalls, the Marianas and Leyte, the Battles of the Philippine Sea and Leyte Gulf, and raids on Japanese positions elsewhere in the Pacific.
 Following overhaul and training in January–April 1945, the battleship rejoined the fleet for operations in the Western Pacific, including attacks on the Japanese home islands. She took part in the occupation of Japan and the return of veterans to the United States in August–October 1945. Alabama was decommissioned at the Puget Sound Navy Yard, Bremerton, Washington, in January 1947 and remained in reserve until struck from the Naval Vessel Register in June 1962. Two years later, she was turned over to the State of Alabama. Since September 1964, Alabama has been berthed at Mobile, Alabama, as a memorial to those who served and sacrificed during World War II.
 1 South Dakota-class (BB 51-54) 2 Design characteristics 3 South Dakota-class (BB 57-60) 4 Design characteristics 5 Ships
5.1 USS South Dakota (BB 57)
5.2 USS Indiana (BB 58)
5.3 USS Massaachusetts (BB 59)
5.4 USS Alabama (BB 60)
 5.1 USS South Dakota (BB 57) 5.2 USS Indiana (BB 58) 5.3 USS Massaachusetts (BB 59) 5.4 USS Alabama (BB 60) 6 Links South Dakota (BB-49). Fiscal year 1918 program. Keel laid at the New York Navy Yard in March 1920. Construction cancelled when the ship was 38.5% completed and she was broken up on the building ways. Indiana (BB-50). Fiscal year 1918 program. Keel laid at the New York Navy Yard in November 1920. Construction cancelled and the ship scrapped on the building ways. Montana (BB-51). Fiscal year 1918 program. Keel laid at the Mare Island Navy Yard, California, in September 1920. Construction cancelled and the ship was broken up on the building ways. North Carolina (BB-52). Fiscal year 1919 program. Keel laid at Norfolk Navy Yard, Virginia, in January 1920. Construction cancelled and the ship was broken up on the building ways. Iowa (BB-53). Fiscal year 1919 program. Keel laid at Newport News, Virginia, in May 1920. Construction cancelled and the ship was scrapped on the building ways. Massachusetts (BB-54). Fiscal year 1919 program. Keel laid at Quincy, Massachusetts, in April 1921. Construction cancelled when the ship was about 11% completed and she was broken up on the building ways. Displacement: 43,200 tons Dimensions: 684' (length overall); 106' (maximum beam) Powerplant: 60,000 horsepower steam turbines with electric drive, producing a 23 knot maximum speed Armament (Main Battery): Twelve 16"/50 guns in four triple turrets Armament (Secondary Battery): Sixteen 6"/53 guns in single mountings (eight guns on each side of the ship) Displacement: 35,000 tons (standard) Dimensions: 680' (length overall); 108' 2" (maximum beam) Powerplant: 130,000 horsepower steam turbines, producing a 27 knot maximum speed Armament (Main Battery): Nine 16"/45 guns in three triple turrets Armament (Secondary Battery): Twenty 5"/38 guns in ten twin mountings (ten guns on each side of the ship). South Dakota had sixteen 5"/38 guns in eight twin mountings (eight guns on each side of the ship). Battleship Cove Museum, Fall River, Massachusetts Battleship Memorial Park, Mobile, Alabama Ships United States Navy Warships Battleships Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 23 August 2016, at 11:43. This page has been accessed 5,179 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 South Dakota-class Career Flag Owner Type Authorized Keel laid Status Characteristics Displacement Length Beam Speed Armament South Dakota South Dakota-class Career Flag Owner Type Authorized Keel laid Commissioned Status Characteristics Displacement Length Beam Draft Speed Armament Copyright Details 
  Career
 Flag
 
  Owner
  United States Navy
  Type
  Battleship
  Authorized
  Fiscal years 1918-1919
  Keel laid
  1920-1921
  Status
  Program cancelledAll vessels scrapped on the ways by 1923
  Characteristics
  Displacement
  43,200 t
  Length
  684 ft
  Beam
  106 ft
  Speed
  23 knots
  Armament
  Twelve 16"/50 main gunssixteen 6"/53 secondary guns
 
  Career
 Flag
  
  Owner
  United States Navy
  Type
  Battleship
  Authorized
  Fiscal year 1939
  Keel laid
  1920-1921
  Commissioned
  South DakotaIndianaMassachusettsAlabama
  Status
  South Dakota, Indiana scrappedMassachusetts, Alabama museums
  Characteristics
  Displacement
  35,000 t
  Length
  684 ft
  Beam
  106 ft
  Draft
  36 feet 4 inches
  Speed
  27 knots
  Armament
  Nine 16"/45 main gunsTwenty 5"/38 secondary guns
  This work is in the Public Domain in the United States because it is a work of the United States Federal Government under the terms of Title 17, Chapter 1, Section 105 of the U.S. Code
  File available from the United States Federal Government [1][2].
 South Dakota-class battleship Contents South Dakota-class (BB 51-54) Design characteristics South Dakota-class (BB 57-60) Design characteristics Ships Links Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console USS South Dakota (BB 57) USS Indiana (BB 58) USS Massaachusetts (BB 59) USS Alabama (BB 60) 
