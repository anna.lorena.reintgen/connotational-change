 The United States Coast Guard is a military, multimission, maritime service within the Department of Homeland Security and one of the nation's five armed services. Its core roles are to protect the public, the environment, and U.S. economic and security interests in any maritime region in which those interests may be at risk, including international waters and America's coasts, ports, and inland waterways.
 The United States Coast Guard (USCG) is a military branch of the United States involved in maritime law, mariner assistance, and search and rescue, among other duties of coast guards elsewhere. One of the seven uniformed services of the United States, and the smallest armed service of the United States, its stated mission is to protect the public, the environment, and the United States economic and security interests in any maritime region in which those interests may be at risk, including international waters and America's coasts, ports, and inland waterways.
 USCG has a broad and important role in homeland security, law enforcement, search and rescue, marine environmental pollution response, and the maintenance of river, intracoastal and offshore aids to navigation (ATON). It also lays claim to being the United States' oldest continuous seagoing service. The United States Coast Guard has about 40,150 men and women on active duty.
 The Coast Guard's motto is Semper Paratus, meaning "Always Ready".
 The Coast Guard's roots lie in the Revenue Cutter Service, which was founded on August 4, 1790 as part of the Department of the Treasury. An act of the U.S. Congress created the Coast Guard in 1915, with the merger of the Revenue Cutter Service and the United States Lifesaving Service. The United States Lighthouse Service was merged into the Coast Guard in 1939. The legal basis for the Coast Guard is Title 14 of the United States Code, which states: "The Coast Guard as established January 28, 1915, shall be a military service and a branch of the armed forces of the United States at all times." Upon the declaration of war or when the President directs, the Coast Guard operates under the authority of the United States Navy. The Coast Guard later moved to the Department of Transportation in 1967, and on February 25, 2003 it became part of the Department of Homeland Security.
 As members of a military service, Coast Guardsmen on active and reserve service are subject to the Uniform Code of Military Justice and receive the same pay and allowances as members of the same pay grades in the other four armed services.
 The Coast Guard provides unique benefits to the nation because of its distinctive blend of military, humanitarian, and civilian law-enforcement capabilities. To serve the public, the Coast Guard has five fundamental roles:
 Maritime Safety
 The goal of safety is pursued primarily through our search and rescue and marine safety missions. Search and Rescue (SAR) is perhaps the Coast Guard's best-known mission area, and the service is recognized as the world's leader by the international SAR community. When the rescue alarm sounds, the Coast Guard is ready to confront the inherently dangerous maritime environment, frequently going into harm's way to save others. The Coast Guard works closely with other federal, state, and local agencies, and with foreign nations, to provide the world's fastest and most effective response to distress calls. It also maintains a vessel-tracking system called Amver (automated mutual assistance vessel rescue) that allows it to divert nearby commercial vessels to render assistance when necessary.
 The Coast Guard's Marine Safety Program promotes safety through both its regulatory and inspection roles, inspecting merchant vessels and licensing their masters and crews. The Coast Guard's goal is to reduce crewmember deaths and injuries on U.S. commercial vessels; passenger deaths and injuries; and the number of collisions and groundings in the waters under Coast Guard jurisdiction.In a dedicated effort to prevent future mishaps, the Coast Guard investigates maritime accidents. Lessons learned from accident investigations are fed back into prevention programs, frequently in the form of revised regulations and safety standards. As an international leader, the Coast Guard works with other nations and agencies—like the International Maritime Organization, for example—to promote higher safety standards for commercial vessels and their crews.
 As the lead U.S. representative to the International Maritime Organization, a specialized agency of the United Nations, we are the driving force behind the implementation of international safety and pollution standards. However, not every country enforces these standards. With the increase in so-called flags of convenience, has come an increase in the number of substandard vessels. The Coast Guard has developed a comprehensive U.S. strategy to promote and improve the Port State Control program, an international effort to bring substandard ships into compliance with applicable international standards or remove them from the sea.
 In addition to commercial vessels, more than 76 million recreational boaters use our waterways. Our recreational boating safety program is focused on minimizing the loss of life and property and damage to the environment. The Coast Guard Auxiliary, the 35,000-person civilian volunteer arm of the Coast Guard, is a key contributor to these boating safety efforts and has augmented our missions for over 60 years. The Coast Guard Auxiliary provides free boating safety courses, courtesy marine examinations for recreational boaters, verification for aids to navigation, and inspections of commercial facilities.
 Maritime Security
 Since 1790, the Coast Guard has served as America's principal "law of the sea" agency. Originally established by Alexander Hamilton as the Revenue Marine, the Coast Guard began with the mission of enforcing import tariffs. Since then its maritime-security responsibilities have expanded exponentially, and almost always synergistically, to include the enforcement of all federal laws at sea—from stopping pirates to enforcing vessel-safety regulations and fisheries conservation laws to interdicting drug and migrant smugglers. Because the Coast Guard has law-enforcement authority, it can apprehend foreign fishing vessels engaged in poaching, interdict vessels carrying illegal drugs and undocumented migrants, and stop unsafe boaters.Today, U.S. national-security interests can no longer be defined solely in terms of direct military threats to America and its allies. Working under the necessarily broader current definition of national security, the Coast Guard is seeking to reduce the risk from terrorism to U.S. passengers at foreign and domestic ports and in designated waterfront facilities, but it faces the extremely difficult challenge of enforcing increasingly complex laws against highly sophisticated adversaries. Coast Guard boarding teams deal continuously with violations of multinational fisheries agreements and foil high-tech attempts to smuggle drugs into the United States.
 The influx of illegal drugs is one of America's maritime-security problems. As the nation's leading maritime agency in protecting the U.S. public from the drug threat, the Coast Guard plays a key role in implementing the President's national drug-control strategy. Despite the vast complications in enforcement, the Coast Guard performs this new task with only modest additional funding. A tremendous number of assets are required to patrol the long coastlines of the United States and the even greater expanse of waters encompassing the maritime "transit zones" used by drug smugglers. This six-million-square-mile area, roughly the size of the continental United States itself, includes the Caribbean, the Gulf of Mexico, and the Eastern Pacific.  
 The protection of U.S. living marine resources—primarily through the detection and deterrence of illegal fishing activity—is another of the Coast Guard's historic mission areas of responsibility that continues to expand. Beginning with the protection of the Bering Sea fur seal and sea otter herds and continuing through the vast expansion following World War II in the size and efficiency of global fishing fleets, Coast Guard responsibilities in this mission area now include enforcement of laws and treaties in the 3.36-million-square-mile U.S. Exclusive Economic Zone (EEZ), the largest in the world.
 The flood of undocumented migrants in boats onto America's shores is both a threat to human life and a violation of U.S. and international laws. Coast Guard migrant-interdiction operations are as much humanitarian efforts as they are law-enforcement missions. In fact, the majority of migrant interdiction cases handled by the Coast Guard actually begin as search-and-rescue missions, usually on the high seas rather than in U.S. coastal waters. The Coast Guard is the lead agency for the enforcement of U.S. immigration laws at sea, stressing sensitivity in dealing with undocumented migrants in all realms: mass migration, asylum/refugee requests, smuggling and repatriation. In its effort to increase U.S. security against undocumented migrations, the Coast Guard constantly monitors maritime transit zones, interdicting undocumented migrants, rescuing people from sinking or unsafe vessels, providing humanitarian assistance, and training nations to discourage undocumented migration into the United States.
 Maritime Mobility
 As the nation's lead agency for waterways management, port safety and security, and vessel safety inspection and certification, the Coast Guard maintains a continuous and clear focus not only on the prevention of marine accidents but also on the response measures needed to cope with manmade and natural disasters. The Coast Guard also is responsible for maintaining and patrolling the safe and efficient navigable waterways system needed to support domestic commerce, facilitate international trade, and ensure the continued availability of the military sealift fleet required for national defense. Domestic icebreakers that keep shipping lanes open for commercial traffic in winter and the Vessel Traffic Services system that coordinates the safe and efficient movement of commercial vessels through congested harbors are two examples of how the Coast Guard maintains the waterways.
 The Coast Guard maintains the "signposts" and "traffic signals"—more than 50,000 federal aids to navigation, including buoys, lighthouses, day beacons, and radio-navigation signals—on the nation's waterways. These navigation aids provide a critical component of the overall navigational picture needed by all mariners. The Coast Guard's maritime Differential Global Positioning System network is fully operational and provides boaters and mariners the most accurate electronic maritime navigation system available. Like plowing snow-covered roads, Coast Guard domestic icebreakers keep shipping lanes open as much as is reasonably possible for commercial traffic in winter. In congested harbors, the Coast Guard coordinates the safe and efficient movement of commercial vessels through its Vessel Traffic Services system.The Coast Guard is also responsible for approximately 18,000 highway and railroad bridges that span navigable waterways throughout the country. The Coast Guard issues permits for bridge construction, orders obstructive bridges to be removed, and oversees drawbridge operations.
 National Defense
 The Coast Guard's national defense role to support U.S. military commanders-in-chiefs (CINCs) is more explicitly outlined in a memorandum of agreement signed by the Secretaries of Defense and Transportation in 1995. Four major national-defense missions were assigned to the Coast Guard. These missions—maritime intercept operations, deployed port operations/security and defense, peacetime engagement, and environmental defense operations—are essential military tasks assigned to the Coast Guard as a component of joint and combined forces in peacetime, crisis, and war. 
 Outside of U.S. coastal waters, the Coast Guard assists foreign naval and maritime forces through training and joint operations. Many of the world's maritime nations have forces that operate principally in the littoral seas and conduct missions that resemble those of the Coast Guard. And, because it has such a varied mix of assets and missions, the Coast Guard is a powerful role model that is in ever-increasing demand abroad. The service's close working relations with these nations not only improve mutual cooperation during specific joint operations in which the Coast Guard is involved but also support U.S. diplomatic efforts in general: promoting democracy, economic prosperity, and trust between nations.
 Protection of Natural Resources
 The Coast Guard's role in environmental protection dates back more than 175 years to the 1822 Timber Act that tasked the Revenue Cutter Service with protecting government timber from poachers. The Coast Guard is still protecting the country's valuable natural marine resources. Today, however, the principal dangers are overfishing and foreign poaching. In the fight to protect the biomass within the U.S. Exclusive Economic Zone, the Coast Guard is working on numerous fronts—e.g. patrolling the closed fishing grounds off New England so that depleted species have an opportunity to return to harvestable levels. Through close cooperation with other federal and foreign agencies, the Coast Guard also is gaining ground against the illegal use of high-seas driftnets in the Pacific Ocean. Coast Guard cutters remain on constant patrol in the Bering Sea to prevent foreign vessels from poaching in the fish-rich Alaskan waters.
 The Coast Guard is also playing an increasingly important role in the nation's efforts to protect its threatened and endangered species. In the Gulf of Mexico, the Coast Guard helps protect endangered sea turtles from being caught in indiscriminate fishing nets. Along the Atlantic coast, Coast Guard units help free endangered northern right whales that have become entangled in fishing gear. In Hawaii, Coast Guard buoy tenders remove tons of marine debris from the coral reef habitat of the Hawaiian monk seal. From patrolling Steller sea lion rookeries in Alaska to enforcing manatee speed zones in Florida, the Coast Guard plays a vital role in helping the nation recover and maintain healthy populations of marine protected species.
 The Coast Guard also has pioneered the fight against water pollution. Its Research and Development Center developed a technique to "fingerprint" oil to identify the source of a spill. Today, the Coast Guard's National Strike Teams are on-call 24 hours a day to respond to accidents and spills in the marine environment. The service also enforces federal regulations to reduce the dumping of refuse and sewage from vessels of all types. Through a public education program called Sea Partners, the Coast Guard is promoting the importance of a clean marine environment and is, in addition, working closely with foreign nations and international agencies to reduce the number of marine accidents (and resulting spills) by establishing and rigorously enforcing improved safety standards for commercial vessels and their crews.
 To reach the longer-term goal of virtually eliminating environmental damage to U.S. waterways, the Coast Guard pursues an aggressive three-pronged approach encompassing prevention, enforcement, and response. The service has partnered with the maritime industry to develop new safety standards for commercial vessels and their crews, and enforces those standards through rigorous testing and thorough investigations into marine accidents and spills.
 The Secretary of Homeland Security is the head of the Department of Homeland Security, the agency of the government charged with protecting the American homeland and the safety of American citizens. The department was created in the aftermath of the September 11, 2001 terrorist attacks on New York, Washington, and Pennsylvania. The Secretary oversees components also involved with homeland security in addition to the Coast Guard, such as the United States Border Patrol and the Federal Emergency Management Agency (FEMA). 
 The Commandant is the highest-ranking member of the United States Coast Guard. He is the only four-star Admiral of the Coast Guard, and is appointed for a four-year term by the President of the United States upon confirmation of the Senate. He is assisted by a Vice-Commandant, two Assistant Commandants/Area Commanders and a Chief of Staff, all of whom are three-star Vice Admirals. Unlike the Commandant of the Marine Corps, The Coast Guard Commandant is not a member of the Joint Chiefs of Staff. He reports to the President, the Secretary of Homeland Security and the Secretary of Defense. Prior to the creation of the Department of Homeland Security in 2003, the Coast Guard Commandant reported to the Secretary of Transportation.
 Enlisted personnel begin service at the Coast Guard Training Center Cape May in Cape May, New Jersey, for eight weeks of basic training.
 The office of the Master Chief Petty Officer of the Coast Guard (MCPOCG) was established by legislative action on August 27, 1969 to provide the Commandant with a personal advisor and assistant in matters affecting the enlisted members of the Coast Guard, both active and reserve, and their families. The MCPOCG is the most senior enlisted member of the Coast Guard, with the pay grade designation of E-10. The normal tour of assignment is four years, which runs concurrently with the Commandant of the Coast Guard.
 Enlisted personnel of pay grades E-4 and above have a specific symbol identifying their job specialty (called a "rate") above their chevrons (the rating pictured is that of boatswain mate). Sailors E-3 and below who have successfully struck for a rating also have their rating symbol above their stripes.  
 The chevrons of all petty officers are normally red in color; they change to gold when that sailor has past twelve consecutive years of good conduct on his/her service record. In addition, the color change is also reflected in the service striping on the lower-left sleeve of the service dress blue uniform.
 The U.S. Coast Guard uses a variety of platforms to conduct its daily business. Cutters and small boats are used on the water and fixed and rotary wing (helicopters) aircraft are used in the air. 
 Aircraft
 There are a total of 211 aircraft in CG inventory. This figure fluctuates operationally due to maintenance schedules. Major Missions: Search/Rescue, Law Enforcement, Environmental Response, Ice Operations, and Air Interdiction. Fixed-wing aircraft (C-130 Hercules turboprops and HU-25 Falcon jets) operate from large and small Air Stations. Rotary wing aircraft (HH-65 Dolphin and HH-60 Jayhawk helicopters) operate from flight-deck equipped Cutters, Air Stations and Air Facilities.
 Boats
 (Approximately 1400 - number fluctuates). All vessels under 65 feet in length are classified as boats and usually operate near shore and on inland waterways. Craft include: Motor Lifeboats; Motor Surf Boats; Large Utility Boats; Surf Rescue Boats; Port Security Boats; Aids to Navigation Boats; and a variety of smaller, non-standard boats including Rigid Inflatable Boats. Sizes range from 64 feet in length down to 12 feet.
 Cutters
 A "Cutter" is basically any CG vessel 65 feet in length or greater, having adequate accommodations for crew to live on board. Larger cutters (over 179 feet in length) are under control of Area Commands (Atlantic Area or Pacific Area). Cutters at or under 175 feet in length come under control of District Commands. Cutters, usually have a motor surf boat and/or a rigid hull inflatable boat on board. Polar Class icebreakers also carry an Arctic Survey Boat (ASB) and Landing Craft. 
 This article was last edited in 2010. Some of its information may be outdated. 1 Overview 2 Organization
2.1 Secretary of Homeland Security
2.2 Commandant of the Coast Guard
 2.1 Secretary of Homeland Security 2.2 Commandant of the Coast Guard 3 Personnel
3.1 Enlisted
 3.1 Enlisted 4 Vessals and aircraft 5 See also 6 External links HC-130 Hercules  HU-25 Guardian  HH-60 Jayhawk  HH-65 Dolphin  MH-68A Stingray  47' Motor Life Boat (MLB)  41' Utility Boat (UTB)  21'-64' Aids to Navigation Boats  25' Transportable Port Security Boat (TPSB)  25' Defender Class Boats (RB-HS/RB-S)  420' Icebreaker (WAGB)  399' Polar Class Icebreaker (WAGB)  378' High Endurance Cutter (WHEC)  295' Training Barque Eagle (WIX)  282' Medium Endurance Cutter (WMEC)  270' Medium Endurance Cutter (WMEC)  240' Seagoing Buoy Tender/ Icebreaker (WLBB)  230' Medium Endurance Cutter (WMEC)  225' Seagoing Buoy Tender (WLB)  213' Medium Endurance Cutter(WMEC)  210' Medium Endurance Cutter (WMEC)  175' Coastal Buoy Tender (WLM)    179' Patrol Coastal (WPC)    160' Inland Construction Tender (WLIC)   140' Icebreaking Tug (WTGB)  123' Patrol Boat (WPB)  110' Patrol Boat (WPB)    100' Inland Buoy Tender (WLI)  100' Inland Construction Tender (WLIC)  87' Coastal Patrol Boat (WPB)    75' River Buoy Tender (WLR)  75' Inland Construction Tender (WLIC)  65' River Buoy Tender (WLR)  65' Inland Buoy Tender (WLI)  65' Small Harbor Tug (WYTL)  There are no atheists on a sinking ship USCG Home Page Daily Chronology of Coast Guard History The Coast Guard saves about 10 lives each day. Fox News Video interviewing Admiral Mary Landry on Coast Guard Day on August 4, 2007 celebrating 217 years of Coast Guard history. [1] Outdated Articles United States Department of Homeland Security Military Strategies and Concepts Military Glossary United States Government Systems of Support United States Government Agencies Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 21 August 2019, at 12:02. This page has been accessed 14,918 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 United States Coast Guard "Semper Paratus" Created04 August 1790 Material Cutters Boats Helicopters Aircraft Personnel Officers Enlisted Reserves Cadets Civilian employees Chief officer candidate school Enlisted bootcamp Leadership Secretary of Homeland Security Commandant Master Chief Petty Officerof the Coast Guard United States Coast Guard Maritime Safety Maritime Security Maritime Mobility National Defense Protection of Natural Resources Commissioned Officers (collar device, shoulder board, sleeve insignia) Pay grade Chief Warrant Officers Enlisted Pay grade Aircraft Boats Cutters Copyright Details 
 "Semper Paratus"Created04 August 1790
 Material
 Cutters
 252
 Boats
 1600
 Helicopters
 145
 Aircraft
 32
 Personnel
 Officers
 8,051 (2008)
 Enlisted
 32,647 (2008)
 Reserves
 11,000
 Cadets
 225
 Civilian employees
 7,396 (2008)
 Chief officer candidate school
 United States Coast Guard AcademyNew London, Connecticut
 Enlisted bootcamp
 Coast Guard Training CenterCape May, New Jersey
 Leadership
 Secretary of Homeland Security
 Janet Napolitano
 Commandant
 Admiral Thad Allen
 Master Chief Petty Officerof the Coast Guard
 MCPOCG Charles W. Bowen
 Insignia
  
  
  
  
  
  
  
  
  
  
 Title
 Admiral
 Vice Admiral
 Rear Admiral (UH)
 Rear Admiral (LH)
 Captain
 Commander
 Lieutenant Commander
 Lieutenant
 Lieutenant, Junior Grade
 Ensign
 
  
  
  
 Sleeve patch
  
  
  
  
  
  
  
  
  
  
 Collar and cap devices
  
  
  
  
  
  
  
 
 
 
 Title
 Master Chief Petty Officer of the Coast Guard
 Master Chief Petty Officer
 Senior Chief Petty Officer
 Chief Petty Officer
 Petty Officer, 1st Class
 Petty Officer, 2nd Class
 Petty Officer, 3rd Class
 Seaman
 Seaman Apprentice
 Seaman Recruit
 
 Copyright Details
 License:
  This work is in the public domain in the United States because it is a work of the United States Federal Government under the terms of Title 17, Chapter 1, Section 105 of the US Code.
 Source:
  [2]
  
  This image may not be freely used on user pages.
  If you think this image is incorrectly licensed you may discuss this on the image's talk page.
 United States Coast Guard Contents Overview Organization Personnel Vessals and aircraft See also External links Navigation menu Secretary of Homeland Security Commandant of the Coast Guard Enlisted Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
