Josiah (Hebrew יֹאשִׁיָּהוּ, "supported by YHWH", or Greek Ἰωσίας) (649-r. 641-610 BC according to Ussher,[1] or 648-r. 640-609 BC according to Thiele[2]) was the fifteenth king of the Southern Kingdom of Israel in direct line of descent.
 Some commentators suggest that Josiah, rather than David, qualifies as the greatest of the kings of ancient Israel.[3] He certainly stands as one of the greatest of Israel's kings and one of that society's two greatest reformers.[4][5][6][7][8][9][10][11][12][13][14] His reign is also significant for the beginning of the assembly of the Old Testament[1][2][15][16] and for being king during a pivotal time in Middle Eastern history.[1][2][7][9]
 Josiah was probably born in 649 BC, the son of Amon and Amon's wife Jedidah. He was six years old when his grandfather Manasseh died. A mere two years later, he suddenly found himself king when his father was murdered by a palace conspiracy. The outraged people of the Southern Kingdom arrested and killed all the conspirators, and then crowned the eight-year-old Josiah king.[1][2][6][7][11][12][13][17]
 For the first eight years of his life, he seems to have followed the same idolatrous worship that his father followed. But at the age of sixteen, he had a change of heart: he began to seek after God.[6][7][11][12][13][18]
 Also at sixteen, he married a woman named Zebudah and by her had a son named Jehoiakim, called Eliakim when he was born. Two years later (at eighteen), he married another woman, named Hamutal, and by her had a son named Jehoahaz (originally named Shallum). He also had another son by this woman, named Zedekiah (or Mattaniah at his birth), when he, Josiah, was thirty-one years old.[1]
 Josiah did have one other son, named Johanan.[19] His position in the listing in that verse makes him the firstborn of Josiah's sons. The Bible does not mention him again, however. Possibly he perished by his father's side in the military fiasco involving Pharaoh Necho II (see below).
 The political situation in the ancient Near East at the time was critical to Josiah's activities and the eventual fate of the Southern Kingdom. The Assyrian king Esarhaddon had died long ago, in 668 BC.[20] Conventional Assyriologists[2][7] hold that Assur-bani-pal (or "Saosduchinos"), Esarhaddon's successor, was still on the throne at the time; Ussher[21] states that his successor Ashur-etil-ilani, called "Saraco" or "Saracen" or Kineladanos" by classical sources, had succeeded to the throne a year after Josiah was born. At least one other source says that Assur-bani-pal died at this time, and that near-chaos supervened.[12] Everyone seems to agree, however, that the empire of Assyria was far weaker than it once had been under men like Esarhaddon and his predecessors. Egypt, under Pharaoh Psammtik I, had broken away from Assyria, and Babylonia would soon see the rise of a new king (Nabopolassar) who would break it away and supplant Assyria as the dominant power in the region.
 When Josiah was twenty years old (in the twelfth year of his reign), he began in ernest to reform the society of the Southern Kingdom. He started in Jerusalem, systematically destroying all pagan images and altars of Baal, all Asherah poles, and all the high places that his grandfather had built up after Hezekiah had destroyed them. He made the most thorough desecration possible of the pagan icons and their followers:
 Nor did he stay within what was, strictly speaking, his home territory of Judah and Benjamin. In what some commentators suggest was part of a bid to regain control of the former territories of the Northern Kingdom, he ventured into the old tribal territories of Ephraim, Manasseh, Simeon, and Naphtali.[24] The Assyrian king (whichever was on the throne at the time) was too weak to interfere.
 In the thirteenth year of his reign, the prophet Jeremiah began his career, howbeit reluctantly.[25] Baleful as Jeremiah's message was, Josiah never once molested Jeremiah in any way, shape or form. His sons would not be so forbearing.
 In the sixteenth year of his reign came an event of which Josiah appears to have taken little notice at the time, though it would be relevant to the manner of his death. In that year, Nabopolassar contracted a military alliance with Astyages of the Medes. Together they attacked Nineveh, the capital of Assyria, and reduced it to a ruin. Nabopolassar was now in command, and Ashur-etil-ilani was now dead. His successor (likely Ashur-uballit II) commanded a shadow of what Assyria had recently been.[26]
 In the eighteenth year of his reign, Josiah ordered the priest Hilkiah to begin yet another renovation of the Temple of Jerusalem. He sent the royal secretary, the royal recorder, and the mayor of Jerusalem to carry this order to Hilkiah and to give Hilkiah the money, which had been collected at the Temple door, for this purpose.[27][28]
 In the course of clearing out a Temple storeroom, Hilkiah found a scroll.[8][11][29][30] The Bible calls this "the book of the Law as given by Moses." Most scholars theorize that this was the book of Deuteronomy,[15][16] but at least some hold that this book contained all five of the "Books of Moses".[2][16] Still others state that even more books of the Old Testament were included in the find.[9]
 Malick presents, and effectively refutes, the proposition that Hilkiah's finding was a "late edition" of Deuteronomy, mainly on the ground that Josiah must have had some education in the laws as stated in Deuteronomy during the first six years of his reform program. He therefore concludes that it was at least an early edition of Deuteronomy and might indeed have been the entirety of the Torah.[16] Wood[2] theorizes that Solomon left this book in the Temple when he laid its cornerstones, and that this is why it survived the destruction, presumably by Manasseh, of all other copies.
 Hilkiah gave this scroll to the royal secretary, who read it all the way through. He in turn requested an audience of the king. The scribe began by giving Josiah a progress report on the renovation. He then said, simply, "Hilkiah has given me a book," and proceeded to read it. Josiah seems to have listened, rapt, at the reading. When the reading was concluded, Josiah tore his royal robes. Evidently Josiah had not realized, until then, just how far the people of his kingdom had strayed from God's law.[31][32]
 Josiah's first order was to inquire of a prophet as to the attitude of God toward his people. His scribe, the priest, and his other advisers found a prophetess named Huldah. She gave a dire warning: that God would indeed bring a dire judgment on the land, on account of the apostasy, the killing of earlier prophets, and all the other many provocations that the people had given God. But because Josiah had expressed such deep sorrow, God granted to him that he would not live to see this calamity.[8][33][34]
 Josiah worked harder than ever at his reform program after that. First he called an assembly of the people and read the Book of the Law aloud to all of them.[10][35][36] Then he renewed his religious cleansing campaign.[37] At this time he fulfilled an earlier prophecy delivered to Jeroboam I concerning Jeroboam's golden-calf cult: that the bones of the priests who officiated at that altar would be burned upon it. He also found the bones of the prophet who had uttered that prophecy; those bones he left where they were.[7][15][38]
 He then reinstituted the Passover, exactly as Hezekiah had done, except that Josiah was able to keep it in the appointed month.[7][39][40]
 In 610 BC, Pharaoh Necho II marched toward Carchemish in an effort to intervene against the rising empire of Babylonia.[2][7][12][41] Or perhaps he was settling the old score that Egypt had against Assyria.[1][5]
 His march would carry him across the territory of the Southern Kingdom.[6][13] Josiah, for whatever reason, determined to oppose Necho. Necho sent ambassadors warning him that he was on an errand from God himself and that Josiah had no business interfering. Nevertheless, Josiah joined battle with Necho at Megiddo.[42]
 Josiah was seriously wounded by an arrow early in the battle.[43] Here the Biblical accounts differ. The author of the Kings books suggests that Josiah died instantly and was brought back dead to Jerusalem.[44] The Chronicler, on the other hand, stated in detail that Josiah was wounded, asked his charioteer to evacuate him, and was brought swiftly back to Jerusalem, where he ultimately died of his wounds.[45] Jeremiah composed a special lamentation for him.[46]
 Josiah's son Shallum, or Jehoahaz, succeeded him.
 1 Significance 2 Early Life and Family 3 Near Eastern politics 4 Beginnings of reform 5 Political flux 6 The Finding of the Book of the Law 7 Continued Reforms 8 Military tragedy and succession 9 References Cited 10 See also  He ground the images (including engraved and cast images) to powder and strewed this powder on the graves of Baal worshippers.[22]  He burned the bones of the priests of Baal on the very altars they had used.[23] ↑ 1.0 1.1 1.2 1.3 1.4 1.5 James Ussher, The Annals of the World, Larry Pierce, ed., Green Forest, AR: Master Books, 2003 (ISBN 0890513600), pghh. 720, 728-9, 732, 737-8, 740-741, 743-44, 746, 750, 754-760
 ↑ 2.0 2.1 2.2 2.3 2.4 2.5 2.6 2.7 Leon J. Wood, A Survey of Israel's History, rev. ed. David O'Brien, Grand Rapids, MI: Academie Books, 1986 (ISBN 031034770X), pp. 310-314
 ↑ Anonymous, The Greatest King of Israel, 1998. Retrieved April 9, 2007 from Cross Pollen
 ↑ II_Chronicles 34:2 (KJV)
 ↑ 5.0 5.1 Josiah at the WebBible Encyclopedia
 ↑ 6.0 6.1 6.2 6.3 Wayne Blank, Josiah's Reforms, Church of God Daily Bible Study. Retrieved April 9, 2007
 ↑ 7.0 7.1 7.2 7.3 7.4 7.5 7.6 7.7 Dennis Bratcher, The Rise of Babylon and Exile (640-538 BC), 2006. Retrieved April 9, 2007.
 ↑ 8.0 8.1 8.2 David Holt Boshert, Jr., and David Ettinger, Josiah King of Judah, Christ-Centered Mall. Retrieved April 9, 2007
 ↑ 9.0 9.1 9.2 Marvin A. Sweeney, King Josiah of Judah: the Lost Messiah of Israel, Oxford University Press, 2001. ISBN 978-0-19-513324-0 Online version retrieved April 9, 2007.
 ↑ 10.0 10.1 Jennifer Rosania, Josiah: Founded in Faithfulness, Mighty in Spirit, In-touch Ministries, 2006. Retrieved April 9, 2007
 ↑ 11.0 11.1 11.2 11.3 Emil G. Hirsch and J. F. McLaughlin, Josiah, Jewish Encyclopedia, 202. Retrieved April 9, 2007
 ↑ 12.0 12.1 12.2 12.3 12.4 Anonymous, Josiah, Encyclopedia Britannica, 2007. Retrieved April 9, 2007
 ↑ 13.0 13.1 13.2 13.3 Anonymous, King Josiah - Biography, Kings of Israel. Retrieved April 9, 2007
 ↑ Anonymous, God's Judgment Regarding King Josiah, Kings of Israel. Retrieved April 9, 2007
 ↑ 15.0 15.1 15.2 John L. Kachelman, Jr., Josiah: Serving God in Youth, ChristianLibrary.org, 1999. Retrieved April 9, 2007
 ↑ 16.0 16.1 16.2 16.3 David Malick, The Book of Josiah's Reform, Bible.org, 2007. Retrieved April 9, 2007
 ↑ II_Kings 22:1 (KJV)
 ↑ II_Chronicles 34:3 (KJV)
 ↑ I_Chronicles 3:15 (KJV)
 ↑ Ussher, op. cit., pgh. 705
 ↑ Ussher, op. cit., pgh. 715
 ↑ II_Chronicles 34:4 (KJV)
 ↑ II_Chronicles 34:5 (KJV)
 ↑ II_Chronicles 34:6 (KJV)
 ↑ Jeremiah 1:1-17 (KJV)
 ↑ Ussher, op. cit., pgh. 740
 ↑ II_Chronicles 34:7-13 (KJV)
 ↑ II_Kings 22:3-7 (KJV)
 ↑ II_Kings 22:8 (KJV)
 ↑ II_Chronicles 34:14-15 (KJV)
 ↑ II_Kings 22:9-11 (KJV)
 ↑ II_Chronicles 34:16-19 (KJV)
 ↑ II_Kings 22:12-20 (KJV)
 ↑ II_Chronicles 34:20-29 (KJV)
 ↑ II_Kings 23:1-3 (KJV)
 ↑ II_Chronicles 34:30-32 (KJV)
 ↑ II_Chronicles 34:33 (KJV)
 ↑ II_Kings 23:4-20,24-25 (KJV)
 ↑ II_Kings 23:21-23 (KJV)
 ↑ II_Chronicles 35:1-19 (KJV)
 ↑ Josiah by Wikipedia®
 ↑ II_Chronicles 35:20-22 (KJV)
 ↑ II_Chronicles 35:23 (KJV)
 ↑ II_Kings 23 (KJV)
 ↑ II_Chronicles 35:23-24 (KJV)
 ↑ Lamentations 4:20 (KJV)
  Hezekiah  Manasseh King of Judah  Amon  Jehoahaz II  Esarhaddon  Assur-bani-pal  Ashur-etil-ilani  Ashur-uballit II  Nabopolassar  Nebuchadnezzar II  House of David  Northern Kingdom  Biblical chronology dispute Kings of Israel Jewish People Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 26 September 2018, at 11:54. This page has been accessed 10,089 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Josiah יֹאשִׁיָּהוּ Ἰωσίας v • d • e
Kings of IsraelUnited Kingdom of IsraelSaul · Ishbosheth · David · SolomonKingdom of IsraelJeroboam I · Nadab · Baasha · Elah · Zimri · Omri · Ahab · Ahaziah · Jehoram · Jehu · Jehoahaz · Joash · Jeroboam II · Zachariah · Shallum · Menahem · Pekahiah · Pekah · HosheaKingdom of JudahRehoboam · Abijam · Asa · Jehoshaphat · Jehoram · Ahaziah · Athaliah · Joash · Amaziah · Uzziah · Jotham · Ahaz · Hezekiah · Manasseh · Amon · Josiah · Jehoahaz II · Jehoiakim · Jehoiachin · ZedekiahCategories  United Kingdom of Israel Saul · Ishbosheth · David · Solomon   Kingdom of Israel Jeroboam I · Nadab · Baasha · Elah · Zimri · Omri · Ahab · Ahaziah · Jehoram · Jehu · Jehoahaz · Joash · Jeroboam II · Zachariah · Shallum · Menahem · Pekahiah · Pekah · Hoshea  Kingdom of Judah Rehoboam · Abijam · Asa · Jehoshaphat · Jehoram · Ahaziah · Athaliah · Joash · Amaziah · Uzziah · Jotham · Ahaz · Hezekiah · Manasseh · Amon · Josiah · Jehoahaz II · Jehoiakim · Jehoiachin · Zedekiah  Categories Josiah Contents Significance Early Life and Family Near Eastern politics Beginnings of reform Political flux The Finding of the Book of the Law Continued Reforms Military tragedy and succession References Cited See also Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
