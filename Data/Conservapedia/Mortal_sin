Sections
Old Testament
New Testament
Pentateuch
The Gospels
 About
History
Canon
Exegesis
Accuracy
Criticism
Inerrancy
Literalism
Chronology
Translations
Hermeneutics
 Concepts
Sabbath
Sin
Resurrection
Prophet
The Virgin Birth
Tithe
 See also
Christianity
 Sin is a failure to conform to the standard of morality, or is the state of the world that results from such failure, and includes both actively conscious and implicitly unconscious rejection of God.
Although we fantasize that sinful, selfish acts will bring us relief from pain, they don't really make us happy. "Reality is that sin is heartless and cruel. Reality is that sin always, always hurts you as well as others. The reality is that sin leads to death. Every single time. No good can come from sin. Ever." [1] According to theologian Emil Brunner, sin is desire for the autonomy of a man; therefore, in the last resort, it is the denial of God and self-deification.[1] Bible (namely in 1 John 3:4), calls sin a lawlessness.[2]
 The root meaning of the English word sin is actually, "He is guilty as charged." This in turn implies that the person committing the offense knew, or ought to have known, that his act would be an offense before he committed it. This word actually captures the meaning that many religious traditions ascribe to sin.
 The Greek word used in the original New Testament and translated "sin" in English is ἁμαρτία (hamartia) (whence hamartoma a malformation of cells that are within their usual place in the body). This word captures the Christian meaning of sin much better. Hamartia means missing a target. Thus sin does not require bad intentions, but might result from a misunderstanding. This is not to say that the consequences are any less dire, however. It does mean that sin need not be intentional; it can be a knowing (but not intentional), reckless, or negligent act.
 Sin, which is a moral failure, cannot exist in the absence of a code of morality. 
 This is not to say that sin requires a context of divinity. The context in which sin ceases to have meaning is not atheism per se but rather amoralism — the doctrine that says that no person may properly define what is moral for another, because no two people will ever value the same thing to the same degree, or value the same collection of things by the same rank structure.
 Whether any man ever lives without some form of moral code—even if that code is, "I denounce as sin any disagreement with myself or any attempt to stand in my way"—is debatable. Nevertheless, without some form of moral context, sin has no meaning.
 In Judaism, and certainly in ancient practice, the preferred method of atonement for sin was blood sacrifice. The Book of Leviticus prescribed multiple animal sacrifices for atonement for various types of sin. But by far the most important atonement—on the Day of Atonement (Yom Kippur)—was made by the high priest of the Hebrew people (originally Aaron, brother of Moses). This involved making a blood sacrifice, entering the Holy of Holies (in Latin, sanctum sanctorum), and sprinkling some of the blood from the sacrifice on the Ark of the Covenant.
 Animal sacrifice has not been part of Jewish practice since the destruction of the Temple of Jerusalem by the future Emperor Titus in 70 AD. But the Old Testament did not imply that animal sacrifice was the sole necessary measure for reconciliation. Simple repentance and prayer sufficed numerous times in Bible history.
 Today, though animal sacrifices are no longer observed, Jews do observe Yom Kippur and other festivals related to repentance and reconciliation. But a movement now exists to attempt to regain exclusive control of the Temple Mount area in Jerusalem, rebuild the Temple, and resume the burnt offerings, sin offerings, and guilt offerings prescribed in the Torah.
 In traditional Jewish thought, sin may be balanced by the good deeds one performs throughout life; the image of scales weighing one's life are prominent in folklore, similar to other ancient Middle Eastern religions such as ancient Egypt.
 Sin (Greek: hamartia. The word exists in English in a contracted form, harm, which refers to a failing or "missing the mark". Harm can be done to self or others) as defined by the New Testament is found in 1st John 3:4, "Whosoever committeth sin transgresseth also the law; for sin is the transgression of the law." (KJV)  There are many scriptures that give testimony to the punishment for sin, Rom 6:23 for example, lets us know the wages of sin is death, but the basic definition of sin is breaking Gods law.
 Catholics classify sins as venial sin and mortal sin (grave sin).[4] See John 19:11 "the greater sin", and 1 John 5:16-17 "All wrongdoing is sin, but there is sin which is not mortal."
 Roman Catholic doctrine distinguishes original sin, or the sin of Adam, from personal sins, which require individual reconciliation with God. A personal sin may be mortal (a particularly grave matter that the perpetrator commits by deliberate intent) or venial (a less grave matter that could result from a misunderstanding). The RCC distinction is: Mortal sins cut a person off from the grace of God; venial sins do not, but still injure one's relationship to God. A mortal sin is defined in the Roman Catholic church as being any transgression (a) regarding a grave matter, (b) which was premeditated and (c) committed with full knowledge of its sinful nature. A venial sin is defined as any sin which does not fulfil all three conditions for a mortal sin. The Bible teaches differing degrees of sin and the fact that some sins are not mortal. 
 A person may receive reconciliation for a mortal sin by making confession to a priest  and receiving absolution ("For where two or three are gathered in my name, there am I in the midst of them" Matthew 18:20, and verse 18; see John 20:22-23, 2 Corinthians 5:18-20, James 5:18; compare John 8:3-11); venial sins may optionally be confessed also. A Catholic who has committed mortal sin is to refrain from receiving the Holy Eucharist until receiving sacramental confession.
 Roman Catholic doctrine holds that not only was Jesus sinless, but so, also, was His Mother, Mary, not of herself, but by the unmerited grace of God through His Son bestowed upon her. The belief that the Virgin Mary was sinless at conception is called the Immaculate Conception. 
 The Eastern Orthodox Church teaches that the "all-holy" (Greek panagia) Virgin Mary was born with Original Sin but that she was cleansed of this sin at the time of the Annunciation. The Church also teaches that by free will she did not commit any actual sin. 
 This doctrine, called Marianism by some, is not universally held by all Catholics, however. Nevertheless, the Four Marian Dogmas[5] are required beliefs for all Catholics, and an "obstinate, post-baptismal", firm rejection of any one of them automatically excommunicates the disbeliever from communion with the Catholic Church.[6]
 The Anglican Communion, essentially the breakaway church of the United Kingdom, retains the Roman Catholic tradition of confession and absolution of sin. But instead of an individual process, confession in Anglican churches is a group activity, and absolution is pronounced by a minister to the entire church-going group. Purgatory and indulgences have no place in Anglican doctrine.
 In the Eastern Orthodox Church there are no "categories" of sin as found in the Christian West. In the pre-Vatican II Roman Catholic catechism, sins were categorized as "mortal" and "venial."  These categories do not exist in the Orthodox Church.  
 Orthodoxy believes that, while everyone bears the consequences of the first sin, the foremost of which is death, only Adam and Eve are guilty of that sin. Roman Catholicism teaches that everyone bears not only the consequence, but also the guilt, of that sin. 
 The Orthodox churches, including Greek Orthodox, Russian Orthodox, and Oriental Orthodox, differ little from Judaism in their holding of what sin is and how to reconcile it—except that animal sacrifices have never been part of Orthodox tradition.
 In 1517, Martin Luther launched the Protestant Reformation. In his famous "95 theses" he asserted the doctrine of Sola Scriptura—literally, "by Scripture only." Luther rejected out-of-hand most of the various categories of sin, and the elaborate rituals that attended its so-called reconciliation.
 Sin, according to Luther, is any failure to observe God's commandments or other Godly precepts that one may determine from the New Testament. Reconciliation from sin was a private matter between God and any of His followers. A priest was not required, because that would imply that a human being, of whatever office, could stand in the place of God. Christ is the only mediator and high priest between the sinner and God (Hebrews 4:14-18, Hebrews 8:1, Hebrews 9:15, 1 Timothy 2:5).
 Salvation was strictly by faith - believing God's word, and strictly by grace - accepting God's offer of salvation. Luther drew directly on the letters of Paul that addressed this very subject.
 The Baptists would generally agree with Luther. Like him, Baptists hold that sin is an act or omission that displeases God, and the state of being that the perpetrator falls into, and remains in until he has confessed this sin.
 Baptist churches do not tend to engage in corporate, collective confessions. Rather, Baptist practice depends heavily on Matthew 18:15-20 (KJV), which prescribes this procedure for the handling of sin that gives offense to a fellow believer:
 Not all Baptist churches practice this discipline, however. In such environments, sin is a matter between the offender and God (and the person or persons to whom the offender might have given offense). Some observers suggest that churches suffer when they fail to practice discipline as they should.
 Jehovah's Witnesses teach that Sin as the rejecting of God as the one qualified to decide what is 'Good and Evil', and thus a rejection of his right to rule. (Romans 1:18-32) 
 It is the condition in which all humans are born alienated from God as a result of disobedience, of his first command not to eat from the Tree of Knowledge of Good and Evil, and thus sentenced to death (Genesis 2:16-17). In this condition they fail to measure up to God's standards, and unable to reflect his qualities, or they “Fall Short” of God's Glory. (Romans 3:23) Created in God's image, but unable to reflect his qualities Humans eventually die.  (James 1:15) 
 (Romans 5:12) says that Sin is a condition that is passed on from parents to their children and is thus inherited, causing eventual death for the human who is "Born into Sin". See also- (Romans 6:23). 
 (Romans 5:12) Therefore, as through one man Adam Sin entered into the world, and death through Sin; and so death passed unto all men, for they had all sinned.
 This condition also has, according to (Romans 5:12), a one hundred percent infection rate, with the exception of Jesus Christ, who was born and was killed with no Sin (Hebrews 4:15) so as to pay back what was lost (Deuteronomy 19:21) with Adam, a Sinless Human life for a Sinless Human life. (1 Corinthians 15:22, 45)
 The connection of Death and Sin is first seen in the Bible at (Genesis 2:17) where God said that Sinning, or disobeying his commandment, would result in death for the otherwise immortal humans, Adam and Eve. Eve bore no children prior to becoming Sinful, so all of their offspring were born with Sin in their bodies and they would die. See also- (Romans 7:15-23).
 The "dying process" so-to-speak took much longer for those closer to the original state of being clean of Sin, with humans prior to the flood of Noah's day living as old as 900 plus years (Genesis 5:14). A good example of this can be seen in death of Adam at 930 years at (Genesis 5:5), or Noah's at 950 years at (Genesis 9:29). As the narrative in the Bible continues, the "dying process" of Sin shortens the length of life in humans until it is stated in (Psalms 90:10) that, "The length of our days is seventy years--or eighty, if we have the strength".
 The inherited aspect of Sin (Romans 5:12) is linked directly to the willful acts of Sinning (James 1:14-15). The Book of Genesis states that Humans born into sin before and after the flood had Hearts that were inclined towards "Evil" from their youth up (8:21&src=NIV Genesis 6:5; 8:21). Jesus confirmed this by saying that it is not what goes into a person's mouth that makes him sinful, but that which comes up out of their mouth from within their 'Heart'. See also- (Romans 7:15-23).
 (Jeremiah 17:9) says of the Human heart, "The heart is deceitful above all things, and it is exceedingly corrupt: who can know it?"
 The book of James gives a detailed description of this process of the act of Sinning and Death:
 (James 1:13) When under trial let no one say: “I am being tried by God." For with evil things God cannot be tried nor does he try anyone.
 (James 1:14) but each one is tried by being drawn out and enticed by his OWN desire,
 (James 1:15) Then the desire, when it has become fertile, gives birth to Sin; in turn, Sin, when it becomes accomplished brings forth death.
 The ultimate removal of this condition from the human race is said to occur when The Kingdom of God comes (Daniel 2:44) and rules alone, with no contemporary kingdoms. Then, it is said, that death will be removed. (1 Corinthians 15:26)
 The above article is a comprehensive theory of sin, and its effects on the offender, on mankind as a whole, and on the entire world.
 In Islam, sin (Arabic: haram) is very simple: anything that runs counter to the will of Allah (Arabic for God). In Islam, sin is simply an act, never a state of being.
 Islam recognizes five gradations of sin, in order of severity:
 In Islam, permanent reconciliation cannot come in life. Instead, at death, a person has his "good deeds" measured against his "bad deeds" (sins). If the good outweighs the bad, then the person must walk a tightrope over a lake of fire in order to cross into heaven. Islam, however, offers one important exception: any person who dies in the course of an act that advances the Muslim faith gains automatic entry into heaven, regardless of any prior sins he has committed. The usual, and some Muslims (particularly the Wahabbi sect) say the only, context for such a death is jihad, literally holy war. Furthermore, jihad is a physical war against non-adherents to Islam.
 Other Muslims will say that jihad is not physical at all, but refers instead to a personal struggle that every Muslim must engage in continually against sin and the temptations thereto.
 Still others state that two forms of Jihad are known:
 Those who make that distinction state that what they call the "lesser jihad" is practiced only by fanatical holders to Islam. A fanatic is one who holds to a set of beliefs uncritically—that is, without being willing to judge them. Since the World Trade Center incident, a number of Muslims have begun a critical analysis of Muslim doctrine (both verbal and written) and the place of Islam in the civilized world. Whether those voices can find sufficient logical support in the written texts that define Islam is debatable.
 Totalitarianism is a philosophy of government that holds that the government must have absolute authority over all aspects of life. Communist regimes have always been totalitarians in practice, just as the Nazis were.
 Totalitarian authorities and their apologists have their concept of sin as well: it is anything that works against the government, or tries to thwart its goals. This is true even in a regime that is explicitly atheistic in world view. Reconciliation generally requires recantation.
 Environmentalism also has a standard of value: the good of the planet as a wild system. Anything that harms the earth or makes it less wild is sinful in this system. The controversy surrounding global warming is, at root, a controversy about environmentalism as a source of moral values, and the allegation that humans in general, and/or some humans in particular, have transgressed against the environmentalist moral standard.
 Some religious and philosophical traditions consider sin to be a deliberate act; others say that it can be merely a mistake. Some say that reconciliation for sin requires a certain work; others that reconciliation is by the grace of God (or, for some, a cult leader), and still others (most notably the Objectivists) say that reconciliation is not required, except perhaps to any particular affronted person.
 Atheists typically lack a concept of sin, and may even ridicule it. More generally, amoral individuals deny that sin exists, because sin presupposes a moral code that someone might breach.
 1 Meaning of sin
1.1 Required: a moral context
 1.1 Required: a moral context 2 Differing views of sin
2.1 Jewish
2.2 Christian views
2.2.1 Roman Catholic
2.2.1.1 Virgin Mary
2.2.2 Anglican Communion
2.2.3 Eastern and Oriental Orthodox
2.2.4 The Reformation
2.2.5 The Baptist Tradition
2.2.6 Jehovah's Witnesses
2.2.7 Fundamentalists
2.3 Islam
2.4 Totalitarianism
2.5 Environmentalism
 2.1 Jewish 2.2 Christian views
2.2.1 Roman Catholic
2.2.1.1 Virgin Mary
2.2.2 Anglican Communion
2.2.3 Eastern and Oriental Orthodox
2.2.4 The Reformation
2.2.5 The Baptist Tradition
2.2.6 Jehovah's Witnesses
2.2.7 Fundamentalists
 2.2.1 Roman Catholic
2.2.1.1 Virgin Mary
 2.2.1.1 Virgin Mary 2.2.2 Anglican Communion 2.2.3 Eastern and Oriental Orthodox 2.2.4 The Reformation 2.2.5 The Baptist Tradition 2.2.6 Jehovah's Witnesses 2.2.7 Fundamentalists 2.3 Islam 2.4 Totalitarianism 2.5 Environmentalism 3 Summary 4 See also 5 Further reading 6 References  The aggrieved person, or the minister if the aggrieved person has asked him to, tries to encourage the perpetrator to repent. Repentance (Greek metanoia) means a change of mind, or a change of heart.  Failing that, whoever tried to talk to the perpetrator the first time, tries a second time and brings a witness.  If this still does not bring repentance, and a cessation of the behavior, then and only then does the matter come before the entire church. The minister lays out the case, and puts the matter to a vote as to whether the offender ought to be allowed to remain in fellowship.  Mistakes.  Immorality.  Transgressions.  Wickedness and depravity.  Ascribing a partner to God. Thus this one special instance of blasphemy is held worse than any other sin a man can commit.  The greater Jihad, which is the internal struggle within a person against sin.  The lesser Jihad, physical war against the infidel.  Indifference Atheism and sin  Atheist population and immorality   Debate: Do genuinely saved Christians sin?  Repentance - Confession - Penance  Revelation, Book of (historical exegesis)   Bretzke, James T. A Morally Complex World: Engaging Contemporary Moral Theology  (2004) excerpt and text search  Connolly, Hugh. Sin (2002) 168pp very brief history of sin in Christian theology; 120pp  Paulson, Ronald. Sin and Evil: Moral Values in Literature(2007) 403pp.; focus on Swift, Dickens, Hawthorne, Melville, James, Conrad, Faulkner, Greene, and Vonnegut ↑ 
John Stott (2003). Evangelical Truth: A Personal Plea For Unity, Integrity And Faithfulness, 3rd, Inter-Varsity Press, 87. ISBN 978-0851-119885. 
 ↑ New International Version (NIV). 1 John 3:4. ®, NIV® Copyright © 1973, 1978, 1984, 2011 by Biblica, Inc.®. “Everyone who sins breaks the law; in fact, sin is lawlessness.”
 ↑ 
Mahatma Gandhi (1968). in Shriman Narayan: The Selected Works of Mahatma Gandhi: The voice of truth. Navajivan Publishing House, 172. 
 ↑ What is a Mortal Sin?
 ↑ The Four Marian Dogmas (catholicnewsagency.com)
 ↑ Catechism of the Catholic Church, numbers 88-90, 487-493, 817-820, 981-982, 1463, 2089. See also Vatican II Lumen Gentium Dogmatic Constitution on the Church, numbers 52-67, and Code of Canon Law 1983, Libreria Editrice Vaticana, Vatican City, Canon Law Society of America, Washington, D.C., canons 1311-1330.
 Religion Sin Christian Theology Liberal Traits Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 27 April 2019, at 01:45. This page has been accessed 62,489 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Bible Sections About Concepts See also Sin ἁμαρτία For the mathematical function, see sine. See also: Seven Deadly Sins John 19:11 "He who delivered me to you has the greater sin."  1 John 5:10-17 "...All wrongdoing is sin, but there is sin which is not mortal."  Main Article: Sin (Fundamentalism) For the mathematical function, see sine. See also: Seven Deadly Sins John 19:11 "He who delivered me to you has the greater sin." 
1 John 5:10-17 "...All wrongdoing is sin, but there is sin which is not mortal."  Main Article: Sin (Fundamentalism) 
 Bible
 
Sections
Old Testament
New Testament
Pentateuch
The Gospels
About
History
Canon
Exegesis
Accuracy
Criticism
Inerrancy
Literalism
Chronology
Translations
Hermeneutics
Concepts
Sabbath
Sin
Resurrection
Prophet
The Virgin Birth
Tithe
See also
Christianity
 "‘Hate the sin and not the sinner’ is a precept which, though easy enough to understand, is rarely practized, and this is why the poison of hatred spreads in the world."
— Mahatma Gandhi [3]
 Sin Contents Meaning of sin Differing views of sin Summary See also Further reading References Navigation menu Required: a moral context Jewish Christian views Islam Totalitarianism Environmentalism Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console Roman Catholic Anglican Communion Eastern and Oriental Orthodox The Reformation The Baptist Tradition Jehovah's Witnesses Fundamentalists Virgin Mary 
