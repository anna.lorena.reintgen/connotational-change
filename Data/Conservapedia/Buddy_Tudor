(Louisiana real estate developer]]​
 Resting place:
Greenwood Memorial Park in Pineville
 (2) Patricia Murphy (divorced)​
(3) Patsy Graham Tudor (widowed)​
Children:
All from first marriage:​
Anne Elizabeth "Lisa" Tudor ​
Robert B. Tudor, III​
John Michael Tudor​
William A. Tudor​
Geoffrey Trever Tudor​
Three step-children:​
Michelle H. Jennings​
Justin Michael Hathorn​
Holly Hathorn
 Robert Beall Tudor, Jr. (May 18, 1935 –March 14, 2010), known as Buddy Tudor, was an American real estate developer. Based in Pineville, Louisiana, his family-owned Tudor Construction Company and Tudor Enterprises together surpassed a billion dollars in development. An active member of the Baptist Church, Tudor was also a contributor to the Louisiana Republican Party and a personal friend of David C. Treen, Louisiana's first Republican U.S.  Representative and then governor since Reconstruction.​
 In 1946, Tudor's grandfather, Simon Woodson Tudor (1887–1956), a Kentucky native, founded Tudor Construction Company in Pineville. Tudor Hall, a men's dormitory, at Baptist-affiliated Louisiana College in Pineville is named for him. After a decade in business, Simon Tudor died, and the company passed to son Robert Tudor, Sr. (1914–1987). Simon Tudor was married to the former Frances Ollie Beall (1894–1992).[1] After the death of Robert Tudor, Sr., Lorraine Price Tudor (1915–2009), Buddy Tudor's mother, married Leonard F. Sanderson, Sr. (1914–2005), a widower and a retired official of the Louisiana Baptist Convention, based in Alexandria.[2]​
 Tudor, Jr., graduated in 1953 from Bolton High School in Alexandria prior to the opening of Pineville High School. At Bolton, he played basketball and tennis and was the state runner-up in men's singles tennis in both his junior and senior years. As a civil engineering student at Louisiana State University in Baton Rouge from 1953 to 1957, he lettered in varsity tennis all four years and was the team captain in 1956 and 1957. He was a member of Sigma Tau Alpha and Tau Beta Pi national engineering fraternities, Omicron Delta Kappa leadership and scholastic fraternity, and Sigma Chi social fraternity. He was named the outstanding engineering graduate of the Class of 1957. Tudor completed Reserve Officers Training Corps and was named LSU's distinguished military graduate. He was commissioned as a second lieutenant in the anti-aircraft artillery of the United States Army, with active duty at Fort Bliss near El Paso, Texas, and Fort Chaffee near Fort Smith, Arkansas.[3]​
 After military service, Tudor returned to Alexandria-Pineville to join his father as the third generation at Tudor Construction Company and became managing partner in 1961. The company was one of the few Louisiana firms of its kind to operate statewide and then to assume projects out-of-state and across the American South. The firm established offices in Baton Rouge, Lafayette, and Hammond, Louisiana, and in Denver, Colorado, and Washington, D.C. The company is now owned and operated by John Michael Tudor (born September 22, 1961), a great-grandson of Simon Tudor and the representative of the fourth generation of Tudors in charge of the business.[3]​
 Among the structures developed or constructed by Tudor Enterprises are Alexandria Mall, the Rapides Parish Coliseum in Alexandria, and the Central Louisiana Electric Company headquarters in Pineville. He also built or expanded churches: Calvary Baptist and Emmanuel Baptist, and First United Methodist Church, all in Alexandria, and Pineville's First Baptist Church. Other projects are the Rapides Regional Women's and Children's Hospital, Tudor Hall and Guinn Auditorium at Southern Baptist-affiliated Louisiana College, Grande Theatre, and the One Centre Court and Two Centre Court office complexes on Jackson Street in Alexandria. Tudor owned or was the principal developer of downtown Alexandria properties the Bentley Hotel (since sold), Jackson Place, the Commercial Building, and the Diamond Grill restaurant.[4] Tudor constructed the tennis courts and the diving pool at the Alexandria Aquatic and Racquet Club. He also assisted in the building of the athletics complex, including tennis courts, at Pineville High School. He often played tennis even with later his health problems.[4]​
 During the Treen administration, Tudor lost the contract for the building of the Cajundome complex in Lafayette to Blount Brothers of Montgomery, Alabama. Winton M. Blount, the company founder, was also a Republican, having been U.S. Postmaster General in the Nixon administration and his party's unsuccessful U.S. Senate nominee in 1972 against John Sparkman. Ron Gomez, then a member of the Louisiana House of Representatives from Lafayette, recalled the machinations over the approval of the sports complex, home of the University of Louisiana at Lafayette Ragin' Cajuns team. Treen had expected Tudor to win the Cajundome contract, but he was ostensibly outbid by Blount. However, considering five alternates and other changes in the contract in the end could have made Tudor the lower bidder. The project ultimately was completed in 1985 at a cost of $60 million.[5]​
 Tudor had contacted Treen's aide, John Hamilton Cade, Jr. of Alexandria and commissioner of administration Edgerton L. "Bubba" Henry to ask them to intervene on his behalf. In a letter to Treen, Tudor said that the Cajundome plans "as drawn, reflect the needs of the Lafayette area, and it would certainly be a shame to build something less than that needed which might be outdated before completion. I realize than my arguments may be taken as being strictly self-serving, but I honestly must say that whether we are the successful contractor or not would in no way alter these facts. I know that this is a difficult decision for you."[6]​
 Treen's long-time Louisiana rival, Edwin Edwards, a Democrat, appearing at the Central Louisiana Press Club gridiron show, once poked fun at Tudor by saying how happy he was to be in "Tudorville", his epithet for the Alexandria-Pineville community.[4]​ Though a Republican, Tudor donated $1,000 in 2008 to the reelection of Democratic U.S. Senator Mary Landrieu of Louisiana,[7]​ who that year turned back the Republican challenger, John Neely Kennedy. Landrieu was unseated in 2014 by the Republican Bill Cassidy of Baton Rouge, and John Kennedy won the other Senate seat vacated by David Vitter in 2016.
 Tudor was the founding member of Central Cities Development Corporation, which focuses on the revitalization of downtown Alexandria. He was the first chairman of the Rapides Area Planning Commission (1970–1974). He served as president of the Alexandria Rotary  (1969–1970), and on the boards of numerous civic organizations, such as the Alexandria-Pineville Chamber of Commerce, the LSU Foundation, the Boy Scouts, and the Young Men's Christian Association. He was active in the "good government interest groups, the Council for a Better Louisiana and the Louisiana Association of Business and Industry. He was also affiliated with the industry trade group, Associated General Contractors, and the non-profit organization, the Urban Land Institute.[3]​
 In 1970, he was named Outstanding Young Man of the Year in Central Louisiana and one of three such honorees statewide. He received awards from the Louisiana Preservation Alliance and the National Trust for Historic Preservation for his work on the Bentley Hotel and the Diamond Grill.[4] In 2007, he was awarded the Trustee's Distinguished Service Award by Louisiana College, at which his grandfather had been a faculty member and coach prior to entering the construction business.[3]​
 Tudor was married three times. He was divorced from the first wife, now Jane Anne Welch (born 1936) of Santa Fe, New Mexico. He was divorced from his second wife, Patricia Murphy of Baton Rouge. His third and surviving wife is the former Patsy Graham (born November 21, 1959) of Pineville. He had five children from the first marriage, Anne Elizabeth "Lisa" Tudor (born c. 1957), formerly Anne T. Stoller, of New Orleans, Robert B. Tudor, III (born c. 1959), and wife, the former Phoebe Brian, of Houston, Texas; John M. Tudor; William A. Tudor (born 1964), and Geoffrey Trever Tudor (born ca. 1966) of Austin, Texas. He is also survived by three stepchildren, Michelle H. Jennings and husband Nathan of Fort Hood near Killeen, Texas, Justin Michael Hathorn and wife Ali of Monroe, and Holly Hathorn and her fiancé, Brandon Malone, of Pineville. Other survivors are a sister, Sue Tudor Miller (born ca. 1933), and a brother, attorney Michael Simon Tudor of Pineville.
 Tudor died at the age of seventy-four at his Pineville home after a 17-year struggle with cancer. Services were held on March 17 at the First Baptist Church of Pineville, where he had been a deacon and taught Sunday school, with pastor, Dr. Stewart Holloway, officiating. Tudor is interred at Greenwood Memorial Park in Pineville.​
 
​​​​​​​
 1 Background 2 Expanding the company 3 Losing Cajundome contract 4 Civic leadership 5 Family and death 6 References ↑ Simon Woodson Tudor,  A Dictionary of Louisiana Biography, Vol. 2 (1988), p. 799.
 ↑ Social Security Death Index. ssdi.rootsweb.ancestry.com. Retrieved on March 28, 2010.
 ↑ 3.0 3.1 3.2 3.3 Robert Beall Tudor, Jr.. The Alexandria  Town Talk (March 16, 2010). Retrieved on March 28, 2010.
 ↑ 4.0 4.1 4.2 4.3 Bob Tompkins, "Prolific real estate developer Robert "Buddy" Tudor Jr., dies",The Alexandria  Town Talk, March 15, 2010.
 ↑ Ron Gomez, My Name Is Ron, And I'm a Recovering Legislator: Memoirs of a Louisiana State Representative, (Lafayette, Louisiana: Zemog Publishers, 2000), pp. 104–105.
 ↑ Letter from Buddy Tudor to David C. Treen, quoted in Ron Gomez, Recovering Legislator, p. 106.
 ↑ Buddy Tudor: Political Campaign Contributions, 2008 Election Cycle. Campaignmoney.com. Retrieved on March 29, 2010.
 Louisiana People Business People Engineers Baptists Republicans United States Army Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 10 October 2019, at 05:43. This page has been accessed 619 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Robert Beall "Buddy" Tudor, Jr. Political party  Born Died Spouse Religion Robert Beall Tudor, Jr. Buddy Tudor 
  Political party 
  Republican​
 
  Born
  May 18, 1935​ Alexandria,  Louisiana, USA​
 Died
  March 14, 2010 (aged 74) Pineville, Rapides Parish, Louisiana
Resting place:
Greenwood Memorial Park in Pineville
 Spouse
  (1) Jane Ann Welch (divorced)​
(2) Patricia Murphy (divorced)​
(3) Patsy Graham Tudor (widowed)​
Children:
All from first marriage:​
Anne Elizabeth "Lisa" Tudor ​
Robert B. Tudor, III​
John Michael Tudor​
William A. Tudor​
Geoffrey Trever Tudor​
Three step-children:​
Michelle H. Jennings​
Justin Michael Hathorn​
Holly Hathorn
 Religion
  Southern Baptist​
 Buddy Tudor Contents Background Expanding the company Losing Cajundome contract Civic leadership Family and death References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
