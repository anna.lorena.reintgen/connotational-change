The constructed languages and scripts created by J. R. R. Tolkien, and those used in his fictional Middle-earth legendarium.
 J. R. R. Tolkien was a philologist and had loved language since he had been a child. He invented several languages, which he later used as languages for the inhabitants of his Middle-earth world. Indeed, the languages came first, and he often stated that he created Middle-earth as a world in which his languages could be spoken.[1]
 While some of Tolkien's languages have a detailed grammar and vocabulary, some are a lot less detailed, and many consist of only a few  names and words. Other languages appear in his works through a real-world 'stand-in' language: Rohirric is represented by Old English, and the names of the Dwarves and the Men of northeastern Middle-earth are Norse. Quenya and Sindarin are the most developed and probably also the most well known of Tolkien's languages. 
 In addition to the external history his languages went though in the process of their creation, Tolkien also invented an "internal history" for his languages, how they evolved inside the world over the time they were spoken by the fictional people. 
 Tolkien also invented several scripts, used for his languages and in his fictional world, among them the scripts Tengwar and Cirth.
 The Eldarin languages or Elvish languages are the languages of the Elves. In earlier times, the Elvish languages were the common speech. Later the Elvish languages were also used by humans, especially by scientists and the nobility. The Eldarin languages can be divided into two groups: the Quenya language and the Telerin languages, which include Telerin, Nandorin, and Sindarin.
 Quenya is an Elvish language. It was spoken by the Vanyar and Noldor in Valinor. When the Noldor returned to Middle-earth in the First Age, they brought the Quenya language with them. In the Second Age and especially the Third Age, Quenya was used in Middle-earth more as a scientific and ritual language, which prompted Tolkien to nickname Quenya the "Elf-Latin". Quenya was in its design influenced by Latin and Finnish.[2]
 Telerin is an Elvish language. It was spoken by the Teleri. From Telerin developed several new languages and dialects, for example Sindarin.
 Sindarin is an Elvish language descended from the Telerin language. It was spoken by the Sindar (or Grey Elves) in Middle-earth. After the decline of Quenya it became the most common Elvish language spoken in Middle-earth. It was designed to sound similar to Welsh and Celtic languages.[3]
 Adûnaic is the language of the people of Númenor. After the Fall of Númenor, Adûnaic fell out of use. From Adûnaic and the native languages of the western coastlands of Middle-earth developed Westron, the Common Speech.
 Westron (Westron: Adûni, Sindarin: Annúnaid) or the Common Speech (Westron: Sôval Phârë) is a language of Middle-earth. It evolved out of the Adûnaic of Númenor and the native Mannish languages of Middle-earth. In the Third Age, Westron was used as the common language, as it was spoken by most peoples. In the books set in those later times, the English names and text are meant to be Westron, most of the time.
 Rohirric (also Rohirian or Rohanese) is the language of the Rohirrim of Rohan. Some actual Rohirric names and words exist, but they never appear in the narrative texts. In the narrative, Rohirric is represented by Old English, because Rohirric should feel older but vaguely related to the Hobbit's Westron, which in the books is represented by Modern English. For example, the Rohirrim are called Éothéod in Old English, but Lohtûr in "true" Rohirric.
 Khuzdûl is the language of the Dwarves. The Dwarves are a very secretive people, and as such not much is known of their language to outsiders, aside from a few Khuzdûl place-names and phrases. The Dwarves also have always kept their Khuzdûl names a secret from non-dwarven people, instead using other "nicknames" for talking to non-dwarves or in writing. The Dwarven "nicknames" as they appear in the books are taken from Norse mythology. The Khuzdûl spoken language was called aglab; in addition Khuzdûl also had a form of sign language, the iglishmek. The iglishmek was very subtle, in such a way that Dwarves could hold a conversation without anybody else understanding or even noticing.
 Entish is the language of the Ents. It is, like the Ents themselves, a very "unhasty" language. The language is very descriptive and repetitive, because of which it takes a lot of time to say anything. The amount of time needed to say even very simple phrases, and the complicated structure, makes the Entish language unsuitable for anybody but the Ents themselves.
 Valarin was the language spoken by the Ainur.
 Black Speech is the language created by Sauron and used by him and his followers. The most well known example of the language is the inscription on the One Ring, which is in Black Speech written in Tengwar letters. Aside from the Ring-inscription there are a few names and words of Black Speech known.
 The Cirth are a script invented by the Sindar elves of Doriath in the First Age. The original Certhas Daeron was created by the Elf Daeron, the minstrel of King Thingol. Later the Noldor added some letters to create the Angerthas Daeron. This form of the script was adopted and changed by the Dwarves to write their language Khuzdûl, creating the Angerthas Moria or Angerthas Erebor.
 Sarati is a script invented by the Elf Rúmil of Tirion in Valinor. Sarati could be written in several directions, most often from top to bottom; it could also be written left to right, right to left, and alternating. Fëanor later based the Tengwar script on the Sarati.
 Tengwar is a script invented by Fëanor, who based it on the Sarati. The Tengwar became widely used by all the peoples of Middle-earth. There are a variety of modes to write Tengwar, depending on the language written, and the place and time it was used.
 1 External history 2 Languages of Middle-earth
2.1 Eldarin languages
2.1.1 Quenya
2.1.2 Telerin
2.1.3 Sindarin
2.2 Mannish languages
2.2.1 Adûnaic
2.2.2 Westron
2.2.3 Rohirric
2.3 Khuzdûl
2.4 Entish
2.5 Valarin
2.6 Black Speech
 2.1 Eldarin languages
2.1.1 Quenya
2.1.2 Telerin
2.1.3 Sindarin
 2.1.1 Quenya 2.1.2 Telerin 2.1.3 Sindarin 2.2 Mannish languages
2.2.1 Adûnaic
2.2.2 Westron
2.2.3 Rohirric
 2.2.1 Adûnaic 2.2.2 Westron 2.2.3 Rohirric 2.3 Khuzdûl 2.4 Entish 2.5 Valarin 2.6 Black Speech 3 Scripts
3.1 Cirth
3.2 Sarati
3.3 Tengwar
 3.1 Cirth 3.2 Sarati 3.3 Tengwar 4 References 5 External links ↑ "Nobody believes me when I say that my long book is an attempt to create a world in which a form of language agreeable to my personal aesthetic taste might seem real. But it is true." The Letters of J. R. R. Tolkien, #264
 Ardalambion, about Tolkien's languages (includes a Quenya course) Middle-earth Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 8 December 2008, at 15:09. This page has been accessed 27,496 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Eldarin languages Elvish languages Quenya Telerin Sindarin Adûnaic Westron Common Speech Rohirric Khuzdûl aglab iglishmek Entish Valarin Black Speech Cirth Certhas Daeron Angerthas Daeron Angerthas Moria Angerthas Erebor Sarati Tengwar  J. R. R. Tolkien's Middle-earthBooksThe Hobbit • The Lord of the Rings • (Posthumous:) The SilmarillionPeoplesAinur • Elves • Hobbits • Dwarves • Ents • OrcsGeographyMiddle-earth locations • Númenor • AmanOtherObjects • Languages • Scripts • Chronology • List of topics  Books The Hobbit • The Lord of the Rings • (Posthumous:) The Silmarillion   Peoples Ainur • Elves • Hobbits • Dwarves • Ents • Orcs  Geography Middle-earth locations • Númenor • Aman  Other Objects • Languages • Scripts • Chronology • List of topics Tolkien's languages and scripts Contents External history Languages of Middle-earth Scripts References External links Navigation menu Eldarin languages Mannish languages Khuzdûl Entish Valarin Black Speech Cirth Sarati Tengwar Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console Quenya Telerin Sindarin Adûnaic Westron Rohirric 
