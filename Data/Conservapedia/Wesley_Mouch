Wesley Mouch, in Ayn Rand's novel Atlas Shrugged, was the Washington lobbyist employed by Henry Rearden to look after his interests, and afterwards the Senior Coordinator of the Bureau of Economic Planning and Natural Resources. He built a career on political favoritism, a career that came to nothing when the United States degenerated into anarchy in the last year of the strike of the men of the mind.
 Wesley Mouch came from a family that had always sent its sons to college—and college in those days taught its students to despise men of business.
 Wesley managed the money of his rich uncle, Julius Mouch, and mostly let it run through his fingers while he completed his studies. After graduating, Wesley took a job in the ad department of a company that made a quack cure-all from corn syrup. That sort of thing he could do well. He went on to take similar advertising jobs—selling hair restorers, a patented article of intimate clothing, a new brand of soap, and then a "soft drink." Then he became vice-president in charge of advertising for an automobile company.
 That proved disastrous. Automobiles are not corn-syrup cure-alls, but Wesley Mouch didn't know the difference. The cars didn't sell—but Wesley Mouch could talk himself out of anything.
 Then the head of the auto company recommended him to Henry Rearden. Rearden asked him to look out for him in Washington, DC and warn him when any bill or executive order was ready to pass that could affect him. That would prove disastrous—for Henry Rearden.
 On September 3, Mouch was one of four men who met to discuss the state of the economy of the United States—or rather, what advantage they might gain by colluding in political rule-making. The other three were James Taggart, Orren Boyle, and Paul Larkin, who had ingratiated himself with Henry Rearden's family. Mouch carefully confined himself to laconic remarks of agreement with whatever any of the other three were saying. This was the meeting in which James Taggart and Orren Boyle each agreed to support a rule-making project that would benefit the other over his competitors: James Taggart wanted the National Alliance of Railroads to promulgate an "Anti-dog-eat-dog Rule" stating that no railroad shall offer "cutthroat competition" to any railroad in a territory where the other railroad had prior operations, and Orren Boyle wanted an "Equalization of Opportunity Act" to force Henry Rearden to divest himself of certain iron ore and coal mines so that he would be in the same predicament, as regards obtaining raw materials, as was Boyle, whose Associated Steel Company did not own any such mines. Paul Lutkin agreed to step in to buy Rearden's iron-ore mines but make sure that Orren Boyle would get all his shipments.
 As this meeting concluded, James Taggart observed to Mouch, 
 Later, Taggart made clear that he would see that Mouch received a high-ranking Washington appointment if he would keep quiet about the legislative and other measures to come. So Mouch said nothing to Rearden until the Equalization of Opportunity Act passed, and then made himself unreachable. Finally he left Rearden a message containing his resignation. Not long after that, he received the appointment he sought: as assistant coordinator of the Bureau of Economic Planning and Natural Resources.
 After the John Galt Line opened in Colorado (July 22), many businessmen started to relocate to that State, because at the time it had a very limited government. By now, Wesley Mouch had become the Senior Coordinator at the Economic Planning Bureau. In that capacity he heard the complaints, and the pleas for favors, from various unions and trade associations, and from his new friend Orren Boyle, who resented the competition that Henry Rearden posed to him, with his new Metal, essentially a new copper and iron alloy. Ever willing to grant such favors as lay within his power, Mouch, in November, promulgated a series of directives, using a power recently granted his Bureau by the United States Congress, to impose a strict speed limit on trains, to require all trains to have crews of minimum size, to limit sharply the output of Rearden Metal, and to require that Henry Rearden give a "fair share" of his Metal to anyone who asked. The Bureau appointed a "Deputy Director of Distribution" to monitor Henry Rearden's compliance with these last two positions from an on-site office.
 The immediate consequence of these directives was the disappearance of Ellis Wyatt, of the Wyatt Oil Company, and the incendiary destruction of the shale oil fields that Wyatt had established. These oil well fires, collectively known as Wyatt's Torch, continued to burn for years afterward. The more far-reaching consequence was that the economy of Colorado collapsed, a result about which Mouch seems not to have cared.
 In April, Mr. Thompson, the Head of State, personally authorized Mouch to promulgate his latest rule-making scheme: Directive 10-289. Mouch won this authority mainly by complaining to Fred Kinnan (head of Amalgamated Labor of America), James Taggart, Orren Boyle, and Clem Weatherby (who now functioned as his assistant) that he couldn't meet their needs with the authority he had. Directive 10-289 was an effort to freeze all economic inputs and outputs at present levels, to stop the economy from deteriorating any further. He was forced to make two major concessions:
 Directive 10-289 went into effect on May 1. Whether Wesley Mouch ever noticed that the distribution compliance reports from his Deputy Director of Distribution at Rearden Steel suddenly became less than reliable, the novel never makes clear.
 Dagny Taggart, the sister of James Taggart who served Taggart Transcontinental as its Vice-President of Operations, resigned immediately, or so Mouch gathered. (Jim Taggart never confirmed the resignation officially.) Then on May 28, a major wreck involving the Taggart Comet (the TTRR's flagship coast-to-coast passenger limited train) and an Army ammunition train collapsed the Taggart Tunnel at a cost of the lives of three hundred passengers (including Kip Chalmers, who had intended to run for the United States Senate from California), plus nearly every member of the crews of both trains. The day after that disaster, Dagny Taggart called Wesley Mouch at his office and demanded to speak directly to Clem Weatherby. Mouch, not knowing how to judge that request, passed her through. Afterward, Weatherby informed him that Dagny Taggart had now stated her official policy to deal exclusively with Weatherby in all official matters touching the TTRR. Whether Mouch asked Weatherby why Dagny Taggart had taken that position, the novel does not specifically say.
 That evening, Dagny Taggart traveled westward on the Comet, which stopped in mid-route as a "frozen train." The reports that Mouch later received indicated that Miss Taggart rented a monoplane and took off for a Utah destination. She reached it and then took off again, and whether she filed a flight plan or not, Mouch never heard. All he could gather was that Miss Taggart was last seen flying into the Rocky Mountains, and that her aircraft was never seen again.
 In June, Mouch and Fred Kinnan allowed the Unification Board (the Board that the Directive had established to govern all economic matters, including even job placement for new citizens as they came of voting age) to allow the establishment of a Railroad Unification Plan, under which all the nation's railroads would operate as a team, and receive payment according to the track they owned. The Board appointed Cuffy Meigs as Director of Railroad Unification.
 At the end of June, Dagny Taggart suddenly "returned from the dead." Perhaps Meigs assured Wesley that he could "handle things" at the TTRR, whether Dagny Taggart was present or not. (The other railroads were of no consequence. The president of the Atlantic Southern Railroad, the only railroad that could have offered any serious resistance, committed suicide shortly after the Plan became effective.)
 In October, Mouch and his various Washington friends and associates prepared to promulgate a Steel Unification Plan, along the lines of the Railroad Unification Plan. Mouch knew from previous experience that Henry Rearden, who stood to lose the most from this Plan, would resist. Therefore, Mouch and his friends prepared an elaborate plan to make Rearden believe that his mills were about to erupt in labor violence.
 On October 20, the new union representing Rearden Steel's workforce petitioned the Unification Board for a raise in salary. On October 23, the Board, on orders from Fred Kinnan, refused. Then came the editorials saying that Rearden's workers were downtrodden, and that violence could break out at any moment.
 Which Washington official arranged with the Internal Revenue Service to place liens on Rearden's personal and company assets to satisfy a tax obligation that actually did not exist, the novel never names. The lien went into effect on October 31. On November 2, Tinky Holloway, at Mouch's orders, invited Rearden to meet with Mouch, Taggart, Holloway, Floyd Ferris, and Eugene Lawson at the Wayne-Falkland Hotel in New York City (in the very suite once rented by Francisco d'Anconia) on the pretext of helping "straighten out" the tax liens.
 But another part of the plan was to start a workers' uprising at Rearden Steel. To that end, someone, with the full blessing of the Unification Board, arranged to have several men apply for jobs at Rearden Steel who were not experienced steelworkers and whose skills were better suited to rough-housing than to steelmaking. On the appointed day of the meeting with Rearden (November 4), a Unification Board official asked the Deputy Director of Distribution to sign several passes to admit a last-minute contingent of workers (actually labor agitators) to the plant. Much to everyone's surprise, the DDD refused to sign the passes; Mouch no doubt later learned that the DDD was shot for his trouble and ended up dead.
 Henry Rearden did attend the meeting. For about fifteen minutes he disputed Mouch and the others as to the wisdom and practicality of their economic policies. The assembled men described the Steel Unification Plan to Rearden, and Rearden told them flatly that their numbers didn't add up. Then he asked, in a manner that was not quite sarcastic, what his hosts thought could save them.
 That was when Jim Taggart made a major blunder. He said, "Oh, you'll do something." The minute Taggart said that, Mouch knew that that had been the wrong place, the wrong time, and the wrong mode of expression. The look on Henry Rearden's face said it all, and when Rearden abruptly turned to leave, Mouch knew that all the efforts to stop him from returning to his mills at that moment would be in vain.
 The end result was almost an anticlimax. Henry Rearden vanished from society, leaving behind his mills, his home, his New York apartment, his bank accounts, and indeed everything listed in his name. Furthermore, word of his vanishing reached the mills, and before Mouch or anyone else could stop it, many more workers quit and vanished, including the mills superintendent, the chief metallurgist, the chief engineer, the furnace foreman, Mr. Rearden's personal secretary, and even the doctor at the mill infirmary.
 Mouch would send two men to run Rearden Steel for the government, and both appointments proved disastrous. The first man, one of Orren Boyle's friends, was obviously in over his head and begged for a transfer within a month. The second man, one of Cuffy Meigs' cronies, literally sold off all the equipment and pocketed the proceeds. After that, conditions continued to deteriorate, until finally, on January 22, a sixty-year-old worker whose name went unrecorded started a fire that made the mills inoperable. Upon his arrest, this worker said that he had acted "to avenge Hank Rearden."
 Wesley Mouch, now thoroughly out of his depth, could only rely on the consummate deal-making skills of his ultimate boss, Mr. Thompson. Mr. Thompson prepared to address the nation on November 22. But fifteen minutes before he was to speak, all the radio and television stations were jammed. Then at the appointed hour, a voice spoke through the jamming: a voice identifying itself as that of John Galt, a name that Mouch had known only as part of a slang phrase. The man calling himself John Galt took credit for the economic collapse, and for three hours essentially said that he and all the other "men of the mind" were on strike, and then called for a general strike of anyone else whom he personally had not yet been able to reach.
 That John Galt was who he said he was, neither Mouch nor Mr. Thompson nor anyone else in Legislature or administration could doubt—not after Galt specifically named several of the vanished men in his speech. (Galt also named Robert Stadler, the Director of the State Science Institute, but Mouch never figured out that reference, as unflattering as it apparently was.)
 Three months later, John Galt was found—in New York! Mr. Thompson had him kept under guard in the penthouse suite of the Wayne-Falkland Hotel in New York. Mouch learned later that Thompson intended to fire him and hire John Galt in his place, as Economic Dictator of the country. Mouch soon learned that Galt had thrown the offer in Mr. Thompson's face.
 Mouch continued to trust Mr. Thompson, who then arranged a television special to announced something called "The John Galt Plan for Peace and Prosperity." Mouch knew full well that no such plan was in the offing, but he went along with the program anyway, hoping that Galt would not refuse such power when offered him on national television.
 But Galt did refuse. When his turn came to speak, he moved so fast that the camera caught the hidden gun trained on him. Then he said, 
 Mouch now found himself in a room full of moaning and sobbing men. Morale Conditioner Chick Morrison threw his hands up and stalked out. Apparently he had his own hideout in Tennessee, but Mouch wouldn't lay any odds on his being able to reach it, and neither would any other man present.
 Now Floyd Ferris, the man whom Mouch always knew was the real power at the State Science Institute (Robert Stadler had fled New York three days before), challenged Mouch directly on what he proposed to do. Mouch could only reply, in a halting tone, that Galt had to "take over and save the system." Ferris sarcastically asked whether Mouch intended to "write him a love letter about it," whereupon Mouch found himself saying that the assembled men had to force John Galt to take over the system. The very absurdity of Mouch's words ("force him to rule") escaped him now.
 Ferris then relentlessly pressed his own solution: to put John Galt to torture by electric shock. Jim Taggart practically jumped at the chance to participate. Wesley Mouch went along—because by then, Mr. Thompson had thrown up his own hands, said that he could not help it, and bade the others "do whatever you want."
 At the secret installation of what was known as Project F, Mouch, Taggart, and Ferris watched as John Galt, stripped and lashed to a bed, endured one jolt after another that made him writhe. Galt took this treatment without an outcry as Mouch and the others continued to shout at Galt that they wanted him to rule and to give orders. Finally, Mouch couldn't take it anymore. He cried out to Floyd Ferris that they did not dare kill him, because if he died, they all would die. Ferris insisted that the treatments continue. Mouch might or might not have noticed Jim Taggart sitting quietly and taking in the sight with actual enjoyment.
 Then the electroshock generator stopped working. Floyd shouted at the technician in charge to repair the generator or face imprisonment. Then Galt stunned them all by telling them not only what was wrong with the generator but how to repair it!
 The technician backed away from Galt and the three interrogators and then bolted. Galt, still lashed to the bed, roared with laughter. Then Jim Taggart rushed forward and tried to repair the generator himself. Wesley tried to stop him, but Jim said, "I want to make him scream." And then Jim let out a scream, and muttered, "No...no...no..."
 Galt looked at Jim and said, "Yes. I told you that on the radio, didn't I?"
 Whereupon Jim collapsed completely. Floyd then said something about getting Jim to a doctor. Wesley and Floyd helped Jim to his feet and half carried him out of the basement torture chamber, never to return.
 Whether Mouch, Ferris, and Taggart even reached a hospital, the novel does not say. Mouch would doubtless learn later that the government had collapsed totally, after Project X had blown up (had Robert Stadler had anything to do with that?) and cut the Taggart Bridge in half. The immediate result was the closure of the Lebanon (New Hampshire) Airport. Mouch and Ferris might have driven down the road from the State Science Institute to Lebanon and the Dartmouth-Hitchcock Medical Center, affiliated with Dartmouth College. But whether that hospital was even functional in this version of alternate history is not clear.
 One may infer that various citizens' militias strove with the various criminal gangs that were running rampant throughout the land and asserted themselves as the new force for law and order. Either Mouch and the others fell into the hands of one of those gangs (in which case they were probably killed out of hand), or they fell into the hands of one of the militias. In which case that militia held them for eventual trial by a reconstituted Federal judiciary, either before Judge Narragansett or at least before a court that operated under rules that Judge Narragansett, as Chief Justice of the United States, made and enforced.
 Spoilers end here.
 Wesley Mouch is, of course, a major villain in the novel. He is also a type, or rather a stereotype, of political appointees in the executive branch of either the Federal government, any State government, or the government of any of the largest cities in America. But Mouch's career is the reverse of the stereotypical career of a political appointee. Most such men (and women) begin as political appointees and end as lobbyists; Mouch starts as a lobbyist and becomes an appointee. That he comes to a sad end is because John Galt had begun, and then accelerated, a process that knocks the props out from under the system that Wesley Mouch serves.
 The most salient criticism of government appointees, whether they begin as lobbyists or become lobbyists, is that they never actually achieved anything in what is called "the private sector" of the economy. Their entire careers have been in public life—which is to say, that their entire careers have consisted of deriving their livelihoods from the government, in a circumstance in which the government is involved in matters that are beyond the legitimate purlieu of any government.
 Any government, in short, that practices intervention in the economy, will inevitably attract such men as Wesley Mouch. It appoints such men to its positions of authority over the economy, this although they know almost nothing about businesses or how to run them. And after their terms of "service" are completed, such men usually sell their influence, consisting mainly of the friendships they formed while in government service, to businessmen who now need political allies in order to stay solvent, and who often use such alliances in order to advance themselves at the expense of their competitors.
 The power of such men depends, ultimately, on two things. One is the direct sanction of the victim, which is why Wesley Mouch and the others make such a strenuous effort to get a man like Henry Rearden to cooperate with them. The other is the application of brute force, which in Mouch's case takes two forms. One is the incitement of unrest, in this case labor unrest, a project that Mouch must have known about, whether he had a direct hand in it or not. The other is the direct application of force against a person. Mouch participates directly in that application against John Galt, and when that application fails, neither he nor his associates can act. (The most horrendous direct-force application was, of course, Project X, an application that turned out to be self-destructive in more ways than one.)
 Ayn Rand's point was, of course, that on the day that the victims of the bureaucrats suddenly decide not to be victims anymore, and not to surrender in the face of the worst that the bureaucrats and their allied "goons" can throw at them, the bureaucrats will lose, and find themselves at the mercy of their own former allies and dependents, who will consider that the bureaucrats have failed them. That Rand did not fully develop the point by expounding more directly on the ultimate fate of Wesley Mouch and certain of his associates, was probably because her novel was far too long (1100 pages) as it was, and also because she cared little about the ultimate fate of such men, and far more about the triumph of men like John Galt and Henry Rearden.
 Wesley Mouch's behavior in selling out Henry Rearden is quite possibly being repeated today. Kimberly A. Strassel, in describing Democratic Party strategy in its attempts to make socialized medicine a reality in the United States, describes the behavior of a number of health-care industry lobbyists who appear to be dealing in a less-than-honorable fashion with their own clients in order to curry favor with the party in power.[1]
 Wesley Mouch's job, described as "Top Coordinator" of economic planning, is very similar to that of a "czar" in the administration of President Barack Obama. Like any of these thirty-plus "czars," Wesley Mouch was accountable to no one, and answerable to no one, except only Mr. Thompson, who appointed him. In the end, Mr. Thompson surrenders all his authority to Mouch and to the acting "science czar," Floyd Ferris, for all the good that this does for either man.
 Literary Figures Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 16 September 2016, at 13:04. This page has been accessed 20,185 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Wesley Mouch Spoiler warning  “
  What I like most about you, Mouch, is that you do not talk too much.
  ”
  “
  Get...out of my way.
  ”
 v • d • e
Atlas ShruggedBackgroundChronology · Historical contextHeroesDagny Taggart · Henry ReardenAnti-villainsJohn Galt · Francisco d'Anconia · Ragnar DanneskjöldVillainsJames Taggart · Wesley Mouch · Floyd Ferris · Mr. Thompson · Robert Stadler ·The strikersHugh Akston · William Hastings · Richard Halley · Midas Mulligan · Judge Narragansett · Ellis Wyatt · Quentin DanielsMemorable episodesDirective 10-289 · Project XMemorable places and organizationsGalt's Gulch · State Science Institute · Taggart Transcontinental Railroad · Taggart Bridge · Taggart Tunnel · Twentieth Century Motor CompanyReviewsInternal · ExternalMotion picture adaptationsAtlas Shrugged, Part 1  Background Chronology · Historical context   Heroes Dagny Taggart · Henry Rearden  Anti-villains John Galt · Francisco d'Anconia · Ragnar Danneskjöld  Villains James Taggart · Wesley Mouch · Floyd Ferris · Mr. Thompson · Robert Stadler ·  The strikers Hugh Akston · William Hastings · Richard Halley · Midas Mulligan · Judge Narragansett · Ellis Wyatt · Quentin Daniels  Memorable episodes Directive 10-289 · Project X  Memorable places and organizations Galt's Gulch · State Science Institute · Taggart Transcontinental Railroad · Taggart Bridge · Taggart Tunnel · Twentieth Century Motor Company  Reviews Internal · External  Motion picture adaptations Atlas Shrugged, Part 1 Wesley Mouch Contents Background The Betrayal Killing Colorado Directive 10-289 Dagny Taggart Railroad Unification Plan Henry Rearden's disappearance The unraveling The torture session Aftermath Typology Similarity to present day References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
