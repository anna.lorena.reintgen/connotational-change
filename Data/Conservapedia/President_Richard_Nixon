Richard Milhous Nixon was the 37th President of the United States of America, serving from 1969 to 1974. He was the only U.S. President to resign the office.  He also served as the 36th Vice President of the United States of America under President Dwight D. Eisenhower from 1953 to 1961.
 In 1946, he was elected as a U.S. Representative. As a Congressman from California, and as a member of the House Committee on Un-American Activities, he investigated Communists and instigated the successful prosecution of Alger Hiss for spying for the Soviet Union during World War II.
 After two terms in the House, he was elected to the U.S. Senate in November 1950. After losing his first presidential race to John F. Kennedy by a narrow margin in 1960, he unsuccessfully ran for Governor of California in 1962, losing to incumbent Edmund G Brown.
 In 1968 he was elected president, and was reelected in 1972 by a landslide, but resigned the presidency on August 9, 1974 due to a threat of impeachment by Congress for the Watergate Affair. The main impeachment charge was that Nixon obstructed justice by telling employees to mislead FBI investigators about the Watergate burglary. 
 Richard Milhous Nixon was born in Yorba Linda, California, on January 9, 1913. Soon after, his family moved to Whittier, California. Nixon's childhood years were not unusual for someone growing up in two small towns near Los Angeles. His parents, Frank and Hannah Nixon, were devout Quakers.  Nixon had four siblings and saw two of his brothers die from tuberculosis. Nixon grew up relatively poor, as his father earned a modest income from his gas station and grocery store. Due to these hard times, he established a quality of determination and strong work ethic. A good student and a hard worker, Nixon excelled scholastically.
 Nixon attended Fullerton High School and Whittier High School. He graduated second in his class from Whittier with honor in the study of Shakespeare and Latin. He was awarded scholarships to Harvard and Yale University, but declined due to his family's financial condition. He instead enrolled at Whittier College, a local Quaker school, where he co-founded the "Orthogonian Society", a new organization to the campus geared towards working-class students. At Whittier, Nixon, a formidable debater, was elected freshman class president, and served as student body vice president in his junior year and president in his senior year. While at Whittier, he taught Sunday school at East Whittier Friends Church and remained a member all his life. 
 A lifelong football fan, Nixon practiced with the team, but played little. In 1934, he graduated second in his class from Whittier, and went on to Duke University School of Law, where he received a full scholarship, was elected president of the Duke Bar Association, and graduated third in his class. In 1942 Nixon became a lawyer for the Office of Price Administration, the wartime liberal New Deal program that regulated all prices and rationed basic commodities. 
 During World War II, Nixon served in the Navy as a reserve officer, serving in the supply corps on several islands in the South Pacific, commanding cargo handling units in the SCAT. There he was known as "Nick" and for his exceptional poker-playing skills, banking a large sum of money that helped finance his first campaign for Congress. He rose to the rank of lieutenant commander and resigned after the war in March, 1946.
 After service in the Navy he entered an entirely unstructured California political environment—parties hardly existed there in the 1940s, and many voters were recent arrivals. As a result, Nixon never built a secure base in California (or anywhere else). In 1946 he defeated five-term Democrat Representative Jerry Voorhis, a leading liberal. Two years later, Nixon ran for reelection in both the Republican and Democrat primaries and won endorsement of both parties in the general election.[1] Nixon took typical positions for a California Republican: he was hostile to Communism, internationalist in outlook, and  middle-of-the road in economic and social issues.
 Nixon's first major breakthrough in the national limelight came in 1948 after an illegal break-in, misuse of the FBI and intelligence agencies, domestic spying and violations of civil rights, a cover-up, Congressional investigations, and a claim of Executive Privilege by President Truman.[2] The scandal began when the OSS and FBI illegally broke into the offices of Amerasia magazine, arrested 6 co-conspirators on theft of classified documents and espionage charges.[3] The Truman administration covered up the illegal break-in, stonewalled the ensuing investigations, and ultimately claimed Executive Privilege in response to a Congressional subpoena.[4]
 In 1950 Nixon was elected to the United States Senate by defeating a leading Hollywood liberal,  Helen Gahagan Douglas using tough campaign tactics that emphasized her votes with the far left.
 Because of his membership of the California delegation at the 1952 Republican National Convention, his strong anti-communist credentials, and his appeal to the western part of the United States, Nixon was named as General Dwight Eisenhower's vice presidential running mate. 
 In the midst of the campaign questions arose about a group of seventy-six businessman from southern California who had contributed to a secret slush fund for Richard Nixon, being paid $900 a month (totaling $18,168 up to that point). There was talk of Nixon dropping from the ticket. Nixon claimed that money was used for office expenses only. On September 23, 1952 he gave the now infamous "Checkers Speech" in which he said that he and his wife, Pat Nixon do not live lavishly, saying that his wife had not even owned a fur coat but only "a respectable Republican cloth coat". He went on to bring up a gift someone gave his children, a "little cocker spaniel dog" named Checkers, and said defiantly, "regardless of what they about it, we're going to keep it." The speech was meet with overwhelming public approval. In November, Eisenhower and Nixon swept their way in office, winning 55 percent of the vote, to 44 percent for Democrat opponent Adlai Stevenson.
 As Vice President Richard Nixon occasionally presided over the Senate and chaired the President's Commission on Government Contracts, which dealt with racial and religious discrimination by government contractors, and the Cabinet Committee on Price Stability for Economic growth (although Nixon had little influence over it). Nixon also chaired the National Security Council. However, in a press conference President Eisenhower was asked to give an example of Richard Nixon's contributions as Vice President, to which Eisenhower replied, "If you give me a week, I might think of one."
 Nixon did have an influential role in White House political operations. He campaigned for Republican members of Congress in 1954 and 1958. Nixon positioned himself as Presidential and his famous 1959 "Kitchen debate" in Moscow with Soviet Union President Nikita Khrushchev boosted his public appeal. By the end of the Eisenhower administration Nixon had become the top contender to be the Republican nomination for the 1960 Presidential election.
 Nixon invited Dr. Martin Luther King, Jr. to Washington, D.C., for a meeting on 13 June 1957. This meeting, described by Bayard Rustin as a “summit conference,” marked national recognition of King's role in the civil rights movement (Rustin, 13 June 1957). Seeking support for a voter registration initiative in the South, King appealed to Nixon to urge Republicans in Congress to pass the 1957 Civil Rights Act and to visit the South to express support for civil rights. Optimistic about Nixon's commitment to improving race relations in the United States, King told Nixon, “How deeply grateful all people of goodwill are to you for your assiduous labor and dauntless courage in seeking to make the civil rights bill a reality.” 
 For a more detailed treatment, see United States presidential election, 1960.
 Nixon easily won the Republican nomination for the presidency, but ran a poor campaign in the general election. Despite division over the modern civil rights movement, the country was enjoying a period of relative prosperity. 
 The most prominent issues were the Cold War and the new assumption of power of dictator Fidel Castro of Cuba. Public opinion polls showed that the country trusted Nixon more on foreign policy issues while Democrat opponent John F. Kennedy was favored on domestic issues. 1960 marked the first presidential election in which televised debates were used. Kennedy won the debates, which resulted in him defeating Nixon by a razor-thin margin of 49.7 percent of the vote to 49.5 percent. Nixon believed that there was voter fraud in Cook County, Illinois which resulted in him losing that state, however Nixon chose not to contest the results. Kennedy's lead in the electoral college was such that he still would have been elected even if he had lost Illinois.
 After the election Nixon returned to California and ran for Governor in 1962 against incumbent Pat Brown. Brown defeated Nixon with 52 percent of the vote to 46 percent. In a post-election press conference Nixon announced the end of his political career and said to the press "you won't have Richard Nixon to kick around with anymore." However, Nixon continued to campaign for Republican congressional candidates and traveled the world, sharpening his knowledge of foreign issues.
 After President Lyndon Johnson's 1964 landslide election, Nixon paid Johnson a visit to the Oval Office. The two had been old friends from the Senate where Johnson served as Majority Leader and Nixon the presiding officer. Both had eyed the presidency but Johnson got there first. Johnson asked Nixon what he was going to do now that he was a two-time loser; given his unemployment, Nixon was unsure. Johnson suggested he donate his vice-presidential papers to the National Archives and take a large charitable tax deduction which Johnson had done with his Senate and vice presidential papers. The top rate for income earners was 70% at this time and a large deduction would produce some income to start over with again. The papers of federal office holders then were considered the personal property of the office holder and it was a legal tax deduction.
 For a more detailed treatment, see United States presidential election, 1968.
 By 1967, Nixon's financial backers were raising funds to bankroll another bid for the White House. In the Republican primaries and caucuses moderates and liberals supported Michigan Governor George Romney and later New York Governor Nelson Rockefeller, while conservatives supported California Governor Ronald Reagan. Nixon was able to win support from southern conservatives and pass Reagan in the polls, eventually winning the nomination. From 1965 to 1974, Patrick Buchanan was the conservative, populist speechwriter and advisor to Nixon; in 1992 and 1996, Buchanan lost bids for the Republican presidential nomination to George Herbert Walker Bush and Bob Dole, respectively.
 Nixon's second candidacy for the presidency came amid the Vietnam War and general urban unrest stemming from perceived shortcomings of the civil rights legislation. With President Lyndon Johnson losing credibility because of the increasingly unpopular war, Nixon ran once more. Then sitting Vice President Hubert Humphrey narrowly won the Democrat nomination. Alabama Democratic Governor George Wallace, a strong segregationist with blue collar appeal, entered the race as a third party candidate. There was no "Southern strategy," Wallace carried all the electoral votes of the "Solid South." Nixon promised to end the war, unify the nation and restore law and order to the country. 
 President Johnson announced an October Surprise intended to help Humphrey, that bombing in North Vietnam would be halted and that a cease-fire would follow, however his announcement was too late. On election day, Nixon defeated Humphrey by over 100 electoral votes, although he won the election with only 43 percent of the vote to 42 percent, partly because Wallace took 13 percent.
 For a more detailed treatment, see United States presidential election, 1972.
 President Nixon's reelection campaign got underway in 1972. He had high approval ratings for his handling China and the Soviet Union. Nixon's Democrat opponent, South Dakota Senator George McGovern was viewed too liberal by many Americans. However, there was still concern in the Nixon camp because of his close victory in 1968 and the continued involvement in the Vietnam War. President Nixon beat McGovern in 49 out of 50 states.
 On April 30, 1970, ten days after announcing that 150,000 American troops would be withdrawn from Vietnam in the following year, Nixon announced that U.S. troops had invaded Cambodia.  This announcement brought widespread protests and violence at college and university campuses across the nation.  Four students died at Kent State University in Ohio and two died at Jackson State University in Mississippi.  Many campuses shut down, some for the remainder of the academic year.  The Paris Peace Accords were signed on January 27, 1973, signaling the beginning of the peace process that ended with the evacuation of the last American personnel two years later on April 30, 1975.
 After a long series of highly secret negations between Kissinger and Chinese leaders, Nixon announced that he would visit China in February 1972. During the historic trip, the leaders of both nations agreed to have a more normal relationship. Nixon told the Chinese during a banquet toast, "Let us start a long march together, not in lockstep, but on different roads leading to the same goal, the goal of building world structure of peace and justice." In taking the trip, Nixon hoped to both strengthen ties with China but also believed it would encourage the Soviet Union to be more diplomatic. He proved to be correct.
 Shortly after the public learned about China, the Soviets proposed an American-Soviet summit, a high-level diplomatic meeting that was held in May 1972. President Nixon flew to Moscow for a week-long summit, thus becoming the first American President since World War 2 to visit the Soviet Union. The two superpowers signed the first Strategic Arms Limitation Treaty, or SALT 1, a plan to limit nuclear arms that the two nations had been working on for years. Nixon and Soviet President Leonid Brezhnev also agreed to increase trade and exchange scientific information. President Nixon had made a significant mark on the world stage with major foreign policy triumphs.
 Although Nixon was a member of the Republican Party, he had many liberal positions. In his tenure he enacted the Environmental Protection Agency (EPA) and the Occupational Safety and Health Act. Nixon also fought "Global warming".[6]
 Nixon's enemies went back 20 years and consisted of many individuals and partisans who were outraged that a Congressional back-bencher disgraced an FDR protégé, a principal author of the United Nations Charter, and its first Secretary General.[7] Alger Hiss was an obvious choice to carry on the New Deal tradition in the post-World War II era, but Nixon won a successful prosecution against him that landed Hiss in prison.[8] From there, Nixon got himself elected Senator, Vice President, and eventually President. 
 Watergate was a vendetta among leftists for Nixon tarnishing the reputation of a celebrated New Dealer to advance his own career.[9] The irony of Nixon's downfall however must not be overlooked: Nixon was attempting to cover up the mistakes of the two previous Democratic administrations.[10]  The leak of the Pentagon Papers largely pertained to decisions made during the Kennedy and Johnson administrations; Nixon avoided blame.[11]  Nixon was old school - he did not see the leak of classified government secrets in partisan terms of Democrats vs Republicans, but rather American interests vs anti-American interests.  Once again, as in the Amerasia and Hiss cases, the theft of government secrets and classified documents by extreme leftists was something Nixon was all too familiar with.[12] FBI Director J. Edgar Hoover was reluctant to act, based upon Hoover's own experience in the foregoing cases.[13] So Nixon authorized the creation of a secret, non-governmental "Plumbers unit" to fix government leaks. The outfit was funded by his 1972 re-election committee. 
 But Nixon's enemies were ready to pounce based on any scintilla of evidence of wrongdoing. It began before he took office, during the transition when Nixon deeded over a portion of his vice-presidential papers to the National Archives to qualify for a charitable tax deduction. The remainder would be deeded over later to offset future income.[14] Since the time of George Washington it was customary to allow presidents to treat their papers as their own personal property,[15] but midway through his first year Congress eliminated the deduction for the gift of papers completely and established a retroactive cutoff date.[16] Nixon aides backdated some documents to make it appear the donations of the remaining papers had taken place before the cutoff date.[17]
 Several times throughout Nixon's presidency he voluntarily requested the congressional Joint Committee on Taxation (JCT) to examine his affairs. Nixon invested his $576,000 tax refund in real estate, properties that became known as the "Florida White House" and the "Western White House" in San Clemente, California. But Nixon's enemies still couldn't attach the aura of scandal in the public's eyes until after the 1972 election. Nixon won re-election, carrying a full one-third of all Democratic voters with him in his Silent Majority coalition.[18]
 Amidst the charges that Nixon illegally misused the IRS to harass enemies, the IRS promptly turned around and audited a sitting president to display its independence.[19] Nixon's $576,000 deduction - nearly three times his annual salary - meant he paid virtually no taxes for several years. The IRS found nothing wrong and gave him a clean bill of health, but the press implied Nixon was unscrupulous. This is when Nixon made his famous remark in a press conference with 400 Associated Press managing editors:
 As is readily apparent, Nixon's statement had nothing to do whatsoever with the Watergate burglary. But it was raw meat for a biased, liberal media to impugn Nixon and somehow make the connection in the public's mind, which til now had difficulty understanding what, if anything, Nixon did wrong in either Watergate or his tax troubles. To this day, "scholars" and "reputable journalists" still attempt to link the context of Nixon's remarks to Watergate. 
 The IRS re-opened the audit. Nixon again asked the JCT to look into the matter. By then, the notorious segregationist Sen. Sam Ervin was becoming an afternoon television matinee idol of the left with his Senate Watergate Hearings.
 The event that finally ended the Nixon presidency began on June 17, 1972, when five men, all employees of Nixon's reelection campaign, were caught breaking into rival Democratic headquarters at the Watergate complex in Washington, DC. The intruders and two other accomplices were convicted of burglary and wiretapping in Jan. 1973. The Watergate affair ultimately caused Nixon to resign on August 9, 1974. On September 9, 1974, his successor Gerald Ford granted him "a full, free, and absolute pardon."  This effectively ended investigation into the depth of Nixon's involvement in the break-in at Democratic National Committee headquarters in the Watergate hotel, or any other criminal activities. Former White House Counsel John Dean testified to a Congressional investigating committee of Nixon's involvement in the cover-up.
 The FBI was seeking to apprehend Bernadine Dohrn and William Ayers after they bombed the Pentagon and the United States Capitol.[25] Deputy FBI Director Mark Felt, who became a turncoat known as "Deepthroat", authorized an illegal break-in of Dohrn's sisters' apartment, Jennifer Dohrn, seeking information on the fugitives whereabouts.[26] Ayers and Dohrn, who were never prosecuted, years later groomed and  mentored future President Barack Hussein Obama.
 The president, citing Executive Privilege, refused to turn the tapes over to the committee.  In October 1973 Nixon ordered Elliot Richardson, the attorney general, to fire Archibald Cox, the special prosecutor who had subpoenaed the tapes, but Richardson resigned in protest. Richardson's assistant, William Ruckelshaus, also refused to fire Cox and was fired by Nixon. Finally, Solicitor General Robert Bork fired Cox. The incident, which was trumped in the press as the "Saturday Night Massacre", led to widespread calls for Nixon's impeachment.
 The White House released edited transcripts of the tapes in April 1974, and eventually the tapes themselves, after the Supreme Court rejected Nixon's claim to executive privilege. The House Judiciary Committee issued three articles of impeachment on July 30, 1974.
 "In all of this," the articles of impeachment summarize, "Richard M. Nixon has acted in a manner contrary to his trust as President and subversive of constitutional government, to the great prejudice of the cause of law and justice, and to the manifest injury of the people of the United States." After conferring with Republican Senators Nixon resigned on August 9, 1974. Nixon was succeeded in office the same day by Gerald Ford. Ford later pardoned Nixon.
 The Nixon White House was also involved in controversies in Latin America, which included an alleged assassination attempt in Chile, among other questionable activities.
 Nixon married Thelma Catherine Ryan, known as "Pat", in 1940.  They had two children: Patricia (b. 1946) and Julie (b. 1948).
 
 Mostly because of his involvement in the Watergate affair as well as his testifying against Alger Hiss during the McCarthy Era, Nixon has frequently been demonized by various liberals and members of the left-wing, many times even attributing any bad or unpopular stuff to him even when he had little if any involvement in them.
 One example is claiming that Nixon had been behind the Watergate break-in, even though Nixon actually had no prior knowledge to or support of the break in at all.
 Another example, generally given by the anti-war left, involved claiming that he had been responsible for American involvement in the Vietnam War as a form of American imperialism. This is ignoring that the Vietnam War actually had its roots with John F. Kennedy sending military advisors into Vietnam, and it was escalated under Lyndon B. Johnson's watch. Nixon was elected to end a "Democrat war", which he did.
 
 
 1 Early life 2 Congressional career 3 Vice Presidency 4 1960 Presidential Campaign 5 1962 - 1968 6 1968 Presidential campaign 7 1972 Reelection campaign 8 Presidency (1969-1974)
8.1 Administration
8.2 Vietnam War
8.3 Policy of Detente
8.4 Expansion of government
8.5 Impeachment proceedings
8.6 Watergate
 8.1 Administration 8.2 Vietnam War 8.3 Policy of Detente 8.4 Expansion of government 8.5 Impeachment proceedings 8.6 Watergate 9 Family 10 Liberal falsehoods about Nixon 11 References 12 Further reading 13 Primary Sources by Nixon 14 External links ↑ Only California allowed this sort of "cross filing," and they later dropped it and went to normal intra-party primaries. Richard Matthew Pious, The Presidents, pg. 515
 ↑ Blacklisted by History: The Untold Story of Senator Joe McCarthy and His Fight Against America's Enemies, M. Stanton Evans, Three Rivers Press, 2009.
 ↑ Nixon May Request Investigation Of Amerasia Scandal, Associated Press, Lewiston Evening Journal, 30 June 1950.
 ↑ Blacklisted by History: The Untold Story of Senator Joe McCarthy and His Fight Against America's Enemies, M. Stanton Evans, Three Rivers Press, 2009, pp. 579-583.
 ↑ United Nations Oral History, Interview with: Alger Hiss, 13 February 1990.  www.unmultimedia.org
 ↑ "What was the Hiss Case?": An answer for Tricia, by Richard Nixon, Six Crises, Doubleday, 1962.  law2.umkc.edu
 ↑ Does America Owe Richard Nixon An Apology? Author Geoff Shepard Says So, Book Review by Christopher N. Malagisi.  www.conservativebookclub.com .
 ↑ The Pentagon Papers for example revealed the congressional Gulf of Tonkin Resolution to be a fraud. The Tonkin incident was cooked up so President Johnson could win a Authorization for Use of Military Force from the overwhelmingly Democratic controlled Congress. Just as Barack Obama was not a member of Congress and did not vote to authorize the use of force in Iraq nor carry the baggage his opponents did over Weapons of Mass Destruction, likewise Nixon had clean hands when the Pentagon Papers were leaked showing Johnson and the Democratic Congress's Authorization for Use of Military Force was a fraud. Nixon was protecting his partisan enemies who voted for the Tonkin Resolution as the backlash of public opinion turned against the war that the Democrats had started under a false pretext.  Pentagon Papers: The Secret War, TIME, June 28, 1971. CNN.com
 ↑ THE PENTAGON PAPERS: SECRETS, LIES AND AUDIOTAPES, The National Security Archive (2013).  gwu.edu
 ↑ House Probe of Amerasia Suggested by Coast Congressman, The Deseret News, AP, Jun 23, 1950.
 ↑ First Domino: Nixon and the Pentagon Papers, Jordan Moran.  millercenter.org
 ↑ Statement of information : hearings before the Committee on the Judiciary, House of Representatives, Ninety-third Congress, second session, pursuant to H. Res. 803, a resolution authorizing and directing the Committee on the Judiciary to investigate whether sufficient grounds exist for the House of Representatives to exercise its constitutional power to impeach Richard M. Nixon, President of the United States of America. May-June 1974. Page 31 (page 43 pdf),
by United States Congress. House Committee on the Judiciary.
 ↑ Nixon Still Has $1.5 Million in Papers, Lou Cannon, Washington Post.  jfk.hood.edu
 ↑ Citizen Hughes, Michael Drosnin, Howard Hughes, Broadway Books, 2004, p. 290.
 ↑ How Nixon's Tax Scheme Backfired, Tad Szulc, New Yorker, April 15, 1974.
 ↑ An Interpretation of the 1972 Presidential Election Landslide, by CRAIG W. COOPER, 1975.
 ↑ Former IRS chief recalls defying Nixon, David Dykes, USA Today, May 26, 2013.
 ↑ Nixon Tells Editors, 'I'm Not a Crook', By Carroll Kilpatrick, Washington Post, November 18, 1973.
 ↑ President Nixon's Troublesome Tax Returns, William D. Samson, April 11, 2005.  www.taxhistory.org/
 ↑ John Dean later wrote: "By a vote of 26 to 12, the House Judiciary Committee decided against including an article in the bill of impeachment charging Nixon with tax fraud. And President Ford's pardon eliminated any risk of a criminal tax prosecution. But nothing hurt Nixon more in the public's eyes. The public understood tax fraud, and in particular, the backdating of a document such as Nixon's deed of gift." Reviving The Creative Works Tax Deduction: Why the CARE Act, Pending In Congress, Should Be Made Law, By JOHN W. DEAN, Apr. 25, 2003.  writ.news.findlaw.com
 ↑ How Richard Nixon Created Hillary Clinton, Sam Tanenhaus, Nov 5, 2015. bloomberg.com
 ↑ Statutory Law and Intelligence 2011, David Alan Jordan.
 ↑ FBI Ex-Aides' Trial: Fight Over Memos, By Laura A. Kiernan, Washington Post, October 14, 1980.
 ↑ Watergate Exposed: A Confidential Informant Reveals How the President of the United States and the Watergate Burglars Were Set-Up, by Robert Merritt as told to Douglas Caddy, Original Attorney for the Watergate Seven, Trine Day, Oct 1, 2010.
 "study the problems with respect to control, disposition, and preservation of records and documents produced by or on behalf of, Federal Officials".[24] Richard Nixon Vice President Predecessor Successor President Predecessor Successor Predecessor Successor Predecessor Successor Richard Milhous Nixon See related article : Legacy of Alger Hiss See related article : Legacy of Alger Hiss 
 Vice President Spiro AgnewGerald Ford
 Predecessor Lyndon Johnson
 Successor Gerald Ford
 President Dwight Eisenhower
 Predecessor Alben W. Barkley
 Successor Lyndon Johnson
 Predecessor Sheridan Downey
 Successor Thomas Kuchel
 Predecessor Jerry Voorhis
 Successor Patrick J. Hillings
 Republican
 Thelma Catherine "Pat" Ryan
 Quaker
  President
  Richard Nixon
  1969-1974
  Vice President
  Spiro Agnew
  1969-1973
 
  Gerald Ford
  1973-1974
  Secretary of State
  William P. Rogers
  1969-1973
 
  Henry Kissinger
  1973-1974
  Secretary of Treasury
  David M. Kennedy
  1969-1971
 
  John Connally
  1971-1972
 
  George Shultz
  1972-1974
 
  William Simon
  1974
  Secretary of Defense
  Melvin R. Laird
  1969-1973
 
  Elliot Richardson
  1973
 
  James Schlesinger
  1973-1974
  Attorney General
  John N. Mitchell
  1969-1972
 
  Richard Kleindienst
  1972-1973
 
  Elliot Richardson
  1973-1974
 
  William B. Saxbe
  1974
  Secretary of Interior
  Walter Joseph Hickel
  1969-1971
 
  Rogers Morton
  1971-1974
  Secretary of Agriculture
  Clifford M. Hardin
  1969–1971
 
  Earl Butz
  1971–1974
  Secretary of Commerce
  Maurice Stans
  1969–1972
 
  Peter Peterson
  1972–1973
 
  Frederick B. Dent
  1973-1974
  Secretary of Labor
  George Shultz
  1969–1970
 
  James D. Hodgson
  1970–1973
 
  Peter J. Brennan
  1973–1974
  Secretary of Health, Education, and Welfare
  Robert Finch
  1969–1970
 
  Elliot Richardson
  1970–1973
 
  Caspar Weinberger
  1973–1974
  Secretary of Housing and Urban Development
  George Romney
  1969–1973
 
  James Thomas Lynn
  1973-1974
  Secretary of Transportation
  John A. Volpe
  1969–1973
 
  Claude Brinegar
  1973-1974
  “
  People have got to know whether or not their President is a crook. Well, I'm not a crook. I've earned everything I've got.[20]
  ”
 v • d • e
Presidents of the United States Washington • J.Adams • Jefferson • Madison • Monroe • J.Q.Adams • Jackson • Van Buren • W.Harrison • Tyler • Polk • Taylor • Fillmore • Pierce • Buchanan • Lincoln • A.Johnson • Grant • Hayes • Garfield • Arthur • Cleveland • B.Harrison • Cleveland • McKinley • T.Roosevelt • Taft • Wilson • Harding • Coolidge • Hoover • F.Roosevelt • Truman • Eisenhower • Kennedy • L.Johnson • Nixon • Ford • Carter • Reagan • G.H.W.Bush • Clinton • G.W.Bush • Obama • Trump  Washington • J.Adams • Jefferson • Madison • Monroe • J.Q.Adams • Jackson • Van Buren • W.Harrison • Tyler • Polk • Taylor • Fillmore • Pierce • Buchanan • Lincoln • A.Johnson • Grant • Hayes • Garfield • Arthur • Cleveland • B.Harrison • Cleveland • McKinley • T.Roosevelt • Taft • Wilson • Harding • Coolidge • Hoover • F.Roosevelt • Truman • Eisenhower • Kennedy • L.Johnson • Nixon • Ford • Carter • Reagan • G.H.W.Bush • Clinton • G.W.Bush • Obama • Trump v • d • e
Vice Presidents of the United States J.Adams • Jefferson • Burr • G.Clinton • Gerry • Tompkins • Calhoun • Van Buren • R. Johnson • Tyler • Dallas • Fillmore • King • Breckinridge • Hamlin • A. Johnson • Colfax • H.Wilson • Wheeler • Arthur • Hendricks • Morton • Stevenson • Hobart • T.Roosevelt • Fairbanks • Sherman • Marshall • Coolidge • Dawes • Curtis • Garner • Wallace • Truman • Barkley • Nixon • L. Johnson • Humphrey • Agnew • Ford • Rockefeller • Mondale • Bush • Quayle • Gore • Cheney • Biden • Pence  J.Adams • Jefferson • Burr • G.Clinton • Gerry • Tompkins • Calhoun • Van Buren • R. Johnson • Tyler • Dallas • Fillmore • King • Breckinridge • Hamlin • A. Johnson • Colfax • H.Wilson • Wheeler • Arthur • Hendricks • Morton • Stevenson • Hobart • T.Roosevelt • Fairbanks • Sherman • Marshall • Coolidge • Dawes • Curtis • Garner • Wallace • Truman • Barkley • Nixon • L. Johnson • Humphrey • Agnew • Ford • Rockefeller • Mondale • Bush • Quayle • Gore • Cheney • Biden • Pence Richard Nixon Contents Early life Congressional career Vice Presidency 1960 Presidential Campaign 1962 - 1968 1968 Presidential campaign 1972 Reelection campaign Presidency (1969-1974) Family Liberal falsehoods about Nixon References Further reading Primary Sources by Nixon External links Navigation menu Administration Vietnam War Policy of Detente Expansion of government Impeachment proceedings Watergate Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
