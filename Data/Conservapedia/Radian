The International System of Units (French Système International d'Unités, Système International or SI for short) is the agreed-upon system for measurement as adopted by periodic meetings of the forty-six-member Conférence Générale des Poids et Mesures (General Conference on Weights and Measures, abbreviated CGPM). It was developed in 1960 from the meter-kilogram-second (MKS) system and replaces the centimeter-gram-second (cgs) system.
 The SI system introduced several new units for physical terms and continues to change to this day. As measurement accuracy improves, so the standards against which measures are calibrated must also be defined to higher accuracy. Definitions have been subject to change through the years, and generally depend on assumptions that the CGPM makes about what is permanent and what is not.
 Nearly all countries have adopted the SI system of units as their official system of weights and measures. The United States of America also uses the US customary system, which is defined in terms of metric units since the Convention on the Meter. 
 The value of a physical quantity is considered to a number multiplied by a unit. Units may be base units or derived units. Derived units are constructed by multiplying, dividing or raising to powers the base units or their multiples and submultiples. Thus  a unit of speed, the meter per second, can be constructed by dividing the base unit of length, the meter, by the base unit for time, the second. The symbol for a derived unit is constructed from the symbols for its constituent base units. Multiplication should be denoted by either a space or the half-high dot (ANSI symbol 0183). Division is denoted by the solidus (oblique stroke) or by a negative exponent. For example, 25 meters per second could be written in symbols as 25 m/s or 25 m·s−1. New derived units can always be constructed as new situations arise. There is no limit to the number of multiplications, divisions and raising of powers that can be used, so there is no limit to the number of possible derived units. 22 of the derived units have their own names.
 SI depends on seven base units of measurement from which all other units derive. They are:
 There are 60 seconds in a minute and 3600 seconds in an hour.
 Historically the second was without definition, until 1820, when it was defined as 1/86,400 of the mean solar day. In 1956 the CGPM adopted the ephemeris second definition of 1/31 556 925.9747 of the tropical year 1900. The present definition dates from 1963.
 The meter (or metre outside the US) (symbol m) is the unit of length. Originally (see Metric system) it had a definition depending on the dimensions of the earth. It was defined so that the circumference of the Earth would be exactly 40 million meters. However, the circumference of the Earth depends on where you measure it, so the French creators of the metric system chose the circumference which travels through both poles, along the meridian that passes through Paris. (This was a snub to the British, who had defined longitude by reference to the meridian passing through Greenwich).
It later had a definition dependent on a particular type of radiation. Today its definition depends upon the second and on the speed of light in a vacuum—which the CGPM now assumes to be constant. Thus a meter is 1⁄299,792,458th of a light-second. (Therefore, the speed of light is exactly 299,792,458 meters per second.)
 The kilogram (symbol kg) is the unit of mass. It is defined as an amount of mass equal to that of the standard prototype kilogram kept at the International Bureau of Weights and Measures at Sèvres, France. It is the only unit of measure based on a physical artifact. This is the same artifact that has provided the definition of grams and kilograms for over a century. It is also the only base SI unit to retain its decimal prefix, kilo-, which comes from the Greek word for "thousand."  Efforts are presently underway to redefine the kilogram without the use of a physical artifact.
 The kelvin (symbol K, named after Lord Kelvin) is the unit of temperature, and specifically of absolute temperature. It is 1/273.16 of the absolute temperature of the triple point of water. The size of the kelvin is the same as the size of the Celsius degree, and thus to convert from degrees Celsius to kelvins, one adds 273.15. (The zero of the Celsius scale, which is the melting point of water ice under standard atmospheric pressure, happens to be 273.15 kelvins.) Note that one says '200 kelvins' not '200 degrees kelvin'.
 The mole (symbol mol) is the unit of amount of substance. Its definition depends on that of the kilogram. It is that mass of any substance having as many elementary entities of that substance as are to be counted in 12 grams of carbon-12. An elementary entity is either an atom or a molecule—but for an ionic compound, the number of elementary entities is the total number of cations and anions required to make the compound electrically neutral.
 Avogadro's number gives the required number of elementary entities, and is typically represented by NA. NA has the approximate value of 6.022X1023
 Effectively, one mole of any substance is its formula mass in grams.
 The term hertz means "vibrations, transitions, or other events per second." For the definitions of the terms watt and steradian, see below. Light having the stated frequency is generally perceived as red.
 SI has multiple derived units that measure quantities in several areas of measurement and inquiry. The following is a comprehensive list of the named derived units, grouped by category.
 The newton (symbol N, named for Sir Isaac Newton) is the SI unit of force.  Force is generally found by multiplying the mass times the acceleration of the mass (giving us the more complicated form  )
. One newton is that amount of force required to give a one-kilogram mass an acceleration of one meter per second per second. To calculate, multiply mass by acceleration.
 The pascal (symbol Pa, named for Blaise Pascal) is that amount of pressure that exerts a force of one newton per square meter of area.
 The joule (symbol J, named for James Joule) is that amount of energy required to do the work of exerting a force of one newton over a distance of one meter. To calculate, multiply force by distance, which reveals a joule is a Newton-meter or .  To expand further (keeping in mind a newton is a  ) a joule is a 
 The watt (symbol W, named for James Watt) is the power required to do one Joule of work in one second. To calculate, divide work by time.
 The coulomb (symbol C, for C. A. Coulomb) is that amount of electric charge that passes through a conductor in one second at a current of one ampere. To calculate, multiply current by time.
 The volt (symbol V, named after Alessandro Volta) is the electric potential difference between two conductors having a current flow of one ampere from one to another, and dissipating one watt of power.
 The ohm (symbol Ω,[1] for Georg Simon Ohm) is that amount of electric resistance that requires an electric potential difference of one volt to push a current of one ampere through it. To calculate, divide electrical potential by current.
 The siemens (symbol S, for Werner von Siemens) is the unit of electric conductance, which is merely the reciprocal of resistance. In essence, the siemens is that level of conductance required to allow a current of one ampere to flow through a load after an electric potential of one volt is applied. To calculate it, divide current by electric potential or "voltage," rather than dividing voltage by current.
 The farad (symbol F, for Michael Faraday) is that amount of capacitance that exists in a capacitor (essentially a pair or collection of metal plates that store charge) if a charge of one coulomb increases the electrical potential by one volt. To calculate, divide charge by electrical potential.
 The henry (symbol H) is that inductance in a closed loop or coil that produces a magnetic flux of one weber given a current of one ampere. To calculate, divide magnetic flux by current.
 The tesla (symbol T, for Nikola Tesla) is the magnetic flux density required to produce one weber of magnetic flux in one square meter of surface area. To calculate, divide flux by area.
 The lumen (symbol lm) is that amount of luminous flux emitted in a solid-angle region of one steradian that produces one candela of intensity. To calculate, multiply luminous intensity by solid angle.
 The lux (symbol lx) is that illuminance that represents one lumen in one square meter. To calculate, divide luminous flux by area.
 The degree Celsius (symbol °C) is a derived unit of temperature centered on the freezing temperature of water ice rather than absolute zero. It is defined by the temperature in kelvins minus 273.15. Note that 
 The becquerel (symbol Bq, for Henri Becquerel) is that amount of activity of a radioactive substance represented by one decaying atom per second.
 The gray (symbol Gy) is that absorbed dose of radiation that imparts one joule of energy to one kilogram of target mass. To calculate, divide imparted energy by mass.
 The sievert (symbol Sv) is the unit of dose equivalent. It is that dose of any given form of ionizing radiation that would cause a human being or an animal as much injury as would a one-gray dose of X rays.
 The metric system originally had the two units of plane angle and solid angle. Since they consist of a unit of length divided by a unit of length and thus dimensionless CGPM decided not to retain them in the base units. They were put into a separate category called Supplementary Units. They have been moved again and are now listed as named Derived Units.
 The radian (symbol rad) is the plane angle unit. It is the measure of a central angle (one having its origin as the center of a circle) that subtends an arc of the circle having a length equal to that of the radius. By convention, the circumference of the unit circle (a circle having a radius of one) is 2π, and therefore the maximum "major angle" of any circle is 2π radians.
 The steradian (symbol sr) is the solid angle unit. A solid angle is actually one nappe of a right circular conical surface. One steradian is the measure of a solid angle that subtends a portion of the surface of a sphere having an area equal to that of a square having sides equal in length to the radius. By convention, the surface area of a sphere is 4πr2, where r is the length of the radius, and thus the maximum "major solid angle" of any sphere is 4π steradians.
 As the metric system has changed over time, some units have been abandoned and replaced with others. Examples of obsolete metric units, which should not be used.
 The base and derived units can be multiplied by prefixes in order to extend their range to virtually every magnitude used in practice, from subatomic physics to the large-scale structure of the universe. The following prefixes are available.
 The units of the SI do not have abbreviations, as these would vary depending on the language used and SI is designed to be the global standard. An English speaker might write 'sec' as an abbreviation for 'second', whereas a Spanish speaker would write 'seg' for 'segundo'. To get around this problem, a set of language-independent symbols were defined for each unit. Thus the symbol for the second is 's', for both the Spanish and English speaker. The SI symbols are a global standard, just like the Arabic numerals and the symbols for the chemical elements, that are the same whatever language is being used. For example, the language of Welsh does not contain the letter 'k' and the word 'kilometer' is spelt 'cilometr'. Nevertheless, a Welsh person writing in Welsh that 'the runway is 3 km long' would use '3 km' for 'three kilometers'. By contrast, use of an abbreviation such as 'cm' for 'cilometr' would cause confusion, as 'cm' denotes the centimeter.
 The rules of the SI system are defined in the International Organization for Standardization documents ISO 1000 and ISO 31. They define not only the units, but the ways in which they are to be written.
 The liter was a unit of volume in the original 1795 definition of the metric system.It was defined as a cubic decimeter, i.e. a volume of 1/1000th of a cubic meter. However, the 1901 redefinition of the liter as the volume of 1 kg of water at the temperature when it takes its maximum density (3.98 °C) yields a volume greater than a cubic decimeter by 28 parts per million. For everyday purposes this was not a problem, but for highly accurate scientific purposes it then became important to specify whether one actually meant a liter or cubic decimeter. The liter was thus not recommended for scientific purposes and technically became no longer part of the metric system. Officially, volume is measured in cubic meters and its multiples and submultiples, such as the cubic centimeter. The 1964 definition of the liter changed it back to exactly a cubic decimeter but technically it is a now a unit approved for use alongside the metric system but not part of it, alongside the minute, hour, year and others. 
 In the English system of weights and measures used in the USA, different units of volume are used for dry goods than for liquids. The Queen Anne gallon of 231 cubic inches is used for liquids and the Winchester gallon is the basis of the Winchester bushel of 2150.42 cubic inches. However, in the metric system no distinction is made between units of volume used for dry goods and units of volume used for liquids. The cubic meter and its multiples and submultiples are used for all volumetric purposes. This is part of a general difference of approach between the two systems. The English system tends to use different measures for the same quantity depending on the context whereas the metric system uses the same measure. For example, in the English system pressure is measured in inches of mercury for weather, pounds per square inch for tire pressure and pounds per square foot for floor pressure. In the metric system these would be all measured in pascals or kilopascals. 
 As scientific and engineering measurements become more accurate over time, the measurement standards they are compared against need to be continually refined.  The National Institute for Science and Technology (NIST), based in Gaithersburg, Maryland, carries out world-class research in metrological science. NIST represents the United States in international committees on metrology.
 The official, definitive definition of the SI system, from the Bureau Internationale des Poids et Mesures
 Guide to the SI System from NIST, the lead federal agency on all metrology issues for the US
 
SI at Sizes.com
 1 Principles 2 The Base Units
2.1 Second
2.2 Meter
2.3 Kilogram
2.4 Ampere
2.5 Kelvin
2.6 Mole
2.7 Candela
 2.1 Second 2.2 Meter 2.3 Kilogram 2.4 Ampere 2.5 Kelvin 2.6 Mole 2.7 Candela 3 Named Derived Units
3.1 Dynamics
3.1.1 Newton
3.1.2 Pascal
3.2 Work and Power
3.2.1 Joule
3.2.2 Watt
3.3 Electricity and Magnetism
3.3.1 Coulomb
3.3.2 Volt
3.3.3 Ohm
3.3.4 Siemens
3.3.5 Farad
3.3.6 Weber
3.3.7 Henry
3.3.8 Tesla
3.4 Light
3.4.1 Lumen
3.4.2 Lux
 3.1 Dynamics
3.1.1 Newton
3.1.2 Pascal
 3.1.1 Newton 3.1.2 Pascal 3.2 Work and Power
3.2.1 Joule
3.2.2 Watt
 3.2.1 Joule 3.2.2 Watt 3.3 Electricity and Magnetism
3.3.1 Coulomb
3.3.2 Volt
3.3.3 Ohm
3.3.4 Siemens
3.3.5 Farad
3.3.6 Weber
3.3.7 Henry
3.3.8 Tesla
 3.3.1 Coulomb 3.3.2 Volt 3.3.3 Ohm 3.3.4 Siemens 3.3.5 Farad 3.3.6 Weber 3.3.7 Henry 3.3.8 Tesla 3.4 Light
3.4.1 Lumen
3.4.2 Lux
 3.4.1 Lumen 3.4.2 Lux 4 Temperature
4.1 Radioactivity, Radiation, Health Physics, and Radiation Safety
4.1.1 Becquerel
4.1.2 Gray (Unit)
4.1.3 Sievert
 4.1 Radioactivity, Radiation, Health Physics, and Radiation Safety
4.1.1 Becquerel
4.1.2 Gray (Unit)
4.1.3 Sievert
 4.1.1 Becquerel 4.1.2 Gray (Unit) 4.1.3 Sievert 5 Supplementary Units
5.1 Radian
5.2 Steradian
 5.1 Radian 5.2 Steradian 6 Obsolete metric units 7 Metric Prefixes 8 Symbols, not Abbreviations 9 Writing Metric 10 Units of volume in the metric system
10.1 Metric and English Volume Units Compared
 10.1 Metric and English Volume Units Compared 11 Lead Agency 12 References 13 See also 14 References  The centimeter (or centimetre outside the US) (symbol cm) is one-hundredth of a meter.  It is not a base unit but is included here because it is a commonly used unit of length.  the degree symbol (°) is a mandatory part of the symbol, as 'C' denotes the coulomb, the unit of electric charge  It is perfectly correct, though rather unusual in practice, to use metric prefixes with the degree Celsius. Thus one can say 'the melting point of the metal is in excess of 2 k°C'  While one does not say '200 degrees Kelvin' one does say '200 degrees Celsius'   Prior to 1960 the degree Celsius was known as the degree Centigrade, a term that is now deprecated.  The calorie was originally defined as the amount of energy required to raise 1 g of water through 1 °C. It later was clarified to raising the temperature from 15 °C to 16 °C, since the energy required varies slightly with temperature. Confusingly, a second calorie, the Calorie (spelt with an upper case C) was introduced that was defined as the amount of energy required to raise 1 kg of water through 1 °C. Thus, 1 Calorie = 1000 calories. The Calorie (big C) became widely used as a measure of food energy. To avoid the confusion the Calorie was renamed the kilocalorie and the term Calorie was deprecated. The kilocalorie has now been replaced by the joule as the unit of energy, though energy values in kilocalories are still widely seen.  The Ångstrom was a very small unit of length equal to 10−10 m. It was used for describing the wavelength of light, for example. The ångstrom was replaced by the nanometer. 1 nm = 10 Å.  The curie (symbol Ci, for Marie Curie) is an obsolete unit of radioactivity. Its original definition was "the amount of radioactivity in one gram of radium-226." Madame Curie, of course, is most famous as the discoverer of radium. It was replaced by the becquerel, though it is still widely used in the US nuclear power industry. One curie is equal to 3.7 * 1010 becquerel.  The micron (symbol µ) was a unit of length equal to one thousandth of a millimeter. It was replaced by the micrometer (symbol µm) in 1960.  The degree Centigrade (symbol °C) was renamed the degree Celsius (symbol °C) in 1960.  A space is to be put between the number and the unit. '25 km' is correct, '25 km' is incorrect. This is a common mistake for users of English units, where no space is placed between the number and the unit.  SI units do not take a trailing 's' to denote the plural. Thus '5 cms' is wrong and '5 cm' is correct. The reason is that 's' is the symbol for the second and 'cms' (intended to mean centimeters) could be confused with 'cm s' (meaning centimeter-seconds, a derived unit)  SI symbols do not take a trailing period. (This would be appropriate if they were abbreviations, but they are not.)  Since the SI symbols are symbols they are not altered to fit in with the surrounding text. The case of the symbol does not change - kelvin is always written as 'K' and kilograms is always written as 'kg'.  Furthermore, they are never written in italics. Thus The woman lost 25 kg in weight is incorrect and should be The woman lost 25 kg in weight. Similarly, it is incorrect to alter the symbols to upper case (as often observed on shipping crates).  Metric system This describes the original metric system as defined at its creation in 1795. ↑ That's the Greek letter omega.  Never let it be said that physicists don't have a sense of humor.
 Health Measurements Radio Amateur Radio Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 5 July 2019, at 10:28. This page has been accessed 62,749 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 
the duration of 9,192,631,770 periods of the radiation corresponding to the transition between two hyperfine levels of the ground state of the cesium-133 atom (13th CGPM, 1967). One ampere is that constant current which, if maintained in two straight parallel conductors of infinite length, of negligible circular cross section, and placed 1 meter apart in a vacuum, would produce between these conductors a force equal to 2 × 10−7 newtons per meter of length. the luminous intensity, in a given direction, of a [light] source that emits monochromatic radiation of frequency 540 × 1012 hertz and that has a radiant intensity in that direction of 1⁄683 watt per steradian. the magnetic flux which, linking a circuit of one turn, would produce in it an electromotive force of 1 volt if it were reduced to zero at a uniform rate in 1 second. International System of Units SI base units derived units solidus second meter metre centimeter centimetre kilogram ampere kelvin mole candela watt steradian newton pascal joule watt coulomb volt ohm siemens farad weber henry tesla lumen lux becquerel gray sievert radian steradian calorie Ångstrom curie micron degree Centigrade symbols  Main Article: Mole (chemistry)  Main Article: Pascal  Main Article: Mole (chemistry)  Main Article: Pascal  yocto-
  y
  10−24
  zepto-
  z
  10−21
  atto-
  a
  10−18
  femto-
  f
  10−15
  pico-
  p
  10−12
  nano-
  n
  10−9
  micro- (Greek small)
  µ
  10−6
  milli- (Latin one thousand)
  m
  10−3
  centi- (Latin one hundred)
  c
  10−2
  deci- (Latin ten)
  d
  10−1
  deka- (Greek ten)
  da
  101
  hecto- (Greek one hundred)
  H
  102
  kilo- (Greek one thousand)
  k
  103
  mega- (Greek large)
  M
  106
  giga- (Latin gigantic)
  G
  109
  tera- (Greek wonder)
  T
  1012
  peta-
  P
  1015
  exa-
  E
  1018
  zetta-
  Z
  1021
  yotta-
  Y
  1024
 International System of Units Contents Principles The Base Units Named Derived Units Temperature Supplementary Units Obsolete metric units Metric Prefixes Symbols, not Abbreviations Writing Metric Units of volume in the metric system Lead Agency References See also References Navigation menu Second Meter Kilogram Ampere Kelvin Mole Candela Dynamics Work and Power Electricity and Magnetism Light Radioactivity, Radiation, Health Physics, and Radiation Safety Radian Steradian Metric and English Volume Units Compared Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console Newton Pascal Joule Watt Coulomb Volt Ohm Siemens Farad Weber Henry Tesla Lumen Lux Becquerel Gray (Unit) Sievert 
