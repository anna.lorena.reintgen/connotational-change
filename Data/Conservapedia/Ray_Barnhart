Mallory Ann Barnhart Rousellot​
 St. Clairsville, Belmont County, Ohio
 Marietta College​
University of Houston
 United States Army service
 Ray Anderson Barnhart (January 12, 1928 – May 26, 2013) was a businessman and Republican politician, formerly from Pasadena in Harris County in suburban Houston, Texas.​
 From February 12, 1981 to December 31, 1987[2], Barnhart was director of the Federal Highway Administration under U.S. President Ronald W. Reagan. In 1976, he, along with Ernest Angelo, Jr., of Midland, and Barbara Staff of Dallas were co-chairmen of the Reagan presidential primary campaign against sitting President Gerald  Ford. From 1973 to 1974, Barnhart was a one-term member of the Texas House of Representatives. He previously served on the Pasadena City Council from 1965 to 1969.[3]​
 Barnhart was born to Ora E. Barnhart (1904–1991)[4] and the former Alice Mildred Anderson in Elgin in Kane County west of Chicago in northeastern Illinois, to the east of Reagan's birthplace in  Tampico in western Illinois. An Eagle Scout, Barnhart graduated from Elgin High School in January 1945, a semester earlier than most of his classmates. He enlisted in the United States Army and served from 1946 to 1947.[3]
 Thereafter, he studied speech and theatre arts at Marietta College in Marietta, Ohio, from which he obtained the Bachelor of Arts degree in 1950. He married the former Jacqueline Price[3] (born 1927) in Bellaire in Belmont County,  Ohio. In 1951, he received a Master of Arts in speech, theater, and radio communication from the University of Houston.[5] He returned to Marietta College as a professor from 1951 to 1955. With an ending salary of $3,050 annually at Marietta College, the Barnharts relocated to Houston, where he entered the construction businesses. He first dug water and sewer lines and later specialized in underground utilities. Thereafter, he was an insurance agent from 1978 to 1981.[3]​
 Barnhart's political interest began with two terms on the city council in Pasadena, which he describes at the time as "a lovely community but politically corrupt."[6] He and other businessmen worked to establish a mayor-council form of government to replace the city commission government then in place in Pasadena and first used in 1901 in nearby Galveston. "We found it was hard to get good people to run for the city offices," he said. After two terms on the council, Barnhart ran unsuccessfully for mayor but was defeated by Clyde Doyle, a Democrat.[6] In Texas, municipal offices are technically non-partisan, and hence no party labels appear on the ballot. Barnhart recalls that a half dozen Pasadena officials were indicted in the late 1950s and early 1960s for public corruption.[6]​
 In 1972, Barnhart was elected to a two-year term in revised District 100 (previously District 24-6)[7] in the state House of Representatives, when President Richard M. Nixon and U.S. Senator John Tower were heading the state Republican ticket, along with the defeated gubernatorial nominee Henry C. Grover of Houston. Barnhart's House colleagues included Ray Hutchison (1932-2014) of Dallas and Kay Bailey of Harris County. The two married during their common legislative tenure, and Kay Bailey Hutchison went on to the United States Senate in 1993, a post she held until 2013, with the accession of Ted Cruz. 
 In the 1974 session, the legislature became an ex officio constitutional convention headed by Speaker Price Daniel, Jr., of Liberty, Texas. In the deliberations Barnhart opposed a motion to "guarantee equal educational opportunity and free public schools" to all citizens. As drafted, Barnhart said that the motion, offered by the University of Texas Board of Regents chairman Frank Erwin would have prevented the state from charging tuition and fees at its public colleges and universities. Legislators acting as delegates fell three votes short of the 121/188, or 2/3 supermajority needed to adopt the new constitution. The state is hence still governed by the Texas Constitution of 1876. In 1974, a strongly Democratic year nationally as well as in Texas, Barnhart was defeated for a second term in the legislature by the Democrat Bill Caraway. Barnhart recalls that certain members of the Sagemont Church, a mission of First Baptist Church of Pasadena, organized in 1966 by founding pastor John D. Morgan,[8] worked against Barnhart because of his conservative fiscal policies and succeeded in bringing about the freshman lawmaker's defeat. Barnhart added that the long-serving pastor John Morgan had no role in the political action.[6]​
 At the national level, Barnhart had supported Barry Goldwater's ill-fated campaign in Texas against native son Lyndon B. Johnson. In 1975 and 1976, as chairman of the Harris County Republican Party, Barnhart was asked by Reagan to manage the Californian's presidential primary campaign in Texas. Barnhart, then a United Methodist, enlisted as co-chairmen Ernest Angelo, the mayor of Midland and a Roman Catholic, and Barbara Staff, a party leader then active in the First Baptist Church of Dallas under pastor Wallie Amos Criswell (1909-2002). The campaign was so successful that the Reagan forces prevailed, 96-0, over President Ford, who had the backing of Senator Tower. Then four more Reagan delegates were chosen at the state convention. Barnhart recalled that wealthy Texas contributors were loyal to President Ford, but the Reagan forces had the "plain people" as voters and election workers. Barnhart said that he offered Tower and John Connally, the former Democratic governor who switched parties in 1973 after having served as United States Secretary of the Treasury under President Nixon, delegate positions at the 1976 Republican National Convention in Kansas City, Missouri, provided they would agree to support Reagan for the first three potential ballots. The two declined Barnhart's offer and were hence not delegates to the convention. Barnhart chaired the Texas delegation at the convention.[9]​
 Barnhart himself was a Reagan delegate in Texas' 22nd congressional district, then including |Fort Bend, Brazoria, and parts of Harris and Waller counties. His side prevailed, 15,054 to 7,934 for the Ford forces.[10][11]
 From 1975 to 1977, Barnhart was the Harris County Republican chairman. He was then state Republican chairman from 1977 to 1979, having succeeded Ray Hutchison, who stepped down to run unsuccessfully for governor in the primary against Bill Clements. Barnhart then worked to elect Clements, who secured a narrow gubernatorial victory over the Democrat John Luke Hill (1923-2007), the state attorney general and former chief justice of the Texas Supreme Court. As state GOP chairman, Barnhart was succeeded by Chester R. Upham, Jr., an oil and natural gas businessman from Mineral Wells, who held the position until 1983.[12]
 In May 1979, incoming Governor Clements appointed Barnhart to the then three-member (since five members) Texas Department of Transportation. He served on the Texas Turnpike Authority,[3] the state agency responsible for construction and operation of major toll facilities.. At that time, Barnhart left his position with the Barmore Insurance Agency in Pasadena.[13]​
 Barnhart served for nearly two years under Clements in the Texas DOT until Reagan offered him the directorship of the Federal Highway Administration in Washington, D.C. There Barnhart was known for his leadership, easy-going manner, and communication skills. In 1982, he fought to increase highway user fees for the first time in twenty-three years. He streamlined procedures, shortened delays, upheld the soundness of the Highway Trust Fund, and returned management authority to the states.[14]
 Barnhart worked to establish forty-two technology transfer centers at American colleges and universities and restructured and strengthened the motor carrier and international highway programs.[14] In his first meeting with Congress on February 25, 1981, Barnhart vowed to present the "best professional judgments of the experts in my administration, tempered, of course, by the economic restraints which must be applied to all of government."[15]
 In 2004, Barnhart was inducted into the Texas Transportation Institute Hall of Fame. On leaving the FHWA, he had been recognized by the United States House of Representatives for his "honest, effective and meaningful efforts to preserve and improve the Federal-Aid [Highway] System – one of the nation's most vital assets."[5]​
 Years after leaving the position, Barnhart described the agency during his tenure as "a strictly professional, competent, caring agency."  His resignation from the highway administration involved the supervision of 3,400 employees, took effect on December 31, 1987. Barnhart was briefly considered for the position of United States Secretary of Transportation to succeed Elizabeth Dole. However, the appointment went instead to James H. Burnley, IV, of North Carolina.[16]​
 Barnhart claims that the agency lost its professionalism and became "politically oriented" during the administration of President Bill Clinton,[6] whom Barnhart opposed in 1992 and 1996 when he supported George Herbert Walker Bush and Robert J. Dole for the presidency. Similarly, Barnhart said that  Ann Richards, Clements' second successor as governor, "wiped out the career people and put in political appointees" at the Texas DOT.[6]​
 Barnhart's seven-year tenure as FHWA director is still an agency record for longevity. Under the "New Federalism" proposals offered in 1982, Reagan sought to return many federal highway and mass transit programs to the states except for the interstate highway system. However, the Surface Transportation Assistance Act continued strong federal involvement in both highways and mass transit. Because of infrastructure needs, the law increased the gasoline tax by a nickel, the first such increase since 1959.[17]​
 On January 6, 1983, Reagan signed the Surface Transportation Assistance Act so that the United States could improve its paving and bridge commitments by the middle of the decade. The legislation devoted one cent (later doubled) of each nickel to establish a transit account in the Highway Trust Fund.[17]​
 Despite the need for highway improvements and increasing fuel prices of recent years, Texas has not raised its 20-cent-per-gallon gasoline tax since the early 1990s, and the diesel tax has been unchanged even longer. In 2008, Barnhart said that only twenty-three states had increased the fuel tax. In a letter to Democratic state Senator Kirk Watson of the capital city of Austin, Barnhart said that the legislature, not the Texas Department of Transportation, is responsible for problems involving highways. "If the legislature had acted when I warned them fifteen years ago, Texas highway problems wouldn't be as critical as they are today. They [legislators] didn't have the guts to deal with the big truck lobby," Barnhart said in an interview. Barnhart said that 18 wheelers and other large trucks place excessive strain on state highways, but the legislature has not increased the diesel tax to maintain the transportation network. A $5 billion bond package Texas voters approved in November [2007] won't be enough to alleviate some of those problems because the state needs much more than that amount just to maintain its highway system.[18]
 Former state Representative Carl Isett, a Republican from Lubbock, said that Barnhart made some valid points in his assessment but is too prone to blame the legislators for financial woes of TxDOT: "It's a combination of those factors. We have not given them everything they wanted but we have given them a lot. We try to fund more money for roads but a lot of that money comes from the feds." Isett said that critics blame the legislature "every time something goes wrong."[18]
 During his highway tenure, the Barnharts resided in Falls Church, Virginia. The couple has two daughters, Whitney Allison Barnhart (born 1948), formerly Whitney Ziegler,[3] of Shadyside in Belmont County, Ohio, and Mallory Ann Barnhart Rousellot (born 1955) and husband, Mark W. Rousellot (born 1954), ranchers from Sonora in Sutton County, Texas. Mallory Rousellot owns a restaurant in Sonora known as Mercantile on Main, located at the foot of the hill on which sits the county courthouse. Mallory Rousellot is a former board member of the Sonora Independent School District and has been the chairman of the Sutton County Republican Party. 
 The Barnharts relocated to Mrs. Barnhart's home county and resided in St. Clairsville, Ohio, within the Wheeling, West Virginia Standard Metropolitan Statistical Area. Barnhart had serious health problems in later years and underwent various medical treatments in Wheeling.[6]
 Barnhart died at the age of eighty-five on May 26, 2013.[19] At his request there was no funeral. He was interred privately in a family tomb on the Rousselot Ranch in Sonora.[20]
 ​​​​
 1 Background 2 State politics 3 National politics 4 Supporting Bill Clements 5 Federal Highway Administration 6 Later life 7 References ↑ 1.0 1.1 Members and Leaders of the Texas Legislature. Lrl.state.tx.us. Retrieved on January 9, 2010.
 ↑ [1]
 ↑ 3.0 3.1 3.2 3.3 3.4 3.5 Who's Who in America, 1984–1985, 43rd ed., Chicago: Marquis Publishers, p. 175.
 ↑ , Ancestry.com, accessedJanuary 8, 2010.
 ↑ 5.0 5.1 Texas Transportation Institute Hall of Honor:Ray A. Barnhart. Tamu.edu. Retrieved on January 9, 2009.
 ↑ 6.0 6.1 6.2 6.3 6.4 6.5 6.6 Statement of Ray Barnhart, January 7, 2010.
 ↑ Because of changes in district lines over the years, District 100 is now in Dallas, rather than Harris County.
 ↑ Biographical Data. Sagemontchurch.org. Retrieved on July 20, 2014.
 ↑ Billy Hathorn, "Mayor Ernest Angelo, Jr., of Midland and the 96-0 Reagan Sweep of Texas, May 1, 1976," West Texas Historical Association Yearbook Vol. 86 (2010), pp. 81, 86.
 ↑ 1976 Texas presidential primary returns, Texas State Library and Archives Commission, P.O. Box 12927, Austin, Texas, 78711-2927
 ↑ WTHA Yearbook, p. 81.
 ↑ Laci Morrison and Libby Cluett, (January 25, 2008). Upham's death mourned by local, state political leaders. Mineral Wells Index. Retrieved on Aprl 6, 2019.
 ↑ Ronald Reagan: Nomination of Ray A. Barnhart To Be Administrator of the Federal Highway Administration, January 20, 1981. Presidency.ucsb.edu. Retrieved on January 9, 2010.
 ↑ 14.0 14.1 Ray A. Barnhart. Federal Highway Administration (July 3, 2003). Retrieved on April 6, 2019.
 ↑ Statement of Ray A. Barnhart, Administrator, Federal Highway Administration. Testimony.ost.dot.gov. Retrieved on March 9, 2010.
 ↑ "Highway Agency Chief to Resign." The Washington Post, October 20, 1987.
 ↑ 17.0 17.1 Richard Weingroff (December 23, 2008). In Memory of President Reagan. Federal Highway Administration. Retrieved on February 1, 2010.
 ↑ 18.0 18.1 Enrique Rangel (February 18, 2008). TxDOT traveling bumpy road. Lubbock Avalanche-Journal. Retrieved on February 1, 2010.
 ↑ Ray Barnhart (May 27, 2013). Retrieved on May 30, 2013.
 ↑ Ray Barnhart. Ilnewsfeed.com. Retrieved on April 6, 2019.
 Copied Articles Texas Illinois Ohio Professors Business People Politicians Republicans Conservatives United Methodists United States Army Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 7 October 2019, at 12:57. This page has been accessed 557 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 ! In office In office Ray Anderson Barnhart !
 This article was slightly edited from Wikipedia but the text was originally written by BHathorn (under the name) and does not include alterations made by others from that site.
 
 Ray Anderson Barnhart​
 Director of theFederal Highway Administration
 In officeFebruary 12, 1981​ – December 31, 1987​
  Ronald Reagan​
  John S. Hassell, Jr.​
  Robert E. Farris​
 State chairman of the Texas Republican Party ​
 In office1977​ – 1979
  Ray Hutchison​
  Chet Upham​
 Texas State Representativefor Harris County
  Johnny Nelms (District 24-6)[1]​
  Bill Caraway[1]​
 
  January 12, 1928​Elgin, Kane County,Illinois, USA​
  May 26, 2013 (aged 85)​Wheeling, West Virginia​
  Rousselot Ranch in Sonora in Sutton County, Texas​
  American
  Jacqueline Price Barnhart ​
  Whitney Allison Barnhart​
Mallory Ann Barnhart Rousellot​
  Pasadena, Texas
St. Clairsville, Belmont County, Ohio
  Elgin High School​
Marietta College​
University of Houston
  Businessman​
United States Army service
  United Methodist
 Ray Barnhart Contents Background State politics National politics Supporting Bill Clements Federal Highway Administration Later life References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
