Born Yesterday was a 1950 motion picture comedy about influence-buying in Washington, DC. In it, the strident and ignorant kept woman of an unscrupulous businessman receives an education in civics from a newspaperman who has been investigating her keeper for a long time. When she realizes that her life of luxury is empty, she becomes his most formidable foe.
 Millionnaire junk tycoon Harry Brock has arrived in Washington to do business with a number of United States congressmen. He arrives at the (fictional) Hotel Stadtler, with his kept woman, former chorus girl Emma "Billie" Dawn, in tow. As the hotel manager greets him effusively and shows him to his private elevator, journalist Paul Verrell, who has been waiting in the hotel gift shop for Brock to arrive, tries to ask Brock for an interview. Brock dismisses Varrell and, after sarcastically observing that the manager had after all said that the elevator was private, peremptorially orders him to send the elevator up to his private seventh-floor wing.
 This wing consists of three complete suites of rooms, all sharing a common corridor (and an interior service corridor connecting all three suites), with access to a public corridor and elevator bank. The manager tries to tell Brock that this wing is normally reserved for visiting foreign dignitaries, but Brock, taking his shoes off in front of the man, doesn't seem to care. Billie installs herself in the north suite, which is her exclusive quarters—but not before Jim Devery collects Billie's signature on several corporate instruments that she does not pretend to understand. The only question she asks is what happened to several other papers she signed about a week before; Jim says only that "they're all used up."
 Paul Varrell uses the public elevators to reach the seventh floor, and now Brock receives him, on the strenuous advice of his lawyer, Jim Devery. Brock insists on taking a private shave, manicure, and shoe shine while interviewing Varrell. Varrell entices him to admit that he started crooked, employing strong-arm tactics and shady practices (such as stealing junk from a junkyard and selling it back to that same junk dealer in the morning) since he was twelve years old. Brock actually takes a liking to Varrell, not realizing that he is telling a sworn enemy something very close to what he wants to find out, because Varrell cannily treats Brock's story as the most natural thing in the world, and never hints at how repulsive he finds it.
 In the middle of this interview, Billie walks in and tries to swipe a bottle of "booze." Brock spots her attempt at petty theft and orders her to put the bottle back where she found it, saying that he will be entertaining some company that night and he does not want her to be drunk in front of his guests. As she sullenly leaves, Jim Devery says something that he will soon regret. He asks the barber how much of a bribe he might take for cutting Brock's throat instead of his beard. Brock leaps out of his private barber's chair and shoves Jim Devery to the floor, saying that he does not find anything amusing in that sort of joke. Then he dismisses the barber, the manicurist, and the bootblack, but still asks Paul Varrell to "stick around," saying that Paul is "the only friend I've got in this town."
 Congressman Norval Hedges and his wife pay Brock a visit in his suite that night. Brock tells Hedges that what he wants is simple: he wants to buy, move, and sell scrap iron, and doesn't want to get caught when prices fall. Hedges mutters some high-sounding pabulum about "support for American businessmen," when what he has actually agreed to do is to shape legislation to allow a cartel in scrap iron, an arrangement that is, of course, illegal.
 Billie, however, proves an embarrassment. She doesn't know what the Supreme Court is, and thinks it is some kind of show, like the ones in which she used to appear (which is how Brock met her). She also insists on playing canned music of the type that she used to dance to, and even vocalizing along with it. The Congressman's wife asks Billie whether she plays bridge; she replies that all she knows how to play is gin rummy.
 Brock is actually pained to realize how utterly ignorant she is. Jim suggests that he send her back to his home in Plainfield, New Jersey, but he admits that he is in love with her (if one grants that he understands what love is). Then he conceives of the idea of having someone educate her, and even has a candidate in mind: none other than Paul Varrell.
 Jim Devery doubts seriously whether Paul Varrell will agree to any such arrangement. Harry Brock, however, never met a man he couldn't bribe, and is sure that he can induce Varrell to do the job, if he offers him a high-enough price. As he expects, Varrell agrees, especially when Harry offers him two hundred dollars a week.
 But Varrell has in mind more than merely teaching Billie manners. In fact, he remembers having seen Billie when the Brock party arrived at the hotel, and he is beginning to fall in love with her, though he doesn't quite realize it yet. More to the point, he has already conceived a plan to teach her vocabulary, grammar, and most important of all, civics, as well as manners. In that way, he hopes to gain her confidence and cultivate her as a spy, or at least a source.
 His first meeting with Billie is a little awkward, because Billie is quite prepared to offer herself to him for what she calls "a little action," and is totally unconcerned about whether Harry would be jealous, or even whether he might find out. Varrell has no such intentions, at least not right away, but he does give in to her charms and kiss her on that first meeting, an act that makes a profound and favorable impression on her.
 The next morning, Varrell comes to see her in her suite. She has started to read a newspaper, as he suggested, and has drawn circles around half the words, as he also suggested whenever she came upon a word she did not know. But she is also dressed highly informally and invites him to sit next to her on her bed. He sits in a chair beside the bed instead, and says that he doesn't want to complicate their relationship any more than it already is. To which she replies,
 Nevertheless, he firmly insists on keeping their relationship platonic for the time being. He also begins her civics education by taking her on tours of the Capitol, the Jefferson and Lincoln Memorials, and the National Archives. He also includes such cultural attractions as a night concert at the Watergate Hotel (a name that would become infamous more than thirty years later, but at the time was simply one of the most luxurious hotels in Washington, and nothing more).
 As the weeks pass, Billie acquires a large library, beginning with a heavy dictionary and the works of Thomas Paine and other philosophers whose works informed the Founding Fathers. Varrell has excited her curiosity about American institutions and their origins. Yet at the same time her relationship with Varrell is causing her to lose sleep, and she tells Varrell forthrightly that she is feeling many things that she has never felt before and does not understand.
 Her relationship with Harry Brock, however, comes to the breaking point. She is unhappy with her life, because suddenly the fur coats, the jewelry, and the occasional large quantities of alcoholic beverages are not enough for her anymore. Harry Brock notices her unhappiness and is upset himself, because he doesn't know what to do about it.
 Norval Hedges comes to pay a visit, and to tell Harry that the legislation he seeks will take "a bit more time" to arrange. Harry, in his usual roar, tells Hedges that if he can't do the job, Harry will find someone who will. When Harry walks away, Billie protests to Hedges that neither Harry nor anyone else ought to be able to talk that way to a United States congressman.
 While this is happening, Jim Devery hands her yet more documents to sign, but now, instead of signing them as ordered, she insists that she wants to look them over first, and asks Jim what those documents mean. He at first says that they are a merger, but she soon realizes that those documents represent no mere merger, but a cartel, a concept that Paul has already explained to her and a thing she wants no part of.
 Harry, when he finds out her attitude, is furious. He insists that she sign, and she refuses, a thing she has never done before. In response, he does something that he has never done before: he slaps her across the face, one forehand stroke, and then one backhand stroke. He then forces her to sign, and then orders her to go out and take a walk. After taking time to change her clothes, she does leave the wing, but not before she tells Brock,
 She walks all the way to the Jefferson Memorial, and pays particular attention to the statement by Jefferson that Paul once explained to her:
 Then she calls Paul Varrell and asks him to meet her, which he does. At first she says that she wants to run away. Now Paul encourages her to help him with the project that has been his real object all along: to acquire copies of those cartel documents, documents that can easily send him to prison.
 Harry, back at the hotel, starts talking rashly of forgetting about mere congressmen and trying to bribe a Senator instead. Jim tells Brock flatly that Harry could never offer any Senator a high-enough price.
 While the two are arguing the point, Billie returns, with Paul in tow. Harry "welcomes" Billie back and dismisses Paul. But he does not realize that Billie has already told Paul about the hidden service corridors that connect all the suites, and so all that Paul has to do is gain entry to one of the suites, by walking in through an unguarded door, and then make his way to the service corridor. He does so, and so is able to find the documents.
 This results in a final confrontation among the three, in which Paul announces that he has the documents, and has mailed them to his own address in Washington. Harry at first offers Paul a bribe of $100,000, but Paul refuses. Then Harry locks both hands around Paul's throat, but Jim Devery pries Harry loose and throws him to the floor, yelling at him that he is an accursed fool for thinking that such "strong-arm tactics" can ever work in Washington.
 Now that Harry understands the position he is in, Billie calmly tells him that she will sign all his junkyards back to him, but not all at once. Instead she will relinquish to him one property a year for the foreseeable future—and that if he doesn't "behave," she will tell all she knows to the proper authorities, and he will go to prison. At last she and Paul leave the wing; she announces that she will send for her belongings later. Jim Devery offers Harry a toast to all the "dumb" people who run rings around crooks like Harry and Jim.
 The last seen finds Paul and Billie being pulled over for speeding. The traffic officer asks Paul for his license, and he hands her their marriage license. The officer offers not to cite them for speeding, as a wedding present, but that they ought to be careful, or they'll "never make it."
 "Oh, we'll make it, all right," says Billie. "It's a clear case of predestination."
 "Pre-what?"
 "Look it up," says Billie, and then Paul and Billie drive away.
 Spoilers end here.
 The most important theme is the raising of consciousness through education. Judy Holliday's Billie Dawn is a somewhat more "brassy" version of Eliza Doolittle in George Bernard Shaw's Pygmalion (and, of course, the musical My Fair Lady).
 In addition, William Holden and Broderick Crawford present two opposites that must inevitably clash: the conscientious righter of wrongs against one who does wrong and excuses it by saying that "it's done every day." To which Holden replies:
 Actress Judy Holliday won the Academy Award for Best Performance by an Actress in a Leading Role for her performance as Billie Dawn. Her competition, including Bette Davis (All About Eve), Anne Baxter (also in All About Eve), and Eleanor Parker (Caged), was unusually formidable. Many of her contemporaries protested, saying that Judy Holliday had performed the same role on Broadway and thus had had more time to perfect her performance, but others said that she won her Oscar honestly, on no other strength but her undeniable stage presence.
 The project as a whole also received a nomination for Best Picture of the Year.
 This work was remade in 1993 under the same title and with almost the same story.
 1 Plot
1.1 Arrival
1.2 Interview
1.3 Congressman and cocktails
1.4 An education
1.5 Strains
1.6 The Unraveling
 1.1 Arrival 1.2 Interview 1.3 Congressman and cocktails 1.4 An education 1.5 Strains 1.6 The Unraveling 2 Cast 3 Themes 4 Awards and nominations 5 Allusions to this work by other works  Broderick Crawford as Harry Brock, the junk tycoon.
 Judy Holliday as Emma "Billie" Dawn, his kept woman.
 Howard St. John as Jim Devery, his attorney and "general counsel."
 Frank Otto as Eddie, his odd-jobber.  Judy Holliday as Emma "Billie" Dawn, his kept woman.  Howard St. John as Jim Devery, his attorney and "general counsel."  Frank Otto as Eddie, his odd-jobber.  William Holden as Paul Varrell, journalist.  Larry Oliver as Representative Norval Hedges.
 Barbara Brown as Congressman Hedges' wife.  Barbara Brown as Congressman Hedges' wife. Movies Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 24 June 2010, at 07:24. This page has been accessed 2,319 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Allmovie profile IMDb profile Born Yesterday Spoiler warning   1950 movie poster
  George Cukor
  S. Sylvan Simon
  Garson Kanin (stage play)Albert Mannheimer (screen adaptation)
  Judy HollidayBroderick CrawfordWilliam Holden
  Friedrich Hollaender
  Joseph Walker
  Charles Nelson
  Columbia Pictures Corporation
  February 1951
  104 min
  United States
  English
  “
  I ought to take a pencil and draw a circle around you.
  ”
  “
  Hey, would you do me a favor? Drop dead.
  ”
  “
  I have sworn, upon the altar of God, eternal hostility to any form of tyranny over the mind of man.
  ”
  “
  For all I know, there's an undiscovered murder committed every day. What does that prove?
  ”
 Born Yesterday Contents Plot Cast Themes Awards and nominations Allusions to this work by other works Navigation menu Arrival Interview Congressman and cocktails An education Strains The Unraveling Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
