OnePlace.com is a website owned and operated by Salem Web Network and headquartered in Richmond, Virginia. The site claims to be the leading provider of online streaming for Christian ministries. In addition to online radio streams for more than 200 of Salem's radio broadcasts, the site is home to more than 150 ministry broadcasts and includes a 30-day archive, on-demand audio, podcasts, and MP3 downloads.[1]
 OnePlace.com receives an average of 13 million page views and 1.3 million unique users per month.[2]
 Salem Web Network acquired OnePlace.com in 1998. Charles Stanley, Chuck Swindoll, and Focus on the Family were some of the first ministries to broadcast on the site.[3] In October 1999, Salem Web Network announced that they would begin streaming radio broadcasts on OnePlace.com which would give users the ability to listen to their favorite Christian broadcasts from anywhere.[4]
 On September 20, 2005, Salem Web Network announced the launch of podcasting services.[5]
 On February 14, 2006, OnePlace.com launched a complete redesign and new content platform. The redesign featured a simplified navigation and organization to ministry listings as well as topical programming such as family, worldview, and kids.[6]
 OnePlace.com was named Best Broadcast Website and received the 2007 NRB Media Award for Internet - Best Broadcast Website by the National Religious Broadcasters Association on February 27, 2007. The award recognizes excellence in design and content, creativity in presentation, and effectiveness in fulfilling its mission.[7]
 OnePlace.com allows users to listen to the latest broadcasts of leading ministry voices as well as browse the 30-day archive.
Some of the ministries broadcasting on OnePlace.com are;
 
 Salem Radio Networks Faith Talk radio programs stream live on the site. Their "Faith Talk Web Radio" features radio broadcasts from Chuck Swindoll, Kay Arthur, John Piper, John F. MacArthur, Adrian Rogers, Greg Laurie, and more.
 The site has interactive Bible Study Tools, featuring a large amount of Biblical content for personal Bible study. The site features 29 Bible translations (7 contain the Apocryphal books), 12 commentaries, 4 concordances, 5 dictionaries, 2 encyclopedias, 2 lexicons (1 Hebrew, 1 Greek), Biblical maps, sermon helps, sermon illustrations, and more.[8]
 Pastors, authors, and speakers who broadcast on OnePlace also provide essay and commentary content that address theological questions, Christian living issues, Christian foundations, and more.
 OnePlace.com offers free devotionals that you can read on the site or receive by email.
 Salem Web Network announced the addition of podcasting services on September 20, 2005. During that year, several ministries had been quick to adopt the new technology including Focus on the Family, Winning Walk with Dr. Ed Young, Power Point Ministries with Jack Graham, and Family Life Today with Dennis Rainey. These organizations saw an exponential increase in the number of daily programs downloaded during the initial testing phase. Oneplace.com ministries were on pace for more than 125,000 program downloads in the month of September.[9]
 Today, more than 150 ministry podcast subscriptions are available to download.
 
 Salem Web Network
 1 History 2 Awards 3 Content 4 Faith talk radio 5 Bible study tools 6 Ministry articles 7 Devotionals 8 Podcasts and MP3 downloads 9 References 10 See also Beth Moore R.C. Sproul Focus on the Family Adrian Rogers D. James Kennedy Jack Graham Left Behind J. Vernon McGee Ed Young Kay Arthur John F. MacArthur David Jeremiah Hank Hanegraaff Alistair Begg Kirk Cameron Tony Perkins (politician) Bayless Conley Dr. Michael Youssef Ken Ham Tony Evans Dr. James MacDonald ↑ Oneplace.com, "About Us" Retrieved 2007-10-15
 ↑ Business Wire, "OnePlace.com Named Best Broadcast Website by National Religious Broadcasters Association", Feb. 12, 2007 Retrieved 2007-10-15.
 ↑ "The dot.com of Christian Broadcasting" by Larry Amon, February 2004 Retrieved 2007-10-15.
 ↑ "Salem CC, "Salem Web Network Unveils New Podcasting Initiative; OnePlace.com, Largest Christian Content Provider Offers New Service", Sep. 20, 2005 Retrieved 2007-10-15.
 ↑ "Salem CC, "Salem Web Network Unveils New Podcasting Initiative; OnePlace.com, Largest Christian Content Provider Offers New Service", Sep. 20, 2005 Retrieved 2007-10-15.
 ↑ Business Wire, "OnePlace.com Named Best Broadcast Website by National Religious Broadcasters Association", Feb. 12, 2007 Retrieved 2007-10-15.
 ↑ "Salem CC, "Salem Web Network Unveils New Podcasting Initiative; OnePlace.com, Largest Christian Content Provider Offers New Service", Sep. 20, 2005 Retrieved 2007-10-15.
 Christian Organizations Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 13 July 2016, at 10:58. This page has been accessed 2,319 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Oneplace.com Contents History Awards Content Faith talk radio Bible study tools Ministry articles Devotionals Podcasts and MP3 downloads References See also Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
