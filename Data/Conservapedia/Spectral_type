In Genesis, the stars were made in the fourth day,[1] and their number is compared to the number of descendants of Abraham.[2]
 The Bible implies that the number of stars is virtually countless,[3] but for many years this was not accepted. Hipparchus in 128 B.C. stated there were 1,026 stars in the sky. Kepler in 1600 A.D. did his own count and found the number to be 1,005. Today, thanks to telescopes (especially the Hubble Telescope) showing many stars previously too dim to be seen, we are now aware of some 70,000,000,000,000,000,000,000,000 (7×1025) stars.[4]
 The oldest method of measuring the distance from our solar system to a distant star is the parallax method. To use this method, astronomers measure the right ascension on the sky of the star at two times of the year, half a year apart. The two measurements will differ by a small angle with respect to the most distant stars in that region of the sky. Exactly half this angle is the parallax angle, having symbol p. This is the angle that the star makes with the sun and the position of the earth at a right angle with that star.[5] The distance s of the star, in astronomical units (AU), is:
 In the range of the very small angles typically encountered, the cotangent of the angle measure (in radians) is very nearly equal to the reciprocal, and thus:
 where p is measured in seconds of arc.
 The cotangent of one second (1/3600 of a degree) of arc is approximately 206,264.81. No parallax angle for any star will be larger than one second. Therefore, astronomers initially defined a unit of stellar distance, the parsec (symbol pc), from this relationship. One parsec is the distance corresponding to a parallax angle of one second of arc. Hence:
 However, the error of measurement of parallax angle is 0.005 arc seconds, and beyond a distance of 100 parsecs, this error becomes significant. 700 stars are near enough to measure their distances directly by using parallax.[5] To measure distances further out than this, astronomers typically use absolute and relative magnitudes, or they apply Hubble's Law to the star's estimated redshift.
 The most common system for describing the position of a star in the sky is the equatorial system. This system uses two coordinates:
 All stars move, but the most distant stars are considered "fixed" because their motion would be undetectable. The proper motion (symbol m) of any star is the angular velocity of its position across the sky. This describes the motion at right angles to the line of sight of the observer. To convert this to actual tangential velocity, multiply the tangent of this angular velocity by the star's distance.
 The motion in line of sight, or radial velocity, is currently determined from spectral shift.
 The visual magnitude system is defined as follows: a star of any given magnitude is about 2.512 times as bright as is a star of the next magnitude. Hipparchus devised the magnitude system, and Ptolemy refined it further. By convention, an arbitrary sample of the twenty brightest stars that they could observe were assigned to the first magnitude, and the stars that they could barely observe were assigned to the sixth. Sixth-magnitude stars are actually 100 times less bright than first-magnitude stars. Magnitude levels between these extremes are assigned on a logarithmic scale. Thus, given two stars of brightness l1 and l2, their magnitude difference (V2 - V1) relates to their respective brightnesses in this way:[8]
 The absolute magnitude of any star is the visual magnitude that it would have if it were ten parsecs distant. To convert apparent magnitude V to actual magnitude M, use this formula:
 where s0 is the standard distance. This distance is ten parsecs, or about 2,062,650 AU.
 Brightness declines with the square of distance, and squares correspond to doubling of logarithms. One must then multiply that result by 2.5 to stay within the magnitude scale.
 The color of a star is objectively quantifiable. To determine color, astronomers view the star through a variety of colored filters and compute color indices as the differences in apparent magnitudes through the various filters. Stellar colors vary, in order from the coolest to the hottest, from red to yellow to white to blue-white to blue or violet. This is the same gamut of colors that a black body shows as its temperature rises.
 In addition, each star has a unique spectrum, which depends on the gases and other elements that it contains, and their distribution. A spectrum can serve two purposes:
 To accomplish the latter, astronomers note the placement of various lines in the spectrum and then determine the star's likely constituent elements from the spacing of those lines. Lines that are out of place are shifted, either toward the blue or toward the red. Nearly all stellar spectra are shifted toward the red; this redshift indicates a recession, either of the star or of the part of space where the star resides.[9]
 In the late nineteenth century, astronomers at the Harvard University observatory developed the first classification scheme for stellar spectra that would become known as the Harvard spectral classification. In 1924, Annie Jump Cannon[10] refined the classification from the original A-Q gamut to the familiar "OBAFGKM" gamut. Astronomers have since added classes to this range at the high end and the low.[11][12]
 The classic Harvard spectral classes are O, B, A, F, G, K, and M. Each of these has ten subclasses, varying from 0 to 9 in order of decreasing stellar temperature. Thus, for example, the next class after an F9 star is a G0 star. Recently astronomers recognized one class of stars hotter than the O stars (the very hot Wolf-Rayet stars) and three classes of stars (the N, R, and S stars) cooler than the M stars. (Some astronomers include the N and R stars in one class, the C stars, for the carbon compounds that their spectra exhibit).  There is an additional spectral class for the smallest and dimmest stars (Class L), that still fuse hydrogen, although warmer brown dwarfs also fall into this class (but referred to as L dwarfs instead of L stars). Cooler still methane dwarfs are classified as T dwarfs.[13]  A proposed spectral class Y has been suggested for the coolest brown dwarfs, which also have a different spectra from T class dwarfs.[14]
 In the early twentieth century, astronomers Ejnar Hertzsprung and Henry Norris Russell prepared the first plot of stellar temperature as a function of luminosity, or brightness. Other astronomers have since prepared versions of the diagram showing absolute magnitude as a function of color. This diagram shows a "main sequence" of stars for which brightness declines as temperature increases, but also shows a "white dwarf" population of very hot but dim stars, and the population of giants and supergiants that are far brighter than their temperatures would indicate.[15]
 In addition to the spectral type, astronomers today add a luminosity class, which varies from 0 to VII in order of decreasing brightness. This is known as the Yerkes spectral classification.  This classification was first developed by astronomers William Wilson Morgan, Phillip C. Keenan and Edith Kellman at the Yerkes Observatory in 1943.[16]  Adding a luminosity classification added a second dimension to the single dimensional Harvard spectral sequence.  Today the two classifications of temperature and luminosity is used to give the spectral sequence for a star.[17]  For example, the sun's spectral type is G2 and its luminosity class is V (five).
 Some stars vary in brightness and are known as variable stars. The star Algol in the  constellation of Perseus can drop from its normal magnitude of 2.3 to magnitude 3.5. This is now known to be caused by a dim companion star orbiting Algol, which occasionally passes between Algol and the Earth, blocking some of the light. Other variable stars vary in brightness due to actual variations in the luminosity of the star itself. The time taken from one maximum brightness to the next one is called the period. The most famous of the variable stars is delta Cepheus, the first-found member of the Cepheid group of variable stars. In 1908 Henrietta Swan Leavitt noticed that the variable stars in the Magellanic Clouds (two nearby galaxies in the Local Group) had a relationship between their period and their apparent brightness. At that time galaxies outside our own (the Milky Way) had been discovered, but it was not possible to measure the distances to them. It was soon realized that the variable stars in the Magellanic Cloud were of the Cepheid type. Since Cepheid variables also occur in our galaxy it was possible measure their distances and thus convert (using the inverse square law) Leavitt's relationship between apparent brightness and period to one of actual brightness and period. Once this formula was discovered, it became possible to apply to Cepheids of unknown distance. By observing their periods, their actual brightness can be calculated and, by the inverse square law, their distance. Through observations of Cepheids in globular clusters (compact bunches of stars) in our galaxy it was shown that our galaxy is about 300,000 light-years in diameter.
 In 1938 and 1989, two physicists, Carl F. von Weizsäcker[18] and Hans Bethe[19] independently proposed a nuclear fusion process, the Carbon-Nitrogen-Oxygen cycle, by which stars more massive than the sun produce energy. In this process, stars convert hydrogen to helium using carbon, nitrogen, and oxygen as catalysts. The reaction also produces two positrons and two electron neutrinos.[20]
 The equations for the cycle are as follows:
 The last reaction reproduces the  nucleus that the first reaction consumes. The end result of this process is:
 Rarely, this cycle branches into a somewhat different cycle involving fluorine, and that second cycle is thought to branch again in some of the most massive stars.
 Christian scientists assert that materialistic explanations of the origin of stars are errant and contra-evidence and reports of stars forming are invalid.[21][22][23][24][25] In addition, creationists cite the secular scientific literature in order to make the case that materialist explanations of star formation are inadequate:
 “We don’t understand how a single star forms, yet we want to understand how 10 billion stars form.” Carlos Frenk, as quoted by Robert Irion, “Surveys Scour the Cosmic Deep,” Science, Vol. 303, 19 March 2004, p. 1750.[26]
 “Nobody really understands how star formation proceeds. It’s really remarkable.” Rogier A. Windhorst, as quoted by Corey S. Powell, “A Matter of Timing,” Scientific American, Vol. 267, October 1992, p. 30.[27]
 A star's habitable zone is the region in which a terrestrial planet of the right size could have a surface temperature that might allow for liquid water and potentially life.[28]
 For example, if a star much similar our Sun has a lifetime of one million years and temperature of 6094K. Its habitable zone lies within 1.02AU and 1.49AU.[29]
 The Observer's Book of Astronomy, by Patrick Moore. Published by Frederick Warne and Co. 1967.
 The Cosmological Distance Ladder, by Michael Rowan-Robinson. Published by Freeman. 1985.
 1 Measuring stellar positions
1.1 Distances
1.2 Positions in sky
1.3 Proper motion
 1.1 Distances 1.2 Positions in sky 1.3 Proper motion 2 Measuring stellar magnitudes 3 Stellar colors and spectra
3.1 Spectral Type
3.2 Luminosity Class
 3.1 Spectral Type 3.2 Luminosity Class 4 Variable stars 5 Energy production 6 Origins 7 Habitable zone 8 See also 9 References 10 Other References  Right ascension on the sky, or the number of hours required for the earth to rotate before an observer can see the star at its highest point in the sky. The zero for right ascension is midnight on the day of the vernal equinox.[6]  Declination, or the north-south angle between the star and the celestial equator.[7]  It can serve as a unique signature for the star, to distinguish it from other stars.  It can provide information on the star's radial velocity vis-à-vis the earth. 61 Virginis ↑ Gen 1:14
 ↑ Gen 15:5; an earlier count of the number of descendants of Abraham was the number of grains of dust of the Earth (Gen 13:16)
 ↑ Jeremiah 33:22; similarly to Genesis, the number of descendants of David is compared to the number of stars and the number of grains of sand
 ↑ Star survey reaches 70 sextillion. Retrieved on 2019-01-24.
 ↑ 5.0 5.1 "Star: Determining stellar distances." Encyclopædia Britannica. 2008. Encyclopædia Britannica Online. Accessed 24 Jan. 2019
 ↑ Weisstein, Eric W. "Right Ascension." Eric Weisstein's World of Astronomy, 2007. Accessed January 24, 2019.
 ↑ Weisstein, Eric W. "Declination." Eric Weisstein's World of Astronomy, 2007. Accessed January 24, 2019.
 ↑ Haworth, David. "Star Magnitudes." Observational Astronomy, 2003. Accessed January 24, 2019.
 ↑ Some cosmological models call for an expansion of space itself, not merely the matter in it. According to these models, a redshifted star is in a part of space that was still expanding as the incident light was generated.
 ↑ "Life Cycles of Stars." Goddard Space Flight Center, November 21, 2002. Accessed January 24, 2019.
 ↑ "Harvard Spectral Classification." Study Astronomy Online at Swinburne University. Accessed January 24, 2019.
 ↑ Irizarry, David. "The Secrets of the Harvard Classification Revealed." The Webfooted Astronomer, Seattle Astronomical Society, February 2000. Accessed January 24, 2019.
 ↑ Kirkpatrick, J. (2008). Outstanding Issues in Our Understanding of L, T, and Y Dwarfs. In 14th Cambridge Workshop on Cool Stars, Stellar Systems, and the Sun. Astronomical Society of the Pacific Conference Series, 384, p.85. Bibcode:2008ASPC..384...85K arXiv:0704.1522 [astro-ph]
 ↑ Deacon, N. and Hambly, N. (2006). The possiblity of detection of ultracool dwarfs with the UKIRT Infrared Deep Sky Survey. Monthly Notices of the Royal Astronomical Society, 371(4), pp.1722-1730. Bibcode:2006MNRAS.371.1722D arXiv:astro-ph/0607305
 ↑ "Hertzsprung-Russell Diagram." Study Astronomy Online at Swinburne University. Accessed January 24, 2019.
 ↑ Morgan, William Wilson; Keenan, Philip Childs; Kellman, Edith (1943), "An atlas of stellar spectra, with an outline of spectral classification", Chicago, Ill., The University of Chicago press
 ↑ Morgan, W. and Keenan, P. (1973). Spectral Classification. Annual Review of Astronomy and Astrophysics, 11(1), pp.29-50. Bibcode:1973ARA&A..11...29M
 ↑ Von Weizsäcker, Carl F. Physik. Zeitsch. 39:633, 1938.
 ↑ Bethe, Hans A. "Energy Production in Stars." Physics Review 55(5):434-456, 1939. doi:10.1103/PhysRev.55.434 Accessed january 24, 2019.
 ↑ Krane, Kenneth S. Introductory Nuclear Physics. New York: John Wiley and Sons, 1988, p. 537. ISBN 9780471805533
 ↑ New Stars, New Planets?. Retrieved on 2019-01-24.
 ↑ Were Stars Created?. Retrieved on 2019-01-24.
 ↑ Fast Binaries. Retrieved on 2019-01-24.
 ↑ Astronomy and the Bible. Retrieved on 2019-01-24.
 ↑ Letters to the Editor: December 1996. Retrieved on 2019-01-24.
 ↑ Surveys Scour the Cosmic Deep. Retrieved on 2019-01-24.
 ↑ In the beginning, Hydrogen. Retrieved on 2019-01-24.
 ↑ Bennet, Jeffrey, et al. "Life Around Stars." The Essential Cosmic Perspective. 4th ed. San Francisco: Pearson Education, Inc., 2008. 508-13.
 ↑ "Exploring the Habitable Zone and Central Star", CADRE design Pty. Ltd.
 Astronomy Stars Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 9 April 2019, at 15:54. This page has been accessed 31,889 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Stars Harvard spectral classification Yerkes spectral classification period proton-proton chain reaction Carbon-Nitrogen-Oxygen cycle                          W
  106,000 K
  Violet
  Ionized helium, carbon, oxygen, nitrogen
  Wolf-Rayet stars. Additional subclasses include WC (overabundant carbon and oxygen) and WN (overabundant nitrogen)
  O
  30,000 K
  Blue
  Ionized Helium, nitrogen, oxygen
  Weak Balmer lines (hydrogen) at higher subclasses.
  B
  13,000 K to 20,000 K
  Blue
  Neutral helium; ionized silicon, oxygen and magnesium.
  Hydrogen (Balmer lines) appear in strength
  A
  75,00 to 10,000 K
  Blue-white
  Hydrogen, calcium, helium
  Balmer lines dominant. K lines (calcium) now appearing.
  F
  7,000K to 9,000K
  White-yellow
  Hydrogen, calcium, iron, manganese, sodium
  Balmer lines weakening. K lines stronger.
  G
  5,200 to 6,000K
  Yellow
  Calcium, hydrogen, other metals
  Balmer lines weaker still. K lines dominant. Metals now appearing. Contains the sun.
  K
  4000K to 5100K
  Orange
  Calcium, neutral metals, titanium oxide
 
  M
  3000K
  Red
  Titanium oxide, iron iodide
  Strong molecular bands
  N,R
  2300K to 2600K
  Red
  Carbon compounds
 
  S
  2300K to 2600K
  Red
  Hydrogen, zirconium oxide
 
  0 - 0Ia - Ia0
  hypergiants
  Ia - Iab - Ib
  supergiants
  IIa - IIab - IIb
  bright giants
  IIIa - IIIab - IIIb
  giants
  IVa - IVab - IVb
  subgiants
  Va - Vab - Vb
  main sequence stars (dwarfs)
  VI
  subdwarfs
  VII
  white dwarfs
 v • d • e
StarGeneralHertzsprung–Russell diagram · Open cluster · Star chart · Star magnitude · Starlight problem · SupernovaHypergiantsCygnus OB2-12 · Eta Carinae · LBV 1806-20 · MWC 314 · P Cygni · Pistol Star · Rho Cassiopeiae · RW Cephei · S Doradus · S Persei · VY Canis Majoris · Zeta Scorpii · 6 CassiopeiaeSupergiantsAntares · Betelgeuse · Gamma Orionis · KW Sagittarii · KY Cygni · Mu Cephei · Rigel · VV Cephei · V354 Cephei · Wolf-Rayet starBright giant starsAlpha Herculis · Alpha Hydrae · Beta Capricorni · Epsilon Canis Majoris · Gamma Ursae Minoris · Theta ScorpiiGiant starsAlcyone · Mira · Pollux · Sigma Octantis · ThubanSubgiant starsProcyonMain sequence starsAlpha Coronae Borealis · Alpha Mensae · Beta Comae Berenices · Beta Pictoris · Eta Arietis · Gamma Virginis · Pi Andromedae · Phi Orionis · Sun · Zeta Puppis · 70 OphiuchiBinary starsAcrux · Alpha Centauri · Luyten 726-8 · Sirius  General Hertzsprung–Russell diagram · Open cluster · Star chart · Star magnitude · Starlight problem · Supernova  Hypergiants Cygnus OB2-12 · Eta Carinae · LBV 1806-20 · MWC 314 · P Cygni · Pistol Star · Rho Cassiopeiae · RW Cephei · S Doradus · S Persei · VY Canis Majoris · Zeta Scorpii · 6 Cassiopeiae  Supergiants Antares · Betelgeuse · Gamma Orionis · KW Sagittarii · KY Cygni · Mu Cephei · Rigel · VV Cephei · V354 Cephei · Wolf-Rayet star  Bright giant stars Alpha Herculis · Alpha Hydrae · Beta Capricorni · Epsilon Canis Majoris · Gamma Ursae Minoris · Theta Scorpii  Giant stars Alcyone · Mira · Pollux · Sigma Octantis · Thuban  Subgiant stars Procyon  Main sequence stars Alpha Coronae Borealis · Alpha Mensae · Beta Comae Berenices · Beta Pictoris · Eta Arietis · Gamma Virginis · Pi Andromedae · Phi Orionis · Sun · Zeta Puppis · 70 Ophiuchi  Binary stars Acrux · Alpha Centauri · Luyten 726-8 · Sirius Star Contents Measuring stellar positions Measuring stellar magnitudes Stellar colors and spectra Variable stars Energy production Origins Habitable zone See also References Other References Navigation menu Distances Positions in sky Proper motion Spectral Type Luminosity Class Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
