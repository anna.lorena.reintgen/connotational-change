(Louisiana journalist and publisher)
 Resident of Shreveport, Louisiana
 Children:
Robin Lynette Pierce
Perry Loyce Pierce
Jeremy Winter Pierce
 Sarah Rachel Hudson-Pierce (born February 22, 1948) is an author of inspirational books, a publisher, a journalist, and a former cable television host in Shreveport in Caddo Parish in northwestern Louisiana.
 She was born to Roy Earnest Hudson (1895–1958) and the former Marcella May Morris (1906–1986) near Sulphur Springs in Benton County in northwestern Arkansas. Her girlhood home, still in existence, was a house constructed in the 1840s.
 Sarah's mother, Marcella, was born in an underground American Indian sod dwelling near tiny Fairvalley in Woods County in north central Oklahoma. Marcella's grandparents, William Henry Morris (1833-1901) and the former Mary J. Barter (born 1838), had been among the pioneers who staked out 160 acres in the Oklahoma Land Rush. W. H. Morris was born Monmouth County, New Jersey. Prior to the American Civil War, he came to Linn County, Iowa, where in 1855, he married the 16-year-old Mary Barter. He then attended medical school in Keokuk, Iowa, having completed his instruction in 1864. The couple later moved to Oklahoma; three years after his death, Mary B. Morris was still living in Alva in Woods County, Oklahoma.
 Hudson-Pierce traces her ancestry to Lewis Morris (1671-1746), the British governor of New Jersey from 1738 until his death. Previously Lewis Morris had been royal governor of the New York colony, which included New Jersey prior to the separation in 1738. This Lewis Morris' grandson, also Lewis Morris (1726-1798), was a landowner and developer who in 1776 signed the Declaration of Independence as a representative at the Second Continental Congress for New York. Lewis Morris was a half-brother of Gouverneur Morris (1752-1816), the New York City native who was editor-in-chief of the United States Constitution and the author of its preamble.
 Before her third birthday, Marcella Morris was playing on the soft dirt roof of her dwelling. She slipped, fell through the roof, sustained brain damage, and lapsed into a coma. Marcella's mother, meanwhile, was dying of typhoid fever when this accident occurred. Marcella's six-year-old brother, Jimmy, had died of the fever some six weeks earlier. Marcella's father left his two daughters in the care of a kindly neighbor, Clara Knox, who became Marcella's foster mother. In 1914, Marcella's father remarried, and Marcella, at the age of eight, left the security of life with Clara. Marcella did not marry until she was in her late thirties and then only after her father, Sarah's grandfather, had died. Having lived into her nineties, Clara thereafter told Sarah of the tragic circumstances of Marcella's life.
 Roy Hudson died, and Sarah lived with Marcella for four years. Marcella subsisted by taking odd jobs but became unable to care for Sarah. Hence, at the age of fourteen, Sarah began living in the foster home of the late Cullen and Martha Adair of Grove in Delaware County in  northeastern Oklahoma. Sarah was later placed in an orphanage, the Turley Children's Home, now known as Hope Harbor, in Claremore near Tulsa. She resided at Turley from 1962 to 1966.
 Hudson-Pierce graduated in 1966 from McLain High School in Tulsa. Marcella lived for another three decades, having died in the nursing home in Grove, Oklahoma. While she was in the eleventh grade at McLain, Sarah turned to creative writing and public speaking. From 1966 to 1967, she attended the conservative Church of Christ-affiliated Harding University (then College) in Searcy in White County, north of the capital city of Little Rock.[1]
 Hudson-Pierce has written five books, three being free verse poetry:
 Friendship Is A Journey (1987)
 The Warming of Winter (1989)
 To Soar Again! (1994)
 The other two are inspirational pieces:
 Southern Vignettes (1995)[2]
 Turning Points (1996), semi-autobiographical.
 One of Hudson-Pierce's articles, "The Old Steamer Trunk", was published in Guideposts magazine in January 1998. Richard "Dick" Schneider, a senior staff editor at Guideposts, said that Hudson-Pierce's selection was particularly poignant and fitting for the publication. Stories about the article were written in numerous newspapers.[3][4]
 From 1995 to 2008, Hudson-Pierce hosted her own television program in Shreveport through the former Time Warner Company. She interviewed artists, authors, political figures, or anyone else of interest. She writes an occasional column for the The Shreveport Times, one of which is on the difficulties of moving.[5] Another is her autobiographical recollection of Christmas 1957.[6] She has also written for The Bossier Press-Tribune newspaper in Bossier City, located across the Red River from Shreveport.
 In 2002, Hudson-Pierce launched, on a shoestring budget, Ritz Publications, named for her maternal great-randfather Nicholas Ritz, who came to the United States in 1851 from Bern, Switzerland. Her first selection was the rejuvenation of an out-of-print book titled Poems by Julia Pleasants Creswell, the great-grandmother of the late Shreveport Mayor James Creswell Gardner. Ritz also released two more of Julia Creswell's books: Athalie and Other Poems and Callamura.[7] Hudson-Pierce published two volumes of Jim Gardner's memoirs entitled Jim Gardner and Shreveport. She also compiled in 2010 a tribute book to Gardner.[8]
 In 2002, Tanya Brasher Alexander, who resides near Shreveport, wrote a  biographical poem of Hudson-Pierce's life.[9]
 Successful Ritz books have included: Stone Justice (2001) by Debi King McMartin and Lyn Morgan, which details the tragic life of Toni Jo Henry of Shreveport, who on November 28, 1942, became the only woman ever to have been executed in Louisiana's electric chair. Henry's story has been compared to that of Karla Faye Tucker in Texas, executed in 1998. The motion picture The Pardon, filmed in Shreveport, is adapted from Stone Justice.[10]
 Still another Ritz publication is Will Somebody Call the Coroner?, an autobiography of the 56-year Caddo Parish Coroner Dr. Willis P. Butler, with the preface by the late Shreveport historian Eric Brock.[11][12] Ritz Publications also offers Sarah Dorsey's Recollections of Henry Watkins Allen, an updated 2004 study of Louisiana's Confederate governor. Dorsey owned the Biloxi estate, Beauvoir, and was a benefactor of Confederate President Jefferson Davis, who lived for a time rent-free at Beauvoir.[13]
 Another Ritz release is Tinkerbelle, the story of Robert Manry, a copy editor for The Plain Dealer in Cleveland, Ohio, who in 1965 solo-navigated the smallest sailboat ever across the Atlantic. Ritz offers a rare book, Our Baby's History (1898), by the American artist Frances Brundage, particularly known for her depictions of wide-eyed Victorian children.
 In 2009, Ritz published Jerry Wray: Pioneer Artist Of The South by Shreveport artist Jerry Wray. That same year, Ritz published Mama's Boys, a novel by Matt Whitehead, a journalist and a former child protective services worker. Another 2009 publication is Investing Without A Net a collection of short vignettes by Shreveport author Larry LaBorde, the owner of a silver trading company and a family-owned oil business. 
 In 2010, Ritz published an autobiography of Virginia Shehee, the businesswoman from Shreveport who served in the Louisiana State Senate from 1976 to 1980. The book has a coffee-table format with an introduction by former Mayor James Gardner, one of Shehee's classmates at Alexander Elementary School and C. E. Byrd High School in Shreveport. The book is titled Virginia Kilpatrick Shehee: First Lady of Shreveport.[14]
 In 2012, Ritz published the autobiography of George Dement, the late mayor Bossier City, entitled George Dement: I will, If you will, Saith the Lord. The introduction is written by former Governor Edwin Edwards.[15]
 Ritz Publications is the publicist for the inspirational book, Valerie, Jasmine Morelock-Field's biography of her daughter, Valerie Morelock, who was murdered in 1973 on the Louisiana State University campus in Baton Rouge. Morelock-Field turns a tragedy into a story of hope by placing all of her trust in God. She was interviewed in 2012 by Donna LaFleur on Louisiana Public Broadcasting's "Authors in Shreveport" feature at the Louisiana State Exhibit Building Museum.[16] Jasmine Morelock-Field was also interviewed on Rick Rowe's Promise of Hope by KTBS-TV, the American Broadcasting Company outlet in Shreveport.[17]
 Ritz is the publisher for Home to Holly Grove: Cherishing Our Rich Heritage, by Frances Swayzer Conley,[18] an English professor at Bossier Parish Community College. The book is a pictorial family history of the Holly Grove Colored Baptist Church in Wisner in Franklin Parish in northeastern Louisiana. The church was established after the Civil War by one of Swayzer's ancestors.[19]
 Another book available through Ritz is Gardening to Attract Butterflies: The Beauty and the Beast by Loice Kendrick-Lacy of Haynesville in northern Claiborne Parish, Louisiana. The book begins with Kendrick-Lacy as a young girl introduced to butterflies by her grandmother.[20]
 Ritz is also the publicist for James Robertson's biography of his brother, Phil Robertson, from the Arts and Entertainment Network former television series, Duck Dynasty. The book is entitled The Legend Of The Duck Commander. In 2012, James Robertson was interviewed on LPB's "Authors in Shreveport" feature at the Louisiana State Exhibit Museum.[21]
 In 2013, Ritz published the World War II memoir, A Time to Remember, by Ken Cochran.[22]
 Sarah left college in 1967 to marry Charles Edwin Pierce (born 1941), a minister in the Church of Christ, originally from Falcon in Nevada County near Magnolia in south Arkansas. The couple has three children: Robin Lynette Pierce (born 1969) of Shreveport, Perry Loyce Pierce (born 1970) of Boston, Massachusetts, and Jeremy Winter Pierce (born 1976) of Searcy, Arkansas. There are two grandchildren. Hudson-Pierce has an older sister, Alice H. Roberts of Bastrop, Texas.
 Quoting M. Scott Peck, psychiatrist and author of The Road Less Traveled, Hudson-Pierce says that "Life is difficult. ... It is in the attaining of goals that we blossom and grow. ... I know that God is the source of my strength and that without Him, I am nothing. ..."[23]
 1 Early years 2 Author 3 Ritz Publications 4 Personal life 5 References ↑ "Dedication to writing brings success", Harding College Alumni News, Spring 1998.
 ↑ Lenora Nazworth, "Women's Days Marked with Celebration and Challenge", The Shreveport Times, March 8, 2002.
 ↑ Dave Morris, Sr., Review of "The Old Steamer Trunk", Cedar Rapids Gazette, Cedar Rapids, Iowa, March 1998.
 ↑ Tim Morris, Review of "The Old Steamer Trunk" in Guideposts, Morganville, New Jersey News Transcript, March 27, 1998.
 ↑ Sarah Hudson-Pierce, "Moves are a physical hassle, emotional drain," The Shreveport Times, May 26, 2007
 ↑ Sarah Hudson Pierce, "My greatest Christmas gift," The  Shreveport Times, December 23, 2010.
 ↑ Callamura. ritzpublications.com. Retrieved on February 9, 2011.
 ↑ James Creswell Gardner: First Citizen of Shreveport (Shreveport: Ritz Publications, 2010), 250pp., ISBN=1-886032-11-4.
 ↑ Sarah Hudson-Pierce. sarahhudsonpierce.net. Retrieved on February 4, 2011.
 ↑ Alexandyr Kent, "'The Pardon', a locally produced feature, begins shooting May 15", The Shreveport Times, April 28, 2007.
 ↑ Eric Brock, Shreveport historian, The Forum magazine, November 10, 2004
 ↑ Eric John Brock obituary, The Shreveport Times, December 3, 2011
 ↑ Beauvoir. beauvoir.org. Retrieved on February 4, 2011.
 ↑ Virginia Shehee, Virginia Kilpatrick Shehee: First Lady of Shreveport (Shreveport: Ritz Publications, 2010), 250pp, ISBN: 1886032106.
 ↑ Former Bossier City Mayor holds book signing, July 7, 2011. KTBS-TV (ABC in Shreveport). Retrieved on February 3, 2013.
 ↑ A Promise of Hope. KTBS-TV. Retrieved on January 6, 2013.
 ↑ Interview of Frances Conley by Gary Calligas. thebestoftimesnews.com. Retrieved on January 27, 2013.
 ↑ Book signing at LA State Museum on July 7. KSLA-TV (CBS in Shreveport). Retrieved on January 26, 2013.
 ↑ Loice Kendrick-Lacy. media.lpb.org. Retrieved on January 26, 2013.
 ↑ James Robertson. Louisiana Public Broadcasting. Retrieved on January 26, 2013.
 ↑ A Time to Remember by Ken Cochran. KTBS-TV. Retrieved on April 24, 2013.
 ↑ M. Scott Peck (1978). The Road Less Traveled: New Psychology of Love, Traditional Values and Spiritual Growth. Simon & Schuster. Retrieved on February 9, 2011.
 Arkansas Oklahoma Louisiana People Publishers Journalists American Authors Christians Women Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 29 August 2019, at 05:38. This page has been accessed 2,892 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Sarah Rachel Hudson-Pierce Political party  Born Spouse Religion Sarah Rachel Hudson-Pierce 
  Political party 
  Independent
 
  Born
  February 22, 1948 Benton County, Arkansas
Resident of Shreveport, Louisiana
 Spouse
  Charles Edwin Pierce (divorced)
Children:
Robin Lynette Pierce
Perry Loyce Pierce
Jeremy Winter Pierce
 Religion
  Church of Christ
 Sarah Hudson-Pierce Contents Early years Author Ritz Publications Personal life References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
