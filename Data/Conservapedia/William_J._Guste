William J. Guste, III​
Bernard R. "Randy" Guste​
Marie Louise Guste Nix, Sr.​
Melanie Ann Guste​
Valerie Guste Johnson​
Althea Guste Wise​
Elizabeth Therese Guste​
James Patrick Guste​
Anne Duchesne Guste​
One son deceased​
Parents:
William J. Guste, Sr.​
Marie Louise Alciatore​
 Loyola University New Orleans​
Loyola College of Law​
 ​
William Joseph "Billy" Guste, Jr. (May 26, 192 – July 25, 2013),[1] was a New Orleans attorney, businessman and Democratic attorney general of his native Louisiana from 1972 to 1992. He succeeded Jack Paul Faustin Gremillion, Sr. (1914-2001) a fellow Democrat engulfed in scandal who had held the position since 1956. Guste received recognition for molding the office into a model of integrity and efficiency. In 1976, he helped Jimmy Carter, the former governor of Georgia, carry Louisiana and served as the first elector listed on the ballot for the Carter-Mondale slate.​
 ​Guste was born in New Orleans to a wealthy couple, William J. Guste, Sr., and the former Marie Louise Alciatore. The senior Guste, like his son, was an attorney and a member of the Roman Catholic men's organization, the Knights of Columbus. Guste, Jr., graduated from Jesuit High School, Loyola University New Orleans, and the Loyola College of Law. After admission to the bar, Guste served in the United States Army, with overseas duty primarily in France.[1]​
 Early in his career, from 1956 to 1957, Guste served on the New Orleans Metropolitan Crime Commission at the invitation of then Mayor deLesseps Story "Chep" Morrison, Sr. (1912-1964).​
 Guste was married to the former Dorothy "Butsie" Schutten (born December 1, 1923), the daughter of Mr. and Mrs. Bernard Schutten. Guste was the father of ten children, nine of whom survived him.[1]  He was widely viewed as a quiet, unassuming, almost shy man with dark wavy hair, in sharp contrast to the flamboyant, in-your-face image of his predecessor Jack Gremillion. Voters rewarded Guste's competence and integrity and kept him in the attorney general's position for two decades until his retirement from the office. He is therefore the longest-serving attorney general in Louisiana history.​
​
 ​
Guste was a one-term state senator from Orleans Parish from 1968 to 1972, but he had ambition beyond the legislature and ran unsuccessfully for mayor of New Orleans in the 1969 Democratic primary. Two other candidates, Maurice Edwin "Moon" Landrieu, with 33,093 votes, and Jimmy Fitzmorris, with 59,301 ballots, went into a runoff primary. Guste polled 29,487 votes, just 3,606 ballots behind Landrieu. The Landrieu scored a come-from-behind victory over Fitzmorris. Landrieu then went on to defeat the Republican mayoral candidate, Ben C. Toledano, in the general election held in the spring of 1970.
 The state senators were elected at-large until 1972, and Guste was replaced by Fritz Windhorst, who represented Orleans and Jefferson parishes.[2]​
 ​
In 1971, Guste did not seek reelection to the state Senate but instead entered a crowded Democratic field for attorney general. Gremillion was seeking a fifth term, but it was believed that the corruption allegations then pending against him, which later resulted in conviction and a prison sentence, would doom his candidacy.​ In addition to Guste and Gremillion, the other candidates included Guste's state Senate colleague, George T. Oubre of Vacherie in St. James Parish, who also represented St. Charles and St. John the Baptist parishes. Also running was Joseph Minos Simon, Sr. (1923-2004), an articulate Lafayette lawyer known for his theatrics, humor, and yet stern demeanor in the courtroom. Waiting to face the winner of the Democratic primaries was Thomas Eaton Stagg, Jr. (1923-2015) of Shreveport, the first Republican to wage an active contest for Louisiana attorney general in the 20th century. Stagg would later become a long-serving judge of the United States District Court for the Western District of Louisiana]] on the appointment of U.S. President Richard M. Nixon.​
 Guste and Oubre went into a primary runoff in which Guste was a comfortable winner. Guste then overwhelmed Stagg, whose campaign, while blessed by Republican gubernatorial nominee David C. Treen, then of Jefferson Parish, fell far short. Guste prevailed in the general election with 763,276 votes (74.1 percent) to Stagg's 270,038 (25.9 percent). Stagg won only his native Caddo Parish with 54 percent of the ballots cast. The other sixty-three parishes backed Guste, most by margins of more than 57 percent. In that same election, Treen was defeated by Democrat Edwin Edwards.​
 Once in office, Guste placed on his staff the attorney Joseph A. Sims of Hammond, a former legal advisor to Governor Earl Kemp Long, who had "rescued" Long from his confinement at Southeastern Louisiana State Hospital in Mandeville in the summer of 1959. Sims died a year after his appointment. Sims himself had run unsuccessfully for attorney general in 1952.​
 Guste retained the assistant attorney general Edward M. Carmouche of Lake Charles, a holdover from the Gremillion administration and a leader of the liberal faction of the Louisiana Democratic Party.[3]​
 Guste was an activist attorney general in many areas. Governor Edwards named him in 1974 to the Governor's Commission on Law Enforcement and the Administration of Justice. In 1983, President Ronald W. Reagan named Guste, despite their partisan difference, to the President's Commission on Organized Crime, a role that he  filled for three years.[1]​ In 1989, Guste was named "Conservationist of the Year" among the elected official category by the Louisiana Wildlife Federation.[4]​
 Guste worked for passage of state consumer and environmental protection laws. He coordinated the work of some thirty agencies involved in matters of the environment. He worked to revise state criminal laws in regard to narcotics. Guste helped more than 150 citizens' groups break through government bureaucracies. Guste argued successfully the Tidelands offshore oil revenue case before the United States Supreme Court and obtained a settlement of $136 million for the state.[1]​
​
Guste was involved in litigation referring to Louisiana's shrinking coastline, or wetlands. In such cases, he often took the environmentalist position, with the view that once such wetlands are lost, they cannot be reclaimed. Property rights advocates, however, often quarreled with Guste by taking the view that he defined "wetlands" too broadly.
 Guste defended Tulane University in New Orleans in one of his advisory opinions, which have the force of law, unless the legislature rules otherwise. He said that the institution was tax-exempt under a law, and that the exemption applied to sales and use taxes too, unless the legislature stipulated otherwise.​
 In 1984, Guste announced that his office would no longer enforce blue laws after a federal judge, Charles Schwartz, Jr.,  of New Orleans, ruled that three particular department stores could open on Sundays. Guste said that it would be unfair to give some businesses an advantage over their competitors by allowing only those given court permission to open on Sunday.[5]​
​
In 1986, Guste urged Governor Edwin Edwards to call a special session of the legislature to raise the drinking age from eighteen to twenty-one so that the state could qualify for $13 to $30 million in state highway aid which would otherwise be forfeited.[6] Earlier in the regular legislative session, the state House by a two-vote margin had refused to raise the drinking age; forty-eight states at the time raised the drinking age to keep their highway funds on target.[7]​
​
Guste is a former president of the National Association of Attorneys General, based in Washington, D.C.[8]​
He was an organizer of the advocacy group UNITY for the Homeless, Inc., which seeks to provide housing for the homeless.[1]​
​
 In the Treen gubernatorial administration from 1980 to 1984, the legislature passed a law permitting public school teachers to instruct the tenets of creation science in their lessons if the theory of evolution is also presented. The law neither required the teaching of evolution or creation science, but the instruction of both if the other were taught. Guste, regardless of his personal views on the matter, as attorney general was compelled to defend the Louisiana law. He argued that teachers should be able to present evidence favoring creation because the Supreme Court recognizes that teachers "already possess" the "flexibility to teach all the scientific evidence about the origins of life." Guste noted that the monitoring of state textbooks and science curricula would continue under the creation-science law.​
​
The high Court ruled 7-2 in Edwards v. Aguillard,[9] long after the initial issue had arisen, that creation science is "not science" but a "religion" in the name of "science." The "Edwards" in the case referred to Governor Edwin  Edwards, who had returned to office in 1984, by having unseated David Treen in the primary the preceding fall. The Supreme Court found that the Louisiana legislature's actual intent was "to discredit evolution by counterbalancing its teaching at every turn with the introduction of creationism, a religious belief." Therefore, the court claimed that creation science instruction would be a violation of the establishment clause of the First Amendment. Defending the Louisiana law were then Chief Justice William Rehnquist and Associate Justice Antonin Scalia, who argued that the court had no reason to interfere with a state law regarding school instruction. Scalia later said that such decisions meant that the court "bristled with hostility toward religion."[10]​
 Arkansas, under Republican Governor Frank D. White, had passed a similar law at the time. Some fifteen years later, an alternative view called "intelligent design" came before federal courts for scrutiny, and lower courts ruled that intelligent design too is "not science" but a form of "religion" in the name of "science."​
​
 ​
Guste pleased liberals by being a strong defender of affirmative action. He submitted an amicus curiae brief in the 1986-1987 case Johnson v. Transportation Agency of Santa Clara County, California on behalf of a female county employee who was promoted over an equally-qualified male employee. The plan provided that, in making promotions to positions within a traditionally segregated job classification in which women have been significantly underrepresented, the transportation agency was authorized to consider as one factor the sex of a qualified applicant. The agency said that women were represented in numbers far less than their proportion of the county labor force. Therefore, the county plan was intended to achieve a statistically measurable yearly improvement in the hiring, training, and promotion of minorities and women. The U.S. Supreme Court agreed with the county government that the voluntary affirmative-action plan did not violate Title VII of the Civil Rights Act of 1964.​
 The court held that the Santa Clara County plan was "not established to remedy prior sex discrimination by the agency, but imposed racial and sexual tailoring that would, in defiance of normal expectations and laws of probability, give each protected racial and sexual group a governmentally determined 'proper' proportion of each job category," or, in other words, racial and sexual quotas.​
​
 ​
Like many Louisiana Democrats in the 1980s, Guste pleased conservatives by taking a pro-life position on abortion. In 1989, in his last term in office, he filed an amicus curiae brief in Webster v. Reproductive Health Services. The Supreme Court in this case reversed an appeals court decision that required public hospitals to offer abortion services. The case stemmed from Missouri, which has such facilities in St. Louis and Kansas City, but had the appeals ruling stood, it would have applied in the other forty-nine states as well. The "Webster" in the case was Missouri's Republican Attorney General William L. Webster. Abortion rights groups around the country rallied on behalf of the defendant. The result is that state hospitals need not provide elective abortions except in cases of impregnation by rape, incest, or in rare situations where the medical personnel cannot save the lives of both the woman and the child.[11]​
 ​
In the first ever nonpartisan blanket primary on November 1, 1975, Guste had won a second term as attorney general, having easily defeated intra-party rival, state  Representative Risley C. Triche (1927-2012), a colorful figure from Napoleonville in Assumption Parish in south Louisiana, 672,065 (63 percent) to 398,088 (37 percent).[12]​
 Though he had considered running for governor in 1987, Guste instead waged his last race for attorney general by a closer vote than what he had become accustomed. He faced two fellow Democrats in the nonpartisan blanket primary, Orleans Parish District Attorney Harry A. Connick, Sr., the father of the popular entertainer Harry Connick, Jr., and Manuel A. "Manny" Fernandez, a state senator from nearby Chalmette in St. Bernard Parish. Guste led the October 1987 balloting with 655,979 votes (47 percent), and Connick trailed with 440,865 ballots (31 percent). Fernandez drew a significant 309,065 votes (22 percent). In the November 1987 general election (usually called a "runoff" in Louisiana), with a much lower turnout, Guste prevailed over Connick, 516,658 (54 percent) to 440,984 (46 percent) and also won in their Orleans Parish by some 16,000 votes.[13] It was a striking statistical quirk that Connick received just under 441,000 ballots in both the primary and the general election. The Guste-Connick contest pleased many in New Orleans because the city could offer to the rest of the state two of its most famous and successful men.​
 Guste, who was nearing seventy at the time, did not seek a sixth term in the 1991 primary. He was succeeded by fellow Democrat Richard Phillip Ieyoub of Lake Charles. In the general election, Ieyoub defeated Republican Ben Bagert, a state senator from New Orleans known for his interest in conservation of natural resources, by a lopsided margin of 69 to 31 percent.[14]
 With his  brother, Roy Francis Guste, Sr. (1923-2010), William Guste was a fourth-generation proprietor of the elegant Antoine's Restaurant in New Orleans, founded by their mother's family. Their two sons, cousins Roy F. Guste, Jr. (born 1951), and Bernard R. "Randy" Guste (born 1949), have split time serving the family as managers since the 1970s. The restaurant is known for many menu items, particularly Oysters Rockefeller, for which more than a million orders have been served since the business opened in the 19th century.[15]​
 In addition to their ten children, William and Dorothy Guste have twenty-seven grandchildren and twenty-one great-grandchildren.[1]​
​
In 2000, Guste received the papal honor of "Knight Commander of the Order of St. Gregory the Great" from Pope John Paul II, who cited Guste's  "tireless efforts to serve God and neighbor." Guste, who had attended mass daily during most of his life, died at Touro Infirmary in New Orleans in 2013.​
​
Funeral services were private. William J. Guste Elementary School in New Orleans is named in his honor.[1]​
​
 ​
​​​​​​​
 1 Background 2 Running for mayor of New Orleans, 1969 3 Election as attorney general 4 Critical cases
4.1 Defending creation science
4.2 Affirmative action
4.3 Pro-Life activities
 4.1 Defending creation science 4.2 Affirmative action 4.3 Pro-Life activities 5 The last campaign of 1987 6 Family and death 7 References ↑ 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 William J. Guste, Jr.. nola.com. Retrieved on July 28, 2013.
 ↑ Membership of the Louisiana State Senate, 1880-2012. legis.la.gov. Retrieved on July 28, 2013.
 ↑ Lake Charles American Press, April 7, 1990.
 ↑ Louisiana Wildlife Federation Conservation Award Winners. lawildlifefed.org. Retrieved on January 8, 2015.
 ↑ "Guste won't enforce blue laws," Minden Press-Herald, August 10, 1984, p. 1.
 ↑ "Guste advocates session to hike legal drinking age", Minden Press-Herald, August 14, 1986, p. 1.
 ↑ "Louisiana lost $15 million in federal highway funds to keep drinking age at 18," Minden Press-Herald, July 1, 1986, p. 1.
 ↑ Minden Press-Herald, July 18, 1983, p. 3.
 ↑ Edwards v. Aguillard. talkorigins.org. Retrieved on July 28, 2013.
 ↑ Katherine Ching. The U.S. Supreme Court Rules against Creation Science. talkorigins.org. Retrieved on July 28, 2013.
 ↑ Supreme Court of the United States, Webster, Attorney General of Missouri et al v. Reproductive Health Services, et al. pregnantpause.org. Retrieved on July 28, 2013.
 ↑ Election returns, Minden Press-Herald, November 3, 1975, p. 1.
 ↑ Louisiana general election returns, November 21, 1987. staticresults.sos.la.gov. Retrieved on July 28, 2013.
 ↑ Louisiana Secretary of State, Election Returns, November 16, 1991.
 ↑ Restaurant Antoine. gumbopages.com. Retrieved on July 28, 2013.
 Louisiana People Attorneys Politicians State Senators Democrats Catholic Politicians United States Army World War II Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 25 October 2019, at 08:10. This page has been accessed 390 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 In office In office Parents William Joseph "Billy" Guste, Jr. William Joseph "Billy" Guste, Jr.​
 Louisiana Attorney General​
 In office1972​ – 1992​
  Jack Paul Faustin Gremillion,Sr. ​
  Richard Ieyoub​
 State Senator for Orleans Parish​
 In office1968​ – 1972​
  At-large election of eight senators​
  Fritz Windhorst​
 
  May 22, 1922​New Orleans, Louisiana​
  July 25, 2013 (aged 91)Touro Infirmary in New Orleans​
  American​
  Democratic Party ​
  Dorothy Schutten Guste ​
  Ten children, including:
William J. Guste, III​
Bernard R. "Randy" Guste​
Marie Louise Guste Nix, Sr.​
Melanie Ann Guste​
Valerie Guste Johnson​
Althea Guste Wise​
Elizabeth Therese Guste​
James Patrick Guste​
Anne Duchesne Guste​
One son deceased​
Parents:
William J. Guste, Sr.​
Marie Louise Alciatore​
  Jesuit High School​
Loyola University New Orleans​
Loyola College of Law​
  Attorney; Businessman​
 William J. Guste Contents Background Running for mayor of New Orleans, 1969 Election as attorney general Critical cases The last campaign of 1987 Family and death References Navigation menu Defending creation science Affirmative action Pro-Life activities Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
