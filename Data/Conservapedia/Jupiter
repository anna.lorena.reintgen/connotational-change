Jupiter is the fifth planet from the sun, and the most massive. In fact, this gas giant is the largest body in the solar system except for the sun itself, and more than two times as massive as all the other planets combined. In composition it closely resembles a star, so much so that some authorities hold that had it been eighty times as massive, it would have become a star.
 Named after the ruler of the Roman gods, Jupiter is large and bright enough to be seen by the naked eye. In fact it is the fourth-brightest object in the night sky, after the Sun, the Moon, and the planet Venus.[7] Jupiter is conspicuous and easy to recognize. An observer carrying 7-power field glasses (of a type used for birding or hunting) will see a round disk that shines steadily. An observer with a small telescope should also be able to resolve the Galilean moons, as Galileo Galilei himself did. They will appear, as they did to Galileo, as "stars" traveling in "formation" with Jupiter and changing their positions relative to Jupiter in a single night of observation.
 The Babylonians believed that their god Marduk set Jupiter in the sky to guide the stars.[8] Jupiter might have had an influence in the invention of several ancient calendars, including the Mayan calendar, the Egyptian calendar, the ancient (pre-Hillel II) Hebrew calendar, and possibly the calendar in use (at least by the Sethite generations of Adam before the Great Flood).[8]
 The name of the Roman god derives from the Latin "dyeu-pater" and "ius-pater."[9]
 Jupiter completes one slightly eccentric orbit around the Sun in 11.86 years, and returns to the same position in Earth's night sky in roughly 399 years.[2][8] Its mean distance from the Sun is about 5.2 AU—almost exactly the distance predicted by the Titius-Bode law of planetary distances. Though Jupiter is the fifth planet from the Sun, it is actually the sixth object of a size requiring the assumption of a spheroidal shape. Hence n=6 is the proper number to use in the Bode's Law formula.
 Jupiter has a very short sidereal day of about 9.92 hours. However, its latitudinal cloud bands rotate at different speeds, and some of these appear to rotate retrograde. Astronomers once calculated the Jovian day from observations of the equatorial cloud band, but today they rely on the periodicity of Jupiter's magnetic field.[10]
 Jupiter is about 318 times as massive as Earth, and has a radius of 71,492 km (44,423 miles). Its atmosphere is composed primarily of 86% hydrogen, 14% helium, and traces of methane, ammonia, phosphine, water, acetylene, ethane, germanium, and carbon monoxide.[10] It is so thick that no record exists of the surface, if Jupiter has one in the traditional sense—though Jupiter might have a heavy-metal core having a composition similar to that of Earth but 20 to 30 times as massive.[10]
 The planet endures continual storms. The most famous of these is the Great Red Spot, which has raged for at least three hundred years. The most precise estimate of its duration is 342 years,[11] but this might refer merely to the first time that someone observed it with a telescope capable of resolving it. It is large enough to encompass the entirety of the Earth and rotates anticlockwise along Jupiter's equator. Winds at its edge circulate at 360 km/h,[10] far faster than those of the most powerful hurricane ever recorded on Earth.
 Jupiter actually radiates 1.7 times the heat that strikes it from the Sun.[12] This could be due to Kelvin-Helmholtz gravitational compression, a process that also limits the size that Jupiter can have.[7] In this process, the planet's cooling causes it to compress, which in turn heats up Jupiter's core.
 Jupiter has a magnetic field about 14 times as strong as that of Earth. Specifically, its magnetic dipole moment amounts to 1.55 × 1027 N-m/T, higher than that of any object except the Sun. It is so high, in fact, that by Russell Humphreys' model for planetary magnetic fields, Jupiter's field must have formed with all of its mass aligned for the maximum cumulative magnetic dipole moment, instead of the usual 25%. By this assumption, the magnetic dipole moment of Jupiter at creation must have been 1.79 × 1027 N-m/T. The half-life of this field is longer than 31,500 Julian years.
 This field tends to shield Jupiter from the solar wind. However, the field has also trapped large numbers of radioactive particles in a Van-Allen-like radiation belt[10] that actually encompasses the orbits of the seven innermost moons. The magnetic field extends beyond the far side of Jupiter for at least 700 million kilometers.[10]
 The very strong magnetic field of Jupiter is extraordinary by any standard. The Humphreys model has passed two key tests of its predictive value, one at Uranus and the other at Neptune.[13] That model normally assumes that God formed any given celestial body initially out of water, with its molecules partially aligned for maximum cumulative effect, and then transmuted the molecules after the magnetic field was established. For most celestial bodies thus far observed, an alignment fraction of 25% suffices to establish a magnetic dipole moment at creation. But Jupiter's magnetic dipole moment is far too high to have resulted from an initial alignment of 25%. An alignment of 100% seems to be required, the highest fraction allowable and the highest fraction of all celestial bodies observed to date. This implies that God made Jupiter to be a beacon in the night sky, in more ways than merely by making it large.
 Jupiter's orbit is inclined 1.305 degrees to the ecliptic, so Jupiter appears, in turn, in each constellation of the Zodiac. In fact, its 399-day synodic year places it in a different constellation of the Zodiac at the same point in each succeeding year. Pratt[14] shows that the members of the Zodiac, and a number of closely associated constellations, presage the life and ministry of Jesus Christ. If Jupiter is indeed a beacon, then it could not have been better placed.
 Jupiter does have a ring, consisting of three components, called the halo, the main ring, and the gossamer ring. This ring lies entirely within the region occupied by the four innermost moons, and probably derives its substance from escaping dust from the two innermost moons, Metis and Adrastea. The average size of the ring particles is 10 microns, comparable in size to the particles in tobacco smoke.[15]
 In 1994, scientists were able to witness fragments of the comet Shoemaker-Levy 9 collide with Jupiter. These collisions lasted for 6 days, starting on July 16 of that year, and was the first cosmic collision to be observed. During the impacts, at least 21 separate fragments with a size of up to 2 kilometers hit Jupiter, leaving scars in its atmosphere larger than several Earths.
 Jupiter's sidereal day is less than ten Earth hours long. If Jupiter formed simply as an accretion aggregate of the solar nebula, then it should not have acquired such tremendous angular momentum. The problem involves not merely the short sidereal day but also Jupiter's tremendous mass.
 The alternative theory is that Jupiter formed as a "failed protostar" at the center of its own nebula. This theory has several problems:
 Galileo Galilei studied Jupiter extensively  and in the process discovered its four largest moons--Io, Europa, Ganymede, and Callisto, also known as the Galilean moons. Many other astronomers studied Jupiter from Earth-bound telescopes for hundreds of years. In the process they discovered twelve of Jupiter's moons and the Great Red Spot, but did not discover Jupiter's ring system.
 Eight spacecraft, all from the United States, have visited Jupiter thus far. First to do so was Pioneer 10 (December 3, 1973), which suffered tremendously from the radiation belt but still provided the evidence for Jupiter's magnetosphere. Pioneer 11 was next (December, 1974) and took far better images of Jupiter and its Great Red Spot.
 Voyager 1 (March, 1979) and Voyager 2 (July, 1979) gave the first comprehensive views of Jupiter and the Jovian system, including the discovery of its rings, the discovery of four moons inside Io's orbit, and the first extensive studies of the Galilean moons.
 Ulysses (February 1992) made a brief flyby of Jupiter in order to place itself in polar orbit around the Sun. Nevertheless, European Space Agency scientists used this opportunity to make further measurements of Jupiter's magnetosphere and the effect upon it by the solar wind.
 Galileo reached Jupiter in 1995. It released a probe to dive into Jupiter's atmosphere; that probe transmitted for nearly an hour before the tremendous pressures crushed it. Galileo's orbiter remained in the system for nearly eight years, through two extensions of its mission, and conducted the most extensive surveys of the Galilean moons to day. Eventually, with the craft low on fuel, mission planners dived it into Jupiter to prevent its possibly crashing into Europa, rupturing Europa's ice sheet, and contaminating the liquid ocean that astronomers now suspect lies a mere 10 km deep to the ice and could yet harbor extraterrestrial life.[10]
 Cassini flew by Jupiter briefly in 2000 on its way to Saturn. While in the Jovian system, it took the image shown at the top of this article. Similarly the New Horizons mission included a Jupiter flyby in its journey to the Pluto system, passing Jupiter on February 28, 2007.[17]
 The latest probe to visit Jupiter is NASA's Juno mission.[18] It reached Jupiter in July 2016 and has returned highly detailed images of the planet. It aims to investigate Jupiter's magnetosphere further, as well as measuring its magnetic and gravitational fields to determine its internal structure.
 1 Ancient knowledge and naming 2 Orbital characteristics 3 Rotational characteristics 4 Physical characteristics
4.1 The Great Red Spot and other storms
4.2 Radiation of heat
 4.1 The Great Red Spot and other storms 4.2 Radiation of heat 5 Magnetosphere 6 Ring system 7 Shoemaker-Levy 8 Problems for uniformitarian theories 9 Exploration 10 References  Jupiter never ignited, though its magnetic field is four times as strong as its mass would normally predict.  None of Jupiter's 63 moons is gaseous. Therefore, a key event in the nebular sequence did not take place.  The four dwarf-planet-sized Galilean moons have vastly differing apparent geological "ages" coming from times estimated to be as disparate as ten million years to as recent as thirteen hundred (although the times cannot be fixed with any reliability).[16]  Many of Jupiter's outer moons move retrograde to Jupiter's own day. ↑ 1.00 1.01 1.02 1.03 1.04 1.05 1.06 1.07 1.08 1.09 1.10 1.11 1.12 1.13 1.14 1.15 1.16 1.17 1.18 "Entry for Jupiter." Solar System Exploration, NASA. Accessed March 3, 2008.
 ↑ 2.0 2.1 2.2 2.3 2.4 2.5 2.6 Williams, David R. "Jupiter Fact Sheet." National Space Science Data Center, NASA, November 2, 2007. Accessed March 2, 2008.
 ↑ 3.0 3.1 3.2 Calculated
 ↑ "Planet Physical Characteristics." Solar System Dynamics, JPL, NASA. Accessed March 3, 2008.
 ↑ 5.0 5.1 Humphreys, D. R. "The Creation of Planetary Magnetic Fields." Creation Research Society Quarterly 21(3), December 1984. Accessed April 29, 2008.
 ↑ "Jupiter Fact Sheet," NASA, November 2, 2007. Accessed May 12, 2008.
 ↑ 7.0 7.1 Arnett, Bill. "Entry for Jupiter." The Nine 8 Planets, April 10, 2005. Accessed March 3, 2008.
 ↑ Weisstein, Eric W. "Jupiter." Eric Weisstein's World of Astronomy, accessed March 3, 2008.
 ↑ Humphreys, D. R. "Beyond Neptune: Voyager II Supports Creation." Institute for Creation Research. Accessed April 30, 2008
 ↑ Pratt, John C. "The Constellations Tell of Christ." Meridian, June 15, 2005. Accessed May 12, 2008.
 ↑ Harvey, Samantha. "Jupiter:Rings." Solar System Exploration, NASA, February 7, 2008. Accessed March 3, 2008.
 ↑ Fulbright, Jeannie. Exploring Creation with Astronomy. Apologia Educational Ministries, 2004.
 ↑ Pluto-Bound New Horizons Sees Changes in Jupiter System from nasa.gov
 ↑ Juno mission pagefrom nasa.gov
 Planets Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 9 April 2019, at 10:09. This page has been accessed 22,609 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Jupiter Symbol Name of discoverer Name origin Primary Order from primary Perihelion Aphelion Semi-major axis Titius-Bode prediction Circumference Orbital eccentricity Sidereal year Synodic year Avg. orbital speed Inclination Sidereal day Solar day Axial tilt Mass Density Equatorial radius Surface gravity Escape speed Surface area Mean temperature Number of moons Composition Color Albedo Magnetic flux density Magnetic dipole moment at present Magnetic dipole moment at creation Decay time Half life Jupiter This processed color image of Jupiter was produced in 1990 by the U.S. Geological Survey from a Voyager image captured in 1979
  Symbol
  
  Name of discoverer
  Known to ancients
  Name origin
  Roman king of gods[1]
  Primary
 Sun
  Order from primary
  6
  Perihelion
 4.95 AU[1]
  Aphelion
 5.46 AU[1]
  Semi-major axis
 5.203 AU[1][2]
  Titius-Bode prediction
 5.2 AU[3]
  Circumference
 32.675 AU[1]
  Orbital eccentricity
 0.048[1]
  Sidereal year
 11.862 a[1][2]
  Synodic year
 398.88 da (1.092 a)[2]
  Avg. orbital speed
 13.07 km/s[1][2]
  Inclination
 1.305°[1] to the ecliptic
  Sidereal day
  9.925 h (0.414 da)[1][2]
  Solar day
  9.9259 h[2]
  Axial tilt
  3.13°[1]
  Mass
  1.8986 × 1027 kg (317.705 × earth)[1]
  Density
  1,326 kg/m³[1]
  Equatorial radius
  71,492 km[1]
  Surface gravity
  22.88 m/s² (2.333 g)[1]
  Escape speed
  59.56 km/s[1]
  Surface area
  61,400,000,000 km² (120.375 × earth)[1]
  Mean temperature
  152 K[1]
  Number of moons
  66
  Composition
  90% hydrogen and 10% helium[1]
  Color
  Colored latitudinal bands
  Albedo
  0.52[4]
  Magnetic flux density
  4.28 G [6]
  Magnetic dipole moment at present
  1.55 × 1027 N-m/T[5]
  Magnetic dipole moment at creation
  1.79 × 1027 N-m/T[5]
  Decay time
  45,469 a[3]
  Half life
  31,516 a[3]
 v • d • e
The Solar SystemStarSolTerrestrial PlanetsMercury · Venus · Earth · MarsGas GiantsJupiter · Saturn · Uranus · NeptuneDwarf PlanetsCeres · Haumea · Makemake · Pluto · ErisAsteroid BeltMajor asteroids · C-type asteroids · S-type asteroids · M-type asteroidsTrans-Neptunian ObjectsKuiper belt · Scattered disk · Oort Cloud · NemesisSatellitesMoon · Phobos · Deimos · Io · Europa · Ganymede · Callisto · Mimas · Enceladus · Tethys ·  Dione · Rhea ·  Titan ·  Hyperion ·  Iapetus ·  Miranda · Ariel · Umbriel · Titania · Oberon · Triton ·  Nereid · Charon · Nix · Hydra · DysnomiaCategories  Star Sol   Terrestrial Planets Mercury · Venus · Earth · Mars  Gas Giants Jupiter · Saturn · Uranus · Neptune  Dwarf Planets Ceres · Haumea · Makemake · Pluto · Eris  Asteroid Belt Major asteroids · C-type asteroids · S-type asteroids · M-type asteroids  Trans-Neptunian Objects Kuiper belt · Scattered disk · Oort Cloud · Nemesis  Satellites Moon · Phobos · Deimos · Io · Europa · Ganymede · Callisto · Mimas · Enceladus · Tethys ·  Dione · Rhea ·  Titan ·  Hyperion ·  Iapetus ·  Miranda · Ariel · Umbriel · Titania · Oberon · Triton ·  Nereid · Charon · Nix · Hydra · Dysnomia  Categories Jupiter Contents Ancient knowledge and naming Orbital characteristics Rotational characteristics Physical characteristics Magnetosphere Ring system Shoemaker-Levy Problems for uniformitarian theories Exploration References Navigation menu The Great Red Spot and other storms Radiation of heat Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
