Brant L. Thompson
Melissa T. Blanchfield
 University of Louisiana at Monroe
 Francis Coleman Thompson (born October 29, 1941)[1] is a wealthy developer from Delhi in Richland Parish, Louisiana, who served as a senior Democratic member of the Louisiana House of Representatives from 1975 until 2007. Then by virtue of term limits, Thompson was ineligible to have sought a ninth four-year term in the nonpartisan blanket primary held on October 20, 2007.
 Instead, Thompson was elected outright over two fellow Democrats to the District 34 seat in the Louisiana State Senate vacated by the also term-limited Charles Jones (born 1950) of Monroe. Thompson received 13,763 votes (51 percent) to 10,937 (42 percent) for African-American State Representative Willie Hunter, Jr., of Monroe and 2,113 (8 percent) for Paxton J. Branch.[2] Ten days after he vacated the seat to Thompson, Charles Jones was charged with two counts of making and subscribing a false federal income tax return and one count of tax evasion.[3] Hunter charged irregularities in the primary election in part on grounds that certain Thompson supporters in heavily black East Carroll Parish distributed food packages to the poor with instructions that they should vote for Francis Thompson to maintain such assistance.[4]
 In addition to his own Richland Parish, Thompson's House district included all or portions of East Carroll, Madison, Morehouse, Ouachita, and West Carroll parishes in northeast Louisiana. His Senate district included all or parts parts of Concordia, East Carroll, Madison, Ouachita, Richland, and Tensas parishes.[5]
 Now term-limited in the state Senate, Thompson is unopposed in the nonpartisan blanket primary on October 12, 2019, to return to his former District 19 state House seat. He will hence return to the House in 2020 after twelve years in the Senate. The primary includes the race for governor, in which incumbent Democratic Governor John Bel Edwards faces a divided Republican field, including U.S. Representative Ralph Abraham, from Thompson's own Richland Parish, and Eddie Rispone, a Baton Rouge businessman.[6]
 Thompson graduated from Delhi High School in 1959. He received his Bachelor and Master of Science degrees from Louisiana Tech University in Ruston in Lincoln Parish. He also procured an Ed.D. degree from the University of Louisiana at Monroe (then Northeast Louisiana University). Thompson was a teacher from 1963-1965, but he vacated the classroom to become vice president of a manufacturing company from 1965-1972. Later, he returned to the classroom as a ULM assistant professor of education.
 From 1968 to 1975, Thompson was an elected member of the Richland Parish School Board. He won a special election in 1975 to fill the House seat vacated by the resignation of Democrat Benny Gay Christian (1925-1982),[7] who had served in the state House since 1964. Later in the year, Thompson won a full term in the seat, which in time became the single-member District 19. Prior to his legislative years, Thompson worked in Baton Rouge for the Louisiana Department of Education under Superintendent Louis J. Michot from 1974 to 1975. He was a member of the Louisiana Commission on Law Enforcement from 1973 to 1974. Thompson has also been a member of the Southern Regional Education Board and the Education Commission of the States. He has been active in the Retarded Children's Association and the Louisiana Mental Health Drug Advisory Council. He is a member of the Masonic lodge and the Lions International.
 Thompson is married to the former Marilyn Bryant (born October 6, 1944)[8] The couple resides 456 Robin Hood Lane in Delhi. There are three Thompson children: Francis Todd Thompson (born April 3, 1964) and Brant L. Thompson (born April 1, 1965), both of Delhi, and Melissa T. Blanchfield (born October 21, 1972) of Baton Rouge.[9] Thompson is a Presbyterian.
 Thompson's brother and the oldest of his five siblings, Clyde Nolan Thompson (April 25, 1937 — July 31, 2015), was a multi-sport athlete at Louisiana Tech who for twenty years held the school record for stolen bases. He was also a Tech football quarterback. With his doctorate in professional education from the University of Southern Mississippi at Hattiesburg, Mississippi, Clyde Thompson was a teacher and coach in several Louisiana high school and then at Louisiana Tech. In 1975, he became the drug education coordinator for District 8 for the Louisiana Department of Education. In 1980, Republican Governor David C. Treen named Clyde Thompson, like his brother a Democrat, as the deputy director of the Louisiana Department of Transportation and Development. He then became the assistant to the president of the State Board of Education before he returned to Delhi and became the director of the Madison Parish Port, a position which he held for more than two decades until retirement in 2014.[10]
 In 2000, Thompson was named chairman of the House Agriculture Committee.[11] In 2003, he authored the "Master Farmer" program, which the legislature approved without dissent. The program was developed by the Louisiana State University Agriculture Center and sponsored by the Louisiana Farm Bureau Federation. It is an environmental education program designed to help farmers and ranchers identify and adopt best management practices to improve water quality in lakes, streams and bayous. It involves classroom instruction, field days and implementation of farm-specific conservation plans to maximize productivity while minimizing environmental impacts. Thompson's model program has been proposed for national acceptance.[12]
 During his long legislative career, Thompson worked successfully to procure state funding for Poverty Point Reservoir, a 3,000-acre (12 km2) lake north of Delhi that was completed in 2001. The reservoir has since been converted to a state park. Thompson's brother, Michael L. "Mike" Thompson (born February 1, 1949) of Delhi is the former executive director of the reservoir district. On June 26, 2008, Mike Thompson along with District Attorney William R. "Billy" Coenen (born July 1, 1947) of the Fifth Judicial District and a resident of Rayville and engineer Terry Denmon of Monroe were indicted by a federal grand jury in Shreveport for having conspired secretly to purchase land along what would become Poverty Point Reservoir and then selling the land at an elevated price.[13] The trio is each charged with one count of conspiracy and eight counts of mail fraud. Mike Thompson is also under indictment for alleged violations of the Hobbs Act. Thompson is accused of having used district employees to perform personal work for him at the lake.[14]
 According to the indictment, Thompson, Coenen, and Denmon bought a 5-acre (20,000 m2) tract of land on what would later form the shores of the reservoir for $16,800. They reportedly used a nominee purchaser to hide their interest. Subsequently, Thompson used his position as executive director to have trees removed from the property. Thompson and Denmon (born May 15, 1944),[6] whose engineering firm was contracted to work for the district, had the property excavated, the indictment states. The tract was subdivided, and six of the eight lots sold for a total of $250,000. Coenen is implicated through his role as attorney for the Poverty Point District as well as being district attorney.[14]
 In 1996, Thompson ran for the open Fifth District seat in the United States House of Representatives. He polled 50,144 votes (28 percent) and went into a general election with the Republican ophthalmologist John Cooksey of Monroe, who led in the nonpartisan blanket primary with 60,853 ballots (34 percent). Former U.S. Representative Clyde Cecil Holloway (1943-2016) of Forest Hill in Rapides Parish trailed in third place with 48,226 (27 percent).[15] Holloway then endorsed Cooksey, who defeated Thompson by a comfortable margin. Cooksey received 135,990 votes (58 percent) to the more liberal Thompson's 97,363 (42 percent).[16] Cooksey served three terms before leaving the U.S. House early in 2003. Cooksey's campaign manager, Lee Fletcher, was thereafter named as his chief of staff. Fletcher tried to win the House seat himself in 2002 but lost to Democrat (later Republican) Rodney Alexander.
 Though he is a Democrat, Thompson broke party ranks in the 2003 gubernatorial primary to support Republican candidate Brigadier General Huntington Blair "Hunt" Downer of Houma in Terrebonne Parish, a former state House Speaker, who finished sixth in the balloting though he had the support of a cross-section of state legislators from both parties.[17]
 In 2005, Francis Thompson was inducted into the Louisiana Political Museum and Hall of Fame in Winnfield.[18] In 2015, he was unopposed for reelection to the Senate.
 In 2017, Thompson successfully introduced legislation to name the Louisiana School for Math, Science and the Arts in Natchitoches in honor of the late state Representative Jimmy D. Long, who was among those instrumental in establishment of the institution through Long's chairmanship of the House Education Committee.[19]
 1 Background 2 Interest in agriculture 3 Poverty Point 4 Other political developments 5 References ↑ Francis Thompson. Mylife.com. Retrieved on July 19, 2019.
 ↑ Louisiana election returns, Secretary of STate's office, October 20, 2007.
 ↑ The Monroe News-Star, January 25, 2008
 ↑ The Monroe News-Star, November 10, 2007.
 ↑ Senator Francis C. Thompson. Louisiana State Senate. Retrieved on July 19, 2019.
 ↑ Candidate Inquiry (Richland Parish). Louisiana Secretary of State. Retrieved on August 9, 2019.
 ↑ "Social Security Death Index". ssdi.rootsweb.com. Retrieved January 2, 2010; now under pay wall.
 ↑ Marilyn Thompson of Delhi, Louisiana. Mylife.com. Retrieved on July 19, 2019.
 ↑ Net Detective, People Search.
 ↑ Clyde Nolan Thompson. Findagrave.com. Retrieved on July 19, 2019.
 ↑ "Rep. Thompson to Head Agriculture Committee". Legis.state.la.us, accessed January 5, 2009.
 ↑ "Master Farmer program cited as good example for nation,"  Deltafarmpress.com., November 12, 2004, accessed January 2, 2010.
 ↑ "How Much Better Can Louisiana Politics Get?", Louisianaconservative.com., accessed January 5, 2009.
 ↑ 14.0 14.1 Michael DeVault (June 26, 2008). Federal grand jury indicts district attorney. The Ouachita Citizen. Retrieved on July 19, 2019.
 ↑ Louisiana election returns, September 21, 1996," Sos.louisiana.gov., accessed January 2, 2009.
 ↑ Louisiana election returns, November 5, 1996, Sos.louisiana.gov, accessed  January 2, 2009.
 ↑ 33 State Legislators Announce Support for Hunt Downer (LA Gov). Free Republic (June 23, 2003). Retrieved on July 19, 2019.
 ↑ New inductions, Louisiana Political Museum and Hall of Fame," Cityofwinnfield.com., accessed August 22, 2009.
 ↑ House approves renaming Louisiana School for Math, Science and the Arts. KSA-TV (June 5, 2017). Retrieved on July 19, 2019.
 Louisiana People Business People Educators Politicians State Senators Democrats Presbyterians Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 17 September 2019, at 10:05. This page has been accessed 1,125 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 In office In office Incumbent Assumed office  Francis Coleman Thompson Francis Coleman Thompson
  
 Louisiana State Representative for all or portions of East Carroll, Madison, Morehouse, Ouachita, Richland, and West Carroll parishes
 In office1975–2008
  Charles "Bubba" Chaney
  Charles "Bubba" Chaney
 Louisiana State Senator for Concordia, East Carroll, Madison, Ouachita, Richland, and Tensas parishes
 In officeJanuary 2008 – January 2020
  Charles Jones
 Louisiana State Representative for District 19 (East Carroll, Madison, Morehouse, Ouachita, Richland, and West Carroll parishes)
  Incumbent
 Assumed office January 2020
 
  October 29, 1941
  Democrat
  Marilyn Bryant Thompson
  Francis Todd Thompson
Brant L. Thompson
Melissa T. Blanchfield
  Delhi, Richland Parish, Louisiana
  Louisiana Tech University
University of Louisiana at Monroe
  Presbyterian
 Francis C. Thompson Contents Background Interest in agriculture Poverty Point Other political developments References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
