(2) Violet McMurty Culbertson (married 1952)
(3) Evelyn H. Johnson Davis Culbertson
Children:
Douglas Floyd Culbertson (1953–1979)
Stepsons:
Bryan, Allan, and Darrell Davis
 Louisiana College
Dedman School of Law at Southern Methodist University
 Floyd Douglas Culbertson, Jr. (April 15, 1908 – April 28, 1989), was a lawyer in Louisiana, Texas, and Oklahoma, who served from 1940 to 1942 as the mayor of his hometown, Minden, Louisiana.[1] He resigned early in his second term to enter the United States Army with stateside service in World War II.
 Culbertson's mother was the former Mary Leana "Mollie" Alford (1887–1977); his father, Floyd Culbertson, Sr. (1879–1958),[2] is mentioned in 1894 at the age of fifteen in the publication, The Southern Cultivator and Industrial Journal.[3] Culbertson had a sister and three brothers.[2][2][4][4] He graduated in 1925 from Minden High School[5] and in 1930 from the Southern Baptist affiliated Louisiana College in Pineville.[6]
 Culbertson studied the law privately and was admitted to the bar in Minden in 1937. In 1952, he graduated from the Dedman School of Law at Southern Methodist University in University Park, Texas.[7]
 In 1936, Culbertson was an unsuccessful candidate for the Webster Parish seat in the Louisiana House of Representatives. He finished third, with 1,118 votes, in a four-way Democratic primary election. The winner of the position, Drayton Boucher of Springhill, unseated incumbent Eddie Nuton Payne (1873-1951) in a runoff contest a few weeks later.[8]
 In the 1940 primary for mayor, Culbertson unseated David William Thomas, who was seeking a third consecutive two-year term. McIntyre H. Sandlin (1870-1955), a former state representative, Minden mayor, and tax assessor, led in the primary with 668 votes to Culbertson's 596 and Thomas' 345 ballots.[9] In the runoff election, Culbertson polled 827 votes to Sandlin's 780.[10] In the fall of 1940, Mayor Culbertson appointed nine men to the Webster Parish Selective Service Board, including Paul L. Miller, an oilman from the Couchwood community near Cotton Valley who earlier that year had lost a race for the Louisiana State Senate to Culbertson's former opponent for state representative, Drayton Boucher. The other appointees included Dr. Claude M. Baker (1895–1975) and two prominent Minden attorneys, John T. Campbell (1903–1993), and Daniel Webster Stewart Jr. (1897–1982).[11]
 In 1942, Culberton won his second term as mayor over his predecessor, David Thomas, 770 to 355 votes. Mayoral terms, then for two years, were expanded to four in 1954 with John Thomas David.[12] Soon after his reelection, Culbertson ran unsuccessfully in the 1942 primary for district attorney of the 26th Judicial District.[13] Culbertson polled 1,431 votes in the primary.[14] The position was decided in a runoff contest in which Arthur M. Wallace of Benton, an interim appointee of Governor Sam Houston Jones, defeated Minden attorney Graydon K. Kitchens, Sr.,[15] a former law partner of subsequent Governor Robert F. Kennon and later a Kennon appointee to the Louisiana Tax Commission. Kennon himself had served as mayor of Minden from 1926 to 1928.[16]
 In November 1942, Culbertson resigned as mayor to enter the Army National Guard.[17][18] Culbertson advanced to the rank of lieutenant by October 1943.[19] 
His secretary, Zenobia Camp West[20]  (1919–2008),[21] who later became a registered nurse, left as well to work with Edwin Sanders Richardson, the former Webster Parish school superintendent and past president of Louisiana Tech University in Ruston, in a program to find housing for workers moving to Minden to take jobs at the since defunct Louisiana Army Ammunition Plant,  which was opened early in the war during the last few months that Culbertson was still the mayor.[22] John Calhoun Brown,[23] a member of the Minden City Council since 1932,[24] served as mayor pro tem for the remainder of Culbertson's term until the spring of 1944, when J. Frank Colbert, a former member of the state legislature, was elected to the position. Culbertson spent most of his military service assigned to the Judge Advocate General office in the Brooklyn borough of New York City before he re-opened his law office in Minden in December 1946.[25]
 In 1947, Culbertson joined Minden businessman Larkin L. Greer (1902–1991) and future state Representative Ernest Dewey Gleason as co-chairmen of the Webster Parish "Kennon Club" to support Judge Robert Kennon for governor. Kennon, however, was eliminated in the Democratic primary early in 1948. Former Governor Earl Long handily defeated former Governor Sam Jones in a runoff contest. In 1940, Jones had unseated Long, who held the office for the preceding year.[26]
 Except for the years in which he was in the military, Culbertson headed the Red Cross office in Webster Parish from 1938 to 1948, when Minden businessman Willard Roberts (1899–1994) assumed those duties.[27] In 1950, Culbertson and his political opponent, former Mayor David Thomas, were opposing counsel in a legal dispute over a $196 debt deemed collectible to the plaintiff by City Judge Richard Harmon Drew Sr. The case was appealed unsuccessfully to the Louisiana Court of Appeal for the Second Circuit in Shreveport.[28] On March 8, 1952, Culbertson was admitted to the practice of law in Texas, his law office was located in Dallas and later in Tulsa, Oklahoma.[7] In 1977, when his mother died, Culbertson was living in Keller in suburban Tarrant County, Texas.[2][4]
 In 1933, Culbertson married Gladys Day (1907–1995), daughter of William Hartwell Day, Sr. (1884–1959) and Minnie W. Day (1888–1964) of Gibsland in neighboring Bienville Parish. Gladys was a legal secretary and real estate agent, who operated from her husband's law office. In 1940, Floyd Culbertson was listed in the census as a "single" lodger in Minden.[29] Gladys Culbertson was listed as a married high school teacher in the 1940 census but living with her mother and two adult siblings in Gibsland.[30] In a February 1940 article in The Minden Herald, Culbertson was listed as still married to Gladys Day. They were apparently divorced a few weeks later.[6]
 In 1952, Culbertson wed in Tulsa the former Violet McMurty (1921–1970). Their son, Douglas Floyd Culbertson, was born in San Gabriel, California, on September 5, 1953. A brilliant law student at the University of Texas at Austin, Douglas Culbertson died of cancer in Austin on March 19, 1979, at the age of twenty-five.[31] Violet died of cancer in Tulsa in January 1970. Douglas graduated from Memorial High School in Tulsa and received a National Merit Scholarship to Princeton University, from which he graduated in 1976 Phi Beta Kappa summa cum laude. His history thesis was judged the best in the department. He was recognized statewide for his piano talent. An unnamed member of the Austin law firm where he had been working while in school recalled: "In Doug Culbertson we lost a lawyer who would have brought honor and dignity to the profession by standing up tall true to his ideals, true to his word and obligations, and sensitive to the proprieties and the interests of others." A Douglas Culbertson Memorial Fund was established at Princeton in his honor,[31] but, without a sponsor, it has since lapsed.[32]
 Culbertson's third wife was the former Evelyn Davis (1915-2006),[33] a native of Carrington in Foster County in east central North Dakota, who was residing in Ocala, Florida, at the time of her death in 2006 at the age of ninety-one.[34] She obtained her Ph.D. from the University of Maryland at College Park.. As "Evelyn Davis," she was from 1965 to 1980 a professor and director of the music education program at the private Oral Roberts University in Tulsa. She retired as professor emeritus.[35] In 1992, she published the documentary work, He Heard America Singing: Arthur Farwell, Composer and Crusading Music Educator, a study of the American composer Arthur Farwell, based on thirty years of research, beginning with her dissertation.[36][37]  
 From Evelyn's first marriage to Joseph M. Davis, she had three sons, Bryan, Allan, and Darrell Davis, all of Ocala, the stepsons of Floyd Culbertson.[33]
 Floyd and Evelyn Culbertson and Floyd's second wife, Violet, are interred at Memorial Park Cemetery in Tulsa.[38]
[39] First wife Gladys Culbertson, who did not remarry, is interred at Bear Creek Cemetery in Bienville Parish, Louisiana.[40]
 1 Background 2 Career 3 Marriages and children 4 References ↑ 
Louisiana. Attorney General's Office (1940). Opinions of the Attorney General of the State of Louisiana. Moran Industries, Incorporated. 
 ↑ 2.0 2.1 2.2 2.3 "Funeral Services Held Tuesday for Floyd Culbertson Sr.", The Minden Herald, September 11, 1958, p. 5.
 ↑ The Young Folks. Southern Cultivator and Industrial Journal, Vol. 52 (February 1894). Retrieved on March 10, 2015.
 ↑ 4.0 4.1 4.2 Mary Culbertson (mother of Floyd Culbertson Jr.) obituary, Minden Press-Herald, September 20, 1977, p. 3.
 ↑ Minden High School, 1925 The Grig yearbook (lower left link). mindenmemories.com. Retrieved on June 5, 2009.
 ↑ 6.0 6.1 "Culbertson Tells Qualifications: Said Sole Promise to Make City a Good Mayor", Minden Herald, February 16, 1940, pp. 1, 8.
 ↑ 7.0 7.1 Floyd D. Culbertson Jr.. Texas Bar Association. Retrieved on March 8, 2015.
 ↑ Minden Signal-Tribune and Springhill Journal, January 28, 1936, p. 2.
 ↑ "Culbertson and Sandlin to Be in Runoff for Mayor of Minden", Minden Herald, January 19, 1940, p. 1.
 ↑ "Culbertson Is Named Next Mayor of Minden in Tuesday's Balloting", Minden Herald, February 23, 1940, p. 1.
 ↑ "Mayor Names Draft Board for Webster: Selection Is Made after Mayor Receives Request from Civil Service Commission", Minden Herald, September 20, 1940, p. 1.
 ↑ Election results, Webster Review and Signal Tribune, April 14, 1942, p. 1.
 ↑ Minden Herald, July 13, 1942, p. 1.
 ↑ "Kitchens and Wallace to Be in DA Runoff", Minden Herald, September 11, 1942, p. 1.
 ↑ Minden Herald, September 11, 1942, p. 1.
 ↑ Webster Review and Signal Tribune, September and October 1942.
 ↑ ”Culbertson to Enter Army: Two Boards Attempt to Line Mayor up for Army Service”, Webster Tribune, October 27, 1942, p. 1.
 ↑ 
Evans J. Casso (1976). Louisiana Legacy: A History of the State National Guard. Gretna, Louisiana: Pelican Publishing Company. ISBN 1-56554-546-X. Retrieved on March 10, 2015. 
 ↑ "Newcomers here are Listed by Merchant's Group (Galveston Merchant's Association)", October 16, 1943, p. 8. “Lt. Floyd D. Culbertson ... from Minden, La.” 
 ↑ Michelle Bates (September 15, 2016). Zenobia West band uniform donated to Dorcheat Museum. The Minden Press-Herald. Retrieved on January 18, 2017.
 ↑ Zenobia Camp West (secretary of Mayor Floyd Culbertson). findagrave.com. Retrieved on March 10, 2015.
 ↑ Juanita Agan (September 19, 2008). Remembering my friend, Zenobia Camp West. The Minden Press-Herald in mindenmemories.net. Retrieved on March 10, 2015.
 ↑ Earlene Mendenhall Lyle, Minden Cemetery: A Peaceful Resting Place," June 2004, p. 65.
 ↑ "J. C. Brown re-elected", Minden Herald, May 10, 1934, p. 1.
 ↑ "Law offices of former mayor re-opened Wednesday", Minden Herald, December 20, 1946, p. 1.
 ↑ Minden Herald, November 7, 1947, p. 1.
 ↑ "W. Roberts Named to Head Red Cross in Webster Parish," Minden Herald, November 26, 1948, p. 1.
 ↑ Brown v. Harvey. casetext.com (April 5, 1950). Retrieved on March 7, 2015.
 ↑ Floyd D. Culbertson Jr., Minden, Louisiana. United States of America, Bureau of the Census. Sixteenth Census of the United States, 1940. National Archives and Records Administration. Retrieved on March 8, 2015.
 ↑ Gladys D. Culbertson in the 1940 census. United States of America, Bureau of the Census. Sixteenth Census of the United States, 1940. National Archives and Records Administration. Retrieved on March 23, 2015.
 ↑ 31.0 31.1 
 (September 10, 1979) Memorials: Douglas Floyd Culbertson. Princeton Alumni Weekly, Vol. 80. Retrieved on March 17, 2015. 
 ↑ Endowed Undergraduate Scholarships (A-E). Princeton University. Retrieved on March 17, 2015.
 ↑ 33.0 33.1 Dr. Evelyn Davis Culbertson (third wife of Floyd Culbertson, Jr.). Ocala Star-Banner (August 8, 2006). Retrieved on March 16, 2015.
 ↑ Culbertson born in 1915. faqs.org. Retrieved on March 10, 2015.
 ↑ Emeriti Faculty. Oral Roberts University. Retrieved on March 17, 2015.
 ↑ 
 (1992) He Heard America Singing: Arthur Farwell, Composer and Crusading Music Educator. Scarecrow Press, 885 total. ISBN 978-0810825802. Retrieved on March 17, 2015. 
 ↑ Davis, Evelyn H. Johnson (1972). The Significance of Arthur Farwell as an American Music Educator. University of Maryland at College Park. Retrieved on March 28, 2015.
 ↑ Floyd Douglas Culbertson, Jr.. Old.findagrave.com. Retrieved on January 26, 2018.
 ↑ Violet McMurty Culbertson. Old.findagrave.com.
 ↑ Gladys Day Culbertson. Oldfindagrave.com. Retrieved on January 26, 2018.
 Louisiana People Texas Oklahoma Attorneys Mayors Politicians Democrats United States Army World War II Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 2 October 2019, at 08:46. This page has been accessed 5,336 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 In office Floyd Douglas Culbertson, Jr. Floyd Douglas Culbertson, Jr.
 Mayor of Minden, Webster Parish Louisiana
 In officeJuly 1940 – November 1942
  David William Thomas
  John Calhoun Brown (previous Mayor Pro-tem)
 
  April 15, 1908Minden, Louisiana
  April 28, 1989 (aged 81)Tulsa, Oklahoma
  Memorial Park Cemetery in Tulsa
  American
  Democrat
  (1) Gladys Day Culbertson (married 1933, divorced)
(2) Violet McMurty Culbertson (married 1952)
(3) Evelyn H. Johnson Davis Culbertson
Children:
Douglas Floyd Culbertson (1953–1979)
Stepsons:
Bryan, Allan, and Darrell Davis
  Minden High School
Louisiana College
Dedman School of Law at Southern Methodist University
  Attorney
 Floyd D. Culbertson, Jr. Contents Background Career Marriages and children References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
