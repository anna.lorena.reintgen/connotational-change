Christianity.com is a site owned and operated by Salem Web Network and headquartered in Richmond, Virginia. The stated focus of Christianity.com is to provide Christian content and interactive tools to help people understand Christianity. The site has a conservative, Bible-based theological tone. Pastors, authors, and speakers such as John F. MacArthur, Adrian Rogers, Kay Arthur, Chuck Swindoll, Hank Hanegraff, John Piper (theologian), and more, contribute to the site by way of articles, FAQ, ministry video & audio, and more.
 Traffic for the site is between 2 and 3 million page views per month.[1]
 When it originally debuted in 1999, Christianity.com was headquartered in Silicon Valley, California. Spencer Jones from Christian Broadcasting Network (who invested $10 million in the startup) and David Davenport, who was head of Pepperdine University for 10 years, served as COO and CEO. Other funding and credit partners are Sequoia Capital, which invested $10 million, and Comdisco Ventures Group, which loaned $10 million for equipment and services.[2] In the middle of the dotcom bust the company went bankrupt and on December 18, 2001, the domain name was purchased by a successful startup.com named Renewal Enterprises, LLC,[3] located in Alexandria, Virginia, which had also started in 1999, but to much less fanfare.
 Salem Web Network announced the acquisition of Christianity.com from Renewal Enterprises on February 11, 2005 for approximately $3.4 million.[4]
 "This site is foundational to our efforts to serve the vast audience interested in this content," said Jim Cumbee, Salem's President of Non-Broadcast Media. "The site will undergo a complete re-design and substantial upgrade of services and features for the millions of people who are seeking compelling Christian content and faith-influenced services.".[5]
 Christian Foundations
 Christianity.com offers information on basic tenets of Christianity,[6] authorship and inspiration of the Bible,[7] questions and answers to frequently asked questions about Christian doctrine,[8] how to grow spiritually,[9] and how live as a Christian.[10]
 
Community
 The site features a Christian community section for fellowship, sharing life's events, and asking questions. It features forums for various topics and concerns as well as Christian chat.
 
Devotionals
 Christianity.com includes a free devotional section with topical devotions including; Favorite Pastors, For Women, Spiritual Growth, Classics, Lifestyle, Family, Bible Reading, and more.[11] Devotionals are contributed by popular ministry leaders including:
 
Bible Study Tools
 Christianity.com launched it's new interactive Bible Study Tools in 2007. The new personalizable application features an extensive amount of Biblical content for personal Bible study including 29 Bible translations (7 contain the Apocryphal books), 12 commentaries, 4 concordances, 5 dictionaries, 2 encyclopedias, 2 lexicons (1 Hebrew, 1 Greek), and Biblical maps.[19][20]
 1 Traffic 2 History 3 Content 4 References 5 External links 6 See also Max Lucado[12] Jack Graham[13] Charles Spurgeon[14] F.B. Meyer[15] Charles Stanley[16] John Piper[17] Chuck Swindoll,[18] and more. ↑ Salem Web Network
 ↑ God.com Dead? | Christianity Today | A Magazine of Evangelical Conviction
 ↑ Renewal Enterprises LLC acquires Starwire
 ↑ Salem Communications Corporation - Investor Relations - Press Release
 ↑ Salem Communications Corporation - Investor Relations - Press Release
 ↑ What is Christianity? - The essentials - Christian foundations - Christianity.com
 ↑ Our God-Breathed Bible - The bible - Christian foundations - Christianity.com
 ↑ Our God-Breathed Bible - The bible - Christian foundations - Christianity.com
 ↑ Christianity.com - Rich Christian Content for all Areas of Life from Trusted Christian Leaders
 ↑ Spiritual Intimacy: Fulfilling God's Plan for Your Marriage - Features - Christian living - Christianity.com
 ↑ Devotionals - Christianity.com
 ↑ Upwords - Devotionals - Christianity.com
 ↑ Powerpoint - Devotionals - Christianity.com
 ↑ Morningandevening - Devotionals - Christianity.com
 ↑ Dailyhomily - Devotionals - Christianity.com
 ↑ In_touch - Devotionals - Christianity.com
 ↑ Desiringgod - Devotionals - Christianity.com
 ↑ Daybyday - Devotionals - Christianity.com
 ↑ Bible Study Tools
 ↑ Bible Study Tools & Resources with Study Guides - Software and Bibles
 Christianity.com Salem Communications Salem Web Network Fundamentalism Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 29 June 2016, at 09:54. This page has been accessed 1,809 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Christianity.com Christian Foundations Community Devotionals Bible Study Tools Christianity.com Contents Traffic History Content References External links See also Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
