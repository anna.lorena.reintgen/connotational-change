The Second Amendment to the United States Constitution states:[1]
 For several decades, the lower federal courts had interpreted the Second Amendment as protecting merely a collective right of state militias.[2] However, the U.S Supreme Court has always called it an individual right. The 2008 Supreme Court decision of District of Columbia v. Heller ruled 5–4 that the Second Amendment protects an individual right. This decision was expanded upon in McDonald v. Chicago two years later, which ruled that individual Second Amendment rights apply to the states.
 In 1786, the United States existed as a loose national government under the Articles of Confederation. This confederation was perceived to have several weaknesses, among which was the inability to mount a Federal military response to an armed uprising in western Massachusetts known as Shays' Rebellion.
 In 1787, to address these weaknesses, the Constitutional Convention was held with the idea of amending the Articles. When the convention ended with a proposed Constitution, those who debated the ratification of the Constitution divided into two camps; the Federalists (who supported ratification of the Constitution) and the Anti-Federalists (who opposed it).
 Among their objections to the Constitution, anti-Federalists feared a standing army that could eventually endanger democracy and civil liberties. Although the anti-Federalists were unsuccessful at blocking ratification of the Constitution, through the Massachusetts Compromise they insured that a Bill of Rights would be made, which would provide constitutional guarantees against taking away certain rights.
 One of those rights was the right to bear arms.  This was intended to prevent the Federal Government from taking away the ability of the states to raise an army and defend itself and arguably to prevent them from taking away from individuals the ability to bear arms.
 The U.S. Supreme Court has heard only three major Second Amendment cases in the last 140 years:
 The meaning of this amendment is controversial with respect to gun control and what type of weapons are covered.
 The National Rifle Association, which supports gun rights, has a stone plaque in front of its headquarters bearing the words "The right of the people to keep and bear arms shall not be infringed." By dropping the "militia" phrase, the slogan advocates that the Second Amendment should mean that individual citizens have the right to own and use guns. 
 American law has always said that the militia includes ordinary private citizens, and gun rights advocates say that the amendment means individuals have the right to own and use guns. Gun control advocates began in the late 20th century to say it means only that there is only some sort of collective or state-controlled right.
 Supreme Court opinions have all been consistent with the individual rights interpretation of the Second Amendment, but the lower court opinions are mixed. 
 As of 2007, people argue about the meaning of the Second Amendment, but there is no definitive answer. The latest ruling is Parker v District of Columbia, in which the DC Circuit court of appeals ruled on March 9, 2007, that the DC gun ban violated individual rights under the Second Amendment.
 A well regulated militia being necessary to the security of a free state, the right of the people to keep and bear arms shall not be infringed.'''''
 Quoted from:
 Down to the Last Second (Amendment)
 Participants in the various debates on firearms, crime, and constitutional law may have noticed that the Second Amendment is often quoted differently by those involved. The two main variations differ in punctuation — specifically, in the number of commas used to separate those twenty-seven words. But which is the correct one? The answer to this question must be found in official records from the early days of the republic when the Bill of Rights was sent by the First Congress to the states for ratification. Therefore, a look at the progression of this declaratory and restrictive clause from its inception to its final form is in order.
 Before beginning, one must note that common nouns, like "state" and "people," were often capitalized in official and unofficial documents of the era. Also, an obsolete formation of the letter "s" used to indicate the long "s" sound was in common usage. The long 's' is subject to confusion with the lower case  'f', therefore, Congress" is sometimes spelled as "Congrefs," as is the case in the parchment copy of the Bill of Rights displayed by the National Archives. The quotations listed here are accurate. With the exception of the omission of quotations marks, versions of what is now known as the Second Amendment in boldface appear with the exact spelling, capitalization, and punctuation as the cited originals.
 During ratification debates on the Constitution in the state conventions, several states proposed amendments to that charter. Anti-Federalist opposition to ratification was particularly strong in the key states of New York and Virginia, and one of their main grievances was that the Constitution lacked a declaration of rights. During the ratification process, Federalist James Madison became a champion of such a declaration, and so it fell to him, as a member of the 1st Congress, to write one. On June 8, 1789, Madison introduced his declaration of rights on the floor of the House. One of its articles read:
 
 On July 21, John Vining of Delaware was appointed to chair a select committee of eleven to review, and make a report on, the subject of amendments to the Constitution. Each committeeman represented one of the eleven states (Rhode Island and North Carolina had not ratified the Constitution at that time), with James Madison representing Virginia. Unfortunately, no record of the committee's proceedings is known to exist. Seven days later, Vining duly issued the report, one of the amendments reading:
 
 In debates on the House floor, some congressmen, notably Elbridge Gerry of Massachusetts and Thomas Scott of Pennsylvania, objected to the conscientious objector clause in the fifth article. They expressed concerns that a future Congress might declare the people "religiously scrupulous" in a bid to disarm them, and that such persons could not be called up for military duty. However, motions to strike the clause were not carried. On August 21, the House enumerated the Amendments as modified, with the fifth article listed as follows:
 
 Finally, on August 24, the House of Representatives passed its proposals for amendments to the Constitution and sent them to the Senate for their consideration. The next day, the Senate entered the document into their official journal. The Senate Journal shows Article the Fifth as:
 
 On September 4, the Senate debated the amendments proposed by the House, and the conscientious objector clause was quickly stricken. Sadly, these debates were held in secret, so records of them do not exist. The Senators agreed to accept Article the Fifth in this form:
 
 In further debates on September 9, the Senate agreed to strike the words, "the best," and replace them with, "necessary to the." Since the third and fourth articles had been combined, the Senators also agreed to rename the amendment as Article the Fourth. The Senate Journal that day carried the article without the word, "best," but also without the replacements, "necessary to." Note that the extraneous commas have been omitted:
 
 With two-thirds of the Senate concurring on the proposed amendments, they were sent back to the House for the Representatives' perusal. On September 21, the House notified the Senate that it agreed to some of their amendments, but not all of them. However, they agreed to Article the Fourth in its entirety:
 
 By September 25, the Congress had resolved all differences pertaining to the proposed amendments to the Constitution. On that day, a Clerk of the House, William Lambert, put what is now known as the Bill of Rights to parchment. Three days later, it was signed by the Speaker of the House, Frederick Augustus Muhlenberg, and the President of the Senate, Vice President John Adams. This parchment copy is held by the National Archives and Records Administration, and shows the following version of the fourth article:
 
 The above version is used almost exclusively today, but aside from the parchment copy, the author was unable to find any other official documents from that era which carry the amendment with the extra commas. In fact, in the appendix of the Senate Journal, Article the Fourth is entered as reading:
 
 Also, the Annals of Congress, formally called The Debates and Proceedings in the Congress of the United States, show the proposed amendment as follows:
 
 Further, once two-thirds of both chambers of the Congress agreed to the proposed amendments, the House passed a resolve to request that the President send copies of them to the governors of the eleven states in the Union, and to those of Rhode Island and North Carolina. The Senate concurred on September 26, as recorded in their journal:
 
 Fortunately, an original copy of the amendments proposed by the Congress, and sent to the State of Rhode Island and the Providence Plantations, does survive. Certified as a true copy by Assembly Secretary Henry Ward, it reads in part:
 
 And so, the proposed amendments to the Constitution were sent to the states for ratification. When notifying the President that their legislatures or conventions had ratified some or all of the proposed amendments, some states attached certified copies of them. New York, Maryland, South Carolina, and Rhode Island notified the general government that they had ratified the fourth amendment in this form:
 Article the Fourth. A well regulated militia being necessary to the security of a free State, the right of the People to keep and bear arms shall not be infringed.[15]
 Articles the First and Second were not ratified by the required three-fourths of the states, but by December 15, 1791, the last ten articles were. These, of course, are now known as the Bill of Rights. Renumbering the amendments was required since the first two had not been ratified. The 1796 revision of The Federalist on the New Constitution reflects the change as such:
 
 A well regulated militia being necessary to the security of a free state, the right of the people to keep and bear arms shall not be infringed.[16]
 This version still appears today, as is the case with the annotated version of the Constitution they sponsored on the Government Printing Office web site (1992, supplemented in 1996 and 1998). The Second Amendment is shown as reading:
 
 Thus, many scholars argue that correct rendition of the Second Amendment carries but a single comma, after the word "state." It was in this form that those twenty-seven words were written, agreed upon, passed, and ratified.
 Why the Commas are Important
 It is important to use the proper Second Amendment because it is clearly and flawlessly written in its original form. Also, the function of the words, "a well regulated militia being necessary to the security of a free state," are readily discerned when the proper punctuation is used. On the other hand, the gratuitous addition of commas serves only to render the sentence grammatically incorrect and unnecessarily ambiguous.
 Liberals have made various efforts to subvert the Second Amendment by enacting unconstitutional gun laws which restrict the ability of individuals to protect themselves against the excesses of government. Examples include:
 The drafters of the Second Amendment probably had muskets and handguns in mind, but courts are now struggling to define the weapons covered by the "right to bear arms" in the 21st Century.
 For example, the Connecticut Supreme Court in State v. DeCiccio (2015) upheld the constitutionality of a state law prohibiting the transportation of a police batton or dirk knife in a motor vehicle. Yet, the Court struck down the statute to the extent that it infringed on the defendant's right to transport them between his old and new residences.
 There are also lower court cases finding Second Amendment protection for tasers, billy clubs, switchblades and other weapons that are less lethal than handguns.[22]
 Bill of Rights:
1 - Freedom of speech, press, religion, etc. 
2 - Right to bear arms 
3 - Quartering of soldiers 
4 - Warrants 
5 - Due process 
6 - Right to a speedy trial 
7 - Right by trial of a jury 
8 - No cruel or unusual punishments 
9 - Unenumerated rights 
10 - Power to the people and states 
 11 - Immunity of states to foreign suits
12 - Revision of presidential election procedures
13 - Abolition of slavery
14 - Citizenship
15 - Racial suffrage
16 - Federal income tax
17 - Direct election of the United States Senate
18 - Prohibition of alcohol
19 - Women's suffrage
20 - Terms of the presidency
21 - Repeal of Eighteenth Amendment
22 - Limits the president to two terms
23 - District of Columbia Voting for President
24 - Prohibition of poll taxes
25 - Presidential disabilities
26 - Voting age lowered to 18
27 - Variance of congressional compensation
 
 1 Individual or Collective Right? 2 Where the Second Amendment comes from 3 Supreme Court Decisions 4 Comments
4.1 Individual right
4.2 The One Comma vs. The Three Comma Debate
4.3 Chronological History
 4.1 Individual right 4.2 The One Comma vs. The Three Comma Debate 4.3 Chronological History 5 Liberal targeting 6 Weapons covered 7 Further reading 8 Footnotes 9 See also 10 External links United States v. Miller (1939) (held that the Second Amendment did not guarantee a citizen's right to possess a sawed-off shotgun because that weapon had not been shown to be "ordinary military equipment" that could "contribute to the common defense") District of Columbia v. Heller (2008) (a 5–4 decision written by Justice Antonin Scalia that recognized an individual right under the Second Amendment) McDonald v. Chicago (2010) (merely extended the Heller decision to apply against the States too).  news reports appealing predominately to emotion in the aftermath of mass shootings while ignoring merits of self-defense   celebrity-showings demanding a plan to end mass shootings  gun "buy back" programs officiated by city police Charles, Robert (March 28, 2018). If the Second Amendment falls, our entire Bill of Rights falls. Fox News. ↑ This is the most-often quoted form of this Amendment, but note later in this entry the significant objection to including three commas rather than just one.
 ↑ U.S. House Journal. 1st Cong., 1st sess., 21 August 1789.
 ↑ U.S. Senate Journal. 1st Cong., 1st sess., 25 August 1789.
 ↑ U.S. Senate Journal, 1st Cong., 1st sess., 4 September 1789.
 ↑ U.S. Senate Journal, 1st Cong., 1st sess., 9 September 1789.
 ↑ U.S. House Journal. 1st Cong., 1st sess., 21 September 1789.
 ↑ U.S. Senate Journal. 1st Cong., 1st sess., Appendix.
 ↑ Annals of Congress, 1st Cong., 1st sess., Appendix
 ↑ U.S. Senate Journal. 1st Cong. 1st sess., 26 September 1789.
 ↑ U.S. House Journal, 1st Cong., 3rd sess., Appendix Note: Maryland and South Carolina capitalized the "m" in "Militia."
 ↑ Statutes at Large 1845, 21.
 ↑ Volokh, Eugene (December 16, 2014). Second Amendment protects dirk knives and police batons. Washington Post. Retrieved on February 7, 2015.
  List of state constitutions containing a RKBA provision  United States Constitution  Castle Doctrine  National Rifle Association  Gun Owners of America  Veterans  Oath Keepers  Battle of Athens (1946)  List of instances of armed citizens stopping active shooters  Second Amendment Center, nraila.org.  Professor Eugene Volokh's comprehensive compilation of resources on the Second Amendment Second Amendment Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 28 October 2019, at 16:31. This page has been accessed 111,146 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 
Amendments to the Constitution of the United States of America
Bill of Rights:
1 - Freedom of speech, press, religion, etc. 
2 - Right to bear arms 
3 - Quartering of soldiers 
4 - Warrants 
5 - Due process 
6 - Right to a speedy trial 
7 - Right by trial of a jury 
8 - No cruel or unusual punishments 
9 - Unenumerated rights 
10 - Power to the people and states 
11 - Immunity of states to foreign suits
12 - Revision of presidential election procedures
13 - Abolition of slavery
14 - Citizenship
15 - Racial suffrage
16 - Federal income tax
17 - Direct election of the United States Senate
18 - Prohibition of alcohol
19 - Women's suffrage
20 - Terms of the presidency
21 - Repeal of Eighteenth Amendment
22 - Limits the president to two terms
23 - District of Columbia Voting for President
24 - Prohibition of poll taxes
25 - Presidential disabilities
26 - Voting age lowered to 18
27 - Variance of congressional compensation
 Second Amendment A well regulated militia, being necessary to the security of a free state, the right of the people to keep and bear arms, shall not be infringed. A well regulated militia being necessary to the security of a free state, the right of the people to keep and bear arms shall not be infringed.''''' See also: gun control  See also: Debate:Is the right to bear arms unalienable? See also: gun control 
See also: Debate:Is the right to bear arms unalienable?  “
  A well regulated militia, being necessary to the security of a free state, the right of the people to keep and bear arms, shall not be infringed.
  ”
  “
  The right of the people to keep and bear arms shall not be infringed; a well armed and well regulated militia being the best security of a free country: but no person religiously scrupulous of bearing arms shall be compelled to render military service in person.[3]
  ”
  “
  A well regulated militia, composed of the body of the people, being the best security of a free State, the right of the people to keep and bear arms shall not be infringed, but no person religiously scrupulous shall be compelled to bear arms.[4]
  ”
  “
  5. A well regulated militia, composed of the body of the People, being the best security of a free State, the right of the People to keep and bear arms shall not be infringed; but no one religiously scrupulous of bearing arms, shall be compelled to render military service in person.[5]
  ”
  “
  Art. V. A well regulated militia, composed of the body of the people, being the best security of a free state, the right of the people to keep and bear arms, shall not be infringed, but no one religiously scrupulous of bearing arms shall be compelled to render military service in person.[6]
  ”
  “
  ...a well regulated militia, being the best security of a free state, the right of the people to keep and bear arms, shall net be infringed.[7]
  ”
  “
  A well regulated militia being the security of a free state, the right of the people to keep and bear arms shall not be infringed.[8]
  ”
  “
  Resolved, That this House doth agree to the second, fourth, eighth, twelfth, thirteenth, sixteenth, eighteenth, nineteenth, twenty-fifth, and twenty-sixth amendments...[9]
  ”
  “
  Article the Fourth. A well regulated Militia, being necessary to the security of a free State, the right of the people to keep and bear Arms, shall not be infringed.[10]
  ”
  “
  Art. IV. A well regulated militia being necessary to the security of a free state, the right of the people to keep and bear arms shall not be infringed.[11]
  ”
  “
  Article the Fourth. A well regulated militia being necessary to the security of a free State, the right of the People to keep and bear arms shall not be infringed.[12]
  ”
  “
  Resolved by the Senate and House of Representatives of the United States of America in Congress assembled, That the President of the United States be requested to transmit to the executives of the United States, which have ratified the constitution copies of the amendments proposed by Congress, to be added thereto; and like copies to the executives of the states of Rhode Island and North Carolina.[13]
  ”
  “
  Article the Fourth, — A well regulated Militia being neceffary to the Security of a free State, the Right of the People to keep and bear Arms fhall not be infringed.[14]
  ”
  “
  ARTICLE THE SECOND
A well regulated militia being necessary to the security of a free state, the right of the people to keep and bear arms shall not be infringed.[16]
  ”
  “
  A well regulated Militia being necessary to the security of a free State, the right of the people to keep and bear Arms shall not be infringed.[20]
  ”
 v • d • e
FirearmsFirearmsRifles • Shotguns • Handguns • Pistols • Revolvers • Glossary •AmmunitionAmmunitions • Ammunition Category • Handloading • Bullets • Smokeless powder • Ammunition • Copper-jacketed lead • Ammunition shortage • Ammunition controlBroader IssuesHome security • Self-defense • Second Amendment • Concealed carry • Open carry • Constitutional carry • Gun control • Free fire zone • Gun rights • Gun enthusiast • Weapons • Knives  Firearms Rifles • Shotguns • Handguns • Pistols • Revolvers • Glossary •  Ammunition Ammunitions • Ammunition Category • Handloading • Bullets • Smokeless powder • Ammunition • Copper-jacketed lead • Ammunition shortage • Ammunition control  Broader Issues Home security • Self-defense • Second Amendment • Concealed carry • Open carry • Constitutional carry • Gun control • Free fire zone • Gun rights • Gun enthusiast • Weapons • Knives Second Amendment Contents Individual or Collective Right? Where the Second Amendment comes from Supreme Court Decisions Comments Liberal targeting Weapons covered Further reading Footnotes See also External links Navigation menu Individual right The One Comma vs. The Three Comma Debate Chronological History Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
