(Historian of the American Civil War and the U.S. South)​​
 Children: 
John Clifford Roland
Charles Franklin Roland
Karen Jean Roland Anderson​
Parents: ​​
Clifford Paul and Grace Paysinger Roland​
Alma mater:​
George Washington University
Louisiana State University​​
 Charles Pierce Roland (born April 8, 1918) is an American historian and professor emeritus of the University of Kentucky in Lexington, Kentucky. His research specialties are the American Civil War and the U.S. South. 
 Roland was born to Clifford Paul Roland and the former Grace Paysinger in rural Maury City in Crockett County in western Tennessee.[1] His 132-page My Odyssey Through History: Memoirs of War and Academe[2] mixes personal recollections with social commentary, particularly on the Great Depression, World War II, and his lengthy academic career.[3]
 The son and grandson of educators, Roland attended from 1934 to 1936 the Christian-affiliated Freed-Hardeman University (then a junior college) in nearby Henderson, Tennessee. He then transferred to Vanderbilt University in the capital city of Nashville, from which he graduated in 1938 at the age of twenty. From 1938 to 1940, he was a young schoolteacher at Alamo High School in Alamo, also in Crockett County. From 1940 to 1942 and again from 1946 to 1947, he was an historian for the National Park Service within the United States Department of the Interior in Washington, D.C.[1] After the 1941 Japanese attack on Pearl Harbor, Hawaii, Roland joined the United States Army as a captain in the infantry in the European Theatre of World War II. He fought in the 1944 Battle of the Bulge in Belgium. Roland earned a Purple Heart fighting in the Remagen Bridgehead, had close brushes with death, and mourned the loss of friends in battle. He witnessed the destruction of German cities.[3] He was also awarded a Bronze Star medal.[1] His memoir compares and contrasts World War II with the Civil War.[3]
 Using the GI Bill of Rights of 1944, Roland studied briefly at George Washington University in Washington, D.C., and then procured his Ph.D. in history in 1951 from Louisiana State University in Baton Rouge,[1] where his scholarly mentors included Francis Butler Simkins, specialist in southern studies, T. Harry Williams, the authority on both the Civil War and on U.S. Senator Huey Pierce Long, Jr., and Bell I. Wiley, the pioneer historian of the common soldiers of the Civil War. After his graduate studies, Roland returned to the military during the Korean War as the assistant to the chief historian of the Army. He then pursued his academic career, having taught from 1952 to 1970 at Tulane University in New Orleans, with service as department chairman from 1960 to 1970.[1] He then taught another eighteen years from 1970 to 1988 at UK in Lexington, Kentucky.[3]
 Roland's An American Iliad: The Story of the Civil War, was originally published in New York City in 1991 by McGraw Hill. The book compares the American war to the ancient fight between Greece and Troy from the standpoint of heroism and sacrifice.[4]
 The Improbable Era: The South since World War II is a study of the political, social, and economic changes that occurred in the American South in the thirty years since the end of World War II. The book ends before the massive partisan shift which later took root in the former one-party region but covers the civil rights movement and the rise of industrial development.[5]
 Louisiana Sugar Plantations during the Civil War was originally published in 1957 by E. J. Brill of Leiden, Netherlands, and re-released in 1997 by the Louisiana State University Press in Baton Rouge. The book is a study of the metamorphosis that sugar planters and farmers endured with the Union occupation of the cane sugar lands. Roland could not get the book published until the civil rights movement aroused new interest in 19th century southern slavery. His mentor, Francis Butler Simkins, advised him on the book.[6]
 Reflections on Lee: A Historian's Assessment is Roland's attempt to offer a balanced treatment of General Robert E. Lee, to show weaknesses beyond the strengths long attributed to the icon of what became known as "The Lost Cause."[7]
 Albert Sidney Johnston: Soldier of Three Republics examines the improbable career of the Kentucky-born Confederate general Albert Sidney Johnston (1803-1862) who was living in Texas at the outbreak of the Civil War. He was mortally wounded at the Battle of Shiloh in western Tennessee. Roland describes Johnston as "a leader of men ... He considered the welfare of the troops under him an inviolable trust. When hardship or hazard was their lot, he shared it without hesitation. Johnston's last words to his embattled troops were, 'I will lead you.' Small wonder that his associates looked upon him with deference. They 'not only respected him but loved him.'..."[8] Perhaps only the loss of General Stonewall Jackson in 1863 was greater to the Confederacy than the death of Johnston. 
 Roland's The Confederacy was published in 1962 as part of the series, The Chicago History of American Civilization, edited by Daniel Joseph Boorstin (1914-2004), the twelfth person to have been the Librarian of Congress. The Confederacy was released by the University of Chicago Press. A publisher's promotion brochure maintains that Roland heeds "careful attention to President Davis' struggle against dividing forces within. ... [and that] he skillfully narrates the attempt of the Confederacy to wage total war against superior forces. All the poignant events and conditions are here: the formation of the government, the Upper South's final commitment to the cause, the doomed attempts to combat the Northern blockade at home and Northern diplomacy overseas, an agrarian economy's heroic defiance of an industrial enemy, the desperate measures by which the Davis government tried to sustain the Confederacy, and, at last, the dissolution and flight of the administration in 1865."[9]
 In 1979, Roland was elected president of the Southern Historical Association.[10] He was the president too of the Louisiana Historical Association from 1969–1970.[11] Roland is a recipient of the Civil War Education Association's William Woods Hassler Award for notable achievements in the discipline.[3] From 1959 to 1965, he was a member of the U.S. Civil War Centennial Commission. From 1960–1961, he was a Guggenheim fellow.[1]
 Roland retired from UK in 1988 at the age of seventy but at 101 remains an active scholar and author. He resides in Lexington, Kentucky;[3] his wife, the former Allie Lee Aycock (1925-2018), whom he married in 1948, died three weeks after Professor Roland turned 100. Allie was a native of Buckeye in Rapides Parish, Louisiana, who attended Northwestern State University in Natchitoches, Louisiana, at which she procured a degree in home economics and became a registered dietitian. [12][1] The couple has two sons, John Clifford Roland and Charles Franklin Roland, and a daughter, Karen Jean Anderson.[1]
 1. 1.0 1.1 1.2 1.3 1.4 1.5 1.6 Who's Who in America, 1982–1983 (Chicago, Illinois: Marquis Who's Who, 1982), p. 2844.
2. Charles P. Roland, My Odyssey Through History: Memoirs of War and Academe (Baton Rouge: Louisiana State University Press, 2003; ISBN 978-0-8071-2853-4 cloth)
6. Louisiana Sugar Plantations during the Civil War, Leiden, Netherlands: E.J. Brill ; re-released in 1997 by the LSU Press.
7. Reflections on Lee: A Historian's Assessment (Baton Rouge: LSU Press, 1995; ISBN 0-8071-2911-9).
8. Albert Sidney Johnston: Soldier of Three Republics (Austin: University of Texas Press, 1964; re-released University Press of Kentucky, 2001, with new foreword by Gary W. Gallegher; ISBN 0-8131-9000-2).
10. "Historical News and Notices". JSTOR: The Journal of Southern History Vol. 48:1 (February 1982).
11. "Presidents of the Louisiana Historical Association," Lahistory.org, retrieved February 3, 2011; no longer on-line.
12. Allie Lee Roland obituary, Clark Legacy Center [1], April 27, 2018.
 1 Biographical sketch 2 Books 3 Associations 4 Retirement 5 References ↑ Allie Lee Roland obituary. Clark Legacy Center.com. Retrieved on September 24, 2019.
 Tennessee Louisiana People Kentucky Historians Educators American Authors United States Army World War II Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 12 October 2019, at 05:34. This page has been accessed 331 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Charles Pierce Roland​​ Born Spouse Children Parents Alma mater Charles Pierce Roland 
 
  Born
  April 8, 1918​​ Maury City, Crockett County, Tennessee, USA​​
 Spouse
  Allie Lee Aycock Roland (married 1948-2018, her death)​​
Children: 
John Clifford Roland
Charles Franklin Roland
Karen Jean Roland Anderson​
Parents: ​​
Clifford Paul and Grace Paysinger Roland​
Alma mater:​
George Washington University
Louisiana State University​​
 Charles P. Roland Contents Biographical sketch Books Associations Retirement References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
