Objects from the fictional Middle-earth legendarium.
 Aeglos (Sindarin "Snow Point" or "snow-thorn") was the spear of King Gil-galad of the Noldorin Elves. He was slain during the War of the Last Alliance, and the fate of Aeglos after his death is unknown.
 Herugrim was the sword of King Théoden of Rohan. It had been kept by Gríma Wormtongue along with "many other things that men had missed", but was taken back to Théoden when he rode to war.
 Glamdring (Sindarin: "Foe-hammer") was a sword forged for the Elven King Turgon in the First Age. It was missing for thousands of years until it was found alongside Orcrist and the dagger later known as Sting in a troll cave in The Hobbit, by Gandalf and company, who kept it for himself. It was made by the Elves of Gondolin in the First Age, and its blade glows blue if Orcs are near.
 Gúthwinë was the sword of Éomer, nephew of King Théoden of Rohan.
 The mithril-shirt was a small chainmail shirt made of mithril. It was given to Bilbo Baggins by Thorin Oakenshield from the hoard of the dragon Smaug in the Lonely Mountain. When Bilbo left the Shire in The Lord of the Rings, he took the mithril-shirt with him to Rivendell. Before the Fellowship set out from Rivendell, Bilbo gave Frodo Baggins his sword Sting and his mithril-shirt. The mithril-shirt was among the things taken from Frodo when he was imprisoned at the Tower of Cirith Ungol and later presented to his allies at the Morannon. Gandalf took  Frodo's things, which were later returned to him.
 Narsil was the sword of King Elendil. The sword was broken during the War of the Last Alliance, and its hilt-shard was used to cut the One Ring from Sauron's hand; the shards were later brought to Rivendell. In T.A. 3018, Narsil was reforged for Aragorn before he set out with the Fellowship of the Ring, upon which he renamed it Andúril (Sindarin: "Flame of the West").
 Orcrist (Sindarin: "Goblin-cleaver") was a sword forged by the Elves of Gondolin in the First Age, and its blade glows blue if Orcs are near. It was missing for thousands of years until it was found alongside Glamdring and the dagger later known as Sting in a troll cave in The Hobbit, by Thorin Oakenshield and company, who kept it for himself. After the Battle of Five Armies, Orcrist was placed on Thorin's tomb.
 Sting 
is the dagger or small sword wielded by Bilbo in The Hobbit and by Frodo Baggins in The Lord of the Rings. 
It was made by the Elves of Gondolin in the First Age.
Its blade glows blue if Orcs are near. 
It was found Bilbo and his companions 
alongside the swords Glamdring and Orcrist 
when they came upon the hoard of the trolls they had encountered earlier. 
Being even smaller than a dwarf, Bilbo used this dagger as a small sword. 
After defeating the spiders of Mirkwood he noted that it was more powerful than a spider's sting and named it accordingly. 
 When Bilbo left the Shire in The Lord of the Rings, he took Sting with him to Rivendell. Before the Fellowship set out on the quest of the Mount Doom, Bilbo gave Frodo his sword Sting and his mithril-shirt. Samwise Gamgee carries Sting for a while after leaving the supposedly dead Frodo, but later returns it to him.
 The Rings of Power were a group of twenty rings made by the Elves of Eregion and Sauron. Those who wore one of the Rings of Power were called Ring-bearers. In the Second Age, Sauron took on the guise of Annatar and came to the Elves of Eregion to teach them how to forge rings of power. Together with Annatar the Elves made the Nine and the Seven. The Three were forged by Celebrimbor without Sauron knowing. Sauron himself made the One Ring.
 The Nine Rings were given to nine kings and leaders of Men. They became the Nazgûl, servants of Sauron.
 The Seven Rings were given to the Dwarves, but Sauron could never gain control over them. By the end of the Third Age, all of the Seven had been destroyed or retaken by Sauron.
 The Three Rings of the Elves were made by Celebrimbor. These three were not influenced by Sauron, and were free of Sauron's taint, but later were still bound to the One Ring.
 Vilya, the Ring of Air or Ring of Sapphire was one of the Three Rings. It was a golden ring with a blue stone. Celebrimbor sent the ring Vilya Gil-galad, who later gave it to Elrond before his death in the Siege of Barad-dûr.
 Nenya, the Ring of Adamant and the Ring of Water, was one of the Three Rings. It was a ring made of mithril and set with a white stone. The ring Nenya was given to the Lady Galadriel, who used it to preserve and protect Lothlórien.
 Narya, also named the Ring of Fire or Red Ring, was one of the Three Rings. It was a ring with a red stone. According to the Unfinished Tales, Celebrimbor sent Narya together with the ring Vilya to Gil-galad, who later gave Narya to Círdan. According to The Lord of the Rings, it was directly to Círdan. Círdan later gave the ring to Gandalf, who wore it until he left Middle-earth at the end of the Third Age together with the other Ring-bearers.
 The One Ring was one of the Rings of Power. It first appeared in The Hobbit as a seemingly unimportant magic ring making its wearer invisible, but in the later The Lord of the Rings became the object of the central quest. 
 The One Ring was created by Sauron alone, in the Sammath Naur in Mount Doom, sometime during the Second Age. With this Ring he planned to control the other Rings of Power and their wearers. In the One Ring Sauron put half of his power and lifeforce and will, which made it very powerful. But this also meant that when the Ring was cut from his hand later that he was instantly defeated and without power or form for a long time. But because part of his power and life was in the One Ring, Sauron would exist as long as the Ring remained. The Ring could only be destroyed by casting it back into the Sammath Naur where it was forged.
 The One Ring appeared as a simple golden ring without any decoration or stones. If the Ring was put into fire, the heat would reveal a fiery inscription in the Black Speech written in Tengwar letters: 
 Translated roughly the inscription means: 
 If put on by a mortal, the person would be invisible to the normal world. The invisible person would see the world differently, for example being able to see the Nazgûl in their true form.
 Sauron lost the One Ring when it was cut from his hand by Isildur during the last battle of the Siege of Barad-dûr in the War of the Last Alliance. Isildur was advised by both Elrond and Círdan to destroy the Ring, but he refused and kept the Ring as a weregild for his father and brother. When Isildur later journeyed north, he was attacked by Orcs, resulting in the Disaster of the Gladden Fields. While Isildur tried to get the Ring to safety by crossing the river Anduin, it slipped from his finger and he was shot by archers, earning it the name Isildur's Bane. The One Ring remained lost for two and a half millennia. It was found again by the Stoor Hobbit Déagol while fishing in the Gladden Fields around T.A. 2463. He was killed by is friend Sméagol, later known as Gollum, who took the Ring and took it with him into the caves under the Misty Mountains, where the corrupting influence of the Ring prolonged his life and changed him over a time of five hundred years. As told in The Hobbit, Gollum lost the Ring in the caves where it was found by Bilbo Baggins in T.A. 2941. Bilbo took the Ring with him on his return to the Shire; he later left it for his heir Frodo Baggins when he left. In September of T.A. 3018, Frodo set out on his quest to take the One Ring to Rivendell, and after that to take it to Mount Doom and destroy it. In March T.A. 3019, the One Ring was destroyed when Gollum slipped and fell into the Sammath Naur while holding onto the One Ring, finally ending Sauron's life.
 The Arkenstone was a large white gem shining with an inner light, found by the Dwarves beneath the Lonely Mountain. When the dragon Smaug conquered Erebor it was lost. Upon the return to Erebor in The Hobbit it was sought for by Thorin Oakenshield, but found by Bilbo Baggins in the dragon's hoard. Later Bilbo offered the Arkenstone to the besieging armies as a bargaining tool to trade Thorin for a part of the treasure, to avoid bloodshed. After Thorin was slain in the Battle of Five Armies, the Arkenstone was placed on his tomb along the sword Orcrist.
 The palantíri or Seeing Stones were a means of communication created by the Noldorin Elves. They were perfectly ball-shaped, black in colour, and looked like made from crystal or glass. Their size could range from only a foor in diameter to very large ones that could not be carried. By looking into a palantír one could see faraway places, and "talk" to other people using another of the palantíri. 
Seven palantíri were brought to Middle-earth by Elendil when he fled the destruction of Númenor. These were put in the tower of Elostirion on the Tower Hills, the Tower of Amon Sûl, the cities Annúminas, Osgiliath, Minas Ithil, Minas Anor, and the tower Orthanc.
 The Red Book of Westmarch is a fictional book written by hobbits, from which many of Tolkien's writings are supposedly "translated". Among many other things it also contains the stories of The Hobbit and The Lord of the Rings. 
 Bilbo Baggins began writing the story of his journey titled There and Back Again, which is also part of the full title of the published The Hobbit. The story of the War of the Ring and the events of The Lord of the Rings were begun by Bilbo and mostly written by Frodo Baggins, titled The Downfall of the Lord of the Rings and the Return of the King. Added were three volumes of Translations from the Elvish, by B.B.. The Red Book was given to Samwise Gamgee, who added the events of the beginning of the Fourth Age, before passing it on to his daughter Elanor. The Red Book was also copied several times, kept by the Shire and the Kingdom of Arnor and Gondor.
 The Silmarils (Quenya plural Silmarilli) were three jewels shining with the light of the Two Trees. They were forged by the Noldorin Elf Fëanor in Valinor in the First Age. The Silmarils  were stolen by Morgoth, promting a large group of Noldor to return to Middle-earth, to retake them and take revenge for Morgoth's crimes. The wars lasted for the First Age of the Sun, their story told in the Quenta Silmarillion. At the end of the Age, two of the Silmarils were lost, one thrown into the fires of the earth and another into the Sea. The third Silmaril was taken to the Valar, who set it as a star in the sky, on a ship steered by Elrond's father Eärendil; the star became known as Eärendil or as Gil-Estel, the Star of High Hope.
 1 Weapons and armour
1.1 Aeglos
1.2 Herugrim
1.3 Glamdring
1.4 Gúthwinë
1.5 Mithril-shirt
1.6 Narsil
1.7 Orcrist
1.8 Sting
 1.1 Aeglos 1.2 Herugrim 1.3 Glamdring 1.4 Gúthwinë 1.5 Mithril-shirt 1.6 Narsil 1.7 Orcrist 1.8 Sting 2 Rings of Power
2.1 The Nine
2.2 The Seven
2.3 The Three
2.3.1 Vilya
2.3.2 Nenya
2.3.3 Narya
2.4 The One Ring
 2.1 The Nine 2.2 The Seven 2.3 The Three
2.3.1 Vilya
2.3.2 Nenya
2.3.3 Narya
 2.3.1 Vilya 2.3.2 Nenya 2.3.3 Narya 2.4 The One Ring 3 Other
3.1 Arkenstone
3.2 Palantír
3.3 Red Book of Westmarch
3.4 Silmaril
 3.1 Arkenstone 3.2 Palantír 3.3 Red Book of Westmarch 3.4 Silmaril 4 References Middle-earth Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 27 July 2016, at 11:26. This page has been accessed 27,329 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Aeglos Herugrim Glamdring Gúthwinë mithril-shirt Narsil Andúril Orcrist Sting Rings of Power Ring-bearers Nine Rings Seven Rings Three Rings Vilya Ring of Air Ring of Sapphire Nenya Ring of Adamant Ring of Water Narya Ring of Fire Red Ring One Ring Isildur's Bane Arkenstone palantíri Red Book of Westmarch There and Back Again The Downfall of the Lord of the Rings and the Return of the King Translations from the Elvish, by B.B. Silmarils Silmarilli Ash nazg durbatulûk, ash nazg gimbatul, ash nazg thrakatulûk, agh burzum-ishi krimpatul.  One ring to rule them all, One ring to find them, One ring to bring them all, and in the darkness bind them. Ash nazg durbatulûk, ash nazg gimbatul, ash nazg thrakatulûk, agh burzum-ishi krimpatul.  One ring to rule them all, One ring to find them, One ring to bring them all, and in the darkness bind them.  J. R. R. Tolkien's Middle-earthBooksThe Hobbit • The Lord of the Rings • (Posthumous:) The SilmarillionPeoplesAinur • Elves • Hobbits • Dwarves • Ents • OrcsGeographyMiddle-earth locations • Númenor • AmanOtherObjects • Languages • Scripts • Chronology • List of topics  Books The Hobbit • The Lord of the Rings • (Posthumous:) The Silmarillion   Peoples Ainur • Elves • Hobbits • Dwarves • Ents • Orcs  Geography Middle-earth locations • Númenor • Aman  Other Objects • Languages • Scripts • Chronology • List of topics Middle-earth objects Contents Weapons and armour Rings of Power Other References Navigation menu Aeglos Herugrim Glamdring Gúthwinë Mithril-shirt Narsil Orcrist Sting The Three The One Ring Arkenstone Palantír Red Book of Westmarch Silmaril Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console The Nine The Seven Vilya Nenya Narya 
