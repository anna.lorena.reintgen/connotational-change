Anthony Guarisco, Jr.​
 Claude Berwick Duval, I (October 24, 1914 – March 3, 1986), was an attorney from his native Houma, Louisiana, who from 1968 to 1980 was a three-term Democratic state senator for Terrebonne and St. Mary parishes.[1]
 He is best known for his profound oratory, his accommodation of Senate colleagues, his unsuccessful 1963-1964 campaign for lieutenant governor, and his opposition to the national holiday on the third Monday of January which honors civil rights activist Martin Luther King, Jr.​
 Duval was born to Stanwood Duval (1868–1928), and the former Mamie Richardson. Claude Duval and his older brother, Stanwood Richardson Duval, Sr., attended the United States Marine Corps reserve officers training school in Quantico, Virginia, where they were jointly commissioned on October 31, 1942, as second lieutenants. The brothers served during World War II with the First and Third battalions, 23rd Marine Regiment of the 4th Marine Division. They landed in the assault waves in the seizure and capture of the Pacific islands of Saipan, Tinian, and Iwo Jima. Stanwood Duval wrote a memoir of his war experiences, which he dedicated to the 3,298 officers and enlisted men of the 4th Marine Division who lost their lives in the fighting.​
 After the war, Claude Duval resumed his Houma law practice, which became Duval, Funderburk, and Sundbery. Stanwood Duval established a successful insurance agency. Both were active in their community. In 1957, Claude Duval was named president of the Houma branch of Rotary International.[2]​
​
 In 1963, Duval ran for lieutenant governor on the intraparty "ticket" of former New Orleans Mayor (and also former Ambassador to the Organization of American States) deLesseps Story "Chep" Morrison, Sr. (1912-1964). In his own race, Duval was pitted against Clarence C. "Taddy" Aycock (1915-1987) of neighboring St. Mary Parish, who agreed with Duval on many issues. Aycock was informally paired with John J. McKeithen in the Democratic runoff though he was actually an independent candidate who made no alliance with any gubernatorial candidate that year. Aycock had the advantage in experience and name recognition.​
 There was little evidence of "ticket-splitting," where Morrison supporters backed Aycock, or where McKeithen backers chose Duval. In retrospect, all four men had far more in common than otherwise. Duval in fact was arguably as conservative as Aycock, but in central and north Louisiana, voters perceived Duval unfavorably as a Morrison lieutenant. He had also been Morrison's campaign manager in the unsuccessful 1959 election against Jimmie Davis.[3]
 Other candidates on the Morrison slate were state Representative Jack M. Dyer of Baton Rouge for insurance commissioner, and Raymond Laborde, the mayor of Marksville in Avoyelles Parish for custodian of voting machines, a position later renamed elections commissioner. In 1968, when Duval entered the state Senate, Lieutenant Governor Aycock, presiding officer of the Senate, began his third term in the second-highest state office.​
​
 ​
Duval was state senator-elect at the time of the King assassination in Memphis,Tennessee, on April 4, 1968. He issued a statement critical of the direction of the civil rights movement:​
​
 The American flag flew at half-mast to [honor] a man who aided and abetted the communists of North Vietnam, as he publicly supported the draft card burners and sought to undermine and betray our fighting sons in Vietnam.​
​
In the avalanche of propaganda, hypocrisy, and falsehood that followed the death of King, the President, Lyndon B. Johnson, and national figures, together with the news media, have undertaken to eulogize and commit to martyrdom Martin Luther King [Jr.], who, under the guise of non-violence, caused violence wherever he went.​
​The voice of truth is not heard in the land. All has been forgiven, all has been forgotten. None seem to remember that only the day before his death, King openly declared his intention to violate law and order—a federal court order. This was nothing new, since he had previously violated a federal court order....​
​We witness in our major cities looting, theft, burglary, arson, robbery, murder-all, indeed, a fitting tribute to an advocate of violence.​
​'I call upon all men, the responsible Negro community as well as the white, to face the facts and truth and to dispel from all minds the falsehood and hypocrisy that have been visited upon us by our leaders and the news media. If the men who died in World War II, in Korea, and Vietnam should return, they would cry out in horror at the eulogizing of a man who.  .  . aided and abetted the enemies of this nation, who preached disobedience of law and who incited violence and riot.​​
 ​
Once in the state Senate, Duval spoke eloquently and for long periods on nearly any topic brought before the body. Some called him the "Cicero of the Louisiana Senate." He was also helpful to colleagues in obtaining office space and other personal favors. In 2006, the state Senate posthumously honored him with the dedication of the Senate building known as "Duval Hall."​
 Robert G. Jones, a Lake Charles stockbroker and the son of former Governor Sam Houston Jones, served in the Senate with Duval from 1972 to 1976. According to Jones, Duval was "a very bright guy ... one of the most respected of all the senators." Jones said that he believed Morrison had tapped Duval as a running mate because of Duval's towering character and reputation, not because of political philosophical considerations.
 In 1976, Duval was the co-chair of the Gerald Ford-Bob Dole campaign in Louisiana, which was defeated by the Democrat Jimmy Carter and Walter Mondale forces.[4]​
 Duval retired from the Senate in March 1980. In 1983, President Ronald W. Reagan signed legislation to designate the third Monday of January as a federal holiday in honor of Dr. King. The law took effect in January 1986, two months before King critic Claude Duval died.​
 Duval's social conservatism also included opposition to the proposed Equal Rights Amendment. When the measure failed to gain ratification after an extended deadline in 1982, Duval recalled that "in the Sixties everybody was obsessed with their [sic] rights. Everybody worried about their [sic] rights. Rather than rights, people should talk more about their responsibility to their country."​[5]
​
​In 1970, Duval told the Central Louisiana Press Club in Alexandria that he was considering entering the gubernatorial field because he said the state "needs a new look established on old values."[6] Ultimately, he did not file in a race won by U.S. Representative Edwin Edwards.​
 After he left the state Senate, Duval's political contributions went mostly to Republicans. There is no indication, however, that he himself switched parties. Among recipients of his donations were the Ronald W. Reagan presidential campaign, the National Republican Senatorial Campaign Committee, GOP Congressman William Henson Moore, III, of Baton Rouge, who ran for the U.S. Senate in 1986, and Democratic and later Republican Congressman Wilbert Joseph "Billy" Tauzin, Jr., of Lafourche Parish]]. Duval's contribution to Moore came in the spring of 1985.​
 Duval died at the age of seventy-one. He was survived his wife, the former Betty Bowman (1914–1985), by only five months. He was survived by a daughter, Dorothy Duval Nelson,, whose husband, Charles Waldemar Nelson (both born 1947), is the former president of the World Trade Center in New Orleans; two grandchildren; his brother, Stanwood Duval (1913–2001); a sister, Catherine Duval Dean (1916–1997) of Albuquerque, New Mexico; a maternal aunt, Alice Richardson Butler (1910–1995), and a maternal uncle who was his junior in age, Frank D. Richardson (1916–1993), both of St. Joseph in Tensas Parish.
 Duval also had two nephews, his namesake Claude Berwick Duval, II (born 1955), a prominent Houma attorney, and U.S. District Judge Stanwood Richardson Duval, Jr. (born 1942), an appointee of President Bill Clinton, based in New Orleans.  Judge Stanwood Duval, a Democrat, blocked the implementation of the "Choose Life" license plates approved by the state legislature on grounds that the optional plates were in violation of the First Amendment]] to the United States Constitution. Judge Duval's argument was unanimously reversed in 2005 by the Fifth Circuit Court of Appeals, also in New Orleans. A perfectly divided court denied a petition for rehearing by a vote of eight-to-eight. Judge Duval is clearly to the political left of his late uncle. However, Claude Duval was a strong advocate of the First Amendment but also supported abortion.​
 Claude and Betty Duval are interred at Magnolia Cemetery in Houma. Duval was Episcopalian. In addition, to the Senate office facility, Duval is honored through the "Senator Claude B. Duval Scholarship" given at Nicholls State University in Thibodaux, Louisiana.​
​
 ​
 ​Several other sources are no longer available.
 ​​​​​​​​​
 1 Background 2 Race for lieutenant governor 3 Opposition to Martin Luther King holiday 4 Duval in the state Senate 5 Family and death 6 References ↑ Membership in the Louisiana Senate, 1880 - Present (St. Mary and Terrebonne parishes). Louisiana State Senate. Retrieved on November 3, 2019.
 ↑ Minden Herald, November 30, 1959, p. 13.
 ↑ Louisiana, September 25, 1976. fordlibrarymuseum.gov. Retrieved on May 31, 2014.
 ↑ Anabelle Armstrong, "ERA foes celebrate, challenge," The Baton Rouge Advocate, July 1, 1982​.
 ↑ Minden Press-Herald, September 23, 1970, p. 1.
 Chauvin Funeral Home, Houma, Louisiana, obituary information on Claude B. Duval​. Louisiana People Attorneys Politicians State Senators Democrats World War II United States Marine Corps Episcopalians Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 26 November 2019, at 07:55. This page has been accessed 348 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 ​
The American flag flew at half-mast to [honor] a man who aided and abetted the communists of North Vietnam, as he publicly supported the draft card burners and sought to undermine and betray our fighting sons in Vietnam.​
​
In the avalanche of propaganda, hypocrisy, and falsehood that followed the death of King, the President, Lyndon B. Johnson, and national figures, together with the news media, have undertaken to eulogize and commit to martyrdom Martin Luther King [Jr.], who, under the guise of non-violence, caused violence wherever he went.​
​The voice of truth is not heard in the land. All has been forgiven, all has been forgotten. None seem to remember that only the day before his death, King openly declared his intention to violate law and order—a federal court order. This was nothing new, since he had previously violated a federal court order....​
​We witness in our major cities looting, theft, burglary, arson, robbery, murder-all, indeed, a fitting tribute to an advocate of violence.​
​'I call upon all men, the responsible Negro community as well as the white, to face the facts and truth and to dispel from all minds the falsehood and hypocrisy that have been visited upon us by our leaders and the news media. If the men who died in World War II, in Korea, and Vietnam should return, they would cry out in horror at the eulogizing of a man who.  .  . aided and abetted the enemies of this nation, who preached disobedience of law and who incited violence and riot.​​
I know I speak against the tide of overwhelming emotion ... but let the voice of truth be heard in the land. If it is possible, let the voice of reason be heard. Then may the Negro and the white communities join together in a truthful and realistic effort to build a better society. In office Claude Berwick Duval, I  Claude Berwick Duval, I​
 Louisiana State Senator for Terrebonne and St. Mary parishes​
 In office1968​ – 1980​
  ​Harvey Peltier, Jr.
  Leonard J. Chabert
Anthony Guarisco, Jr.​
 
  October 24, 1914​Houma, Terrebonne ParishLouisiana, USA​
  March 3, 1986 (aged 71)​
  Magnolia Cemetery in Houma​
  Democrat​
  Betty Bowman Duval (1914–1985)​
  Dorothy Duval Nelson​
  Attorney​
  Episcopalian​
 Claude B. Duval Contents Background Race for lieutenant governor Opposition to Martin Luther King holiday Duval in the state Senate Family and death References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
