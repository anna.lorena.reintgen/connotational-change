Storks are large birds of the family Ciconiidae, comprising six genera and nineteen species, and found in much of the warm, dry regions of the world. Characteristics of these birds are the long neck, long legs and the large, often elongated beak. All storks are carnivorous, with the diet depending on the species. The best-known stork is perhaps the white stork (Ciconia ciconia), a bird as familiar in folklore as it is in European towns and villages.
 In general, storks are characterized by a heavy build, long legs, long neck, and a long, stout bill.  Broad-winged, storks use thermal air flows to soar and glide, conserving energy.  Storks are gregarious year-round, and are usually found in large flocks.
 Storks are medium to very large birds; outwardly they resemble the related herons, but in appearance seem more massive and heavy.  Males are slightly larger than females.  The smallest stork is Abdim's stork (Ciconia abdimii), standing at just over two feet tall and weighing 2.2 pounds.  The largest is the marabou (Leptoptilos crumeniferus), which stands nearly five feet tall and weighs up to twenty pounds; with a wingspan in excess of ten feet, the marabou is among the largest living flying birds.
 The beak is large, but very diverse in shape. The typical, slender bill is found only in the genus Ciconia; it is powerful with a slight upwards bend in the saddle-stork, giant stork and jabiru.  It is especially large in the marabou, where it grows throughout its life and can reach over a foot in length; the marabou's stout beak is used to take food from deep within animal carcasses, where it competes with vultures.  The yellow-billed stork and his relatives have a beak with a slight downwards bend containing sensory cells, which are helpful in finding food in the murky water. 
 The stork's foot has the first toe pointed to the back and the remaining three to the front (anisodactyl), with stunted webbing at the toe base. The long legs allow a slow walk.  The wings are big and wide, well suited for gliding, interrupted by slow wing beats.  Of the storks only the marabou flies with its neck retracted, the others fly with necks fully extended, enabling the laymen to distinguish them easily from herons.  When storks fly in groups, they form no formations.
 The plumage consists of black and white tones, which are distributed differently depending on the species. The black plumage often has a metallic sheen, which strengthens during the breeding season, in which white plumage also appears brighter. Many species have a completely unfeathered face, which in the marabou both head and neck are nearly naked. 
 Unlike many storks, storks are not voiceless. Croaking, squeaking and mooing sounds occur, with the addition of a beak rattle. The species of the genus Ciconia make whistling sounds, which are more pronounced with the black stork than with the white stork. 
 The name has its origins in either Old English (storc or stearc) or Old Norse (storkr),  and refers to the stiff appearance of the birds when standing.[1][2]
 Two species of bird have also been referred to as storks: the hammerkop (Scopus umbretta) and the shoebill (Balaeniceps rex).  Both species have been classed with storks in the family Ciconiidae for a number of years; however, recent evidence indicates a relationship with pelicans. 
 Storks are found on all continents except Antarctica. They are common in large parts of Eurasia, Africa, Australia and South America; in North America, on the other hand, they inhabit only the extreme south. Most species are native to the tropics; only three species live in the temperate zones.
 They generally live in wetlands and feed mainly on fish, frogs, small reptiles, crustaceans and molluscs, insects and small mammals.  The yellow-bill and the openbills are found near the shore, while the marabou and Abdim's stork often stay far away from water in the open savannah. However, most species live at least near swamps, lakes or riverbanks.
 Because the white stork migration is so famous, it may come as a surprise that most storks are not migratory birds. They stay close to their breeding grounds and move around relatively small-scale outside the breeding season. In contrast, the white stork belongs to the extreme migrators, with representatives of some northern European populations covering some 12,000 miles annually to reach the African winter quarters and return to their breeding grounds. The black stork and Oriental white stork breed in the temperate zone and migrate to tropical regions in winter. With the Abdim's stork, however, a tropical species is also a true migratory bird: it breeds in the steppes and semi-deserts north of the equator and overwinters in the eastern and southern African savannahs.
 For the most part storks are monogamous; they will mate for life, and return to the same nests every year.  However, it has been observed that some storks have changed mates after migrations.  Nests are large, up to six feet across and ten feet deep, with new material added every year.  Most species nests in colonies, located on trees, in swamps or rarely on rocky walls.  Some storks also nest on bell towers, chimneys, and light poles. Eggs are laid in clutches of two to five, with an incubation period of between 25–35 days.  Young reach maturity after four years, and as adults they have a life expectancy of between 30 and 40 years.
 The stork was a highly respected bird among the ancient peoples.  The Egyptians venerated the storks because they believed that they fed their parents who became elders; it was associated with the ba, an image of the soul which migrates every night back to the body in the afterlife[3].  The fact that they also feed on snakes has made both Plutarch[4] and Pliny the Elder[5] write that in Thessaly a man could be put to death for killing a stork.  
 Greek mythology attributes to the Trojan Antigone, daughter of Laomedon and sister of Priam, a legend which had Hera punish Antigone for her pride; the goddess would have turned her hair into snakes that constantly bite her, but she begged for mercy and was turned into a stork instead[6]. In addition, the good qualities of Antigone, such as love for husband and children, would have moved into the storks, which became a symbol of these virtues.  The fables of Aesop also includes the stork; in one, a farmer has caught a flock of cranes intent on stealing his grain, and included among them is an unlucky stork who pleads for his life, but has been judged "by the company you keep" according to the farmer[7].
 In the Christian tradition there is not much room for the stork, otherwise confused with other waders.  It was in the Middle Ages given a comparison to Christ and considered a symbol of filial piety, with the habit of eating snakes also making it an emblem of Christ, who incessantly fights the devil[8].  The stork also appears in numerous noble arms and on the mark of a porcelain factory of the second half of the eighteenth century with headquarters in The Hague, intent on devouring a snake[9].
 But the stork as a symbol also had a negative value. In the Bible the stork is considered an unclean bird[10]. To it Saint Jerome, perhaps because of the croaking he emits, associates the ambiguous and/or mocking attitudes[11] and even, when it was represented with a fish in the beak (the storks also feed on small fishes), was considered the emblem of the devil who takes a soul from God.
 The stork is notoriously mentioned today and through much of history as a metaphor for the birth of children, either by a glance at a woman who becomes pregnant as a result, or in the famous deliveries in which these flying birds have a baby slung in a sack hanging from their beaks[12]. There are several versions, not very dissimilar to each other, to explain the belief that lies at the origin. One, native to Northern Europe, is given by its symbolic link to the Great Mother; the other, always connected to the aforementioned primordial deity, derives from the nature of the stork that "would fish" the life in the waters of the womb Great Mother; while a third is provided by the writer and linguist Angelo de Gubernatis, who explains the Germanic popular belief that storks would fish newborns in a fountain, with the fact that this bird lives in watery areas and therefore rainy: the newborn would thus represent the new sun rising from the clouds after long days of rain[13].  Another interpretation is that the belief stems from the fact that the storks build the nest on hot spots such as the chimneys and at the time of the legend the only houses that had the fireplace always on were those where there was a newborn[14].
 1 Description
1.1 Species
 1.1 Species 2 Range and habitat 3 Nesting 4 Symbolism and folklore
4.1 The stork and babies
 Storks Birds Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 26 September 2018, at 14:28. This page has been accessed 3,091 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Stork Kingdom Information Domain Kingdom Subkingdom Branch Phylum Information Phylum Sub-phylum Infraphylum Class Information Superclass Class Sub-class Infra-class Order Information Order Family Information Family Storks African openbill, Anastomus lamelligerus
Asian openbill, Anastomus oscitans African openbill, Anastomus lamelligerus Asian openbill, Anastomus oscitans Abdim's stork, Ciconia abdimii
Black stork, Ciconia nigra
Maguari stork, Ciconia maguari
Oriental white stork, Ciconia boyciana
Storm's stork, Ciconia stormi
White stork, Ciconia ciconia
Woolly-necked stork, Ciconia episcopus Abdim's stork, Ciconia abdimii Black stork, Ciconia nigra Maguari stork, Ciconia maguari Oriental white stork, Ciconia boyciana Storm's stork, Ciconia stormi White stork, Ciconia ciconia Woolly-necked stork, Ciconia episcopus Black-necked stork, Ephippiorhynchus asiaticus
Saddle-billed stork, Ephippiorhynchus senegalensis Black-necked stork, Ephippiorhynchus asiaticus Saddle-billed stork, Ephippiorhynchus senegalensis Jabiru, Jabiru mycteria Jabiru, Jabiru mycteria Greater adjutant, Leptoptilos dubius
Lesser adjutant, Leptoptilos javanicus
Marabou stork, Leptoptilos crumeniferus Greater adjutant, Leptoptilos dubius Lesser adjutant, Leptoptilos javanicus Marabou stork, Leptoptilos crumeniferus Milky stork, Mycteria cinerea
Painted stork, Mycteria leucocephala
Wood stork, Mycteria americana
Yellow-billed stork, Mycteria ibis  Milky stork, Mycteria cinerea Painted stork, Mycteria leucocephala Wood stork, Mycteria americana Yellow-billed stork, Mycteria ibis  African openbill, Anastomus lamelligerus
Asian openbill, Anastomus oscitans African openbill, Anastomus lamelligerus
Asian openbill, Anastomus oscitans Abdim's stork, Ciconia abdimii
Black stork, Ciconia nigra
Maguari stork, Ciconia maguari
Oriental white stork, Ciconia boyciana
Storm's stork, Ciconia stormi
White stork, Ciconia ciconia
Woolly-necked stork, Ciconia episcopus Abdim's stork, Ciconia abdimii
Black stork, Ciconia nigra
Maguari stork, Ciconia maguari
Oriental white stork, Ciconia boyciana
Storm's stork, Ciconia stormi
White stork, Ciconia ciconia
Woolly-necked stork, Ciconia episcopus Black-necked stork, Ephippiorhynchus asiaticus
Saddle-billed stork, Ephippiorhynchus senegalensis Black-necked stork, Ephippiorhynchus asiaticus
Saddle-billed stork, Ephippiorhynchus senegalensis Jabiru, Jabiru mycteria Jabiru, Jabiru mycteria Greater adjutant, Leptoptilos dubius
Lesser adjutant, Leptoptilos javanicus
Marabou stork, Leptoptilos crumeniferus Greater adjutant, Leptoptilos dubius
Lesser adjutant, Leptoptilos javanicus
Marabou stork, Leptoptilos crumeniferus Milky stork, Mycteria cinerea
Painted stork, Mycteria leucocephala
Wood stork, Mycteria americana
Yellow-billed stork, Mycteria ibis  Milky stork, Mycteria cinerea
Painted stork, Mycteria leucocephala
Wood stork, Mycteria americana
Yellow-billed stork, Mycteria ibis  
  Yellow-billed storkMycteria ibis
 Kingdom Information
  Domain
  Eukaryota
  Kingdom
  Animalia
  Subkingdom
  Bilateria
  Branch
  Deuterostomia
 Phylum Information
  Phylum
  Chordata
  Sub-phylum
  Vertebrata
  Infraphylum
  Gnathostomata
 Class Information
  Superclass
  Tetrapoda
  Class
  Aves
  Sub-class
  Neornithes
  Infra-class
  Neoaves
 Order Information
  Order
  Ciconiiformes
 Family Information
  Family
  Ciconiidae
 Stork Contents Description Range and habitat Nesting Symbolism and folklore References Navigation menu Species The stork and babies Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
