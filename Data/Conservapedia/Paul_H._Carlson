(Historian and professor emeritus of the American West at Texas Tech University in Lubbock)​
 Resident of Ransom Canyon, Lubbock CountyTexas​
 Children:
Steven Y. Carlson​
Kevin A. Carlson​
Diane K. McLaurin​
 Paul Howard Carlson (born August 30, 1940), an historian of Texas, the  American West, and Native American, is a professor emeritus at Texas Tech University in Lubbock.[1]​
 In 1973, Carlson received his Ph.D. from Texas Tech, taught at Texas Lutheran College in Seguin in Guadalupe County, and returned to Texas Tech in the early 1980s as a professor of history. He retired from the university in 2009. He has also been active throughout his career as a fellow of both the West Texas Historical Association, based at Texas Tech, and the Texas State Historical Association, headquartered at the University of Texas at Austin. Carlson concentrates on ranching, frontier life, the military, and Indian affairs.[2] He has through 2010 published eighteen books and more than two hundred articles, essays, and book reviews.[3]
  ​
In the popular view cowboys were men on horseback. In fact, most of the time they spent their days on foot working at such farm-related chores as repairing fences and cutting hay. Even in Wister’s defining cowboy novel, for example, the hero of the story—the prototypal cowboy—herded neither cows nor cattle of any kind.​
 ​
 ​
Other chapters of The Cowboy Way are "Cowboy Humor" by Kenneth W. Davis, "Stockyards Cowboys" by J'Nell L. Pate, "English Cowboy: The Earl of Aylesford in the American West," by James Irving Fenton (1932–2011) of Lubbock, "Cowboy Songs" by Robert G. Weiner, and "Vaqueros in the Western Cattle Industry" by Jorge Iber.[4]​
​
 ​
Pecos Bill, a Military Biography of William R. Shafter, (Texas A&M University Press, 1989) is a study of a controversial military officer  who was stationed for a time in West Texas. The  officer is of course unrelated to the western character Pecos Bill, a creation of folklore. Carlson seeks to set the historical record straight in regard to General Shafter, formerly considered a "fat, incompetent buffoon" who headed the American Expeditionary Forces to Cuba in 1898. Much of the success of the AEF has been attributed to future U.S. President Theodore Roosevelt and his friend, General Leonard Wood.[5]​
 After the American Civil War, Shafter returned to his native Michigan but found that he preferred military to civilian life. In 1867, he received a commission in the regular Army and was sent to Texas as a lieutenant colonel of the 41st Infantry, an African American regiment. Carlson describes Shafter's Texas sojourn as service "with distinction." Thereafter, Shafter fought in several Indian campaigns throughout the West. He was involved in peace restoration at Pine Ridge, South Dakota, in the wake of the 1890 Wounded Knee Massacre.[5]​
 In the Spanish-American War, Shafter transported a force of 16,000 men some 1,200 miles by water, and within ten days of landing drove back the enemy to his last line of defense at Santiago de Cuba. Within another two weeks, the city surrendered, and a Spanish army of 24,000 laid down its arms. Carlson concludes that Shafter's work in Cuba was certainly not that of a "buffoon."[5]​
 According to the reviewer Roger D. Launius, Carlson:​
​
 ​
 With Tom Crum, Carlson published in 2010 Myth, Memory, and Massacre: The Pease River Capture of Cynthia Ann Parker. The mother of Comanche chief Quanah Parker, Cynthia Ann Parker and had been taken captive in 1836, when she was a young girl. In 1860, she was taken prisoner in a raid on the Pease River by a contingent of the Texas Ranger Division, led by Sul Ross, and United States cavalry. Carlson and Crum re-examine the plight of Parker and reveal a century of historical falsifications that have made the facts of the case a continuing mystery.[1]​
 In 2006, Carlson published Amarillo: The Story of a Western Town, a history of Amarillo, largest city in the Texas Panhandle.[3]
 In 2005, Texas Tech Press published Carlson's short volume of history and archaeology of the Llano Estacado, entitled Deep Time and the Texas High Plains: History and Geology.[4] A reviewer noted that early inhabitants of the Plains who came to the Lubbock Lake Landmark in the long Yellow House Draw, "camped, hunted game, and sought shelter from harsh winter weather." Carlson surveys the geologic past of the area, with emphasis on "human activity in the region ...  how early peoples adapted to shifting environmental conditions and changing animal resources. ... Carlson places this significant national archaeological site in broad perspective, connecting it to geology and history in the larger upper Brazos River drainage system and, by extension, the central Llano Estacado. ..."[6]​
 The Plains Indians (College Station, Texas: Texas A&M University Press) won the History Book Club selection of 1998 and was subsequently published in 2004 in a French language translation in Paris. In 2005, The Plains Indians was named one of the 100 most outstanding books on the American West published in the 20th century.[3]​
 Empire Builder in the Texas Panhandle: William Henry Bush (1849-1931) (Texas A&M University Press, 1996), is the story of a versatile entrepreneur who made a fortune in many enterprises, including the Panhandle cattle industry.[7]
 Texas Woollybacks: The Range Sheep and Goat Industry (Texas A&M University Press, 1982) is a study of the sheep and goat industries in West Texas.[8]
 With the historians Donald R. Abbe (born 1949) and David J. Murrah (born 1941), Carlson co-authored Lubbock and the South Plains. In the 2014 West Texas Historical Review, Carlson published "The Nicolett Hotel and the Founding of Lubbock," a study based in part on the hotel register of the former landmark Nicolett Hotel in Lubbock.[9]
 ​
In 2000, Carlson garnered the "Outstanding Researcher Award" from the Texas Tech College of Arts and Sciences. He served on the advisory committees for the Handbook of Texas, a creation of the Texas State Historical Association. He has worked on the Charles Goodnight]papers. In 1993, he received the Texas Tech "President's Excellence in Teaching Award." In 2005, he was named outstanding professor by the Residence Life students. He was twice named the "outstanding faculty member" of the Texas Tech history department. He has also been a director of the Texas Tech Center for the Southwest.[3] In 2006, Carlson was elected to membership in the Philosophical Society of Texas.[1]​
 On April 11, 2015, the West Texas Historical Association at its annual meeting at Amarillo College  honored Carlson with the presentation "A Prof's Prof: A Timely Tribute to Paul Howard Carlson and His Versatile Body of Work." Susan Dickey of Jackson, Mississippi, discussed Carlson as "Young Teacher and Military Historian"; Leland Turner of Midwestern State University in Wichita Falls, presented "Carlson: Ranching Historian", and Scott Sosebee of Stephen F. Austin State University in Huntsville, Texas, concluded with Carlson as "Native American Historian."[10]​
 Carlson and his wife, the former Ellen J. Opperman (born ca. 1941), reside in scenic Ransom Canyon in Lubbock County.[1][11] There are two Carlson sons, Steven Y. Carlson (born ca. 1966) of Schertz, Texas,[12] ​and Kevin A. Carlson (born 1971) of Ransom Canyon,[11] and one daughter, Diane K. McLaurin of Lubbock.  ​
 ​
​​​
 1 Carlson's The Cowboy Way 2 Another Pecos Bill 3 Other Carlson works 4 Academic honors 5 Personal life 6 References ↑ 1.0 1.1 1.2 1.3 
 Myth, Memory and Massacre: The Pease River Capture of Cynthia Ann Parker. Lubbock: Texas Tech Press, 2010, 216 pp. ISBN 978-0-89672-707-6. Retrieved on November 7, 2010; no longer accessible. 
 ↑ 2.0 2.1 Paul Carlson Papers. lib.utexas.edu. Retrieved on November 7, 2010.
 ↑ 3.0 3.1 3.2 3.3 Paul H. Carlson. depts.ttu.edu. Retrieved on November 7, 2010; no longer on-line.
 ↑ 4.0 4.1 4.2 
 The Cowboy Way: An Explanation of History and Culture. Lubbock: Texas Tech University Press, 2006, 236 pp. ISBN 0-89672-583-9. Retrieved on November 7, 2010. 
 ↑ 5.0 5.1 5.2 5.3 
 Pecos Bill, a Military Biography of William R. Shafter. College Station: Texas A&M Press, 1989, 225 pp., ISNBN: 0890963487. Retrieved on November 8, 2010. 
 ↑ 
 Deep Time and the Texas High Plains: History and Geology. Lubbock: Texas Tech University Press, 2005, 141 pp. ISBN 0-89672-553-7. Retrieved on November 7, 2010; no longer on-line. 
 ↑ 
 Empire Builder in the Texas Panhandle: William Henry Bush. College Station: Texas A&M Press,  1996, ISBN 978-0-89096-712-6. Retrieved on November 7, 2010. 
 ↑ 
 Texas Woollybacks: The Range Sheep and Goat Industry. College Station: Texas A&M Press, 1982, 236 pp., ISBN 0-89096-133-6. Retrieved on November 8, 2010. 
 ↑ Paul H. Carlson, West Texas Historical Review, Vol. 90 (2014), pp. 8-19.
 ↑ A Prof's Prof. West Texas Historical Association (April 11, 2015). Retrieved on March 25, 2015; no longer on-line.
 ↑ 11.0 11.1 Net Detective People Search. Internet
 ↑ People Search and Background Check, Internet
 Texas Historians Educators American Authors Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 3 October 2019, at 05:08. This page has been accessed 297 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 dime novels and Buffalo Bill's Wild West Exhibition, then in the enormous popularity of Owen Wister's  1902 novel, The Virginian, and subsequently in the success of popular western novels of the type by Zane Grey and Max Brand, in western films (made in Italy, Germany, Hollywood, and elsewhere), in television programs in public television documentaries, and in other formats, including the highly effective use of cowboys as advertising symbols. Serious scholars—including historians, sociologists, literary critics, and others—have studied cowboys and the symbols and myths that surround them.​
 ​
In the popular view cowboys were men on horseback. In fact, most of the time they spent their days on foot working at such farm-related chores as repairing fences and cutting hay. Even in Wister’s defining cowboy novel, for example, the hero of the story—the prototypal cowboy—herded neither cows nor cattle of any kind.​
 ​
Nonetheless, in both his actual and his imagined life the cowboy has become a popular hallmark for defining what it means to be a 'real' American male. Perceived as a tough, mobile, and independent outdoorsman, he has become a symbolic yardstick against which modern men might measure their own manhood.[4] corrects such misconceptions and rescues Shafter from ill consideration and obscurity. [His] portrait of William R. Shafter, therefore, is a refreshing revisionist analysis of an important nineteenth century military figure. Perhaps at times the author is too persistent in trying to rescue Shafter from his reputation as an incompetent, but the arguments he makes are compelling. Even so, Carlson, does not paper over flaws in his character. Shafter was obstinate, profane, a womanizer, a sometimes drunk, and single-mindedly hard-boiled. He was also, Carlson admits, energetic, ambitious, self-reliant, and hard-working. When one finishes this book, there is a sense that Shafter was a flawed but capable figure. 'Pecos Bill' is a fine book, well worth the reading.[2][5] Paul Howard Carlson​ Born Spouse Children Paul Howard Carlson 
 
  Born
  August 30, 1940​ Place of birth missing
Resident of Ransom Canyon, Lubbock CountyTexas​
 Spouse
  Ellen Opperman Carlson
Children:
Steven Y. Carlson​
Kevin A. Carlson​
Diane K. McLaurin​
 Paul H. Carlson Contents Carlson's The Cowboy Way Another Pecos Bill Other Carlson works Academic honors Personal life References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
