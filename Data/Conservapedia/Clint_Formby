(Texas radio broadcaster)
 Children:
Larry C. "Chip" Formby
Ben Formby
Marshall Clark Formby
Scott C. Formby
Linda Kay Formby (deceased)​
Parents:
John C. and Willie Elsby Formby​
Alma mater:
McAdoo High School
University of Basel
Texas Tech University​
 John Clinton Formby, known as Clint Formby (December 22, 1923 – July 31, 2010), was a veteran radio broadcaster called the "Old Philosopher" based in the small city of Hereford in Deaf Smith County (pronounced DEEF) in northwestern Texas. His daily broadcast ran continuously on his PAN AM & FM Country music station since October 10, 1955. Eventually reduced to five minutes in length, Formby's commentary was the oldest continuously-running broadcast by a single host in radio history.[1]​
 Formby was born in McAdoo in Dickens County, also in West Texas, to John C. Formby (1902–1989), a Rural Free Delivery mail carrier, and the former Willie Elsby (1903–1994).[2] John and Willie married in 1922, and Clint was their only child. Clint Formby was the paternal grandson of Marshall Clinton Formby, Sr. (1877–1957) and the former Rosa Mae Freeman (1882–1971).[2]He attended public schools in Plainview in Hale County and then Dickens County, where he graduated in 1942 from McAdoo High School. He played basketball, tennis, and was quarterback on the six-man football team at McAdoo. He then began college at Texas Tech University in Lubbock.[3]​
 However, he soon left college to enlist in the United States Army. He served as a staff sergeant and medic, having been attached to the 235th General Hospital in Marseille, France. He remained in Switzerland after the end of the war in April 1945 and attended the University of Basel in Basel.[3] After the war, he returned to Texas Tech where he met and married the first official Tech beauty queen, the former Margaret Clark.[2] Reared on a ranch near Van Horn, Texas, Margaret founded the National Cowgirl Museum and Hall of Fame, first in the Hereford library and later in a large modern building in Fort Worth.[4] While a student at Texas Tech, Formby worked as a carpenter and painter during the summer of 1948 to help his maternal uncle, Marshall Formby, construct KPAN AM in Hereford. Clint Formby's was the first voice carried on the new radio station when it went on the air on August 4, 1948, exactly sixty-two years to the date of his own funeral.[3]​
 In addition to his business pursuits, Marshall Formby was a Democratic state senator, a state highway commissioner, and a 1962 gubernatorial candidate. Clint Formby was an older first cousin of former state Senator Robert Duncan, a Moderate Republican from Lubbock. Clint Formby practically grew up with Duncan's mother, Mae Robena Formby Duncan (1921–2009), his aunt who was only two years his senior.[5]​
 Clint and Margaret Formby lived for a time in Colorado City in Mitchell County north of San Angelo, where they rented a garage apartment from attorney and future U.S. Representative George Herman Mahon (1900-1985). Formby established a radio station in Snyder in Scurry County and then began working on August 22, 1951, at his Uncle Marshall’s radio station in Hereford[3] for $47.50 per week. He became a partner in the station and eventually bought out his uncle. His persistent broadcasts, called "predictably unpredictable," touched on nearly any subject.[1] He urged listeners to tip waiters and waitresses more generously. He refuses to reveal his political party preference, advising listeners that they should be able to tell his inclination from his commentaries.[1]
 Once Vice President Lyndon B. Johnson contacted Formby to advise him that he would like to hear Formby speak at the First Baptist Church of Hereford, filling in for the absent pastor, but Johnson was unsure if his plane could land in the small Hereford airport.[6] About this time, Marshall Formby, the state highway commissioner, was seeking the Democratic gubernatorial nomination against Johnson’s presumed favorite and eventual winner, John B. Connally, Jr. Clint Formby served as his uncle's campaign manager in the gubernatorial primary.[3]​
 Formby in time charted a radio path across Texas including outlets in Floydada, Tulia, Levelland, Andrews, Seminole, Tyler, Huntsville, Temple, and Marshall. In the 1950s, Formby went by train from Hereford to Chicago to attend the annual convention of the National Association of Broadcasters.[3]
 In one of his broadcasts, Clint Formby advocated dropping trade restrictions with Cuba because the Fidel Castro government is the only communist regime that has yet to kill an American citizen. He also visited Cuba. On another occasion, he scolded Hereford residents for parking their vehicles in their front yards. A widower, Formby each week advises his married male listeners to kiss their wives goodnight. KPAN calls itself the "only radio station in the world that gives a hoot about Hereford, Texas." Local sports are carried live on the Internet by KPAN. Otherwise, listeners must connect through regular AM or FM radio.[7] Formby traveled widely across the United States and the world and reported on his varied trips to his listeners.[3]
 In 2010, Formby's Monday-Saturday program marked its 60th year on the air. Officially known as the "Day-By-Day Philosopher," the program ran for fifteen minutes from 7:45 to 8 a.m. for the first ten years and during the peak of radio drive time. In later years, Formby reduced the time to five minutes, saying that listeners now have too short of an attention span to justify a longer broadcast. It was the longest-running daily radio broadcast by an individual in America. "The Old Philosopher," as he was usually known, was featured on NBC's The Today Show on December 29, 2007.[6]
 Formby claimed to have not missed a broadcast in more than 16,000 consecutive mornings, broadcasting commentaries from other radio stations while he was traveling.[6] His "Most Beautiful Alley" contest raised $89,000 in scholarship money for students attending the Hereford campus of the two-year Amarillo College.[6]​
​
In 1984, Formby was inducted into the Hall of Fame of the Texas Broadcasters Association.[8]Governor George W. Bush named Formby to the board of the Texas Telecommunication Infrastructure Fund. In addition to KPAN, Formby’s other remaining radio stations include KSAM/KHUN in Huntsville and KTEM in Temple, Texas. Formby was a former board member of Broadcast Music, Inc.[9]
 ​
Formby was involved over the years in civic affairs in Hereford, first as a member of the Jaycees. He was a past president of the Kiwanis Club as well as district lieutenant governor and was involved in chartering two new clubs in West Texas. He served as president of Deaf Smith County Chamber of Commerce and in 1964 received the "Citizen of the Year" award from that group. He also won the "Good Neighbor Award" from the West Texas Chamber of Commerce. He was a member of First Baptist Church, where he taught the boys' high school Sunday school class for several years. He and Margaret were later charter members of Fellowship of Believers Church. He received the Texas Communicator of the Year from the Southern Baptist Convention. He worked to bring cable television to Hereford as the managing partner of Hereford Cablevision from 1975 to 2006.[3]​
 Formby was the only person thus far to have appeared on the cover of Texas Highways magazine. He was also the only individual thus far to have served Texas Tech University as president of the student body, head of the Texas Tech Alumni Association, and chairman of the board of regents of the Texas Tech University.
[3]​
 ​Five children came from the marriage of Clint and Margaret Formby: Larry C. "Chip" Formby (born 1953), who worked with his father at the station, along with Chip's two sons by his wife Lisa, Jonathan and Lane Formby. The other sons are Marshall Clark Formby and wife Betty along with their two children of San Antonio, Ben Formby, and Scott C. Formby (born 1961) and wife Kathy, all of New York City.[4] A daughter, Linda Kay Formby, predeceased her parents.​
 Formby died of cancer at the age of eighty-six. Services were held at First Baptist Church of Hereford on August 4, 2010, with then pastor Kyle Streun, who in 2016 relocated to the First Baptist Church of Denver City, Texas, officiating.[10] He is interred beside his wife and daughter at West Park Cemetery on the west edge of Hereford.​[3]
 ​
​​​​​​
 1 Background 2 Radio career 3 Civic matters 4 Family and death 5 References ↑ 1.0 1.1 1.2 Henri Brickey, Hereford radioman still going after decades. The Lubbock Avalanche-Journal, (March 6, 2008). Retrieved on December 21, 2009; no longer on-line.
 ↑ 2.0 2.1 2.2 Social Security Death Index. ssdi.rootsweb.ancestry.com. Retrieved on December 20, 2009; under pay wall.
 ↑ 3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 Clint Formby obituary. The Lubbock Avalanche-Journal. Retrieved on November 18, 2019.
 ↑ 4.0 4.1 Douglas Martin (April 20, 2003). Margaret Formby, 73, Dies; Began Cowgirl Hall of Fame". The New York Times. Retrieved on November 18, 2009.
 ↑ Robena Formy Duncan obituary. The Lubbock Avalanche-Journal (January 18, 2009). Retrieved on December 18, 2009; no longer on-line.
 ↑ 6.0 6.1 6.2 6.3 The Old Philosopher. kpanradio.com. Retrieved on November 18, 2019.
 ↑ KPAN Radio. kpanradio. Retrieved on November 18, 2019.
 ↑ Texas Association of Broadcasters Bulletin. enewsbuilder.net. Retrieved on December 21, 2009; no longer on-line.
 ↑ Margaret Formby, former National Cowgirl Hall of Fame Director, Dies in Texas. bmi.com. Retrieved on November 18, 2019.
 ↑ Pastor Kyle Streun. First Baptist Church of Denver City, Texas. Retrieved on November 18, 2019.
 Texas Radio Journalists Business People Southern Baptists United States Army World War II Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 18 November 2019, at 15:28. This page has been accessed 8,921 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 John Clinton "Clint" Formby​ Political party  Born Died Spouse Children Parents Alma mater Religion John Clinton Formby Clint Formby 
  Political party 
  Democrat in 1962; thereafter claimed to be Independent
 
  Born
  December 22, 1923​ McAdooDickens County Texas, USA​
 Died
  July 31, 2010 (aged 86)​ ​
 Spouse
  Margaret Clark Formby (married 1950-2003, her death)​
Children:
Larry C. "Chip" Formby
Ben Formby
Marshall Clark Formby
Scott C. Formby
Linda Kay Formby (deceased)​
Parents:
John C. and Willie Elsby Formby​
Alma mater:
McAdoo High School
University of Basel
Texas Tech University​
 Religion
  Southern Baptist​
 Clint Formby Contents Background Radio career Civic matters Family and death References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
