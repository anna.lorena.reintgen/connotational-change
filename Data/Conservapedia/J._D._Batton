John Willis Batton​
Karen Sue Batton​
 Tyler (Texas) Commercial College​
 Businessman​
 John David Batton, known as J. D. Batton (February 13, 1911 – February 10, 1981),[1] was from 1952 to 1964 the sheriff of his native Webster Parish in northwestern Louisiana. He was defeated after three terms by O. H. Haynes, Jr., a fellow Democrat and the son of the former sheriff, O. H. Haynes, Sr., whom Batton had himself unseated twelve years earlier.​​
 Born in  Minden, Batton was the older of two sons of James Bryant Batton (1880-1939) and Nolie K. Batton (1881-1971). Batton graduated in 1929 from Minden High School and for a year thereafter attended Tyler Commercial College in Tyler, Texas.[2]​
 Batton's father, J. B. Batton, a native of rural Dubberly in south Webster Parish,[3] was a former Webster Parish deputy sheriff, a two-term Minden police chief,[4] and an unsuccessful candidate for sheriff in an all-Democratic special election conducted on June 6, 1933. Held barely a month after a devastating tornado struck the city on May 1, the election was required to replace Sheriff Arthur Montgomery Hough (1873-1933), who died on May 7 of influenza.[5]​
 As the chief deputy under Sheriff Hough, the senior O. H. Haynes won the special election in which 80 percent of registered voters cast ballots. His bare majority, 50.8 percent of the vote, negated the need for a runoff election. The runner-up in the race, with 22.2 percent, was Louie A. Jones (1900-1965),[6] the assistant superintendent of the Louisiana State Police and personal bodyguard of Governor Huey Pierce Long, Jr., and subsequently the warden of the Louisiana State Penitentiary at Angola in West Feliciana Parish. Another candidate, J. D. Huckaby, was the president of the Webster Parish Police Jury. J. Bryant Batton finished last in the race with 7.8 percent of the ballots cast; he won a plurality only in Dubberly.[7] Haynes, Sr., held the sheriff's position for nineteen years.[8]
 Like his father, J. D. Batton, was the Minden police chief prior to making his own first run for sheriff.[9] His brother, Jack Batton, was a businessman and rodeo enthusiast who served on the Minden City Council and as mayor from 1978 to 1982. At times J. D. Batton was in business with his brother.[2] In January 1948, while his brother was already a city council member, J. D. Batton lost a race for Webster Parish clerk of court to the incumbent Thomas Jenkins "Tom" Campbell (1895–1968). Batton received 3,037 votes to Campbell's 4,528.[10] Campbell was part of a local political family[4] whose current members include Louisiana Public Service Commissioner Foster Lonnie Campbell, Jr., and the Bossier-Webster District Attorney Schuyler Marvin.​
 After Batton's defeat for clerk of court, the position of Minden police chief opened when on March 1, 1948, Benjamin Garey Gantt (1890-1948), an Alabama native[11] who had been the chief since 1944, died suddenly of a heart attack. Gantt had been an unsuccessful candidate for sheriff a few weeks earlier in the race against the senior Haynes. Gantt's widow, whose first name was Clyde (1889-1950), briefly filled the position of police chief pending the election for a full term.[12] Not long before his death, Gantt had been one of six defendants charged in the lynching at Dorcheat Bayou near Minden of John Jones and Albert Harris, Jr., two African-Americans. The charges against Gantt were dropped because of his cooperation with federal authorities.[13]​
 Batton nearly won the race for police chief in the first primary in a six-candidate field. In runoff balloting, Batton defeated another Democrat, C. M. Ritchie (born c. 1897), a former deputy sheriff and former employee of the since closed Louisiana Army Ammunition Plant, 935 to 632 votes.[14]​
 Police Chief Batton and Sheriff O. H. Haynes, Sr., net in a runoff election held on February 19, 1952. Batton unseated the senior sheriff by 43 votes, 5,444 (50.2 percent) to 5,401 (49.8 percent).[15] On January 17, 1956, in conjunction with the successful gubernatorial comeback waged by Earl Kemp Long, Batton thwarted the attempt by former Sheriff Haynes, then sixty-seven, to return to office as well as the candidacy of C. Kelly McWilliams of Sarepta. Batton received 5,254 votes; Haynes, Sr., 3,134, and McWilliams, 1,493.[16]​
 Shortly after his first reelection, Batton was named one of seventeen directors of the Webster Parish Citizens' Council, a body which sought to prevent school desegregation.  Others in the council were the mortician Ed Kleinegger, Tax Assessor Richard B. Garrison, and the Minden High School principal, W. W. Williams, Sr..[17]​
 Long-serving deputy Clarence Roland Hennigan (1905-1992),[18] a native of bordering Bienville Parish and the father of the retired professional football player Charlie Hennigan, joined the sheriff's department under Batton and was still in service at the time of his death[18] At the time he was the oldest active deputy in the state. Hennigan recalls that in 1952 the department had eight deputies, two per shift. So primitive were communications that the sheriff's telephone was on a pole in front of the current downtown courthouse, which opened in 1953.[9]​
 Another long-serving deputy who began his law-enforcement tenure in the tax-collection division in 1956 under Sheriff Batton was Thomas Cameron "T. C." Bloxom, Jr. (1929-2014), a native of Mansfield in DeSoto Parish. Bloxom continued in the department and was named chief criminal deputy under Sheriffs Haynes, Jr., and then Royce L. McMahen until he resigned in 1982, joined the coroner's office as an investigator,[19] and then challenged McMahen unsuccessfully for reelection to a second term as sheriff.[20] The Democrat Bloxom ran again for sheriff in 1999, when he polled 45.7 percent of the vote against the incumbent Ted Riser.[21]​ McMahen's son, Royce Wayne McMahen is a Republican and the current state representative for Webster Parish. 
 In 1962, Batton hired as a deputy John T. Kennon, Jr. (1928-2005), a nephew of former Louisiana Governor Robert F. Kennon, a former Minden mayor, district attorney, and circuit court judge.[22] John Kennon later served as the Minden city marshal and ran unsuccessfully for sheriff in the 1971 Democratic primary against O. H. Haynes, Jr.​
 Batton listed his principal accomplishments as sheriff: 24-hour patrols, opening a sheriff's sub-station in Springhill, a parish-wide radio communications system, rescue trucks, escorts for funerals and school events, expansion of the criminal investigation office, and the establishment of a juvenile officer position,[23][24] which was proposed to be jointly funded by the sheriff, the cities of Minden and Springhill, and the Webster Parish Police Jury. Willard Harvey McClung (1932-2017) was appointed to the position and sworn into office by 26th Judicial District Judge Enos McClendon.[25] In 1974, McClung ran for mayor of Minden but lost the Democratic nomination to J. E. "Pat" Patterson,[26] who then unseated the Republican incumbent, Arthur Thomas "Tom" Colten (1922-2004).
 Early in 1959, Sheriff Batton joined Otis F. "Bill" Minter, Sr. (1902-1960), his successor as police chief, and Dr. Thomas A. Richardson (1911-1976), the Webster Parish coroner, in the investigation of the mysterious death of Minden railroad engineer Marcelous Douglas Cheshire (1906-1959), who was found with a blow to his head and his wallet missing on January 4 in the parking lot of a local grocery store. He died three days later in a Shreveport hospital. Cheshire and his wife, later Ruth Cheshire Lowe (1906-1991), owned the Joy and the Joy Drive-In theaters in Minden,[27] both long since closed. The investigation first focused on Duke E. Schultz (1929-1999),[28] a then 30-year-old gasoline station attendant with a history of arrests, a suicide attempt, and mental instability. Batton said that he believed Cheshire was struck by a rubber hammer used in service station work.[29] Nevertheless, after more than a half-century, the Cheshire death remains unsolved.[30]
 Batton announced a crackdown on obscene telephone calls at a time in which Caller ID was unavailable. "Persons making those calls will be apprehended and brought to trial," Batton promised while he noted a recent six-month jail sentence received by one violator.[31]​
 In the 1959 sheriff's campaign for his third term, Batton pledged to continue "a business-like administration" of the department, to fight juvenile delinquency and to maintain "our southern way of life."[32]​
 In the 1959 returns, Batton narrowly prevailed over two opponents, businessman Lawrence Harold Gilbert (1911-1995) of Minden and Thomas Guice McGinty (1893-1983) of Sibley, the older brother of the Louisiana historian Garnie William McGinty (1900-1984). Batton polled 4,211 votes to Gilbert's 4,063. McGinty held the remaining 145 votes cast, three votes more than required to avoid a runoff contest.[33] Sixteen absentee ballots from the Minden City Hall precinct which were not available for counting on the night of the election were tossed aside. A recount increased Batton's total to 4,222; Gilbert, 4,065, and McGinty, 148. Batton was four votes, instead of three, ahead of the 4,218 need to win the position outright.[34]​
 Though Harold Gilbert had threatened legal action,[35] he soon conceded his narrow loss. According to Gilbert, the attorney that he consulted said that any appeal had to be presented by midnight December 20 but more time was needed to prepare for any challenge which was likely to be successful.[36] Gilbert further indicated that any plan for legal action died when the Webster Parish Democratic Executive Committee certified Batton as the party nominee for sheriff.[37]​
 Batton's salary as sheriff in 1961 was $10,715, or $88,500 in 2019 dollars. Some $104,000 was disbursed for salaries of all the deputies.[38]​
 In a field of eight candidates, the junior Haynes emerged as Batton's principal rival in the 1963-1964 primaries. Haynes had once been a deputy under his father's tutelage and thereafter for eight years supervised the state driver's license office with jurisdiction over all of North Louisiana.[39] He was also the Exxon distributor in Webster Parish for some four decades. Freddie Lynn "Fred" Haynes (1946-2006), the second of four sons of the junior Haynes, was a  Louisiana State University Tigers football star. Fred Haynes was the winning quarterback for his Minden High School Crimson Tide team which won the 1963 state championship.[8]​
 Other candidates included Royce L. McMahen, a veterinarian and a member of the Springhill City Council in Springhill in the northern end of the parish, Harold Gilbert, by then the Minden police chief, H. G. "Cy" Williams, an automobile dealer who was then the president of the Minden City Council,[40] and offered an "Independent and conservative" platform.[41] and G. E. "Doc" Steed, a former contractor and the town marshal in Cotton Valley.[42] Another primary candidate for sheriff, Guice McGinty ran again in 1963 and polled thirty-three votes parishwide. George Alvin Pipes (1913-1976), a graduate of Plain Dealing High School in Bossier Parish, a guard at the Louisiana Army Ammunition Plant, and a pipefitter from Dubberly ran in 1963[43] too and later switched to Republican affiliation to challenge Haynes in the general elections of February 6, 1968, and February 1, 1972.[44][45]​
 Haynes trailed Batton by 722 votes in the primary, 1,993 to 2,719 votes.[46] McMahen finished third with 1,484 votes in the primary. He then endorsed Haynes in the runoff contest with Batton, and Haynes announced McMahen as his choice for chief deputy. In the second campaign, Haynes ran a newspaper advertisement in which he promised "capable, conscientious, and sober leadership." He claimed that the issue was not one of physical equipment or the training of deputies but the leadership of the sheriff.[47] In the January 11 runoff, Haynes prevailed, 5,190 votes (53.4 percent) to Batton's 4,523 (46.6 percent).[48][49]​
 As sheriff, Haynes appointed the first African-American deputy in the 20th century, Louis Dunbar, Sr. (1914-1986),[50] whose son "Sweet" Lou Dunbar became, like Haynes' son, a distinguished athlete.[51]​
 In November 1967, Haynes scored a second nomination as sheriff by again defeating Batton and another primary rival, Chester J. "Red" Vaughan (1909-1992), a service station operator in Springhill[52] and a former city marshal, who described himself as an "independent" within the Democratic Party and was critical of "political bosses".[53] Vaughan also challenged Haynes in regard to unsolved crimes growing in number, such as the theft of cattle and oilfield equipment and the stripping of tires and accessories from automobiles.[54]​
 Haynes prevailed with 6,952 ballots (53.3 percent) to Batton's 5,456 (41.8 percent) and Vaughan's 634 (4.9 percent). Batton won the Minden area by five hundred votes but fell short in the remainder of the parish.[55]​
 Batton was married to the former Maxine Evans (1916–1982). The couple was Southern Baptist and had three children, Roger Evans Batton (1943–2001), married to the former Frances "Gayle" Barton (1946-2018), a native of Winnsboro in Franklin Parish;[56] John Willis Batton (born 1947), a businessman from Hot Springs, Arkansas, and his wife, Judith Yvonne "Judy" Batton (born 1944), and Karen Sue Batton (born 1949) of Heflin in south Webster Parish.[57]​
 Batton died in Minden three days before his 70th birthday in 1981. He, his wife, son, and mother are interred there at Gardens of Memory Cemetery.[58] His father, J. B. Batton, is interred at Brushwood Cemetery in Dubberly;[1] his brother, Jack Batton, the mayor of Minden from 1978 to 1982, and nephew James Howard "Jimmy" Batton (1943–1997), are interred at the Minden Cemetery. Jimmy Batton was a sheriff's deputy and later an investigator for the district attorney's office.[59]​
 ​​​ ​
 1 Background 2 Law enforcement career
2.1 Minden police chief
2.2 Webster Parish Sheriff
2.2.1 Key deputies
2.2.2 Accomplishments
2.2.3 Reelection by four disputed votes, 1959
2.2.4 Defeat in 1964
2.2.5 The failed comeback attempt
 2.1 Minden police chief 2.2 Webster Parish Sheriff
2.2.1 Key deputies
2.2.2 Accomplishments
2.2.3 Reelection by four disputed votes, 1959
2.2.4 Defeat in 1964
2.2.5 The failed comeback attempt
 2.2.1 Key deputies 2.2.2 Accomplishments 2.2.3 Reelection by four disputed votes, 1959 2.2.4 Defeat in 1964 2.2.5 The failed comeback attempt 3 Personal life 4 References ↑ 1.0 1.1 "Former Chief, Sheriff J. D. Batton, Succumbs", Minden Press-Herald, February 10, 1981, p. 1.
 ↑ 2.0 2.1 "J. D. Batton Issues Statement Giving Candidacy", Minden Herald, October 24, 1947, pp. 1, 5.
 ↑ James Bryant Batton. findagrave.com. Retrieved on September 12, 2014.
 ↑ 4.0 4.1 Minden Herald, October 24, 1947, p. 1.
 ↑ "Parish Pays Final Tribute to Sheriff Hough Monday", Minden Signal-Tribune, May 9, 1933, p. 1.
 ↑ Louie A. Jones (1900-1965). findagrave.com. Retrieved on September 12, 2014.
 ↑ John A. Agan. Echoes of the Past: Sheriff's Race 1933. Minden Press-Herald. Retrieved on September 12, 2014.
 ↑ 8.0 8.1 
John Allison Agan (2002). Minden: Perseverance and Pride. Charleston,South Carolina: Arcadia Publishing Company. ISBN 9781439630532. Retrieved on September 1, 2014. 
 ↑ 9.0 9.1 Teri Herren, "Deputy C. R. Hennigan: 80-years young and still upholding the law", Minden Press-Herald', February 5, 1986.
 ↑ Minden Herald, January 23, 1948, p. 1.
 ↑ Benjamin Geary Gantt. search.ancestry.com. Retrieved on January 24, 2015.
 ↑ "Chief of Police Appointment Made by Council", Minden Press, March 12, 1948, p. 2.
 ↑ 
Rachel L. Emanuel and Alexander P. Tureaud, Jr. (2011). A More Noble Cause: A.P. Tureaud and the Struggle for Civil Rights in Louisiana. Baton Rouge: Louisiana State University Press. ISBN 978-0-8071-3793-2. Retrieved on February 13, 2015. 
 ↑ "Batton Given High Vote for Police Chief", Minden Herald, May 14, 1948, p. 1.
 ↑ "Batton Elected Sheriff," Minden Herald, February 21, 1952, p. 1.
 ↑ "Batton Wins in First Primary", Minden Herald, January 19, 1956, p. 1.
 ↑ "William Rainach to Speak at Council Meet Tomorrow Night," Minden Press, April 16, 1956, p. 1.
 ↑ 18.0 18.1 Clarence Roland Hennigan. Findagrave.com. Retrieved on September 12, 2014.
 ↑ Marilyn Miller, "T. C. Bloxom Retires from Law Enforcement: Well, I Gave It My Best Shot", Minden Press-Herald, September 6, 1982, p. 1.
 ↑ Results for Election Date: 10/22/1983: Webster Parish. Louisiana Secretary of State. Retrieved on September 13, 2014.
 ↑ Results for Election Date: 10/23/1999: Webster Parish. staticresults.sos.la.gov. Retrieved on September 15, 2014.
 ↑ "Kennon Hired As Sheriff's Deputy Here", Minden Herald, May 17, 1962, p. 1.
 ↑ Minden Press-Herald (advertisement), October 19, 1967, p. 8.
 ↑ Minden Press-Herald (advertisement), November 3, 1967, p. 6.
 ↑ "Full time juvenile officer sought for Webster Parish", Minden Herald, February 8, 1962, p. 1.
 ↑ "Pat Patterson is Democrat Mayor Nominee", Minden Press-Herald, August 19, 1974, p. 1.
 ↑ "M. D. Cheshire Rites Today; Probe Started",  Minden Herald, January 8, 1959, p. 1.
 ↑ Duke E. Schultz (1929-1999). findagrave.com. Retrieved on November 9, 2014.
 ↑ "Sheriff Holds Cheshire Suspect", Minden Press, September 14, 1959, p. 1.
 ↑ 1926: M. Douglas Cheshire. mindenmemories.net. Retrieved on September 12, 2014.
 ↑ "Crackdown promised on obscene calls", Minden Press, July 29, 1963, p. 1.
 ↑ Batton advertisement, Minden Press, November 30, 1959, p. 11.
 ↑ "Batton Apparent Winner in Webster Sheriff's Race", The Shreveport Times, December 7, 1959, p. 2-A.
 ↑ "Batton Increases Majority in Recount", Minden Herald, December 10, 1959, p. 1.
 ↑ "Batton Declared Winner After 16 Absentee Votes Are Voided; Gilbert Plans Legal Action", Minden Press, December 15, 1959, p. 1.
 ↑ "Sheriff's Race Uncontested; Write-In Candidate Hinted", Minden Herald, December 17, 1959, p. 1.
 ↑ "Candidate Denies Plan to Contest Close Vote", The Shreveport Times, December 16, 1959, p. 8-A.
 ↑ "1961 Audit Report Released for Sheriff's Salary Fund", Minden Herald, June 14, 1962, p. 1.
 ↑ 
John A. Agan (2000). Minden, Images of America. Charleston, South Carolina: Arcadia Publishing Company, 94. ISBN 0-7385-0580-3. Retrieved on September 1, 2014. 
 ↑ Minden Herald, July 5, 1962, p. 1.
 ↑ Minden Herald, May 29, 1962, p. 8.
 ↑ Minden Press, September 16, 1963, p. 3.
 ↑ "Seventh candidate for sheriff's race revealed", Minden Press, April 29, 1963, p. 1.
 ↑ "Haynes Winner Over Pipes in Sheriff's Race", Minden Press-Herald, February 7, 1968, p. 1.
 ↑ Minden Press-Herald, February 2, 1972.
 ↑ "Batton and Haynes Paired in Runoff for Sheriff's Post", Minden Press, December 9, 1963, p. 1.
 ↑ Minden Press, January 6, 1964.
 ↑ Minden Press, January 13, 1964, p. 1.
 ↑ "Haynes Defeats Batton in Webster Runoff Vote", The Shreveport Times, January 12, 1964, p. 1.
 ↑ "Haynes for Sheriff:  1967 Election Guide", Minden Press-Herald, October 30, 1967, p. 8.
 ↑ Louis Dunbar. YouTube. Retrieved on September 1, 2014.
 ↑ "'Red' Vaughan One of Three Seeking Parish Sheriff's Post", Minden Press-Herald 1967 Election Guide, p. 6.
 ↑ Minden Press-Herald, October 10, 1967, p. 2.
 ↑ Minden Press-Herald (advertisement), November 1, 1967, p. 11.
 ↑ "Haynes Uses Ward 2 Margin for Victory", Minden Press-Herald, November 6, 1967, p. 1.
 ↑ Frances "Gayle" Batton obituary. The Minden Press-Herald (October 29, 2018). Retrieved on October 31, 2018.
 ↑ Roger Evans Batton (1943–2001). The Shreveport Times (November 30, 2001). Retrieved on September 12, 2014.
 ↑ John David "J. D." Batton. Findagrave.com. Retrieved on September 12, 2014.
 ↑ "James Howard (Jimmy) Batton", Minden Press-Herald, December 30, 1997, p. 1.
 Copied Articles Louisiana People Politicians Democrats Baptists Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 11 October 2019, at 13:43. This page has been accessed 993 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 ! In office In office John David Batton J. D. Batton !
 This article was slightly edited from Wikipedia but the text was originally written by BHathorn (under the name) and does not include alterations made by others from that site.
 
 John David "J. D." Batton​
 Sheriff of Webster Parish, Louisiana​
 In officeJuly 1, 1952​ – May 1, 1964​
  O. H. Haynes, Sr.
  O. H. Haynes, Jr.​
 Chief of Police of Minden, Louisiana​
 In officeJuly 1, 1948​ – July 1, 1952​
  Mrs. Benjamin Garey Gantt (interim)​
  Otis F. Minter, Sr. ​
 
  February 13, 1911​Minden, Louisiana​
  February 10, 1981 (aged 69)Minden, Louisiana​
  Gardens of Memory Cemetery in Minden​
  American​
  Democrat​
  Maxine Evans Batton ​
  Jack Batton (brother)​
  Roger Evans Batton​
John Willis Batton​
Karen Sue Batton​
  Minden High School​
Tyler (Texas) Commercial College​
  Law-enforcement officer​
Businessman​
  Southern Baptist​
 J. D. Batton Contents Background Law enforcement career Personal life References Navigation menu Minden police chief Webster Parish Sheriff Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console Key deputies Accomplishments Reelection by four disputed votes, 1959 Defeat in 1964 The failed comeback attempt 
