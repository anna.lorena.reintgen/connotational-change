​ Tulane University Law School​
 ​
John Smoker Hunt, II (June 21, 1928 – January 14, 2001), was an attorney and a nephew of Governors Huey Pierce Long, Jr., and Earl Kemp Long who served on the elected Louisiana Public Service Commission from May 1964, to December 31, 1972. On September 30, 1972, Hunt was unseated in the  Democratic runoff primary election by Edward Kennon, a nephew of former Governor Robert F. Kennon, an intra-party rival of the Longs.​
​
 ​
Hunt was born in Ruston in Lincoln Parish in north Louisiana, to the former Lucille Long (1898–1985) and Stewart Smoker Hunt (1895–1966), a forester. Hunt's grandfather was John Smoker Hunt, I. Lucille Long, a native of Winn Parish, was the last of the nine children born to Huey Pierce Long, Sr. (1852–1937), and the former Caledonia Palestine Tison (1860–1913). Formerly a teacher in Shreveport in Caddo Parish, Lucille Hunt was a prominent civic leader thereafter in Ruston. Hunt also had a sister, Martha, who died in 1965.[2]
 Hunt graduated from Ruston High School and attended The Citadel, a military school in South Carolina, and Tulane University in New Orleans.  While at Tulane he was a member of the International legal fraternity Phi Delta Phi and received his law degree in 1950. He served his country in the armed forces from 1950 to 1953 both as an enlisted man and officer, including service during the Korean Conflict.  He and his wife, Rosemary, had three children, including Stewart T. Hunt (born September 9, 1960) of Lake Charles, Louisiana, and two daughters named Lucy and Mary.
 Hunt practiced law in Monroe in Ouachita Parish in northeastern Louisiana. In 1959, Hunt, along with Blanche Long, Victor Bussie, Russell Long, and others, flew Governor Earl Long out of state to a Galveston, Texas, hospital for treatment of mental illness, which resulted in Governor Long being relieved of his governor duties because he was  taken out of state and then committed to Southeast Louisiana Hospital, a Louisiana mental hospital.[3][4] Hunt resurfaced to public attention in 1964, when he was appointed by Governor John J. McKeithen to fill the remaining year and a half of McKeithen's term in the then District 3 (since District 5) seat on the state Public Service Commission,[5] a position originally held by Hunt's uncle, Huey Long. In February 1965, the two other commissioners named Hunt as the chairman because the presiding officer is traditionally the member whose seat is up for election in the next calendar year."[6]​
​
 ​
In August 1966, Hunt won a full six-year term on the regulatory body by defeating in a heated party runoff his fellow Democrat, then state Representative John Sidney Garrett of Haynesville in northern Claiborne Parish just south of the Arkansas state line.[7] While McKeithen endorsed Hunt for a PSC term of his own, he also had a good relationship with Garrett, whom he later tapped to be the state House Speaker from 1968 to 1972.[6] McKeithen also ​had close ties with a third candidate in the Democratic primary for the PSC, John Boyd McKinley (1924-2005), an Alexandria native who for ten years had been McKeithen's law partner in Monroe and was the governor's appointee as chairman of the Louisiana Sovereignty Commission, a former state agency which pursued states rights' issues in the 1950s and 1960s.[8][9]
 Hunt stressed that he had worked closely with the Louisiana Department of Commerce and Industry to create "thousands of new jobs" within Louisiana.[10] After an inconclusive first primary in which four candidates, including John B. McKinley, state Representative Parey Pershing Branton, Sr. (1918-2011) of Shongaloo and former legislator Wellborn Jack (1907-1991) of Shreveport, were eliminated, Hunt and Garrett met in the September 24 runoff election. Hunt had finished the primary with a considerable plurality.[11]
​
Garrett claimed after the primary that Hunt had received 93.1 percent of the votes of African Americans in nine selected precincts throughout the district, which then embraced a third of the state.[12] The runoff results were much closer. Hunt and Garrett each carried fourteen parishes; there were then twenty-eight parishes in the district. Hunt prevailed by 9,896 votes: 91,971 (52.5 percent) to 83,075 (47.5 percent).[13] Hunt led in the more populous parishes of Rapides, Natchitoches, and three others where he had resided at one time or the other: Caddo, Lincoln, and Ouachita. Garrett won the entire northern tier of parishes which borders Arkansas except for Caddo on the west and East Carroll in the far northeast. He also won several parishes in north central and northeastern Louisiana: Grant, La Salle, Catahoula, Franklin, Richland, and Winn, the ancestral home of the Longs, who had traditionally remained loyal Democrats.[7]
 In the campaign, Hunt defended his two-year record on the PSC, which regulates all interstate transportation and utility services within the state except those owned by a municipality. He said that utility companies should make a "fair profit but no more." He listed his goals in a full term as providing parish-wide toll-free telephone service and to reduce intrastate tolls on calls."[6]​
 A self-described "conservative Democrat," Hunt had endorsed Republican presidential nominee U.S. Senator Barry Goldwater of Arizona in 1964. Hunt's cousin, Senator Russell Long, however, had worked unsuccessfully to carry Louisiana for U.S. President Lyndon B. Johnson. Garrett, a member of the state House since 1948, was the chairman of the former Joint Legislative Committee on Segregation, a panel once chaired by legendary state Senator William Rainach, also of Claiborne Parish. This particular runoff election was the first significant test in Louisiana politics between party factions since President Johnson had signed the Voting Rights Act into law the preceding year. The new law, which enforced the Fifteenth Amendment to the United States Constitution led to the registration of large numbers of African-American voters throughout the Deep South. Many of these newer voters provided crucial support to Hunt, who was seen as more moderate on the racial issue than the segregationist Garrett. In fact, Garrett, who won the backing of three of the eliminated primary candidates, had claimed that Hunt was dependent on the "black bloc vote." Some even accused Hunt of having catered to "black power" elements.[6]
 In his victory statement, Hunt said that he had "overcome a slanderous campaign, and by winning I have tremendously enhanced the image of this state. . . . I was known by my opponent and his associates to be a conservative, but in spite of this, they attacked my character and made charges that I was a liberal, despite my public record to the contrary."
 ​
Hunt sought a second term on the PSC in 1972. He told voters that he had never missed a PSC meeting during his eight years on the panel and had handled more than two thousand cases. He ran into serious opposition from Edward Kennon, who had placed third in the Democratic primary for lieutenant governor in 1971. In a first primary in August, Hunt trailed Kennon, 106,212 (40.8 percent) to 122,573 (47.1 percent). Another 31,692 votes (12.2 percent) were cast for a Long kinsman, "Huey P. Long" (1929–2004), then of Pineville in Rapides Parish. Hunt won only ten of the then thirty-three parishes in the district, including his home bases of Lincoln and Ouachita. Hunt accused Kennon of having recruited Long into the race to split Hunt's pro-Long backing. Kennon led in twenty-three parishes in the sprawling district, which then stretched as far south as West Baton Rouge Parish. He won 58 percent in his native Webster Parish and also procured pluralities in Natchitoches, LaSalle, DeSoto, Avoyelles, St. Landry, and the Long traditional stronghold of Winn, which Hunt had also lost in 1966 despite his family connections. Hunt's strongest parishes were Caddo, Bossier, Lincoln, Ouachita, and Jackson.[14]
 In the September 30 party runoff, Kennon easily defeated Hunt, 125,877 votes (58 percent) to 90,833 (42 percent), having procured twenty-nine parishes to Hunt's four. Hunt lost his native Lincoln Parish in the runoff by 176 votes and held his home base, Ouachita Parish, by a single vote, 15,502 to 15,501, presumably his own. Kennon was unopposed in the November 7 general election because, as in 1966, no Republican candidate qualified for the ballot.[15] Three Louisiana governors, Long, McKeithen, and Jimmie Davis all served on the PSC prior to having been elected to the state's top political position. A fourth, Kathleen Blanco, served on the PSC prior to having become lieutenant governor.​ Current Commissioner Foster Lonnie Campbell, Jr., fell short in a gubernatorial bid against Bobby Jindal in 2007.
 On February 1, 2014, Hunt, along with his aunt by marriage, Rose McConnell Long, was posthumously inducted into the Louisiana Political Museum and Hall of Fame in Winnfield. Six others were honored as well. All of the 2014 honorees were Democrats[16]​ though the GOP by that time had made considerable inroads in Louisiana. 
​
 ​
​​​​
 1 Background 2 The election of 1966 3 The ill-fated 1972 campaign 4 References ↑ Picture of John S. Hunt, II, with his wife and family in Hunt advertisement, Minden Press-Herald, July 31, 1972, p. 4.
 ↑ Obituary of Lucille Long Hunt, The Winn Parish Enterprise, March 6, 1985.
 ↑ Iris Kelso, "Newman's Long," The New Orleans Times-Picayune, p. B7; no longer on-line.
 ↑ "Long widow criticizes film: She says `Blaze' misrepresented Louisiana governor's affair," Dallas Morning News, December 14, 1989, p. A31
 ↑ Kim Chatelain (September 4, 2013). Mandeville's Bubby Lyons to be inducted into Louisiana Political Hall of Fame. The New Orleans Times-Picayune. Retrieved on October 20, 2013.
 ↑ 6.0 6.1 6.2 6.3 "Hunt Captures PSC Post," The Shreveport Times, September 25, 1966, p. 1.
 ↑ 7.0 7.1 Louisiana Secretary of State, Election Returns, September 24, 1966.
 ↑ John Boyd McKinley. Findagrave.com. Retrieved on September 12, 2014.
 ↑ Everyone gets into state politics. Lake Charles American Press (August 12, 1966). Retrieved on September 30, 2019.
 ↑ Minden Press-Herald, July 28, 1966, p. 5.
 ↑ Minden Press-Herald, August 15, 1966, p. 1.
 ↑ Minden Press-Herald, September 16, 1966, p. 4.
 ↑ Minden Press-Herald, September 26, 1966, p. 1.
 ↑ Louisiana Secretary of State, Election Returns, August 19, 1972.
 ↑ Louisiana Secretary of State, Election Returns, September 30, 1972.
 ↑ Who's famous?. Bossier Press-Tribune (October 2, 2013). Retrieved on October 2, 2013; no longer on-line.
 Louisiana People Attorneys Politicians Democrats Korean War Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 7 October 2019, at 11:14. This page has been accessed 352 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 In office John Smoker Hunt, II John Smoker Hunt, II
​
 Louisiana Public Service Commissioner​
 In officeMay 1964​ – December 31, 1972​
  John J. McKeithen​
  Edward Kennon​
 
  June 21, 1928​Ruston, Louisiana, USA​
  January 14, 2001 (aged 72) ​Monroe, Louisiana
  American
  Democrat​
  Rosemary Hunt[1]​
  Stewart T., Lucy, and Mary​
  Ruston High School​
Tulane University Law School​
  Attorney​
 John S. Hunt Contents Background The election of 1966 The ill-fated 1972 campaign References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
