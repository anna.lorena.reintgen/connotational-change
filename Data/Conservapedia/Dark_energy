In 1998, the Supernova Cosmology Project observed 42 Type Ia supernovae, most of these from the ground, in an effort to measure the rate of deceleration of the expansion of the universe.[3] (Type Ia supernovae are objects of easily discernible brightness and thus are favorite objects for standardization of redshift and hence of the speed of expansion.) These supernovae were actually much dimmer than expected, a finding that indicated an acceleration of expansion, not the deceleration that gravitational attraction would produce. A competing group, the High-Z Supernova Search Team, reported similar results from their observations of 14 other supernovae.[4] (The symbol z stands for redshift in this context.) The findings of an accelerated universe came as a profound surprise to all interested observers and commentators.[5] More recent surveys have shown that the discrepancy persists.[6][7]
 John Hartnett, in Starlight, Time and the New Physics,[8] reminds his readers that Moshe Carmeli first formed a new model, called Cosmological Relativity, and through this model predicted that the universe would in fact appear to be accelerating. He made this prediction in 1996, two years before the publication of the Type Ia supernova data and the introduction of the idea of "dark energy" into cosmological discussions. In making this prediction, Carmeli did not invoke either dark energy or dark matter.
 Hartnett extended Carmeli's model and discarded several assumptions that Carmeli initially had thought were safe. This included the assumption that the matter density of the universe is at the critical level for a "coasting" universe.
 The full derivation of Hartnett's field equation that describes the motion of far-off objects is included in Appendix 2 of his book. Briefly, Hartnett begins with Moshe Carmeli's Cosmological Relativity, which adds a dimension of the radial velocity of a far-off object to the Einsteinian dimensions of space and time. This radial velocity is related to the distance of the object by this equation:
 
 where  is a constant (evaluated at 4.28 * 1017 s) that is the reciprocal of the Hubble factor H0 in weak gravity.[9]
 Adding this dimension requires adding a new term to the classic space-time interval, and so:
 
 where r is the distance of the object from earth (or more properly, our galaxy).
 An observation of a far-off object is typically made at a given moment and from a given place, and so ds=dt=0. So the above equation simplifies to:
 
 Carmeli's solution is:
 
 where  is the mass-energy density fraction of the universe. Specifically, , where  is the critical mass density, above which the universe would be closed and destined to collapse.
 The integral of the above equation, expressed in dimensionless numbers, is:
 
 where  =  is the radial velocity of the object, as a fraction of the speed of light. This fraction is itself a function of the redshift of the object (symbol: z):
 
 The key fact is that the matter density anywhere in the universe is also a function of redshift. In the simple case of "flat" space,
 
 where  is the weighted-average mass-energy density of the universe in the present epoch.
 Hartnett tested these equations against the High-Z Supernova Search Team data. To do this, he used these equations to convert  to an absolute magnitude:
 
 Here,
 
 is an independent luminosity distance, and
 
 where  is the highest absolute magnitude of any given supernova and  is expressed in megaparsecs.
 Recall that  is a function of z, and r is sensitive to the mass-energy density fraction.
 Hartnett plotted the difference  against redshift for the supernovas studied most recently by Riess and Astier.[6][7] He then adjusted the parameters  and  to achieve the best statistical fit to the observations. He obtained these values:
 
 and
 
 The second value is consistent with a mass-energy density that is four percent of critical. This is about twice as much matter as Fukugita et al. have determined that the universe has within it, but also at the upper limit of the range of that value.[10] It is also the same as the fraction of mass-energy in the universe that most evolutionists now believe to be composed of ordinary or baryonic matter (see below).
 Hartnett's model thus predicts as much mass-energy as other astronomers have already estimated that the universe has, and no more, within the limits of that estimate.
 Reiss, Astier, and their respective teams did not use this model. Instead, they relied upon the Friedmann-Lemaître cosmological model. That model predicted far more mass-energy than the universe possesses, beyond any rational estimate.
 Saul Perlmutter, Michael Turner, and Martin White appear to have coined the term dark energy to name the phenomenon that is causing the apparent acceleration that they found.[11] The term appears again in the more comprehensive paper of Bahcall, Ostriker, Perlmutter, and Steinhardt, that proposes that a heretofore unsuspected form of energy "overcomes the gravitational self-attraction of matter and causes the expansion [of the universe] to speed up."[12][13]
 In 2001, Reiss and his colleagues used the Hubble Space Telescope to capture on film the furthest supernova then seen, SN1997ff, at a distance of 10 billion light-years. The magnitude and redshift of this object were consistent with a decelerating expansion. This is consistent with the model that Reiss and others were forming at the time, stating that the expansion of the universe was initially decelerating and later accelerated after its matter density dropped below a critical level.[14]
 In 2003, a survey of 11 Type Ia supernovae by the HST confirmed the earlier findings of an accelerated expansion of the universe. Astronomers consider this the most definitive evidence to date for the existence of dark energy.[15]
 Naturalistic cosmologists estimate that 70 percent of the total energy in the universe consists of dark energy.[17] Dark matter occupies another 25 percent. The remaining portion is the familiar, or baryonic matter of which all objects are made.[18]
 Uniformitarian cosmologists have three theories of what form this energy might take:
 This has not stopped them from speculating on what might happen to the universe in the future. They currently recognize three possibilities:[18]
 Other scientists raise the question of whether the proportion of dark energy in the universe is a fundamental quantity of space. If it is not, they say, then this universe is only one of many.[20]
 Dr. Walt Brown, a young earth creationist and opponent of atheistic evolution, wrote about the concept of dark energy:
 Dark energy is a mathematical convenience. Some authorities admit that it may never be detectable. One reason why no one would ever detect it is that it represents an error. Evolutionistic cosmologists have admitted that dark energy might represent an error of their understanding of gravity. Creationists suggest that the error is far more fundamental.
 David Cline wrote in Scientific American in 2003 the following:
 Timothy Clifton, at Oxford University, said in an interview published at Space.com on October 1, 2008 that the solar system might be enclosed in a volume of space having a disproportionate lack of matter. According to his "void model," light from far-distant objects appears dim, not because the objects are retreating from the earth or even because the cosmos is expanding, but because the void condition does not focus the light as a matter-filled cosmos normally would. Clifton expresses one misgiving about his model that clearly reveals his worldview, however: it would negate the so-called Copernican Principle and require astronomers and cosmologists to regard the earth as a special place.[24]
 Fermilab's Experimental Astrophysics Group has proposed a Dark Energy Survey, using a special 500-megapixel camera mounted on a ground-based telescope.[25]
 NASA and the United States Department of Energy have also proposed a Joint Dark Energy Mission, essentially a new, very-high-resolution space telescope. Three different telescope designs are now under study.[26] Timothy Clifton, the proponent of the "void model" mentioned above, hopes that observations by JDEM might provide a reliable test of his model.[24]
 1 The problem 2 Young Earth Creation Science solution 3 Evolutionist/uniformitarian view
3.1 The dark energy concept
3.2 Nature of dark energy
3.3 Implications of dark energy
 3.1 The dark energy concept 3.2 Nature of dark energy 3.3 Implications of dark energy 4 Criticisms of the dark energy concept
4.1 Creationist criticism
4.2 Secular criticism
4.3 A Competing Secular Explanation
 4.1 Creationist criticism 4.2 Secular criticism 4.3 A Competing Secular Explanation 5 Proposed investigations 6 External links 7 References  It is a fundamental property of the universe, as Albert Einstein originally suggested. Einstein's original idea was that this force, which he called a "cosmological constant," would exactly counterbalance gravity and thus keep all galaxies and other objects of similar size in the same place. Einstein initially discarded his own findings after Edwin Hubble showed that the universe was expanding. Some modern cosmologists suggest that Einstein might have been right after all.[13][14][16][17][18]  It is a previously unknown type of energy fluid or field, and perhaps even a fifth elemental force, in addition to the previously known four forces of gravity, the electromagnetic force, and the weak and strong nuclear forces. Some scientists name this new force "quintessence" (literally, fifth essence), a term that ancient Greek philosophers once coined for a fifth "element" of nature in addition to the four elements that they thought they knew (fire, air, earth, and water).[13][17]  It is not a new property or force but a manifestation of an error of our understanding of an old one, namely gravity.[13][17]  The universe will expand indefinitely and isolate our galaxy.[16]  The "quintessential" substance will reverse its repulsive effect and become attractive. This will stop the expansion and contract the universe into a point, an event they call the "Big Crunch."  The universe will expand rapidly enough to tear the fabric of space and ultimately cause all baryonic matter to disintegrate, an event they call the "Big Rip." Relativity Calculator - Glossary  ↑ New World Encyclopedia
 ↑ Michail S. Turner. Dark Matter, Dark Energy and Inflation: The Big Mysteries of Cosmology 0:01:46/1:11:39. Arizona connection, Lectures series. Retrieved on 2012-10-14. “Michail S. Turner professor, Kavli institute for Cosmological Physics, University of Chicago: His contributions include Coining the term dark energy ...and several key ideas that lead to dark matter theory of structure formation.”
 ↑ Perlmutter S., Aldering G., Goldhaber G., et al. "Measurements of Omega and Lambda from 42 High-Redshift Supernovae." Astrophys. J. 517 (1999) 565-586. arXiv:astro-ph/9812133v1 Accessed July 26, 2008
 ↑ Reiss AG, Filippenko AV, Challis P, et al. "Observational Evidence from Supernovae for an Accelerating Universe and a Cosmological Constant." Astron. J. 116 (1998) 1009-1038. arXiv:astro-ph/9805201v1 Accessed July 26, 2008
 ↑ Newman P, and Tyler P, eds. "Beyond Einstein: What is the Mysterious Dark Energy Pulling the Universe Apart?" NASA, n.d. Accessed July 26, 2008.
 ↑ 6.0 6.1 Reiss AG, et al., "Type Ia supernovae discoveries at z > 1 from the Hubble Space Telescope: Evidence for Past Deceleration and Constraints on Dark Energy Evolution", Ap. J. 607:665-687, 2004
 ↑ 7.0 7.1 Astier P, et al., "The Supernova Legacy Survey: Measurement of Ωm, ΩΛ, and w from the First Year Data Set," A&A 447:31-48, 2006
 ↑ Hartnett, John. Starlight, Time and the New Physics. Creation Book Publishers, 2007. ISBN 9780949906687.
 ↑ Evolutionistic astronomers might assume that this value gives the age of the universe; it does in fact give a value very close to the visible radius of the universe, measured in light-years. It probably does represent a value that an observer at the limits of the visible universe might measure for its age—because the Carmeli-Hartnett system also predicts tremendous time dilation at the center of the expansion.
 ↑ Fukugita M, Hogan CJ, and Peebles PJE, "The cosmic baryon budget", Ap. J. 503:518-530, 1998
 ↑ Perlmutter S, Turner MS, and White M. "Constraining dark energy with SNe Ia and large-scale structure." Phys. Rev. Lett. 83 (1999) 670-673. arXiv:astro-ph/9901052v2 Accessed July 26, 2008.
 ↑ Bahcall NA, Ostriker JP, Perlmutter S, and Steinhardt PJ. "The Cosmic Triangle: Revealing the State of the Universe." Science 28 May 1999: Vol. 284. no. 5419, pp. 1481-1488. doi:10.1126/science.284.5419.1481 Accessed July 26, 2008
 ↑ 13.0 13.1 13.2 13.3 Preuss P. "Dark Energy Fills the Cosmos." ScienceBeat, Lawrence Berkeley Laboratory, June 1, 1999. Accessed July 26, 2008.
 ↑ Knop RA, Aldering G, Amanullah R, et al. "New Constraints on $\Omega_M$, $\Omega_\Lambda$, and w from an Independent Set of Eleven High-Redshift Supernovae Observed with HST" Astrophys. J. 598 (2003) 102 arXiv:astro-ph/0309368v1 Accessed July 26, 2008
 ↑ 16.0 16.1 16.2 16.3 Chaikin, Andrew. "Dark Energy: Astronomers Still 'Clueless' About Mystery Force Pushing Galaxies Apart ." Space.com, January 15, 2002. Accessed July 26, 2008.
 ↑ 17.0 17.1 17.2 17.3 17.4 Williams G. "Dark Energy, Dark Matter." Science Mission Directorate, NASA, May 15, 2008. Accessed July 26, 2008.
 ↑ 18.0 18.1 18.2 18.3 Authors unknown. "Dark energy changes the universe." NASA, February 27, 2004. Accessed July 25, 2008.
 ↑ Hinshaw GF, and Griswold, B. "WMAP Mission Results." NASA, April 17, 2008. Accessed July 26, 2008.
 ↑ Britt RR. "Dark Energy Tied to Human Origins." Space.com, May 31, 2004. Accessed July 26, 2008.
 ↑ Brown, Walt, "Big Bang?", In the Beginning: Compelling Evidence for Creation and the Flood, 8th ed., 2008
 ↑ David B. Cline, "The Search for Dark Matter," Scientific American, Vol. 288, March 2003, p. 52.. Cited in Brown, Walt, Big Bang?" In the Beginning: Compelling Evidence for Creation and the Flood, 8th ed., 2008.
 ↑ Shiga D, "Is Dark Energy an Illusion?" New Scientist, March 30, 2007. Accessed January 6, 2009.
 ↑ Authors unknown. "The Dark Energy Survey." Accessed July 26, 2008.
 ↑ Newman P and Tyler P. "Beyond Einstein: The Joint Dark Energy Mission." NASA, n.d. Accessed July 26, 2008.
 Cosmology Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 9 April 2019, at 01:05. This page has been accessed 17,209 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 dark energy dark energy 607 447 503  “
  Neither “dark matter” (created to hold the universe together) nor “dark energy” (created to push the universe apart) has been seen or measured.  We are told that “most of the universe is composed of invisible dark matter and dark energy.” Few realize that both mystical concepts were devised to preserve the big bang theory.[21]
  ”
  “
  We know little about that sea. The terms we use to describe its components, dark matter and dark energy, serve mainly as expressions of our ignorance.[22]
  ”
 Dark energy Contents The problem Young Earth Creation Science solution Evolutionist/uniformitarian view Criticisms of the dark energy concept Proposed investigations External links References Navigation menu The dark energy concept Nature of dark energy Implications of dark energy Creationist criticism Secular criticism A Competing Secular Explanation Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
