Japan has what many consider to be the world's strictest gun control laws, and the idea is often put forward that the Japanese gun control laws should serve as model for other nations. However, although gun related crimes in Japan are lower per-capita than elsewhere, such reasoning ignores many other factors, which at the end of the day makes for an unpersuasive argument. Many people simply recite the statistics that show that Japan has less guns and less gun crime. However, the claim that fewer guns correlate to less violence is plainly wrong. Japan, even with its severe gun controls, still suffers from a murder rate similar to that of Switzerland, one of the most gun-intensive societies on earth.
 One United States Government advisory commission, in a 1973 report, while conceding that cultural factors may have something to do with Japan's lesser crime, argued that gun crime in Japan declined every year since 1964, when the controls were enacted in their current form.[1] But the selection of 1964 was highly artificial. Near absolute gun prohibition had existed for over three centuries, and the current system of controls was put in place during the American occupation, and formally codified in 1958. There was no reason to pick 1964 as the year that gun control went into effect.
 Many people cite the Japanese example, without understanding exactly what the law is, how it is implemented and enforced, as well as the culture within the country itself towards both gun ownership and authority. Although crime figures, especially gun-related crimes, are lower in Japan than many other places, it comes at a cost to the civilian population. This cost can be broadly summarised as a combination between increased police monitoring and reduced civil liberties.
 In January 2008, then-Prime Minister Yasuo Fu'kuda of Japan's ruling conservative LDP, in response to an increase in violent crimes committed by minors and organised crime (bouryokudan 暴力団), called for an amendment to the constitution, to allow for even tighter firearms control measures.[2]
 Extensive police authority is one reason the Japanese gun control system works. Another reason is that Japan has no cultural history of gun ownership by citizens.
 This culture (or lack thereof) dates back to the "Sword Hunt" (taiko no katanagari) declared on 29 August 1588, by one General Hideyoshi Toyotomi, which prohibited the ownership of swords and firearms by the non-noble classes. He decreed: "The people in the various provinces are strictly forbidden to have in their possession any swords, short swords, bows, spears, firearms or other arms. The possession of unnecessary implements makes difficult the collection of taxes and tends to foment uprisings... Therefore the heads of provinces, official agents and deputies are ordered to collect all the weapons mentioned above and turn them over to the Government." It can be argued that this marked the end of social freedom in Japan. The abolition of firearms probably would not have succeeded if Japan had a free economy or a free political system at the time.
 The Firearms Act opens by stating "No-one shall possess a fire-arm or fire-arms or a sword or swords" and there are very few exceptions to this rule.[3] The only firearm a Japanese citizen may own is a rifle or shotgun. Sportsmen are permitted to possess shotguns or rifles for hunting (hunting licenses are valid for 3 years) and for skeet and trap shooting, but only after a lengthy licensing procedure.[4] Without a license, a person may not even hold a gun in their hands. Civilians may also apply for licenses to possess air rifles, but can never own handguns. In addition, the ownership of small calibre rifles was outlawed in 1971, and although current rifle license holders may continue to own them, their heirs must turn them into the police when the license-holder dies.
 This has resulted in a tiny proportion of citizens owning guns privately (approximately 500,000 licenses in a population of 100 million [5]). However, gun-related crimes in Japan are also infrequent.
 The licensing procedure is thorough and strenuous. Any prospective gun owner has to first attend classes and pass a written exam. This is followed by classes held on a shooting range and a shooting test. 95% of applicants usually pass.[6]
 Even if they pass the safety exam, applicants then undertake a simple "mental aptitude test" at a local hospital, which ensures that the applicant is not suffering from any readily detectable mental illness. The applicant then provides the police with a medical certificate attesting that they are is mentally sound and not addicted to drugs. The total cost of the procedure is some ￥15000 ($150), with an additional ￥17000 ($170) for a 3-month hunting license.[7]
 The police then investigate the applicant's background, including his relatives, to ensure that both have no criminal records. In addition, known membership of "aggressive" political or activist groups automatically disqualifies an applicant. The police have the discretion to deny licenses to any person for whom "there is reasonable cause to suspect may be dangerous to other persons' lives or properties or to the public peace." [8]
 The test and all-day lecture are only held once a month. The lecture almost always requires that the licensee take a full day off from work—not a highly regarded activity by Japanese employers. An annual gun inspection is scheduled at the convenience of the police, and also requires time off from work. Licenses must be renewed every three years, with another all-day safety lecture and examination at police headquarters.
 Successful applicants are required to store their weapons in a locker, with any ammunition being kept in a separate locked safe. They are required to provide the police with a map of their apartment, which clearly indicates the location of the locker.  The license also allows the holder to purchase a few thousand rounds of ammunition, with each transaction being registered.
 There is no mandatory minimum penalty for unlicensed firearm possession. Some 81% of convictions for illegal firearm or sword possession usually result in sentences of a year or more, possibly because the majority of gun-related crimes are committed by gangsters. The maximum penalty is ten years in prison and a one million yen ($10,000) fine.
 Gun-related crimes do exist, but in very low numbers. There were only 30 reported crimes committed in 1989, using shotguns or air rifles.[9] Most violent crimes perpetrated with a handgun, are committed by members of the Bouryokudan, although must gun-related crimes that come to trial involve unlicensed possession, and not the commission of another crime. Including the possession cases, there are about 600 handgun crimes a year and 900 long gun crimes.[10]
 Tokyo is ranked amongst the safest major cities in the world. Actually Japanese cities account for 9 of the top 15 safest cities in the world [11] There are only 59,000 licensed gun owners live in Tokyo and per one million inhabitants, Tokyo has 40 reported muggings a year[12] The official homicide rate in Japan is around 1.2 homicide cases per 100,000 population. 
 Robbery is almost as rare as murder. Indeed, armed robbery and murder are both so rare that they usually make the national news, regardless of where they occur. Japan's robbery rate is 1.4 per 100,000 inhabitants. People can still walk just about anywhere in Japan at night.
 There is one almost over-riding factor that is the result of Japan's low crime rate. More than gun control, more than the lack of criminal procedure safeguards (see below), and more than the 
authority of the police, it is the pervasive social controls of Japan. It is these socially-accepted and internalised restraints on individual behaviour that makes Japan stand out from other countries. For example, the former Soviet Union, had severe gun controls, far less criminal justice safeguards, and a more unconstrained police force than Japan, but suffered from a much higher crime rate than Japan. During the 1960s, whilst the English-speaking world suffered from a decline in social controls and a rise in crime, social controls remained and crime fell in Japan.[14]
 In addition, the Japanese people fully accept the authority of their police and trust their government. In this cultural context, it is easy to see why gun control has succeeded in Japan - the people accept gun control with the same readiness that they accept other Government controls. Further, they have little incentive to disobey gun controls, since they have hardly any cultural heritage of gun ownership.
 The Japanese Government itself promotes a social climate for gun control by essentially disarming itself. The police, although they carry guns when on duty, rarely use them. This too, has a cultural background, as when the National Police Agency was created in the late 19th century, as part of the Meiji Restoration, many of the founder-members were former samurai, who were now unemployed as a result of the abolition of feudalism. Their code believed that guns were for cowards, and that real men fought using the martial arts. 
 Ironically, the Japanese police only started to carry firearms when ordered to do so in 1946 by General MacArthur. Two years later, when the American occupation forces noticed that few police officers had obeyed the order to arm, the Americans supplied the police with guns and ammunition.[15]
 The police only carry .38 (Smith & Wesson or Nambu .38s) special revolvers, and must always leave their guns at the station when off duty. Desk-bound police administrators, traffic police, most plainclothes detectives, and even the riot police do not carry guns.[16]
 Instead of using guns, the police rely on their martial arts training in judo or the use of their police batons. Indeed, police recruit training spends 60 hours on firearms training, compared to 90 hours on judo, and another 90 on kendo. After police training, very few officers show any interest in any further firearms training; but continued judo and kendo practice is frequent. Annual police martial arts contests are important events and most of the top ranks in judo and kendo are filled by police officers.
 The official Japanese police culture discourages the use of guns. Shooting at a fleeing suspect is unlawful under any circumstance and police and civilians can both be punished for any act of self-defense in which the harm caused was greater than the harm averted.[17]
 However, the police are equally equipped with draconian powers to enforce the gun controls.
 In legal terms, illegal gun possession, much like the possession of illegal drugs, is a "consensual offense", as there is no "victim" involved who would lodge a complaint with the police. Japanese police have wide-ranging search-and-seizure powers in order to find illegal weapons. The Firearms Act permits a policeman to stop and search anybody, if the officer feels that there is "sufficient suspicion that a person is carrying a fire-arm, a sword or a knife," or if he feels that somebody "is likely to endanger life or body of other persons judging reasonably from his abnormal behavior or any other surrounding circumstances."[18] If a weapon is found, the policeman may confiscate it and even if the confiscation is later found to be in error, the confiscated firearm is sometimes not returned.
 Ironically, the law allowing for special weapons searches is almost redundant, as routine searches by the police are commonplace. They can stop anybody they deem to look suspicious and ask them to show the contents of their purse, case or bag. Technically, citizens are not obligated to show the policeman what is in their bag, but given the Japanese culture of deferring to authority, plus the fact that a refusal could result in the citizen being taken in for further questioning, few refuse. Even if a policeman's search does discover a gun or any other illegal substance and the search is subsequently ruled illegal, it makes no difference to the case, as Japanese courts permit the use of illegally seized evidence.[19]
 One of the most important duties carried out by the police is the so-called "house visit." Twice a year, "residence information cards" are filled in by police officers during door-to-door visits of each household within their precinct. These cards contain information about who lives in the house and which family member to contact in case of emergency, the relationship between the various occupants of the house, their occupations, working hours, and even the make of car they own.[20] As part of the home visits, all gun licensees are checked, to ensure that their gun has not been stolen or misused, that it is securely stored as per regulations, and that the licensees are emotionally stable.
 However, police surveillance goes beyond house visits and checking up on license owners. Japanese police have a long-standing tradition of keeping tabs on the local populace and monitoring every aspect of life (a fact which came to light with the inclusion of such statistics as the disturbing "Background and Motives for Girls' Sexual Misconduct" in the nation's official year-end police report.[21]) Virtually every citizen is the subject of a police dossier of one form or another.
 Virtually all Japanese citizens believe that the police should be respected and not questioned. Due to the esteem in which the police are held, the Japanese people willingly co-operate with them, and this co-operation also extends to obeying the laws which almost everyone believes in. The Japanese people, and even the large majority of Japanese criminals, voluntarily obey the gun control laws. 
 This is despite the fact that as well as not having the right to bear arms, citizens also have no right to privacy against police searches and many other rights are so restricted that Japan could easily be called a "police state."
 If an individual is arrested, whatever the crime, the suspect may be detained - without bail - for a maximum of 28 days, before the prosecutor brings the suspect before a judge.[22] Bail can be denied if there is reason to believe it could interfere with the police's interrogation of the defendant. 
 Even if the initial 28 day detention period has expired, detention in a Japanese police station often continues, based on numerous pretexts, such as preventing the defendant from destroying evidence. Another common tactic is to re-arrest the suspect on another charge, called bekken taihou (別件大法; literally, "separate case, basic law"), which means a fresh 28-day detention period under interrogation. This "rearrest" often occurs whilst the suspect is still being held on the first charge. Some defendants may be held for several months without ever being brought before a judge. The courts approve around 99.5% of prosecutors' requests for extended detentions.[23][24]
 During a suspect's detention, he may only receive visits from his criminal defense lawyer, and these meetings are strictly limited to 15 minutes at a time and between 1 and 5 visits, depending on the length of the detention. These visits can even be denied if they hamper the police investigation. During the same time, police can interrogate the suspect for up to 12 hours a day and complaints of abuse and torture are rife.[25][26] Amnesty International have called the Japanese police custody system a "flagrant violation of United Nations human rights principles".[27]
 The Japanese legal system can best be described as "an omnipotent and unitary state authority." As all the law enforcement administrators in Japan are appointed and funded by the National Police Agency, the police are effectively protected from complaints from politicians or citizens.[28] The only check on the State's power is its own conscience.
 These excessive police powers have a significant impact on the implementation of gun controls. The Japanese gun control laws exist within a society that currently has little need for guns for self-defense purposes. The powers of the Police make owning and hiding an illegal weapon extremely difficult. Add to that a criminal justice system based on a Government which possesses the seemingly inherent authority to do as it pleases, and it results in a society where almost everyone accepts nearly limitless, unchecked Government power, and people do not wish to own guns, even to resist oppression or to protect themselves should the criminal justice system ever break down.
 The situation within Japan also explodes the myth that "fewer guns means fewer suicides." Although the Japanese gun-suicide rate is one-fiftieth of America's, the overall suicide rate is nearly twice as high as America's.[29]
 In particular, teenage suicide is 30% more prevalent in Japan than in America and on average two Japanese under the age of 20 kill themselves every day. Japan also suffers from double or multiple suicides, shinjuu (心中). These can range from young lovers whose parents disapprove of their relationship, to unrelated groups of people who plan their "suicide-meetings" online. In addition, parents resigned to suicide often take their children with them, at the average rate of one per day, in so-called oyako-shinjuu (親子心中 "parent multiple suicides"). Some 17% of all Japanese officially defined as homicide victims are actually children killed by suicidal parents.[30]
 This is one of the reasons that the official Japanese homicide rate is so low - if a Japanese mother kills her children and then herself, the police statistics sometimes record it as a family suicide, rather than a murder-suicide. Thus, Japan's tight-knit family structures, and social norms, also conspire to massage the true numbers relating to violent crime.
 1 Gun Control and Crime
1.1 Licensing
1.2 Is Japan Safe?
1.3 Comparison of Gun Crime Statistics Worldwide
1.4 Why is Japan "Gun Free"?
 1.1 Licensing 1.2 Is Japan Safe? 1.3 Comparison of Gun Crime Statistics Worldwide 1.4 Why is Japan "Gun Free"? 2 Enforcing the Law
2.1 Stop, Search and Seize
2.2 House Visits
2.3 Is Japan a Police State?
 2.1 Stop, Search and Seize 2.2 House Visits 2.3 Is Japan a Police State? 3 Gun Control and Suicide 4 References Source:  W. Cukier, Firearms Regulation: Canada in the International Context, Chronic Diseases in Canada, April, 1998 (These statistics updated to reflect most recent figures, as at January 2001).[13] Only the top and bottom five countries, with available information, are shown, out of a total of 25 countries on the list. ↑ National Advisory Commission on Criminal Justice Standards and Goals, in A National Strategy to Reduce Crime; January 1973; p 141
 ↑ 'Law Controlling Possession, Etc. of Fire-Arms and Swords' (1978), Law No 6, Art 3, EHS Law Bulletin Series, No 3920.
 ↑ 'Law Controlling Possession, Etc. of Fire-Arms and Swords' (1978), Law No 6, Art 4-23
 ↑ 493,000 shotgun and airgun licenses and 27,000 remaining rifle licenses in 1999
 ↑ The test covers the maintenance and inspection of the gun, loading and unloading cartridges, shooting from various positions, and target practice for stationary and moving objects. "Gun Control Laws in Foreign Countries; 1981; Library of Congress; Washington; Page 130
 ↑ Article 29 of the Act
 ↑ Articles 5-1 and 6-3 of the Act
 ↑ White Paper on Police 1990, Page 80
 ↑ D Bayley; Forces of Order: Police Behavior in Japan and the United States; University of California Press; 1976; p 168
 ↑ White Paper on Police 1990
 ↑ Anthropologist Joy Hendry attributes the low Japanese crime rate to the effect of group controls such as shame. J Hendry; Understanding Japanese Society; Routledge; 1989
 ↑ D Bayley; Forces of Order: Police Behavior in Japan and the United States; University of California Press; 1976; pp 164 & 180
 ↑ D Bayley; Forces of Order: Police Behavior in Japan and the United States; University of California Press; 1976; pp 162 & 170
 ↑ Keiho 刑法 (Japanese Penal Code), Art 37.
 ↑ 'Law Controlling Possession, Etc. of Fire-Arms and Swords' (1978), Law No 6, Art 24-2, EHS Law Bulletin Series, No 3920.
 ↑ Japan v Hashimoto, 32 Keishu 1672 (Supreme Court, 1st PB, 7 September 1978).
 ↑ L Craig Parker; The Japanese Police System Today: An American Perspective; Kodasha International; 1984
 ↑ National Police Agency, Japanese Government, White Paper on Police 1986 (excerpt), translation: Police Association (1986), page 70
 ↑ In terms of the law, detentions are only allowed for 3 days, which are followed by two 10 day extensions which must be approved by a judge. This can be followed by an additional 5 day extension. However, defense lawyers rarely oppose the request by the prosecution for an extension, out of fear of offending the prosecutor. L Craig Parker; The Japanese Police System Today: An American Perspective; Kodasha International; 1984
 ↑ The Joint Committee of the Three Tokyo Bar Associations for the Study of the Daiyö-Kangoku (Substitute Prison) System, Torture and Unlawful or Unjust Treatment of detainees in Daiyö-Kangoku (Substitute Prisons) in Japan (1989)
 ↑ K Nakayama; "Japan", published in G Cole, S Frankowski, and M Gertz (eds); Major Criminal Justice Systems: A Comparative Survey; Sage Press; 1987; page 181
 ↑ Baba v Japan (Sapporo High Court, 1950) (defense counsel had 20 minutes of access on the third day, and 30 minutes each on the ninth and tenth days of detention; this amount of access was deemed reasonable by the court)
 ↑ 'Japan, Behind the Myth of Japanese Justice', American Lawyer, July/August 1985
 ↑ "News and Notes"; 1990' 7 CJ International
 ↑ J Williams; "Japan: The price of safe streets"; Washington Post; 13 October 1991
 ↑ World Health Organization, World Health Statistics; pp 183 & 189; United States Bureau of the Census, Statistical Abstract of the United States
 ↑ M Iga and K Tatai, "Characteristics of Suicide and Attitudes toward Suicides in Japan", in N Farberaw (ed); Suicide in Different Cultures; University Park Press; 1975
 Japan Gun Control Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 9 April 2019, at 08:38. This page has been accessed 16,996 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 kuda of Japan's ruling conservative LDP, in response to an increase in violent crimes committed by minors and organised crime (bouryokudan 暴力団), called for an amendment to the constitution, to allow for even tighter firearms control measures.[2] Country Owners licensed? Firearms registered? Other % Households with Firearms Gun Homicides per 100 000 Gun Suicide per 100 000 Intentional Gun Deaths per 100 000 Country
 Owners licensed?
 Firearms registered?
 Other
 % Households with Firearms
 Gun Homicides per 100 000
 Gun Suicide per 100 000
 Intentional Gun Deaths per 100 000
 Japan
 Yes
 Yes
 Prohibits handguns with few exceptions
 0.60
 0.03
 0.04
 0.07
 Singapore
 Yes
 Yes
 Most handguns and rifles prohibited
 0.01
 0.07
 0.17
 0.24
 England and Wales
 Yes
 Yes
 Prohibits handguns
 4.00
 0.07
 0.33
 0.40
 Scotland
 Yes
 Yes
 Prohibits handguns
 4.00
 0.19
 0.30
 0.49
 Netherlands
 Yes
 Yes
 
 1.90
 0.27
 0.28
 0.55
 Northern Ireland
 Yes
 Yes
 Prohibits handguns
 8.40
 3.55
 1.18
 4.73
 France
 Yes
 Yes, except sporting rifles
 
 22.60
 0.55
 4.93
 5.48
 Switzerland
 Yes
 Yes
 
 27.20
 0.46
 5.74
 6.20
 Finland
 Yes
 Yes
 No prohibitions
 50.00
 0.87
 5.78
 6.55
 U.S.A.
 In some states
 Handguns, in some states
 Some weapons in some states
 41.00
 6.24
 7.23
 13.47
 Gun Control in Japan Contents Gun Control and Crime Enforcing the Law Gun Control and Suicide References Navigation menu Licensing Is Japan Safe? Comparison of Gun Crime Statistics Worldwide Why is Japan "Gun Free"? Stop, Search and Seize House Visits Is Japan a Police State? Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
