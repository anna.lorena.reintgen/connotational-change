Giovanni Domenico Cassini discovered Tethys, along with the moons Dione, Rhea, and Iapetus, in the latter half of the seventeenth century, during the reign of "Sun-King" Louis XIV of France.[1] Cassini named these moons the "Sidera Lodoicea" in honor of the king. Later generations (especially after the French Revolution) would not retain such regal flattery.[9]
 Sir John Herschel, son of the astronomer William Herschel, suggested the current names of the seven largest satellites of Saturn, including Rhea. Titan received a generic name, and the other six received names of the Titans of mythology. Tethys was the wife of Ocean and the mother of all rivers.[10]
 Tethys is in an almost circular orbit around Saturn, at a distance of 294,660 kilometers. Its sidereal month is about 1.89 Earth days. Tethys appears to be in a 2:1 orbital resonance with Mimas.[11]
 Tethys shares its orbit with two other moons that occupy its Trojan points, with Telesto in the leading point and Calypso in the trailing point.[11][12]
 Tethys actually lies within Saturn's E ring and might therefore be subject to a continuous shower of particles from this ring. This, some authorities suggest, might also be responsible for Tethys' relatively high albedo.[11]
 Tethys is in tidal lock with Saturn.
 Tethys has a density of 960 kg/m³, less than that of water. For that reason, astronomers now believe that Tethys is composed entirely of water ice.[5][8][11] Furthermore, its physical dimensions are most consistent with a homogeneous interior.[13]
 Astronomers currently suggest that the surface of Tethys is geologically old, on account of its many impact craters.[12] But the surface might have been re-formed in the past.[11] The craters of Tethys also have bright, reflective floors.[11]
 One of the two most prominent features of the surface of Tethys is the great chasm called Ithaca Chasma. This extends for three-fourths of the circumference of Tethys and measures 65 km wide and several kilometers deep. Most observers suggest that Tethys was once fluid and later froze, with the crust freezing before the interior.[5]
 The other feature is the Odysseus basin, a 400-kilometer-wide impact crater that dominates Tethys' western hemisphere. Today the floor of this basin conforms to the spherical shape of Tethys. This basin has no central mountain, and its walls are relatively low.[5][11][12]
 Tethys is in a circular orbit and therefore could not be subject to tidal heating. Yet if it had been a solid object at the time of the impact that made the Odysseus basin, it would have shattered. To explain this paradox, Chen and Nimmo suggest that Tethys once had a differentiated interior of "a silicate core, a liquid water ocean, a viscous icy mantle and a rigid icy crust."[14] This would require an eccentricity in the distant past as high as 0.02. Chen and Nimmo also believe that Tethys was once in a 3:2 orbital resonance with Dione, but they tend to doubt that this would have had a significant effect.
 The obvious problem with this theory is how Tethys could have changed its orbit to one that was nearly circular.
 Another problem is what sort of impact could have left the Odysseus basin without itself sending Tethys into a more eccentric orbit, and what became of the impactor. Arnett[8] suggests that the two Trojan moons that share Tethys's orbit might be remnants of that or another collision.
 Voyager 1 and Voyager 2 both photographed Tethys from a distance, though Voyager 2 made a much closer rendezvous.[15] However, the orbiter of the Cassini-Huygens Mission made an even closer rendezvous (at a distance of 1500 km) on September 23, 2005. No further rendezvous are planned at present.
 The Odysseus Basin, taken by Cassini
 Tethys, taken by Voyager 1; first view of Ithaca Chasma
 Tethys, taken by Voyager 2; another view of Ithaca Chasma
 1 Discovery and naming 2 Orbital characteristics
2.1 Co-orbital objects
 2.1 Co-orbital objects 3 Rotational characteristics 4 Physical characteristics
4.1 Surface
 4.1 Surface 5 Problems for uniformitarian theories posed by this body 6 Observation and Exploration 7 Gallery 8 References 
The Odysseus Basin, taken by Cassini
 
Tethys, taken by Voyager 1; first view of Ithaca Chasma
 
Tethys, taken by Voyager 2; another view of Ithaca Chasma
 ↑ 1.0 1.1 1.2 "Gazetteer of Planetary Nomenclature: Planetary Body Names and Discoverers." US Geological Survey, Jennifer Blue, ed. March 31, 2008. Accessed April 17, 2008.
 ↑ 2.0 2.1 2.2 2.3 2.4 2.5 Calculated
 ↑ 3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 Williams, David R. "Saturnian Satellite Fact Sheet." National Space Science Data Center, NASA, November 23, 2007. Accessed June 4, 2008.
 ↑ "Classic Satellites of the Solar System." Observatorio ARVAL, April 15, 2007. Accessed June 12, 2008.
 ↑ 5.0 5.1 5.2 5.3 5.4 5.5 Hamilton, Calvin J. "Entry for Tethys." Views of the Solar System, 2001. Accessed June 13, 2008.
 ↑ Jacobson, R.A., Spitale, J., Porco, C.C., and Owen, W.M., Jr. "The GM Values of Mimas and Tethys and the Libration of Methone." Astron. J. 132:711-713, 2006. doi:10.1086/505209 Accessed June 13, 2008.
 ↑ "Planetary Satellite Physical Parameters." Solar System Dynamics, JPL, NASA. Accessed June 13, 2008.
 ↑ 8.0 8.1 8.2 Arnett, Bill. "Entry for Tethys." The Nine 8 Planets, October 2, 2005. Accessed June 13, 2008.
 ↑ Boulay, J. C. "Saturne: les satellites." Astronomie-astronautique. Accessed June 5, 2008.
 ↑ Lassell, William. "Satellites of Saturn." Monthly Notices of the Royal Astronomical Society, 8(3):42-43, January 14, 1848. Accessed June 4, 2008.
 ↑ 11.0 11.1 11.2 11.3 11.4 11.5 11.6 Hubbard, William B.  "Tethys." Encyclopædia Britannica. 2008. Encyclopædia Britannica Online. 13 June 2008.
 ↑ 12.0 12.1 12.2 "Entry for Tethys." The Planetary Society, n.d. Accessed June 13, 2008.
 ↑ Thomas, P.C., Veverka, J., Helfenstein, P., et al. "Shapes of the Saturnian Icy Satellites." 37th Annual Lunar and Planetary Science Conference, 2006. Accessed June 13, 2008.
 ↑ Chen, E. M. A., Nimmo, F. "Thermal and Orbital Evolution of Tethys as Constrained by Surface Observations." Thirty-ninth Lunar and Planetary Science Conference, 2008. Accessed June 13, 2008.
 ↑ "Voyager Mission Description." February 19, 1997. Accessed June 13, 2008.
 Moons Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 26 September 2018, at 14:41. This page has been accessed 6,805 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Tethys Date of discovery Name of discoverer Name origin Primary Order from primary Perikrone Apokrone Semi-major axis Circumference Orbital eccentricity Sidereal month Avg. orbital speed Inclination Sidereal day Axial tilt Mass Density Mean radius Surface gravity Escape speed Surface area Mean temperature Composition Color Albedo Tethys Saturn III Tethys, showing Ithaca Chasma, taken by Cassini
  Date of discovery
  March 21, 1684[1]
  Name of discoverer
  Giovanni Domenico Cassini[1]
  Name origin
  Titaness, wife of Ocean and mother of all rivers
  Primary
 Saturn
  Order from primary
  10
  Perikrone
 294,660 km[2]
  Apokrone
 294,660 km[2]
  Semi-major axis
 294,660 km[3][4]
  Circumference
 1,851,026 km[2]
  Orbital eccentricity
 0.0000[3]
  Sidereal month
 1.887802 da[3]
  Avg. orbital speed
 11.36 km/s[5]
  Inclination
 1.86°[3] to Saturn's equator
  Sidereal day
  1.887802 da[3]
  Axial tilt
  0°[3]
  Mass
  6.18 * 1020 kg[3][6]
  Density
  960 kg/m³[3]
  Mean radius
  536.3 km[7]
  Surface gravity
  0.143 m/s²[2]
  Escape speed
  0.392 km/s[2]
  Surface area
  3,614,310 km²[2]
  Mean temperature
  86 K[5]
  Composition
  Water ice[5][8]
  Color
  Neutral gray
  Albedo
  0.8[3]
 v • d • e
The Solar SystemStarSolTerrestrial PlanetsMercury · Venus · Earth · MarsGas GiantsJupiter · Saturn · Uranus · NeptuneDwarf PlanetsCeres · Haumea · Makemake · Pluto · ErisAsteroid BeltMajor asteroids · C-type asteroids · S-type asteroids · M-type asteroidsTrans-Neptunian ObjectsKuiper belt · Scattered disk · Oort Cloud · NemesisSatellitesMoon · Phobos · Deimos · Io · Europa · Ganymede · Callisto · Mimas · Enceladus · Tethys ·  Dione · Rhea ·  Titan ·  Hyperion ·  Iapetus ·  Miranda · Ariel · Umbriel · Titania · Oberon · Triton ·  Nereid · Charon · Nix · Hydra · DysnomiaCategories  Star Sol   Terrestrial Planets Mercury · Venus · Earth · Mars  Gas Giants Jupiter · Saturn · Uranus · Neptune  Dwarf Planets Ceres · Haumea · Makemake · Pluto · Eris  Asteroid Belt Major asteroids · C-type asteroids · S-type asteroids · M-type asteroids  Trans-Neptunian Objects Kuiper belt · Scattered disk · Oort Cloud · Nemesis  Satellites Moon · Phobos · Deimos · Io · Europa · Ganymede · Callisto · Mimas · Enceladus · Tethys ·  Dione · Rhea ·  Titan ·  Hyperion ·  Iapetus ·  Miranda · Ariel · Umbriel · Titania · Oberon · Triton ·  Nereid · Charon · Nix · Hydra · Dysnomia  Categories Tethys (moon) Contents Discovery and naming Orbital characteristics Rotational characteristics Physical characteristics Problems for uniformitarian theories posed by this body Observation and Exploration Gallery References Navigation menu Co-orbital objects Surface Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
