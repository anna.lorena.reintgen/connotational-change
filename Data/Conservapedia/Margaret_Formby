(Educator and journalist who founded the National Cowgirl Museum and Hall of Fame)​
 Children:
Larry C. "Chip" Formby​
Ben Formby​
Marshall Clark Formby​
Scott C. Formby​
Linda Kay Formby (deceased)​
Parents:
Fred and Mabel Clark​
Relations:
Marshall Formby (uncle by marriage)
Robert Duncan (cousin by marriage)
Alma mater:
Van Horn (Texas) High School
Texas Tech University
 ​
Margaret Clark Formby (July 12, 1929 – April 10, 2003),[1] was the daughter of southwest Texas ranchers who founded the National Cowgirl Museum and Hall of Fame[2] in the basement of the public library in Hereford in Deaf Smith County, Texas, before she relocated the collection to Fort Worth.[3]​
​
 Formby was born to Fred and Mabel Clark in Van Horn in Culberson County east of El Paso.[4] She once described her upbringing as "growing up western," and she fought to recognize women for their influence in western culture.[5] She graduated second in her class in 1946 from Van Horn High School and attended Texas Tech University in Lubbock.[1]
 Formby launched her collection of western and rodeo artifacts beginning in 1975 in Hereford,[3] where her husband, Clint Formby, was a radio broadcaster at the country-music station KPAN AM and FM.[6]​
 In 1994, the museum was relocated to a $21 million structure at 1720 Gendy Street in the heart of the Fort Worth cultural district. The first inductee into the Hall of Fame was Alice Greenough Orr (1902-1995) of Montana, winner of three national rodeo championships in the 1930s and 1940s. Orr rode bulls in Spain, was a riding instructor of Dale Evans Rogers, and performed film stunts even into her eighties. In 1993, Formby herself was the first woman elected to the Texas Tech University Rodeo Hall of Fame in Lubbock. The next year, she was inducted into the National Cowgirl Hall of Fame through her role as the founder of the museum.[3] She was also the first ever "Miss Texas Tech" in 1949 and graduated in 1950 as an English and speech teacher. She taught at the high school level in both Colorado City in Mitchell County and Hereford, and she worked as the KPAN news director. She was the editor of the Cowgirl Hall of Fame magazine, Sidesaddle.[4]​
 In 1996, Formby told The Dallas Observer that much of the history of the early cowgirls has been lost over the previous decades. Six years after she started her museum, a Hereford couple donated a 6,000-square-foot house to hold her collection, which included a rare rhinestone hat worn by Patsy Montana (1908-1996), whose "I Want to Be a Cowboy's Sweetheart" recording sold a million copies. She continued to add to the collection, but few visitors came to Hereford, and the museum faced financial failure. "We’d go for stretches, where for days on end, nobody would come," she told The Fort Worth Star-Telegram in a 2002 interview.[3] Concerts and bake sales and renting out the building for social occasions did not bring the needed revenue. She then sought to move the museum to a larger city where a steady flow of tourists would visit the expanded array of exhibits. When new people took over operation of the museum, Formby's role declined: "It's kind of like having a child and having to give that child up. It's sad."[3]​
 Conflict soon developed between Hereford and Fort Worth values in regard to the museum. The Hereford originators claim rude treatment and hurt feelings from the Fort Worth multimillionaire socialites who took over the museum. The relocation has since "bred a dust storm of conflict," referring to  a lawsuit that the new managers filed in Fort Worth [in 1996] against a well-liked New York City restaurateur who put Hereford's museum on the national map."[7]​
 Formby was also active in local and state civic affairs, including the Chamber of Commerce, the Kings Manor Methodist Home, and the American Cancer Society. In 1977, she was appointed to the Select Committee on Child Pornography: Its Related Causes and Control by the Texas House of Representatives. Serving with Formby on the panel was later Houston Mayor Bob Lanier (1925-2014) and former state Representative Claiborne Washington "Clay" Smothers, I (1935-2004), an African-American advocate for family values from Dallas.[8]​
 In 1982, she was appointed to the Texas State Committee on  Teenage Pregnancy. She served as president of Friends of the Library and the Southwest Collection at Texas Tech from 1986 to 1987 and was a board member of the National Ranching Heritage Center at Texas Tech from 1983 to 1985, having worked to develop the "Cowboy Symposium" held there each September.[1] In 2000, she donated $450 to the Republican National Committee.[9]​
​
 Margaret Formby died in her Hereford home after falling in her bathroom. Her body was found by her housekeeper.[3] She and her husband had five children, Larry C. "Chip" Formby (born 1953) who works at KPAN, and his wife, Lisa, Marshall Clark Formby and wife, Betty, of San Antonio, Ben Formby and Scott C. Formby (born 1961) and wife, Kathy, all of New York City, and the late Linda Kay Formby. She had four grandchildren and a sister, Mary Beth Clark Powell of Hereford.[3] Formby was a niece by marriage of former state senator and highway commissioner, Marshall Formby. She was a cousin by marriage of a former state Senator Robert Duncan of Lubbock.
 Services were held at the First Baptist Church of Hereford on April 14, 2003. Though she was a member of the Fellowship of Believers Church in Hereford, Formby often taught Sunday school and Bible study at her husband's Baptist church. Interment was at West Park Cemetery north of Hereford.[1]​
 In 2000, Formby was named to the "100 That Made a Difference: History Makers of the High Plains" by The Amarillo Globe-News.[10] She also received the "Pioneer Woman Award" from the American Cowboy Culture Society.[4]​
 ​
​​​​​
 1 Background 2 Career 3 Death and legacy 4 References ↑ 1.0 1.1 1.2 1.3 Margaret Formby obituary, The Amarillo Globe-News, April 13, 2003.
 ↑ Home: Cowgirl Hall of Fame & Museum. National Cowgirl Museum and Hall of Fame. Retrieved on November 18, 2019.
 ↑ 3.0 3.1 3.2 3.3 3.4 3.5 3.6 Douglas Martin (April 20, 2003). Margaret Formby, 73, Dies; Began Cowgirl Hall of Fame". The New York Times. Retrieved on November 18, 2019.
 ↑ 4.0 4.1 4.2 Thomas Korosec. Margaret Formby, former National Cowboy Hall of Fame Director, Dies in Texas. bmi.com. Retrieved on November 14, 2019.
 ↑ Hall of Fame & Museum Margaret Formby: Cowgirl Hall of Fame & Museum. Retrieved on November 18, 2019.
 ↑ Henri Brickey (March 6, 2008). Hereford radioman still going after decades. The Lubbock Avalanche-Journal. Retrieved on December 21, 2009; no longer on-line.
 ↑ Saddlesore: How Fort Worth's rhinestone socialites bushwacked the National Cowgirl Hall of Fame. The Dallas Observer. Retrieved on November 18, 2019.
 ↑ Child Pornography: Its Related Causes and Control. Texas legislative Reference Library. Retrieved on November 18, 2019.
 ↑ Margaret Formby from zip code 79045. watchdog.net. Retrieved on November 18, 2019.
 ↑ History Makers of the High Plains: Margaret Formby. amarillo.com. Retrieved on December 21, 2009; no longer on-line.
 Texas Women Journalists Educators Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 19 November 2019, at 19:27. This page has been accessed 395 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Margaret Clark Formby​ Political party  Born Died Spouse Children Parents Relations Alma mater Religion Margaret Clark Formby National Cowgirl Museum and Hall of Fame 
  Political party 
  Republican ​
 
  Born
  July 12, 1929​ Van Horn, Culberson CountyTexas, USA​
 Died
  April 10, 20003 (aged 73)​ Hereford, Deaf Smith County, Texas​
 Spouse
  John Clinton "Clint" Formby (married 1950-2003, her death)
Children:
Larry C. "Chip" Formby​
Ben Formby​
Marshall Clark Formby​
Scott C. Formby​
Linda Kay Formby (deceased)​
Parents:
Fred and Mabel Clark​
Relations:
Marshall Formby (uncle by marriage)
Robert Duncan (cousin by marriage)
Alma mater:
Van Horn (Texas) High School
Texas Tech University
 Religion
  Fellowship of Believers Church in Hereford
 Margaret Formby Contents Background Career Death and legacy References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
