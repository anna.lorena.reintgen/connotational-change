Etymology:
 As stated above, cryptography consists of encoding messages to prevent the information they contain from falling into the wrong hands, using a process called encryption. In its most general form, cryptography consists of ciphers (colloquially codes) that define a set of rules for converting the original message, or plaintext, into a hopefully undecipherable message, or ciphertext.
The study of cryptography is virtually always partnered with the study of cryptanalysis, which is the process of decrypting encoded messages, through logical and/or mathematical means of reversing the original cipher. Throughout history, the studies of these two fields have been inexorably linked.
 Historically, the study and art of cryptography consisted of the development, enhancement, and defense of simple techniques for disguising simple messages by converting them into other ambiguous forms in a process called encryption. These techniques are now referred to as classical cryptography. As is the case with most attempts at protection and secrecy of information, the development of classical cryptography was paralleled, like cryptography in the present day, by advancements in techniques of cryptanalysis.
 The civilizations of the Fertile Crescent often used simple character substitutions to protect trade secrets, from specific recipes of stronger clays to the basic financial and barter records of the royalty's treasuries. Although primitive by modern standards, these techniques of substitution form some of the earliest examples of cryptography, in any sense.[4]
 During the early years of the Old Kingdom of Ancient Egypt, archaeologists have found basic examples of encrypted hieroglyphs, in which certain symbols were exchanged for other seemingly nonsensical characters that, at first glance, appeared only to obscure the meaning of the text. However, it is primarily assumed that these substitutions were not made to cloister or protect critical information, but rather to provide enjoyment for the literate and intellectual members of the community.[4]
 As civilizations expanded, so did their respective methods and implementations of cryptography. Early in their history, the Spartan army developed the first known cryptographic device, albeit a simple one: a wooden rod of varying diameters known as a scytale. To encrypt a message, a Spartan soldier would wrap a thin strip of parchment around the scytale and write the message across it. Once unwound, the coil of parchment would be easy to transport, but when read straight across, it would appear as nothing more than a meaningless jumble of letters. Decryption was as simple as wrapping the parchment around a scytale of identical diameter to the one used in the original encryption, thus allowing the message to be easily read out.[5]
 In his writings, Herodotus reports the use of other forms of secret writing in the Grecian war to repel the Persian invasion. He writes of Greek soldiers cleanly shaving their heads, then writing messages on their bare skin before allowing their hair to grow back.[6] Although this more accurately describes stenography, it was often used in conjunction with the simple substitution ciphers that were common at the time. Despite the fact that this method did now allow for the quick transmission of message, it was still widely used against the Persian empire to communicate information of relatively low classification or importance.
 Throughout the history of Indian civilization, cryptography and the hiding of secrets advanced rapidly with the growth of their civilization. Many Indian rulers used cryptography to encode messages and directives to the vast network of spies they operated over the Indian subcontinent, as well as to covertly transmit and protect basic operational and financial information from subterfuge. Indian ciphers did not normally consist of direct character substitutions, but rather phonetic changes that allowed secret messages to be communicated through sign language and specialized spoken languages.[7]
 Because the ancient Chinese language, like its modern-day equivalents, was composed of a set of symbols that represented varying syllables, tones, and ideas,[8] it readily lent itself to a language of the cryptographic arts. Messages would be altered based on content, tone, and similarity between symbols to messages unrecognizable to other factions at the time or people without advanced knowledge of the language.[7]
 Although many examples of cryptography, secret transmission of messages, and the protection of information through encryption existed before the dawn of the Roman Empire, the cryptography used in Rome was the first such example that led to widespread military conquest. Julius Caesar was famous for using the so-called Caesar cipher, which consisted of a simple alphabetic shift by two characters to the right. For example, using the English word CAESAR would become:
 C
 A
 E
 S
 A
 R
 E
 C
 G
 S
 C
 T
 Decoding a message encrypted with the Caesar cipher follows a similar process, but decryption is possible by shifting an encoded message two characters to the left, in an exact reversal of the encryption process.[7]
 By encoding his battle plans, as well as the instructions to the commanders in the field, Caesar was able to hide his objectives from regional enemies and further the expansion of the Roman Empire.
 Although many examples of the simple substitution ciphers doubtlessly existed throughout the Middle Ages, few records of advancements in the cryptographic arts remain from the time period. However, near the end of the Middle Ages, the science of cryptography began to progress, albeit slowly at first. As powerful nation-states began to rise from the feudal predecessors, cryptography and the study of encryption began increasing in importance.
 The first and most important advances in the study of cryptography during this time period were made in Italy.[7] The city-state of Venice was the first of the regional Italian governments to dedicate a part of its government solely to the study and advancement of cryptographic techniques. As many other countries adopted these so-called "black chambers," it became more and more important to protect diplomatic communications between monarchs, ambassadors, and other governing officials.
 As the influence of the Renaissance became increasingly apparent and widespread throughout European civilization, so to did cryptography. In what is now regarded as one of the most important moments in the history of cryptography, Leon Battista Alberti, now regarded as the "Father of Western Cryptology," contributed several years of his life towards the development of poly-alphabetic substitution ciphers. By designing a cryptographic system that used, in the encrypted message, used different letters in different places for the same letter in the original message, he defeated the most common tactic of cryptanalysis at the time:  frequency analysis.
 In 1518, another major breakthrough in the advancement of cryptography occurred in 1518 with the German monk Trithemius's publication of his six volume "Polygraphia". He developed a system for repeating a key every 26 letters, so in essence, his encryption system consisted of 26 different, albeit basic, cipher alphabets. 
 In 1553, Giovan Batista Belaso extended Trithemius' technique by restarting the keyword after every individual letter in the original message. This varied the size of the text between the length of each text, so without prior knowledge of the beginning text, decryption by the means available at the time became virtually impossible.
 The event that thrust cryptography into the modern age, however, and caused governments and armies around the world to take notice occurred in 1628, with the Frenchman Antoine Rossignol's defeat of a Huguenot army by decoding a captured message that detailed their coming deployment plans. Soon after his victory, the French government began asking him to solve numerous ciphers, and other nations and city-states began forming dedicated organizations to break ciphers and protect information.[5]
 In the opening days of World War I, the British navy severed every German and Austrian telecommunications line leading through the Atlantic, thus forcing the Germans to send all messages destined for the states, including diplomatic communications, through American-owned cables.[5] This caused significant problems for the German high command later in the war, because without their own dedicated cables, their messages were subjected to American interception and cryptanalysis.
In January, 1917, two cryptanalysis working for Room 40, the American equivalent of the European "black chambers," successfully deciphered the majority of a telegram from the German State Secretary of Foreign Affairs, Arthur Zimmermann, asking the Mexican president to intervene in the war on the German side, as well as request the same from the Japanese military.[9] The decipherment of this message convinced the United States to enter into the war against the Germans, dramatically shifting the odds against Germany. According to David Kahn, the foremost American historian of classical and early modern cryptography,[10][11]
 Coinciding with the final days of World War I, United States Army Major Joseph Mauborgne, the current head of Room 40 and all cryptographic research for the United States, suggested the possibility of encrypting a message using a truly random key. By printing two identical pads with a random key, then using that key to encrypt one message and one message alone, this cipher obliterated the past problems with poly-alphabetic ciphers:  the repetition of the key. Assuming that each random key, and therefore each set of pads, were only used one time, this encryption system formed the first and to this day only known cryptographic algorithm, or cryptosystem, that provides perfect secrecy.[4][5][7]
 After seeing the unbridled success of the cryptographic sciences in the First World War, more and more governments began investing considerable effort in the study, both to decipher information intercepted from foreign nations and to make their own messages more secure against these tactics.
 A significant example of the power of decipherment and the benefits derived from research into cryptography came on April 13, 1943, during the height of America's war against Japan. The visiting commander in chief of Japanese naval forces in the Pacific, Admiral Yamamoto, forwarded his itinerary to the Japanese naval fleet. When an American listening station in Hawaii intercepted the message and decoded it, the United States seized the opportunity, and less than a week later, downed Yamamoto's plane as it prepared to leave a Japanese runway. Through a direct application of cryptography, the American Navy had killed one of the most powerful and beloved figures in the Japanese military, thus striking a critical blow to the morale of the Japanese.[5]
 The Japanese continued to use a similar cryptographic system, however, still blissfully unaware of the fact that the American researchers had long since broken it completely. Because of this overwhelming American advantage in both knowledge and warning of attacks, the United States was able to fend off a massive Japanese assault near the Midway Islands, now infamous as the site of the Battle of Midway.
 In the European theater of World War II, a British-run group of cryptanalysts, consisting mostly of Polish mathematicians that had fled their home country before the outbreak of the war, enjoyed great success in 1942 when they first broke the codes of the German Enigma machines.[5] Although the decoded information often revealed crucial parts of the German war strategy to the Allies, the paranoia and overly suspicious nature of the Nazi commanders led them to practice extreme security with their codes and ciphers alike.[7]
 Today, the science of cryptography is divided between public key and private key cryptosystems. Although mathematically, these are two distinct systems of encryption, they are often combined into a system referred to as a hybrid key cryptosystem, where different parts of a code rely on either public or private keys. The most commonly used hybrid key cryptosystem today is used throughout the Internet, and is a combination of the public key RSA system and the private key AES system.
 
 Contrast with:
 
 1 General Information 2 History of Cryptography
2.1 Early civilizations
2.1.1 Mesopotamia and Sumer
2.1.2 Ancient Egypt
2.1.3 Ancient Greece
2.1.4 India
2.1.5 China
2.1.6 Rome
2.2 Medieval and Renaissance Cryptography
2.2.1 Italy
2.2.2 German Contribution
2.2.3 Later Advancements
2.3 World War I
2.3.1 Zimmermann Telegram
2.3.2 The One Time Pad and Perfect Cryptography
2.4 World War II
2.4.1 Pacific Theater
2.4.2 European Theater
 2.1 Early civilizations
2.1.1 Mesopotamia and Sumer
2.1.2 Ancient Egypt
2.1.3 Ancient Greece
2.1.4 India
2.1.5 China
2.1.6 Rome
 2.1.1 Mesopotamia and Sumer 2.1.2 Ancient Egypt 2.1.3 Ancient Greece 2.1.4 India 2.1.5 China 2.1.6 Rome 2.2 Medieval and Renaissance Cryptography
2.2.1 Italy
2.2.2 German Contribution
2.2.3 Later Advancements
 2.2.1 Italy 2.2.2 German Contribution 2.2.3 Later Advancements 2.3 World War I
2.3.1 Zimmermann Telegram
2.3.2 The One Time Pad and Perfect Cryptography
 2.3.1 Zimmermann Telegram 2.3.2 The One Time Pad and Perfect Cryptography 2.4 World War II
2.4.1 Pacific Theater
2.4.2 European Theater
 2.4.1 Pacific Theater 2.4.2 European Theater 3 Modern Cryptography 4 See also 5 References 6 External links  List of military strategies and concepts  Encryption: Cryptography-Cryptanalysis-Cryptology-Data encryption-Public-key encryption  Unalienable rights of the Bill of Rights: First Amendment, Fourth Amendment, Fifth Amendment  Right to Privacy  Conservative values and Libertarian American values of Limited government and liberty  Mass surveillance of Big government ObamaCare Welfare state leads to Nanny state, leads to Police state: Globalist-Statist-Socialist-Communist  Liberal totalitarianism ↑ cryptography from Merriam-Webster
 ↑ "cryptography" from Online Entymology Dictionary
 ↑ "graphy" from Online Entymology Dictionary
 ↑ 4.0 4.1 4.2 Simon Singh, The Code Book
 ↑ 5.0 5.1 5.2 5.3 5.4 5.5 A Brief History of Cryptography
 ↑ The Landmark Herodotus: The Histories, from Google Books
 ↑ 7.0 7.1 7.2 7.3 7.4 7.5 A Short History of Cryptography
 ↑ Ancient Chinese Language Supports Creation
 ↑ 9.0 9.1 Teaching with Documents: The Zimmermann Telegram
 ↑ Biography of David Kahn
 ↑ David Kahn: Historian of Secret Codes
 ↑ What is HTTPS?
  Kryptos Cryptography First Amendment Fourth Amendment Military Strategies and Concepts Privacy Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 9 April 2019, at 01:00. This page has been accessed 16,212 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 "No other single cryptanalysis has had such enormous consequences. ... Never before or since has so much turned upon the solution of a secret message".[9] Cryptography Etymology: hybrid key cryptosystem Contrast with: Main article: Zimmermann Telegram Main article: Zimmermann Telegram 
C
 
A
 
E
 
S
 
A
 
R
 
E
 
C
 
G
 
S
 
C
 
T
 Cryptography Contents General Information History of Cryptography Modern Cryptography See also References External links Navigation menu Early civilizations Medieval and Renaissance Cryptography World War I World War II Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console Mesopotamia and Sumer Ancient Egypt Ancient Greece India China Rome Italy German Contribution Later Advancements Zimmermann Telegram The One Time Pad and Perfect Cryptography Pacific Theater European Theater 
