Children:
Evelyn Neill Marriott 
George M. Foote, Jr.
Edward S. Foote
W. Ross Foote (retired state district judge)
A. Lee Foote
R. Hale Foote
Ray A. Foote
Parents:
Henry Dade Foote, Sr.
Lois Jeannette Ray Foote
 Judge Elizabeth Erny Foote (daughter-in-law)
 Washington and Lee University
Tulane University Law School
 Colonel in the United States Marine Corps in World War II
 George Messenger Foote, Sr. (November 4, 1919 – June 21, 2010), was a city judge for thirty years and a civic figure in his native Alexandria, Louisiana.
 Foote was one of two sons and two daughters of Henry Dade Foote, Sr. (1882-1941), a timber businessman, and the former Lois Jeannette Ray (1886-1974). The Foots resided in Hattiesburg, Mississippi, but moved to Alexandria prior to 1919. Lois Foote was a native of Americus in Sumter County in southwestern Georgia.[2] George Foote's brother, Henry, Jr. (1912-1955), was born in Hattiesburg and died in Alexandria; a sister, Ray Foote Schlaben (1914-2006), was born in Hattiesburg and lived after her marriage in Edinburg in Hidalgo County in south Texas, where she is interred.[3][4]
 The obituary of his friend, former Alexandria Mayor W. George Bowdon, Jr. indicateds that Foote and Bowdon first met c. 1935, by which time Foote was a student at Bolton High School, from which he graduated in 1936, three years before Bowdon. Foote worked as a lifeguard during summers in the middle 1930s at Magnolia Park in southern Grant Parish north of Alexandria. According to copy desk editor Wallace Anthony (1936-2010) of The Alexandria Daily Town Talk, the chilly waters of Hudson Creek at Magnolia Park were dammed to form a large swimming pool. A bathhouse, snack bar, and as many as thirty summer houses were subsequently added. The park had shade from pine and beech trees. The pool had concrete walls. A wooden-gated dam included a large wooden water wheel placed as it developed for aesthetic reasons.[5]
 Judge Foote graduated from Washington and Lee University in Lexington, Virginia.[6] During World War II, Foote served in the United States Marine Corps, with two and a half years in the Pacific Theater of Operations He attained the rank of colonel.[7] While still a captain, he was awarded the Silver Star for "conspicuous gallantry and intrepidity" on July 21–22, 1944, while as part of the Third Amphibian Tractor Battalion, he engaged in action against Japanese forces during the assault on enemy-held Guam in the Marianas Islands.[8]
 Foote received his legal training after the war at Tulane University Law School  in New Orleans and was admitted to the practice of law in 1947. For three years he was a Rapides Parish assistant district attorney.[6] He became the full-time city judge in 1955, a post he held until 1985.[7] Judge Foote was particularly known for his commitment to juvenile justice and his role in the founding of the Renaissance Home for Youth, a juvenile offender facility located west of Alexandria.[7] Joining Foote in 1972 in co-founding the Renaissance Home were Guy Humphries, a Louisiana 9th Judicial District Court judge who died three months before Judge Foote, and Dr. Glenn Bryant, the former pastor of the large Emmanuel Baptist Church, a Southern Baptist congregation in downtown Alexandria.[9]
 Foote was a long-term member of the First United Methodist Church of Alexandria, at which he was the chairman of the building committee that constructed the sanctuary on Jackson Street, and a Sunday school teacher. He was affiliated with Rotary International and the Boy Scouts.[7]
 Foote was married for sixty-five years until his death to the former Antonia "Toni" Voelker (1924-2017),[10] whose family in 1936 became the John Deere dealer in Alexandria. The Footes had seven children: Evelyn Neill Marriott (David); George M. Foote (Jane); Edward S. Foote; William Ross Foote (Elizabeth); A. Lee Foote (Naomi); R. Hale Foote (Beth); and Ray A. Foote (Diana). Foote's surviving sister was Jane Ann Foote Culpepper (1926-2017), whose third husband was Judge William A. Culpepper[11] of Alexandria, who served one four-year term on the state 9th Judicial District Court and twenty-two years on the Louisiana Circuit Court of Appeal for the Third Circuit, part of the time as chief judge. He also instituted the United Way in Alexandria and in 1973 chaired the Alexandria Charter Commission.[12]
 Judge Foote also had twenty-eight surviving grandchildren and twenty-two great-grandchildren.[7] The Footes' oldest child and only daughter, Neill, and her husband, David Cannon Marriott, are members of The Church of Jesus Christ of Latter-day Saints  in Salt Lake City, Utah. They have ten children. An eleventh child, Judge Foote's namesake granddaughter, Georgia Marriott (1980-2002), was struck by a truck and killed while she was riding her bicycle near the Indiana University campus in Bloomington, at which she was studying violin.[13]
 Foote's son, W. Ross Foote, served for thirteen years prior to 2004 on the Louisiana 9th Judicial District Court and then returned to his Alexandria law firm, Smith Foote. Since 2010, Ross Foote's wife, George Foote's daughter-in-law, Elizabeth Erny Foote, a native of Lafayette, has been a judge of the United States District Court for the Western District of Louisiana, an appointee of U.S. President Barack H. Obama, sponsored by then U.S. Senator Mary Landrieu. Prior to her appointment to the federal judiciary, Elizabeth Foote was engaged in the full-time practice of law as a partner at Smith Foote. From 1978 to 1979, she was a law clerk for Judge William Culpepper.[14]
 Judge Foote was a close friend and Bolton classmate of Howard B. Gist, Jr., the Alexandria city attorney during three municipal administrations prior to 1973. Like Foote, Gist attended Washington and Lee and the Tulane Law School. Both were veterans of the War in the Pacific but in different branches of the military. Both were avid fishermen and hunters in the community.[15][16]
 Alexandria businessman Edwin Caplan said upon the news of Judge Foote's death at the age of ninety: "He made a difference in everything he did and every life he touched. [If one's] purpose in life was to set a good example ... George Foote certainly excels at that."[17]
 1 Background 2 Legal career 3 Personal life 4 References ↑ In Memoriam: Alexandria City Court Judge Edward E. Roberts, Jr. (died 2001). lasc.org. Retrieved on June 6, 2014.
 ↑ Lois Jeannette Ray. records.ancestryl.com. Retrieved on June 6, 2014.
 ↑ Ray Foote Schlaben. findagrave.com. Retrieved on June 6, 2014.
 ↑ Henry Dade Foote. records.ancestry.com. Retrieved on June 6, 2014.
 ↑ Magnolia Park brings back memories. Alexandria Daily Town Talk (August 16, 2003). Retrieved on February 7, 2018.
 ↑ 6.0 6.1 In Memoriam: Retired Alexandria City Court Judge George M. Foote. lasc.org. Retrieved on June 6, 2014.
 ↑ 7.0 7.1 7.2 7.3 7.4 George Messenger Foote (1919-2010) obituary, The Alexandria Daily Town Talk, accessdate=June 6, 2014
 ↑ George M. Foote: Awards and Citations, Silver Star. Projects.militarytimes.com.
 ↑ Richard P. Sharkey, "Retired Judge Humphries, Co-founder of Renaissance Home, dies in Alexandria", The Alexandria Town Talk, March 23, 2010.
 ↑ Antonia Foote Obituary. The Alexandria Town Talk (November 22, 2017). Retrieved on February 7, 2018.
 ↑ Judge William A. Culpepper's first wife was Thelma Gilham Polk Culpepper (1921-2000).
 ↑ 9th JDC Holds 2002 Opening of Court Ceremony. lasc.org. Retrieved on June 6, 2014.
 ↑ Rachel Sterzer. "New auxiliary leader: 'Stand forth' and share testimony of faith: New leader is fortified by her knowledge of the gospel", July 6, 2013. ldschurcnnewsarchive.com. Retrieved on June 6, 2014.
 ↑ LSU Law Center Honors 2012 Distinguished Alumnus and Distinguished Achievement Honorees at Awards Brunch. law.lsu.edu. Retrieved on June 6, 2014.
 ↑ Howard Battle Gist, Jr.. The Alexandria Town Talk (August 21, 2011). Retrieved on October 13, 2014.
 ↑ It is unclear if Judge Foote is related to the southern author Shelby Foote (1916-2005), a native of Greenville, Mississippi, who also carried the middle name "Dade"; so did Shelby's father and Judge Foote's father. There was also a "George M. Foote" (1873-1935) who served prior to 1920 as the mayor of Gulfport, Mississippi.
 ↑ Judge Foote Passed Away. KALB-TV in Alexandria (June 22, 2010). Retrieved on February 7, 2018.
 Louisiana People Attorneys Judges Democrats United Methodists World War II United States Marine Corps Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 23 August 2019, at 09:23. This page has been accessed 1,425 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 In office George Messenger Foote, Sr. George Messenger Foote, Sr.
 City Judge, Alexandria, Louisiana
 In office1955–1985
  Edward E. Roberts, Jr.[1]
 
  November 4, 1919Alexandria, Rapides Parish
  June 21, 2010 (aged 90)
  Democrat
  Antonia "Toni" Voelker Foote (married c. 1945-2010, his death)
Children:
Evelyn Neill Marriott 
George M. Foote, Jr.
Edward S. Foote
W. Ross Foote (retired state district judge)
A. Lee Foote
R. Hale Foote
Ray A. Foote
Parents:
Henry Dade Foote, Sr.
Lois Jeannette Ray Foote
  Judge William A. Culpepper (brother-in-law)
Judge Elizabeth Erny Foote (daughter-in-law)
  Alexandria, Louisiana
  Bolton High School
Washington and Lee University
Tulane University Law School
  Judge; Attorney
Colonel in the United States Marine Corps in World War II
  United Methodist
 George M. Foote Contents Background Legal career Personal life References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
