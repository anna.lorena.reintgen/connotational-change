T. C. Brister​
W. George Bowdon, Jr.​
Lawrence T. Fuglaar​
 Ben F. Holt​
Robert J. Munson​
Lloyd George Teekell​
 Reared in Sicily Island in Catahoula Parish, Louisiana
Long-time resident of Alexandria
and Lecompte, Louisiana
 Richard "Nippy" Blair​
Robert "Bobby" Blair​
Jane Blair Couvillion (deceased)​
 Louisiana Tech University​
Louisiana State University​
 United States Navy in World War II
 ​
Cecil Ray Blair (April 2, 1916 – July 6, 2001) was a Rapides Parish farmer and businessman who was a Democratic member of both houses of the Louisiana state legislature during the 1950s, 1960s and 1970s. He served in the House from 1952 to 1956. His Senate service came in two segments, 1960 to 1964 and 1966 to 1976.​
​
Blair lived, first, in the Paradise Community north of the Red River and later on Jackson Street Extension in Alexandria. He is most associated, however, with his farm near Lecompte (pronounced LEE COUNT), a community south of Alexandria.​
 Blair was the third of eight children born to a sharecropping family, a son of Alabama native Homer Franklin Blair (1889–1956) and the Mississippi native, the former Hersie Elnora Pearson (1888–1979). He was born in rural Morgantown in Marion County near Columbia in southwestern Mississippi. Blair grew up in Sicily Island, a small community in Catahoula Parish northeast of Alexandria.​ Coincidentally, years later, Blair served in the state Senate alongside another Catahoula Parish native, J. C. "Sonny" Gilbert of Sicily Island. 
 After he graduated in 1934 from Sicily Island High School, 18-year-old Cecil Blair, with virtually no money, went to Ruston to attend Louisiana Tech University. He worked his way through college and in 1938 completed a Bachelor of Science degree in graduate school at Louisiana State University in Baton Rouge to procure a Master of Science in his chosen field of entomology. Susie joined him at LSU to complete her studies.​
 The daughter of the Reverend Albert George and the former Ruth Hoffpauir, Susie George grew up in Methodist parsonages throughout Louisiana. She was born in the village of Bonita in Morehouse Parish northeast of Monroe. Susie wrote two children's books, which focused on life around the Blair farm. The book Easter Pony received international acclaim and was listed as one of the year's best books by the American Library Association in 1963, the year it was published.[1]  She also had unrealized talent for art, which particularly developed in the next generation through her older son and younger daughter.
 The couple moved to Alexandria in 1940 and had four children: Rebecca "Becky" Blair Tisdale (born 1942), Richard "Nippy" Blair (born 1943), Robert Blair (born 1948), all of Alexandria, and Jane Blair Couvillon (1950-1988: a cancer victim). Becky is a retired teacher and the widow of history professor Garry Lee Tisdale, who taught at LSU at Alexandria. He died of brain cancer in 2000 at the age of fifty-six, and LSUA subsequently honored him with the naming of Garry Tisdale Drive on the campus. Robert "Bobby" Blair operates the Blair farm and Blair's former vegetable stand called the "Old Gray Mule," a favorite gathering place for Louisiana politicos off U.S. Highway 71 near the farm. It was at the Old Gray Mule that Blair developed his skills as a raconteur. He dubbed his farm "The Sweet Corn Capital of Louisiana." Nippy Blair is a well-known regional artist and has been the building superintendent of Emmanuel Baptist Church in downtown Alexandria.
 Though he had two small children, Blair enlisted in 1944 in the United States Navy and served in the Pacific Theater of Operations for the remainder of World War II.​
 Blair was affiliated with the anti-Long faction of his state's party, principally because he loathed corruption and favoritism in government.[2]
 As a House member, Blair supported farmers in need of open range lands. During the administration of Governor Robert F. Kennon, Blair authored the bill to fence the highways to keep roaming cattle off the roads. He worked to obtain the relocation to Alexandria of St. Mary's Training School for the handicapped. In the Senate, Blair pushed for the creation of Buhlow Lake adjacent to the Red River in Pineville, where boat races are held. In the House multi-district, Blair served with two colleagues from Rapides Parish, Lloyd George Teekell and H. N. Goff. In effect, Blair replaced W. George Bowdon, Jr., who left the legislature after a single term to run successfully in 1953 for mayor of Alexandria, a post that Bowdon held for sixteen years.​
​
Early in 1955, a dispute broke out at the then 1,150-patient Louisiana Colony and Training School at nearby Kingsville north of Pineville. Willie Kees, a Pineville businessman had resigned as mayor in 1952 to head the school. When Kees was named superintendent in December 1954, a dispute broke out with William P. Hurder, the clinical director who was both a psychologist and a physician. The two had previously shared authority. Kees' promotion touched off charges of "politics" in his selection. Blair defended the Kees selection, but because of the controversy, Kees resigned from the school to return to the private sector.[3][4]​ Kees Park in Pineville is named for the late mayor.
 Blair worked for the establishment of the original two-year LSUA, located near his Lecompte farm in south Rapides Parish. Years later, the school was given four-year status, a breakthrough which came only a few weeks before Blair's death.​
 Blair was known for his constituent services and his efforts to improve old Louisiana Highway 1 between Shreveport and Baton Rouge. He also supported highway beautification and personally planted flowers along U.S. Highway 71 near his farm. He opposed having the office of state superintendent of education be made appointive because he preferred the judgment of more than a million voters, rather than a small group, now the Board of Elementary and Secondary Education, which decides who holds the top post in education.
 Blair first ran for the state Senate in 1956 but was defeated in the Earl Kemp Long landslide by the Longite choice, Crawford Hugh "Sammy" Downs (1911-1985) of Alexandria,  the son of a former Rapides Parish sheriff, U. T. Downs, and the father of the former district attorney, Jam Downs. On December 5, 1959, Blair unseated Downs in the Democratic primary.[5] In 1964, Blair was beaten by George Ray Lee, who died in office midway through his term. Lee was the son of former Senator George W. Lee, who filled the seat from 1936 to 1940 , and also the brother of later Pineville municipal and state court Judge Richard E. "Dick" Lee. In the 1966 special election to replace George Ray Lee, Blair waged a victorious comeback. He won again in 1968 and 1972. From 1969 to 1972, he and Sylvan Friedman of Natchitoches Parish represented a two-member district that included Grant Parish.
​
In his last Senate term, Blair was also elected on a nonpartisan ballot as a delegate to the 1973 Louisiana Constitutional Convention, which drafted the state's current framework of government, as approved by voters in the spring of 1974.
 In the 1971 closed primary, the last held for Louisiana legislative races, Blair was forced into a second primary by Floyd Smith, a Pineville City Councilman and former mayor. The third-placed candidate, Arnold Jack Rosenthal, elected in 1973 as the last of Alexandria's finance and utilities commissioners, backed Smith in the runoff election, but Blair, defeated Smith by some two thousand votes. It was something of a Long v. anti-Long battle in that Smith was a native of Winnfield and his mother was a Long. There was no Republican opposition in the general election held on February 1, 1972; so Blair secured his fourth and final term in the new single-member Senate District 29.
 After a total of eighteen years in both legislative chambers, Blair was unseated in the 1975 nonpartisan blanket primary, the first held in Louisiana, by then state Representative and later Alexandria Mayor Edward Gordon "Ned" Randolph, Jr. (1942-2016) Thereafter, Blair failed in comeback attempts for his former Senate seat in 1979, 1983, 1987, and 1995. While Randolph unseated Blair, in the same election, then Democrat Jock Scott won Randolph's House seat. Randolph and Scott purported to be "new" politicians not molded to failures of the past. It was the same kind of environment which a year later enabled former Georgia Jimmy Carter to win the presidency. Randolph and Scott comanaged Carter's Rapides Parish campaign in 1976.​
 ​
Blair was never reconciled to the election of Randolph, who was a Bolton High School classmate of Blair's two older children.  He tried to dislodge Randolph in the 1979 primary and failed. In 1983, Blair attempted again. In a surprise of sorts, Randolph was unseated, not by Blair, but by Pineville businessman Joe McPherson, later of Woodworth in south Rapides Parish. Randolph led in the initial balloting with 13,501 votes (38.4 percent) to McPherson's 11,032 ballots (31.4 percent). Blair received 6,096 votes (17.4 percent), and Alexandria Mayor John K. Snyder, in fourth place, received 4,496 votes (12.8 percent).[6] In the runoff contest —officially the Louisiana general election— McPherson prevailed, 16,360 votes (53.9 percent) to Randolph's 13,973 (46.1 percent).[7]
 In 1987, Blair attempted to regain the Senate seat. He and outgoing state Representative Jock Scott, then a convert to the Republican Party, challenged McPherson, who carried the support of AFL-CIO state president Victor Bussie. McPherson polled 16,950 (51 percent) in the primary and hence retained the seat outright. Scott trailed with 12,346 votes (37 percent). Blair netted 4,245 votes (13 percent). [8] In 1995, Blair once more attempted to regain the Senate seat but failed to make the general election runoff in a field of seven candidates. The Reverend B. G. Dyess, the retired Rapides Parish voter registrar and a Baptist clergyman who campaigned against gambling, won the state Senate seat. Dyess served for four years and, because of his wife's health, did not seek a second term in 1999. McPherson made an unsuccessful bid for Public Service Commissioner in 1995, having been defeated by Dale Sittig of Eunice. McPherson returned to the Senate in 2000 and easily won a third consecutive term in the primary held on October 20, 2007, the same election which propelled Bobby Jindal to the governorship.​[9]
 Blair put his entomological skills to use with his Blair Laboratories pesticide business, which he operated for many years in Alexandria. Daughter Rebecca Tisdale said that her father was too independent to work for others; so he chose self-employment in farming and pesticides. She said that her father struggled as a youth and was dismissed as "white trash" because of his poverty. Such obstacles made him exceptionally determined to succeed: he was the kind of man who could not easily accept rejection. His life story fits the Horatio Alger mode[2]
 Blair was active in community affairs, having been a former president of the Alexandria-Pineville Chamber of Commerce and Kiwanis International. In the late 1950s, while he was returning from church with his family when the Blairs resided in the Paradise community, he once saved from drowning two young men that he saw floundering in the Red River from an overturned boat.​[2]
​
Blair, who had smoked cigars since he was a teenager, died of heart failure at St. Luke's Episcopal Hospital in Houston. He had undergone open-heart surgery for a valve replacement. While he appeared to have recovered from the surgery itself, his systems shut down thereafter. Politicos from throughout central Louisiana attended the funeral, including legendary attorney Camille F. Gravel, Jr., Rapides Parish Sheriff William Earl Hilton, then Alexandria Mayor Randolph, who had ended Blair's Senate career, and District Attorney James Crawford "Jam" Downs, the son of Blair's old rival, C. H. "Sammy" Downs.[10]
 The Reverend Larry Taylor, pastor of Blair's Emmanuel Baptist Church, said that the first time he met Blair he thought that he had seen "a figure who had stepped out of the pages of southern literature." Taylor lamented that "Even someone with a heart as big as Cecil R. Blair's couldn't keep going forever. Cecil had a big heart that finally gave out."​[10]
 Cecil and Susie Blair are interred at Greenwood Memorial Park in Pineville.​
 ​​​​​​​
 1 Background 2 Legislative highlights 3 Elections of 1971 and 1975 4 Four later campaigns 5 Legacy 6 References ↑ Susie 
Blair. Easter Pony. Ariel Books at Amazon.com. Retrieved on August 23, 2019.
 ↑ 2.0 2.1 2.2 Statement of Rebecca Blair Tisdale of Alexandria.
 ↑ Hurder takes post as colony superintendent. louisdl.louislibraries.org (January 19, 1955). Retrieved on July 24, 2015.
 ↑ State Colony Resignation Brings Rift. Lake Charles American-Press (January 19, 1955). Retrieved on July 24, 2015.
 ↑ The Shreveport Times, December 6, 1959, p. 1.
 ↑ Louisiana Secretary of State, Election Returns, October 22, 1983.
 ↑ Louisiana Secretary of State, Election Returns, November 19, 1983.
 ↑ Louisiana Secretary of State, Election Returns, October 24, 1987.
 ↑ Louisiana Secretary of State, Election Returns, October 21, 1995, and October 20, 2007.
 ↑ 10.0 10.1 Steve Bannister, "Farewell: Cecil Blair Was an Original," Alexandria Town Talk, July 11, 2001,  1.
 Louisiana People Mississippi Business People Farmers State Senators Democrats Southern Baptists World War II United States Navy Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 12 November 2019, at 15:29. This page has been accessed 867 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 In office In office In office Cecil Ray Blair Cecil Ray Blair​
 Louisiana State Senator for Rapides Parish​
 In office1960​ – 1964​
  Crawford Hugh "Sammy" Downs​
  George Ray Lee​
 In office1966​ – 1976​
  George Ray Lee​
  Edward Gordon"Ned" Randolph, Jr.​
 Louisiana State Representative for Rapides Parish​
 In office1952​ – 1956​
  At-large delegation:​
T. C. Brister​
W. George Bowdon, Jr.​
Lawrence T. Fuglaar​
  At-large delegation:​
Ben F. Holt​
Robert J. Munson​
Lloyd George Teekell​
 
  April 2, 1916​Morgantown, Marion County Mississippi, USA​
Reared in Sicily Island in Catahoula Parish, Louisiana
Long-time resident of Alexandria
and Lecompte, Louisiana
  July 6, 2001 (aged 85)Houston, Texas, USA​
  Greenwood Memorial Park in Pineville, Louisiana​
  Democrat​
  Virginia Susan Ruth "Susie" George Blair​
  Rebecca "Becky" Blair Tisdale​
Richard "Nippy" Blair​
Robert "Bobby" Blair​
Jane Blair Couvillion (deceased)​
  Sicily Island High School​
Louisiana Tech University​
Louisiana State University​
  Farmer; Businessma
United States Navy in World War II
  Southern Baptist​
 Cecil R. Blair Contents Background Legislative highlights Elections of 1971 and 1975 Four later campaigns Legacy References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
