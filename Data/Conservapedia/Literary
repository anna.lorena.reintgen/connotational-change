Literature, as an art, consists of writings whose value lies in "the beauty of form or emotional effect",[1]  and encompasses such diverse forms of expression as novels, short stories, plays and poems. Its content is as limitless as the desire of human beings to communicate.   
 The art of literature is not reducible to the words on the page; they are there because of the craft of writing. As an art, literature is the organization of words to give pleasure; through them it elevates and transforms experience; through them it functions in society as a continuing symbolic criticism of values.The Art of Literature - Encyclopædia Britannica 
 Literature is not only a storehouse of creativity. Through its topics of discussion literature is also a take on the history of the world. Dealing as it does with language it is also, a repository of the history of language. Ancient literature like those of Greek, Latin, Hebrew, Celtic, Saxon, Aryan, Dravidan and the like help us learn how these languages have evolved through time and through the ravages of the different cultures conquering them. Literature has also helped preserve for man the different schools of thought, Sophism to Modernism, the Dead Sea scrolls to Structuralism. [3]
 The story of English literature begins with the Germanic tradition of the Anglo-Saxon settlers... Beowulf, an epic poem of the 8th century, stands at its head. [4].
 Geoffrey Chaucer, a poet of the court, wrote the poem The House of Fame in 1380, and his masterpiece Troilus and Criseyde in 1385; he was also the author of Canterbury Tales, which ranks as one of the greatest epic works of world literature. [5]
 Pamela, indeed, was printed as early as 1744 in Philadelphia, by Benjamin Franklin, and in the same year in New York and in Boston. But the only other novels printed in America before the Declaration of Independence seem to have been Robinson Crusoe (1768), Rasselas (1768), The Vicar of Wakefield (1772), Juliet Grenville (1774), and The Works of Laurence Sterne M.A. (1774). [6]
 The history of literature in many other countries, both ancient and modern, has record real treasures of writing pieces.
 Some of the most popular authors are: 
 We call it a grain of sand
 but it calls itself neither grain nor sand.
 It does just fine without a name,
 whether general, particular,
 permanent, passing, 
 incorrect or apt.
 Wislawa Szymborska, from "View with a Grain of Sand". Poetry Samples
 
 From The Raven by Poe.
 ..............................................................
 And the raven, never flitting, still is sitting, still is sitting
 On the pallid bust of Pallas just above my chamber door;
 And his eyes have all the seeming of a demon's that is dreaming,
 And the lamp-light o'er him streaming throws his shadow on the floor;
 And my soul from out that shadow that lies floating on the floor
 Shall be lifted - nevermore!
 
 (EZRA POUND, PISAN CANTOS, LXXXI)
 What thou lovest well remains,
 What thou lov'st well shall not be reft from thee
 What thou lov'st well is thy true heritage
 Whose world, or mine or theirs
 First came the seen, then thus the palpable
 What thou lovest well is thy true heritage
 What thou lov'st well shall not be reft from thee.
 About Anna Karenina.
 Anna Karenina was published in serial form from 1873-1877. It created a great stir in society; reports from the time claim that everyone in Russian Society was discussing the book and waiting eagerly for the next installment to appear. The critical reaction was mostly positive and, like the novel itself, passionate. It was published on the heels of Leo Tolstoy's great opus, War and Peace (1863-1869) and solidified his reputation as one of Russia's most important 19th-century writers. This was quite a feat, given that his contemporaries included Dostoevsky, Turgenev, Gogol and Lermontov....  Although the critical reaction to Anna Karenina was favorable and the public was shaken by the strength of both the story and Tolstoy's prose, Tolstoy himself was dissatisfied with the novel... [7]
 
Uncle Tom’s Cabin. 
 "That Uncle Tom’s Cabin stands higher in the history of reform than in the history of the art of fiction no one needs to say again. Dickens, Kingsley, and Mrs. Gaskell had already set the novel to humanitarian tunes, and Mrs. Stowe did not have to invent a type. She had, however, no particular foreign master, not even Scott, all of whose historical romances she had been reading just before she began Uncle Tom. Instead she adhered to the native tradition, which went back to the eighteenth century, of sentimental, pious, instructive narratives written by women chiefly for women. Leave out the merely domestic elements of the book—slave families broken up by sale, ailing and dying children, negro women at the mercy of their masters, white households which at the best are slovenly and extravagant by reason of irresponsible servants—and little remains. To understand why the story touched the world so deeply it is necessary to understand how tense the struggle over slavery had grown, how thickly charged was the moral atmosphere awaiting a fatal spark. But the mere fact of an audience already prepared will not explain the mystery of a work which shook a powerful institution and which, for all its defects of taste and style and construction, still has amazing power." [8]
 Book I 	   
 Sing, O goddess, the anger of Achilles son of Peleus, that brought countless ills upon the Achaeans. Many a brave soul did it send hurrying down to Hades, and many a hero did it yield a prey to dogs and vultures, for so were the counsels of Jove fulfilled from the day on which the son of Atreus, king of men, and great Achilles, first fell out with one another.
 And which of the gods was it that set them on to quarrel? It was the son of Jove and Leto; for he was angry with the king and sent a pestilence upon the host to plague the people, because the son of Atreus had dishonoured Chryses his priest. Now Chryses had come to the ships of the Achaeans to free his daughter, and had brought with him a great ransom: moreover he bore in his hand the sceptre of Apollo wreathed with a suppliant's wreath and he besought the Achaeans, but most of all the two sons of Atreus, who were their chiefs.
 "Sons of Atreus," he cried, "and all other Achaeans, may the gods who dwell in Olympus grant you to sack the city of Priam, and to reach your homes in safety; but free my daughter, and accept a ransom for her, in reverence to Apollo, son of Jove."
 On this the rest of the Achaeans with one voice were for respecting the priest and taking the ransom that he offered; but not so Agamemnon, who spoke fiercely to him and sent him roughly away. "Old man," said he, "let me not find you tarrying about our ships, nor yet coming hereafter. Your sceptre of the god and your wreath shall profit you nothing. I will not free her. She shall grow old in my house at Argos far from her own home, busying herself with her loom and visiting my couch; so go, and do not provoke me or it shall be the worse for you."
 Homer. [9]
 Some are thoughtful on their way
 Some are doubtful, so they pray.
 I hear the hidden voice that may
 Shout, "Both  paths lead astray." Rubaiyat of Omar Khayyam.
 "Love is an endless mystery, for it has nothing else to explain it."  
 Rabindranath Tagore. [10]
 1 Origins 2 Popular authors 3 Poetry samples 4 Novel criticism 5 The Iliad (fragment) 6 See also 7 External links 8 References  Charles Dickens  Mark Twain   William Shakespeare   Arthur Conan Doyle   Jane Austen   Jules Verne   L. Frank Baum (Lyman Frank - The Wizard of Oz)    Lewis Carroll   Edgar Allan Poe   Oscar Wilde   Johann Wolfgang von Goethe   H. G. Wells (Herbert George)    Homer   Leo Tolstoy  J. R. R. Tolkien  Antoine de Saint-Exupéry  Paulo Coelho Literary criticism Author With a list of "Best books and authors". Poetry Spanish Golden Age Liberal Arts college World History Lecture One Theater Famous American writers The Internet Classics Archive by Daniel C. Stevenson. Project Gutenberg An Online Library of Literature Literature Open Directory.  Top 50 Poems GLOSSARY OF POETIC TERMS HISTORY OF LITERATURE Literature of the Western World: Greek & Roman HISTORY OF LITERATURE IN ENGLISH The Cambridge History of English and American Literature An Encyclopedia in Eighteen Volumes.  A Brief History of Spanish-language Literature CHINESE LITERATURE History of Japan's Literature History of Indian Literature Literature in Ancient Egypt LITERATURE, HEBREW Arabic literature ↑ The Concise Oxford Dictionary
 ↑ Diamond, Jared. Guns, Germs, and Steel. New York: Norton, 2005.
 ↑  The importance of the history of Literature
 ↑  Alliterative verse: 8th - 14th century AD 
 ↑  Geoffrey Chaucer 
 ↑  The Novel in the Colonies. 
 ↑ About Anna Karenina  Literature ClassicNotes™ 
 ↑ Uncle Tom’s Cabin. 
 ↑ The Iliad. Translated by Samuel Butler. 
 ↑  Rabindranath Tagore 
 Literature Art Featured articles Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 31 May 2012, at 12:39. This page has been accessed 20,530 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 
The art of literature is not reducible to the words on the page; they are there because of the craft of writing. As an art, literature is the organization of words to give pleasure; through them it elevates and transforms experience; through them it functions in society as a continuing symbolic criticism of values.The Art of Literature - Encyclopædia Britannica 
 
Literature is not only a storehouse of creativity. Through its topics of discussion literature is also a take on the history of the world. Dealing as it does with language it is also, a repository of the history of language. Ancient literature like those of Greek, Latin, Hebrew, Celtic, Saxon, Aryan, Dravidan and the like help us learn how these languages have evolved through time and through the ravages of the different cultures conquering them. Literature has also helped preserve for man the different schools of thought, Sophism to Modernism, the Dead Sea scrolls to Structuralism. [3]
 
Pamela, indeed, was printed as early as 1744 in Philadelphia, by Benjamin Franklin, and in the same year in New York and in Boston. But the only other novels printed in America before the Declaration of Independence seem to have been Robinson Crusoe (1768), Rasselas (1768), The Vicar of Wakefield (1772), Juliet Grenville (1774), and The Works of Laurence Sterne M.A. (1774). [6]
 Literature the rest is dross the rest is dross the rest is dross or is it of none? or is it of none? or is it of none? Elysium, though it were in the halls of hell, Elysium, though it were in the halls of hell, Elysium, though it were in the halls of hell, the rest is dross the rest is dross the rest is dross or is it of none? or is it of none? or is it of none? Elysium, though it were in the halls of hell, Elysium, though it were in the halls of hell, Elysium, though it were in the halls of hell, Literature Contents Origins Popular authors Poetry samples Novel criticism The Iliad (fragment) See also External links References Navigation menu Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
