William Cranch Bond and George Phillips Bond first noticed Hyperion as a faint object in Saturn's ring plane, between the moons Titan and Iapetus. In the course of several days, the Bonds saw this object again, and it was clearly keeping station with Saturn at the time of Saturn's apparent retrograde motion. They determined its sidereal month with remarkable accuracy and reported their findings in the Monthly Notices.[1]
 Independently of them, William Lassell first observed Hyperion on September 18, 1848. He was searching for Iapetus and found two objects, Iapetus and another, much fainter object. The next day, the faint object had appeared to approach Saturn, while the other object had not. Lassell was satisfied that the brighter object was Iapetus and determined that he had discovered a new object.[9]
 Sir John Herschel had earlier suggested a naming convention for the seven satellites of Saturn already discovered. Lassell and the Bonds independently selected Hyperion as the name of the new object, in accordance with that convention.
 Hyperion's rotation is chaotic and nearly unpredictable.[11] Astronomers cite a number of factors, including the 4:3 orbital resonance with Titan, to account for this.[8][12][13]
 Hyperion has an irregular shape, with dimensions 185 x 140 x 113 km.[5] In fact it is the largest irregularly shaped body in the solar system. Proteus, moon of Neptune, is often cited as a larger irregular body, but in fact Proteus is not irregular and is almost spherical.[12] Hyperion is also highly porous, with a density only 57% of that of water.[5]
 Hyperion is one of the least reflective of Saturn's moons, with an albedo of 30%. Many astronomers believe that Hyperion is covered with dark material that comes from Phoebe, an outer moon.[6] Recent data from the Cassini probe indicates that this material consists of hydrocarbons.[15]
 The favorite theory of the origin of Hyperion is that it is a fragment of a larger body that suffered a collision.[12][13] At least one astronomer speculated that debris from this collision contributed to the dark material that covers the Cassini Region of Iapetus.[16]
 Hyperion was under observation for centuries, but only with modern rocket probes has detailed examination been possible. The first such probe to make rendezvous with Hyperion was Voyager 2, on August 24, 1981. The best examination of Hyperion to date was by the Cassini spacecraft, which made rendezvous with Hyperion on September 26, 2005 and took the remarkable images of its spongiform surface.
 1 Discovery and naming 2 Orbital characteristics 3 Rotational characteristics 4 Physical characteristics
4.1 Surface
 4.1 Surface 5 Observation and Exploration 6 References ↑ 1.0 1.1 1.2 Bond, W. C. "Discovery of a new Satellite of Saturn." Monthly Notices of the Royal Astronomical Society, 9:1, November 10, 1848. Accessed June 9, 2008.
 ↑ "Gazetteer of Planetary Nomenclature: Planetary Body Names and Discoverers." US Geological Survey, Jennifer Blue, ed. March 31, 2008. Accessed May 9, 2008.
 ↑ Lassell, William. "Satellites of Saturn." Monthly Notices of the Royal Astronomical Society, 8:42-43, 1848. Accessed June 9, 2008.
 ↑ 4.0 4.1 Calculated
 ↑ 5.0 5.1 5.2 5.3 5.4 5.5 5.6 5.7 5.8 Williams, David R. "Saturnian Satellite Fact Sheet." National Space Science Data Center, NASA, November 23, 2007. Accessed June 4, 2008.
 ↑ 6.0 6.1 6.2 6.3 Hamilton, Calvin J. "Entry for Hyperion." Views of the Solar System, 2001. Accessed June 9, 2008.
 ↑ "Planetary Satellite Physical Parameters." JPL, NASA. Retrieved June 9, 2008.
 ↑ 8.0 8.1 8.2 8.3 8.4 "Hyperion." Encyclopædia Britannica. 2008. Encyclopædia Britannica Online. 9 June 2008.
 ↑ Lassell, William. "Discovery of a new Satellite of Saturn." Monthly Notices of the Royal Astronomical Society, 8(9):195, November 1848. Accessed June 9, 2008.
 ↑ "Hyperion, Chaotic Moon." The Planetary Society. Accessed June 9, 2008.
 ↑ J. Wisdom, S.J. Peale, F. Mignard. "The chaotic rotation of Hyperion". (IAU, COSPAR, NASA, et al., Colloquium on Natural Satellites, 77th, Cornell University, Ithaca, NY, July 5–9, 1983) Icarus (ISSN 0019-1035) 58: 137-152. doi:0.1016/0019-1035(84)90032-0 Accessed June 9, 2008.
 ↑ 12.0 12.1 12.2 Arnett, Bill. "Entry for Hyperion." The Nine 8 Planets, July 12, 2007. Accessed June 9, 2008.
 ↑ 13.0 13.1 13.2 Piazza, Eric. "Cassini-Huygens: Moons: Hyperion." November 3, 2006. Accessed June 9, 2008.
 ↑ Thomas, P. C.; et al. (2007). "Hyperion's sponge-like appearance". Nature 448: 50-56. doi:10.1038/nature05779. Accessed June 9, 2008.
 ↑ NASA/Ames Research Center. "Hydrocarbons, Necessary For Life, Found On Saturn's Moon Hyperion." ScienceDaily 6 July 2007. Accessed 9 June 2008.
 ↑ Matthews, Robert A. J. "The Darkening of Iapetus and the Origin of Hyperion." Quarterly Journal of the Royal Astronomical Society, 33:253–258, September 1992. Accessed June 9, 2008.
 Moons Astronomy Create account Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 26 September 2018, at 11:33. This page has been accessed 8,613 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Hyperion (moon) Date of discovery Name of discoverer Name origin Primary Order from primary Perikrone Apokrone Semi-major axis Orbital eccentricity Sidereal month Avg. orbital speed Inclination Mass Density Mean radius Escape speed Composition Color Albedo Hyperion Saturn VII False-color image of Hyperion's sponge-like surface, taken by spacecraft Cassini
  Date of discovery
  September 16, 1848[1]
  Name of discoverer
  William Cranch Bond[2]
  Name origin
  Member of the Titans, per an earlier suggestion by Sir John Herschel as reported by William Lassell[1][3]
  Primary
 Saturn
  Order from primary
  21
  Perikrone
 1,326,769 km[4]
  Apokrone
 1,635,431 km[4]
  Semi-major axis
 1,481,100 km[5]
  Orbital eccentricity
 0.1042[5]
  Sidereal month
 21.276609 da[5]
  Avg. orbital speed
 5.07 km/s[6]
  Inclination
 0.43°[5] to Saturn's equator
  Mass
  5.5 * 1018 kg[5]
  Density
  570 kg/m³[5]
  Mean radius
  133 km[7]
  Escape speed
  0.107 km/s[6]
  Composition
  Water ice and little rock[8]
  Color
  reddish rose tan
  Albedo
  0.3[5][6][8]
 v • d • e
The Solar SystemStarSolTerrestrial PlanetsMercury · Venus · Earth · MarsGas GiantsJupiter · Saturn · Uranus · NeptuneDwarf PlanetsCeres · Haumea · Makemake · Pluto · ErisAsteroid BeltMajor asteroids · C-type asteroids · S-type asteroids · M-type asteroidsTrans-Neptunian ObjectsKuiper belt · Scattered disk · Oort Cloud · NemesisSatellitesMoon · Phobos · Deimos · Io · Europa · Ganymede · Callisto · Mimas · Enceladus · Tethys ·  Dione · Rhea ·  Titan ·  Hyperion ·  Iapetus ·  Miranda · Ariel · Umbriel · Titania · Oberon · Triton ·  Nereid · Charon · Nix · Hydra · DysnomiaCategories  Star Sol   Terrestrial Planets Mercury · Venus · Earth · Mars  Gas Giants Jupiter · Saturn · Uranus · Neptune  Dwarf Planets Ceres · Haumea · Makemake · Pluto · Eris  Asteroid Belt Major asteroids · C-type asteroids · S-type asteroids · M-type asteroids  Trans-Neptunian Objects Kuiper belt · Scattered disk · Oort Cloud · Nemesis  Satellites Moon · Phobos · Deimos · Io · Europa · Ganymede · Callisto · Mimas · Enceladus · Tethys ·  Dione · Rhea ·  Titan ·  Hyperion ·  Iapetus ·  Miranda · Ariel · Umbriel · Titania · Oberon · Triton ·  Nereid · Charon · Nix · Hydra · Dysnomia  Categories Hyperion (moon) Contents Discovery and naming Orbital characteristics Rotational characteristics Physical characteristics Observation and Exploration References Navigation menu Surface Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console 
