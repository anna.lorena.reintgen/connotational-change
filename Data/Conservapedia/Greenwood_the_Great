This article is about fictional locations, landscapes, mountains, rivers etc. from J. R. R. Tolkien's Middle-earth. 
 Beleriand was a large region in northwestern Middle-earth until the end of the First Age. Beleriand lay between the sea in the west and south, and the Blue Mountains in the east. 
At the end of the First Age, Beleriand was destroyed and covered by the sea.
 Eriador is a large region in Middle-earth. Is lies between the Blue Mountains in the west, the Misty Mountains in the east, the Ice-bay of Forochel in the north, and the rivers Glanduin and Greyflood in the south.
 In the earlier part of the Third Age much of Eriador was part of the kingdom of Arnor. Apart from Arnor, other settlements in Eriador were Rivendell, Breeland and the Shire.
 Rhovanion or Wilderland is a large region in Middle-earth. It lies east of the Misty Mountains and south of the Grey Mountains and the Iron Hills. The river Anduin flows through it from the Grey Mountains to the Emyn Muil. A large part of Rhovanion is covered by the forest of Mirkwood.
 Rhûn is a large region in Middle-earth. It is the lands east of Rhovanion and around the inland Sea of Rhûn.
 Harad or Haradwaith is the name for the lands south of Gondor and Mordor. The haven Umbar was established by the Númenóreans, and was later a central settlement of the Black Númenóreans. The peoples living in Harad were often under the influence of Sauron, and made war with the western realms. In the Fourth Age the Reunited Kingdom of Arnor and Gondor made peace with Harad. In Harad also lived the elephant-like mûmakil (or oliphaunts).
 The Emyn Beraid or Tower Hills are a series of hills at the western end of Eriador. Upon them stood three towers, and the tallest, Elostirion, housed a palantír.
 The Ered Luin (Sindarin: "Blue Mountains"), also known as Ered Lindon, are a mountain range in the far west of Eriador. Until the end of the First Age, the Ered Luin separated Beleriand and Eriador. In the middle of the mountain range is a gap, creating the Gulf of Lhûn/Lune, where the Grey Havens lie.
 The Ered Mithrin (Sindarin: "Grey Mountains") are a mountain range north of Rhovanion. At its western end the Ered Mithrin meet the northern end of the Misty Mountains, the location of Mount Gundabad. East of the Ered Mithrin lie the Iron Hills.
 The Ered Nimrais (Sindarin: "Whitehorn Mountains") or White Mountains are a mountain range. It lies between Gondor in the south and Rohan in the north. In its western part the White Mountains are separated from the Misty Mountains by the Gap of Rohan, near which lies the valley of Helm's Deep. Through the Mountains ran the Paths of the Dead. The easternmost end of the White Mountains is the mountain Mindolluin, on which lies the city of Minas Tirith.
 The Misty Mountains, or Sindarin Hithaeglir, are a great mountain range running from north to south, forming the barrier between Eriador in the west and Rhovanion in the east. At the southern end of the Misty Mountains lies Isengard, at the northern end Mount Gundabad. Under the Misty Mountains lies the ancient Dwarven city Khazad-dûm.
 Mount Doom (Sindarin: Amon Amarth) or Orodruin (Sindarin: "fiery mountain") is a volcanic mountain in northeastern Mordor. Inside the volcano are the Sammath Naur (or Cracks of Doom) where Sauron forged the One Ring, and which is the only place it could be destroyed.
 Weathertop or Amon Sûl (Sindarin: "Hill of Wind") is a hill in Eriador, the southernmost and highest summit of the Weather Hills. The Weather Hills lie next to the Great East Road, about halfway between the Shire and Rivendell. 
 On Weathertop was the Tower of Amon Sûl, a watch-tower built by the Kingdom of Arnor, where one of the palantíri was kept. In T.A. 1409 the Tower was destroyed, and only ruins were left. In October T.A. 3018, Aragorn and the hobbits camped on Weathertop and were attacked by the Ringwraiths; Frodo Baggins was wounded by a Morgul-blade.
 The Anduin (also Langflood) is the longest river in Middle-earth. Its sources are in the Grey Mountains, and it flows through Rhovanion between the Misty Mountains and the forest Mirkwood. It runs though the Emyn Muil, where it goes through the rapids Sarn Gebir, flows by the Argonath (also known as the Pillars of the Kings, carved into the likeness of Isildur and Anárion) on either side of the river, then into the lake Nen Hithoel and over the Falls of Rauros. After that it flows between the White Mountains and the Mountains of Shadow, before reaching the Sea in a broad river delta Ethir Anduin.
 The Bruinen or Loudwater is a river in Eriador. It began in the Misty Mountains, and flowed south, through the valley of Rivendell, and into the river Mitheithel.
 The Baranduin or Brandywine River is a river in Eriador. Its source is Lake Evendim in northern Eriador, after which it flows south. It flows through the Shire, and at last into the Sea.
 Angmar was a realm in northern Middle-earth in the Third Age. It was founded in T.A. 1300 by the Lord of the Nazgûl, the Witch-king of Angmar. The capital of Angmar was Carn Dûm. Angmar waged war against Arnor, and later its successors Arthedain, Cardolan and Rhudaur. In T.A. 1975, Angmar was defeated at the Battle of Fornost by forces from Gondor, the northern Dúnedain and the Elves of Lindon and Rivendell.
 Arnor was a human realm in Eriador. It was founded by Elendil and his people, who had survived the Fall of Númenor towards the end of the Second Age. The peoples living in Arnor were the descendants of the Númenórean survivors, and the indigenous human peoples who had lived there before. The capital of Arnor was the city Annúminas on the shores of Lake Evendim in northern Eriador. Elendil became the first King of Arnor; after his death, he was followed by his older son Isildur.
 After the death of the tenth King, Eärendur, in T.A. 861, his three sons split Arnor among themselves, creating three new lands: Arthedain and its capital Fornost Erain in the west, Cardolan in the south, and Rhudaur in the north. With time, all three countries were destroyed in wars against the northern land Angmar. 
After the War of the Ring, the land again became part of the Reunited Kingdom of Arnor and Gondor, ruled by King Elessar Telcontar and his descendants.[1]
 Breeland is a small region in Eriador, which lies around the Breehill. It has four villages: Bree (the largest), Archet, Combe and Staddle. The peoples living in the villages of Breeland are Men and Hobbits; it is the only place where both people live together. The only other place inhabited by Hobbits is the Shire.
Bree lies at the meeting point of two large roads, the Great East Road and the North-South Road, which made it an important centre of trade and travel.
 The Lonely Mountain or Sindarin Erebor is a mountain in the northeast of Rhovanion. When the dwarves had to leave their ancient home Khazad-dûm, it became the new home of Durin's folk, led by Thráin I, who became King under the Mountain. In T.A. 2770 Erebor was attacked by the dragon Smaug, who took it as his home, forcing the surviving dwarves to flee again. In T.A. 2941, as told in The Hobbit, the dragon Smaug was slain. The dwarves returned to the Lonely Mountain, ruled by the new King under the Mountain Dáin II Ironfoot.
 Esgaroth or Lake-town is a town in the northeast of Rhovanion, built at the Long Lake, south of the Lonely Mountain and east of Mirkwood. It was destroyed by the dragon Smaug, but was rebuilt.
 Fangorn or Fangorn Forest is a large forest on the eastern side of the southern end of the Misty Mountains. Fangorn is the only known home of the Ents, a people of giant tree-like beings. The Sindarin "Fangorn" translates to "Beardtree", and is also the Sindarin name of the Ent Treebeard.
 The Gap of Rohan, formerly known as the Gap of Calenardhon, is the opening between the mountain ranges of the Misty Mountains to the north and the White Mountains to the south. Across the Gap flowed the river Isen. On each side of the Gap lay a fortress, Isengard to the north and the Hornburg to the south. The Gap lay west of the land of Rohan, formerly Calenardhon, after which the Gap was named.
 For a more detailed treatment, see Gondor.
 The Great East Road or Dwarf Road is an ancient Dwarven road, which ran from western Beleriand, over the Ered Luin, through the Shire, to the Misty Mountains, and to the Dwarven lands in the east.
 Mirkwood was a large forest in Rhovanion. Originally it was known as Greenwood the Great (Sindarin: Eryn Galen). 
 In the Second Age an Elven people settled in the Greenwood, founding the Woodland Realm, with its capital on the hill Amon Lanc. After the end of the Second Age, the Elves retreated further north. In the 11th century of the Third Age, Sauron established himself in the hill-fortress of Dol Guldur on Amon Lanc in the south of the forest. His evil influence darkened the woods, although it lessened towards the north; afterwards the forest became known as Mirkwood. After the War of the Ring the forest was cleared of Sauron's influence and became known as Eryn Lasgalen (Sindarin: "Wood of Greenleaves"). 
 Helm's Deep was a deep valley on the north side of the western part of the White Mountains, south to the Gap of Rohan. The valley was blocked by a natural series of hills called Helm's Dike. Behind this lay the fortress Hornburg. At the end of the valley lies the entrance to the Glittering Caves.
 Isengard, Sindarin Angrenost, is a fortress at the southern end of the Misty Mountains. It is a small valley inside a ring-shaped wall, the Ring of Isengard, and in its middle stands the tall black tower Orthanc.
Isengard was built in the Second Age by the people of Gondor. In the 28th century of the Third Age, Isengard was given to Saruman by Steward Beren of Gondor. In the War of the Ring the Ents destroyed the ring-wall. After the War, the tower Orthanc was given back to King Elessar and the Reunited Kingdom.
 Khazad-dûm was a great underground city and the ancient home of Durin's Dwarves under the Misty Mountains. Other names of Khazad-dûm, Khuzdûl for the Dwarrowdelf, include Sindarin Hadhodrond, Casarrondo by the Noldor, and Westron Phurunargian. Later in the Third Age it became known as Moria. 
 Khazad-dûm was founded by Durin the Deathless a long time ago in First Age, before the Sun and Moon. Its mines were the only place in Middle-earth where the precious metal mithril could be found, making the dwarves wealthy and prosperous.  In 1980 of the Third Age, the dwarves woke a Balrog deep in the mines of Khazad-dûm. The Balrog killed King Durin VI, earning it the name "Durin's Bane", and Khazad-dûm was deserted. The survivors of Durin's folk found a new home in Erebor, and some other places. Khazad-dûm became afterwards known as Moria, which is Sindarin for "black pit". In the years 2989-2994 T.A., the Dwarves try to re-establish a colony in Moria led by Balin, which fails when all are killed by Orcs.[2]
 Lindon is an Elven realm between the Sea and the Blue Mountains. The Gulf of Lune divides it into Forlindon ("North Lindon") and Harlindon ("South Lindon"). In Lindon were also the Elven port Mithlond, or the "Grey Havens".
 Lothlórien was an Elven realm. It lay east of the Misty Mountains, on the land between the Anduin and Silverlode, close to the Dwarven realm Khazad-dûm. Lothlórien was founded in the Second Age, and Amdír became its first King. The elves of Lothlórien fought in the War of the Last Alliance against Sauron. In the war Amdír was killed, succeeded as King by his son Amroth, who later left Lothlórien. Since then until the end of the Third Age it was ruled by Lord Celeborn and Lady Galadriel. Caras Galadhon was the main city of Lothlórien. Other names of Lothlórien are the shortened form Lórien, and the names Laurelindórenan, Rohirric Dwimordene, or The Golden Wood.
 For a more detailed treatment, see Mordor.
 The North-South Road was a long road running from the realm of Arnor in the north to Gondor in the south. The road began in the city Fornost Erain, the capital of Arthedain, crossed the Great East Road at Bree, and ran further to the southern end of the Misty Mountains. There the road turned east, towards the city Minas Anor, across Osgiliath, and ended at Minas Ithil.
A part of the road south of Bree is also known as the Greenway.
 Rivendell or Sindarin Imladris was an elven residence in eastern Eriador. It was founded in 1697 of the Second Age by Elrond, who ruled it as Lord and Master of Imladris until the end of the Third Age. Many Dúnedain heirs were raised in Rivendell, among them Aragorn.
 Rohan was a human realm in the Third Age which lay north of the White Mountains, southeast of Isengard and southwest of the Emyn Muil. Its capital was Edoras, where stood Meduseld, the Golden Hall of the King.
 Originally the land was a part of Gondor, and named Calenardhon. In 2509 T.A. the Rohirrim came from the north, and helped Gondor fight against a combined invasion of Men from the north-east and Orcs from Mordor. The Rohirrim helped Gondor win the Battle of the Field of Celebrant and the war. As a reward, Steward Cirion of Gondor gave the land Calenardhon to the Rohirrim, and the King of the Rohirrim, Eorl the Young, swore the Oath of Eorl: that Rohan would come and help Gondor whenever needed. Afterwards, the people of the Rohirrim moved into their new land, which they named the Riddermark or just The Mark. Gondor called the land Rohan (Sindarin: "Land of the Horse-lords"), and its people Rohirrim (Sindarin: "People of the Horse-lords").[3]
 The Shire is a land in Eriador, inhabited by Hobbits. 
 The Shire was settled by the Hobbits in the 24th century of the Third Age. It is divided into four Farthings: the North-, West-, South- and Eastfarthing. 
 Hobbits were an independent, orderly people. It would be hard to say that they had any government at all.
Among the Shire's most influential residents of the Shire were the Mayor of Michel Delving, the Thain from Tuckborough and the Master of Buckland.
 The Shire was invaded by Saruman (called "Sharkey"), and the Hobbits were beset by "gatherers" and "sharers" - indicative of Socialism or Communism, although Tolkien disclaimed any allegorical aspects.
 1 Middle-earth regions
1.1 Beleriand
1.2 Eriador
1.3 Rhovanion
1.4 Rhûn
1.5 Harad
 1.1 Beleriand 1.2 Eriador 1.3 Rhovanion 1.4 Rhûn 1.5 Harad 2 Middle-earth mountains
2.1 Emyn Beraid
2.2 Ered Luin
2.3 Ered Mithrin
2.4 Ered Nimrais
2.5 Misty Mountains
2.6 Mount Doom
2.7 Weathertop
 2.1 Emyn Beraid 2.2 Ered Luin 2.3 Ered Mithrin 2.4 Ered Nimrais 2.5 Misty Mountains 2.6 Mount Doom 2.7 Weathertop 3 Middle-earth bodies of water
3.1 Middle-earth rivers
3.1.1 Anduin
3.1.2 Bruinen
3.1.3 Baranduin
 3.1 Middle-earth rivers
3.1.1 Anduin
3.1.2 Bruinen
3.1.3 Baranduin
 3.1.1 Anduin 3.1.2 Bruinen 3.1.3 Baranduin 4 Other Middle-earth locations
4.1 Angmar
4.2 Arnor
4.3 Breeland
4.4 Erebor
4.5 Esgaroth
4.6 Fangorn
4.7 Gap of Rohan
4.8 Gondor
4.9 Great East Road
4.10 Mirkwood
4.11 Helm's Deep
4.12 Isengard
4.13 Khazad-dûm
4.14 Lindon
4.15 Lothlórien
4.16 Mordor
4.17 North-South Road
4.18 Rivendell
4.19 Rohan
4.20 The Shire
 4.1 Angmar 4.2 Arnor 4.3 Breeland 4.4 Erebor 4.5 Esgaroth 4.6 Fangorn 4.7 Gap of Rohan 4.8 Gondor 4.9 Great East Road 4.10 Mirkwood 4.11 Helm's Deep 4.12 Isengard 4.13 Khazad-dûm 4.14 Lindon 4.15 Lothlórien 4.16 Mordor 4.17 North-South Road 4.18 Rivendell 4.19 Rohan 4.20 The Shire 5 References 6 See also ↑ J. R. R. Tolkien, The Lord of the Rings, Appendix A, I  The Númenorean Kings, iii  Eriador, Arnor, and the Heirs of Isildur
 ↑ J. R. R. Tolkien, The Lord of the Rings, Appendix A, III  Durin's Folk
 ↑ J. R. R. Tolkien, The Lord of the Rings, Appendix A, II  The House of Eorl
 Middle-earth Middle-earth Log in Page talk page Read View source View history Main Page Recent changes New Pages Random page Statistics What links here Related changes Special pages Printable version Permanent link Page information  This page was last modified on 23 February 2017, at 12:32. This page has been accessed 52,320 times. Privacy policy About Conservapedia Disclaimers Mobile view 
 Beleriand Eriador Rhovanion Wilderland Rhûn Sea of Rhûn Harad Haradwaith Umbar Emyn Beraid Tower Hills Elostirion Ered Luin Blue Mountains Ered Lindon Ered Mithrin Grey Mountains Ered Nimrais White Mountains Mindolluin Misty Mountains Hithaeglir Mount Doom Amon Amarth Orodruin Sammath Naur Cracks of Doom Weathertop Amon Sûl Weather Hills Tower of Amon Sûl Anduin Langflood Sarn Gebir Argonath Nen Hithoel Falls of Rauros Ethir Anduin Bruinen Loudwater Baranduin Brandywine River Angmar Carn Dûm Arnor Annúminas Arthedain Fornost Erain Cardolan Rhudaur Reunited Kingdom of Arnor and Gondor Breeland Breehill Bree Archet Combe Staddle Lonely Mountain Erebor Esgaroth Lake-town Fangorn Fangorn Forest Gap of Rohan Gap of Calenardhon Great East Road Dwarf Road Mirkwood Greenwood the Great Woodland Realm Amon Lanc Dol Guldur Eryn Lasgalen Helm's Deep Helm's Dike Hornburg Glittering Caves Isengard Angrenost Ring of Isengard Orthanc Khazad-dûm the Dwarrowdelf Moria Moria Lindon Forlindon Harlindon Mithlond Grey Havens Lothlórien Caras Galadhon Lórien Laurelindórenan Dwimordene The Golden Wood North-South Road Greenway Rivendell Imladris Rohan Edoras Meduseld Calenardhon Shire  J. R. R. Tolkien's Middle-earthBooksThe Hobbit • The Lord of the Rings • (Posthumous:) The SilmarillionPeoplesAinur • Elves • Hobbits • Dwarves • Ents • OrcsGeographyMiddle-earth locations • Númenor • AmanOtherObjects • Languages • Scripts • Chronology • List of topics  Books The Hobbit • The Lord of the Rings • (Posthumous:) The Silmarillion   Peoples Ainur • Elves • Hobbits • Dwarves • Ents • Orcs  Geography Middle-earth locations • Númenor • Aman  Other Objects • Languages • Scripts • Chronology • List of topics Middle-earth locations Contents Middle-earth regions Middle-earth mountains Middle-earth bodies of water Other Middle-earth locations References See also Navigation menu Beleriand Eriador Rhovanion Rhûn Harad Emyn Beraid Ered Luin Ered Mithrin Ered Nimrais Misty Mountains Mount Doom Weathertop Middle-earth rivers Angmar Arnor Breeland Erebor Esgaroth Fangorn Gap of Rohan Gondor Great East Road Mirkwood Helm's Deep Isengard Khazad-dûm Lindon Lothlórien Mordor North-South Road Rivendell Rohan The Shire Personal tools Namespaces Variants Views More 
Search
 Popular Links donate Edit Console Anduin Bruinen Baranduin 
