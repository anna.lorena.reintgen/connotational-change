The 1998 Football Association Charity Shield (also known as The AXA FA Charity Shield for sponsorship reasons) was the 76th FA Charity Shield, an annual English football match organised by The Football Association and played between the winners of the previous season's Premier League and FA Cup competitions. It was contested on 9 August 1998 by Arsenal – who won a league and FA Cup double the previous season – and Manchester United – who finished runners-up in the league. Watched by a crowd of 67,342 at Wembley Stadium, Arsenal won the match 3–0.
 This was Manchester United's 18th Charity Shield appearance to Arsenal's 14th. Manchester United began the game more strongly, but Arsenal took the lead when Marc Overmars scored 11 minutes before half-time. They extended their lead in the second half, as Overmars and Nicolas Anelka found Christopher Wreh, who put the ball into an empty net at the second attempt. In the 72nd minute, Arsenal scored a third goal, when Anelka got around Jaap Stam in the penalty box and shot the ball past goalkeeper Peter Schmeichel.
 Arsenal's victory marked Manchester United's first Shield defeat in 13 years. The teams later faced each other in the FA Cup semi-final, which was won by Manchester United in a replay. Manchester United finished the league season one point ahead of Arsenal and went on to win the FA Cup and UEFA Champions League, thereby completing a treble of trophies in the 1998–99 season.
 Founded in 1908 as a successor to the Sheriff of London Charity Shield,[4] the FA Charity Shield began as a contest between the respective champions of the Football League and Southern League, although in 1913 it was played between an Amateurs XI and a Professionals XI.[5] In 1921, it was played by the league champions of the top division and FA Cup winners for the first time.[6][a] The match was played at Wembley Stadium, which first hosted the Shield in 1974.[8]
 Arsenal qualified for the 1998 FA Charity Shield as winners of the 1997–98 FA Premier League.[9] Although they were 12 points behind league leaders Manchester United by the end of February 1998, a nine-match winning streak, culminating in a 4–0 win over Everton on 3 May 1998, ensured Arsenal won the title.[10] Arsenal beat Newcastle United 2–0 in the 1998 FA Cup Final to complete the domestic double.[11] Given they won both honours, the other Charity Shield place went to league runners-up Manchester United.[9]
 The most recent meeting between the two teams was in the Premier League on 14 March 1998, when a second-half goal by Marc Overmars gave Arsenal a 1–0 win at Old Trafford.[12][13] Arsenal were the only team in the 1997–98 league to beat United home and away, with the corresponding home fixture ending 3–2.[14] Arsenal manager Arsène Wenger acknowledged the Shield game was the "only opportunity to play our first-team men together against top-class opposition" before their league campaign commenced the following week.[15] Manchester United manager Alex Ferguson was preoccupied with the team's match against ŁKS Łódź in the second qualifying round of the UEFA Champions League three days later. He felt the contest with Arsenal would get his "players' sharpness up and provide plenty of benefit" for their upcoming matches.[16]
 The match was officially referred to as "The AXA FA Charity Shield" as part of a sponsorship deal between The Football Association and French insurance group AXA, agreed in July 1998. The deal also saw the FA Cup referred to as "The AXA Sponsored FA Cup" for its four-year duration.[17]
 Manchester United winger Jesper Blomqvist was ruled out with an ankle injury, but Roy Keane was fit enough to start his first competitive match since damaging his ligaments eleven months previously.[18][19] Defender Jaap Stam, who signed for United in May 1998,[20] made his competitive debut for the club, partnering centre-back Ronny Johnsen.[21] For Arsenal, new signing Nelson Vivas began the match on the substitutes' bench,[22] in spite of being expected to make his full debut,[23] while Dennis Bergkamp started alongside Nicolas Anelka up front.[24]
 Arsenal employed a traditional 4–4–2 formation: a four-man defence (comprising two centre-backs and left and right full-backs), four midfielders (two in the centre, and one on each wing) and two centre-forwards.[25] Manchester United organised themselves slightly differently, with Paul Scholes playing ahead of the midfield in a supporting role behind the main striker, Andy Cole. The team lined up in a 4–4–1–1 formation.[25]
 In pitch-side temperatures of 30 °C (86 °F),[2] Manchester United enjoyed their best spell of the match early on, while Arsenal's pair Patrick Vieira and Emmanuel Petit adjusted themselves.[25] United fashioned their first chance through David Beckham, who was booed throughout the match on account of many fans blaming him for England's elimination from the 1998 FIFA World Cup.[25][b] His pass eventually met Scholes, whose attempt forced Arsenal goalkeeper David Seaman to clear.[21] In spite of United's promising start, it was Arsenal who scored the opening goal. Vieira played the ball down the right side of the penalty area in the direction of Bergkamp and Anelka. Bergkamp got there first and back-heeled the ball to Anelka, but the Frenchman was unable to take control; however, he was able to put pressure on Johnsen in the Manchester United defence and blocked the Norwegian's attempted clearance. The ball ran across the edge of the penalty area to Overmars, who lashed it right-footed past Manchester United goalkeeper Peter Schmeichel into the net.[21] A shot by Keane from 25 yards (23 m) prompted a save from Seaman in the 42nd minute.[2]
 Arsenal began dominating in the second half, and increased their lead after 57 minutes. From the left wing, Overmars used his pace to get the better of Gary Neville and passed the ball to Anelka, who turned and passed to an unmarked Christopher Wreh. Schmeichel blocked the Liberian's initial shot with his feet, but he was unable to stop the second attempt or Wreh's acrobatic celebration.[2] Despite the setback, United continued to press Arsenal; defender Martin Keown almost put the ball into his own goal from Ryan Giggs's corner.[2] Both teams made mass substitutions in the final third of the game, notably Teddy Sheringham and Luís Boa Morte coming on for Cole – who rarely threatened – and Petit, respectively.[21] Arsenal scored their third in the 72nd minute – Parlour's pass found Anelka, who got around Stam and shot the ball past Schmeichel from a narrow angle, inside the goalkeeper's near post.[21] Near the end, Sheringham wasted a goal-scoring opportunity, shooting wide.[27]
 Match rules
 Source:[29]
 The result marked Manchester United's first Shield defeat in 13 years,[2] and was the ninth time Arsenal had won the Charity Shield.[30] Arsenal became the first southern club[c] since Tottenham Hotspur in 1962 to win the Shield outright.[2] Wenger described the scoreline as "unexpected" and cited the first goal as crucial in the match, given the weather conditions.[33][34] He was content with how his international players, who had been in the World Cup, coped with the game's physicality. Wenger believed the result gave Arsenal a psychological boost for the Champions League campaign, as the club planned to stage their home matches at Wembley Stadium.[34] Bergkamp felt the result showed that Arsenal had what it took to retain the Premier League title: "We've still got the same mentality and that will be the basis for this year's challenge. This is a good start. It is harder to retain the trophy."[34]
 Ferguson admitted his team had been beaten by the better side and agreed with Wenger that the first goal was important.[21][35] He was pleased that Keane got through the match after 11 months out of action and was confident his team would fare better against ŁKS Łódź, the following Wednesday.[35] Schmeichel felt the upcoming Champions League qualifier was more important than the Charity Shield game, which he considered as a pre-season match.[36] Ferguson anticipated another challenge from Arsenal in the league: "I think you could make a strong case for four teams to challenge for the Premiership but I think Arsenal pose the biggest threat."[35]
 Three days after the Charity Shield match, United beat ŁKS Łódź 2–0 and qualified for the Champions League group stage following a goalless match a fortnight later.[37][38] Arsenal had the upper hand in their two league meetings with United during the season, winning 3–0 at Highbury in September 1998,[39] before a 1–1 draw at Old Trafford in February 1999.[40] The two teams went into the final day of the 1998–99 FA Premier League vying for the title, but United's 2–1 win against Tottenham meant they finished one point above Arsenal.[41] The two sides met twice more that season in the FA Cup semi-final, which was settled in a replay after the original match finished goalless.[42] Manchester United won in extra time – the winning goal scored by Giggs.[43] United then went on to defeat Newcastle United 2–0 in the 1999 FA Cup Final.[44] Whereas Arsenal failed to progress past the group stage of the Champions League,[45] Manchester United went on to reach the final, where they beat Bayern Munich to win the competition for the second time.[46] Ferguson's team therefore completed a treble of trophies in one season.[47]
 
 1 Background 2 Match

2.1 Team selection
2.2 Summary
2.3 Details
2.4 Statistics

 2.1 Team selection 2.2 Summary 2.3 Details 2.4 Statistics 3 Post-match 4 See also 5 Notes 6 References Marc Overmars (Arsenal) Assistant referees:
Dave Morrall (South Yorkshire)[28]
Peter Walton (Northamptonshire)[28] Dave Morrall (South Yorkshire)[28] Peter Walton (Northamptonshire)[28] Fourth official: Neale Barry (Lincolnshire)[28] 90 minutes. Penalty shootout if scores level. Seven named substitutes. Maximum of six substitutions. 1998–99 Arsenal F.C. season 1998–99 Manchester United F.C. season ^ The Premier League replaced the Football League First Division at the top of the English football pyramid after its inception in 1992.[7]
 ^ Beckham was sent off for kicking Diego Simeone in the second round of the 1998 World Cup against Argentina. His dismissal led to considerable abuse from certain sections of the media and England fans. The player became a scapegoat for the national team's failure to progress, as in the match England were eliminated on penalties.[26]
 ^ One which is located in the southern counties of England. Initially these were amateur clubs, as professionalism in football was not as readily accepted in the south as in the north. In the 1893–94 season, Arsenal (under its former name Woolwich Arsenal) turned professional and became the first southern club admitted to the northern-oriented Football League. The following year saw the creation of the Southern Football League, which was composed of amateur and professional teams. By the 1920–21 season, the top division of the Southern Football League was absorbed by the Football League, to create its third division.[31][32]
 ^ "The week's fixtures with ticket prices and booking information". The Guardian. London. 8 August 1998. p. A11..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c d e f g Moore, Glenn (10 August 1998). "Football: Arsenal show United little charity". The Independent. London. Retrieved 18 July 2014.
 ^ "History for London City, United Kingdom". Weather Underground. Retrieved 11 January 2013.
 ^ "Abandonment of the Sheriff Shield". The Observer. London. 19 April 1908. p. 11.
 ^ "The F.A. Charity Shield". The Times. 7 October 1913. p. 10.
 ^ Ferguson, Peter (4 August 2011). "The Shield: From the beginning". Manchester City F.C. Retrieved 21 April 2014.
 ^ Fynn, Alex (2 December 2001). "Continental or the full English?". The Observer. London. Retrieved 3 July 2014.
 ^ "The FA Community Shield history". TheFA.com (The Football Association). Archived from the original on 6 July 2013. Retrieved 3 July 2013.
 ^ a b "Arsenal soon back in the groove". Courier Mail. Queensland. 10 August 1998. p. 48.
 ^ Lacey, David (4 May 1998). "Gunners rest their case for the defence". The Guardian. London. p. A3.
 ^ "Arsenal at the double". BBC News. 16 May 1998. Retrieved 11 January 2013.
 ^ "Arsenal v Manchester United head-to-head record". United Mad. Digital Sports Group. Archived from the original on 6 July 2013. Retrieved 3 July 2013.
 ^ "Overmars keeps title race alive". BBC News. 14 March 1998. Retrieved 11 January 2013.
 ^ "Manchester United – 1997–98". Statto Organisation. Archived from the original on 24 March 2013. Retrieved 3 July 2013.
 ^ Hart, Michael (7 August 1998). "Arsenal need spirit to cure the hangover". London Evening Standard. p. 71.
 ^ Cass, Bob (10 August 1998). "Keane to succeed". The Mail on Sunday. London.
 ^ "F.A. Gets Four Year AXA Sponsorship". Newsline. Mediatel Group. 23 July 1998. Retrieved 17 June 2014.
 ^ Brodkin, Jon (7 August 1998). "Blomqvist out as Keane eyes return". The Guardian. London. p. B6.
 ^ Millar, Steve (12 August 1998). "Keane can't wait for the sparks to fly". The Mirror. London. Retrieved 3 August 2014.
 ^ Maddock, David (6 May 1998). "Stam's arrival relieves the gloom for United". The Times. p. 41.
 ^ a b c d e f Holt, Oliver (10 August 1998). "Arsenal warm to their second home". The Times. p. 32.
 ^ "Wenger is gunning for domestic success". The Herald. Glasgow. 17 August 1998. Retrieved 26 July 2014.
 ^ Martin, Andrew (9 August 1998). "Charity and faith is Vivas' hope". The Independent. London. Retrieved 16 February 2013.
 ^ a b Dillon, John (10 August 1998). "Wenger's hot shots have fun in the sun; Arsenal 3 Man Utd 0". The Mirror. London. Retrieved 16 February 2013.
 ^ a b c d Lacey, David (10 August 1998). "Wenger's all-stars write an epitaph to United; FA Charity Shield Arsenal 3 Manchester United 0: Overmars sets Double winners on way to victory that promises more success". The Guardian. London. p. 21.
 ^ Hill, Dave (15 August 1998). "Beckham". The Independent. London. Retrieved 13 August 2013.
 ^ "Boo-boy Beckham fails to paper over Ferguson's cracks". Birmingham Mail. 10 August 1998.
 ^ a b c d O'Malley, Peter, ed. (9 August 1998). Official Matchday Programme. p. 66.
 ^ Hunt, Chris, ed. (22 August 1998). "Match Facts". Match. Peterborough: EMAP Pursuit Publishing: 6.
 ^ Ross, James (15 August 2013). "List of FA Charity/Community Shield Matches". Rec.Sport.Soccer Statistics Foundation (RSSSF). Retrieved 11 January 2014.
 ^ Tomlinson, Alan (2010). A Dictionary of Sports Studies. Oxford University Press. p. 196. ISBN 0-19-921381-X.
 ^ Freeman, Nicholas (2011). 1895: Drama, Disaster and Disgrace in Late Victorian Britain. Edinburgh University Press. p. 39. ISBN 0-7486-4056-8.
 ^ Dorward, Philip (10 August 1998). "Charity Shield victory gives Arsenal important psychological edge over Old Trafford rivals". The Scotsman. Edinburgh. p. 21.
 ^ a b c Lipton, Martin (10 August 1998). "Fans will make it hell for Beckham". Daily Mail. London.
 ^ a b c "Arsene approves of that Wembley winning habit". Herald Express. Torquay. 10 August 1998. p. 32.
 ^ "Man U on new ground in early cup clash". Hobart Mercury. 12 August 1998. p. 25.
 ^ Pierson, Mark (14 August 1998). "Roving role is fine by Giggs". The Independent. London. Retrieved 3 July 2013.
 ^ Hodgson, Guy (27 August 1998). "United poles apart from Lodz". The Independent. London. Retrieved 3 July 2013.
 ^ Winter, Henry (21 September 1998). "Fergie shell-shocked by awesome Gunners". Irish Independent. Dublin. Retrieved 11 January 2013.
 ^ Hodgson, Guy (18 February 1999). "United rescued by Cole". The Independent. London. Retrieved 11 January 2013.
 ^ Holt, Oliver; Dickinson, Matt (17 May 1999). "One down, two to go for United". The Times. p. 25.
 ^ Holt, Oliver (12 April 1999). "Odds grow longer on treble chance". The Times. p. 29.
 ^ "Giggs magic sinks Gunners". BBC News. 14 April 1999. Retrieved 11 January 2013.
 ^ "Double joy for United". BBC News. 22 May 1999. Retrieved 23 May 2014.
 ^ Tongue, Steve (26 November 1998). "Parlour off as Arsenal go out". The Independent. London. Retrieved 3 August 2014.
 ^ "Treble joy for United fans". BBC News. 27 May 1999. Retrieved 3 July 2013.
 ^ "United crowned kings of Europe". BBC News. 26 May 1999. Retrieved 3 July 2013.
 v t e 1908 1909 1910 1911 1912 1913 1920 1921 1922 1923 1924 1925 1926 1927 1928 1929 1930 1931 1932 1933 1934 1935 1936 1937 1938 1948 1949 1950 1951 1952 1953 1954 1955 1956 1957 1958 1959 1960 1961 1962 1963 1964 1965 1966 1967 1968 1969 1970 1971 1972 1973 1974 1975 1976 1977 1978 1979 1980 1981 1982 1983 1984 1985 1986 1987 1988 1989 1990 1991 1992 1993 1994 1995 1996 1997 1998 1999 2000 2001 2002 2003 2004 2005 2006 2007 2008 2009 2010 2011 2012 2013 2014 2015 2016 2017 2018 2019 List of matches v t e FA Cup (Qualifying rounds Final) Charity Shield FA Trophy (Final) FA Vase Premier League Football League (First Division Second Division Third Division) League Cup (Final) Football League Trophy (Final) Play-offs (First Division Final Second Division Final Third Division Final) Football Conference Conference League Cup Isthmian League Northern Premier League Southern League Northern Counties East League Northern League North West Counties League Spartan South Midlands League Wessex League Western League Champions League (Final) UEFA Cup Cup Winners' Cup UEFA Super Cup UEFA Euro 2000 qualifying (Qualification Group 5) Glenn Hoddle Howard Wilkinson (caretaker) Kevin Keegan Arsenal Aston Villa Blackburn Rovers Charlton Athletic Chelsea Coventry City Derby County Everton Leeds United Leicester City Liverpool Manchester United Middlesbrough Newcastle United Nottingham Forest Sheffield Wednesday Southampton Tottenham Hotspur West Ham United Wimbledon Barnsley Birmingham City Bolton Wanderers Bradford City Bristol City Bury Crewe Alexandra Crystal Palace Grimsby Town Huddersfield Town Ipswich Town Norwich City Oxford United Port Vale Portsmouth Queens Park Rangers Sheffield United Stockport County Sunderland Swindon Town Tranmere Rovers Watford West Bromwich Albion Wolverhampton Wanderers Bournemouth Blackpool Bristol Rovers Burnley Chesterfield Colchester United Fulham Gillingham Lincoln City Luton Town Macclesfield Town Manchester City Millwall Northampton Town Notts County Oldham Athletic Preston North End Reading Stoke City Walsall Wigan Athletic Wrexham Wycombe Wanderers York City Barnet Brentford Brighton & Hove Albion Cambridge United Cardiff City Carlisle United Chester City Darlington Exeter City Halifax Town Hartlepool United Hull City Leyton Orient Mansfield Town Peterborough United Plymouth Argyle Rochdale Rotherham United Scarborough Scunthorpe United Shrewsbury Town Southend United Swansea City Torquay United Newport v t e 1927 1930 1932 1936 1950 1952 1971 1972 1978 1979 1980 1993 1998 2001 2002 2003 2005 2014 2015 2017 1941 1943 1968 1969 1987 1988 1993 2007 2011 2018 1930 1931 1933 1934 1935 1936 1938 1948 1953 1979 1989 1991 1993 1998 1999 2002 2003 2004 2005 2014 2015 2017 2006 1980 1994 1995 2000 2019 1970 1994 Leicester City 6–6 Arsenal (1930) Aston Villa 1–7 Arsenal (1935) Liverpool 0–2 Arsenal (1989) Manchester United–Arsenal brawl (1990) Battle of Old Trafford (2003) Battle of the Buffet (2004) Wrexham 2–1 Arsenal (1992) MLS All-Star Game (2016) Joan Gamper Trophy (2019) v t e 1909 1948 1957 1958 1963 1976 1977 1979 1983 1985 1990 1994 1995 1996 1999 2004 2005 2007 2016 2018 1983 1991 1992 1994 2003 2006 2009 2010 2017 1908 1911 1948 1952 1956 1957 1963 1965 1967 1977 1983 1985 1990 1993 1994 1996 1997 1998 1999 2000 2001 2003 2004 2007 2008 2009 2010 2011 2013 2016 1968 1999 2008 2009 2011 1991 2017 1991 1999 2008 2017 1968 1999 2008 3–5 v West Bromwich Albion (1978) Manchester United–Arsenal brawl (1990) 9–0 v Ipswich Town (1995) 8–1 v Nottingham Forest (1999) Battle of Old Trafford (2003) Battle of the Buffet (2004) 4–3 v Manchester City (2009) UEFA Celebration Match (2007) 2010 MLS All-Star 2011 MLS All-Star 2013 A-League All Stars FA Community Shield Arsenal F.C. matches Manchester United F.C. matches 1998–99 in English football August 1998 sports events CS1: Julian–Gregorian uncertainty Featured articles Use dmy dates from March 2012 Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version العربية Español فارسی Hrvatski Bahasa Indonesia Italiano Nederlands Русский Українська Tiếng Việt 中文  This page was last edited on 2 October 2019, at 20:21 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  1998 Football Association Charity Shield The AXA FA Charity Shield Arsenal Manchester United 1 2 6 14 3 15 17 4 11 9 10 Substitutes: 13 5 7 16 18 21 12 Manager: 1 2 5 6 3 7 8 16 11 18 9 Substitutes: 31 4 12 21 25 10 20 Manager: Match rules ^ ^ ^ ^ a b c d e f g ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b c d e f ^ ^ a b a b c d ^ ^ a b c d ^ ^ ^ ^ ^ a b c a b c ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ Club seasons Man of the match Match officials The match programme cover 

Arsenal
Manchester United




3
0

 9 August 1998[1] Wembley Stadium, London Marc Overmars (Arsenal)[2] Graham Poll (Hertfordshire) 67,342 Clear22 °C (72 °F)[3] ← 1997 1999 →  Overmars  34'Wreh  57'Anelka  72'   












Arsenal

 












Manchester United

 






GK
1
 David Seaman


RB
2
 Lee Dixon
 81'


CB
6
 Tony Adams (c)

 80'


CB
14
 Martin Keown
 22'


LB
3
 Nigel Winterburn


RM
15
 Ray Parlour


CM
17
 Emmanuel Petit

 73'


CM
4
 Patrick Vieira

 84'


LM
11
 Marc Overmars

 67'


CF
9
 Nicolas Anelka


CF
10
 Dennis Bergkamp

 46'


Substitutes:


GK
13
 Alex Manninger


DF
5
 Steve Bould

 80'


DF
7
 Nelson Vivas


MF
16
 Stephen Hughes

 67'


MF
18
 Gilles Grimandi

 84'


MF
21
 Luís Boa Morte

 73'


FW
12
 Christopher Wreh

 46'


Manager:


 Arsène Wenger

 GK 1  David Seaman
 RB 2  Lee Dixon  81'
 CB 6  Tony Adams (c)   80'
 CB 14  Martin Keown  22'
 LB 3  Nigel Winterburn
 RM 15  Ray Parlour
 CM 17  Emmanuel Petit   73'
 CM 4  Patrick Vieira   84'
 LM 11  Marc Overmars   67'
 CF 9  Nicolas Anelka
 CF 10  Dennis Bergkamp   46'
 Substitutes:
 GK 13  Alex Manninger
 DF 5  Steve Bould   80'
 DF 7  Nelson Vivas
 MF 16  Stephen Hughes   67'
 MF 18  Gilles Grimandi   84'
 MF 21  Luís Boa Morte   73'
 FW 12  Christopher Wreh   46'
 Manager:
  Arsène Wenger
 
 






GK
1
 Peter Schmeichel


RB
2
 Gary Neville
 3'


CB
5
 Ronny Johnsen


CB
6
 Jaap Stam


LB
3
 Denis Irwin
 26'


RM
7
 David Beckham


CM
8
 Nicky Butt

 53'


CM
16
 Roy Keane (c)

 76'


LM
11
 Ryan Giggs

 70'


CF
18
 Paul Scholes

 70'


CF
9
 Andy Cole

 70'


Substitutes:


GK
31
 Nick Culkin


DF
4
 David May


DF
12
 Phil Neville
 79'
 70'


DF
21
 Henning Berg

 76'


MF
25
 Jordi Cruyff

 70'


FW
10
 Teddy Sheringham

 70'


FW
20
 Ole Gunnar Solskjær

 53'


Manager:


 Alex Ferguson


 GK 1  Peter Schmeichel
 RB 2  Gary Neville  3'
 CB 5  Ronny Johnsen
 CB 6  Jaap Stam
 LB 3  Denis Irwin  26'
 RM 7  David Beckham
 CM 8  Nicky Butt   53'
 CM 16  Roy Keane (c)   76'
 LM 11  Ryan Giggs   70'
 CF 18  Paul Scholes   70'
 CF 9  Andy Cole   70'
 Substitutes:
 GK 31  Nick Culkin
 DF 4  David May
 DF 12  Phil Neville  79'  70'
 DF 21  Henning Berg   76'
 MF 25  Jordi Cruyff   70'
 FW 10  Teddy Sheringham   70'
 FW 20  Ole Gunnar Solskjær   53'
 Manager:
  Alex Ferguson
 
Man of the match
Marc Overmars (Arsenal)
Match officials
Assistant referees:
Dave Morrall (South Yorkshire)[28]
Peter Walton (Northamptonshire)[28]
Fourth official: Neale Barry (Lincolnshire)[28]
 
Match rules

90 minutes.
Penalty shootout if scores level.
Seven named substitutes.
Maximum of six substitutions.
 3 0
 55% 45%
 7 2
 1 3
 2 11
 3 5
 2 3
 0 0
 Source:[24]
 
1908
1909
1910
1911
1912
1913
1920
1921
1922
1923
1924
1925
1926
1927
1928
1929
1930
1931
1932
1933
1934
1935
1936
1937
1938
1948
1949
1950
1951
1952
1953
1954
1955
1956
1957
1958
1959
1960
1961
1962
1963
1964
1965
1966
1967
1968
1969
1970
1971
1972
1973
1974
1975
1976
1977
1978
1979
1980
1981
1982
1983
1984
1985
1986
1987
1988
1989
1990
1991
1992
1993
1994
1995
1996
1997
1998
1999
2000
2001
 
2002
2003
2004
2005
2006
2007
2008
2009
2010
2011
2012
2013
2014
2015
2016
2017
2018
2019
 
List of matches
  « 1997–98  1999–2000 »  
FA Cup (Qualifying rounds
Final)
Charity Shield
FA Trophy (Final)
FA Vase
 
Premier League
Football League (First Division
Second Division
Third Division)
League Cup (Final)
Football League Trophy (Final)
Play-offs (First Division Final
Second Division Final
Third Division Final)
 
Football Conference
Conference League Cup
 
Isthmian League
Northern Premier League
Southern League
Northern Counties East League
Northern League
North West Counties League
Spartan South Midlands League
Wessex League
Western League
 
Champions League (Final)
UEFA Cup
Cup Winners' Cup
UEFA Super Cup
 
UEFA Euro 2000 qualifying (Qualification Group 5)
Glenn Hoddle
Howard Wilkinson (caretaker)
Kevin Keegan
 Club seasonsPremier League
Arsenal
Aston Villa
Blackburn Rovers
Charlton Athletic
Chelsea
Coventry City
Derby County
Everton
Leeds United
Leicester City
Liverpool
Manchester United
Middlesbrough
Newcastle United
Nottingham Forest
Sheffield Wednesday
Southampton
Tottenham Hotspur
West Ham United
Wimbledon
First Division
Barnsley
Birmingham City
Bolton Wanderers
Bradford City
Bristol City
Bury
Crewe Alexandra
Crystal Palace
Grimsby Town
Huddersfield Town
Ipswich Town
Norwich City
Oxford United
Port Vale
Portsmouth
Queens Park Rangers
Sheffield United
Stockport County
Sunderland
Swindon Town
Tranmere Rovers
Watford
West Bromwich Albion
Wolverhampton Wanderers
Second Division
Bournemouth
Blackpool
Bristol Rovers
Burnley
Chesterfield
Colchester United
Fulham
Gillingham
Lincoln City
Luton Town
Macclesfield Town
Manchester City
Millwall
Northampton Town
Notts County
Oldham Athletic
Preston North End
Reading
Stoke City
Walsall
Wigan Athletic
Wrexham
Wycombe Wanderers
York City
Third Division
Barnet
Brentford
Brighton & Hove Albion
Cambridge United
Cardiff City
Carlisle United
Chester City
Darlington
Exeter City
Halifax Town
Hartlepool United
Hull City
Leyton Orient
Mansfield Town
Peterborough United
Plymouth Argyle
Rochdale
Rotherham United
Scarborough
Scunthorpe United
Shrewsbury Town
Southend United
Swansea City
Torquay United
Non-League
Newport
 Club seasons 
Arsenal
Aston Villa
Blackburn Rovers
Charlton Athletic
Chelsea
Coventry City
Derby County
Everton
Leeds United
Leicester City
Liverpool
Manchester United
Middlesbrough
Newcastle United
Nottingham Forest
Sheffield Wednesday
Southampton
Tottenham Hotspur
West Ham United
Wimbledon
 
Barnsley
Birmingham City
Bolton Wanderers
Bradford City
Bristol City
Bury
Crewe Alexandra
Crystal Palace
Grimsby Town
Huddersfield Town
Ipswich Town
Norwich City
Oxford United
Port Vale
Portsmouth
Queens Park Rangers
Sheffield United
Stockport County
Sunderland
Swindon Town
Tranmere Rovers
Watford
West Bromwich Albion
Wolverhampton Wanderers
 
Bournemouth
Blackpool
Bristol Rovers
Burnley
Chesterfield
Colchester United
Fulham
Gillingham
Lincoln City
Luton Town
Macclesfield Town
Manchester City
Millwall
Northampton Town
Notts County
Oldham Athletic
Preston North End
Reading
Stoke City
Walsall
Wigan Athletic
Wrexham
Wycombe Wanderers
York City
 
Barnet
Brentford
Brighton & Hove Albion
Cambridge United
Cardiff City
Carlisle United
Chester City
Darlington
Exeter City
Halifax Town
Hartlepool United
Hull City
Leyton Orient
Mansfield Town
Peterborough United
Plymouth Argyle
Rochdale
Rotherham United
Scarborough
Scunthorpe United
Shrewsbury Town
Southend United
Swansea City
Torquay United
 
Newport
 List of transfers 
1927
1930
1932
1936
1950
1952
1971
1972
1978
1979
1980
1993
1998
2001
2002
2003
2005
2014
2015
2017
 
1941
1943
 
1968
1969
1987
1988
1993
2007
2011
2018
 
1930
1931
1933
1934
1935
1936
1938
1948
1953
1979
1989
1991
1993
1998
1999
2002
2003
2004
2005
2014
2015
2017
 
2006
 
1980
1994
1995
 
2000
2019
 
1970
 
1994
 
Leicester City 6–6 Arsenal (1930)
Aston Villa 1–7 Arsenal (1935)
Liverpool 0–2 Arsenal (1989)
Manchester United–Arsenal brawl (1990)
Battle of Old Trafford (2003)
Battle of the Buffet (2004)
 
Wrexham 2–1 Arsenal (1992)
MLS All-Star Game (2016)
Joan Gamper Trophy (2019)
 
1909
1948
1957
1958
1963
1976
1977
1979
1983
1985
1990
1994
1995
1996
1999
2004
2005
2007
2016
2018
 
1983
1991
1992
1994
2003
2006
2009
2010
2017
 
1908
1911
1948
1952
1956
1957
1963
1965
1967
1977
1983
1985
1990
1993
1994
1996
1997
1998
1999
2000
2001
2003
2004
2007
2008
2009
2010
2011
2013
2016
 
1968
1999
2008
2009
2011
 
1991
 
2017
 
1991
1999
2008
2017
 
1968
1999
 
2008
 
3–5 v West Bromwich Albion (1978)
Manchester United–Arsenal brawl (1990)
9–0 v Ipswich Town (1995)
8–1 v Nottingham Forest (1999)
Battle of Old Trafford (2003)
Battle of the Buffet (2004)
4–3 v Manchester City (2009)
 
UEFA Celebration Match (2007)
2010 MLS All-Star
2011 MLS All-Star
2013 A-League All Stars
 