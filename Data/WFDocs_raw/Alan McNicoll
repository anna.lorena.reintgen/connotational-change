
 Vice Admiral Sir Alan Wedel Ramsay McNicoll, KBE, CB, GM (3 April 1908 – 11 October 1987) was a senior officer in the Royal Australian Navy (RAN) and a diplomat. Born in Melbourne, he entered the Royal Australian Naval College at the age of thirteen and graduated in 1926. Following training and staff appointments in Australia and the United Kingdom, he was attached to the Royal Navy at the outbreak of the Second World War. As torpedo officer of the 1st Submarine Flotilla in the Mediterranean theatre, McNicoll was decorated with the George Medal in 1941 for disarming enemy ordnance. He served aboard HMS King George V from 1942, sailing in support of several Arctic convoys and taking part in the Allied invasion of Sicily. McNicoll was posted for staff duties with the Admiralty from September 1943 and was involved in the planning of the Normandy landings. He returned to Australia in October 1944.
 McNicoll was made executive officer of HMAS Hobart in September 1945. Advanced to captain in 1949, he successively commanded HMAS Shoalhaven and HMAS Warramunga before being transferred to the Navy Office in July 1950. In 1952, McNicoll chaired the planning committee for the British nuclear tests on the Montebello Islands, and was appointed commanding officer of HMAS Australia. He commanded the ship for two years before it was sold off for scrap, at which point he returned to London to attend the Imperial Defence College in 1955. He occupied staff positions in London and Canberra before being posted to the Naval Board as Chief of Personnel in 1960. This was followed by a term as Flag Officer Commanding HM Australian Fleet.
 McNicoll's career culminated with his promotion to vice admiral and appointment as First Naval Member and Chief of Naval Staff (CNS) in February 1965. As CNS, McNicoll had to cope with significant morale and recruitment issues occasioned by the February 1964 collision between HMAS Melbourne and Voyager and, furthermore, oversaw an extensive modernisation of the Australian fleet. In 1966, he presided over the RAN contribution to the Vietnam War, and it was during his tenure that the Australian White Ensign was created. McNicoll retired from the RAN in 1968 and was appointed as the inaugural Australian Ambassador to Turkey. He served in the diplomatic post for five years, then retired to Canberra. McNicoll died in 1987 at the age of 79.
 Alan McNicoll was born in the Melbourne suburb of Hawthorn, Victoria, on 3 April 1908. He was the second of five sons of Walter McNicoll, a school teacher and Militia officer, and Hildur (née Wedel Jarlsberg).[1][2] The young McNicoll was of noble Norwegian descent through his mother.[2][3] He was initially educated at Scotch College, Melbourne,[4] before the family moved to Goulburn, from where he was sent to attend The Scots College in Sydney.[5] On 1 January 1922, at the age of thirteen, McNicoll entered the Royal Australian Naval College at Jervis Bay.[1][6][7] Described as "urbane and studious",[8] he performed well both academically and in sport, ultimately placing first in seamanship, history and English.[8] On graduation in 1926, McNicoll was posted to Britain for service and further training with the Royal Navy.[9]
 Advanced to acting sub-lieutenant in September 1928, McNicoll's appointment to the United Kingdom concluded the following year, at which point he returned to Australia and was initially posted to the land base HMAS Cerberus. He was attached to HMAS Penguin soon after, before being assigned for duties with HMAS Australia.[9] In his Lieutenants' Examinations in 1929, McNicoll achieved 1st Class Certificates in all of his subjects and was awarded a prize of ₤10 as a result.[8][9] He was promoted to lieutenant in July 1930, with seniority from 1 April that year. Completing a twelve-month posting aboard HMAS Canberra between 1932 and 1933, McNicoll decided to specialise as a torpedo officer and returned to the United Kingdom in order to undertake the long course at the Royal Naval College in Dartmouth.[9][10] While in the UK, McNicoll wrote and published Sea Voices, a book of poems centred on naval life.[5][10]
 McNicoll's detachment to the Royal Navy was terminated in 1935 on his graduation from Dartmouth, and he returned to Australia. Over the next three years, he saw service in HMAS Canberra, HMAS Sydney and Cerberus, advancing to lieutenant commander on 1 April 1938.[9] On 18 May 1937, McNicoll wed Ruth Timmins at St Stephen's Church of England at Brighton.[11][12][13] From March 1939, McNicoll was once again seconded to the Royal Navy, receiving a posting to the torpedo school HMS Vernon; he was serving in Vernon on the outbreak of the Second World War.[9] While residing at Portsmouth, McNicoll and his wife had their first child, a son named Ian, in June that year. Ian died when one week old.[12][13][14][15] The couple later had two more sons, Guy and Anthony, and a daughter, Deborah.[16]
 On 14 September 1939, eleven days after the outbreak of the Second World War, McNicoll was posted to HMS Victory, the flagship of Admiral Sir William James, the Commander-in-Chief, Portsmouth. In April 1940, McNicoll was transferred to the light cruiser HMS Fiji as a member of the ship's commissioning crew.[9] During McNicoll's time aboard the ship, it was severely damaged by a torpedo in operations on 1 September and barely made it back to harbour.[5] McNicoll ultimately served on Fiji for six months before being attached to HMS Medway, a submarine depot ship stationed at Alexandria, Egypt, in October 1940. In this post he was made torpedo officer of the 1st Submarine Flotilla operating in the Mediterranean theatre.[5][9][10] In addition to his standard duties, McNicoll was regularly involved in rendering safe captured enemy ordnance.[5] On one such occasion, he was tasked with disarming the captured Italian submarine Galileo Galilei, which entailed removing the inertia pistols from eight torpedoes that had badly corroded.[6][8][10] As a consequence of his "gallant and undaunted devotion to duty" in this action,[17] McNicoll was awarded the George Medal and presented a Commander-in-Chief's Commendation.[8] His decoration was promulgated in a supplement to the  London Gazette on 8 July 1941.[18]
 In April 1942, McNicoll transferred to the battleship HMS King George V and served as Squadron Torpedo Officer.[8][9][10] As part of the Home Fleet, King George V provided support to several Arctic convoys throughout the conflict. From April to May 1942, King George V formed up as a support component to Convoy PQ 15, the first for McNicoll. While sailing in thick fog on 1 May, King George V collided with the destroyer HMS Punjabi after the latter crossed under the bow of the battleship. Punjabi was sliced in two during the collision, and sank with heavy loss of life. Several depth charges were also ignited on the damaged stern of King George V during the accident. King George V was patched up at Seidisfjord, before sailing to Gladstone Dock, Liverpool, to receive repairs.[19]
 In December 1942, HMS King George V deployed in support of Convoy JW 51A, the first Russian convoy to sail direct from the United Kingdom without stopping at Iceland. The journey was completed without incident. On receiving word of the German naval attack on Convoy JW 51B in what became known as the Battle of the Barents Sea, King George V was dispatched along with nine other ships from Scapa Flow on 31 December to provide cover for the returning Convoy RA 51 and to attempt to catch the German ships engaged in the previous assault. The German ships were ultimately not encountered, and RA 51 was returned safely.[19] King George V later provided a covering force for two further convoys during early 1943, before being transferred to the Mediterranean during May in preparation for Operation Husky, the Allied invasion of Sicily.[19] Promoted to commander on 30 June 1943,[9] McNicoll took part in the Sicilian invasion the following month, with King George V serving as part of the covering force. Prior to the invasion, King George V, along with HMS Howe, had executed a bombardment of Trapani and the islands of Favignana and Levanzo on the night of 11/12 July, as part of a deception suggesting landings on the west coast of Sicily.[19]
 McNicoll was briefly reposted to HMS Victory on 1 September 1943, before being transferred for staff duties with the Admiralty in London the following month. He completed a year-long attachment with the Admiralty,[9] and was involved in the planning for the Normandy landings.[5] On 15 February 1944, he attended an investiture ceremony at Buckingham Palace, where he was formally presented his George Medal by King George VI.[17] McNicoll returned to Australia and was attached to the staff of HMAS Cerberus in October 1944; he spent the remainder of the war in this post.[8][9][10] Up to this period, McNicoll had completed all but five of his years of military service attached to the Royal Navy.[10] McNicoll's three brothers also served in the Second World War:[12][15] Ronald Ramsay, who ultimately retired with the rank of major general and served in the Korean War,[2][20] as a colonel with the Royal Australian Engineers;[21] Frederick Oscar Ramsay as a lieutenant in the Royal Australian Navy (RAN);[22] and David Ramsay—who would become an accomplished journalist—as a lieutenant in the 7th Division up to 1944,[23] before spending the remainder of the conflict as a war correspondent for Consolidated Press, in which capacity he covered the Normandy landings.[24]
 McNicoll was appointed executive officer of the light cruiser HMAS Hobart on 16 September 1945, a fortnight after the cessation of hostilities in the Pacific theatre.[8][9] From November 1945 until July 1947, Hobart spent nine months operating in Japanese waters over three distinct periods as part of the British Commonwealth Occupation Force. The ship was placed in reserve from December 1947,[25] and McNicoll briefly transferred to HMAS Penguin before assuming the post of Director of Plans and Operations at the Navy Office in Melbourne on 6 January 1948.[8][9][10] Advanced to captain in June 1949, he was posted two months later to HMAS Shoalhaven, a River-class frigate, as the ship's commanding officer, and was simultaneously placed in charge of the 1st Frigate Squadron. He was appointed an honorary aide-de-camp to the Governor-General of Australia in December for a period of three years. In January 1950, McNicoll transferred to command the destroyer HMAS Warramunga and was subsequently made Captain (D) in control of the 10th Destroyer Squadron.[5][8][9]
 During McNicoll's tenure as commanding officer of Warramunga, the ship operated in Australian waters as part of the Australia Station, sailing to New Zealand for a visit during March 1950.[26] On the outbreak of the Korean War in June that year, Warramunga was selected as part of the Australian contribution to the conflict. Moreover, the ship was to be attached to a force of five Royal Navy destroyers led by a captain, making it expedient to have the Australian ship commanded by an officer of lower rank; McNicoll was consequently replaced by Commander Otto Becher on 28 July.[27] McNicoll was then posted to the Navy Office to assist in the introduction and co-ordination of National Service in the Australian military in response to the National Service Act 1951. He moved to the land base HMAS Lonsdale in October 1951, on being made Deputy Chief of Naval Staff.[9]
 In 1952, McNicoll was appointed chairman of the planning committee for the British nuclear tests on the Montebello Islands, off the coast of Western Australia.[5][10] Later that year, he was made commanding officer of the heavy cruiser HMAS Australia, a post he held for the next two years.[9][10] As commander of Australia, McNicoll also served as Chief Staff Officer to the Flag Officer Commanding HM Australian Fleet.[8] HMAS Australia was near the end of its naval service and had been relegated to training duties from 1950. As such, the cruiser was primarily consigned to Australian waters, though a brief trip to New Zealand did occur in October 1953.[28] McNicoll was appointed a Commander of the Order of the British Empire in the 1954 New Year Honours List for his involvement in the British atomic program;[5][10][29] he was presented with the decoration three months later by Queen Elizabeth II in a ceremony at Government House, Melbourne.[16]
 The year of 1954 was to be HMAS Australia's last in service, with the ship conducting Royal and Vice Regal tasks as some of its final duties. In February and March, HMAS Australia served as part of the escort for the Royal Yacht Gothic during the Australian leg of Queen Elizabeth II's coronation tour.[28] The cruiser was presented with the Gloucester Cup on 25 March as the ship "considered to be foremost in general efficiency, cleanliness, seamanship and technical training" during the year of 1953.[28][30] As one of the ship's final duties with the Navy, Australia was tasked with transporting Field Marshal Sir William Slim, the Governor-General of Australia, along with his wife and their staff on a cruise around the Coral Sea, the Great Barrier Reef and the Whitsunday Passage. The voyage embarked on 4 May, and two days later Australia fired its 8-inch guns for the final time.[28] While in the Coral Sea, a Dutch naval ship was discovered to be incapacitated off the coast of Hollandia, Netherlands New Guinea, and was consequently towed by Australia to Cairns. McNicoll was later appointed a Commander of the Order of Orange-Nassau by the Dutch government for his rescue of the ship.[5][28][31]
 McNicoll relinquished command of HMAS Australia in July 1954 before the cruiser was paid off and marked for disposal the following month, and he briefly returned to duties at the Navy Office.[9][28] In November, he embarked for London to attend the Imperial Defence College as part of the 1955 course intake, which signified that he had been marked for senior command.[5][9] McNicoll and his wife, Ruth, had separated in 1950 and their divorce, which cited adultery as the cause, was finalised in October 1956, while the former was still in London. On 17 May the following year, McNicoll wed Frances Mary Chadwick, a journalist, in the Hampstead register office.[4][5] Made acting rear admiral in January 1957, McNicoll was appointed as Head of the Australian Joint Service Staff in London.[5][9][10] He returned to Australia in February 1958 and was selected to serve as Deputy Secretary (Military) at the Department of Defence; McNicoll's rank was made substantive in July that year.[4][5][9]
 On 8 January 1960, McNicoll was posted to the Naval Board in Canberra as Second Naval Member and Chief of Personnel.[8][9] As noted by historian Ian Pfennigwerth, McNicoll held this position at a time in which recruitment and retention in the Navy particularly lagged behind targets.[5] McNicoll was additionally appointed as a trustee of the RAN Relief Trust Fund during this period. Completing his term on the Naval Board, McNicoll was posted as Flag Officer Commanding HM Australian Fleet on 8 January 1962 and hoisted his standard aboard HMAS Melbourne, the flagship of the RAN.[4][9] The Australian government had designated the role of the RAN to be primarily one of anti-submarine warfare, a posture which McNicoll thought unwise. McNicoll argued that surface and air weapons posed a threat equal to that of submarines toward vessels in modern naval warfare. As such, he campaigned for a contemporary aircraft carrier to replace that of HMAS Melbourne. The Army and Air Force opposed McNicoll's stance, and the government ultimately concluded that there was no strategic requirement for a new carrier in light of agreements contained in the Southeast Asia Treaty Organization.[32] In any event, McNicoll experienced a particularly demanding tenure as Fleet Commander since the RAN was in the process of a complete overhaul of its order of battle and, as a consequence, he had to manage the introduction and deployment into service of six Ton-class minesweepers acquired from the Royal Navy, along with the first batch of Westland Wessex helicopters and modernised afloat support capabilities. Furthermore, McNicoll was charged with the responsibility of ensuring Australian naval commitments to the Far East Strategic Reserve were met.[5][33]
 McNicoll's two-year term as Fleet Commander concluded on 6 January 1964, at which point he returned to the Naval Board as Fourth Naval Member and Chief of Supply. However, this post proved short-lived with his appointment as Flag Officer-in-Charge East Australia Area, headquartered at the land base HMAS Kuttabul in Sydney, from June that year.[5][8][9] In the 1965 New Year Honours, McNicoll was appointed a Companion of the Order of the Bath.[34]
 On 24 February 1965, McNicoll was promoted vice admiral and made Chief of Naval Staff (CNS) in succession to Vice Admiral Sir Hastings Harrington.[5][8][9] By virtue of this position, McNicoll was head of the Naval Board and the functional commander of the RAN.[35] McNicoll's term as CNS was characterised by a period of heightened activity for the RAN in light of the Australian commitments to the Indonesia–Malaysia Konfrontasi and the Vietnam War. He furthermore had to oversee an extensive modernisation of the fleet, with the introduction into service of the Perth-class destroyers, Attack-class patrol boats, and the initial batch of Oberon-class submarines. The Fleet Air Arm was also re-equipped with American fixed wing aircraft.[5][10] Despite these acquisitions, the RAN possessed a rather thin and limited fleet during this period, which McNicoll blamed on past naval planning. He criticised the lack of foresight in earlier decisions that had led to "inconsistencies and inadequate estimating" in the future needs of the RAN, which had consequently left the fleet outdated and minimal.[36] In addition to the RAN's materiel issues, McNicoll faced significant problems with morale and recruitment. A series of mishaps and accidents over the previous decade led to what naval historian Tom Frame termed as "an appreciable erosion of public confidence in the navy's professional standards".[37] The situation intensified following the February 1964 collision between HMAS Melbourne and Voyager. The two subsequent Royal Commissions into the incident subjected the RAN to unprecedented scrutiny and damaged the public perception of its senior leadership.[5][38] McNicoll had to cope with the turmoil occasioned by these events and concerned himself with the restoration of morale in the Navy.[10][39]
 The tenure of Air Chief Marshal Sir Frederick Scherger as Chairman, Chiefs of Staff Committee was set to expire in May 1966, and a replacement had to be selected from the service chiefs.[40] The Chief of the General Staff, Lieutenant General Sir John Wilton, had among defence and military circles been assumed to be the natural successor. However, mounting speculation arose from late 1965 over who was to be selected for the position as it became known Prime Minister Sir Robert Menzies preferred McNicoll for the post, as did Secretary of the Department of Defence Sir Ted Hicks, who thought McNicoll more intelligent and objective than his army counterpart.[41] McNicoll lobbied ardently for the position, and was supported by his wife, Frances, who actively campaigned on her husband's behalf. By December 1965, Scherger's replacement had still not been decided upon and Menzies chose to delay the decision until the new year. However, Menzies retired in January 1966 and was succeeded by his deputy, Harold Holt. Holt and the newly appointed Minister for Defence, Allen Fairhall, preferred Wilton and ultimately selected him to succeed Scherger.[42] In any event, McNicoll was created a Knight Commander of the Order of the British Empire in the 1966 New Year Honours for his service as CNS.[43]
 McNicoll was eager for a RAN contribution to the Vietnam War and, in July 1966, proposed that the four Australian minesweepers operating out of Singapore be deployed to Vietnamese waters since Konfrontasi was at an end and the vessels were no longer necessary in that area. The notion was rejected by Fairhall, however, who was conscious of an upcoming election and was adamant that nothing be decided until afterward.[44][45] The possibility of a naval contribution to Vietnam was raised again in December, and it was decided that the guided missile destroyer HMAS Hobart and a clearance diving team of six personnel be deployed as the Royal Australian Navy Force Vietnam.[46][47] Per an agreement between McNicoll and Admiral Roy L. Johnson, Commander of the United States Pacific Fleet, HMAS Hobart was to be attached to the United States Seventh Fleet and conduct shore bombardment operations.[47] The deployment of an Australian destroyer to Vietnam became permanent, with the ships operating on a six-month rotation.[48] To McNicoll's satisfaction, the RAN contribution to the theatre was further bolstered in 1967 with the formation of the RAN Helicopter Flight Vietnam and the dispatch of naval aviators to serve in an Army support role with No. 9 Squadron RAAF.[49][50]
 The visible legacy of McNicoll's service as CNS is the Australian White Ensign.[5][8][10] The British White Ensign had been flown by Australian vessels since the formation of the RAN in 1911, but the Australian contribution to Vietnam—a conflict in which the United Kingdom was not involved—served to complicate the situation. Federal politician Sam Benson questioned the Australian use of the British ensign before parliament in October 1965, and McNicoll later raised the issue with the Naval Board. The Naval Board ultimately decided to recommend to the government that the RAN create its own unique white ensign. A design accompanied the recommendation, which described the ensign as a "white flag with the Union Flag in the upper canton at the hoist with six blue stars positioned as in the Australian flag".[51] The government approved the proposal, and the Australian White Ensign was formally introduced throughout the RAN on 1 March 1967.[51]
 After 46 years of service, McNicoll retired from the RAN on 2 April 1968 and was succeeded as CNS by Vice Admiral Victor Smith.[8][9] In the lead-up to his retirement, McNicoll completed a farewell tour by visiting several ships and naval establishments throughout Australia. The trip culminated with a two-week visit to Vietnam, and McNicoll was present in Saigon when the city was attacked by Viet Cong forces as part of the Tet Offensive.[8][52][53] As a man who "liked action", McNicoll later stated that he received a "great thrill" during the assault as he awaited transportation back to Australia.[8]
 On his retirement from the Navy, McNicoll was appointed by the Australian government as its inaugural ambassador to Turkey.[5][10][54] He was able to form amiable relations between the governments of Australia and Turkey, despite the physical and logistic issues associated with the establishment of a new embassy and the lack of knowledge both nations had of one another.[5] McNicoll held his diplomatic post in Ankara for five years, before he returned to Australia in 1973 and retired to Canberra.[4] A man of "culture and refined literary tastes",[39] McNicoll engaged his passion for the arts during retirement and in 1979 published his translation of The Odes of Horace.[4][5] He was also a music lover and a keen fly-fisherman.[4]
 Sir Alan McNicoll died on 11 October 1987 at the age of 79.[5] Remembered as a "well-informed, hard working and skilled administrator",[39] he was cremated with full naval honours. McNicoll was survived by his wife, and by the children from his first marriage.[5]
 Battle of the Mediterranean Arctic convoys Italian Campaign Operation Husky 1 Early life and career 2 Second World War 3 Senior command

3.1 Ships' captain
3.2 Rise to Chief of Naval Staff
3.3 Chief of Naval Staff

 3.1 Ships' captain 3.2 Rise to Chief of Naval Staff 3.3 Chief of Naval Staff 4 Ambassador and later life 5 Notes 6 References ^ a b "McNicoll, Alan Wedel Ramsay". World War II Nominal Roll. Commonwealth of Australia. Retrieved 4 May 2013..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c McNicoll, Ronald (1986). "McNicoll, Sir Walter Ramsay (1877–1947)". Australian Dictionary of Biography. Canberra: Australian National University. Retrieved 4 May 2013.
 ^ "Lady McNicoll: Life in Rabaul". Sydney Morning Herald. 15 December 1937. Retrieved 15 July 2013.
 ^ a b c d e f g Draper 1985, p. 572
 ^ a b c d e f g h i j k l m n o p q r s t u v w x y z Pfennigwerth, Ian (2012). "McNicoll, Sir Alan Wedel Ramsay (1908–1987)". Australian Dictionary of Biography. Canberra: Australian National University. Retrieved 4 May 2013.
 ^ a b Gill 1968, p. 716
 ^ Cunningham 1988, p. 123
 ^ a b c d e f g h i j k l m n o p q r "Vice Admiral McNicoll Ends Long Career" (PDF). Royal Australian Navy News. Royal Australian Navy. 12 April 1968. Retrieved 4 May 2013.
 ^ a b c d e f g h i j k l m n o p q r s t u v w x y z "McNicoll, Alan Wedel Ramsay". Records Search. National Archives of Australia. Retrieved 4 May 2013.
 ^ a b c d e f g h i j k l m n o p Dennis, Peter, Jeffrey Grey, Ewan Morris, Robin Prior and Jean Bou. "McNicoll, Vice-Admiral Sir Allan Wedel Ramsay". The Oxford Companion to Australian Military History. Oxford Reference Online. Retrieved 4 May 2013.CS1 maint: uses authors parameter (link)
 ^ "Wedding in Melbourne". Sydney Morning Herald. 19 May 1937. Retrieved 4 May 2013.
 ^ a b c "George Medal for Lt. Cdr. A. McNicoll". The Argus. 10 July 1941. Retrieved 4 May 2013.
 ^ a b "15 Couples Divorced". The Argus. 18 October 1956. Retrieved 4 May 2013.
 ^ "General Cable News". Sydney Morning Herald. 28 June 1939. Retrieved 4 May 2013.
 ^ a b "Lieut. Cdr McNicoll Honoured". Sydney Morning Herald. 10 July 1941. Retrieved 4 May 2013.
 ^ a b Irving, Freda (5 March 1954). "Animated Queen at Investiture: 180 Received Special Smile". The Argus. Retrieved 4 May 2013.
 ^ a b "Recommendation for Alan Wedel Ramsay McNicoll to be awarded the George Medal" (PDF). Honours and Awards. Australian War Memorial. Archived from the original (PDF) on 25 April 2016. Retrieved 4 May 2013.
 ^ "No. 35212". The London Gazette (Supplement). 8 July 1941. p. 3915.
 ^ a b c d "HMS King George V". Service Histories of Royal Navy Warships in World War II. Naval History. Archived from the original on 19 May 2013. Retrieved 4 May 2013.
 ^ "McNicoll, Ronald Ramsay". Korean War Nominal Roll. Commonwealth of Australia. Retrieved 4 May 2013.
 ^ "McNicoll, Ronald Ramsay". World War II Nominal Roll. Commonwealth of Australia. Retrieved 4 May 2013.
 ^ "McNicoll, Frederick Oscar Ramsay". World War II Nominal Roll. Commonwealth of Australia. Retrieved 4 May 2013.
 ^ "McNicoll, David Ramsay". World War II Nominal Roll. Commonwealth of Australia. Retrieved 4 May 2013.
 ^ "David McNicoll". Fifty Australians. Australian War Memorial. Retrieved 4 May 2013.
 ^ "HMAS Hobart (I)". Ship Histories. Royal Australian Navy. Retrieved 4 May 2013.
 ^ "HMAS Warramunga (I)". Ship Histories. Royal Australian Navy. Retrieved 4 May 2013.
 ^ O'Neill 1985, p. 420
 ^ a b c d e f "HMAS Australia (II)". Ship Histories. Royal Australian Navy. Retrieved 4 May 2013.
 ^ "No. 40054". The London Gazette (Supplement). 1 January 1954. p. 39.
 ^ "H.M.A.S. Australia Wins Cup". Sydney Morning Herald. 25 March 1954. p. 7. Retrieved 4 May 2013.
 ^ "Alan Wedel Ramsay McNicoll awarded Dutch Order of Orange-Nassau". Honours and Awards. Australian War Memorial. Archived from the original (PDF) on 27 June 2013. Retrieved 4 May 2013.
 ^ Grey 1998, pp. 17–18
 ^ Frame 2004, pp. 221–222
 ^ "No. 43530". The London Gazette (Supplement). 1 January 1965. p. 37.
 ^ Horner 2005, p. 257
 ^ Grey 1998, p. 321
 ^ Frame 2004, p. 222
 ^ Frame 2004, pp. 222–223
 ^ a b c Horner 2005, p. 264
 ^ Horner 2005, p. 263
 ^ Horner 2005, pp. 264–265
 ^ Horner 2005, p. 266
 ^ "No. 43855". The London Gazette (Supplement). 1 January 1966. p. 37.
 ^ Grey 1998, p. 78
 ^ Horner 2005, p. 274
 ^ Horner 2005, p. 276
 ^ a b Grey 1998, pp. 139–140
 ^ Frame 2004, p. 231
 ^ Frame 2004, p. 236
 ^ Grey 1998, p. 84
 ^ a b "Australian White Ensign". History of the RAN. Royal Australian Navy. Retrieved 4 May 2013.
 ^ "McNicoll, Alan Wedel Ramsay". Vietnam War Nominal Roll. Department of Veterans Affairs. Retrieved 4 May 2013.
 ^ Horner 2005, p. 287
 ^ "New Envoys". The Canberra Times. 8 June 1968. Retrieved 4 May 2013.
 Cunningham, Ian (1988). Work Hard Play Hard: The Royal Australian Naval College, 1913–1988. Canberra, Australia: Australian Government Publishing Service. ISBN 0-644-08303-4. Dennis, Peter; Grey, Jeffrey; Morris, Ewan; Prior, Robin; Bou, Jean (2008). The Oxford Companion to Australian Military History (2nd ed.). Melbourne, Australia: Oxford University Press. ISBN 978-0-19-551784-2. Draper, W.J. (1985). Who's Who in Australia 1985. Melbourne, Australia: The Herald and Weekly Times. Frame, Tom (2004). No Pleasure Cruise: The Story of the Royal Australian Navy. Crows Nest, Australia: Allen & Unwin. ISBN 1-74114-233-4. Gill, George Hermon (1968). Royal Australian Navy, 1942–1945 (PDF). Australia in the War of 1939–1945. Series 2 – Navy. Volume 2. Canberra, Australia: Australian War Memorial. Retrieved 4 May 2013. Grey, Jeffrey (1998). Up Top – The Royal Australian Navy and Southeast Asian Conflicts 1955–1972. The Official History of Australia's Involvement in Southeast Asian Conflicts 1948–1975. Volume Seven. St Leonards, Australia: Allen & Unwin. ISBN 1-86448-290-7. Horner, David (2005). Strategic Command: General Sir John Wilton and Australia's Asian Wars. Oxford, United Kingdom: Oxford University Press. ISBN 0-19-555282-2. McNicoll, Ronald (1986). "McNicoll, Sir Walter Ramsay (1877–1947)". Australian Dictionary of Biography. Volume 10. Melbourne, Australia: Melbourne University Press. ISBN 978-0-522-84327-9. O'Neill, Robert (1985). Combat Operations. Australia in the Korean War 1950–53. Volume 2. Canberra, Australia: Australian War Memorial & Australian Government Publishing Service. Pfennigwerth, Ian (2012). "McNicoll, Sir Alan Wedel Ramsay (1908–1987)". Australian Dictionary of Biography. Volume 18. Melbourne, Australia: Melbourne University Press. ISBN 978-0-522-86131-0. ADB: mcnicoll-sir-alan-wedel-ramsay-14934 ISNI: 0000 0001 2024 7586 LCCN: n80045481 NLA: 35393879 Trove: 512019 VIAF: 43134001  WorldCat Identities (via VIAF): 43134001 v t e Sir William Rooke Creswell Sir William Rooke Creswell Sir Percy Grant Sir Allan Everett Percival Hall-Thompson William Napier Sir William Munro Kerr Sir George Hyde Sir Ragnar Colvin Sir Guy Royle Sir Louis Keppel Hamilton Sir John Augustine Collins Sir Roy Dowling Sir Henry Burrell Sir Hastings Harrington Sir Alan McNicoll Sir Victor Smith Sir Richard Peek Sir David Stevenson Sir Anthony Synnot Sir James Willis David Leach Michael Hudson Ian MacDougall Rodney Taylor Rodney Taylor Donald Chalmers David Shackleton Chris Ritchie Russ Shalders Russell Crane Raymond Griggs Timothy Barrett Michael Noonan v t e Sir George Patey Sir William Pakenham Arthur Leveson Sir Lionel Halsey John Dumaresq Albert Addison Thomas Wardle George Hyde Edward Evans Leonard Holbrook Robin Dalglish Wilbraham Ford Richard Lane-Poole Wilfred Custance Wilfrid Patterson John Crace Victor Crutchley John Collins Charles Nichols Harold Farncomb John Collins Harold Farncomb John Eccles John Eaton Roy Dowling Henry Burrell David Harries Henry Burrell Galfry Gatacre Hastings Harrington Alan McNicoll Otto Becher Thomas Morrison Victor Smith Richard Peek Gordon Crabb David Stevenson William Dovers David Stevenson William Dovers Anthony Synnot David Wells Geoffrey Gladstone Neil McDonald James Willis David Leach Peter Doyle John Stevens Michael Hudson Geoffrey Woolrych Ian Knox Peter Sinclair Peter Sinclair Ian MacDougall Ken Doolan Robert Walls Donald Chalmers Chris Oxenbould Chris Ritchie John Lord Geoffrey Smith Raydon Gates Rowan Moffitt Davyd Thomas Davyd Thomas Nigel Coates Steve Gilmore Tim Barrett Stuart Mayer Jonathan Mead 1908 births 1987 deaths 20th-century Australian poets Australian male poets Ambassadors of Australia to Turkey Australian military personnel of World War II Australian Companions of the Order of the Bath Australian Knights Commander of the Order of the British Empire Australian military personnel of the Indonesia–Malaysia confrontation Australian people of Norwegian descent Australian recipients of the George Medal Chiefs of Naval Staff (Australia) Commanders of the Order of Orange-Nassau Deputy Chiefs of Naval Staff (Australia) People educated at Scotch College, Melbourne People educated at The Scots College People from Melbourne Royal Australian Navy admirals 20th-century Australian male writers CS1 maint: uses authors parameter Pages containing London Gazette template with parameter supp set to y Featured articles Use Australian English from February 2014 All Wikipedia articles written in Australian English Use dmy dates from May 2013 CS1: long volume value Wikipedia articles with ADB identifiers Wikipedia articles with ISNI identifiers Wikipedia articles with LCCN identifiers Wikipedia articles with NLA identifiers Wikipedia articles with Trove identifiers Wikipedia articles with VIAF identifiers Wikipedia articles with WorldCat-VIAF identifiers Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Français Српски / srpski  This page was last edited on 26 October 2019, at 06:06 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Sir Alan Wedel Ramsay McNicoll a b a b c ^ a b c d e f g a b c d e f g h i j k l m n o p q r s t u v w x y z a b ^ a b c d e f g h i j k l m n o p q r a b c d e f g h i j k l m n o p q r s t u v w x y z a b c d e f g h i j k l m n o p ^ a b c a b ^ a b a b a b ^ a b c d ^ ^ ^ ^ ^ ^ ^ ^ a b c d e f ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b c ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ a b ^ ^ ^  Chief of Naval Staff  Flag Officer-in-Charge East Australia Area  Flag Officer Commanding HM Australian Fleet  Deputy Chief of Naval Staff New title  Australian Ambassador to Turkey Commander Alan McNicoll c. mid-1940s (1908-04-03)3 April 1908Hawthorn, Victoria 11 October 1987(1987-10-11) (aged 79)Canberra, Australian Capital Territory Australia Royal Australian Navy 1922–1968 Vice Admiral Chief of Naval Staff (1965–68)HM Australian Fleet (1962–64)HMAS Australia (1952–54)Deputy Chief of Naval Staff (1951–52)HMAS Warramunga (1950)HMAS Shoalhaven (1949–50) Second World War
Battle of the Mediterranean
Arctic convoys
Italian Campaign
Operation Husky
Indonesia–Malaysia confrontationVietnam War Knight Commander of the Order of the British EmpireCompanion of the Order of the BathGeorge MedalCommander of the Order of Orange-Nassau (Netherlands) Brigadier General Sir Walter McNicoll (father)Major General Ronald McNicoll (brother) Australian Ambassador to Turkey (1968–73) Preceded byVice Admiral Sir Hastings Harrington
  Chief of Naval Staff1965–1968
 Succeeded byVice Admiral Sir Victor Smith
 Preceded byRear Admiral Galfry Gatacre
  Flag Officer-in-Charge East Australia Area1964–1965
 Succeeded byRear Admiral Otto Becher
 Preceded byRear Admiral Hastings Harrington
  Flag Officer Commanding HM Australian Fleet1962–1964
 Succeeded byRear Admiral Otto Becher
 Preceded byCaptain Galfry Gatacre
  Deputy Chief of Naval Staff1951–1952
 Succeeded byCaptain Otto Becher
 New titleNew position
  Australian Ambassador to Turkey1968–1973
 Succeeded byJohn McMillan
 
ADB: mcnicoll-sir-alan-wedel-ramsay-14934
ISNI: 0000 0001 2024 7586
LCCN: n80045481
NLA: 35393879
Trove: 512019
VIAF: 43134001
 WorldCat Identities (via VIAF): 43134001
 
Sir William Rooke Creswell
  
Sir William Rooke Creswell
Sir Percy Grant
Sir Allan Everett
Percival Hall-Thompson
William Napier
Sir William Munro Kerr
Sir George Hyde
Sir Ragnar Colvin
Sir Guy Royle
Sir Louis Keppel Hamilton
Sir John Augustine Collins
Sir Roy Dowling
Sir Henry Burrell
Sir Hastings Harrington
Sir Alan McNicoll
Sir Victor Smith
Sir Richard Peek
Sir David Stevenson
 
Sir Anthony Synnot
Sir James Willis
David Leach
Michael Hudson
Ian MacDougall
Rodney Taylor
 
Rodney Taylor
Donald Chalmers
David Shackleton
Chris Ritchie
Russ Shalders
Russell Crane
Raymond Griggs
Timothy Barrett
Michael Noonan
 
Sir George Patey
Sir William Pakenham
Arthur Leveson
Sir Lionel Halsey
John Dumaresq
Albert Addison
Thomas Wardle
  
George Hyde
Edward Evans
Leonard Holbrook
Robin Dalglish
Wilbraham Ford
Richard Lane-Poole
Wilfred Custance
Wilfrid Patterson
John Crace
Victor Crutchley
John Collins
Charles Nichols
Harold Farncomb
John Collins
Harold Farncomb
 
John Eccles
John Eaton
Roy Dowling
Henry Burrell
David Harries
Henry Burrell
Galfry Gatacre
Hastings Harrington
Alan McNicoll
Otto Becher
Thomas Morrison
Victor Smith
Richard Peek
Gordon Crabb
David Stevenson
William Dovers
David Stevenson
William Dovers
Anthony Synnot
David Wells
Geoffrey Gladstone
Neil McDonald
James Willis
David Leach
Peter Doyle
John Stevens
Michael Hudson
Geoffrey Woolrych
Ian Knox
Peter Sinclair
 
Peter Sinclair
Ian MacDougall
Ken Doolan
Robert Walls
Donald Chalmers
Chris Oxenbould
Chris Ritchie
John Lord
Geoffrey Smith
Raydon Gates
Rowan Moffitt
Davyd Thomas
 
Davyd Thomas
Nigel Coates
Steve Gilmore
Tim Barrett
Stuart Mayer
Jonathan Mead
 