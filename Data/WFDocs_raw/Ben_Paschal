Benjamin Edwin Paschal (October 13, 1895 – November 10, 1974) was an American baseball outfielder who played eight seasons in Major League Baseball from 1915 to 1929, mostly for the New York Yankees. After two "cup of coffee" stints with the Cleveland Indians in 1915 and the Boston Red Sox in 1920, Paschal spent most of his career as the fourth outfielder and right-handed pinch hitter of the Yankees' Murderers' Row championship teams of the late 1920s. Paschal is best known for hitting .360 in the 1925 season while standing in for Babe Ruth, who missed the first 40 games with a stomach ailment.
 During his time in baseball, Paschal was described as a five-tool player who excelled at running, throwing, fielding, hitting for average, and power.[1] However, his playing time with the Yankees was limited because they already had future Baseball Hall of Famers Ruth and Earle Combs, and star Bob Meusel, in the outfield. Paschal was considered one of the best bench players in baseball during his time with the Yankees, and sportswriters wrote how he would have started for most other teams in the American League.[1] He was one of the best pinch hitters in the game during the period, at a time when the term was still relatively new to baseball.[2][3]
 The son of farmers, Paschal was born in Enterprise, Alabama, and grew up in nearby Sanford. 
 He played collegiate sports at the University of Alabama,[4] before beginning his professional career with Dothan of the Georgia State League, where he played with future Hall of Fame player Bill Terry.[5][6] Paschal played in 64 games, with a .280 batting average,[6] and his ability attracted the attention of scouts in the area. 
 Signed as a pinch hitter for the Cleveland Indians at age 19, Paschal appeared in nine games, collecting one hit on August 16, which broke up a no-hitter by Bernie Boland with two outs in the ninth inning.[7][8] The Indians declared Paschal too inexperienced, and he was sent to the Muskegon Reds of the Central League. The league disbanded in the middle of the 1917 season, and Paschal became a free agent.[9]
 After a two-year break from baseball because of World War I, Paschal moved on to the Charlotte Hornets of the South Atlantic League, where he played from 1920 to 1923.[5][9] He finished third in the league in batting average in 1920.[9] While in the Southern League, he was nicknamed "the man who hits sticks of dynamite".[4]
 At the conclusion of the 1920 season, Paschal's contract was purchased by the Boston Red Sox, with an option to keep him if he met certain playing expectations.[9] He appeared in nine games for the Red Sox; his first game brought three hits against pitcher José Acosta of the Washington Senators, and in total he batted .357 with five runs batted in (RBI), but the Red Sox believed he lacked fielding experience and he returned to Charlotte.[7][9] In August 1921, Paschal was sold to the Rochester Red Wings. However, while sliding in a game on August 20, 1921, he suffered a broken leg which sidelined him for the rest of the season and voided the contract with the Red Wings.[10] He was hitting .317 at the time of the injury.[11] In 1922, Paschal played in 142 games, hitting .326 with 18 home runs and improved these figures in 1923, achieving 200 hits, 22 triples, and 26 home runs in 141 games for a batting average of .351, the fourth best in the league.[12]  Paschal began the 1924 season with the Atlanta Crackers of the Southern Association. He scored 136 runs, while batting .341 and stealing 24 bases.[5]
 The New York Yankees bought Paschal from the Crackers near the end of the 1924 season and he played in four games. His only three hits, as well as three RBI, came in a defeat by the Detroit Tigers on September 19.[13]
 During spring training, Paschal narrowly escaped serious injury while traveling on a bus. The vehicle rolled backwards down a hill and Paschal, along with several other teammates, jumped off before it hit a tree at high speed.[14] The media expected Paschal to be Babe Ruth's understudy prior to the 1925 season,[15] but Ruth collapsed at an Asheville, North Carolina, train station just before the regular season's start. Emergency surgery for a "intestinal abscess" left him hospitalized for six weeks.[16] Originally, Paschal was only to be used against left-handed pitchers, but Yankees manager Miller Huggins named him as Ruth's temporary replacement in the outfield.[17] In the first game of the year, Paschal hit a home run in a 5–1 win against the defending World Series-champion Washington Senators.[18] After another game-winning home run against the Senators two weeks later, the New York press noted that he was "making fans forget about Babe Ruth".[19] Paschal's weakness against right-handed pitchers prompted the Yankees to acquire veteran outfielder Bobby Veach,[20] but his declining skills allowed Paschal to retain his position on the team. He hit another game-winning home run against the Cleveland Indians on May 23.[21] At the time, Paschal was fifth in the league in batting average at .403, behind Sammy Hale, Ty Cobb, Tris Speaker, and teammate Earle Combs.[22] His six home runs in May set a Yankee rookie record for a month, later equaled by Joe Gordon, then topped by Shane Spencer's nine in September 1998.[23]
 Ruth returned to the lineup on June 1, relegating Paschal to the bench. In July, an injury to Combs allowed Paschal to start several games in center field.[24] He then started the majority of August and all of September when Bob Meusel moved to third base to cover for an injured Joe Dugan.[25][26] He hit two home runs during a September 8 game against the Red Sox,[27] but his season ended when he was hit on the leg with a pitch on September 12 against the Philadelphia Athletics.[28] In 89 games, Paschal's batting average for the season was .360, 70 points higher than Ruth, with 12 home runs and 56 RBI.[16]
 Paschal was set to enter the 1926 season as the fourth outfielder, for which he was sent a new contract.  After threatening to hold out for more money,[29] the Yankees sent him a new contract which he signed on February 17 for an estimated $7,000 (99,000 today).[29] He began the season as a pinch hitter, but injuries quickly took their toll on the Yankees.[30] Paschal started most of July and August, replacing an injured Meusel, who broke a bone in his right foot.[31] Paschal hit an inside-the-park home run in a victory against the Indians on July 9.[32] With the Yankees in a close pennant race in mid-August, Paschal hit a home run in a loss to the Detroit Tigers.[33] Further successes came with a vital pinch-hit double in a win against the Athletics on September 6 and a home run on September 8.[34][35] The Yankees clinched the pennant on September 15, and Paschal scored the game-winning single.[36] The Yankees faced the St. Louis Cardinals in the 1926 World Series, and Paschal, pinch hitting for Joe Dugan, singled in Lou Gehrig tying the contest at 2–2 in the ninth inning of Game 5.[37] Tony Lazzeri hit a sacrifice fly in the tenth to win the game for the Yankees, but they lost the next two games and the Series.[37] He had played in 96 games, hitting 7 home runs with 32 runs batted in.[7]
 Before the 1927 season, Paschal returned his playing contract unsigned because of a salary dispute.[38] By that time, the Yankees were forming the nucleus of what became the Murderers' Row teams of the late 1920s. He signed for an estimated $8,000 ($115,000 today), a 13% raise.[37] In the season-opening win against the Athletics, right field starter Babe Ruth struck out twice and popped out, forcing Huggins to replace him with Paschal in the sixth inning.[39] As the last man ever to pinch-hit for Ruth, Paschal singled.[40] In one of his few starts of the 1927 season, Paschal was a single short of hitting for the cycle, and almost had three home runs.[41] Replacing the injured Bob Meusel, Paschal hit two home runs, a triple that was yards shy of a home run, and a double which bounced off the right field stands during an 11–2 rout of the Indians.[42] Paschal did not play in the Yankees' 1927 World Series victory over the Pittsburgh Pirates. Overall, he played in 50 games, primarily as a pinch hitter.[7] After the season, Paschal was discussed as a trade for Boston Red Sox pitcher Red Ruffing, but discussions fell apart (Ruffing was later acquired in a proposed trade during the 1930 season).[43][44]
 Paschal was used heavily as a pinch hitter during the 1928 season. Huggins credited Paschal's timely pinch hitting as part of the Yankees' success that season.[45] One of the few highlights of his season was his RBI pinch-hit double in the 10th inning that helped the Yankees beat the Chicago White Sox on August 4.[46] Paschal played in 65 games that season, having a .316 batting average.[7] He shared center field duties with Cedric Durst for an injured Earle Combs during the Yankees' win over the Cardinals in the 1928 World Series.[47] He started the first and last games of the series on a platoon situation; Paschal faced left-handed pitchers and Durst faced right-handed pitchers.[48]
 Before the 1929 season, Paschal and Durst were mentioned in several trade rumors,[49] and Paschal was rarely used, appearing in only 42 games as a sixth outfielder in the season.[7] A rare start came on June 1 against the White Sox, when he scored a run.[50] On July 2, Paschal hit a pinch-hit home run for Herb Pennock in the seventh inning of a game against the Red Sox to give the Yankees a 3–2 win.[51] He played in 42 games in his final season in the majors, posting a .208 batting average in 81 at-bats.[7]
 During his time with the Yankees, Paschal was considered a quiet player with a colorless personality.[5] His appearances were limited by the presence of future Hall of Famers Ruth and Combs, and star Bob Meusel in the outfield. He was part a group including Lou Gehrig and Mark Koenig which preferred watching a film to carousing after a game; they were dubbed the team's "movie crowd".[52]
 After the 1929 season, Paschal was, along with Wilcy Moore and Johnny Grabowski, part of a trade for catcher Bubbles Hargrave to the St. Paul Saints of the American Association (AA).[53] In one 1930 game against the Toledo Mud Hens, Paschal had four hits and four RBI in a 23–4 win that broke the AA record for most runs scored in a game.[54] In 144 games, Paschal finished the 1930 season with 204 hits, 10 home runs, and a .350 batting average.[11] The following season, Paschal played 121 games to hit .336,[11] while his average in 1932 was .325 in 147 games.[11] During one game in the 1932 season, Paschal had three doubles and three singles, tying the AA record for most hits in a game.[55] His skills declined during the 1933 season; in 130 games he hit just .272 with seven home runs.[11] He left St. Paul and signed as a free agent with the Knoxville Smokies on December 30, 1933.[56] The St. Petersburg Evening Independent reported a few months later that Paschal was "struggling to keep his job" in the minors.[57] He was released by Knoxville and signed with the Scranton Miners of the New York–Penn League.[58] After a few games with the Miners, Paschal returned home to North Carolina, where he accepted a managerial job for a semi-professional baseball team in Catawba County.[58]
 Paschal was married and had a child, Ben Jr.[9]
 He died in Charlotte, North Carolina, at the age of 79, and is interred at Sharon Memorial Park.[59]
 
 Cleveland Indians (1915) Boston Red Sox (1920) New York Yankees (1924–1929) 2× World Series champion (1927, 1928) 1 Early life 2 Career

2.1 Early career
2.2 Yankees career
2.3 Later career

 2.1 Early career 2.2 Yankees career 2.3 Later career 3 Personal life 4 References 5 External links ^ a b Bell, Brian (October 31, 1928). "Trading Season Opens as Phils, Giants Act". St. Petersburg Evening Independent. p. 13..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ Harrison, James R. (October 8, 1926). "Yanks Win In Tenth, 3 To 2; Lead Series". The New York Times. p. 1.
 ^ Bell, Brian (July 5, 1929). "Bench Warmers of Early Season May Decide Races". Los Angeles Times. p. 16.
 ^ a b "Fence Busting Slugger Paschal dies in Charlotte". The Davidson County Dispatch. United Press. November 9, 1974. p. 24.
 ^ a b c d Trachtenberg, Leo (July 1995). The Wonder Team: The True Story of the Incomparable 1927 New York Yankees. Popular Press. p. 166. ISBN 978-0-87972-677-5.
 ^ a b "1915 Dothan". Baseball-Reference.com. Retrieved August 17, 2010.
 ^ a b c d e f g "Ben Paschal". Baseball-Reference.com. Retrieved January 19, 2009.
 ^ "Each Start Against Senators, A Victory". Lewiston Daily Sun. Associated Press. August 17, 1915. p. 4.
 ^ a b c d e f Tillinghaust, D.A. (April 21, 1921). "Ben Paschal is Strong With Fans". Spartanburg Herald-Journal. p. 13.
 ^ Latimer, J.C. (August 21, 1921). "Ben Paschal's Leg Broken in Sliding". Spartanburg Herald-Journal. p. 7.
 ^ a b c d e "Ben Paschal". Baseball-Reference.com. Retrieved August 17, 2010.
 ^ "1923 South Atlantic League Batting Leaders". Baseball-Reference.com. Retrieved November 1, 2015.
 ^ "Yanks's Lose in 9th, Fall Back on Race". The New York Times. Associated Press. September 20, 1924. p. 10.
 ^ "Yankees Escape Death by Leaping From Bus". The New York Times. Associated Press. April 10, 1925. p. 22.
 ^ Walsh, Davis J. (April 15, 1925). "Yankees Always Rise to Heights With Babe Ruth Idle". The Pittsburgh Press. p. 27.
 ^ a b Spector, Jessie (June 19, 2005). "Touching Base. Close Calls. One-run games make big difference to even best teams". New York Daily News. Archived from the original on August 15, 2011. Retrieved March 2, 2010.
 ^ "Helps Handles Babe Chores". The Sporting News. April 23, 1925. p. 1.
 ^ Cross, Harry (April 15, 1925). "50,000 See Yankees Beat Senators, 5–1, in Season's Opener". The New York Times. p. 13.
 ^ Harrison, James R. (April 27, 1925). "Yanks Win in Ninth on Paschal's Homer". The New York Times. p. 10.
 ^ "Yankees Get Veach and Ferguson in Trade With the Red Sox for Francis and $8,000". The New York Times. Associated Press. May 6, 1925. p. 18.
 ^ "Paschal's Home Run Wins for Yanks, 7–6". Harford Courant. Associated Press. May 24, 1925. p. B12.
 ^ "Hawks Leads National Batters; Hale Tops American Leaguers; Phillies Clouter Has Average of .448". The New York Times. Associated Press. May 24, 1925. p. S6.
 ^ Anderson, Dave (September 27, 1998). "Sports of The Times; Home Runs, Inc.: Sosa, McGwire and Spencer". The New York Times. p. 82.
 ^ Cross, Harry (July 29, 1925). "Babe Heaps Five Runs On Browns Back". The New York Times. p. 17.
 ^ "Ben Paschal 1925 Batting Gamelog". Baseball-Reference.com. Retrieved August 25, 2010.
 ^ Harrison, James R. (September 11, 1925). "Yanks Break Even Ruth Hits Two Home Runs". The New York Times. p. 27.
 ^ "Airtight Mound Work By Harriss Halts Nationals". Spartanburg Herald-Journal. Associated Press. September 9, 1925. p. 5.
 ^ "Mackmen Nose Out Yankees By 3 To 2". The Hartford Courant. Associated Press. September 12, 1925. p. 14.
 ^ a b "Paschal Signs New Contract With Yankees". St. Petersburg Evening Independent. Associated Press. February 18, 1926. p. 11.
 ^ Harrison, James R. (May 13, 1926). "Yanks Go 10 Frames Beat Indians 6–5". The New York Times. p. 28.
 ^ "Pickups and Putouts". The New York Times. June 26, 1926. p. 12.
 ^ Harrison, James R. (July 10, 1926). "Ruth's 27 Sets Yanks on Rampage". The New York Times. p. 9.
 ^ Harrison, James R. (August 9, 1926). "40,000 See Tigers Claw Yanks Again". The New York Times. p. 11.
 ^ Harrison, James R. (September 7, 1926). "60,000 see Yankees and Athletics Split". The New York Times. p. 25.
 ^ Harrison, James R. (September 9, 1926). "Yanks are Beaten as Cards look on". The New York Times. p. 16.
 ^ Harrison, James R. (September 16, 1926). "Yanks Draw Away Defeats Indians 6–4". The New York Times. p. 16.
 ^ a b c Eig, Jonathan (2005-03-29). Luckiest man: the life and death of Lou Gehrig. Simon & Schuster. p. 77. ISBN 978-0-7432-4591-3.
 ^ "Ben Paschal Returns Contract Unsigned". Hartford Courant. Associated Press. January 28, 1927. p. 12.
 ^ "Yanks Trounce Highly Touted A's 8–3". Sarasota Herald. Associated Press. April 13, 1927. p. 8.
 ^ "Red Sox Fan Tries to Spark His Team". Los Angeles Times. Associated Press. April 12, 1987.
 ^ McGowan, Bill (June 19, 1927). "Busy With Yanks, Sees Little of Lindburgh". The Delmarvia Star. p. 16.
 ^ Trachtenberg, Leo (July 1995). The Wonder Team: The True Story of the Incomparable 1927 New York Yankees. Popular Press. p. 51. ISBN 978-0-87972-677-5.
 ^ Harrison, James R. (December 13, 1927). "Trade Winds Blow as Magnates Meet; But Not a Deal Is Closed Despite Brisk Bargaining by Major Leaguers". The New York Times. p. 35.
 ^ "Ruffing, Red". Baseball Hall of Fame. Retrieved 3 August 2010.
 ^ "Diamond Pick Life". The Pueblo Indicator. Associated Press. September 29, 1928. p. 2.
 ^ Harrison, James R. (August 5, 1928). "Yanks win in 10th; Ruth Hits His 43rd". The New York Times. p. 129.
 ^ Bell, Brian (October 4, 1928). "Eyes Focus on Series". Los Angeles Times. p. 1.
 ^ Harrison, James R. (October 3, 1928). "Injury to Prevent Combs From Starting in World's Series". The New York Times. p. 25.
 ^ "Yanks may trade Durst and Paschal". The Pittsburgh Press. United Press. January 29, 1929. p. 36.
 ^ Drebinger, John (June 2, 1929). "Ruth Wallops No. 10 as Yanks Win, 8 to 1". The New York Times. p. S1.
 ^ "Pinch Hit Homer Win's for Yanks 3–2". Chicago Daily Tribute. Associated Press. July 2, 1929. p. 23.
 ^ Eig, p. 94
 ^ "Yanks Get Hargrave in Trade for Moore, Paschal, Grabowski". The New York Times. United Press. November 22, 1929. p. 37.
 ^ "Apostles Collect 23 Tallies in Beating of Toledo Hens". Milwaukee Journal Sentinel. Associated Press. August 23, 1930. p. 6.
 ^ "Did you know that...". St. Petersburg Evening Independent. United Press. June 17, 1932. p. 13.
 ^ "Ben Paschal To Play With Smokes". Spartanburg Herald-Journal. Associated Press. January 1, 1934. p. 3.
 ^ Moshier, Jeff (May 30, 1934). "Playing Square". St. Petersburg Evening Independent. p. 9.
 ^ a b "Paschal Heads Baldwin Club". The Rock Hill Herald. May 22, 1934. p. 2.
 ^ "Ben Paschal". retrosheet.org. Retrosheet, Inc. Retrieved January 28, 2010.
 Baseball portal Career statistics and player information from Baseball-Reference, or Baseball-Reference (Minors) Ben Paschal at Find a Grave  v t e v t e 1895 births 1974 deaths Cleveland Indians players Boston Red Sox players New York Yankees players Charlotte Hornets (baseball) players Muskegon Muskies players Atlanta Crackers players St. Paul Saints (AA) players Jeanerette Blues players Knoxville Smokies players Scranton Miners players Major League Baseball outfielders Major League Baseball right fielders Baseball players from Alabama People from Enterprise, Alabama CS1: Julian–Gregorian uncertainty Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version العربية Français  This page was last edited on 13 September 2019, at 23:44 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Outfielder Born: Died: Batted: Threw: Benjamin Edwin Paschal a b ^ ^ a b a b c d a b a b c d e f g ^ a b c d e f ^ a b c d e ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ a b c ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ Benny Bengough Pat Collins Earle Combs Joe Dugan Cedric Durst Mike Gazella Lou Gehrig Joe Giard Johnny Grabowski Waite Hoyt Mark Koenig Tony Lazzeri Bob Meusel Wilcy Moore Ray Morehart Ben Paschal Herb Pennock George Pipgras Dutch Ruether Babe Ruth Bob Shawkey Urban Shocker Myles Thomas Julie Wera Miller Huggins Art Fletcher Charley O'Leary Regular season Murderers' Row Benny Bengough George Burns Archie Campbell Pat Collins Earle Combs Bill Dickey Joe Dugan Leo Durocher Cedric Durst Mike Gazella Lou Gehrig Johnny Grabowski Fred Heimach Waite Hoyt Hank Johnson Mark Koenig Tony Lazzeri Wilcy Moore Bob Meusel Ben Paschal Herb Pennock George Pipgras Gene Robertson Babe Ruth Al Shealy Myles Thomas Tom Zachary Miller Huggins Art Fletcher Charley O'Leary Regular season Benny Bengough
Pat Collins
Earle Combs
Joe Dugan
Cedric Durst
Mike Gazella
Lou Gehrig
Joe Giard
Johnny Grabowski
Waite Hoyt
Mark Koenig
Tony Lazzeri
Bob Meusel
Wilcy Moore
Ray Morehart
Ben Paschal
Herb Pennock
George Pipgras
Dutch Ruether
Babe Ruth
Bob Shawkey
Urban Shocker
Myles Thomas
Julie Wera Manager
Miller Huggins Coaches
Art Fletcher
Charley O'Leary Regular season
Murderers' Row Benny Bengough
George Burns
Archie Campbell
Pat Collins
Earle Combs
Bill Dickey
Joe Dugan
Leo Durocher
Cedric Durst
Mike Gazella
Lou Gehrig
Johnny Grabowski
Fred Heimach
Waite Hoyt
Hank Johnson
Mark Koenig
Tony Lazzeri
Wilcy Moore
Bob Meusel
Ben Paschal
Herb Pennock
George Pipgras
Gene Robertson
Babe Ruth
Al Shealy
Myles Thomas
Tom Zachary Manager
Miller Huggins Coaches
Art Fletcher
Charley O'Leary Regular season Paschal before a game during the 1925 New York Yankees season Outfielder Born: (1895-10-13)October 13, 1895Enterprise, Alabama Died: November 10, 1974(1974-11-10) (aged 79)Charlotte, North Carolina 
Batted: Right
Threw: Right
 Batted: Right Threw: Right August 16, 1915, for the Cleveland Indians October 6, 1929, for the New York Yankees .309 24 138 
Cleveland Indians (1915)
Boston Red Sox (1920)
New York Yankees (1924–1929)
 
2× World Series champion (1927, 1928)
 
Benny Bengough
Pat Collins
Earle Combs
Joe Dugan
Cedric Durst
Mike Gazella
Lou Gehrig
Joe Giard
Johnny Grabowski
Waite Hoyt
Mark Koenig
Tony Lazzeri
Bob Meusel
Wilcy Moore
Ray Morehart
Ben Paschal
Herb Pennock
George Pipgras
Dutch Ruether
Babe Ruth
Bob Shawkey
Urban Shocker
Myles Thomas
Julie Wera
 
Manager
Miller Huggins
Coaches
Art Fletcher
Charley O'Leary
 
Regular season
Murderers' Row
 
Benny Bengough
George Burns
Archie Campbell
Pat Collins
Earle Combs
Bill Dickey
Joe Dugan
Leo Durocher
Cedric Durst
Mike Gazella
Lou Gehrig
Johnny Grabowski
Fred Heimach
Waite Hoyt
Hank Johnson
Mark Koenig
Tony Lazzeri
Wilcy Moore
Bob Meusel
Ben Paschal
Herb Pennock
George Pipgras
Gene Robertson
Babe Ruth
Al Shealy
Myles Thomas
Tom Zachary
 
Manager
Miller Huggins
Coaches
Art Fletcher
Charley O'Leary
 
Regular season
 