
 Hurricane Patricia was the most intense tropical cyclone ever recorded in the Western Hemisphere and the second-most intense worldwide in terms of barometric pressure. It also featured the highest one-minute maximum sustained winds ever recorded in a tropical cyclone. Originating from a sprawling disturbance near the Gulf of Tehuantepec in mid-October 2015, Patricia was first classified a tropical depression on October 20. Initial development was slow, with only modest strengthening within the first day of its classification. The system later became a tropical storm and was named Patricia, the twenty-fourth named storm of the annual hurricane season. Exceptionally favorable environmental conditions fueled explosive intensification on October 22. A well-defined eye developed within an intense central dense overcast and Patricia grew from a tropical storm to a Category 5 hurricane in just 24 hours—a near-record pace. The magnitude of intensification was poorly forecast and both forecast models and meteorologists suffered from record-high prediction errors.
 On October 23, two Hurricane Hunter missions both revealed the storm to have acquired maximum sustained winds of 205 mph (330 km/h) and a pressure of 879 mbar (hPa; 25.96 inHg). Since the peak intensity was assessed to have occurred between the missions, the National Hurricane Center ultimately estimated Patricia to have acquired winds of 215 mph (346 km/h) and pressure of 872 mbar (hPa; 25.75 inHg). This ranked it just below Typhoon Tip of 1979 as the most intense tropical cyclone on record. Patricia's exceptional intensity prompted the retirement of its name in April 2016.[1] Late on October 23, Patricia made landfall in a significantly weakened state near Cuixmala, Jalisco. Despite weakening greatly, it was the strongest recorded landfalling Pacific hurricane with winds estimated at 150 mph (240 km/h). Interaction with the mountainous terrain of Mexico induced dramatic weakening, faster than the storm had intensified. Within 24 hours of moving ashore, Patricia degraded into a tropical depression and dissipated soon thereafter late on October 24.
 On October 11, 2015, an area of disturbed weather traversed Central America and emerged over the eastern Pacific Ocean.[2] The disturbance moved slowly over the next few days, and coalesced into a Central American gyre[3]—a broad monsoonal circulation.[4] A tropical wave crossed the Caribbean Sea and eventually reached Central America on October 15; the two systems merged the following day near the Gulf of Tehuantepec.[2] A concurrent Tehuantepec gap wind event on the western side of the gyre, complimented by anticyclonic flow behind a cold front, enhanced vorticity and spurred the formation of an elongated area of low pressure on October 17.[2][3] The broad system spanned several hundred miles from the Yucatán Peninsula into the eastern Pacific.[2] A large, disorganized area of convection—showers and thunderstorms—accompanied the system,[5] increasing in coverage substantially throughout the day. A strong pulse in the Madden–Julian oscillation—a propagating climate pattern associated with increased tropical cyclogenesis[6]—may have aided in creating favorable conditions for further development.[2]
 Moving south of the Gulf of Tehuantepec on October 18, the system consolidated and developed a small, defined circulation.[2] Associated convection became more concentrated around its center.[7] Another gap wind event soon impacted the system,[2] temporarily delaying development of the disturbance into a tropical depression.[8][9] The low soon relocated to the northeast, aligning itself east of the gap wind event which aided in development. A small, well-defined circulation formed by early on October 20 within a broader cyclonic circulation. With increasing deep convection, the system is estimated to have become a tropical depression, assigned the identifier Twenty-E, by 06:00 UTC. Upon its designation, the depression was situated roughly 205 mi (335 km) south-southeast of Salina Cruz, Mexico.[2]
 Located south of a mid-level ridge and the continuing gap wind event, the nascent depression moved slowly west-southwest on October 20. Initial environmental conditions were modestly favorable, allowing for steady intensification. The depression achieved tropical storm status by 00:00 UTC on October 21; the National Hurricane Center (NHC) assigned it the name Patricia accordingly. Throughout much of October 21, Patricia moved through a region of drier, more stable air and over relatively cool sea surface temperatures. Both of these factors served to delay intensification of the cyclone.[2] The system unraveled substantially, with banding features dissipating and the low-level circulation becoming poorly defined.[10][11] Once clear of the hindering factors, convection blossomed over Patricia late on October 21 and a central dense overcast formed over the center. Simultaneously, the storm accelerated west-northwest.[2]
 Exceptionally favorable atmospheric conditions, consisting of little wind shear, anomalously high sea surface temperatures of 87 to 88 °F (30.5 to 31 °C),[nb 1] and high moisture levels yielded an environment highly conducive to rapid intensification.[2][12] Consequently, Patricia commenced explosive intensification late on October 21. Patricia reached hurricane strength shortly after 00:00 UTC on October 22,[2] featuring prominent outflow, well-defined banding features,[13] and a developing eye.[14] Upon becoming a hurricane, Patricia was located 230 mi (370 km) south of Acapulco, Mexico. In the following 12 hours, a well-defined 12 mi (19 km) wide eye formed within a ring of intense convection—with cloud tops of −80 to −90 °C (−112 to −130 °F)—forming "an almost perfectly symmetric [central dense overcast]".[2] Data from NOAA Hurricane Hunters investigating the cyclone indicated Patricia to have reached Category 4 status on the Saffir–Simpson hurricane wind scale by 18:00 UTC; maximum sustained winds were estimated at 130 mph (215 km/h) alongside a barometric pressure of 957 mbar (hPa; 28.26 inHg) at this time.[2]
 The rapid intensification of Patricia was well-anticipated but poorly forecast. Meteorologists at the NHC indicated the possibility of such in the system's first advisory as a tropical depression. They noted the only inhibiting factor would be how quickly the storm could organize an inner core.[12] Just before the onset of rapid intensification, the agency was unable to utilize the Statistical Hurricane Intensity Prediction Scheme rapid intensification guidance due to technical errors. This likely contributed to even greater errors in the agency's forecast.[2] Initial forecasts were consistently conservative with intensity and dramatic strengthening was not explicitly shown until rapid intensification was already underway.[2]
 At 03:00 UTC on October 22, the NHC forecast Patricia to achieve major hurricane status in 36 hours;[14] less than 15 hours later, the system exceeded their forecast peak.[2] Strengthening into a Category 5 hurricane was not forecast at all until Patricia had already reached such intensity,[15][16] although in the intermediate advisory immediately before Patricia's upgrade to Category 5, the NHC noted that "Patricia could become a category 5 hurricane overnight",[17] and in the preceding tropical weather discussion, noted that "Patricia could ... reach Category 5 intensity".[18] This trend continued throughout the rapid intensification period, resulting in some of the largest errors on record through 48 hours; they were the worst-ever for the Eastern Pacific since the NHC took over operations for the basin in 1988. All forecast models saw enormous errors, most of which performed worse than the official NHC forecasts. No model accurately prognosticated the magnitude nor rate of the intensification. The EMXI—an output from the European Centre for Medium-Range Weather Forecasts—saw the largest average error with 98.5 mph (158.5 km/h) at 48 hours.[2]
 During the overnight hours of October 22–23, Patricia turned northwest and decelerated slightly as it reached the western edge of the mid-level ridge.[15] Rapid development continued into October 23, and the hurricane reached Category 5 status by 00:00 UTC, with winds estimated at 175 mph (280 km/h). Convection cooled even further, with cloud tops colder than −130 °F (−90 °C) surrounding an 8 mi (13 km) wide eye by 03:00 UTC. In a 24-hour span, Patricia's winds increased by 120 mph (195 km/h) and its central pressure fell by 95 mbar (hPa; 2.81 inHg).[2] Around 06:00 UTC, an Air Force Reserve reconnaissance aircraft measured flight-level winds of 221 mph (356 km/h) and the aircraft's stepped frequency microwave radiometer (SFMR) observed surface winds of 210 mph (340 km/h).[nb 3] Furthermore, the final dropsonde observation from that mission at about 06:45 UTC indicated a central pressure of 879 mbar (hPa; 25.96 inHg).[nb 4] Rapid development continued after the aircraft left the hurricane, as the three pressure readings during the mission indicated that the pressure fell at a rate of more than 7 mbar (hPa; 0.21 inHg) per hour. Their findings also revealed an extraordinarily tight pressure gradient of 24 mbar (hPa; 0.71 inHg) per nautical mile, among the steepest gradients on record.[2]
 Based on continued improvement of the hurricane's satellite appearance, Patricia is assessed to have achieved its peak intensity around 12:00 UTC on October 23; the storm was situated about 150 mi (240 km) southwest of Manzanillo, Mexico. Maximum winds are estimated at 215 mph (345 km/h) alongside a pressure of 872 mbar (hPa; 25.75 inHg), making Patricia the second-most intense tropical cyclone ever observed. It is possible that Patricia surpassed the all-time record of 870 mbar (hPa; 25.69 inHg) set by Typhoon Tip in 1979 given the rate of deepening observed during the early morning mission.[2] The violent, compact core of Patricia was roughly 25 mi (40 km) wide with the radius of maximum winds extending only 7 mi (11 km).[2][19]
 Little change in strength took place for the next six hours; a shortwave trough crossing the Baja California Peninsula turned Patricia to the northeast and induced acceleration.[2] Another reconnaissance mission around 18:00 UTC recorded a central pressure of 879 mbar (hPa; 25.96 inHg).[2] The aircraft was battered by severe turbulence (the result of updrafts and downdrafts) and the crew experienced maximum g-forces of +3.0 and -1.5.[20]
 Late on October 23, radar imagery depicted the formation of a secondary outer eyewall, indicative of an eyewall replacement cycle. By 20:30 UTC, the final pass by reconnaissance, the hurricane's flight-level winds fell by 60 mph (95 km/h) and its central pressure rose at 8 mbar (hPa; 0.24 inHg) per hour. Coinciding with the eyewall replacement cycle was an increase in southwesterly wind shear, a factor that further accelerated Patricia's degradation.[2] The hurricane's eye soon became cloud-filled and rapid weakening ensued at an unprecedented pace.[2][21]
 At 23:00 UTC, the cyclone made landfall at Cuixmala in the municipality of La Huerta, Jalisco—about 55 mi (85 km) west-northwest of Manzanillo—with winds of 150 mph (240 km/h) and an estimated pressure of 932 mbar (hPa; 27.49 inHg).[2][22][nb 5] This made Patricia the strongest hurricane to strike Mexico's Pacific coast, exceeding an unnamed storm in 1959 and Madeline in 1976 (the latter of which has not been reanalyzed).[23][24] Although Patricia was operationally thought to have made landfall as a Category 5 hurricane with winds of 165 mph (270 km/h) and a pressure of 920 mbar (hPa; 27.17 inHg),[25] reanalysis of available data suggested that the hurricane weakened more rapidly than originally thought:[2] an automated station in Cuixmala measured a pressure of 934.2 mbar (hpa; 27.54 inHg),[2] while Josh Morgerman in Emiliano Zapata, just inside the eye of Patricia, measured a pressure of 937.8 mbar (hPa; 27.70 inHg). His observations also indicated a pressure gradient of 11 mbar (hPa; 0.32 inHg) per nautical mile.[19]
 Patricia's winds at landfall are relatively uncertain, and the 150 mph (240 km/h) value is based upon the Knaff-Zehr-Courtney pressure-wind relationship and an extrapolation of a 54 mbar (hPa; 1.59 inHg) filling using the Dvorak Technique. An additional equation stemming from work by Willoughby (1993) yielded a landfall intensity of 147 mph (237 km/h). A NOAA automated weather station at the Chamela-Cuixmala Biosphere Reserve, at an elevation of 280 ft (85 m), recorded sustained winds of 185 mph (298 km/h) and a maximum gust of 211 mph (340 km/h).[2] Further raw data from this station indicated unrealistically high sustained winds of 266 mph (428 km/h) and a maximum gust of 1,138 mph (1,831 km/h).[2][26] Based on the station's distance from Patricia's eye, outside the radius of maximum winds, the observations from this station are considered unreliable. The highest reliably measured winds of 98 mph (158 km/h) occurred in Pista between 22:30 and 23:00 UTC on October 23 before the anemometer failed.[2]
 Even faster weakening ensued through October 24 as the hurricane traversed the Sierra Madre mountains;[2] its eye disappeared from satellite imagery within hours of moving ashore.[27] The system weakened below hurricane strength by 03:00 UTC as it passed west of Guadalajara.[2] Patricia accelerated inland between a trough over Northwestern Mexico and the ridge over the Gulf of Mexico. Convection dramatically decreased in organization and the low- and mid- to upper-level circulation centers of the cyclone soon decoupled.[28][29] The system degraded into a tropical depression by 12:00 UTC as little organized convection remained, and the storm dissipated shortly thereafter over central Mexico.[2] Unimpeded by the mountains of Mexico, the mid- to upper-level circulation of Patricia, accompanied by considerable moisture, continued northeast and interacted with a cold front over the western Gulf of Mexico. The new system produced flooding rains across large areas of Arkansas, Louisiana, Mississippi, and Texas.[30][31][32]
 With maximum sustained winds of 215 mph (345 km/h) and a minimum pressure of 872 mbar (hPa; 25.75 inHg), Hurricane Patricia is the second-most intense tropical cyclone ever observed, just shy of Typhoon Tip in 1979 which had a minimum pressure of 870 mbar (hPa; 25.69 inHg). It is also the strongest tropical cyclone ever recorded in the Western Hemisphere.[2] It exceeded the previous sustained wind record of 190 mph (305 km/h) set by Hurricane Allen in 1980 and the pressure record of 882 mbar (hPa; 26.05 inHg) set by Hurricane Wilma in 2005, both in the Atlantic basin.[33] In the Eastern Pacific basin, north of the equator and east of the International Dateline, the previous basin record-holder was Hurricane Linda in 1997 with winds of 185 mph (295 km/h) and a pressure of 902 mbar (hPa; 26.64 inHg).[23] Reconnaissance also found a pressure gradient of 24 mbar (hPa; 0.71 inHg) per nautical mile early on October 23, among the steepest gradients ever observed in a tropical cyclone.[2]
 On a global scale, Patricia's one-minute maximum sustained winds rank as the highest ever reliably observed or estimated globally in a tropical cyclone, surpassing Typhoon Haiyan of 2013, although the intensity of Haiyan was only estimated via satellite imagery (T8.0, the highest rating on the Dvorak scale). Since no aircraft reconnaissance was available for Haiyan, the record set by Patricia is uncertain and comparing the intensities of the two storms is problematic.[35] According to the World Meteorological Organization, Typhoon Nancy of 1961 also produced 215 mph (345 km/h) sustained winds; however, it is widely accepted that Western Pacific reconnaissance during the 1940s to 1960s overestimated cyclone intensity and Nancy's record is considered questionable.[36][37] The most powerful wind gust produced by a tropical cyclone, as well as the highest non-tornadic winds ever recorded, is still retained by Cyclone Olivia in 1996: 253 mph (407 km/h) was observed on Barrow Island, Western Australia.[38]
 The magnitude of Patricia's rapid intensification is among the fastest ever observed. In a 24-hour period, 06:00–06:00 UTC October 22–23, its maximum sustained winds increased from 85 mph (140 km/h) to 205 mph (335 km/h). This represents a record increase of 120 mph (195 km/h). During the same period, Patricia's central pressure fell by 95 mbar (hPa; 2.81 inHg).[2] This fell just short of the world-record intensification set by Typhoon Forrest in 1983, which featured a pressure drop of 100 mbar (hPa; 2.95 inHg) in just under 24 hours.[39] With a pressure of 932 mbar (hPa; 27.52 inHg), Patricia is the strongest landfalling Pacific hurricane on record. The previous record was 941 mbar (hPa; 27.73 inHg) set by Hurricane Odile in 2014. Similarly, the hurricane featured the fastest weakening while still over water in NHC's area of responsibility, with a pressure rise of 54 mbar (hPa; 1.59 inHg) in the five hours before it made landfall. Furthermore, a dropsonde observed a 700 mbar height temperature of 32.2 °C (90.0 °F) in the eye of Patricia. This is one of the highest temperatures ever observed in a tropical cyclone's eye worldwide.[2]
 Other record-strength tropical cyclones:
 Typhoon Tip1979 · 870 hPa
 Hurricane Patricia2015 · 872 hPa
 Hurricane Wilma2005 · 882 hPa
 Odisha cyclone1999 · 912 hPa
 Cyclones Gwenda and Inigo1999 & 2003 · 900 hPa
 Cyclone Winston2016 · 884 hPa
 Hurricane Catarina2004 · 972 hPa
 Cyclone Gafilo2004 · 895 hPa
 Central America Mexico Texas 1 Origins 2 Rapid intensification

2.1 Forecast errors

 2.1 Forecast errors 3 Peak strength 4 Landfall and dissipation 5 Records 6 See also 7 Notes 8 References Tropical cyclones portal Hurricane Linda in 1997 – Previous record intensity in eastern Pacific basin, during a similarly strong El Niño event Typhoon Megi in 2010 – Research reconnaissance observed similarly intense sustained winds Typhoon Haiyan in 2013 – Most intense landfalling tropical cyclone Typhoon Nancy in 1961 – Tied with Patricia for the highest winds observed in a tropical cyclone, considered unreliable Typhoon Tip in 1979 – Most intense tropical cyclone recorded in terms of pressure Hurricane Wilma in 2005 – Previous record low central pressure in the Western Hemisphere Hurricane Allen in 1980 – Previous record high sustained winds in the Western Hemisphere Typhoon Forrest in 1983 – Record-fastest intensification of any tropical cyclone; underwent a 100 mbar (hPa; 2.95 inHg) pressure drop in just under 24 hours ^ These sea surface temperatures were at record levels for mid-October in the region and were largely undisturbed since Hurricane Carlos in June.[2]
 ^ Post-storm reanalysis concluded that Patricia's peak intensity occurred between the reconnaissance flights, and thus the actual minimum pressure at peak intensity was estimated at 872 mbar (hPa; 25.75 inHg). However, this measured value of 879 mbar (hPa; 25.96 inHg)—extrapolated from a direct observation of 883 mbar (hPa; 26.08 inHg) with surface winds of 52 mph (84 km/h)—remains the lowest directly-measured central pressure in a Western Hemisphere tropical cyclone.[2]
 ^ The maximum SFMR winds for the 06:00 UTC mission were operationally withheld for quality assurance and later determined to be accurate.[2]
 ^ This pressure value was extrapolated from an observation of 885 mbar (hPa; 26.14 inHg) with surface winds of 66 mph (106 km/h).[2]
 ^ The landfall pressure is based upon several observations in the hurricane's eye and has an error margin of ± 2–3 mbar (hPa; 0.06–0.09 inHg).[2]
 ^ "World Meteorological Organization retires storm names Erika, Joaquin and Patricia" (Press release). National Oceanic and Atmospheric Administration. April 25, 2016. Archived from the original on June 1, 2016. Retrieved April 26, 2016..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as Todd B. Kimberlain; Eric S. Blake & John P. Cangialosi (February 1, 2016). Hurricane Patricia (PDF) (Report). Tropical Cyclone Report. Miami, Florida: National Hurricane Center. Retrieved February 5, 2016.
 ^ a b Lance F. Bosart; Philippe P. Papin; Andrew S. Levine & A. M. Bentley (April 19, 2016). "The Influence of a Precursor Central American Gyre and a Northerly Surge into the Gulf of Tehuantepec on the Formation of Hurricane Patricia in October 2015". University of Albany. American Meteorological Society. 2016: A54F–07. Bibcode:2016AGUFM.A54F..07B. Archived from the original on April 28, 2016. Retrieved April 21, 2016.
 ^ Philippe P. Papin; Kyle S. Griffin; Lance F. Bosart & Ryan D. Torn (2013). "A Climatology of Central American Gyres" (PDF). University of Albany. Archived (PDF) from the original on May 9, 2016. Retrieved April 21, 2016. Cite journal requires |journal= (help)
 ^ Todd B. Kimberlain (October 17, 2015). Tropical Weather Outlook (.TXT) (Report). Miami, Florida: National Hurricane Center. Archived from the original on March 4, 2016. Retrieved February 5, 2016.
 ^ Philip J. Klotzbach (January 15, 2010). "On the Madden–Julian Oscillation–Atlantic Hurricane Relationship" (PDF). Journal of Climate. American Meteorological Society. 23 (2): 282–293. Bibcode:2010JCli...23..282K. doi:10.1175/2009JCLI2978.1. Archived (PDF) from the original on November 1, 2012. Retrieved February 5, 2016.
 ^ Todd B. Kimberlain (October 18, 2015). Tropical Weather Outlook (.TXT) (Report). Miami, Florida: National Hurricane Center. Archived from the original on March 4, 2016. Retrieved February 5, 2016.
 ^ Todd B. Kimberlain (October 19, 2015). Tropical Weather Outlook (.TXT) (Report). Miami, Florida: National Hurricane Center. Archived from the original on November 23, 2015. Retrieved February 5, 2016.
 ^ Dave Roberts (October 19, 2015). Tropical Weather Outlook (.TXT) (Report). Miami, Florida: National Hurricane Center. Archived from the original on March 4, 2016. Retrieved February 5, 2016.
 ^ Richard J. Pasch (October 21, 2015). Tropical Storm Patricia Discussion Number 4 (Report). Miami, Florida: National Hurricane Center. Archived from the original on April 28, 2016. Retrieved February 5, 2016.
 ^ Jack L. Beven (October 21, 2015). Tropical Storm Patricia Discussion Number 5 (Report). Miami, Florida: National Hurricane Center. Archived from the original on April 28, 2016. Retrieved February 5, 2016.
 ^ a b Todd B. Kimberlain (October 20, 2015). Tropical Depression Twenty-E Discussion Number 1 (Report). Miami, Florida: National Hurricane Center. Archived from the original on April 28, 2016. Retrieved April 15, 2016.
 ^ Richard J. Pasch (October 22, 2015). Hurricane Patricia Discussion Number 8 (Report). Miami, Florida: National Hurricane Center. Archived from the original on April 28, 2016. Retrieved February 5, 2016.
 ^ a b Stacy R. Stewart (October 22, 2015). Tropical Storm Patricia Discussion Number 7 (Report). Miami, Florida: National Hurricane Center. Archived from the original on April 28, 2016. Retrieved April 15, 2016.
 ^ a b Michael J. Brennan (October 22, 2015). Hurricane Patricia Discussion Number 11 (Report). Miami, Florida: National Hurricane Center. Archived from the original on April 28, 2016. Retrieved April 15, 2016.
 ^ Eric S. Blake (October 23, 2015). Hurricane Patricia Discussion Number 12 (Report). Miami, Florida: National Hurricane Center. Archived from the original on April 28, 2016. Retrieved April 15, 2016.
 ^ Eric S. Blake (October 23, 2015). Hurricane Patricia Intermediate Advisory Number 11a (Report). Miami, Florida: National Hurricane Center. Archived from the original on March 20, 2016. Retrieved April 24, 2016.
 ^ Scott Stripling (October 22, 2015). Tropical Weather Discussion (Report). Miami, Florida: National Hurricane Center. Archived from the original on July 14, 2017. Retrieved May 12, 2018.
 ^ a b Josh Morgerman (November 2, 2015). iCyclone Chase Report: Hurricane Patricia (PDF) (Report). iCyclone. Archived (PDF) from the original on December 8, 2015. Retrieved November 2, 2015.
 ^ Scott Sistek (October 25, 2015). "Watch: 'Most intense turbulence' as NOAA flies through Hurricane Patricia". KOMO-TV. American Broadcasting Company. Archived from the original on February 5, 2016. Retrieved December 29, 2015.
 ^ Jack L. Beven (October 23, 2015). Hurricane Patricia Discussion Number 16 (Report). Miami, Florida: National Hurricane Center. Archived from the original on April 28, 2016. Retrieved October 24, 2015.
 ^ Patricia Tocó Tierra en Costa de Jalisco (PDF) (Report) (in Spanish). Comisión Nacional de Agua. October 24, 2015. Archived from the original (PDF) on October 26, 2015. Retrieved October 26, 2015.
 ^ a b National Hurricane Center; Hurricane Research Division; Central Pacific Hurricane Center. "The Northeast and North Central Pacific hurricane database 1949–2018". United States National Oceanic and Atmospheric Administration's National Weather Service. A guide on how to read the database is available here.
 ^ Josh Morgerman (October 27, 2014). "Great Mexico Hurricane of 1959: Reanalyzing a Monster". iCyclone. Archived from the original on April 28, 2016. Retrieved October 24, 2015.
 ^ Eric S. Blake & Stacy R. Stewart (October 23, 2015). "Hurricane Patricia Tropical Cyclone Update". Miami, Florida: National Hurricane Center. Archived from the original on April 28, 2016. Retrieved October 24, 2015.
 ^ "7 Day Observations from Data Collection Platform (DCP) CCXJ1". National Weather Service. October 23, 2015. Archived from the original on October 29, 2015. Retrieved October 29, 2015.
 ^ Eric S. Blake & Stacy R. Stewart (October 24, 2015). Hurricane Patricia Discussion Number 17 (Report). Miami, Florida: National Hurricane Center. Archived from the original on April 28, 2016. Retrieved February 8, 2016.
 ^ John P. Cangialosi & Stacy R. Stewart (October 24, 2015). Hurricane Patricia Discussion Number 18 (Report). Miami, Florida: National Hurricane Center. Archived from the original on April 28, 2016. Retrieved February 8, 2016.
 ^ Jack L. Beven (October 24, 2015). Tropical Depression Patricia Discussion Number 19 (Report). Miami, Florida: National Hurricane Center. Archived from the original on April 28, 2016. Retrieved February 8, 2016.
 ^ Jeff Masters & Bob Henson (October 24, 2015). "Patricia's Remnants to Fuel Dangerous Rains in Texas". Weather Underground. Archived from the original on October 25, 2015. Retrieved October 25, 2015.
 ^ Amanda K. Fanning (October 24, 2015). Storm Summary Number 10 for Southern Plains Heavy Rainfall (Report). College Park, Maryland: Weather Prediction Center. Archived from the original on October 26, 2015. Retrieved October 26, 2015.
 ^ Amanda K. Fanning (October 24, 2015). Storm Summary Number 15 for Southern Plains Heavy Rainfall (Report). College Park, Maryland: Weather Prediction Center. Archived from the original on October 26, 2015. Retrieved October 26, 2015.
 ^ "North Atlantic Hurricane Best Tracks (HURDAT2)". Hurricane Research Division. National Hurricane Center. February 17, 2016. Archived from the original on June 2, 2016. Retrieved April 30, 2016.
 ^ "Tropical Cyclones: Image Enhancements FAQ". Cooperative Institute for Meteorological Satellite Studies. Madison, Wisconsin: University of Wisconsin-Madison. n.d. Archived from the original on February 13, 2016. Retrieved February 4, 2016.
 ^ Dennis Mersereau (October 23, 2015). "At 200 MPH, Hurricane Patricia Is Now the Strongest Tropical Cyclone Ever Recorded". The Vane. Gawker Media. Archived from the original on October 23, 2015. Retrieved October 23, 2015.
 ^ Christopher W. Landsea (April 21, 2010). "Subject: E1) Which is the most intense tropical cyclone on record?". Frequently Asked Questions. National Oceanic and Atmospheric Administration. Archived from the original on December 6, 2010. Retrieved October 24, 2015.
 ^ "5 Things to Know About Hurricane Patricia". Atlanta, Georgia: The Weather Channel. October 23, 2015. Archived from the original on March 15, 2016. Retrieved October 24, 2015.
 ^ "World: Maximum Surface Wind Gust (3-Second)". World Meteorological Organization. 2010. Archived from the original on March 3, 2016. Retrieved October 24, 2015.
 ^ "Tropical Cyclone: Fastest Intensification of Tropical Cyclone". World Meteorological Organization. n.d. Archived from the original on April 20, 2016. Retrieved October 24, 2015.
 v t e v t e  Book  Category  Portal 2015 Pacific hurricane season Meteorological histories of individual tropical cyclones CS1 errors: missing periodical CS1 Spanish-language sources (es) Wikipedia pages semi-protected due to dispute Featured articles Use mdy dates from February 2016 Not logged in Talk Contributions Create account Log in Article Talk Read View source View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version 中文  This page was last edited on 22 October 2019, at 01:04 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  2015 Pacific hurricane season ^ ^ ^ ^ ^ ^ a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as a b 2016 ^ ^ ^ 23 ^ ^ ^ ^ ^ a b ^ a b a b ^ ^ ^ a b ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ · · · · · · · ·  Book  Category  Portal Category 5 major hurricane (SSHWS/NWS) Track of Hurricane Patricia October 20, 2015 (October 20, 2015) October 24, 2015 (October 24, 2015)   
 1-minute sustained: 215 mph (345 km/h)  872 mbar (hPa); 25.75 inHg(Record low in Western Hemisphere; second-lowest globally)   
 
Central America
Mexico
Texas
  Part of the 2015 Pacific hurricane season 
 12 hours
 22.7
 26.1
 42.0
 5.9
 6.8
 10.9
 24 hours
 35.0
 40.3
 64.8
 9.8
 11.3
 18.1
 36 hours
 47.7
 58.9
 88.3
 12.5
 14.4
 23.2
 48 hours
 57.8
 66.5
 107.0
 14.0
 16.1
 25.9
 72 hours
 55.0
 63.3
 101.9
 15.5
 17.8
 28.7
 96 hours
 25.0
 28.8
 46.3
 16.3
 18.8
 30.2
 Northwest PacificNortheast PacificNorth AtlanticNorth IndianAustralian regionSouth PacificSouth AtlanticSouthwest Indian
Typhoon Tip1979 · 870 hPa


Hurricane Patricia2015 · 872 hPa


Hurricane Wilma2005 · 882 hPa


Odisha cyclone1999 · 912 hPa


Cyclones Gwenda and Inigo1999 & 2003 · 900 hPa


Cyclone Winston2016 · 884 hPa


Hurricane Catarina2004 · 972 hPa


Cyclone Gafilo2004 · 895 hPa

 Northwest Pacific Northeast Pacific North Atlantic North Indian Australian region South Pacific South Atlantic Southwest Indian 
Typhoon Tip1979 · 870 hPa

 
Hurricane Patricia2015 · 872 hPa

 
Hurricane Wilma2005 · 882 hPa

 
Odisha cyclone1999 · 912 hPa

 
Cyclones Gwenda and Inigo1999 & 2003 · 900 hPa

 
Cyclone Winston2016 · 884 hPa

 
Hurricane Catarina2004 · 972 hPa

 
Cyclone Gafilo2004 · 895 hPa

 Timeline  

4Andres
4Blanca
1Carlos
TSEla
TSHalola
TSIune
4Dolores
TSEnrique

TSFelicia
TDEight-E
2Guillermo
4Hilda
TDEleven-E
4Kilo
1Loke
4Ignacio

4Jimena
TSKevin
3Linda
TSMalia
TDSixteen-E
TSNiala
1Marty
2Oho

TDEight-C
TSNora
4Olaf
5Patricia(history)
TSRick
4Sandra
TDNine-C

 4 Andres 4 Blanca 1 Carlos TS Ela TS Halola TS Iune 4 Dolores TS Enrique TS Felicia TD Eight-E 2 Guillermo 4 Hilda TD Eleven-E 4 Kilo 1 Loke 4 Ignacio 4 Jimena TS Kevin 3 Linda TS Malia TD Sixteen-E TS Niala 1 Marty 2 Oho TD Eight-C TS Nora 4 Olaf 5 Patricia(history) TS Rick 4 Sandra TD Nine-C 
 Book
 Category
 Portal
 