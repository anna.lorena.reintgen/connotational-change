
 Amanita ocreata, commonly known as the death angel, destroying angel, angel of death or more precisely western North American destroying angel, is a deadly poisonous basidiomycete fungus, one of many in the genus Amanita. Occurring in the Pacific Northwest and California floristic provinces of North America, A. ocreata associates with oak trees. The large fruiting bodies (the mushrooms) generally appear in spring; the cap may be white or ochre and often develops a brownish centre, while the stipe, ring, gill and volva are all white.
 Amanita ocreata resemble several edible species commonly consumed by humans, increasing the risk of accidental poisoning. Mature fruiting bodies can be confused with the edible A. velosa, A. lanei or Volvopluteus gloiocephalus, while immature specimens may be difficult to distinguish from edible Agaricus mushrooms or puffballs. Similar in toxicity to the death cap (A. phalloides) and destroying angels of Europe (A. virosa) and eastern North America (A. bisporigera), it is a potentially deadly fungus responsible for several poisonings in California.[1] Its principal toxic constituent, α-amanitin, damages the liver and kidneys, often fatally, and has no known antidote, though silybin and N-acetylcysteine show promise.[2] The initial symptoms are gastrointestinal and include abdominal pain, diarrhea and vomiting. These subside temporarily after 2–3 days, though ongoing damage to internal organs during this time is common; symptoms of jaundice, diarrhea, delirium, seizures, and coma may follow with death from liver failure 6–16 days post ingestion.
 Amanita ocreata was first described by American mycologist Charles Horton Peck in 1909 from material collected by Charles Fuller Baker in Claremont, California.[3] The specific epithet is derived from the Latin ocrěātus 'wearing greaves' from ocrea 'greave',[4] referring to its loose, baggy volva.[5] Amanita bivolvata is a botanical synonym. The mushroom belongs to the same section (Phalloideae) and genus (Amanita) as several deadly poisonous fungi including the death cap (A. phalloides) and several all-white species of Amanita known as "destroying angels": A. bisporigera of eastern North America, and the European A. virosa. "Death angel" is used as an alternate common name.[5]
 A. ocreata is generally stouter than the other fungi termed destroying angels. It first appears as a white egg-shaped object covered with a universal veil. As it grows, the mushroom breaks free, though there may rarely be ragged patches of veil left at the cap edges. The cap is initially hemispherical, before becoming more convex and flattening, sometimes irregularly. This may result in undulations in the cap, which may reach up to 12 cm (5 in) in diameter. The colour varies from white, through yellowish-white to shades of ochre, sometimes with a brownish centre. Occasionally parts of the fruiting bodies may have pinkish tones. The rest of the fungus below the cap is white. The crowded gills are free to narrowly adnate. The stipe is 8–20 cm (3–8 in) high and 1.5–2 cm (½–⅔ in) thick at the apex, and bears a thin white membranous ring. The volva is thin, smooth and sac-like, although may be quite extensive and contain almost half the stipe. The spore print is white, and the subglobose to ovoid to subellipsoid, amyloid spores are 9–14 x 7–10 μm viewed under a microscope.[1] There is typically no smell, though some fruiting bodies may have a slight odour, described as that of bleach or chlorine, dead fish or iodine. Like other destroying angels, the flesh stains yellow when treated with potassium hydroxide (KOH).[6][7]
 This fungus resembles the edible mushrooms Agaricus arvensis and A. campestris, and the puffballs (Lycoperdon spp.) before the caps have opened and the gills have become visible, so those collecting immature fungi run the risk of confusing the varieties. It also resembles and grows in the same areas as the edible and prized Amanita velosa, which can be distinguished from A. ocreata by its lack of ring, striate cap margin and thick universal veil remnants comprising the veil.[5] The edible Amanita calyptroderma lacks a ring and is more likely to have veil patches remaining on its cap, which is generally darker. Volvariella speciosa has pink spores and no ring or volva.[8]
 Appearing from January to April, A. ocreata occurs later in the year than other amanitas except A. calyptroderma. It is found in mixed woodland on the Pacific coast of North America,[1] from Washington south through California to Baja California in Mexico.[7] It may feasibly occur on Vancouver Island in British Columbia though this has never been confirmed.[9] It forms ectomycorrhizal relationships and is found in association with coast live oak (Quercus agrifolia),[10] as well as hazel (Corylus spp.).[7] In Oregon and Washington, it may also be associated with the Garry oak (Quercus garryana).[9]
 Amanita ocreata is highly toxic, and has been responsible for mushroom poisonings in western North America, particularly in the spring. It contains highly toxic amatoxins, as well as phallotoxins, a feature shared with the closely related death cap (A. phalloides), half a cap of which can be enough to kill a human, and other species known as destroying angels.[1][11] There is some evidence it may be the most toxic of all the North American phalloideae, as a higher proportion of people consuming it had organ damage and 40% perished.[12] Dogs have also been known to consume this fungus in California with fatal results.[13]
 Amatoxins consist of at least eight compounds with a similar structure, that of eight amino-acid rings;[14] of those found in A. ocreata, α-amanitin is the most prevalent and along with β-amanitin is likely to be responsible for the toxic effects.[1][15][16] The major toxic mechanism is the inhibition of RNA polymerase II, a vital enzyme in the synthesis of messenger RNA (mRNA), microRNA, and small nuclear RNA (snRNA). Without mRNA, essential protein synthesis and hence cell metabolism stop and the cell dies.[17] The liver is the principal organ affected, as it is the first organ encountered after absorption by the gastrointestinal tract, though other organs, especially the kidneys, are susceptible to the toxins.[18]
 The phallotoxins consist of at least seven compounds, all of which have seven similar peptide rings. Although they are highly toxic to liver cells,[19] phallotoxins have since been found to have little input into the destroying angel's toxicity as they are not absorbed through the gut.[17] Furthermore, one phallotoxin, phalloidin, is also found in the edible (and sought-after) blusher (Amanita rubescens).[14]
 Signs and symptoms of poisoning by A. ocreata are initially gastrointestinal in nature and include colicky abdominal pain, with watery diarrhea and vomiting which may lead to dehydration and, in severe cases, hypotension, tachycardia, hypoglycemia, and acid-base disturbances.[20][21] The initial symptoms resolve two to three days after ingestion of the fungus. A more serious deterioration signifying liver involvement may then occur—jaundice, diarrhea, delirium, seizures, and coma due to fulminant liver failure and attendant hepatic encephalopathy caused by the accumulation of normally liver-removed substances in the blood.[22] Renal failure (either secondary to severe hepatitis[23][24] or caused by direct toxic renal damage[17]) and coagulopathy may appear during this stage. Life-threatening complications include increased intracranial pressure, intracranial hemorrhage, sepsis, pancreatitis, acute renal failure, and cardiac arrest.[20][21] Death generally occurs six to sixteen days after the poisoning.[25]
 Consumption of A. ocreata is a medical emergency that requires hospitalization. There are four main categories of therapy for poisoning: preliminary medical care, supportive measures, specific treatments, and liver transplantation.[2]
 Preliminary care consists of gastric decontamination with either activated carbon or gastric lavage. However, due to the delay between ingestion and the first symptoms of poisoning, it is commonplace for patients to arrive for treatment long after ingestion, potentially reducing the efficacy of these interventions.[2][26] Supportive measures are directed towards treating the dehydration which results from fluid loss during the gastrointestinal phase of intoxication and correction of metabolic acidosis, hypoglycemia, electrolyte imbalances, and impaired coagulation.[2]
 No definitive antidote for amatoxin poisoning is available, but some specific treatments such as intravenous penicillin G have been shown to improve survival.[27] There is some evidence that intravenous silibinin, an extract from the blessed milk thistle (Silybum marianum), may be beneficial in reducing the effects of amatoxins, preventing their uptake by hepatocytes, thereby protecting undamaged hepatic tissue.[28][29] In patients developing liver failure, a liver transplant is often the only option to prevent death. Liver transplants have become a well-established option in amatoxin poisoning.[30] This is a complicated issue, however, as transplants themselves may have significant complications and mortality; patients require long-term immunosuppression to maintain the transplant.[2] Evidence suggests that, although survival rates have improved with modern medical treatment, in patients with moderate to severe poisoning up to half of those who did recover suffered permanent liver damage.[31] However, a follow-up study has shown that most survivors recover completely without any sequelae if treated within 36 hours of mushroom ingestion.[32]
 Works cited
 
 1 Taxonomy and naming 2 Description 3 Distribution and habitat 4 Toxicity

4.1 Signs and symptoms
4.2 Treatment

 4.1 Signs and symptoms 4.2 Treatment 5 See also 6 References 7 External links Fungi portal List of Amanita species List of deadly fungi ^ a b c d e Ammirati, Joseph F.; Thiers, Harry D.; Horgen, Paul A. (1977). "Amatoxin containing mushrooms:Amanita ocreata and Amanita phalloides in California". Mycologia. Mycologia, Vol. 69, No. 6. 69 (6): 1095–1108. doi:10.2307/3758932. JSTOR 3758932. PMID 564452..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c d e Enjalbert F, Rapior S, Nouguier-Soulé J, Guillon S, Amouroux N, Cabot C (2002). "Treatment of amatoxin poisoning: 20-year retrospective analysis". Journal of Toxicology: Clinical Toxicology. 40 (6): 715–57. doi:10.1081/CLT-120014646. PMID 12475187.
 ^ Peck, Charles Horton (1909). "New species of fungi". Bull. Torrey Bot. Club. Bulletin of the Torrey Botanical Club, Vol. 36, No. 6. 36 (6): 329–39. doi:10.2307/2479371. JSTOR 2479371.
 ^ Simpson, D.P. (1979). Cassell's Latin Dictionary (5 ed.). London: Cassell Ltd. p. 883. ISBN 978-0-304-52257-6.
 ^ a b c Arora, David (1986). Mushrooms demystified: a comprehensive guide to the fleshy fungi (2nd ed.). Berkeley: Ten Speed Press. pp. 271–73. ISBN 978-0-89815-169-5.
 ^ Thiers HD. (1982). The Agaricales (Gilled Fungi) of California 1: Amanitaceae. Eureka, CA: Mad River Press. ISBN 0-916422-24-0.
 ^ a b c Tulloss, Rodham E. "Amanita ocreata Peck "Western American Destroying Angel"". Studies in the Amanitaceae. Retrieved 2011-02-11.
 ^ Wood, Michael; Fred Stevens (1998–2007). "California fungi:Amanita ocreata". The Fungi of California. Archived from the original on 11 October 2007. Retrieved 2007-11-13.
 ^ a b Birch, Shannon (April 2006). "Is Amanita ocreata on Vancouver Island?" (PDF). Fungifama: 5. Retrieved 2007-12-11.
 ^ Benjamin, Mushrooms: poisons and panaceas, p. 205
 ^ Benjamin, Mushrooms: poisons and panaceas p. 211
 ^ Beug, Michael (April 2006). "Reflections on Mushroom Poisoning – Part I" (PDF). Fungifama: 3–5. Retrieved 2007-12-11.
 ^ Tegzes, John H.; Birgit Puschner (2002). "Amanita mushroom poisoning: efficacy of aggressive treatment of two dogs". Veterinary and Human Toxicology. 44 (2): 96–99. PMID 11931514.
 ^ a b Litten, Walter (March 1975). "The most poisonous mushrooms". Scientific American. 232 (3): 90–101. Bibcode:1975SciAm.232c..90L. doi:10.1038/scientificamerican0375-90. PMID 1114308.
 ^ Köppel, C. (1993). "Clinical symptomatology and management of mushroom poisoning". Toxicon. 31 (12): 1513–40. doi:10.1016/0041-0101(93)90337-I. PMID 8146866.
 ^ Dart, Richard C. (2004). "Mushrooms". Medical toxicology. Philadelphia: Williams & Wilkins. pp. 1719–35. ISBN 978-0-7817-2845-4.
 ^ a b c Karlson-Stiber, Christine; Hans Persson (2003). "Cytotoxic fungi - an overview". Toxicon. 42 (4): 339–49. doi:10.1016/S0041-0101(03)00238-1. PMID 14505933.
 ^ Benjamin, Mushrooms: poisons and panaceas, p. 217
 ^ Wieland, Thomas; V.M. Govindan (1974). "Phallotoxins bind to actins". FEBS Letters. 46 (1): 351–3. doi:10.1016/0014-5793(74)80404-7. PMID 4429639.
 ^ a b Pinson CW;  et al. (May 1990). "Liver transplantation for severe Amanita phalloides mushroom poisoning". American Journal of Surgery. 159 (5): 493–9. doi:10.1016/S0002-9610(05)81254-1. PMID 2334013.
 ^ a b Klein AS, Hart J, Brems JJ, Goldstein L, Lewin K, Busuttil RW (February 1989). "Amanita poisoning: Treatment and the role of liver transplantation". American Journal of Medicine. 86 (2): 187–93. doi:10.1016/0002-9343(89)90267-2. PMID 2643869.
 ^ North, Pamela Mildred (1967). Poisonous plants and fungi in colour. London: Blandford Press. OCLC 955264.
 ^ Nicholls DW, Hyne BE, Buchanan P (1995). "Death cap mushroom poisoning". The New Zealand Medical Journal. 108 (1001): 234. PMID 7603660.
 ^ Vetter, János (January 1998). "Toxins of Amanita phalloides". Toxicon. 36 (1): 13–24. doi:10.1016/S0041-0101(97)00074-3. PMID 9604278.
 ^ Fineschi V, Di Paolo M, Centini F (1996). "Histological criteria for diagnosis of Amanita phalloides poisoning". Journal of Forensic Sciences. 41 (3): 429–32. doi:10.1520/JFS13929J. PMID 8656182.
 ^ Vesconi S, Langer M, Iapichino G, Costantino D, Busi C, Fiume L (1985). "Therapy of cytotoxic mushroom intoxication". Critical Care Medicine. 13 (5): 402–6. doi:10.1097/00003246-198505000-00007. PMID 3987318.
 ^ Floerscheim GL, Weber O, Tschumi P, Ulbrich M (August 1982). "Clinical death-cap (Amanita phalloides) poisoning: Prognostic factors and therapeutic measures". Schweizerische medizinische Wochenschrift (in German). 112 (34): 1164–1177. PMID 6291147.
 ^ Hruby K, Csomos G, Fuhrmann M, Thaler H (1983). "Chemotherapy of Amanita phalloides poisoning with intravenous silibinin". Human Toxicology. 2 (2): 183–95. doi:10.1177/096032718300200203. PMID 6862461.
 ^ Carducci R;  et al. (1996). "Silibinin and acute poisoning with Amanita phalloides". Minerva Anestesiologica (in Italian). 62 (5): 187–93. PMID 8937042.
 ^ Ganzert M, Felgenhauer N, Zilker T (2005). "Indication of liver transplantation following amatoxin intoxication". Journal of Hepatology. 42 (2): 202–09. doi:10.1016/j.jhep.2004.10.023. PMID 15664245.
 ^ Benjamin, Mushrooms: poisons and panaceas, pp. 231–232
 ^ Giannini L, Vannacci A, Missanelli A, Mastroianni R, Mannaioni PF, Moroni F, Masini E (2007). "Amatoxin poisoning: A 15-year retrospective analysis and follow-up evaluation of 105 patients". Clinical Toxicology. 45 (5): 539–42. doi:10.1080/15563650701365834. PMID 17503263.
 Benjamin DR (1995). Mushrooms: poisons and panaceas — a handbook for naturalists, mycologists and physicians. New York, New York: WH Freeman and Company. ISBN 978-0-7167-2600-5. Key to species of Amanita Section Phalloideae from North and Central America - Amanita studies website California Fungi—Amanita ocreata v t e A. albocreata A. altipes A. aprica A. armeniaca A. augusta A. breckonii A. eliae A. farinosa A. frostiana A. gemmata A. ibotengutake A. multisquamosa A. muscaria A. nehuta A. orientigemmata A. parcivolvata A. pantherina A. parvipantherina A. regalis A. roseotincta A. rubrovolvata A. velatipes A. virgineoides A. viscidolutea A. wellsii A. xanthocephala A. basii A. caesarea A. caesareoides A. chepangiana A. hemibapha A. jacksonii A. lanei A. spreta A. zambiana A. battarrae A. beckeri A. ceciliae A. crocea A. fulva A. liquii A. lividopallescens A. nivalis A. orientifulva A. pachycolea A. pekeoides A. umbrinolutea A. vaginata A. velosa A. curtipes A. ovoidea A. proxima A. volvata A. arocheae A. bisporigera A. exitialis A. fuliginea A. griseorosea A. magnivelaris A. marmorata subsp. myrtacearum A. manginiana A. molliuscula A. ocreata A. pallidorosea A. parviexitialis A. pseudoporphyria A. phalloides A. rimosa A. subfuliginea A. subjunquillea A. subpallidorosea A. verna A. virosa A. virosiformis A. abrupta A. atkinsoniana A. austroviridis A. ananaeceps A. cokeri A. daucipes A. echinocephala A. magniverrucata A. ochrophylla A. ochrophylloides A. onusta A. ravenelii A. rhopalopus A. smithiana A. solitaria A. sphaerobulbosa A. aestivalis A. australis A. brunnescens A. brunneolocularis A. citrina A. excelsa A. excelsa v. spissa A. flavella A. flavoconia A. flavorubens A. franchetii A. luteofusca A. nothofagi A. novinupta A. porphyria A. rubescens A. nauseosa A. thiersii A. vittadinii v t e A. albocreata A. crenulata A. farinosa A. frostiana A. gemmata A. multisquamosa A. muscaria A. pantherina A. porphyria A. regalis A. strobiliformis A. xanthocephala Toxins: Ibotenic Acid Muscimol Other compounds: Muscarine Muscazone Amavadine A. thiersii unknown toxin A. bisporigera A. exitialis A. magnivelaris A. ocreata A. verna A. virosa A. virosiformis A. arocheae A. phalloides A. subjunquilea Amanitins
alpha-
beta-
gamma-
epsilon- alpha- beta- gamma- epsilon- Amanullin Amanullinic acid Amaninamide Amanin Proamanullin Phallacidin Phallacin Phallisacin Phallisin Phalloidin Phalloin Prophalloin Alaviroidin Viroisin Deoxoviroisin Viroidin Deoxoviroidin Antamanide Phallolysin Toxophallin A. proxima A. smithiana A. sphaerobulbosa Allenic norleucine (2-amino-4,5-hexadienoic acid) Propargylglycine A. rubescens (A. amerirubescens nom. prov.) Rubescenslysin Wikidata: Q588770 EoL: 6691958 Fungorum: 451380 GBIF: 5452199 iNaturalist: 67356 MycoBank: 451380 NCBI: 235532 Amanita Fungi of North America Hepatotoxins Deadly fungi Poisonous fungi Fungi described in 1909 Taxa named by Charles Horton Peck CS1 German-language sources (de) CS1 Italian-language sources (it) Articles with short description Articles with 'species' microformats Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Asturianu Català Español Euskara فارسی Français Polski Português Русский Українська Tiếng Việt 中文  This page was last edited on 14 October 2019, at 19:04 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  A. ocreata Amanita ocreata gills convex flat free ring and volva white mycorrhizal deadly Amanita ocreata death angel destroying angel angel of death western North American destroying angel a b c d e 69 a b c d e 40 ^ 36 ^ a b c ^ a b c ^ a b ^ ^ ^ ^ 44 a b 232 ^ 31 ^ a b c 42 ^ ^ 46 a b 159 a b 86 ^ ^ 108 ^ 36 ^ 41 ^ 13 ^ 112 ^ 2 ^ 62 ^ 42 ^ ^ 45 Works cited Toxins: Other compounds: 
 Kingdom:
 Fungi
 Division:
 Basidiomycota
 Class:
 Agaricomycetes
 Order:
 Agaricales
 Family:
 Amanitaceae
 Genus:
 Amanita
 Section:
 Phalloideae
 Species:
 A. ocreata
 Amanita ocreataPeck
 
 Approximate distribution (green)
  Mycological characteristics gills on hymenium cap is convex
   or flat hymenium is free stipe has a ring and volva spore print is white ecology is mycorrhizal edibility: deadly Section Amanita
A. albocreata
A. altipes
A. aprica
A. armeniaca
A. augusta
A. breckonii
A. eliae
A. farinosa
A. frostiana
A. gemmata
A. ibotengutake
A. multisquamosa
A. muscaria
A. nehuta
A. orientigemmata
A. parcivolvata
A. pantherina
A. parvipantherina
A. regalis
A. roseotincta
A. rubrovolvata
A. velatipes
A. virgineoides
A. viscidolutea
A. wellsii
A. xanthocephala
Section Caesareae
A. basii
A. caesarea
A. caesareoides
A. chepangiana
A. hemibapha
A. jacksonii
A. lanei
A. spreta
A. zambiana
Section Vaginatae
A. battarrae
A. beckeri
A. ceciliae
A. crocea
A. fulva
A. liquii
A. lividopallescens
A. nivalis
A. orientifulva
A. pachycolea
A. pekeoides
A. umbrinolutea
A. vaginata
A. velosa
 
A. albocreata
A. altipes
A. aprica
A. armeniaca
A. augusta
A. breckonii
A. eliae
A. farinosa
A. frostiana
A. gemmata
A. ibotengutake
A. multisquamosa
A. muscaria
A. nehuta
A. orientigemmata
A. parcivolvata
A. pantherina
A. parvipantherina
A. regalis
A. roseotincta
A. rubrovolvata
A. velatipes
A. virgineoides
A. viscidolutea
A. wellsii
A. xanthocephala
 
A. basii
A. caesarea
A. caesareoides
A. chepangiana
A. hemibapha
A. jacksonii
A. lanei
A. spreta
A. zambiana
 
A. battarrae
A. beckeri
A. ceciliae
A. crocea
A. fulva
A. liquii
A. lividopallescens
A. nivalis
A. orientifulva
A. pachycolea
A. pekeoides
A. umbrinolutea
A. vaginata
A. velosa
  Section Amidella
A. curtipes
A. ovoidea
A. proxima
A. volvata
Section Phalloideae
A. arocheae
A. bisporigera
A. exitialis
A. fuliginea
A. griseorosea
A. magnivelaris
A. marmorata subsp. myrtacearum
A. manginiana
A. molliuscula
A. ocreata
A. pallidorosea
A. parviexitialis
A. pseudoporphyria
A. phalloides
A. rimosa
A. subfuliginea
A. subjunquillea
A. subpallidorosea
A. verna
A. virosa
A. virosiformis
Section Roanokenses
A. abrupta
A. atkinsoniana
A. austroviridis
A. ananaeceps
A. cokeri
A. daucipes
A. echinocephala
A. magniverrucata
A. ochrophylla
A. ochrophylloides
A. onusta
A. ravenelii
A. rhopalopus
A. smithiana
A. solitaria
A. sphaerobulbosa
Section Validae
A. aestivalis
A. australis
A. brunnescens
A. brunneolocularis
A. citrina
A. excelsa
A. excelsa v. spissa
A. flavella
A. flavoconia
A. flavorubens
A. franchetii
A. luteofusca
A. nothofagi
A. novinupta
A. porphyria
A. rubescens
 
A. curtipes
A. ovoidea
A. proxima
A. volvata
 
A. arocheae
A. bisporigera
A. exitialis
A. fuliginea
A. griseorosea
A. magnivelaris
A. marmorata subsp. myrtacearum
A. manginiana
A. molliuscula
A. ocreata
A. pallidorosea
A. parviexitialis
A. pseudoporphyria
A. phalloides
A. rimosa
A. subfuliginea
A. subjunquillea
A. subpallidorosea
A. verna
A. virosa
A. virosiformis
 
A. abrupta
A. atkinsoniana
A. austroviridis
A. ananaeceps
A. cokeri
A. daucipes
A. echinocephala
A. magniverrucata
A. ochrophylla
A. ochrophylloides
A. onusta
A. ravenelii
A. rhopalopus
A. smithiana
A. solitaria
A. sphaerobulbosa
 
A. aestivalis
A. australis
A. brunnescens
A. brunneolocularis
A. citrina
A. excelsa
A. excelsa v. spissa
A. flavella
A. flavoconia
A. flavorubens
A. franchetii
A. luteofusca
A. nothofagi
A. novinupta
A. porphyria
A. rubescens
 Section Lepidella(=Saproamanita)
A. nauseosa
A. thiersii
A. vittadinii
 
A. nauseosa
A. thiersii
A. vittadinii
 Section AmanitaSpecies
A. albocreata
A. crenulata
A. farinosa
A. frostiana
A. gemmata
A. multisquamosa
A. muscaria
A. pantherina
A. porphyria
A. regalis
A. strobiliformis
A. xanthocephala
Compounds
Toxins: Ibotenic Acid
Muscimol
Other compounds: Muscarine
Muscazone
Amavadine
 Species
A. albocreata
A. crenulata
A. farinosa
A. frostiana
A. gemmata
A. multisquamosa
A. muscaria
A. pantherina
A. porphyria
A. regalis
A. strobiliformis
A. xanthocephala
Compounds
Toxins: Ibotenic Acid
Muscimol
Other compounds: Muscarine
Muscazone
Amavadine
 
A. albocreata
A. crenulata
A. farinosa
A. frostiana
A. gemmata
A. multisquamosa
A. muscaria
A. pantherina
A. porphyria
A. regalis
A. strobiliformis
A. xanthocephala
 
Toxins: Ibotenic Acid
Muscimol
Other compounds: Muscarine
Muscazone
Amavadine
  Subgenus Lepidella(=Saproamanita)Species
A. thiersii
Compounds
unknown toxin
Section PhalloideaeSpeciesDestroying angels
A. bisporigera
A. exitialis
A. magnivelaris
A. ocreata
A. verna
A. virosa
A. virosiformis
Other members
A. arocheae
A. phalloides
A. subjunquilea
CompoundsAmatoxins
Amanitins
alpha-
beta-
gamma-
epsilon-
Amanullin
Amanullinic acid
Amaninamide
Amanin
Proamanullin
Phallotoxins
Phallacidin
Phallacin
Phallisacin
Phallisin
Phalloidin
Phalloin
Prophalloin
Virotoxins
Alaviroidin
Viroisin
Deoxoviroisin
Viroidin
Deoxoviroidin
Other compounds
Antamanide
Phallolysin
Toxophallin
Section RoanokensesSpecies
A. proxima
A. smithiana
A. sphaerobulbosa
Compounds
Allenic norleucine (2-amino-4,5-hexadienoic acid)
Propargylglycine
Section ValidaeSpecies
A. rubescens (A. amerirubescens nom. prov.)
Compounds
Rubescenslysin
 Species
A. thiersii
Compounds
unknown toxin
 
A. thiersii
 
unknown toxin
 SpeciesDestroying angels
A. bisporigera
A. exitialis
A. magnivelaris
A. ocreata
A. verna
A. virosa
A. virosiformis
Other members
A. arocheae
A. phalloides
A. subjunquilea
CompoundsAmatoxins
Amanitins
alpha-
beta-
gamma-
epsilon-
Amanullin
Amanullinic acid
Amaninamide
Amanin
Proamanullin
Phallotoxins
Phallacidin
Phallacin
Phallisacin
Phallisin
Phalloidin
Phalloin
Prophalloin
Virotoxins
Alaviroidin
Viroisin
Deoxoviroisin
Viroidin
Deoxoviroidin
Other compounds
Antamanide
Phallolysin
Toxophallin
 Destroying angels
A. bisporigera
A. exitialis
A. magnivelaris
A. ocreata
A. verna
A. virosa
A. virosiformis
Other members
A. arocheae
A. phalloides
A. subjunquilea
 
A. bisporigera
A. exitialis
A. magnivelaris
A. ocreata
A. verna
A. virosa
A. virosiformis
 
A. arocheae
A. phalloides
A. subjunquilea
 Amatoxins
Amanitins
alpha-
beta-
gamma-
epsilon-
Amanullin
Amanullinic acid
Amaninamide
Amanin
Proamanullin
Phallotoxins
Phallacidin
Phallacin
Phallisacin
Phallisin
Phalloidin
Phalloin
Prophalloin
Virotoxins
Alaviroidin
Viroisin
Deoxoviroisin
Viroidin
Deoxoviroidin
Other compounds
Antamanide
Phallolysin
Toxophallin
 
Amanitins
alpha-
beta-
gamma-
epsilon-
Amanullin
Amanullinic acid
Amaninamide
Amanin
Proamanullin
 
Phallacidin
Phallacin
Phallisacin
Phallisin
Phalloidin
Phalloin
Prophalloin
 
Alaviroidin
Viroisin
Deoxoviroisin
Viroidin
Deoxoviroidin
 
Antamanide
Phallolysin
Toxophallin
 Species
A. proxima
A. smithiana
A. sphaerobulbosa
Compounds
Allenic norleucine (2-amino-4,5-hexadienoic acid)
Propargylglycine
 
A. proxima
A. smithiana
A. sphaerobulbosa
 
Allenic norleucine (2-amino-4,5-hexadienoic acid)
Propargylglycine
 Species
A. rubescens (A. amerirubescens nom. prov.)
Compounds
Rubescenslysin
 
A. rubescens (A. amerirubescens nom. prov.)
 
Rubescenslysin
 
Wikidata: Q588770
EoL: 6691958
Fungorum: 451380
GBIF: 5452199
iNaturalist: 67356
MycoBank: 451380
NCBI: 235532
 