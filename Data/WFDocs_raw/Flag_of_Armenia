
 The national flag of Armenia, the Armenian Tricolour, consists of three horizontal bands of equal width, red on the top, blue in the middle, and orange (also described as "colour of apricot") on the bottom. The Armenian Supreme Soviet adopted the current flag on 24 August 1990. On 15 June 2006, the Law on the National Flag of Armenia, governing its usage, was passed by the National Assembly of Armenia.
 Throughout history, there have been many variations of the Armenian flag. In ancient times, Armenian dynasties were represented by different symbolic animals displayed on their flags.[1] In the twentieth century, various Soviet flags represented the Armenian SSR.
 The meanings of the colors are interpreted in many different ways. For example, red stands for the blood of the 1.5 million Armenians killed in the Armenian Genocide, blue is for the Armenian pure sky, and orange represents the country's courage.[2]
 The official definition of the colors, as stated in the Constitution of the Republic of Armenia, is:
 The red emblematizes the Armenian Highland, the Armenian people's continued struggle for survival, maintenance of the Christian faith, Armenia's independence and freedom. The blue emblematizes the will of the people of Armenia to live beneath peaceful skies. The orange emblematizes the creative talent and hard-working nature of the people of Armenia.[3]
 In 2012, the Armenian National Institute of Standards (SARM) issued specifications about the construction and colors on the national flag.:[4]
 Today's tricolor flag bears little resemblance to the earliest Armenian 'flags'. In ancient times, armies went into battle behind carvings mounted on poles. The carvings might represent a dragon, an eagle, a lion or "some mysterious object of the gods".[1] With the advent of Christianity, the Armenian empire adopted many different flags representing various dynasties. The Artaxiad Dynasty's flag, for instance, consisted of a red cloth displaying two eagles gazing at each other, separated by a flower.
 After Armenia was split between the Persian and the Ottoman Empires, the idea of an Armenian flag ceased to exist for some time. The Armenian Catholic priest Father Ghevont Alishan created a new flag for Armenia in 1885, after the Armenian Students Association of Paris requested one for the funeral of the French writer Victor Hugo. Alishan's first design was very similar to today's Armenian flag: a horizontal tricolor. However, it looked more like an upside-down variation of the current flag of Bulgaria. The top band was red, symbolizing the first Sunday of Easter (called "Red" Sunday), followed by a green band to represent the "Green" Sunday of Easter, and finally an arbitrary color, white, was chosen to complete the combination.[1] While in France, Alishan also designed a second flag, identified today as the "Nationalist Armenian Flag". It too was a tricolor, but unlike the previous design, this one was a vertical tricolor similar to the French flag. Its colors were red, green, and blue, from left to right, representing the rainbow that Noah saw after landing on Mount Ararat.[1]
 In 1828, Persian Armenia was annexed to the Russian Empire after the last Russo-Persian War, and became known as Russian Armenia. When the Russian Empire collapsed, Russian Armenia declared its independence and joined the short-lived Transcaucasian Democratic Federative Republic, together with Georgia and Azerbaijan. This unified state hardly lasted a year and was soon dissolved. Since the Republic was short-lived, it did not use any flags or symbols. Nevertheless, some historians consider a horizontal gold, black, and red tricolor, similar to that of the German flag but arranged differently, to have been the flag of Transcaucasia.[5] The federation was dissolved on May 26, 1918, when Georgia declared its independence as the Democratic Republic of Georgia. Both Armenia and Azerbaijan declared their independence two days later, on May 28, 1918, as the First Republic of Armenia and the Azerbaijan Democratic Republic, respectively.
 [6]]]
After gaining independence, the First Republic of Armenia adopted the modern Armenian tricolor. Upon Stepan Malkhasyants's appearance in the Armenian National Council,[7] the independent Armenian government selected the colors used during the Lusignan period: red, blue and yellow. An earlier prototype, which was eventually rejected, was the rainbow flag. This prototype can be seen at the Martiros Saryan House Museum in Yerevan, Armenia. They chose to replace the yellow with orange "because it merged better with the other two colors, presenting a more pleasing composition".[1] The flag of independent Armenia then had a ratio of 2:3, but on August 24, 1990, when the Armenian Supreme Soviet adopted it as the flag of the Republic of Armenia, the ratio was changed to 1:2.[8]
 On November 29, 1920 Bolsheviks established the Armenian SSR. A new flag was introduced and fixed in the Constitution, accepted on February 2, 1922 by the First Congress of Soviets of the Armenian SSR.[9] That flag existed only for a month, because on March 12 the Armenian SSR united with the Georgian SSR and the Azerbaijan SSR under the Transcaucasian SFSR (TSFSR). On December 30, 1922 the Transcaucasian SFSR became one of the four Soviet republics that united to form the USSR. The flag of the republic had a hammer and sickle inserted into a star with initials "ЗСФСР" (ZSFSR) written in Russian sans-serif script. These letters stand for Закавказская Советская Федеративная Социалистическая Республика (Zakavkazskaya Sovetskaya Federativnaya Socialisticheskaya Respublika, "Transcaucasian Soviet Federative Socialist Republic").[9] In 1936, the TSFSR was broken up into its three constituent regions, which were named the Georgian SSR, the Armenian SSR, and the Azerbaijan SSR.
 As a republic of the USSR, the Armenian SSR  introduced its first flag in 1936. Very similar to the flag of the Soviet Union, it was red and featured a yellow hammer and sickle in the corner. Underneath that, there were "H-Kh-S-H" initials  written in Armenian serif script. These initials, in the Western Armenian language, stand for Haygagan Khorhurtayin Sodzialistakan Hanrabedutyun, or the "Armenian Soviet Socialist Republic".  In the 1940s, the flag was altered to use the Eastern Armenian language spoken in the Republic. The initials were changed to "H-S-S-R" meaning "Hayastani Sovetakan Sotsialistikakan Respublika" in the Eastern Armenian pronunciation. In 1952, a new flag was introduced. The initials were removed completely and in their place a horizontal blue stripe was added.
 In late May 1988, amid rising nationalist tensions from glasnost and perestroika, Armenia's new Communist party leader allowed the banned tricolour of the DRA to fly in Yerevan for the first time in over sixty years.[10] A year later, following a Nagorno-Karabakh-themed mass demonstration where the tricolour was flown, he urged its official recognition.[11] This came on August 24, 1990, a day after the Armenian Supreme Soviet declared the republic's sovereignty and renamed the country the Republic of Armenia. At that point, just over a year before Armenia declared its formal independence from the USSR, the tricolour replaced the 1952 flag.
 The 2006 law on the National Flag of Armenia states that the flag has to be raised on the following public buildings:
 The law requires the lowering of the flag to the midpoint of the flagpole on the days of mourning or during mourning ceremonies. A black ribbon needs to be placed at the top of the flag; the length of the ribbon should be equal to the length of the flag. The flying flag has to be raised in its entirety, clean, and unfaded; moreover, the lower part of the flag should be at least 2.5 m off the ground.[12]
 The day of the National Flag of Armenia is marked on 15 June every year. The day is chosen for the reason that the Armenian law on the National Flag of Armenia was passed on 15 June 2006.[13] The day of the Armenian tricolour was celebrated for the first time on 15 June 2010 in Yerevan.[14]
 The daily display of the Armenian flag is encouraged, but legally required only on the following days:[15][16]
 The national flag is also mentioned in the song "Mer Hayrenik" (Our Fatherland), the national anthem of Armenia. Specifically, the second and third stanzas sing about the creation of the national flag:
 Here is, brother, for you a flag,
That I made with my hands.
Nights I didn't sleep,
With tears I washed it.
(repeat previous two lines)

Look at it, the three colors
Which are our gifted symbol.
Let it be bright against the enemy.
Let Armenia always be glorious.
(repeat previous two lines)[17]
 On June 2, 1992, the self-proclaimed Republic of Artsakh, a de facto independent republic in South Caucasus claimed by Azerbaijan, adopted a flag derived from the flag of Armenia, with only a white pattern added. A white, five-toothed, stepped carpet pattern was added to the flag, beginning at the two verges of the cloth's right side and connecting at a point equal to one-third of the distance from that side.[18] The white pattern symbolizes the current separation of Artsakh from Armenia proper and its aspiration for eventual union with "the Fatherland".[19] This symbolises the Armenian heritage, culture and population of the area and represents Artsakh as being a separated region of Armenia by the triangular shape and the zigzag cutting through the flag. The pattern is also similar to the designs used on rugs. The ratio of the flag's breadth to its length is 1:2, same as the Armenian Tricolor.[18]
 In addition to the flag of Artsakh, the Armenian flag colors influenced the design of the Pan-Armenian Games flag. In the center of the light blue flag are six interlocking rings, derived from the Olympic rings. The sixth, orange-colored ring, interlocks with the blue and red rings, which symbolize Armenia. Above the rings is a flame in the colors of the Armenian flag.[20]
 1 Design 2 History

2.1 19th century
2.2 Transcaucasian Democratic Federative Republic
2.3 First Republic of Armenia
2.4 Early Soviet Armenia and the Transcaucasian SFSR
2.5 Armenian SSR

 2.1 19th century 2.2 Transcaucasian Democratic Federative Republic 2.3 First Republic of Armenia 2.4 Early Soviet Armenia and the Transcaucasian SFSR 2.5 Armenian SSR 3 Usage

3.1 National flag days

 3.1 National flag days 4 Influence

4.1 Flag of Artsakh
4.2 Flag of the Pan-Armenian Games

 4.1 Flag of Artsakh 4.2 Flag of the Pan-Armenian Games 5 See also 6 References 7 External links Residence of the President Parliament Government Constitutional Court Office of Public Prosecutor Central Bank of Armenia Other governmental buildings January 1, January 2 – New Year January 6 – Christmas March 8 – International Women's Day April 7 – Motherhood and Beauty Day May 1 – International Worker's Solidarity Day May 9 – Victory and Peace Day May 28 – First Armenian Republic Day, 1918 July 5 – Constitution Day, 1995 September 21 – Independence Day, 1991 December 7 – Spitak Earthquake Memorial Day, 1988 Coat of arms of Armenia List of Armenian flags Flag of the Armenian Soviet Socialist Republic ^ a b c d e "The Evolution of the Armenian Flag". Armenianheritage.com. Retrieved 2007-01-05..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ "Armenia". Vexilla Mundi. Retrieved 2007-01-06.
 ^ "General Information: section the Flag". Government of Republic of Armenia. Retrieved 2010-10-21.
 ^ "HST 50-2012 - General Specifications of the Republic of Armenia flag (AM)" (PDF). Retrieved 2011-12-18.
 ^ "Закавказская Федерация (Transcaucasian Democratic Federative Republic)" (in Russian). Russian Centre of Vexillology and Heraldry. 2003-05-30. Archived from the original on 2006-06-22. Retrieved 2006-12-27.
 ^ Hovhannissian, Petros (2009). "Հայաստանի աոաջին Հանրապետության պետական դրոշի՝ Մարտիրոս Սարյանի նախագիծը [The design of the national flag of the First Republic of Armenia by Martiros Sarian]". Etchmiadzin (in Armenian). Mother See of Holy Etchmiadzin. 65 (5): 118–119.
 ^ "Республика Армения (Republic of Armenia)" (in Russian). Russian Centre of Vexillology and Heraldry. 2003-03-28. Archived from the original on 2006-06-22. Retrieved 2006-01-09.
 ^ "Armenia: First Republic (1918–1921)". Flags of the World. Archived from the original on 2006-12-07. Retrieved 2007-01-09.
 ^ a b "Cоветская Армения (Soviet Armenia)" (in Russian). Russian Centre of Vexillology and Heraldry. 2004-11-14. Retrieved 2007-01-20.
 ^ De Waal, Thomas. Black Garden: Armenia and Azerbaijan Through Peace and War, pp. 60–1. NYU Press, 2003, ISBN 0-8147-1945-7.
 ^ King, Sarah Sanderson and Cushman, Donald P. Political Communication: Engineering Visions of Order in the Socialist World, p. 102. SUNY Press, 1992, ISBN 0-7914-1201-6.
 ^ "The Law of the Republic of Armenia about the national flag" (in Armenian). Armenian Legal Information System. Retrieved 2008-03-28.
 ^ "Demotix news: State Flag Day celebrations in Armenia". Archived from the original on 2010-06-20. Retrieved 2010-06-15.
 ^ "Yerevan Report: Armenian Flag Day Marked on June 15". Archived from the original on 2013-02-10. Retrieved 2010-06-16.
 ^ "Flag Days Of The World". Flags of the World. Retrieved 2006-12-29.
 ^ "About Armenia". Permanent Mission of Armenia to the United Nations. Archived from the original on 2007-06-13. Retrieved 2006-12-29.
 ^ "Lyrics of Mer Hayrenik". National Anthems.net. Retrieved 2007-01-10.
 ^ a b "Attributes of Statehood". Ministry of Foreign Affairs of Artsakh. Archived from the original on December 16, 2006. Retrieved 2007-01-09.
 ^ "Flag of Artsakh". Flags of the World. Retrieved 2007-01-09.
 ^ "Armenia: Sport flags". Flags of the World. Retrieved 2007-01-09.
 Armenia at Flags of the World (in Russian) Flags of Armenia Armenica.org - Symbolic values and information about the Armenian flag and coat of arms Yeraguyn.com - The Flag of the Republic of Armenia (in English, Armenian and Russian) v t e Origins Name Kura–Araxes culture Hayk Hayasa-Azzi Mitanni Nairi Kingdom of Urartu Median kingdom Orontid Dynasty Achaemenid Empire
Satrapy of Armenia Satrapy of Armenia Kingdom of Armenia Roman Armenia Parthian Empire Byzantine Armenia Sasanian Armenia Arminiya Sajids Bagratuni Armenia Armenian Kingdom of Cilicia Sallarids Ilkhanate Chobanids Aq Qoyunlu Kara Koyunlu Ottoman Armenia 1508–1828 Iranian Armenia Safavid Iran Afsharid Iran Qajar Iran
Erivan Khanate
Karabakh Khanate
Treaty of Turkmenchay Erivan Khanate Karabakh Khanate Treaty of Turkmenchay Russian Armenia First Republic of Armenia Soviet Armenia Independent Armenia Armenian Genocide Nagorno-Karabakh conflict Armenian national liberation movement more... Ararat Plain Armenian Highlands Cities Earthquakes Extreme points Lake Sevan Mountains Municipalities Rivers and lakes Shikahogh State Reserve Shirak Plain more... Administrative divisions Constitution Corruption Elections Foreign relations Government Human rights Judiciary Military National Assembly National Security Service Police Political parties President Prime Minister President of the National Assembly more on government on politics Agriculture Armex (stock exchange) Central Bank Dram (currency) Energy Mining Pension reform Telecommunications Tourism Transport Waste management Alphabet Architecture Art Cinema Cuisine Dance Language
Eastern
Western Eastern Western Literature Music Sport Theatre more... Census Crime Education Ethnic minorities Health People
diaspora diaspora Social issues Women more... Armenian Apostolic Church Armenian Catholic Church Armenian Evangelical Church Armenian Brotherhood Church Judaism Islam more... Armenian Cross Armenian eternity sign Coat of arms Flag Mount Ararat National anthem Apricot Grape Pomegranate Outline Index Book Category v t e Albania Andorra Armenia Austria Azerbaijan Belarus Belgium Bosnia and Herzegovina Bulgaria Croatia Cyprus Czech Republic Denmark Estonia Finland France Georgia Germany Greece Hungary Iceland Ireland  Italy Kazakhstan Latvia Liechtenstein Lithuania Luxembourg Malta Moldova Monaco Montenegro Netherlands North Macedonia Norway Poland Portugal Romania Russia San Marino Serbia Slovakia Slovenia Spain Sweden Switzerland Turkey Ukraine United Kingdom
England
Northern Ireland
Scotland
Wales England Northern Ireland Scotland Wales Vatican City Abkhazia Artsakh Kosovo Northern Cyprus South Ossetia Transnistria Åland Faroe Islands Gibraltar Guernsey Isle of Man Jersey Svalbard European Union Sovereign Military Order of Malta v t e Afghanistan Armenia Azerbaijan Bahrain Bangladesh Bhutan Brunei Cambodia China Cyprus East Timor (Timor-Leste) Egypt Georgia India Indonesia Iran Iraq Israel Japan Jordan Kazakhstan North Korea South Korea Kuwait Kyrgyzstan Laos Lebanon Malaysia Maldives Mongolia Myanmar Nepal Oman Pakistan Philippines Qatar Russia Saudi Arabia Singapore Sri Lanka Syria Tajikistan Thailand Turkey Turkmenistan United Arab Emirates Uzbekistan Vietnam Yemen Abkhazia Artsakh Northern Cyprus Palestine South Ossetia Taiwan British Indian Ocean Territory Christmas Island Cocos (Keeling) Islands Hong Kong Macau  Book  Category  Asia portal v t e Sovereign states Dependent territories Timeline Sovereign states Dependent territories National flags National symbols of Armenia Flags of Armenia Flags introduced in 1918 Flags introduced in 1990 CS1 Russian-language sources (ru) CS1 Armenian-language sources (hy) Articles with short description Featured articles Articles containing Armenian-language text Commons category link from Wikidata Articles with Russian-language external links Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Afrikaans አማርኛ العربية Azərbaycanca تۆرکجه Беларуская Беларуская (тарашкевіца)‎ Български Bosanski Brezhoneg Català Čeština Cymraeg Dansk Deutsch Eesti Ελληνικά Español Esperanto Euskara فارسی Français Galego ગુજરાતી 한국어 Հայերեն हिन्दी Hrvatski বিষ্ণুপ্রিয়া মণিপুরী Bahasa Indonesia Italiano עברית ქართული Latviešu Lietuvių Magyar Македонски मराठी Bahasa Melayu Nederlands 日本語 Norsk Norsk nynorsk Oʻzbekcha/ўзбекча ਪੰਜਾਬੀ Polski Português Română Русский Scots Simple English Slovenčina Српски / srpski Srpskohrvatski / српскохрватски Sunda Suomi Svenska Tagalog ไทย Türkçe Українська Vepsän kel’ Tiếng Việt Yorùbá 中文  This page was last edited on 21 October 2019, at 14:22 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  The red emblematizes the Armenian Highland, the Armenian people's continued struggle for survival, maintenance of the Christian faith, Armenia's independence and freedom. The blue emblematizes the will of the people of Armenia to live beneath peaceful skies. The orange emblematizes the creative talent and hard-working nature of the people of Armenia.[3]
 
Here is, brother, for you a flag,
That I made with my hands.
Nights I didn't sleep,
With tears I washed it.
(repeat previous two lines)

Look at it, the three colors
Which are our gifted symbol.
Let it be bright against the enemy.
Let Armenia always be glorious.
(repeat previous two lines)[17]


 Flag of Armenia a b c d e ^ ^ ^ ^ ^ 65 ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ National flag of Armenia   Armenian: Եռագոյն (Yeřagoyn) National flag 1:2 24 August 1990 A horizontal tricolour of red, blue, and orange Stepan Malkhasyants 
  Variant flag of  Flag of Armenia Presidential Standard 1:2 A horizontal tricolour of red, blue, and orange defaced with the Presidential seal at its centre. 
 Pantone
 485
 286
 1235
 CMYK
 0-100-100-0
 100-80-0-0
 0-35-100-0
 RGB
 217-0-18
 0-51-160
 242-168-0
 RGB hex triplet
 D90012
 0033A0
 F2A800
   National flag 1:2 June 2, 1992 A horizontal tricolor of red, blue, and orange with a white sideways zig-zag chevron in the fly 
  Wikimedia Commons has media related to National flag of Armenia. Early
Origins
Name
Kura–Araxes culture
Hayk
Hayasa-Azzi
Mitanni
Nairi
Kingdom of Urartu
Median kingdom
Orontid Dynasty
Achaemenid Empire
Satrapy of Armenia
Kingdom of Armenia
Roman Armenia
Parthian Empire
Byzantine Armenia
Sasanian Armenia
Middle
Arminiya
Sajids
Bagratuni Armenia
Armenian Kingdom of Cilicia
Sallarids
Ilkhanate
Chobanids
Aq Qoyunlu
Kara Koyunlu
Ottoman Armenia
1508–1828 Iranian Armenia
Safavid Iran
Afsharid Iran
Qajar Iran
Erivan Khanate
Karabakh Khanate
Treaty of Turkmenchay
Russian Armenia
Modern
First Republic of Armenia
Soviet Armenia
Independent Armenia
By topic
Armenian Genocide
Nagorno-Karabakh conflict
Armenian national liberation movement
more...
 
Origins
Name
Kura–Araxes culture
Hayk
Hayasa-Azzi
Mitanni
Nairi
Kingdom of Urartu
Median kingdom
Orontid Dynasty
Achaemenid Empire
Satrapy of Armenia
Kingdom of Armenia
Roman Armenia
Parthian Empire
Byzantine Armenia
Sasanian Armenia
 
Arminiya
Sajids
Bagratuni Armenia
Armenian Kingdom of Cilicia
Sallarids
Ilkhanate
Chobanids
Aq Qoyunlu
Kara Koyunlu
Ottoman Armenia
1508–1828 Iranian Armenia
Safavid Iran
Afsharid Iran
Qajar Iran
Erivan Khanate
Karabakh Khanate
Treaty of Turkmenchay
Russian Armenia
 
First Republic of Armenia
Soviet Armenia
Independent Armenia
 
Armenian Genocide
Nagorno-Karabakh conflict
Armenian national liberation movement
more...
 
Ararat Plain
Armenian Highlands
Cities
Earthquakes
Extreme points
Lake Sevan
Mountains
Municipalities
Rivers and lakes
Shikahogh State Reserve
Shirak Plain
more...
 
Ararat Plain
Armenian Highlands
Cities
Earthquakes
Extreme points
Lake Sevan
Mountains
Municipalities
Rivers and lakes
Shikahogh State Reserve
Shirak Plain
more...
 
Administrative divisions
Constitution
Corruption
Elections
Foreign relations
Government
Human rights
Judiciary
Military
National Assembly
National Security Service
Police
Political parties
President
Prime Minister
President of the National Assembly
more on government
on politics
 
Administrative divisions
Constitution
Corruption
Elections
Foreign relations
Government
Human rights
Judiciary
Military
National Assembly
National Security Service
Police
Political parties
President
Prime Minister
President of the National Assembly
more on government
on politics
 
Agriculture
Armex (stock exchange)
Central Bank
Dram (currency)
Energy
Mining
Pension reform
Telecommunications
Tourism
Transport
Waste management
 
Agriculture
Armex (stock exchange)
Central Bank
Dram (currency)
Energy
Mining
Pension reform
Telecommunications
Tourism
Transport
Waste management
 
Alphabet
Architecture
Art
Cinema
Cuisine
Dance
Language
Eastern
Western
Literature
Music
Sport
Theatre
more...
Demographics
Census
Crime
Education
Ethnic minorities
Health
People
diaspora
Social issues
Women
more...
Religion
Armenian Apostolic Church
Armenian Catholic Church
Armenian Evangelical Church
Armenian Brotherhood Church
Judaism
Islam
more...
Symbols
Armenian Cross
Armenian eternity sign
Coat of arms
Flag
Mount Ararat
National anthem
Apricot
Grape
Pomegranate
 
Alphabet
Architecture
Art
Cinema
Cuisine
Dance
Language
Eastern
Western
Literature
Music
Sport
Theatre
more...
 
Census
Crime
Education
Ethnic minorities
Health
People
diaspora
Social issues
Women
more...
 
Armenian Apostolic Church
Armenian Catholic Church
Armenian Evangelical Church
Armenian Brotherhood Church
Judaism
Islam
more...
 
Armenian Cross
Armenian eternity sign
Coat of arms
Flag
Mount Ararat
National anthem
Apricot
Grape
Pomegranate
 OutlineIndex
Book
Category 
Albania
Andorra
Armenia
Austria
Azerbaijan
Belarus
Belgium
Bosnia and Herzegovina
Bulgaria
Croatia
Cyprus
Czech Republic
Denmark
Estonia
Finland
France
Georgia
Germany
Greece
Hungary
Iceland
Ireland

Italy
Kazakhstan
Latvia
Liechtenstein
Lithuania
Luxembourg
Malta
Moldova
Monaco
Montenegro
Netherlands
North Macedonia
Norway
Poland
Portugal
Romania
Russia
San Marino
Serbia
Slovakia
Slovenia
Spain
Sweden
Switzerland
Turkey
Ukraine
United Kingdom
England
Northern Ireland
Scotland
Wales
Vatican City
  
Abkhazia
Artsakh
Kosovo
Northern Cyprus
South Ossetia
Transnistria
 
Åland
Faroe Islands
Gibraltar
Guernsey
Isle of Man
Jersey
Svalbard
 
European Union
Sovereign Military Order of Malta
 
Afghanistan
Armenia
Azerbaijan
Bahrain
Bangladesh
Bhutan
Brunei
Cambodia
China
Cyprus
East Timor (Timor-Leste)
Egypt
Georgia
India
Indonesia
Iran
Iraq
Israel
Japan
Jordan
Kazakhstan
North Korea
South Korea
Kuwait
Kyrgyzstan
Laos
Lebanon
Malaysia
Maldives
Mongolia
Myanmar
Nepal
Oman
Pakistan
Philippines
Qatar
Russia
Saudi Arabia
Singapore
Sri Lanka
Syria
Tajikistan
Thailand
Turkey
Turkmenistan
United Arab Emirates
Uzbekistan
Vietnam
Yemen
 
Abkhazia
Artsakh
Northern Cyprus
Palestine
South Ossetia
Taiwan
 
British Indian Ocean Territory
Christmas Island
Cocos (Keeling) Islands
Hong Kong
Macau
 
 Book
 Category
 Asia portal
 
Sovereign states
Dependent territories
Timeline
 
Sovereign states
Dependent territories
  Heraldry portal 