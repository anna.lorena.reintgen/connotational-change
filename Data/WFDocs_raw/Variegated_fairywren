
 The variegated fairywren (Malurus lamberti) is a fairywren that lives in eastern Australia. In a species that exhibits sexual dimorphism, the brightly coloured breeding male has chestnut shoulders and azure crown and ear coverts, while non-breeding males, females and juveniles have predominantly grey-brown plumage, although females of two subspecies have mainly blue-grey plumage.
 Like other fairywrens, the variegated fairywren is a cooperative breeding species, with small groups of birds maintaining and defending small territories year-round. Groups consist of a socially monogamous pair with several helper birds who assist in raising the young. Male wrens pluck yellow petals and display them to females as part of a courtship display. These birds are primarily insectivorous and forage and live in the shelter of scrubby vegetation east of the Great Dividing Range. Populations across central, northern and western Australia were considered subspecies of this species until 2018, when they were reclassified as the purple-backed fairywren.
 The variegated fairywren was originally described by Nicholas Aylward Vigors and Thomas Horsfield in 1827,[2] and was at first considered a colour variant of the superb fairywren.[3] It is one of eleven species of the genus Malurus, commonly known as fairywrens, found in Australia and lowland New Guinea.[4] Within the genus it belongs to a group of four very similar species known collectively as chestnut-shouldered fairywrens. There are well-defined borders between the variegated fairywren and the other chestnut-shouldered wrens in the group, which are the lovely fairywren, red-winged fairywren, and the blue-breasted fairywren.[5] Gregory Mathews erected the genus Leggeornis for the group, with the variegated fairywren as the type species.[6] However, the genus has been reclassified as a subgenus within Malurus.[7]
 Like other fairywrens, the variegated fairywren is unrelated to the true wrens. Initially, fairywrens were thought to be a member of the Old World flycatcher family, Muscicapidae, or the warbler family, Sylviidae, before being placed in the newly recognised Maluridae in 1975.[8] More recently, DNA analysis has shown the family to be related to the honeyeaters (Meliphagidae) and the pardalotes (Pardalotidae) in a large superfamily Meliphagoidea.[9][10]
 "Variegated fairywren" has been designated the official name by the International Ornithologists' Union (IOC).[11] The scientific name commemorates the British collector Aylmer Bourke Lambert. The variegated fairywren was formerly known as the variegated wren, until 1978 when the RAOU pushed for the current name to be used. It is also known as Lambert's wren.[12]
 In his 1982 monograph, ornithologist Richard Schodde proposed a northern origin for the chestnut-shouldered fairywren group due to the variety of forms in north and their absence in the southeast of the continent. Ancestral birds spread south and colonised the southwest during a warm wetter period around 2 million years ago at the end of the Pliocene or beginning of the Pleistocene. Subsequent cooler and drier conditions resulted in loss of habitat and fragmentation of populations. Southwestern birds gave rise to what is now the red-winged fairywren, while those in the northwest of the continent became the variegated fairywren and yet another isolated in the northeast became the lovely fairywren. Further warmer, humid conditions again allowed birds to spread southwards, this group occupying central southern Australia east to the Eyre Peninsula became the blue-breasted fairywren. Cooler climate after this resulted in this being isolated as well and evolving into a separate species. Finally, after the end of the last glacial period 12,000–13,000 years ago, the northern variegated forms have again spread southwards, resulting in the purple-backed fairywren. This has resulted in the variegated fairywren's range to overlap with all three other species. Schodde also proposed that the blue-grey coloured females of the lavender-flanked subspecies were ancestral, while the browner coloration of females of southern forms was an adaptation to dry climates. Further molecular studies may result in this hypothesis being modified.[13]
 A 2017 molecular analysis by Alison J. McLean and colleagues of the former subspecies of the variegated fairywren largely supported Schodde's hypothesis. The Great Dividing Range was a major barrier and there is a deep genetic split between the variegated fairywren to its east, and the purple-backed fairywren to the west.[14] In 2018, this split was recognized by the International Ornithological Committee with the subspecies to the north and west reallocated to the purple-backed fairywren.[15]
 The variegated fairywren is 14–15 cm (5.5–6 in) long[16] and weighs 6–11 g (0.21–0.39 oz).[17] Like other fairywrens, it is notable for its marked sexual dimorphism, males adopting a highly visible breeding plumage of brilliant iridescent blue and chestnut contrasting with black and grey-brown. The brightly coloured crown and ear tufts are prominently featured in breeding displays.[18] The male in breeding plumage has striking bright blue ear coverts, with the crown often slightly darker, a black throat and nape, a royal blue upper back, chestnut shoulders and a bluish-grey tail. The wings are grey-brown and the belly creamy white. Non-breeding males, females and juveniles are predominantly grey-brown in colour; all males have a black bill and lores (eye-ring and bare skin between eyes and bill), while females have a red-brown bill and bright rufous lores. Immature males will develop black bills by six months of age,[19] and moult into breeding plumage the first breeding season after hatching, though this may be incomplete with residual brownish plumage and may take another year or two to perfect.[20] Both sexes moult in autumn after breeding, with males assuming an eclipse non-breeding plumage. They will moult again into nuptial plumage in winter or spring.[21] The blue coloured plumage, particularly the ear-coverts, of the breeding males is highly iridescent due to the flattened and twisted surface of the barbules.[21] The blue plumage also reflects ultraviolet light strongly, and so may be even more prominent to other fairywrens, whose colour vision extends into that part of the spectrum.[22]
 Vocal communication among variegated fairywrens is used primarily for communication between birds in a social group and for advertising and defending a territory.[23] The basic song type is a high-pitched reel of a large number of short elements (10–20 per second); this lasts 1–4 seconds. The reel of the variegated fairywren is the softest of all malurids.[24] Birds maintain contact with each other by tsst or seeee calls, while a short, sharp tsit serves as an alarm call.[17]
 The variegated fairywren is found in scrubland with plenty of vegetation providing dense cover.[25] They have been reported to shelter in mammal burrows to avoid extreme heat.[26] In urban situations such as suburban Sydney, these wrens have been said prefer areas with more cover than the related superb fairywren,[27] though a 2007 survey in Sydney's northern suburbs has proposed that variegated fairywrens may prefer areas of higher plant diversity rather than denser cover as such.[28] Forestry plantations of pine and eucalypts are generally unsuitable as they lack undergrowth.[29]
 Like all fairywrens, the variegated fairywren is an active and restless feeder, particularly on open ground near shelter, but also through the lower foliage. Movement is a series of jaunty hops and bounces,[30] its balance assisted by a relatively large tail, which is usually held upright, and rarely still. The short, rounded wings provide good initial lift and are useful for short flights, though not for extended jaunts.[31] During spring and summer, birds are active in bursts through the day and accompany their foraging with song. Insects are numerous and easy to catch, which allows the birds to rest between forays. The group often shelters and rests together during the heat of the day. Food is harder to find during winter and they are required to spend the day foraging continuously.[32]
 Like other fairywrens, male variegated fairywrens have been observed carrying brightly coloured petals to display to females as part of a courtship ritual. In this species, the petals that have been recorded have been yellow.[33] Petals are displayed and presented to a female in the male fairywren's own or another territory.[34]
 The variegated fairywren is a cooperative breeding species, with pairs or small groups of birds maintaining and defending small territories year-round. Though less studied than the superb- and splendid fairywrens, it is presumably socially monogamous and sexually promiscuous, with each partner mating with other individuals.[25] Females and males feed young equally, while helper birds assist in defending the territory and feeding and rearing the young.[35] Birds in a group roost side-by-side in dense cover as well as engage in mutual preening.[25] Occasionally larger groups of around 10 birds have been recorded, though it is unclear whether this was incidental or a defined flock.[25]
 Breeding occurs from spring through to late summer; the nest is generally situated in thick vegetation and less than 1 m (3.3 ft) above the ground. It is a round or domed structure made of loosely woven grasses and spider webs, with an entrance in one side. Two or more broods may be laid in an extended breeding season. A clutch consists of three or four matte white eggs with reddish-brown splotches and spots, measuring 12 mm × 16 mm (0.47 in × 0.63 in).[36] The female incubates the eggs for 14 to 16 days, after which newly hatched nestlings are fed and their fecal sacs removed by all group members for 10–12 days, by which time they are fledged. Parents and helper birds will feed them for around one month. Young birds often remain in the family group as helpers for a year or more before moving to another group, though some move on and breed in the first year.[25] Variegated fairywrens commonly play host to the brood parasite Horsfield's bronze cuckoo and, less commonly, the brush cuckoo and fan-tailed cuckoo.[37]
 The variegated fairywren consumes a wide range of small creatures, mostly insects, including ants, grasshoppers, bugs, flies, weevils and various larvae.[38] Unlike the more ground-foraging superb fairywrens, they mostly forage deep inside shrubby vegetation, which is less than 2 m (7 ft) above the ground.[35]
 Major nest predators include Australian magpies, butcherbirds, laughing kookaburra, currawongs, crows and ravens, and shrike-thrushes, as well as introduced mammals such as the red fox, feral cats and black rat.[39] The variegated fairywren readily adopts a 'rodent-run' display to distract predators from nests with young birds. The head, neck and tail are lowered, the wings are held out and the feathers are fluffed as the bird runs rapidly and voices a continuous alarm call.[25]
 The variegated fairywren appeared on a 45c postage stamp in the Australia Post Nature of Australia – Desert issue released in June 2002.[40]
 
 1 Taxonomy and systematics

1.1 Evolutionary history

 1.1 Evolutionary history 2 Description

2.1 Vocalisations

 2.1 Vocalisations 3 Distribution and habitat 4 Behaviour and ecology

4.1 Breeding
4.2 Food and feeding
4.3 Threats

 4.1 Breeding 4.2 Food and feeding 4.3 Threats 5 Cultural depictions 6 References

6.1 Cited text

 6.1 Cited text 7 External links ^ BirdLife International (2017). "Malurus lamberti". The IUCN Red List of Threatened Species. IUCN. 2017: e.T22703744A118653224. doi:10.2305/IUCN.UK.2017-3.RLTS.T22703744A118653224.en..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ Vigors NA, Horsfield T (1827). "A description of the Australian birds in the collection of the Linnean Society; with an attempt at arranging them according to their natural affinities" (PDF). Trans. Linn. Soc. Lond. 15: 170–331. doi:10.1111/j.1095-8339.1826.tb00115.x.
 ^ Rowley & Russell 1997, p. 160.
 ^ Rowley & Russell 1997, p. 143.
 ^ Rowley & Russell 1997, p. 159.
 ^ Mathews GM (1923). The Birds of Australia. Supplement 2. London: Witherby & Co. p. 94.
 ^ Australian Biological Resources Study (28 February 2013). "Subgenus Malurus (Leggeornis) Mathews, 1912". Australian Faunal Directory. Canberra, Australian Capital Territory: Department of the Environment, Water, Heritage and the Arts, Australian Government. Retrieved 8 February 2018.[permanent dead link]
 ^ Schodde R (1975). "Interim List of Australian Songbirds". Melbourne: RAOU. Cite journal requires |journal= (help)
 ^ Barker, FK; Barrowclough GF; Groth JG (2002). "A phylogenetic hypothesis for passerine birds: taxonomic and biogeographic implications of an analysis of nuclear DNA sequence data". Proc. R. Soc. Lond. B. 269 (1488): 295–308. doi:10.1098/rspb.2001.1883. PMC 1690884. PMID 11839199.
 ^ Barker, FK; Cibois A; Schikler P; Feinstein J; Cracraft J (2004). "Phylogeny and diversification of the largest avian radiation" (PDF). Proc. Natl. Acad. Sci. USA. 101 (30): 11040–11045. doi:10.1073/pnas.0401892101. PMC 503738. PMID 15263073. Archived from the original (PDF) on 2007-10-25. Retrieved 2007-10-12.
 ^ Gill, Frank; Donsker, David, eds. (2017). "Lyrebirds, scrubbirds, bowerbirds, Australasian wrens". World Bird List Version 7.1. International Ornithologists' Union. Retrieved 27 January 2018.
 ^ Gray, Jeannie; Fraser, Ian (2013). Australian Bird Names: A Complete Guide. Collingwood, Victoria: Csiro Publishing. pp. 170, 174. ISBN 978-0-643-10471-6.
 ^ Schodde, R. (1982). The fairywrens: a monograph of the Maluridae. Melbourne: Lansdowne Editions. ISBN 0-7018-1051-3.
 ^ McLean AJ, Toon A, Schmidt DJ, Hughes JM, Joseph L (2017). "Phylogeography and geno-phenotypic discordance in a widespread Australian bird, the Variegated Fairy-wren, Malurus lamberti (Aves: Maluridae)". Biol J Linn Soc. 121 (3): 655–669. doi:10.1093/biolinnean/blx004.
 ^ Gill, Frank; Donsker, David, eds. (2018). "Species Updates". World Bird List Version 8.1. International Ornithologists' Union. Retrieved 26 January 2018.
 ^ Simpson K, Day N, Trusler P (1993). Field Guide to the Birds of Australia. Ringwood, Victoria: Viking O'Neil. p. 392. ISBN 0-670-90478-3.
 ^ a b Rowley & Russell 1997, p. 162.
 ^ Rowley & Russell 1997, pp. 143–44.
 ^ Rowley & Russell 1997, pp. 160–61.
 ^ Rowley & Russell 1997, p. 145.
 ^ a b Rowley & Russell 1997, p. 144.
 ^ Bennett AT, Cuthill IC (1994). "Ultraviolet vision in birds: what is its function?". Vision Research. 34 (11): 1471–78. doi:10.1016/0042-6989(94)90149-X. PMID 8023459.
 ^ Rowley & Russell 1997, p. 163.
 ^ Rowley & Russell 1997, pp. 165-66.
 ^ a b c d e f Rowley & Russell 1997, p. 164.
 ^ Marchant S (1992). "A bird observatory at Moruya, N.S.W. 1975–84". Eurobodalla Natural History Society, Occasional Publication (1): 1–99.
 ^ Roberts, Peter (1993). Birdwatcher's Guide to the Sydney Region. Kenthurst, New South Wales: Kangaroo Press. p. 131. ISBN 0-86417-565-5.
 ^ Dalby-Ball, Mia (2007). "Results in of Inaugural Fairy Wren Survey". Pittwater Council website. Pittwater Council. Archived from the original on November 10, 2007. Retrieved 2007-10-23.
 ^ Rowley & Russell 1997, p. 134.
 ^ Rowley & Russell 1997, p. 142.
 ^ Rowley & Russell 1997, p. 141.
 ^ Rowley & Russell 1997, p. 161-62.
 ^ Strong M, Cuffe E (1985). "Petal display by the Variegated Wren". Sunbird. 15: 71.
 ^ Rowley & Russell 1997, p. 75.
 ^ a b Tidemann SC (1986). "Breeding in Three Species of Fairy-Wrens (Malurus): Do Helpers Really Help?" (Abstract). Emu. 86 (3): 131–38. doi:10.1071/MU9860131. Retrieved 2007-09-18.
 ^ Beruldsen, G (2003). Australian Birds: Their Nests and Eggs. Kenmore Hills, Qld: self. p. 279. ISBN 0-646-42798-9.
 ^ Rowley & Russell 1997, p. 119.
 ^ Barker RD, Vestkens WJ (1990). Food of Australian Birds: Vol. 2 – Passerines. CSIRO. p. 557.
 ^ Rowley & Russell 1997, p. 121.
 ^ "Desert Birds". Australian Stamps. Australia Post. 9 August 2001. Archived from the original on 8 September 2007. Retrieved 2007-10-13.
 Rowley, Ian; Russell, Eleanor (1997). Bird Families of the World:Fairy-wrens and Grasswrens. Oxford, United Kingdom: Oxford University Press. ISBN 0-19-854690-4. BirdLife Species Factsheet Variegated fairywren videos on the Internet Bird Collection Variegated fairywren stamp v t e Broad-billed fairywren Campbell's fairywren Emperor fairywren Lovely fairywren Purple-backed fairywren Variegated fairywren Blue-breasted fairywren Red-winged fairywren Superb fairywren Splendid fairywren Purple-crowned fairywren White-shouldered fairywren Red-backed fairywren White-winged fairywren Southern emu-wren Mallee emu-wren Rufous-crowned emu-wren Grey grasswren Black grasswren White-throated grasswren Carpentarian grasswren Short-tailed grasswren Striated grasswren Eyrean grasswren Western grasswren Thick-billed grasswren Dusky grasswren Kalkadoon grasswren Australia portal Birds portal Wikidata: Q1164379 Wikispecies: Malurus lamberti ADW: Malurus_lamberti Avibase: 468816FA BirdLife: 22703744 BirdLife-Australia: variegated-fairy-wren eBird: varfai5 EoL: 918367 GBIF: 2487495 iNaturalist: 12097 IRMNG: 11435406 ITIS: 560976 IUCN: 22703744 NCBI: 228352 uBio: 2337134 IUCN Red List least concern species Malurus Birds of Australia Endemic birds of Australia Birds described in 1827 CS1: long volume value All articles with dead external links Articles with dead external links from May 2018 Articles with permanently dead external links CS1 errors: missing periodical Wikipedia indefinitely move-protected pages Articles with 'species' microformats Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Wikispecies Create a book Download as PDF Printable version Asturianu বাংলা Български Brezhoneg Català Cebuano Cymraeg Dansk Diné bizaad Español Euskara فارسی Français Magyar Nederlands 日本語 پنجابی Polski Русский Suomi Svenska Türkçe Tiếng Việt Winaray  This page was last edited on 19 September 2019, at 17:53 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  M. lamberti Malurus lamberti variegated fairywren ^ 2017 ^ 15 ^ ^ ^ ^ ^ ^ ^ 269 ^ 101 ^ ^ ^ ^ 121 ^ ^ a b ^ ^ ^ a b ^ 34 ^ ^ a b c d e f ^ ^ ^ ^ ^ ^ ^ ^ 15 ^ a b 86 ^ ^ ^ ^ ^ Variegated fairywren Subfamily Malurinae Sipodotus Chenorhamphus Malurus Clytomyias Stipiturus Subfamily Amytornithinae Amytornis 
 Male in breeding plumage in Brisbane, subspecies lamberti
 Least Concern (IUCN 3.1)[1]
 Kingdom:
 Animalia
 Phylum:
 Chordata
 Class:
 Aves
 Order:
 Passeriformes
 Family:
 Maluridae
 Genus:
 Malurus
 Species:
 M. lamberti
 Malurus lambertiVigors & Horsfield, 1827
  Wikimedia Commons has media related to Variegated fairywren. Tribe MaluriniSipodotusWallace's fairywrenChenorhamphus
Broad-billed fairywren
Campbell's fairywren
Malurus
Emperor fairywren
Lovely fairywren
Purple-backed fairywren
Variegated fairywren
Blue-breasted fairywren
Red-winged fairywren
Superb fairywren
Splendid fairywren
Purple-crowned fairywren
White-shouldered fairywren
Red-backed fairywren
White-winged fairywren
ClytomyiasOrange-crowned fairywrenTribe StipituriniStipiturus
Southern emu-wren
Mallee emu-wren
Rufous-crowned emu-wren
 SipodotusWallace's fairywrenChenorhamphus
Broad-billed fairywren
Campbell's fairywren
Malurus
Emperor fairywren
Lovely fairywren
Purple-backed fairywren
Variegated fairywren
Blue-breasted fairywren
Red-winged fairywren
Superb fairywren
Splendid fairywren
Purple-crowned fairywren
White-shouldered fairywren
Red-backed fairywren
White-winged fairywren
ClytomyiasOrange-crowned fairywren Wallace's fairywren 
Broad-billed fairywren
Campbell's fairywren
 
Emperor fairywren
Lovely fairywren
Purple-backed fairywren
Variegated fairywren
Blue-breasted fairywren
Red-winged fairywren
Superb fairywren
Splendid fairywren
Purple-crowned fairywren
White-shouldered fairywren
Red-backed fairywren
White-winged fairywren
 Orange-crowned fairywren Stipiturus
Southern emu-wren
Mallee emu-wren
Rufous-crowned emu-wren
 
Southern emu-wren
Mallee emu-wren
Rufous-crowned emu-wren
 Amytornis
Grey grasswren
Black grasswren
White-throated grasswren
Carpentarian grasswren
Short-tailed grasswren
Striated grasswren
Eyrean grasswren
Western grasswren
Thick-billed grasswren
Dusky grasswren
Kalkadoon grasswren
 
Grey grasswren
Black grasswren
White-throated grasswren
Carpentarian grasswren
Short-tailed grasswren
Striated grasswren
Eyrean grasswren
Western grasswren
Thick-billed grasswren
Dusky grasswren
Kalkadoon grasswren
 
Wikidata: Q1164379
Wikispecies: Malurus lamberti
ADW: Malurus_lamberti
Avibase: 468816FA
BirdLife: 22703744
BirdLife-Australia: variegated-fairy-wren
eBird: varfai5
EoL: 918367
GBIF: 2487495
iNaturalist: 12097
IRMNG: 11435406
ITIS: 560976
IUCN: 22703744
NCBI: 228352
uBio: 2337134
 