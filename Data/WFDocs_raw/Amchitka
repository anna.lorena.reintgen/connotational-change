
 Amchitka (/æmˈtʃɪtkə/; Aleut: Amchixtax̂[1]) is a volcanic, tectonically unstable island in the Rat Islands group of the Aleutian Islands in southwest Alaska. It is part of the Alaska Maritime National Wildlife Refuge. The island, with a land area of roughly 116 square miles (300 km2), is about 42 miles (68 km) long and 1 to 4 miles (1.6 to 6.4 km) wide.[2] The area has a maritime climate, with many storms, and mostly overcast skies.
 Amchitka was populated for more than 2,500 years by the Aleut people, but has had no permanent population since 1832. The island has been part of the United States since the Alaska Purchase of 1867. During World War II, it was used as an airfield by US forces in the Aleutian Islands Campaign.
 Amchitka was selected by the United States Atomic Energy Commission to be the site for underground detonations of nuclear weapons. Three such tests were carried out: Long Shot, an 80-kiloton (330 TJ) blast in 1965; Milrow, a 1-megaton (4.2 PJ) blast in 1969; and Cannikin in 1971 – at 5 Mt (21 PJ), the largest underground test ever conducted by the United States. The tests were highly controversial, with environmental groups fearing that the Cannikin explosion, in particular, would cause severe earthquakes and tsunamis. Amchitka is no longer used for nuclear testing. It is still monitored for the leakage of radioactive materials.
 Amchitka is the southernmost of the Rat Islands group in the Aleutian Chain,[2] located between 51°21′N 178°37′E﻿ / ﻿51.350°N 178.617°E﻿ / 51.350; 178.617 and 51°39′N 179°29′E﻿ / ﻿51.650°N 179.483°E﻿ / 51.650; 179.483.[3] It is bounded by the Bering Sea to the north and east, and the Pacific Ocean to the south and west.[3]
 The eastern part of the island is a lowland plateau, with isolated ponds[4] and gently rolling hills.[3] There is low but abundant vegetation,[3] consisting of mosses, lichens, liverworts, ferns, grasses, sedges, and crowberry.[4] The center of the island is mountainous, and the western end is barren and vegetation is sparse.[3]
 Amchitka has a maritime climate, often foggy and windswept, with cloud cover 98 percent of the time.[3] While temperatures are moderated by the ocean, storms are frequent.[5] Geologically, the island is volcanic, being a part of a small crustal block on the Aleutian Arc that is being torn apart by oblique subduction. It is "one of the least stable tectonic environments in the United States."[6]
 The human history of Amchitka dates back at least 2,500 years, with the Aleut people.[5][7] Human remains, thought to be of an Aleut dating from about 1000 AD, were discovered in 1980.[8]
 Amchitka is said to have been seen and named St. Makarius by Vitus Bering in 1741, was sighted by Joseph Billings in 1790, and visited by Shishmaref in 1820.[9]
 In 1783, Daikokuya Kōdayū and 15 Japanese castaways landed on Amchitka after drifting for seven months. The castaways were taken care of by Russian employees of Zhigarev and hunted with indigenous people. Six of the castaways died in three years.[10]
 In June 1942, the Japanese occupied some of the western Aleutian islands, and hoped to occupy Amchitka.[11] Eager to remove the Japanese, the Joint Chiefs of Staff agreed to move quickly to regain the territory. American planners decided to build a series of airfields to the west of Umnak, from which bombers could attack the invading forces.[12]
 The U.S. Army established bases at Adak and 13 other locations.[12] At the War Department's suggestion, an initial reconnaissance of Amchitka was carried out in September 1942, finding that it would be difficult to build an airstrip on the island.[11] Nevertheless, planners decided on December 13 that the airfield "had to be built" to prevent the Japanese from doing the same.[11] A further reconnaissance mission visited Amchitka from 17 to 19 December, and reported that a fighter strip could be built in two to three weeks, and a main airfield in three to four months.[11] The plan was approved and began in 1942.[11]
 American forces made an unopposed landing on Amchitka on January 12, 1943. Despite facing difficult weather conditions and bombing from the Japanese,
the airfield was usable by February 16.[11] The Alaska Command was now 80 km (50 mi) away from their target, Kiska.[12] The military eventually built numerous buildings, roads, and a total of three airstrips on the island,[13] one of which would later be rebuilt and used by the Atomic Energy Commission in the late 1960s.[14] At its peak, the occupancy of Amchitka reached 15,000 troops.[13]
 The Aleutian Islands campaign was successfully completed on August 24, 1943.[12] In that month, a strategic intercept station was established on the island, remaining until February 1945.[15] On 31 December 1949 the Air Force Base was closed due to insufficient personnel and staff.[16] The Army closed its communications facility at Amchitka in August 1950.[17] On 31 December 1950 the Air Force 2107th Air Weather Group pulled the last of its personnel out of Amchitka and the facility was abandoned.[18]
 The site later hosted an Air Force White Alice telecommunication system in 1959 to 1961, and a temporary relay station in the 1960s and 1970s.[13] A prototype Relocatable Over-the-Horizon Radar system existed on Amchitka between 1991 and 1993 to conduct surveillance on Russia.[19]
 Amchitka first appeared as a census-designated place (CDP) on the 1990 U.S. Census with a population of 25.[21] This was the only time it appeared on the census, and the CDP was abolished in 2000.[22]
 
 
 
 With the pullout of military forces from Amchitka in 1950, the Department of Defense initially considered the island for nuclear testing planned for 1951. Requiring information about the cratering potential of nuclear weapons, plans were made to detonate two 20-kiloton (84 TJ) devices.[5] After approximately 34 test holes had been drilled, the site was deemed unsuitable,[17] and the project was moved to the Nevada test site.[5]
 In the late 1950s, scientists realized that improved seismological knowledge was necessary for the detection of Soviet underground nuclear explosions.[23] The 1.7-kiloton (7.1 TJ) "Rainier" test (part of Operation Plumbbob, performed in Nevada) produced strong seismic signals, but looked much like an ordinary earthquake. In 1959, Dr. James R. Killian, the Special Assistant to the President for Science and Technology, formed the Panel on Seismic Improvement (which subsequently recommended the program that came to be known as Vela Uniform), with the twin goals of improving seismic instruments and deploying them globally, and researching in more depth the seismic effects of nuclear explosions.[24] The project was subsequently initiated by the Eisenhower administration.[23]
 Together with the Atomic Energy Commission, the DoD began assessing Amchitka for use as part of the Vela Uniform tests.[5]
 To conduct the Vela Uniform test Long Shot,51°25′35.84″N 179°11′14.13″E﻿ / ﻿51.4266222°N 179.1872583°E﻿ / 51.4266222; 179.1872583 the Department of Defense occupied Amchitka from 1964 to 1966, with the AEC providing the device, measuring instruments, and scientific support.[17] The goal was "to determine the behavior and characteristics of seismic signals generated by nuclear detonations and to differentiate them from seismic signals generated by naturally occurring earthquakes."[25]
 Although it would not be publicly announced until March 18, 1965, senior Alaskan officials were notified the previous February.[26] After the devastating Great Alaska earthquake of March 27, 1964, the governor expressed concern about the psychological effects of the test on the populace. He was quickly reassured.[26]
 Long Shot was detonated on October 29, 1965, and the yield was 80 kilotons (330 TJ). It was the first underground test in a remote area, and the first test managed by the DoD.[5] While there was no surface collapse,[3] tritium and krypton were found at the surface following the test;[3][27] this was not made public until 1969.[27]
 Though performed as part of the Nuclear Weapons Testing Program,[25] "[the] purpose of the Milrow test was to test an island, not a weapon."[28] It was a "calibration shot", intended to produce data from which the impact of larger explosions could be predicted, and specifically, to determine whether the planned Cannikin detonation could be performed safely. Milrow was detonated on October 2, 1969 51°24′52.06″N 179°10′44.84″E﻿ / ﻿51.4144611°N 179.1791222°E﻿ / 51.4144611; 179.1791222, with an approximate yield of 1 to 1.2 megatons (4.2–5.0 PJ).[3][29]
 The shockwave reached the surface with an acceleration of over 35 g (340 m/s2), causing a dome of the Earth's surface, approximately 3 km (2 mi) in radius, to rise about 5 meters (16 ft).[30] The blast "turned the surrounding sea to froth" and "forced geysers of mud and water from local streams and lakes 50 feet (15 m) into the air".[27] A "surface collapse feature", also known as a subsidence crater, was formed by material collapsing into the cavity formed by the explosion.[3]
 Cannikin was intended to test the design of the Spartan anti-ballistic missile (ABM) interceptor – a high-yield warhead that "produced copious amounts of x-rays and minimized fission output and debris to prevent blackout of ABM radar systems."[31] The test would "measure the yield of the device, measure the x-ray flux and spectrum, and assure deployment of a reliable design."[32]
 A few days after the Milrow test, the Don't Make A Wave Committee was organized at a meeting in Vancouver, British Columbia, Canada. The Committee's name referred to predictions made by a Vancouver journalist named Bob Hunter, later to become Greenpeace member 000. He wrote that the test would cause earthquakes and a tsunami.[33] On the agenda was whether to fight another blast at the island, or whether to expand their efforts to fight all perceived threats against the environment. As he was leaving, one man gave the traditional farewell of the peace-activist movement, "Peace." "Make it a green peace," replied another member. The Committee would later become Greenpeace.[34]
 The AEC considered the likelihood of the test triggering a severe earthquake "very unlikely", unless one was already imminent on a nearby fault, and considered a tsunami "even more unlikely".[14] Others disagreed. Russell Train, then Chairman of the Council on Environmental Quality, argued that "experience with Milrow ... does not provide a sure basis for extrapolation. In the highly nonlinear phenomena involved in earthquake generation, there may be a threshold value of the strain that must be exceeded prior to initiation of a large earthquake. ... The underground explosion could serve as the first domino of the row of dominoes leading to a major earthquake. ... as in the case of earthquakes it is not possible at this time to assess quantitatively the probability of a tsunami following the explosion."[35]
 In July 1971, a group called the Committee for Nuclear Responsibility filed suit against the AEC, asking the court to stop the test.[36] The suit was unsuccessful, with the Supreme Court denying the injunction by 4 votes to 3,[37] and Richard Nixon personally authorized the $200 million test, in spite of objections from Japan, Peru, and Sweden.[38] "What the Court didn't know, however, was that six federal agencies, including the departments of State and Interior, and the fledgling EPA, had lodged serious objections to the Cannikin test, ranging from environmental and health concerns to legal and diplomatic problems. Nixon issued an executive order to keep the comments from being released."[39] The Don't Make A Wave Committee chartered a boat, in which they had intended to sail to the island in protest, but due to weather conditions they were unable to reach their destination.[34]
 Cannikin was detonated on November 6, 1971 51°28′13.20″N 179°6′40.75″E﻿ / ﻿51.4703333°N 179.1113194°E﻿ / 51.4703333; 179.1113194, as the thirteenth test of the Operation Grommet (1971–1972) underground nuclear test series. The announced yield was 5 megatons (21 PJ) – the largest underground nuclear test in US history.[27] (Estimates for the precise yield range from 4.4[40] to 5.2[41] megatons or 18 to 22 PJ). The ground lifted 20 feet (6 m), caused by an explosive force almost 400 times the power of the Hiroshima bomb.[42] Subsidence and faulting at the site created a new lake, several hundred meters wide.[3] The explosion caused a seismic shock of 7.0 on the Richter scale, causing rockfalls and turf slides of a total of 35,000 square feet (3,300 m2).[27] Though earthquakes and tsunamis predicted by environmentalists did not occur,[37] a number of small tectonic events did occur in the following weeks, (some registering as high as 4.0 on the richter scale) thought to be due to the interaction of the explosion with local tectonic stresses.[43]
 The AEC withdrew from the island in 1973, though scientists continue to visit the island for monitoring purposes.[17] In 2001, the DoE returned to the site to remove environmental contamination. Drilling mud pits were stabilized by mixing with clean soil, covering with a polyester membrane, topped with soil and re-seeded.[13]
 Concerns have been expressed that new fissures may be opening underground, allowing radioactive materials to leak into the ocean.[42] A 1996 Greenpeace study found that Cannikin was leaking both plutonium and americium into the environment,.[27] In 2004, scientific divers from the University of Alaska Fairbanks collected shallow subtidal organisms[44] and reported that "There were no indications of any radioactive leakage, and all that was really wonderful news."[32] Similar findings are reported by a 2006 study, which found that levels of plutonium "were very small and not significant biologically".[45]
 The Department of Energy continues to monitor the site as part of their remediation program. This is expected to continue until 2025, after which the site is intended to become a restricted access wildlife preserve.[46]
  This article incorporates public domain material from the Air Force Historical Research Agency website http://www.afhra.af.mil/.
 
 1 Geography 2 Early history 3 World War II and after 4 Demographics 5 Nuclear testing

5.1 Plans for nuclear testing
5.2 Long Shot test
5.3 Milrow and Cannikin tests

5.3.1 Controversy
5.3.2 Cannikin tested


5.4 1973 and beyond

 5.1 Plans for nuclear testing 5.2 Long Shot test 5.3 Milrow and Cannikin tests

5.3.1 Controversy
5.3.2 Cannikin tested

 5.3.1 Controversy 5.3.2 Cannikin tested 5.4 1973 and beyond 6 Notes and references 7 Further reading 8 External links ^ Bergsland, K. (1994). Aleut Dictionary. Fairbanks: Alaska Native Language Center. ISBN 1-55500-047-9..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b Faller, S. H.; D. E. Farmer (1997). "Long Term Hydrological Monitoring Program" (PDF). Department of Energy. Archived from the original (PDF) on 2007-06-30. Retrieved 2006-10-11.
 ^ a b c d e f g h i j k Hassan, Ahmed; Karl Pohlmann; Jenny Chapman. "Modeling Groundwater Flow and Transport of Radionuclides at Amchitka Island's Underground Nuclear Tests: Milrow, Long Shot, and Cannikin" (PDF). Retrieved 2006-10-09.
 ^ a b Powers, Charles W.;  et al. "Amchitka Independent Assessment Science Plan" (PDF). CRESP Amchitka Oversight Committee. Archived from the original (PDF) on 2008-04-06.
 ^ a b c d e f Giblin, Michael O.; David C. Stahl; Jodi A. Bechtel. Surface remediation in the Aleutian Islands: A case study of Amchitka Island, Alaska (PDF). WM '02 Conference, Tucson AZ, February 24–28, 2002. Retrieved 2006-10-07.
 ^ Eichelberger, John; Jeff Freymueller; Graham Hill; Matt Patrick (March 2002). "Nuclear Stewardship: Lessons from a Not-So-Remote Island". GeoTimes. Archived from the original on 2003-09-01. Retrieved 2006-10-11.
 ^ Miller states "at least 9,000 years" (see Miller, Pam, "Nuclear Flashback")
 ^ "Federal Register: Notice of Inventory Completion: U.S. Department of the Interior, U.S. Fish and Wildlife Service, Region 7, Anchorage, AK" (PDF). 2003-12-01. Archived (PDF) from the original on 5 December 2008. Retrieved 2008-11-07.
 ^ Baker, Marcus (1902). Geographic Dictionary of Alaska (Bulletin of the United States Geological Survey, no 187, Series F, Geography, 27). Washington: Government Printing Office.
 ^ Yamashita, Tsuneo Daikokuya Kodayu(Japanese) 2004. Iwanami, Japan ISBN 4-00-430879-8
 ^ a b c d e f Conn, Stetson (2000). "Chapter X: Alaska in the War, 1942". Guarding the United States and its outposts. United States Army Center of Military History. ISBN 0-16-001904-4. CMH 4–2, Library of Congress no 62–60067. Retrieved 2006-10-09.
 ^ a b c d MacGarrigle, George L. (October 2003). Aleutian Islands. The Campaigns of World War II. United States Army Center of Military History. CMH Pub 72–6, paper, GPO S/N 008-029-00232-9. Retrieved 2006-10-07.
 ^ a b c d "Amchitka, Alaska, Site Fact Sheet" (PDF). Department of Energy Office of Legacy Management. Archived from the original (PDF) on 2007-06-30. Retrieved 2006-10-07.
 ^ a b "Environmental Statement Cannikin". Atomic Energy Commission. Retrieved 2006-10-11.
 ^ "Pre-1952 Historical Timeline". National Security Agency. Archived from the original on 2006-10-03. Retrieved 2006-10-07.
 ^ AFHRA document 00076530
 ^ a b c d "Amchitka Island, Alaska: Potential U.S. Department of Energy site responsibilities (DOE/NV-526)" (PDF). Department of Energy. December 1998. Retrieved 2006-10-09.
 ^ AFHRA Document 00496942
 ^ "AN/TPS-71 ROTHR (Relocatable Over-the-Horizon Radar)". Federation of American Scientists. June 29, 1999. Retrieved 2014-07-09.
 ^ "U.S. Decennial Census". Census.gov. Retrieved June 6, 2013.
 ^ https://www.census.gov/prod/cen1990/cph5/cph-5-3.pdf
 ^ https://www.census.gov/prod/cen2000/phc-1-3.pdf
 ^ a b Barth, Kai-Henrik (2003). "The politics of seismology: Nuclear testing, arms control, and the transformation of a discipline". Social Studies of Science. 33 (5): 743–781. doi:10.1177/0306312703335005.
 ^ Van der Vink, Gregory E.;  et al. (February 1994). Nuclear testing and nonproliferation: The role of seismology in deterring the development of nuclear weapons. The Iris Consortium. Archived from the original on 7 September 2006. Retrieved 2006-10-08.
 ^ a b "Project Baseline Report (NVNO0227)". 1998-01-16. Archived from the original on 2006-09-26. Retrieved 2006-10-09.
 ^ a b Kohlhoff, Dean W. (November 2002). Amchitka and the Bomb. University of Washington Press. ISBN 0-295-98255-1.
 ^ a b c d e f Miller, Pam. "Nuclear Flashback: Report of a Greenpeace Scientific Expedition to Amchitka Island, Alaska – Site of the Largest Underground Nuclear Test in U.S. History" (PDF). Archived (PDF) from the original on 2006-09-28. Retrieved 2006-10-09.
 ^ "The Milrow Test (DOE Historical Test Film 800040)". Archived from the original on 28 September 2006. Retrieved 2006-10-09.
 ^ See Miller "Nuclear Flashback" or Schneider "Amchitka's nuclear legacy".
 ^ Merritt, Melvin (June 1971). "Ground Shock and Water Pressures from Milrow". BioScience. American Institute of Biological Sciences. 21 (12): 696–700. doi:10.2307/1295751. JSTOR 1295751.
 ^ "Accomplishments in the 1970s: Lawrence Livermore National Laboratory". Archived from the original on 2005-02-17. Retrieved 2006-10-09.
 ^ a b Schneider, Doug. "Amchitka's nuclear legacy". University of Alaska Fairbanks. Archived from the original on 2006-09-12. Retrieved 2006-10-09.
 ^ Vidal, John (2005-05-04). "The original Mr Green". London: The Guardian. Retrieved 2006-10-11.
 ^ a b The Greenpeace Story in: Chuck Davis, editor in chief. (1997). The Greater Vancouver Book: An Urban Encyclopedia. Linkman Press. ISBN 1-896846-00-9. Archived from the original on 6 October 2006. Retrieved 2006-10-09.
 ^ "COMMITTEE FOR NUCLEAR RESPONSIBILITY, INC. v. SCHLESINGER , 404 U.S. 917 (1971)". US Supreme Court. 1971-11-06. Archived from the original on 2007-03-22. Retrieved 2006-10-11.
 ^ "Round 2 at Amchitka". U.S. TIME. New York City. 1971-07-17. Archived from the original on 2008-12-21. Retrieved 2006-10-09.
 ^ a b "The Amchitka Bomb Goes Off". TIME. 1971-11-15. Retrieved 2006-10-09.
 ^ "Green Light on Cannikin". TIME. 1971-11-08. Retrieved 2006-10-09.
 ^ Jeffrey St. Clair, CounterPunch, 27 September 2013, The Bomb that Cracked an Island
 ^ Sykes, Lynn R.; Graham C. Wiggins (January 1986). "Yields of Soviet Underground Nuclear Explosions at Novaya Zemlya, 1964–1976, from Seismic Body and Surface Waves". Proceedings of the National Academy of Sciences of the United States of America. 83 (2): 201–5. Bibcode:1986PNAS...83..201S. doi:10.1073/pnas.83.2.201. PMC 322824. PMID 16593645.
 ^ Fritz, Stacey (April 2000). "The Role of National Missile Defense in the Environmental History of Alaska". University of Alaska Fairbanks.
 ^ a b Perlman, David (2001-12-17). "Blast from the past: Researchers worry that radiation from nuclear test decades ago may be damaging marine life today". The San Francisco Chronicle. Retrieved 2006-10-11.
 ^ Engdahl, E. R. (December 1972). "Seismic effects of the MILROW and CANNIKIN nuclear explosions" (PDF). Bulletin of the Seismological Society of America. 62 (6): 1411–1423. doi:10.2172/4687405.
 ^ Jewett, Stephen; Hoberg, Max; Chenelot, Heloise; Harper, Shawn; Burger, Joanna; Gochfeld, Michael. (2005). "Scuba Techniques Used In Risk Assessment Of Possible Nuclear Leakage Around Amchitka Island, Alaska". In: Godfrey, JM; Shumway, SE. Diving for Science 2005. Proceedings of the American Academy of Underwater Sciences Symposium on March 10–12, 2005 at the University of Connecticut at Avery Point, Groton, Connecticut. American Academy of Underwater Sciences. Retrieved 2011-01-10.
 ^ Burger, J;  et al. (October 2006). "Radionuclides in marine macroalgae from Amchitka and Kiska Islands in the Aleutians: establishing a baseline for future biomonitoring". J Environ Radioact. 91 (1–2): 27–40. doi:10.1016/j.jenvrad.2006.08.003. PMID 17029666.
 ^ "Amchitka Island". Department of Energy. Archived from the original on 2006-09-25. Retrieved 2006-10-11.
 ^ "United States nuclear tests: July 1945 through September 1992" (PDF). Department of Energy. Archived from the original (PDF) on 12 October 2006. Retrieved 2006-10-11.
 ^ a b c Johnson, "Mark". "Results from the Amchitka Oceanographic Survey" (PDF). University of Alaska, Fairbanks. Archived from the original (PDF) on 2008-04-06. Retrieved 2006-10-11.
 Hunter, Robert. The Greenpeace to Amchitka An Environmental Odyssey. Vancouver, B.C.: Arsenal Pulp Press, 2004. ISBN 1-55152-178-4 Sense, Richard G., and Roger J. Desautels. Amchitka Archaeology Progress Reports. Las Vegas, Nev: Holmes & Narver, Inc.?, 1970. Home page of USS Worden (DD 352) A U.S. Navy destroyer that sank during the landing at Amchitka, January 12, 1943. Contains eyewitness accounts of the landings. The following links are to Department of Energy films about the Amitchka test facility. The videos include footage of the tests.
The Amchitka Program (Video on YouTube)
Project Long Shot (Video on YouTube)
The Milrow Test (Video on YouTube)
Project Cannikin Review (Video on YouTube)
Amchitka Island Remediation Activities The Amchitka Program (Video on YouTube) Project Long Shot (Video on YouTube) The Milrow Test (Video on YouTube) Project Cannikin Review (Video on YouTube) Amchitka Island Remediation Activities The following links are to current articles from the Anchorage Daily News about the Cannikin test.
Why a bomb test in the Aleutians still strikes fear in workers 46 years later
Veterans pay the cost for U.S. nuclear tests Why a bomb test in the Aleutians still strikes fear in workers 46 years later Veterans pay the cost for U.S. nuclear tests v t e "Anvil" "Aqueduct" "Arbor" "Argus" "Bedrock" "Bowline" "Buster–Jangle" "Castle" "Chariot" "Charioteer" "Cornerstone" "Cresset" "Crossroads" "Crosstie" "Dominic"
Starfish Prime Starfish Prime "Emery" "Fishbowl" "Flintlock" "Fulcrum" "Fusileer" "Greenhouse" "Grenadier" "Grommet" "Guardian" "Hardtack I"
"Hardtack Teak" "Hardtack Teak" "Hardtack II" "Ivy" "Julin" "Latchkey" "Little Feller" "Mandrel" "Musketeer" "Niblick" "Nougat" "Phalanx" "Plowshare" "Plumbbob" "Praetorian" Project 56 Project 57 Project 58 Project 58A "Quicksilver" "Ranger" "Redwing" "Roller Coaster" "Sandstone" "Sculpin" "Storax" "Sunbeam" "Teapot" "Tinderbox" "Toggle" "Touchstone" "Trinity" "Tumbler–Snapper" "Upshot–Knothole" "Whetstone" "Wigwam" Area 15 Area 16 Area 18 Area 30 Frenchman Flat Pahute Mesa Rainier Mesa Yucca Flat South Atlantic Pacific Proving Grounds Alamogordo Amchitka Carlsbad Central Nevada Sand Springs Range Carson National Forest Parachute Salmon Site Nevada Test and Training Range Rifle National Atomic Testing Museum Fallout: An American Nuclear Tragedy Alvin C. Graves Reed Hadley Chuck Hansen Corbin Harney Desert Rock exercises Unethical human experimentation in the United States International Day against Nuclear Tests Nevada Desert Experience Nuclear weapons testing Radiation Exposure Compensation Act Radio Bikini Reactor-grade plutonium nuclear test Trinity and Beyond: The Atomic Bomb Movie Vela Uniform Louie Vitale Nuclear testing at Bikini Atoll v t e Adak Adugak Agattu Aiktak Akun Akutan Amak Amaknak Amatignak Amchitka Amlia Amukta Anangula Ananiuliak Arakamchechen Atka Attu Avatanak Aziak Bering Besboro Bobrof Bogoslof Buldir Carlisle Chagulak Chuginadak Chugul Derbin Egg Gareloi Great Sitkin Hagemeister Hall Hawadax Herbert Igitkin Ilak Kagalaska Kagamil Kamen Ariy Kanaga Karaginsky Kasatochi Khvostof King Kiska Koniuji Kritskoi Little Sitkin Little Tanaga Medny Nelson Nunivak Oglodak Otter Pancake Rock Poa Rootok Sagchudak Samalga Sanak Sea Lion Rock Sea Otter Rocks Sedanka Seguam Segula Semisopochnoi Shemya Sledge St. Lawrence St. Matthew St. Michael St. Paul Stuart Tagalak Tanaga Tigalda Tufted Puffin Rock Ugamak Ulak Uliaga Umak Umnak Unalaska Unalga Unimak Walrus Walrus (Pribilof) Wislow Yttygran Yunaska Island groupsAleutian Andreanof Baby Commander Delarof Diomede Fox Four Mountains Krenitzin Kudobin Near Pribilof Punuk Rat Sanak Seal Walrus Walrus and Kritskoi v t e Adak/Davis Alexai Point Amchitka Elmendorf Fort Glenn Fort Morrow Fort Randall Shemya Annette Island Cordova Gakona Juneau Kiska Ogliuga Island Yakutat Bethel Big Delta Galena Gambell Ladd McGrath Marks Mile 26 Moses Point Naknek Northway Tanacross XI Bomber XI Fighter 28th Bombardment 343d Fighter United States Army Air Forces First Second Third Fourth Fifth Sixth Seventh Eighth Ninth Tenth Eleventh Twelfth Thirteenth Fourteenth Fifteenth Twentieth Aleutian Islands Campaign American nuclear weapons testing American nuclear test sites Rat Islands Former populated places in Alaska Nuclear test sites Uninhabited islands of Alaska Islands of Unorganized Borough, Alaska Islands of Alaska Wikipedia pages move-protected due to vandalism Coordinates on Wikidata Articles containing Aleut-language text Wikipedia articles incorporating text from the Air Force Historical Research Agency Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version العربية Azərbaycanca Беларуская Cebuano Čeština Deutsch Español Esperanto Français Galego Italiano עברית Magyar Nederlands 日本語 Русский Српски / srpski Suomi Tiếng Việt 中文  This page was last edited on 15 October 2019, at 17:05 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Amchitka 1990 Nuclear tests at Amchitka ^ a b a b c d e f g h i j k a b a b c d e f ^ ^ ^ ^ ^ a b c d e f a b c d a b c d a b ^ ^ a b c d ^ ^ ^ ^ ^ a b 33 ^ a b a b a b c d e f ^ ^ ^ 21 ^ a b ^ a b ^ ^ a b ^ ^ ^ 83 ^ a b ^ 62 ^ ^ 91 ^ ^ a b c Island groups United States Army Air Forces Cannikin warhead being lowered into test shaft Location of the site 51°32′32″N 178°59′00″E﻿ / ﻿51.54222°N 178.98333°E﻿ / 51.54222; 178.98333﻿ (Amchitka Underground Test Site)Coordinates: 51°32′32″N 178°59′00″E﻿ / ﻿51.54222°N 178.98333°E﻿ / 51.54222; 178.98333﻿ (Amchitka Underground Test Site) Nuclear testing range United States Department of Energy Inactive 1965–1971 3 2001–2025 (DoE estimate) 1990 25  — U.S. Decennial Census[20] Long Shot 21:00, October 29, 1965 51°26′12″N 179°10′47″E﻿ / ﻿51.43655°N 179.17976°E﻿ / 51.43655; 179.17976﻿ (Long Shot Nuclear Test)
 80 kt (330 TJ) 2,343 ft (714 m) shaft
 Milrow 22:06, October 2, 1969 51°24′56″N 179°10′48″E﻿ / ﻿51.41559°N 179.17992°E﻿ / 51.41559; 179.17992﻿ (Milrow Nuclear Test)
 ~ 1 Mt (4.2 PJ) 4,002 ft (1,220 m) shaft
 Cannikin 22:00, November 6, 1971 51°28′11″N 179°06′12″E﻿ / ﻿51.46961°N 179.10335°E﻿ / 51.46961; 179.10335﻿ (Cannikin Nuclear Test)
 < 5 Mt (21 PJ) 6,104 ft (1,860 m) shaft
 
"Anvil"
"Aqueduct"
"Arbor"
"Argus"
"Bedrock"
"Bowline"
"Buster–Jangle"
"Castle"
"Chariot"
"Charioteer"
"Cornerstone"
"Cresset"
"Crossroads"
"Crosstie"
"Dominic"
Starfish Prime
"Emery"
"Fishbowl"
"Flintlock"
"Fulcrum"
"Fusileer"
"Greenhouse"
"Grenadier"
"Grommet"
"Guardian"
"Hardtack I"
"Hardtack Teak"
"Hardtack II"
"Ivy"
"Julin"
"Latchkey"
"Little Feller"
"Mandrel"
"Musketeer"
"Niblick"
"Nougat"
"Phalanx"
"Plowshare"
"Plumbbob"
"Praetorian"
Project 56
Project 57
Project 58
Project 58A
"Quicksilver"
"Ranger"
"Redwing"
"Roller Coaster"
"Sandstone"
"Sculpin"
"Storax"
"Sunbeam"
"Teapot"
"Tinderbox"
"Toggle"
"Touchstone"
"Trinity"
"Tumbler–Snapper"
"Upshot–Knothole"
"Whetstone"
"Wigwam"
 Nevada
Area 15
Area 16
Area 18
Area 30
Frenchman Flat
Pahute Mesa
Rainier Mesa
Yucca Flat
other
South Atlantic
Pacific Proving Grounds
Alamogordo
Amchitka
Carlsbad
Central Nevada
Sand Springs Range
Carson National Forest
Parachute
Salmon Site
Nevada Test and Training Range
Rifle
 
Area 15
Area 16
Area 18
Area 30
Frenchman Flat
Pahute Mesa
Rainier Mesa
Yucca Flat
 
South Atlantic
Pacific Proving Grounds
Alamogordo
Amchitka
Carlsbad
Central Nevada
Sand Springs Range
Carson National Forest
Parachute
Salmon Site
Nevada Test and Training Range
Rifle
 
National Atomic Testing Museum
Fallout: An American Nuclear Tragedy
Alvin C. Graves
Reed Hadley
Chuck Hansen
Corbin Harney
Desert Rock exercises
Unethical human experimentation in the United States
International Day against Nuclear Tests
Nevada Desert Experience
Nuclear weapons testing
Radiation Exposure Compensation Act
Radio Bikini
Reactor-grade plutonium nuclear test
Trinity and Beyond: The Atomic Bomb Movie
Vela Uniform
Louie Vitale
Nuclear testing at Bikini Atoll
 
Adak
Adugak
Agattu
Aiktak
Akun
Akutan
Amak
Amaknak
Amatignak
Amchitka
Amlia
Amukta
Anangula
Ananiuliak
Arakamchechen
Atka
Attu
Avatanak
Aziak
Bering
Besboro
Bobrof
Bogoslof
Buldir
Carlisle
Chagulak
Chuginadak
Chugul
Derbin
Egg
Gareloi
Great Sitkin
Hagemeister
Hall
Hawadax
Herbert
Igitkin
Ilak
Kagalaska
Kagamil
Kamen Ariy
Kanaga
Karaginsky
Kasatochi
Khvostof
King
Kiska
Koniuji
Kritskoi
Little Sitkin
Little Tanaga
Medny
Nelson
Nunivak
Oglodak
Otter
Pancake Rock
Poa
Rootok
Sagchudak
Samalga
Sanak
Sea Lion Rock
Sea Otter Rocks
Sedanka
Seguam
Segula
Semisopochnoi
Shemya
Sledge
St. Lawrence
St. Matthew
St. Michael
St. Paul
Stuart
Tagalak
Tanaga
Tigalda
Tufted Puffin Rock
Ugamak
Ulak
Uliaga
Umak
Umnak
Unalaska
Unalga
Unimak
Walrus
Walrus (Pribilof)
Wislow
Yttygran
Yunaska
 
Island groupsAleutian
Andreanof
Baby
Commander
Delarof
Diomede
Fox
Four Mountains
Krenitzin
Kudobin
Near
Pribilof
Punuk
Rat
Sanak
Seal
Walrus
Walrus and Kritskoi
 Previously: Alaskan Air Force (1941-1942) Combat Airfields
Adak/Davis
Alexai Point
Amchitka
Elmendorf
Fort Glenn
Fort Morrow
Fort Randall
Shemya
Support Airfields
Annette Island
Cordova
Gakona
Juneau
Kiska
Ogliuga Island
Yakutat
Air Transport CommandNorthwest Staging Route(xfr to 11AF 1945)
Bethel
Big Delta
Galena
Gambell
Ladd
McGrath
Marks
Mile 26
Moses Point
Naknek
Northway
Tanacross
 
Adak/Davis
Alexai Point
Amchitka
Elmendorf
Fort Glenn
Fort Morrow
Fort Randall
Shemya
 
Annette Island
Cordova
Gakona
Juneau
Kiska
Ogliuga Island
Yakutat
 
Bethel
Big Delta
Galena
Gambell
Ladd
McGrath
Marks
Mile 26
Moses Point
Naknek
Northway
Tanacross
  Commands
XI Bomber
XI Fighter
GroupsBombardment
28th Bombardment
Fighter
343d Fighter
 
XI Bomber
XI Fighter
 Bombardment
28th Bombardment
Fighter
343d Fighter
 
28th Bombardment
 
343d Fighter
 
United States Army Air Forces
First
Second
Third
Fourth
Fifth
Sixth
Seventh
Eighth
Ninth
Tenth
Eleventh
Twelfth
Thirteenth
Fourteenth
Fifteenth
Twentieth
 