
 George Abramovich Koval (Russian: Жорж (Георгий) Абра́мович Кова́ль, IPA: [ˈʐorʐ (ɡʲɪˈorɡʲɪj) ɐˈbraməvʲɪtɕ kɐˈvalʲ] (listen), Zhorzh Abramovich Koval, December 25, 1913 – January 31, 2006) was an American who acted as a Soviet intelligence officer for the Soviet atomic bomb project. According to Russian sources, Koval's infiltration of the Manhattan Project as a GRU (Soviet military intelligence) agent "drastically reduced the amount of time it took for Russia to develop nuclear weapons."[1][2][3][4][5]
 Koval was born to Russian Jewish immigrants in Sioux City, Iowa. As an adult, he traveled with his parents to the Soviet Union to settle in the Jewish Autonomous Region near the Chinese border. Koval was recruited by the Soviet GRU (military intelligence), trained, and assigned the code name DELMAR.  He returned to the United States in 1940 and was drafted into the U.S. Army in early 1943.  Koval worked at atomic research laboratories and, according to the Russian government, relayed back to the Soviet Union information about the production processes and volumes of the polonium, plutonium, and uranium used in American atomic weaponry, and descriptions of the weapon production sites.  In 1948, Koval left on a European vacation but never returned to the United States.  In 2007 Russian President Vladimir Putin posthumously awarded Koval the Hero of the Russian Federation decoration for "his courage and heroism while carrying out special missions".[6]
 George Koval's father, Abram Koval, left his home town of Telekhany in Belarus to immigrate to the United States in 1910. Abram, a carpenter, settled in Sioux City, Iowa, which, at the turn of the 20th century, was home to a sizeable Jewish community  of merchants and craftsmen. Many of these settlers moved from Russia, in which Jews had been ruthlessly persecuted under the czar's anti-Semitic policies and pogroms.[7]
Abram and his wife Ethel Shenitsky Koval raised three sons: Isaya, born 1912; George (or Zhorzh), born Christmas 1913; and Gabriel, born 1919.[8]
 George Koval attended Central High School, a red-brick Victorian building better known as "the Castle on the Hill". Neighbors recalled that Koval spoke openly of his Communist beliefs. While attending Central High he was a member of the Honor Society and the debate team. He graduated in 1929 at the age of 15. Meanwhile, his parents left Sioux City as the Great Depression deepened. Abram Koval became the secretary for ICOR, the Organization for Jewish Colonization in the Soviet Union.[8] Founded by American Jewish Communists in 1924, the group helped to finance and publicize the development of the "Jewish Autonomous Region" – the Soviet answer to Jewish emigration to the British Mandate of Palestine then being undertaken by the Zionist movement.[9] The Koval family emigrated in 1932, traveling with a United States family passport.[8] They settled in Birobidzhan, near the border of Manchuria.[10]
 The Koval family worked on a collective farm and were profiled by an American Communist daily newspaper in New York City. The journalist Paul Novick wrote to his readers that the family "had exchanged the uncertainty of life as small storekeepers ... for a worry-free existence for themselves and their children."[10] While Isaya became a champion tractor driver, George Koval improved his Russian language skills in the collective and began studies at the Mendeleev Institute of Chemical Technology in 1934. At the university, he met and married fellow student Lyudmila Ivanova. Koval graduated with honors in five years and received Soviet citizenship.[10]
 Later, Koval was recruited by the Soviet Main Intelligence Directorate (Главное Разведывательное Управление), or GRU. By the time he received his degree he had left Moscow under orders as part of a subterfuge. He was drafted into the Soviet army in 1939 to explain his sudden disappearance from the city. Though his parents had relinquished their US family passport, Koval returned to the US in 1940,[10] replacing a spy recalled during Stalin's purges.[11] His code name was DELMAR.[12]  Arriving in San Francisco, he traveled to New York City. According to Arnold Kramish, an American colleague he befriended and with whom he re-established contact in 2000, it was there that Koval assumed deputy command of the local GRU cell. This outpost operated under the cover of the Raven Electric Company, a supplier to firms such as General Electric. Koval told coworkers he was a native New Yorker and an only child. He ingratiated himself with everyone he met.[10] While Koval originally worked under a pseudonym, gathering information on toxins for use in chemical weapons, his handlers decided to have him work under his real name.[13]
 During the beginning of World War II, President Franklin D. Roosevelt had re-introduced the draft (conscription) in September 1940, and Koval registered for it on January 2, 1941. Raven Electric Company secured him a year's deferment from service until February 1942. According to historian Vladimir Lota, Koval's handlers wanted him to steal information about chemical weapons, and felt that he would not be able to do so while drafted. When the deferment expired, Koval was inducted into the United States Army. He received basic training at Fort Dix in New Jersey before being sent to the Citadel in Charleston, South Carolina. There, Koval served as a private in the 3410th Specialized Training and Reassignment Unit. On August 11, 1943, he was transferred to the Army Specialized Training Program, a unit established in December 1942 to provide talented enlistees with an education and technical training.[10] Koval attended the City College of New York (CCNY) and studied electrical engineering. His CCNY classmates looked up to the older Koval as a role model and father figure who never did homework and was a noted ladies' man, never knowing about his Soviet education and wife.[14] Colleagues recalled that he never discussed politics or the Soviet Union.[13]
 The Specialized Training Program was dissolved in early 1944, as the progress of the war tipped in favor of the Allies; many of the CCNY classmates were transferred to the infantry, while Koval and a dozen others were selected for the Special Engineer Detachment. The Detachment was part of the covert project to design, engineer, and fabricate an atomic bomb—an American, Canadian and British initiative known as the Manhattan Project. Koval was assigned to Oak Ridge, Tennessee; at the time, Project scientists were researching enriched uranium and plutonium-based bombs, with the Oak Ridge laboratories central to the development of both.[14] The Project suffered from a lack of human resources, and asked the Army for technically qualified men.[13]
 Koval enjoyed free access to much of Oak Ridge;[13] he was made a "health physics officer", and monitored radiation levels across the facility. According to Federal Bureau of Investigation records, the job gave him top-secret security clearance. At the time, Project scientists discovered reactor-produced plutonium was too unstable for the intended bomb designs, and that polonium initiators (urchin) were needed for the necessary chain reactions to occur.[14] Koval was charged by his handlers with watching Oak Ridge's polonium supply to transmit information about it through a Soviet contact named "Clyde". His information reached Moscow via coded dispatches, couriers, and the Soviet Embassy. Among the intelligence he sent was that Oak Ridge's polonium was being sent to another Project site at Los Alamos National Laboratory.[15]
 Koval was transferred from Oak Ridge to a top-secret lab in Dayton, Ohio on June 27, 1945, where polonium initiators were fabricated. The world's first atomic bomb was detonated in New Mexico on July 16 of that year. Atomic bombs were dropped on Japan on August 6 and 9, leading to Japan's surrender and the end of World War II. The Soviet Union responded by increasing efforts to develop its own atomic bomb. While the American Central Intelligence Agency estimated the Soviets would not succeed until 1950–53, the first Soviet atomic bomb was detonated on August 29, 1949.[15] The initiator for the plutonium bomb was, according to Russian military officials, "prepared to the 'recipe' provided by military intelligence agent Delmar [Koval]".[15]
 After World War II, Koval was discharged from the Army. He returned to New York and CCNY, where he received his bachelor's degree in electrical engineering on February 1, 1948. Telling his friends he was thinking about taking a trip to Poland or Israel, Koval secured a passport for six months' travel to Europe. According to the Russian publication Rossiyskaya Gazeta, he might have left because American counter-intelligence agents had discovered Soviet literature about his parents[13] after being tipped off about the leak by a Soviet defector.[11] He left by sea in October 1948 and never returned to his birth country.[16] In Russia, he left the Soviet military with discharge papers as an untrained rifleman and the rank of private. His foreign background and service record made him "a very suspicious character", he wrote to Kramish. Turned down for education and research positions, Koval turned to his old GRU contact, who secured him a job as a laboratory assistant at the Mendeleev Institute. Eventually, Koval managed to obtain a teaching job there; his students often laughed at his foreign pronunciations for technical terms.[17]
 While other spies such as Julius and Ethel Rosenberg and Klaus Fuchs were caught after the war, Koval apparently went unscrutinized for years. Among the reasons given for his maintained cover was that inter-service politics undermined efforts to perform proper security checks on employees. Another possibility is that the U.S. government chose scientific ability over clear records and political sympathies.[16] In the 1950s, the FBI investigated his wartime activities and interviewed his former colleagues, leaving them with the impression that he might have been a spy.[17] The matter was kept confidential for sixty years[11] as the US was afraid of the damage that would result from the exposure of Koval's activities.[18]
 In 1999, Koval was living on his small pension in Russia and had heard that U.S. war veterans like himself could apply for Social Security payments. He applied. In 2000, the Social Security Administration's Office of Central Operations, Baltimore, Maryland responded with a one-sentence letter: "We are writing to tell you that you do not qualify for retirement benefits."[12]
 Koval described his 57 years of post-spy life living in Russia as "uneventful". His family knew he had done work for the GRU, but the subject was never discussed. He did not receive any high awards upon his return, a fact that bothered him. Bigger awards went to "career men", he told Kramish. However, he ended his correspondence by saying that he was not protesting his treatment; "[I am thankful] that I did not find myself in a Gulag, as might well have happened".[17] Koval died in his Moscow apartment on January 31, 2006, at the age of 92.[13]
 Koval's activities as a spy began to emerge after the publication of a 2002 book, The GRU and the Atomic Bomb, which mentioned Koval by his code name and listed him as one of a handful of spies who evaded counterintelligence groups.[13] On November 3, 2007, he received the posthumous title of Hero of the Russian Federation bestowed by Russian President Vladimir Putin. When Koval was honored, the Russian presidential proclamation stated, "Mr Koval, who operated under the pseudonym Delmar, provided information that helped speed up considerably the time it took for the Soviet Union to develop an atomic bomb of its own".[6]
 
 1 Early life 2 Recruitment and espionage

2.1 Atomic secrets

 2.1 Atomic secrets 3 Later years 4 Notes 5 References ^ Bruno Navasky. "Koval, George Abramovich (1913–2006)". DocumentsTalk.com. Retrieved September 9, 2010. [Koval] drastically reduced the amount of time it took for Russia to develop nuclear weapons..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ John Earl Haynes; Harvey Klehr; Alexander Vassiliev (2010). Spies: The Rise and Fall of the KGB in America. Yale University Press. ISBN 978-0-300-15572-3.
 ^ Agence France-Presse (November 3, 2007), "Russia: Award for a Soviet Spy", The New York Times p. A11
 ^ William J. Broad (November 12, 2007), "A Spy's Path: Iowa to A-Bomb to Kremlin Honor", The New York Times p. A1
 ^ Robert Meeropol (November 17, 2007), The Case of the Rosenbergs: Their Sons' View, The New York Times
 ^ a b Moscow Kremlin.
 ^ Pagano, Owen N. (2014). The Spy Who Stole The Urchin: George Koval's Infiltration of the Manhattan Project (PDF) (B.A). The George Washington University. p. 3.
 ^ a b c Walsh, 42.
 ^ Srebrnik, 80–108.
 ^ a b c d e f Walsh, 43.
 ^ a b c Doyle.
 ^ a b Soldatov
 ^ a b c d e f g Broad, 2.
 ^ a b c Walsh, 44.
 ^ a b c Walsh, 45.
 ^ a b Walsh, 46.
 ^ a b c Walsh, 47.
 ^ Broad, 1.
 Svetlana Chervonnaya. Koval, George Abramovich (1913–2006). DocumentsTalk.com. Retrieved 2010-09-09. Broad, William J (November 12, 2007). "A Spy's Path: Iowa to A-Bomb to Kremlin Honor". The New York Times. pp. 1–2. Retrieved May 22, 2009. Doyle, Leonard (November 13, 2007). "US embarrassed as Putin honours spy who came in from the cornfields". The Independent. Retrieved May 20, 2009. Moscow Kremlin (November 2, 2007). "President Vladimir Putin handed over to the GRU (military intelligence) Museum the Gold Star medal and Hero of Russia certificate and document bestowed on Soviet intelligence officer George Koval". Kremlin.ru. President of Russia. Archived from the original on February 3, 2009. Retrieved June 20, 2009. Soldatov, Andrei. "The Soviet Atomic Spy Who Asked for a U.S. Pension". The Daily Beast. Retrieved May 28, 2016. Srebrnik, Henry (2001).  Gennady Estraikh; Mikhail Krutikov (ed.). Yiddish and the Left (Diaspora, Ethnicity and Dreams of Nationhood: North American Jewish Communists and the Soviet Birobidzhan Project ed.). Oxford: Legenda Press. Walsh, Michael (May 2009). "George Koval: Atomic Spy Unmasked". Smithsonian. 40 (2): 40–47. Archived from the original on April 11, 2013. "The Judicial Literature" of the Administration of the President of the Russian Federation. "УКАЗ Президента РФ от 22.10.2007 N 1404 'О ПРИСВОЕНИИ ЗВАНИЯ ГЕРОЯ РОССИЙСКОЙ ФЕДЕРАЦИИ КОВАЛЮ Ж.А.'" [October 22, 2007 Presidential Decree N 1404 'On conferring the title Hero of the Russian Federation to Koval GA'] (in Russian). Archived from the original on June 17, 2016. Retrieved May 19, 2016. v t e Ames Berkeley Chicago Dayton Hanford Inyokern Los Alamos Montreal New York Oak Ridge Trinity Wendover Heavy water sites Vannevar Bush Arthur Compton James B. Conant Priscilla Duffield Thomas Farrell Leslie Groves John Lansdale Ernest Lawrence James Marshall Franklin Matthias Dorothy McKibbin Kenneth Nichols Robert Oppenheimer Deak Parsons William Purnell Frank Spedding Charles Thomas Paul Tibbets Bud Uanna Harold Urey Stafford Warren Ed Westcott Roscoe Wilson Luis Alvarez Robert Bacher Hans Bethe Aage Bohr Niels Bohr Norris Bradbury James Chadwick John Cockcroft Harry Daghlian Enrico Fermi Richard Feynman Val Fitch James Franck Klaus Fuchs Maria Goeppert Mayer George Kistiakowsky George Koval Willard Libby Edwin McMillan Mark Oliphant Norman Ramsey Isidor Isaac Rabi James Rainwater Bruno Rossi Glenn Seaborg Emilio Segrè Louis Slotin Henry DeWolf Smyth Leo Szilard Edward Teller Stanisław Ulam John von Neumann John Wheeler Eugene Wigner Robert Wilson Leona Woods Alsos Mission Bombings of Hiroshima and Nagasaki Operation Crossroads Operation Peppermint Project Alberta Silverplate 509th Composite Group Enola Gay Bockscar The Great Artiste Fat Man Little Boy Pumpkin bomb Thin Man Atomic Energy Act of 1946 British contribution Chicago Pile-1 Demon core Einstein–Szilárd letter Franck Report Interim Committee Oppenheimer security hearing Plutonium Quebec Agreement RaLa Experiment S-1 Executive Committee S-50 Project Smyth Report Uranium X-10 Graphite Reactor v t e John Abt Joel Barr Elizabeth Bentley Earl Browder Boris Bukov Whittaker Chambers Lona Cohen Morris Cohen Judith Coplon Noel Field Klaus Fuchs Harold Glasser Harry Gold David Greenglass Theodore Hall John Herrmann Alger Hiss Donald Hiss George Koval William Malisoff Hede Massing Boris Morros Isaiah Oggins William Perl Victor Perlo J. Peters William Ward Pigman Lee Pressman Vincent Reno Julius and Ethel Rosenberg Alfred Sarant Saville Sax Morton Sobell Alexander Ulanovsky Nadezhda Ulanovskaya Julian Wadleigh Harold Ware Bill Weisband Nathaniel Weyl Harry Dexter White Maria Wicher Nathan Witt Flora Wovschin Anatoli Yatskov Rudolf Abel Aldrich Ames David Sheldon Boone Christopher John Boyce Thomas Patrick Cavanaugh Jack Dunlap James Hall III Robert Hanssen Reino Häyhänen Edward Lee Howard Robert Lee Johnson Karl Koecher Andrew Daulton Lee Robert Lipka Clayton J. Lonetree Richard Miller Harold James Nicholson Ronald Pelton Earl Edwin Pitts Jack Soble Myra Soble Robert Soblen Robert Thompson George Trofimoff John Anthony Walker Jerry Whitworth Evgeny Buryakov Anna Chapman Illegals Program Anthony Blunt Guy Burgess John Cairncross Donald Maclean Kim Philby Lona Cohen Morris Cohen Ethel Gee Harry Houghton Konon Molody Michael Bettaney George Blake David Crook Litzi Friedmann Klaus Fuchs Percy Glading Melita Norwood Alan Nunn May John Peet Geoffrey Prime Goronwy Rees Michael John Smith Dave Springhall John Alexander Symonds Edith Tudor-Hart John Vassall Arthur Wynn Sam Carr Jeffrey Delisle Igor Gouzenko Elena Miller Gerda Munsinger Stephen Joseph Ratkai Fred Rose Hirohide Ishida Yotoku Miyagi Sanzō Nosaka Hotsumi Ozaki Ryūzō Sejima Alexander Gregory Barmine Stig Bergling Dieter Gerhardt Walter Krivitsky Kerttu Nuorteva Vladimir Mikhaylovich Petrov Fyodor Raskolnikov Ignace Reiss Vitaly Shlykov Herman Simm Richard Sorge Stig Wennerström v t e Alexander Gelver Bill Haywood John Reed Thomas Sgovio Arthur Adams Flora Wovschin Isaiah Oggins Joel Barr Alfred Sarant Arnold Lockshin Bernon Mitchell and William Martin Edward Lee Howard George Koval Harold M. Koch Joseph Dutkanich Lee Harvey Oswald Lona Cohen Morris Cohen Robert Edward Webster Shirley Dubinsky    Category Biography portal Communism portal Politics portal Nuclear technology portal Physics portal Soviet Union portal World War II portal LCCN: n2014072216 VIAF: 311333453  WorldCat Identities (via VIAF): 311333453 Heroes of the Russian Federation 1913 births 2006 deaths American communists American defectors to the Soviet Union American military personnel of World War II American people of Russian-Jewish descent American spies for the Soviet Union City College of New York alumni Espionage in the United States GRU officers Manhattan Project people Nuclear weapons program of the Soviet Union People from Sioux City, Iowa Mendeleev Russian University of Chemistry and Technology alumni Mendeleev University faculty Use mdy dates from July 2019 Pages using deprecated image syntax Articles with hCards Articles containing Russian-language text Articles with hAudio microformats CS1 Russian-language sources (ru) Wikipedia articles with LCCN identifiers Wikipedia articles with VIAF identifiers Wikipedia articles with WorldCat-VIAF identifiers Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version العربية Azərbaycanca Deutsch Español فارسی Français עברית Nederlands 日本語 Polski Português Русский Suomi Tagalog Türkçe Українська Tiếng Việt 中文  This page was last edited on 15 October 2019, at 01:19 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Espionage activity George Abramovich Koval ^ ^ ^ ^ ^ a b ^ a b c ^ a b c d e f a b c a b a b c d e f g a b c a b c a b a b c ^ 40 Category  George Abramovich Koval(1913-12-25)December 25, 1913Sioux City, Iowa, United States January 31, 2006(2006-01-31) (aged 92)Moscow, Russia City College of New York (CCNY) City College of New York (CCNY)  Hero of Russia Espionage activity  Soviet Union GRU Delmar Manhattan Project Infiltration 
 Timeline 
Ames
Berkeley
Chicago
Dayton
Hanford
Inyokern
Los Alamos
Montreal
New York
Oak Ridge
Trinity
Wendover
Heavy water sites
  
Vannevar Bush
Arthur Compton
James B. Conant
Priscilla Duffield
Thomas Farrell
Leslie Groves
John Lansdale
Ernest Lawrence
James Marshall
Franklin Matthias
Dorothy McKibbin
Kenneth Nichols
Robert Oppenheimer
Deak Parsons
William Purnell
Frank Spedding
Charles Thomas
Paul Tibbets
Bud Uanna
Harold Urey
Stafford Warren
Ed Westcott
Roscoe Wilson
 
Luis Alvarez
Robert Bacher
Hans Bethe
Aage Bohr
Niels Bohr
Norris Bradbury
James Chadwick
John Cockcroft
Harry Daghlian
Enrico Fermi
Richard Feynman
Val Fitch
James Franck
Klaus Fuchs
Maria Goeppert Mayer
George Kistiakowsky
George Koval
Willard Libby
Edwin McMillan
Mark Oliphant
Norman Ramsey
Isidor Isaac Rabi
James Rainwater
Bruno Rossi
Glenn Seaborg
Emilio Segrè
Louis Slotin
Henry DeWolf Smyth
Leo Szilard
Edward Teller
Stanisław Ulam
John von Neumann
John Wheeler
Eugene Wigner
Robert Wilson
Leona Woods
 
Alsos Mission
Bombings of Hiroshima and Nagasaki
Operation Crossroads
Operation Peppermint
Project Alberta
Silverplate
509th Composite Group
Enola Gay
Bockscar
The Great Artiste
 
Fat Man
Little Boy
Pumpkin bomb
Thin Man
 
Atomic Energy Act of 1946
British contribution
Chicago Pile-1
Demon core
Einstein–Szilárd letter
Franck Report
Interim Committee
Oppenheimer security hearing
Plutonium
Quebec Agreement
RaLa Experiment
S-1 Executive Committee
S-50 Project
Smyth Report
Uranium
X-10 Graphite Reactor
  Manhattan Project 1940s and before
John Abt
Joel Barr
Elizabeth Bentley
Earl Browder
Boris Bukov
Whittaker Chambers
Lona Cohen
Morris Cohen
Judith Coplon
Noel Field
Klaus Fuchs
Harold Glasser
Harry Gold
David Greenglass
Theodore Hall
John Herrmann
Alger Hiss
Donald Hiss
George Koval
William Malisoff
Hede Massing
Boris Morros
Isaiah Oggins
William Perl
Victor Perlo
J. Peters
William Ward Pigman
Lee Pressman
Vincent Reno
Julius and Ethel Rosenberg
Alfred Sarant
Saville Sax
Morton Sobell
Alexander Ulanovsky
Nadezhda Ulanovskaya
Julian Wadleigh
Harold Ware
Bill Weisband
Nathaniel Weyl
Harry Dexter White
Maria Wicher
Nathan Witt
Flora Wovschin
Anatoli Yatskov
Cold War
Rudolf Abel
Aldrich Ames
David Sheldon Boone
Christopher John Boyce
Thomas Patrick Cavanaugh
Jack Dunlap
James Hall III
Robert Hanssen
Reino Häyhänen
Edward Lee Howard
Robert Lee Johnson
Karl Koecher
Andrew Daulton Lee
Robert Lipka
Clayton J. Lonetree
Richard Miller
Harold James Nicholson
Ronald Pelton
Earl Edwin Pitts
Jack Soble
Myra Soble
Robert Soblen
Robert Thompson
George Trofimoff
John Anthony Walker
Jerry Whitworth
Post-Soviet
Evgeny Buryakov
Anna Chapman
Illegals Program
 
John Abt
Joel Barr
Elizabeth Bentley
Earl Browder
Boris Bukov
Whittaker Chambers
Lona Cohen
Morris Cohen
Judith Coplon
Noel Field
Klaus Fuchs
Harold Glasser
Harry Gold
David Greenglass
Theodore Hall
John Herrmann
Alger Hiss
Donald Hiss
George Koval
William Malisoff
Hede Massing
Boris Morros
Isaiah Oggins
William Perl
Victor Perlo
J. Peters
William Ward Pigman
Lee Pressman
Vincent Reno
Julius and Ethel Rosenberg
Alfred Sarant
Saville Sax
Morton Sobell
Alexander Ulanovsky
Nadezhda Ulanovskaya
Julian Wadleigh
Harold Ware
Bill Weisband
Nathaniel Weyl
Harry Dexter White
Maria Wicher
Nathan Witt
Flora Wovschin
Anatoli Yatskov
 
Rudolf Abel
Aldrich Ames
David Sheldon Boone
Christopher John Boyce
Thomas Patrick Cavanaugh
Jack Dunlap
James Hall III
Robert Hanssen
Reino Häyhänen
Edward Lee Howard
Robert Lee Johnson
Karl Koecher
Andrew Daulton Lee
Robert Lipka
Clayton J. Lonetree
Richard Miller
Harold James Nicholson
Ronald Pelton
Earl Edwin Pitts
Jack Soble
Myra Soble
Robert Soblen
Robert Thompson
George Trofimoff
John Anthony Walker
Jerry Whitworth
 
Evgeny Buryakov
Anna Chapman
Illegals Program
 Cambridge Five
Anthony Blunt
Guy Burgess
John Cairncross
Donald Maclean
Kim Philby
Portland Spy Ring
Lona Cohen
Morris Cohen
Ethel Gee
Harry Houghton
Konon Molody

Michael Bettaney
George Blake
David Crook
Litzi Friedmann
Klaus Fuchs
Percy Glading
Melita Norwood
Alan Nunn May
John Peet
Geoffrey Prime
Goronwy Rees
Michael John Smith
Dave Springhall
John Alexander Symonds
Edith Tudor-Hart
John Vassall
Arthur Wynn
 
Anthony Blunt
Guy Burgess
John Cairncross
Donald Maclean
Kim Philby
 
Lona Cohen
Morris Cohen
Ethel Gee
Harry Houghton
Konon Molody
 
Michael Bettaney
George Blake
David Crook
Litzi Friedmann
Klaus Fuchs
Percy Glading
Melita Norwood
Alan Nunn May
John Peet
Geoffrey Prime
Goronwy Rees
Michael John Smith
Dave Springhall
John Alexander Symonds
Edith Tudor-Hart
John Vassall
Arthur Wynn
 
Sam Carr
Jeffrey Delisle
Igor Gouzenko
Elena Miller
Gerda Munsinger
Stephen Joseph Ratkai
Fred Rose
 
Hirohide Ishida
Yotoku Miyagi
Sanzō Nosaka
Hotsumi Ozaki
Ryūzō Sejima
 
Alexander Gregory Barmine
Stig Bergling
Dieter Gerhardt
Walter Krivitsky
Kerttu Nuorteva
Vladimir Mikhaylovich Petrov
Fyodor Raskolnikov
Ignace Reiss
Vitaly Shlykov
Herman Simm
Richard Sorge
Stig Wennerström
 
Alexander Gelver
Bill Haywood
John Reed
Thomas Sgovio
 
Arthur Adams
Flora Wovschin
Isaiah Oggins
Joel Barr
 
Alfred Sarant
Arnold Lockshin
Bernon Mitchell and William Martin
Edward Lee Howard
George Koval
Harold M. Koch
Joseph Dutkanich
Lee Harvey Oswald
Lona Cohen
Morris Cohen
Robert Edward Webster
Shirley Dubinsky
 



 

 

 
 Category
 
LCCN: n2014072216
VIAF: 311333453
 WorldCat Identities (via VIAF): 311333453
 