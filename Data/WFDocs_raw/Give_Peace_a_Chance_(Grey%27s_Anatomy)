"Give Peace a Chance" is the seventh episode of the sixth season of the American television medical drama Grey's Anatomy, and the show's 109th episode overall. Written by Peter Nowalk and directed by Chandra Wilson, the episode was originally broadcast on the American Broadcasting Company (ABC) in the United States on October 29, 2009. Grey's Anatomy centers on a group of young doctors in training. In this episode, Dr. Derek Shepherd (Patrick Dempsey) performs an operation on a hospital technician's "inoperable" tumor, despite the objections of the chief of surgery, Dr. Richard Webber (James Pickens, Jr.).
 The episode was designed to revolve around Dempsey's character. Katherine Heigl (Dr. Izzie Stevens) was absent from the episode, as she was filming the 2010 blockbuster Life as We Know It. Mark Saul, Jesse Williams, and Nora Zehetner returned as guest stars, while Faran Tahir made his only appearance. "Give Peace a Chance" won Wilson an NAACP Image Award, and was generally well received among critics, with Tahir's character particularly praised. The initial episode broadcast was ranked #4 for the night with 13.74 million viewers, and a 5.2/13 Nielsen rating/share in the 18–49 demographic.
 "Give Peace a Chance" opens with Seattle Grace Mercy West Hospital's chief of surgery, Dr. Richard Webber (James Pickens, Jr.) implementing a new computerized surgical scheduling system, which is disliked by many of the hospital's staff. Thereafter, Isaac (Faran Tahir), a hospital lab technician, brings Dr. Derek Shepherd (Patrick Dempsey) a scan of his tumor, which has been declared inoperable by several other physicians due to its complexity. Isaac asks Shepherd to remove it. Shepherd agrees, but Webber refuses permission for the procedure because of the high risk involved. Shepherd ignores the chief's directions and schedules the surgery. The interns and residents all want the chance to assist due to the rarity of such a tumor, so Shepherd hosts a competition in which the winner gets to join him in the operating room. After seeing Dr. Steve Mostow (Mark Saul) make a mistake, Dr. Cristina Yang (Sandra Oh) is confident that she will win the competition, but loses to Dr. Jackson Avery (Jesse Williams). Dr. Lexie Grey (Chyler Leigh) is not invited to compete because Shepherd wants her to be his caregiver in what would be a long surgery. Knowing that she will not be able to use the bathroom for the entire procedure, Lexie decides to wear a diaper into the operating room; her courage and dedication impresses Yang.
 Dr. Izzie Stevens (Katherine Heigl) is scheduled for an interleukin 2 (IL-2) treatment but is absent from the hospital. Dr. Alex Karev (Justin Chambers) calls her, but she does not answer or come, so Karev cries to fellow resident Dr. Reed Adamson (Nora Zehetner). Shepherd spends ten hours in the operating room contemplating what to do about the tumor, with the motivational support of fellow surgeons Dr. Mark Sloan (Eric Dane), Dr. Miranda Bailey (Chandra Wilson), and Dr. Callie Torres (Sara Ramirez). Webber orders Shepherd to end the surgery because he is wasting hospital resources. Shortly thereafter, Isaac awakens to Shepherd explaining that the surgery would not have been possible without paralyzing him. Isaac convinces Shepherd to operate again in secret the next day, and the latter maps out a diagram of the surgery on his bedroom wall while discussing it with his wife Dr. Meredith Grey (Ellen Pompeo), who is home on bed rest after donating part of her liver to her father in the episode "Tainted Obligation". Backed by his fellow attendings, Shepherd obtains Webber's permission to cut the chord but tells Avery and Lexie that they were not cutting the chord and playing by ear instead. He removes all but a tiny section of the tumor. Webber discovers that Shepherd is operating and is about to put an end to it, but Dr. Arizona Robbins (Jessica Capshaw) tells him to go away. Shepherd has to cut a nerve to remove the rest of the tumor, but does not know which will result in paralysis. He decides with a game of "Eeny, meeny, miny, moe", ultimately cutting the correct nerve and eliminating the entirety of the tumor. Despite the successful surgery, Webber is enraged and verbally fires Shepherd. Shepherd dismisses this in a light tone and goes home to drink champagne with his wife.
 Running for 43 minutes,[1] the episode was written by Peter Nowalk and directed by Wilson; the latter portrays Bailey. Jenny Barak edited the music and Donald Lee Harris served as the production designer.[2] Heigl was absent from the episode, as she was filming the romantic comedy Life as We Know It (2010).[3] The song featured in the episode was Bat for Lashes' "Moon and Moon", featured on their second album Two Suns.[4] Saul, Zehetner, and Williams returned to the episode as Mostow, Adamson, and Avery, respectively, while Tahir made his first and only appearance as Isaac.[5] The scenes in the operating room were filmed at the Prospect Studios in Los Feliz, Los Angeles;[6] Nowalk said the scenes were difficult to shoot, due to the technicality involved.[7] Pompeo's appearances in the episode were scarce, as she was eight and a half months pregnant during shooting.[7]
 
According to Nowalk, the idea to have Lexie wear a diaper to get through the operation was inspired by an episode of The Oprah Winfrey Show. He added: "It wasn't that big of a leap for us to go diaper. Our doctors are hardcore by nature."[7] Nowalk said that this episode was primarily focused around Shepherd, which he considered an "experiment".[7] He commented that the idea of Isaac's storyline came from Dr. Robert Bray, a neurosurgeon in Los Angeles.[7] Nowalk intended the nature of the episode to be "different", attributing it to the fact that Heigl's character was absent.[7] He also praised Wilson for directing the episode, commenting that "she acts, she sings, and now she directs".[7] The majority of the episode involved Shepherd staring at his patient's tumor, contemplating what to do. Nowalk offered his insight on this: .mw-parser-output .templatequote{overflow:hidden;margin:1em 0;padding:0 40px}.mw-parser-output .templatequote .templatequotecite{line-height:1.5em;text-align:left;padding-left:1.6em;margin-top:0} It's quieter than a typical episode. More single-minded. Derek is our sole focus. And really, what an amazing character to spend an entire episode with. Watch Patrick Dempsey on your screen and you can't help but be struck by how much he says without saying anything. The guy can pretty much give you an entire soliloquy with just one look. That's a rare talent, and we really wanted to use that to our advantage in an episode about stillness and peace. "Give Peace a Chance" was originally broadcast on October 29, 2009 in the United States on the American Broadcasting Company (ABC). The episode underperformed the previous installment, "I Saw What I Saw", in terms of viewership. It was viewed by a total of 13.74 million people,[8] down 1.66 percent from the previous episode, which garnered 15.04 million viewers.[9] In terms of viewership, "Give Peace a Chance" ranked fourth for the night, behind game two of the 2009 World Series, and CBS's CSI and The Mentalist.[8] The installment did not rank high for viewership, but its 5.2/13 Nielsen rating ranked second in its 9:00 Eastern time-slot and the entire night for both the rating and share percentages of the 18–49 demographic, losing to the 2009 World Series but beating CSI, The Mentalist, and Private Practice.[8] Although its rating was in the top rankings for the night, it was a decrease from the previous episode, which garnered a 5.6/14 rating/share in the 18–49 demographic.[9]
 Critics were largely positive in their reviews of the episode. The Huffington Post's Michael Pascua called "Give Peace a Chance" a "hit and miss" episode, criticizing the slang dialogue by saying it "sounded like it came from an MTV drama", but praising the installment's "character driven development".[10] Pascua was positive on the development of Tahir's character, writing: "I hope he comes back in a later episode just to remind these people about patience and hope."[10] TV Fanatic's Steve Marsi gave a positive review of the episode, saying it "won [him] over", and also praising Tahir's character.[11] Marsi applauded the development of Dempsey's character, calling him "the best [doctor]", and noted that Wilson's directing may "net her an Emmy nomination".[11]
 Writing for Entertainment Weekly, Jennifer Armstrong had mixed feedback on the episode, writing: "The all-medical, all-the-time episodes need to stop."[12] However, she found it "fantastic" when Shepherd drew on the wall, and enjoyed Tahir's character, calling him "lovely".[12] Armstrong also said that "Give Peace a Chance" was in "ER territory", adding: "I do not watch Grey's Anatomy to get my ER fix."[12] Adam Bryant of TV Guide enjoyed this episode compared to the previous one, but disliked the possible romantic development between Karev and Adamson. In his review, he concluded that the installment "proves that Meredith Grey doesn't have to do all the heavy lifting on this show".[13]
 People's Carrie Bell enjoyed the episode, praising the balance of cast members. She called Isaac "beloved", and found that the teamwork in the episode proved "there's no 'I' in team".[14] Former Star-Ledger editor Alan Sepinwall gave a positive review of the entry, applauding the shift in themes and Shepherd's character development.[15] Writing for BuddyTV, Glenn Diaz found the episode comical, calling the scene in which Robbins stands up to Webber "hilarious", and naming Yang the installment's "comic relief".[16] Referring to Shepherd and Webber's constant arguments, an AfterEllen senior editor said: "Seriously, these two need to drop their pants and get it over with."[17] Peter Nowalk's writing of the episode was nominated for a Humanitas Prize in the 60 Minute Category.[18] The episode also earned Wilson an NAACP Image Award under the Outstanding Directing in a Dramatic Series category.[19]
 
 Mark Saul as Dr. Steve Mostow Nora Zehetner as Dr. Reed Adamson Jesse Williams as Dr. Jackson Avery Faran Tahir as Isaac 1 Plot 2 Production 3 Reception 4 References 5 External links ^ "Grey's Anatomy, Season 6". iTunes Store. Apple. Retrieved August 20, 2012..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ Writer: Nowalk, Peter. Director: Wilson, Chandra (October 29, 2009). "Give Peace a Chance". Grey's Anatomy. Season 6. American Broadcasting Company.
 ^ Ausiello, Michael (September 2, 2009). "Exclusive: Katherine Heigl takes 'Grey's Anatomy' leave of absence". Entertainment Weekly. Time Inc. Retrieved July 8, 2012.
 ^ "Grey's Anatomy 'Give Peace a Chance' Music". TV Fanatic. SheKnows Entertainment. Retrieved July 20, 2012.
 ^ "Grey's Anatomy Cast and Details". TV Guide. Retrieved August 14, 2012.
 ^ Clarendon, Dan (December 20, 2010). "Where Is Grey's Anatomy Filmed, Anyway?". Wetpaint. The Cambio Network. Retrieved May 24, 2012.
 ^ a b c d e f g h Nowalk, Peter (October 29, 2009). "Pete Nowalk on 'Give Peace a Chance'..." Grey Matter. American Broadcasting Company. Archived from the original on May 13, 2013. Retrieved July 20, 2012.
 ^ a b c Seidman, Robert (October 30, 2009). "Thursday finals: 'Grey's Anatomy', 'The Mentalist', '30 Rock' tick up, 'Parks & Recreation', 'The Jay Leno Show' down". TV by the Numbers. Zap2it. Retrieved September 12, 2010.
 ^ a b Gorman, Bill (October 23, 2010). "TV Ratings Thursday: 'Community', 'FlashForward', 'Leno' Down; 'Survivor', 'Grey's' Up". TV by the Numbers. Zap2it. Archived from the original on January 4, 2011. Retrieved September 12, 2010.
 ^ a b Pascua, Michael (October 30, 2009). "Review: Grey's Anatomy – Give Peace a Chance". The Huffington Post. AOL, Inc. Retrieved July 21, 2012.
 ^ a b Marsi, Steve (October 30, 2009). "Grey's Anatomy Review: 'Give Peace a Chance'". TV Fanatic. SheKnows Entertainment. Retrieved July 21, 2012.
 ^ a b c Armstrong, Jennifer (October 30, 2009). "'Grey's Anatomy' recap: Surgical Precision". Entertainment Weekly. Time Inc. Retrieved August 17, 2012.
 ^ Bryant, Adam (October 29, 2009). "Grey's Anatomy Episode Recap: 'Give Peace a Chance' Season 6, Episode 6". TV Guide. Archived from the original on August 28, 2011. Retrieved July 21, 2012.
 ^ Bell, Carrie (October 30, 2009). "Grey's: McDreamy Goes Rogue". People. Time Inc. Retrieved August 17, 2012.
 ^ Sepinwall, Alan (October 30, 2009). "Grey's Anatomy, 'Give Peace a Chance': Eeeny-meeny diaper genie" What's Alan Watching? Blogspot. Retrieved July 21, 2012.
 ^ Diaz, Glenn (October 29, 2009). "'Grey's Anatomy' 'Give Peace a Chance' Recap – The Mother of All Tumors and Diapers". BuddyTV. Archived from the original on December 24, 2013. Retrieved July 21, 2012.
 ^ "'Grey's Anatomy: mini-cap: 'Give Peace a Chance'". AfterEllen. Logo. November 2, 2009. Archived from the original on February 9, 2010. Retrieved July 21, 2012.
 ^ "Finalists Announced for 36th Annual HUMANITAS Prize Honoring Great Television and Film Writing". PR Newswire. July 14, 2010. Retrieved May 30, 2013.
 ^ "2010 NAACP Image Awards winners: Darryl 'Chill' Mitchell, Precious, Chris Rock and more". Zap2it. Tribune Company. February 26, 2010. Archived from the original on April 3, 2010. Retrieved July 21, 2012.
 Television portal "Give Peace a Chance" at ABC.com "Give Peace a Chance" on IMDb "Give Peace a Chance" at TV.com v t e "A Hard Day's Night" "If Tomorrow Never Comes" "Into You Like a Train" "It's the End of the World" "As We Know It" "Losing My Religion" "Time Has Come Today" "I Am a Tree" "Great Expectations" "Wishin' and Hopin'" "Didn't We Almost Have It All?" "A Change Is Gonna Come" "Stand By Me" "Elevator Love Letter" "What a Difference a Day Makes" "Now or Never" "Good Mourning" "Goodbye" "I Always Feel Like Somebody's Watchin' Me" "Tainted Obligation" "Invasion" "I Saw What I Saw" "Give Peace a Chance" "Invest in Love" "I Like You So Much Better When You're Naked" "Death and All His Friends" "Song Beneath the Song" "Flight" "Going, Going, Gone" "Beautiful Doom" "Perfect Storm" "Fear (Of the Unknown)" "I Must Have Lost it on the Wind" "Puzzle with a Piece Missing" "Got to Be Real" "Only Mama Knows" "Bend & Break" "Don't Let's Start" "One Flight Down" "How to Save a Life" "She's Leaving Home" "Time Stops" "You're My Home" "Sledgehammer" "Guess Who's Coming to Dinner" "Something Against You" "Things We Lost in the Fire" "The Sound of Silence" "Family Affair" "True Colors" "Danger Zone" "Personal Jesus" "All of Me" "Flowers Grow Out of My Grave" "We Didn't Start the Fire"  2009 American television episodes Grey's Anatomy (season 6) episodes Articles with short description Television episode articles with short description for single episodes Television episode articles with short description and disambiguated page names Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Português 中文  This page was last edited on 23 September 2019, at 01:46 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  It's quieter than a typical episode. More single-minded. Derek is our sole focus. And really, what an amazing character to spend an entire episode with. Watch Patrick Dempsey on your screen and you can't help but be struck by how much he says without saying anything. The guy can pretty much give you an entire soliloquy with just one look. That's a rare talent, and we really wanted to use that to our advantage in an episode about stillness and peace.— Peter Nowalk, Grey Matter[7]
 Give Peace a Chance Previous Next Give Peace a Chance ^ ^ ^ ^ ^ ^ a b c d e f g h a b c a b a b a b a b c ^ ^ ^ ^ ^ ^ ^ Grey's Anatomy episode Season 6Episode 7 Chandra Wilson Peter Nowalk "Moon and Moon" October 29, 2009 (2009-10-29) 43 minutes 
Mark Saul as Dr. Steve Mostow
Nora Zehetner as Dr. Reed Adamson
Jesse Williams as Dr. Jackson Avery
Faran Tahir as Isaac
 


← Previous"I Saw What I Saw"

Next →"Invest in Love"
 ← Previous"I Saw What I Saw"
 Next →"Invest in Love"
 Grey's Anatomy (season 6) List of Grey's Anatomy episodes 
"A Hard Day's Night"
"If Tomorrow Never Comes"
 
"Into You Like a Train"
"It's the End of the World"
"As We Know It"
"Losing My Religion"
 
"Time Has Come Today"
"I Am a Tree"
"Great Expectations"
"Wishin' and Hopin'"
"Didn't We Almost Have It All?"
 
"A Change Is Gonna Come"
 
"Stand By Me"
"Elevator Love Letter"
"What a Difference a Day Makes"
"Now or Never"
 
"Good Mourning"
"Goodbye"
"I Always Feel Like Somebody's Watchin' Me"
"Tainted Obligation"
"Invasion"
"I Saw What I Saw"
"Give Peace a Chance"
"Invest in Love"
"I Like You So Much Better When You're Naked"
"Death and All His Friends"
 
"Song Beneath the Song"
 
"Flight"
 
"Going, Going, Gone"
"Beautiful Doom"
"Perfect Storm"
 
"Fear (Of the Unknown)"
 
"I Must Have Lost it on the Wind"
"Puzzle with a Piece Missing"
"Got to Be Real"
"Only Mama Knows"
"Bend & Break"
"Don't Let's Start"
"One Flight Down"
"How to Save a Life"
"She's Leaving Home"
"Time Stops"
"You're My Home"
 
"Sledgehammer"
"Guess Who's Coming to Dinner"
"Something Against You"
"Things We Lost in the Fire"
"The Sound of Silence"
"Family Affair"
 
"True Colors"
 
"Danger Zone"
"Personal Jesus"
"All of Me"
 
"Flowers Grow Out of My Grave"
"We Didn't Start the Fire"
 

 