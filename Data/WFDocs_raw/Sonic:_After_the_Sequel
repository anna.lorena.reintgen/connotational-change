
 Sonic: After the Sequel is a 2013 platform video game created by Brazilian student Felipe Daneluz (LakeFeperd). It is an unofficial game based on the Sonic the Hedgehog series and set between the official games Sonic the Hedgehog 2 and Sonic the Hedgehog 3. Daneluz's second Sonic game, it follows Sonic: Before the Sequel, which set between the original Sonic the Hedgehog and Sonic the Hedgehog 2. Like its predecessor, After the Sequel stars Sonic the Hedgehog and his sidekick Tails in a quest to retrieve the Chaos Emeralds from Doctor Eggman.
 After the Sequel was inspired by Sonic Heroes and other games both inside and outside the Sonic series, and it was developed with Sonic Worlds, an engine based in Multimedia Fusion 2 that reduces the amount of computer programming involved in game creation. It was released as a free download for Microsoft Windows personal computers on June 15, 2013. The game was very well received by video game journalists, who lauded its preservation of retro Sonic gameplay and its eclectic, 1990s-style soundtrack. The trilogy of Before the Sequel, After the Sequel, and their successor Sonic: Chrono Adventure performed unusually well for fangames, having been downloaded 120,000 times by March 2014.
 After the Sequel is a 2D platformer in the style of the Sonic games for the Sega Genesis.[1][2] As such, it lets the player control either the blue hedgehog Sonic or his orange fox friend Tails.[3] Both characters can move left and right with the arrow keys and jump with the "Z" key;[4] Tails can also fly to reach areas Sonic cannot.[5] The game takes place in seven levels, known as zones,[6] each divided into three acts followed by a boss fight with Doctor Eggman.[7] These zones are designed for fast-paced gameplay,[2] featuring typical Sonic obstacles such as bottomless pits and vertical loops.[3] The zones are based on various themes, including haunted houses,[8] cities,[9] magma caverns,[10] winter theme parks,[11] and sugar processing plants.[12]
 The player collects rings in zones and boss fights as a form of health: upon being hit by an enemy or harmful obstacle, the player's rings will scatter and can be recollected. Being hit while carrying no rings, being crushed by an obstacle, or falling into a bottomless pit causes the player to lose a life; running out of lives results in a game over screen, after which the player must restart the zone from act one. Conversely, collecting 100 rings gets the player an extra life, and completing an act with 50 takes the player to a special stage, where they can collect rings for extra lives. The game also features power-ups throughout its zones,[5] which are activated with the "X" key:[4] these include typical Sonic power-ups such as shields and extra lives,[5] as well as new ones. The "Beam" power-up for Sonic and the "Mirror" power-up for Tails are borrowed from and credited to Nintendo's Kirby series.[13]
 At the end of Sonic the Hedgehog 2, Sonic and Tails fly over the ocean in Tails' biplane, the Tornado, after defeating Doctor Eggman.[14] Beginning from this point,[1] After the Sequel depicts Sonic and Tails noticing a forested island and deciding to make a landing and explore. After completing the first zone, Sonic finds a robot resembling Tails—a trap sprung by Eggman. The robot ensnares Sonic in a forcefield while Eggman steals Sonic's Chaos Emeralds. Sonic escapes and sets out with Tails to recover the Emeralds.
 The two travel through more zones and fight Eggman at the end of each one. They follow Eggman to a forest, where he leads a massive logging operation. Together with Mighty the Armadillo, they destroy his machinery and stop the operation. They continue through the forest and find ghosts that frighten Tails. Destroying one of Eggman's robots lets Sonic see its internal architecture, including a screen that shows Eggman's plans to create an empire and drop a floating island into the sea.
 In Parhelion Peak, the game's snow zone, Sonic and Tails notice a feather float down from the sky. They board another of Eggman's airships and complete another zone, whereupon they find another feather. They trace the feathers to Eggman's bird-like robot, which is guarding the Emeralds. Sonic retrieves the Emeralds and uses them to become Super Sonic. He fights the robot as the game's final boss. After its defeat, Sonic and Tails fly aboard the Tornado once more—the segue into the events of Sonic the Hedgehog 3.[1]
 After the Sequel was created by Felipe Daneluz (known on the Internet as "LakeFeperd"), a student from São Paulo, Brazil. Unlike many longtime Sonic fans, Daneluz remained supportive of the series through its "dark age" in the mid to late 2000s and enjoyed games such as Sonic Riders.[i] Because of his continued enthusiasm toward later Sonic games, Daneluz decided to create a retro-styled Sonic game. Not being well versed in computer programming, Daneluz took to the open-source, visual game engine Sonic Worlds, which is tailored to create Sonic zones[3] within the program Multimedia Fusion 2.[21] Determined to make his game stand out despite his limited technical proficiency, Daneluz decided to set his first game in the time between Sonic the Hedgehog and Sonic 2, calling it Sonic: Before the Sequel. He set the follow-up, entitled Sonic: After the Sequel, between Sonic 2 and Sonic 3, and a third installment, Sonic Chrono Adventure, between Sonic 3 and Sonic CD.[3]
 After the Sequel's zones were inspired largely by those of Sonic Heroes.[8] One level called RedHot Ride Zone, however, was based mainly on a level of the same name in Donkey Kong Country 2: Diddy's Kong Quest.[10] Ideas for other zones came from Sonic Riders[9] and a Sonic-style song called "Combat Night Zone" by electronic artist MaxieDaMan.[11] Daneluz imagined Sonic being high on sugar in the Sugar Splash Zone.[12] Daneluz began creating each level by sketching out ideas on paper, then transferring them to Adobe Photoshop before working on the enemies and level design.[21] Unlike the publishers of many other games on which fangames have been based, particularly Nintendo, Sonic series publisher Sega has not sent a cease and desist order or other indication of disapproval to Daneluz. He has speculated that the company does not want to upset its fanbase.[3]
 The music composition and recording were handled by underground musicians Falk Au Yeong, Funk Fiction, Andy Tunstall,[3] James Landino, DJ Max-E, Mr. Lange, and Li Xiao'an.[5] Daneluz had not made plans to incorporate original music until Falk approached him requesting collaboration on the game. Funk Fiction has claimed that the music spans more than twenty genres and was influenced by rock, jazz, disco, and trip hop and the soundtracks of game franchises like Sonic, Donkey Kong, and Kirby.[3] Due to the levels in After the Sequel generally taking longer to complete than those in Before the Sequel, the music tracks are longer, estimated by Falk as ranging from one minute and 45 seconds to three minutes.[21]
 In August 2017, Daneluz re-released the game as Sonic: After the Sequel DX. This version features improved physics, the addition of the drop dash from Sonic Mania, and a new final boss.[22]
 After the Sequel has received positive coverage for its revitalization of retro Sonic gameplay. Tony Ponce of Destructoid summarized it as "quite the fun little gem that keeps the Genesis-era Sonic spirit alive". He contrasted the retro, fan-made After the Sequel with Sega's efforts to create high-quality 3D titles, lamenting that, fifteen years after the release of Sonic Adventure, Sega was "only now starting to get the hang of the third dimension."[23] John Polson from IndieGames.com called the game "fantastic" and "stunning",[24] while Kotaku's András Neltz stated that "it looks amazing" and advised readers not to let the game's status as a fan work turn them away from it.[1] Nintendo Life writer Damien McFerran also called the game "impressive".[25] The UK gaming staff for Red Bull's website stated that, despite Sega's continual releases of Sonic games, "few have quite recaptured the thrill of blazing through a 2D labyrinth at lightspeed in the way that fan made Sonic After The Sequel has."[2] A second Red Bull article, by author Ben Sillis, exclaimed that "you have to play" it.[3]
 The game's music has been particularly well received. Ponce called it "the best music ever" and "simply indescribable", opining that it raised an already high-quality product "to god tier".[23] Ponce wrote an article dedicated to the game's music two days later, clarifying that it equals or surpasses the quality of any other Sonic game's soundtrack.[26] The more reserved Polson claimed that the music "definitely rocks" and is clearer than that of Genesis games.[24] Similarly, the Red Bull staff called the music "absolutely stunning", likening it to gaming soundtracks of the early 1990s.[2] For McFerran, the soundtrack was "just as noteworthy" as the rest of the game.[25]
 The game is available as a free download for Windows personal computers.[2] As of March 2014, the trilogy had been downloaded 120,000 times—an unusually high number for fangames—as compared to the 640,000 copies of the official game Sonic Lost World (also released in 2013) sold on the Wii U by the same time.[3]
 Falk Au Yeong Funk Fiction Andy Tunstall James Landino Mr Lange DJ Max-E Li Xiao'an 1 Gameplay 2 Plot 3 Development 4 Reception 5 References 6 External links ^ GameRankings scores of titles released in 2006:
Sonic Riders: 43.33% – 63.46%[15][16]
Sonic the Hedgehog (2006): 46.12% – 48.74%[17][18]
Sonic Genesis: 32.50%[19]
Sonic Rivals: 66.17%[20]
 Sonic Riders: 43.33% – 63.46%[15][16] Sonic the Hedgehog (2006): 46.12% – 48.74%[17][18] Sonic Genesis: 32.50%[19] Sonic Rivals: 66.17%[20] ^ a b c d Neltz, András (June 20, 2013). "There's a New Sonic Out on PC. It's a Fangame and It Looks Amazing". Kotaku. Archived from the original on April 19, 2014. Retrieved May 11, 2014..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c d e Red Bull UK (August 30, 2013). "Extra life: The amazing fan-made game revivals". Red Bull. Archived from the original on December 12, 2013. Retrieved May 11, 2014.
 ^ a b c d e f g h i Sillis, Ben (March 19, 2014). "The fan made Sonic trilogy you have to play". Red Bull. Archived from the original on May 6, 2014. Retrieved May 11, 2014.
 ^ a b LakeFeperd (June 15, 2013). Sonic: After the Sequel. Instructions: Arrows: Move / Z: Jump / X: Special
 ^ a b c d LakeFeperd (June 15, 2013). Sonic: After the Sequel.
 ^ LakeFeperd (June 15, 2013). Sonic: After the Sequel. Credits: Music: Special Stages: 1: Falk; 2: Falk; 3: Falk; 4: Mr Lange; 5: Mr Lange; 6: Funk Fiction; 7: KgZ + andY singing roflrofl
 ^ LakeFeperd (June 15, 2013). Sonic: After the Sequel. Credits: Act 1: Falk; Act 2: Funk Fiction; Act 3: KgZ; Boss: Falk
 ^ a b LakeFeperd (June 15, 2013). Sonic: After the Sequel. Credits: Moon Mansion Zone: Inspired by the Haunted Levels in Sonic Heroes, well actually a lot of the levels in this game were inspired by Sonic Heroes' levels °-°
 ^ a b LakeFeperd (June 15, 2013). Sonic: After the Sequel. Credits: Cyan City Zone: It was originally inspired by Sonic Heroes and Riders' city stages, along with some elements seen around where I live
 ^ a b LakeFeperd (June 15, 2013). Sonic: After the Sequel. Credits: RedHot Ride Zone: It really looks like Rocky Ride from Before the Sequel, right? But actually, a lot of ideas for this level came from Donkey Kong Country 2. There is also a level in that game with the same name. Also, the lava section was NOT inspired by Super Metroid.
 ^ a b LakeFeperd (June 15, 2013). Sonic: After the Sequel. Credits: Parhelion Peak Zone: It was planned since the beginning, but the whole overall concept of the zone came from the song Combat Night Zone by MaxieDaMan.
 ^ a b LakeFeperd (June 15, 2013). Sonic: After the Sequel. Credits: Sugar Splash Zone: Originally came from just a simple concept: what if Sonic was high on sugar, in a sugar factory!
 ^ LakeFeperd (June 15, 2013). Sonic: After the Sequel. Credits: Kirby Power-ups: Nintendo
 ^ Sonic Team (November 24, 1992). Sonic the Hedgehog 2. Level/area: Ending sequence.
 ^ "Sonic Riders for PC". GameRankings. Archived from the original on June 20, 2012. Retrieved July 18, 2014.
 ^ "Sonic Riders for GameCube". GameRankings. Archived from the original on June 20, 2012. Retrieved July 18, 2014.
 ^ "Sonic the Hedgehog for PS3". GameRankings. Archived from the original on April 20, 2012. Retrieved July 18, 2014.
 ^ "Sonic the Hedgehog for Xbox 360". GameRankings. Archived from the original on April 20, 2012. Retrieved July 18, 2014.
 ^ "Sonic the Hedgehog Genesis for GBA". GameRankings. Archived from the original on February 17, 2012. Retrieved July 18, 2014.
 ^ "Sonic Rivals for PSP". Game Rankings. Archived from the original on March 11, 2012. Retrieved July 18, 2014.
 ^ a b c Balzani, Louis (interview with LakeFeperd) (August 9, 2012). "SAGE 2012: Sonic Before (and After) the Sequel". TSSZNews. Archived from the original on July 27, 2015. Retrieved October 29, 2014.
 ^ "Sonic After the Sequel DX - Sonic and Sega Retro Message Board". Sonic Retro. Archived from the original on October 13, 2017. Retrieved August 29, 2017.
 ^ a b Ponce, Tony (July 8, 2013). "Sonic After the Sequel fan game has the BEST MUSIC EVER". Destructoid. Archived from the original on April 11, 2014. Retrieved May 11, 2014.
 ^ a b Polson, John (July 14, 2013). "Fan Game Picks: MegaMan Unlimited and Sonic After the Sequel". IndieGames.com. Archived from the original on March 1, 2014. Retrieved May 11, 2014.
 ^ a b McFerran, Damien (March 20, 2014). "How One Man Is Giving Sonic Fans The Game They Want To Play". Nintendo Life. Archived from the original on March 27, 2014. Retrieved May 11, 2014.
 ^ Ponce, Tony (July 10, 2013). "Listen to the AMAZING Sonic After the Sequel OST". Destructoid. Archived from the original on October 23, 2015. Retrieved May 11, 2014.
 Video games portal Official website hosted by Google Official website of Sonic: Before the Sequel After the sequel's prequel, hosted by Google v t e List of games List of features Sonic the Hedgehog (1991) 2 CD 3 Sonic & Knuckles 3D Blast Adventure Adventure 2 Heroes Sonic the Hedgehog (2006) Unleashed 4: Episode I Colors Generations 4: Episode II Lost World Mania Forces Sonic the Hedgehog 2 Chaos Triple Trouble Blast Pocket Adventure Advance Advance 2 Advance 3 Rush Rush Adventure Rise of Lyric Shattered Crystal Fire & Ice Knuckles' Chaotix Tails Adventure Shadow the Hedgehog Rivals Secret Rings Rivals 2 Black Knight Drift Drift 2 R Riders Riders: Zero Gravity All-Stars Racing Free Riders All-Stars Racing Transformed Team Sonic Racing Mario & Sonic at the Olympic Games Olympic Winter Games London 2012 Olympic Games Sochi 2014 Olympic Winter Games Rio 2016 Olympic Games Olympic Games Tokyo 2020 Jump Jump Fever Dash Dash 2: Sonic Boom Runners Runners Adventure Educational games SegaSonic the Hedgehog Spinball Dr. Robotnik's Mean Bean Machine Labyrinth Fighters Shuffle Pinball Party Battle Chronicles: The Dark Brotherhood X-treme Extreme Sonic the Hedgehog Doctor Eggman Tails Knuckles the Echidna Shadow the Hedgehog Chao Adventures of Sonic Sonic the Hedgehog (TV series) OVA Underground
characters characters X
episodes episodes Boom
episodes episodes Feature film Sonic the Comic Archie Comics series (issues)
Knuckles the Echidna
Sonic Universe Knuckles the Echidna Sonic Universe IDW Publishing series Sega All-Stars Super Smash Bros. Green Hill Zone Sonic Team Retro Engine Somari Sonic: After the Sequel Sonic Dreams Collection 2013 video games Fangames Freeware games Indie video games Platform games Side-scrolling video games Sonic the Hedgehog video games Video game sequels Video games developed in Brazil Windows games Windows-only games Sonic the Hedgehog fangames Articles with short description Featured articles Articles using Infobox video game using locally defined parameters Articles using Wikidata infoboxes with locally defined images Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Français Hawaiʻi  This page was last edited on 26 September 2019, at 15:50 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Sonic: After the Sequel ^ a b c d a b c d e a b c d e f g h i a b Instructions: a b c d ^ Credits: ^ Credits: a b Credits: a b Credits: a b Credits: a b Credits: a b Credits: ^ Credits: ^ ^ ^ ^ ^ ^ ^ a b c ^ a b a b a b ^ Notes Footnotes Title screen LakeFeperd (Felipe Daneluz) LakeFeperd 
Falk Au Yeong
Funk Fiction
Andy Tunstall
James Landino
Mr Lange
DJ Max-E
Li Xiao'an
 Sonic the Hedgehog (unofficially) Sonic Worlds (Multimedia Fusion 2) Microsoft Windows June 15, 2013 Platform Single-player 
List of games
List of features
 Console
Sonic the Hedgehog (1991)
2
CD
3
Sonic & Knuckles
3D Blast
Adventure
Adventure 2
Heroes
Sonic the Hedgehog (2006)
Unleashed
4: Episode I
Colors
Generations
4: Episode II
Lost World
Mania
Forces
Handheld
Sonic the Hedgehog
2
Chaos
Triple Trouble
Blast
Pocket Adventure
Advance
Advance 2
Advance 3
Rush
Rush Adventure
 
Sonic the Hedgehog (1991)
2
CD
3
Sonic & Knuckles
3D Blast
Adventure
Adventure 2
Heroes
Sonic the Hedgehog (2006)
Unleashed
4: Episode I
Colors
Generations
4: Episode II
Lost World
Mania
Forces
 
Sonic the Hedgehog
2
Chaos
Triple Trouble
Blast
Pocket Adventure
Advance
Advance 2
Advance 3
Rush
Rush Adventure
 PlatformersSonic Boom
Rise of Lyric
Shattered Crystal
Fire & Ice
Other
Knuckles' Chaotix
Tails Adventure
Shadow the Hedgehog
Rivals
Secret Rings
Rivals 2
Black Knight
Racing
Drift
Drift 2
R
Riders
Riders: Zero Gravity
All-Stars Racing
Free Riders
All-Stars Racing Transformed
Team Sonic Racing
Mario & Sonic
Mario & Sonic at the Olympic Games
Olympic Winter Games
London 2012 Olympic Games
Sochi 2014 Olympic Winter Games
Rio 2016 Olympic Games
Olympic Games Tokyo 2020
Mobile
Jump
Jump Fever
Dash
Dash 2: Sonic Boom
Runners
Runners Adventure
Other
Educational games
SegaSonic the Hedgehog
Spinball
Dr. Robotnik's Mean Bean Machine
Labyrinth
Fighters
Shuffle
Pinball Party
Battle
Chronicles: The Dark Brotherhood
 Sonic Boom
Rise of Lyric
Shattered Crystal
Fire & Ice
Other
Knuckles' Chaotix
Tails Adventure
Shadow the Hedgehog
Rivals
Secret Rings
Rivals 2
Black Knight
 
Rise of Lyric
Shattered Crystal
Fire & Ice
 
Knuckles' Chaotix
Tails Adventure
Shadow the Hedgehog
Rivals
Secret Rings
Rivals 2
Black Knight
 
Drift
Drift 2
R
Riders
Riders: Zero Gravity
All-Stars Racing
Free Riders
All-Stars Racing Transformed
Team Sonic Racing
 
Mario & Sonic at the Olympic Games
Olympic Winter Games
London 2012 Olympic Games
Sochi 2014 Olympic Winter Games
Rio 2016 Olympic Games
Olympic Games Tokyo 2020
 
Jump
Jump Fever
Dash
Dash 2: Sonic Boom
Runners
Runners Adventure
 
Educational games
SegaSonic the Hedgehog
Spinball
Dr. Robotnik's Mean Bean Machine
Labyrinth
Fighters
Shuffle
Pinball Party
Battle
Chronicles: The Dark Brotherhood
 
X-treme
Extreme
 
Sonic the Hedgehog
Doctor Eggman
Tails
Knuckles the Echidna
Shadow the Hedgehog
Chao
 
Adventures of Sonic
Sonic the Hedgehog (TV series)
OVA
Underground
characters
X
episodes
Boom
episodes
Feature film
 
Sonic the Comic
Archie Comics series (issues)
Knuckles the Echidna
Sonic Universe
IDW Publishing series
 Video games
Sega All-Stars
Super Smash Bros.
Other
Green Hill Zone
Sonic Team
Retro Engine
Unlicensed
Somari
Sonic: After the Sequel
Sonic Dreams Collection
 
Sega All-Stars
Super Smash Bros.
 
Green Hill Zone
Sonic Team
Retro Engine
 
Somari
Sonic: After the Sequel
Sonic Dreams Collection
 