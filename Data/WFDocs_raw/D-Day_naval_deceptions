
 Initial Airborne AssaultBritish Sector
 American Sector
 Normandy landingsAmerican Sector
 Anglo-Canadian Sector
 British logistics
 Initial ground campaignAmerican Sector
 
Anglo-Canadian Sector
 Breakout
 Air and Sea Operations
 Supporting operations
 Aftermath
 Operations Taxable, Glimmer and Big Drum were tactical military deceptions conducted on 6 June 1944 in support of the Allied landings in Normandy. The operations formed the naval component of Operation Bodyguard, a wider series of tactical and strategic deceptions surrounding the invasion.
 Small boats, along with aircraft from RAF Bomber Command, simulated invasion fleets approaching Cap d'Antifer, Pas-de-Calais and Normandy. Glimmer and Taxable played on the German belief, amplified by Allied deception efforts over the preceding months, that the main invasion force would land in the Calais region. Big Drum was positioned on the western flank of the real invasion force to try to confuse German forces about the scale of the landings. These operations complemented Operation Titanic, which was intended to confuse the Germans about the D-Day airborne forces.
 It is unclear whether the operations were successful, due to the complexity of their execution, poor weather, and lack of response from German forces. It is possible that they contributed to the overall confusion of D-Day as part of the wider Bodyguard plan.
 Glimmer, Taxable and Big Drum were World War II deception operations. They were conducted as part of Operation Bodyguard, a broad strategic military deception intended to support the Allied invasion of German-occupied France in June 1944. Bodyguard was designed to confuse the Axis high command as to Allied intentions during the lead-up to the invasion. The London Controlling Section (LCS) had spent some time convincing German commanders that the fictional First United States Army Group (FUSAG) represented the bulk of the Allied invasion force. FUSAG's existence was fabricated through Operation Fortitude South.[1]
 The Allied story for FUSAG was that the army group, based in south-east England, would invade the Pas-de-Calais region several weeks after a smaller diversionary landing in Normandy. In reality, the main invasion force would land in Normandy on D-Day. As D-Day approached, the LCS moved on to planning tactical deceptions to help cover the progress of the real invasion forces.[2] As well as naval operations, the LCS also planned operations involving paratroopers and ground deceptions. The latter would come into effect once landings were made but the former (involving naval, air and special forces units) were used to cover the approach of the true invasion fleet.[3]
 In preparation for the coming landings, Allied scientists had worked on techniques for obscuring the size and disposition of an invasion force.[4] The German defences relied on the Seetakt radar system. Scientists from the Telecommunications Research Establishment discovered that the resolution of the Seetakt was about 520 yards (480 m). To deceive the radar system they proposed dropping clouds of aluminium foil (chaff, then code-named Window) at two mile intervals. The clouds would appear as a continuous blip, similar to one created by an approaching fleet, on German screens. The Allies also repurposed radio equipment, code named Moonshine, to jam the Seetakt signal.[5] Allied command decided that, rather than mask the approaching fleet, these measures would serve to alert German defences. So it was decided to combine these techniques with small groups of boats to simulate an entire invasion fleet aimed at the Calais region.[4]
 Allied planners proposed that small boats, towing large radar reflecting balloons (code named Filbert) and carrying both Moonshine jamming and standard wireless equipment (for transmitting fake traffic), would advance toward the French coast under a cloud of Window. The chaff and other countermeasures would hide the small size of the naval force while wireless traffic would play on the FUSAG story to mislead the Germans into expecting a major landing. A third deceptive force, Operation Big Drum, would use radar countermeasures on the western flank of the true invasion fleet. This operation was intended to lend confusion as to the extent of the landings in Normandy.[4][5]
 Glimmer and Taxable were very similar operations. They were executed in the early hours of 6 June 1944 whilst the invasion fleet was approaching Normandy. Taxable simulated an invasion force approaching Cap d'Antifer (about 80 km from the actual D-Day landings) and Glimmer spoofed an invasion at Pas-de-Calais (far from Normandy). By dropping chaff in progressive patterns, Royal Air Force (RAF) bombers for both operations were able to create the illusion of a large fleet on coastal radar screens. Beneath the chaff, small boats towed radar reflector balloons and simulated the radio traffic expected of a large fleet.[4] Once German forces were drawn to the coast, it was planned that the RAF would attempt to contain them in this region, and away from the actual invasion site, by bombing bridges and roads.[6] The operations required precise flying in elongated circuits with replacement aircraft having to merge in seamlessly to avoid tell-tale gaps.[7] The bombers were staged at 2-mile (3.2 km) intervals parallel to the French coast. Once in position they would spend two and a half minutes flying toward the coast, dropping chaff at fifteen-second intervals. Then the aircraft would turn and head away from the coast for two minutes and ten seconds. By repeating this circuit, the wide cloud of chaff edged toward the coast just like a real sea-borne fleet.[6] The aircraft had to be modified by cutting a hole in the nose to allow the large quantities of chaff to be dropped.[8]
 The larger of the two operations, Taxable, was carried out by 18 small boats, a mix of Harbour Defence Motor Launches (HDML) and RAF Pinnaces, designated Special Task Force A.[7][9] Chaff was dropped by Lancaster bombers from the No. 617 "Dam Busters" Squadron. Each aircraft carried an expanded crew of up to 14. The squadron began training for the operation on 7 May, but were not aware of their final target.[3][6] Task Force A left port in the evening of 5 June, but struggled in bad seas which affected their equipment and ability to converge at their meeting point. By 00:37 on 6 June the lead boats were on schedule and had reached the muster point. Between 02:00 and 04:00 the ships operated radar and radio equipment as they headed toward a point 7 miles (11 km) offshore. From there the task force simulated a landing attempt; by running fast to within 2 miles (3.2 km) of the beach before returning to the 7-mile marker under cover of smoke. During this time only a small German response was observed including searchlights and intermittent gunfire. Shortly after 05:00 the operation ended and the task force laid mines before heading toward Newhaven, reaching port by midday.[7]
 The air operations for Glimmer were conducted by No. 218 "Gold Coast" Squadron under Wing Commander R. M. Fenwick-Wilson. The squadron flew six Short Stirling bombers on the operation, with two additional airborne reserve aircraft.[10] Each aircraft carried two pilots who rotated flying duties. The naval contingent, Special Task Force B under the command of Lieutenant Commander W. M. Rankin, consisted of 12 HDMLs equipped with jamming gear, radios and radar-reflecting balloons. The task force began jamming operations at approximately 01:00 followed by radio chatter around an hour later.[11]
 Glimmer elicited more response from German forces than Taxable including reconnaissance planes sent to investigate the "fleet". After completing their assignment (which, unlike Taxable, did not include laying mines) the ships returned to port, reaching their berths by 13:00 on D-Day.[11]
 Big Drum was similar to the other D-Day naval deceptions, but without an airborne component. Task Force C consisted of four HDMLs, whose job was to operate as a distraction on the western flank of the invasion. The plan originally called for the task force, which was attached to Force U (the westernmost convoy of the invasion fleet), to operate radar jamming equipment as it approached the French coast, holding 2 miles (3.2 km) off shore until first light. After the Germans failed to respond, the ships moved to within 1.5 miles (2.4 km) of the coast. No response, either in the air or on the shore, was observed, and the convoy returned safely to Newhaven.[12]
 Taxable, Glimmer and Big Drum were complicated in execution, requiring coordination of air and naval forces. Launched in poor weather conditions, Taxable did not appear to have the desired effect and failed to elicit any significant response from the Germans. The reaction to Glimmer was more encouraging. The attacks on the bomber squadrons indicated, at least to the satisfaction of RAF Bomber Command, that the Germans believed a genuine threat existed. There is no evidence that Big Drum elicited any specific response from the shore. According to historian Mary Barbier, the adverse conditions and complexity of the operations contributed to the limited enemy response.[11]
 From intelligence intercepts it appears that German forces in the Pas de Calais region reported an invasion fleet. In addition, there are reports of the decoys being fired on by shore batteries in that area. In an 11 June report on the operations, Lieutenant Commander Ian Cox (who was in charge of deception units) indicated that German forces had been convinced by the fake radio traffic.[7] Intercepted dispatches from Hiroshi Ōshima, the Japanese ambassador to Germany, made reference to the naval deceptions. An 8 June dispatch referred to the Calais region and stated "an enemy squadron that had been operating there has now withdrawn".[3]
 Although disappointed not to have seen any action during the night of D-Day, and still unsure of their actual impact, the bomber crews felt proud of the operations. Squadron Leader Les Munro of No. 617 Squadron wrote, "I have always considered the operation in one sense to be the most important the squadron carried out in my time – not because [of] bad weather, nor because of any threat of enemy action and not measured by any visible results, but because of the very exacting requirements to which we had to fly and navigate".[13]
 No. 617 "Dam Busters" Squadron No. 218 "Gold Coast" Squadron Royal Naval Reserve v t e Atlantic Wall Bodyguard
Fortitude
Zeppelin
Titanic
Taxable, Glimmer & Big Drum Fortitude Zeppelin Titanic Taxable, Glimmer & Big Drum Combined Bomber Offensive Pointblank Transport Plan Postage Able Tarbrush Tiger Fabius Tonga
Deadstick
Merville Battery Deadstick Merville Battery Mallard Albany Boston Chicago Detroit Elmira Omaha Utah Pointe du Hoc Gambit Sword Juno Gold Port-en-Bessin Mulberry
Pluto Mulberry Pluto Brécourt Manor Graignes Saint-Lô Carentan
Hill 30 Hill 30 Cherbourg
Naval Naval Caen Bréville Perch
Villers-Bocage
Le Mesnil-Patry Villers-Bocage Le Mesnil-Patry Douvres Martlet Epsom Windsor Charnwood Jupiter 2nd Odon Atlantic Goodwood Verrières Ridge Cobra Spring Bluecoat Totalize Lüttich Tractable Hill 262 Chambois Falaise Brest Mantes-Gassicourt Paris La Rochelle Ushant La Caine Cherbourg Pierres Noires Audierne Bay Dingson Samwest Titanic Cooney Bulbasket Houndsworth Loyton Jedburgh Dragoon Wallace & Hardy Cemeteries 1 Background 2 Glimmer and Taxable 3 Big Drum 4 Impact 5 References

5.1 Bibliography

 5.1 Bibliography 6 Further reading ^ Latimer (2001), pp. 218–232
 ^ Latimer (2001), pp. 232–234
 ^ a b c Holt (2004), pp. 578–579
 ^ a b c d Barbier (2007), pp. 70–71
 ^ a b West (2010), p. 277
 ^ a b c Bateman (2009), p. 68
 ^ a b c d Barbier (2007), pp. 108–109
 ^ Levine (2011), p. 269
 ^ Brickhill (1977), pp. 207–208
 ^ Smith, Stephen (2015). From St Vith to Victory: 218 (Gold Coast) Squadron and the Campaign Against Nazi Germany. Pen and Sword. ISBN 9781473835054..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c Barbier (2007), pp. 110–111
 ^ Barbier (2007), pp. 111–112
 ^ Wilson (2008), p. 362
 Barbier, Mary (2007). D-Day Deception: Operation Fortitude and the Normandy Invasion. Westport: Greenwood Publishing Group. ISBN 0-275-99479-1. Bateman, Alex (2009). No. 617 'Dambusters' Sqn. Oxford: Osprey Publishing. ISBN 1-84603-429-9. Brickhill, Paul (1977). The Dam Busters. London: Evans Bros. ISBN 0-237-44886-6. Holt, Thaddeus (2004). The Deceivers: Allied Military Deception in the Second World War. New York: Scribner. ISBN 0-7432-5042-7. Latimer, Jon (2001). Deception in War. New York: Overlook Press. ISBN 978-1-58567-381-0. Levine, Joshua (2011). Operation Fortitude: The True Story of the Key Spy Operation of WWII That Saved D-Day. London: HarperCollins UK. ISBN 0-00-741324-6. Wilson, Kevin (2008). Men of Air: The Doomed Youth of Bomber Command. London: Hachette UK. ISBN 0-297-85704-5. West, Nigel (2010). Historical Dictionary of Naval Intelligence. Scarecrow Press. ISBN 0-8108-7377-X. Beevor, Antony (2009). The Battle for Normandy. London: Penguin UK. ISBN 0-14-195926-6. v t e Dudley Clarke Victor Jones John Bevan Dennis Wheatley Ronald Wingate Noel Wild Roger Fleetwood-Hesketh List of Ops (B) staff Peter Fleming Soviet military deception Geoffrey Barkas Tony Ayrton Hugh Cott Peter Proud Steven Sykes Louis Dalton Porter Ellsworth Kelly David Slepian Bill Blass Art Kane Ernest Townsend Jasper Maskelyne more David Strangeways Paradummy Starfish site Johnny Jebsen (Artist) Juan Pujol García (Garbo) Roman Czerniawski (Brutus) Roger Grosjean (Fido) Günther Schütz (Rainbow) Arthur Owens (Snow) Gösta Caroli (Summer) Wulf Schmidt (Tate) Nathalie Sergueiew (Treasure) Dušan Popov (Tricycle) Werner von Janowski (Watchdog) Eddie Chapman (Zig-Zag) Josef Jakobs Mutt and Jeff First United States Army Group Fourth British Army Twelfth British Army Fourteenth United States Army British XIV British XVI US XXXIII US XXXV Airborne US XXXVII US 6th Airborne US 9th Airborne US 11th Infantry US 17th Infantry US 21st Airborne US 25th Armored US 48th Infantry US 55th Infantry British 58th Infantry US 59th Infantry 1st SAS Brigade Copperhead D-Day naval deceptions Ferdinand Fortitude Graffham Ironside Titanic Quicksilver Zeppelin Accumulator Barclay Bertram Boardman Cascade Chettyford Cockade Forfar Hardboiled Mincemeat Pastel Scherhorn Span Bodyguard of Lies British Intelligence in the Second World War (Vol. 5) The Deceivers: Allied Military Deception in the Second World War Aerial operations and battles of World War II Naval operations and battles World War II deception operations Operation Bodyguard Operation Overlord Featured articles Use British English from May 2014 Pages using deprecated image syntax Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version العربية Español Français Slovenčina ไทย Українська  This page was last edited on 8 September 2019, at 05:49 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Prelude Initial Airborne Assault Normandy landings British logistics Initial ground campaign Breakout Air and Sea Operations Supporting operations Aftermath Taxable Glimmer Big Drum ^ ^ a b c a b c d a b a b c a b c d ^ ^ ^ a b c ^ ^ Part of Operation Bodyguard A Harbour Defence Motor Launch, similar to those used during the operations Tactical deception English Channel 1944 London Controlling Section, Ops (B) Cap d'Antifer, Pas-de-Calais, Normandy 5–6 June 1944 
No. 617 "Dam Busters" Squadron
No. 218 "Gold Coast" Squadron
Royal Naval Reserve
 Limited success in deceiving the Axis as to Allied intentions Prelude
Atlantic Wall
Bodyguard
Fortitude
Zeppelin
Titanic
Taxable, Glimmer & Big Drum
Combined Bomber Offensive
Pointblank
Transport Plan
Postage Able
Tarbrush
Tiger
Fabius
Initial Airborne AssaultBritish Sector

Tonga
Deadstick
Merville Battery
Mallard
American Sector

Albany
Boston
Chicago
Detroit
Elmira
Normandy landingsAmerican Sector

Omaha
Utah
Pointe du Hoc
Anglo-Canadian Sector

Gambit
Sword
Juno
Gold
Port-en-Bessin
British logistics

Mulberry
Pluto
Initial ground campaignAmerican Sector

Brécourt Manor
Graignes
Saint-Lô
Carentan
Hill 30
Cherbourg
Naval

Anglo-Canadian Sector

Caen
Bréville
Perch
Villers-Bocage
Le Mesnil-Patry
Douvres
Martlet
Epsom
Windsor
Charnwood
Jupiter
2nd Odon
Atlantic
Goodwood
Verrières Ridge
Breakout

Cobra
Spring
Bluecoat
Totalize
Lüttich
Tractable
Hill 262
Chambois
Falaise
Brest
Mantes-Gassicourt
Paris
La Rochelle
Air and Sea Operations

Ushant
La Caine
Cherbourg
Pierres Noires
Audierne Bay
Supporting operations

Dingson
Samwest
Titanic
Cooney
Bulbasket
Houndsworth
Loyton
Jedburgh
Dragoon
Wallace & Hardy

Aftermath

Cemeteries 'A' Force
Dudley Clarke
Victor Jones
London Controlling Section
John Bevan
Dennis Wheatley
Ronald Wingate
Ops (B)
Noel Wild
Roger Fleetwood-Hesketh
List of Ops (B) staff
D Division
Peter Fleming

Soviet military deception
 
Dudley Clarke
Victor Jones
 
John Bevan
Dennis Wheatley
Ronald Wingate
 
Noel Wild
Roger Fleetwood-Hesketh
List of Ops (B) staff
 
Peter Fleming
 
Soviet military deception
 Middle East Cmd Camouflage Directorate
Geoffrey Barkas
Tony Ayrton
Hugh Cott
Peter Proud
Steven Sykes
Ghost Army
Louis Dalton Porter
Ellsworth Kelly
David Slepian
Bill Blass
Art Kane
Other
Ernest Townsend
Jasper Maskelyne
more
 
Geoffrey Barkas
Tony Ayrton
Hugh Cott
Peter Proud
Steven Sykes
 
Louis Dalton Porter
Ellsworth Kelly
David Slepian
Bill Blass
Art Kane
 
Ernest Townsend
Jasper Maskelyne
more
 R Force
David Strangeways
OtherBeach Jumpers 
David Strangeways
 Beach Jumpers 
Paradummy
Starfish site
 Twenty CommitteeJohn Cecil MastermanDouble agents
Johnny Jebsen (Artist)
Juan Pujol García (Garbo)
Roman Czerniawski (Brutus)
Roger Grosjean (Fido)
Günther Schütz (Rainbow)
Arthur Owens (Snow)
Gösta Caroli (Summer)
Wulf Schmidt (Tate)
Nathalie Sergueiew (Treasure)
Dušan Popov (Tricycle)
Werner von Janowski (Watchdog)
Eddie Chapman (Zig-Zag)
Josef Jakobs
Mutt and Jeff
 John Cecil Masterman 
Johnny Jebsen (Artist)
Juan Pujol García (Garbo)
Roman Czerniawski (Brutus)
Roger Grosjean (Fido)
Günther Schütz (Rainbow)
Arthur Owens (Snow)
Gösta Caroli (Summer)
Wulf Schmidt (Tate)
Nathalie Sergueiew (Treasure)
Dušan Popov (Tricycle)
Werner von Janowski (Watchdog)
Eddie Chapman (Zig-Zag)
Josef Jakobs
Mutt and Jeff
 Field armies
First United States Army Group
Fourth British Army
Twelfth British Army
Fourteenth United States Army
Corps
British XIV
British XVI
US XXXIII
US XXXV Airborne
US XXXVII
Divisions
US 6th Airborne
US 9th Airborne
US 11th Infantry
US 17th Infantry
US 21st Airborne
US 25th Armored
US 48th Infantry
US 55th Infantry
British 58th Infantry
US 59th Infantry
Brigades
1st SAS Brigade
 
First United States Army Group
Fourth British Army
Twelfth British Army
Fourteenth United States Army
 
British XIV
British XVI
US XXXIII
US XXXV Airborne
US XXXVII
 
US 6th Airborne
US 9th Airborne
US 11th Infantry
US 17th Infantry
US 21st Airborne
US 25th Armored
US 48th Infantry
US 55th Infantry
British 58th Infantry
US 59th Infantry
 
1st SAS Brigade
 Bodyguard
Copperhead
D-Day naval deceptions
Ferdinand
Fortitude
Graffham
Ironside
Titanic
Quicksilver
Zeppelin
Other
Accumulator
Barclay
Bertram
Boardman
Cascade
Chettyford
Cockade
Forfar
Hardboiled
Mincemeat
Pastel
Scherhorn
Span
 
Copperhead
D-Day naval deceptions
Ferdinand
Fortitude
Graffham
Ironside
Titanic
Quicksilver
Zeppelin
 
Accumulator
Barclay
Bertram
Boardman
Cascade
Chettyford
Cockade
Forfar
Hardboiled
Mincemeat
Pastel
Scherhorn
Span
 
Bodyguard of Lies
British Intelligence in the Second World War (Vol. 5)
The Deceivers: Allied Military Deception in the Second World War
 