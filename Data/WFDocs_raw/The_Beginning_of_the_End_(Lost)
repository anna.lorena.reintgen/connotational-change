Dominic Monaghan as Charlie PaceMira Furlan as Danielle RousseauSam Anderson as Bernard NadlerL. Scott Caldwell as Rose NadlerTania Raymonde as AlexBlake Bashoff as Karl MartinMarsha Thomason as Naomi DorritMichael Cudlitz as "Big Mike" WaltonLance Reddick as Matthew AbaddonJohn Terry as Christian ShephardFisher Stevens as MinkowskiBilly Ray Gallion as Randy NationsGrisel Toledo as Susie LazenbySteven Neumeier as Lewis
 "The Beginning of the End" is the fourth season premiere, and 73rd episode overall, of the American Broadcasting Company's television drama series Lost. It was aired on ABC in the United States and CTV in Canada on January 31, 2008.[2]  Co-creator/executive producer Damon Lindelof and executive producer Carlton Cuse wrote the premiere in late July 2007,[3] with most of the episode directed on location in Oahu, Hawaii, in August and September by executive producer Jack Bender.[4]  With this premiere, Jeff Pinkner no longer serves as an executive producer and staff writer.[5]  The episode was watched by 18 million Americans, bringing in the best ratings for Lost in 17 episodes.[6]  According to Metacritic, "The Beginning of the End" garnered "universal acclaim".[7]
 The narrative takes place over 90 days after the crash of Oceanic Flight 815, on December 23, 2004. The stranded crash survivors make contact with associates of Naomi Dorrit (played by Marsha Thomason) on a nearby freighter, but the survivors divide when they hear that those on the freighter may not be coming to rescue the survivors. Flashforwards show the post-island lives of Hugo "Hurley" Reyes (Jorge Garcia) and Jack Shephard (Matthew Fox). They are lying to the public about their time on the island. In flashforwards, Hurley has visions of his deceased friend Charlie Pace (Dominic Monaghan); in the present, Hurley grieves over Charlie's death on the island.  Daniel Faraday (Jeremy Davies) makes his first appearance in "The Beginning of the End".[4]
 After being knifed in the back by John Locke (Terry O'Quinn) in the third-season finale, Naomi uses her satellite phone to call George Minkowski (Fisher Stevens) on the freighter.  Before she dies, she tells him that her injury was an accident and to give her love to her sister.  Meanwhile, Hurley finds Jacob's cabin.  He looks through the window and sees an unidentified man in a rocking chair, before someone steps up to the glass, only the left eye visible.  Hurley runs away, but finds the cabin again—in a different location.  He squeezes his eyes shut and when he opens them, the building is gone and Locke appears.[8]
 Desmond Hume (Henry Ian Cusick) returns from the Looking Glass, bearing Charlie's final message that the freighter offshore is not owned by Penny Widmore (Sonya Walger).  The survivors reunite at 815's cockpit.  Jack knocks Locke to the ground, takes his gun and pulls the trigger, but finds that the gun is not loaded because Locke had no intention of killing Jack earlier that day.  Locke tells the castaways that they are in great danger and leaves for the Barracks with Hurley, James "Sawyer" Ford (Josh Holloway), Claire Littleton (Emilie de Ravin) and her baby Aaron, Danielle Rousseau (Mira Furlan) and her captive Ben Linus (Michael Emerson), Alex (Tania Raymonde) and her boyfriend Karl (Blake Bashoff), Vincent the dog (Pono)[9] and four other survivors.  Soon after, Jack and Kate see a helicopter and meet Daniel.[10]
 Flashforwards show that Hurley is famous as one of the "Oceanic Six" after his escape from the island and is keeping quiet about his time there.  Hurley encounters an apparition of Charlie.  Shocked, he speeds away in his Camaro and is apprehended by Los Angeles police.[8]  Hurley is interrogated by Ana Lucia Cortez's (Michelle Rodriguez) former partner Detective "Big" Mike Walton (Michael Cudlitz) and he lies that he has no knowledge of Ana Lucia.  Hurley, looking at the interrogation room's mirror glass, imagines seeing Charlie swimming in water until he breaks the glass and floods the room.  Hurley willingly returns to the Santa Rosa Mental Health Institution, where he is visited by Matthew Abaddon (Lance Reddick), who claims to be an attorney for Oceanic Airlines.  When Abaddon fails to supply a business card, he asks if they are still alive before stealthily exiting.  An apparition of Charlie appears who tells Hurley that "they" need him.  Finally, Hurley is visited by Jack, who is thinking of growing a beard.  Jack confirms that Hurley will not reveal the Oceanic Six's secrets.  Hurley apologizes for going with Locke and insists that they return to the island, but Jack refuses (which shows that these flashforwards occur before Jack's flashforwards).[8]
 During casting, fake names, occupations and scenes were temporarily assigned to limit the leak of spoilers.[11] Lance Reddick was told that he was auditioning for the part of "Arthur Stevens", a "ruthless corporate recruiter", instead of Matthew Abaddon.  "Matthew" and "Abaddon" were revealed as season 4 clue words in the alternate reality game Find 815.[12]  The writers chose the character's surname after they read the Wikipedia article on Abaddon, which states that it means "place of destruction".[13]  The writer-producers were originally interested in having Reddick play Mr. Eko during the second season, however, he was busy starring on HBO's The Wire.[14] Jeremy Davies was cast as Daniel because he is one of the writer-producers' favorite character actors, and they think that his "transformative quality [and] the tremendous intelligence that seems to emanate from him ... seemed perfect for [the part]",[15] which was originally planned to be a recurring role.[16]  When Davies met costume designer Roland Sanchez, he was wearing a thin black tie.  Sanchez merged this "cool, edgy look" with his idea for the character's clothes: a "nerdy" loosely woven dress shirt from J.Crew.[17]
 Several different titles were proposed for the episode.[3]  The ultimate title is a reference to a line in the previous episode when Ben warns Jack that contacting the freighter "is the beginning of the end".[18]  Filming began on August 17[19] and ended on or just after September 7, 2007.[20]  Garcia felt "a little pressure" because he had the lead role in the episode, but "was really excited, too [because it] was a different direction for a season premiere [that he] felt the fans would probably dig".[21]  In the mental institution, Hurley is seen painting a picture of an Inuit man and an igloo.  This was painted by Garcia.[22]  When the episode was broadcast, Christian appeared in Jacob's cabin; however, the scene was shot with another Hurley inside.  Additionally, when Garcia was filming his interrogation scene in an aquarium, he was unaware that Charlie would be swimming outside and breaking the glass in the finished product.[23]  Charlie's swim was filmed weeks after the rest of the episode had been shot, alongside production of "Meet Kevin Johnson" and the Lost: Missing Pieces mobisodes in late November 2007.  The scene was filmed with stunt double Jake Kilfoyle at the Looking Glass set that was previously used for the third season episodes titled "Greatest Hits" and "Through the Looking Glass".[24]
 Most Lost episodes feature crossovers and "Easter eggs"—intentionally hidden clues and references to the show's mythology[25]—and "The Beginning of the End" is no exception.[26]  Despite being dead, Christian appears for a couple of seconds in Jacob's cabin with no dialogue.[27]  Big Mike, who appears in Ana Lucia's flashbacks in the second-season episode "Collision", returns in Hurley's flashforward.  Randy Nations (Billy Ray Gallion) appears in a few seconds with no dialogue, videotaping Hurley's arrest.[28]  When Hurley hallucinates that Charlie is swimming outside the interrogation room, Charlie has "they need you" written on his hand.  This is what Charlie tells Hurley later in the episode.[29]
 Due to production of the fourth season being put on hold due to the 2007–2008 Writers Guild of America strike,[30] the show runners wanted to hold the eight episodes that had been completed until they were able to make more of the season.[31]  ABC decided against this and announced that "The Beginning of the End" would be aired at the end of January 2008, regardless of when the strike was to end.[32]  This was the first Lost episode to be aired on Thursday at 9:00 pm ET,[33] a competitive and prestigious timeslot[34] normally occupied by Grey's Anatomy;[35] previous episodes had been aired on Wednesdays.[36]  Like the previous Lost season premieres, "The Beginning of the End" was scheduled for an outdoor premiere at Sunset on the Beach in Waikiki, Honolulu,[37] where movies are regularly shown on a 30-foot (9 m) screen free to the public,[38] but it was cancelled due to the writers' strike.[39]  The original television broadcast of the episode was immediately preceded by a clip-show titled "Lost: Past, Present & Future".[40]
 Don Williams of BuddyTV dubbed "The Beginning of the End" "the most anticipated season premiere of the year".[41]  It was watched by approximately 16.137 million American viewers live or within six hours with a 6.7/17 in the key adults 18–49 demographic,[42] bringing in the best Nielsen ratings for Lost in seventeen episodes and ranking Lost eighth in the weekly charts.[43]  The episode was watched by a total of 17.766 million viewers live or recorded and watched within seven days of broadcast and this number went toward the year-end average.[44]  In Canada, "The Beginning of the End" was seen by 1.855 million viewers, making Lost the sixth most watched program of the week.[45]  It brought in an audience almost double the size of that of the previous episode and greater than any third-season episode,[46] with the exception of the season premiere.[47]  The fourth-season premiere was successful in the United Kingdom with 1.1 million viewers.[48]  In Australia, Lost was the fifteenth most watched show of the night with 912 000 viewers, which was deemed disappointing by David Dale of The Sun-Herald.[49]
 American critics were sent screener DVDs of "The Beginning of the End" and "Confirmed Dead" on January 28, 2008. Metacritic gave the episode a Metascore—a weighted average based on the impressions of a select twelve critical reviews—of 87.[7]  Robert Bianco of USA Today wrote that "returning with a heart-stopping, perfectly pitched episode ... Lost is an oasis in a strike-parched TV desert."[50]  Mary McNamara of the Los Angeles Times described "The Beginning of the End" as an "emotion-churning chemical dump right in the old brain stem—horror, hysteria, regret, adrenaline and what ... will happen next?"[51]  Adam Buckman of the New York Post gave the episode four out of four stars.[52]  Maureen Ryan of the Chicago Tribune "blissfully enjoyed every minute" and noted that "there aren't any faults".[53]  Diane Werts of Newsday raved the episode as "superb" and "insanely entertain[ing]" and concluded her review with "Lost seems to have found itself".[54]  Tim Goodman of the San Francisco Chronicle declared that it and "Confirmed Dead" "are roller coasters of fast action and revelation [that are] good to see".[55]  Matthew Gilbert of The Boston Globe pointed out that "Lost can still make the pulse race and the brain tingle ... [and] remains TV's most gripping serial".[56]  Alan Sepinwall of The Star-Ledger was unsure "if Lost is ever going to give satisfying answers to its many, many remaining mysteries ... but when it's as scary and hilarious and moving and exciting as these two episodes, I'm okay with that."[57]  In less positive reviews, Rodney Ho of The Atlanta Journal-Constitution called it "a satisfactory return episode with a fair share of drama and pathos ... [that] provides just enough revelations to keep fans hungry for more"[58] and David Hinckley of the Daily News rated the episode with three stars out of a possible five.[59]
 Brian Lowry of Variety said that "Lost's return goes down like a welcome tonic as scripted TV fades to black ... providing an unusually generous array of juicy moments for the large (and, at times, neglected) cast."[60]  Mark Medley of the National Post called it "a brilliant season premiere" with multiple "jaw-dropping moments".[61]  Jeff Jensen of Entertainment Weekly felt that the premiere was mind-blowing[62] and featured good acting by Garcia.[21]  Frazier Moore of the Associated Press wrote that "Lost is further upping the ante, and heightening the pressure on us as the show's vast mythology continues to metastasize."[63] Kristin Dos Santos of E! called it "so well written, produced, acted and directed it felt like a movie".[64] Michael Ausiello of TV Guide described it as "easily one of the best hours of TV so far this season."[65]  Bruce Fretts of TV Guide responded well to Reddick's performance.[66]  Chris Carabott of IGN gave the episode 9.1/10, stating that it was "a great start to what promises to be an exciting ... season 4.  The momentum and pacing is on par with last season's finale".[67]  LTG of Television Without Pity graded it as an "A–".[68]  Jon Lachonis of UGO gave the episode an "A+", calling it "a crushingly emotional, action packed introduction ... [which proves] that ... Lost's groundbreaking protean form still has plenty of blinding ways to dazzle and entertain in a way that is nonetheless unique unto itself."[69]  Oscar Dahl of BuddyTV wrote that "the episode was pretty much a masterpiece".[70]  Daniel of TMZ graded it as an "A", saying that it was perfect and set up the rest of the season well.[71]
 
 1 Plot 2 Production 3 Reception 4 References 5 External links ^ "Lost - Netflix". Netflix. Retrieved 24 November 2017..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}[permanent dead link]
 ^ ABC Medianet, (January 18, 2008) "Primetime Program Schedule Grid Archived 2012-02-13 at the Wayback Machine".  Retrieved on January 19, 2008.
 ^ a b Lindelof, Damon and Cuse, Carlton, (July 26, 2007) "Lost Season 4 Panel Archived 2012-02-10 at the Wayback Machine", Comic Con International.  Retrieved on August 18, 2007.
 ^ a b ABC Medianet, (January 14, 2008) "Feeling that Their Rescue is Imminent, the Survivors Ponder Charlie's Final Message that the People Claiming to Liberate Them Aren't Who They Seem to Be Archived 2012-04-21 at the Wayback Machine".  Retrieved on January 14, 2008.
 ^ Lost: Missing Pieces Credits, ABC.  Retrieved on November 12, 2007.
 ^ Kissell, Rick, (February 1, 2008) "Auds Find Lost on Thursday", Variety.  Retrieved on February 1, 2008.
 ^ a b Metacritic, (January 31, 2008) "Lost (ABC): Season 4".  Retrieved on February 16, 2008.
 ^ a b c Lindelof, Damon (writer), Cuse, Carlton (writer) and Bender, Jack (director), "The Beginning of the End".  Lost, ABC.  Episode 1, season 4.  Aired on January 31, 2008.
 ^ Nichols, Katherine, (March 25, 2007) "Chewing the Scenery", Honolulu Star Bulletin.  Retrieved on March 30, 2007.
 ^ Goddard, Drew (writer), Vaughan, Brian K. (writer) and Williams, Stephen (director), "Confirmed Dead".  Lost, ABC.  Episode 2, season 4.  Aired on February 7, 2008.
 ^ Littleton, Cynthia, (October 12, 2007) "Lost: The Weight of the Wait", Variety. Retrieved on October 13, 2007.
 ^ McGee, Ryan, (January 21, 2008) "Just Another Manic Monday", Zap2it.  Retrieved on February 17, 2008.
 ^ Jensen, Jeff "Doc", (February 21, 2008) "Lost: Mind-Blowing Scoop From Its Producers", Entertainment Weekly.  Retrieved on February 21, 2008.
 ^ Jensen, Jeff, (August 29, 2007) "Lost: Five Fresh Faces", Entertainment Weekly. Retrieved on August 30, 2007.
 ^ Jensen, Jeff, (August 29, 2007) "Lost Producers on Their Five New Actors", Entertainment Weekly. Retrieved on August 30, 2007.
 ^ ABC Medianet, (January 18, 2008) "Four Strangers Arrive on the Island, Leading the Survivors to Question the Intentions of Their Supposed Rescuers Archived 2013-12-12 at the Wayback Machine".  Retrieved on February 17, 2008.
 ^ Sanchez, Roland, (February 8, 2008) "The Official Lost Video Podcast Archived 2012-02-10 at the Wayback Machine", ABC.  Retrieved on March 17, 2008.
 ^ Lindelof, Damon (writer), Cuse, Carlton (writer) and Bender, Jack (director). "Through the Looking Glass". Lost, ABC.  Episode 23, season 3.  Aired on May 23, 2007.
 ^ Garcia, Jorge, (August 14, 2007) "Working Friday", Dispatches from the Island.  Retrieved on August 18, 2007.
 ^ Perez, Mario, (September 7, 2007) "Terry O'Quinn, Jorge Garcia, Josh Holloway, Naveen Andrews, Henry Ian Cusick and Sam Anderson in 'The Beginning of the End' Archived 2009-01-07 at the Wayback Machine", ABC Medianet.  Retrieved on December 21, 2007.
 ^ a b Jensen, Jeff "Doc", (February 6, 2008) "Hugs for Hurley", Entertainment Weekly.  Retrieved on February 17, 2008.
 ^ Garcia, Jorge, (February 2, 2008) "Eskimo Drawing", The Fuselage.  Retrieved on February 9, 2008.
 ^ a b Dos Santos, Kristin, (February 1, 2008) "Who Was in the Chair? What's Next? Lost's Jorge Garcia Spills Some Secrets Archived 2011-06-29 at the Wayback Machine", E!.  Retrieved on February 1, 2008.
 ^ Higgins, Jean, (February 1, 2008) "Official Lost Video Podcast", ABC.  Retrieved on February 2, 2008.
 ^ Veitch, Kristin, (November 18, 2005) "Easter Egg or Fake Egg?", E!.  Retrieved on February 16, 2008.
 ^ Kubicek, John, (February 1, 2008) "Lost Easter Eggs: Episode 4.1 'The Beginning of the End' Archived 2012-10-22 at the Wayback Machine", BuddyTV.  Retrieved on February 16, 2008.
 ^ Kubicek, John, (February 1, 2008) "Lost Easter Eggs: Episode 4.1 'The Beginning of the End' – #2 Jacob's Cabin Archived 2012-10-22 at the Wayback Machine", BuddyTV.  Retrieved on February 16, 2008.
 ^ Kubicek, John, (February 1, 2008) "Lost Easter Eggs: Episode 4.1 'The Beginning of the End' – #3 Randy Nations Archived 2012-10-22 at the Wayback Machine", BuddyTV.  Retrieved on February 16, 2008.
 ^ ABC, (February 1, 2008) "'The Beginning of the End': Season 4, Episode 401 Recap Archived 2010-07-23 at the Wayback Machine".  Retrieved on February 17, 2008.
 ^ Lindelof, Damon and Cuse, Carlton, (November 5, 2007) "Lost Writers: "Like Putting Down a Harry Potter Book in the Middle" Archived 2011-05-20 at the Wayback Machine", Variety.  Retrieved on November 8, 2007.
 ^ Goldman, Eric, (November 7, 2007) "Writers Strike: Should Lost Air This Season?", IGN.  Retrieved on November 8, 2007.
 ^ Grossman, Ben, (November 7, 2007) "Strike Coverage: ABC to Air Partial Season of Lost", Broadcasting & Cable.  Retrieved on November 8, 2007.
 ^ ABC Medianet, (December 14, 2007) "ABC Unveils Midseason Primetime Schedule Archived 2012-04-21 at the Wayback Machine", ABC Medianet.  Retrieved on December 14, 2007.
 ^ de Moraes, Lisa, (December 15, 2007) "Strike That!  Sans Writers, ABC Reshuffles Time Slots for Lost, Housewives", The Washington Post.  Retrieved on September 27, 2008.
 ^ Martell, Erin, (February 13, 2008) "Second Batch of Lost Episodes Will Follow Grey's Anatomy", TV Squad.  Retrieved on February 25, 2008.
 ^ Dos Santos, Kristin, (December 14, 2007) "Lost: It's Baaack!", E!.  Retrieved on December 14, 2007.
 ^ Lachonis, Jon "DocArzt", (October 29, 2007) "Lost Public Premiere Planned, Time Slot Still Up in the Air Archived 2012-10-22 at the Wayback Machine", BuddyTV.  Retrieved on October 28, 2007.
 ^ "Waikiki Beach Movie Schedule and Events", Sunset on the Beach.  Retrieved on October 29, 2007.
 ^ Kunz, Caryn, (January 10, 2008) "Lost Premiere in Waikiki Archived 2008-01-21 at the Wayback Machine", The Honolulu Advertiser.  Retrieved on January 19, 2008.
 ^ ABC Medianet, (January 14, 2008) "A Look Over the Past Three Seasons will Attempt to Shed Some Light on Where the Survivors of Oceanic Flight 815 are and if Rescue Truly is at Hand Archived 2012-03-29 at the Wayback Machine".  Retrieved on January 20, 2008.
 ^ Williams, Don, (January 31, 2008) "Lost: Episode 4.1 'The Beginning of the End' Live Thoughts Archived 2013-10-02 at the Wayback Machine", BuddyTV.  Retrieved on January 31, 2008.
 ^ ABC Medianet, (February 5, 2008) "Weekly Primetime Ratings Archived 2012-04-21 at the Wayback Machine".  Retrieved on February 10, 2008.
 ^ ABC Medianet, (February 5, 2008) "Weekly Program Rankings Archived 2013-12-28 at the Wayback Machine".  Retrieved on February 5, 2008.
 ^ Gorman, Bill, (February 24, 2008) "Top Time-Shifted Broadcast Shows, Jan 28–Feb 3 Archived 2009-01-21 at the Wayback Machine", TV by the Numbers.  Retrieved on March 26, 2008.
 ^ BBM Canada, (February 5, 2008) "Top Programs – Total Canada (English) Archived 2008-02-16 at the Wayback Machine".  Retrieved on February 10, 2008.
 ^ BBM Canada, (May 27, 2007) "Top Programs – Total Canada (English) Archived 2008-04-14 at the Wayback Machine".  Retrieved on January 31, 2008.
 ^ BBM Canada, (October 10, 2006) "Top Programs – Total Canada (English) Archived 2008-04-14 at the Wayback Machine".  Retrieved on February 10, 2008.
 ^ Holmwood, Leigh, (February 3, 2008) "Warm Welcome for Lost's Return", The Guardian.  Retrieved on February 17, 2008.
 ^ Dale, David, (February 9, 2008) "The Ratings Race: The Silly Season Ends Here", The Sun-Herald.  Retrieved on February 10, 2008.
 ^ Bianco, Robert, (January 30, 2008) "Lost Rescues a TV Season That's Adrift", USA Today.  Retrieved on February 2, 2008.
 ^ McNamara, Mary, (January 31, 2008) "Lost in the Moment", Los Angeles Times.  Retrieved on February 2, 2008.
 ^ Buckman, Adam, (January 31, 2008) "Island Fever: Thank God, At Least Lost is Back Archived 2011-06-06 at the Wayback Machine", New York Post.  Retrieved on February 2, 2008.
 ^ Ryan, Maureen, (January 29, 2008) "Lost's Fab Start to Season 4, and a Chat with Co-Creator Damon Lindelof", Chicago Tribune.  Retrieved on January 30, 2008.
 ^ Werts, Diane, (January 30, 2008) "Review: Lost Finds Itself Again in New Season Archived 2007-07-03 at the Wayback Machine", Newsday.  Retrieved on February 12, 2008.
 ^ Goodman, Tim, (January 30, 2008) "Want to Get Lost?  There's Still Time as Season Starts", San Francisco Chronicle.  Retrieved on February 2, 2008.
 ^ Gilbert, Matthew, (January 31, 2008) "Lost Finds New Ways to Intrigue", The Boston Globe.  Retrieved on February 17, 2008.
 ^ Sepinwall, Alan, (January 31, 2008) "Lost Season 4 Review", The Star-Ledger.  Retrieved on February 12, 2008.
 ^ Ho, Rodney, (January 31, 2008) "Lost is Back! Archived 2008-02-02 at the Wayback Machine", The Atlanta Journal-Constitution.  Retrieved on February 15, 2008.
 ^ Hinckley, David, (January 30, 2008) "Lost Still Heading in the Right Direction", Daily News.  Retrieved on February 17, 2008.
 ^ Lowry, Brian, (January 29, 2008) "Lost Review", Variety.  Retrieved on February 17, 2008.
 ^ Medley, Mark, (February 1, 2008) "The Oceanic Six: Lost Season 4, Episode 1 Recap[permanent dead link]", National Post.  Retrieved on February 17, 2008.
 ^ Jensen, Jeff "Doc", (February 1, 2008) "Lost: Back and Forth", Entertainment Weekly.  Retrieved on February 17, 2008.
 ^ Moore, Frazier, (January 30, 2008) "Lost is Found as Fourth Season Puts Islanders in New Jeopardy—with New Mysteries", Associated Press.  Retrieved on February 12, 2008.
 ^ Dos Santos, Kristin and Godwin, Jennifer, (January 31, 2008) "Lost Redux: Oh My God, I Love This Show So Much", E!.  Retrieved on February 1, 2008.
 ^ Ausiello, Michael, (February 6, 2008) "Heroes, Moonlight, Lauren Graham, Scrubs and More! Archived 2008-07-24 at the Wayback Machine", TV Guide.  Retrieved on April 13, 2008.
 ^ Fretts, Bruce, (February 1, 2008) "Cheers: Lost Finds Lance Reddick Archived 2008-02-06 at the Wayback Machine", TV Guide.  Retrieved on February 17, 2008.
 ^ Carabott, Chris, (January 30, 2008) "Lost: 'The Beginning of the End' Advance Review", IGN.  Retrieved on February 1, 2008.
 ^ LTG, (February 5, 2008) "Can't Keep a Dead Hobbit Down", Television Without Pity.  Retrieved on February 25, 2008.
 ^ Lachonis, Jon "DocArzt", Lost Season 4 Premiere: 'Beginning of the End' Reviewed Archived 2008-01-23 at the Wayback Machine, UGO.  Retrieved on January 22, 2008.
 ^ Dahl, Oscar, (February 1, 2008) "Lost: The Oceanic Six Archived 2012-01-11 at the Wayback Machine", BuddyTV.  Retrieved on February 1, 2008.
 ^ Daniel, (February 1, 2008) "Lost Diary: 'The Beginning of the End', TMZ.  Retrieved on March 15, 2008.
 "The Beginning of the End" at ABC "The Beginning of the End" on IMDb "The Beginning of the End" at TV.com v t e "Pilot" "Tabula Rasa" "Walkabout" "White Rabbit" "House of the Rising Sun" "The Moth" "Confidence Man" "Solitary" "Raised by Another" "All the Best Cowboys Have Daddy Issues" "Whatever the Case May Be" "Hearts and Minds" "Special" "Homecoming" "Outlaws" "...In Translation" "Numbers" "Deus Ex Machina" "Do No Harm" "Lost: The Journey" (clip show) "The Greater Good" "Born to Run" "Exodus" "Man of Science, Man of Faith" "Adrift" "Orientation" "Everybody Hates Hugo" "...And Found" "Abandoned" "The Other 48 Days" "Collision" "What Kate Did" "The 23rd Psalm" "The Hunting Party" "Fire + Water" "The Long Con" "One of Them" "Maternity Leave" "The Whole Truth" "Lockdown" "Dave" "S.O.S." "Two for the Road" "?" "Three Minutes" "Live Together, Die Alone" "A Tale of Two Cities" "The Glass Ballerina" "Further Instructions" "Every Man for Himself" "The Cost of Living" "I Do" "Not in Portland" "Flashes Before Your Eyes" "Stranger in a Strange Land" "Tricia Tanaka Is Dead" "Enter 77" "Par Avion" "The Man from Tallahassee" "Exposé" "Left Behind" "One of Us" "Catch-22" "D.O.C." "The Brig" "The Man Behind the Curtain" "Greatest Hits" "Through the Looking Glass" Lost: Missing Pieces "The Beginning of the End" "Confirmed Dead" "The Economist" "Eggtown" "The Constant" "The Other Woman" "Ji Yeon" "Meet Kevin Johnson" "The Shape of Things to Come" "Something Nice Back Home" "Cabin Fever" "There's No Place Like Home" "Because You Left" "The Lie" "Jughead" "The Little Prince" "This Place Is Death" "316" "The Life and Death of Jeremy Bentham" "LaFleur" "Namaste" "He's Our You" "Whatever Happened, Happened" "Dead Is Dead" "Some Like It Hoth" "The Variable" "Follow the Leader" "The Incident" "LA X" "What Kate Does" "The Substitute" "Lighthouse" "Sundown" "Dr. Linus" "Recon" "Ab Aeterno" "The Package" "Happily Ever After" "Everybody Loves Hugo" "The Last Recruit" "The Candidate" "Across the Sea" "What They Died For" "The End" "The New Man in Charge" Lost (season 4) episodes 2008 American television episodes All articles with dead external links Articles with dead external links from January 2018 Articles with permanently dead external links Webarchive template wayback links Articles with dead external links from April 2017 Articles with short description Television episode articles with short description for single episodes Television episode articles with short description and disambiguated page names Pages using deprecated image syntax Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Dansk Español Hrvatski Magyar Polski Português Română Русский Türkçe 中文  This page was last edited on 23 September 2019, at 16:19 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  The Beginning of the End Previous Next The Beginning of the End ^ ^ a b a b ^ ^ a b a b c ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ The Beginning of the End Lost episode Jack Shephard and Kate Austen waiting for rescuers from the freighter. Season 4Episode 1 Jack Bender Damon LindelofCarlton Cuse 401 January 31, 2008 42 minutes[1] 
Dominic Monaghan as Charlie PaceMira Furlan as Danielle RousseauSam Anderson as Bernard NadlerL. Scott Caldwell as Rose NadlerTania Raymonde as AlexBlake Bashoff as Karl MartinMarsha Thomason as Naomi DorritMichael Cudlitz as "Big Mike" WaltonLance Reddick as Matthew AbaddonJohn Terry as Christian ShephardFisher Stevens as MinkowskiBilly Ray Gallion as Randy NationsGrisel Toledo as Susie LazenbySteven Neumeier as Lewis

 


← Previous"Through the Looking Glass"

Next →"Confirmed Dead"
 ← Previous"Through the Looking Glass"
 Next →"Confirmed Dead"
 Lost (season 4) List of Lost episodes  Wikiquote has quotations related to: The Beginning of the End 
"Pilot"
"Tabula Rasa"
"Walkabout"
"White Rabbit"
"House of the Rising Sun"
"The Moth"
"Confidence Man"
"Solitary"
"Raised by Another"
"All the Best Cowboys Have Daddy Issues"
"Whatever the Case May Be"
"Hearts and Minds"
"Special"
"Homecoming"
"Outlaws"
"...In Translation"
"Numbers"
"Deus Ex Machina"
"Do No Harm"
"Lost: The Journey" (clip show)
"The Greater Good"
"Born to Run"
"Exodus"
 
"Man of Science, Man of Faith"
"Adrift"
"Orientation"
"Everybody Hates Hugo"
"...And Found"
"Abandoned"
"The Other 48 Days"
"Collision"
"What Kate Did"
"The 23rd Psalm"
"The Hunting Party"
"Fire + Water"
"The Long Con"
"One of Them"
"Maternity Leave"
"The Whole Truth"
"Lockdown"
"Dave"
"S.O.S."
"Two for the Road"
"?"
"Three Minutes"
"Live Together, Die Alone"
 
"A Tale of Two Cities"
"The Glass Ballerina"
"Further Instructions"
"Every Man for Himself"
"The Cost of Living"
"I Do"
"Not in Portland"
"Flashes Before Your Eyes"
"Stranger in a Strange Land"
"Tricia Tanaka Is Dead"
"Enter 77"
"Par Avion"
"The Man from Tallahassee"
"Exposé"
"Left Behind"
"One of Us"
"Catch-22"
"D.O.C."
"The Brig"
"The Man Behind the Curtain"
"Greatest Hits"
"Through the Looking Glass"
 
Lost: Missing Pieces
 
"The Beginning of the End"
"Confirmed Dead"
"The Economist"
"Eggtown"
"The Constant"
"The Other Woman"
"Ji Yeon"
"Meet Kevin Johnson"
"The Shape of Things to Come"
"Something Nice Back Home"
"Cabin Fever"
"There's No Place Like Home"
 
"Because You Left"
"The Lie"
"Jughead"
"The Little Prince"
"This Place Is Death"
"316"
"The Life and Death of Jeremy Bentham"
"LaFleur"
"Namaste"
"He's Our You"
"Whatever Happened, Happened"
"Dead Is Dead"
"Some Like It Hoth"
"The Variable"
"Follow the Leader"
"The Incident"
 
"LA X"
"What Kate Does"
"The Substitute"
"Lighthouse"
"Sundown"
"Dr. Linus"
"Recon"
"Ab Aeterno"
"The Package"
"Happily Ever After"
"Everybody Loves Hugo"
"The Last Recruit"
"The Candidate"
"Across the Sea"
"What They Died For"
"The End"
 
"The New Man in Charge"
 