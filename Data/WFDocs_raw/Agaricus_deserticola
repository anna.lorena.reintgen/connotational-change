
 Secotium texense Berk. & M.A.Curtis (1873)
Gyrophragmium texense (Berk. & M.A.Curtis) Massee (1891)
Secotium decipiens Peck (1895)
Podaxon strobilaceus Copeland (1904)
Gymnopus texensis (Berk. & M.A.Curtis) Murrill (1916)
Longia texensis (Berk. & M.A.Curtis) Zeller (1943)
Longula texensis (Berk. & M.A.Curtis) Zeller (1945)
Agaricus texensis (Berk. & M.A.Curtis) Geml, Geiser & Royse (2004)
 Agaricus deserticola, commonly known as the gasteroid agaricus, is a species of fungus in the family Agaricaceae. Found only in southwestern and western North America, A. deserticola is adapted for growth in dry or semi-arid habitats. The fruit bodies are secotioid, meaning the spores are not forcibly discharged, and the cap does not fully expand. Unlike other Agaricus species, A. deserticola does not develop true gills, but rather a convoluted and networked system of spore-producing tissue called a gleba. When the partial veil breaks or pulls away from the stem or the cap splits radially, the blackish-brown gleba is exposed, which allows the spores to be dispersed.
 The fruit bodies can reach heights of 18 cm (7.1 in) tall with caps that are up to 7.5 cm (3.0 in) wide. The tough woody stems are 1–2 cm (0.4–0.8 in) wide, thickening towards the base. Fruit bodies grow singly or scattered on the ground in fields, grasslands, or arid ecosystems. Other mushrooms with which A. deserticola might be confused include the desert fungus species Podaxis pistillaris and Montagnea arenaria. The edibility of Agaricus deserticola mushrooms is not known definitively. Formerly named Longula texensis (among several other synonyms), the fungus was transferred to the genus Agaricus in 2004 after molecular analysis showed it to be closely related to species in that genus. In 2010, its specific epithet was changed to deserticola after it was discovered that the name Agaricus texensis was illegitimate, having been previously published for a different species.
 The species was first described scientifically as Secotium texense by Miles Joseph Berkeley and Moses Ashley Curtis in 1873, based on specimens sent to them from western Texas.[3] George Edward Massee transferred it to the genus Gyrophragmium in 1891, because of its resemblance to the species Gyrophragmium delilei, and because he felt that the structure of the volva as well as the internal morphology of the gleba excluded it from Secotium.[4] In 1916, William Murrill listed the species in Gymnopus, but did not explain the reason for the generic transfer.[5] In a 1943 publication, Sanford Zeller compared a number of similar secotioid genera: Galeropsis, Gyrophragmium and Montagnea. He concluded that the species did not fit in the limits set for the genus Gyrophragmium and so created the new genus Longia with Longia texensis as the type species. The generic name was to honor William Henry Long, an American mycologist noted for his work in describing Gasteromycetes. Zeller also mentioned two additional synonyms:[2] Secotium decipiens (Peck, 1895),[6] and Podaxon strobilaceous (Copeland, 1904).[7]
 Two years later in 1945, Zeller pointed out that the use of the name Longia was untenable, as it had already been used for a genus of rusts described by Hans Sydow in 1921,[8] so he proposed the name Longula and introduced the new combination Longula texensis in addition to L. texensis var. major.[9] The species was known by this name for about 60 years, until a 2004 phylogenetic study revealed the taxon's close evolutionary relationship with Agaricus,[10][11] a possibility insinuated by Curtis Gates Lloyd a century before.[12] This resulted in a new name in that genus, but it soon came to light that the name Agaricus texensis had already been used, ironically enough, by Berkeley and Curtis themselves in 1853,[13] for a taxon now treated as a synonym of Flammulina velutipes.[14] Since this made the new Agaricus texensis an unusable homonym, Gabriel Moreno and colleagues published the new name Agaricus deserticola in 2010.[15] The mushroom is commonly known as the gasteroid Agaricus.[16]
 A. lanipes
 A. maskae
 A. subrutilescens
 A. silvaticus var. pallidus
 A. aridicola
 A. deserticola
 The classification of Agaricus deserticola has been under debate since the taxon was first described. It was thought by some mycologists to be a member of the Gasteromycetes, a grouping of fungi in the basidiomycota that do not actively discharge their spores. The Gasteromycetes are now known to be an artificial assemblage of morphologically similar fungi without any unifying evolutionary relationship. When the species was known as a Gyrophragmium, Fischer thought it to be close to Montagnites, a genus he considered a member of the family Agaricaceae.[17] Conrad suggested a relationship with Secotium, which he believed to be close to Agaricus.[18] Curtis Gates Lloyd said of Gyrophragmium: "[it] has no place in the Gasteromycetes. Its relations are more close to the Agarics. It is the connecting link between the two passing on one hand through Secotium to the true Gasteromycetes."[12] Elizabeth Eaton Morse believed that Gyrophragmium and the secotioid genus Endoptychum formed a transition between the Gasteromycetes and the Hymenomycetes (the gilled fungi).[19]
 The species is now thought to have evolved from an Agaricus ancestor, and adapted for survival in dry habitats.[10][11] These adaptations include: a cap that does not expand (thus conserving moisture); dark-colored gills that do not forcibly eject spores (a mechanism known to depend on turgor pressure achievable only in sufficiently hydrated environments); and a partial veil that remains on the fruit body long after it has matured.[20] This form of growth is called secotioid development, and is typical of other desert-dwelling fungi like Battarrea phalloides, Podaxis pistillaris, and Montagnea arenaria. Molecular analysis based on the sequences of the partial large subunit of ribosomal DNA and of the internal transcribed spacers shows that A. deserticola is closely related to but distinct from A. aridicola.[10] A separate analysis showed A. deserticola to be closely related to A. arvensis and A. abruptibulbus.[21]
 The fruit body of Agaricus deserticola can grow up to 5 to 18 cm (2.0 to 7.1 in) in height. Fresh specimens are usually white, but will age to a pale tan; dried fruit bodies are light gray or tan mixed with some yellow.[22] The cap is 4 to 7.5 cm (1.6 to 3.0 in) in diameter, initially conic, later becoming convex to broadly convex as it matures.[23] The cap is composed of three distinct tissue layers: an outer volval layer, a middle cuticular layer (cutis), and an inner (tramal) layer which supports the gleba. The surface of the cap is white with yellow-brown to brown-tipped raised small scales; these scales result from the breakup of the volva and the cutis.[24]
 Initially, the caps are covered by a peridium—an outer covering layer of tissue. After the fruit body matures and begins to dry out, the lower part of the peridium starts to rip, usually starting from small longitudinal slits near where the peridium attaches to the top of the stem. However, the pattern of tearing is variable; in some instances, the slits may appear higher up on the peridium, in others, the peridium rips more irregularly.[12][22] The peridium may also rip in such a way that it appears as if there is a ring at the top of the stem. The torn peridium exposes the internal gleba. The gleba is divided into wavy plates or lamellae, some of which are fused together to form irregular chambers. The gleba is a drab brown to blackish-brown color, and it becomes tough and brittle as it dries out. The flesh is firm when young, white, and will stain light to bright yellow when it is bruised.[16]
 The stem is cylindrical, 4 to 15 cm (1.6 to 5.9 in) long and 1 to 2 cm (0.4 to 0.8 in) thick.  It is shaped like a narrow club, and the base may reach widths up to 4.5 cm (1.8 in). It is typically white, staining yellow to orange-yellow or pink when bruised, and becomes woody with age.[16][24] Mature specimens develop longitudinal grooves in maturity.[25] Numerous white rhizoids are present at the base of the stem; these root-like outgrowths of fungal mycelium help the mushroom attach to its substrate.[24] The apex of the stem extends into the gleba to form a columella that reaches the top of the cap. The internal gills are free from attachment to the stem,[2] but are attached full-length to the inside of the cap.[24] The partial veil is thick, white, and often sloughs off as the cap expands.[23]
 A larger variety of the mushroom has been described by Zeller,[2] A. deserticola var. major (originally Longula texensis var. major), whose range overlaps that of the typical variety. Its caps are scalier than the typical variety, and range from 6 to 12 cm (2.4 to 4.7 in) or more in diameter, with a stem 10 to 25 cm (3.9 to 9.8 in) and up to 4.5 cm (1.8 in) thick.[16][24]
 In deposit, such as with a spore print, the spores appear almost black, tinged with purple.[22] The spores are spherical in shape or nearly so, smooth, thick-walled, and lack a germ pore. They are nonamyloid (not absorbing iodine when stained with Melzer's reagent), black-brown, and have dimensions of 4.5–7.5 by 5.5–6.5 µm.[23] There is a prominent scar where the spore was once attached to the basidium (the spore-bearing cell) through the sterigma. The basidia are broadly club-shaped, and mostly four-spored, with long, slender sterigmata. Unlike other Agaricus species, the spores of A. deserticola are not shot off, but are instead dispersed when they sift out of the dried, mature fruit bodies after the peridium breaks open.[2]
 Schaeffer's chemical test is often used to help identify and differentiate Agaricus species. In this test, aniline plus nitric acid are applied to the surface of the fruit body, and if positive, a red or orange color forms.[26] Agaricus deserticola has a positive Schaeffer's reaction, similar to species in section Arvensis in the genus Agaricus.[10]
 Species that resemble A. deserticola include the desert fungi Montagnea arenaria and Podaxis pistillaris.[20] Montagnea arenaria is a whitish stalked puffball with a hollow, woody stalk and a loose sac-like volva at the base of the stem. It is topped by a thin disc-like cap with blackish gill plates suspended around the margin. Podaxis pistillaris has a cylindrical to oval white to brownish cap with a paper-thin wall atop a slender stem. When mature, the cap contains powdery, dark brown spores.[16]
 The edibility of the fruit bodies of Agaricus deserticola is not known definitively, and there are conflicting opinions in the literature, with some sources claiming that the edibility is unknown, and consumption should be avoided.[20][27] However, one popular field guide to North American mushrooms suggests they are edible when they are young, and have a pleasant odor and mild taste.[23]
 In one early study of the mushroom's development, the fruit bodies appeared above the surface of the ground two or three days after rainfall or an irrigation, and required between five and eight days to mature. Slender and fragile rhizomorphs—dense masses of hyphae that form root-like structures—grow horizontally 2.5 to 5 cm (1.0 to 2.0 in) below the soil surface. Fruit bodies start as enlarged tips on the rhizomorphs, and manifest as numerous small, almost-spherical protuberances just beneath the surface of the soil. When the fruit bodies reach a diameter of about 4 to 6 mm (0.16 to 0.24 in), the stem and peridial regions begin to be distinguishable; the peridial region first appears as a small swelling at the apex of the much larger stem regions.[22]
 The fruit bodies push upward through the soil when they are about 2 cm (0.8 in) tall. As growth progresses, the stem elongates and the peridium becomes more rounded, increasing in size until maturity. At about the time the peridium reaches 1 cm (0.4 in) or slightly more in diameter, the columella exerts an upward tension on the tissue of the partial veil, and it begins to pull away from the stem. Typically, the veil tissue is weakest near the attachment to the stem, rather than to the attachment at the edge of the peridium, and the veil separates from the stem. The lower edge of the peridium is further stretched as it is pulled upward and outward. Usually, the arid environment causes the gleba to dry out rapidly. If the veil tissue at the base of the stem is stronger than that attached to the edge of the peridium, the veil can rip so it remains attached to the stem as a ring. Scales begin to appear on the surface of the peridium of some specimens at about this time.[22]
 Like other Agaricus species, A. deserticola is saprobic—feeding off dead or decaying organic matter. The fruit bodies are found growing singly to sometimes more numerous, at low elevations, and typically in sandy soil. 
The species' usual habitats include drylands, coastal sage scrub, and desert ecosystems.[23] It also grows in lawns and fields.[27] The range of the fungus is restricted to southwestern and western North America, where it fruits throughout the year, typically during or following cold, wet weather.[23] Zeller gives a range that includes as its eastern border central Texas, and extends westward to San Diego County, California, and north to Josephine County, Oregon.[2] The mushroom used to be common in the San Francisco Bay area before land development reduced its preferred habitats.[16] A. deserticola has been collected in several states in northwestern Mexico, including Sonora,[28] Chihuahua,[15] and Baja California.[29]
 
 1 Taxonomic history 2 Classification and phylogeny 3 Description

3.1 Microscopic characteristics
3.2 Similar species
3.3 Edibility

 3.1 Microscopic characteristics 3.2 Similar species 3.3 Edibility 4 Fruit body development 5 Habitat and distribution 6 See also 7 References 8 External links Fungi portal List of Agaricus species ^ "Species synonymy: Agaricus texensis (Berk. & M.A. Curtis) Geml, Geiser & Royse". Species Fungorum. CAB International. Retrieved 2011-03-21..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c d e f Zeller SM. (1943). "North American species of Galeropsis, Gyrophagmium, Longia, and Montagnea". Mycologia. 35 (4): 409–21. doi:10.2307/3754593. JSTOR 3754593.
 ^ Berkeley MJ. (1873). "Notices of North American fungi (cont.)". Grevillea. 2 (15): 33–5.
 ^ Massee G. (1891). "New or imperfectly known Gasteromycetes". Grevillea. 19 (92): 94–8.
 ^ Murrill WA. (1916). "Agaricaceae tribe Agariceae". North American Flora. 9 (5): 297–374. (see p. 356)
 ^ Peck CH. (1895). "New species of fungi". Bulletin of the Torrey Botanical Club. 22 (12): 485–93. doi:10.2307/2477972. JSTOR 2477972.
 ^ Copeland EB. (1904). "New and interesting California fungi". Annales Mycologici. 2 (1): 1–7.
 ^ Longia as described by Sydow in 1921 is currently known as Haploravenelia. Kirk PM, Cannon PF, Minter DW, Stalpers JA (2008). Dictionary of the Fungi (10th ed.). Wallingford, UK: CAB International. p. 392. ISBN 978-0-85199-826-8.
 ^ Zeller SM. (1945). "Notes and brief articles: A new name". Mycologia. 37 (5): 636. doi:10.1080/00275514.1945.12024022. JSTOR 3754700.
 ^ a b c d e Geml J, Geiser DM, Royse DJ (2004). "Molecular evolution of Agaricus species based on ITS and LSU rDNA sequences". Mycological Progress. 3 (2): 157–76. doi:10.1007/s11557-006-0086-8.
 ^ a b Geml J. (2004). "Evolution in action: Molecular evidence for recent emergence of secotioid genera Endoptychum, Gyrophragmium and Longula from Agaricus ancestors". Acta Microbiologica et Immunologica Hungarica. 51 (1–2): 97–108. doi:10.1556/AMicr.51.2004.1-2.7. PMID 15362291.
 ^ a b c Lloyd CG. (1904). "Gyrophragmium decipiens". Mycological Notes. 18: 196.
 ^ Berkeley MJ, Curtis MA (1853). "Centuries of North American fungi". Annals and Magazine of Natural History. II. 12 (72): 417–35. doi:10.1080/03745485709495068.
 ^ Halling RE. (2004). "Extralimited, excluded and doubtful species". A revision of Collybia s.l. in the northeastern United States & adjacent Canada. New York Botanical Garden. Retrieved 2011-03-25.
 ^ a b Moreno G, Marcos Lizárraga ME, Coronado ML (2010). "Gasteroids and secotioids of Chihuahua (Mexico)". Mycotaxon. 112: 291–315. doi:10.5248/112.291.
 ^ a b c d e f Arora D. (1986). Mushrooms Demystified: A Comprehensive Guide to the Fleshy Fungi. Berkeley, California: Ten Speed Press. pp. 725–30. ISBN 978-0-89815-169-5.
 ^ Fischer E. (1900). "Hymenogastrineae".  In Engler A, Krause K, Pilger RKF, Prantl KAE (eds.). Natürlichen Pflanzenfamilien I (in German). 1. Leipzig, Germany: W. Engelmann. pp. 296–313.
 ^ Conrad HS. (1915). "The structure and development of Secotium agaricoides". Mycologia. 7 (2): 94–104. doi:10.2307/3753132. JSTOR 3753132. Archived from the original on 2015-09-23. Retrieved 2011-03-22.
 ^ Morse EE. (1933). "A study of the genus Podaxis". Mycologia. 25 (1): 1–33. doi:10.2307/3754126. JSTOR 3754126. Archived from the original on 2015-09-23. Retrieved 2011-03-22.
 ^ a b c Wood M, Stevens F. "Longula texensis". California Fungi. MykoWeb. Retrieved 2011-03-18.
 ^ Capelari M, Rosa LH, Lachance M-A (2006). "Description and affinities of Agaricus martineziensis, a rare species" (PDF). Fungal Diversity. 21: 11–8.
 ^ a b c d e Barnett HL. (1943). "The development and structure of Longia texensis". Mycologia. 35 (4): 399–408. doi:10.2307/3754592. JSTOR 3754592.
 ^ a b c d e f Miller HR, Miller OK Jr (2006). North American Mushrooms: A Field Guide to Edible and Inedible Fungi. Guilford, Connecticut: Falcon Guide. p. 489. ISBN 978-0-7627-3109-1.
 ^ a b c d e Harding PR Jr. (1957). "Notes on Longula texensis var. major". Mycologia. 49 (2): 273–6. doi:10.2307/3755637. JSTOR 3755637.
 ^ Miller HR, Miller OK Jr (1988). Gasteromycetes: Morphological and Developmental Features, with Keys to the Orders, Families, and Genera. Eureka, California: Mad River Press. p. 120. ISBN 978-0-916422-74-5.
 ^ Rumack BH, Spoerke DG (1994). Handbook of Mushroom Poisoning: Diagnosis and Treatment. Boca Raton, Florida: CRC Press. p. 146. ISBN 978-0-8493-0194-0.
 ^ a b Phillips R. "Longula texensis". Rogers Mushrooms. Archived from the original on 2012-06-16. Retrieved 2011-03-18.
 ^ Moreno G, Esqueda M, Perez-Silva E, Herrera T, Altes A (2007). "Some interesting gasteroid and secotioid fungi from Sonora, Mexico". Persoonia. 19 (2): 265–80.
 ^ Ochoa C, Moreno G (2006). "Hongos gasteroides y secotioides de Baja California, México" [Gasteroid and secotioid fungi of Baja California, Mexico]. Boletín de la Sociedad Micológica de Madrid (in Spanish). 30: 121–166.
  Media related to Agaricus deserticola at Wikimedia Commons Wikidata: Q623048 Fungorum: 516712 GBIF: 5898938 iNaturalist: 349998 MycoBank: 516712 NCBI: 181864 Agaricus Edible fungi Fungi described in 1873 Fungi of North America Secotioid fungi CS1 German-language sources (de) CS1 Spanish-language sources (es) Articles with short description Articles with 'species' microformats Commons category link is on Wikidata Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Cebuano Español Français Nederlands Português Ślůnski Svenska Winaray  This page was last edited on 28 October 2019, at 20:58 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  A. deserticola Agaricus deserticola Agaricus deserticola gasteroid agaricus gills convex free ring bare purple-black saprotrophic edible A. deserticola ^ a b c d e f 35 ^ 2 ^ 19 ^ 9 ^ 22 ^ 2 ^ ^ 37 a b c d e 3 a b 51 a b c 18 ^ 12 ^ a b 112 a b c d e f ^ 1 ^ 7 ^ 25 a b c ^ 21 a b c d e 35 a b c d e f a b c d e 49 ^ ^ a b ^ 19 ^ 30 
 Kingdom:
 Fungi
 Division:
 Basidiomycota
 Class:
 Agaricomycetes
 Order:
 Agaricales
 Family:
 Agaricaceae
 Genus:
 Agaricus
 Species:
 A. deserticola
 Agaricus deserticolaG.Moreno, Esqueda & Lizárraga (2010)
 
Secotium texense Berk. & M.A.Curtis (1873)
Gyrophragmium texense (Berk. & M.A.Curtis) Massee (1891)
Secotium decipiens Peck (1895)
Podaxon strobilaceus Copeland (1904)
Gymnopus texensis (Berk. & M.A.Curtis) Murrill (1916)
Longia texensis (Berk. & M.A.Curtis) Zeller (1943)
Longula texensis (Berk. & M.A.Curtis) Zeller (1945)
Agaricus texensis (Berk. & M.A.Curtis) Geml, Geiser & Royse (2004)

  Mycological characteristics gills on hymenium cap is convex hymenium is free stipe has a ring
   or is bare spore print is purple-black ecology is saprotrophic edibility: edible 


 




 




 




 


.mw-parser-output table.clade{border-spacing:0;margin:0;font-size:100%;line-height:100%;border-collapse:separate;width:auto}.mw-parser-output table.clade table.clade{width:100%}.mw-parser-output table.clade td.clade-label{width:0.7em;padding:0 0.2em;vertical-align:bottom;text-align:center;border-left:1px solid;border-bottom:1px solid}.mw-parser-output table.clade td.clade-label.first{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-label.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-slabel{padding:0 0.2em;vertical-align:top;text-align:center;border-left:1px solid}.mw-parser-output table.clade td.clade-slabel.last{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-slabel.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-bar{vertical-align:middle;text-align:left;padding:0 0.5em;position:relative}.mw-parser-output table.clade td.clade-bar.reverse{text-align:right}.mw-parser-output table.clade td.clade-leaf{border:0;padding:0;text-align:left}.mw-parser-output table.clade td.clade-leafR{border:0;padding:0;text-align:right}.mw-parser-output table.clade td.clade-leaf.reverse{text-align:right}


 


A. lanipes



 


 


A. maskae



 


 


A. subrutilescens



 



 


 


A. silvaticus var. pallidus



 



 


 


A. aridicola



 



 


 


A. deserticola



 



 

  
 


 




 




 


.mw-parser-output table.clade{border-spacing:0;margin:0;font-size:100%;line-height:100%;border-collapse:separate;width:auto}.mw-parser-output table.clade table.clade{width:100%}.mw-parser-output table.clade td.clade-label{width:0.7em;padding:0 0.2em;vertical-align:bottom;text-align:center;border-left:1px solid;border-bottom:1px solid}.mw-parser-output table.clade td.clade-label.first{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-label.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-slabel{padding:0 0.2em;vertical-align:top;text-align:center;border-left:1px solid}.mw-parser-output table.clade td.clade-slabel.last{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-slabel.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-bar{vertical-align:middle;text-align:left;padding:0 0.5em;position:relative}.mw-parser-output table.clade td.clade-bar.reverse{text-align:right}.mw-parser-output table.clade td.clade-leaf{border:0;padding:0;text-align:left}.mw-parser-output table.clade td.clade-leafR{border:0;padding:0;text-align:right}.mw-parser-output table.clade td.clade-leaf.reverse{text-align:right}


 


A. lanipes



 


 


A. maskae



 


 


A. subrutilescens



 



 


 


A. silvaticus var. pallidus



 



 


 


A. aridicola



 



 


 


A. deserticola



 

  
 


 




 


.mw-parser-output table.clade{border-spacing:0;margin:0;font-size:100%;line-height:100%;border-collapse:separate;width:auto}.mw-parser-output table.clade table.clade{width:100%}.mw-parser-output table.clade td.clade-label{width:0.7em;padding:0 0.2em;vertical-align:bottom;text-align:center;border-left:1px solid;border-bottom:1px solid}.mw-parser-output table.clade td.clade-label.first{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-label.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-slabel{padding:0 0.2em;vertical-align:top;text-align:center;border-left:1px solid}.mw-parser-output table.clade td.clade-slabel.last{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-slabel.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-bar{vertical-align:middle;text-align:left;padding:0 0.5em;position:relative}.mw-parser-output table.clade td.clade-bar.reverse{text-align:right}.mw-parser-output table.clade td.clade-leaf{border:0;padding:0;text-align:left}.mw-parser-output table.clade td.clade-leafR{border:0;padding:0;text-align:right}.mw-parser-output table.clade td.clade-leaf.reverse{text-align:right}


 


A. lanipes



 


 


A. maskae



 


 


A. subrutilescens



 



 


 


A. silvaticus var. pallidus



 



 


 


A. aridicola



 

  
 


 


.mw-parser-output table.clade{border-spacing:0;margin:0;font-size:100%;line-height:100%;border-collapse:separate;width:auto}.mw-parser-output table.clade table.clade{width:100%}.mw-parser-output table.clade td.clade-label{width:0.7em;padding:0 0.2em;vertical-align:bottom;text-align:center;border-left:1px solid;border-bottom:1px solid}.mw-parser-output table.clade td.clade-label.first{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-label.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-slabel{padding:0 0.2em;vertical-align:top;text-align:center;border-left:1px solid}.mw-parser-output table.clade td.clade-slabel.last{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-slabel.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-bar{vertical-align:middle;text-align:left;padding:0 0.5em;position:relative}.mw-parser-output table.clade td.clade-bar.reverse{text-align:right}.mw-parser-output table.clade td.clade-leaf{border:0;padding:0;text-align:left}.mw-parser-output table.clade td.clade-leafR{border:0;padding:0;text-align:right}.mw-parser-output table.clade td.clade-leaf.reverse{text-align:right}


 


A. lanipes



 


 


A. maskae



 


 


A. subrutilescens



 



 


 


A. silvaticus var. pallidus



 

  
 
.mw-parser-output table.clade{border-spacing:0;margin:0;font-size:100%;line-height:100%;border-collapse:separate;width:auto}.mw-parser-output table.clade table.clade{width:100%}.mw-parser-output table.clade td.clade-label{width:0.7em;padding:0 0.2em;vertical-align:bottom;text-align:center;border-left:1px solid;border-bottom:1px solid}.mw-parser-output table.clade td.clade-label.first{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-label.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-slabel{padding:0 0.2em;vertical-align:top;text-align:center;border-left:1px solid}.mw-parser-output table.clade td.clade-slabel.last{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-slabel.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-bar{vertical-align:middle;text-align:left;padding:0 0.5em;position:relative}.mw-parser-output table.clade td.clade-bar.reverse{text-align:right}.mw-parser-output table.clade td.clade-leaf{border:0;padding:0;text-align:left}.mw-parser-output table.clade td.clade-leafR{border:0;padding:0;text-align:right}.mw-parser-output table.clade td.clade-leaf.reverse{text-align:right}


 


A. lanipes



 


 


A. maskae



 


 


A. subrutilescens



 

  
 
A. lanipes

  
  
 
A. maskae

  
  
 
A. subrutilescens

  
  
  
 
A. silvaticus var. pallidus

  
  
  
 
A. aridicola

  
  
  
 
A. deserticola

  
  
 Cladogram showing the phylogeny and relationships of Agaricus deserticola and related Agaricus species based on ribosomal DNA sequences.[10]
 
Wikidata: Q623048
Fungorum: 516712
GBIF: 5898938
iNaturalist: 349998
MycoBank: 516712
NCBI: 181864
 