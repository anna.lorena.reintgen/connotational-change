
   Resident   Non-breeding
 Cotyle borbonica Gmelin, 1789Hirundo borbonica. Bonaparte, 1850.
 The Mascarene martin or Mascarene swallow (Phedina borbonica) is a passerine bird in the swallow family that breeds in Madagascar and in the Mascarene Islands. The nominate subspecies occurs on Mauritius and Réunion and has never been found away from the Mascarene Islands, but the smaller Madagascan subspecies, P. b. madagascariensis, is migratory and has been recorded wintering in East Africa or wandering to other Indian Ocean islands.
 The Mascarene martin is a small swallow that has grey-brown underparts becoming white on the throat and lower abdomen, dark grey-brown upperparts and a slightly forked tail. The underparts are heavily streaked with black. It nests in small colonies anywhere with suitably sheltered sites for constructing a nest, such as ledges, buildings, tunnels, caves or amongst rocks. The nest is a shallow cup of twigs and other plant material, and the normal clutch is two or three brown-spotted white eggs. The incubation and fledging times are unknown. The Mascarene martin has a heavy flight with slow wingbeats interspersed with glides, and frequently perches on wires. It feeds on insects in flight, often hunting low over the ground or vegetation. In eastern Africa, open habitats such as deforested areas are frequently used for hunting. A number of internal and external parasites have been detected in this species.
 Tropical cyclones can adversely affect populations on the smaller islands, but the Mascarene martin is a locally common bird with an apparently stable population and is classed as a species of Least Concern by the International Union for Conservation of Nature (IUCN). Its legal protection ranges from none on the French overseas department of Réunion to a status on Mauritius as a "species of wildlife in respect of which more severe penalties are provided".
 The Mascarene martin was first formally described in 1789 as Hirundo borbonica by German zoologist Johann Friedrich Gmelin in his 13th edition of Linnaeus's Systema Naturae.[2] It is likely that the species had previously been described by French naturalist Philibert Commerson who died in Mauritius in 1773. His huge collection of specimens and notes was sent back to the Paris Museum in 1774, but destroyed by sulphur fumigation in about 1810.[3] French biologist Charles Lucien Bonaparte moved the martin to his newly created genus Phedina in 1855.[4] The genus name is derived from the Greek phaios (φαιός) "brown" and the Italian rondine "swallow",[5] and the species name refers to the Île de Bourbon (old French name for Réunion).[6] There are two subspecies, nominate P. borbonica borbonica on Mauritius and Réunion, and P. b. madagascariensis in Madagascar.[7]
 The Phedina swallows are placed within the Hirundininae subfamily, which comprises all swallows and martins except the very distinctive river martins. DNA sequence studies suggest that there are three major groupings within the Hirundininae, broadly correlating with the type of nest built. These groups are the "core martins", including burrowing species like the sand martin; the "nest-adopters", which are birds like the tree swallow that utilise natural cavities; and the "mud nest builders", such as the barn swallow, which build a nest from mud. The Phedina species nest in burrows and therefore belong to the "core martins".[8][9]
 The genus Phedina is thought to be an early offshoot from the main swallow lineage, although the striped plumage of its two species suggests a distant relationship with streaked African Hirundo species.[10][11] The other member of the genus is the Brazza's martin P. brazzae, although in the past it has sometimes been suggested that Brazza's martin should be moved to its own genus, Phedinopsis, due to the significant differences in vocalisations and nest type from its relative.[10][12] The nearest relation of the two Phedina martins is the banded martin, Riparia cincta, which appears not to be closely related to the other members of its current genus and resembles Brazza's martin in its nesting habits and calls.[8][13] The current Association of European Rarities Committees (AERC)-recommended practice is to move the banded martin to its own genus as Neophedina cincta, rather than to merge it into Phedina, since the banded martin's larger size, different bill and nostril shape and non-colonial nesting are differences from the current Phedina species.[14]
German ornithologist Gustav Hartlaub separated the Madagascan population of the Mascarene martin as a full species, P. madagascariensis,[15] but more recent authorities have considered it to be only a subspecies, P. b. madagascariensis.[1][7]
 Adult Mascarene martins of the nominate subspecies are 15 cm (5.9 in) long with wings averaging 117 mm (4.6 in)[7] and weigh 23.9 g (0.84 oz).[16] This small hirundine has dark brown-grey upperparts with faint streaking. It has grey-brown underparts becoming white on the throat and lower abdomen, all being heavily streaked with black. The slightly forked tail averages 54.6 mm (2.15 in) long and has white edges to the brown undertail coverts. The wings are blackish-brown and the bill and legs are black. The eyes are dark brown and the black bill averages 11.3 mm (0.44 in) long. The sexes are similar, but juvenile birds have more diffuse breast streaking, and white tips to the feathers covering the closed wing. The Madagascan subspecies is overall paler and larger-billed than the nominate form. It has denser streaking on the breast, but only very fine lines on the lower abdomen and on the white undertail.[7] It is distinctly smaller than the nominate subspecies, 12–14 cm (4.7–5.5 in)[17] in length with an average weight of 20.6 g (0.73 oz).[16] This martin moults in December and January on Mauritius, and Madagascan breeders wintering on the African mainland moult in June and July.[7]
 The Mascarene martin is a relatively quiet bird, but it has a warbled siri-liri siri-liri song given in flight or when perched;[7] some calls given by perched birds end in a glissando.[18] Other vocalisations may be used during mating or displays of aggression. There is a chip contact call,[7] and the young birds produce a fast twittering sound when begging for food.[18] Birds wintering in mainland Africa are usually silent.[17]
 No other streaked swallow species occur within the island breeding range of the Mascarene martin, and in Africa the lesser striped swallow is larger, has a deeply forked tail and a very different plumage, with dark blue upperparts, a red rump and a chestnut head.[19] The brown-throated sand martin  has similar structure and plumage colour to the Mascarene martin, but has plain, unstreaked underparts.[7] The small Mascarene swiftlet has longer, narrower wings than the martin, and a much lighter flight.[20] The Brazza's martin is smaller, has a plainer back and finer dashing on the throat and chest,[21] but there is no range overlap.[22]
 The Mascarene martin's breeding range is restricted to Madagascar and the Mascarene Islands. The nominate subspecies breeds on Mauritius and Réunion and P. b. madagascariensis occurs in Madagascar. It may also nest on Pemba Island where it has been seen in the breeding season. Breeding habitat can be anywhere with suitable sites for constructing a nest, such as ledges, buildings, tunnels, caves or amongst rocks. The martin is found on the east side of Réunion between 200–500 m (660–1,640 ft), and on the south and west coasts of Mauritius. It also occurs on inland cliffs on Mauritius.[7]
 The subspecies P. b. borbonica is resident on Mauritius and Réunion, although there are local seasonal movements on these island, but the Madagascan subspecies is migratory. The Imerina Plateau is deserted from April to September, the martins moving to lower ground or to the African mainland.[7] It is normally uncommon and local in coastal Mozambique,[17][23][24] Zambia, Malawi and Pemba Island,[25] and very rare in Kenya and mainland Tanzania,[26][27][28] although large numbers sometimes winter in Mozambique or Malawi. It has also been recorded from Comoros and other Indian Ocean locations including at least four islands in the Seychelles. As of 2012, a total of eight birds had been sighted in the Seychelles, occurring in both the spring and autumn migration periods.[29][30] Some of these records may be due to vagrant birds carried by cyclones.[7] There are unsubstantiated claims of occurrences in the Transvaal.[31]
 The Mascarene martin has a heavy flight with slow wingbeats interspersed with glides,[32] and may repeatedly return to a favourite perch.[33] This martin is often seen perched on wires,[34] and sometimes rests on sandy beaches.[33] The martin roosts in small flocks in bushes, on buildings or on cliffs. Sometimes it is joined at the roost by other birds, such as blue-cheeked bee-eaters in the Seychelles.[7]
 The Mascarene martin nests in the wet season, August to November in Madagascar, and September to early January on Mauritius and Réunion. It breeds in groups typically comprising a few pairs, although a colony of about 20 pairs has been recorded on Mauritius. The nest is a shallow cup of twigs and coarse plant material such as grass and Casuarina with a softer lining of feathers and finer vegetation. It may be constructed anywhere suitably flat and inaccessible to predators, including locations 3–5 m (9.8–16.4 ft) over water, on slate ledges, or in underground passageways;[7] one particularly unusual nesting site was on a small boat moored 20 m (66 ft) off the coast.[35] The normal clutch is two eggs on Madagascar and Mauritius, but two or three on Réunion. The eggs are white with brown spots and average 21.6 mm × 15 mm (0.85 in × 0.59 in) with a weight of 2.5 g (0.088 oz) and are incubated by the female alone. The incubation and fledging times are unknown,[7] although as with all hirundines the chicks are altricial, hatching naked and blind.[36] The male helps to feed the young, and the chicks are fed by the parents after fledging,[7] and one pair on Mauritius was observed to feed its two chicks at roughly five-minute intervals.[35]
 The martins feed in flight, often low over the ground or vegetation. They hunt singly, in small groups or with other swallows and swifts, and are most active just before dusk.[7] The flying insects that make up their diet include scarab, click and other beetles, bugs and flying ants.[37] The feeding habitat in Madagascar includes woodlands, agricultural land, wetlands, semi desert and open ground at altitudes up to 2,200 m (7,200 ft). In Mauritius and Réunion this martin feeds from sea level up to 1,500 m (4,900 ft) over reservoirs and coasts, along cliffs and over Casuarina or other trees and scrubs, and in eastern Africa, areas deforested by logging or conversion to agriculture are used for hunting.[7][38]
 Mascarene martins will mob the Mauritius kestrel, suggesting that it is perceived as a potential predator.[39] Martins on Mauritius may be infected by an endemic trypanosome, Trypanosoma phedinae,[40] although the pathogenicity is unknown.[34] Protozoan blood parasites of the genus Haemoproteus have also been found in the martin on Mauritius,[41] although no blood parasites were found in a Madagascan specimen.[42] A new species of louse fly, Ornithomya cecropis, was first found on a martin in Madagascar,[43] and another bird from that island carried the feather mite Mesalges hirsutus, more commonly found in parrots.[44][45]
 The breeding range of the Mascarene martin is restricted to three islands. Madagascar has an area of 592,800 square kilometres (228,900 sq mi),[46] but the next largest island, Réunion, is just 2,512 square kilometres (970 sq mi).[47] Although this bird has a limited range, it is abundant on Mauritius and Réunion, and locally common in Madagascar. The population size is unknown, but exceeds the vulnerability threshold of 10,000 mature individuals and is believed to be stable. This martin is therefore classed as a species of Least Concern by the International Union for Conservation of Nature.[1]
 Tropical cyclones present a natural threat, particularly on the small islands inhabited by the nominate subspecies. The populations on Mauritius and Réunion were badly affected by a cyclone in February 1861, and a British ornithologist, Edward Newton, claimed not to have seen a single specimen on Mauritius between the six-day storm and June of the following year.[48] It took many years for this population to fully recover, but by about 1900 it was reported to be common but local, and in 1973–74 there were 200–400 pairs on Réunion and 70–75 pairs in Mauritius. More recent cyclones, like one in 1980, seem to have had less damaging effects than the 1861 storm.[7] A number of species in the region are vulnerable partly because they are restricted to one island, or are badly affected by habitat degradation or introduced predators, and several species have been lost from the Mascarene islands since human colonisation in the seventeenth century. The martin and the Mascarene Swiftlet occur on all the main islands, and are less vulnerable to the effects of human activities, especially since they can utilise houses for nest sites.[49]
 In Mauritius, the Mascarene martin is legally protected as a "species of wildlife in respect of which more severe penalties are provided". It is illegal to kill any bird of the species or to take or destroy their nests under section 16 of the Wildlife and National Parks Act 1993.[50] although Madagascar and the African mainland countries have no special measures beyond general bird protection legislation.[51] Réunion is an overseas department of France, but the Birds Directive does not apply outside Europe, so there is no European-level bird protection legislation effective on the island, despite the possibility that European Union agricultural and other funding may be adversely affecting birds and vulnerable habitats.[49][52]
 1 Taxonomy 2 Description 3 Distribution and habitat 4 Behaviour

4.1 Breeding
4.2 Feeding

 4.1 Breeding 4.2 Feeding 5 Predators and parasites 6 Status 7 References 8 Cited texts 9 External links ^ a b c BirdLife International (2012). "Phedina borbonica". IUCN Red List of Threatened Species. Version 2013.2. International Union for Conservation of Nature. Retrieved 26 November 2013..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ Gmelin (1789) p. 1017.
 ^ Cheke, Anthony S (2009). "Data sources for 18th century French encyclopaedists – what they used and omitted: evidence of data lost and ignored from the Mascarene Islands, Indian Ocean" (PDF). Journal of the National Museum (Prague), Natural History Series. 177 (9): 91–117. ISSN 1802-6842.
 ^ Bonaparte, Charles Lucien (1855). "Note sur les Salanganes et sur les nids". Comptes Rendus Hebdomadaires des Séances de l'Académie des Sciences (in French). 41: 976–979.
 ^ Jobling (2010) p. 302.
 ^ Jobling (2010) p. 74.
 ^ a b c d e f g h i j k l m n o p q r Turner & Rose (1989) pp. 155–157.
 ^ a b Sheldon, Frederick H; Whittingham, Linda A; Moyle, Robert G; Slikas, Beth; Winkler, David W (2005). "Phylogeny of swallows (Aves: Hirundinidae) estimated from nuclear and mitochondrial DNA". Molecular Phylogenetics and Evolution. 35 (1): 254–270. doi:10.1016/j.ympev.2004.11.008. PMID 15737595.
 ^ Winkler, David W; Sheldon, Frederick H (1993). "Evolution of nest construction in swallows (Hirundinidae): A molecular phylogenetic perspective" (PDF). Proceedings of the National Academy of Sciences USA. 90 (12): 5705–5707. Bibcode:1993PNAS...90.5705W. doi:10.1073/pnas.90.12.5705. PMC 46790. PMID 8516319. Archived from the original (PDF) on 2011-07-17.
 ^ a b Turner & Rose (1989) p. 8.
 ^ Turner & Rose (1989) pp. 70–72.
 ^ Wolters, Hans Edmund (1971). "Probleme der Gattungsabgrenzung in der Ornithologie" (PDF). Bonner Zoologische Beitraege (in German). 22 (3–4): 210–219.
 ^ Mills, Michael S L; Cohen, Callan (2007). "Brazza's Martin Phedina brazzae: new information on range and vocalisations". Ostrich. 78 (1): 51–54. doi:10.2989/OSTRICH.2007.78.1.8.52.
 ^ Crochet et al. (2011) p. 4.
 ^ Sharpe & Wyatt (1894) pp. 199–208.
 ^ a b Dunning (2007) p. 327.
 ^ a b c Sinclair et al. (2002) p. 298.
 ^ a b Diamond (1987) p. 110.
 ^ Turner & Rose (1989) pp. 194–197.
 ^ Sinclair & Langrand (2004) p. 295.
 ^ Reichenow (1903) p. 425.
 ^ Turner & Rose (1989) p. 157.
 ^ Spottiswoode, Claire; Ryan, Peter G (2002). "First record of Mascarene martin Phedina borbonica in Sul do Save, Mozambique". Bird Numbers. 11 (1): 23.
 ^ Cohen, Callan; Leslie, Rob; Winter, Dave (1997). "Second record of Mascarene martin for Southern Africa". Africa - Birds & Birding. 2 (4): 14.
 ^ Williams & Arlott (1980) p. 260.
 ^ Zimmerman et al. (2005) p. 427.
 ^ Stevenson et al. (2004) p. 290.
 ^ Medland, R D (1988). "Mascarene martin, Phedina borbonica, near Chiromo". Nyala. 12 (1–2): 73.
 ^ Skerrett, Adrian; Betts, Michael; Bullock, Ian; Fisher, David; Gerlach, Ron; Lucking, Rob; Phillips, John; Scott, Bob (2006). "Third report of the Seychelles Bird Records Committee" (PDF). Bulletin of the African Bird Club. 13 (1): 65–72.
 ^ "March 2012". Seychelles Bird Records Committee. Archived from the original on 16 December 2012. Retrieved 16 December 2012.
 ^ Tarburton (1987) pp. 4, 176.
 ^ Langrand (1991) p. 254.
 ^ a b Newton, Edward (1861). "Ornithological notes from Mauritius". The Ibis. 3 (3): 270–278. doi:10.1111/j.1474-919X.1861.tb07460.x.
 ^ a b Diamond (1987) pp. 171–172.
 ^ a b Evans, Steven W; Bouwman, Henk (2011). "An unusual nesting site of a Mascarene Martin Phedina borbonica on Mauritius". Ostrich. 82 (2): 155–156. doi:10.2989/00306525.2011.603480.
 ^ Turner & Rose (1989) p. 4.
 ^ Goodman, Steven M; Parrillo, Phillip (1997). "A study of Malagasy birds based on stomach contents". Ostrich. 68 (2–4): 104–113. doi:10.1080/00306525.1997.9639723.
 ^ Clancey, P A; Lawson, Walter J; Irwin, Michael P Stuart (1969). "The Mascarene Martin Phedina borbonica (Gmelin) in Mozambique: a new species to the South African list". Ostrich. 40 (1): 5–8. doi:10.1080/00306525.1969.9634318.
 ^ Diamond (1987) p. 229.
 ^ Peirce, M A; Cheke A S; Cheke R A (1977). "A survey of blood parasites of birds in the Mascarene Islands, Indian Ocean: with descriptions of two new species and taxonomic discussion". Ibis. 119 (4): 451–461. doi:10.1111/j.1474-919X.1977.tb02053.x.
 ^ Peirce, M A; Mead, C J (1976). "Haematozoa of British birds. I. Blood parasites of birds from Dumfries and Lincolnshire". Bulletin of the British Ornithologists' Club. 96 (4): 128–132.
 ^ Bennetti, Gordon F; Blancoul, J (1974). "A note on the blood parasites of some birds from the Republic of Madagascar". Journal of Wildlife Diseases. 10 (3): 239–240. doi:10.7589/0090-3558-10.3.239. PMID 4210766.
 ^ Hutson, A M (1971). "New species of the Ornithomya biloba-group and records of other Hippoboscidae (Diptera) from Africa". Journal of Entomology Series B, Taxonomy. 40 (2): 139–148. doi:10.1111/j.1365-3113.1971.tb00116.x.
 ^ Gaut, Jean (1952). "Sarcoptides plumicoles des oiseaux de Madagascar". Mémoires de l'Institut scientifique de Madagascar: Biologie animale (in French). 7: 81–107.
 ^ Schöne, Richard; Schmidt, Volker; Sachse, Margit; Schmäschke, Ronald (2009). "Federmilben bei Papageienvögeln" (PDF). Papageien (in German). 22 (2): 55–61.
 ^ Bureau of African Affairs (3 May 2011). "Background Note: Madagascar". US Department of State. Retrieved 24 August 2011.
 ^ Petit & Prudent (2010) pp. 84–87.
 ^ Sharpe, Richard Bowdler (1870). "On the Hirundinidae of the Ethiopian Region". Proceedings of the Zoological Society: 286–321. (from 295).
 ^ a b Maggs (2009) pp. 10–12.
 ^ "Wildlife and National Parks Act 1993". Government of Mauritius. Archived from the original on 14 November 2010. Retrieved 15 December 2012.
 ^ de Klemm & Lausche (1986) pp. 357–360, 369–375, 488.
 ^ Papazoglou et al. (2004) p. 23.
 Crochet, P-A; Barthel P H; Bauer H-G; van den Berg A B; Bezzel E; Collinson J M; Dietzen C; Dubois P J; Fromholtz J; Helbig A J; Jiguet F; Jirle E; Knox A G; Krüger T; Le Maréchal P; van Loon A J; Päckert M; Parkin D T; Pons J-M; Raty L; Roselaar C S; Sangster G; Steinheimer F D; Svensson L; Tyrberg T; Votier S C; Yésou P (2011). AERC TAC's taxonomic recommendations: 2011 report (PDF). Luxembourg: AERC. Diamond, Anthony William (1987). Studies of Mascarene Island Birds. Cambridge: Cambridge University Press. ISBN 0-521-25808-1. Dunning, John Barnard (2007). CRC Handbook of Avian Body Masses (Second ed.). Boca Raton: CRC Press. ISBN 1-4200-6444-4. Gmelin, Johann Friedrich (1789). Caroli a Linné systema naturae per regna tria naturae, secundum classes, ordines, genera, species, cum characteribus, differentiis, synonymis, locis. Editio decima tertia, aucta, reformata. Tomus I. Pars II (in Latin). Lipsiae [Leipzig]: Beer. Jobling, James A (2010). The Helm Dictionary of Scientific Bird Names (PDF). London: Christopher Helm. ISBN 978-1-4081-2501-4. de Klemm, Cyrille; Lausche, Barbara J (1986). African Wildlife Laws (IUCN Environmental Policy & Law Occasional Paper; No. 3). Seiburg: World Conservation Union. ISBN 2-88032-091-7. Langrand, Olivier (1991). Guide to the Birds of Madagascar. New Haven: Yale University Press. ISBN 0-300-04310-4. Maggs, Gwen; Ladkoo, Amanda; Poongavanan, Sandra; Chowrimootoo, Aurélie; Tucker, Rachel; Mangroo, Walter; Dawson, Kimberly; Cole, Julie; Baross, Sally; Morris, Anne; Whitfield, Harriet (2009). Olive White-Eye Recovery Program Annual Report 2008–09. Vacoas: Mauritian Wildlife Foundation. Papazoglou, Clairie; Kreiser, Konstantin; Waliczky, Zoltán; Burfield, Ian (2004). Birds in the European Union: a status assessment (PDF). Wageningen: BirdLife International. ISBN 0-946888-56-6. Archived from the original (PDF) on 2013-06-26. Petit, Jérôme (2010).  Prudent, Guillaume (ed.). Climate Change and Biodiversity in the European Union Overseas Entities (PDF). Gland & Brussels: International Union for Conservation of Nature. ISBN 978-2-8317-1315-1. Reichenow, Anton (1903). Die Vögel Afrikas: Zweiter Band (in German). Neudam: J Neuman. Sharpe, Richard Bowdler; Wyatt, Claude Wilmott (1894). A Monograph of the Hirundinidae: Volume 1. London: Self-published. Sinclair, Ian; Hockey, Phil; Tarboton, Warwick (2002). SASOL Birds of Southern Africa. Cape Town: Struik. ISBN 1-86872-721-1. Sinclair, Ian; Langrand, Olivier (2004). Birds of the Indian Ocean Islands: Madagascar, Mauritius, Réunion, Rodrigues, Seychelles and the Comoros. Cape Town: Struik. ISBN 1-86872-956-7. Stevenson, Terry; Fanshawe, John; Small, Brian; Gale, John; Arlott, Norman (2004). Birds of East Africa: Kenya, Tanzania, Uganda, Rwanda, Burundi. London: Christopher Helm. ISBN 0-7136-7347-8. Tarburton, Warwick (1987). Birds of the Transvaal. Pretoria: Transvaal Museum. ISBN 0-620-10006-0. Turner, Angela K; Rose, Chris (1989). A Handbook to the Swallows and Martins of the World. London: Christopher Helm. ISBN 0-7470-3202-5. Williams, John; Arlott, Norman (1980). A Field Guide to the Birds of East Africa. London: Collins. ISBN 0-00-219179-2. Zimmerman, Dale A; Pearson, David J; Turner, Donald A (2005). Birds of Kenya and Northern Tanzania. London: Christopher Helm. ISBN 0-7136-7550-0. Calls at AVoCet v t e African river martin White-eyed river martin Square-tailed saw-wing Mountain saw-wing White-headed saw-wing Black saw-wing Fanti saw-wing Grey-rumped swallow White-backed swallow Mascarene martin Brazza's martin Brown-throated sand martin Congo sand martin Sand martin Pale martin Banded martin Tree swallow Violet-green swallow Golden swallow Bahama swallow Tumbes swallow Mangrove swallow White-winged swallow White-rumped swallow Chilean swallow Purple martin Caribbean martin Sinaloa martin Gray-breasted martin Galápagos martin Peruvian martin Southern martin Brown-chested martin Brown-bellied swallow Blue-and-white swallow Pale-footed swallow Black-capped swallow Andean swallow White-banded swallow Black-collared swallow White-thighed swallow Northern rough-winged swallow Southern rough-winged swallow Tawny-headed swallow Barn swallow Red-chested swallow Angola swallow Pacific swallow Welcome swallow White-throated swallow Ethiopian swallow Wire-tailed swallow White-throated blue swallow Pied-winged swallow White-tailed swallow Pearl-breasted swallow Blue swallow Black-and-rufous swallow Eurasian crag martin Rock martin Dusky crag martin Pale crag martin Common house martin Asian house martin Nepal house martin Greater striped swallow Lesser striped swallow Rufous-chested swallow Mosque swallow Red-rumped swallow Sri Lanka swallow West African swallow Striated swallow Striated swallow Forest swallow Streak-throated swallow Fairy martin Tree martin American cliff swallow Red-throated cliff swallow Preuss's cliff swallow Red Sea cliff swallow South African cliff swallow Cave swallow Chestnut-collared swallow White-backed swallow Wikidata: Q1041651 Wikispecies: Phedina borbonica ADW: Phedina_borbonica Avibase: 6C7BEEC6D6D89B83 BirdLife: 22712198 eBird: masmar1 EoL: 1050804 Fossilworks: 373501 GBIF: 2489195 iNaturalist: 11985 IRMNG: 10216117 ITIS: 561975 IUCN: 22712198 NCBI: 317107 IUCN Red List least concern species Phedina Birds of Réunion Birds of Madagascar Birds described in 1789 CS1 French-language sources (fr) CS1 German-language sources (de) Articles with short description Featured articles Articles with 'species' microformats CS1 Latin-language sources (la) Taxonomy articles created by Polbot Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Wikispecies Create a book Download as PDF Printable version Afrikaans Asturianu বাংলা Български Cebuano Cymraeg Diné bizaad Español Euskara فارسی Français Italiano Magyar Nederlands Polski Português Svenska Tiếng Việt Winaray  This page was last edited on 25 September 2019, at 23:42 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  P. borbonica Phedina borbonica Mascarene martin Mascarene swallow a b c ^ ^ 177 ^ 41 ^ ^ a b c d e f g h i j k l m n o p q r a b 35 ^ 90 a b ^ ^ 22 ^ 78 ^ ^ a b a b c a b ^ ^ ^ ^ ^ 11 ^ 2 ^ ^ ^ ^ 12 ^ 13 ^ ^ ^ a b 3 a b a b 82 ^ ^ 68 ^ 40 ^ ^ 119 ^ 96 ^ 10 ^ 40 ^ 7 ^ 22 ^ ^ ^ a b ^ ^ ^ subfamily: Pseudochelidoninae genus: Pseudochelidon Genus Species subfamily: Hirundininae Genus Species 
 At Ranomafana, Madagascar
 Least Concern (IUCN 3.1)[1]
 Kingdom:
 Animalia
 Phylum:
 Chordata
 Class:
 Aves
 Order:
 Passeriformes
 Family:
 Hirundinidae
 Genus:
 Phedina
 Species:
 P. borbonica
 Phedina borbonica(Gmelin, 1789)
 
 Approximate range in Africa
  Resident   Non-breeding

 
Cotyle borbonica Gmelin, 1789Hirundo borbonica. Bonaparte, 1850.

 River martins (subfamily: Pseudochelidoninae · genus: Pseudochelidon)GenusSpeciesPseudochelidon
African river martin
White-eyed river martin
 GenusSpeciesPseudochelidon
African river martin
White-eyed river martin
 Species Species 
African river martin
White-eyed river martin
 All other swallows and martins (subfamily: Hirundininae)GenusSpeciesPsalidoprocne(saw-wings)
Square-tailed saw-wing
Mountain saw-wing
White-headed saw-wing
Black saw-wing
Fanti saw-wing
Pseudhirundo
Grey-rumped swallow
Cheramoeca
White-backed swallow
Phedina
Mascarene martin
Brazza's martin
Riparia(sand martins)
Brown-throated sand martin
Congo sand martin
Sand martin
Pale martin
Banded martin
Tachycineta  (tree swallows)
Tree swallow
Violet-green swallow
Golden swallow
Bahama swallow
Tumbes swallow
Mangrove swallow
White-winged swallow
White-rumped swallow
Chilean swallow
Progne
Purple martin
Caribbean martin
Sinaloa martin
Gray-breasted martin
Galápagos martin
Peruvian martin
Southern martin
Brown-chested martin
Notiochelidon
Brown-bellied swallow
Blue-and-white swallow
Pale-footed swallow
Black-capped swallow
Haplochelidon
Andean swallow
Atticora
White-banded swallow
Black-collared swallow
Neochelidon
White-thighed swallow
Stelgidopteryx
Northern rough-winged swallow
Southern rough-winged swallow
Alopochelidon
Tawny-headed swallow
Hirundo  (barn swallows)
Barn swallow
Red-chested swallow
Angola swallow
Pacific swallow
Welcome swallow
White-throated swallow
Ethiopian swallow
Wire-tailed swallow
White-throated blue swallow
Pied-winged swallow
White-tailed swallow
Pearl-breasted swallow
Blue swallow
Black-and-rufous swallow
Ptyonoprogne  (crag martins)
Eurasian crag martin
Rock martin
Dusky crag martin
Pale crag martin
Delichon(house martins)
Common house martin
Asian house martin
Nepal house martin
Cecropis
Greater striped swallow
Lesser striped swallow
Rufous-chested swallow
Mosque swallow
Red-rumped swallow
Sri Lanka swallow
West African swallow
Striated swallow
Striated swallow
Petrochelidon
Forest swallow
Streak-throated swallow
Fairy martin
Tree martin
American cliff swallow
Red-throated cliff swallow
Preuss's cliff swallow
Red Sea cliff swallow
South African cliff swallow
Cave swallow
Chestnut-collared swallow
Cheramoeca
White-backed swallow
 GenusSpeciesPsalidoprocne(saw-wings)
Square-tailed saw-wing
Mountain saw-wing
White-headed saw-wing
Black saw-wing
Fanti saw-wing
Pseudhirundo
Grey-rumped swallow
Cheramoeca
White-backed swallow
Phedina
Mascarene martin
Brazza's martin
Riparia(sand martins)
Brown-throated sand martin
Congo sand martin
Sand martin
Pale martin
Banded martin
Tachycineta  (tree swallows)
Tree swallow
Violet-green swallow
Golden swallow
Bahama swallow
Tumbes swallow
Mangrove swallow
White-winged swallow
White-rumped swallow
Chilean swallow
Progne
Purple martin
Caribbean martin
Sinaloa martin
Gray-breasted martin
Galápagos martin
Peruvian martin
Southern martin
Brown-chested martin
Notiochelidon
Brown-bellied swallow
Blue-and-white swallow
Pale-footed swallow
Black-capped swallow
Haplochelidon
Andean swallow
Atticora
White-banded swallow
Black-collared swallow
Neochelidon
White-thighed swallow
Stelgidopteryx
Northern rough-winged swallow
Southern rough-winged swallow
Alopochelidon
Tawny-headed swallow
Hirundo  (barn swallows)
Barn swallow
Red-chested swallow
Angola swallow
Pacific swallow
Welcome swallow
White-throated swallow
Ethiopian swallow
Wire-tailed swallow
White-throated blue swallow
Pied-winged swallow
White-tailed swallow
Pearl-breasted swallow
Blue swallow
Black-and-rufous swallow
Ptyonoprogne  (crag martins)
Eurasian crag martin
Rock martin
Dusky crag martin
Pale crag martin
Delichon(house martins)
Common house martin
Asian house martin
Nepal house martin
Cecropis
Greater striped swallow
Lesser striped swallow
Rufous-chested swallow
Mosque swallow
Red-rumped swallow
Sri Lanka swallow
West African swallow
Striated swallow
Striated swallow
Petrochelidon
Forest swallow
Streak-throated swallow
Fairy martin
Tree martin
American cliff swallow
Red-throated cliff swallow
Preuss's cliff swallow
Red Sea cliff swallow
South African cliff swallow
Cave swallow
Chestnut-collared swallow
Cheramoeca
White-backed swallow
 Species Species 
Square-tailed saw-wing
Mountain saw-wing
White-headed saw-wing
Black saw-wing
Fanti saw-wing
 
Grey-rumped swallow
 
White-backed swallow
 
Mascarene martin
Brazza's martin
 
Brown-throated sand martin
Congo sand martin
Sand martin
Pale martin
Banded martin
 
Tree swallow
Violet-green swallow
Golden swallow
Bahama swallow
Tumbes swallow
Mangrove swallow
White-winged swallow
White-rumped swallow
Chilean swallow
 
Purple martin
Caribbean martin
Sinaloa martin
Gray-breasted martin
Galápagos martin
Peruvian martin
Southern martin
Brown-chested martin
 
Brown-bellied swallow
Blue-and-white swallow
Pale-footed swallow
Black-capped swallow
 
Andean swallow
 
White-banded swallow
Black-collared swallow
 
White-thighed swallow
 
Northern rough-winged swallow
Southern rough-winged swallow
 
Tawny-headed swallow
 
Barn swallow
Red-chested swallow
Angola swallow
Pacific swallow
Welcome swallow
White-throated swallow
Ethiopian swallow
Wire-tailed swallow
White-throated blue swallow
Pied-winged swallow
White-tailed swallow
Pearl-breasted swallow
Blue swallow
Black-and-rufous swallow
 
Eurasian crag martin
Rock martin
Dusky crag martin
Pale crag martin
 
Common house martin
Asian house martin
Nepal house martin
 
Greater striped swallow
Lesser striped swallow
Rufous-chested swallow
Mosque swallow
Red-rumped swallow
Sri Lanka swallow
West African swallow
Striated swallow
Striated swallow
 
Forest swallow
Streak-throated swallow
Fairy martin
Tree martin
American cliff swallow
Red-throated cliff swallow
Preuss's cliff swallow
Red Sea cliff swallow
South African cliff swallow
Cave swallow
Chestnut-collared swallow
 
White-backed swallow
 
Wikidata: Q1041651
Wikispecies: Phedina borbonica
ADW: Phedina_borbonica
Avibase: 6C7BEEC6D6D89B83
BirdLife: 22712198
eBird: masmar1
EoL: 1050804
Fossilworks: 373501
GBIF: 2489195
iNaturalist: 11985
IRMNG: 10216117
ITIS: 561975
IUCN: 22712198
NCBI: 317107
 