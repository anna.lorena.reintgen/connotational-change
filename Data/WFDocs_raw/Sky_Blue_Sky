
 Sky Blue Sky is the sixth studio album by American rock band Wilco, released on May 15, 2007 by Nonesuch Records. Originally announced on January 17, 2007 at a show in Nashville, Tennessee, it was the band's first studio album with guitarist Nels Cline and multi-instrumentalist Pat Sansone. Before its release, the band streamed the entire album on its official website and offered a free download of "What Light".
 Sky Blue Sky was Wilco's highest debuting album on the Billboard 200 at number four. The self-produced album received mostly favorable reviews by critics. Publications such as PopMatters and Rolling Stone praised its maturity, while Playlouder and Pitchfork criticized its "dad-rock" sound. While some critics praised the direct lyrical approach, others criticized it when compared to previous Wilco albums. The band licensed six songs from the Sky Blue Sky sessions to a Volkswagen advertisement campaign, a move that generated criticism from fans and the media.
 In April 2006, Wilco was still touring in support of their most recent studio album, A Ghost Is Born which was released in 2004. The band performed a few new songs during this leg of the tour, including "Walken", "Either Way", and "On and On and On". The following month, drummer Glenn Kotche mentioned to Pitchfork that those new songs were going to be recorded as demos for a new album release.[1] During a January 17, 2007 solo concert, frontman Jeff Tweedy announced that the band would release their sixth studio album on May 15, 2007 through Nonesuch Records.[2][3] The album was named Sky Blue Sky, a reference to a childhood memory of Tweedy's, of a Memorial Day parade in Belleville, Illinois. He had come home from St. Louis with his family, but could not reach his house because the parade blocked the main street. This led Tweedy to reflect upon his future in the town: he knew that he would have to leave when he grew up because it was too small.[4]
 The album was recorded by TJ Doherty at The Loft in Irving Park, Chicago, where Tweedy had recorded Loose Fur's Born Again in the USA and most of Wilco's Yankee Hotel Foxtrot. In an interview with Billboard, the band revealed that the album would be less experimental than the two previous albums and more influenced by The Beatles, The Beach Boys, and The Rolling Stones. Also, unlike the previous albums, the album was made with only minimal involvement of Jim O'Rourke; the album which was produced with very few overdubs.[5]
 The band sought to be more direct with this record than the previous two, resulting in a more mellow album. Tweedy attributes the lyrical directness to his listening to material by The Byrds and Fairport Convention while recording the album.[6] He disliked the reliance on studio effects on previous albums:[7]
 I got nervous about the technology on Yankee Hotel Foxtrot. If you need a certain amp or pedal to make a song what it is, it isn't a song. Many of the album's songs were recorded in a single day, with the band reaching a consensus on how each song should sound.[7] Eighteen songs were recorded for the album and twelve were selected for the album's track listing.[8] The outtake "Let's Not Get Carried Away" was included with iTunes digital downloads, and a full-band version of "The Thanks I Get" was released on the band's Myspace page. Some albums that were shipped to independent record stores included an EP featuring the outtake "One True Vine" and a live version of "Theologians" recorded at The Vic Theater in Chicago.[5] A deluxe edition of Sky Blue Sky was also released, which included a DVD with eight live performances from the Wilco Loft.[9]
 Unlike previous Wilco albums, Sky Blue Sky features more songwriting collaboration between Tweedy and the other members of the band. As a result, a variety of lyrical themes appear on Sky Blue Sky (Tweedy was the only designer of the songs on A Ghost Is Born, using Pro Tools, before presenting them to the band).[10][11] The title track references the worries Tweedy had as a child in a small town while "On and On and On" is an ode to Tweedy's father's experience after the death of Tweedy's mother.[4][10] "Hate It Here" describes a man who tries to fill his free time with chores after breaking up with his lover.[12]
 Sky Blue Sky was the band's first studio record to feature the expanded lineup that premiered on Kicking Television: Live in Chicago. Guitarist Jeff Tweedy provided the lead vocals for the album and John Stirratt, the only other original member of the band, played bass guitar and added background vocals. Glenn Kotche played drums and Mikael Jorgensen performed on a variety of keyboards. New to the band since the previous studio album were lead guitarist Nels Cline and multi-instrumentalist Pat Sansone. Additional instrumentation was provided by violist Karen Waltuch and multi-instrumentalist Jim O'Rourke. The album was mixed by Jim Scott at PLYRZ Studios in Santa Clarita, California.[9]
 The cover artwork of Sky Blue Sky is a photograph by Manuel Presti titled "Sky Chase."[9] The photograph was taken in Rome, Italy, and helped Presti win the 2005 Wildlife Photographer of the Year competition.[13] The same photograph is featured in the July 2007 issue of National Geographic.  Nathaniel Murphy provided several illustrations for the liner booklet, which include a tree on a hill, a hand, and a flock of birds. The booklet also contains photographs of the band members by Frank Ockenfels. Graphic design was provided by Jeff Tweedy and Lawrence Azerrad.[9]
 On March 3, 2007, Wilco's official website hosted a Sky Blue Sky "listening party", which streamed the new album in its entirety.[14][15] Two days later, the track "What Light" was made available for download on the band's website and was streamed on its MySpace page.[16] On March 11, 2007, the official website streamed the album in its entirety again. "The Thanks I Get", a song recorded during the Sky Blue Sky sessions but not included on the album, was made available as a free download to purchasers of the album.[17] During the European segment of the promotional tour, Wilco included a five-song extended play with purchase of the album. The EP contained the songs "The Thanks I Get", "One True Vine", "Let's Not Get Carried Away", and live versions of "Impossible Germany" and "Hate It Here". Wilco made the EP available online for free to previous purchasers of Sky Blue Sky.[18]
 Frustrated by the lack of radio airplay received by previous Wilco albums, the band decided to branch out into television advertising. Wilco had previously licensed songs for Telefónica Moviles, and Jeff Tweedy appeared in a series of advertisements for Apple Computer.[19] In May 2007, Volkswagen began running a pair of commercials with "You Are My Face" and bonus track "The Thanks I Get" playing in the background. The band commented on their website that "we feel okay about VWs. Several of us even drive them."[20] The band licensed six songs for the campaign, which was created by advertising agency Crispin Porter and Bogusky.[21] The move was met with criticism from both fans and popular media.[22][23]
 A promotional tour followed the release of the album. The band performed "Sky Blue Sky" and "You Are My Face" on Later... with Jools Holland on May 25, 2007 and was interviewed on The Dermot O'Leary Show the next day.[20][24] Beginning June 13, 2007, Wilco played fourteen shows in North America  with Low as its opening act. Following this, the band made plans to tour Norway, Denmark, Switzerland, the United Kingdom, Belgium, Italy, and Spain before playing a few more North American shows, including a performance on The Tonight Show.[25]
 Nonesuch released the album on May 15, 2007; the following week became Wilco's best-ever sales week. The album debuted at number four on the U.S. Billboard 200, selling 87,000 copies domestically in its first week.[37] Sky Blue Sky was also an international success, peaking at number 7 in Norway, number 21 in Belgium, number 23 in Australia and Ireland, number 26 in Sweden, number 32 in New Zealand, number 36 in Germany, and number 39 in the United Kingdom.[38]
 The album maintains a score of 73 out of 100 from Metacritic based on "generally favorable reviews" upon its release.[26] Rob Sheffield of Rolling Stone pondered in his review whether Wilco had ever made a song as good as "Impossible Germany," praising how the song builds into a "twin guitar epic" in the mold of Television and Peter Green-era Fleetwood Mac.[35] Michael Metivier of PopMatters commented that while the album took a while to understand, it was full of "exquisitely beautiful melodies and performances".[39] AllMusic writer Mark Deming called the album "Wilco's strongest album as an ensemble to date," and found the return to roots rock music a fresh new method for the band.[27] Blender gave it three-and-a-half stars out of five and said that the album "often feels like the [Grateful] Dead's American Beauty if Jerry Garcia had taken Paxil instead of acid."[40]
 Colin Stutz of Filter gave the album a score of 91% and stated: "Wilco has constructed their most straightforward release in recent memory, which relies heavily on the inspired intricacies of a full-hearted band."[41] Richard Cromelin of Los Angeles Times gave the album three-and-a-half stars out of four and called it "The most musically direct and down to earth of the band's six-album career."[31] Andrew C. Bradick of Prefix Magazine gave the album a favorable review and called it "Wilco's first step toward aging well, but it transcends transition and is an album that sounds right in its place and time."[42] Alan Shulman of No Ripcord gave it a score of eight stars out of ten and said, "Wilco has come up with 50% of a classic album and 50% of a merely decent one. Buy it for the moments you simply won’t hear anywhere else."[43] Will Hermes of Spin gave it a score of eight out of ten and called it "A near-perfect album by a band that seems, finally, to have found their identity."[36] Alternative Press gave it four stars out of five and said, "It's apparent it takes deft skill to sound this simple."[26] Tiny Mix Tapes also gave it four stars out of five and stated: "While the elders will rejoice [in] this sober, satisfied, and craftily subdued effort, the younglings of the bunch, with their abbreviated attention spans, iPod shuffles, and demand for instant gratification, will declare the album a boring and lethargic affair."[44] Jonathan Keefe of Slant Magazine likewise gave it four stars out of five and said that "Though it may not fit comfortably alongside any other albums in Wilco's catalogue, Sky Blue Sky is further confirmation that, even at their most retro, they're among contemporary pop music's most vital acts."[45] Likewise, Graeme Thomson of The Observer gave it four stars and said, "The closer you listen to the jazzy guitars, Beatles touches and easy, shuffling rhythms ... the more it transpires that Tweedy is simply allowing the songs sufficient room to speak up for themselves."[46]
 John Pareles of The New York Times gave the album a positive review and said, "The production is straightforward, but the song structures aren’t; that’s where Wilco’s idiosyncrasies still hide out."[47] Joan Anderman of The Boston Globe also gave it a positive review and said the band "hasn't forsaken its experimental streak, and the group uses it in the service of darkness -- or rather the threat of darkness."[48]
 However, not all publications praised the new style of Sky Blue Sky. Stylus Magazine editor Ian Cohen criticized the album's disregard for the "fourth wall", and expressed concern about its dissimilarities to Kicking Television: Live in Chicago.[49] Dorian Lynskey of The Guardian gave the album three stars out of five and said, "On its own terms, Sky Blue Sky succeeds: it's tender, poignant and sumptuously textured, occasionally jolted into fiery life by flaring guitar passages redolent of Neil Young or Television."[30] Now gave it a positive review and stated: "All those self-consciously avant bits of the two previous albums have been ditched along with Jeff Tweedy's laughable lyrical abstractions in favour of tuneful, direct songs that at least seem to carry some emotional weight."[26] Ted Grant of Playlouder gave the album two stars out of five and called it the "blandest and most creatively uninspired record of their career", finding that the album was leading to tame "dad-rock".[50] Pitchfork writer Rob Mitchum also used the "dad-rock" colloquialism, dismissing its straightforwardness and arguing "Tweedy merely ended up with the wrong personnel to articulate his mood here."[34] Mojo also gave the album three stars out of five, stating that "Many longtime listeners... are sure to be disappointed with the radio-friendly production and sheer innocuousness of [the] lyrics."[26] Andy Gill of Uncut gave the album three stars out of five and called it "a slight disappointment".[51] Billboard gave it an average review and stated: "On first listen, it might seem too derivative, even dull, but Jeff Tweedy's intricate vocal melodies and Nels Cline's ferocious guitar work keep things interesting."[26] Under the Radar gave it five stars out of ten and called it "a very professional but almost inconsequential set... flat and ultimately uninspired."[26] John Walshe of Hot Press gave it a mixed review and said the album was "just too 'nice'."[52]
 The lyrical content was considered by critics to be somewhat experimental, but more straightforward than previous Wilco albums.[28][39] Michael Metevier of PopMatters found the lyrics to be "some of the most affecting and least clumsy" of the band's career, though he worried that they might strike some Wilco fans as dull.[39] Rob Sheffield said that while he was unimpressed with the lyrics of other Wilco albums, he liked the songwriting on Sky Blue Sky.[35] However, Brandon Kreitler of Dusted Magazine felt that the lyrics seem like an insular Tweedy confessional, and Doug Freeman of The Austin Chronicle described the collaborative songwriting as yielding "fatalistic ambivalence" while giving the album two stars out of five.[53][54]
 The album received a nomination at the 50th Annual Grammy Awards for Best Rock Album.[55] It placed 12th in the 2008 Pazz & Jop Poll.[56] This album was #42 on Rolling Stone's list of the Top 50 Albums of 2007, and the song "Impossible Germany" was #71 on Rolling Stone's list of the 100 Best Songs of 2007.[57][58] WXPN named "Impossible Germany" as the #1 song of 2007 and named the album as a whole the #1 album of 2007.[59][60] Sky Blue Sky was named one of the ten best albums of the year by Billboard, Paste, Uncut, and The A.V. Club.[61] The album was placed at #97 on the Rolling Stone 100 Best Albums Of The 2000s list.
 
All tracks are written by Jeff Tweedy, except where noted. 
 Alternative rock folk rock alternative country 1 Production

1.1 Composition
1.2 Artwork

 1.1 Composition 1.2 Artwork 2 Marketing and promotion 3 Release and reception 4 Track listing 5 Personnel 6 References 7 Notes 8 External links Jeff Tweedy Nels Cline Tweedy Wilco Tweedy Mikael Jorgensen Tweedy Wilco Tweedy Wilco Tweedy Wilco Nels Cline – electric guitar, 12 string guitar, lap steel guitar Mikael Jorgensen – piano, organs, Wurlitzer Glenn Kotche – drums, percussion, glockenspiel Pat Sansone – organs, guitar, Chamberlin, Mellotron, Wurlitzer, harpsichord, piano, backing vocals John Stirratt – bass guitar, backing vocals Jeff Tweedy – vocals, guitar, graphic design Jim O'Rourke – feedback, percussion, acoustic guitar, string arrangements Karen Waltuch – viola, violin Lawrence Azerrad – graphic design TJ Doherty – recording Robert Ludwig – mastering Nathaniel Murphy – illustrations Frack Ockenfels – photography Manuel Presti – cover photograph Jim Scott – mixing Jason Tobias, Tom Gloady, Kevin Dean – assistant engineering Kot, Greg (2004). Wilco: Learning How to Die (1st ed.). New York City, NY: Broadway Books. ISBN 0-7679-1558-5..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} ^ Llewellyn, Kati (May 8, 2006). "Glenn Kotche Talks New Wilco Record". Pitchfork. Archived from the original on May 2, 2007. Last accessed June 9, 2007.
 ^ "Wilco reveals album title, release date". Paste. January 19, 2007. Last accessed November 12, 2013.
 ^ Cohen, Jonathan (January 18, 2007). "Wilco Soars into 'Blue Sky' in May". Billboard. Last accessed June 9, 2007.
 ^ a b A Parade of Inspiration on Wilco's Sky Blue Sky, NPR, Washington D.C., May 26, 2007. Last accessed June 10, 2007.
 ^ a b Cohen, Jonathan (April 13, 2007). "Wilco: In the Comfort Zone". Billboard. Archived from the original on April 3, 2013. Last accessed June 9, 2007.
 ^ Kandell, Steve (May 2, 2007). "The Main Attractions: Jeff Tweedy". Spin. Archived from the original on June 4, 2007. Last accessed June 10, 2007.
 ^ a b Fricke, David (April 5, 2007). "Spring Music Preview: Fifty Must-Hear Albums". Rolling Stone.
 ^ Mills, Fred (January 20, 2007). "New Wilco Album Sky Blue Sky Drops May 15". Harp. Archived from the original on September 26, 2007. Last accessed June 10, 2007.
 ^ a b c d Sky Blue Sky album notes, May 15, 2007. Nonesuch Records, 131388.
 ^ a b Kot, Greg (May 13, 2007). "Wilco Pares Down for Simpler, More Intimate Work". Chicago Tribune.
 ^ Kot 2004. p. 240–1
 ^ Collis, Clark. "The Trouble With Tweedy". Entertainment Weekly. Last accessed June 16, 2007.
 ^ Presti, Manuel. "Sky Chase – B156". wildlifephoto-presti.com. Archived from the original on September 28, 2007. Last accessed July 5, 2007.
 ^ "Wilco Official Website". wilcoworld.net. Archived from the original on March 5, 2007. Last accessed March 3, 2007.
 ^ Crock, Jason (May 7, 2007). "Interview: Wilco". Pitchfork. Archived from the original on June 8, 2007. Last accessed June 10, 2007.
 ^ Suarez, Jessica (March 5, 2007). "New Music: Wilco: "What Light" (MP3)". Pitchfork. Archived from the original on June 29, 2007. Last accessed June 10, 2007.
 ^ "Wilco Official Website". wilcoworld.net. Archived from the original on March 5, 2007. Last accessed June 10, 2007.
 ^ "Wilco: Sky Blue Sky". wilcoworld.net. Archived from the original on November 13, 2007. Last accessed November 14, 2007.
 ^ Volkswagen erroneously stated in its press release that it was the first licensing deal for the band.
 ^ a b "Wilco – News". wilcoworld.net. Last accessed June 10, 2007.
 ^ Welte, Jim (June 4, 2007). "VW Taps Wilco's Sky Blue Sky for Ads". Archived from the original on September 29, 2007. Last accessed June 10, 2007.
 ^ Cohen, Jonathan (June 5, 2007). "Wilco Takes a Spin with Volkswagen for TV Ads". MP3.com. Last accessed June 10, 2007.
 ^ Caro, Mark. "Does VW Deal Make Wilco a Sellout?". Chicago Tribune, retrieved May 15, 2012
 ^ "Show 4: 11.35pm, BBC2, Friday 25th May 2007". BBC. Archived from the original on May 30, 2007. Last accessed June 16, 2007.
 ^ "Sky Blue Sky Tour 2007". wilcoworld.net. Archived from the original on June 14, 2007. Last accessed June 10, 2007.
 ^ a b c d e f g "Reviews for Sky Blue Sky by Wilco". Metacritic. Retrieved June 2, 2013.
 ^ a b Deming, Mark. "Sky Blue Sky – Wilco". AllMusic. Retrieved June 2, 2013.
 ^ a b Murray, Noel (May 17, 2007). "Wilco: Sky Blue Sky". The A.V. Club. Retrieved June 2, 2013.
 ^ Nashawaty, Chris (May 18, 2007). "Sky Blue Sky". Entertainment Weekly. Retrieved June 2, 2013.
 ^ a b Lynskey, Dorian (May 10, 2007). "Wilco, Sky Blue Sky". The Guardian. Retrieved June 2, 2013.
 ^ a b Cromelin, Richard (May 13, 2007). "Wilco offers surprise that really is one". Los Angeles Times. Retrieved June 2, 2013.
 ^ Christgau, Robert (June 2007). "Consumer Guide". MSN Music. Retrieved June 16, 2009.
 ^ Sterry, Mike (May 23, 2007). "Wilco: Sky Blue Sky". NME. Archived from the original on June 5, 2011. Retrieved June 16, 2009.
 ^ a b Mitchum, Rob (May 14, 2007). "Wilco: Sky Blue Sky". Pitchfork. Retrieved April 14, 2012.
 ^ a b c Sheffield, Rob (May 14, 2007). "Sky Blue Sky". Rolling Stone. Archived from the original on October 21, 2007. Retrieved June 2, 2013.
 ^ a b Hermes, Will (June 2007). "Not Much. Just Chillin'". Spin. 23 (6): 89. Retrieved June 2, 2013.
 ^ Hasty, Katie (May 23, 2007). "Linkin Park Scores Year's Best Debut with 'Midnight'". Billboard. Retrieved June 9, 2007.
 ^ "Wilco – Sky Blue Sky – Music Charts". acharts.com. Retrieved June 9, 2007.
 ^ a b c Metivier, Michael (May 14, 2007). "Wilco: Sky Blue Sky". PopMatters. Retrieved June 2, 2013.
 ^ Dolan, Jon (June 2007). "Wilco: Sky Blue Sky". Blender. p. 103. Retrieved June 16, 2009.[dead link]
 ^ Stutz, Colin (June 19, 2007). "Wilco: Sky Blue Sky - Nonesuch". Filter. Archived from the original on June 24, 2007. Retrieved June 2, 2013.
 ^ Bradick, Andrew C. (May 15, 2007). "Album Review: Wilco - Sky Blue Sky". Prefix Magazine. Retrieved June 2, 2013.
 ^ Shulman, Alan (June 5, 2007). "Wilco: Sky Blue Sky - Music Review". No Ripcord. Retrieved June 2, 2013.
 ^ Chadwicked (May 14, 2007). "Wilco - Sky Blue Sky". Tiny Mix Tapes. Retrieved June 2, 2013.
 ^ Keefe, Jonathan (May 13, 2007). "Wilco: Sky Blue Sky". Slant Magazine. Retrieved June 2, 2013.
 ^ Thomson, Graeme (April 21, 2007). "Wilco, Sky Blue Sky". The Observer. Retrieved June 2, 2013.
 ^ Pareles, Jon (May 14, 2007). "New CDs (Wilco: 'Sky Blue Sky')". The New York Times. Retrieved June 2, 2013.
 ^ Anderman, Joan (May 15, 2007). "Wilco comes back to earthy". The Boston Globe. Retrieved June 2, 2013.
 ^ Cohen, Ian (May 14, 2007). "Wilco – Sky Blue Sky – Review". Stylus Magazine. Archived from the original on May 16, 2007.
 ^ Grant, Ted (June 6, 2007). "Sky Blue Sky (2007) Review". Playlouder. Archived from the original on May 4, 2002. Retrieved June 9, 2007.
 ^ Gill, Andy (June 2007). "Wilco - Sky Blue Sky". Uncut. p. 88. Retrieved June 2, 2013.
 ^ Walshe, John (May 25, 2007). "Sky Blue Sky". Hot Press. Retrieved June 2, 2013.
 ^ Kreitler, Brandon (June 14, 2007). "Dusted Reviews – Wilco – Sky Blue Sky". Dusted Magazine. Retrieved June 16, 2007.
 ^ Freeman, Doug (May 18, 2007). "Wilco: Sky Blue Sky (Nonesuch)". The Austin Chronicle. Retrieved June 2, 2013.
 ^ "50th Annual Grammy Awards Nomination List". Grammy.com. Archived from the original on December 8, 2007. Retrieved December 6, 2007.
 ^ "Village Voice > Winners: Albums / Singles". Archived from the original on January 26, 2008.
 ^ Robert Christgau, David Fricke, Christian Hoard, Rob Sheffield (December 17, 2007). "The Top 50 Albums of 2007" Rolling Stone. Retrieved December 20, 2007
 ^ No byline (December 11, 2007). "The 100 Best Songs of 2007". Rolling Stone. Retrieved December 21, 2007
 ^ "Year in Review (2007): Top 100 Songs". Xpn.org. Archived from the original on June 30, 2012. Retrieved August 16, 2011.
 ^ "Year in Review (2007): Top 50 Albums". Xpn.org. Archived from the original on July 25, 2011. Retrieved August 16, 2011.
 ^ "Metacritic: Best Albums of 2007". Metacritic. Archived from the original on December 28, 2007. Retrieved December 29, 2007.
 Sky Blue Sky at Metacritic v t e Jeff Tweedy John Stirratt Nels Cline Pat Sansone Mikael Jorgensen Glenn Kotche Leroy Bach Jay Bennett Ken Coomer Bob Egan Brian Henneman Max Johnston A.M. Being There Summerteeth Yankee Hotel Foxtrot A Ghost Is Born Sky Blue Sky Wilco (The Album) The Whole Love Star Wars Schmilco Ode to Joy Kicking Television: Live in Chicago Mermaid Avenue Mermaid Avenue Vol. II Mermaid Avenue Vol. III Mermaid Avenue: The Complete Sessions More Like the Moon Man in the Sand I Am Trying to Break Your Heart Wilco Live: Ashes of American Flags Discography The Autumn Defense Jim O'Rourke Billy Bragg Down with Wilco At the Organ Golden Smog Jeff Tweedy discography Loose Fur Sunken Treasure: Live in the Pacific Northwest Tweedy Uncle Tupelo The Wilco Book Wilco: Learning How to Die Record Club  dBpm Records MusicBrainz release group: 1aa7ce82-1b74-3f83-b426-531edf4d4284 2007 albums Nonesuch Records albums Wilco albums Albums produced by Jeff Tweedy All articles with dead external links Articles with dead external links from May 2016 Use mdy dates from April 2011 Articles with short description Articles with hAudio microformats Featured articles Wikipedia articles with MusicBrainz release group identifiers Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Español Français Galego Hrvatski Italiano Nederlands Norsk Svenska  This page was last edited on 6 November 2019, at 13:53 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  I got nervous about the technology on Yankee Hotel Foxtrot. If you need a certain amp or pedal to make a song what it is, it isn't a song. Sky Blue Sky Sky Blue Sky Total length: 50:56 ^ ^ ^ a b a b ^ a b ^ a b c d a b ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ a b c d e f g a b a b ^ a b a b ^ ^ a b a b c a b 23 ^ ^ a b c ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ Jeff Tweedy John Stirratt Nels Cline Pat Sansone Mikael Jorgensen Glenn Kotche Wilco Additional musicians Production and design  May 15, 2007 (2007-05-15) November 2006 – January 2007 The Wilco Loft, Irving Park, Chicago, Illinois, United States Alternative rockfolk rockalternative country 50:56 Nonesuch Wilco 


A Ghost Is Born(2004)

Sky Blue Sky(2007)

Wilco (The Album)(2009)
 A Ghost Is Born(2004)
 Sky Blue Sky(2007)
 Wilco (The Album)(2009)
  
"What Light"

Sample of "What Light", the eleventh track from Sky Blue Sky which was offered as a free download from the official Wilco website. The song exemplifies the straightforward melody, contrasting Yankee Hotel Foxtrot and A Ghost Is Born. Problems playing this file? See media help. Metacritic 73/100[26] AllMusic [27] The A.V. Club A−[28] Entertainment Weekly A−[29] The Guardian [30] Los Angeles Times [31] MSN Music (Consumer Guide) B+[32] NME 7/10[33] Pitchfork 5.2/10[34] Rolling Stone [35] Spin [36] 1. "Either Way"   3:05 2. "You Are My Face" Jeff TweedyNels Cline 4:38 3. "Impossible Germany" TweedyWilco 5:57 4. "Sky Blue Sky"   3:23 5. "Side with the Seeds" TweedyMikael Jorgensen 4:15 6. "Shake It Off"   5:40 7. "Please Be Patient with Me"   3:17 8. "Hate It Here" TweedyWilco 4:31 9. "Leave Me (Like You Found Me)"   4:09 10. "Walken" TweedyWilco 4:26 11. "What Light"   3:35 12. "On and On and On" TweedyWilco 4:00 Total length: 50:56 
Jeff Tweedy
John Stirratt
Nels Cline
Pat Sansone
Mikael Jorgensen
Glenn Kotche
Leroy Bach
Jay Bennett
Ken Coomer
Bob Egan
Brian Henneman
Max Johnston
 
A.M.
Being There
Summerteeth
Yankee Hotel Foxtrot
A Ghost Is Born
Sky Blue Sky
Wilco (The Album)
The Whole Love
Star Wars
Schmilco
Ode to Joy
 
Kicking Television: Live in Chicago
 
Mermaid Avenue
Mermaid Avenue Vol. II
Mermaid Avenue Vol. III
Mermaid Avenue: The Complete Sessions
 
More Like the Moon
 Alpha Mike Foxtrot: Rare Tracks 1994 - 2014 
Man in the Sand
I Am Trying to Break Your Heart
Wilco Live: Ashes of American Flags
 
Discography
The Autumn Defense
Jim O'Rourke
Billy Bragg
Down with Wilco
At the Organ
Golden Smog
Jeff Tweedy discography
Loose Fur
Sunken Treasure: Live in the Pacific Northwest
Tweedy
Uncle Tupelo
The Wilco Book
Wilco: Learning How to Die
Record Club
 dBpm Records
 
MusicBrainz release group: 1aa7ce82-1b74-3f83-b426-531edf4d4284
 