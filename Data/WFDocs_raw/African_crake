
 Ortygometra egregiaCrecopsis egregiaPorzana egregia
 The African crake (Crex egregia) is a small to medium-size ground-living bird in the rail family, found in most of central to southern Africa. It is seasonally common in most of its range other than the rainforests and areas that have low annual rainfall. This crake is a partial migrant, moving away from the equator as soon as the rains provide sufficient grass cover to allow it to breed elsewhere. There have been a few records of vagrant birds reaching Atlantic islands. This species nests in a wide variety of grassland types, and agricultural land with tall crops may also be used.
 A smallish crake, the African crake has brown-streaked blackish upperparts, bluish-grey underparts and black-and-white barring on the flanks and belly. It has a stubby red bill, red eyes, and a white line from the bill to above the eye. It is smaller than its closest relative, the corn crake; that species is also lighter-plumaged, and has an eye stripe. The African crake has a range of calls, the most characteristic being a series of rapid grating krrr notes. It is active during the day, and is territorial on both the breeding and non-breeding grounds; the male has a threat display, and may fight at territory boundaries. The nest is a shallow cup of grass leaves built in a depression under a grass tussock or small bush. The 3–11 eggs start hatching after about 14 days, and the black, downy precocial chicks fledge after four to five weeks. The African crake feeds on a wide range of invertebrates, along with some small frogs and fish, and plant material, especially grass seeds. It may itself be eaten by large birds of prey; snakes; or mammals, including humans, and can host parasites. Although it may be displaced temporarily by the burning of grassland, or permanently by agriculture, wetland drainage or urbanisation, its large range and population mean that it is not considered to be threatened.
 The rails are a bird family comprising nearly 150 species. Although the origins of the group are lost in antiquity, the largest number of species and the most primitive forms are found in the Old World, suggesting that this family originated there. The taxonomy of the small crakes is complicated, but the closest relative of the African crake is the corn crake, C. crex, which breeds in Europe and Asia, but winters in Africa. The African crake was first described as Ortygometra egregia by Wilhelm Peters in 1854 from a specimen collected in Mozambique,[2] but the genus name failed to become established. For some time it was placed as the sole member of the genus Crecopsis[3] but subsequently moved to Crex, created for this species by German naturalist and ornithologist Johann Matthäus Bechstein in 1803.[4] Richard Bowdler Sharpe considered that the African bird differed sufficiently from the corn crake to have its own genus Crecopsis, and later authors sometimes placed it in Porzana, based on a resemblance to the ash-throated crake, P. albicollis. Structural differences rule out Porzana, and the placement in Crex is now the most common and best-supported treatment.[5][6] Phylogeny and morphology confirm that the Porzana crakes are the closest relatives of the genus Crex.[7] The genus name is onomatopoeic, referring to the repetitive grating call of the corn crake,[8] and the species name egregia derives from Latin egregius, "outstanding, prominent".[9]
 The African crake is a smallish crake, 20–23 cm (7.9–9.1 in) long with a 40–42 cm (16–17 in) wingspan. The male has blackish upperparts streaked with olive-brown, apart from the nape and hindneck which are plain pale brown; there is a white streak from the base of the bill to above the eye. The sides of the head, foreneck, throat and breast are bluish-grey, the flight feathers are dark brown, and the flanks and sides of the belly are barred black and white. The eye is red, the bill is reddish, and the legs and feet are light brown or grey. The sexes are similar in appearance, although the female is slightly smaller and duller than the male, with a less contrasting head pattern. Immature birds have darker and duller upperparts than the adult, a dark bill, grey eye, and less barring on the underparts. There are no subspecific or other geographical variations in plumage. This crake has a complete moult after breeding, mainly prior to migration.[2] Although this species occurs in fairly open habitats, it lacks the pure white undertail used for signalling in open water or gregarious species like the coots and moorhens.[10]
 The African crake is smaller than the corn crake, which also has darker upperparts, a plain grey face and different underparts barring pattern. In flight, the African species has shorter, blunter wings with a less prominent white leading edge, and deeper wingbeats than its relative. Other sympatric crakes are smaller with white markings on the upperparts, different underparts patterns and a shorter bill. The African rail has dark brown upperparts, a long red bill and red legs and feet.[2]
 Like other rails, this species has a wide range of vocalisations. The male's territorial and advertising call is a series of rapid grating krrr notes repeated two or three times a second for several minutes. It is given most often in the breeding season, usually early or late in the day, but sometimes continues after dark or starts before dawn. The male stands upright with his neck extended when advertising, but will also call when chasing intruders on the ground or in flight. Both sexes give a sharp, loud kip call as an alarm or during territorial interactions, adapting a similar pose as for the advertising call. Once breeding starts, the birds become much quieter, but territorial birds commence the kip call again during the non-breeding season, especially when there is a high density of African crakes in the area. A wheezy kraaa is associated with threat displays and copulation; imitation of this call by a human can bring a rail to within 10 m (33 ft). Newly hatched chicks make a soft wheeeez call, and older chicks cheep.[2]
 The rasping advertising call is readily distinguished from the hwitt-hwitt-hwitt of spotted crake, the monotonous clockwork tak-tak-tak-tak-tak of striped crake, or the quick-quick of Baillon's crake.[11] The corn crake is silent in Africa.[12]
 The African crake occurs throughout sub-Saharan Africa from Senegal east to Kenya, and south to KwaZulu-Natal, South Africa, except in arid areas of south and southwest Africa where the annual summer rainfall is less than 300 mm (12 in). It is widespread and locally common in most of its range, apart from the rainforests and the drier regions. Nearly all the South Africa population of about 8,000 birds occur in KwaZulu-Natal and the former Transvaal Province, and much good habitat is protected in the Kruger National Park and iSimangaliso Wetland Park. This crake is only a vagrant to southern Mauritania, southwest Niger, Lesotho, South Africa's northern and eastern Cape Province and North West Province,[2][13] and southern Botswana.[14] Further afield, it is rare on Bioko Island (Equatorial Guinea),[15] and there have been two records each for Sao Tome and Tenerife, the Canary Islands birds being the first records for the Western Palaeartic.[16][17] Holocene remains from North Africa suggest that the species was more widespread when the climate was wetter in what is now the Sahara.[18][19]
 This crake is a partial migrant, but although it is less skulking than many of its relatives, its movements are complex and poorly studied; the distribution map is therefore largely hypothetical. It is mainly a wet-season breeder, and many birds move away from the equator as soon as the rains provide sufficient grass cover to allow them to breed elsewhere. Southward movement is mainly from November to April, the return north beginning when burning or drought reduces the grass cover again. This species is present throughout the year in some West African countries, and in equatorial regions, but even in those areas numbers vary seasonally due to local movements; north-south migration has been noted within countries including Nigeria, Senegal, The Gambia, Ivory Coast and Cameroon.[2] Migration takes place at night and involves small groups of up to eight birds;[1] It may be one or two months after the rains begin before the grass is sufficiently high for breeding birds to arrive. Even in southern Africa, some birds may stay after breeding if enough usable habitat remains.[14]
 The habitat is predominantly grassland, ranging from wetland edges and seasonal marshes to savanna, lightly wooded dry grassland, and grassy forest clearings. The crake also frequents corn, rice and cotton fields, derelict farmland and sugarcane plantations close to water. A wide range of grass species are used, with a preferred height of 0.3–1 m (0.98–3.28 ft) tall but vegetation is acceptable up to 2 m (6.6 ft) tall. It normally prefers moister and shorter grassland habitats than does the corn crake, and its breeding territories often contain or are close to thickets or termite mounds. It occurs from sea level to 2,000 m (6,600 ft) but is rare in the higher altitude grasslands. Its grassland habitat is frequently burned in the dry season, forcing the birds to move elsewhere.[2] In an East African study, the average area occupied by one bird was 2.6 hectares (6.4 acres) when breeding, and 1.97 to 2.73 ha (4.9 to 6.7 acres) at other times.[20] The highest densities occur in lush or moist grassland such as the Okavango Delta.[14]
 The African crake is active during the day, especially at dusk, during light rain, or after heavier rain. It is less skulking and easier to flush from cover than other crakes, and is often seen at the edges of roads and tracks. An observer in a vehicle can approach to within 1 m (3.3 ft). When a bird is flushed it normally flies less than 50 m (160 ft), but new arrivals may occasionally fly twice as far. A flushed crake will frequently land in a wet area or behind a thicket, and crouch on landing. In short grass, it can escape from a dog using its speed and manoeuvrability, running with the body held almost horizontal. It may roost in a depression near grass tussock and it will bathe in puddles.[2]
 The African crake is territorial on both the breeding and non-breeding grounds; the male threat display involves the bird standing upright and spreading the feathers of the flanks and belly like a fan to show the barred underparts. He may march towards the intruder, or walk side by side with another displaying male. The female may accompany the male, but with feathers less widely fanned. Fighting at territorial boundaries involves the male birds jumping at each other and pecking. Paired females will attack other females in the territory, especially if the male has shown an interest in them.[2]
 Breeding behavior commences with a courtship chase with the female running in a crouch, pursued by the male, who adopts a more upright stance and has his neck outstretched. The female may stop and lower her head and tail to allow copulation; this takes just a few seconds, but may be repeated several times in an hour. The nest is a shallow cup of grass leaves, sometimes with a loose canopy, built in a depression and hidden under a grass tussock or small bush; it may be on dry ground or slightly raised above standing water, or occasionally floating. The nest is about 20 cm (7.9 in) across with the internal cup 2–5 cm (0.79–1.97 in) deep, and 11–12 cm (4.3–4.7 in) wide. The clutch size is from 3 to 11 pink-coloured[21] eggs; the first is often laid when the nest is little more than a pad of grass, and a further egg is laid on each subsequent day. Both sexes incubate, and the eggs start hatching after about 14 days; all hatch with 48 hours despite the extended laying period. The black, downy precocial chicks soon leave the nest but are fed and protected by the parents. Fledging occurs after four to five weeks, and the young can fly before they are fully grown. It is not known whether a second brood is raised.[2]
 The African crake feeds on invertebrates including earthworms, gastropods, molluscs and the adults and larvae of insects, especially termites, ants, beetles and grasshoppers. Vertebrate prey such as small frogs or fish may also be taken. Plant material is eaten, especially grass seeds, but also green shoots, leaves and other seeds. The crake searches for food both within vegetation and in the open, picking insects and seeds from the ground, turning over leaf litter, or digging with its bill in soft or very dry ground. It will chase faster moving prey, reach up to take food from plants, and wade to pluck food items from the water.[2] Crop plants such as rice, maize and peas may sometimes be eaten, but this bird is not an agricultural pest species.[22][23] It forages singly, in pairs or in family groups, sometimes in association with other grassland birds such as great snipes, blue quails and corn crakes.[2] Chicks are fed mainly on animal food. As with other rails, grit is swallowed to help break up food in the stomach.[24]
 Predators include the leopard,[25] serval, cats, the black-headed heron, dark chanting goshawk, African hawk-eagle and Wahlberg's eagle.[2] In South Africa, newly hatched chicks were taken by a boomslang.[26] If surprised, an African crake will leap vertically into the air before running away, a tactic believed to help it to evade snakes or terrestrial mammals.[27]
 Parasites of this species include ticks of the family Ixodidae,[28][29] and a feather mite, Metanalges elongatus, of the subspecies M. e. curtus. The nominate form of the mite occurs thousands of kilometres away in New Caledonia.[30]
 The African crake has a huge breeding range estimated at 11,700,000 km2 (4,500,000 mi2). Its population is unknown, but it is common in most of its range, and its numbers appear to be stable. It is therefore classed as Least Concern on the IUCN Red List.[1] Overgrazing, agriculture and the loss of wetland and moist grassland have reduced the availability of suitable habitat in many areas, such as some parts of the southern KwaZulu-Natal coast which have been urbanised or planted with sugarcane. In other areas, grassland may have increased locally in recent years as woodland is cleared. This crake is considered to be good eating, and is killed for food in some regions. Despite these adverse factors, it appears to be under no real threat.[2]
 Although most rails in the Old World are covered by the Agreement on the Conservation of African-Eurasian Migratory Waterbirds (AEWA), the African crake is not listed even in Kenya, where it is considered "near-threatened". Like its relative, the corn crake, it is too terrestrial to be classed as a wetland species.[31]
 
 1 Taxonomy 2 Description

2.1 Voice

 2.1 Voice 3 Distribution and habitat 4 Behaviour

4.1 Breeding
4.2 Feeding

 4.1 Breeding 4.2 Feeding 5 Predators and parasites 6 Status 7 References 8 Cited texts 9 External links ^ a b c "BirdLife International Species factsheet:  Crecopsis egregia ". BirdLife International. Retrieved 15 May 2011..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c d e f g h i j k l m n Taylor & van Perlo (2000) pp. 316–320
 ^ Peters, James Lee (1934). Check-list of birds of the World. Volume 2. Cambridge, Massachusetts: Harvard University Press. p. 181.
 ^ Bechstein, Johann Matthäus (1803). Ornithologisches Taschenbuch von und für Deutschland oder kurze Beschreibung aller Vogel Deutschlands, vol 2 (in German). Leipzig: Richter. p. 336.
 ^ Taylor & van Perlo (2000) p. 30
 ^ Livezey (1998) p. 2098
 ^ Livezey (1998) p. 2134
 ^ Smith, John Maynard; Harper, David (2003). Animal Signals (Oxford Series in Ecology and Evolution). Oxford: Oxford University Press. p. 11. ISBN 0-19-852685-7.
 ^ Brookes, Ian (editor-in-chief) (2006). The Chambers Dictionary, ninth edition. Edinburgh: Chambers. p. 477. ISBN 0-550-10185-3.
 ^ Stang, Alexandra T; McRae, Susan B (2009). "Why some rails have white tails: the evolution of white undertail plumage and anti-predator signaling". Evolutionary Ecology. 23 (6): 943–961. doi:10.1007/s10682-008-9283-z.
 ^ Newman, Kenneth (2002). Newman's Birds of Southern Africa. Cape Town: Struik. pp. 120–122. ISBN 1-86872-735-1.
 ^ Serle, William; Morel, Gérard J (1977). A Field Guide to the Birds of West Africa. London: Collins. p. 60. ISBN 0-00-219204-7.
 ^ Hudson, Adrian; Bouwman, Henk (2006). "New records of 45 bird species in the desert margins area of the North-West Province, South Africa". Koedoe. 49 (1): 91–98. doi:10.4102/koedoe.v49i1.102.
 ^ a b c Taylor P B. "African Crake" (PDF). Southern African Bird Atlas Project. Animal Demography Unit (Department of Zoology, University of Cape Town), BirdLife South Africa, South African National Biodiversity Institute. Retrieved 26 May 2011
 ^ Larison, Brenda; Smith, Thomas B; Milá, Borja; Stauffer, Donald; Nguema, José (1999) " Bird and Mammal Surveys of Rio Muni" pp. 9–57 in Larison, Brenda;  et al. (1999). "Biotic Surveys of Bioko and Rio Muni, Equatorial Guinea" (PDF). Cite journal requires |journal= (help)
 ^ de Juana, Eduardo; El Comité de Rarezas de la Sociedad Española de Ornitología (2003). "Observaciones de aves raras en España, 2001" (PDF). Ardeola (in Spanish). 50 (1): 123–149.
 ^ Amadon, Dean (1953). "Avian systematics and evolution in the Gulf of Guinea: The J. G. Corrleia collection" (PDF). Bulletin of the American Museum of Natural History. 100 (3): 393–452.
 ^ Peter, Joris; Pöllath, Nadja (2002) "Holocene faunas of the Eastern Sahara: zoogeographical and palaeoecological aspects" pp. 34–51 in Albarella, Umberto; Dobney, Keith; Rowley-Conwy, Peter (eds) (2002). "Proceedings of the 9th Conference of the International Council of Archaeozoology, Durham, August 2002" (PDF). Cite journal requires |journal= (help)CS1 maint: extra text: authors list (link)
 ^ Gautier, Achiel (1998). "Animals and people in the Holocene of North Africa". ArchaeoZoologia. 9 (1/2): 1–181.
 ^ Taylor, P B (1985). "Field studies of the African Crake Crex egregia in Zambia and Kenya". Ostrich. 56 (1, 3): 170–185. doi:10.1080/00306525.1985.9639587.
 ^ Sclater, W L (1906). The Birds of South Africa. Volume 4. London: R H  Porter. pp. 248–249.
 ^ Manikowski, S (1984). "Birds injurious to crops in West Africa". International Journal of Pest Management. 30 (4): 379–387. doi:10.1080/09670878409370914.
 ^ Funmilayo, O; Akande, M (1977). "Vertebrate pests of rice in southwestern Nigeria". International Journal of Pest Management. 23 (1): 38–48. doi:10.1080/09670877709412395.
 ^ Taylor & van Perlo (2000) pp. 39–41
 ^ Hill, R A (2001). "Leopard cub kills crake". CCA Ecological Journal. 3: 63.
 ^ Haagner, G V; Reynolds, D S (1988). "Notes on the nesting of the African Crake at Manyeleti Game Reserve, eastern Transvaal". Ostrich. 59: 45. doi:10.1080/00306525.1988.9633925.
 ^ Taylor & van Perlo (2000) p. 44
 ^ Elbl, Alena; Anastos, George (1966). Ixodid ticks (Acarina, Ixodidae) of Central Africa, Volume 4. Tervuren, Belgium: Musée royal de l'Afrique centrale. p. 58.
 ^ Zumpt, Fritz (1958). "A preliminary survey of the distribution and host-specificity of ticks (Ixodoidea) in the Bechuanaland Protectorate". Bulletin of Entomological Research. 49 (2): 201–223. doi:10.1017/S0007485300053554.
 ^ Zumpt, Fritz (1961). The Arthropod Parasites of Vertebrates in Africa South of the Sahara (Ethiopian Region), Volume 1: Chelicerata. Johannesburg: South African Institute for Medical Research. pp. 200–201.
 ^ Ng'weno, Fleur; Matiku, Paul; Mwangi, Solomon (eds) (1999). Kenya and the African-Eurasian Waterbird Agreement (AEWA). Nairobi: NatureKenya. pp. 16–17. Archived from the original on 2014-09-03.CS1 maint: extra text: authors list (link)
 Livezey, Bradley C (1998). "A phylogenetic analysis of the Gruiformes (Aves) based on morphological characters, with an emphasis on the rails (Rallidae)". Philosophical Transactions of the Royal Society of London. 353 (378): 2077–2151. doi:10.1098/rstb.1998.0353. PMC 1692427. Taylor, Barry; van Perlo, Berl (2000). Rails. Robertsbridge, Sussex: Pica. ISBN 1-873403-59-3. Videos, photos and sound recordings at the Internet Bird Collection African Crake. The Atlas of Southern African Birds. BirdLife International 2012. Crex egregia. The IUCN Red List of Threatened Species. Version 2014.3. Downloaded on 9 April 2015. v t e Corn crake African crake  Book:Crex  Portal:Birds v t e Outline Bird anatomy Flight Eggs Feathers Plumage Beak Vision Dactyly Preen gland Singing Intelligence Migration Sexual selection Lek mating Seabird breeding Incubation Brood parasites Nesting Hybrids Origin of birds
Theropoda
dinosaurs Theropoda dinosaurs Origin of flight Evolution of birds Darwin's finches Seabirds Archaeopteryx Omnivoropterygiformes Confuciusornithiformes Enantiornithes Chaoyangiiformes Patagopterygiformes Ambiortiformes Songlingornithiformes Gansuiformes Ichthyornithiformes Hesperornithes Lithornithiformes Dinornithiformes Aepyornithiformes Gastornithiformes Ringing Ornithology Ornithomancy Bird collections Birdwatching Bird feeding Conservation Aviculture Waterfowl hunting Cockfighting Pigeon racing Falconry Pheasantry Egg collecting Families and orders Genera Glossary of bird terms List by population Lists by region Recently extinct birds Late Quaternary prehistoric birds Notable birds
Individuals
Fictional Individuals Fictional Struthioniformes (ostriches) Rheiformes (rheas) Tinamiformes (tinamous) Apterygiformes (kiwis) Casuariiformes (emus and cassowaries) Anatinae
Aythyini
Mergini
Oxyurini Aythyini Mergini Oxyurini Anserinae
swans
true geese swans true geese Dendrocygninae Stictonettinae Tadorninae Anhima Chauna Anatalavis Anseranas Cracinae Oreophasinae Penelopinae Aepypodius Alectura Eulipoa Leipoa Macrocephalon Megapodius Talegalla Acryllium Agelastes Guttera Numida Callipepla Colinus Cyrtonyx Dactylortyx Dendrortyx Odontophorus Oreortyx Philortyx Rhynchortyx Meleagridinae (turkeys) Perdicinae Phasianinae (pheasants and relatives) Tetraoninae Columbiformes (doves and pigeons) Mesitornithiformes (mesites) Pterocliformes (sandgrouses) Phoenicopteriformes (flamingos) Podicipediformes (grebes) Cuculiformes (cuckoos) Musophagiformes (turacos) Otidiformes (bustards) Caprimulgiformes (nightjars and relatives) Steatornithiformes Podargiformes Apodiformes (swifts and hummingbirds) Opisthocomiformes (hoatzin) Charadriiformes (gulls and relatives) Gruiformes (cranes and relatives) Phaethontiformes (tropicbirds) Eurypygiformes (kagu and sunbittern) Gaviiformes (loons or divers) Sphenisciformes (penguins) Procellariiformes (albatrosses and petrels) Ciconiiformes (storks) Suliformes (cormorants and relatives) Pelecaniformes (pelicans and relatives) Cariamiformes (seriemas and relatives) Falconiformes (falcons and relatives) Psittaciformes (parrots) Passeriformes (perching birds) Cathartiformes (New World vultures and condors) Accipitriformes (eagles and hawks) Strigiformes (owls) Coliiformes (mousebirds) Trogoniformes (trogons and quetzals) Leptosomiformes (cuckoo roller) Bucerotiformes (hornbills and hoopoes) Coraciiformes (kingfishers and rollers) Piciformes (woodpeckers and relatives) Category  Commons  Portal  WikiProject Birds portal Animals portal Biology portal Africa portal Wikidata: Q1267654 Wikispecies: Crex egregia ADW: Crex_egregia Avibase: 09A0F2EBC063CF30 BirdLife: 22692539 eBird: afrcra1 EoL: 335802 GBIF: 5228155 IRMNG: 10199806 ITIS: 176278 IUCN: 22692539 uBio: 276228 IUCN Red List least concern species Crex Birds of Africa Birds of Sub-Saharan Africa Birds described in 1854 CS1 German-language sources (de) CS1 errors: missing periodical CS1 Spanish-language sources (es) CS1 maint: extra text: authors list Articles with short description Articles with 'species' microformats Featured articles Taxonbars without secondary Wikidata taxon IDs Taxonbars with automatically added original combinations Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Wikispecies Create a book Download as PDF Printable version Afrikaans العربية Asturianu বাংলা Български Brezhoneg Català Cebuano Cymraeg Deutsch Diné bizaad Ελληνικά Español Esperanto Euskara فارسی Français Italiano עברית Livvinkarjala Magyar Nederlands 日本語 Português Русский Српски / srpski Suomi Svenska தமிழ் Tiếng Việt Winaray  This page was last edited on 12 September 2019, at 13:49 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  C. egregia Crex egregia African crake a b c a b c d e f g h i j k l m n ^ ^ ^ ^ ^ ^ ^ ^ 23 ^ ^ ^ 49 a b c ^ ^ 50 ^ 100 ^ ^ 9 ^ 56 ^ ^ 30 ^ 23 ^ ^ 3 ^ 59 ^ ^ ^ 49 ^ ^ 353 Crex egregia Crex egregia  Book:Crex  Portal:Birds Category Commons Portal WikiProject 
 On the Zaagkuildrift Road, South Africa
 Least Concern (IUCN 3.1)[1]
 Kingdom:
 Animalia
 Phylum:
 Chordata
 Class:
 Aves
 Order:
 Gruiformes
 Family:
 Rallidae
 Genus:
 CrexBechstein, 1803
 Species:
 C. egregia
 Crex egregia(Peters, 1854)
 
    Breeding summer visitor    Resident year-round(ranges are very approximate)
 
Ortygometra egregiaCrecopsis egregiaPorzana egregia

  Wikimedia Commons has media related to Crex egregia.  Wikispecies has information related to Crex egregia 
Corn crake
African crake
 
 Book:Crex
 Portal:Birds
 
Outline
 
Bird anatomy
Flight
Eggs
Feathers
Plumage
Beak
Vision
Dactyly
Preen gland
 
Singing
Intelligence
Migration
Sexual selection
Lek mating
Seabird breeding
Incubation
Brood parasites
Nesting
Hybrids
 
Origin of birds
Theropoda
dinosaurs
Origin of flight
Evolution of birds
Darwin's finches
Seabirds
 
Archaeopteryx
Omnivoropterygiformes
Confuciusornithiformes
Enantiornithes
Chaoyangiiformes
Patagopterygiformes
Ambiortiformes
Songlingornithiformes
Gansuiformes
Ichthyornithiformes
Hesperornithes
Lithornithiformes
Dinornithiformes
Aepyornithiformes
Gastornithiformes
 
Ringing
Ornithology
Ornithomancy
Bird collections
Birdwatching
Bird feeding
Conservation
Aviculture
Waterfowl hunting
Cockfighting
Pigeon racing
Falconry
Pheasantry
Egg collecting
 
Families and orders
Genera
Glossary of bird terms
List by population
Lists by region
Recently extinct birds
Late Quaternary prehistoric birds
Notable birds
Individuals
Fictional
 Palaeognathae
Struthioniformes (ostriches)
Rheiformes (rheas)
Tinamiformes (tinamous)
Apterygiformes (kiwis)
Casuariiformes (emus and cassowaries)
NeognathaeGalloanserae  (fowls)Anseriformes  (waterfowls)Anatidae  (ducks)
Anatinae
Aythyini
Mergini
Oxyurini
Anserinae
swans
true geese
Dendrocygninae
Stictonettinae
Tadorninae
Anhimidae
Anhima
Chauna
Anseranatidae
Anatalavis
Anseranas
Galliformes  (landfowls-  gamebirds)Cracidae
Cracinae
Oreophasinae
Penelopinae
Megapodidae
Aepypodius
Alectura
Eulipoa
Leipoa
Macrocephalon
Megapodius
Talegalla
Numididae
Acryllium
Agelastes
Guttera
Numida
Odontophoridae
Callipepla
Colinus
Cyrtonyx
Dactylortyx
Dendrortyx
Odontophorus
Oreortyx
Philortyx
Rhynchortyx
Phasianidae
Meleagridinae (turkeys)
Perdicinae
Phasianinae (pheasants and relatives)
Tetraoninae
NeoavesColumbeaColumbimorphae
Columbiformes (doves and pigeons)
Mesitornithiformes (mesites)
Pterocliformes (sandgrouses)
Mirandornithes
Phoenicopteriformes (flamingos)
Podicipediformes (grebes)
PassereaOtidimorphae
Cuculiformes (cuckoos)
Musophagiformes (turacos)
Otidiformes (bustards)
Strisores
Caprimulgiformes (nightjars and relatives)
Steatornithiformes
Podargiformes
Apodiformes (swifts and hummingbirds)
Opisthocomiformes
Opisthocomiformes (hoatzin)
Cursorimorphae
Charadriiformes (gulls and relatives)
Gruiformes (cranes and relatives)
Phaethontimorphae
Phaethontiformes (tropicbirds)
Eurypygiformes (kagu and sunbittern)
Aequornithes
Gaviiformes (loons or divers)
Sphenisciformes (penguins)
Procellariiformes (albatrosses and petrels)
Ciconiiformes (storks)
Suliformes (cormorants and relatives)
Pelecaniformes (pelicans and relatives)
Australaves
Cariamiformes (seriemas and relatives)
Falconiformes (falcons and relatives)
Psittaciformes (parrots)
Passeriformes (perching birds)
Afroaves
Cathartiformes (New World vultures and condors)
Accipitriformes (eagles and hawks)
Strigiformes (owls)
Coliiformes (mousebirds)
Trogoniformes (trogons and quetzals)
Leptosomiformes (cuckoo roller)
Bucerotiformes (hornbills and hoopoes)
Coraciiformes (kingfishers and rollers)
Piciformes (woodpeckers and relatives)
 
Struthioniformes (ostriches)
Rheiformes (rheas)
Tinamiformes (tinamous)
Apterygiformes (kiwis)
Casuariiformes (emus and cassowaries)
 Galloanserae  (fowls)Anseriformes  (waterfowls)Anatidae  (ducks)
Anatinae
Aythyini
Mergini
Oxyurini
Anserinae
swans
true geese
Dendrocygninae
Stictonettinae
Tadorninae
Anhimidae
Anhima
Chauna
Anseranatidae
Anatalavis
Anseranas
Galliformes  (landfowls-  gamebirds)Cracidae
Cracinae
Oreophasinae
Penelopinae
Megapodidae
Aepypodius
Alectura
Eulipoa
Leipoa
Macrocephalon
Megapodius
Talegalla
Numididae
Acryllium
Agelastes
Guttera
Numida
Odontophoridae
Callipepla
Colinus
Cyrtonyx
Dactylortyx
Dendrortyx
Odontophorus
Oreortyx
Philortyx
Rhynchortyx
Phasianidae
Meleagridinae (turkeys)
Perdicinae
Phasianinae (pheasants and relatives)
Tetraoninae
NeoavesColumbeaColumbimorphae
Columbiformes (doves and pigeons)
Mesitornithiformes (mesites)
Pterocliformes (sandgrouses)
Mirandornithes
Phoenicopteriformes (flamingos)
Podicipediformes (grebes)
PassereaOtidimorphae
Cuculiformes (cuckoos)
Musophagiformes (turacos)
Otidiformes (bustards)
Strisores
Caprimulgiformes (nightjars and relatives)
Steatornithiformes
Podargiformes
Apodiformes (swifts and hummingbirds)
Opisthocomiformes
Opisthocomiformes (hoatzin)
Cursorimorphae
Charadriiformes (gulls and relatives)
Gruiformes (cranes and relatives)
Phaethontimorphae
Phaethontiformes (tropicbirds)
Eurypygiformes (kagu and sunbittern)
Aequornithes
Gaviiformes (loons or divers)
Sphenisciformes (penguins)
Procellariiformes (albatrosses and petrels)
Ciconiiformes (storks)
Suliformes (cormorants and relatives)
Pelecaniformes (pelicans and relatives)
Australaves
Cariamiformes (seriemas and relatives)
Falconiformes (falcons and relatives)
Psittaciformes (parrots)
Passeriformes (perching birds)
Afroaves
Cathartiformes (New World vultures and condors)
Accipitriformes (eagles and hawks)
Strigiformes (owls)
Coliiformes (mousebirds)
Trogoniformes (trogons and quetzals)
Leptosomiformes (cuckoo roller)
Bucerotiformes (hornbills and hoopoes)
Coraciiformes (kingfishers and rollers)
Piciformes (woodpeckers and relatives)
 Anseriformes  (waterfowls)Anatidae  (ducks)
Anatinae
Aythyini
Mergini
Oxyurini
Anserinae
swans
true geese
Dendrocygninae
Stictonettinae
Tadorninae
Anhimidae
Anhima
Chauna
Anseranatidae
Anatalavis
Anseranas
Galliformes  (landfowls-  gamebirds)Cracidae
Cracinae
Oreophasinae
Penelopinae
Megapodidae
Aepypodius
Alectura
Eulipoa
Leipoa
Macrocephalon
Megapodius
Talegalla
Numididae
Acryllium
Agelastes
Guttera
Numida
Odontophoridae
Callipepla
Colinus
Cyrtonyx
Dactylortyx
Dendrortyx
Odontophorus
Oreortyx
Philortyx
Rhynchortyx
Phasianidae
Meleagridinae (turkeys)
Perdicinae
Phasianinae (pheasants and relatives)
Tetraoninae
 Anatidae  (ducks)
Anatinae
Aythyini
Mergini
Oxyurini
Anserinae
swans
true geese
Dendrocygninae
Stictonettinae
Tadorninae
Anhimidae
Anhima
Chauna
Anseranatidae
Anatalavis
Anseranas
 
Anatinae
Aythyini
Mergini
Oxyurini
Anserinae
swans
true geese
Dendrocygninae
Stictonettinae
Tadorninae
 
Anhima
Chauna
 
Anatalavis
Anseranas
 Cracidae
Cracinae
Oreophasinae
Penelopinae
Megapodidae
Aepypodius
Alectura
Eulipoa
Leipoa
Macrocephalon
Megapodius
Talegalla
Numididae
Acryllium
Agelastes
Guttera
Numida
Odontophoridae
Callipepla
Colinus
Cyrtonyx
Dactylortyx
Dendrortyx
Odontophorus
Oreortyx
Philortyx
Rhynchortyx
Phasianidae
Meleagridinae (turkeys)
Perdicinae
Phasianinae (pheasants and relatives)
Tetraoninae
 
Cracinae
Oreophasinae
Penelopinae
 
Aepypodius
Alectura
Eulipoa
Leipoa
Macrocephalon
Megapodius
Talegalla
 
Acryllium
Agelastes
Guttera
Numida
 
Callipepla
Colinus
Cyrtonyx
Dactylortyx
Dendrortyx
Odontophorus
Oreortyx
Philortyx
Rhynchortyx
 
Meleagridinae (turkeys)
Perdicinae
Phasianinae (pheasants and relatives)
Tetraoninae
 ColumbeaColumbimorphae
Columbiformes (doves and pigeons)
Mesitornithiformes (mesites)
Pterocliformes (sandgrouses)
Mirandornithes
Phoenicopteriformes (flamingos)
Podicipediformes (grebes)
PassereaOtidimorphae
Cuculiformes (cuckoos)
Musophagiformes (turacos)
Otidiformes (bustards)
Strisores
Caprimulgiformes (nightjars and relatives)
Steatornithiformes
Podargiformes
Apodiformes (swifts and hummingbirds)
Opisthocomiformes
Opisthocomiformes (hoatzin)
Cursorimorphae
Charadriiformes (gulls and relatives)
Gruiformes (cranes and relatives)
Phaethontimorphae
Phaethontiformes (tropicbirds)
Eurypygiformes (kagu and sunbittern)
Aequornithes
Gaviiformes (loons or divers)
Sphenisciformes (penguins)
Procellariiformes (albatrosses and petrels)
Ciconiiformes (storks)
Suliformes (cormorants and relatives)
Pelecaniformes (pelicans and relatives)
Australaves
Cariamiformes (seriemas and relatives)
Falconiformes (falcons and relatives)
Psittaciformes (parrots)
Passeriformes (perching birds)
Afroaves
Cathartiformes (New World vultures and condors)
Accipitriformes (eagles and hawks)
Strigiformes (owls)
Coliiformes (mousebirds)
Trogoniformes (trogons and quetzals)
Leptosomiformes (cuckoo roller)
Bucerotiformes (hornbills and hoopoes)
Coraciiformes (kingfishers and rollers)
Piciformes (woodpeckers and relatives)
 Columbimorphae
Columbiformes (doves and pigeons)
Mesitornithiformes (mesites)
Pterocliformes (sandgrouses)
Mirandornithes
Phoenicopteriformes (flamingos)
Podicipediformes (grebes)
 
Columbiformes (doves and pigeons)
Mesitornithiformes (mesites)
Pterocliformes (sandgrouses)
 
Phoenicopteriformes (flamingos)
Podicipediformes (grebes)
 Otidimorphae
Cuculiformes (cuckoos)
Musophagiformes (turacos)
Otidiformes (bustards)
Strisores
Caprimulgiformes (nightjars and relatives)
Steatornithiformes
Podargiformes
Apodiformes (swifts and hummingbirds)
Opisthocomiformes
Opisthocomiformes (hoatzin)
Cursorimorphae
Charadriiformes (gulls and relatives)
Gruiformes (cranes and relatives)
Phaethontimorphae
Phaethontiformes (tropicbirds)
Eurypygiformes (kagu and sunbittern)
Aequornithes
Gaviiformes (loons or divers)
Sphenisciformes (penguins)
Procellariiformes (albatrosses and petrels)
Ciconiiformes (storks)
Suliformes (cormorants and relatives)
Pelecaniformes (pelicans and relatives)
Australaves
Cariamiformes (seriemas and relatives)
Falconiformes (falcons and relatives)
Psittaciformes (parrots)
Passeriformes (perching birds)
Afroaves
Cathartiformes (New World vultures and condors)
Accipitriformes (eagles and hawks)
Strigiformes (owls)
Coliiformes (mousebirds)
Trogoniformes (trogons and quetzals)
Leptosomiformes (cuckoo roller)
Bucerotiformes (hornbills and hoopoes)
Coraciiformes (kingfishers and rollers)
Piciformes (woodpeckers and relatives)
 
Cuculiformes (cuckoos)
Musophagiformes (turacos)
Otidiformes (bustards)
 
Caprimulgiformes (nightjars and relatives)
Steatornithiformes
Podargiformes
Apodiformes (swifts and hummingbirds)
 
Opisthocomiformes (hoatzin)
 
Charadriiformes (gulls and relatives)
Gruiformes (cranes and relatives)
 
Phaethontiformes (tropicbirds)
Eurypygiformes (kagu and sunbittern)
 
Gaviiformes (loons or divers)
Sphenisciformes (penguins)
Procellariiformes (albatrosses and petrels)
Ciconiiformes (storks)
Suliformes (cormorants and relatives)
Pelecaniformes (pelicans and relatives)
 
Cariamiformes (seriemas and relatives)
Falconiformes (falcons and relatives)
Psittaciformes (parrots)
Passeriformes (perching birds)
 
Cathartiformes (New World vultures and condors)
Accipitriformes (eagles and hawks)
Strigiformes (owls)
Coliiformes (mousebirds)
Trogoniformes (trogons and quetzals)
Leptosomiformes (cuckoo roller)
Bucerotiformes (hornbills and hoopoes)
Coraciiformes (kingfishers and rollers)
Piciformes (woodpeckers and relatives)
 
Category
 Commons
 Portal
 WikiProject
 
Wikidata: Q1267654
Wikispecies: Crex egregia
ADW: Crex_egregia
Avibase: 09A0F2EBC063CF30
BirdLife: 22692539
eBird: afrcra1
EoL: 335802
GBIF: 5228155
IRMNG: 10199806
ITIS: 176278
IUCN: 22692539
uBio: 276228
    Breeding summer visitor    Resident year-round 