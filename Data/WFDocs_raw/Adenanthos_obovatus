
 Adenanthos obovatus, commonly known as basket flower (which usually refers to Centaurea, though), or, jugflower, is a shrub of the plant family Proteaceae endemic to Southwest Australia. Described by French naturalist Jacques Labillardière in 1805, it had first been collected by Archibald Menzies in 1791. Within the genus Adenanthos, it lies in the section Eurylaema and is most closely related to A. barbiger. A. obovatus has hybridized with  A. detmoldii to produce the hybrid A. × pamela. Several common names allude to the prominent red flowers of the species. It grows as a many-stemmed spreading bush up to 1 m (3.3 ft) high, and about 1.5 m (4.9 ft) across, with fine bright green foliage. Made up of single red flowers, the inflorescences appear from April to December, and peak in spring (August to October).
 The shrub grows on sandy soils in seasonally wet lowland areas as well as hills and dunes. It regenerates after bushfire by resprouting from its underground lignotuber. Pollinators include honeyeaters, particularly the western spinebill, which can access the nectar with its long curved bill, and the silvereye, which punctures the flower tube. The most commonly cultivated Adenanthos species in Australia, it has a long flowering period and attracts honeyeaters to the garden. It is harvested for the cut flower industry.
 The growth habit of Adenanthos obovatus is that of a lignotuberous shrub, with many stems arising from a single underground lignotuber. It typically reaches about 1 m (3.3 ft) in height, and about 1.5 m in width, but plants occasionally reach a height of 2 m (6.6 ft).[1]
 The leaves of this species are bright green, oval in shape, up to 20 mm (0.79 in) long and 15 mm (0.59 in) wide, sessile, and arranged in a spiral pattern on the branches.[1] The flowers appear steadily between April and December, and are most frequent between August and October.[2]  They are red or orange, and emerge from the leaf axils. They are usually solitary, but occasionally an axil will carry two flowers. As with other Proteaceae species, each flower consists of a perianth of four united tepals, and a single style. In A. obovatus, the perianth is around 25 mm (0.98 in) long, and the style around 40 mm (1.6 in).[1] Although the flowering period is lengthy (six months), there are generally only a few flowers at any one time, and a seven-year study at Harry Waring Marsupial Reserve south of Perth revealed fairly constant flowering from year to year, even when shrubs were cut back to ground level to simulate a bushfire.[2]
 The species is very similar to A. barbiger. There are obvious differences in typical leaf shape, with leaves of most populations of A. barbiger being very much longer and narrower than those of A. obovatus. However, leaf shape is variable in both species, and some southern populations of A. barbiger have leaves that are indistinguishable from those of A. obovatus. The most systematically important properties by which the two may be distinguished are the shape of the perianth limb, which is rounded in A. obovatus and pointed in A. barbiger; the bracts, glabrous versus hirsute; and the style-ends, which are green and scarlet respectively. The species also differ slightly in range of flower colours: A. barbiger has scarlet to bright red flowers, whereas A. obovatus flowers are scarlet to orange.[3]
 The first known botanical specimen collection of A. obovatus was made by Scottish surgeon and naturalist Archibald Menzies during the visit of the Vancouver Expedition to King George Sound in September and October 1791.[5] However, this collection did not result in publication of the species. Other early collections include a specimen collected by Scottish botanist Robert Brown during the visit of HMS Investigator to King George Sound in December 1801 and January 1802;[6] and, thirteen months later, King George Sound specimens collected by Jean Baptiste Leschenault de la Tour, botanist to Nicolas Baudin's voyage of exploration,[4] and "gardener's boy" Antoine Guichenot.[7] In his notes on vegetation published in the official account of the expedition, Leschenault writes:
 "Sur les bords de la mer, croissent, en grande abondance, l'adenanthos cuneata, l'adenanthos sericea au feuillage velouté, et une espèce du même genre dont les feuilles sont arrondies."[8]
 ("On the seashore, grows, in great abundance, Adenanthos cuneata, the softer-leaved Adenanthos sericea, and a species of the same genus with rounded leaves.")
 The species with rounded leaves was A. obovatus.[9]
 A description of the species was published by Jacques Labillardière in his 1805 Novae Hollandiae Plantarum Specimen,[1] accompanied by a figure drawn by Pierre Antoine Poiteau and engraved by Auguste Plée.[10] Labillardière chose the specific name obovata, in reference to the leaves of his specimen, which were obovate (egg-shaped, with the narrow end at the base).[11][12] This leaf shape is often seen in this species. The term obovata derives from the Latin ob- ("inverse") and ovum ("egg"),[13] and has feminine gender, consistent with the gender assigned by Labillardière to the genus.[14]
 Labillardière did not acknowledge any collector, and so it was long thought that Labillardière himself had collected the first botanical specimens in 1792 while naturalist to Bruni d'Entrecasteaux's expedition in search of the lost ships of Jean-François de Galaup, comte de La Pérouse. However, this and eight other species described by Labillardière do not occur in any locations that he visited, suggesting that he obtained specimens from someone else whom he failed to credit. Though he did not specify a type specimen for A. obovata, a specimen upon which the accompanying figure in Novae Hollandiae Plantarum Specimen appears to be based has been located; it is annotated, apparently in Labillardière's hand, as having been collected by Leschenault.[4] Ernest Charles Nelson states with certainty that Labillardière based this species on specimens collected by Leschenault,[15] and this view has been accepted by some scholars[16] though others treat it more cautiously.[17]
 In 1870, George Bentham published the first infrageneric arrangement of Adenanthos in Volume 5 of his landmark Flora Australiensis. Bentham divided the genus into two sections, placing A. obovata in A. sect. Eurylaema, defined as containing those species with one sterile stamen, and perianth tubes that are curved and swollen above the middle.[18]
 A phenetic analysis of the genus undertaken by Ernest Charles Nelson in 1975 yielded results in which the members of A. sect. Eurylaema occurred together, with A. obovata appearing most closely related to A. barbigera (now A. barbiger), and then A. detmoldii.[19] A. obovata was therefore retained in A. sect. Eurylaema in Ernest Charles Nelson's 1978 revision of Adenanthos,[20] and again in his 1995 treatment of the genus for the Flora of Australia series. By this time, the ICBN had issued a ruling that all genera ending in -anthos must be treated as having masculine gender. Hence the species is now known as Adenanthos obovatus.[21][22] Its placement in Nelson's arrangement of Adenanthos may be summarised as follows:[21]
 Adenanthos obovatus hybridises with A. detmoldii to produce A. × pamela. Known only from road verges in the Scott River region, it was first discovered in 1979, and it is now known from more than twenty individual plants. The discovery of it in such large numbers, together with its recognised horticultural potential, prompted Nelson to formally describe and name it in 1986. Morphologically it is intermediate between its parents; that is, taller than A. obovatus, with longer leaves and light red flowers. It is fertile, raising the possibility of the establishment of a hybrid swarm.[23]
 It is also possible that A. obovatus infrequently hybridises with A. barbiger: in 1921, Carl Hansen Ostenfeld described A. intermedia (now A. intermedius), based on specimens found near Ngilgi Cave at Yallingup with leaf shape intermediate between these two species.[24] The new species was rejected in 1924 by Charles Gardner, and again in the 1970s by Nelson, who argued that leaf shape is inappropriate grounds for erecting a new species in this context, and that, in terms of systematically important characteristics, A. intermedius is indistinguishable from A. barbiger. He therefore synonymized A. intermedius with A. barbiger, but noted the possibility that A. intermedius is of hybrid origin.[25][26] Recently, however, a specimen collected by Greg Keighery was held to represent a new species, provisionally named "A. barbiger subsp. intermedius (Ostenf.) Keighery ms" (later revised to "Adenanthos sp. Whicher Range (G.J. Keighery 9736)"),[27] suggesting that Keighery, at least, does not consider A. intermedius to be of hybrid origin.
 Common names for this species include basket flower, glandflower, jugflower and stick-in-the-jug. In the King George Sound vicinity the Aboriginal name Cheeuk is sometimes used.[11][28]
 Abundant and widespread, A. obovatus occurs on a wide range of soils, favouring soils in the acidic pH range 3.9–5.4. Like most Adenanthos species it is common on deeply leached siliceous sandplain sand. It also occurs on gravelly quartz sand derived from rock outcrops, such as on the rocky hillslopes of the Stirling Range. It is rarely found on gravelly lateritic soils. It is also one of the few Adenanthos species to grow well in moist environments; it will not tolerate seasonal waterlogging—that niche is filled by A. detmoldii—but thrives in damp soils not subject to waterlogging.[30]
 Consistent with these edaphic preferences, A. obovatus is widespread and common in the scrub and heath commonly found on the sandplains of Southwest Australia, and is also common in the sedgelands that develop in moister areas of the region. It is uncommon in forest or woodland areas, because these are usually associated with lateritic soils; but it may be found in stands of jarrah or marri forest where these grow in laterite-free sand. The climate in its range is mediterranean, with annual rainfall from 625–1,250 mm (24.6–49.2 in).[30]
 The species occurs in coastal regions of Southwest Australia, from Gingin and Muchea north of Perth south to Augusta and east along the south coast to Green Range, east of Albany. It also occurs in the Stirling Range, a possible disjunction; and at Narrogin, a certain and substantial disjunction.[29][31] Nelson tentatively explains these disjunct populations in terms of natural climate fluctuations: during times of higher rainfall, the distribution of A. obovatus would have been much more extensive. Reductions in rainfall would cause the distribution to contract, but isolated populations could survive in favourable refugia.[32]
 The western spinebill (Acanthorhynchus superciliosus) is the most frequent visitor to the flowers. A territorial species, the territories are smaller when they contain more Adenanthos obovatus bushes. Their long curved bills fit the tube-like flowers exactly, so that the pollen-presenter brushes against the spinebill's head while it is probing for nectar. The birds then carry pollen from plant to plant, fertilising other plants. A field study marking the pollen with fluorescent dye found that pollen could be deposited on flowers of plants up to 20 metres (66 feet) away from the original flower visited. The New Holland honeyeater (Phylidonyris novaehollandiae)  and brown honeyeater (Lichmera indistincta) have also been recorded with pollen from A. obovatus. The silvereye (Zosterops lateralis) drinks nectar from the flowers, but punctures the tube with its short bill. Larger honeyeaters in one field study tended to avoid A. obovatus, possibly seeking sources of more abundant nectar elsewhere. These species are too heavy for the fine branches and their bills are too large for the tubes.[33]
 Adenanthos obovatus has been recorded as a source of nectar for the honey possum (Tarsipes rostratus) in winter and spring (early June to September), from field studies in the Scott National Park, replaced by A. meisneri in late spring and summer (late October to February).[34]
 Adenanthos obovatus is highly susceptible to dieback caused by the water mould Phytophthora cinnamomi.[35] Specimens in coastal dune vegetation were reported killed by the fungus Armillaria luteobubalina, with mycelial sheaths of the fungus beneath the bark of the root collar.[36]
 Described by Ken Newbey as "an outstanding ornamental species with average foliage and very attractive in flower",[37] A. obovatus was first grown in Great Britain in 1824, and is the most commonly cultivated Adenanthos species in Australia. It flowers for most of the year, is an excellent attractor of honeyeaters, and grows in a range of climates.[13][38] Propagation is by cuttings of the current season's growth, from which it strikes readily,[13] and subsequently makes fairly quick growth.[37] Despite its natural occurrence in damp locations, in cultivation it grows best in a light, well-drained soil. It prefers a sunny aspect. Regular hard pruning is recommended to maintain an attractive form.[13][38] Wildflowers of this species are harvested by licensed pickers in the cut flower industry, for sale in both local and export markets.[13] The long, curved stems with flowers in the leaf axils have been described as "excellent for picking".[39]
 
 1 Description 2 Taxonomy

2.1 Discovery and naming
2.2 Infrageneric placement
2.3 Hybrids
2.4 Common names

 2.1 Discovery and naming 2.2 Infrageneric placement 2.3 Hybrids 2.4 Common names 3 Distribution and habitat 4 Ecology 5 Cultivation 6 Footnotes 7 References 8 External links ^ a b c d Nelson (1978): 329.
 ^ a b Wooller, Ronald D.; Wooller, A.J. (1998). "Consistent individuality in the timing and magnitude of flowering by Adenanthos obovatus (Proteaceae)". Australian Journal of Botany. 46 (5–6): 595–608. doi:10.1071/BT97050..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ Nelson (1975b): 135.
 ^ a b c Nelson (1975a): 332.
 ^ Knight, Joseph; Salisbury, Richard (1809). On the Cultivation of the Plants Belonging to the Natural Order of Proteeae. pp. 96–97.
 ^ "Adenanthos obovatus Labill". Robert Brown's Australian Botanical Specimens, 1801–1805 at the BM. FloraBase, Western Australian Herbarium. Retrieved 2011-01-05.
 ^ Nelson, E. Charles (1976). "Antoine Guichenot and Adenanthos (Proteaceae) specimens collected during Baudin's Australian Expedition, 1801-1803". Journal of the Society for the Bibliography of Natural History. 8 (1): 1–10. doi:10.3366/jsbnh.1976.8.PART_1.1. ISSN 0260-9541.
 ^ Leschenault de la Tour; Jean Baptiste (1816). "Notice sur la Végétation de la Nouvelle-Hollande et da la terre de Diémen". Voyage de découvertes aux terres australes (in French). 2. p. 366. Retrieved 21 December 2010.
 ^ Nelson (1975b) 1: 27.
 ^ Nelson (1975b) 1: 41.
 ^ a b Nelson (1978): 331.
 ^ Nelson (1975b) 2: A38.
 ^ a b c d e Wrigley (1991): 70.
 ^ Nelson (1978): 320.
 ^ Nelson (1978): 331. "Labillardière could not have collected this species himself in Western Australia (terra van-Leuwin). The type material was collected by Leschenault de la Tour, and passed to Labillardière who used that material for the type description."
 ^ Diels, Ludwig (1981). "Extra-tropical Western Australia".  In Carr, D. J.; Carr, S. G. M. (eds.). People and Plants in Australia. Carr, D. J. (translator, annotations). Academic Press. p. 77 note 5. ISBN 0-12-160720-8. Labillardière fails to give credit to Leschenault for this and for other specimens which he used (e.g. Adenanthos obovata).
 ^ Duyker, Edward (2003). Citizen Labillardière. Carlton: Miegunyah Press. p. 232. ISBN 0-522-85160-6. [A]nother nine anomalous species cannot be explained as a result of human interference, for their distribution patterns do not coincide with any location Labillardière visited. Some of these species may have been collected by Jean-Baptiste Louis Leschenault de La Tour during Nicolas Baudin's expedition to Australia...
 ^ Bentham, George (1870). "Adenanthos". Flora Australiensis. 5. London: L. Reeve & Co. pp. 350–356.
 ^ Nelson (1975b) 1: 130.
 ^ Nelson (1978): 321
 ^ a b Nelson (1995): 324.
 ^ "Australian Plant Census". Council of Heads of Australasian Herbaria (CHAH). Retrieved 26 March 2010.
 ^ Nelson, E. Charles (1986). "Adenanthos × pamela (Proteaceae), a hybrid from south-western W. Australia". Glasra. 9: 1–6.
 ^ Ostenfeld, Carl Hansen (1921). "Contributions to West Australian Botany (III)". Biologiske meddelelser. 3 (2): 48–50. Retrieved 3 December 2010.
 ^ Nelson (1978): 329, 393.
 ^ Nelson (1975b) 1: 137.
 ^ "Name currency: Adenanthos sp. Whicher Range (G.J. Keighery 9736)". FloraBase. West Australian Herbarium, Department of Environment and Conservation, Government of Western Australia. Retrieved 2011-01-17.
 ^ Nelson, Ernest Charles (2005). "The koala plant and related monickers" (PDF). Australian Systematic Botany Society Newsletter (125): 2–3. Retrieved 17 April 2010.
 ^ a b "Adenanthos obovatus". FloraBase. Western Australian Government Department of Parks and Wildlife.
 ^ a b Nelson (1975b) 1: 253, 257.
 ^ Nelson (1978): 330.
 ^ Nelson (1975b) 1: 313–314.
 ^ Newland, C. E.; Wooller, Ronald D. (1985). "Seasonal changes in a honeyeater assemblage in Banksia woodland, near Perth, Western Australia". New Zealand Journal of Zoology. 12: 631–36. doi:10.1080/03014223.1985.10428312. Retrieved 24 November 2010.
 ^ Bradshaw, S. Don; Phillips, Ryan D.; Tomlinson, Sean; Holley, Rebecca J.; Jennings, Sarah; Bradshaw, Felicity J. (2007). "Ecology of the Honey possum, Tarsipes rostratus, in Scott National Park, Western Australia". Australian Mammalogy. 29 (1): 25–38. doi:10.1071/AM07003.
 ^ "Part 2, Appendix 4: The responses of native Australian plant species to Phytophthora cinnamomi" (PDF). Management of Phytophthora cinnamomi for Biodiversity Conservation in Australia. Department of the Environment and Heritage, Australian Government. 2006. Archived from the original (PDF) on 5 March 2011. Retrieved 17 November 2010.
 ^ Shearer, Bryan L.; Crane, C.E.; Fairman, Richard G.; Grant, M.J. (1998). "Susceptibility of Plant Species in Coastal Dune Vegetation of South-western Australia to Killing by Armillaria luteobubalina". Australian Journal of Botany. 46 (2): 321–34. doi:10.1071/BT97012.
 ^ a b Newbey, Ken (1968). West Australian Wildflowers for Horticulture. 1. Picnic Point, New South Wales: Society for Growing Australian Plants. p. 66. OCLC 223733597.
 ^ a b Walters, Brian. "Adenanthos obovatus". Australian Native Plants Society (Australia). Archived from the original on 12 December 2010. Retrieved 25 November 2010.
 ^ Matthews, Lewis J. (1993). The Protea Growers Handbook. Durban: Bok Books International. Trade Winds Press. p. 34. ISBN 1-874849-84-6.
 Nelson, Ernest Charles (1975a). "The collectors and type locations of some of Labillardière's "terra van-Leuwin" (Western Australia) specimens". Taxon. 24 (2/3): 319–36. doi:10.2307/1218341. Nelson, Ernest Charles (1975b). Taxonomy and Ecology of Adenanthos in Southern Australia (PhD thesis). Australian National University. Nelson, Ernest Charles (1978). "A taxonomic revision of the genus Adenanthos Proteaceae". Brunonia. 1: 303–406. doi:10.1071/BRU9780303. Nelson, Ernest Charles (1995). "Adenanthos".  In McCarthy, Patrick (ed.). Flora of Australia. 16. Collingwood, Victoria: CSIRO Publishing / Australian Biological Resources Study. pp. 314–342. ISBN 0-643-05692-0. Wrigley, John; Fagg, Murray (1991). Banksias, Waratahs and Grevilleas. Sydney: Angus & Robertson. ISBN 0-207-17277-3. "Adenanthos obovatus Labill". Flora of Australia Online. Department of the Environment and Heritage, Australian Government.  "Adenanthos obovatus Labill". FloraBase. Western Australian Government Department of Parks and Wildlife.  "Adenanthos obovatus Labill". Australian Plant Name Index (APNI), IBIS database. Centre for Plant Biodiversity Research, Australian Government. Wikidata: Q2619478 Wikispecies: Adenanthos obovatus APNI: 75801 FloraBase: 1791 FoAO: 45044 FoAO2: Adenanthos obovatus GBIF: 3996576 iNaturalist: 550363 IPNI: 702967-1 NCBI: 54927 Plant List: kew-2621372 POWO: urn:lsid:ipni.org:names:702967-1 Tropicos: 100283599 Adenanthos Eudicots of Western Australia Garden plants of Australia Plants described in 1805 CS1 French-language sources (fr) FloraBase ID same as Wikidata Articles with short description Articles with 'species' microformats Commons category link is on Wikidata Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Wikispecies Create a book Download as PDF Printable version Cebuano Español Français Nederlands Português Svenska Tiếng Việt Winaray  This page was last edited on 18 October 2019, at 17:23 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  "Sur les bords de la mer, croissent, en grande abondance, l'adenanthos cuneata, l'adenanthos sericea au feuillage velouté, et une espèce du même genre dont les feuilles sont arrondies."[8]
("On the seashore, grows, in great abundance, Adenanthos cuneata, the softer-leaved Adenanthos sericea, and a species of the same genus with rounded leaves.")

 A. obovatus Adenanthos obovatus Adenanthos obovatus basket flower jugflower Adenanthos A. sect. Eurylaema A. obovatus a b c d a b 46 ^ a b c ^ ^ ^ 8 ^ 2 ^ 1 ^ 1 a b ^ 2 a b c d e ^ ^ ^ ^ ^ 5 ^ 1 ^ a b ^ ^ 9 ^ 3 ^ ^ 1 ^ ^ a b a b 1 ^ ^ 1 ^ 12 ^ 29 ^ ^ 46 a b 1 a b ^ 24 1 16 Adenanthos obovatus Adenanthos obovatus Adenanthos
A. sect. Eurylaema
A. detmoldii
A. barbiger
A. obovatus
A. × pamela
A. sect. Adenanthos (29 species, 8 subspecies) A. sect. Eurylaema
A. detmoldii
A. barbiger
A. obovatus
A. × pamela A. detmoldii A. barbiger A. obovatus A. × pamela A. sect. Adenanthos (29 species, 8 subspecies) Adenanthos
A. sect. Eurylaema
A. detmoldii
A. barbiger
A. obovatus
A. × pamela
A. sect. Adenanthos (29 species, 8 subspecies) A. sect. Eurylaema
A. detmoldii
A. barbiger
A. obovatus
A. × pamela
A. sect. Adenanthos (29 species, 8 subspecies) A. detmoldii
A. barbiger
A. obovatus
A. × pamela 
 Kingdom:
 Plantae
 Clade:
 Tracheophytes
 Clade:
 Angiosperms
 Clade:
 Eudicots
 Order:
 Proteales
 Family:
 Proteaceae
 Genus:
 Adenanthos
 Section:
 Adenanthos sect. Eurylaema
 Species:
 A. obovatus
 Adenanthos obovatusLabill.
  Wikisource has original works on the topic: Adenanthos obovatus  Wikimedia Commons has media related to Adenanthos obovatus. 
Wikidata: Q2619478
Wikispecies: Adenanthos obovatus
APNI: 75801
FloraBase: 1791
FoAO: 45044
FoAO2: Adenanthos obovatus
GBIF: 3996576
iNaturalist: 550363
IPNI: 702967-1
NCBI: 54927
Plant List: kew-2621372
POWO: urn:lsid:ipni.org:names:702967-1
Tropicos: 100283599
 