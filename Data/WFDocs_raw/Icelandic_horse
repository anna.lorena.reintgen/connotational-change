The Icelandic horse is a breed of horse developed in Iceland. Although the horses are small, at times pony-sized, most registries for the Icelandic refer to it as a horse. Icelandic horses are long-lived and hardy. In their native country they have few diseases; Icelandic law prevents horses from being imported into the country and exported animals are not allowed to return. The Icelandic displays two gaits in addition to the typical walk, trot, and canter/gallop commonly displayed by other breeds. The only breed of horse in Iceland, they are also popular internationally, and sizable populations exist in Europe and North America. The breed is still used for traditional sheepherding work in its native country, as well as for leisure, showing, and racing.
 Developed from ponies taken to Iceland by Norse settlers in the 9th and 10th centuries, the breed is mentioned in literature and historical records throughout Icelandic history; the first reference to a named horse appears in the 12th century. Horses were venerated in Norse mythology, a custom brought to Iceland by the country's earliest settlers. Selective breeding over the centuries has developed the breed into its current form. Natural selection has also played a role, as the harsh Icelandic climate eliminated many horses through cold and starvation. In the 1780s, much of the breed was wiped out in the aftermath of a volcanic eruption at Laki. The first breed society for the Icelandic horse was created in Iceland in 1904, and today the breed is represented by organizations in 19 different nations, organized under a parent association, the International Federation of Icelandic Horse Associations.
 Icelandic horses weigh between 330 and 380 kilograms (730 and 840 lb)[1] and stand an average of 13 and 14 hands (52 and 56 inches, 132 and 142 cm) high, which is often considered pony size, but breeders and breed registries always refer to Icelandics as horses.[2][3] Several theories have been put forward as to why Icelandics are always called horses, among them the breed's spirited temperament and large personality.[4][5] Another theory suggests that the breed's weight, bone structure and weight-carrying abilities mean it can be classified as a horse, rather than a pony.[6] The breed comes in many coat colors, including chestnut, dun, bay, black, gray, palomino, pinto and roan. There are over 100 names for various colors and color patterns in the Icelandic language.[2][3]  They have well-proportioned heads, with straight profiles and wide foreheads. The neck is short, muscular, and broad at the base; the withers broad and low; the chest deep; the shoulders muscular and slightly sloping; the back long; the croup broad, muscular, short and slightly sloping. The legs are strong and short, with relatively long cannon bones and short pasterns. The mane and tail are full, with coarse hair, and the tail is set low. The breed is known to be hardy and an easy keeper.[7] The breed has a double coat developed for extra insulation in cold temperatures.[8]
 Characteristics differ between various groups of Icelandic horses, depending on the focus of individual breeders. Some focus on animals for pack and draft work, which are conformationally distinct from those bred for work under saddle, which are carefully selected for their ability to perform the traditional Icelandic gaits. Others are bred solely for horsemeat. Some breeders focus on favored coat colors.[2]
 Members of the breed are not usually ridden until they are four years old, and structural development is not complete until age seven. Their most productive years are between eight and eighteen, although they retain their strength and stamina into their twenties. An Icelandic mare that lived in Denmark reached a record age of 56,[4] while another horse, living in Great Britain, reached the age of 42.[9] The horses are highly fertile, and both sexes are fit for breeding up to age 25; mares have been recorded giving birth at age 27. The horses tend to not be easily spooked, probably the result of not having any natural predators in their native Iceland.[4] Icelandics tend to be friendly, docile and easy to handle, although also enthusiastic and self-assured.[10] As a result of their isolation from other horses, disease in the breed within Iceland is mostly unknown, except for some kinds of internal parasites. The low prevalence of disease in Iceland is maintained by laws preventing horses exported from the country being returned, and by requiring that all equine equipment taken into the country be either new and unused or fully disinfected. As a result, native horses have no acquired immunity to disease; an outbreak on the island would be likely to be devastating to the breed.[4] This presents problems with showing native Icelandic horses against others of the breed from outside the country, as no livestock of any species can be imported into Iceland, and once horses leave the country they are not allowed to return.[10]
 The Icelandic is a "five-gaited" breed, known for its sure-footedness and ability to cross rough terrain. As well as the typical gaits of walk, trot, and canter/gallop, the breed is noted for its ability to perform two additional gaits. Although most horse experts consider the canter and gallop to be separate gaits, on the basis of a small variation in the footfall pattern,[11] Icelandic breed registries consider the canter and gallop one gait, hence the term "five-gaited".[12]
 The first additional gait is a four-beat lateral ambling gait known as the tölt. This is known for its explosive acceleration and speed; it is also comfortable and ground-covering.[7] There is considerable variation in style within the gait, and thus the tölt is variously compared to similar lateral gaits such as the rack of the Saddlebred, the largo of the Paso Fino, or the running walk of the Tennessee Walking Horse. Like all lateral ambling gaits, the footfall pattern is the same as the walk (left hind, left front, right hind, right front), but differs from the walk in that it can be performed at a range of speeds, from the speed of a typical fast walk up to the speed of a normal canter. Some Icelandic horses prefer to tölt, while others prefer to trot; correct training can improve weak gaits, but the tölt is a natural gait present from birth.[1][12][13] There are two varieties of the tölt that are considered incorrect by breeders. The first is an uneven gait called a "Pig's Pace" or "Piggy-pace" that is closer to a two-beat pace than a four-beat amble. The second is called a Valhopp and is a tölt and canter combination most often seen in untrained young horses or horses that mix their gaits. Both varieties are normally uncomfortable to ride.[13]
 The breed also performs a pace called a skeið, flugskeið or "flying pace". It is used in pacing races, and is fast and smooth,[2][4] with some horses able to reach up to 30 miles per hour (48 km/h).[10] Not all Icelandic horses can perform this gait; animals that perform both the tölt and the flying pace in addition to the traditional gaits are considered the best of the breed.[10] The flying pace is a two-beat lateral gait with a moment of suspension between footfalls; each side has both feet land almost simultaneously (left hind and left front, suspension, right hind and right front). It is meant to be performed by well-trained and balanced horses with skilled riders. It is not a gait used for long-distance travel. A slow pace is uncomfortable for the rider and is not encouraged when training the horse to perform the gait.[12] Although most pacing horses are raced in harness using sulkies, in Iceland horses are raced while ridden.[10]
 The ancestors of the Icelandic horse were probably taken to Iceland by Viking Age Scandinavians between 860 and 935 AD. The Norse settlers were followed by immigrants from Norse colonies in Ireland, the Isle of Man and the Western Isles of Scotland.[2] These later settlers arrived with the ancestors of what would elsewhere become Shetland, Highland, and Connemara ponies, which were crossed with the previously imported animals.[7] There may also have been a connection with the Yakut pony,[14] and the breed has physical similarities to the Nordlandshest of Norway.[15] Other breeds with similar characteristics include the Faroe pony of the Faeroe Islands[16] and the Norwegian Fjord horse.[17] Genetic analyses have revealed links between the Mongolian horse and the Icelandic horse.[18][19][20]  Mongolian horses are believed to have been originally imported from Russia by Swedish traders; this imported Mongol stock subsequently contributed to the Fjord, Exmoor, Scottish Highland, Shetland and Connemara breeds, all of which have been found to be genetically linked to the Icelandic horse.
 About 900 years ago, attempts were made to introduce eastern blood into the Icelandic, resulting in a degeneration of the stock.[2] In 982 AD the Icelandic Althing (parliament) passed laws prohibiting the importation of horses into Iceland, thus ending crossbreeding. The breed has now been bred pure in Iceland for more than 1,000 years.[21][22]
 The earliest Norse people venerated the horse as a symbol of fertility, and white horses were slaughtered at sacrificial feasts and ceremonies. When these settlers arrived in Iceland, they brought their beliefs, and their horses, with them.[2] Horses played a significant part in Norse mythology, and several horses played major roles in the Norse myths, among them the eight-footed pacer named Sleipnir, owned by Odin, chief of the Norse gods.[23] Skalm, a mare who is the first Icelandic horse known by name, appeared in the Book of Settlements from the 12th century. According to the book, a chieftain named Seal-Thorir founded a settlement at the place where Skalm stopped and lay down with her pack. Horses also play key roles in the Icelandic sagas Hrafnkel's Saga, Njal's Saga and Grettir's Saga. Although written in the 13th century, these three sagas are set as far back as the 9th century. This early literature has an influence today, with many riding clubs and horse herds in modern Iceland still bearing the names of horses from Norse mythology.[10]
 Horses were often considered the most prized possession of a medieval Icelander.[24]  Indispensable to warriors, war horses were sometimes buried alongside their fallen riders,[10] and stories were told of their deeds. Icelanders also arranged for bloody fights between stallions; these were used for entertainment and to pick the best animals for breeding, and they were described in both literature and official records from the Commonwealth period of 930 to 1262 AD.[2] Stallion fights were an important part of Icelandic culture, and brawls, both physical and verbal, among the spectators were common. The conflicts at the horse fights gave rivals a chance to improve their political and social standing at the expense of their enemies and had wide social and political repercussions, sometimes leading to the restructuring of political alliances.  However, not all human fights were serious, and the events provided a stage for friends and even enemies to battle without the possibility of major consequences. Courting between young men and women was also common at horse fights.[25]
 Natural selection played a major role in the development of the breed, as large numbers of horses died from lack of food and exposure to the elements. Between 874 and 1300 AD, during the more favorable climatic conditions of the medieval warm period,[26] Icelandic breeders selectively bred horses according to special rules of color and conformation. From 1300 to 1900, selective breeding became less of a priority; the climate was often severe and many horses and people died. Between 1783 and 1784, around 70% of the horses in Iceland were killed by volcanic ash poisoning and starvation after the 1783 eruption of Lakagígar. The eruption lasted eight months, covered hundreds of square miles of land with lava, and rerouted or dried up several rivers.[4][27] The population slowly recovered during the next hundred years, and from the beginning of the 20th century selective breeding again became important.[4] The first Icelandic breed societies were established in 1904, and the first breed registry in Iceland was established in 1923.[1]
 Icelandics were exported to Great Britain before the 20th century to work as pit ponies in the coal mines, because of their strength and small size. However, those horses were never registered and little evidence of their existence remains. The first formal exports of Icelandic horses were to Germany in the 1940s.[24] Great Britain's first official imports were in 1956, when a Scottish farmer, Stuart McKintosh, began a breeding program. Other breeders in Great Britain followed McKintosh's lead, and the Icelandic Horse Society of Great Britain was formed in 1986.[21][28] The number of Icelandic horses exported to other nations has steadily increased since the first exports of the mid-19th century.[24] Since 1969, multiple societies have worked together to preserve, improve and market these horses under the auspices of the International Federation of Icelandic Horse Associations.[29] Today, the Icelandic remains a breed known for its purity of bloodline, and is the only horse breed present in Iceland.[7]
 The Icelandic is especially popular in western Europe, Scandinavia, and North America.[4] There are about 80,000 Icelandic horses in Iceland (compared to a human population of 317,000), and around 100,000 abroad. Almost 50,000 are in Germany, which has many active riding clubs and breed societies.[10]
 Icelandic horses still play a large part in Icelandic life, despite increasing mechanization and road improvements that diminish the necessity for the breed's use. The first official Icelandic horse race was held at Akureyri in 1874,[2] and many races are still held throughout the country from April through June. Both gallop and pace races are held, as well as performance classes showcasing the breed's unique gaits.[30] Winter events are often held, including races on frozen bodies of water. In 2009 such an event resulted in both horses and riders falling into the water and needing to be rescued.[31] The first shows, focused on the quality of animals as breeding stock, were held in 1906.[10] The Agricultural Society of Iceland, along with the National Association of Riding Clubs, now organizes regular shows with a wide variety of classes.[2] Some horses are still bred for slaughter, and much of the meat is exported to Japan.[1] Farmers still use the breed to round up sheep in the Icelandic highlands, but most horses are used for competition and leisure riding.[10]
 Today, the Icelandic horse is represented by associations in 19 countries, with the International Federation of Icelandic Horse Associations (FEIF) serving as a governing international parent organization.[32] The FEIF was founded on May 25, 1969, with six countries as original members: Austria, Denmark, Germany, Iceland, the Netherlands, and Switzerland. France and Norway joined in 1971, and Belgium and Sweden in 1975. Later, Finland, Canada, Great Britain, USA, Faroe Islands, Luxembourg, Italy, Slovenia and Ireland became members, but Ireland subsequently left because of a lack of members. New Zealand has been given the status of "associate member" as its membership base is small.[33] In 2000, WorldFengur was established as the official FEIF registry for Icelandic horses.[34] The registry is a web database program that is used as a studbook to track the history and bloodlines of the Icelandic breed.[35]  The registry contains information on the pedigree, breeder, owner, offspring, photo, breeding evaluations and assessments, and unique identification of each horse registered. The database was established by the Icelandic government in cooperation with the FEIF.[34] Since its inception, around 300,000 Icelandic horses, living and dead, have been registered worldwide.[35]  The Islandpferde-Reiter- und Züchterverband is an organization of German riders and breeders of Icelandic horses and the association of all Icelandic horse clubs in Germany.[36]
 
 United States Icelandic Horse Conference The Icelandic Horse Society of Great Britain Equus ferus caballus 1 Breed characteristics

1.1 Gaits

 1.1 Gaits 2 History 3 Uses 4 Registration 5 See also 6 References 7 External links Icelandic cattle Icelandic Chicken Icelandic goat Icelandic sheep Icelandic Sheepdog ^ a b c d "Icelandic". Breeds of Livestock. Oklahoma State University. Retrieved 2016-02-17..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c d e f g h i j Edwards, Elwyn Hartley (1994). The Encyclopedia of the Horse (1st American ed.). New York, NY: Dorling Kindersley. pp. 194–195. ISBN 1-56458-614-6.
 ^ a b "Colors". United States Icelandic Horse Congress. Retrieved 2009-02-22.
 ^ a b c d e f g h Hendricks, Bonnie (1995). International Encyclopedia of Horse Breeds. University of Oklahoma Press. p. 232. ISBN 978-0-8061-3884-8.
 ^ Becker, Theresa;  et al. (2007). Why Do Horses Sleep Standing Up?: 101 of the Most Perplexing Questions Answered About Equine Enigmas, Medical Mysteries, and Befuddling Behaviors. HCI. p. 46. ISBN 0-7573-0608-X.
 ^ Chamberlin, J. Edward (2007). Horse: How the Horse Has Shaped Civilizations. Random House, Inc. p. 81. ISBN 0-676-97869-X.
 ^ a b c d Bongianni, Maurizio (editor) (1988). Simon & Schuster's Guide to Horses and Ponies. New York, NY: Simon & Schuster, Inc. p. Entry 133. ISBN 0-671-66068-3.CS1 maint: extra text: authors list (link)
 ^ Strickland, Charlene (January 1, 2001). "Pony Power!". The Horse. Retrieved 2009-02-21.
 ^ "About the Icelandic Horse". The Icelandic Horse Society of Great Britain. Archived from the original on 2009-05-27. Retrieved 2009-02-22.
 ^ a b c d e f g h i j "Icelandic Horse". International Museum of the Horse. Kentucky Horse Park. Archived from the original on 2015-05-09. Retrieved May 21, 2012.
 ^ Roberts, Tristan David Martin (1995). Understanding balance: the mechanics of posture and locomotion. Nelson Thornes. pp. 204–206. ISBN 1-56593-416-4.
 ^ a b c "The Gaits of the Icelandic Horse". The Icelandic Horse Society of Great Britain. Archived from the original on 2009-05-28. Retrieved 2009-02-22.
 ^ a b "Buyer's Checklist". United States Icelandic Horse Congress. Retrieved 2009-02-22.
 ^ Edwards, Elwyn Hartley (1994). The Encyclopedia of the Horse (1st American ed.). New York, NY: Dorling Kindersley. pp. 184–185. ISBN 1-56458-614-6.
 ^ Edwards, Elwyn Hartley and Candida Geddes (editors) (1987). The Complete Horse Book. North Pomfret, VT: Trafalgar Square, Inc. p. 121. ISBN 0-943955-00-9.CS1 maint: extra text: authors list (link)
 ^ "Faeroes Pony". Breeds of Livestock. Oklahoma State University. Archived from the original on 2009-06-11. Retrieved 2009-09-05.
 ^ Neville, Jennifer (2008). "Hrothgar's horses:feral or thoroughbred?".  In Godden, Malcolm and Simon Keynes (ed.). Anglo-Saxon England, Volume 35. Cambridge University Press. p. 152. ISBN 0-521-88342-3.
 ^ Nolf, Pamela M (2012). "Detecting Icelandic Horse Origins" (PDF). Icelandic Horse Quarterly. Archived from the original (PDF) on 24 September 2015. Retrieved 1 May 2015.
 ^ "Archived copy" (PDF). Archived from the original (PDF) on 2015-09-24. Retrieved 2015-05-01.CS1 maint: archived copy as title (link)
 ^ Thomas Jansen. "Mitochondrial DNA and the origins of the domestic horse". Proceedings of the National Academy of Sciences. 99: 10905–10910. doi:10.1073/pnas.152330099. PMC 125071. Retrieved 1 May 2015.
 ^ a b "The History of Icelandic Horses". The Icelandic Horse Society of Great Britain. Archived from the original on 2009-05-28. Retrieved 2009-02-22.
 ^ Evans, Andrew (2008). Iceland. Bradt Travel Guides. p. 60. ISBN 1-84162-215-X.
 ^ Littleton, C. Scott (2005). Gods, Goddesses, and Mythology. Marshall Cavendish. pp. 1024–1030. ISBN 0-7614-7559-1.
 ^ a b c "Thousand Year History". United States Icelandic Horse Conference. Retrieved 2009-02-21.
 ^ Martin, John D. (2003). "Sports and Games in Icelandic Saga Literature". Scandinavian Studies. 75: 27–32.
 ^ "The "Medieval Warm Period"". National Climatic Data Center. Retrieved 2009-03-10.
 ^ "Lakagígar Skaftafell National Park" (PDF). The Environment Agency of Iceland. Archived from the original (PDF) on 2008-12-17. Retrieved 2009-02-22.
 ^ Sponenberg, D. Phillip (1996). "The Proliferation of Horse Breeds". Horses Through Time (First ed.). Boulder, CO: Roberts Rinehart Publishers. p. 171. ISBN 1-57098-060-8. OCLC 36179575.
 ^ "FEIF Homepage". International Federation of Icelandic Horse Associations. Archived from the original on 2008-05-11. Retrieved 2009-09-06.
 ^ Björnsson, Gisli B; Sveinsson, Hjalti Jón (2006). The Icelandic horse. Reykjavik.: Mál og Menning. pp. 250–259. ISBN 9979-3-2709-X.
 ^ White, Charlotte (February 5, 2009). "Ponies and riders fall through ice during racing in Reykjavík". Horse & Hound. Archived from the original on February 8, 2009. Retrieved 2009-02-21.
 ^ "Welcome to FEIF". International Federation of Icelandic Horse Associations. Archived from the original on 2008-05-14. Retrieved 2009-02-21.
 ^ "The Development of FEIF". International Federation of Icelandic Horse Associations. Archived from the original on 2016-08-10. Retrieved 2009-02-21.
 ^ a b "WorldFengur". International Federation of Icelandic Horse Associations. Archived from the original on 2008-05-14. Retrieved 2009-02-21.
 ^ a b "WorldFengur: The Studbook of Origin for the Icelandic Horse". WorldFengur. Retrieved 2008-02-22.
 ^ "IPZV e.V. – ein Kurzportrait" (in German). Islandpferde-Reiter- und Züchterverband. Archived from the original on 2010-12-27. Retrieved 2009-09-05.
 International Federation of Icelandic Horse Associations Canadian Icelandic Horse Federation United States Icelandic Horse Congress The Icelandic Horse Society of Great Britain Belgian Studbook of the Icelandic horse Studbook of origin of the Icelandic horse Icelandic Horse magazine Tölt News - US Icelandic Horse magazine Landsmot - National Horse Show v t e Equine anatomy Equine nutrition Horse behavior Horse care Horse breeding Horse genome Equine conformation Equine coat color Horse gait Glossary of equestrian terms Horse industry List of equestrian sports Horse tack Bit Bridle Saddle Harness English riding Western riding Driving Horse training Horse racing Equestrian at the Summer Olympics (medalists, venues) Horse show Equitation Domestication of the horse Horses in warfare Horses in the Middle Ages Horses in the United States Horses in East Asian warfare History of the horse in South Asia Horses in the Napoleonic Wars Horses in World War I Horses in World War II History of the horse in Britain Horse worship Horse burial List of horse breeds Draft horse Feral horse Gaited horse Mountain and moorland pony breeds Sport horse Stock horse Warmblood Wild horse List of donkey breeds Donkey Zebra Onager Hinny Mule Zebroid  Category  Horses portal GND: 4198406-7 SELIBR: 149267 Horse breeds Horse breeds originating in Iceland CS1 maint: extra text: authors list CS1 maint: archived copy as title CS1 German-language sources (de) Wikipedia indefinitely move-protected pages Articles with short description Articles with 'species' microformats Featured articles Wikipedia articles with GND identifiers Wikipedia articles with SELIBR identifiers Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version العربية ܐܪܡܝܐ Беларуская Беларуская (тарашкевіца)‎ Català Čeština Dansk Davvisámegiella Deutsch Eesti Español Esperanto Euskara Føroyskt Français Íslenska Italiano עברית Latviešu Lietuvių Magyar Nederlands Norsk Norsk nynorsk پنجابی Polski Português Русский Scots Simple English Suomi Svenska Українська Tiếng Việt 中文  This page was last edited on 22 September 2019, at 01:06 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Icelandic horse a b c d a b c d e f g h i j a b a b c d e f g h ^ ^ a b c d ^ ^ a b c d e f g h i j ^ a b c a b ^ ^ ^ ^ ^ ^ ^ 99 a b ^ ^ a b c ^ 75 ^ ^ ^ ^ ^ ^ ^ ^ a b a b ^ Icelandic horse List of horse breeds List of donkey breeds Breed Associations Other Websites Icelandic horse performing the tölt. Icelandic Pony Iceland Sturdy build, heavy coat, two unique gaits. United States Icelandic Horse ConferenceThe Icelandic Horse Society of Great Britain Equus ferus caballus  Wikimedia Commons has media related to Icelandic horse. 
Equine anatomy
Equine nutrition
Horse behavior
Horse care
Horse breeding
Horse genome
Equine conformation
Equine coat color
Horse gait
 
Glossary of equestrian terms
Horse industry
List of equestrian sports
Horse tack
Bit
Bridle
Saddle
Harness
English riding
Western riding
Driving
Horse training
Horse racing
Equestrian at the Summer Olympics (medalists, venues)
Horse show
Equitation
 
Domestication of the horse
Horses in warfare
Horses in the Middle Ages
Horses in the United States
Horses in East Asian warfare
History of the horse in South Asia
Horses in the Napoleonic Wars
Horses in World War I
Horses in World War II
History of the horse in Britain
Horse worship
Horse burial
 Horses
List of horse breeds
Draft horse
Feral horse
Gaited horse
Mountain and moorland pony breeds
Sport horse
Stock horse
Warmblood
Wild horse
Other Equus
List of donkey breeds
Donkey
Zebra
Onager
Hybrids
Hinny
Mule
Zebroid
 
List of horse breeds
Draft horse
Feral horse
Gaited horse
Mountain and moorland pony breeds
Sport horse
Stock horse
Warmblood
Wild horse
 
List of donkey breeds
Donkey
Zebra
Onager
 
Hinny
Mule
Zebroid
 
 Category  Horses portal
 
GND: 4198406-7
SELIBR: 149267
 