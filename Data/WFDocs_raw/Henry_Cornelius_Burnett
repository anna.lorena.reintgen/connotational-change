
 Henry Cornelius Burnett (October 25, 1825 – October 1, 1866) was an American politician who served as a Confederate States Senator from Kentucky from 1862 to 1865. From 1855 to 1861, Burnett served four terms in the United States House of Representatives. A lawyer by profession, Burnett had held only one public office—circuit court clerk—before being elected to Congress. He represented Kentucky's 1st congressional district immediately prior to the Civil War. This district contained the entire Jackson Purchase region of the state, which was more sympathetic to the Confederate cause than any other area of Kentucky. Burnett promised the voters of his district that he would have President Abraham Lincoln arraigned for treason. Unionist newspaper editor George D. Prentice described Burnett as "a big, burly, loud-mouthed fellow who is forever raising points of order and objections, to embarrass the Republicans in the House".[1]
 Besides championing the Southern cause in Congress, Burnett also worked within Kentucky to bolster the state's support of the Confederacy. He presided over a sovereignty convention in Russellville in 1861 that formed a Confederate government for the state. The delegates to this convention chose Burnett to travel to Richmond, Virginia to secure Kentucky's admission to the Confederacy. Burnett also raised a Confederate regiment at Hopkinsville, Kentucky, and briefly served in the Confederate States Army. Camp Burnett, a Confederate recruiting post two miles west of Clinton in Hickman County, Kentucky, was named after him.[2]
 Burnett's actions were deemed treasonable by his colleagues in Congress, and he was expelled from the House in 1861. He is one of only five members of the House of Representatives ever to be expelled.[3] Following his expulsion, Burnett served in the Provisional Confederate Congress and the First and Second Confederate Senates. He was indicted for treason after the war, but never tried. He returned to the practice of law, and died of cholera in 1866 at the age of 40.
 Henry Cornelius Burnett was born to Dr. Isaac Burnett (1801-1865) and his wife, the former Martha F. Garrett on October 25, 1825, in Essex County, Virginia.[4][5][6] In his early childhood, the family moved to Cadiz, Trigg County, Kentucky.[5][7] Henry was educated privately at an academy in Hopkinsville, the neighboring Christian County government seat.[8] He then read law and was a member of the Cadiz Christian Church.[9]
 On April 13, 1847, Burnett married Mary A. Terry, the daughter of a prominent Cadiz merchant.[4][10]  They had four children: John, Emeline, Henry, and Terry (who died shortly after birth).[4] The younger Henry C. Burnett (educated at the University of Virginia after his father's death) became a successful lawyer in Paducah and, later, Louisville.[11]
 Admitted to the bar in 1847, Burnett established his legal practice in Cadiz.[8] He also began owning slaves of his own. He owned five slaves in Trigg County in 1850.[12] Ten years later, in 1860, Burnett owned seven slaves in Cadiz, as well as leased three slaves to J.L. Parrish in Christian County.[13]
 In the first election following the ratification of the Kentucky Constitution of 1850, Trigg County voters elected Burnett clerk of the circuit court; he defeated James E. Thompson.[14][15] Burnett resigned in 1853 to run for Congress.[15]
 Later that year, he was elected as a Democrat to the 34th Congress, succeeding Speaker of the House Linn Boyd.[5][14] He was re-elected to the three succeeding Congresses; during the 35th Congress, he chaired the Committee of Enquiry regarding the sale of Fort Snelling and served on the Committee on the District of Columbia.
 Burnett supported fellow Kentuckian John C. Breckinridge for president in the 1860 presidential election, but Breckinridge lost to Abraham Lincoln. Lincoln had campaigned against the expansion of slavery beyond the states in which it already existed. His victory in the election resulted in seven Southern states declaring their secession from the Union. Despite this, most Americans believed the Union could still be saved. Burnett, however, disagreed. In the January 7, 1861 issue of Paducah's Tri-Weekly Herald, he declared, "There is not the slightest hope of any settlement or adjustment of existing troubles."[16] Despite his pessimism, Burnett endorsed the ill-fated Peace Conference of 1861.[17]
 Following the rapid secessions of Mississippi, Florida, Alabama, Georgia, Louisiana, and Texas, Congress began preparing the nation for war, including by strengthening the army and navy and raising funds for the treasury. Burnett attempted to circumvent these measures by proposing an amendment stipulating that none of these new appropriations could be used to subdue or make war against any of the southern states, but the amendment was defeated.[18]
 To avert war then, the Kentucky General Assembly called for a meeting of border states to convene in Frankfort on May 27. Kentucky's twelve delegates to the convention were to be chosen by special election on May 4. However, after the Confederates fired on Fort Sumter on April 12, the secessionist candidates withdrew from the election. Expressing the view of the majority of these delegates, Burnett opined in the Tri-Weekly Herald that the convention would not occur. He was wrong; the convention was held as scheduled, but it failed to accomplish anything of significance.[19]
 President Lincoln called for special congressional elections to be held in Kentucky in June 1861. The voters of the First District's Southern Rights party called a meeting to be held May 29, 1861 at the Graves County courthouse in Mayfield. The purpose of the meeting was ostensibly to re-nominate Burnett for his congressional seat, but some Unionists believed an ulterior motive was in play. George D. Prentice, editor of the Unionist Louisville Journal, wrote on May 21, 1861 that "the object of [the Mayfield Convention], though not officially explained, is believed to be the separation of the First District from Kentucky if Kentucky remains in the Union, and its annexation to Tennessee".[20]
 Most of the records of the Mayfield Convention were lost, presumably in a fire that destroyed the courthouse in 1864. The most extensive surviving record comes from the notes of James Beadles, a Unionist observer of the proceedings. After a number of speeches were delivered, a majority committee chaired by Paducah circuit judge James Campbell presented a report containing seven resolutions. The resolutions declared the region's sympathy with the South, although it pledged to abide by Kentucky's present policy of neutrality. It condemned President Lincoln for waging an unjust war, and praised Governor Beriah Magoffin for refusing Lincoln's call for troops. The report also condemned the federal government for arming Union sympathizers in the state with so-called "Lincoln guns". A minority committee report was given by Ballard County resident and future U.S. Representative Oscar Turner. This report called Kentucky's neutrality "futile" and "cowardly," promised to fight off any invasion by the North, and recommended calling for aid from Tennessee and the Confederate States in the event of such an invasion. It further warned that if the entire state did not adopt this position, the Purchase region would secede and align itself with Tennessee.[21]
 Burnett, along with Lyon County's Willis B. Machen and Union County's Benjamin P. Cissell, initially endorsed Campbell's majority report. After some debate, Burnett proposed four resolutions in lieu of both reports. The resolutions condemned President Lincoln for the war against the South and the federal government for the provision of the "Lincoln guns". They also praised Governor Magoffin for rebuffing Lincoln's call for troops and encouraged him to drive away any Union invasion of the state. Burnett's resolutions were passed by large margins in preference to both the majority and minority reports.[22]
 Finally, the convention turned to the issue of nominating Burnett. Four others, including Turner, Machen, and Cissell, were also offered as nominees. Burnett received 124 of 155 votes on the first ballot and was chosen unanimously on the second ballot. In his acceptance speech, Burnett declared that he was undecided as to whether he would take the oath of office if elected. This statement alluded to an earlier comment by Turner that "no man who is engaged in the cause of the South could go to Congress and take the oath of office without perjuring himself."[23] Burnett promised that if he did assume his seat, he was determined to arraign President Lincoln for treason.[23]
 In the special elections, Burnett defeated Lawrence Trimble of Paducah. He was the only states' rights candidate elected in the statewide canvass. He won handily in the Jackson Purchase region, which was by far the most pro-Southern area of the state. However, outside the Purchase, he won only his home county of Trigg, and that by a slim margin of 20 votes.[24] (Besides the Purchase counties, the First District also included Caldwell, Crittenden, Hopkins, Livingston, Lyon, Trigg, Union, and Webster counties.)[16]
 Burnett took his seat in the 37th Congress; sources make no mention of his making good on his threat not to take the oath of office. Just days after the First Battle of Bull Run, Burnett's fellow Kentuckian, John J. Crittenden proposed a resolution blaming the war on the disloyal Southerners and defining the war's aim as preservation of the Union without interference in the rights or institutions of the states. Burnett asked that the question be divided. His request was granted, but he only found one colleague willing to vote with him against blaming Southerners for the war.[25]
 After Congress adjourned on August 6, 1861, Burnett returned home to Cadiz and spoke at a number of pro-Southern rallies. On September 4, 1861, Confederate Major General Leonidas Polk violated Kentucky's neutrality by ordering Brigadier General Gideon Johnson Pillow to occupy Columbus.[26] In response, Ulysses S. Grant captured Paducah on September 6, 1861.[27] With neutrality no longer a tenable option, Burnett presided over a conference of Kentucky's Southern sympathizers that occurred at Russellville between October 29 and October 31, 1861. The self-appointed delegates to this conference called for a sovereignty convention on November 18, 1861 for the purpose of establishing a Confederate government for the state.[28]
 In the interim between the two conventions, Burnett traveled to Hopkinsville, where he and Colonel W.C.P. Breckinridge raised a Confederate regiment dubbed the 8th Kentucky Infantry.[10][29] On November 11, 1861, Burnett himself enlisted in the Confederate States Army at Camp Alcorn; he was chosen as colonel of the 8th Kentucky, but never took command.[4][29]
 The sovereignty convention gathered at the William Forst House in Russellville as scheduled on November 18, 1861. Burnett also presided over this convention.[8] Fearing for the safety of the delegates, he first proposed postponing proceedings until January 8, 1862, but Scott County's George W. Johnson convinced the majority of the delegates to continue.[30] By the third day, the military situation was so tenuous that the entire convention had to be moved to a tower on the campus of Bethel Female College, a now-defunct institution in Russellville.[31]
 The convention passed an ordinance of secession and established a provisional Confederate government for Kentucky.[32] Burnett, William Preston of Fayette County and William E. Simms of Bourbon County were chosen as commissioners for the provisional government and were dispatched to Richmond, Virginia to negotiate with Confederate President Jefferson Davis to secure Kentucky's admission to the Confederacy.[33] For reasons unexplained by the delegates, Dr. Luke P. Blackburn, a native Kentuckian living in Mississippi, was invited to accompany the commissioners.[34] Despite the fact that Kentucky's elected government in Frankfort had opposed secession, the commissioners convinced Davis to recommend Kentucky's admission to the Confederacy; the Confederate Congress officially admitted Kentucky on December 10, 1861.[33]
 Following his successful mission to Richmond, Burnett joined the 8th Kentucky at Fort Donelson.[35] On February 16, 1862, Ulysses S. Grant led a combined Federal army-navy attack against the fort.[35] Most of the Confederate garrison was captured, including the 8th Kentucky, but Burnett escaped in General John B. Floyd's retreat following the defeat.[5][11] This battle ended Burnett's military service.[35]
 Burnett's subversive activities did not go unnoticed by his colleagues in Congress. He was absent when the body reconvened December 2, 1861. The following day, Indiana representative W. McKee Dunn introduced a resolution to expel Burnett from Congress. The resolution passed easily, removing Burnett from the seat he had occupied continuously since 1855.[35]
 Burnett represented Kentucky in the Provisional Confederate Congress from November 18, 1861 to February 17, 1862, and served as a member of that body's Finance Committee.[8][36] He was then elected as a senator to the First and Second Confederate Congresses, serving from February 19, 1862 to February 18, 1865.[8] In the Confederate Senate, he served on the Engrossment and Enrollment and Military Affairs Committees.[36]
 On March 29, Confederate president Jefferson Davis called on the Confederate Congress to pass a conscription bill. The bill would require a three-year term of service for all able-bodied white men between the ages of 18 and 35. At first, the bill was unpopular, but as the military situation grew more desperate for the Confederacy, both houses quickly passed it. Still, the measure caused some to question Davis' military decisions; among them was Burnett, usually one of Davis' staunchest allies. In an April 19, 1862 address to the legislature, Burnett denounced Davis' preference for those who were, like Davis himself, graduates of West Point. The speech drew such a vigorous positive response from the gallery that some of the most zealous had to be removed.[37]
 Following the conclusion of the Civil War, Burnett sought an audience with President Andrew Johnson, an old congressional colleague, but Johnson told him to go home.[38] Burnett was indicted for treason at Louisville, but released on bond and never prosecuted.[38] He partnered with Judge John R. Grace and resumed the practice of law in Cadiz.[10][38] He died of cholera in Hopkinsville on September 28, 1866.[5] Initially buried in the Old Cadiz Cemetery, he was moved to the East End Cemetery in Cadiz.[4] His tombstone bears no mention of his Confederate service.[38]
 1 Early and family life 2 Early career 3 Outset of the Civil War 4 Special congressional elections of 1861 5 Confederate military service and expulsion 6 Confederate political service 7 See also 8 References 9 Bibliography List of United States Representatives expelled, censured, or reprimanded ^ Craig, "Henry C. Burnett", p. 268
 ^ Camp Burnett, Kentucky
 ^ "Members of Congress Expelled From House"
 ^ a b c d e Trigg County, Kentucky Veterans
 ^ a b c d e Craig, The Kentucky Encyclopedia p. 144
 ^ Appleton's Cyclopedia, Vol. 1 p. 459 indicates his middle name as "Clay".
 ^ The 1840 U.S.Federal Census for Trigg County, Kentucky indicates the Isaac Burnett household included 8 white persons(6 of them under 20 years old) and 3 slaves.
 ^ a b c d e "Burnett, Herny Cornelius". United States Congress
 ^ Perrin, p. 100
 ^ a b c Kerr, p. 68
 ^ a b Johnson, p. 615
 ^ U.S. Federal Census Slave Schedule for District 1, Trigg County, Kentucky, available on ancestry.com.
 ^ 1860 U.S. Federal Census Slave Schedule for Christian County, Kentucky; 1860 U.S. Federal Census Slave Schedule for Cadiz, Trigg County
 ^ a b Allen, p. 279
 ^ a b Perrin, p. 47
 ^ a b Craig, "Henry C. Burnett", p. 266
 ^ Craig, "Henry C. Burnett", pp. 266–267
 ^ Craig, "Henry C. Burnett", pp. 267–268
 ^ Craig, "Henry C. Burnett", pp. 269
 ^ Craig, "The Jackson Purchase Considers Secession" pp. 344–345, 348
 ^ Craig, "The Jackson Purchase Considers Secession" pp. 347–352
 ^ Craig, "The Jackson Purchase Considers Secession" pp. 352–353
 ^ a b Craig, "The Jackson Purchase Considers Secession" p. 353
 ^ Craig, "Henry C. Burnett", p. 270
 ^ Rawley, p. 59
 ^ Harrison, The Civil War in Kentucky, p. 12
 ^ Harrison, The Civil War in Kentucky, p. 13
 ^ Craig, "Henry C. Burnett", pp. 271–272
 ^ a b Craig, "Henry C. Burnett", p. 272
 ^ Harrison in Register, p. 13
 ^ Milliken, p. 222
 ^ Harrison, The Civil War in Kentucky, pp. 20–22
 ^ a b Harrison, The Civil War in Kentucky, p. 22
 ^ Brown, p. 85
 ^ a b c d Craig, "Henry C. Burnett", p. 273
 ^ a b Kentucky Members of the Confederate Congress (1861–1862)
 ^ Walther, pp. 339–340
 ^ a b c d Craig, "Henry C. Burnett", p. 274
 Allen, William B. (1872). A History of Kentucky: Embracing Gleanings, Reminiscences, Antiquities, Natural Curiosities, Statistics, and Biographical Sketches of Pioneers, Soldiers, Jurists, Lawyers, Statesmen, Divines, Mechanics, Farmers, Merchants, and Other Leading Men, of All Occupations and Pursuits. Bradley & Gilbert. p. 279. Retrieved 2008-11-10..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} Kent Masterson Brown, ed. (2000). The Civil War in Kentucky: Battle for the Bluegrass. Mason City, Iowa: Savas Publishing Company. ISBN 1-882810-47-3. "Burnett, Henry Cornelius, (1825–1866)". United States Congress. Retrieved 2008-11-10. "Camp Burnett, Kentucky". National Park Service. Retrieved 2008-11-10. Craig, Berry F. (August 1979). "Henry C. Burnett: Champion of Southern Rights". The Register of the Kentucky Historical Society. 77: 266–274. Craig, Berry F. (1992). "Burnett, Henry C.".  In Kleber, John E. (ed.). The Kentucky Encyclopedia. Associate editors: Thomas D. Clark, Lowell H. Harrison, and James C. Klotter. Lexington, Kentucky: The University Press of Kentucky. ISBN 0-8131-1772-0. Craig, Berry F. (Autumn 2001). "The Jackson Purchase Considers Secession: The 1861 Mayfield Convention". The Register of the Kentucky Historical Society. 99 (4): 339–361. Harrison, Lowell H. (1975). The Civil War in Kentucky. Lexington, Kentucky: The University Press of Kentucky. ISBN 0-8131-0209-X. Harrison, Lowell Hayes (Winter 1981). "George W. Johnson and Richard Hawes: The Governors of Confederate Kentucky". The Register of the Kentucky Historical Society. 79 (1): 3–39. Johnson, E. Polk (1912). A History of Kentucky and Kentuckians: The Leaders and Representative Men in Commerce, Industry and Modern Activities. Lewis Publishing Company. pp. 614–615. Retrieved 2008-11-10. "Kentucky Members of the Confederate Congress (1861–1862)". Kentucky Department of Libraries and Archives. Archived from the original on 2008-12-31. Retrieved 2008-11-10. Kerr, Charles; William Elsey Connelley; Ellis Merton Coulter (1922). History of Kentucky. The American Historical Society. p. 68. Retrieved 2008-11-10. "Members of Congress Expelled From House". FOX News. 2002-07-25. Archived from the original on 2011-02-08. Retrieved 2008-11-10. Milliken, Rena (1992). "Confederate State Government".  In Kleber, John E. (ed.). The Kentucky Encyclopedia. Associate editors: Thomas D. Clark, Lowell H. Harrison, and James C. Klotter. Lexington, Kentucky: The University Press of Kentucky. ISBN 0-8131-1772-0. Perrin, William Henry (1884). Counties of Christian and Trigg, Kentucky : historical and biographical. F.A. Battey Publishing Company. Rawley, James A. (1989). Turning Points of the Civil War. Lincoln University of Nebraska Press. ISBN 978-0-8032-8935-2. Trigg County, Kentucky Veterans. Turner Publishing Company. 2002. p. 22. ISBN 1-56311-837-8. Retrieved 2008-11-10. Walther, Eric H. (2006). William Lowndes Yancey and the Coming of the Civil War. Chapel Hill University of North Carolina Press. ISBN 978-0-8078-3027-7. v t e Baker (Fla.) Clark (Mo.) Clay (Ala.) Davis (N.C.) Graham (N.C.) H. Johnson (Ga.) R. Johnson (Ark.) Lewis (Ga.) Phelan (Miss.) Reade (N.C.) Simms (Ky.) Vest (Mo.) Walker (Ala.) Watson (Miss.) Barnwell (S.C.) Brown (Miss.) Caperton (Va.) Dortch (N.C.) Henry (Tenn.) W. Johnson (Mo.) Maxwell (Fla.) Peyton (Mo.) Preston (Va.) Semmes (La.) Wigfall (Tex.) Burnett (Ky.) Garland (Ark.) Haynes (Tenn.) Hill (Ga.) Hunter (Va.) Jemison (Ala.) Mitchel (Ark.) Oldham (Tex.) Orr (S.C.) Sparrow (La.) Yancey (Ala.)  Category  Commons v t e American Civil War portal Biography portal United States portal Politics portal LCCN: n86809778 SNAC: w6tm93c2 US Congress: B001120 VIAF: 21173170  WorldCat Identities (via VIAF): 21173170 1825 births 1866 deaths American Disciples of Christ American members of the Churches of Christ Confederate States Army officers Confederate States Senators Deaths from cholera Deputies and delegates to the Provisional Congress of the Confederate States Expelled members of the United States House of Representatives Kentucky lawyers Members of the United States House of Representatives from Kentucky People from Essex County, Virginia People of Kentucky in the American Civil War Kentucky Democrats Democratic Party members of the United States House of Representatives 19th-century American politicians People from Cadiz, Kentucky Featured articles Wikipedia articles with LCCN identifiers Wikipedia articles with SNAC-ID identifiers Wikipedia articles with USCongress identifiers Wikipedia articles with VIAF identifiers Wikipedia articles with WorldCat-VIAF identifiers Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Deutsch فارسی Français 中文  This page was last edited on 10 October 2019, at 04:43 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  In office In office Henry Cornelius Burnett ^ ^ ^ a b c d e a b c d e ^ ^ a b c d e ^ a b c a b ^ ^ a b a b a b ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ a b ^ ^ ^ a b ^ a b c d a b ^ a b c d 77 99 79  Member of the U.S. House of Representativesfrom Kentucky's 1st congressional district New constituency  Confederate States Senator (Class 3) from Kentucky William Simms Constituency abolished 34th Senate: House: 35th Senate: House: 36th Senate: House: 37th Senate: House:   In officeFebruary 18, 1862 – May 10, 1865 Constituency established Constituency abolished In officeMarch 4, 1855 – December 3, 1861 Linn Boyd Samuel Casey 
 (1825-10-25)October 25, 1825Essex County, Virginia, U.S. October 1, 1866(1866-10-01) (aged 40)Hopkinsville, Kentucky, U.S. Democratic   Confederate States  Confederate States Army 1861–1862  Colonel 8th Kentucky Infantry American Civil War Preceded byLinn Boyd
  Member of the U.S. House of Representativesfrom Kentucky's 1st congressional district1855–1861
 Succeeded bySamuel Casey
 New constituency
  Confederate States Senator (Class 3) from Kentucky1862–1865        Served alongside: William Simms
 Constituency abolished
 
Baker (Fla.)
Clark (Mo.)
Clay (Ala.)
Davis (N.C.)
Graham (N.C.)
H. Johnson (Ga.)
R. Johnson (Ark.)
Lewis (Ga.)
Phelan (Miss.)
Reade (N.C.)
Simms (Ky.)
Vest (Mo.)
Walker (Ala.)
Watson (Miss.)
  
Barnwell (S.C.)
Brown (Miss.)
Caperton (Va.)
Dortch (N.C.)
Henry (Tenn.)
W. Johnson (Mo.)
Maxwell (Fla.)
Peyton (Mo.)
Preston (Va.)
Semmes (La.)
Wigfall (Tex.)
 
Burnett (Ky.)
Garland (Ark.)
Haynes (Tenn.)
Hill (Ga.)
Hunter (Va.)
Jemison (Ala.)
Mitchel (Ark.)
Oldham (Tex.)
Orr (S.C.)
Sparrow (La.)
Yancey (Ala.)
 
 Category
 Commons
 34th
 Senate: J. B. Thompson | J. J. Crittenden
 House: J. M. Elliot | L. Cox | H. Burnett | J. P. Campbell Jr. | A. G. Talbott | J. Jewett | W. Underwood | H. Marshall | A. K. Marshall | S. F. Swope
 35th
 Senate: J. B. Thompson | J. J. Crittenden
 House: J. M. Elliot | H. Burnett | A. G. Talbott | J. Jewett | W. Underwood | H. Marshall | S. Peyton | J. B. Clay | J. C. Mason | J. W. Stevenson
 36th
 Senate: J. J. Crittenden | L. W. Powell
 House: H. Burnett | S. Peyton | J. W. Stevenson | F. Bristow | W. C. Anderson | J. Y. Brown | G. Adams | R. Mallory | W. E. Simms | L. T. Moore
 37th
 Senate: L. W. Powell | J. C. Breckinridge
 House: H. Burnett | R. Mallory | J. Jackson | H. Grider | A. Harding | C. Wickliffe | G. W. Dunlap | J. J. Crittenden | W. H. Wadsworth | J. Menzies
 
LCCN: n86809778
SNAC: w6tm93c2
US Congress: B001120
VIAF: 21173170
 WorldCat Identities (via VIAF): 21173170
 