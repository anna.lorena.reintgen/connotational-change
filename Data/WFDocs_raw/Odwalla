Odwalla Inc. /oʊˈdwɔːlə/ is an American food product company that sells fruit juices, smoothies and food bars. It was founded in Santa Cruz, California, in 1980 and since 1995 is headquartered in Half Moon Bay, California.  Odwalla's products include juices, smoothies, soy milk, bottled water, organic beverages, and several types of energy bars, known as "food bars".
 The company experienced strong growth after its incorporation in 1985, expanding its distribution network from California to most of North America, and went public in 1993.  In 2001, Odwalla was acquired by The Coca-Cola Company for US$181 million and became a wholly owned subsidiary.
 A period of decline occurred as the result of a fatal outbreak of E.coli O157:H7 in 1996 that was caused by using bruised fruit that had been contaminated. Odwalla originally sold unpasteurized juices, claiming that the process of pasteurization altered the flavor of the juice. Following the E.coli outbreak, an outbreak that was caused by Odwalla not following proper sanitary procedures that caused the death of at least one child, Odwalla adopted flash pasteurization and other sanitization procedures. Odwalla recalled its juices and experienced a 90% reduction in sales following the event. The company gradually recovered and the following year became profitable again.
 Odwalla was founded in Santa Cruz, California, in 1980 by Greg Steltenpohl, Gerry Percy, and Bonnie Bassett.[2][3] Odwalla's production facility is in Dinuba, California. The trio took the idea of selling fruit juices from a business guidebook, and they began by squeezing orange juice with a secondhand juicer in a shed in Steltenpohl's backyard. They sold their product from the back of a Volkswagen van to local restaurants,[3][4] employing slogans such as "soil to soul, people to planet and nourishing the body whole".[5]
 The name for their start-up, "Odwalla", was taken from that of a character who guided "the people of the sun" out of the "gray haze" in the song-poem "Illistrum", a favorite of the founders, which was composed by Roscoe Mitchell and performed by the Art Ensemble of Chicago jazz group, of which Mitchell was a member.[6][7] Steltenpohl, Percy, and Bassett related this to their products, which they believe "help humans break free from the dull mass of over-processed foods so prevalent today".[8]
 Odwalla was incorporated in September 1985 after five years of growth[2] and expanded to sell products in San Francisco in 1988. Hambrecht & Quist Inc., a venture capital firm in San Francisco, was one of Odwalla's main investors at the time, investing several million dollars in the company.[6] By 1992, the company employed 80 people at its company headquarters in Davenport, California, and sold around 20 different flavors of juice for between $1.50 and $2.00 a pint.[6][9] Odwalla went public in December 1993 (NASDAQ:ODWA);[3] the company had 35 delivery trucks, almost 200 employees, and made about $13 million a year.[10] Soon afterwards, Odwalla expanded into new markets when it bought two companies in the Pacific Northwest and Colorado.[3]
 Odwalla built a new production facility located in Dinuba, California, in 1994 in order to better meet production demands.[11] The following year, the company moved its headquarters to Half Moon Bay, California.[12]
 Continual growth and outside investments during these years allowed the company to expand and grow: Odwalla's revenue tripled from 1994 to 1995,[9] and in 1996 they made more than $59 million in sales, their highest ever.[13] This constant growth made Odwalla one of the largest fresh-juice companies in America by 1996,[14] when the company was selling its products to stores in seven states and parts of Canada.[2][15] It was estimated that they would reach $100 million in sales by 1999. Much of this growth resulted from the perception that Odwalla's products were healthier than regular juice because of unpasteurization.[6][14]
 On October 7, 1996, Odwalla made a batch of apple juice using blemished fruit, resulting in one death and 66 sickened customers. Odwalla made and marketed unpasteurized fruit juices for the health segment of the juice market. This batch was contaminated with E. coli.[14]
 Despite a net loss for most of 1997, Odwalla worked to rehabilitate its brand name. In addition to advertising its new safety procedures, Odwalla released its line of food bars (its first solid food product line) and entered the $900 million fruit bar market.[3][16] Another new product was the Future Shake, a "liquid lunch" aimed at younger consumers.[17] Because of these efforts, Odwalla was again profitable by the end of 1997, reporting a profit of $140,000 for the third quarter.[18]
 Having recovered, the company worked to expand geographically into markets like Philadelphia and Washington, D.C.,[19] and by the end of 1998 reported that revenue had surpassed pre-crisis levels.[9] Growth continued over subsequent years,[20][21] in part through the $29 million acquisition of Fresh Samantha, a large juice company based in Saco, Maine, in 2000.[22][23][24] This allowed Odwalla to expand into additional East Coast markets, but incurred high transportation costs as products had to be shipped across the United States from California. To address this problem the company announced plans to build a second production facility in Palm Beach County, Florida. However, facing difficulties in obtaining building permits and allocating sufficient funds, the project was first delayed and eventually cancelled.[23][25] Odwalla produced and sold products under both its own and the Fresh Samantha brand names for a few years; however, in 2003, the company decided to stop selling juice under the Fresh Samantha name and to only sell Odwalla-brand juice.[24][26]
 Odwalla was purchased by The Coca-Cola Company in 2001 for $15.25 a share, a deal which totalled $181 million and was unanimously approved by the Odwalla board of directors.[27][28] Under the terms of the merger, Odwalla's management stayed on as heads of the company, and it was "folded" into Coca-Cola's Minute Maid department.[29] The acquisition was one of several similar mergers which were aimed at expanding Coca-Cola's product line to include non-carbonated drinks.[30][31] Odwalla benefited by obtaining up to a 124.3 percent premium on shares of the company, as well as from the stability and strength that ownership by The Coca-Cola Company offered.[32] Odwalla also was able to expand into new markets because of Coca-Cola's well-established distribution network.[31]
 Odwalla continued to grow following the acquisition. This growth resulted in part from new product releases, which included a line of PomaGrand pomegranate juice (released at the 2006 Sundance Film Festival),[5] two flavors of energy drinks,[33] and three flavors of "Soy Smart" drinks, which contain soy protein, omega-3 fatty acids, and calcium.[34][35]
 Coca-Cola promoted Odwalla products in 2006 when the company aimed at removing carbonated soda products in schools.[36] Odwalla continued to have good growth in 2007, when Coca-Cola, squeezed by poor growth in its North American markets, issued a company-wide hiring freeze; Odwalla, because of its good performance, was one of the few exceptions to the rule.[37]
 Odwalla uses what it characterizes as "fresh-sourced" produce (fruits and vegetables that have recently been harvested) to make many of its products,[2][38] as well as organic oats for food bars and certain tropical fruits in a frozen purée form, purchased from an outside source and blended with fruit juice.[2][23] Because Odwalla uses fresh produce, some juices are seasonal.[39][40] Fruit availability and price is also affected by adverse weather, disease, and natural disasters.[23] Throughout the year, Odwalla juice colors and flavors change slightly because different types of fruit are used.[13][38]
 After the E. coli outbreak, Odwalla improved the safety of several of its production processes. Before the fruit enters the factory, it is washed, sorted and sanitized. Once it has reached the plant, the apples, carrots, and citrus fruits are separated and washed again. The fruit is pressed to get the juice, which is then flash pasteurized and bottled.[38][41] A sample undergoes quality testing, and, if it passes, the batch is shipped in refrigerated trucks to various distribution centers in the United States.[23][38] Odwalla juice has a relatively short shelf life compared to other beverages and thus must be refrigerated. However, after the introduction of flash pasteurization in 1996 and a new plastic bottle in 2001, the shelf life has been considerably extended.[2][14][23] Generally, Odwalla products are sold in special Odwalla-brand displays at grocery and convenience stores, instead of being intermixed with other products.[5][23]
 Odwalla Inc. produces many flavors of fruit and vegetable drinks, as well as dairy-free soy milk (marketed under the "Soy Smart" name),[23][34] "PomaGrand" pomegranate juice,[5] "Serious Energy" caffeinated fruit juice,[33][42] bottled spring water,[2] and Odwalla's Superfood smoothie line of products (see green smoothie), which are viewed as the core products of the company and are made of various fruit purées, wheat grass, and barley grass.[5][43] These products accounted for roughly 95 percent of Odwalla's revenue in 2001.[23]
Odwalla juice is sold in individual 12 fl oz (formerly 450 millilitre or 15.4 US fl oz) bottles made of plant-based recyclable HDPE plastic,[44] as well as larger 64 US fluid ounce (1.9 litre) containers.[40][43][45]
Odwalla's juice, because of production costs, is "typically sold at prices higher than most other juice products", and the price of the juice can vary over time because of weather or disease affecting the supply of fruit and vegetables.[2]
 Odwalla's sugar content can be even higher than Coca-Cola's.  For example, Odwalla's "Mango Tango" has 3.67 grams of sugar per ounce—almost 1 full teaspoon of sugar in every ounce of juice.[46]  Coca-Cola contains 3.25 grams of sugar per ounce.[47] This translates to 44 grams of sugar (nearly four tablespoons) in a 12-ounce "Mango Tango" versus 39 grams of sugar in a 12-ounce Coke.
 Throughout its history, Odwalla has produced and subsequently withdrawn various juice flavors due to their lower popularity, including the Odwalla Superfood Amazing Purple,[48] Soy Vanilla,[49] and Pomegranate Mango drinks.[12]
 In September 1998, Odwalla began to sell energy bars made with fruit and grains, named food bars,[50] as an alternative to its drinks in an attempt to raise revenue following the 1996, that caused the death of at least one child,E. coli outbreak.[3] The first three flavors released were Cranberry Citrus, Organic Carrot & Raisin, and Peach Crunch, all three of which are now discontinued.[16] Within eight weeks of their release, the Odwalla Bar was one of the top-selling energy bar brands in the market.[51] The bars come in a variety of flavors, and weigh 2 ounces (56 grams) per bar. As of 2001, before Odwalla was acquired by Coca-Cola, food bars accounted for less than five percent of Odwalla's revenue.[23]
 
 Greg Andrew Steltenpohl Gerry Keith Percy Jeannine Bonstelle (Bonnie) Bassett 1 History

1.1 Origin
1.2 Incorporation – 1996
1.3 From 1996

 1.1 Origin 1.2 Incorporation – 1996 1.3 From 1996 2 Production 3 Products

3.1 Drinks
3.2 Food bars

 3.1 Drinks 3.2 Food bars 4 See also 5 References 6 External links List of food companies Companies portal Drink portal Food portal San Francisco Bay Area portal ^ "Odwalla Inc. Company Profile". Yahoo Finance. 2008. Archived from the original on February 11, 2007. Retrieved 2008-08-08..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c d e f g h "Odwalla Inc. 10-K405" (Press release). Odwalla Inc. 1996-08-31. Retrieved 2008-08-20.
 ^ a b c d e f "History". Hoovers.com. 2008. Retrieved 2008-09-06.
 ^ Groves, Martha (1997-01-08). "Juice Left in Odwalla; Company Posts Loss, but Sales and Cash Up Despite Recall". The Los Angeles Times (fee required). Retrieved 2008-09-10.
 ^ a b c d e "Odwalla: Growwing [sic] Naturally". Beverage Industry. 2006-03-01. Retrieved 2008-08-18.
 ^ a b c d Kaufman, Steven B. (February 1994). "Freshness by the bottle — Odwalla Inc". Nation's Business. BNET. Archived from the original on 2009-02-03. Retrieved 2008-08-07.
 ^ Strassman, Roy, "Drink No Evil" [biographical article on Steltenpohl], common ground; The Bay Area's Magazine for Conscious Community since 1974, July/August 2009, p. 14.
 ^ "Our Roots Go Deep". Odwalla Inc. 2008. Archived from the original on May 9, 2008. Retrieved 2008-08-07.
 ^ a b c Thomsen, Steven R; Rawson, Bret (1998). "Purifying a tainted corporate image: Odwalla's response to an E.coli poisoning". Public Relations Quarterly. 43 (3). p. 35. Archived from the original on March 4, 2012. Retrieved 2008-09-08.
 ^ Bianchi, Alessandra (July 1993). "Best Love of Product: True Believers". Inc. Magazine. Retrieved 2008-09-06.
 ^ "Labor Pool". City of Dinuba. Archived from the original on October 6, 2007. Retrieved 2008-08-12.
 ^ a b "Odwalla PomaGrand" (Press release). Odwalla Inc. 2006-03-13. Archived from the original on February 3, 2009. Retrieved 2008-08-07.
 ^ a b "Quarterly Report Form 10-Q" (Press release). Odwalla Inc. 1997-11-29. Retrieved 2008-08-20.
 ^ a b c d Christopher Drew and Pam Belluck (January 4, 1988). "Deadly Bacteria a New Threat To Fruit and Produce in U.S." New York Times. Retrieved 2008-08-11. Interviews with former Odwalla managers and company documents show that in the weeks before the outbreak, Odwalla began relaxing its standards on accepting blemished fruit and reining in the authority of its own safety officials, culminating in tense, dramatic moments on the morning of Oct. 7, 1996, the day the contaminated juice was pressed. ...
 ^ Evan, Thomas J. (1996-07-01). "Odwalla". Public Relations Quarterly. Archived from the original on 2015-03-29. Retrieved 2008-08-19.
 ^ a b "Juice maker Odwalla, Inc". Food & Drink Weekly. 1998-09-14. Archived from the original on 2015-03-29. Retrieved 2008-08-18.
 ^ Woolfolk, John (1997-05-14). "California-Based Odwalla Inc. Unveils Soy-Based Shakes". Knight-Ridder/Tribune Business News. Retrieved 2008-09-10.
 ^ "Agreement Reached Between Odwalla, Inc. and U.S. Attorney in Fresno, CA" (Press release). Odwalla Inc. 1998-07-23. Retrieved 2008-08-19.
 ^ "Odwalla Inc. 10-K405" (Press release). Odwalla Inc. 1998-08-29. Retrieved 2008-09-10.
 ^ "Odwalla Sales Increase 24%, Fiscal Year 1999 Momentum Continues; Odwalla Enters Atlanta Market As Geographic Growth Continues". Business Wire. BNET. 1999-07-01. Archived from the original on 2009-02-03. Retrieved 2008-08-18.
 ^ "Odwalla Announces Return to Profitability; Odwalla and Fresh Samantha Merger Completed in May". Business Wire. BNET. 2000-07-06. Archived from the original on 2009-02-03. Retrieved 2008-08-18.
 ^ "Odwalla Completes Merger With Fresh Samantha". Business Wire. BNET. 2000-05-03. Archived from the original on 2009-02-03. Retrieved 2008-08-18.
 ^ a b c d e f g h i j "Annual Report Form 10-K" (Press release). Odwalla Inc. 2001-09-01. Retrieved 2008-08-20.
 ^ a b "Odwalla drops Fresh Samantha". Oakland Tribune. 2003-07-30. Archived from the original on 2016-02-10. Retrieved 2008-08-18.
 ^ Salisbury, Susan (2003-08-04). "Half Moon Bay, Calif.-Based Odwalla to End Its Fresh Samantha Juice Line". Knight Ridder/Tribune Business News. Retrieved 2008-08-18.
 ^ Munarriz, Rick Aristotle (2000-11-27). "Odwalla Gets Juiced Up". The Motley Fool. Archived from the original on 2008-10-05. Retrieved 2008-08-11.
 ^ "EX-99.(A)(2) · Letter to Shareholders" (Press release). Odwalla Inc. 2001-11-06. Retrieved 2008-08-20.
 ^ McClam, Erin (2001-10-31). "To juice up its offerings, Coke buys Odwalla". The Seattle Times. Retrieved 2008-08-08.
 ^ Marino-Nachison, Dave (2001-10-30). "Coke Swallows Odwalla". The Motley Fool. Archived from the original on 2009-02-03. Retrieved 2008-08-11.
 ^ "Tucker Anthony Reiterates Buy on Coca-Cola". BusinessWeek. 2001-10-22. Retrieved 2008-08-11.
 ^ a b Duff, Mike (2002-11-11). "New Age drinks go mainstream: mega manufacturers betting trend is more than passing phase". DSN Retailing Today. BNET. Archived from the original on 2009-02-03. Retrieved 2008-08-18.
 ^ "Odwalla Inc. SC 14D9" (Press release). Odwalla Inc. 2001-11-06. Retrieved 2008-09-15. Under "Reasons for the recommendation of the Board; Factors considered"
 ^ a b "Getting Serious" (Press release). Odwalla Inc. 2007-11-12. Archived from the original on February 3, 2009. Retrieved 2008-08-07.
 ^ a b "Healthful Beverages to Watch in 2007". Beverage Industry. 2007-02-01. Retrieved 2008-08-07.
 ^ "New Products from Coca-Cola FoodService" (PDF) (Press release). The Coca-Cola Company. Archived from the original (PDF) on January 26, 2009. Retrieved 2008-08-13.
 ^ Saletta, Chuck (2006-05-11). "Dueling Fools: Coke Bull". The Motley Fool. Retrieved 2008-08-11.
 ^ "Section of Coke Freezes Hiring". The Atlanta Journal-Constitution. 2008-07-16. Archived from the original on August 26, 2008. Retrieved 2008-10-19.
 ^ a b c d "Freshology". Odwalla Inc. Archived from the original on July 14, 2008. Retrieved 2008-08-07.
 ^ Dornblaster, Lynn (1994-04-15). "Beverages turn icy … and steamy - beverage sales - New Food Products Annual - Industry Overview". Prepared Foods. BNET. Archived from the original on 2009-02-03. Retrieved 2008-08-18. Under the "It's Still the New Age" section.
 ^ a b "Season's Squeezings from Odwalla: 100% Pure Squeezed Tangerine Juice; Now's the Time for Tangerine". Business Wire. BNET. 1999-12-20. Archived from the original on 2009-02-03. Retrieved 2008-08-20.
 ^ "Food Manufacturing Industrial Assessment". U.S. Department of Energy. 2002-06-13. Archived from the original (PDF) on 2009-02-03. Retrieved 2008-08-20.
 ^ "Odwalla Serious Energy". Convenience Store News. 2008-08-25. Archived from the original on 2009-02-03. Retrieved 2008-08-20.
 ^ a b "Coca-Cola Foodservice Meets Restaurant Operators' Juice Needs with Odwalla, Simply Orange, Minute Maid and More" (PDF) (Press release). The Coca-Cola Company. 2008-05-17. Archived from the original (PDF) on February 25, 2009. Retrieved 2008-08-13.
 ^ "Good To The Earth - Fresh Juices & Fruits From Natural Ingredients". Odwalla. Retrieved 15 December 2012.
 ^ "Odwalla's PomaGrand Juices". Convenience Store News. 2006-03-13. Archived from the original on 2009-02-03. Retrieved 2008-08-26.
 ^ "Lose weight & improve your health with a real food diet | Fooducate". www.fooducate.com. Retrieved 2017-07-07.
 ^ "Lose weight & improve your health with a real food diet | Fooducate". www.fooducate.com. Retrieved 2017-07-07.[1]
 ^ Toops, Diane (February 2006). "Royal Treatment from Odwalla". Food Processing. Archived from the original on 2016-02-10. Retrieved 2008-08-07.
 ^ "New Products". Beverage Industry. Archived from the original on February 3, 2009. Retrieved 2008-08-07.
 ^ "Odwalla Bars". Odwalla Inc. Archived from the original on June 7, 2008. Retrieved 2008-08-12.
 ^ "Odwalla Raises the Bar Again; Chocolate Raspberry and Fruity C Monster Join Odwalla Bar Nourishment Ensemble". Business Wire. BNET. 1999-03-17. Archived from the original on 2012-07-10. Retrieved 2008-09-19.
 Odwalla Odwalla on Twitter  v t e A&W Root Beer (Canada only) Ambasa Ameyal Barq's Beat Beverly Bjäre Julmust Blue Sky Cannings Chinotto Coca-Cola Fanta Fioravanti Fresca Fruktime Frutonic Guaraná Jesus Hit Inca Kola Iron Brew Joya Kinley Kola Inglesa Krest Kuat Lemon & Paeroa Lift Lilt Mare Rosso Mello Yello Mezzo Mix Mr. Pibb Nordic Mist OK Soda Pibb Xtra Pibb Zero Portello Quatro Quwat Jabal Ramblin' Root Beer RimZim Red Flash Royal Tru Santiba Sarsi Seagram's Senzao Smart Sparkle Sparletta Sprite Stoney Surge Tab Tanora Tiky Urge Vault VegitaBeta Victoria Yoli Citra Gold Spot Limca Thums Up Ayataka Cappy Capri Sun Delaware Punch Enviga Far Coast Five Alive Fruitopia Frutonic Fuze Glaceau Gold Peak Hi-C Honest Tea Innocent Maaza Matte Leão Minute Maid Nestea Oasis Odwalla Qoo Peace Iced Tea Royal Tru Simply Sokenbicha Sparkle Tum-E Yummies VitaminWater ZICO Aquarius Powerade BPM Energy Burn Formula 50 Full Throttle Lift Plus Monster Mother NOS RAC 124 Relentless Tab Energy Vault Von Dutch AdeS Arwa Aquarius Ciel Dasani Deep River Rock Malvern Water Smart Water Topo Chico Valpre Caribou Coffee Costa Coffee Georgia illy Fairlife Swerve Vio Coca-Cola Beverages Philippines (49%) Coca-Cola Hellenic (23.2%) Coca-Cola European Partners (18%) Coca-Cola Beverages Africa (11.3%) Beverage Partners Worldwide Columbia Pictures TriStar Pictures Escola v. Coca-Cola Bottling Co. (1944) POM Wonderful v. Coca-Cola (2014) Sinaltrainal v. Coca-Cola (2001) U.S. v. 40 Barrels & 20 Kegs of Coca-Cola (1916) Coca-Cola buildings and structures Criticism of Coca-Cola Coca-Cola Freestyle Limca Book of Records My Coke Rewards v t e Balance Bar CalorieMate Clif Bar Lärabar LUNA Bar Naak Odwalla Bar PowerBar Soldier Fuel Taos Mountain Energy Bars Tiger's Milk ZonePerfect Drink companies based in California Food and drink companies established in 1980 1980 establishments in California Food recalls Companies based in San Mateo County, California Companies based in Tulare County, California Coca-Cola brands 1993 initial public offerings 2001 mergers and acquisitions Coca-Cola acquisitions Juice brands Half Moon Bay, California Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Español Français 日本語 Русский  This page was last edited on 4 November 2019, at 08:21 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Odwalla Inc. ^ a b c d e f g h a b c d e f ^ a b c d e a b c d ^ ^ a b c 43 ^ ^ a b a b a b c d ^ a b ^ ^ ^ ^ ^ ^ a b c d e f g h i j a b ^ ^ ^ ^ ^ ^ a b ^ a b a b ^ ^ ^ a b c d ^ a b ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ Wholly owned subsidiary Consumer products Santa Cruz, California, United States (1980) Greg Andrew SteltenpohlGerry Keith PercyJeannine Bonstelle (Bonnie) Bassett Half Moon Bay, California, United States United States Steven M. McCormick, COO and General ManagerJames R. Steichen, SVP Finance and CFOChris Brandt, Director Brand Drinks, food bars US$187.9 million (2007) 900[1] Minute Maid, a division of The Coca-Cola Company Odwalla.com Soft drinks
A&W Root Beer (Canada only)
Ambasa
Ameyal
Barq's
Beat
Beverly
Bjäre Julmust
Blue Sky
Cannings
Chinotto
Coca-Cola
Fanta
Fioravanti
Fresca
Fruktime
Frutonic
Guaraná Jesus
Hit
Inca Kola
Iron Brew
Joya
Kinley
Kola Inglesa
Krest
Kuat
Lemon & Paeroa
Lift
Lilt
Mare Rosso
Mello Yello
Mezzo Mix
Mr. Pibb
Nordic Mist
OK Soda
Pibb Xtra
Pibb Zero
Portello
Quatro
Quwat Jabal
Ramblin' Root Beer
RimZim
Red Flash
Royal Tru
Santiba
Sarsi
Seagram's
Senzao
Smart
Sparkle
Sparletta
Sprite
Stoney
Surge
Tab
Tanora
Tiky
Urge
Vault
VegitaBeta
Victoria
Yoli
Purchased from Bisleri
Citra
Gold Spot
Limca
Thums Up

Juices and teas
Ayataka
Cappy
Capri Sun
Delaware Punch
Enviga
Far Coast
Five Alive
Fruitopia
Frutonic
Fuze
Glaceau
Gold Peak
Hi-C
Honest Tea
Innocent
Maaza
Matte Leão
Minute Maid
Nestea
Oasis
Odwalla
Qoo
Peace Iced Tea
Royal Tru
Simply
Sokenbicha
Sparkle
Tum-E Yummies
VitaminWater
ZICO
Sports drinks
Aquarius
Powerade
Monster Beverage(17.9% stake)
BPM Energy
Burn
Formula 50
Full Throttle
Lift Plus
Monster
Mother
NOS
RAC 124
Relentless
Tab Energy
Vault
Von Dutch
Bottled water
AdeS
Arwa
Aquarius
Ciel
Dasani
Deep River Rock
Malvern Water
Smart Water
Topo Chico
Valpre
Coffee-based
Caribou Coffee
Costa Coffee
Georgia
illy
Dairy-based
Fairlife
Swerve
Vio
 
A&W Root Beer (Canada only)
Ambasa
Ameyal
Barq's
Beat
Beverly
Bjäre Julmust
Blue Sky
Cannings
Chinotto
Coca-Cola
Fanta
Fioravanti
Fresca
Fruktime
Frutonic
Guaraná Jesus
Hit
Inca Kola
Iron Brew
Joya
Kinley
Kola Inglesa
Krest
Kuat
Lemon & Paeroa
Lift
Lilt
Mare Rosso
Mello Yello
Mezzo Mix
Mr. Pibb
Nordic Mist
OK Soda
Pibb Xtra
Pibb Zero
Portello
Quatro
Quwat Jabal
Ramblin' Root Beer
RimZim
Red Flash
Royal Tru
Santiba
Sarsi
Seagram's
Senzao
Smart
Sparkle
Sparletta
Sprite
Stoney
Surge
Tab
Tanora
Tiky
Urge
Vault
VegitaBeta
Victoria
Yoli
Purchased from Bisleri
Citra
Gold Spot
Limca
Thums Up

 
Citra
Gold Spot
Limca
Thums Up
 
Ayataka
Cappy
Capri Sun
Delaware Punch
Enviga
Far Coast
Five Alive
Fruitopia
Frutonic
Fuze
Glaceau
Gold Peak
Hi-C
Honest Tea
Innocent
Maaza
Matte Leão
Minute Maid
Nestea
Oasis
Odwalla
Qoo
Peace Iced Tea
Royal Tru
Simply
Sokenbicha
Sparkle
Tum-E Yummies
VitaminWater
ZICO
 
Aquarius
Powerade
 
BPM Energy
Burn
Formula 50
Full Throttle
Lift Plus
Monster
Mother
NOS
RAC 124
Relentless
Tab Energy
Vault
Von Dutch
 
AdeS
Arwa
Aquarius
Ciel
Dasani
Deep River Rock
Malvern Water
Smart Water
Topo Chico
Valpre
 
Caribou Coffee
Costa Coffee
Georgia
illy
 
Fairlife
Swerve
Vio
  
Coca-Cola Beverages Philippines (49%)
Coca-Cola Hellenic (23.2%)
Coca-Cola European Partners (18%)
Coca-Cola Beverages Africa (11.3%)
 
Beverage Partners Worldwide
Columbia Pictures
TriStar Pictures
 
Escola v. Coca-Cola Bottling Co. (1944)
POM Wonderful v. Coca-Cola (2014)
Sinaltrainal v. Coca-Cola (2001)
U.S. v. 40 Barrels & 20 Kegs of Coca-Cola (1916)
 
Coca-Cola buildings and structures
Criticism of Coca-Cola
Coca-Cola Freestyle
Limca Book of Records
My Coke Rewards
 
Balance Bar
CalorieMate
Clif Bar
Lärabar
LUNA Bar
Naak
Odwalla Bar
PowerBar
Soldier Fuel
Taos Mountain Energy Bars
Tiger's Milk
ZonePerfect
 