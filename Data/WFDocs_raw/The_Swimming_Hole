
 The Swimming Hole (also known as Swimming and The Old Swimming Hole) is an 1884–85 painting by the American artist Thomas Eakins (1844–1916), Goodrich catalog #190, in the collection of the Amon Carter Museum of American Art in Fort Worth, Texas. Executed in oil on canvas, it depicts six men swimming naked in a lake, and is considered a masterpiece of American painting.[1] According to art historian Doreen Bolger it is "perhaps Eakins' most accomplished rendition of the nude figure",[2] and has been called "the most finely designed of all his outdoor pictures".[3] Since the Renaissance, the human body has been considered both the basis of artists' training and the most challenging subject to depict in art,[4] and the nude was the centerpiece of Eakins' teaching program at the Pennsylvania Academy of the Fine Arts.[2] For Eakins, this picture was an opportunity to display his mastery of the human form.
 In this work, Eakins took advantage of an exception to the generally prudish Victorian attitude to nudity: swimming naked was widely accepted,[5] and for males was seen as normal, even in public spaces. Eakins was the first American artist to portray one of the few occasions in 19th-century life when nudity was on display. The Swimming Hole develops themes raised in his earlier work, in particular his treatment of buttocks and his ambiguous treatment of the human form; in some cases it is uncertain as to whether the forms portrayed are male or female. Such themes had earlier been examined in his The Gross Clinic (1875) and William Rush (1877), and would continue to be explored in his paintings of boxers (Taking the Count, Salutat, and Between Rounds) and wrestlers (Wrestlers).[6]
 Although the theme of male bathers was familiar in Western art, having been explored by artists from Michelangelo to Daumier,[7] Eakins' treatment was novel in American art at the time. The Swimming Hole has been "widely cited as a prime example of homoeroticism in American art".[8] In 2008, the art critic Tom Lubbock described Eakins' work as:
 a classic of American painting. It shows a scene of healthy, manly, outdoor activity: a group of young fellows having stripped off for a dip. It is based on the swimming excursions that were enjoyed by the artist and his students. Eakins himself appears in the water at bottom right—in signature position, so to speak."[9]
 Eakins referred to the painting as Swimming in 1885, and as The Swimmers in 1886. The title The Swimming Hole dates from 1917 (the year after Eakins died), when the work was so described by the artist's widow, Susan Macdowell Eakins.[2] Four years later, she titled the work The Old Swimming Hole, in reference to the 1882 poem The Old Swimmin'-Hole; by James Whitcomb Riley.[11][12] The Amon Carter Museum has since returned to Eakins' original title, Swimming.[13]
 The painting shows Eakins and five friends or students bathing at Dove Lake, an artificial lake in Mill Creek outside Philadelphia.[2] Each of the men is looking at the water, in the words of Martin A. Berger, "apparently lost in a contemplative moment".[14] Eakins' precise rendering of the figures has enabled scholars to identify all those depicted in the work. They are (from left to right): Talcott Williams (1849–1928), Benjamin Fox (c. 1865 – c. 1900), J. Laurie Wallace (1864–1953), Jesse Godley (1862–1889), Harry the dog (Eakins' Irish Setter, c. 1880–90), George Reynolds (c. 1839–89), and Eakins himself.[14] The rocky promontory on which several of the men rest is the foundation of the Mill Creek mill, which was razed in 1873. It is the only sign of civilization in the work—no shoes, clothes, or bath houses are visible.[4] The foliage in the background provides a dark background against which the swimmers' skin tones contrast.
  The composition is pyramidal. The figure reclining at left leads the viewer's eye to the seated figure, whose gesture in turn points to Godley at the apex of the compositional pyramid. The diving figure at right leads to the swimming form of Eakins, who painted himself into the scene and whose leftward movement directs attention back into the painting.[3][16] Eakins enforces this pyramidal structure by manipulating the focus of the painting: the center area containing the swimmers is extremely precise, while the outer areas are diffuse, with "virtually no moderating zones in between".[17] The lighting within the picture is unnatural—too bright in some places, and too dark in others—although the effect, which tends to accentuate the body lines of the swimmers, is generally subtle.[17]
 The composition is notable for both its adherence to academic tradition (the mastery of the figure as an end in itself), and its uniqueness in transposing the male nude to an outdoor setting. The depiction of someone diving into water was very rare in the history of Western art.[4] The other figures are artfully arranged to imply a continuous narrative of movement, the poses progressing "from reclining to sitting to standing to diving"; at the same time, each figure is carefully positioned so that no genitalia are visible.[4] As in his previous works, Eakins chose to include a self-portrait, here as the swimmer at bottom-right. Unlike his appearances in The Gross Clinic or Max Schmitt in a Single Scull, here the artist's presence is more ambiguous—he may be seen as companion, teacher, or voyeur.[18] The ripple in the water next to Eakins, and the bubbles around the diver, are the only indications of movement in a painting where motion is otherwise arrested;[18] the water next to the red-headed figure in the lake is still enough to offer a clear reflection.[19] This contrast underscores the tension in the picture between classical prototypes and scientific naturalism.[20]
 The positioning of the bodies and their musculature refers to classical ideals of physical beauty and masculine camaraderie evocative of Greek art.[22] The reclining figure is a paraphrase of the Dying Gaul, and is juxtaposed with the far less formal self-depiction by the artist.[10] It is possible that Eakins was seeking to reconcile an ancient theme with a modern interpretation; the subject was contemporary, but the poses of some of the figures recall those of classical sculpture.[23] One possible influence by a contemporary source was Scène d'été, painted in 1869 by Frédéric Bazille (1841–70). It is not unlikely that Eakins saw the painting at the Salon while studying in Paris, and would have been sympathetic to its depiction of male bathers in a modern setting.[15]
 In Eakins' oeuvre, The Swimming Hole was immediately preceded by a number of similar works on the Arcadian theme. These correspond to lectures he gave on Ancient Greek sculpture and were inspired by the Pennsylvania Academy's casts of Phidias' Pan-Athenaic procession from the Parthenon marbles.[21] A series of photographs, relief sculptures, and oil sketches culminated in the 1883 Arcadia, a painting that also featured nude figures—posed for by a student, a nephew, and the artist's fiancée—in a pastoral landscape.[24]
 Eakins made several on-site oil sketches and photographic studies before painting The Swimming Hole. It is unknown whether the photographs were taken before the oil sketches were produced or vice versa (or, indeed, whether they were created on the same day).
 By the early 1880s, Eakins was using photography to explore sequential movement and as a reference for painting.[25] Some time in 1883 or 1884, he photographed his students engaged in outdoor activities.[22] Four photographs of his students swimming naked in Dove Lake have survived, and bear a clear relationship to The Swimming Hole. The swimmers are seen in the same spot and from the same vantage point, although their positions are entirely different from those in the painting. None of the photographs closely matches the poses depicted in the painting; this was unusual for Eakins, who typically adhered closely to his photographic studies. "The divergence between these sets of images may hint at lost or destroyed pictures, or it may tell us that the photographs came first, before Eakins' mental image had crystallized, and before the execution of his first oil sketch.[26] The poses in the photographs are more spontaneous, while those of the painting are deliberately composed with a classical "severity".[3] Although no photographic studies have survived that would suggest a more direct connection between the photographs and the painting, recent scholarship has proposed that marks incised onto the canvas and later covered by paint indicate that Eakins made use of light-projected photographs.[27]
 Eakins' students swimming naked in Dove Lake, c. 1883–84
 Eakins' students swimming naked in Dove Lake, c. 1883–84
 Eakins' students swimming naked in Dove Lake, c. 1883–84
 Eakins' students swimming naked in Dove Lake, c. 1883–84
 Landscape sketch, from two-sided sketch for Swimming, oil on paperboard, 4 × ​5 3⁄4 in (10 × 15 cm), 1884
 Landscape sketch, for Swimming, oil on cardboard, 4 × ​5 3⁄4 in (10 × 15 cm), 1884
 Sketch of figure, from two-sided sketch for Swimming, oil on paperboard, ​5 3⁄4 × 4 in (15 × 10 cm), 1884
 Sketch of torso, from two-sided sketch for Swimming, oil on cardboard, 10½ × 14½ in (27 × 37 cm), 1884
 An Eakins photograph from 1883, discovered in 1973. "The scene with the three men on a platform may show the setting up of a pose—possibly for the reclining figure in The Swimming Hole."[28]
 Eakins' preparatory sketch of his dog for The Swimming Hole. Sketch of Harry's head, from two sided sketch for Swimming, oil on cardboard, ​10 1⁄2 × ​14 1⁄2 in (27 × 37 cm), 1884
 Swimming Hole sketch, Eakins' final study for The Swimming Hole. Oil on fiberboard mounted on fiberboard, ​8 3⁄4 × ​10 3⁄4 in (22 × 27 cm), Hirshhorn Museum and Sculpture Garden, Washington, DC.
 Eakins combined his studies into a final oil sketch in 1884, which became the basis for the finished painting. The basic composition remained unchanged, as all six men and the dog appeared in the sketch; however, Eakins, who usually adhered closely to his sketches when developing a final work, made several uncharacteristic alterations to the specific movements and positions of the figures.[29] A friend and student, Charles Bregler, described the process:
 ... For a picture ... like the Swimming Hole, a small sketch was made 8 x 10 inches [20 x 25 cm], then separate studies of the landscape and figures, to get the true tone and color, etc. The diving figure being the most difficult to paint, was first modelled in wax. This gave him a thorough knowledge of every form.[30]
 The painting was commissioned in 1884 by Edward Hornor Coates, a Philadelphia businessman who chaired the Committee on Instruction at the Pennsylvania Academy of the Fine Arts, where Eakins taught. Coates intended to pay Eakins $800 ($ 22,000 in 2018 dollars), which at the time was the largest commission Eakins had been offered.[31][32]
 Coates intended the painting for an exhibition at the Pennsylvania Academy of the Fine Arts, and it was shown at the Academy's exhibition in the fall of 1885. However, Coates rejected it as unrepresentative of Eakins' oeuvre.[14] In a November 27, 1885 letter to Eakins, Coates reasoned:
 as you will recall one of my chief ideas was to have from you a picture which might some day become part of the Academy collection. The present canvas is to me admirable in many ways but I am inclined to believe that some of the pictures you have are even more representative, and it has been suggested would be perhaps more acceptable for the purpose which I have always had in view. You must not suppose from this that I depreciate the present work—such is not the case.[33]
 It is not known precisely why Coates failed to purchase the painting; however, it seems likely that Coates felt the work was too controversial to acquire.[34] Coates, as Head of Instruction at Eakins' academy, would have been familiar with the subject matter of Eakins' works, and thus it seems unlikely that the nudity in the painting would have surprised or shocked him.[35] Rather, it seems certain that Coates would have recognized the majority of men in the painting, as all but one were students of Eakins at the academy. He was undoubtedly familiar with the site depicted in the painting too, as it was only a half a mile (800 m) from Haverford College, where Coates studied as an undergraduate.[14] The depiction of a professor and his students together in the nude would have been a sensitive subject for the academy's directors, who had forbidden Eakins from using Academy students as models, as modeling was considered indecent.[35] Coates chose to exchange The Swimming Hole for the "less controversial genre scene" of Eakins' The Pathetic Song—today housed in the Corcoran Gallery of Art—[2] and paid Eakins the $800 he had offered for the original commission.
 On February 9, 1886, Eakins was forced to resign from the Academy because of his removal of a loincloth from a male model in a class where female students were present. In a letter to Coates on February 15 in which Eakins explained his reasons for resigning, he addressed the issue of nudity in his artwork:
 My figures at least are not a bunch of clothes with a head and hands sticking out but more nearly resemble the strong living bodies that most pictures show. And in the latter end of a life so spent in study, you at least can imagine that painting is with me a very serious study. That I have but little patience with the false modesty which is the greatest enemy to all figure painting. I see no impropriety in looking at the most beautiful of Nature's works, the naked figure. If there is impropriety, then just where does such impropriety begin? Is it wrong to look at a picture of a naked figure or at a statue? English ladies of the last generation thought so and avoided the statue galleries, but do so no longer. Or is it a question of sex? Should men make only the statues of men to be looked at by men, while the statues of women should be made by women to be looked at by women only? Should the he-painters draw the horses and bulls, and the she-painters like Rosa Bonheur the mares and cows? Must the poor old male body in the dissecting room be mutilated before Miss Prudery can dabble in his guts? ... Such indignities anger me. Can not anyone see into what contemptible inconsistencies such follies all lead? And how dangerous they are? My conscience is clear, and my suffering is past.[36]
 Following its rejection by Coates, the painting remained in Eakins' possession until his death. It was exhibited just twice more during Eakins' lifetime: at the 1886 Southern Exposition in Louisville, Kentucky, and in 1887 at Chicago's Inter-State Industrial Exposition, and ignored by critics on both occasions. The painting then disappears from the historical record—there is no further reference to the painting in any records from Eakins or his circle of friends during Eakins' lifetime.[37] Following Eakins' death, the painting was exhibited in Philadelphia and New York at memorial exhibitions in 1917.
 In 1925, The Swimming Hole was purchased from the artist's widow by the community of Fort Worth, Texas for $750 ($10,700 in 2018 dollars).[2] Thereafter it was in the collection of the Fort Worth Art Association, the institutional predecessor of the Modern Art Museum of Fort Worth, and was displayed in the city's public library. In 1990, the museum announced it intended to sell the painting to build an endowment for the purchase of contemporary art.[2] A public outcry ensued, prompting the museum to search for a local buyer. Eventually, after tumultuous negotiations, the Amon Carter Museum of American Art agreed to purchase The Swimming Hole for $10 million ($19 million in 2018 dollars).[38][39]
 Before its purchase by the Amon Carter Museum of American Art, The Swimming Hole appears to have undergone seven different conservatory treatments.[2] It may have been restored prior to its inclusion in Eakins' memorial exhibition at the Metropolitan Museum of Art in 1917. A photograph from that time reveals cracks in the glazes and a drip mark, possibly caused by the splash of a caustic liquid.[2] After the painting was acquired by the Fort Worth Art Association, it was often lent out for exhibitions and was damaged as a result. In 1937 it was relined by a private gallery in New York City and the drip was painted out. In 1944 it was relined and restored and in 1947 it was restored again, both times by a private New York dealer.[2] The Brooklyn Museum performed two minor restorations in 1954 and 1957. Although it continued to travel frequently, The Swimming Hole received no comprehensive treatment until 1993.
 Following its purchase by the Amon Carter, in June 1993, Claire M. Barry and staff from the Amon Carter and the Kimbell Art Museums began a major restoration of the painting. According to Barry, "The restoration revealed relatively little significant damage or deterioration not previously visible. Several layers of discolored varnish and overpaint were removed, exposing a rich and varied surface with brushwork ranging from the controlled, almost miniaturistic strokes forming the figures to the freer treatment of the landscape elements."[2]
 Much effort went into distinguishing the original glazes from those added during subsequent restorations. Previous retouches were removed and a natural resin varnish was applied. The painting's original frame, long missing, was located in 1992. It too was cleaned, restored, and reinstalled to the painting.[2]
 During the restoration, it was discovered that a long-standing ascription of the painting's date to 1883 was the result of a misinterpretation: the artist's original inscription of 1885 was painted in a fugitive red-lake pigment that had faded, and was mistakenly repainted by a conservator to the earlier date.[2]
 The Swimming Hole represented the full range of Eakins' techniques and academic principles. He used life study, photography, wax studies, and landscape sketches to produce a work that manifested his interest in the human form.[40] Lloyd Goodrich (1897–1987) believed the work was "Eakins' most masterful use of the nude", with the solidly conceived figures perfectly integrated into the landscape, an image of subtle tonal construction and one of the artist's "richest pieces of painting".[41] Another biographer, William Innes Homer (1929-2012), was more reserved and described the poses of the figures as rigidly academic. Homer found inconsistencies in paint quality and atmospheric effect, and wrote that the painting was unsuccessful in reconciling antique and naturalistic ideals. For him, "it is as though these nudes had been abruptly transplanted from the studio into nature".[20]
 Before the mid-19th century, the subject of the nude male figure in Western art had long been reserved for classical subject matter. In the 19th century, it was not unusual for boys and men to swim without clothing in public, but there was no precedent for this subject in American painting.[42] Although there was an informal convention for multiple-figure compositions featuring female nudes, in America such paintings were exhibited in saloons rather than galleries; Eakins altered the gender and presented the subject as fine art.[43] Viewed in a broader context, The Swimming Hole has been cited as one of the few 19th-century American paintings that "engages directly with a newly emerging European tradition"—that of the male bather.[44] Eakins' picture, although not as stylistically progressive as the works of his French contemporaries, parallels the novel thematic direction taken by Bazille in Summer Scene, Georges Seurat (1859–91) (Bathers at Asnières, 1884) and Paul Cézanne (1839–1906) in his numerous explorations of the subject.[45]
 Eakins' work influenced the subsequent generation of American realists, particularly the artists of the Ashcan School. George Bellows' (1882–1925) Forty-two Kids, painted in 1907, bears obvious similarity to The Swimming Hole, although Bellows' painting has been interpreted as a parody of the Eakins, and the many naked children of the title are playing in the urban Hudson River of New York City rather than in a rural setting.[46] In a sentiment that reflected Eakins' philosophy, Bellows later explained his motivation for painting Forty-two Kids: "Prizefighters and swimmers are the only types whose muscular action can be painted in the nude legitimately."[47]
 Eakins' widow's retitling of the picture after his death reinforced the popular association with the nostalgic sentiment of Riley's poem.[12] More recently, the painting's subject has been compared to the poem "Song of Myself" by Walt Whitman (1819–92), particularly the section "Twenty-Eight Young Men Bathe by the Shore", given the shared interest in the imagery of men bathing in the nude.[10][48] Whitman may have provided inspiration: the celebration of nudity, which in Whitman's case was an open expression of his homosexuality, informs the art of both men.[49] In 1895, one of Eakins' male students reminisced about "us Whitman fellows", which has been interpreted as a reference to homosexuality.[20][50][51] "But for their marital status, however, virtually nothing concrete is known of the private realms or sexual propensities of any of the men depicted (in The Swimming Hole), with the exception of Eakins."[50]
 Although the painting has been viewed as a platonic vision of the male nude seen unselfconsciously in a natural setting,[52] by the 1970s some American writers were beginning to see Eakins' work, and specifically The Swimming Hole, as having homoerotic implications.[53] Critics have paid particular attention to the compositional prominence of the standing figure's buttocks, which has been interpreted as suggestive of "homoerotic interests".[54] According to Jonathan Weinberg, The Swimming Hole marked the beginning of homoerotic imagery in American art.[55] Eakins left a record simultaneously provocative and ambiguous on matters of sex. On the basis of the same visual evidence, that of the photographs, oil sketches, and the finished painting of swimmers, art historians have drawn markedly varying conclusions as to the artist's intent.[55]
 
 1 Title and composition 2 Studies 3 Commission and reception 4 Provenance 5 Restorations 6 Interpretation 7 See also 8 Notes 9 References 10 External links 


Eakins' students swimming naked in Dove Lake, c. 1883–84


 


Eakins' students swimming naked in Dove Lake, c. 1883–84


 


Eakins' students swimming naked in Dove Lake, c. 1883–84


 


Eakins' students swimming naked in Dove Lake, c. 1883–84


 


Landscape sketch, from two-sided sketch for Swimming, oil on paperboard, 4 × ​5 3⁄4 in (10 × 15 cm), 1884


 


Landscape sketch, for Swimming, oil on cardboard, 4 × ​5 3⁄4 in (10 × 15 cm), 1884


 


Sketch of figure, from two-sided sketch for Swimming, oil on paperboard, ​5 3⁄4 × 4 in (15 × 10 cm), 1884


 


Sketch of torso, from two-sided sketch for Swimming, oil on cardboard, 10½ × 14½ in (27 × 37 cm), 1884


 


An Eakins photograph from 1883, discovered in 1973. "The scene with the three men on a platform may show the setting up of a pose—possibly for the reclining figure in The Swimming Hole."[28]


 


Eakins' preparatory sketch of his dog for The Swimming Hole. Sketch of Harry's head, from two sided sketch for Swimming, oil on cardboard, ​10 1⁄2 × ​14 1⁄2 in (27 × 37 cm), 1884


 


Swimming Hole sketch, Eakins' final study for The Swimming Hole. Oil on fiberboard mounted on fiberboard, ​8 3⁄4 × ​10 3⁄4 in (22 × 27 cm), Hirshhorn Museum and Sculpture Garden, Washington, DC.


 Heroic nudity August Blue, Henry Scott Tuke (1893–94) ^ Bolger & Cash 1996, p. vii.
 ^ a b c d e f g h i j k l m Bolger & Barry 1994.
 ^ a b c Goodrich 1982, p. 239.
 ^ a b c d Bolger & Cash 1996, p. 1.
 ^ Adams 2005, p. 305.
 ^ Adams 2005, p. 306.
 ^ Brown Price 1997, pp. 56–63.
 ^ Figliano 2005.
 ^ Lubbock 2008.
 ^ a b c Sewell 1982, p. 90.
 ^ Adams 2005, pp. 306–07.
 ^ a b Bolger & Cash 1996, pp. 28–29.
 ^ Amon Carter Museum 2007.
 ^ a b c d Berger 1997.
 ^ a b Glueck 2004.
 ^ Kirkpatrick 2006, p. 285.
 ^ a b Bolger & Cash 1996, pp. 1–3.
 ^ a b Bolger & Cash 1996, p. 66.
 ^ Bolger & Cash 1996, p. 25: Eakins himself said "There is so much beauty in reflections that it is generally worthwhile to try to get them right."
 ^ a b c Homer 1992, p. 116.
 ^ a b Sewell & Foster 2001, p. 113.
 ^ a b Getty Museum 2008.
 ^ Sewell 1982, pp. 89–90.
 ^ Sewell & Foster 2001, p. 113b: Eakins later gave the unfinished painting to William Merritt Chase.
 ^ Homer 1992, pp. 116, 141–45.
 ^ Bolger & Cash 1996, pp. 21–22.
 ^ Sewell & Foster 2001, pp. 235–36.
 ^ McCoy 1972, pp. 15–22.
 ^ Bolger & Cash 1996, pp. 19.
 ^ Sewell 1982, p. 89.
 ^ Bolger & Cash 1996, p. 13.
 ^ Bolger & Cash 1996, pp. 15–16: At the time, Eakins' annual salary was $1,200 ($ 33,000 in 2018 dollars).
 ^ Bolger & Cash 1996, p. 44.
 ^ Bolger & Cash 1996, p. 45.
 ^ a b Bolger & Cash 1996, p. 26.
 ^ Foster 2002.
 ^ Bolger & Cash 1996, p. 4: "During the following three decades, likely no one beyond the painter's immediate circle of family and friends saw the painting. Nor is there any extant anecdotal or pictorial data to testify to the painter's sense of the work during these years ... The painting simply failed to register in any significant, public way during Eakins' lifetime."
 ^ Reif 1990.
 ^ Kimmelman 1990.
 ^ Sewell & Foster 2001, p. 100.
 ^ Goodrich 1982, pp. 239–40.
 ^ Adams 2005, pp. 305, 311.
 ^ Adams 2005, p. 311.
 ^ Bolger & Cash 1996, p. 83.
 ^ Bolger & Cash 1996, pp. 80–95: Bazille seems to have drawn back from his own more radical instincts, having first painted the figures in Summer Scene completely nude, before deciding to clothe them.
 ^ Turner 2003, pp. 211–12.
 ^ Zurier 2006, p. 216.
 ^ Bolger & Cash 1996, p. 7.
 ^ Homer 1992, pp. 116, 210–13: Several years after the painting was completed, Eakins and Whitman became friends, and in 1887 Eakins traveled to the poet's home in Camden, New Jersey to paint his portrait.
 ^ a b Bolger & Cash 1996, p. 59.
 ^ Davis 1994.
 ^ Sewell 1982, p. 100.
 ^ Bolger & Cash 1996, pp. 7–8: Refers specifically to Cooper 2005, p. 29, Ellenzweig 2012,Fried 1987, and Gopnik 1994
 ^ Adams 2005, pp. 306–08.
 ^ a b Adams 2005, pp. 308–09: Referenced from Weinberg 2004, pp. 15–19
 Adams, Henry (2005). Eakins revealed : the secret life of an American artist. Oxford [England]: Oxford University Press. ISBN 978-0195156683. OCLC 70216284..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} Amon Carter Museum (2007). "Thomas Eakins (1844–1916), Swimming, 1885". Archived from the original on December 7, 2008. Retrieved January 7, 2009. Berger, Martin A. (Autumn 1997). "Modernity and Gender in Thomas Eakins' "Swimming"". American Art. University of Chicago Press on behalf of the Smithsonian American Art Museum. 11 (3): 33–47. doi:10.1086/424303. JSTOR 3109280. Berger, Martin A (2000). Man Made: Thomas Eakins and the Construction of Gilded Age Manhood. Berkeley: University of California Press. OCLC 43397150. (Not cited in article text.) Brown Price, Aimee (December 1997). "How the 'Bathers' emerged—the painting 'Bathers at Asnieres' – Georges Seurat, National Gallery, London, England". Art in America. 85 (12). Bolger, Doreen; Barry, Claire M. (March 1994). "Thomas Eakins's Swimming Hole". Magazine Antiques. 145 (3): 408. ISSN 0161-9284. Bolger, Doreen; Cash, Sarah (1996). Thomas Eakins and the swimming picture. Fort Worth: Amon Carter Museum. ISBN 978-0883600856. OCLC 33668082. Cooper, Emmanuel (2005). The Sexual Perspective: Homosexuality and Art in the Last 100 Years in the West. London: Routledge. ISBN 978-1-134-83458-7. Davis, Whitney (1994). "Erotic Revision in Thomas Eakins's Narratives of Male Nudity". Art History. 17 (3): 301–341. doi:10.1111/j.1467-8365.1994.tb00582.x. ISSN 0141-6790. Ellenzweig, Allen (2012). The Homoerotic Photograph: Male Images from Durieu/Delacroix to Mapplethorpe. Columbia University Press. ISBN 978-0-231-07537-4. Gopnik, Adam (1994). Eakins in the Wilderness. New York Magazine, Incorporated. Fried, Michael (1987). Realism, Writing, Disfiguration: On Thomas Eakins and Stephen Crane. University of Chicago Press. ISBN 978-0-226-26211-6. Figliano, Laurie (2005). "Naked and Exposed: A Historical, Psychosexual and Comparative Analysis of Thomas Eakins' Masterpiece, The Swimming". Concordia Undergraduate Journal of Art History. 2. Archived from the original on June 21, 2016. Retrieved December 21, 2008. Foster, Kathleen A. (2002). "Biography: 1886: Eakins Resigns: Indicted By Rumor". Thomas Eakins: Scenes From a Modern Life Film Companion Website. WHYY, Incorporated. Retrieved January 6, 2008. Getty Museum (February 1, 2008). "Eakins' Students at "The Swimming Hole"". The Getty Museum. Archived from the original on July 28, 2009. Retrieved January 4, 2009. Goodrich, Lloyd (1982). Thomas Eakins, Volume 1. Cambridge, Mass.: Published for the National Gallery of Art [by] Harvard University Press. ISBN 978-0674884908. OCLC 8689426. Glueck, Grace (September 3, 2004). "European Influences on Americans' Views". The New York Times. Retrieved January 6, 2008. Homer, William Innes (1992). Thomas Eakins : His Life and Art. New York: Abbeville Press. ISBN 978-1558592810. OCLC 25711805. Kimmelman, Michael (June 16, 1990). "An Eakins Classic Stays in Texas". The New York Times. Retrieved January 14, 2008. Turner, Jeffrey (2003).  Levander, Caroline Field; Singley, Carol J. (eds.). "On Boyhood and Public Swimming : Sidney Kingsley's Dead End and Representations of Underclass Street Kids in American Cultural Production". The American Child : A Cultural Studies Reader. New Brunswick, N.J.: Rutgers University Press. ISBN 978-0813532233. OCLC 51266247. Lubbock, Tom (February 1, 2008). "Eakins, Thomas The Swimming Hole (1885): The Independent's Great Art series". The Independent. Retrieved January 6, 2009. Kirkpatrick, Sidney (2006). The Revenge of Thomas Eakins. New Haven: Yale University Press. ISBN 9780300108552. OCLC 162135712. McCoy, Garnett (1972). "Some Recently Discovered Thomas Eakins Photographs". Archives of American Art Journal. The Smithsonian Institution. 12 (4): 15–22. doi:10.1086/aaa.12.4.1557158. JSTOR 1557158. Reif, Rita (April 21, 1990). "Fort Worth Strives to Keep Eakins' 'Swimming Hole'". The New York Times. Retrieved January 14, 2008. Sewell, Darrel (1982). Thomas Eakins : Artist of Philadelphia. [Philadelphia]: Philadelphia Museum of Art. ISBN 978-0876330470. OCLC 8387221. Sewell, Darrel; Foster, Kathleen A (2001). Thomas Eakins. Philadelphia, Pa.: Philadelphia Museum of Art; Musée d'Orsay; Metropolitan Museum of Art. ISBN 978-0876331439. OCLC 47785680. Weinberg, Jonathan (2004). Male desire: the homoerotic in American art. H.N. Abrams. ISBN 978-0-8109-5894-4. Zurier, Rebecca (2006). Picturing the City : Urban Vision and the Ashcan School. Berkeley: University of California Press. ISBN 978-0520220188. OCLC 65978585. Amon Carter Center webpage for The Swimming Hole v t e List of works Max Schmitt in a Single Scull (1871) Portrait of Professor Benjamin H. Rand (1874) The Gross Clinic (1875) The Chess Players William Rush Carving His Allegorical Figure of the Schuylkill River (1876) The Fairman Rogers Four-in-Hand Crucifixion (1880) The Writing Master Arcadia (1883) The Swimming Hole (1885) Cowboys in the Badlands (1888) The Artist's Wife and His Setter Dog The Agnew Clinic (1889) Miss Amelia Van Buren (1891) The Concert Singer (1892) Portrait of Maud Cook (1895) The Cello Player The Pianist (1896) Taking the Count Salutat (1898) Between Rounds Wrestlers Portrait of Mary Adeline Williams (1899) Portrait of Mary Adeline Williams The Thinker: Portrait of Louis N. Kenton Antiquated Music: Portrait of Sarah Sagehorn Frishmuth (1900) Portrait of Leslie W. Miller (1901) Self-portrait (1902) Archbishop William Henry Elder (1903) Music (1904) William Rush and His Model (1908) Susan Macdowell Eakins (wife) Thomas Eakins House Eakins Oval Conservation-restoration of The Gross Clinic 1885 paintings Bathing in art Nude art Paintings by Thomas Eakins Articles with short description Wikipedia indefinitely move-protected pages Use mdy dates from September 2015 Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version বাংলা Deutsch Español فارسی Français Italiano Latina 日本語 Português Русский Simple English  This page was last edited on 7 November 2019, at 14:23 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  a classic of American painting. It shows a scene of healthy, manly, outdoor activity: a group of young fellows having stripped off for a dip. It is based on the swimming excursions that were enjoyed by the artist and his students. Eakins himself appears in the water at bottom right—in signature position, so to speak."[9]
 ... For a picture ... like the Swimming Hole, a small sketch was made 8 x 10 inches [20 x 25 cm], then separate studies of the landscape and figures, to get the true tone and color, etc. The diving figure being the most difficult to paint, was first modelled in wax. This gave him a thorough knowledge of every form.[30]
 as you will recall one of my chief ideas was to have from you a picture which might some day become part of the Academy collection. The present canvas is to me admirable in many ways but I am inclined to believe that some of the pictures you have are even more representative, and it has been suggested would be perhaps more acceptable for the purpose which I have always had in view. You must not suppose from this that I depreciate the present work—such is not the case.[33]
 My figures at least are not a bunch of clothes with a head and hands sticking out but more nearly resemble the strong living bodies that most pictures show. And in the latter end of a life so spent in study, you at least can imagine that painting is with me a very serious study. That I have but little patience with the false modesty which is the greatest enemy to all figure painting. I see no impropriety in looking at the most beautiful of Nature's works, the naked figure. If there is impropriety, then just where does such impropriety begin? Is it wrong to look at a picture of a naked figure or at a statue? English ladies of the last generation thought so and avoided the statue galleries, but do so no longer. Or is it a question of sex? Should men make only the statues of men to be looked at by men, while the statues of women should be made by women to be looked at by women only? Should the he-painters draw the horses and bulls, and the she-painters like Rosa Bonheur the mares and cows? Must the poor old male body in the dissecting room be mutilated before Miss Prudery can dabble in his guts? ... Such indignities anger me. Can not anyone see into what contemptible inconsistencies such follies all lead? And how dangerous they are? My conscience is clear, and my suffering is past.[36]
 The Swimming Hole Swimming The Old Swimming Hole ^ a b c d e f g h i j k l m a b c a b c d ^ ^ ^ ^ ^ a b c ^ a b ^ a b c d a b ^ a b a b ^ a b c a b a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ a b 11 85 145 17 2 12  Thomas Eakins 1884–85 Oil on canvas 70 cm × 92 cm (​27 3⁄8 in × ​36 3⁄8 in) Amon Carter Museum, Fort Worth, Texas 
List of works
Max Schmitt in a Single Scull (1871)
Portrait of Professor Benjamin H. Rand (1874)
The Gross Clinic (1875)
The Chess Players
William Rush Carving His Allegorical Figure of the Schuylkill River (1876)
The Fairman Rogers Four-in-Hand
Crucifixion (1880)
The Writing Master
Arcadia (1883)
The Swimming Hole (1885)
Cowboys in the Badlands (1888)
The Artist's Wife and His Setter Dog
The Agnew Clinic (1889)
Miss Amelia Van Buren (1891)
The Concert Singer (1892)
Portrait of Maud Cook (1895)
The Cello Player
The Pianist (1896)
Taking the Count
Salutat (1898)
Between Rounds
Wrestlers
Portrait of Mary Adeline Williams (1899)
Portrait of Mary Adeline Williams
The Thinker: Portrait of Louis N. Kenton
Antiquated Music: Portrait of Sarah Sagehorn Frishmuth (1900)
Portrait of Leslie W. Miller (1901)
Self-portrait (1902)
Archbishop William Henry Elder (1903)
Music (1904)
William Rush and His Model (1908)
 
Susan Macdowell Eakins (wife)
Thomas Eakins House
Eakins Oval
Conservation-restoration of The Gross Clinic
 