
 1080° Snowboarding[a] is a snowboard racing video game developed by Nintendo EAD and published by Nintendo. It was released in 1998 for the Nintendo 64, and was re-released in 2008 for the Wii's Virtual Console. In the game, the player controls one of five snowboarders from a third-person perspective, using a combination of buttons to jump and perform tricks over eight levels.
 1080° was announced in November 1997 and developed over nine months; it garnered critical acclaim and won an Interactive Achievement Award from the Academy of Interactive Arts and Sciences. 1080° sold over two million units, and a second installment, 1080° Avalanche, was released for the Nintendo GameCube in November 2003.
 The player controls a snowboarder in one of several modes. 1080° has two trick modes (trick attack and contest),[1] three race modes (race, time attack, and multiplayer),[2] a training mode, and an options mode.[3] The objective of the game is either to arrive quickly at a level's finish line or to receive maximum points for trick combinations.[4]
 In 1080°'s two trick modes, trick attack and contest, players accrue points from completed tricks.[5] In contest mode, players perform tricks and snowboard past flags for points. Trick attack mode requires players to perform a series of tricks throughout a designated level. The game features 25 tricks, all of which are performed by using a combination of circular positions of the control stick, the R button, and the B button; point values are allocated based on complexity and required time.[5] The two types of tricks are grab tricks, in which the board is grabbed in a specific way, or spin tricks, in which the snowboarder spins the board a certain number of degrees.[4] The 1080° spin requires nine actions, the most of any trick in the game.[6]
 1080° has three race modes; in these modes, victory can be achieved by taking separate routes within a course and balancing the snowboarder after a jump to avoid speed loss.[7] Tricks are scored in race modes, but do not count toward victory.[7] In match race mode, the player competes in a series of races against AI-controlled snowboarders.[8] The game times the player throughout the level and players receive a damage meter which fills if the snowboarder falls down or is knocked over. The difficulty level in match races can be set to easy, medium, or hard, adjusting the complexity and number of races. If the player fails at defeating an AI competitor, they must retire. The player is given three chances to beat the computer before the game is over.[7][9]
 Players may initially choose from five snowboarding characters: two from Japan, and one each from Canada, the United States, and the United Kingdom. Each snowboarder has different abilities and is suited for different levels and modes, since each has varying statistics in fields such as technique, speed, and weight.[10][11] Three additional snowboarders are unlocked by completing certain game levels and modes.[11] Eight snowboards are initially available for every character, and one additional snowboard may be unlocked later in the game. Each board also excels in different situations, since each has different strengths in categories such as balance and edge control.[11]
 1080°'s release was announced on 21 November 1997 at Nintendo's SpaceWorld trade show; the game's working title was then Vertical Edge Snowboarding.[12] 1080° was one of several snowboarding games released for the Nintendo 64 in 1998, others being Big Mountain 2000 and Snowboard Kids.[12] Before the game's release, journalists were able to play 1080° at the January 1998 Nintendo Gamers' Summit.[13]
 1080° was programmed by Englishmen Giles Goddard and Colin Reed, developed and published by Nintendo, and produced by Shigeru Miyamoto.[14] Goddard and Reed had previously programmed Wave Race 64,[15] which sold over a million copies and was a huge commercial success. When developing 1080°, Goddard and Reed used a technique called "skinning" to eliminate joints between the polygons composing the characters. Their programming used a combination of standard animation and inverse kinematics, creating characters whose appearance during collisions is affected by what object is hit, what direction the collision occurs in, and the speed at which the collision takes place.[16] Tommy Hilfiger outfits and Lamar snowboards appear throughout 1080° as product placement.[4] 1080°'s soundtrack of "techno and rappy beats" with "thrashy, foozed-out vocals" was composed by Kenta Nagata,[4][14] who also composed soundtracks for Mario Kart 64 and other Nintendo games.[17]
 1080°'s development took place from April or May 1997 to March 1998.[16] The game was released on 28 February 1998 in Japan[18] and on 31 March 1998 in North America. Nintendo delayed the game's European release because they hoped to boost sales with a winter release;[19] 1080° was eventually released on 30 November 1998, in Europe and the PAL region.[4]
 1080° Snowboarding received "generally favorable" reviews, according to review aggregator website Metacritic.[20] It won the Academy of Interactive Arts & Sciences' 1999 Console Sports Game of the Year award,[25] and was called "one of the best values in both sports and racing gaming" by Josh Smith GameSpot.[23] 1080° Snowboarding has been perceived to be a leader among snowboarding titles at the time, with IGN's Levi Buchanan stating: "every single snowboarding game that followed 1080 borrows from Nintendo's formula".[26] Edge hailed it as the "most convincing video game emulation of the snowboarding experience so far" with an "atmosphere of sobriety" unlike any other Nintendo game at the time.[22]
 The game's graphics were of the highest quality for the Nintendo 64 at the time.[22][27] Smith praised general aspects of the game's graphics such as their crispness, detail, smoothness, and lack of polygon dropout.[23] Reviewers praised the game's camera use, the game's "very solid" physics model,[23] the impression of racers' speed, and the game's snow effects (sun reflected in the snow as appropriate, and fluffy snow and packed snow appeared and behaved differently).[4] Graphical faults included occasional pop-up, misplaced shadows, and lag when racers passed through on-track trees;[4] these problems were generally identified as minor.[23]
 Although writing a positive review, Edge found faults in the game's AI, saying the game suffered from "cheating" CPU opponents.[22] They criticized the AI's simplicity and ability to quickly catch up to the player near the end of a race; they also noted the AI's "limited series of predetermined routes" and the possibility of a player learning where and when an AI falls over, "offering an opportunity to pass [the computer], but conveying little satisfaction with it."[22] Edge also said the PAL release delay "is frankly ludicrous."[28] They believed that, due to Nintendo's slump of noteworthy releases, "any quality title is likely to top the charts with little difficulty."[28]
 Next Generation reviewed the Nintendo 64 version of the game, rating it five stars out of five, and said "With 1080° Snowboarding, Nintendo delivers another system seller and once again sets the standard for an entire genre."[24]
 Writing for AllGame, Shawn Sackenheim considered the "highly technical" control scheme of 1080° Snowboarding one of the game's strengths despite its initial difficulty.[21] Alex Constantides of Computer and Video Games positively reviewed the control scheme, but disagreed on its difficulty, noting "the controls have been implemented so brilliantly that you're able to play perfectly well with just one hand on the stick and Z button."[27] GameSpot called the game's control "thoroughly involving" and said that "[t]he crouch move alone –  which makes for supertight turns –  makes this fun to play."[23] The music was also generally praised, with Matt Casamassina of IGN calling it "a shining example of what can be achieved on the format"[4] and Sackenheim calling it "one of the best N64 soundtracks to date".[21] Sackenheim also praised the game's sound effects.[21]
 In a retrospective review by the Official Nintendo Magazine in 2006, Steve Jarratt commented that 1080° Snowboarding "boasted the best video game representation of snow," and was complemented by "swooshy" sound effects. Positive comments were also made about handling and the quality of the multiplayer.[29] In summary, Jarratt believed "this was a straight-up snowboarder, stunt-free but fast and fun."[29] The magazine also ranked it the 87th best game available on Nintendo platforms. The staff felt it was the most realistic snowboarding game ever made.[30]
 PC Data, which tracked sales in the United States, reported that 1080° Snowboarding sold 817,529 units and earned $40.9 million in revenues by the end of 1998. This made it the country's seventh-best-selling Nintendo 64 release of the year.[31] The game ultimately sold 1,230,000 units in the United States, and over 23,000 in Japan.[32] It did not, however, match the success of the developers' first game, Wave Race 64 which sold 1,950,000 units in the United States and 154,000 in Japan.[32] 1080° Avalanche, a sequel to 1080° Snowboarding, was released for the GameCube in 2003; the sequel received a harsher critical reception, due to "frame rate issues and limited gameplay".[33] 1080° Snowboarding was re-released on the Wii's Virtual Console service in 2008.[34]
 
 JP: 28 February 1998 NA: 31 March 1998 PAL: 9 October 1998 1 Gameplay 2 Development 3 Reception 4 See also 5 Notes 6 References

6.1 Citations

 6.1 Citations 7 External links 1080° Avalanche ^ Japanese: テン・エイティ　スノーボーディング, Hepburn: Ten Eiti Sunōbōdingu?  
 ^ 1080 Snowboarding Instruction Booklet. Japan: Nintendo. 1998. pp. 15–16..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ 1080 Snowboarding Instruction Booklet. Japan: Nintendo. 1998. pp. 12–15, &nbsp, 17.
 ^ 1080 Snowboarding Instruction Booklet. Japan: Nintendo. 1998. pp. 4–6.
 ^ a b c d e f g h i Casamassina, Matt (2 April 1998). "1080° Snowboarding Review". IGN. Archived from the original on 4 March 2014. Retrieved 17 April 2007.
 ^ a b Marriott, Scott Alan. "1080° Snowboarding". Allgame. Archived from the original on 2 January 2010. Retrieved 14 May 2007.
 ^ Operation Card for 1080° Snowboarding. Nintendo Co., Ltd. 1998.
 ^ a b c d Dr_Moo. "1080 Snowboarding –  N64". Game Revolution. Archived from the original on 30 September 2007. Retrieved 21 August 2010.
 ^ 1080 Snowboarding Instruction Booklet. Japan: Nintendo. 1998. p. 12.
 ^ 1080 Snowboarding Instruction Booklet. Japan: Nintendo. 1998. pp. 12–13.
 ^ "1080 Snowboarding (n64)". G4. 20 April 1999. Archived from the original on 29 September 2007. Retrieved 14 May 2007.
 ^ a b c "1080° Snowboarding". Prima Games. 9 December 1999. Archived from the original on 8 August 2007. Retrieved 3 June 2007.
 ^ a b "Head for the Slopes". IGN. 21 November 1997. Archived from the original on 15 September 2007. Retrieved 15 May 2007.
 ^ "1080 Shreds the Competition". IGN. 30 January 1998. Archived from the original on 14 September 2007. Retrieved 15 May 2007.
 ^ a b Nintendo Entertainment Analysis and Development (1 April 1998). 1080° Snowboarding. Nintendo of America, Inc. Scene: staff credits.
 ^ "1080 Snowboarding". Rotten Tomatoes. Retrieved 20 April 2007.
 ^ a b "1080 Snowboarding Interview". IGN. 19 March 1998. Archived from the original on 25 May 2007. Retrieved 20 April 2007.
 ^ "Mario Kart 64 Tech Info". GameSpot. Archived from the original on 30 September 2007. Retrieved 16 May 2007.
 ^ "Official website" (in Japanese). Nintendo Co., Ltd. Archived from the original on 27 December 2007. Retrieved 24 October 2007.
 ^ "1080 Delayed in Europe". IGN. 26 March 1998. Archived from the original on 9 July 2007. Retrieved 15 May 2007.
 ^ a b "1080° Snowboarding". Metacritic. Archived from the original on 24 June 2008. Retrieved 21 August 2010.
 ^ a b c d Sackenheim, Shawn. "1080° Snowboarding". Allgame. Archived from the original on 14 November 2014. Retrieved 17 April 2007.
 ^ a b c d e "1080° Snowboarding". Edge (57). April 1998. pp. 86–88.
 ^ a b c d e f Smith, Josh (25 May 1998). "1080 Snowboarding Review". GameSpot. Archived from the original on 17 May 2007. Retrieved 17 April 2007.
 ^ a b "Finals". Next Generation. No. 42. Imagine Media. June 1998. p. 134.
 ^ "Console Sports Game of the Year". Academy of Interactive Arts & Sciences. Archived from the original on 7 March 2008. Retrieved 1 June 2011.
 ^ Buchanan, Levi (1 October 2008). "Nintendo 64 Week: Day Three". IGN. Archived from the original on 25 December 2016. Retrieved 9 December 2016.
 ^ a b Constantides, Alex (15 August 2001). "1080 Snowboarding". Computer and Video Games. Retrieved 26 December 2007.[dead link]
 ^ a b "1080° Snowboarding". Edge (62). September 1998. p. 96.
 ^ a b Jarratt, Steve (May 2006). "What do you mean, you've never played 1080° Snowboarding". Official Nintendo Magazine. p. 19.
 ^ East, Tom (17 February 2009). "Nintendo Feature: 100 Best Nintendo Games: Part One". Official Nintendo Magazine. p. 2. Archived from the original on 18 October 2012. Retrieved 5 December 2013.
 ^ "High Scores: Top Titles in the Game Industry". Feed Magazine. 22 April 1999. Archived from the original on 8 May 1999.
 ^ a b 
United States sales: "US Platinum Videogame Chart". The Magic Box. Archived from the original on 21 April 2007. Retrieved 17 April 2007.
Japan sales: "Nintendo 64 Japanese Ranking". Japan Game Charts. Archived from the original on 3 May 2009. Retrieved 1 July 2011.
 United States sales: "US Platinum Videogame Chart". The Magic Box. Archived from the original on 21 April 2007. Retrieved 17 April 2007. Japan sales: "Nintendo 64 Japanese Ranking". Japan Game Charts. Archived from the original on 3 May 2009. Retrieved 1 July 2011. ^ Kasavin, Greg (3 December 2003). "1080 Avalanche". GameSpot. Archived from the original on 9 January 2016. Retrieved 17 April 2007.
 ^ "Virtual Console". Nintendo. Archived from the original on 12 January 2008. Retrieved 12 January 2008.
 1080° Snowboarding at Nintendo.com (archives of the original at the Internet Archive) Official website (in Japanese) v t e Super Mario Mario Kart Mario Party Dr. Mario Mario vs. Donkey Kong Mario role-playing games Mario sports Donkey Kong Luigi Wario Yoshi Animal Crossing Art Style Bit Generations Brain Age Chibi-Robo! Cruis'n Custom Robo F-Zero Fire Emblem Golden Sun Kid Icarus Kirby The Legendary Starfy The Legend of Zelda Metroid Mother Pikmin Pilotwings Pokémon Punch-Out!! Puzzle League Star Fox Super Smash Bros. Touch! Generations Wars Wii Xenoblade Chronicles 1998 video games Extreme sports video games Interactive Achievement Award winners Multiplayer and single-player video games Nintendo 64 games 1080° (video game series) Nintendo Entertainment Analysis and Development games Snowboarding video games Video games developed in Japan Virtual Console games Virtual Console games for Wii Virtual Console games for Wii U CS1 Japanese-language sources (ja) All articles with dead external links Articles with dead external links from June 2016 Wikipedia indefinitely move-protected pages Use dmy dates from March 2015 Articles using Infobox video game using locally defined parameters Articles using Wikidata infoboxes with locally defined images Articles using Video game reviews template in single platform mode Official website different in Wikidata and Wikipedia Articles with Japanese-language external links Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version العربية Català Deutsch Español فارسی Français Galego 한국어 Italiano Lietuvių Magyar Nederlands 日本語 Norsk Português Русский Српски / srpski Suomi Svenska 吴语 中文  This page was last edited on 1 November 2019, at 13:35 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  1080° Snowboarding ^ ^ ^ ^ a b c d e f g h i a b ^ a b c d ^ ^ ^ a b c a b ^ a b ^ a b ^ ^ ^ a b a b c d a b c d e a b c d e f a b ^ ^ a b a b a b ^ ^ a b ^ ^ North American cover art. Nintendo EAD Nintendo Masamichi AbeMitsuhiro Takano Shigeru Miyamoto Giles GoddardColin Reed Yoshitaka Nishikawa Kenta Nagata 1080° Nintendo 64 JP: 28 February 1998NA: 31 March 1998PAL: 9 October 1998 Racing Single-player, multiplayer Aggregate scoreAggregatorScoreMetacritic88/100[20]Review scoresPublicationScoreAllGame[21]Edge8 out of 10[22]Game RevolutionB+[7]GameSpot8.6 out of 10[23]IGN8.6 out of 10[4]Next Generation[24] Metacritic 88/100[20] AllGame [21] Edge 8 out of 10[22] Game Revolution B+[7] GameSpot 8.6 out of 10[23] IGN 8.6 out of 10[4] Next Generation [24] 
Super Mario
Mario Kart
Mario Party
Dr. Mario
Mario vs. Donkey Kong
Mario role-playing games
Mario sports
Donkey Kong
Luigi
Wario
Yoshi
 
Animal Crossing
Art Style
Bit Generations
Brain Age
Chibi-Robo!
Cruis'n
Custom Robo
F-Zero
Fire Emblem
Golden Sun
Kid Icarus
Kirby
The Legendary Starfy
The Legend of Zelda
Metroid
Mother
Pikmin
Pilotwings
Pokémon
Punch-Out!!
Puzzle League
Star Fox
Super Smash Bros.
Touch! Generations
Wars
Wii
Xenoblade Chronicles
 