
 Eric Banadinović, AM (born 9 August 1968), known professionally as Eric Bana (/ˈbænə/), is an Australian actor and comedian. He began his career in the sketch comedy series Full Frontal before gaining notice in the comedy-drama The Castle (1997). Soon after he gained critical recognition in the biographical crime film Chopper (2000). After a decade of roles in Australian TV shows and films, Bana gained Hollywood's attention for his performance in the war film Black Hawk Down (2001) and the title character in Hulk (2003). He has since played Hector in the movie Troy (2004), the lead in Steven Spielberg's historical drama and political thriller Munich (2005), Henry VIII in The Other Boleyn Girl (2008), and the villain Nero in the science-fiction film Star Trek (2009). Bana also played Henry De Tamble in The Time Traveler's Wife (2009). In 2013, he played Lt. Cmdr. Erik S. Kristensen in the war film Lone Survivor and in the following year he played police sergeant Ralph Sarchie in the horror film Deliver Us from Evil.
 An accomplished dramatic actor and comedian, he received Australia's highest film and television awards for his performances in Chopper, Full Frontal and Romulus, My Father.[1] Bana has performed across a wide spectrum of leading roles in a variety of low-budget and major studio films, ranging from romantic comedies and drama to science fiction and action thrillers.
 Bana was born in Melbourne, Australia.[2][3][4][5] His father, Ivan, was Croatian and worked as a logistics manager for Caterpillar Inc., and his German mother, Eleanor, was a hairdresser, originally from near Mannheim in Germany. He has one older brother, Anthony.[5] Bana grew up in Melbourne's Tullamarine, a suburban area on the northern edge of the city, near Melbourne's international airport, and attended Penleigh and Essendon Grammar School.[6] In a cover story for The Mail on Sunday, he told author Antonella Gambotto-Burke that his family had suffered from racist taunts, and that it had distressed him. "Wog is such a terrible word," he said.[7] He has stated: "I have always been proud of my origin, which had a big influence on my upbringing. I have always been in the company of people of European origin".[2]
 Showing acting skill early in life, Bana began doing impressions of family members at the age of six or seven, first mimicking his grandfather's walk, voice and mannerisms. In school, he mimicked his teachers as a means to get out of trouble.[8] As a teen, he watched the Mel Gibson film Mad Max (1979), and decided he wanted to become an actor.[6] However, he did not seriously consider a career in the performing arts until 1991 when he was persuaded to try comedy while working as a barman at Melbourne's Castle Hotel. His stand-up gigs in inner-city pubs did not provide him with enough income to support himself, however, so he continued his work as a barman and waiting tables.[9][10]
 In 1993, Bana made his television debut on Steve Vizard's late night talk show, Tonight Live.[6] His performance gained the attention of producers from the sketch comedy series, Full Frontal, who invited him to join the show as a writer and performer. During his four years on the show, Bana wrote much of his own material, and based some of his characters on members of his family. His impressions of Columbo, Arnold Schwarzenegger, Sylvester Stallone, and Tom Cruise made Bana popular with the show's audience.[11] This success led him to record the comedy album Out of Bounds in 1994 and to host his own television special, titled Eric, in 1996. The show, a collection of sketches featuring everyday characters, prompted him to launch a sketch comedy series The Eric Bana Show. The series, written and performed by Bana, featured skits, stand-up and celebrity guests, but failed to attract a substantial audience and was cancelled after only eight episodes due to low ratings.[12] Even so, in 1997, he received a Logie Award for "Most Popular Comedy Personality" for his work on the show.[1]
 That same year, Bana made his film debut in the Australian film The Castle, which tells the story of a Melbourne-based family's struggles to keep their home by Melbourne's airport as the airport authority force them to move. He was featured in a supporting role as Con Petropoulous, a kickboxing accountant who is the householder's son-in-law. The Castle was a surprise critical and financial success, earning A$10,326,428 at the box office in Australia.[6]
 In 1997, in spite of his lack of experience in dramatic roles, Bana was approached by director Andrew Dominik to appear in the film Chopper (2000), a biographical film based on the life of infamous Australian criminal Chopper Read. Dominik had been working on the project for five years, but was unable to find an actor to portray Read. Only after Read himself suggested Bana, having seen him perform a skit on television, did Dominik consider him for the part.[13]
 For the role, Bana shaved his head, gained thirty pounds, and spent two days with Read to perfect his mimicry. During filming he arrived on set at four in the morning and spent five hours being covered in Read's trademark tattoos.[14] In spite of the film's limited release outside of Australia, Bana's performance received positive reviews. American film critic Roger Ebert complimented Bana, stating that "in a comedian named Eric Bana the filmmakers have found, I think, a future star ... He has a quality no acting school can teach you and few actors can match. You cannot look away from him".[6][15] Chopper was a critical and financial success in Australia, and was nominated for Best Film at the Australian Film Institute Awards in 2001. Bana's performance won the Australian Film Institute Award for Best Actor.[1]
 In 2001, director Ridley Scott cast Bana as an American soldier in the film Black Hawk Down (2001). Scott, with a recommendation from Russell Crowe and impressed by Bana's performance in Chopper, did not require him to audition.[16] In the film, he played Sergeant First Class Norm 'Hoot' Hooten, an elite Delta Force soldier, who fights his way out of a battle in Mogadishu, Somalia after a mission to capture two top lieutenants of a renegade warlord goes awry. Bana shed the weight he had gained for Chopper and began an exercise regimen months before filming began. He also trained with Delta Force operators at Fort Bragg, learning to fire weapons and clear rooms.[17]
 Bana's next project was the low-budget Australian film The Nugget (2002). A comedy, the film portrays the effect of instant wealth on three working class men and was released with moderate success in Australia. Bana read the script after filming Chopper in 2000 and was drawn to it because it reminded him of his childhood and because he found its characters amusing and likeable.[18] While filming The Nugget, Bana was offered the lead role of Bruce Banner in the film adaptation of the popular Marvel Comic book series The Incredible Hulk. Only after learning of director Ang Lee's involvement in the project did he consider the role.[17] Bana admired Lee for his work on the film The Ice Storm and agreed to work on the film before the final script was complete.[19] He said he was drawn to the film because "the character of Bruce Banner had dramatic potential" and was "a fairly non-traditional superhero".[19] Hulk (2003) received mixed reviews and a moderate success at the box office, but Bana's performance was praised: Jack Matthews of the New York Daily News felt that Bana played the role of Bruce Banner "with great conviction".[20] Bana earned an Academy of Science Fiction, Fantasy & Horror Films nomination for "Cinescape Genre Face of the Future" for the film.[21]
 In 2004, Bana co-starred with Brad Pitt in the big-budget film Troy. In the film, he played Prince Hector, leader of the Trojan forces battling against the Greek warrior Achilles. The film was an international success, grossing US$364 million, with US$133 million in the US.[22]
 After the critical disappointment of Hulk and Troy, critics questioned Bana's bankability in big-budget films. He responded in Empire Magazine: "It's not like it [Hulk] was a flop. When you're on a long shoot it is a long personal investment. If I wasn't happy with the end result I'd be bloody upset, but in every case so far I've been happy. Troy could take $50 and I wouldn't regret it."[23]
 The following year, Bana co-starred with Daniel Craig and Geoffrey Rush in Steven Spielberg's controversial film Munich. Bana played Avner, a Mossad agent, who is ordered to track down and kill the Black September terrorists thought to be responsible for the massacre of Israeli athletes at the 1972 Summer Olympics.[24] The film was a critical success, and was nominated for five Academy Awards in 2006.[25] The Los Angeles Times wrote that Bana as Avner "projects a combination of sensitivity and ruthlessness and ... knows how to present a face for which worry is a new experience."[26]
 In 2006, Bana was invited to join the Academy of Motion Picture Arts and Sciences.[27] Lucky You, a romantic comedy on which Bana worked before filming Munich, was released in early 2007. In the film, he played Huck Cheever, a professional poker player who must overcome his personal problems to win a high-stakes tournament in Las Vegas. His next film was the Australian drama Romulus, My Father (2007). The film, based on Raimond Gaita's memoir of the same name, portrays a couple and their struggle in the face of adversity to raise their son. The film was a critical success, and Bana's performance earned him a second Australian Film Institute Award for Best Actor.[28] In 2007, he also returned to his Australian TV roots by appearing in hit comedy Kath & Kim as himself.
 Bana's next project was the historical drama The Other Boleyn Girl (2008). In the film he played Henry VIII of England opposite Scarlett Johansson and Natalie Portman. Bana was surprised to be offered the role and admitted that he "probably would have just passed it on without even opening it" if it had been presented to him under a different title.[29] The following year, he co-starred with Chris Pine and Zachary Quinto in the science fiction film Star Trek. In the film, Bana played Nero, a Romulan mining ship captain who attempts to exact revenge on Spock, whom he blames for the destruction of his homeworld and its inhabitants. The film was a critical success and grossed over US$380 million worldwide.[30][31]
 In August 2009, he appeared as Henry DeTamble in the film adaptation of The Time Traveler's Wife.[32] Bana also co-starred with Adam Sandler and Seth Rogen in Judd Apatow's third directorial feature, about stand-up comics, titled Funny People, marking Bana's first appearance in an American mainstream comedy.[33]
 In 2009, Bana released a self-produced and directed documentary-style film called Love the Beast. It details his personal relationship with his first car and follows his progression as a car lover. Along the way he seeks guidance and wisdom from the inner sanctum of his three lifelong friends, as well as celebrities Jay Leno, Jeremy Clarkson, and Dr. Phil.[34] In 2011, Bana appeared as ex-CIA operative Erik Heller in the action thriller film Hanna, starring alongside Saoirse Ronan and Cate Blanchett.[35] The film became another success for Bana as it opened at No. 2 at the United States box office.[36] Having read Marcus Luttrell's autobiographical account as a United States Navy SEAL, Lone Survivor, he was willing to appear in the 2013 film adaptation  regardless of which role he was offered.[37] In the film, he portrayed Lieutenant Commander Erik S. Kristensen.[38]
 The following year he starred as Ralph Sarchie, a police sergeant who investigated paranormal cases, in the supernatural horror film Deliver Us from Evil.[39] The film was released on 2 July 2014 and grossed US$87.9 million worldwide.[40] In April 2016, he starred as Frank Bonneville, a struggling radio journalist, in Ricky Gervais' Special Correspondents on Netflix.[41] In Guy Ritchie's King Arthur: Legend of the Sword (2017), Bana took on the role of Uther Pendragon, king of Britain and father of future King Arthur.[42] In 2018, Bana starred as John in the Bravo mini-series Dirty John based on the Los Angeles Times story and hit podcast.[43]
 In 1995, while working on the television series Full Frontal, Bana began dating Rebecca Gleeson, a publicist with the Seven Network and daughter of then Chief Justice of New South Wales, and later Chief Justice of Australia, Murray Gleeson.[12] They married in 1997, after Bana proposed to her on a trip to the United States, which he won from Cleo Magazine after being named their "Bachelor of the Year" in 1996.[44] Bana and Gleeson have two children, a son, Klaus (born August 1998), and a daughter, Sophia (born April 2002). They are based in Melbourne.[1] On official identity documents he still has his birth surname, Banadinović.[2]
 Bana is a motor racing enthusiast, and participates in various motor racing competitions in Australia. At the age of 14, he wanted to leave school to focus full-time on becoming a motor mechanic, but his father convinced him to complete school, advising him to avoid making his hobby a job.[45] Bana purchased his first car, a 1974 XB Ford Falcon coupé, at the age of 15 for A$1100[46] and, driving it, made his motor sport racing debut in Targa Tasmania 1996, a week-long race around Tasmania.[47] (Bana has had this car for more than a quarter century. In 2009 he released a documentary named Love The Beast. The film details the central role that racing and fixing this same car has played in his life and the lives of his friends.) In 2004, Bana purchased a Porsche 944 to compete in Australia's Porsche Challenge. Competing throughout 2004, he often finished in the top ten and in November, finished fourth at the Sandown event, a personal best.[48] On 21 April 2007 Bana crashed his 1974 XB Falcon Coupe in the Targa Tasmania rally. Neither he nor his co-driver were injured.[49] Bana appeared on the British motoring show Top Gear on 15 November 2009 as a guest for its "Star in a Reasonably Priced Car" segment.[50]
 Bana is a prominent fan of Australian rules football. His love of the sport began at a young age when his godfather took him to games to see the St Kilda Football Club, his favourite team in the Australian Football League. Bana can often be seen at AFL games when he is back in Australia.[51][52] Bana's love for St Kilda FC resulted in the club being featured in the film Funny People and in Bana's promotion of the film in 2009, notably on NBC's Late Night with Jimmy Fallon.[53] In 2010, Bana was named the "Saints Number One Ticket Holder".[54]
 Bana is an ambassador for Father Chris Riley's charity for homeless young people, Youth Off The Streets, and has appeared with Riley in advertisements to support the organisation's annual appeal.[55] Bana is also an advocate for the Mental Illness Fellowship, which works to increase the awareness of mental illness in Australia. In 2004, he appeared in several high-profile advertisements for the fellowship.[56] Bana is also active in campaigns with the Australian Childhood Foundation and the Bone Marrow Donor Institute. Since 1995, he has participated in the Motorcycle Riders Association Toy Run in Melbourne, which raises money and toys for needy children at Christmas.[57]
 In 2005, Bana narrated the documentary Terrors of Tasmania about the endangered Tasmanian devil. The film followed the life of a female Tasmanian devil called Manganinnie and discussed the incurable facial cancer which threatens the survival of the species.[58] He has also worked with the Royal Society for the Prevention of Cruelty to Animals, donating money to animal shelters in Berlin while filming Troy in 2004.[59]
 In 2007, Bana introduced the episode Some Meaning in This Life of the ABC-TV documentary series Australian Story. The episode paid tribute to Australian actress Belinda Emmett (who co-starred with Bana in the film The Nugget) and her long struggle with cancer to which she had succumbed the previous year.[60]
 
 Actor comedian Film television stand-up 1 Early life 2 Career

2.1 Beginnings
2.2 1997–2005
2.3 2005–2008
2.4 2009–present

 2.1 Beginnings 2.2 1997–2005 2.3 2005–2008 2.4 2009–present 3 Personal life

3.1 Interests and hobbies
3.2 Charitable work

 3.1 Interests and hobbies 3.2 Charitable work 4 Filmography

4.1 Film
4.2 Television
4.3 Video games

 4.1 Film 4.2 Television 4.3 Video games 5 Awards and nominations 6 References 7 External links ^ a b c d "Eric Bana". Lauren Bergman Management. 2014. Archived from the original on 28 August 2007. Retrieved 10 June 2016..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c Radoš, Ivica (12 March 2006). "Eric Bana: On the official documents I am still Eric Banadinović". Jutarnji list (in Croatian). Archived from the original on 3 March 2016. Kad na poštanskoj pošiljki vidim da piše Eric Bana, odmah pomislim kako u kuverti nije nešto važno. No, kad primim pošiljku na kojoj piše Eric Banadinović, znam da je riječ o nečemu službenom – rekao je Bana. ("When on the postal deliveries I see that it is written Eric Bana, I immediately know that in the envelope it isn't anything important. But when I receive the shipment that says Eric Banadinović, I know that it is about something official – said Bana.")
 ^ Matić, A. (10 March 2006). "Rebecca fall for me because I am – Croat". Slobodna Dalmacija (in Croatian). Eric Bana ili, preciznije, Eric Banadinović, velika australska glumačka zvijezda hrvatskih korijena,...("Eric Bana or, more precisely, Eric Banadinović, is a great Australian movie stars of Croatian descent,...")
 ^ "Eric Bana, porijekom Hrvat, novi James Bond?" [Eric Bana, a Croat in origin, the new James Bond?]. Index.hr (in Croatian). 2 August 2004.
 ^ a b Wills, Dominic. "Eric Bana – Biography". www.talktalk.co.uk. TalkTalk. Archived from the original on 26 April 2015. Retrieved 7 May 2010.
 ^ a b c d e Wills, Dominic. "Eric Bana – Biography". Tiscali Film & TV. Archived from the original on 14 February 2009. Retrieved 15 March 2008.
 ^ Gambotto-Burke, Antonella (8 February 2009). "Never mind the Hulk: Eric Bana on his 25-year obsession with 'the Beast'". Mail Online. Retrieved 8 March 2010.
 ^ "Eric Bana". Marie Claire. March 2002.
 ^ Johnson, Tony (19 June 1994). "Bana Banks on Banter". Herald Sun  Sunday TV Extra.
 ^ Houston, Melinda (29 September 2002). "Eric's Eureka". The Sun-Herald. Archived from the original on 27 October 2009. Retrieved 13 May 2006.
 ^ Devlyn, Darren. "First Impressions" . TV Weekly. 10 February 1993.
 ^ a b Halfpenny, Kate. "Under the Gun". Who Magazine. 8 August 2000.
 ^ Strickland, Christopher (July 2000). "Director's Cut: Andrew Dominik's "Chopper"". If Magazine.
 ^ "Chopping & Changing". Who Weekly. 22 October 2001.
 ^ Ebert, Roger (1 June 2001). "Review of "Chopper"". RogerEbert.com. Retrieved 14 June 2006.
 ^ Woods, Stacey (February 2002). "First Buzz: The Incredible Hulk". Elle.
 ^ a b Hopkins, Mark (April 2002). "Eric Hits Hollywood". GQ Magazine (Australian edition).
 ^ "The Incredible Rise of Eric Bana". What's on Weekly. Archived from the original on 21 March 2012. Retrieved 31 May 2006.
 ^ a b Mootram, James (14 July 2003). "Making it Big". TNT Magazine.
 ^ Mathews, Jack (20 June 2003). "Beast for the Eyes". New York Daily News. Retrieved 22 November 2014.
 ^ "Awards for Eric Bana". Internet Movie Database. Retrieved 15 March 2008.
 ^ "Troy". Box Office Mojo. Retrieved 2 June 2006.
 ^ Eimer, David (June 2004). "Heroes of Troy: Eric Bana". Empire Magazine.
 ^ "Bana Republic". The Irish Times. 20 January 2006. Archived from the original on 24 September 2015. Retrieved 1 July 2006.
 ^ Murray, Rebecca (23 December 2005). "2006 Academy Award Nominees and Winners". About.com.
 ^ Turan, Kenneth (23 December 2005). "Movie Review: 'Munich'". Los Angeles Times. Archived from the original on 2 February 2010. Retrieved 9 April 2009.
 ^ "Academy Invites 120 to Membership". Academy of Motion Picture Arts and Sciences. 5 July 2006. Archived from the original on 6 July 2006. Retrieved 22 November 2014.
 ^ "Romulus, My Father sweeps AFIs". ABC News. 7 December 2007. Retrieved 15 March 2008.
 ^ Fischer, Paul (17 February 2008). "Bana Takes on Kings and Icons". FilmMonthly.com. Retrieved 17 June 2009.
 ^ "Star Trek (2009)". Rotten Tomatoes. Retrieved 17 June 2009.
 ^ "Star Trek (2009)". Box Office Mojo. Retrieved 5 September 2009.
 ^ Flemming, Michael; McNary, Dave (17 April 2007). "New Line finds its cast on 'Time'". Variety. Retrieved 21 February 2008.
 ^ Fleming, Michael (11 June 2008). "Trio joins Judd Apatow film". Variety. Retrieved 13 June 2008.
 ^ Lamont, Tom (15 November 2009). "Eric Bana: Me and my car". The Guardian. Retrieved 7 November 2017.
 ^ Dargis, Manohla (7 April 2011). "Daddy's Lethal Girl Ventures Into the Big, Bad World". The New York Times. Retrieved 7 November 2017.
 ^ Kilday, Gregg (11 April 2011). "'Hanna' Edges Out 'Arthur' for No. 2 Box Office Spot". The Hollywood Reporter. Retrieved 7 November 2017.
 ^ Browning, Justine (2013). "Talking To The Cast Of Lone Survivor At The NYC Premiere". We Got This Covered. Retrieved 24 June 2016.
 ^ Ismay, John (24 January 2014). "Seeing My Friend Depicted in 'Lone Survivor'". The New York Times. Retrieved 24 June 2016.
 ^ Pringle, Gill (18 August 2014). "My role from hell: Eric Bana on starring in cop drama 'Deliver us From Evil'". The Independent. London. Retrieved 24 June 2016.
 ^ "Deliver Us From Evil (2014". Box Office Mojo. Retrieved 24 June 2016.
 ^ Kroll, Justin (31 October 2014). "Eric Bana and Ricky Gervais to Star in Remake of French Comedy 'Special Correspondents'". Variety.
 ^ White, James (9 October 2015). "Eric Bana Will Be Uther In Knights Of The Round Table: King Arthur". Empire. Retrieved 7 November 2017.
 ^ Villarreal, Yvonne (8 August 2018). "You liked the stories and podcast. Here's what to expect from 'Dirty John' as a Bravo scripted series - Los Angeles Times". Retrieved 15 February 2019.
 ^ Northover, Kylie (1 March 1997). "Eric's Secret Love: Going Bananas". The New Post. Archived from the original on 16 December 2010.
 ^ "Transcript of "The Tonight Show with Jay Leno"". 17 June 2003. Archived from the original on 10 May 2006. Retrieved 1 June 2006.
 ^ Hawley, Janet (5 May 2007). "Lucky Eric". The Age.
 ^ "Eric Bana Bloody Brilliant to the Targa in a 351 XB coupe". Street Machine. June 1996. Archived from the original on 16 December 2010. Retrieved 1 June 2006.
 ^ Naulty, Matt (November 2004). "2004 November: Sandown". Australian Porsche Drivers Challenge's. Archived from the original on 21 March 2012. Retrieved 12 July 2006.
 ^ "Bana crashes while competing in Australian rally". The Hollywood Reporter. 23 April 2007. Retrieved 10 June 2016.
 ^ "Episode 1". Top Gear. Season 14. 15 September 2009. BBC Two.
 ^ Kramp, Leif (24 January 2006). "Eric Bana: "Wo bleiben die leichten Stoffe?"". RP Online (in German). Archived from the original on 16 December 2007. Retrieved 12 July 2006.
 ^ Freydkin, Donna (9 January 2003). "'Gentle Giant' Bana". USA Today. Retrieved 1 July 2006.
 ^ "Video: Eric Bana teaches Jimmy Fallon footy". US Footy News. 12 August 2009. Archived from the original on 14 August 2009. Retrieved 11 September 2009.
 ^ "Bana Becomes Saints Number One". Official AFL Website of the St Kilda Football Club. 25 June 2010. Retrieved 19 June 2011.
 ^ "Eric Bana jets in from US to film a promotion for Youth Off The Streets with Father Chris Riley". The Daily Telegraph. Sydney. 1 July 2014. Retrieved 22 November 2014.
 ^ "Celebrities Support MI Fellowship's Biggest Ever Campaign" (PDF). Mental Illness Fellowship of Victoria. 11 October 2004. Retrieved 1 June 2006.
 ^ Houlihan, Liam (12 December 2004). "Toy Run 2004: Troy Boy Leads the Pack". News.com.au. Archived from the original on 16 December 2010. Retrieved 1 June 2006.
 ^ "Sympathy for the Devil". The Age. 20 January 2005. Retrieved 1 June 2006.
 ^ "Eric Visits Berlin Animal Shelter". Monthly Journal: Royal Society for the Prevention of Cruelty to Animals. 9 May 2004. Archived from the original on 16 December 2010. Retrieved 1 June 2006.
 ^ "Belinda Emmett's Aussie Story". The Daily Telegraph. 23 July 2007. Retrieved 5 August 2016.
 ^ "1997 Logie Awards". Australiantelevision.net. Archived from the original on 19 October 2013.
 ^ "AFI | AACTA | Winners & Nominees | 2000–2010 | 2000". www.aacta.org.
 ^ "2005 MTV Movie Awards". MTV (MTV Networks). Retrieved 25 July 2011. Note: Click on the 'Winners' tab.
 ^ "AFI | AACTA | Winners & Nominees | 2000–2010 | 2007". www.aacta.org.
 ^ "Teen Choice Awards 2009 nominees". Los Angeles Times. Tribune Publishing. 15 June 2009. Retrieved 21 July 2014.
 Eric Bana on IMDb Eric Bana at the TCM Movie Database  Eric Bana at AllMovie LaurenBergman.com.au  –  Bana's management v t e Bruce Spence (1972) Robert McDarra (1973) Jack Thompson (1974) Martin Vaughan (1975) Simon Burke and Nick Tate (1976) John Meillon (1977) Bill Hunter (1978) Mel Gibson (1979) Jack Thompson (1980) Mel Gibson (1981) Ray Barrett (1982) Norman Kaye (1983) John Hargreaves (1984) Chris Haywood (1985) Colin Friels (1986) Leo McKern (1987) John Waters (1988) Sam Neill (1989) Max von Sydow (1990) Hugo Weaving (1991) Russell Crowe (1992) Harvey Keitel (1993) Nicholas Hope (1994) John Lynch (1995) Geoffrey Rush (1996) Richard Roxburgh (1997) Hugo Weaving (1998) Russell Dykstra (1999) Eric Bana (2000) Anthony LaPaglia (2001) David Gulpilil (2002) David Wenham (2003) Sam Worthington (2004) Hugo Weaving (2005) Shane Jacobson (2006) Eric Bana (2007) William McInnes (2008) Anthony LaPaglia (2009) Ben Mendelsohn (2010) Daniel Henshall (2011) Chris O'Dowd (2012) Leonardo DiCaprio (2013) David Gulpilil (2014) Michael Caton (2015) Andrew Garfield (2016) Sunny Pawar (2017) Hamilton Morris (2018) BIBSYS: 4103342 BNE: XX1553121 BNF: cb141525494 (data) GND: 140496661 ISNI: 0000 0001 1479 5658 LCCN: no2002052752 NKC: xx0048611 NLP: A22590997 NTA: 230039863 SUDOC: 083316647 Trove: 1457719 VIAF: 107188336  WorldCat Identities (via VIAF): 107188336 1968 births 20th-century Australian male actors 21st-century Australian male actors Australian male comedians Australian male film actors Australian male television actors Australian male video game actors Australian male voice actors Australian people of Croatian descent Australian people of German descent Australian racing drivers Australian stand-up comedians Best Actor AACTA Award winners Living people Logie Award winners Male actors from Melbourne Male actors of German descent Members of the Order of Australia Racing drivers from Melbourne CS1 Croatian-language sources (hr) CS1 German-language sources (de) Articles with short description Use dmy dates from March 2014 Use Australian English from April 2013 All Wikipedia articles written in Australian English Pages which use embedded infobox templates with the title parameter Infobox person using residence Articles with hCards Articles needing additional references from August 2019 All articles needing additional references Commons category link from Wikidata Featured articles Wikipedia articles with BIBSYS identifiers Wikipedia articles with BNE identifiers Wikipedia articles with BNF identifiers Wikipedia articles with GND identifiers Wikipedia articles with ISNI identifiers Wikipedia articles with LCCN identifiers Wikipedia articles with NKC identifiers Wikipedia articles with NLP identifiers Wikipedia articles with NTA identifiers Wikipedia articles with SUDOC identifiers Wikipedia articles with Trove identifiers Wikipedia articles with VIAF identifiers Wikipedia articles with WorldCat-VIAF identifiers Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version العربية Azərbaycanca تۆرکجه বাংলা Bân-lâm-gú Български Català Čeština Dansk Deutsch Eesti Ελληνικά Español Esperanto Euskara فارسی Français Gaeilge Galego 한국어 Հայերեն Hrvatski Bahasa Indonesia Italiano עברית ქართული Қазақша Latviešu Magyar മലയാളം მარგალური Монгол Nederlands 日本語 Norsk Occitan Polski Português Română Русский Scots සිංහල Simple English Slovenščina Српски / srpski Srpskohrvatski / српскохрватски Suomi Svenska தமிழ் ไทย Türkçe Türkmençe Українська 中文  This page was last edited on 24 September 2019, at 17:58 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Comedy career Eric Banadinović Eric Bana does not cite any sources · · · · a b c d a b c ^ ^ a b a b c d e ^ ^ ^ ^ ^ a b ^ ^ ^ ^ a b ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ Eric Bana Bana at the 2009 Tribeca Film Festival Eric Banadinović (1968-08-09) 9 August 1968 (age 51)Melbourne, Victoria, Australia Melbourne, Victoria, Australia Australian Actorcomedian 1993–present Rebecca Gleeson (m. 1997) 2 Comedy career Filmtelevisionstand-up   This section does not cite any sources. Please help improve this section by adding citations to reliable sources. Unsourced material may be challenged and removed.Find sources: "Eric Bana" – news · newspapers · books · scholar · JSTOR (August 2019) (Learn how and when to remove this template message) 1997 The Castle Con Petropoulous 
 2000 Chopper Mark "Chopper" Read 
 2001 Black Hawk Down Norm "Hoot" Gibson 
 2002 The Nugget Lotto 
 2003 Hulk BruceBanner /Hulk 
 Finding Nemo Anchor Voice role
 2004 Troy Hector 
 2005 Munich Avner Kaufman 
 2007 Lucky You Huck Cheever 
 Romulus, My Father Romulus Gaita 
 2008 The Other Boleyn Girl Henry Tudor 
 2009 Mary and Max Damien Popodopolous Voice role
 Love the Beast Himself Documentary; also producer and director
 Star Trek Nero 
 The Time Traveler's Wife Henry DeTamble 
 Funny People Clarke 
 2011 Hanna Erik Heller 
 2012 Deadfall Addison 
 2013 Closed Circuit Martin Rose 
 Lone Survivor Erik S. Kristensen 
 2014 Deliver Us from Evil Ralph Sarchie 
 2016 The Finest Hours Daniel Cluff 
 Special Correspondents Frank Bonneville 
 The Secret Scripture Dr. William Grene 
 2017 King Arthur: Legend of the Sword Uther Pendragon 
 2018 The Forgiven Piet Blomfeld 
 2019 The Dry Aaron Falk Filming
 1993–1996 Full Frontal Various 66 episodes
 1996–1997 The Eric Bana Show Live Various 17 episodes
 1999–2000 All Saints Rob Biletsky 3 episodes
 2000–2001 Something in the Air Joe Sabatini 202 episodes
 2007 Kath & Kim Himself S4: Ep. 2
 2009 Top Gear Himself S14: Ep. 1
 2018
 The Joel McHale Show With Joel McHale
 Himself
 Episode:"Roller Coaster?'"
 Dirty John
 John Meehan
 Main role
 2003 Hulk Bruce Banner Based on the 2003 film of the same name
 1997
 Logie Awards
 Most Popular Comedy Personality
 Full Frontal
 Won
 [61]
 2000
 Australian Film Institute
 Best Actor in a Leading Role
 Chopper
 Won
 [62]
 2004
 MTV Movie Awards
 Best Fight (with Brad Pitt)
 Troy
 Nominated
 [63]
 2007
 Australian Film Institute
 Best Actor in a Leading Role
 Romulus, My Father
 Won
 [64]
 2009
 Teen Choice Awards
 Choice Movie Villain
 Star Trek
 Nominated
 [65]
  Wikimedia Commons has media related to Eric Bana. 
Bruce Spence (1972)
Robert McDarra (1973)
Jack Thompson (1974)
Martin Vaughan (1975)
Simon Burke and Nick Tate (1976)
John Meillon (1977)
Bill Hunter (1978)
Mel Gibson (1979)
Jack Thompson (1980)
Mel Gibson (1981)
Ray Barrett (1982)
Norman Kaye (1983)
John Hargreaves (1984)
Chris Haywood (1985)
Colin Friels (1986)
Leo McKern (1987)
John Waters (1988)
Sam Neill (1989)
Max von Sydow (1990)
Hugo Weaving (1991)
Russell Crowe (1992)
Harvey Keitel (1993)
Nicholas Hope (1994)
John Lynch (1995)
Geoffrey Rush (1996)
Richard Roxburgh (1997)
Hugo Weaving (1998)
Russell Dykstra (1999)
Eric Bana (2000)
Anthony LaPaglia (2001)
David Gulpilil (2002)
David Wenham (2003)
Sam Worthington (2004)
Hugo Weaving (2005)
Shane Jacobson (2006)
Eric Bana (2007)
William McInnes (2008)
Anthony LaPaglia (2009)
Ben Mendelsohn (2010)
Daniel Henshall (2011)
Chris O'Dowd (2012)
Leonardo DiCaprio (2013)
David Gulpilil (2014)
Michael Caton (2015)
Andrew Garfield (2016)
Sunny Pawar (2017)
Hamilton Morris (2018)
 
BIBSYS: 4103342
BNE: XX1553121
BNF: cb141525494 (data)
GND: 140496661
ISNI: 0000 0001 1479 5658
LCCN: no2002052752
NKC: xx0048611
NLP: A22590997
NTA: 230039863
SUDOC: 083316647
Trove: 1457719
VIAF: 107188336
 WorldCat Identities (via VIAF): 107188336
 