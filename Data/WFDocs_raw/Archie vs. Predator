
 Archie vs. Predator is a comic book and intercompany crossover, written by Alex de Campi and drawn by Fernando Ruiz. It was originally published as a four-issue limited series in the United States by Dark Horse Comics and Archie Comics in 2015. The single issues were released between April and July, a hardcover collection went on sale in November 2015, and a paperback collection became available in August 2019.
 Following in a long tradition of all-American teenager Archie Andrews meeting unusual celebrities and pop culture icons, this comic book shows him meeting the galaxy's deadliest hunter, the Predator. The idea was first suggested in the Archie office and then proposed to Dark Horse, who holds the license to comics featuring the Predator character owned by 20th Century Fox. The companies paired de Campi, a horror writer at Dark Horse, with Ruiz, a regular Archie artist. When the comic book was announced, many media outlets noted in their headlines that the new title was not a joke.
 In Archie vs. Predator, a trophy-hunting alien arrives on Earth and begins stalking high school student Archie Andrews and his classmates. After a number of teenagers have been murdered, the survivors realize they are being hunted and decide to fight back.
 The book received positive reviews from critics, who enjoyed the strange crossover matchup and dark humor. The miniseries was the bestselling book for both publishers during its release and won a Ghastly Award for Best Limited Series. A sequel, Archie vs. Predator II, began in July 2019 written by de Campi and illustrated by Robert Hack.[1]
 Since Archie Comics partnered with Marvel Comics for the 1994 one-shot comic Archie Meets the Punisher, the Archie characters have had a tradition of team-ups with characters from other fictional universes.[2][3] Following the success of Archie meets KISS in 2012 and Archie meets Glee in 2013, the Archie Creative Summit held in March 2014 included a brainstorming session for new crossovers.[4][5] Godzilla and Friday the 13th were considered, but the Predator received the most support. Artist Fernando Ruiz was at the summit and liked the premise, but did not believe it would actually be made, or that he would be involved if it was.[5][6]
 Archie Comics proposed the crossover to 20th Century Fox, who owns the Predator character, and Dark Horse Comics, who had licensed the character for comic books.[7] Both quickly agreed. Dark Horse approached Alex de Campi to write based on her work on the horror comic book Grindhouse and the humorous comic book My Little Pony: Friends Forever.[4][6][8] Dark Horse editor Brendan Wright, who was already editing the regular Predator comic book and had previously coordinated with Archie Comics on Dark Horse's Archie Archives reprints, was chosen as editor. Ruiz was the first and only artist considered to pencil the series, and inker Rich Koslowski was selected based on his work with Ruiz on a Thor parody in Archie #648.[6] The project was originally titled Archie meets Predator, but it was changed to vs. because the people involved in production kept calling it that unintentionally, and to parody the Alien vs. Predator franchise.[9]
 To prepare for the project, de Campi read over 4000 pages of Archie comic books.[10] She drew inspiration for the story from the 1940s era of the series, when she says Archie Andrews' girlfriends Betty Cooper and Veronica Lodge had "more edge".[11] Her scripts were subject to notes and tweaks from Archie Comics and Fox, but de Campi described both licensors as "extremely mellow, pleasant and easy to work with".[10] Fox's primary concern was why the Predator, who is known for choosing challenging prey, would be interested in teenagers, but a suitable explanation was included in the story.[6] She expected some of the notes, such as being told, "Alex, you can't make jokes about child predation in an Archie book".[9] Other times, she was surprised when she did not receive notes, such as her use of emojis for the Predator's speech.[9] She was pleased when she got to add depth to Dilton Doiley, a character she feels is often overlooked in regular Archie comics.[4][10][12] The choice of whether Archie Andrews would survive the series or not was changed several times during scripting, and a final decision was not made until the last draft of the script.[6]
 Ruiz experimented with drawing the whole series in a more realistic manner, but from the beginning the plan was to use the traditional Archie design style for the book, which Archie veteran Dan DeCarlo popularized in the classic comics since the 1960s.[5][6][13] The editorial team felt this would make the story feel more "bizarre and unsettling". Although most modern comic art is created using digital tools and scans to accelerate the creation process, the art team for Archie vs. Predator chose to use traditional methods and used FedEx to get the physical copy of each page to the person who would complete the next step in the process.[6]
 Archie Comics and Dark Horse Comics jointly announced Archie vs. Predator and its creative team at the 2014 New York Comic Con.[2] Soon after, many media outlets carrying the news used a headline indicating the bizarre announcement was not a joke.[14][15][16] Wright described it as his "most nutso project" and was continually surprised by the scale of positive reaction to it.[6][17]
 A promotional ashcan comic was released a couple of months before the first issue. It featured eight pages from the first issue and some of Ruiz's character designs, including some of his initial, more lifelike Predator sketches.[5] The four issues were published monthly beginning April 15, 2015, and ending July 22, 2015.[18][19] Each issue had two variant covers, and the first issue had some additional store-exclusive and convention-exclusive covers.[20] All of the issues also included a one-page, humorous bonus strip written by de Campi featuring crossovers of other Archie and Dark Horse characters: Sabrina Meets Hellboy (art by Robert Hack), Little Mask and his Pals (art by Art Baltazar), Jughead Meets MIND MGMT: "S" is for Sleeper (art by Matt Kindt), and Josie and the Pussycats Meet Finder (art by Carla Speed McNeil).[21] A 128-page hardcover collecting the series and bonus content was released November 4, 2015,[22] followed by a 104-page softcover edition on August 21, 2019.[23]
 When Archie and his friends take a spring break trip to Los Perdidos Resort, an alien, trophy-hunting Predator touches down in the nearby jungle. Betty and Veronica get into a fight during a contest and attract the attention of the Predator, who follows them home to Riverdale.[24] While targeting Veronica, the Predator also kills several armed people. The creature's picture is taken by Betty. Kevin Keller's father, a US Military General, identifies it as a teenager from a race of nearly unstoppable alien hunters. The Kellers organize Archie's friends, and Jughead Jones is used as bait to lure the Predator, but the trap fails and the Predator kills most of the ambush party.[25]
 Some members of the group decide to split up in hopes the Predator will continue to pursue Veronica, allowing them to escape, while Archie, Betty, Jughead and Dilton stay with Veronica. When the Predator attacks again, it kills Dilton and Jughead and critically wounds Archie.[26] Betty and Veronica take refuge at Lodge Manor, where they hook Archie to an experimental healing machine. When the Predator attacks, Archie is killed and Veronica and Betty are critically wounded, which causes the Predator to show regret since it was motivated by a crush on Betty. Veronica recovers to find that Betty has used the healing machine to restore them both to full health and is now using it to transform the Predator into an Archie look-alike.[27]
 The first issue was the best-selling issue for both publishers in April 2015, and the 74th best selling comic book among other releases that month.[28] Sales numbers fell just under 30% through the fourth issue, which was the 102nd best selling issue in July.[29] When the hardcover was released in November, it was the 36th best-selling comics collection of the month according to Diamond Comic Distributors.[30]
 The first issue garnered an average review rating of 7.9 out of 10 according to the review aggregator Comic Book Roundup.[31] Writing for The Guardian, Graeme Virtue said the "appealingly strange" series fit with Archie Comics' reputation for pushing comics forward and praised it for remaining true to both the Archie and Predator properties.[32] The book's "deeply funny dark humor" was a selling point for Robin Parrish at Tech Times,[33] and IGN reviewer Jeff Lake described the first issue as "pretty darn great".[18] The use of the traditional cartoon art style to depict gruesome scenes was described as "a visual loss of innocence" by Paste magazine.[34] Comic Bastards' Dustin Cabeal called the miniseries "a hoot".[19] The series won the 2015 Ghastly Award for Best Limited Series.[35]
 Dark Horse Comics Archie Comics Archie Andrews Predator 1 Publication history

1.1 Development
1.2 Publication

 1.1 Development 1.2 Publication 2 Synopsis 3 Reception 4 References 5 External links ^ Burog, Vianne (April 5, 2019). "Archie vs Predator 2: The Predator is Coming to Riverdale". Latin Times. Retrieved April 29, 2019.
 ^ a b C., Ron (October 9, 2014). "Archie Comics And Dark Horse Unveil The Unthinkable: Archie Meets Predator". Archie Comics. Retrieved August 6, 2015.
 ^ Aguirre-Sacasa, Roberto (introduction). Alex de Campi (w), Fernando Ruiz (a). Archie vs. Predator (2015), Milwaukie, Oregon: Dark Horse Comics, ISBN 1616558059
 ^ a b c Anderson, Derek (July 14, 2015). "Comic-Con 2015 Interview: Alex de Campi Talks Archie VS. Predator". Daily Dead. Retrieved March 4, 2016.
 ^ a b c d Sims, Chris (March 6, 2015). "'I'm Drawing Archie Fighting A Freakin' Predator': Fernando Ruiz Talks 'Archie Vs Predator'". Comics Alliance. Archived from the original on March 11, 2016. Retrieved March 4, 2016.
 ^ a b c d e f g h Wright, Brendan (afterword). Alex de Campi (w), Fernando Ruiz (a). Archie vs. Predator (2015), Milwaukie, Oregon: Dark Horse Comics, ISBN 1616558059
 ^ Sunu, Steve (November 26, 2014). "Exclusive: Richardson Looks to Dark Horse's 2015 Future". Comic Book Resources. Retrieved February 21, 2017.
 ^ Emm, Fox (January 7, 2016). "Exclusive Interview: Alex de Campi on Horror Comics". Wicked Horror. Retrieved March 4, 2016.
 ^ a b c Siegel, Lucas (April 14, 2015). "Exclusive: Archie Vs. Predator's Alex de Campi Promises "Horribly Gory, Very Wrong, and Funny" Story". ComicBook.com. Retrieved March 4, 2016.
 ^ a b c Sims, Chris (April 28, 2015). "They're all a little unbalanced: Alex de Campi talks 'Archie vs. Predator'". Comics Alliance. Archived from the original on February 19, 2016. Retrieved March 4, 2016.
 ^ Truitt, Brian (October 9, 2014). "Archie Meets Predator a mash-up for the ages". USA Today. Retrieved February 17, 2016.
 ^ Moccio, Michael (July 15, 2015). "SDCC 2015: Archie vs. Predator Writer Alex de Campi on the Onslaught in Riverdale". Emertainment Monthly. Archived from the original on March 10, 2016. Retrieved March 4, 2016.
 ^ "Dan DeCarlo Dead at Age of 82: Artist Defined Archie Comics Style for Decades". Comic Book Resources. December 20, 2001. Archived from the original on October 10, 2012. Retrieved March 19, 2013.
 ^ Arrant, Chris (October 9, 2014). "Archie vs. Predator ... No, Really". Newsarama. Retrieved March 2, 2016.
 ^ Bonner, Jerry (January 14, 2015). "'Archie vs. Predator' Is a Real Thing and It's Coming Soon". Headlines and Global News. Retrieved March 2, 2016.
 ^ MacDonald, Heidi (October 9, 2014). "Archie vs... Predator?". The Beat. Retrieved March 2, 2016.
 ^ Means-Shannon, Hannah (September 1, 2015). "Brendan Wright, Editor Of Mind MGMT, Grindhouse, Archie vs Predator, leaves Dark Horse". Bleeding Cool News. Retrieved March 2, 2016.
 ^ a b Lake, Jeff (April 15, 2015). "Archie vs. Predator #1 Review". IGN. Retrieved November 9, 2015.
 ^ a b Cabeal, Dustin (July 22, 2015). "Review: Archie vs. Predator #4". Comic Bastards. Retrieved November 9, 2015.
 ^ From the Variant Cover Gallery. de Campi, Alex (w), Fernando Ruiz (a). Archie vs. Predator (2015), Milwaukie, Oregon: Dark Horse Comics, ISBN 1616558059
 ^ Silber, Gregory Paul (November 5, 2015). "Is It Good? Archie vs. Predator HC Review". Adventures in Poor Taste. Retrieved May 25, 2016.
 ^ "Archie vs. Predator HC" (Press release). Dark Horse Comics. Retrieved February 18, 2016.
 ^ "Archie vs. Predator TPB" (Press release). Dark Horse Comics. Retrieved August 19, 2019.
 ^ de Campi, Alex (w), Ruiz, Fernando (a). "Part 1" Archie vs. Predator 1 (April 2015), Dark Horse Comics, Archie Comics
 ^ de Campi, Alex (w), Ruiz, Fernando (a). "To Live and Die in a Small Town" Archie vs. Predator 2 (May 2015), Dark Horse Comics, Archie Comics
 ^ de Campi, Alex (w), Ruiz, Fernando (a). "Part 3" Archie vs. Predator 3 (June 2015), Dark Horse Comics, Archie Comics
 ^ de Campi, Alex (w), Ruiz, Fernando (a). "Guess Who's Coming To Dinner" Archie vs. Predator 4 (July 2015), Dark Horse Comics, Archie Comics
 ^ By units sold. Miller, John Jackson. "Comic Book Sales Figures for April 2015". The Comics Chronicles. Retrieved February 17, 2016.
 ^ By units sold.  Miller, John Jackson. "Comic Book Sales Figures for July 2015". The Comics Chronicles. Retrieved February 17, 2016.
 ^ By units sold.  Miller, John Jackson. "Comic Book Sales Figures for November 2015". The Comics Chronicles. Retrieved February 17, 2016.
 ^ Based on 42 reviews from critics. "Archie vs. Predator #1 Reviews". Comic Book Roundup. Archived from the original on March 8, 2016. Retrieved February 16, 2016.
 ^ Virtue, Grame (April 18, 2015). "Archie Vs Predator, Convergence, The Infinite Loop: the month in comics". The Guardian. Retrieved March 2, 2016.
 ^ Parrish, Robin (April 15, 2015). "New Comic Book Day April 15, 2015: Archie Battles Predator And DC's 'Convergence' Continues". Tech Times. Retrieved March 2, 2016.
 ^ Brown, Billy (April 15, 2015). "Archie vs. Predator #1 by Alex de Campi & Fernando Ruiz Review". Paste. Retrieved September 25, 2018.
 ^ Royer, Decapdan (March 2016). "Ghastly Award Winners". Ghastly Awards. Retrieved March 8, 2016.
 Archie vs. Predator at the Grand Comics Database v t e Mighty Comics/Red Circle Comics/Dark Circle Comics Archie Horror Archie (2 volumes) Little Archie Life with Archie (2 volumes) Archie and Me Everything's Archie Archie at Riverdale High Archie Meets the Punisher Afterlife with Archie Archie vs. Predator Archie vs. Predator II Archie vs. Sharknado Archie (comic strip) Archie's Girls Betty and Veronica Archie's Pal Jughead Comics (2 volumes) Jughead (3 volumes) Jughead's Double Digest Archie's Rival Reggie  Reggie  Reggie and Me Laugh Comics (2 volumes) Pep Comics  Archie's Pals 'n' Gals  Archie's Mad House  Archie Giant Series  Laugh Comics Digest  Archie's TV Laugh-Out  Archie Americana Series  Archie's Holiday Fun Digest  Tales from Riverdale Josie and The Pussycats (2 volumes) Sabrina the Teenage Witch (2 volumes) Sabrina Chilling Adventures of Sabrina New Riverdale Harley & Ivy Meet Betty & Veronica Archie and Katy Keene Sonic the Hedgehog
issues issues  Knuckles the Echidna  Sonic X  Sonic Universe Teenage Mutant Ninja Turtles Adventures (3 volumes) Mighty Mutanimals (2 volumes) Wild West C.O.W.-Boys of Moo Mesa (2 volumes) Street Sharks (2 volumes) Nights into Dreams Mega Man Blue Ribbon Comics (2 volumes) Top-Notch Comics  Shield-Wizard Comics  Zip Comics  Hangman Comics  Black Hood Comics  The Adventures of The Fly  Adventures of the Jaguar  Fly Man Mighty Crusaders (2 volumes) Black Hood  The Comet Pep Comics  Jackpot Comics Super Duck Comics  Wilbur Comics Katy Keene (2 volumes) Li'l Jinx  Cosmo the Merry Martian  That Wilkin Boy "Archie Marries Veronica/Archie Marries Betty" "Bad Boy Trouble" "Love Showdown" See also: List of Sabrina the Teenage Witch books, CDs and DVDs Spire Christian Comics v t e Predator (1987) Predator 2 (1990) Predators (2010) The Predator (2018) Predator Predator 2 Predators Batman Versus Predator Predator: Bad Blood Tarzan vs. Predator: At the Earth's Core Predator vs. Judge Dredd Superman vs. Predator Archie vs. Predator Archie vs. Predator II Predator: Incursion Predator (1987) Predator 2 (1990) Predator 2 (1992) Predator: Concrete Jungle (2005) Predator: Hunting Grounds (2020) Predator Accolades for the film series Alien vs. Predator franchise Predatoroonops  Category Dark Horse Comics limited series 2015 comics debuts 2015 comics endings Archie Comics titles Dark Horse Comics titles Intercompany crossovers Predator (franchise) comics Articles with short description Featured articles Title pop Comics navigational boxes purge Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version  This page was last edited on 1 October 2019, at 04:07 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Archie vs. Predator ^ a b ^ a b c a b c d a b c d e f g h ^ ^ a b c a b c ^ ^ ^ ^ ^ ^ ^ a b a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ See also: Cover of Archie vs. Predator #1; art by Fernando Ruiz 
Dark Horse Comics
Archie Comics
 April – July 2015 4 
Archie Andrews
Predator
 Alex de Campi Fernando Ruiz Fernando Ruiz Rick Koslowski John Workman Jason Millet .mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}ISBN 1616558059 ISBN 1506714668 
Mighty Comics/Red Circle Comics/Dark Circle Comics
Archie Horror
 
Archie (2 volumes)
Little Archie
Life with Archie (2 volumes)
Archie and Me
Everything's Archie
Archie at Riverdale High
Archie Meets the Punisher
Afterlife with Archie
Archie vs. Predator
Archie vs. Predator II
Archie vs. Sharknado
Archie (comic strip)
Archie's Girls Betty and Veronica
Archie's Pal Jughead Comics (2 volumes)
Jughead (3 volumes)
Jughead's Double Digest
Archie's Rival Reggie
 Reggie
 Reggie and Me
Laugh Comics (2 volumes)
Pep Comics
 Archie's Pals 'n' Gals
 Archie's Mad House
 Archie Giant Series
 Laugh Comics Digest
 Archie's TV Laugh-Out
 Archie Americana Series
 Archie's Holiday Fun Digest
 Tales from Riverdale
Josie and The Pussycats (2 volumes)
Sabrina the Teenage Witch (2 volumes)
Sabrina
Chilling Adventures of Sabrina
New Riverdale
Harley & Ivy Meet Betty & Veronica
Archie and Katy Keene
 Sonic the Hedgehog
Sonic the Hedgehog
issues
 Knuckles the Echidna
 Sonic X
 Sonic Universe
Teenage Mutant Ninja Turtles
Teenage Mutant Ninja Turtles Adventures (3 volumes)
Mighty Mutanimals (2 volumes)
Other
Wild West C.O.W.-Boys of Moo Mesa (2 volumes)
Street Sharks (2 volumes)
Nights into Dreams
Mega Man
 
Sonic the Hedgehog
issues
 Knuckles the Echidna
 Sonic X
 Sonic Universe
 
Teenage Mutant Ninja Turtles Adventures (3 volumes)
Mighty Mutanimals (2 volumes)
 
Wild West C.O.W.-Boys of Moo Mesa (2 volumes)
Street Sharks (2 volumes)
Nights into Dreams
Mega Man
 
Blue Ribbon Comics (2 volumes)
Top-Notch Comics
 Shield-Wizard Comics
 Zip Comics
 Hangman Comics
 Black Hood Comics
 The Adventures of The Fly
 Adventures of the Jaguar
 Fly Man
Mighty Crusaders (2 volumes)
Black Hood
 The Comet
 
Pep Comics
 Jackpot Comics
 
Super Duck Comics
 Wilbur Comics
Katy Keene (2 volumes)
Li'l Jinx
 Cosmo the Merry Martian
 That Wilkin Boy
 
"Archie Marries Veronica/Archie Marries Betty"
"Bad Boy Trouble"
"Love Showdown"
 
See also: List of Sabrina the Teenage Witch books, CDs and DVDs
Spire Christian Comics
 
Predator (1987)
Predator 2 (1990)
Predators (2010)
The Predator (2018)
 Soundtracks
Predator
Predator 2
Predators
Comics
Batman Versus Predator
Predator: Bad Blood
Tarzan vs. Predator: At the Earth's Core
Predator vs. Judge Dredd
Superman vs. Predator
Archie vs. Predator
Archie vs. Predator II
Novels
Predator: Incursion
Games
Predator (1987)
Predator 2 (1990)
Predator 2 (1992)
Predator: Concrete Jungle (2005)
Predator: Hunting Grounds (2020)
 
Predator
Predator 2
Predators
 
Batman Versus Predator
Predator: Bad Blood
Tarzan vs. Predator: At the Earth's Core
Predator vs. Judge Dredd
Superman vs. Predator
Archie vs. Predator
Archie vs. Predator II
 
Predator: Incursion
 
Predator (1987)
Predator 2 (1990)
Predator 2 (1992)
Predator: Concrete Jungle (2005)
Predator: Hunting Grounds (2020)
 Characters
Predator
 
Predator
 
Accolades for the film series
Alien vs. Predator franchise
 
Predatoroonops
 
 Category
 