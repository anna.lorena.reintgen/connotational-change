
 Resident Evil: Apocalypse is a 2004 action horror film[9] directed by Alexander Witt and written by Paul W. S. Anderson. A direct sequel to Resident Evil (2002), it is the second installment in the Resident Evil film series, which is loosely based on the video game series of the same name. The film marks Witt's feature directorial debut; Anderson, the director of the first film, turned down the job due to other commitments, though stayed on as one of its producers. Milla Jovovich reprises her role as Alice, and is joined by Sienna Guillory as Jill Valentine and Oded Fehr as Carlos Oliveira.
 Resident Evil: Apocalypse is set directly after the events of the first film, where Alice escaped from an underground facility overrun by zombies. She now bands together with other survivors to escape the zombie outbreak which has spread to the nearby Raccoon City. The film borrows elements from several games in the Resident Evil series, such as the characters Valentine and Oliveira and the villain Nemesis. Filming took place in Toronto at locations including Toronto City Hall and Prince Edward Viaduct.
 Resident Evil: Apocalypse received mostly negative reviews from critics who complained about the plot; however, the film garnered praise for its action sequences. It is the lowest-rated of the six films in the Resident Evil series on Rotten Tomatoes, with an approval rating of 21%. Earning $129 million worldwide on a $45 million budget, it surpassed the box office gross of the original film. It was followed by Resident Evil: Extinction in 2007.
 In the previous film, former security operative Alice and environmental activist Matt Addison fought to escape an underground genetic research facility called the Hive, the source of a zombie outbreak. The pair were part of an attempt to expose illegal experiments being performed there by pharmaceutical company Umbrella Corporation. The film ended as Alice and Addison were taken into custody by Umbrella.
 Umbrella sends a team to the Hive to investigate what happened there; the team is overrun by zombies who quickly spread to the nearby Raccoon City. Umbrella responds by quarantining the city and evacuating their personnel from it. Angela Ashford, the daughter of Umbrella researcher Dr. Charles Ashford, goes missing after her security car is involved in a collision while being extradited from the city. Meanwhile, disgraced Raccoon City Police Department Special Tactics And Rescue Squad (STARS) operative Jill Valentine returns to her former precinct to suggest her fellow officers that they evacuate Raccoon City. Alice awakens in a deserted hospital and wanders the city in search of supplies, while Umbrella uses the only bridge out of the area to evacuate civilians. At the bridge, Valentine meets with her former partner, Sgt. Payton Wells. A zombie bites and infects Wells. In response to the virus reaching the bridge, Major Timothy Cain, leader of the Umbrella forces in Raccoon City, seals the exit and forces the residents to return to the city.
 After being abandoned by their employer following a failed attempt to rescue a civilian, Umbrella soldiers Carlos Oliveira and Nicholai Ginovaef team up with the surviving STARS operatives to repel zombie attacks. Their position is overrun, and Oliveira is bitten and infected. At a separate location, Valentine, Wells, and news reporter Terri Morales, are about to be overrun, though they are saved by Alice. Umbrella deploys a heavily mutated experimental supersoldier, Nemesis, who kills the remaining STARS members before searching for Alice. Dr. Ashford hacks into the city's CCTV system and uses it to contact Alice and the other survivors, offering to arrange their evacuation from the city in exchange for help in locating his daughter. He makes an identical offer to Oliveira and Ginovaef, and explains that Umbrella intends to rid Raccoon City of the zombie infection by destroying it using a nuclear bomb.
 Alice and the others head to Angela's location, where they are ambushed by Nemesis. Valentine kills Wells after he turns into a zombie. Alice engages Nemesis but is wounded and forced to retreat separately, luring Nemesis away from the rest of the group. Valentine and Morales continue, picking up stranded civilian L.J. en route. Valentine meets Oliveira and they find and rescue Angela, although Morales and Ginovaef are both killed. Angela reveals that the zombie outbreak is the result of a virus created by her father to treat the genetic disease from which she suffers: only by regularly injecting herself with the virus is Angela able to survive, though she must also take anti-virus serum to prevent turning into a zombie. Alice uses some of the anti-virus to cure Oliveira. Dr. Ashford gives Alice the location of an extraction point where a helicopter awaits. The group makes it to the rendezvous point, but are ambushed by Umbrella forces. Major Cain kills Dr. Ashford and forces Alice to fight Nemesis. Alice gains the upper hand over the supersoldier, though she ceases fighting after realizing that he is Matt Addison, mutated by Umbrella's experiments.
 Nemesis turns on Major Cain and attacks the Umbrella troops, but is killed protecting Alice. The rest of the survivors seize the helicopter; they eject Major Cain from it and he gets killed by the zombies, including a zombified Dr. Ashford. As the survivors escape, the nuclear bomb detonates over the city, and the resulting blast wave causes the helicopter to crash. Alice sacrifices herself to save Angela and is impaled on a metal pole. TV footage attributes the nuclear attack to a meltdown of the city's power plant, covering up Umbrella's involvement. Alice wakes up in an Umbrella research facility and escapes with help from Oliveira, Valentine, L.J., and Angela. As they are escaping, Dr. Alexander Isaacs, a high-ranking Umbrella employee, reveals that Alice's escape is part of Umbrella's plan for her.
 Media studies scholar Stephen Harper said that both Apocalypse and the first Resident Evil film present "highly ambiguous" perspectives on the themes of corporate power, race, gender and sexuality. Describing them both as postmodern and postfeminist texts, Harper argued that, despite containing some progressive elements including feminist themes that undermine patriarchal power, the films also played into several stereotypes. He said the relationship between Alice and Valentine differs from interactions between male characters in action films as seen by a lack of camaraderie and co-operation between the two and, unlike male characters in Apocalypse, both Valentine and Alice are separately shown being "protective and nurturing" of the young Angela; Harper stated even violent action heroines are often portrayed with such characteristics. Harper also criticized how their revealing clothing and camera angles objectified Alice and Valentine throughout the film, and noted that through the African-American character L.J. Apocalypse showed an "ironic awareness" of racist stereotypes, though "it stops short of challenging them and, indeed, often deploys them".[10]
 Douglas Kellner from the University of California in Los Angeles argued the film's ending played "on fears of out of control nuclear technology and government cover-ups". A news segment shown in the film, which claimed that reports of corporate wrongdoing were false and that people should instead be thanking the Umbrella Corporation, was "a barely disguised allegory of lying by corporations and the state during the Bush-Cheney era".[11]
 While promoting the first Resident Evil film in late February 2002, Milla Jovovich and director Paul W. S. Anderson discussed a potential sequel. Anderson said he began writing the screenplay for the second film after completing the first, and had plans for Alice to meet up with Jill Valentine. Jovovich confirmed her character would return in the sequel if the first film was successful.[12] In early March, Eric Mabius, who played Matt Addison in the first film, stated a sequel was confirmed, would be set in Raccoon City, and would feature the Nemesis character.[13] The sequel was officially greenlit by Sony Pictures in mid-2002 but Anderson chose not to direct due to his commitments to Alien vs. Predator (2004). He stayed on as the film's screenwriter and as one of its producers.[14] Anderson used the game Resident Evil 3: Nemesis as the basis of the story and wrote in elements from his favorite films, such as the perimeter wall in Escape from New York and the deserted city in The Omega Man.[14] Alexander Witt was hired to direct the film, marking his feature film directorial debut. The script had already been completed when Witt was hired. He made some suggestions to Anderson and fellow producer Jeremy Bolt, which resulted in some minor script changes.[15]
 Jovovich was the only person to reprise a role from the first film. It was initially reported that Mabius would be reprising his role as Matt Addison in the form of portraying Nemesis,[13][16] but the part ended up going to Matthew G. Taylor. Mabius still appears via stock footage from the previous film used in a flashback scene.[17] The original film did not feature any characters from the games, but it had always been the intention to add several to Apocalypse.[18] Gina Philips was originally to appear in the film portraying the character Claire Redfield, but she eventually turned down the role which was then given to Emily Bergl, who left before production began. The character was dropped,[19] but did appear in the film's sequel, Resident Evil: Extinction (2007), portrayed by Ali Larter. For the role of Valentine, Anderson first considered Natasha Henstridge, but she was unavailable;[16] he then considered Mira Sorvino though she declined.[20] The role eventually went to Sienna Guillory who prepared for the role by studying Valentine's movements and posture in the games.[21][22] The role of L.J. was written specifically for Snoop Dogg, though he dropped out of production and was replaced by Mike Epps; the character was rewritten to suit Epps's personality.[14] Evanescence guitarist Ben Moody was given a cameo as a zombie Ginovaef kills.[21]
 The film was shot in Ontario, Canada; Toronto and its surrounding suburbs stood in for Raccoon City.[23] Cinematography was performed by Christian Sebaldt and Derek Rogers,[7] and filming took place at 47 locations.[18] Very few sets were made for the film.[14] Several city blocks were closed down and the Prince Edward Viaduct bridge was closed for three days so scenes could be filmed on it. Scenes were shot outside Toronto City Hall for two weeks.[24] Jovovich and Matthew Taylor spent several hours a day for six weeks practicing martial arts together for the fight scene between Alice and Nemesis.[25] The fight was originally scripted to appear in a train station and focus heavily on interaction with props, though was eventually shot in an open space outside Toronto City Hall after Witt decided to give the fight less screen time.[14] Actors portraying zombies spent four days training with choreographers at a zombie "boot camp" to make sure they all had consistent behavior and movements.[26] Anderson and other crew members considered making the zombies move faster but decided that it would be breaking a fundamental element of the games.[14] Anderson only appeared on set for a couple of days due to other commitments, though he communicated with Witt via email about several dialogue and production changes during filming.[15] The script's original ending had Alice escaping from Umbrella on her own via greater use of her telekinetic powers before meeting up with Valentine. Approximately half of the scene was filmed before the ending was rewritten.[14]
 The original Resident Evil film took only broad elements from the games; Apocalypse incorporated many specific features from them, such as the re-enactment of scenes.[18] The introductory cutscene of Resident Evil – Code: Veronica featuring Claire Redfield inspired the scene where Alice runs through a building while an Umbrella helicopter fires.[24] The introduction of Resident Evil 3: Nemesis inspired another scene where Raccoon City is overrun by zombies, and the police and Umbrella soldiers are fighting back.[14] Valentine's outfit in the film, which consisted of a tube top and miniskirt, is based on her costume from Nemesis.[10] Anderson considered several ways to justify having the revealing costume in the storyline, such as making it her undercover outfit, though eventually decided to ignore the issue on the grounds that anyone questioning her attire "probably shouldn't be watching a Resident Evil movie".[14] The film also references several aspects from the original Resident Evil game and Resident Evil 2, such as locations, place names, character moves, props, and camera perspectives.[14]
 Special effects for the film included green screens, computer-generated imagery (CGI), matte paintings, tracking, wire removal and scale models.[27] The Nemesis character was created with a costume, and the only CGI effect added was an adjustment to his eye.[27] Taylor was chosen as his height at 6.7 feet (2.04 m) and weight of 320 lb (145 kg) made him suitable to portray the character. The costume was specifically built for his body and weighed about 65 lb (29 kg).[25] Despite the character's height, aspect ratios were still modified to make him appear 10–20% larger in certain scenes.[14] The Lickers, a type of mutated zombie, were completely CGI. The effects team had originally used animatronics for some of the scenes, but were unhappy with the results.[27] C.O.R.E. Digital Pictures won the contract to animate the Lickers, beating several other effects companies who had submitted preliminary designs, and described it as the most challenging special effect they created for the film. The studio created over 250 special effects including superimposing Jovovich's face onto a stunt double;[28] Jovovich performed most of her own stunts though her insurance company would not allow her to attempt several of the more dangerous ones.[21] Frantic Films created 78 special effects for the film including tracer fire, muzzle flashes, lasers and slow motion, using effects programs Eyeon Fusion, Autodesk 3ds Max as well as in-house software.[28] Mr. X Inc. created additional effects including the scene showing the destruction of the Toronto City Hall building. Four months were spent making a 43 ft (13 m) 1/6 scale model of the building with 1,600 panes of glass, each of which was wired with an explosive to create the final effect.[27] Digital intermediate work was completed by the Computer Film Company. Colors in the film were edited heavily in post-production, giving it a darker look overall while enhancing the brightness of blood and gore. The colors of the Nemesis costume were also tweaked to make it look more lifelike and Alice and Valentine were given modifications such as increasing the glow of their skin and redness of their lips.[14]
 The soundtrack for Apocalypse was released on August 31, 2004, and contained alternative metal songs both featured in the film and "inspired" by it. Johnny Loftus of AllMusic gave the soundtrack three stars out of five, saying it was an "unscrupulous moneymaker" that predictably catered to the film's target audience of teenage boys, adding the "aggression, mania, and generally apocalyptic tone of this material fits well with a movie based on a video game about blowing away crazy zombies".[29]
 Jeff Danna composed the film's score performed by the London Philharmonia Orchestra. It was released on September 28, 2004.[30] Mike Brennan from Soundtrack.net gave the score 2½ stars out of five, praising the blend of orchestral and electronic styles, though saying it "could have easily benefited from some more thematic development and a bit more variation in the sound of the music".[31]
 Marcus Nispel was hired to create a teaser trailer, titled Regenerate. It purports to be advertising a skin rejuvenation product created by the Umbrella Corporation, before the woman in the commercial turns into a zombie. By May 2004, the teaser had been downloaded 8.5 million times from the film's official website.[32] Part of the teaser was shown in the film briefly on a television in the background and a part of it also appears in a mid-credits scene.[14] The film's theatrical trailer was released on Yahoo! Movies in July 2004.[33] A novelization of the film written by Keith DeCandido was published by Simon & Schuster the following month.[34]
 Apocalypse opened at number one in the United States on September 10, 2004, where it grossed over $23 million on its opening weekend.[9] The film also opened at number one in Hong Kong, Singapore, Malaysia, the Philippines and Mexico, and performed well in Japan, France and Brazil, though the horror film Saw overshadowed it at the box office in the United Kingdom, and it received a lackluster reception in Sweden,[35][36] where it grossed $473,550.[37] Earning over $6 million in Canada, Resident Evil: Apocalypse was the highest grossing, domestically produced Canadian film in 2004.[38] Apocalypse went on to earn $129,394,835 worldwide against a budget of $45 million,[9] surpassing the earnings of the first film which generated $102,984,862.[39]
 
Based on 124 reviews, Apocalypse holds an approval rating of 21% on Rotten Tomatoes, giving it the lowest rating of the six films in the series.[40] The site's critics consensus reads: "Resident Evil: Apocalypse has lots of action, but not much in terms of plot or creativity".[41] Metacritic gives the film a score of 35/100 based on 26 reviews.[42]
 Leonard Maltin rated the film a "bomb" in his book Leonard Maltin's Movie Guide and called it a "tiresome" sequel that ended up playing more like a remake.[43] Roger Ebert gave the film a score of half a star out of four, calling it "an utterly meaningless waste of time" that lacked any wit or imagination and also failed to provide entertaining violence or special effects.[44] Carrie Rickey of The Philadelphia Inquirer gave the film one star out of four, concluding that even for people interested in the biological horror genre, Apocalypse was "pretty generic stuff".[45]
 Dave Kehr of The New York Times gave the film a positive review, praising Anderson's screenplay and describing Witt's direction as "fast, funny, smart and highly satisfying in terms of visceral impact".[23] M. E. Russell of The Oregonian said, "The bad news? The movie is monumentally stupid. The good news? It's a fun kind of stupid".[46] The A.V. Club's Nathan Rabin  said that it progressed too slowly to be considered a good film, "but when Jovovich finally starts kicking zombified ass, it becomes good enough".[47] Ben Kenigsberg of The Village Voice said the film is "not without its moments of elemental dread" though he complained there was too much action and padding and not enough irony.[48]
 Gregory Kirschling of Entertainment Weekly, who gave the film a 'D−' rating, praised Jovovich but felt that "the rest of the cast was strictly straight-to-DVD";[49] Cinefantastique, on the other hand, commented that Jovovich looked bored and that Guillory's portrayal of Jill Valentine was the film's "saving grace".[50]
 In 2009, Time ranked the film as one of the top ten worst video game films. While criticizing all three films released in the Resident Evil series at the time, they concluded that Apocalypse deserved their vote "Because, like any sequel, it’s an enabler ... sequels to bad movies just enable further sequels to be considered".[51] In 2016, separate journalists from Bloody Disgusting ranked it as both the best and worst film in the  series.[52][53] In 2017, Michael Nordine of TheWrap ranked it as the worst film in the series, saying its only redeeming features were the fact it expanded the series and the "awesomely stupid" fight between Alice and Nemesis.[54]
 Resident Evil: Apocalypse won Best Sound Editing and the Golden Reel Award at the 25th Genie Awards,[55][56] and was also nominated for Best Overall Sound. It was nominated for Best Sound Editing in a feature film by the Directors Guild of Canada, and for Best Make-Up at the 31st Saturn Awards.[57] For composing the film's score, Jeff Danna was awarded the SOCAN International Film Music Award each year from 2006 through 2009.[58]
 
The film was released on DVD in North America on December 28, 2004. The release included three audio commentaries, 20 deleted scenes, several featurettes and a blooper reel. DVD Talk awarded the film 3½ stars out of five for both video quality and special features.[59] Releases on UMD and Blu-ray Disc formats followed in 2005 and 2007, respectively.[60][61] High-Def Digest gave the Blu-ray release three stars out of five for video quality and 3½ stars for special features.[62]
 Special "Resurrection Editions" of both Resident Evil (2002) and Resident Evil: Apocalypse were released in a two-disc set on September 4, 2007. An exclusive scene for the then upcoming sequel Resident Evil: Extinction (2007) was included, along with several other bonus features.[63][64] Trilogy sets containing the first three films were released on DVD and Blu-ray in 2008.[65][66] "The Resident Evil Collection" consisting of the first four films was released in September 2012 on DVD and Blu-ray,[67] a version containing the first five films was released on DVD and Blu-ray in December 2012,[68] and "Resident Evil The Complete Collection" containing all six films was released on Blu-ray in May 2017.[69]
 Jeremy Bolt Paul W. S. Anderson Don Carmody Milla Jovovich Sienna Guillory Oded Fehr Thomas Kretschmann Jared Harris Mike Epps Christian Sebaldt Derek Rogers Constantin Film[1] Davis Films[1] Impact Pictures[1] Screen Gems[2] Constantin Film (Germany)[3] Metropolitan Filmexport (France)[4] Alliance Atlantis (Canada)[5] Sony Pictures Releasing (United States)[6] September 10, 2004 (2004-09-10) (United States) September 23, 2004 (2004-09-23) (Germany) October 6, 2004 (2004-10-06) (France) Canada[7] France[7] Germany[8] United Kingdom[7] United States[7] 1 Plot 2 Cast 3 Themes 4 Production

4.1 Pre-production
4.2 Casting
4.3 Filming
4.4 Effects
4.5 Soundtrack

 4.1 Pre-production 4.2 Casting 4.3 Filming 4.4 Effects 4.5 Soundtrack 5 Release

5.1 Marketing and box office
5.2 Critical response
5.3 Legacy
5.4 Accolades
5.5 Home media

 5.1 Marketing and box office 5.2 Critical response 5.3 Legacy 5.4 Accolades 5.5 Home media 6 References 7 External links Milla Jovovich as Alice Sienna Guillory as Jill Valentine Oded Fehr as Carlos Oliveira Thomas Kretschmann as Major Timothy Cain Sophie Vavasseur as Angela "Angie" Ashford Jared Harris as Dr. Charles Ashford Mike Epps as Lloyd Jefferson "L.J." Wade Iain Glen as Dr. Alexander Isaacs Matthew G. Taylor as Nemesis Razaaq Adoti as Sgt. Peyton Wells Sandrine Holt as Terri Morales Zack Ward as Nicholai Ginovaef ^ a b c "Resident Evil: Apocalypse". AFI Catalog of Feature Films. Retrieved December 12, 2018..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ Foundas, Scott (October 21, 2004). "Resident Evil: Apocalypse". Variety. Retrieved June 16, 2019.
 ^ a b "Resident Evil: Apocalypse". Filmportal.de. Retrieved June 16, 2019.
 ^ a b "Resident Evil: Apocalypse" (in French). Bifi.fr. Retrieved June 16, 2019.
 ^ "Item:". Library and Archives Canada. Retrieved June 16, 2019.
 ^ "Resident Evil: Apocalypse (2004)". American Film Institute. Retrieved June 16, 2019.
 ^ a b c d e "Resident Evil Apocalypse (2004)". British Film Institute. Archived from the original on December 3, 2017.
 ^ "Resident Evil - Apocalypse". Constantin Film (in German). Archived from the original on August 4, 2019.
 ^ a b c d e "Resident Evil: Apocalypse (2004)". Box Office Mojo. Archived from the original on January 2, 2013.
 ^ a b Harper, Stephen (2007). ""I could kiss you, you bitch": race, gender, and sexuality in Resident Evil and Resident Evil 2: Apocalypse". Jump Cut: A Review of Contemporary Media (49). Archived from the original on July 15, 2017.
 ^ Kellner, Douglas (2016). "Social apocalypse in contemporary Hollywood film". MATRIZes. 10 (1): 19.
 ^ "MSN Talks to Director Paul Anderson and Actress Milla Jovovich". MSN. February 27, 2002. Archived from the original on July 4, 2003.
 ^ a b "Mabius Talks Resident Evil 2". Syfy. March 4, 2002. Archived from the original on February 25, 2009.
 ^ a b c d e f g h i j k l m Paul W.S. Anderson, Jeremy Bolt (2004). Resident Evil: Apocalypse Writer and Producer Commentary (DVD). Sony Entertainment.
 ^ a b Otto, Jeff (August 23, 2004). "Interview: Alexander Witt". IGN. Archived from the original on December 9, 2017.
 ^ a b "Anderson Shopping for Resident Talent". IGN. May 21, 2002. Archived from the original on February 28, 2017.
 ^ Resident Evil: Apocalypse (DVD). Sony Entertainment. 2004.
 ^ a b c Robert Kulzer, Dan Carmody, Jeremy Bolt (2004). Game Over (Resident Evil: Apocalypse, DVD featurette). Sony Entertainment.
 ^ Chapman, Tom (February 19, 2017). "Everything The Resident Evil Movies Got Right (And Wrong)". Screen Rant. Archived from the original on December 12, 2017.
 ^ Davidson, Paul (April 17, 2003). "Resident Evil: Nemesis Character Exposé". IGN. Archived from the original on September 14, 2017.
 ^ a b c Milla Jovovich, Oded Fehr, Sienna Guillory (2004). Resident Evil: Apocalypse Cast Commentary (DVD). Sony Entertainment.
 ^ Milla Jovovich, Sienna Guillory (2004). Game Babes (Resident Evil: Apocalypse, DVD featurette). Sony Entertainment.
 ^ a b Kehr, Dave (September 10, 2004). "Call to Arms, With Trouble Right Here in Zombie City". The New York Times. Archived from the original on December 4, 2017.
 ^ a b Jeremy Bolt, Dan Carmody (2004). Building Raccoon City (Resident Evil: Apocalypse, DVD featurette). Sony Entertainment.
 ^ a b Matthew Taylor, Steve Lucescu (2004). Running, Jumping, Fighting (Resident Evil: Apocalypse, DVD featurette). Sony Entertainment.
 ^ Derek Aasland, Sharon Moore (2004). Zombie Choreography (Resident Evil: Apocalypse, DVD featurette). Sony Entertainment.
 ^ a b c d Alison O'Brien, Kyle Menzes, Eric Robertson (2004). Smoke and Mirrors (Resident Evil: Apocalypse, DVD featurette). Sony Entertainment.
 ^ a b Dillon, Mark (October 25, 2004). "Shops join forces to wreak apocalypse". Playback: Canada's Broadcast and Production Journal: 17.
 ^ Loftus, Johnny. "Resident Evil: Apocalypse [Original Soundtrack]". AllMusic. Archived from the original on December 4, 2017.
 ^ "Resident Evil: Apocalypse [Original Motion Picture Soundtrack]". AllMusic. Archived from the original on December 4, 2017.
 ^ Brennan, Mike (April 10, 2005). "Resident Evil: Apocalypse". Soundtrack.net. Archived from the original on December 4, 2017.
 ^ LaPorte, Nicole (May 2, 2004). "Nispel takes an 'Evil' turn". Variety. Archived from the original on December 4, 2017.
 ^ "Resident Evil: Apocalypse". Yahoo! Movies. Archived from the original on July 10, 2004.
 ^ DeCandido, Keith R. A. (August 31, 2004). Resident Evil: Apocalypse. Simon & Schuster. ISBN 978-0743499378.
 ^ Groves, Dan (October 18, 2004). "Film/International: 'shark tale' plays swimmingly overseas". Variety. 396 (9): 12.
 ^ Groves, Dan (October 11, 2004). "'Collateral' leads newcomers in tepid o'seas sesh". Variety. 396 (8): 10.
 ^ "Resident Evil: Apocalypse - Foreign". Box Office Mojo. Archived from the original on January 3, 2018.
 ^ Dillon, Mark (March 28, 2005). "Genie 2005: Prix Jutra, part deux". Playback: Canada's Broadcast and Production Journal: 1.
 ^ "Resident Evil (2002)". Box Office Mojo. Archived from the original on January 18, 2016.
 ^ "Resident Evil". Rotten Tomatoes. Archived from the original on December 22, 2017.
 ^ "Resident Evil: Apocalypse (2004)". Rotten Tomatoes. Archived from the original on December 4, 2017.
 ^ "Resident Evil: Apocalypse 2004". Metacritic. Archived from the original on June 11, 2017.
 ^ Maltin, Leonard (2005). Leonard Maltin's Movie Guide 2006. Signet. p. 1072. ISBN 978-0451216090.
 ^ Ebert, Roger (September 10, 2005). "Resident Evil: Apocalypse". RogerEbert.com. Archived from the original on June 30, 2013.
 ^ Rickey, Carrie (September 10, 2004). "Fighting evil in the near nude: Yet another video-game flick". The Philadelphia Inquirer. Archived from the original on December 5, 2017.
 ^ Russell, M. E. (September 10, 2004). "Resident Evil: Apocalypse". The Oregonian.
 ^ Nathan Rabin (September 14, 2004). "Resident Evil: Apocalypse". The A.V. Club. The Onion. Archived from the original on December 5, 2008.
 ^ Kenigsberg, Ben (September 7, 2004). "Film". The Village Voice. Archived from the original on December 5, 2017.
 ^ Kirschling, Gregory (September 24, 2004). "Resident Evil: Apocalypse". Entertainment Weekly. Archived from the original on December 5, 2017.
 ^ Clarke, Frederick S. (2005). "Resident Evil: Apocalypse". Cinefantastique. 37: 63. Archived from the original on December 21, 2017.
 ^ "Top 10 Worst Video Game Movies". Time. October 17, 2008. Archived from the original on April 11, 2013.
 ^ Thurman, Trace (April 13, 2016). "Unpopular Opinion: 'Apocalypse' is the Worst Film in the 'Resident Evil' Franchise". Bloody Disgusting. Archived from the original on December 8, 2017.
 ^ Barkan, Jonathon (March 27, 2016). "How Would You Rank The 'Resident Evil' Movies?". Bloody Disgusting. Archived from the original on December 8, 2017.
 ^ Nordine, Michael (January 27, 2017). "All 6 'Resident Evil' Movies, Ranked Worst to Best". TheWrap. Archived from the original on December 8, 2017.
 ^ Adams, James (February 1, 2011). "Resident Evil: Afterlife is top-grossing Canadian flick". The Globe and Mail. Archived from the original on September 19, 2012.
 ^ Tillson, Tamson (March 21, 2005). "Genie toons in Triplets". Variety. Archived from the original on December 6, 2017.
 ^ "The 31st annual Saturn Award Nominations". Saturn Award. Archived from the original on October 29, 2005.
 ^ "The SOCAN Awards". Society of Composers, Authors and Music Publishers of Canada. Archived from the original on October 29, 2017. Retrieved December 6, 2017. Note: Search for "Jeff Danna"
 ^ Belerie, Aaron (December 20, 2004). "Resident Evil: Apocalypse". DVD Talk. Archived from the original on December 7, 2017.
 ^ "PSP Movies". IGN. Archived from the original on September 27, 2012.
 ^ Liebman, Martin (July 21, 2009). "Resident Evil: Apocalypse Blu-ray". Blu-ray.com. Archived from the original on December 7, 2017.
 ^ "Resident Evil: Apocalypse". High-Def Digest. March 8, 2007. Archived from the original on December 7, 2017.
 ^ McWhertor, Michael (June 26, 2007). "Resident Evil Flicks Resurrected, Bundled". Kotaku. Archived from the original on December 16, 2007.
 ^ "Resident Evil/Resident Evil: Apocalypse (Double Feature)". Sony Pictures. Archived from the original on November 25, 2007.
 ^ "Resident Evil Trilogy 1-3". Amazon. Archived from the original on December 7, 2017.
 ^ Zyber, Joshua (January 28, 2008). "Resident Evil – The High Definition Trilogy". High-Def Digest. Archived from the original on December 7, 2017.
 ^ "The Resident Evil Collection". Amazon. Archived from the original on December 7, 2017.
 ^ "The Resident Evil Collection". Amazon. Archived from the original on December 7, 2017.
 ^ "Resident Evil The Complete Collection". Amazon. Archived from the original on December 7, 2017.
 Resident Evil: Apocalypse on IMDb Resident Evil: Apocalypse at AllMovie Resident Evil: Apocalypse at Rotten Tomatoes  Resident Evil: Apocalypse at the Internet Movie Firearms Database Resident Evil: Apocalypse Production Notes at MovieWeb v t e List of media Resident Evil 2 3: Nemesis Code: Veronica Zero 4 5 6 7: Biohazard Resident Evil 2 Survivor Survivor 2 – Code: Veronica Dead Aim Outbreak File #2 The Umbrella Chronicles The Darkside Chronicles Revelations Revelations 2 Gaiden The Mercenaries 3D Operation Raccoon City Umbrella Corps Mobile games Resident Evil Apocalypse Extinction Afterlife Retribution The Final Chapter Biohazard 4D-Executer Degeneration Damnation Vendetta Ada Wong Albert Wesker Alice Chris Redfield Claire Redfield Jill Valentine Leon S. Kennedy Nemesis Rebecca Chambers Sheva Alomar Tyrant Shinji Mikami Hideki Kamiya Hiroyuki Kobayashi Noboru Sugimura Jun Takeuchi  Book  Category v t e Shopping (1994) Mortal Kombat (1995) Event Horizon (1997) Soldier (1998) Resident Evil (2002) Alien vs. Predator (2004) Death Race (2008) Resident Evil: Afterlife (2010) The Three Musketeers (2011) Resident Evil: Retribution (2012) Pompeii (2014) Resident Evil: The Final Chapter (2016) Monster Hunter (2020) Resident Evil: Apocalypse (2004) Resident Evil: Extinction (2007) Death Race 2 (2010) Death Race 3: Inferno (2013) Death Race: Beyond Anarchy (2018) The Dark (2005) DOA: Dead or Alive (2006) Pandorum (2009) El C.I.D. (1990-1992) The Sight (2000) Origin (2018) Milla Jovovich 2004 films English-language films 2000s action films 2000s science fiction films 2000s sequel films 2004 horror films Apocalyptic films American films American horror films British action films British films British horror films British science fiction films British zombie films Canadian action films Canadian films Canadian horror films Canadian sequel films Constantin Film films Directorial debut films Films about viral outbreaks Films produced by Bernd Eichinger Films produced by Don Carmody Films produced by Paul W. S. Anderson Films scored by Jeff Danna Films set in the United States Films shot in Hamilton, Ontario Films shot in Toronto Girls with guns films Resident Evil (film series) Science fiction action films Films with screenplays by Paul W. S. Anderson Sequel films CS1 French-language sources (fr) CS1 German-language sources (de) Use mdy dates from March 2019 Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikiquote Create a book Download as PDF Printable version العربية Azərbaycanca Български Català Čeština Deutsch Español فارسی Fiji Hindi Français Galego 한국어 हिन्दी Hrvatski Bahasa Indonesia Italiano Lietuvių Magyar Nederlands 日本語 Oʻzbekcha/ўзбекча Polski Português Română Русский Slovenščina کوردی Српски / srpski Suomi Svenska Tagalog தமிழ் ไทย Türkçe Українська 中文  This page was last edited on 9 November 2019, at 19:49 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Resident Evil: Apocalypse a b c ^ a b a b ^ ^ a b c d e ^ a b c d e a b ^ 10 ^ a b a b c d e f g h i j k l m a b a b ^ a b c ^ ^ a b c ^ a b a b a b ^ a b c d a b ^ ^ ^ ^ ^ ^ ^ 396 ^ 396 ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ 37 ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ Resident Evil: Apocalypse  Book  Category Theatrical release poster Alexander Witt 
Jeremy Bolt
Paul W. S. Anderson
Don Carmody
 Paul W. S. Anderson Resident Evilby Capcom 
Milla Jovovich
Sienna Guillory
Oded Fehr
Thomas Kretschmann
Jared Harris
Mike Epps
 Jeff Danna 
Christian Sebaldt
Derek Rogers
 Eddie Hamilton 
Constantin Film[1]
Davis Films[1]
Impact Pictures[1]
Screen Gems[2]
 
Constantin Film (Germany)[3]
Metropolitan Filmexport (France)[4]
Alliance Atlantis (Canada)[5]
Sony Pictures Releasing (United States)[6]
 
September 10, 2004 (2004-09-10) (United States)
September 23, 2004 (2004-09-23) (Germany)
October 6, 2004 (2004-10-06) (France)
[3][4] 94 minutes 
Canada[7]
France[7]
Germany[8]
United Kingdom[7]
United States[7]
 English $45 million[9] $129.3 million[9]  Wikiquote has quotations related to: Resident Evil: Apocalypse 
List of media
 Main series
Resident Evil
2
3: Nemesis
Code: Veronica
Zero
4
5
6
7: Biohazard
Remakes
Resident Evil
2
Survivor
Survivor
Survivor 2 – Code: Veronica
Dead Aim
Outbreak
Outbreak
File #2
Chronicles
The Umbrella Chronicles
The Darkside Chronicles
Revelations
Revelations
Revelations 2
Other games
Gaiden
The Mercenaries 3D
Operation Raccoon City
Umbrella Corps
Mobile games
 
Resident Evil
2
3: Nemesis
Code: Veronica
Zero
4
5
6
7: Biohazard
 
Resident Evil
2
3: Nemesis
Code: Veronica
Zero
4
5
6
7: Biohazard
 
Resident Evil
2
 
Survivor
Survivor 2 – Code: Veronica
Dead Aim
 
Outbreak
File #2
 
The Umbrella Chronicles
The Darkside Chronicles
 
Revelations
Revelations 2
 
Gaiden
The Mercenaries 3D
Operation Raccoon City
Umbrella Corps
Mobile games
 Live-action series
Resident Evil
Apocalypse
Extinction
Afterlife
Retribution
The Final Chapter
Animated films
Biohazard 4D-Executer
Degeneration
Damnation
Vendetta
 
Resident Evil
Apocalypse
Extinction
Afterlife
Retribution
The Final Chapter
 
Biohazard 4D-Executer
Degeneration
Damnation
Vendetta
 
Ada Wong
Albert Wesker
Alice
Chris Redfield
Claire Redfield
Jill Valentine
Leon S. Kennedy
Nemesis
Rebecca Chambers
Sheva Alomar
Tyrant
 
Shinji Mikami
Hideki Kamiya
Hiroyuki Kobayashi
Noboru Sugimura
Jun Takeuchi
 
 Book
 Category
 
Shopping (1994)
Mortal Kombat (1995)
Event Horizon (1997)
Soldier (1998)
Resident Evil (2002)
Alien vs. Predator (2004)
Death Race (2008)
Resident Evil: Afterlife (2010)
The Three Musketeers (2011)
Resident Evil: Retribution (2012)
Pompeii (2014)
Resident Evil: The Final Chapter (2016)
Monster Hunter (2020)
 
Resident Evil: Apocalypse (2004)
Resident Evil: Extinction (2007)
Death Race 2 (2010)
Death Race 3: Inferno (2013)
Death Race: Beyond Anarchy (2018)
 
The Dark (2005)
DOA: Dead or Alive (2006)
Pandorum (2009)
 
El C.I.D. (1990-1992)
The Sight (2000)
Origin (2018)
 
Milla Jovovich
 