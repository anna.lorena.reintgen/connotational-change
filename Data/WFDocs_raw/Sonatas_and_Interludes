Sonatas and Interludes is a cycle of twenty pieces for prepared piano by American avant-garde composer John Cage (1912–1992). It was composed in 1946–48, shortly after Cage's introduction to Indian philosophy and the teachings of art historian Ananda K. Coomaraswamy, both of which became major influences on the composer's later work. Significantly more complex than his other works for prepared piano,[1][2] Sonatas and Interludes is generally recognized as one of Cage's finest achievements.[3][4]
 The cycle consists of sixteen sonatas (thirteen of which are cast in binary form, the remaining three in ternary form) and four more freely structured interludes. The aim of the pieces is to express the eight permanent emotions of the rasa Indian tradition. In Sonatas and Interludes, Cage elevated his technique of rhythmic proportions to a new level of complexity.[2] In each sonata a short sequence of natural numbers and fractions defines the structure of the work and that of its parts, informing structures as localized as individual melodic lines.[5]
 Cage underwent an artistic crisis in the early 1940s.[6] His compositions were rarely accepted by the public,[7] and he grew more and more disillusioned with the idea of art as communication. He later gave an account of the reasons: "Frequently I misunderstood what another composer was saying simply because I had little understanding of his language. And I found other people misunderstanding what I myself was saying when I was saying something pointed and direct".[8] At the beginning of 1946, Cage met Gita Sarabhai, an Indian musician who came to the United States concerned about Western influence on the music of her country. Sarabhai wanted to spend several months in the US, studying Western music. She took lessons in counterpoint and contemporary music with Cage, who offered to teach her for free if she taught him about Indian music in return.[9] Sarabhai agreed and through her Cage became acquainted with Indian music and philosophy. The purpose of music, according to Sarabhai's teacher in India, was "to sober and quiet the mind, thus rendering it susceptible to divine influences",[8][10] and this definition became one of the cornerstones of Cage's view on music and art in general.
 At around the same time, Cage began studying the writings of the Indian art historian Ananda K. Coomaraswamy. Among the ideas that influenced Cage was the description of the rasa aesthetic and of its eight "permanent emotions". These emotions are divided into two groups: four white (humor, wonder, erotic, and heroic—"accepting one's experience", in Cage's words[8]) and four black (anger, fear, disgust, and sorrow). They are the first eight of the navarasas or navrasas ("nine emotions"), and they have a common tendency towards the ninth of the navarasas: tranquility.[11] Cage never specified which of the pieces relate to which emotions, or whether there even exists such direct correspondence between them.[12] He mentioned, though, that the "pieces with bell-like sounds suggest Europe and others with a drum-like resonance suggest the East".[13] (A short excerpt from Sonata II, which is clearly inspired by Eastern music: listen (help·info).)[14] Cage also stated that Sonata XVI, the last of the cycle (listen (help·info)), is "clearly European. It was the signature of a composer from the West."[15]
 Cage started working on the cycle in February 1946, while living in New York City. The idea of a collection of short pieces was apparently prompted by the poet Edwin Denby, who had remarked that short pieces "can have in them just as much as long pieces can".[16] The choice of materials and the technique of piano preparation in Sonatas and Interludes were largely dependent on improvisation: Cage later wrote that the cycle was composed "by playing the piano, listening to differences [and] making a choice".[17] On several accounts he offered a poetic metaphor for this process, comparing it with collecting shells while walking along a beach.[13][18] Work on the project was interrupted in early 1947, when Cage made a break to compose The Seasons, a ballet in one act also inspired by ideas from Indian philosophy. Immediately after The Seasons Cage returned to Sonatas and Interludes, and by March 1948 it was completed.
 Cage dedicated Sonatas and Interludes to Maro Ajemian, a pianist and friend. Ajemian performed the work many times since 1949, including one of the first performances of the complete cycle on January 12, 1949, in Carnegie Hall. On many other occasions in the late 1940s and early 1950s, Cage performed it himself. Critical reaction was uneven,[19] but mostly positive,[20] and the success of Sonatas and Interludes led to a grant from the Guggenheim Foundation, which Cage received in 1949, allowing him to make a six-month trip to Europe. There he met Olivier Messiaen, who helped organize a performance of the work for his students in Paris on June 7, 1949; and he befriended Pierre Boulez, who became an early admirer of the work and wrote a lecture about it for the June 17, 1949 performance at the salon of Suzanne Tézenas in Paris.[21] While still living in Paris, Cage began writing String Quartet in Four Parts, yet another work influenced by Indian philosophy.
 In the text accompanying the first recording of Sonatas and Interludes, Cage specifically stated that the use of preparations is not a criticism of the instrument, but a simple practical measure.[13] Cage started composing for prepared piano in 1940, when he wrote a piece called Bacchanale for a dance by Syvilla Fort, and by 1946 had already composed a large number of works for the instrument. However, in Sonatas and Interludes the preparation is very complex, more so than in any of the earlier pieces.[1] Forty-five notes are prepared, mostly using screws and various types of bolts, but also with fifteen pieces of rubber, four pieces of plastic, several nuts and one eraser.[22] It takes about two or three hours to prepare a piano for performance. Despite the detailed instructions, any preparation is bound to be different from any other, and Cage himself suggested that there is no strict plan to adhere to: "if you enjoy playing the Sonatas and Interludes then do it so that it seems right to you".[13]
 For the most part Cage avoids using lower registers of the piano, and much of the music's melodic foreground lies in the soprano range. Of the forty-five prepared notes, only three belong to the three lowest octaves below F#3: D3, D2 and D1. Furthermore, D2 is prepared in such a way that the resulting sound has the frequency of a D4 (resulting in two variants of D4 available, one more prepared than the other).[23] The portion of the keyboard above F#3 is divided into roughly three registers: low, middle, and high. The low register has the heaviest preparation, and the high register the lightest.[1] Different methods are used: certain notes produce sounds that retain the original frequency and a pianistic character; others become drum-like sounds, detuned versions of the original notes, or metallic, rattling sounds that have no sense of the fundamental frequency at all. The use of the soft pedal, which makes the hammers strike only two of the three strings of each note (or one, for notes with only two strings), complicates the matter further. For example, the note C5 is a metallic sound with no fundamental discernible when the soft pedal is depressed, but it sounds fairly normal if the pedal is released.[23] It appears that Cage was fully aware of the implications of this: certain sonatas feature interplay between two versions of one note, others place special emphasis on particular notes, and still others are very dependent on particular note combinations.[24]
 The cycle comprises sixteen sonatas and four interludes, arranged symmetrically. Four groups of four sonatas each are separated by interludes in the following way: 
 Cage refers to his pieces as  sonata in the sense that these works are cast in the form that early classical keyboard sonatas (such as those of Scarlatti) were: AABB. The works are not cast in the later sonata form which is far more elaborate. The only exceptions are sonatas IX–XI, which feature three sections: prelude, interlude, and postlude.[13] Sonatas XIV–XV follow the AABB scheme but are paired and given the joint title Gemini—after the work of Richard Lippold, referring to a sculpture by Lippold. The interludes, on the other hand, do not have a unifying scheme. The first two are free-form movements, whereas interludes 3 and 4 have a four-section structure with repeats for each section.[2]
 The main technique Cage used for composition is that of nested proportions: an arbitrary sequence of numbers defines the structure of a piece on both the macroscopic and the microscopic level, so that the larger parts of each piece are in the same relation to the whole as the smaller parts are to a single unit of it.[5] For instance, the proportion for Sonata III is 1, 1, 3¼, 3¼ (in whole notes), and a unit here is equal to 8½ bars (the end of a unit is marked with a double barline in the score, unless it coincides with the end of a section). The structure of this sonata is AABB. Section A consists of a single unit, composed according to the given proportion: correlation on the microscopic level. A is repeated, and AA forms the first part of the proportion on the macroscopic level: 1, 1. B consists of three units and an appendix of ¼ of a unit. B is also repeated, and BB gives the second half of the proportion: 3¼, 3¼. Therefore, AABB has proportions 1, 1, 3¼, 3¼: correlation on the macroscopic level. The musical phrases within each unit are also governed by the same proportion.[27] See Example 2 for a graph of the structure of Sonata III.
 The proportions were chosen arbitrarily in all but the last four pieces in the cycle: sonatas XIII and XVI use symmetrical proportions, and sonatas XIV and XV share the 2, 2, 3, 3 proportion. This symmetry, and the adherence of all four sonatas to the ten-bar unit, were explained by Cage as an expression of tranquility.[28] The complexity of proportions prompted Cage to use asymmetric musical phrases and somewhat frequent changes of time signature to achieve both microscopic and macroscopic correlation. For example, unit length of 8½ in the first section of Sonata III is achieved by using six bars in 2/2 time and two in 5/4 (rather than eight bars in 2/2 and one in 1/2). In many sonatas the microstructure—how the melodic lines are constructed—deviates slightly from the pre-defined proportion.[29]
 Cage had frequently used the nested proportions technique and its variations before, most notably in First Construction (in Metal) (1939), which was the first piece to use it,[30] and numerous dance-related works for prepared piano. In Sonatas and Interludes, however, the proportions are more complex, partly because fractions are used.[2][29] In his 1949 lecture on Sonatas and Interludes Pierre Boulez specifically emphasized the connection between tradition and innovation in Sonatas and Interludes: "The structure of these sonatas brings together a pre-Classical structure and a rhythmic structure which belong to two entirely different worlds."[21]
 Sonatas and Interludes has been recorded many times, both in its complete form and in parts. This list is organized chronologically and presents only the complete recordings. Years of recording are given, not years of release. Catalogue numbers are indicated for the latest available CD versions. For the complete discography with reissues and partial recordings listed, see the link to the John Cage database below.
 
 1 History of composition 2 Analysis

2.1 Piano preparation
2.2 Structure

 2.1 Piano preparation 2.2 Structure 3 Recordings 4 See also 5 Notes 6 References and further reading

6.1 Books
6.2 Dissertations and articles

 6.1 Books 6.2 Dissertations and articles 7 External links

7.1 Media

 7.1 Media Maro Ajemian – 1951, Dial Records 20–21. Reissued in the 1960s, Composers Recordings Inc. CRI 700. Reissued on CD, él records ACMEM88CD[31] Yuji Takahashi:
1965, Fylkingen Records FYCD 1010 (mono)[32]
1975, Denon COCO 70757 (stereo, digital) 1965, Fylkingen Records FYCD 1010 (mono)[32] 1975, Denon COCO 70757 (stereo, digital) John Damgaard – 1971, Membran Quadromania 222190-444 (4CD, incl. many other works) John Tilbury – 1974, Explore Records EXP0004[33] Joshua Pierce:
1975, Wergo WER 60156-50[34]
1988, Newport Classic NPD 85526
1999, Ants Records AG 06 (2CD, live recording)
2000, SoLyd Records SLR 0303 (live recording) 1975, Wergo WER 60156-50[34] 1988, Newport Classic NPD 85526 1999, Ants Records AG 06 (2CD, live recording) 2000, SoLyd Records SLR 0303 (live recording) Gérard Frémy – 1980, Pianovox PIA 521–2, Ogam Records 488004-2, Etcetera Records KTC 2001[35] Nada Kolundžija – c. 1981, Diskos LPD-930 (2LP)[36] Darryl Rosenberg – c. 1986, VQR Digital VQR 2001[37][38] (LP) Mario Bertoncini – 1991, released 2001 as Edition RZ 20001 (Parallele 20001)[39] Nigel Butterley – 1992, Tall Poppies TP025[40] Louis Goldstein – 1994, Greensye Music 4794[41] (incl. Dream) Philipp Vandré – 1994, Mode 50[42] (according to the liner notes,[42] this is the first recording made on a Steinway "O"-type baby grand piano, the model Cage originally composed the piece on) Julie Steinberg – 1995, Music & Arts 937[43] Markus Hinterhäuser – 1996, Col Legno WWE 1CD 20001[44] Steffen Schleiermacher – 1996, MDG 613 0781-2[45] (3CD, part of John Cage: Complete Piano Works 18CD series) Aleck Karis – 1997, Bridge 9081 A/B[46] (2CD, incl. Cage's lecture Composition in Retrospect) Jean Pierre Dupuy – 1997, Stradivarius 33422 Boris Berman – 1998, Naxos 8.559042[47] or Naxos 8.554345[48] Joanna MacGregor – 1998, SoundCircus SC 003[49] (2CD, includes miscellaneous other works by Cage and other composers) Giancarlo Cardini – 1999, Materiali Sonori[50][51] Kumi Wakao – 1999, Mesostics MESCD-0011[52] Herbert Henck – 2000, ECM New Series 1842[53] (2CD, incl. Henck's Festeburger Fantasien) Tim Ovens – c. 2002, CordAria CACD 566 (incl. a multimedia CD) Margaret Leng Tan – 2003, Mode 158[54] (CD and DVD, incl. many other works and several documentaries) Nora Skuta – 2004, Hevhetia Records HV 0011-2-131[55] (SACD) Giancarlo Simonacci – 2005, Brilliant Classics 8189 (3CD, part of Complete Music for Prepared Piano) Antonis Anissegos – 2014, WERGO (WER 67822) Kate Boyd – 2014, Navona Records (NV5984) [56] (CD, also includes Cage's In a Landscape) List of compositions by John Cage Works for prepared piano by John Cage String Quartet in Four Parts The Seasons Makrokosmos, several collections of works for prepared piano or played using extended technique (or both), by George Crumb ^ a b c Reiko Ishii. The Development of Extended Piano Techniques in Twentieth-Century American Music, pp. 38–41. The Florida State University, College of Music, 2005. Available online Archived 2008-01-15 at the Wayback Machine (accessed December 29, 2007).
 ^ a b c d Pritchett, p. 32.
 ^ Pritchett, Grove: "Sonatas and Interludes is a truly exceptional work and may be said to mark the real start of Cage's mature compositional life."
 ^ Nicholls, p. 80: "Most critics agree that Sonatas and Interludes (1946–48) is the finest composition of Cage's early period."
 ^ a b Cage, p. 57.
 ^ Nicholls, p. 81. In addition to problems in his artistic life, Cage also had to deal with personal troubles: a divorce from his wife of ten years in 1945, and a change in his sexuality that had happened by 1942. For details on this topic, see Marjorie Perloff, Charles Junkerman: John Cage: Composed in America, University of Chicago Press, 1994. .mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}ISBN 0-226-66057-5.
 ^ Pritchett, p. 36.
 ^ a b c John Cage interview by Johnatan Cott, 1963. Available as streaming audio at the Internet Archive (accessed December 29, 2007).
 ^ Cage, p. 127.
 ^ Cage, pp. 158, 226.
 ^ Cage quoted in Kostelanetz, p. 67.
 ^ Pritchett, p. 30.
 ^ a b c d e Cage, booklet text for Ajemian's recording of the cycle: John Cage: Sonatas and Interludes, Composers Recordings Inc. CRI 700 (reissue).
 ^ Perry, p. 48.
 ^ Kostelanetz, p. 67.
 ^ Cage quoted in Pritchett, p. 29.
 ^ Cage, pp. 19–57, essay Composition as Process. Cage uses the same description of the process several times, but it is unclear whether he refers to piano preparation only or composition as well.
 ^ Cage, p. 25.
 ^ "Sonata for Bolt & Screw". Time magazine. January 24, 1949. Retrieved 2007-12-15.
 ^ The New York Times review of the Carnegie Hall concert quoted in Pritchett, p. 35: "The work "left one with the feeling that Mr. Cage is one of this country's finest composers."
 ^ a b For details and the full text of Boulez' lecture, see Pierre Boulez, John Cage, Robert Samuels, Jean-Jacques Nattiez, The Boulez-Cage Correspondence, translated by Robert Samuels, Cambridge University Press, 1995. ISBN 0-521-48558-4.
 ^ Table of preparations in Edition Peters, 6755. Copyright 1960 by Henmar Press.
 ^ a b Perry, p. 39.
 ^ See examples of analysis in Perry, and in Nicholls, pp. 83–84. Nicholls also provides examples of sonatas connected by the use of identical note combinations.
 ^ Pritchett, pp. 29–33.
 ^ Score: Edition Peters, 6755. Copyright 1960 by Henmar Press.
 ^ Pritchett, p. 33. Analogous explanations are given in Nicholls pp. 82–83, Perry pp. 41–42, and many other sources.
 ^ Cage quoted in Pritchett, p. 30.
 ^ a b Nicholls, p. 83.
 ^ Pritchett, Grove.
 ^ "John Cage: Sonatas And Interludes For Prepared Piano". El Records. Archived from the original on January 11, 2008. Retrieved 2007-12-14.
 ^ "Yuji Takahashi plays John Cage – Sonatas and Interludes Vol.1–2". Fylkingen. Retrieved 2007-12-14.
 ^ "Cage: Sonatas & Interludes". explore records. Retrieved 2007-12-14.
 ^ "CDs – Sonatas & Interludes". Wergo. Retrieved 2007-12-14.
 ^ Holland, Bernard (December 13, 1987). "What Hath Liszt Wrought? Jean Sibelius, Henri Dutilleux and John Cage". The New York Times. Retrieved 2007-12-15.
 ^ "Nada Kolundžija – Recordings". NadaKolundzija.info. Archived from the original on 2007-12-30. Retrieved 2007-12-14.
 ^ Ev Grimes, John Cage. Conversation with American Composers, Music Educators Journal, Vol. 73, No. 3 (Nov., 1986), pp. 47–49, 58–59.
 ^ Tommasini, Anthony (April 23, 1989). "The Zest of the Uninteresting". The New York Times. Retrieved 2007-12-15.
 ^ "John Cage: Sonatas and Interludes". Edition RZ. Archived from the original on February 16, 2007. Retrieved 2007-12-14.
 ^ "Music by John Cage for Prepared Piano". Tall Poppies Records. Archived from the original on 2008-08-12. Retrieved 2007-12-14.
 ^ Larry J. Solomon. Review of Sonatas and Interludes for prepared piano by John Cage, performances by Louis Goldstein and Julia Steinberg. American Music, Vol. 16, No. 1 (Spring, 1998), pp. 130–133.
 ^ a b "John Cage – Piano Works 2". Mode Records. Retrieved 2007-12-14.
 ^ "Cage's Prepared Piano Revisited: Julie Steinberg, piano, on Music & Arts CD-937". Musicandarts.com. Archived from the original on 2008-01-27. Retrieved 2007-12-14.
 ^ "col legno". col legno.com. Retrieved 2007-12-14.
 ^ "John Cage (1912–1992) – Complete Piano Music Vol. 1". MDG Recording. Retrieved 2007-12-14.
 ^ "John Cage: Sonatas and Interludes". Bridge Records Inc. Archived from the original on October 21, 2007. Retrieved 2007-12-14.
 ^ "CAGE: Sonatas and Interludes for Prepared Piano". naxos.com. Retrieved 2007-12-14.
 ^ "CAGE: Sonatas and Interludes for Prepared Piano". naxos.com. Retrieved 2007-12-14.
 ^ "Perilous Night". SoundCircus. Archived from the original on 2007-12-06. Retrieved 2007-12-14.
 ^ "Materiali Sonori Produzioni". Materiali Sonori. Retrieved 2015-02-22.
 ^ "Sonatas and Interludes for Prepared Piano". iTunes. Retrieved 2015-02-22.
 ^ "Mesostics releases". d6.dion.ne.jp. Archived from the original on June 25, 2007. Retrieved 2007-12-14.
 ^ "ECM New Series 1842/43". ECM Records.
 ^ "John Cage – Volume 34: The Piano Works 7". Mode Records. Retrieved 2007-12-14.
 ^ "John Cage – Nora Skuta: Sonatas & Interludes". Hevhetia s.r.o. hudobné vydavatel'stvo a distribúcia. Archived from the original on August 8, 2007. Retrieved 2007-12-14.
 ^ "Sonatas and Interludes/In a Landscape – Kate Boyd, piano". Navona Records.
 John Cage. Silence: Lectures and Writings, Wesleyan Paperback, 1973 (first edition 1961). ISBN 0-8195-6028-6 Richard Kostelanetz. Conversing with John Cage, Routledge, 2003. ISBN 0-415-93792-2 David Nicholls. The Cambridge Companion to John Cage, Cambridge University Press, 2002. ISBN 0-521-78968-0 James Pritchett. The Music of John Cage, Cambridge University Press, 1993. ISBN 0-521-56544-8 James Pritchett; Laura Kuhn. "John Cage".  In Deane L. Root (ed.). Grove Music Online. Oxford Music Online. Oxford University Press. (subscription required) E.S. Baumgartner. Sonatas and Interludes, by John Cage: A Structural Analysis, Mills College, 1994. Gregory Jay Clough. Sonatas and Interludes for Prepared Piano (1946–48) by John Cage: An Analytical Basis for Interpretation, MM University of Arkansas, Fayetteville, 1968. Jeffrey Perry. "Cage's Sonatas and Interludes for prepared piano: performance, hearing and analysis", Music Theory Spectrum, Spring 2005, Vol. 27, No. 1, pp. 35–66. Sonatas and Interludes data sheet and discography at the John Cage database DEAD LINKS John Cage Revision Notes: Sonatas and Interludes for prepared piano, a technical essay on the work DEAD LINK James Pritchett: "Six views of the Sonatas and interludes" Sonata V performed by Bobby Mitchell, YouTube link 4'33" and Sonatas and Interludes for prepared piano performed by James Tenney at SASSAS sound, concert archive (streaming QuickTime format). v t e Imaginary Landscapes
No. 1
No. 2
No. 3
No. 4
No. 5 (1939–1952) No. 1 No. 2 No. 3 No. 4 No. 5 (1939–1952) Music for an Aquatic Ballet (1938) Living Room Music (1940) Sonatas and Interludes (1946–48) Music of Changes (1951) 4′33″ (1952) Variations (1958–67) Cheap Imitation (1969) HPSCHD (1969) Song Books (1970) Etudes Australes (1974–75) Etudes Boreales (1978) Freeman Etudes (1977–90) Roaratorio (1979) As Slow as Possible (1985/1987) But what about the noise...? (1986) Europeras (1987–91) Number Pieces (1987–92) Silence (1961) A Year from Monday (1968) Notations (1969) M (1973) Empty Words (1979) X (1983) The Revenge of the Dead Indians (1993) Works for prepared piano Indeterminacy in music West Coast School Foundation for Contemporary Arts v t e Sonata da camera Sonata da chiesa Sonatina Trio sonata Bassoon sonata Cello sonata Clarinet sonata Flute sonata Piano sonata Viola sonata Violin sonata (list) Bach
Organ
Solo Violin
Violin and Harpsichord
Gamba Organ Solo Violin Violin and Harpsichord Gamba Bartók
Piano
Solo Violin
Two Pianos and Percussion Piano Solo Violin Two Pianos and Percussion Beethoven
Piano
Horn Piano Horn Brahms
Clarinet Clarinet Chopin
Piano
Cello Piano Cello Corelli Debussy Elgar
Organ
Violin Organ Violin Franck
Violin Violin Grieg
Cello
Piano
Violin Cello Piano Violin Handel
Flute Flute Haydn
Piano Piano Janáček
Violin
Piano Violin Piano Kodály
Solo Cello Solo Cello Liszt
Piano Piano Mendelssohn
Organ Organ Mozart
Church Church Poulenc
two clarinets
clarinet and bassoon
horn, trumpet and trombone
Violin
Cello
Flute
Clarinet
Oboe two clarinets clarinet and bassoon horn, trumpet and trombone Violin Cello Flute Clarinet Oboe Rachmaninoff
Cello Cello Scarlatti Schubert
Violin Violin Shostakovich
Cello
Violin
Viola Cello Violin Viola Vivaldi
Cello Cello Fitzwilliam Sonatas History Sonatas and Interludes Sonata cycle Sonata form Sonata rondo form Sonata theory  List of sonatas  Category:Sonatas  Portal:Classical music Compositions by John Cage Webarchive template wayback links Articles with hAudio microformats Pages containing links to subscription-only content Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Español Français Italiano 日本語 Suomi Українська  This page was last edited on 27 September 2019, at 17:32 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Sonatas and Interludes Example 1 Forms and structural proportions in Sonatas and Interludes Example 2 a b c a b c d ^ ^ a b ^ ^ a b c ^ ^ ^ ^ a b c d e ^ ^ ^ ^ ^ ^ ^ a b ^ a b ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^  List of sonatas  Category:Sonatas  Portal:Classical music Sonatas I–IV    Interlude 1    Sonatas V–VIII Interludes 2–3 Sonatas IX–XII    Interlude 4    Sonatas XIII–XVI Sonatas I–IV    Interlude 1    Sonatas V–VIII
Interludes 2–3
Sonatas IX–XII    Interlude 4    Sonatas XIII–XVI Sonata I
 AABB
 1¼, ¾, 1¼, ¾, 1½, 1½
 7
 Sonata II
 AABB
 1½, 1½, 2⅜, 2⅜
 7¾
 Sonata III
 AABB
 1, 1, 3¼, 3¼
 8½
 Sonata IV
 AABB
 3, 3, 2, 2
 10
 Interlude 1
 N/A (no repeats)
 1½, 1½, 2, 1½, 1½, 2
 10
 Sonata V
 AABB
 2, 2, 2½, 2½
 9
 Sonata VI
 AABB
 2⅔, 2⅔, ⅓, ⅓
 6
 Sonata VII
 AABB
 2, 2, 1, 1
 6
 Sonata VIII
 AABB
 2, 2, 1½, 1½
 7
 Interlude 2
 N/A (no repeats)
 N/A (unclear)
 8
 Interlude 3
 AABBCCDD
 1¼, 1¼, 1, 1, ¾, ¾, ½, ½
 7
 Sonata IX
 ABBCC
 1, 2, 2, 1½, 1½
 8
 Sonata X
 AABBC
 1, 1, 1, 1, 2
 6
 Sonata XI
 AABCC
 2, 2, 3, 1½, 1½
 10
 Sonata XII
 AABB
 2, 2, 2½, 2½
 9
 Interlude 4
 AABBCCDD
 1, 1, 1, 1, 1, 1, 1¼, 1¼
 8½
 Sonata XIII
 AABB
 1½, 1½, 3½, 3½
 10
 Sonata XIV
 AABB
 2, 2, 3, 3
 10
 Sonata XV
 AABB
 2, 2, 3, 3
 10
 Sonata XVI
 AABB
 3½, 3½, 1½, 1½
 10
 
Imaginary Landscapes
No. 1
No. 2
No. 3
No. 4
No. 5 (1939–1952)
Music for an Aquatic Ballet (1938)
Living Room Music (1940)
Sonatas and Interludes (1946–48)
Music of Changes (1951)
4′33″ (1952)
Variations (1958–67)
Cheap Imitation (1969)
HPSCHD (1969)
Song Books (1970)
Etudes Australes (1974–75)
Etudes Boreales (1978)
Freeman Etudes (1977–90)
Roaratorio (1979)
As Slow as Possible (1985/1987)
But what about the noise...? (1986)
Europeras (1987–91)
Number Pieces (1987–92)
 
Silence (1961)
A Year from Monday (1968)
Notations (1969)
M (1973)
Empty Words (1979)
X (1983)
 
The Revenge of the Dead Indians (1993)
 
Works for prepared piano
Indeterminacy in music
West Coast School
Foundation for Contemporary Arts
 
Sonata da camera
Sonata da chiesa
Sonatina
Trio sonata
 
Bassoon sonata
Cello sonata
Clarinet sonata
Flute sonata
Piano sonata
Viola sonata
Violin sonata (list)
 
Bach
Organ
Solo Violin
Violin and Harpsichord
Gamba
Bartók
Piano
Solo Violin
Two Pianos and Percussion
Beethoven
Piano
Horn
Brahms
Clarinet
Chopin
Piano
Cello
Corelli
Debussy
Elgar
Organ
Violin
Franck
Violin
Grieg
Cello
Piano
Violin
Handel
Flute
Haydn
Piano
Janáček
Violin
Piano
Kodály
Solo Cello
Liszt
Piano
Mendelssohn
Organ
Mozart
Church
Poulenc
two clarinets
clarinet and bassoon
horn, trumpet and trombone
Violin
Cello
Flute
Clarinet
Oboe
Rachmaninoff
Cello
Scarlatti
Schubert
Violin
Shostakovich
Cello
Violin
Viola
Vivaldi
Cello
 
Fitzwilliam Sonatas
History
Sonatas and Interludes
Sonata cycle
Sonata form
Sonata rondo form
Sonata theory
 
 List of sonatas
 Category:Sonatas
 Portal:Classical music
 