
 Sterna portlandicaSterna pikei
 The Arctic tern (Sterna paradisaea) is a tern in the family Laridae. This bird has a circumpolar breeding distribution covering the Arctic and sub-Arctic regions of Europe, Asia, and North America (as far south as Brittany and Massachusetts). The species is strongly migratory, seeing two summers each year as it migrates along a convoluted route from its northern breeding grounds to the Antarctic coast for the southern summer and back again about six months later. Recent studies have shown average annual roundtrip lengths of about 70,900 km (44,100 mi) for birds nesting in Iceland and Greenland[3] and about 90,000 km (56,000 mi) for birds nesting in the Netherlands.[4] These are by far the longest migrations known in the animal kingdom. The Arctic tern flies as well as glides through the air. It nests once every one to three years (depending on its mating cycle); once it has finished nesting it takes to the sky for another long southern migration.
 Arctic terns are medium-sized birds. They have a length of 28–39 cm (11–15 in) and a wingspan of 65–75 cm (26–30 in).[5] They are mainly grey and white plumaged, with a red/orangish beak and feet, white forehead, a black nape and crown (streaked white), and white cheeks. The grey mantle is 305 mm, and the scapulae are fringed brown, some tipped white. The upper wing is grey with a white leading edge, and the collar is completely white, as is the rump. The deeply forked tail is whitish, with grey outer webs.
 Arctic terns are long-lived birds, with many reaching fifteen to thirty years of age. They eat mainly fish and small marine invertebrates. The species is abundant, with an estimated one million individuals. While the trend in the number of individuals in the species as a whole is not known, exploitation in the past has reduced this bird's numbers in the southern reaches of its range.
 The Arctic tern was known as sea swallow describing their slender shape as they swoop over the water.
 The genus name Sterna is derived from Old English "stearn", "tern".[6] The specific paradisaea is from Late Latin paradisus, "paradise".[7]
The Scots names picktarnie,[8] tarrock[9] and their many variants are also believed to be onomatopoeic, derived from the distinctive call.[10] Due to the difficulty in distinguishing the two species, all the informal common names are shared with the common tern.[11]
 The Arctic tern has a continuous worldwide circumpolar breeding distribution; there are no recognized subspecies. It can be found in coastal regions in cooler temperate parts of North America and Eurasia during the northern summer. During the southern summer, it can be found at sea, reaching the northern edge of the Antarctic ice.[12]
 The Arctic tern is famous for its migration; it flies from its Arctic breeding grounds to the Antarctic and back again each year, the shortest distance between these areas being 19,000 km (12,000 mi). The long journey ensures that this bird sees two summers per year and more daylight than any other creature on the planet.[13] One example of this bird's remarkable long-distance flying abilities involves an Arctic tern ringed as an unfledged chick on the Farne Islands, Northumberland, UK, in the northern summer of 1982, which reached Melbourne, Australia in October 1982, just three months from fledging — a journey of over 22,000 km (14,000 mi).[14] Another example is that of a chick ringed in Labrador, Canada, on 23 July 1928. It was found in South Africa four months later.[15]
 A 2010 study using tracking devices attached to the birds showed that the above examples are not unusual for the species. In fact, it turned out, previous research had seriously underestimated the annual distances travelled by the Arctic tern. Eleven birds that bred in Greenland or Iceland covered 70,900 km (44,100 mi) on average in a year, with a maximum of 81,600 km (50,700 mi). The difference from previous estimates is due to the birds' taking meandering courses rather than following a straight route as was previously assumed. The birds follow a somewhat convoluted course in order to take advantage of prevailing winds.[3] The average Arctic tern lives about thirty years, and will, based on the above research, travel some 2.4 million km (1.5 million mi) during its lifetime, the equivalent of a roundtrip from Earth to the Moon over 3 times.[16]
 A 2013 tracking study of half a dozen Arctic terns breeding in the Netherlands[4] shows average annual migrations of c. 90,000 km (56,000 mi). On their way south, these birds roughly followed the coastlines of Europe and Africa.[17] Having rounded the southern tip of Africa, they then turned east, some flying approximately halfway to Australia before again turning south to eventually reach Wilkes Land in the north-eastern Antarctic. One bird flew several hundred kilometres along the south coast of Australia before turning south for the Antarctic, while one flew along the entire south coast of Australia, passing between Australia and Tasmania. Having reached the Melbourne area, it turned south and flew in an arc to Wilkes Land in the north-east Antarctic, passing the south-western tip of New Zealand's South Island en route. Once back in the Netherlands, this bird had journeyed c. 91,000 km (57,000 mi), the longest migration yet recorded for any animal.
 Arctic terns usually migrate sufficiently far offshore that they are rarely seen from land outside the breeding season.[18]
 The Arctic tern is a medium-sized bird around 33–36 cm (13–14 in) from the tip of its beak to the tip of its tail. The wingspan is 76–85 cm (30–33 in).[18] The weight is  86–127 g (3.0–4.5 oz). The beak is dark red, as are the short legs and webbed feet. Like most terns, the Arctic tern has high aspect ratio wings and a tail with a deep fork.[18]
 The adult plumage is grey above, with a black nape and crown and white cheeks. The upperwings are pale grey, with the area near the wingtip being translucent. The tail is white, and the underparts pale grey. Both sexes are similar in appearance. The winter plumage is similar, but the crown is whiter and the bills are darker.[18]
 Juveniles differ from adults in their black bill and legs, "scaly" appearing wings, and mantle with dark feather tips, dark carpal wing bar, and short tail streamers.[18] During their first summer, juveniles also have a whiter forecrown.[19]
 The species has a variety of calls; the two most common being the alarm call, made when possible predators (such as humans or other mammals) enter the colonies, and the advertising call.[20] The advertising call is social in nature, made when returning to the colony and during aggressive encounters between individuals. It is unique to each individual tern and as such it serves a similar role to the bird song of passerines, identifying individuals. Eight other calls have been described, from begging calls made by females during mating to attack calls made while swooping at intruders.
 While the Arctic tern is similar to the common and roseate terns, its colouring, profile, and call are slightly different. Compared to the common tern, it has a longer tail and mono-coloured bill, while the main differences from the roseate are its slightly darker colour and longer wings. The Arctic tern's call is more nasal and rasping than that of the common, and is easily distinguishable from that of the roseate.[21]
 This bird's closest relatives are a group of South Polar species, the South American (Sterna hirundinacea), Kerguelen (S. virgata), and Antarctic (S. vittata) terns.[22] On the wintering grounds, the Arctic tern can be distinguished from these relatives; the six-month difference in moult is the best clue here, with Arctic terns being in winter plumage during the southern summer. The southern species also do not show darker wingtips in flight.
 The immature plumages of Arctic tern were originally described as separate species, Sterna portlandica and Sterna pikei.[23]
 Breeding begins around the third or fourth year.[24] Arctic terns mate for life and, in most cases, return to the same colony each year.[25] Courtship is elaborate, especially in birds nesting for the first time.[26] Courtship begins with a so-called "high flight", where a female will chase the male to a high altitude and then slowly descend. This display is followed by "fish flights", where the male will offer fish to the female. Courtship on the ground involves strutting with a raised tail and lowered wings. After this, both birds will usually fly and circle each other.[26]
 Both sexes agree on a site for a nest, and both will defend the site. During this time, the male continues to feed the female. Mating occurs shortly after this.[26] Breeding takes place in colonies on coasts, islands and occasionally inland on tundra near water. It often forms mixed flocks with the common tern. It lays from one to three eggs per clutch, most often two.[18]
 It is one of the most aggressive terns, fiercely defensive of its nest and young. It will attack humans and large predators, usually striking the top or back of the head. Although it is too small to cause serious injury to an animal of a human's size, it is still capable of drawing blood, and is capable of repelling many raptorial birds, polar bears [27] and smaller mammalian predators such as foxes and cats.[13] Other nesting birds, such as alcids, often incidentally benefit from the protection provided by nesting in an area defended by Arctic terns.
 The nest is usually a depression in the ground, which may or may not be lined with bits of grass or similar materials. The eggs are mottled and camouflaged.[18] Both sexes share incubation duties. The young hatch after 22–27 days and fledge after 21–24 days.[18] If the parents are disturbed and flush from the nest frequently the incubation period could be extended to as long as 34 days.[20]
 When hatched, the chicks are downy. Neither altricial nor precocial, the chicks begin to move around and explore their surroundings within one to three days after hatching.[28] Usually they do not stray far from the nest. Chicks are brooded by the adults for the first ten days after hatching.[29] Both parents care for hatchlings.[18] Chick diets always include fish, and parents selectively bring larger prey items to chicks than they eat themselves.[20] Males bring more food than females. Feeding by the parents lasts for roughly a month before being weaned off slowly.[18] After fledging, the juveniles learn to feed themselves, including the difficult method of plunge-diving.[30] They will fly south to winter with the help of their parents.[31]
 Arctic terns are long-lived birds that spend considerable time raising only a few young, and are thus said to be K-selected.[32] The bird has life span that was thought be around 20 years, however National Geographic, The University of Alberta & Massachusetts Institute of Technology, concluded in 2010 that more than 50% of this species will live past their 30th birthday. A study in the Farne Islands estimated an annual survival rate of 82%.[33]
 The diet of the Arctic tern varies depending on location and time, but is usually carnivorous. In most cases, it eats small fish or marine crustaceans.[12][18] Fish species comprise the most important part of the diet, and account for more of the biomass consumed than any other food. Prey species are immature (1–2-year old) shoaling species such as herring, cod, sandlances, and capelin.[13] Among the marine crustaceans eaten are amphipods, crabs and krill. Sometimes, these birds also eat molluscs, marine worms, or berries, and on their northern breeding grounds, insects.[28]
 Arctic terns sometimes dip down to the surface of the water to catch prey close to the surface. They may also chase insects in the air when breeding.[28] It is also thought that Arctic terns may, in spite of their small size, occasionally engage in kleptoparasitism by swooping at birds so as to startle them into releasing their catches.[28] Several species are targeted—conspecifics, other terns (like the common tern), and some auk and grebe species.[20]
 While nesting, Arctic terns are vulnerable to predation by cats and other animals.[12] Besides being a competitor for nesting sites, the larger herring gull steals eggs and hatchlings. Camouflaged eggs help prevent this, as do isolated nesting sites.[30] Scientists have experimented with bamboo canes erected around tern nests. Although they found fewer predation attempts in the caned areas than in the control areas, canes did not reduce the probability of predation success per attempt.[34] While feeding, skuas, gulls, and other tern species will often harass the birds and steal their food.[35] They often form mixed colonies with other terns, such as common and Sandwich terns.
 Arctic terns are considered threatened or a species of concern in certain states. They are also among the species to which the Agreement on the Conservation of African-Eurasian Migratory Waterbirds applies.[36] The species reduced population in New England in the late nineteenth-century because of hunting for the millinery trade.[20] Exploitation continues in western Greenland, where the population of the species has been reduced greatly since 1950.[37]
 In Iceland, the Arctic tern has been regionally uplisted to Vulnerable as of 2018, due to the crash of sandeel (Ammodytes spp.) stocks.[38]
At the southern part of their range, the Arctic tern has been reducing in numbers. Much of this is due to lack of food.[19] However, most of these birds' range is extremely remote, with no apparent trend in the species as a whole.[28]
 BirdLife International has considered the species to be at lower risk since 1988, believing that there are approximately one million individuals around the world.[2]
 The Arctic tern has appeared on the postage stamps of several countries and dependent territories. The territories include the Åland Islands, Alderney, and Faroe Islands. Countries include Canada, Finland, Iceland, and Cuba.[39]
 An Arctic tern in Finland
 An Arctic tern in flight with wings spread
 An Arctic tern chick on the Farne Islands, Northumberland, England
 An Arctic tern protecting nest on Amsterdam island, Svalbard
 Arctic tern nest with two eggs, at Thingvellir National Park, Iceland
 Chick camouflaged in creek bed (centre of picture), Coppermine River, Nunavut
 Eggs, Collection Museum Wiesbaden
 Close-up
 Drawing by Jos Zwarts
 
 
 1 Etymology 2 Distribution and migration 3 Description and taxonomy 4 Reproduction 5 Ecology and behaviour 6 Conservation status 7 Cultural depictions 8 Image gallery 9 References

9.1 Bibliography

 9.1 Bibliography 10 Further reading 11 External links 


An Arctic tern in Finland


 


An Arctic tern in flight with wings spread


 


An Arctic tern chick on the Farne Islands, Northumberland, England


 


An Arctic tern protecting nest on Amsterdam island, Svalbard


 


Arctic tern nest with two eggs, at Thingvellir National Park, Iceland


 


Chick camouflaged in creek bed (centre of picture), Coppermine River, Nunavut


 


Eggs, Collection Museum Wiesbaden


 


Close-up


 


Drawing by Jos Zwarts


 ^ BirdLife International (2016). "Sterna paradisaea". IUCN Red List of Threatened Species. Version 2013.2. International Union for Conservation of Nature. Retrieved 14 July 2018..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b "Arctic Tern — BirdLife Species Factsheet". birdlife.org. BirdLife International. Retrieved 17 August 2006.
 ^ a b "Arctic terns' flying feat". Reuters. 11 January 2010. Retrieved 20 January 2010.
 ^ a b Fijn, R.C.; Hiemstra, D.; Phillips, R.A.; van der Winden, J. (2013). "Arctic Terns Sterna paradisaea from the Netherlands migrate record distances across three oceans to Wilkes Land, East Antarctica". Ardea. 101: 3–12. doi:10.5253/078.101.0102.
 ^ "Arctic Tern". All About Birds. Cornell Lab of Ornithology.
 ^ "Sterna". Oxford English Dictionary (3rd ed.). Oxford University Press. September 2005. (Subscription or UK public library membership required.)
 ^ Jobling, James A (2010). The Helm Dictionary of Scientific Bird Names. London: Christopher Helm. p. 291. ISBN 978-1-4081-2501-4.
 ^ SND: Pictarnie Archived 30 May 2013 at the Wayback Machine
 ^ SND: tarrock Archived 30 May 2013 at the Wayback Machine
 ^ Hume (1993) pp. 12–13.
 ^ Cocker, Mark; Mabey, Richard (2005). Birds Britannica. London: Chatto & Windus. pp. 246–247. ISBN 0-7011-6907-9.
 ^ a b c "Arctic tern". Royal Society for the Protection of Birds. Retrieved 17 August 2006.
 ^ a b c Cramp, S., ed. (1985). Birds of the Western Palearctic. Oxford University Press. pp. 87–100. ISBN 0-19-857507-6.
 ^ Heavisides, A.; Hodgson, M.S.; Kerr, I. (1983). Birds in Northumbria 1982. Tyneside Bird Club.
 ^ "Birds of Nova Scotia: Arctic Tern". Nova Scotia Museum of Natural History. Archived from the original on 24 August 2006. Retrieved 22 August 2006.
 ^ Inman, Mason (12 January 2010). "World's Longest Migration Found--2X Longer Than Thought". National Geographic Society. Retrieved 19 August 2016.
 ^ "Dutch Arctic Terns migrating to Antarctica via Australia". BirdGuides.com. BirdGuides.
 ^ a b c d e f g h i j k Del Hoyo, Josep; Elliott, Andrew; Sargatal, Jordi, eds. (1996). Handbook of the Birds of the World. vol. 3. Barcelona: Lynx Edicions. p. 653. ISBN 84-87334-20-2.
 ^ a b Howell, Steve N.G.; Jaramillo, Alvaro (2006).  Alderfer, Jonathan (ed.). National Geographic Complete Birds of North America. National Geographic Society. pp. 272–273. ISBN 0-7922-4175-4.
 ^ a b c d e Hatch, J.J. (2002).  Poole, A.; Gill, F. (eds.). Arctic Tern (Sterna paradisaea). The Birds of North America. Philadelphia, PA.: The Birds of North America. p. 707.
 ^ Olson, Klaus Malling; Larsson, Hans (1995). Terns of Europe and North America. Princeton University Press. ISBN 0-7136-4056-1.
 ^ Bridge, E.S.; Jones, A.W.; Baker, A.J. (2005). "A phylogenetic framework for the terns (Sternini) inferred from mtDNA sequences: implications for taxonomy and plumage evolution" (PDF). Molecular Phylogenetics and Evolution. 35 (2): 459–469. doi:10.1016/j.ympev.2004.12.010. PMID 15804415. Archived from the original (PDF) on 20 July 2006. Retrieved 7 September 2006.
 ^ Kaufman, Kenn (1990). "chapter 18". Peterson Field Guides: Advanced Birding. p. 135. ISBN 0-395-53517-4.
 ^ Hawksley, Oscar (1957). "Ecology of a breeding population of Arctic Terns" (PDF). Bird-Banding. 28 (2): 57–92. doi:10.2307/4510623. JSTOR 4510623. Retrieved 1 September 2006.
 ^ Perrins (2003), p. 267
 ^ a b c Perrins (2003), p. 268
 ^ "Archived copy". Archived from the original on 27 November 2016. Retrieved 26 November 2016.CS1 maint: archived copy as title (link)
 ^ a b c d e Kaufman, Kenn (1996). Lives of North American birds. Houghton Mifflin. p. 260. ISBN 0-395-77017-3.
 ^ Klaassen, M.; Bech, C.; Masman, D.; Slagsvold, G. (1989). "Growth and energetics of Arctic tern chicks (Sterna paradisaea)" (PDF). Auk. 106: 240–248. Retrieved 1 September 2006.
 ^ a b Perrins (2003), p. 269
 ^ National Audubon Society. "Arctic Tern (Sterna paradisaea)". Archived from the original on 15 June 2006. Retrieved 1 September 2006.
 ^ Schreiber, Elizabeth A.; Burger, Joanne, eds. (2001). Biology of Marine Birds. Boca Raton: CRC Press. ISBN 0-8493-9882-7.
 ^ Cullen, J.M. (1957). "Plumage, age and mortality in the Arctic Tern". Bird Study. 4 (4): 197–207. doi:10.1080/00063655709475891.
 ^ Boothby, Claire; Redfern, Chris; Schroeder, Julia (2019). "An evaluation of canes as a management technique to reduce predation by gulls of ground-nesting seabirds". Ibis. 161 (2): 453–458. doi:10.1111/ibi.12702. ISSN 1474-919X.
 ^ Perrins (2003), p. 271
 ^ AEWA. "African Eurasian Waterbird Agreement Annex II: Species list". Archived from the original on 1 January 2006. Retrieved 17 August 2006.
 ^ Hansen, K. (2001). "Threats to wildlife in Greenland". Seabird Group Newsletter. 89: 1–2.
 ^ https://www.ni.is/node/27118 Kristinn Haukur Skarphéðinsson, "Arctic Tern," Icelandic Institute of Natural History, last updated October 2018.
 ^ Gibbons, Chris. "Arctic Tern stamps". Archived from the original on 28 September 2006. Retrieved 24 August 2006.
 Perrins, Christopher, ed. (2003). Firefly Encyclopedia of Birds. Buffalo, N.Y.: Firefly Books. ISBN 978-1-55297-777-4. OCLC 51922852. Harrison, Peter (2003). Seabirds of the World: A Photographic Guide. Princeton, N.J.: Princeton University Press. ISBN 978-0-691-01551-4. OCLC 487925419. The Arctic tern Migration Project Arctic tern – Species text in The Atlas of Southern African Birds "Arctic tern media". Internet Bird Collection. Arctic tern images at ARKive Arctic tern photo gallery at VIREO (Drexel University) Audio recordings of Arctic tern on Xeno-canto. Birds portal Animals portal Biology portal Arctic portal Wikidata: Q26800 Wikispecies: Sterna paradisaea ABA: arcter ADW: Sterna_paradisaea ARKive: sterna-paradisaea Avibase: BDC5CF80BE6CFC21 BioLib: 8711 BirdLife: 22694629 BTO: ARCTE CMS: sterna-paradisaea eBird: arcter EoL: 1049640 EUNIS: 1283 Euring: 6160 Fauna Europaea: 96960 Fauna Europaea (new): 28358914-ba1a-416d-8871-b65707053035 Fossilworks: 372658 GBIF: 5229230 GNAB: arctic-tern IBC: arctic-tern-sterna-paradisaea iNaturalist: 4449 IRMNG: 10999098 ITIS: 176890 IUCN: 22694629 NBN: NBNSYS0000000365 NCBI: 279953 Neotropical: arcter NZBO: arctic-tern NZOR: 24b72ab3-b196-4608-8c8c-dd2692ddf49e SeaLifeBase: 74526 TSA: 16886 WoRMS: 137165 Xeno-canto: Sterna-paradisaea GND: 4165908-9 LCCN: sh85006965 IUCN Red List least concern species Sterna Birds of the Arctic Holarctic birds Birds described in 1763 Taxa named by Erik Pontoppidan Webarchive template wayback links CS1: long volume value CS1 maint: archived copy as title Articles with short description Use dmy dates from June 2013 Articles with 'species' microformats Spoken articles Articles with hAudio microformats Commons category link is on Wikidata Featured articles Taxonbars with 30–34 taxon IDs Wikipedia articles with GND identifiers Wikipedia articles with LCCN identifiers Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Wikispecies Create a book Download as PDF Printable version Afrikaans العربية Asturianu Azərbaycanca বাংলা Български བོད་ཡིག Brezhoneg Català Cebuano Čeština Cymraeg Dansk Deutsch Diné bizaad Eesti Español Esperanto Euskara فارسی Français Gaeilge Galego 한국어 Հայերեն Hrvatski Íslenska Italiano עברית Latviešu Lietuvių Livvinkarjala Magyar മലയാളം မြန်မာဘာသာ Nederlands 日本語 Nordfriisk Norsk Norsk nynorsk Nouormand پنجابی Piemontèis Polski Português Русский Саха тыла Scots Simple English Slovenčina Slovenščina Српски / srpski Suomi Svenska தமிழ் తెలుగు Türkçe Українська Tiếng Việt Winaray 中文  This page was last edited on 9 November 2019, at 05:03 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  S. paradisaea Sterna paradisaea Arctic tern ^ a b a b a b 101 ^ ^ ^ ^ ^ ^ ^ a b c a b c ^ ^ ^ ^ a b c d e f g h i j k a b a b c d e ^ ^ 35 ^ ^ 28 ^ a b c ^ a b c d e ^ 106 a b ^ ^ ^ 4 ^ 161 ^ ^ ^ 89 ^ ^ Listen to this article More spoken articles Sterna paradisaea Sterna paradisaea 
 An Arctic tern on the Farne Islands
 Least Concern (IUCN 3.1)[1]
 Kingdom:
 Animalia
 Phylum:
 Chordata
 Class:
 Aves
 Order:
 Charadriiformes
 Family:
 Laridae
 Genus:
 Sterna
 Species:
 S. paradisaea
 Sterna paradisaeaPontoppidan, 1763[2]
 
 Range of S. paradisaea      Breeding grounds     wintering grounds  migration routes
 
Sterna portlandicaSterna pikei

  Wikimedia Commons has media related to Sterna paradisaea.  Wikispecies has information related to Sterna paradisaea 
Wikidata: Q26800
Wikispecies: Sterna paradisaea
ABA: arcter
ADW: Sterna_paradisaea
ARKive: sterna-paradisaea
Avibase: BDC5CF80BE6CFC21
BioLib: 8711
BirdLife: 22694629
BTO: ARCTE
CMS: sterna-paradisaea
eBird: arcter
EoL: 1049640
EUNIS: 1283
Euring: 6160
Fauna Europaea: 96960
Fauna Europaea (new): 28358914-ba1a-416d-8871-b65707053035
Fossilworks: 372658
GBIF: 5229230
GNAB: arctic-tern
IBC: arctic-tern-sterna-paradisaea
iNaturalist: 4449
IRMNG: 10999098
ITIS: 176890
IUCN: 22694629
NBN: NBNSYS0000000365
NCBI: 279953
Neotropical: arcter
NZBO: arctic-tern
NZOR: 24b72ab3-b196-4608-8c8c-dd2692ddf49e
SeaLifeBase: 74526
TSA: 16886
WoRMS: 137165
Xeno-canto: Sterna-paradisaea
 
GND: 4165908-9
LCCN: sh85006965
 