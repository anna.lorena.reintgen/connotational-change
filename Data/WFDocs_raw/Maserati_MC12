The Maserati MC12 (Tipo M144S) is a limited production two-seater sports car produced by Italian car maker Maserati to allow a racing variant to compete in the FIA GT Championship. The car entered production in 2004, with 25 cars produced. A further 25 were produced in 2005, making a total of 50 cars available for customers, each of which was pre-sold for €600,000 (US$670,541).[6][9] With the addition of 12 cars produced for racing, only a total of 62 of these cars were ever produced.[10]
 Maserati designed and built the car on the chassis of the Enzo Ferrari, but the final car is much larger and has a lower drag coefficient.[9] The MC12 is longer, wider and taller and has a sharper nose and smoother curves than the Enzo Ferrari, which has faster acceleration, better braking performance (shorter braking distance) and a higher top speed. The top speed of the Maserati MC12 is 330 kilometres per hour (205 mph) whereas the top speed of the Enzo Ferrari is 350 kilometres per hour (217.5 mph).[9][11]
 The MC12 was developed to signal Maserati's return to racing after 37 years.[12] The road version was produced to homologate the race version. One requirement for participation in the FIA GT is the production of at least 25 road cars. Three GT1 race cars were entered into the FIA GT with great success. Maserati began racing the MC12 in the FIA GT toward the end of the 2004 season, winning the race held at the Zhuhai International Circuit. The racing MC12s were entered into the American Le Mans Series races in 2005 but exceeded the size restrictions and consequently paid weight penalties due to excess range.
 Under the direction of Giorgio Ascanelli, Maserati began development of an FIA GT-eligible race car. This car, which would eventually be named the MC12, was initially called the "MCC" ("Maserati Corse Competizione") and was to be developed simultaneously with a road-going version, called the "MCS" ("Maserati Corse Stradale").[2] Frank Stephenson, Director of Ferrari-Maserati Concept Design and Development at the time, did the majority of the body styling, but the initial shape was developed during wind tunnel testing from an idea presented by Giorgetto Giugiaro.[13] The MCC has a very similar body shape to the MC12, but there are several key differences, most notably the rear spoiler.[2] Andrea Bertolini served as the chief test driver throughout development, although some testing was done by Michael Schumacher, who frequently tested the MCC at the Fiorano Circuit.[2] During the development process, the MCC name was set aside after Maserati established the car's official name, MC12.[2][14]
 The car is based heavily on the Enzo Ferrari, using a slightly modified version of the Ferrari Dino V12 engine, the same gearbox (but given the unique name of "Maserati Cambiocorsa") and the same chassis and track (length of axle between the wheels).[9] The windshield is the only externally visible component shared with the Enzo; the MC12 has a unique body which is wider, longer and slightly taller.[1] The increased size creates greater downforce across the MC12's body in addition to the downforce created by the two-metre spoiler.
 The MC12 is a two-door coupe with a targa top roof, although the detached roof cannot be stored in the car.[1] The mid-rear layout (engine between the axles but behind the cabin) keeps the centre of gravity in the middle of the car, which increases stability and improves the car's cornering ability. The standing weight distribution is 41% front and 59% rear. At speed, however, the downforce provided by the rear spoiler affects this to the extent that at 200 kilometres per hour (125 mph) the downforce is 34% front and 66% rear.[1]
 Even though the car is designed as a homologation vehicle and is a modification of a racing car, the interior is intended to be luxurious. The interior is a mix of gel-coated carbon fibre, blue leather and silver "Brightex", a synthetic material which was found to be "too expensive for the fashion industry".[11] The centre console features the characteristic Maserati oval analogue clock and a blue ignition button, but it has been criticised for lacking a radio, car stereo or a place to install an aftermarket sound system.[13][15]
 The body of the car, made entirely of carbon fibre, underwent extensive wind tunnel testing to achieve maximum downforce across all surfaces. As a result, the rear spoiler is two metres (79 in) wide but only 30 millimetres (1.2 in) thick, the underside of the car is smooth, and the rear bumper has diffusers to take advantage of ground effect.[6] Air is sucked into the engine compartment through the air scoop; its positioning on top of the cabin makes the car taller than the Enzo. The exterior is available only in the white-and-blue colour scheme, a tribute to the America Camoradi racing team that drove the Maserati Tipo Birdcages in the early 1960s. Bespoke colour schemes are available by paying an extra amount.[12][16] The car is noted for the awkwardness that results from its size; very long and wider than a Hummer H2.[15] This, combined with the lack of a rear window, can make parking the MC12 challenging.[15]
 The MC12 sports a 232 kg (511 lb), 6.0 L; 366.0 cu in (5,998 cc) Enzo Ferrari-derived longitudinally-mounted 65° V12 engine.[17] Each cylinder has 4 valves, lubricated via a dry sump system, with a compression ratio of 11.2:1.[18] These combine to provide a maximum torque of 652 N⋅m (481 lb⋅ft) at 5,500 rpm and a maximum power of 630 PS (621 bhp; 463 kW) at 7,500 rpm.[18] The redline rpm is indicated at 7,500—despite being safe up to 7,700—whereas the Enzo has its redline at 8,000 rpm.[1]
 The Maserati MC12 can accelerate from 0 to 100 km/h (62 mph) in 3.8 seconds (though Motor Trend Magazine managed 3.7 seconds) and on to 200 km/h (124 mph) in 9.9 seconds.[1][6][7] It can complete a standing (from stationary) 1⁄4 mile (402 m) in 11.3 seconds with a terminal speed of 200 km/h (124 mph) or a standing kilometre in 20.1 seconds.[1][7] The maximum speed of the Maserati MC12 is 330 km/h (205 mph).[7]
 Power is fed to the wheels through a rear-mounted, six-speed semi-automatic transmission. The gearbox is the same as the Enzo's transmission (tuned to different gear ratios) but renamed "Maserati Cambiocorsa". It provides a shift time of just 150 milliseconds and is mechanical with a 215 mm (8.5 in) twin-plate dry clutch.[6][9][19]
 The MC12's chassis is a monocoque made of carbon and nomex, with an aluminium sub-chassis at the front and rear. It has a roll bar to provide additional strength, comfort and safety.[1][12] Double wishbone suspension with push-rod-operated coil springs provide stability and dampers smooth the ride for the passengers.[20] The front of the car can be raised for speed bumps and hills by pressing a button that extends the front suspension.[13] There are two modes for the chassis' tuning which can also be changed with a button in the cabin: "sport", the standard setting, and "race", which features less of the "Bosch ASR" (anti-slip regulation) traction control, faster shifts and stiffer suspension.[6][21]
 The MC12 has 480-millimetre (19 in) wheels with a width of 230 millimetres (9 in) at the front and 330 millimetres (13 in) at the rear. The tyres are "Pirelli P Zero Corsa" with codes of 245/35 ZR 19 for the front tyres and 345/35 ZR 19 for the rear.[16] The brakes are Brembo disc brakes with a Bosch anti-lock braking system (ABS).[20] The front brakes have a diameter of 380 millimetres (15 in) with six-piston calipers and the rear brakes have a diameter of 335 millimetres (13.2 in) with four-piston calipers.[20] The centre-lock wheel nuts that hold the wheels to the chassis are colour-coded; red on the left of the car, blue on the right.[1]
 The car has generally received mixed reviews, with critics saying it is hard to drive, overpriced and too large.[22] Other criticisms include the lack of a trunk, rear window, spare tire and radio, and the way the car's engine was limited or "drugged".[1][9] Current driver for Vitaphone Racing Team, Andrea Bertolini, the chief test driver throughout the development, said the car, "reacts well and is very reliable in its reactions."[23]
 
The Top Gear television series acquired an MC12, and test driver The Stig achieved a lap time of 1:18.9 around the Top Gear track—0.1 seconds faster than his lap in the Enzo Ferrari.[24] Host Jeremy Clarkson also drove it, comparing it to the Maserati Biturbo, a car he disliked.[22] Clarkson criticised the car greatly, pointing out that, unlike the Enzo, it lacks a rear window. He also commented that it is "difficult" due to its size, and, "one of the twitchiest cars", he has ever driven, meaning a small action by the driver results in an exaggerated reaction from the car. For these reasons, he promptly renamed the car "The MC Hammer".[22] Regarding the design of a racing car and modification to road standards he said, "is it a racer? Is it a GT car? Is it a de-tuned Enzo in a fat suit? You can't really tell."[22] Despite his criticisms, he complimented the smooth ride.[25]
 Motor Trend Magazine reviewer Frank Markus had a more positive opinion. Despite initial skepticism he said, "It turns out that the Enzo makes a more comfortable and attractive road car when made over as a butch Maserati racer in street couture".[1] Markus complimented the stability of braking and the handling ability of the MC12, especially the drifting allowed by the traction control when cornering, commenting that "There's none of the knife-edged limit handling we criticised in the more extreme Enzo. It's even more forgiving at the limit than an Acura NSX."[1]
 When Automobile Magazine tested an MC12, reviewer Preston Lerner called it "user-friendly", praising the responsiveness and simplicity of driving.[26] Lerner approved of Frank Stephenson's work with the styling of both the car's exterior and interior, calling the trim "Speed-Racer-ish" but "without looking as though it belongs in a Nitrous-ized Civic".[26] He also complimented the ASR's level of intervention, commenting that it "lets the fun factor get reasonably high before kicking in".[26]
 In 2008, an MC12 was hotlapped at the Nurburgring Nordschleife and obtained a 7:24.29-second lap time.[27] This was also the second time an MC12 recorded a faster lap time than its Ferrari counterpart, with the Enzo lapping the track 1 second slower.[27]
 In 2004 Maserati completed three MC12 GT1 race cars intended for the FIA GT GT1 class.[28] The AF Corse factory-backed squad debuted the race at Imola, yet the FIA did not allow the MC12 to score points due to its debated homologation. Even with this setback, the team managed to take second and third places. At the next round at Oschersleben, the MC12 of Andrea Bertolini and Mika Salo won for the first time. At the final round of the year at Zhuhai, the FIA finally agreed to homologate the MC12s and allow them to score points towards the championship. With this, the MC12 again took victory, allowing it to score enough points to finish 7th in the teams championship.[29][30]
 In 2005 Maserati won the FIA GT Manufacturers Cup with 239 points: almost double the score of next competitor (Ferrari with 125 points).[31] The two teams that entered MC12s into the FIA GT, Vitaphone Racing and JMB Racing, finished first and second respectively in the Team Cup, with Vitaphone winning by a considerable margin.[31] Four of the MC12 drivers were in the running to win the FIA GT Drivers' Title at the Bahrain International Circuit at the start of the final race of 2005: Karl Wendlinger and Andrea Bertolini each on 71 points and Timo Scheider and Michael Bartels on 70. Gabriele Gardel of Ferrari was also on 70 points, however, and in the crucial race he placed ahead of all of the Maseratis, driving an older Ferrari 550 Maranello. Gardel took the title, leaving all of the Maserati drivers within four points of first place (Scheider and Wendlinger receiving four points for the race).[31][32]
 In 2006 the only team representing Maserati was Vitaphone Racing. On September 30, 2006, Vitaphone secured the Teams' Championship for the 2006 season despite their drivers placing 5th and 7th in the Budapest 500 km race with weight penalties of 85 kilograms and 105 kilograms respectively.[33] Bertolini and Bartels also shared first place in the Drivers' Championship on 71 points but the manufacturers cup went to Aston Martin.[34]
 Vitaphone Racing again won the GT1 Teams' Championship in the 2007 season on 115 points, followed by fellow MC12 team Scuderia Playteam Sarafree on 63 points.[35] JMB Racing also entered two MC12s, but they were used by amateur drivers competing in the Citation Cup, which was won by JMB's driver Ben Aucott.[36] Maserati also won the Manufacturers' Cup by a significant margin while Thomas Biagi won the Drivers' Championship. Fellow Vitaphone drivers Miguel Ramos and Christian Montanari tied for sixth, while Playteam's Andrea Bertolini and Andrea Piccini were just behind.[35]
 For 2008, Vitaphone Racing returned with a pair of MC12s for drivers Andrea Bertolini, Michael Bartels, and Miguel Ramos, as well as newcomer Alexandre Negrão.[37][38] The season ended with another Teams' Championship for Vitaphone Racing (122.5 points) and Drivers' Championship for Bertolini and Bartels. In the ninth round, the team fielded a third car under the name of Team Vitasystem, driven by Pedro Lamy and Matteo Bobbi which scored one point. JMB Racing retained a single MC12 for 2007 Citation Cup winner Ben Aucott and drivers Peter Kutemann and Alain Ferté, competing in the first five events of the championship.
 In the 2009 season the Vitaphone Racing won the fifth consecutive Team Championship, while Bertolini and Bartels gained their third Drivers' Championship. The other two drivers were Miguel Ramos and Alex Müller, who ended in sixth position. Starting from the fourth round, the team entered a third car under the name of Vitaphone Racing Team DHL, driven by Matteo Bobbi and Alessandro Pier Guidi, achieving good results: despite being only a one-car team, with a partial season involvement, they ended the Teams' Championship in fourth position (32 points), scoring a victory in the last round.
 With the inauguration of the FIA GT1 World Championship in 2010, Maserati continued their commitment to the series with two teams entering. Vitaphone Racing Team, the defending FIA GT Champions, won five races en route to the Drivers' and Teams' World Championships, but Maserati lost to Aston Martin in the Manufacturers' Trophy. The second team representing Maserati was Alfrid Heger's Triple H Team Hegersport.
 MC12s have had great success racing in Italy, and have replaced the GT3 "Maserati Trofeo Light" as Maserati's representative in the Italian GT Championship.[39] In 2005 Maserati introduced two MC12s to the GT1 division under Scuderia Playteam and Racing Box, with the teams placing first and third overall respectively.[40] The cars were re-entered in 2006, with Scuderia Playteam again securing overall victory and Racing Box coming second.[41][42][43] From 2007, GT1 cars are not permitted in the championship, and Scuderia Playteam moved to the FIA GT Championship.
 Racing Box also participated in the non-championship 6 Hours of Vallelunga twice, winning in 2005 with Michele Rugolo, Leonardo Maddelena, and Davide Mastracci,[44] then again in 2006 with Pedro Lamy, Marco Cioci, and Piergiuseppe Perazzini.[45]
 In 2006, the Le Mans winning outfit Team Goh was intending to race a Maserati MC12 in the Super GT series in Japan. However, the team was forced to withdraw because of driver problems (Jan Magnussen falling ill suddenly and returning to Denmark) and disappointing lap times at the Suzuka Circuit during testing.[46] While the car was faster than its Super GT rivals down the straights, it was losing more than a second per lap in the corners due to its poorer aerodynamics.[47]
 In 2004 the Maserati MC12s were unable to compete in series backed by the ACO, such as the Le Mans Endurance Series (LMES) in Europe and the American Le Mans Series (ALMS) because they exceeded both the length and width restrictions for their class.[1] The car's nose was shortened by 200 millimetres (7.9 in) to attempt to comply with regulations, but was still 66 millimetres (2.6 in) too wide. In 2005 the governing body of the ALMS, the International Motor Sports Association (IMSA), allowed the MC12s to compete as a guest with the agreement that they were not allowed to score championship points and were forced to run a weight penalty.[16] Some ALMS teams initially objected to the participation of the MC12 due to the possibility that an accident could eliminate their chances at the 24 Hours of Le Mans, but the MC12 was finally allowed to race. The ACO stood by their ruling on the car by forbidding it from entering other Le Mans series.
 The lone MC12 would be campaigned under the Maserati Corse banner, but run by the American Risi Competizione team. The 2005 American Le Mans Series season was not as successful for the team, with the team scoring no wins. In the final race at Mazda Raceway Laguna Seca, the MC12 was clipped by a competitor, causing damage that resulted in a lengthy pit stop.[48] After resuming the race, a loss of traction caused by cold tires made the car hit a curb, which broke the radiator and took the MC12 out of the race.[48]
 In August 2007, Fredy Lienhard and Didier Theys announced their preparation of a former FIA GT MC12 for use in the American Le Mans Series.[49] The car made its debut at Road America, finishing 3rd in the GT1 class after qualifying competitively. The only other race entered was Round 11 at Road Atlanta for the Petit Le Mans where the team failed to finish following an accident, but were still classified second in class.[50] Doran's Maserati however had qualified on the class pole.[51] The team selected Michelin tires instead of the Pirellis originally used by Maserati Corse in 2005,[49] and was also allowed to race with a full-width rear wing instead of the smaller wing used by Maserati Corse and teams in FIA GT, although the wing was not as tall.[52] IMSA also allowed Doran to score points in the American Le Mans Series championships.
 The Corse[53] is a variant of the MC12 intended for racetrack use. In contrast to the race version of the MC12, of which street-legal versions were produced for homologation purposes, the MC12 Corse is intended for private use, albeit restricted to the track, as the Corse's modifications make it illegal to drive on the road.
 The Corse was developed directly from the MC12 GT1, which won the 2005 FIA GT Manufacturers Cup.[54] The car was released in mid-2006, "in response to the customer demand to own the MC12 racing car and fueled by the growth in track days, where owners can drive their cars at high speeds in the safety of a race track", as stated by Edward Butler, General Manager for Maserati in Australia and New Zealand.[54][55] In similar fashion to the Ferrari FXX, although the owners are private individuals, Maserati is responsible for the storage, upkeep, and maintenance of the cars, and they are only driven on specially organized track days. Unlike the FXX, Corsas are not used for research and development, and are used only for entertainment.[56] 3 Maserati MC12 Corse were converted to road legal use by German tuning firm Edo Competition and feature a slight power increase, a butterfly intake exhaust system and adjustable road suspension system.[57]
 Only twelve MC12 Corsas were sold to selected customers, each of whom paid €1 million (US$1.47 million) for the privilege. Another three vehicles were produced for testing and publicity purposes.[55][58] The Corsa shares its engine with the MC12 GT1; the powerplant produces 755 PS (555 kW; 745 hp) at 8,000 rpm, 122 PS (90 kW; 120 hp) more than the road going MC12.[59] The MC12 Corse shares the GT1's shortened nose, which was a requirement for entry into the American Le Mans Series. The car was available in a single standard colour, named "Blue Victory", though the car's paint could be customized upon request.[55] The MC12 Corse possesses steel/carbon racing brakes, but is not fitted with an anti-lock braking system.[54]
 The Birdcage 75th is a concept car developed by automobile manufacturer Maserati and designed by Pininfarina. It was first introduced at the 2005 Geneva Auto Show and was based on the MC12's chassis. It draws inspiration from the Maserati Tipo 61 Birdcage of the 1960s and was made as a celebration of Pininfarina's 75th anniversary.[60]
 
 MC12 Versione Competizione[1][2] Maserati MC12 Stradale[1] Maserati MCC (development codename)[2][3] 2-door coupe 2-door targa top (road car) Enzo Ferrari Ferrari FXX 1 Development 2 Overview

2.1 Interior
2.2 Exterior
2.3 Engine
2.4 Chassis
2.5 Wheels

 2.1 Interior 2.2 Exterior 2.3 Engine 2.4 Chassis 2.5 Wheels 3 Reception 4 Racing

4.1 FIA GT
4.2 FIA GT1 World Championship
4.3 Italian GT
4.4 Super GT
4.5 American Le Mans Series

 4.1 FIA GT 4.2 FIA GT1 World Championship 4.3 Italian GT 4.4 Super GT 4.5 American Le Mans Series 5 MC12 Versione Corse 6 Birdcage 75th 7 See also 8 References 9 External links Riverside International Automotive Museum ^ a b c d e f g h i j k l m n Markus, Frank. "Motor Trend Road Test". Motor Trend. Retrieved 2006-10-02..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c d e f "Maserati MC12/MCC". Maserati Net. Archived from the original on December 14, 2006. Retrieved 2006-12-02.
 ^ "Serious Wheels: Maserati MCC". Serious Wheels. Retrieved 2006-09-29.
 ^ "Frank Stephenson: The Story". frankstephenson.com. Retrieved 10 June 2019.
 ^ Codling, Stuart (2011). Form Follows Function: The Art of the Supercar. James Mann (photographer), Frank Stephenson (commentary). Motorbooks. ISBN 9780760341162.
 ^ a b c d e f "Maserati Indy: MC12". Maserati Indy. Retrieved 2006-09-29.
 ^ a b c d e f g "Carfolio: Maserati MC12". Carfolio. Retrieved 2006-09-28.
 ^ "Car and Driver Maserati MC12 First Drive". Car and Driver.
 ^ a b c d e f Hall, Nick. "World Car Fans test drive MC12". World Car Fans. Retrieved 2006-09-28.
 ^ "The Top 10 Maserati Car Models Of All-Time". Money Inc. 2016-06-06. Retrieved 2018-11-02.
 ^ a b Dron, Peter (2005-04-16). "Telegraph: It costs how much?". The Daily Telegraph. London. Retrieved 2006-10-06.
 ^ a b c "2004 Maserati MC12". RSsportscars. Retrieved 2015-02-19.
 ^ a b c "Maserati MC12". Cool Supercars. 2006-11-26. Archived from the original on 2007-02-09. Retrieved 2006-12-02.
 ^ "Michael In The Maserati". dailysportscar.com. 2004-04-24. Retrieved 2008-05-24.
 ^ a b c Frank Mountain (2004). Fifth Gear: Maserati MC12 (Tv Series). Cadwell Park: Five.
 ^ a b c "Road and Track road tests: MC12". Road and Track. Archived from the original on September 27, 2007. Retrieved 2006-10-02.
 ^ "MC 12". vitaphone-racing.de. Archived from the original on October 21, 2007. Retrieved 2010-04-10.
 ^ a b "Technical Data: 2004 Maserati MC12". Global Car Locator. Archived from the original on 2005-04-04. Retrieved 2006-11-07.
 ^ Tan, Paul. "VW phases out automatics". Paul Tan. Retrieved 2006-12-02.
 ^ a b c d "Maserati MC12". supercars.net. Retrieved 2006-12-02.
 ^ "Cars: Maserati MC12". FIA GT. Archived from the original on 2006-12-15. Retrieved 2006-12-03.
 ^ a b c d Jeremy Clarkson, Richard Hammond (2005). Top Gear: Maserati MC12 (Tv Series). Top Gear Test Track: Top Gear.
 ^ "Interview With Andrea Bertolini". Maserati Corse. Retrieved 2006-10-28.
 ^ "Top Gear Power Laps". BBC. Retrieved 2010-04-10.
 ^ Clarkson, Jeremy (2005-02-27). "Maserati MC12". The Times. London. Retrieved 2010-04-10.
 ^ a b c Lerner, Preston. "2005 MC12". Automobile Mag. Retrieved 2006-11-07.
 ^ a b "Dinosaurs at Nordschleife - Enzo, MC12, CGT and CCX". Fastest Laps. 2008. Retrieved 9 November 2012.
 ^ "Maserati Net: 25 road going MC12s delivered". Maserati Net. Archived from the original on June 29, 2006. Retrieved 2006-09-29.
 ^ "FIA GT Standings of 2004". Maserati Corse. Retrieved 2006-09-29.
 ^ "FIA GT Dubai 2004". crash.net. Archived from the original on 2007-09-30. Retrieved 2006-10-29.
 ^ a b c "FIA GT Standings of 2005". Maserati Corse. Retrieved 2006-09-29.
 ^ "MC12". Luxury Cars. Retrieved 2006-10-26.
 ^ "FIA GT News: GT1 Teams title for Vitaphone Racing". FIA GT. Archived from the original on 2007-09-27. Retrieved 2006-10-06.
 ^ "FIA Championship 2006". FIA. Archived from the original on 2008-05-12. Retrieved 2008-04-14.
 ^ a b "2007 Fia GT Championship". FIA. Archived from the original on 2008-04-16. Retrieved 2008-04-14.
 ^ "Daoudi and Aucott win in Nogaro - Aucott wins the Citation Cup !". FIA. 1 October 2007. Archived from the original on 18 May 2008. Retrieved 2008-04-26.
 ^ "Bertolini returns to Vitaphone Racing Team". FIA. 29 February 2008. Archived from the original on 6 May 2008. Retrieved 2008-04-26.
 ^ "Vitaphone completes three-day test in Estoril". FIA. 19 February 2008. Archived from the original on 15 June 2008. Retrieved 2008-04-26.
 ^ "Maserati Trofeo 'Light'". Maserati Corse. Retrieved 2006-10-25.
 ^ "Italian GT Standings 2005". Maserati Corse. Retrieved 2006-10-25.
 ^ "Maserati 1-2 gives Maserati 1-2 World Titles". Maserati. Archived from the original on November 20, 2006. Retrieved 2006-11-05.
 ^ "Italian GT Standings 2006". Maserati Corse. Retrieved 2006-10-25.
 ^ "2006 Italian GT Championship". IMCA Slotracing. Archived from the original on October 29, 2006. Retrieved 2006-11-05.
 ^ "6 Hours of Vallelunga Reports — November 12–13". Daily Sports Car. Retrieved 2008-08-21.
 ^ "Vallelunga Gold Cup Report, November 19". Daily Sports Car. Retrieved 2008-08-21.
 ^ "Round 1 Test". super GT. Archived from the original on 2011-07-24. Retrieved 2006-11-07.
 ^ "Round 1 Official Test". Super GT. Archived from the original on 2011-07-24. Retrieved 2006-11-10.
 ^ a b "Unlucky Race for MC12 at Laguna Seca". Maserati Corse. Retrieved 2006-10-25.
 ^ a b "Maserati Poised to Make Return to ALMS Competition at Road America". backstretchmotorsports.com. Archived from the original on 2011-07-07. Retrieved 2010-04-10.
 ^ "Doran Racing's Lista and Lista Office Maserati finishes second in GT1 class in Saturday's Petit Le Mans at Road Atlanta". American Le Mans Series. 7 October 2007. Archived from the original on 16 January 2013. Retrieved 2008-04-26.
 ^ "Doran Racing's Bertolini wins the GT1 pole Friday at Petit Le Mans". American Le Mans Series. 5 October 2007. Archived from the original on 16 January 2013. Retrieved 2008-04-26.
 ^ "Lienhard, Theys hoping to make mark for Maserati". American Le Mans Series. 10 August 2007. Archived from the original on 16 January 2013. Retrieved 2008-04-26.
 ^ "Maserati MC12 Versione Corse". Maserati Heritage.
 ^ a b c "Maserati Launches its Ultimate Super car". Maserati. Archived from the original on October 7, 2007. Retrieved 2006-10-29.
 ^ a b c "2006 Maserati MC12 Corsa". RSsportscars. Retrieved 2015-02-19.
 ^ "IGN: Maserati MC12 Corsa". IGN. Retrieved 2006-10-02.
 ^ "Modified MC12 Corsaby Edo Competition". Edo Competition. Retrieved 2017-10-12.
 ^ "Ultimate Car Page: Maserati MC12 Corsa". Ultimate Car Page. Retrieved 2006-09-29.
 ^ "Vorstellung Maserati MC12 Corsa". Autobild. Archived from the original on 2011-07-18. Retrieved 2007-04-28.
 ^ "Maserati's bird of paradise". Top Gear. June 1, 2005. Archived from the original on March 17, 2007. Retrieved March 8, 2015.
 Maserati MC12 Corsa pictures, videos and specifications Maserati MC12 at the Internet Movie Cars Database Maserati MC12 GT1 serial number on Endurance-Series v t e Alfieri Ghibli Quattroporte VI GranTurismo GranCabrio Levante Spyder II Coupé GranSport Quattroporte V MC12 Racing Shamal Ghibli II Quattroporte IV 3200 GT Biturbo
222
2.24v.
420/425
430
422
4.24v.
4.18v.
Spyder I 222 2.24v. 420/425 430 422 4.24v. 4.18v. Spyder I Royale 228 Karif Indy Bora Merak Quattroporte II Khamsin Kyalami Quattroporte III A6 1500 A6G 2000 A6G/54 3500 GT 5000 GT Mistral Quattroporte I Sebring Mexico Ghibli I Maserati in motorsport Trofeo Maserati Grand Prix results Tipo 26 Tipo 26B 4CM Tipo 26M 8C V8RI 8CM 8CLT 8CTF 8CL 6CM 4CL/4CLT A6GCM 250F Tipo 6CS/46 A6GCS 150S 250S 200S 300S 350S 450S Tipo 61 "Birdcage" Tipo 63 Tipo 65 Tipo 151 Tipo 154 Barchetta Ghibli Open Cup Trofeo MC12 GT1 GranTurismo MC Simun Boomerang Tipo 124 Medici Chubasco Buran 320S Auge Kubang GT Wagon Chicane Birdcage 75th A8GCS Kubang Alfieri Maserati Brothers Maserati (motorcycle) A subsidiary of Fiat Chrysler Automobiles Corporate website  Category  Commons v t e Maserati vehicles Sports cars Grand tourer racing cars 2000s cars Rear mid-engine, rear-wheel-drive vehicles Articles with short description Pages using deprecated image syntax Commons category link is on Wikidata Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Bosanski Català Deutsch Español Français Italiano עברית Nederlands 日本語 Polski Português Русский Simple English Suomi Svenska ไทย Türkçe Українська  This page was last edited on 6 November 2019, at 12:45 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Maserati MC12 Ratio a b c d e f g h i j k l m n a b c d e f ^ ^ ^ a b c d e f a b c d e f g ^ a b c d e f ^ a b a b c a b c ^ a b c a b c ^ a b ^ a b c d ^ a b c d ^ ^ ^ a b c a b ^ ^ ^ a b c ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b a b ^ ^ ^ ^ a b c a b c ^ ^ ^ ^ ^ Maserati MC12 Category Commons Type  Maserati MC12 Versione Competizione[1][2]Maserati MC12 Stradale[1]Maserati MCC (development codename)[2][3] 2004–2005 (50+12 MC12 Versione Corse) Modena, Italy Frank Stephenson[4] Sports car (S)Racing car 2-door coupe2-door targa top (road car) Longitudinally-mounted, rear mid-engine, rear-wheel drive Enzo FerrariFerrari FXX 6.0 L (5,998 cc) Ferrari/Maserati F140 V12[5] 630 PS (463 kW; 621 hp) and 652 N⋅m (481 lbf⋅ft) of torque 6-speed Maserati Cambiocorsa semi-automatic[6] 2,800 mm (110.2 in)[7] 5,143 mm (202.5 in)[7] 2,096 mm (82.5 in)[7] 1,205 mm (47.4 in)[7] 1,497 kg (3,300 lb)[8] Maserati Bora Ratio[20] 3.15:1 2.18:1 1.57:1 1.19:1 0.94:1 0.71:1 4.10:1
  FIA GT1American Le Mans GT1 Maserati Carbon-fibre, nomex honeycomb monocoque chassis, front and rear aluminium safety subframes with roll cage Double wishbone, push rod operated over damper As front 5,143 mm (202 in) 2,096 mm (83 in) 1,205 mm (47 in) Front: 1,660 mm (65 in)Rear: 1,650 mm (65 in) 2,800 mm (110 in) Maserati 6.0 L (6,000 cc; 366 cu in) V12 naturally aspirated DOHC mid-engined, longitudinally mounted Maserati Cambiocorsa 6-speed sequential semi-automatic paddle-shift gearbox 1,250 kg (2,760 lb) (FIA GT)1,200 kg (2,600 lb) (ALMS) Ethanol E10 Michelin, Pirelli  AF Corse JMB Racing Vitaphone Racing Team Scuderia Playteam Racing Box Doran Racing Triple H Team Hegersport  Mika Salo, Andrea Bertolini, Karl Wendlinger, Michael Bartels, Timo Scheider, Thomas Biagi, Fabio Babini, Eric van de Poele, Jamie Davies, Andrea Piccini, Christian Montanari, Miguel Ramos, Stéphane Sarrazin, Alexandre Negrão, Alessandro Pier Guidi 2004 FIA GT Imola 500 km 
RacesWinsPolesF.Laps
9440N/AN/A 94 40 N/A N/A 6 (2005 – 2009 FIA GT, 2010 FIA GT1) 2 (2005 FIA GT, 2007 FIA GT) 6 (2006 Italian GT, 2006 – 2009 FIA GT, 2010 FIA GT1)  Wikimedia Commons has media related to Maserati MC12. Upcoming
Alfieri
Current
Ghibli
Quattroporte VI
GranTurismo
GranCabrio
Levante
2000–2009
Spyder II
Coupé
GranSport
Quattroporte V
MC12
1990–1999
Racing
Shamal
Ghibli II
Quattroporte IV
3200 GT
1980–1989
Biturbo
222
2.24v.
420/425
430
422
4.24v.
4.18v.
Spyder I
Royale
228
Karif
1970–1979
Indy
Bora
Merak
Quattroporte II
Khamsin
Kyalami
Quattroporte III
1946–1969
A6 1500
A6G 2000
A6G/54
3500 GT
5000 GT
Mistral
Quattroporte I
Sebring
Mexico
Ghibli I
 
Alfieri
 
Ghibli
Quattroporte VI
GranTurismo
GranCabrio
Levante
 
Spyder II
Coupé
GranSport
Quattroporte V
MC12
 
Racing
Shamal
Ghibli II
Quattroporte IV
3200 GT
 
Biturbo
222
2.24v.
420/425
430
422
4.24v.
4.18v.
Spyder I
Royale
228
Karif
 
Indy
Bora
Merak
Quattroporte II
Khamsin
Kyalami
Quattroporte III
 
A6 1500
A6G 2000
A6G/54
3500 GT
5000 GT
Mistral
Quattroporte I
Sebring
Mexico
Ghibli I
 
Maserati in motorsport
Trofeo Maserati
Grand Prix results
Grand Prix/Monoposto
Tipo 26
Tipo 26B
4CM
Tipo 26M
8C
V8RI
8CM
8CLT
8CTF
8CL
6CM
4CL/4CLT
A6GCM
250F
Sports racing cars
Tipo 6CS/46
A6GCS
150S
250S
200S
300S
350S
450S
Tipo 61 "Birdcage"
Tipo 63
Tipo 65
Tipo 151
Tipo 154
GT/One make
Barchetta
Ghibli Open Cup
Trofeo
MC12 GT1
GranTurismo MC

 
Tipo 26
Tipo 26B
4CM
Tipo 26M
8C
V8RI
8CM
8CLT
8CTF
8CL
6CM
4CL/4CLT
A6GCM
250F
 
Tipo 6CS/46
A6GCS
150S
250S
200S
300S
350S
450S
Tipo 61 "Birdcage"
Tipo 63
Tipo 65
Tipo 151
Tipo 154
 
Barchetta
Ghibli Open Cup
Trofeo
MC12 GT1
GranTurismo MC
 
Simun
Boomerang
Tipo 124
Medici
Chubasco
Buran
320S
Auge
Kubang GT Wagon
Chicane
Birdcage 75th
A8GCS
Kubang
Alfieri
 
Maserati Brothers
Maserati (motorcycle)
 
A subsidiary of Fiat Chrysler Automobiles
Corporate website
 Category
 Commons
 Type
 1980s
 1990s
 2000s
 2010s
 0 1 2 3 4 5 6 7 8 9
 0 1 2 3 4 5 6 7 8 9
 0 1 2 3 4 5 6 7 8 9
 0 1 2 3 4 5 6 7 8 9
 Ownership
 De TomasoGEPI
 De TomasoGEPIChrysler
 De TomasoFiat S.p.A.
 Fiat S.p.A.
 FiatFerrari
 Ferrari
 Fiat S.p.A.
 FCA
 Executive
 Saloon
 
 425 / 420 / 430 / 422 / 4.18v. / 4.24v.
 
 
 
 Ghibli
 Coupé
 
 Biturbo / 222 / 2.24v. / Racing
 
 
 
 Spyder
 
 Spyder
 
 
 
 Luxury
 Saloon
 Quattroporte III
 Royale
 
 Quattroporte IV
 
 Quattroporte V
 Quattroporte VI
 GT
 4-seat
 Coupé
 Kyalami
 
 228
 
 
 GranTurismo
 Convertible
 
 
 
 GranCabrio
 2+2
 Coupé
 
 
 Ghibli
 3200 GT
 Coupé
 
 
 Khamsin
 
 Shamal
 
 
 GranSport
 
 
 2-seat
 Coupé
 
 Karif
 
 
 
 Spyder
 
 
 
 Spyder
 
 
 Mid-engine
 2+2
 Merak
 
 
 
 
 Sports car
 
 
 
 MC12
 
 
 SUV
 
 
 
 
 Levante
 