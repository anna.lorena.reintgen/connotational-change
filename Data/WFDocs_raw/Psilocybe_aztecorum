
 Psilocybe aztecorum is a species of psilocybin mushroom in the family Hymenogastraceae. Known from Arizona, Colorado, central Mexico, India and Costa Rica, the fungus grows on decomposing woody debris and is found in mountainous areas at elevations of 2,000 to 4,000 m (6,600 to 13,100 ft), typically in meadows or open, grassy conifer forests. The mushrooms have convex to bell-shaped caps 1.5–2 cm (0.6–0.8 in) in diameter, atop slender cylindrical stems that are up to 7.5 cm (3.0 in) long. The color of the caps changes with variations in hydration, ranging from dark chestnut brown to straw yellow or whitish when dry. The base of the stem is densely covered with conspicuous white rhizomorphs, a characteristic uncommon amongst Psilocybe species.
 The species was first reported by French mycologist Roger Heim in 1956 as a variety of Psilocybe mexicana before he officially described it under its current name a year later. Named for its association with the Nahua people also called Aztecs, P. aztecorum may have been one of the sacred mushroom species, or teonanácatl (A Nahuatl word translated variously as "sacred mushroom" or "flesh of the gods"), reported in the codices of 16th-century Spanish chronicler Bernardino de Sahagún. The mushrooms are still used for spiritual ceremonies by Nahua Indians in the Popocatépetl region, although this traditional usage is waning. The variety P. aztecorum var. bonetii has smaller spores than the main variety, and is found at lower elevations with Montezuma pine (Pinus montezumae) and sacred fir (Abies religiosa). P. aztecorum may be distinguished from similar temperate species such as P. baeocystis and P. quebecensis by their ranges, and by differences in the morphology of microscopic structures like cystidia.
 The species was first mentioned by French mycologist Roger Heim in 1956 based on material collected by American ethnomycologist R. Gordon Wasson in Paso de Cortés, on the slopes of Popocatépetl mountain in Mexico. Heim originally named the species as a variety of Psilocybe mexicana; limited to dried mushroom material for analysis, he only described the spores, which he explained were "relatively longer and narrower than that of Psilocybe mexicana".[2] A year later, Heim renamed the fungus Psilocybe aztecorum and officially described it, in addition to several other Mexican Psilocybe taxa.[3] Some of these mushrooms, including P. aztecorum, were illustrated in the popular American weekly magazine Life ("Seeking the Magic Mushroom"), in which Wasson recounted the psychedelic visions that he experienced during the divinatory rituals of the Mazatec people, thereby introducing psilocybin mushrooms to Western popular culture.[4]
 In 1978, Mexican mycologist and Psilocybe specialist Gastón Guzmán emended the description of P. aztecorum to include the color variation of the cap resulting from its strongly hygrophanous nature, the mycenoid form, the rhizoids at the base of the stem, the lignicolous habitat, and the size of the spores—all features that he thought were either confused, or not sufficiently detailed, in Heim's original description.[5] In the same publication, Guzmán also characterized the variety P. aztecorum var. bonetii, distinguished from the main variety by its smaller spores.[5] He had originally described this variant as a separate species, Psilocybe bonetii, in 1970.[6] Further, Guzmán later published Psilocybe natarajanii, originally described by him from Tamil Nadu in southern India,[7] as a synonym of P. aztecorum var. bonetii;[8] this putative synonymy, however, is confirmed by neither MycoBank nor Index Fungorum.[9][10] Guzmán called the main variety P. aztecorum var. aztecorum; for this reason, the species authority is often cited as "P. aztecorum var. aztecorum R. Heim emend. Guzmán".[11][12]
 Psilocybe aztecorum is the type species of Guzmán's section Aztecorum, a group of bluing (i.e., psilocybin-containing) Psilocybe mushrooms characterized by having a strongly hygrophanous cap that dries to brown or brownish white when dry; spores that appear asymmetrical when seen in side view; and pleurocystidia that, when present, are hyaline (translucent). Other species classified in section Aztecorum are P. baeocystis and P. quebecensis.[1]
 The specific epithet aztecorum refers to the Aztec Indians of central Mexico, who used this mushroom in traditional ceremonies long before the Spanish came to America. The variety P. aztecorum var. bonneti is named after Dr. Federico Bonet (died 1980), emeritus professor of the Escuela Nacional de Ciencias Biológicas, who assisted Guzmán with his doctoral studies.[1] The popular names of P. aztecorum are niños or niñitos (children or little children), or in the Nahuatl language apipiltzin[1] which means  niños del agua ("children of the water"), alluding to their habitat along ravines.[13]
 The cap is convex to bell-shaped, sometimes developing a broad umbo before expanding and flattening in age; it reaches a diameter of 1.5–2 cm (0.6–0.8 in). In maturity, the cap eventually forms a central depression, and, in some old specimens, opens into the hollow stem. The cap surface is slimy to the touch, and has translucent striations along the margin when moist. The cap is strongly hygrophanous, meaning that it will change color depending on its level of hydration. The color ranges from yellowish brown to golden yellow in young button forms to brownish gray in age, with greenish-gray tints on the margin. The color later changes to whitish from the center to the margin, finally remaining completely white; dried specimens are straw-colored to pale brownish. In contrast to most psilocybin mushrooms, the cap of P. aztecorum does not have a strong bluing reaction upon injury—only the margin stains slightly green-blue.[1]
 The gills are adnate (broadly attached to the stem slightly above the bottom of the gill) or adnexed (reaching the stem, but not attached to it), and are light violet gray to dark violet brown. They are either uniform in color, or have whitish edges. The hollow stem is 5.5 to 7.5 cm (2.2 to 3.0 in) by 3 to 4 mm (0.12 to 0.16 in) thick, equal in width throughout or thicker at the top, cylindric or sometimes flattened, and either straight or with turns and windings. Its surface is smooth, silky-fibrillose, whitish to greyish, and stains blue-green irregularly when touched or in age. The base of the stem is densely covered with well-developed white rhizomorphs. Young mushrooms have a white cobweb-like partial veil that does not last long before it disappears, although it sometimes remains as a non-permanent ring on the upper part of the stem. The flesh is whitish to yellowish or reddish yellow in the cap, or reddish brown in the stem, and shows little or no bluing reaction to injury. Like most of the bluing Psilocybe mushrooms, the odor and taste of P. aztecorum is slightly farinaceous (similar to freshly ground flour) in fresh specimens; dried specimens have a more intense odor. A drop of dilute potassium hydroxide (KOH) stains the cap, stem, and flesh reddish brown; sometimes, the stem does not stain or stains slightly yellowish red. The spore print is blackish violet.[1]
 The spores are elongated-ellipsoid in face view, roughly terete (more or less cylindrical but usually tapering at both ends), slightly inequilateral or asymmetrical in side view—the so-called "mango" form. They typically have dimensions of 12–14 by 6.6–7.7 by 6–7.5 μm, although some spores have irregular shapes and are strongly elongated, up to 23 μm. Spores are thick-walled (generally between 1–1.5 μm), dark yellowish brown, and have a broad germ pore. The variety bonetii has smaller spores measuring 10–13 by 6–7.5 by 6–7 μm.[1]
 The basidia (spore-bearing cells) measure 24–33 by 6.6–8.8 μm, and may be attached to anywhere from one to four spores, although four-spored basidia are most common. They are hyaline to sometimes somewhat yellowish, club-shaped or roughly cylindrical, and some have a slight constriction around the middle. The cheilocystidia (cystidia on the edge of a gill) are abundant, forming a sterile band on the gill edge. They are hyaline, fusoid-ampullaceous (with a shape ranging from a spindle to a swollen bottle), with dimensions of 20–45 by 5–8.2 μm, and have a filamentous neck measuring 6–11 by 1.6–2.5 μm. The pleurocystidia (cystidia on the gill face) are scattered, similar to the cheilocystidia in form and size, hyaline, and some have bifurcated or branched necks.[1]
 The subhymenium (a layer of cells immediately below the hymenium) consists of spherical cells that are interwoven with hyphae; this layer is hyaline to yellowish or brownish, and does not have pigment crusted on the walls of the hyphae. The epicutis (the upper of two layers of the cap cuticle) is made of a thin gelatinous layer of hyaline or brownish hyphae measuring 1.5–2.5 μm in diameter. The hypodermium (the cuticle tissue layer under the epicutis) is hyaline, and has elongated to roughly spherical hyphae that are 10–18 μm in diameter. Clamp connections are present in the hyphae of P. aztecorum.[1]
 Psilocybe pseudoaztecorum, found in India, differs from P. aztecorum in the morphology of the pleurocystidia and cheilocystidia. The characteristic filamentous neck present in the cystidia of P. aztecorum is absent in P. pseudoaztecorum.[14] P. pseudoaztecorum had been previously described by K. Natarajan and N. Raman as P. aztecorum,[15] but they published the taxon with a new name after consultation with Guzmán.[14] Fresh specimens of P. aztecorum resemble P. pelliculosa,[16] but this latter species is found only in the Pacific Northwest region of the United States and Canada.[8] Like P. aztecorum, the caps of the South African species P. natalensis also bleach to nearly white when dried.[17] The closely related P. baeocystis also bleaches in color to white when dry. Found in northwest North America from British Columbia to Washington and Oregon, P. baeocystis has thinner cheilocystidia than P. aztecorum (typically measuring 20–32 by 4.4–6 μm) and its pleurocystidia, when present, are found only near the gill edge.[18] P. quebecensis, known only from Quebec, Canada, has pleurocystidia measuring 12–25 by 5–10 μm. Although the phylogeny of the species comprising section Aztecorum is not known with certainty, Guzmán has suggested that P. aztecorum was the ancestor of P. baeocystis in northwestern North America and of P. quebecensis in northeastern North America.[19]
 A lignicolous species, Psilocybe aztecorum  lives in and decays dead wood, leaves, sticks, or other similar organic debris. Mushrooms typically fruit in groups of 5 to 20, sometimes in bundles. Usual substrates include wood debris buried in soil, twigs or very rotten logs, and, rarely, pine cones. The mushroom is found in woodlands (a low-density forest or wooded area that allows sunlight to penetrate to the forest floor) containing Hartweg's pine (Pinus hartwegii) in addition to grasses such as Festuca tolucensis and Muhlenbergia quadridentata, and the herbaceous plant Alchemilla procumbens, at elevations of 3,200–4,000 m (10,500–13,100 ft).[20] Heim found the type specimens at an altitude of 3,500 m (11,500 ft) in an alpine pine forest.[3] P. aztecorum fruits from August to October.[1]
 Psilocybe aztecorum is known only from the high mountains of central Mexico, such as Sierra Nevada, Nevado de Toluca, and La Malinche in the States of Mexico, Puebla, and Tlaxcala. According to Guzmán, it is likely that the species also grows in other areas with high mountains, such as the States of Nuevo Leon, Veracruz, Colima, and Chiapas, which have ecological conditions similar to those of the known localities. Variety bonetii grows in the same substrata as the type variety, mainly on humus, but only in forests with Montezuma pine (Pinus montezumae) and sacred fir (Abies religiosa), between 2,000–3,300 m (6,600–10,800 ft) elevation; it has not been recorded from Hartweg's pine forests. Also known only from Mexico, in the states of Mexico and Morelos, and in the Federal District, P. aztecorum var. bonetii usually fruits from August to November.[1] According to Guzmán, P. aztecorum should be of conservation concern owing to loss of its natural habitat.[20]
 Psilocybe aztecorum contains the psychoactive compound psilocybin. In 1958, Swiss chemist Albert Hofmann reported a relatively low concentration of 0.02% psilocybin, but this analysis was performed on two-year-old specimens.[1] Jonathan Ott and Guzmán indicated the presence of psilocybin in the variety bonartii.[21] In terms of psychoactive potency, Paul Stamets rates P. aztecorum as "moderately to highly active".[11]
 The statue of the Aztec "god of flowers", Xochipilli, a 16th-century stone effigy unearthed on the side of the volcano Popocatépetl, depicts a single figure seated cross-legged upon a temple-like base; his body is covered in carvings of sacred and psychoactive organisms. Circular patterns on his kneecaps, right forearm, and headdress have been interpreted by R. Gordon Wasson as stylized fruit bodies of Psilocybe aztecorum. Wasson says that the convex shape and incurved margins depicted in these images show the mushroom caps just before maturity.[22][23] P. aztecorum is, in addition to P. caerulescens, one of two mushrooms thought to be the species described by 16th-century Spanish chronicler Bernardino de Sahagún as the teonanácatl. The word teonanácatl (Nahuatl pronunciation: [teoːnaˈnakat͡ɬ]) has been variously translated as "sacred or divine mushroom" or as "flesh of the gods".[13][24][25] These mushrooms, considered holy sacraments by the Aztecs, were consumed during spiritual and divinatory rituals to induce hallucinatory visions.[1][11][26]
 Psilocybe aztecorum is still used ceremonially by the indigenous people of Oaxaca,[27] and by Nahua people in the Popocatépetl region,[12][28] although this usage is gradually diminishing.[1] Traditional folk healers, or curanderos, maintain familiarity with psychoactive mushrooms (and other mind-altering plants used in ceremonial rituals), and diagnose illnesses by having the client ingest the mushrooms. One Mixe curandero  initiation ritual involves ingestion of mushrooms following a period of "abstinence from talking, sexual intercourse, and all foods except nuts for three days, whereupon the individual goes up to a mountain, subsists on nothing but a little honey, and prays to God for the power to heal."[29]
 
 P. mexicana var. longispora Heim (1956) 1 Taxonomy and nomenclature 2 Description

2.1 Microscopic characteristics
2.2 Similar species

 2.1 Microscopic characteristics 2.2 Similar species 3 Habitat and distribution 4 Entheogenic use 5 See also 6 References

6.1 Cited texts

 6.1 Cited texts 7 External links Fungi portal Legal status of psilocybin mushrooms List of psilocybin mushrooms ^ a b c d e f g h i j k l m n Guzmán (1983), pp. 323–6.
 ^ Heim R. (1956). "Les champignons divinatoires recueillis par Mme. V. P. Wasson et M. R. G. Wasson au cours de leurs missions de 1954 et 1955 dans le pays mije, mazateque, zapoteque et nahua du Mexique méridional et central" [The divinatory mushrooms collected by Mrs. V. P. Wasson and R. G. Wasson during their missions in 1954 and 1955 in the Mazatec, Zapotec, and Nahua areas of southern and central Mexico]. Comptes rendus hebdomadaires des séances de l'Académie des sciences (in French). 242: 1389–95..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b Heim R. (1957). "Notes préliminaires sur les agarics hallucinogènes du Mexique" [Preliminary notes on the hallucination-producing Agarics of Mexico]. Revue de Mycologie (in French). 22 (1): 58–79.
 ^ Wasson RG. (13 May 1957). "Seeking the magic mushroom". Life. Time Inc.: 101–20. ISSN 0024-3019.
 ^ a b Guzmán G. (1978). "Variation, distribution, ethno-mycological data and relationships of Psilocybe aztecorum, a Mexican hallucinogenic mushroom". Mycologia. 70 (2): 385–96. doi:10.2307/3759037. PMID 566852.
 ^ Guzmán G. (1968). "Nueva especie de Psilocybe de la Sección Caerulescentes de los bosques de coníferas de México" [New species of Psilocybe of the section caerulescentes from the Mexican coniferous forests]. Anales de la Escuela Nacional de Ciencias Biológicas (in Spanish). 17 (1–4): 9–16. ISSN 0365-1932.
 ^ Guzmán G. (1995). "Supplement to the monograph of the genus Psilocybe". Bibliotheca Mycologica. 159: 91–141.
 ^ a b Guzmán G, Allen JW, Gartz J (2000). "A worldwide geographical distribution of the neurotropic fungi, an analysis and discussion" (PDF). Annali del Museo Civico di Rovereto: Sezione Archeologia, Storia, Scienze Naturali. 14: 189–280.
 ^ "Psilocybe natarajanii Guzmán 1995". MycoBank. International Mycological Association. Retrieved 2012-01-09.
 ^ "Record details: Psilocybe natarajanii Guzmán, Biblthca Mycol. 159: 109 (1995)". Index Fungorum. CAB International. Retrieved 2012-01-09.
 ^ a b c Stamets (1996), pp. 92–3.
 ^ a b Guzmán G. (2008). "Hallucinogenic mushrooms in Mexico: an overview". Economic Botany. 62 (3): 404–12. doi:10.1007/s12231-008-9033-8.
 ^ a b Singer R. (1958). "Mycological investigations on teonanácatl, the Mexican hallucinogenic mushroom. Part I. The history of teonanácatl, field work and culture work". Mycologia. 50 (2): 239–61. doi:10.2307/3756196.
 ^ a b Natarajan K, Raman N (1985). "A new species of Psilocybe from India". Mycologia. 77 (1): 158–61. doi:10.2307/3793262. JSTOR 3793262.
 ^ Natarajan K, Raman N (1983). South Indian Agaricales. Bibliotheca Mycologica. 89. Vaduz, Liechtenstein: Lubrecht & Cramer. pp. 106–10. ISBN 978-3-7682-1344-8.
 ^ Beug M. (2011). "The Genus Psilocybe in North America" (PDF). Fungi Magazine. 4 (3): 6–17. Archived from the original (PDF) on 2012-03-31.
 ^ Stamets (1996), p. 134.
 ^ Guzmán (1983), p. 327.
 ^ Guzmán (1983), p. 331.
 ^ a b Guzmán G. (1998). "Inventorying the fungi of Mexico". Biodiversity and Conservation. 7 (3): 369–84. doi:10.1023/A:1008833829473.
 ^ Ott J. (1976). "Detection of psilocybin in species of Psilocybe, Panaeolus and Psathyrella". Lloydia. 39 (4): 258–60.
 ^ Wasson G. (1974). "The role of 'flowers' in Nahuatl culture: a suggested interpretation". Journal of Psychoactive Drugs. 6 (3): 351–60. doi:10.1080/02791072.1974.10471987.
 ^ Granziera P. (2001). "Concept of the garden in pre-Hispanic Mexico". Garden History. 29 (2): 185–213. JSTOR 1587370.
 ^ Hofmann A. (1971). "Teonanácatl and Ololiuqui, two ancient magic drugs of Mexico". Bulletin on Narcotics. 23 (1): 3–14.
 ^ Knauth LG. (1962). "The teonanacatl in pre-conquest accounts and today" (PDF). Estudios de Cultura Nahuatl. 5: 263–75.
 ^ Stamets (1996), pp. 11–2.
 ^ Stamets (1996), p. 110.
 ^ Montoya A, Hernández-Totomoch O, Estrada-Torres A, Kong A, Caballero J (2003). "Traditional knowledge about mushrooms in a Nahua community in the State of Tlaxcala, México". Mycologia. 95 (5): 793–806. doi:10.2307/3762007. JSTOR 3762007. PMID 21148986.
 ^ Lipp (1998), pp. 150–1.
 Guzmán G. (1983). The Genus Psilocybe: A Systematic Revision of the Known Species Including the History, Distribution, and Chemistry of the Hallucinogenic Species. Beihefte Zur Nova Hedwigia. Heft 74. Vaduz, Liechtenstein: J. Cramer. ISBN 978-3-7682-5474-8. Lipp FJ. (1998). The Mixe of Oaxaca: Religion, Ritual, and Healing. Austin, Texas: University of Texas Press. ISBN 978-0-292-74705-0. Stamets P. (1996). Psilocybin Mushrooms of the World: An Identification Guide. Berkeley, California: Ten Speed Press. ISBN 0-89815-839-7. Schultes RE. (1940). "Teonanacatl: The narcotic mushroom of the Aztecs". American Anthropologist. New Series. 42 (3, Part 1): 429–43. doi:10.1525/aa.1940.42.3.02a00040. Psilocybe aztecorum  in Index Fungorum YouTube Microscopy of pleurocystidia v t e Mushroom hunting mushroom poisoning
list of deadly fungus species
list of poisonous fungus species
list of psilocybin mushrooms list of deadly fungus species list of poisonous fungus species list of psilocybin mushrooms legal status
isoxazole mushrooms
psilocybin mushrooms isoxazole mushrooms psilocybin mushrooms A. muscaria
A. muscaria var. guessowii A. muscaria var. guessowii A. pantherina C. cyanopus C. kuehneriana C. siligineoides G. steglichii G. aeruginosus G. braendlei G. junonius G. liquiritiae G. luteofolius G. luteoviridis G. luteus G. purpuratus G. sapineus G. validipes G. viridans I. aeruginascens I. corydalina var. corydalina I. tricolor M. cyanorrhiza M. pura P. africanus P. bisporus P. cambodginiensis P. cinctulus P. cyanescens P. fimicola P. olivaceus P. tropicalis P. cyanopus P. smithii P. americanus P. brunneidiscus P. cyanopus P. glaucus P. nigroviridis P. phaeocyanopus P. salicinus P. villosus P. allenii P. atlantis P. aucklandii P. aztecorum P. azurescens P. baeocystis P. banderillensis P. caerulescens var. caerulescens P. caerulipes P. cubensis P. cyanescens P. cyanofibrillosa P. fimetaria P. graveolens P. hispanica P. hoogshagenii P. liniformans var. americana P. makarorae P. meridionalis P. mescaleroensis P. mexicana P. ovoideocystidiata P. pelliculosa P. plutonia P. quebecensis P. samuiensis P. semilanceata P. silvatica P. strictipes P. stuntzii P. subaeruginascens P. subaeruginosa P. tampanensis P. villarrealiae P. weraroa P. zapotecorum Wikidata: Q794194 EoL: 191578 Fungorum: 304476 GBIF: 5242509 iNaturalist: 206120 IRMNG: 10987288 MycoBank: 304476 NCBI: 2126013 NZOR: e3238a21-7715-456c-8137-a056510275c0 Entheogens Fungi described in 1957 Fungi of Mexico Psilocybe Psychedelic tryptamine carriers Psychoactive fungi CS1 French-language sources (fr) CS1 Spanish-language sources (es) Articles with short description Articles with 'species' microformats Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Cebuano Deutsch Español Français Italiano Latina Polski Ślůnski Svenska Winaray  This page was last edited on 28 October 2019, at 21:28 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  P. aztecorum Psilocybe aztecorum gills convex campanulate adnate adnexed bare purple saprotrophic psychoactive Psilocybe aztecorum a b c d e f g h i j k l m n ^ 242 a b 22 ^ a b 70 ^ 17 ^ 159 a b 14 ^ ^ a b c a b 62 a b 50 a b 77 ^ 89 ^ 4 ^ ^ ^ a b 7 ^ 39 ^ 6 ^ 29 ^ 23 ^ 5 ^ ^ ^ 95 ^ 42 Psilocybe aztecorum 
 In Nevado de Toluca, Mexico
 Kingdom:
 Fungi
 Division:
 Basidiomycota
 Class:
 Agaricomycetes
 Order:
 Agaricales
 Family:
 Hymenogastraceae
 Genus:
 Psilocybe
 Species:
 P. aztecorum
 Psilocybe aztecorumR.Heim (1957)
 
P. mexicana var. longispora Heim (1956)
  Mycological characteristics gills on hymenium cap is convex
   or campanulate hymenium is adnate
   or adnexed stipe is bare spore print is purple ecology is saprotrophic edibility: psychoactive 
 
 
 
 Cheilocystidia and spores; small divisions are 1 μm
 Basidia
 Cap cuticle
 Spores
  Wikimedia Commons has media related to Psilocybe aztecorum. 
Mushroom hunting
mushroom poisoning
list of deadly fungus species
list of poisonous fungus species
list of psilocybin mushrooms
legal status
isoxazole mushrooms
psilocybin mushrooms
 Amanita
A. muscaria
A. muscaria var. guessowii
A. pantherina
 
A. muscaria
A. muscaria var. guessowii
A. pantherina
 Conocybe
C. cyanopus
C. kuehneriana
C. siligineoides
Galerina
G. steglichii
Gymnopilus
G. aeruginosus
G. braendlei
G. junonius
G. liquiritiae
G. luteofolius
G. luteoviridis
G. luteus
G. purpuratus
G. sapineus
G. validipes
G. viridans
Inocybe
I. aeruginascens
I. corydalina var. corydalina
I. tricolor
Mycena
M. cyanorrhiza
M. pura
Panaeolus
P. africanus
P. bisporus
P. cambodginiensis
P. cinctulus
P. cyanescens
P. fimicola
P. olivaceus
P. tropicalis
Pholiotina
P. cyanopus
P. smithii
Pluteus
P. americanus
P. brunneidiscus
P. cyanopus
P. glaucus
P. nigroviridis
P. phaeocyanopus
P. salicinus
P. villosus
Psilocybe
P. allenii
P. atlantis
P. aucklandii
P. aztecorum
P. azurescens
P. baeocystis
P. banderillensis
P. caerulescens var. caerulescens
P. caerulipes
P. cubensis
P. cyanescens
P. cyanofibrillosa
P. fimetaria
P. graveolens
P. hispanica
P. hoogshagenii
P. liniformans var. americana
P. makarorae
P. meridionalis
P. mescaleroensis
P. mexicana
P. ovoideocystidiata
P. pelliculosa
P. plutonia
P. quebecensis
P. samuiensis
P. semilanceata
P. silvatica
P. strictipes
P. stuntzii
P. subaeruginascens
P. subaeruginosa
P. tampanensis
P. villarrealiae
P. weraroa
P. zapotecorum
 
C. cyanopus
C. kuehneriana
C. siligineoides
 
G. steglichii
 
G. aeruginosus
G. braendlei
G. junonius
G. liquiritiae
G. luteofolius
G. luteoviridis
G. luteus
G. purpuratus
G. sapineus
G. validipes
G. viridans
 
I. aeruginascens
I. corydalina var. corydalina
I. tricolor
 
M. cyanorrhiza
M. pura
 
P. africanus
P. bisporus
P. cambodginiensis
P. cinctulus
P. cyanescens
P. fimicola
P. olivaceus
P. tropicalis
 
P. cyanopus
P. smithii
 
P. americanus
P. brunneidiscus
P. cyanopus
P. glaucus
P. nigroviridis
P. phaeocyanopus
P. salicinus
P. villosus
 
P. allenii
P. atlantis
P. aucklandii
P. aztecorum
P. azurescens
P. baeocystis
P. banderillensis
P. caerulescens var. caerulescens
P. caerulipes
P. cubensis
P. cyanescens
P. cyanofibrillosa
P. fimetaria
P. graveolens
P. hispanica
P. hoogshagenii
P. liniformans var. americana
P. makarorae
P. meridionalis
P. mescaleroensis
P. mexicana
P. ovoideocystidiata
P. pelliculosa
P. plutonia
P. quebecensis
P. samuiensis
P. semilanceata
P. silvatica
P. strictipes
P. stuntzii
P. subaeruginascens
P. subaeruginosa
P. tampanensis
P. villarrealiae
P. weraroa
P. zapotecorum
 
Wikidata: Q794194
EoL: 191578
Fungorum: 304476
GBIF: 5242509
iNaturalist: 206120
IRMNG: 10987288
MycoBank: 304476
NCBI: 2126013
NZOR: e3238a21-7715-456c-8137-a056510275c0
 