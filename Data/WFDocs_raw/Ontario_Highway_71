
  King's Highway 71, commonly referred to as Highway 71, is a provincially maintained highway in the Canadian province of Ontario. The 194-kilometre-long (121 mi) route begins at the Fort Frances–International Falls International Bridge in Fort Frances, continuing from US Route 53 (US 53) and US Route 71 (US 71) in Minnesota, and travels west concurrently with Highway 11 for 40 kilometres (25 mi) to Chapple. At that point, Highway 11 continues west while Highway 71 branches north and travels 154 kilometres (96 mi) to a junction with Highway 17 just east of Kenora. Highway 71 is part of the Trans-Canada Highway for its entire length.
 The current routing of Highway 71 was created out of a route renumbering that took place on April 1, 1960, to extend Highway 11 from Thunder Bay to Rainy River. The portion of the highway that is concurrent with Highway 11 follows the Cloverleaf Trail, constructed by the end of 1880s and improved over the next several decades. The portion between Highway 11 and Highway 17 follows the Heenan Highway, constructed to connect the Rainy River region with Kenora and the remainder of Ontario's road network; before its opening, the area was accessible only from across the United States border. Both highways were incorporated into the provincial highway system in 1937 following the merger of the Department of Highways (DHO) and the Department of Northern Development.
 Highway 71 connects the Rainy River region with the Trans-Canada Highway near Kenora. The first 65 kilometres (40 mi) of the highway traverses the largest pocket of arable land in northern Ontario. Following that, the route suddenly enters the Canadian Shield, where the land is unsuitable for agricultural development.[3]
 The highway begins at the international bridge in Fort Frances; within the United States, the road continues south as US 53 and US 71 in Minnesota. From the bridge, it proceeds along Central Avenue, encountering Highway 11 one block north. The two routes travel north concurrently to 3 Street West, where both turn west. At the Fort Frances Cemetery, the route branches southwest and exits Fort Frances after splitting with the Colonization Road (Highway 602). It follows the old Cloverleaf Trail west through Devlin, where it intersects Highway 613, and Emo, where it merges with the Colonization Road. Approximately six kilometres (3.7 mi) west of Emo, in the Manitou Rapids First Nations Reserve, Highway 71 branches north, while Highway 11 continues west to Rainy River.[4]
 North of the Manitou Rapids Reserve, Highway 71 presses through a large swath of land mostly occupied by horse and cattle ranches. It intersects Highway 600 and Highway 615, both of which have historical connections to Highway 71.[4] The highway passes through Finland and enters the Boreal Forest, descending into the Canadian Shield over the course of a kilometre and a half (approximately one mile).[5] From this point to its northern terminus, the highway crosses through rugged and isolated terrain, curving around lakes, rivers and mountains on its northward journey. It passes through the community of Caliper Lake before crossing between Rainy River District and Kenora District midway between there and Nestor Falls.
 North of Nestor Falls, the highway travels along the eastern shore of Lake of the Woods, providing access to the community of Crow Lake on the Sabaskong Bay 35D reserve of the Ojibways of Onigaming First Nation between Lake of the Woods and Kakagi Lake,[6][7] as well as to Whitefish Bay just southeast of Sioux Narrows.[4] Here the route crosses the Sioux Narrows Bridge, the last part of the highway to be constructed and a formidable engineering obstacle in the 1930s.[8] North of Sioux Narrows, the highway meanders northward through an uninhabited region, zigzagging among the numerous lakes that dot Kenora District and crossing the Black River. It provides access to Eagle Dogtooth and Rushing River Provincial Parks several kilometres south of its northern terminus at Highway 17, four kilometres (2.5 mi) east of the split with Highway 17A and 20 kilometres (12 mi) east of downtown Kenora.[4]
 Highway 71 was created out of a renumbering of several highways in the Rainy River District during the late 1950s as Highway 11 was extended west of Thunder Bay. The history of the route is tied to the two major highways in Rainy River District: the Cloverleaf Trail and the Heenan Highway.
 The Cloverleaf Trail, the older of the two roads, was initially developed as the Rainy River colonization road. A line was blazed as early as 1875, possibly as part of the Dawson Trail,[9] and improved in 1885 into a trail. This initial trail followed the Rainy River west from Fort Frances to Lake of the Woods; Highway 602 now follows the road between Fort Frances and Emo.[10] In 1911, James Arthur Mathieu was elected as a Member of Provincial Parliament (MPP) in the Rainy River riding. As a lumber merchant, Mathieu promoted improved road access in the region. Between 1911 and 1915, he oversaw construction of the gravel Cloverleaf Trail between Fort Frances and Rainy River.[9]
 The Heenan Highway would become the first Canadian link to the Rainy River area; before its opening in the mid-1930s, the only way to drive to the area was via the United States.[10] In 1922, Kenora MPP Peter Heenan and Dr. McTaggart approached the government to lobby for construction of a road between Nestor Falls and Kenora.[11]
Nestor Falls was the northernmost point accessible by road from the Rainy River area.[12] Heenan would become the Minister of Lands and Forests in Mitch Hepburn's cabinet.[13] This provided the impetus for construction to begin in 1934.[14] Unlike the Cloverleaf Trail, the Fort Frances – Kenora Highway, as it was known prior to its opening, was constructed through the rugged terrain of the Canadian Shield. Rocks, forests, lakes, muskeg, and insects served as major hindrances during construction of the 100-kilometre-long (62 mi) highway, which progressed from both ends. By late 1935, the only remaining gap in the road was the Sioux Narrows Bridge. Construction on this bridge was underway by March 1936; it was rapidly assembled using old-growth Douglas fir from British Columbia (BC) as the main structural members. These timbers were cut in BC, and shipped to be built on-site like a jig-saw puzzle. The bridge was finished on June 15, 1936, completing the link between Fort Frances and Kenora.[8][15]
 On July 1, 1936, Premier Mitch Hepburn attended a ceremony in front of the Rainy Lake Hotel in Fort Frances.[8] On a rainy afternoon, at 5:30 p.m., Peter Heenan handed Hepburn a pair of scissors with which to cut the ribbon crossing the road and declare the highway open. Hepburn, addressing the crowd that was gathered, asked "What would you say if we call it the Heenan Highway, what would you think of that?". The crowd cheered and Hepburn cut the ribbon.[11]
 The Cloverleaf Trail and the Heenan Highway were assumed by the DHO shortly after its merger with the Department of Northern Development. Following the merger, the DHO began assigning trunk roads throughout northern Ontario as part of the provincial highway network.[16][17] Highway 71 was assigned on September 1, 1937, along the Cloverleaf Trail. The portion of the Heenan Highway lying within Kenora District was designated as Highway 70 on the same day. The portion within Rainy River District was designated as Highway 70 on September 29.[1]
 The original route of Highway 70 split in two south of Finland; Highway 70 turned east to Off Lake Corner, then south to Emo, while Highway 70A turned west to Black Hawk then south to Barwick. The northern end of the highway was also concurrent with Highway 17 for 21.7 kilometres (13.5 mi) into Kenora, and the southern end concurrent with Highway 71 for 37.0 kilometres (23.0 mi) between Emo and Fort Frances.[18] During 1952, the highway was extended south from its split to Highway 71, midway between Barwick and Emo. By 1953, the new road was opened and informally designated as the new route of Highway 70. The old routes were decommissioned on February 8, and the new route designated several weeks later on March 10, 1954.[19] Both forks were later redesignated as Highway 600 and Highway 615.[4]
 Throughout the mid- to late 1950s, a new highway was constructed west from Thunder Bay towards Fort Frances. Initially this road was designated as Highway 120. In 1959, it was instead decided to make this new link a westward extension of Highway 11; a major renumbering took place on April 1, 1960: Highway 11 was established between Rainy River and Fort Frances, Highway 71 was truncated west of the Highway 70 junction, and the entirety of Highway 70 was renumbered as Highway 71.[20][21][22] This established the current routing of the highway.[4]
 Although now rebuilt as a steel structure, the original Sioux Narrows Bridge was considered to be the longest single span wooden bridge in the world, at 64 metres (210 ft). The original bridge remained in place until 2003, when an engineering inspection revealed that 78% of the structure had failed. A temporary bridge was erected while a new structure was built. The new bridge was completed in November 2007, incorporating the old timber truss as a decorative element. A ribbon cutting ceremony to dedicate the bridge was held on July 1, 2008, 72 years after the original dedication by Mitch Hepburn.[15]
 The following table lists the major junctions along Highway 71, as noted by the Ministry of Transportation of Ontario.[2] 
 Route map:  Ontario provincial highways List 400-series Former 1 Route description 2 History 3 Major intersections 4 References 5 External links       Concurrency terminus       Tolled ^ Canada-bound drivers pay US$7 to cross bridge. No toll for US-bound drivers.
 ^ a b "Appendix 3 - Schedule of Assumptions and Reversions". Annual Report (Report). Department of Highways. March 31, 1938. p. 80..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b Ministry of Transportation of Ontario (2010). "Annual Average Daily Traffic (AADT) counts". Retrieved December 14, 2013.
 ^ Rainy River District Agricultural Economic Impact Study (PDF) (Report). Harry Cummings and Associates. October 2009. pp. iv, 17. Archived from the original (PDF) on March 31, 2012. Retrieved November 19, 2011.
 ^ a b c d e f MapArt (2010). Ontario Back Road Atlas (Map). Peter Heiler. p. 106. § G3–H4. ISBN 978-1-55198-226-7.
 ^ Google (September 5, 2011). "Highway 71 north of Finland, Ontario"  (Map). Google Maps. Google. Retrieved September 5, 2011.
 ^ "Toporama (on-line map and search)". Atlas of Canada. Natural Resources Canada. Retrieved 2018-09-13.
 ^ "Ontario Geonames GIS (on-line map and search)". Ontario Ministry of Natural Resources and Forestry. 2014. Retrieved 2018-09-13.
 ^ a b c "Sioux Narrows Bridge". Sioux Narrows Nestor Falls. Archived from the original on September 3, 2011. Retrieved November 19, 2011.
 ^ a b "Between the Ripples...Stories of Chapple". Fort Frances Times. 1997. Retrieved January 5, 2014.
 ^ a b "Travel's Come a Long Way Since Voyageurs". Alberton Centennial Times. April 5, 1978. Retrieved January 5, 2014.
 ^ a b "Highway Officially Opened By Premier M.F. Hepburn Named "Heenan Highway"". Fort Frances Times and Rainy Lake Herald. July 2, 1936. Retrieved November 19, 2011.
 ^ Touring Department (1927). Road Map of Ontario (Map). Ontario Motor League. Northwestern portion inset.
 ^ Sessional Papers (Report). 66. Province of Ontario. 1935. Retrieved January 5, 2014.
 ^ Sessional Papers (Report). 70. Province of Ontario. 1939. Retrieved November 19, 2011. In Kenora the contract for construction of 48 miles of grading and gravelling on the Heenan Highway, started in 1934, was officially completed and this Highway, linking Fort Frances and Rainy River to Kenora was officially opened in June.
 ^ a b "The Sioux Narrows Bridge". Lake of the Woods Vacation Area. Archived from the original on January 2, 2014. Retrieved January 1, 2014.
 ^ Shragge, John; Bagnato, Sharon (1984). From Footpaths to Freeways. Ontario Ministry of Transportation and Communications, Historical Committee. p. 71. ISBN 0-7743-9388-2.
 ^ Smith, R. M (March 31, 1938). "Ontario and its Highways". Annual Report (Report). Department of Highways. p. 8.
 ^ Robins, C.P. (1956). Ontario Road Map (Map). Ontario Department of Highways. Northern portion inset. § F4–H6.
 ^ "Appendix 3 - Schedule of Assumptions and Reversions of Sections". Annual Report (Report). Department of Highways. March 31, 1954. pp. 154, 156.
 ^ Robins, C.P. (1959). Ontario Road Map (Map). Ontario Department of Highways. Northern portion inset. § F4–H6.
 ^ Robins, C.P. (1960). Ontario Road Map (Map). Ontario Department of Highways. Northern portion inset. § F4–H6.
 ^ Information Section (November 9, 1959). "No title" (Press release). Department of Highways.
   Media related to Ontario Highway 71 at Wikimedia Commons v t e Highway 1 Highway 5 Highway 16 Highway 1 Highway 16 Highway 1 Highway 16 Highway 1 Highway 16 Highway 100 Highway 17 Highway 69 Highway 400 Highway 12 Highway 7 Highway 71 Highway 11 Highway 66 Highway 417 Autoroute 40 Autoroute 25 Autoroute 20 Autoroute 85 Route 185 Route 117 Autoroute 15 Route 2 Route 16 Confederation Bridge Route 1 Highway 104 Highway 105 Highway 106 Route 1  Category  Roads portal  Canada portal  WikiProject v t e 2 3 4 5 6 7 7A 8 9 10 11 11B 12 15 16 17 17A 17B 19 20 21 23 24 26 27 28 33 34 35 37 40 41 48 49 58 58A 60 61 62 63 64 65 66 67 69 71 72 77 85 89 93 94 101 102 105 108 112 115 118 124 125 127 129 130 132 137 138 140 141 144 148 400 401 402 403 404 405 406 407 409 410 412 416 417 418 420 427 QEW 2A 2B 2S 3B 4A 5A 7B 8A 11A 12B 14 15A 18 18A 18B 22 24A 25 29 30 31 32 35A 35B 36 38 39 40B 42 43 44 45 46 47 48B 50 51 52 53 54 55 56 57 59 68 70 73 74 75 76 78 79 80 81 82 83 84 86 87 88 90 91 92 95 96 97 98 99 100 103 104 106 107 109 110 111 114 116 117 119 120 121 122 123 126 128 131 133 134 135 136 169 400A 500 501 502 503 504 505 506 507 508 509 511 512 513 514 515 516 518 519 520 522 522B 523 524 525 526 527 528 528A 529 529A 531 532 533 534 535 536 537 538 539 539A 540 540A 540B 541 541A 542 542A 545 546 547 548 549 550 551 552 553 554 555 556 557 558 559 560 560A 562 563 564 565 566 567 568 569 570 571 572 573 574 575 577 579 580 581 582 583 584 585 586 587 588 589 590 591 592 593 594 595 596 597 599 600 601 602 603 604 605 606 607 607A 608 609 611 612 613 614 615 617 618 619 621 622 623 624 625 626 627 628 630 631 632 633 634 636 637 638 639 640 641 642 643 644 645 646 647 650 651 652 654 655 656 657 658 661 663 664 665 667 668 670 671 672 673 Algoma Cochrane Kenora Manitoulin Nipissing Parry Sound Rainy River Sudbury Thunder Bay Timiskaming 800 801 802 803 804 805 806 807 808 809 810 811 812 813 7016 7036 7037 7041 7042 7044 7048 7051 7057 7059 7082 7087 7094 7102 7104 7116 7125 7146 7162 7174 7180 7181 7182 7186 7187 7189 7195 7197 7202 7236 7237 7241 7250 7260 7270 7271 7272 7273 7274 7275 7276 7901 Ontario provincial highways Trans-Canada Highway Roads in Rainy River District Roads in Kenora District Transport in Fort Frances Transport in Kenora 1937 establishments in Ontario Featured articles Articles using KML from Wikidata Commons category link from Wikidata Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Deutsch Español Français  This page was last edited on 23 September 2019, at 05:11 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Ontario provincial highways King's Highway 71 Highway 71 ^ a b a b ^ a b c d e f ^ ^ ^ a b c a b a b a b ^ ^ 66 ^ 70 a b ^ ^ ^ ^ ^ ^ ^ Route map KML file  Category  Roads portal  Canada portal  WikiProject  List  Category  WikiProject  Article Highway 71 highlighted in red 194.1 km[2] (120.6 mi) September 1, 1937[1]–present  US 53 / US 71 at International Falls, MN  Highway 11 east in Fort Frances35.9 km (22.3 mi) concurrency Highway 11 west near Chapple  Highway 17 near Kenora Fort Frances, Emo, Sioux Narrows, Kenora 
Ontario provincial highways
List
400-series
Former
←  Highway 69Highway 72  →Former provincial highways

←  Highway 70   ←  Highway 69  Highway 72  → Former provincial highways ←  Highway 70     Rainy RiverCanada–US border 0.0  US 53 south / US 71 south – International Falls Continuation into Minnesota Fort Frances–International Falls International Bridge[a] Rainy River Fort Frances 0.062  Highway 11 east (Colonization Road) / TCH – Thunder Bay Eastern end of Highway 11 concurrency 1.4  Highway 602 (Colonization Road) – Emo  Devlin 13.2  Highway 613 – Northwest Bay  Emo 21.1  Highway 602 (Colonization Road) – Fort Frances  Chapple 25.0  Highway 11 west – Rainy River Western end of Highway 11 concurrency Southern end of  Trans-Canada Highway designation. 36.4  Highway 600 west – Black Hawk Highway 615 east – Off Lake Corner  Kenora Sioux Narrows 86.6 Sioux Narrows Bridge Unorganized Kenora District 108.1 Andy Lake Road  120.6  Highway 17 / TCH – Kenora, Dryden  1.000 mi = 1.609 km; 1.000 km = 0.621 mi      Concurrency terminus      Tolled  KML file (edit • help)
 
Highway 1
Highway 5
Highway 16
  
Highway 1
Highway 16
 
Highway 1
Highway 16
 
Highway 1
Highway 16
Highway 100
 
Highway 17
Highway 69
Highway 400
Highway 12
Highway 7
Highway 71
Highway 11
Highway 66
Highway 417
 
Autoroute 40
Autoroute 25
Autoroute 20
Autoroute 85
Route 185
Route 117
Autoroute 15
 
Route 2
Route 16
 
Confederation Bridge
 
Route 1
 
Highway 104
Highway 105
Highway 106
 
Route 1
 
 Category
 Roads portal
 Canada portal
 WikiProject
 The King's Highways of Ontario      Current highways
2
3
4
5
6
7
7A
8
9
10
11
11B
12
15
16
17
17A
17B
19
20
21
23
24
26
27
28
33
34
35
37
40
41
48
49
58
58A
60
61
62
63
64
65
66
67
69
71
72
77
85
89
93
94
101
102
105
108
112
115
118
124
125
127
129
130
132
137
138
140
141
144
148
400-series highways
400
401
402
403
404
405
406
407
409
410
412
416
417
418
420
427
QEW
Former highways
2A
2B
2S
3B
4A
5A
7B
8A
11A
12B
14
15A
18
18A
18B
22
24A
25
29
30
31
32
35A
35B
36
38
39
40B
42
43
44
45
46
47
48B
50
51
52
53
54
55
56
57
59
68
70
73
74
75
76
78
79
80
81
82
83
84
86
87
88
90
91
92
95
96
97
98
99
100
103
104
106
107
109
110
111
114
116
117
119
120
121
122
123
126
128
131
133
134
135
136
169
400A
 
2
3
4
5
6
7
7A
8
9
10
11
11B
12
15
16
17
17A
17B
19
20
21
23
24
26
27
28
33
34
35
37
40
41
48
49
58
58A
60
61
62
63
64
65
66
67
69
71
72
77
85
89
93
94
101
102
105
108
112
115
118
124
125
127
129
130
132
137
138
140
141
144
148
  
400
401
402
403
404
405
406
407
409
410
412
416
417
418
420
427
QEW
 
2A
2B
2S
3B
4A
5A
7B
8A
11A
12B
14
15A
18
18A
18B
22
24A
25
29
30
31
32
35A
35B
36
38
39
40B
42
43
44
45
46
47
48B
50
51
52
53
54
55
56
57
59
68
70
73
74
75
76
78
79
80
81
82
83
84
86
87
88
90
91
92
95
96
97
98
99
100
103
104
106
107
109
110
111
114
116
117
119
120
121
122
123
126
128
131
133
134
135
136
169
400A
 Secondary highways of OntarioSecondary Highways
500
501
502
503
504
505
506
507
508
509
511
512
513
514
515
516
518
519
520
522
522B
523
524
525
526
527
528
528A
529
529A
531
532
533
534
535
536
537
538
539
539A
540
540A
540B
541
541A
542
542A
545
546
547
548
549
550
551
552
553
554
555
556
557
558
559
560
560A
562
563
564
565
566
567
568
569
570
571
572
573
574
575
577
579
580
581
582
583
584
585
586
587
588
589
590
591
592
593
594
595
596
597
599
600
601
602
603
604
605
606
607
607A
608
609
611
612
613
614
615
617
618
619
621
622
623
624
625
626
627
628
630
631
632
633
634
636
637
638
639
640
641
642
643
644
645
646
647
650
651
652
654
655
656
657
658
661
663
664
665
667
668
670
671
672
673
By district
Algoma
Cochrane
Kenora
Manitoulin
Nipissing
Parry Sound
Rainy River
Sudbury
Thunder Bay
Timiskaming
 
500
501
502
503
504
505
506
507
508
509
511
512
513
514
515
516
518
519
520
522
522B
523
524
525
526
527
528
528A
529
529A
531
532
533
534
535
536
537
538
539
539A
540
540A
540B
541
541A
542
542A
545
546
547
548
549
550
551
552
553
554
555
556
557
558
559
560
560A
562
563
564
565
566
567
568
569
570
571
572
573
574
575
577
579
580
581
582
583
584
585
586
587
588
589
590
591
592
593
594
595
596
597
599
600
601
602
603
604
605
606
607
607A
608
609
611
612
613
614
615
617
618
619
621
622
623
624
625
626
627
628
630
631
632
633
634
636
637
638
639
640
641
642
643
644
645
646
647
650
651
652
654
655
656
657
658
661
663
664
665
667
668
670
671
672
673
  
Algoma
Cochrane
Kenora
Manitoulin
Nipissing
Parry Sound
Rainy River
Sudbury
Thunder Bay
Timiskaming
 Tertiary Highways of Ontario      Tertiary Highways
800
801
802
803
804
805
806
807
808
809
810
811
812
813
 
800
801
802
803
804
805
806
807
808
809
810
811
812
813
  7000-series Highways of Ontario7000-series highways
7016
7036
7037
7041
7042
7044
7048
7051
7057
7059
7082
7087
7094
7102
7104
7116
7125
7146
7162
7174
7180
7181
7182
7186
7187
7189
7195
7197
7202
7236
7237
7241
7250
7260
7270
7271
7272
7273
7274
7275
7276
7901
 
7016
7036
7037
7041
7042
7044
7048
7051
7057
7059
7082
7087
7094
7102
7104
7116
7125
7146
7162
7174
7180
7181
7182
7186
7187
7189
7195
7197
7202
7236
7237
7241
7250
7260
7270
7271
7272
7273
7274
7275
7276
7901
  List  Category  WikiProject  Article 