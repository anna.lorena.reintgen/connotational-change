
 
 The Sloan–Parker House[a] is a late 18th-century stone residence near Junction, Hampshire County, in the U.S. state of West Virginia. It was added to the National Register of Historic Places on June 5, 1975, becoming Hampshire County's first property to be listed on the register. The Sloan–Parker House has been in the Parker family since 1854. The house and its adjacent farm are located along the Northwestern Turnpike (US 50/WV 28) in the rural Mill Creek valley.
 The original fieldstone section of the house was erected in about 1790 for Richard Sloan and his wife Charlotte Van Horn Sloan. Originally from Ireland, Sloan arrived in the United States after the American Revolutionary War and became an indentured servant of David Van Horn. Sloan eloped with Van Horn's daughter Charlotte and they settled in the Mill Creek valley, where they built the original "Stone House" portion of the Sloan–Parker House. The Sloans had ten children, including John and Thomas Sloan, who each (later) represented Hampshire County in the Virginia House of Delegates. Richard Sloan and his family operated a successful weaving business from the Stone House and their Sloan counterpanes (quilts with block-designs) became well known in the South Branch Valley region.
 The Sloan family sold the Stone House and 900 acres (360 ha) to three brothers in the Parker family in 1854. The Parker family operated a stagecoach line on the Moorefield and North Branch Turnpike; the journey included a stop at the Stone House, where the family served meals to travelers. During the American Civil War, the Stone House was visited by both Union and Confederate forces, and was ransacked by Union troops for goods and supplies. The Stone House served as a local polling station, and its use as a stagecoach stop ended after the completion of the Hampshire Southern Railroad in 1910. The Parker family opened the house for tours, and it was listed on the National Register of Historic Places in 1975.
 The Sloan–Parker House consists of the original stone section, which faces toward the Northwestern Turnpike, and a wooden frame addition (built c. 1900) adjacent to the original stone section. The stone section's exterior wall is about 36 inches (91 cm) thick at the basement level and tapers to a thickness of about 12 inches (30 cm) at the attic level. Most of the stone section's flooring, and the hardware on the doors, are original. Other features of the Sloan–Parker House property include a large barn (built in 1803) and, to the northeast of the house, the Sloan–Ludwick Cemetery.
 The Sloan–Parker House and farm are situated within a rural, agricultural section of the Mill Creek valley in southwestern Hampshire County. Patterson Creek Mountain, a forested anticlinal mountain ridge, rises to the west of the Mill Creek valley, and the forested western rolling foothills of the anticlinal Mill Creek Mountain rise to the valley's east. The South Branch Potomac River is located across Mill Creek Mountain, approximately 4.4 miles (7.1 km) to the east of the house.[4][5]
 The house and its adjacent farm are located along the Northwestern Turnpike (concurrently US 50 and WV 28) about 0.75 miles (1.21 km) east of the unincorporated community of Junction (also known as Moorefield Junction).[2][4][6] Junction is centered around and named for the intersection of Purgitsville Pike (concurrently US 220 and WV 28) with the Northwestern Turnpike (concurrently US 50 and US 220 westward to Knobly Mountain and concurrently US 50 and WV 28 eastward to Romney).[4][5]
 The Sloan–Parker House is located atop a hillside just south of the Northwestern Turnpike. Mill Creek flows west to east approximately 575 feet (175 m) to the north of the house. An unnamed ephemeral stream that runs just west of the house joins Mill Creek to the north. Mill Creek continues east through the Mechanicsburg Gap in Mill Creek Mountain at Mechanicsburg and joins the South Branch Potomac River.[4][7]
 The Mill Creek valley and the land on which the Sloan–Parker House is located were originally part of the Northern Neck Proprietary, a land grant that the exiled Charles II awarded to seven of his supporters in 1649 during the English Interregnum.[8][9][10] Following the Restoration in 1660, Charles II finally ascended to the English throne.[11] He renewed the Northern Neck Proprietary grant in 1662, revised it in 1669, and again renewed the original grant favoring original grantees Thomas Colepeper, 2nd Baron Colepeper, and Henry Bennet, 1st Earl of Arlington, in 1672.[12] In 1681 Bennet sold his share to Lord Colepeper, who received a new charter for the entire land grant from James II in 1688.[8][13][14] Following the deaths of Lord Colepeper, his wife Margaret, and his daughter Katherine, the Northern Neck Proprietary passed to Katherine's son Thomas Fairfax, 6th Lord Fairfax of Cameron, in 1719.[8][15][16]
 As conflicts with Native Americans began to ease, Lord Fairfax sought to entice European settlers to inhabit the sparsely populated western lands of his Northern Neck Proprietary.[17] The Mill Creek valley was one of the first areas of present-day Hampshire County to be settled by Europeans, beginning in the mid-18th century.[18] Early settlers were drawn by the valley's fertility.[19] Job Parker, a direct ancestor of the Parker family that later acquired the Sloan–Parker House, settled in the Mill Creek valley during this period.[20]
 The original fieldstone section of the Sloan–Parker House was erected around 1790 for Richard Sloan and his wife Charlotte Van Horn Sloan.[20][21] Richard Sloan was originally from County Monaghan, Ulster, Ireland.[3] Following the American Revolutionary War, Sloan sailed on a passenger ship from Great Britain to the United States.[3][21] Despite paying his passage fare prior to his voyage, he was charged again upon his arrival.[3] Unable to make this second payment, he became an indentured servant of David Van Horn.[3][21] After several months with the Van Horn family, Sloan eloped with David Van Horn's daughter Charlotte, and the couple went to Baltimore,[21][22] where they stayed briefly. After observing healthy cattle from Old Fields in Hardy County, they decided to relocate there.[22] On their journey to Old Fields, Sloan and his wife arrived in Romney and chose to settle in Hampshire County instead.[20][21] They later moved 7 miles (11 km) southwest of Romney to their property in the Mill Creek valley, where the Sloan–Parker House stands today.[20]
 To construct the two-story Stone House, Sloan and his wife hired laborers to quarry the native fieldstone and lay the stonework.[20] Richard and Charlotte Sloan had ten children together (four girls and six boys), and the large house afforded the couple sufficient room to raise them.[20][21] The house also provided the family with the necessary space to engage in the manufacture of woven goods.[21] Sloan was a fabric weaver by trade, and he and his family developed a thriving weaving industry on their lands in the Mill Creek valley.[20][21] Their entire production process was initially accomplished inside the Stone House until Sloan erected a separate loom house.[21] There, he and his sons built and operated six looms.[20][21] On their farm, Sloan and his family raised their own sheep and grew flax; they also sourced flax and raw wool from local residents.[21] The majority of the Sloan family's woven products were sold locally, and their Sloan counterpanes became well known in the South Branch Valley region.[20][21] The family's trade in woolen goods and linens thrived until the middle of the 19th century.[20][21] The present Parker family occupants of the Sloan–Parker House retain several woven coverlets produced by the Sloans.[21]
 According to Sloan family tradition, Richard and Charlotte Sloan used a method of casting lots to decide which of their ten children should marry. Their youngest son James won a contest in drawing straws, and was the only one of their ten children to marry when he wed Magdaline Arnold on January 6, 1834.[20][21] James and Magdaline operated the Sloan household while the other members attended to their family's weaving business.[21]
 Two of Richard and Charlotte Sloan's sons, John and Thomas, were active in the military and in local and state politics.[21] John Sloan served in the War of 1812 and was later appointed Major General of the 3rd Division, Virginia Militia, by the Virginia General Assembly in 1842.[23] He also served as a county justice from 1824 until 1828,[21][24] a county surveyor in 1827,[25] a county sheriff in 1839,[21][26] and a member of the Virginia House of Delegates representing Hampshire County from 1825 to 1827.[27] Thomas Sloan represented Hampshire County in the Virginia House of Delegates from 1832 to 1834 and again from 1835 to 1837.[27] He also served as a delegate to the Virginia Constitutional Convention of 1850, representing a district comprising Frederick, Hampshire, and Morgan counties.[27][28][29]
 The Sloan family sold the Stone House and 900 acres (360 ha) of surrounding land to brothers John Peyton Parker, Benjamin Parker, and William Parker on December 29, 1854, for $4,000.[20][21] Following its acquisition by the Parker brothers, the Stone House continued as the center of a large farm.[21]
 Because of its convenient location along the Northwestern Turnpike, the house served as a regional stagecoach stop and local polling place.[30] From the early 19th century until the completion of the Hampshire Southern Railroad to Petersburg in 1910, this stretch of the Northwestern Turnpike between Romney and Junction was part of a major north–south transportation route connecting Cumberland and Romney with Moorefield and Petersburg to the south.[31] By 1845 the Moorefield and North Branch Turnpike stage line connected the Baltimore and Ohio Railroad (B&O) Main Line at Green Spring with the north–south route in Springfield, allowing stagecoach transportation between Green Spring and Moorefield.[32][33] The route of present-day WV 28 aligns with this north–south route from the Maryland state line at Wiley Ford to Petersburg, and is concurrent with the Northwestern Turnpike (US 50) from Romney to Junction and US 220 from Junction to Petersburg.[4][34]
 The Parker family operated their own stagecoach line along the Moorefield and North Branch Turnpike between the B&O Main Line at Green Spring and Moorefield until 1884, when the B&O South Branch Line was completed to Romney Depot.[35][36][37] The Parkers' stagecoach line transported passengers down present-day Green Spring Road (CR 1), Cumberland Road (WV 28), and the Northwestern Turnpike (US 50/WV 28) to the Stone House, where the stagecoaches would stop to allow passengers to have meals before continuing the journey to Moorefield via present-day US 220/WV 28.[20][36] When a stagecoach arrived at the Stone House, a loud horn would sound to announce its arrival, and then meals were provided to the travelers in the basement kitchen and dining areas (or "keeping rooms").[31][35][36] Travelers' horses and other stock were watered and fed outside the basement door. As well as their stagecoach line, the Parker family operated a toll house on the Northwestern Turnpike on the west side of its crossing of the South Branch Potomac River, approximately 4.3 miles (6.9 km) northeast of the Stone House.[31]
 During the American Civil War, the Stone House was visited by both Union and Confederate forces due to its location along the Northwestern Turnpike.[35] The small room under the Stone House's front porch was also used as a picket station to guard the turnpike.[38] Union Army troops ransacked and frequently visited the Stone House to retrieve goods and supplies from the house and farm as they traveled along the Northwestern Turnpike.[20][30] Despite its convenient location and the Parker family's Confederate sympathies, the Stone House and its farm did not suffer major damage during the war.[30] The Federal government later reimbursed the Parker family for the items appropriated by the Union forces during the war. The Parkers were provided with bills annotating the "purchase" of $162.45 in goods and parts during the years 1862, 1863, and 1864. These bills exist today and are in the possession of the Parker family.[31] Extant records also indicate that the Parker family sold farm goods to Confederate forces as well.[38]
 One of the Parker brothers, Benjamin Parker, married Isabel "Belle" Parker and they had two sons together: Renick Seymour Parker, who died at the age of 27 years, and John Henry Parker, Sr., who married Eleanor DuBois Johnson. She had the wooden frame section added to the Stone House around 1900, at which time the Sloan–Parker House took its current form. After Eleanor Parker died, her husband John Henry, Sr., married Kate Parker.[20]
 The Sloan–Parker House occasionally served as a polling place for the Junction community after 1900, and it ceased being used as a stagecoach stop following the completion of the Hampshire Southern Railroad in 1910.[20] The house and farm were formerly serviced by post offices operated in nearby Junction from 1866 to 1868, 1874 to 1876 (as Moorefield Junction), and 1877 until 1997 when service was suspended. Junction's post office was finally shuttered in 2005; afterward, the house was assigned a Romney address.[39]
 By 1962 John Henry Parker, Jr., the son of John Henry, Sr. and Kate Parker, was residing at the Sloan–Parker House with his wife Ruth Harmison Parker and their son David Renick Parker.[40] While under their ownership and through their efforts, the Sloan–Parker House and an adjacent 1.5 acres (0.61 ha)[41] were added to the National Register of Historic Places on June 5, 1975.[1][20][42] It became Hampshire County's first property to be listed on the register.[1]
 Ruth Harmison Parker was a local historian and member of the Hampshire County Historical Society.[43][44] She and her husband opened the Sloan–Parker House for tours in 1962,[40] and again in July 1976, when they displayed furniture, glassware, coverlets, and Civil War relics handed down from the Sloan and Parker families.[42][45] John Henry Parker, Jr., died in 2000,[46] and his wife Ruth died in 2005.[43][44] David Parker is a seventh generation male-line descendant of early Mill Creek settler Jeb Parker.[20] He and his wife Jill have three daughters and one son.[46][47][48]
 The Sloan–Parker House, as of 2018, consists of the original stone section (built c. 1790), which faces toward the Northwestern Turnpike, and a wooden frame addition (built c. 1900) positioned adjacent to the original stone section. Together, the stone section and the frame addition form a T-shaped structure. The original stone section of the house, built of fieldstone, has remained mostly unchanged since its initial construction; slight alterations were made to accommodate the frame addition. Similarly, the frame addition has undergone only minor modifications since its construction.[49]
 The Sloan–Parker House derives its other name as the "Stone House" from this original section of the house. The stone section was built using locally available materials, and its size was probably considered large for its time.[21] It is rectangular in its plan and measures approximately 45 feet (14 m) in width and 40 feet (12 m) in depth. The stone section is symmetrical in its overall interior and exterior design with minor exceptions in its basement. It consists of two stories, one basement level, and an attic.[49] The stone section is topped with a steep metal gabled roof with standing seam profiles. The stone section's exterior wall is about 36 inches (91 cm) thick at the basement level and tapers to a thickness of about 12 inches (30 cm) at the attic level.[20][36][49]
 The north elevation (main façade) has three bays on its first and second stories and one window in the western room of its basement level. Three six-over-nine double-hung wooden sash windows are on the second story, two nine-over-one double-hung wooden sash windows are on the first story, and one small window with two sliding panels of four panes each is on the basement level. At the center of the north elevation, a tall stone platform reaches from the ground to the first story and supports a large covered porch framed with wooden handrails and balustrades on its north and west sides. The porch's roof is supported by five square wooden columns: three along the north edge and two engaged columns against the house's stone exterior. The porch is topped with a metal roof with standing seam profiles. The front door, located between the first story's two large windows, features vertical boards on its exterior side and horizontal boards on its interior side. The front door is topped by a four-pane transom window.[49]
 The east and west elevations of the stone section of the house are similar in design, but the west elevation has a door and large porch at the basement level. It also has a small one-over-one double-hung wooden sash window on the first story that was added after the wooden frame section was constructed. The stone section's east elevation features a small porch and a door, which were built onto the north end of the first floor around 1915. The porch is topped with a metal roof with standing seam profiles, and is supported by three wooden Tuscan columns and two square wooden engaged columns against the house. Both the east and west elevations of the stone section have an interior fieldstone chimney. At the top of each chimney, the original fieldstone has been replaced with brick. Each chimney is centered between two small two-over-four double-hung wooden sash windows at the attic level.[49]
 The south elevation (rear façade) of the stone section remains intact, and serves as the northern wall of the wooden frame section. A window in the stone section's second story was removed to connect the second story of the wooden frame addition. Likewise, what used to be the stone section's rear door now connects the first floor of the stone section with the wooden frame section. This door is also topped by a four-pane transom window.[49]
 The interior of the stone section contains two floors, each with two rooms, in addition to a basement and attic. The former kitchen and dining room (or "keeping rooms") are on the west side of the basement level, and feature a large fireplace. The east side of the basement serves as a large storage area.[49] The two rooms on the first floor formerly served as bedrooms, but they are now used as formal living and sitting rooms.[36][49] The first floor originally had one fireplace on the east side, but the design was changed around 1915 to allow for a fireplace on the west side as well. The second story consists of two bedrooms, each with a fireplace. The attic is completely open from west to east.[49] The rafters are exposed, and secured together with wooden pins.[20][49] They still contain markings of letters and numbers that enabled the proper placement of components as they were lifted from the ground during construction. The majority of the stone section's flooring, and the hardware on the doors, are original.[49]
 The wooden frame section of the Sloan–Parker House features various sizes and types of doors and windows. A deep porch adjoins the east and south elevations of the frame section, and reaches to a canted three-bay window on the frame section's west side. The entire frame section is covered with wooden shingles and is topped with a steep metal roof with standing seam profiles. The interior of the frame section consists of a dining room, kitchen, bathroom, and several bedrooms.[49]
 Ancillary structures on the Sloan–Parker House farm consist of an old log smokehouse, and a large barn erected in 1803. Widely spaced unhewn logs are located on the barn's interior and its south elevation. The remainder of the barn is enclosed with wooden framing. There are also several other barns and storage buildings on the property.[49]
 The Sloan–Ludwick Cemetery[50] is approximately 415 feet (126 m) northeast of the Sloan–Parker House in a grove of trees along the edge of an agricultural field.[4] Richard Sloan and his wife Charlotte Van Horn Sloan are interred in this cemetery.[3][36] Other burials in the cemetery include Richard Sloan's son Major General John Sloan and American Revolutionary War soldier Leonard Ludwick and his wife Katherine.[36][50] Benjamin Parker and some of his descendants were buried in this cemetery before they were reinterred at Indian Mound Cemetery in Romney.[50]
 32049 Northwestern Turnpike (US 50/WV 28) Junction, West Virginia U.S. c. 1790 (stone section) c. 1900 (frame addition) 1 Geography and setting 2 History

2.1 Background
2.2 Sloan family ownership
2.3 Parker family ownership

 2.1 Background 2.2 Sloan family ownership 2.3 Parker family ownership 3 Architecture

3.1 Stone section
3.2 Wooden frame section
3.3 Ancillary structures

 3.1 Stone section 3.2 Wooden frame section 3.3 Ancillary structures 4 Cemetery 5 See also 6 Explanatory notes 7 References 8 Bibliography 9 External links American Civil War portal Architecture portal Geography portal National Register of Historic Places portal List of historic sites in Hampshire County, West Virginia National Register of Historic Places listings in Hampshire County, West Virginia ^ The Sloan–Parker House has been alternatively known as the Stone House, the Parker Family Residence, and the Richard Sloan House.[2][3]
 ^ a b c d "National Register Information System – Sloan–Parker House (#75001892)". National Register of Historic Places. National Park Service. November 2, 2013. Retrieved February 9, 2018..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} 
 ^ a b Harding 1975, p. 1 of the PDF file.
 ^ a b c d e f Brannon 1976, p. 333.
 ^ a b c d e f United States Geological Survey (1973). Romney Quadrangle–West Virginia (Topographic map). 1:24,000. 7.5 Minute Series. Reston, Virginia: United States Geological Survey. OCLC 36344599.
 ^ a b Map centered on the Sloan–Parker House (Map). Google Maps. 2018. Retrieved November 4, 2018.
 ^ McMaster 2010, p. 41.
 ^ Morrison 1971, p. 108.
 ^ a b c Munske & Kerns 2004, p. 9.
 ^ Coleman 1951, p. 246.
 ^ Rose 1976, p. 25.
 ^ William and Mary Quarterly 1898, p. 222.
 ^ William and Mary Quarterly 1898, pp. 222–3.
 ^ Brannon 1976, p. 286.
 ^ William and Mary Quarterly 1898, p. 224.
 ^ William and Mary Quarterly 1898, pp. 224–6.
 ^ Rice 2015, p. 23.
 ^ Pitts & Harding 1976, p. 4 of the PDF file.
 ^ Munske & Kerns 2004, p. 75.
 ^ Scaffidi 2012, p. 7 of the PDF file.
 ^ a b c d e f g h i j k l m n o p q r s t Brannon 1976, p. 334.
 ^ a b c d e f g h i j k l m n o p q r s t u v Harding 1975, p. 3 of the PDF file.
 ^ a b Brannon 1976, pp. 333–4.
 ^ "Brigadier-General John Sloan of Hampshire" (PDF). Richmond Enquirer. Richmond, Virginia. March 15, 1842. p. 2. Archived (PDF) from the original on March 2, 2018. Retrieved March 2, 2018 – via Chronicling America.
 ^ Maxwell & Swisher 1897, p. 276.
 ^ Maxwell & Swisher 1897, p. 278.
 ^ Maxwell & Swisher 1897, p. 279.
 ^ a b c Munske & Kerns 2004, p. 47.
 ^ Pulliam 1901, p. 100.
 ^ Maxwell & Swisher 1897, p. 101.
 ^ a b c Harding 1975, pp. 3–4 of the PDF file.
 ^ a b c d Harding 1975, p. 4 of the PDF file.
 ^ Brannon 1976, p. 432.
 ^ United States Geological Survey (1979). Springfield Quadrangle–West Virginia (Topographic map). 1:24,000. 7.5 Minute Series. Reston, Virginia: United States Geological Survey. OCLC 494927421.
 ^ United States Geological Survey (1970). Old Fields Quadrangle–West Virginia (Topographic map). 1:24,000. 7.5 Minute Series. Reston, Virginia: United States Geological Survey. OCLC 35790339.
 ^ a b c Brannon 1976, pp. 334–5.
 ^ a b c d e f g Munske & Kerns 2004, p. 154.
 ^ Brannon 1976, p. 19.
 ^ a b Brannon 1976, p. 335.
 ^ McMaster 2010, p. 40.
 ^ a b "House Tour Opens Today In Romney". The Cumberland News. Cumberland, Maryland. October 6, 1962. p. 8. Retrieved October 21, 2018 – via NewspaperArchive.com. (subscription required)
 ^ Harding 1975, p. 5 of the PDF file.
 ^ a b "Old Stone House Grew Up With Hampshire County". Cumberland Evening Times. Cumberland, Maryland. October 6, 1975. p. 5. Retrieved October 21, 2018 – via NewspaperArchive.com. (subscription required)
 ^ a b "Ruth H. Parker". The Winchester Star. Winchester, Virginia. October 4, 2005. p. 3. Retrieved October 21, 2018 – via NewspaperArchive.com. (subscription required)
 ^ a b "Ruth H. Parker". Cumberland Times-News. Cumberland, Maryland. October 4, 2005. p. 3B. Retrieved October 21, 2018 – via NewspaperArchive.com. (subscription required)
 ^ "186-Year-Old House Open For Viewing". Cumberland Evening Times. Cumberland, Maryland. July 7, 1976. p. 19. Archived from the original on August 31, 2018. Retrieved November 29, 2017 – via Newspapers.com.
 ^ a b "John H. Parker Jr". Cumberland Times-News. Cumberland, Maryland. March 18, 2000. p. 6. Retrieved October 21, 2018 – via NewspaperArchive.com. (subscription required)
 ^ "New Arrivals". Winchester Evening Star. Winchester, Virginia. February 16, 1976. p. 7. Retrieved October 21, 2018 – via NewspaperArchive.com. (subscription required)
 ^ "New Arrivals". The Winchester Star. Winchester, Virginia. February 29, 1984. p. 19. Retrieved October 21, 2018 – via NewspaperArchive.com. (subscription required)
 ^ a b c d e f g h i j k l m Harding 1975, p. 2 of the PDF file.
 ^ a b c Hall, Charles C. "Sloan-Ludwick (Parker) Cemetery, Junction, WV". HistoricHampshire.org. Charles C. Hall and HistoricHampshire.org. Archived from the original on November 29, 2017. Retrieved November 29, 2017.
 Brannon, Selden W. (1976). Historic Hampshire: A Symposium of Hampshire County and Its People, Past and Present. Parsons, West Virginia: McClain Printing Company. ISBN 978-0-87012-236-1. OCLC 3121468. Coleman, Roy V. (1951). Liberty and Property. New York City: Scribner. OCLC 1020487 – via Google Books. Harding, James E. (April 15, 1975). National Register of Historic Places Registration Form: The Stone House (PDF). United States Department of the Interior, National Park Service. Archived (PDF) from the original on July 13, 2016. Retrieved November 28, 2017. Maxwell, Hu & Swisher, Howard Llewellyn (1897). History of Hampshire County, West Virginia From Its Earliest Settlement to the Present. Morgantown, West Virginia: A. Brown Boughner, Printer. OCLC 680931891. McMaster, Len (2010). Hampshire County West Virginia Post Offices, Part 2 (PDF). LaPosta: A Journal of American Postal History. Archived from the original (PDF) on July 9, 2016. Retrieved July 9, 2016. Morrison, Charles (1971). Wappatomaka: A Survey of the History and Geography of the South Branch Valley. Parsons, West Virginia: McClain Printing Company. ISBN 978-0-87012-107-4. OCLC 222195. Munske, Roberta R. & Kerns, Wilmer L., eds. (2004). Hampshire County, West Virginia, 1754–2004. Romney, West Virginia: The Hampshire County 250th Anniversary Committee. ISBN 978-0-9715738-2-6. OCLC 55983178. Pitts, Phillip R. & Harding, James E. (October 27, 1976). National Register of Historic Places Registration Form: Wilson-Wodrow-Mytinger House (PDF). United States Department of the Interior, National Park Service. Archived from the original (PDF) on October 16, 2015. Retrieved October 16, 2015. Pulliam, David Loyd (1901). The Constitutional Conventions of Virginia from the Foundation of the Commonwealth to the Present Time. Richmond, Virginia: J. T. West. OCLC 228719349 – via Google Books. Rice, Otis K. (2015). The Allegheny Frontier: West Virginia Beginnings, 1730–1830. Lexington, Kentucky: University Press of Kentucky. ISBN 978-0-8131-6438-0. OCLC 900345296 – via Google Books. Rose, Cornelia Bruère (1976). Arlington County, Virginia: A History. Arlington County, Virginia: Arlington Historical Society. OCLC 2401541 – via Google Books. Scaffidi, Sandra (May 15, 2012). National Register of Historic Places Registration Form: Old Pine Church (PDF). United States Department of the Interior, National Park Service. Archived from the original (PDF) on October 16, 2015. Retrieved October 16, 2015. William and Mary Quarterly (April 1898). "The Northern Neck of Virginia". William and Mary Quarterly. 6 (4): 222–6. ISSN 0043-5597. JSTOR 1915885. OCLC 1607858.  Media related to Sloan-Parker House (Junction, West Virginia) at Wikimedia Commons v t e Romney Expedition Union Army: 1st Maryland Cavalry Confederate States Army: 7th Virginia Cavalry 11th Virginia Cavalry 13th Virginia Infantry 23rd Virginia Cavalry 33rd Virginia Infantry 62nd Virginia Mounted Infantry McNeill's Rangers Stonewall Jackson Angus William McDonald John Hanson McNeill Alexander W. Monroe Isaac Parsons Christian Streit White John Baker White Robert White Capon Chapel Confederate Memorial Fort Mill Ridge Civil War Trenches Frenchburg, West Virginia Frye's Inn Hampshire County Courthouse Hanging Rock, West Virginia Hanging Rocks Hook's Tavern Ice Mountain Indian Mound Cemetery Literary Hall (Romney Literary Society) Little Cacapon, West Virginia Mechanicsburg Gap Moorefield and North Branch Turnpike Romney, West Virginia Romney Classical Institute Romney Presbyterian Church Sloan–Parker House Springfield, West Virginia Sycamore Dale Taggart Hall Wappocomo Washington Bottom Farm Wirgman Building v t e Capon Springs North River Mills Historic District Washington Bottom Farm Brill Octagon House Capon Chapel Fort Van Meter French's Mill Hampshire County Courthouse Hebron Church Hickory Grove Hook's Tavern Kuykendall Polygonal Barn Nathaniel and Isaac Kuykendall House Literary Hall Old District Parsonage Old Pine Church Captain David Pugh House Scanlon Farm Sloan–Parker House Springfield Brick House Sycamore Dale Valley View Yellow Spring Mill Wilson-Wodrow-Mytinger House Fort Mill Ridge Civil War Trenches Capon Lake Whipple Truss Bridge Pin Oak Fountain v t e Contributing property Keeper of the Register Historic district History of the National Register of Historic Places National Park Service Property types Barbour Berkeley Boone Braxton Brooke Cabell Calhoun Clay Doddridge Fayette Gilmer Grant Greenbrier Hampshire Hancock Hardy Harrison Jackson Jefferson Kanawha Lewis Lincoln Logan Marion Marshall Mason McDowell Mercer Mineral Mingo Monongalia Monroe Morgan Nicholas Ohio Pendleton Pleasants Pocahontas Preston Putnam Raleigh Randolph Ritchie Roane Summers Taylor Tucker Tyler Upshur Wayne Webster Wetzel Wirt Wood Wyoming Bridges National Historic Landmarks  Category:National Register of Historic Places in West Virginia  Portal:National Register of Historic Places 1790 establishments in Virginia Cemeteries in West Virginia Farms on the National Register of Historic Places in West Virginia Hampshire County, West Virginia, in the American Civil War Houses completed in 1790 Houses completed in 1900 Houses in Hampshire County, West Virginia Houses on the National Register of Historic Places in West Virginia National Register of Historic Places in Hampshire County, West Virginia Northwestern Turnpike Stagecoach stops Stone houses in West Virginia U.S. Route 50 Vernacular architecture in West Virginia CS1: Julian–Gregorian uncertainty Pages containing links to subscription-only content Featured articles Use mdy dates from November 2018 Use American English from November 2017 All Wikipedia articles written in American English Articles with short description Coordinates on Wikidata Commons category link is on Wikidata Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version  This page was last edited on 18 October 2019, at 19:35 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Sloan–Parker House Sloan–Ludwick Cemetery ^ a b c d a b a b c d e f a b c d e f a b ^ ^ a b c ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b c d e f g h i j k l m n o p q r s t a b c d e f g h i j k l m n o p q r s t u v a b ^ ^ ^ ^ a b c ^ ^ a b c a b c d ^ ^ ^ a b c a b c d e f g ^ a b ^ a b ^ a b a b a b ^ a b ^ ^ a b c d e f g h i j k l m a b c 6 Union Army: Confederate States Army: See also:  Sloan–Parker House U.S. National Register of Historic Places 
 Main façade (north elevation) from U.S. Route 50, July 2016 Sloan–Parker HouseShow map of Eastern Panhandle of West VirginiaSloan–Parker HouseShow map of West VirginiaSloan–Parker HouseShow map of the United States 32049 Northwestern Turnpike(US 50/WV 28)Junction, West Virginia U.S. 39°18′46″N 78°50′56″W﻿ / ﻿39.31278°N 78.84889°W﻿ / 39.31278; -78.84889Coordinates: 39°18′46″N 78°50′56″W﻿ / ﻿39.31278°N 78.84889°W﻿ / 39.31278; -78.84889 1.5 acres (0.61 ha) c. 1790 (stone section)c. 1900 (frame addition) Unknown Vernacular 75001892[1] June 5, 1975[1] 
Romney Expedition
  
Union Army: 1st Maryland Cavalry
Confederate States Army: 7th Virginia Cavalry
11th Virginia Cavalry
13th Virginia Infantry
23rd Virginia Cavalry
33rd Virginia Infantry
62nd Virginia Mounted Infantry
McNeill's Rangers
 
Stonewall Jackson
Angus William McDonald
John Hanson McNeill
Alexander W. Monroe
Isaac Parsons
Christian Streit White
John Baker White
Robert White
 
Capon Chapel
Confederate Memorial
Fort Mill Ridge Civil War Trenches
Frenchburg, West Virginia
Frye's Inn
Hampshire County Courthouse
Hanging Rock, West Virginia
Hanging Rocks
Hook's Tavern
Ice Mountain
Indian Mound Cemetery
Literary Hall (Romney Literary Society)
Little Cacapon, West Virginia
Mechanicsburg Gap
Moorefield and North Branch Turnpike
Romney, West Virginia
Romney Classical Institute
Romney Presbyterian Church
Sloan–Parker House
Springfield, West Virginia
Sycamore Dale
Taggart Hall
Wappocomo
Washington Bottom Farm
Wirgman Building
 
Capon Springs
North River Mills Historic District
Washington Bottom Farm
  
Brill Octagon House
Capon Chapel
Fort Van Meter
French's Mill
Hampshire County Courthouse
Hebron Church
Hickory Grove
Hook's Tavern
Kuykendall Polygonal Barn
Nathaniel and Isaac Kuykendall House
Literary Hall
Old District Parsonage
Old Pine Church
Captain David Pugh House
Scanlon Farm
Sloan–Parker House
Springfield Brick House
Sycamore Dale
Valley View
Yellow Spring Mill
Wilson-Wodrow-Mytinger House
 
Fort Mill Ridge Civil War Trenches
 
Capon Lake Whipple Truss Bridge
Pin Oak Fountain
 See also: National Register of Historic Places listings in Hampshire County, West Virginia and List of National Historic Landmarks in West Virginia 
Contributing property
Keeper of the Register
Historic district
History of the National Register of Historic Places
National Park Service
Property types
  
Barbour
Berkeley
Boone
Braxton
Brooke
Cabell
Calhoun
Clay
Doddridge
Fayette
Gilmer
Grant
Greenbrier
Hampshire
Hancock
Hardy
Harrison
Jackson
Jefferson
Kanawha
Lewis
Lincoln
Logan
Marion
Marshall
Mason
McDowell
Mercer
Mineral
Mingo
Monongalia
Monroe
Morgan
Nicholas
Ohio
Pendleton
Pleasants
Pocahontas
Preston
Putnam
Raleigh
Randolph
Ritchie
Roane
Summers
Taylor
Tucker
Tyler
Upshur
Wayne
Webster
Wetzel
Wirt
Wood
Wyoming
 
Bridges
National Historic Landmarks
 
 Category:National Register of Historic Places in West Virginia
 Portal:National Register of Historic Places
 