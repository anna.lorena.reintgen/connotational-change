
 SMS Scharnhorst ("His Majesty's Ship Scharnhorst")[a] was an armored cruiser of the Imperial German Navy, built at the Blohm & Voss shipyard in Hamburg, Germany. She was the lead ship of her class, which included SMS Gneisenau. Scharnhorst and her sister were enlarged versions of the preceding Roon class; they were equipped with a greater number of main guns and were capable of a higher top speed. The ship was named after the Prussian military reformer General Gerhard von Scharnhorst and commissioned into service on 24 October 1907.
 Scharnhorst served briefly with the High Seas Fleet in Germany in 1908, though most of this time was spent conducting sea trials. She was assigned to the German East Asia Squadron based in Tsingtao, China, in 1909. After arriving, she replaced the cruiser Fürst Bismarck as the squadron flagship, a position she would hold for the rest of her career. Over the next five years, she went on several tours of various Asian ports to show the flag for Germany. She frequently carried the squadron commanders to meet with Asian heads of state and was present in Japan for the coronation of the Taishō Emperor in 1912.
 After the outbreak of World War I in August 1914, Scharnhorst and Gneisenau, accompanied by three light cruisers and several colliers, sailed across the Pacific Ocean, to arriving off the southern coast of South America. On 1 November 1914, Scharnhorst and the rest of the East Asia Squadron encountered and overpowered a British squadron at the Battle of Coronel. The defeat prompted the British Admiralty to dispatch two battlecruisers to hunt down and destroy Spee's squadron, accomplished at the Battle of the Falkland Islands on 8 December 1914.
 The two Scharnhorst-class cruisers were ordered as part of the naval construction program laid out in the Second Naval Law of 1900, which called for a force of fourteen armored cruisers. The ships marked a significant increase in combat power over the predecessors of the Roon class, being more heavily armed and armored. These improvements were made to allow for Scharnhorst and Gneisenau to fight in the line of battle should the need arise, a capability requested by the General Department.[2]
 Scharnhorst was 144.6 meters (474 ft 5 in) long overall and had a beam of 21.6 m (70 ft 10 in) and a draft of 8.37 m (27 ft 6 in). The ship displaced 11,616 metric tons (11,433 long tons) as designed and 12,985 t (12,780 long tons) at deep load. She was powered by three triple-expansion steam engines with eighteen coal-fired water-tube boilers. Her engines were rated at 25,644 indicated horsepower (19,123 kW), for a top speed of 22.5 knots (41.7 km/h; 25.9 mph). Scharnhorst's crew consisted of 52 officers and 788 enlisted men; of these, 14 officers and 62 enlisted men were assigned to the squadron commander's staff and were additional to the standard complement.[3]
 Scharnhorst's primary armament consisted of eight 21 cm (8.2 inch) SK L/40 guns,[b] four in twin gun turrets, one fore and one aft of the main superstructure, the other four  mounted in single-gun wing turrets. Secondary armament included six 15 cm (5.9 inch) SK L/40 guns in casemates and eighteen 8.8 cm (3.5 in) SK L/35 guns mounted in casemates. She was also equipped with four 45 cm (18 in) submerged torpedo tubes. One was mounted in the bow, one on each broadside, and the fourth was placed in the stern.[3][4]
 The ship was protected by an armored belt was 15 cm of Krupp armor, decreased to 8 cm (3.1 in) forward and aft of the central citadel. She had an armored deck that was 3.5 to 6 cm (1.4 to 2.4 in) thick, with the heavier armor protecting the ship's engine and boiler rooms and ammunition magazines. The centerline gun turrets had 18 cm (7.1 in) thick sides, while the wing turrets received 15 cm of armor protection. The casemate secondary battery was protected by a strake of armor that was 13 cm (5.1 in) thick.[3][5]
 Named for Generalleutnant (Lieutenant General) Gerhard von Scharnhorst, a Prussian military reformer during the Napoleonic Wars, Scharnhorst was laid down at the Blohm & Voss shipyard in Hamburg, Germany on 22 March 1905,[6] with the construction number 175.[3] She was launched on 23 March 1906, and Generalfeldmarschall (Field Marshal) Gottlieb Graf von Haeseler gave a speech and christened the ship at her launching ceremony.[7] She was commissioned into the fleet a year and a half later on 24 October 1907. She then began sea trials; while conducting speed tests she exceeded her design speed by one knot, reaching 23.5 knots (43.5 km/h; 27.0 mph).[3][7]
 From 6 to 11 November, her trials were interrupted by a voyage to Vlissingen in the Netherlands and Portsmouth in Britain in company with Kaiser Wilhelm II's yacht Hohenzollern and the light cruiser Königsberg. On 14 January 1908, Scharnhorst ran aground off the Bülk Lighthouse and suffered serious underwater damage. Repairs were made at Blohm & Voss, and lasted until 22 February. She then resumed her trials, which continued until the end of April. On 1 May, she replaced Yorck as the flagship of the reconnaissance forces of the High Seas Fleet, under the command of Konteradmiral (Rear Admiral) August von Heeringen. During the rest of the year, she participated in the normal peacetime routine of training exercises and fleet maneuvers.[7]
 On 11 March 1909, Scharnhorst was assigned to the Ostasiengeschwader (East Asia Squadron); Yorck replaced her as the reconnaissance squadron flagship. After completing preparations for the voyage, Scharnhorst left Kiel on 1 April; aboard was Konteradmiral Friedrich von Ingenohl, who was to take command of the East Asia Squadron upon his arrival.[8] On 29 April, Scharnhorst rendezvoused with Fürst Bismarck, the flagship of the East Asia Squadron, in Colombo. There, Scharnhorst took over the role of squadron flagship. At the time, the squadron also included the light cruisers Leipzig and Arcona and several gunboats and torpedo boats. In July and August, Scharnhorst went on a cruise in the Yellow Sea and in August surveyed ports in the area. She spent December and early January 1910 in Hong Kong for the Christmas and New Year's festivities, in company with Leipzig and the gunboat Luchs.[9]
 In January 1910 Scharnhorst, Leipzig, and Luchs went on a tour of East Asian ports, including Bangkok, Manila, and stops in Sumatra and North Borneo. By 22 March, Scharnhorst and Leipzig had returned to the German port at Tsingtao. In the meantime, Arcona had left the East Asia Squadron in February; her replacement, Nürnberg, arrived on 9 April. Ingenohl, by now promoted to Vizeadmiral (Vice Admiral), departed on 6 June and was replaced by Konteradmiral Erich Gühler. The new squadron commander took Scharnhorst and Nürnberg on a tour of Germany's Pacific colonies, starting on 20 June. Stops included the Mariana Islands, Truk, and Apia in German Samoa. In the last port, the cruisers met the unprotected cruisers Cormoran and Condor, the station ships for the South Seas Station. While there, the new light cruiser Emden arrived on 22 July to further strengthen the East Asia Squadron.[9]
 In 1910, Scharnhorst won the Kaiser's Schießpreis (Shooting Prize) for excellent gunnery in the East Asia Squadron. On 25 November, Scharnhorst and the rest of the squadron went on a trip to Hong Kong and Nanjing; while in Hong Kong, an outbreak of typhus struck. Among those who were infected was Gühler, who succumbed to the disease on 21 January 1911. In the meantime, unrest had broken out in Ponape, which required the presence of Emden and Nürnberg. Scharnhorst instead went on a tour of Southeast Asian ports, including Saigon, Singapore, and Batavia. She then returned to Tsingtao by way of Hong Kong and Amoy, arriving on 1 March. There, Konteradmiral Günther von Krosigk was waiting to take command of the squadron. Two weeks later, the squadron was reinforced by the arrival of Scharnhorst's sister ship Gneisenau on 14 March.[9]
 From 30 March to 12 May, Scharnhorst went on a cruise in Japanese waters with Krosigk aboard. She thereafter steamed to the northern area of the German protectorate in early July; at the time tensions were high in Europe due to the Agadir Crisis. Krosigk attempted to keep the situation calm in East Asia and he took his flagship on a tour of harbors in the Yellow Sea. By 15 September, the cruiser was back in Tsingtao. After arriving in Tsingtao, Scharnhorst went into dock for her annual repair; Krosigk accordingly shifted his flag to Gneisenau temporarily. On 10 October, the Xinhai Revolution against the Qing Dynasty broke out, which caused a great deal of tension amongst the Europeans, who recalled the attacks on foreigners during the Boxer Rebellion of 1900–1901. The rest of the East Asia Squadron was placed on alert to protect German interests and additional troops were sent to protect the German consulate. But the feared attacks on Europeans did not materialize and so the East Asia Squadron was not needed.[10]
 By late November, Scharnhorst was back in service and Krosigk returned to the ship. She cruised to Shanghai by way of Tientsin and Yantai, arriving on 12 December. From 14 to 24 January 1912, Scharnhorst toured the ports of the central China coast and returned to Tsingtao on 9 March, where the rest of the squadron had assembled.[11] On 13 April, the ships went on a month-long cruise to Japanese waters, returning to Tsingtao on 13 May. From 17 July to 4 September, Scharnhorst went on another tour of Japanese ports and during this period she also visited Vladivostok in Russia and several ports in the Yellow Sea.[12]
 On 30 July, the Meiji Emperor died; Scharnhorst escorted Leipzig, which carried Prince Heinrich, Wilhelm II's brother, to the Meiji Emperor's funeral and the coronation ceremony for the Taishō Emperor. The ships remained in Japan from 5 to 26 September. After returning to Tsingtao, Prince Heinrich conducted an inspection of the entire East Asia Squadron. On 4 December, Krosigk handed over command of the squadron to Konteradmiral Maximilian von Spee. On 27 December, Spee took Scharnhorst and Gneisenau on a tour of the southwest Pacific, including stops in Amoy, Singapore, and Batavia. The two cruisers reached Tsingtao on 2 March 1913. From 1 April to 7 May, Scharnhorst took Spee to Japan to meet the Taishō Emperor.[12] Starting on 22 June, Spee began a tour of Germany's Pacific colonies aboard his flagship. The ship stopped in the Marianas, the Admiralty Islands, the Hermit Islands, Rabaul in Neupommern, and Friedrich-Wilhelmshafen in German New Guinea.[13]
 While in Rabaul on 21 July, Spee received word of further unrest in China, which prompted his return to the Wusong roadstead outside Shanghai by 30 July. After the situation calmed, Spee was able to take his ships on a short cruise to Japan, which began on 11 November. Scharnhorst and the rest of the squadron returned to Shanghai on 29 November, before she departed for another trip to Southwest Asia. Spee met with Chulalongkorn, the King of Siam, and also visited Sumatra, North Borneo, and Manila. Scharnhorst returned to Tsingtao on 19 March 1914. In early May, Spee, by now promoted to Vizeadmiral, took Scharnhorst and the torpedo boat SMS S90 on a visit to Port Arthur and then to Tianjin; Spee continued on to Beijing, where he met with Yuan Shikai, the first President of the Republic of China. He came back aboard Scharnhorst on 11 May and the ship returned to Tsingtao. Spee thereafter began preparations for a cruise to German New Guinea; Scharnhorst departed on 20 June, leaving only Emden behind in Tsingtao.[14]
 Gneisenau rendezvoused with Scharnhorst in Nagasaki, Japan, where they received a full supply of coal. They then sailed south, arriving in Truk in early July where they would restock their coal supplies. While en route, they received news of the assassination of Archduke Franz Ferdinand, heir to the throne of Austria-Hungary.[15] On 17 July, the East Asia Squadron arrived in Ponape in the Caroline Islands. Spee now had access to the German radio network and he learned of the Austro-Hungarian declaration of war on Serbia and the Russian mobilization against Austria-Hungary and possibly Germany. On 31 July, word came that the German ultimatum that Russia demobilize its armies was set to expire. Spee ordered his ships be stripped for war.[c] On 2 August, Wilhelm II ordered German mobilization against Russia and its ally, France.[16] When the First World War broke out, the East Asia Squadron consisted of Scharnhorst, Gneisenau, Emden, Nürnberg, and Leipzig.[17] At the time, Nürnberg was returning from the west coast of the United States, where Leipzig had just replaced her, and Emden was still in Tsingtao.[18] On 6 August 1914, Scharnhorst, Gneisenau, the supply ship Titania, and the Japanese collier Fukoku Maru were still in Ponape.[19] Spee had issued orders to recall the light cruisers, which had been dispersed on cruises around the Pacific.[20] Nürnberg joined Spee that day,[19] after which Spee moved his ships to Pagan Island in the Northern Mariana Islands, a German possession in the central Pacific.[18]
 All available colliers, supply ships, and passenger liners were ordered to meet the East Asia Squadron in Pagan[21] and Emden joined the squadron there on 12 August.[18] The auxiliary cruiser Prinz Eitel Friedrich joined Spee's ships there as well.[22] The four cruisers, accompanied by Prinz Eitel Friedrich and several colliers, then departed the central Pacific, bound for Chile. On 13 August, Commodore Karl von Müller, captain of the Emden, persuaded Spee to detach his ship as a commerce raider. On 14 August, the East Asia Squadron departed Pagan for Enewetak Atoll in the Marshall Islands, with Scharnhorst in the lead.[23] The ships again coaled after their arrival on 20 August.[24]
 To keep the German high command informed, on 8 September Spee detached Nürnberg to Honolulu to send word through neutral countries. Nürnberg returned with news of the Allied capture of German Samoa, which had taken place on 29 August. Scharnhorst and Gneisenau sailed to Apia to investigate the situation.[25] Spee had hoped to catch a British or Australian warship by surprise, but upon his arrival on 14 September, he found no warships in the harbor.[26] On 22 September, Scharnhorst and the rest of the East Asia Squadron arrived at the French colony of Papeete. The Germans attacked the colony, and in the ensuing Battle of Papeete, they sank the French gunboat Zélée. The ships came under fire from French shore batteries but were undamaged.[27] Fear of mines in the harbor prevented Spee from seizing the coal in the harbor.[28]
 By 12 October, Scharnhorst and the rest of the squadron had reached Easter Island. There they were joined by Dresden and Leipzig, which had sailed from American waters, on 12 and 14 October, respectively. Leipzig also brought three more colliers with her.[27] After a week in the area, the ships departed for Chile.[29] On the evening of 26 October, Scharnhorst and the rest of the squadron steamed out of Mas a Fuera, Chile and headed eastward, arriving in Valparaíso on 30 October. On 1 November, Spee learned from Prinz Eitel Friedrich that the British light cruiser HMS Glasgow had been anchored in Coronel the previous day, so he turned towards the port to try to catch her alone.[30][31]
 The British had scant resources to oppose the German squadron off the coast of South America. Rear Admiral Christopher Cradock commanded the armored cruisers HMS Good Hope and Monmouth, Glasgow, and the converted armed merchant cruiser Otranto. The flotilla was reinforced by the elderly pre-dreadnought battleship Canopus and the armored cruiser Defence. The latter, however, did not arrive until after the Battle of Coronel.[32] Canopus was left behind by Cradock, who probably felt her slow speed would prevent him from bringing the German ships to battle.[30]
 The East Asia Squadron arrived off Coronel on the afternoon of 1 November; to Spee's surprise, he encountered Good Hope, Monmouth, and Otranto in addition to Glasgow. Canopus was still some 300 miles (480 km) behind, with the British colliers.[33] At 16:17, Glasgow spotted the German ships. Cradock formed a line of battle with Good Hope in the lead, followed by Monmouth, Glasgow, and Otranto in the rear. Spee decided to hold off engaging until the sun had set more, at which point the British ships would be silhouetted by the sun, while his own ships would be obscured against the coast behind them. Cradock realized the uselessness of Otranto in the line of battle and detached her.[34][35]
 By 18:07, the distance between the two squadrons had fallen to 13,500 m (44,300 ft) and Spee ordered his ships to open fire thirty minutes later; each ship engaged their opposite in the British line.[36] Scharnhorst engaged Good Hope and hit her on the third salvo, striking between her forward gun turret and her conning tower and starting a major fire. Once the German gunners found the range, they began firing rapidly, with one salvo of high-explosive shells every fifteen seconds.[37] Oberleutnant zur See (Lieutenant at Sea) Knoop, the spotting officer aboard Scharnhorst, reported that "Continual hits could be observed ... in the midships Good Hope was hit repeatedly, with much fire resulting ... the interior of this part of the ship was on fire, which could be seen through the portholes, shining brightly."[38]
 In the meantime, Glasgow began to shoot at both Scharnhorst and Gneisenau, since she could no longer engage the German light cruisers. One of her 4-inch (100 mm) shells struck Scharnhorst in the forecastle but failed to explode. By 18:50, Monmouth had been badly damaged by Gneisenau and she fell out of line; Gneisenau therefore joined Scharnhorst in battling Good Hope.[39] At the same time, Nürnberg closed to point-blank range of Monmouth and poured shells into her.[40] At 19:23, Good Hope's guns fell silent following two large explosions; the German gunners ceased fire shortly thereafter. Good Hope disappeared into the darkness. Spee ordered his light cruisers to close with his battered opponents and finish them off with torpedoes, while he took Scharnhorst and Gneisenau further south to get out of the way.[41]
 Glasgow was forced to abandon Monmouth after 19:20 when the German light cruisers approached, before fleeing south and meeting with Canopus. A squall prevented the Germans from discovering Monmouth, but she eventually capsized and sank at 20:18.[41][42] More than 1,600 men were killed in the sinking of the two armored cruisers, including Cradock. German losses were negligible. However, the German ships had expended over 40% of their ammunition supply.[34] Scharnhorst was hit twice during the engagement, but both shells failed to explode.[42] The second hit passed through her third funnel and did not explode; she was struck by shell splinters that damaged her wireless antenna array. She suffered no casualties and the only German injuries were two slightly wounded men aboard Gneisenau.[43]
 After the battle, Spee took his ships north to Valparaiso. Since Chile was neutral, only three ships could enter the port at a time; Spee took Scharnhorst, Gneisenau, and Nürnberg in first on the morning of 3 November, leaving Dresden and Leipzig with the colliers at Mas a Fuera. There, Spee's ships could take on coal while he conferred with the Admiralty Staff in Germany to determine the strength of remaining British forces in the region. The ships remained in the port for only 24 hours, in accordance with the neutrality restrictions, and arrived at Mas a Fuera on 6 November, where they took on more coal from captured British and French steamers. On 10 November, Dresden and Leipzig were detached for a stop in Valparaiso, and five days later, Spee took the rest of the squadron south to St. Quentin Bay in the Gulf of Penas. On 18 November, Dresden and Leipzig met Spee while en route and the squadron reached St. Quentin Bay three days later. There, they took on more coal, since the voyage around Cape Horn would be a long one and it was unclear when they would have another opportunity to coal.[44]
 Once word of the defeat reached London, the Royal Navy set to organizing a force to hunt down and destroy the East Asia Squadron. To this end, the powerful battlecruisers Invincible and Inflexible were detached from the Grand Fleet and placed under the command of Vice Admiral Doveton Sturdee.[45] The two ships left Devonport on 10 November and while en route to the Falkland Islands, they were joined by the armored cruisers Carnarvon, Kent, and Cornwall, the light cruisers Bristol and Glasgow, and Otranto. The force of eight ships reached the Falklands by 7 December, where they immediately coaled.[46]
 In the meantime, Spee's ships departed St. Quentin Bay on 26 November and rounded Cape Horn on 2 December. They captured the Canadian barque Drummuir, which had a cargo of 2,500 t (2,500 long tons; 2,800 short tons) of good-quality Cardiff coal. Leipzig took the ship under tow and the following day the ships stopped off Picton Island. The crews transferred the coal from Drummuir to the squadron's colliers. On the morning of 6 December, Spee held a conference with the ship commanders aboard Scharnhorst to determine their next course of action. The Germans had received numerous fragmentary and contradictory reports of British reinforcements in the region; Spee and two other captains favored an attack on the Falklands, while three other commanders argued that it would be better to bypass the islands and attack British shipping off Argentina. Spee's opinion carried the day and the squadron departed for the Falklands at 12:00 on 6 December.[47]
 Gneisenau and Nürnberg were delegated for the attack; they approached the Falklands the following morning, with the intention of destroying the wireless transmitter there. Observers aboard Gneisenau spotted smoke rising from Port Stanley, but assumed it was the British burning their coal stocks to prevent the Germans from seizing them.[48] As they closed on the harbor, 30.5 cm (12.0 in) shells from Canopus, which had been beached as a guard ship, began to fall around the German ships, which prompted Spee to break off the attack.[46] The Germans took a south-easterly course at 22 kn (41 km/h; 25 mph) after having reformed by 10:45. Scharnhorst was the center ship, with Gneisenau and Nürnberg ahead and Dresden and Leipzig astern.[49][50] The fast battlecruisers quickly got up steam and sailed out of the harbor to pursue the slower East Asia Squadron.[46]
 By 13:20, the British ships had caught up with Scharnhorst and the other cruisers and began firing at a range of 14 km (8.7 mi).[51] Spee realized his armored cruisers could not escape the much faster battlecruisers and ordered the three light cruisers to attempt to break away while he turned about and allowed the British battlecruisers to engage the outgunned Scharnhorst and Gneisenau. Meanwhile, Sturdee detached his cruisers to pursue the German light cruisers.[52] Invincible opened fire at Scharnhorst while Inflexible attacked Gneisenau and Spee ordered his two armored cruisers to similarly engage their opposites. Spee had taken the lee position; the wind kept his ships swept of smoke, which improved visibility for his gunners. This forced Sturdee into the windward position and its corresponding worse visibility. Scharnhorst straddled Invincible with her third salvo and quickly scored two hits on the British battlecruiser. The German flagship was herself not hit during this phase of the battle.[53]
 Sturdee attempted to widen the distance by turning two points to the north to prevent Spee from closing to within the range of his numerous secondary guns. Spee counteracted this maneuver by turning rapidly to the south, which forced Sturdee to turn south as well to keep within range. This allowed Scharnhorst and Gneisenau to turn back north and get close enough to engage with their secondary 15 cm guns. Their shooting was so accurate that it forced the British to haul away a second time.[54] After resuming the battle, the British gunfire became more accurate;[55] Scharnhorst was hit several times and fires broke out. The pace of her gunfire started to slacken, though she continued to score hits on Invincible. Sturdee then turned to port in an attempt to take the leeward position, but Spee countered the turn to retain his favorable position; the maneuvering did, however, reverse the order of the ships, so Scharnhorst now engaged Inflexible.[56]
 By this stage of the battle, Scharnhorst had a slight list to port and was about a meter lower in the water. Her third funnel had been shot away. Gneisenau was briefly obscured by smoke, which led both battlecruisers to target Scharnhorst. By 16:00, Spee ordered Gneisenau to attempt to escape while he reversed course and attempted to launch torpedoes at his pursuers. The port list had increased significantly by this point and she was well down by the bow, with only 2 meters (6 ft 7 in) of freeboard. At 16:17, the ship finally capsized to port and sank; the British, their attention now focused on Gneisenau, made no attempt to rescue the crew.[57] All 860 officers and men on board, including Spee, went down with the ship.[3] Gneisenau, Leipzig, and Nürnberg were also sunk. Only Dresden managed to escape, but she was eventually tracked to the Juan Fernandez Islands and sunk. The complete destruction of the squadron killed about 2,200 German sailors and officers, including two of Spee's sons.[51]
 
In mid-1915, a coastal steamer found the body of a German sailor off the coast of Brazil. The sailor had a watertight cartridge case from a 21 cm shell attached; inside was one of the Reichskriegsflaggen (Imperial war flags) flown aboard Scharnhorst. The sailor was buried in Brazil and the flag was eventually returned to Germany, where it was placed on display at the Museum für Meereskunde (Marine Science) in Berlin, though it was lost during World War II.[58] In the mid-1930s, the new German navy, the Kriegsmarine, built a battleship named for Scharnhorst. At the launching of the new Scharnhorst in October 1936, the widow of the earlier ship's captain was present.[59] Coordinates: 54°40′S 55°51′W﻿ / ﻿54.667°S 55.850°W﻿ / -54.667; -55.850
 
 18 water-tube boilers 25,644 ihp (19,123 kW) 3 shafts 3 × triple-expansion steam engines 52 officers 788 enlisted 8 × 21 cm (8.3 in) guns 6 × 15 cm (5.9 in) guns 18 × 8.8 cm (3.5 in) SK L/35 guns 4 × 45 cm (17.7 in) torpedo tubes Belt: 15 cm Turrets: 18 cm (7.1 in) Deck: 3.5 to 6 cm (1.4 to 2.4 in) 1 Design 2 Service history

2.1 East Asia Squadron
2.2 World War I

2.2.1 Battle of Coronel
2.2.2 Battle of the Falkland Islands



 2.1 East Asia Squadron 2.2 World War I

2.2.1 Battle of Coronel
2.2.2 Battle of the Falkland Islands

 2.2.1 Battle of Coronel 2.2.2 Battle of the Falkland Islands 3 Notes

3.1 Footnotes
3.2 Citations

 3.1 Footnotes 3.2 Citations 4 References ^  "SMS" stands for "Seiner Majestät Schiff" (German: His Majesty's Ship).
 ^  In Imperial German Navy gun nomenclature, "SK" (Schnelladekanone) denotes that the gun is quick firing, while the L/40 denotes the length of the gun. In this case, the L/40 gun is 40 calibers, meaning that the gun barrel is 40 times as long as it is in diameter.
 ^  This meant the removal of all non-essential items, to include dress uniforms, tapestries, furniture, and other flammable objects. See: Hough, p. 17.
 ^ Rüger, p. 160.
 ^ Dodson, pp. 58–59, 67.
 ^ a b c d e f Gröner, p. 52.
 ^ Gardiner & Gray, p. 142.
 ^ Dodson, p. 206.
 ^ Hildebrand, Röhr & Steinmetz, p. 105.
 ^ a b c Hildebrand, Röhr & Steinmetz, p. 106.
 ^ Hildebrand, Röhr & Steinmetz, pp. 106–107.
 ^ a b c Hildebrand, Röhr & Steinmetz, p. 107.
 ^ Hildebrand, Röhr & Steinmetz, p. 108.
 ^ Hildebrand, Röhr & Steinmetz, pp. 108–109.
 ^ a b Hildebrand, Röhr & Steinmetz, p. 109.
 ^ Hildebrand, Röhr & Steinmetz, pp. 109–110.
 ^ Hildebrand, Röhr & Steinmetz, p. 110.
 ^ Hough, pp. 11–12.
 ^ Hough, pp. 17–18.
 ^ Halpern, p. 66.
 ^ a b c Staff, p. 29.
 ^ a b Halpern, p. 71.
 ^ Hough, pp. 1–2.
 ^ Hough, pp. 3–4.
 ^ Hough, p. 5.
 ^ Hough, p. 23.
 ^ Hough, p. 33.
 ^ Strachan, p. 471.
 ^ Staff, pp. 29–30.
 ^ a b Staff, p. 30.
 ^ Halpern, p. 89.
 ^ Hawkins, p. 34.
 ^ a b Halpern, p. 92.
 ^ Staff, pp. 30–31.
 ^ Herwig, p. 156.
 ^ Halpern, pp. 92–93.
 ^ a b Halpern, p. 93.
 ^ Staff, p. 32.
 ^ Staff, p. 33.
 ^ Staff, pp. 33–34.
 ^ Staff, p. 34.
 ^ Staff, p. 35.
 ^ Herwig, p. 157.
 ^ a b Staff, p. 36.
 ^ a b Strachan, p. 36.
 ^ Staff, p. 39.
 ^ Staff, pp. 58–59.
 ^ Strachan, p. 41.
 ^ a b c Strachan, p. 47.
 ^ Staff, pp. 61–62.
 ^ Staff, p. 62.
 ^ Bennett, p. 115.
 ^ Staff, p. 63.
 ^ a b Herwig, p. 158.
 ^ Bennett, p. 117.
 ^ Staff, p. 66.
 ^ Bennett, p. 118.
 ^ Staff, p. 67.
 ^ Staff, p. 68.
 ^ Staff, p. 69.
 ^ Hildebrand, Röhr & Steinmetz, p. 112.
 ^ Williamson, p. 8.
 Bennett, Geoffrey (2005). Naval Battles of the First World War. London: Pen & Sword Military Classics. ISBN 978-1-84415-300-8..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} Dodson, Aidan (2016). The Kaiser's Battlefleet: German Capital Ships 1871–1918. Barnsley: Seaforth Publishing. ISBN 978-1-84832-229-5. Gardiner, Robert; Gray, Randal, eds. (1985). Conway's All the World's Fighting Ships: 1906–1921. Annapolis: Naval Institute Press. ISBN 978-0-87021-907-8. Gröner, Erich (1990). German Warships: 1815–1945. Vol. I: Major Surface Vessels. Annapolis: Naval Institute Press. ISBN 978-0-87021-790-6. Halpern, Paul G. (1995). A Naval History of World War I. Annapolis: Naval Institute Press. ISBN 978-1-55750-352-7. Hawkins, Nigel (2002). Starvation Blockade: The Naval Blockades of WWI. Annapolis: Naval Institute Press. ISBN 978-0-85052-908-1. Herwig, Holger (1998) [1980]. "Luxury" Fleet: The Imperial German Navy 1888–1918. Amherst: Humanity Books. ISBN 978-1-57392-286-9. Hildebrand, Hans H.; Röhr, Albert; Steinmetz, Hans-Otto (1993). Die Deutschen Kriegsschiffe (Band 7) [The German Warships (Volume 7)]. Ratingen: Mundus Verlag. OCLC 310653560. Hough, Richard (1980). Falklands 1914: The Pursuit of Admiral Von Spee. Penzance: Periscope Publishing. ISBN 978-1-904381-12-9. Rüger, Jan (2007). The Great Naval Game: Britain and Germany in the Age of Empire. Cambridge: Cambridge University Press. ISBN 0-521-87576-5. Staff, Gary (2011). Battle on the Seven Seas: German Cruiser Battles, 1914–1918. Barnsley: Pen & Sword Maritime. ISBN 978-1-84884-182-6. Strachan, Hew (2001). The First World War: Volume 1: To Arms. Oxford: Oxford University Press. ISBN 978-0-19-926191-8. Williamson, Gordon (2003). German Battleships 1939–45. Oxford: Osprey Publishing. ISBN 978-1-84176-498-6. v t e Scharnhorst Gneisenau Preceded by: Roon class Followed by: SMS Blücher List of armored cruisers of Germany v t e SMS Scharnhorst SMS Von der Tann SMS Goeben SMS Moltke SMS Seydlitz SMS Derfflinger Admiral Hipper Bismarck Type VII, Type XVII, Type XXI and Type XXVI U-boats Pamir Passat Peking Priwall Dar Pomorza Gorch Fock (1933) USCGC Eagle Gorch Fock (1958) RMS Majestic SS Cap Arcona SS Europa SS France TS Pretoria TS Windhuk Deutsche Ost-Afrika Linie Prinzessin Victoria Luise MV Wilhelm Gustloff MV Aurora (1955) MV Explorer (2001) A Dubai Eclipse Enigma Grille Lady Moura MV Savarona Aradu Almirante Brown class NRP Vasco da Gama Elbe 17 Hamburger Flugzeugbau List of Blohm & Voss aircraft v t e 8 Dec: SMS Gneisenau, SMS Leipzig, SMS Nürnberg, SMS Scharnhorst  9 Dec: SM U-11 13 Dec: Mesudiye 18 Dec: SM U-5 20 Dec: Curie, Montrose 27 Dec: HMS Success Scharnhorst-class cruisers Ships built in Hamburg 1906 ships World War I cruisers of Germany World War I shipwrecks in the Atlantic Ocean Shipwrecks of the Falkland Islands Maritime incidents in 1914 Ships lost with all hands Articles containing German-language text Use dmy dates from August 2014 CS1: long volume value Coordinates on Wikidata Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version العربية Български Català Čeština Deutsch Eesti Español فارسی Français Italiano Magyar 日本語 Norsk Polski Português Русский Svenska Türkçe Українська Tiếng Việt 中文  This page was last edited on 9 September 2019, at 12:27 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  SMS Scharnhorst ^ ^ ^ ^ ^ a b c d e f ^ ^ ^ a b c ^ a b c ^ ^ a b ^ ^ ^ ^ ^ a b c a b ^ ^ ^ ^ ^ ^ ^ a b ^ ^ a b ^ ^ ^ a b ^ ^ ^ ^ ^ ^ a b a b ^ ^ ^ a b c ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ Scharnhorst steaming at high speed, c. 1907–1908
 Name: 
Scharnhorst Namesake: 
Gerhard von Scharnhorst[1] Laid down: 
22 March 1905 Launched: 
23 March 1906 Commissioned: 
24 October 1907 Fate: 
Sunk in action, Battle of the Falkland Islands, 8 December 1914 Class and type: 
Scharnhorst-class armored cruiser Displacement: 
12,985 t (12,780 long tons) Length: 
144.6 m (474 ft 5 in) Beam: 
21.6 m (70 ft 10 in) Draft: 
8.37 m (27 ft 6 in) Installed power: 

18 water-tube boilers
25,644 ihp (19,123 kW)
 Propulsion: 

3 shafts
3 × triple-expansion steam engines
 Speed: 
22.5 knots (41.7 km/h; 25.9 mph) Crew: 

52 officers
788 enlisted
 Armament: 

8 × 21 cm (8.3 in) guns
6 × 15 cm (5.9 in) guns
18 × 8.8 cm (3.5 in) SK L/35 guns
4 × 45 cm (17.7 in) torpedo tubes
 Armor: 

Belt: 15 cm
Turrets: 18 cm (7.1 in)
Deck: 3.5 to 6 cm (1.4 to 2.4 in)
 
Scharnhorst
Gneisenau
 
Preceded by: Roon class
Followed by: SMS Blücher
 
List of armored cruisers of Germany
 
SMS Scharnhorst
SMS Von der Tann
SMS Goeben
SMS Moltke
SMS Seydlitz
SMS Derfflinger
 
Admiral Hipper
Bismarck
Type VII, Type XVII, Type XXI and Type XXVI U-boats
 
Pamir
Passat
Peking
Priwall
Dar Pomorza
Gorch Fock (1933)
USCGC Eagle
Gorch Fock (1958)
 
RMS Majestic
SS Cap Arcona
SS Europa
SS France
TS Pretoria
TS Windhuk
Deutsche Ost-Afrika Linie
Prinzessin Victoria Luise
MV Wilhelm Gustloff
MV Aurora (1955)
MV Explorer (2001)
 
A
Dubai
Eclipse
Enigma
Grille
Lady Moura
MV Savarona
 
Aradu
Almirante Brown class
NRP Vasco da Gama
 
Elbe 17
Hamburger Flugzeugbau
List of Blohm & Voss aircraft
 
8 Dec: SMS Gneisenau, SMS Leipzig, SMS Nürnberg, SMS Scharnhorst 
9 Dec: SM U-11
13 Dec: Mesudiye
18 Dec: SM U-5
20 Dec: Curie, Montrose
27 Dec: HMS Success
 1913  1914  1915 November 1914   January 1915 