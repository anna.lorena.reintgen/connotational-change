The 2005 Azores subtropical storm was the 19th nameable storm and only subtropical storm of the record-breaking 2005 Atlantic hurricane season. It was not officially named by the US National Hurricane Center as it was operationally classified as a non-tropical low. The storm developed in the eastern Atlantic Ocean out of a low-pressure area that gained subtropical characteristics on October 4. The storm was short-lived, crossing over the Azores later on October 4 before becoming extratropical again on October 5. Neither damages nor fatalities were reported during that time. After being absorbed into a cold front, the system went on to become Hurricane Vince, which affected the Iberian Peninsula.
 Months after the hurricane season, when the National Hurricane Center was performing its annual review of the season and its named storms, forecasters Jack Beven and Eric Blake identified this previously unnoticed subtropical storm. Despite its unusual location and wide wind field, the system had a well-defined centre convecting around a warm core—the hallmark of a subtropical storm.
 The system originated out of an upper-level low just west of the Canary Islands on September 28. The low organized itself over the next days, producing several bursts of convection. While remaining non-tropical with a cold core it moved gradually west to northwest. On October 3, it became a broad surface low about 400 nautical miles (460 mi, 740 km) southwest of São Miguel Island in the Azores.[1] Early on October 4, convection increased as the surface low organized itself, and the system became a subtropical depression.[2]
 Around the same time, the depression turned northeast into a warm sector ahead of an oncoming cold front, and strengthened into a subtropical storm. The system continued to track northeast and strengthened slightly, reaching its peak intensity of 50 mph (85 km/h) as it approached the Azores that evening. After tracking through the Azores, the storm weakened slightly as it moved to the north-northeast. Through an interaction with the cold front early on October 5 the subtropical storm became extratropical. The system was fully absorbed by the front later that day.[2] The newly absorbed system would separate from the dissolving frontal system and become Subtropical Storm Vince on October 8.[3]
 At the time, the system was not believed to have been subtropical. However, there were several post-season findings that confirmed that the system was indeed a subtropical storm. The first was the cloud pattern, in which it had deep convection around the center and was better organized with a well-defined center of circulation. In addition, the system had a warm core more typical of tropical cyclones as opposed to the cold core of extratropical cyclones. The warm-core nature also meant that there were no warm or cold fronts attached to the system, as temperatures did not change ahead of and behind the system[4] until the unrelated cold front passed the Azores.[5] Satellite imagery suggested that the system was briefly a tropical storm as the warm core was found; however, the widespread wind field and the presence of an upper-level trough confirmed that it was merely subtropical.[2]
 Tropical storm-force winds were reported across parts of the Azores, primarily on the eastern islands. The strongest winds were reported on Santa Maria Island, where 10-minute sustained winds reached 49 mph (79 km/h) with gusts to 59 mph (94 km/h).[6] Ponta Delgada faced 38 mph (61 km/h) winds, with the peak recorded gust being 52 mph (85 km/h).[1] No damage or fatalities were reported.[2]
 The storm was not classified as a subtropical storm until April 10, 2006, after a reassessment by the National Hurricane Center. Every year, the NHC re-analyzes the systems of the past hurricane season and revises the storm history frequently if there is new data that was operationally unavailable.[2] If the storm had been operationally recognized, it would have been named Subtropical Storm Tammy, and storms forming after October 4 would have been moved one name down the list. Hurricane Wilma would have been given the name Alpha: a name that, had it been retired like Wilma was, could not be replaced by an "alternate" Greek letter, as is the convention with names on the standard A–W list.[7][8] When the system strengthened into a subtropical storm on October 4, it was the earliest the 19th tropical or subtropical storm of the season formed.[9] The old record was held by an unnamed storm in the 1933 Atlantic hurricane season, which formed on October 25, 1933.[9] It was also only the fourth time that 19 storms formed in a season.[10]
 
 1 Meteorological history 2 Impact, classification, and records 3 See also 4 References 5 External links Tropical cyclones portal 2005 Atlantic hurricane season List of Atlantic hurricanes Timeline of the 2005 Atlantic hurricane season ^ a b Beven II, John L.; Lixion A. Avila; Eric S. Blake; Daniel P. Brown; James L. Franklin; Richard D. Knabb; Richard J. Pasch; Famie R. Rhome & Stacy R. Stewart (March 2008). "Annual Summary: Atlantic Hurricane Season of 2005" (PDF). Monthly Weather Review. 136 (3): 1131–1141. Bibcode:2008MWRv..136.1109B. doi:10.1175/2007MWR2074.1. Archived from the original (PDF) on 10 September 2008. Retrieved 2008-09-08..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c d e National Hurricane Center (2006-04-10). "Tropical Cyclone Report: Unnamed Subtropical Storm" (PDF). National Oceanic and Atmospheric Administration. Retrieved 2006-10-31.
 ^ National Hurricane Center (2006-02-22). "Tropical Cyclone Report: Hurricane Vince" (PDF). National Oceanic and Atmospheric Administration. Archived (PDF) from the original on 24 May 2006. Retrieved 2006-05-04.
 ^ Computer Generated (2005-10-08). "History for Santa Maria, Azores: Week of October 2, 2005 through October 8, 2005". Weather Underground. Retrieved 2008-08-08.
 ^ Computer Generated (2005-10-06). "History for Santa Maria, Azores: Thursday, October 6, 2005". Weather Underground. Retrieved 2008-08-08.
 ^ Computer Generated (2005-10-04). "History for Santa Maria, Azores: Tuesday, October 4, 2005". Weather Underground. Retrieved 2008-08-07.
 ^ Bob King (2006-04-11). "Wilma should've been Alpha". Palm Beach Post. Archived from the original on 7 September 2008. Retrieved 2008-08-11.
 ^ Neal Dorst (2007-05-08). "What happens if they run out of names on the list?". Atlantic Oceanographic and Meteorological Laboratory. Archived from the original on 15 September 2008. Retrieved 2008-08-17.
 ^ a b NHC Hurricane Research Division (2008-01-01). "Atlantic hurricane best track ("HURDAT")". NOAA. Archived from the original on 16 September 2008. Retrieved 2008-08-10.
 ^ National Hurricane Center (2008-06-23). "NHC/TPC Archive of Hurricane Seasons". NOAA. Archived from the original on 8 August 2008. Retrieved 2008-08-09.
 NHC's Tropical Cyclone Report on the storm v t e  Book  Category  Portal 2005 Atlantic hurricane season Hurricanes in the Azores Subtropical storms Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Español Français Português Simple English Türkçe 中文  This page was last edited on 1 October 2019, at 14:12 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  2005 Atlantic hurricane season 2005 Azores subtropical storm a b 136 a b c d e ^ ^ ^ ^ ^ ^ a b ^  Book  Category  Portal Subtropical storm (SSHWS/NWS) The storm at peak intensity near the Azores on October 4 October 4, 2005 October 5, 2005   
 1-minute sustained: 50 mph (85 km/h)  997 mbar (hPa); 29.44 inHg   
 None reported Azores  Part of the 2005 Atlantic hurricane season 
 Timeline  

TSArlene
TSBret
1Cindy
4Dennis(history)
5Emily
TSFranklin
TSGert
TSHarvey

2Irene
TDTen
TSJose
5Katrina(history)
TSLee
3Maria
1Nate
1Ophelia

1Philippe
5Rita
TDNineteen
1Stan
SSUnnamed
TSTammy
TDTwenty-two
1Vince

5Wilma(history)
TSAlpha
3Beta
TSGamma
TSDelta
1Epsilon
TSZeta

 TS Arlene TS Bret 1 Cindy 4 Dennis(history) 5 Emily TS Franklin TS Gert TS Harvey 2 Irene TD Ten TS Jose 5 Katrina(history) TS Lee 3 Maria 1 Nate 1 Ophelia 1 Philippe 5 Rita TD Nineteen 1 Stan SS Unnamed TS Tammy TD Twenty-two 1 Vince 5 Wilma(history) TS Alpha 3 Beta TS Gamma TS Delta 1 Epsilon TS Zeta 
 Book
 Category
 Portal
 