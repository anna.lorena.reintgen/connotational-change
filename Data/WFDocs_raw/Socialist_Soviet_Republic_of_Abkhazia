
 The Socialist Soviet Republic of Abkhazia (SSR Abkhazia)[a] was a short-lived republic within the Caucasus region of the Soviet Union that covered the territory of Abkhazia,[b] and existed from 31 March 1921 to 19 February 1931. Formed in the aftermath of the Red Army invasion of Georgia in 1921, it was independent until 16 December 1921, when it agreed to a treaty uniting it with the Georgian Soviet Socialist Republic (Georgian SSR). The SSR Abkhazia was largely similar to an autonomous Soviet republic, though it retained de facto independence from Georgia, being given certain features only full union republics had, like its own military units. Through its status as a "treaty republic" with Georgia, Abkhazia joined the Transcaucasian Soviet Federative Socialist Republic, which united Armenian, Azerbaijani, and Georgian SSRs into one federal unit, when the latter was formed in 1922. The SSR Abkhazia was abolished in 1931 and replaced with the Abkhaz Autonomous Soviet Socialist Republic within the Georgian SSR.
 Throughout its existence, the SSR Abkhazia was led by Nestor Lakoba, who served officially as the Chairman of the Council of People's Commissars but controlled the republic to such an extent it was jokingly referred to as "Lakobistan". Due to Lakoba's close relationship with Soviet leader Joseph Stalin, collectivisation was delayed until after Abkhazia was incorporated into Georgia. Abkhazia remained a major tobacco producer in this era, growing over half of the USSR's supply. It also produced other agricultural produce, including tea, wine, and citrus fruits, leading to Abkhazia being one of the wealthiest regions in the Soviet Union. Its sub-tropical climate also made it a prime holiday destination; Stalin and other Soviet leaders had dachas (holiday homes) in the region and spent considerable time there.
 An ethnically diverse region, Abkhazia was nominally led by the Abkhaz people, who made up less than 30 percent of the population. Other major groups included Georgians, Armenians, Greeks, and Russians. Even though they did not form the majority, the Abkhaz were heavily favoured and the Abkhaz language was promoted as a result of the korenizatsiia policies of the era. An Abkhaz national identity was promoted through these policies, leading to the rise of Abkhaz nationalism. The main legacy of the SSR Abkhazia is that for the first time in modern history, it created a defined geographic entity under the name Abkhazia. Though the quasi-independent republic was downgraded in 1931, the Abkhaz people did not forget that it had existed. With the advent of glasnost and perestroika in the late 1980s, Abkhaz leaders called for their state to be re-formed and secede from Georgia, citing the SSR Abkhazia as a precedent. This led to them restoring the 1925 SSR Abkhazian constitution, which led to the 1992–1993 war between Abkhazian secessionists and Georgia, and the modern Abkhaz–Georgian conflict.
 The Russian Empire annexed Abkhazia in the early nineteenth century and had consolidated its authority over the region by 1864.[1] Reluctant to create ethno-territorial units, the Russian authorities incorporated the region into the Kutais Governorate.[2] Large-scale population transfers saw the ethnic composition of Abkhazia radically altered, with thousands of ethnic Abkhaz expelled and ethnic Mingrelians brought in to replace them.[3][4] After the 1917 February Revolution, which ended the Russian Empire, the status of Abkhazia became contested and was unclear.[5] Free from Russian rule, it considered joining the Mountainous Republic of the Northern Caucasus in 1917, but ultimately decided against this due to the distance between Abkhazia and the rest of the groups involved.[6] In February 1918, Abkhaz Bolsheviks attempted to create a commune—a similar system to the soviets (councils) being formed in Russia. This was unsuccessful and the Bolshevik leaders, Efrem Eshba and Nestor Lakoba, fled.[7] The Abkhaz People's Council (APC) was formed in the aftermath and effectively controlled the region. When the Democratic Republic of Georgia was formed in May 1918, it annexed Abkhazia, considering it an integral part of its territory. Georgia never fully established control of the region, leaving the APC to rule it until the Bolshevik invasion of 1921.[8]
 The status of Abkhazia was confirmed in the Georgian constitution of 1921. Article 107 guaranteed "Abkhazeti[c] (district of Soukhoum)" autonomy for "the administration of their affairs".[9] The constitution was proclaimed after the Red Army invasion of Georgia in February 1921; the nature of the promised autonomy was never determined.[10] According to the historian Timothy Blauvelt, this had a lasting legacy in the region because it marked the first time in modern history Abkhazia was defined as a distinct geographic entity.[11]
 On 15 February 1921, the Red Army invaded Georgia. Abkhazia was invaded two days later.[12] Eshba and Lakoba returned to Abkhazia before the invasion and formed a Revolutionary Committee (Revkom) in preparation for a Bolshevik government.[13] Sukhumi, the capital, was captured on 4 March. With fighting in Georgia continuing, the Revkom, who did not expect to be the sole authority over Abkhazia, took advantage of the confusion and moved to declare Abkhazia an independent republic.[14] They sent a telegram to Moscow asking for advice on how to proceed, and suggested joining the Russian Soviet Federative Socialist Republic, but Sergo Ordzhonikidze—a leading Bolshevik and the leader of the Caucasus Bureau (Kavbiuro)—dismissed the idea.[15] As a result, on 31 March 1921, it declared that "at the will of workers a new Socialist Soviet Republic of Abkhazia is born."[16] This made Abkhazia a nominally independent republic with the understanding on both the Abkhaz and Georgian sides that eventually Abkhazia would join the newly-formed Georgian Soviet Socialist Republic (Georgian SSR).[16] Until then it was regarded as being completely detached from Georgia and was treated as such. The Georgian Revkom, the governing body of the Georgian SSR, welcomed Abkhazia in a telegram on 21 May 1921, and said the form of relations should be settled during the first Workers' Congresses of both republics.[17]
 The Abkhaz Revkom, in a position of power, was reluctant to schedule a congress to determine the future status of Abkhazia because it would mean relinquishing control over the region. The Kavbiuro forced the Revkom to act and negotiations for a treaty between Abkhazia and Georgia began in October 1921.[18] The result, signed on 16 December 1921, was a two-article treaty:
 1. SSR Georgia and SSR Abkhazia enter into political, military and financial-economic union.2. In order to fulfill the aforementioned goal both governments declare the merging of the following Commissariats: a) military, b) finance, c) peoples' agriculture, d) post and telegraph, e) ChKa, f) RKI, g) People's Commissariat of Justice, and h) [Commissariat of] Sea Transport. The treaty united the two states, leaving Abkhazia as a "treaty republic" nominally subservient to Georgia.[20] The special status of Abkhazia within Georgia was reinforced in the 1922 Georgian constitution, which mentioned the "special union treaty" between the two. The 1925 Abkhazian constitution noted it was united with Georgia "on the base of a special treaty".[21] On 13 December 1922, while united with Georgia, Abkhazia joined the Transcaucasian Socialist Federative Soviet Republic (TSFSR), along with Armenia and Azerbaijan. This new federation was created ostensibly for economic purposes, but was more likely done to consolidate Soviet control over the region, which had been contentious.[22] Abkhazia was mostly treated as an autonomous region of Georgia, though unlike other autonomous states in the Soviet Union, it had its own national symbols—a flag and coat of arms—and national army units, a right only given to full republics.[23] The coat of arms was initially described in the 1925 constitution as being "composed of a golden hammer and sickle on the background of the Abkhazian landscape with inscription in the Abkhaz language 'SSR Abkhazia'".[24] This was slightly modified in 1926, when the republican (and Soviet-wide) motto "Proletarians of all countries, unite!" was written in Abkhaz, Georgian, and Russian (previously it had only been written in Abkhaz).[24] It also had its own constitution, created on 1 April 1925, another right only granted to full republics.[25]
 The union with Georgia was not popular among the Abkhaz populace or leadership.[11] It was also received poorly in Georgia, where it was regarded as a ploy by the Bolsheviks to divert Georgian hostility from the authorities in Moscow towards the Abkhaz, as the Georgians were one of the most hostile groups towards the Bolsheviks.[26] As the only "treaty republic" in the USSR, the exact status of the SSR Abkhazia concerned the Soviet and Georgian authorities, which did not want other regions to demand a similar status.[27] To resolve this it was decided to downgrade Abkhazia, and on 19 February 1931 it was re-formed as the Abkhaz Autonomous Soviet Socialist Republic, subservient to the Georgian SSR while remaining a member of the TSFSR.[28] The move was met with public protests, the first large-scale protests in Abkhazia against the Soviet authorities.[29]
 Initially the Abkhaz Revkom, led by its chairman Efrem Eshba, controlled Abkhazia until a more permanent body could be established.[17] On 17 February 1922 the Council of People's Commissars was established, and Nestor Lakoba was elected its Chairman, becoming the de facto head of the republic; this was a formality for Lakoba, who had effectively been in control of Abkhazia since the Bolsheviks took control in 1921.[30][31] Alongside Eshba, he had been a leading Bolshevik in the aftermath of the Russian Revolution. Lakoba and Eshba led two abortive attempts to seize Abkhazia in February and April 1918. After the latter attempt failed, they both fled, only returning in March 1921 after Bolshevik control had been consolidated; Eshba was soon transferred to other positions, leaving Lakoba alone as the head of Abkhazia.[32]
 Lakoba effectively controlled Abkhazia as a personal fiefdom, which was jokingly referred to as "Lakobistan", and his status as head of the republic was never contested or challenged.[33] He resisted many of the repressive policies that were being implemented elsewhere in the Soviet Union, including collectivisation. Lakoba also financially supported the Abkhaz nobility, which he was able to do because of his close personal relationship with Soviet leader Joseph Stalin.[34]
 Abkhazia was a major producer of tobacco during the Soviet era. In the 1930s, it was responsible for up to 52 percent of the Soviet Union's tobacco exports.[35] Other agricultural products, including tea, wine, and citrus fruits—especially tangerines—were produced in large quantities, making Abkhazia one of the most well-off regions in the entire Soviet Union, and considerably richer than Georgia.[36] The export of these resources turned the region into "an island of prosperity in a war-ravaged Caucasus".[37] Several factories were also built in the region as part of the overall development of the Soviet Union, though they had less impact on the overall economic strength of Abkhazia.[38]
 Abkhazia was also prized as a major holiday destination for both the Soviet elite and the general population. Stalin visited annually throughout the 1920s and was joined by his associates from the Kremlin, who used this time to gain his trust.[39] As host, Lakoba grew increasingly close to Stalin and became a confidant of his, allowing him to keep his dominant position over Abkhazia.[40] This was most apparent when Lakoba refused to implement collectivisation, arguing that there were no kulaks (affluent peasants) in the state.[41] Such a policy was defended by Stalin, who said the anti-kulak policy did not "take account of the specific peculiarities of Abkhaz social structure and made the mistake of mechanically transferring Russian models of social engineering to Abkhaz soil".[42] Collectivisation was first carried out after Abkhazia was downgraded in 1931, and fully implemented in 1936 after Lakoba's death.[43]
 Throughout the SSR's existence, the Soviet ruble was its official currency.[44]
 The SSR Abkhazia was an ethnically diverse region, whose demographics changed considerably in the decades after its annexation by Russia. Up to 100,000 Abkhaz had been deported in the late nineteenth century, mainly to the Ottoman Empire.[45] By the time the SSR Abkhazia was formed, ethnic Abkhaz comprised less than 30 percent of the population. The korenizatsiia (nativization) policy implemented in this era, which was to promote minority groups within the USSR, saw the numbers of Abkhaz increase: between 1922 and 1926, ethnic Abkhaz grew by roughly 8%, while the number of ethnic Georgians dropped by 6%. Thus, according to the 1926 Soviet census, the only census conducted during the SSR's existence, the number of ethnic Abkhaz reached 55,918 or around 27.8% of the total population (which numbered 201,016), while the number of Georgians was around 67,494 (36%). Other major ethnic groups counted in the 1926 census were Armenians (25,677, or 12.7 percent), Greeks (14,045, or 7 percent), and Russians (12,553, or 6.2 percent).[46][47]
 The script used for the Abkhaz language was modified during the era of the SSR Abkhazia. Under korenizatsiia the Abkhaz were not considered one of the "advanced" peoples in the USSR, and thus saw an increased focus on their national language and cultural development.[48] As part of these policies, Abkhaz—along with many other regional languages in the USSR—was Latinized in 1928, moving it away from the original Cyrillic-based script.[49] Emphasis was placed on developing Abkhaz culture, which was heavily promoted and financed.[38] To further this, an Abkhazian Scientific Society was created in 1922, while an Academy of Abkhazian Language and Literature was founded in 1925.[50]
 In recognition of the multiple ethnic groups within Abkhazia, Article 8 of the 1925 Abkhaz constitution called for three official languages—Abkhaz, Georgian, and Russian—while a later amendment stated, "all nationalities populating the SSR Abkhazia are guaranteed the right of free development and use of the native language both in national-cultural and in general state agencies".[51][52] Most of the population did not understand Abkhaz so Russian was the dominant language of government while local regions used the language that was most prevalent there.[53]
 The exact status of Abkhazia as a "treaty republic" was never clarified during its existence, and historian Arsène Saparov has suggested even officials at the time did not know what the phrase meant.[54] The status had symbolic meaning to the Abkhaz people, who never forgot they had, at least in theory, an independent state.[55] With the advent of glasnost and perestroika in the 1980s, calls for Abkhazia to restore its status began. An assembly at Lykhny in 1989 called for the Soviet authorities to make Abkhazia a full union republic, claiming the SSR Abkhazia as a precedent for this move.[56] When Abkhazia declared independence in 1990, it requested the restoration of the 1925 constitution, which called for Abkhazia and Georgia to unite, allowing for the possibility of a future union between the two states.[57] The restoration of the 1925 constitution was a pretext for the 1992–1993 war and the ensuing dispute over the status of Abkhazia, which has led to Abkhazia being de facto independent of Georgia since 1992.[43]
 Coordinates: 43°00′N 41°01′E﻿ / ﻿43.000°N 41.017°E﻿ / 43.000; 41.017
 1 History

1.1 Background
1.2 Formation
1.3 Status

 1.1 Background 1.2 Formation 1.3 Status 2 Politics 3 Economy 4 Demographics 5 Legacy 6 References

6.1 Notes
6.2 Citations
6.3 Bibliography

 6.1 Notes 6.2 Citations 6.3 Bibliography ^ Abkhazian: Социалисттә Советтә Республика Аҧсны, ССР Аҧсны; Sociālicṭṭw Soveṭṭw Resṗubliḳā Āpsnə Georgian: საბჭოთა სოციალისტური რესპუბლიკა აფხაზეთი, სსრ აფხაზეთი; sabch'ota sotsialist'uri resp'ublika apkhazeti; Russian: Социалистическая Советская Республика Абхазия, ССР Абхазия; Sotsialisticheskaya Sovetskaya Respublika Abkhaziya
 ^ Abkhazia is the subject of a territorial dispute between the Republic of Abkhazia and Georgia. The Republic of Abkhazia unilaterally declared independence on 23 July 1992, but Georgia continues to claim it as part of its own sovereign territory. Abkhazia has received formal recognition as an independent state from 7 out of 193 United Nations member states, 1 of which have subsequently withdrawn their recognition.
 ^ The Georgian language version of Abkhazia is used in English translations of the 1921 Constitution.
 ^ Lak'oba 1998a, pp. 89–101
 ^ kartuli sabch'ota entsiklopedia 1985, p. 504
 ^ Lak'oba 1998a, p. 84
 ^ Müller 1998, pp. 220–225
 ^ Blauvelt 2007, p. 206
 ^ Saparov 2015, p. 43
 ^ Blauvelt 2012b, p. 81
 ^ Lakoba 1990, p. 63
 ^ Papuashvili 2012, p. 48
 ^ Welt 2012, pp. 214–215
 ^ a b Blauvelt 2014, p. 26
 ^ Suny 1994, p. 207
 ^ Saparov 2015, p. 48
 ^ Hewitt 2013, p. 39
 ^ Saparov 2015, p. 49
 ^ a b Saparov 2015, p. 50
 ^ a b Saparov 2015, p. 51
 ^ Saparov 2015, p. 52
 ^ Saparov 2015, p. 54
 ^ Saparov 2015, p. 55
 ^ Saparov 2015, pp. 55, 57
 ^ Hewitt 1993, p. 271
 ^ Saparov 2015, pp. 50–56
 ^ a b Saparov 2015, p. 56
 ^ Hewitt 2013, p. 40
 ^ Smith 2013, p. 344
 ^ Saparov 2015, p. 60
 ^ Blauvelt 2007, p. 212
 ^ Lakoba 1995, p. 99
 ^ Bgazhba 1965, p. 39
 ^ Blauvelt 2007, p. 207
 ^ Blauvelt 2014, pp. 24–25
 ^ Lakoba 2004, pp. 100–101
 ^ Lak'oba 1998b, p. 71
 ^ Suny 1994, p. 268
 ^ Zürcher 2007, pp. 120–121
 ^ Rayfield 2004, p. 95
 ^ a b Anchabadze & Argun 2012, p. 90
 ^ Blauvelt 2007, p. 202
 ^ Scott 2016, p. 96
 ^ Marshall 2010, p. 239
 ^ Rayfield 2004, p. 30
 ^ a b Derluguian 1998, p. 266
 ^ Blauvelt 2007, p. 211
 ^ Hewitt 2013, p. 25
 ^ Müller 1998, p. 231
 ^ Jones 1988, pp. 617–618
 ^ Martin 2001, pp. 23–24
 ^ Jones 1988, p. 617
 ^ Blauvelt 2012a, p. 252
 ^ Blauvelt 2012a, pp. 241–242
 ^ Saparov 2015, pp. 58–59
 ^ Blauvelt 2012a, pp. 240–246
 ^ Saparov 2015, pp. 61–62
 ^ Saparov 2015, p. 62
 ^ Anchabadze 1998, p. 132
 ^ Cornell 1998, p. 52
 Anchabadze, Jurij (1998), "History: the modern period",  in Hewitt, George (ed.), The Abkhazians: A Handbook, New York City: St. Martin's Press, pp. 132–146, ISBN 978-0-31-221975-8.mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} Anchabadze, Yu. D.; Argun, Yu. G. (2012), Абхазы (The Abkhazians) (in Russian), Moscow: Nauka, ISBN 978-5-02-035538-5 Bgazhba, Mikhail (1965), Нестор Лакоба (Nestor Lakoba) (in Russian), Tbilisi: Sabtchota Saqartvelo Blauvelt, Timothy (May 2007), "Abkhazia: Patronage and Power in the Stalin Era", Nationalities Papers, 35 (2): 203–232, doi:10.1080/00905990701254318 Blauvelt, Timothy (2012a), "'From words to action!': Nationality policy in Soviet Abkhazia (1921–38)",  in Jones, Stephen F. (ed.), The Making of Modern Georgia, 1918 – 2012: The first Georgian Republic and its successors, New York City: Routledge, pp. 232–262, ISBN 978-0-41-559238-3 Blauvelt, Timothy K. (2012b), "Resistance and Accommodation in the Stalinist Periphery: A Peasant Uprising in Abkhazia", Ab Imperio, 3: 78–108, doi:10.1353/imp.2012.0091 Blauvelt, Timothy K. (2014), "The Establishment of Soviet Power in Abkhazia: Ethnicity, Contestation and Clientalism in the Revolutionary Periphery", Revolutionary Russia, 27 (1): 22–46, doi:10.1080/09546545.2014.904472 Cornell, Svante E. (Autumn 1998), "Religion as a Factor in Caucasian Conflicts", Civil Wars, 1 (3): 46–64, doi:10.1080/13698249808402381 Derluguian, Georgi M. (1998), "The Tale of Two Resorts: Abkhazia and Ajaria Before and Since the Soviet Collapse",  in Crawford, Beverley; Lipshutz, Ronnie D. (eds.), The Myth of "Ethnic Conflict": Politics, Economics, and "Cultural" Violence, Berkeley, California: University of California Press, pp. 261–292, ISBN 978-0-87-725198-9 Hewitt, B.G. (1993), "Abkhazia: a problem of identity and ownership", Central Asian Survey, 12 (3): 267–323, doi:10.1080/02634939308400819 Hewitt, George (2013), Discordant Neighbours: A Reassessment of the Georgian-Abkhazian and Georgian-South Ossetian Conflicts, Leiden, The Netherlands: Brill, ISBN 978-9-00-424892-2 Jones, Stephen F. (October 1988), "The Establishment of Soviet Power in Transcaucasia: The Case of Georgia 1921–1928", Soviet Studies, 40 (4): 616–639, doi:10.1080/09668138808411783 kartuli sabch'ota entsiklopedia (1985), "Sukhumi okrug", kartuli sabch'ota entsiklopedia (Georgian Soviet Encyclopedia) (in Georgian), Vol. 9, Tbilisi: Kartuli Sabch'ota Entsiklopedia Lak'oba, Stanislav (1998a), "History: 18th century–1917",  in Hewitt, George (ed.), The Abkhazians: A Handbook, New York City: St. Martin's Press, pp. 89–101, ISBN 978-0-31-221975-8 Lak'oba, Stanislav (1998b), "History: 1917–1989",  in Hewitt, George (ed.), The Abkhazians: A Handbook, New York City: St. Martin's Press, pp. 67–88, ISBN 978-0-31-221975-8 Lakoba, Stanislav (1990), Очерки Политической Истории Абхазии (Essays on the Political History of Abkhazia) (in Russian), Sukhumi, Abkhazia: Alashara Lakoba, Stanislav (1995), "Abkhazia is Abkhazia", Central Asian Survey, 14 (1): 97–105, doi:10.1080/02634939508400893 Lakoba, Stanislav (2004), Абхазия после двух империй. XIX-XXI вв. (Abkhazia after two empires: XIX–XXI centuries) (in Russian), Moscow: Materik, ISBN 5-85646-146-0 Marshall, Alex (2010), The Caucasus Under Soviet Rule, New York City: Routledge, ISBN 978-0-41-541012-0 Martin, Terry (2001), The Affirmative Action Empire: Nations and Nationalism in the Soviet Union, 1923–1939, Ithaca, New York: Cornell University Press, ISBN 978-0-80-143813-4 Müller, Daniel (1998), "Demography: ethno-demographic history, 1886–1989",  in Hewitt, George (ed.), The Abkhazians: A Handbook, New York City: St. Martin's Press, pp. 218–231, ISBN 978-0-31-221975-8 Papuashvili, George, ed. (2012), The 1921 Constitution of the Democratic Republic of Georgia, Batumi, Georgia: Constitutional Court of Georgia, ISBN 978-9941-0-3458-9 Rayfield, Donald (2004), Stalin and His Hangmen: The Tyrant and Those Who Killed for Him, New York City: Random House, ISBN 978-0-37-575771-6 Saparov, Arsène (2015), From Conflict to Autonomy in the Caucasus: The Soviet Union and the making of Abkhazia, South Ossetia and Nagorno Karabakh, New York City: Routledge, ISBN 978-0-41-565802-7 Scott, Erik R. (2016), Familiar Strangers: The Georgian Diaspora and the Evolution of Soviet Empire, Oxford, United Kingdom: Oxford University Press, ISBN 978-0-19-939637-5 Smith, Jeremy (2013), Red Nations: The Nationalities Experience in and after the USSR, Cambridge, United Kingdom: Cambridge University Press, ISBN 978-0-52-112870-4 Suny, Ronald Grigor (1994), The Making of the Georgian Nation (Second ed.), Bloomington, Indiana: Indiana University Press, ISBN 978-0-25-320915-3 Welt, Cory (2012), "A Fateful Moment: Ethnic Autonomy and Revolutionary Violence in the Democratic Republic of Georgia (1918–1921)",  in Jones, Stephen F. (ed.), The Making of Modern Georgia, 1918 – 2012: The first Georgian Republic and its successors, New York City: Routledge, pp. 205–231, ISBN 978-0-41-559238-3 Zürcher, Christoph (2007), The Post-Soviet Wars: Rebellion, Ethnic Conflict, and Nationhood in the Caucasus, New York City: New York University Press, ISBN 978-0-81-479709-9 v t e Armenia Azerbaijan Byelorussia Estonia1 Georgia Kazakhstan Kirghizia Latvia1 Lithuania1 Moldavia Russian SFSR Tajikistan Turkmenia Ukraine Uzbekistan Karelo-Finnish SSR (1940–1956) Transcaucasian SFSR (1922–1936) SSR Abkhazia (1921–1931) Bukharan SSR (1920–1925) Khorezm SSR (1920–1925) Nakhichevan ASSR (1920–1923) Pridnestrovian Moldavian SSR (1990–1991) South Ossetian SR (1990–1991) 1931 disestablishments in the Soviet Union Early Soviet republics Former socialist republics Politics of Abkhazia History of Abkhazia 20th century in Georgia (country) Georgian Soviet Socialist Republic Russian-speaking countries and territories States and territories established in 1921 States and territories disestablished in 1931 Articles containing Abkhazian-language text Articles containing Georgian-language text Articles containing Russian-language text Featured articles Use British English from March 2018 Pages using infobox country or infobox former country with the symbol caption or type parameters CS1 Russian-language sources (ru) CS1 Georgian-language sources (ka) CS1: long volume value Coordinates on Wikidata Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version العربية Azərbaycanca تۆرکجه Čeština Deutsch Español Euskara Français Hrvatski Bahasa Indonesia עברית ქართული Latviešu Lietuvių Nederlands 日本語 Oʻzbekcha/ўзбекча پنجابی Polski Português Русский Српски / srpski Srpskohrvatski / српскохрватски Suomi Svenska Tagalog Türkçe Українська اردو Tiếng Việt 中文  This page was last edited on 24 October 2019, at 15:38 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  1. SSR Georgia and SSR Abkhazia enter into political, military and financial-economic union.2. In order to fulfill the aforementioned goal both governments declare the merging of the following Commissariats: a) military, b) finance, c) peoples' agriculture, d) post and telegraph, e) ChKa, f) RKI, g) People's Commissariat of Justice, and h) [Commissariat of] Sea Transport.— Union Treaty between SSR Georgia and SSR Abkhazia[19]
 Motto:  Anthem:  Preceded by Succeeded by Socialist Soviet Republic of Abkhazia SSR Abkhazia ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ a b a b ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ 35 3 27 1 12 40 14 1921–1931 


Flag



State Emblem

 Motto: Пролетарии всех стран, соединяйтесь!Proletarii vsekh stran, soyedinyaytes'!"Proletarians of all countries, unite!" Anthem: ИнтернационалInternatsional"The Internationale" The Socialist Soviet Republic of Abkhazia in 1921 Sukhumi Abkhaz, Georgian, Russian Socialist Republic Congress of Soviets    31 March 1921 19 February 1931 
 8,600 km2 (3,300 sq mi)  201,016 
 Ruble 


 Preceded by
Succeeded by







Democratic Republic of Georgia






Abkhaz ASSR




  Preceded by Succeeded by
 




Democratic Republic of Georgia


 
 Democratic Republic of Georgia
 


Abkhaz ASSR




 Abkhaz ASSR
 
 
Armenia
Azerbaijan
Byelorussia
Estonia1
Georgia
Kazakhstan
Kirghizia
Latvia1
Lithuania1
Moldavia
Russian SFSR
Tajikistan
Turkmenia
Ukraine
Uzbekistan
  
Karelo-Finnish SSR (1940–1956)
Transcaucasian SFSR (1922–1936)
 
SSR Abkhazia (1921–1931)
Bukharan SSR (1920–1925)
Khorezm SSR (1920–1925)
Nakhichevan ASSR (1920–1923)
Pridnestrovian Moldavian SSR (1990–1991)
South Ossetian SR (1990–1991)
 1The annexation of Estonia, Latvia and Lithuania in 1940 was considered as an illegal occupation and was not recognized by the majority of the international community such as the United States, United Kingdom and the European Community. The Soviet Union officially recognized their independence on September 6, 1991, prior to its final dissolution three months later. 