
 It Is the Law is a 1924 American silent mystery film directed by J. Gordon Edwards and starring Arthur Hohl, Herbert Heyes, and Mona Palma. It is a film adaptation of the 1922 Broadway play of the same name by Elmer Rice, itself based on a novel by Hayden Talbot. The film depicts the story of Ruth Allen (Palma), who marries Justin Victor (Heyes) over competing suitor Albert Woodruff (Hohl). Seeking revenge for this slight, Woodruff fakes his death by killing a drifter who resembles him, and frames Victor for the murder. Woodruff attempts to renew his courtship of Allen by using an assumed identity, but she sees through his disguise. Once Victor is freed from prison, he kills Woodruff, and goes free because a conviction would constitute double jeopardy.
 This was the final film for director Edwards, who died the following year, and was one of the last produced at Fox Film's New York studio. Contemporary reviews were generally positive. Like many of Fox's early works, it was likely lost in the 1937 Fox vault fire.
 Albert Woodruff and Justin Victor are friends who are both in love with the same woman, Ruth Allen. Allen chooses to wed Victor, and Woodruff storms off in a jealous rage. Woodruff locates his look-alike, "Sniffer" Evans, a drifter and drug addict. On the evening following Allen and Victor's wedding, Woodruff calls Allen and threatens to blackmail her. He also convinces Evans to come to his apartment.
 Victor also travels to Woodruff's apartment, intending to confront his former friend about the threatening phone call. When Woodruff sees Victor outside the apartment building, he feigns a cry for help and shoots Evans to death. Victor is blamed for the murder of the man presumed to be Woodruff, and is sentenced to life imprisonment.
 Five years later, Woodruff disguises himself with a beard and monocle and assumes a new identity in an attempt to court Allen. She is able to recognize him as Woodruff because of his fear of fire tongs. Because she is friends with the governor, she is able to arrange her husband's release from prison. Victor locates Woodruff in a casino and kills him. At trial, he declares that the prohibition against double jeopardy prevents his prosecution for the murder; he is set free to live happily with his wife.[1][2][3]
 Arthur Hohl reprised the role of Woodruff from an earlier production of the story as a Broadway play;[6][7] in addition to Woodruff and Evans, he played a third role as the casino's proprietor.[2] The cast also includes Dorothy Kingdon, Helena D'Algy, Patricia O'Connor, and Nancy Newman as casino regulars.[5] This was Herbert Heyes's final silent film; he returned to acting in the 1940s.[8]
 In 1922, theatrical agent Walter Jordan encouraged successful playwright Elmer Rice to dramatize It Is the Law, an unpublished novel written by Hayden Talbot.[9][10] At the time, Rice was best known for his 1914 Broadway play On Trial, which featured the first use of flashback, a narrative technique he adapted from film,[11] in a Broadway production.[10] Rice's stage adaptation of Talbot's work, also titled It Is the Law, again featured a story told in flashback.[10] The play ran for 125 performances at the Ritz Theatre,[6] and was a modest success.[1][10]
 Fox Film announced production of a film adaptation of It Is the Law early in 1924, with J. Gordon Edwards set to direct.[12] This was the only film he directed that year, as he was primarily serving as Fox Film's director-general at the time.[13] Curtis Benton wrote the screenplay for Fox;[4] unlike the theatrical version, Benton's work presents the story entirely in chronological order.[2] Principal casting was completed in February,[14] and filming took place at Fox's New York studio. Most of Fox's film production had by then moved to Hollywood: It Is the Law was one of only four films Fox made at its East Coast facility in 1924.[a][16] Except for four Allan Dwan films in 1926, they were the last Fox produced in New York.[17] The set constructed for the courtroom climax was a duplicate of a room in The Tombs.[18]
 The copyright registration for It Is the Law stated its length as eight reels,[19] but the released version was a shorter, seven-reel film.[4] When Twentieth Century-Fox Film renewed the copyright in 1951, they again referred to the longer run time; the title was also restyled with an exclamation point, as It Is the Law![20] Fox's advertising for the film included a trailer,[7] as well as novelty items to be given away by exhibitors, such as a small key described as the "key to the mystery" of the film.[21]
 This was Edwards's final work. He retired from Fox after the film's completion,[22] and although he expressed an interest to returning to the role, he died of pneumonia the following year.[23]
 It Is the Law received generally positive reviews. George T. Pardy, writing for Exhibitors Trade Review, noted that the audience was aware of the nature of the mystery while the characters were not, a welcome departure from the conventions of most melodramas.[1] Motion Picture Magazine called the film "tense, suspensive ... and new in its idea."[24] The Film Daily's reviewer believed it would appeal to fans of the genre despite "some hokum and implausible twists".[7] Edwards's direction was praised,[1][25] including his pacing of the story,[7] although the reviewer for Variety felt the film was unnecessarily long.[2] Hohl, in his dual role performance, was considered the strongest of the cast,[1][2] despite some exaggerated expressions.[7] Chicago-based Screen Opinions had a more mixed opinion, giving the film a 65% score; despite praising the direction and cast, its reviewer felt the film was "too unhappy to be popular".[26]
 It Is the Law is believed to be lost. The 1937 Fox vault fire destroyed most of Fox's silent films,[27] and the Library of Congress is not aware of any extant copies.[28] Because little of Edwards's work survives, few of his films have drawn attention from modern authors,[29][30] but film historian Larry Langman included It Is the Law as an example of how avenging-spouse films evolved in the 1920s to "emphasize the inner strengths of their women".[31]
 J. Gordon Edwards Arthur Hohl Herbert Heyes Mona Palma August 31, 1924 (1924-08-31) 1 Plot 2 Cast 3 Production 4 Reception and legacy 5 See also 6 Notes 7 References 8 Bibliography 9 External links Arthur Hohl as Albert Woodruff and "Sniffer" Evans Herbert Heyes as Justin Victor Mona Palma as Ruth Allen (credited as Mimi Palmeri) George Lessey as Inspector Dolan Robert Nat Young as Travers Florence Dixon as Lillian Allen Byron Douglas as Cummings Olaf Hytten as Bill Elliott De Sacia Mooers as Bernice Guido Trento as Manee Byron Russell as Harley[4] Bijou Fernandez as Valerie[5] List of lost silent films (1920–24) Double Jeopardy ^ The others were The Fool, directed by Harry Millarde; and The Warrens of Virginia and Crossed Wires (released as Daughters of the Night[15]), both directed by Elmer Clifton.[16][17]
 ^ a b c d e Pardy, George T. (1924-09-27). "Suspense grips in 'It Is the Law'". Exhibitors Trade Review: 30..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c d e Skig [pseud.] (1924-09-24). "It Is the Law". Variety. 76 (6): 26, 76.
 ^ Langman 1998, p. 40.
 ^ a b c "It Is the Law". AFI Catalogue of Feature Films: The First 100 Years 1893–1993. American Film Institute. Retrieved 2018-05-14.
 ^ a b "Casts of current photoplays". Photoplay. 26 (6): 141. 1924.
 ^ a b Hischak 2009, p. 224.
 ^ a b c d e "It Is the Law". The Film Daily. 29 (57): 5. 1924-09-07.
 ^ Liebman 1998, p. 146.
 ^ Palmieri 1980, p. 56.
 ^ a b c d Bordman 1995, pp. 194–195.
 ^ Palmieri 1980, p. 8.
 ^ "Start 'It Is the Law'". Moving Picture World. 66 (5): 387. 1924-02-02.
 ^ Solomon 2011, p. 63.
 ^ "Add to cast of Fox play: Florence Dixon, Byron Douglas and Olaf Hytton will appear in 'It Is the Law'". Moving Picture World. 66 (7): 553. 1924-02-16.
 ^ "Daughters of the Night". AFI Catalogue of Feature Films: The First 100 Years 1893–1993. American Film Institute. Retrieved 2018-05-14.
 ^ a b Solomon 2011, p. 66.
 ^ a b Koszarski 2008, p. 107.
 ^ "Three big productions from Fox announced for August 31 week". Moving Picture World. 70 (1): 25. 1924-09-06.
 ^ "It Is the Law". Catalog of Copyright Entries Part 1, Group 2. New Series. 21 (1–2): 1362. 1924.
 ^ "It Is the Law!". Catalog of Copyright Entries, Parts 12–13. Series 3. 5 (1): 140. 1951.
 ^ "Fox has many exploitation novelties to aid showmen". Moving Picture World. 70 (2): 126. 1924-09-13.
 ^ "J. Gordon Edwards leaving". Variety. 74 (7): 17. 1924-04-02.
 ^ Clapp 2002, pp. 70–71.
 ^ "It Is the Law". Motion Picture Magazine. 28 (11): 102. 1924.
 ^ "It Is the Law—Fox". Photoplay. 26 (6): 62. 1924.
 ^ "'It Is the Law'—65%". Screen Opinions. 15 (1): 5. September 15–30, 1924.
 ^ Slide 2000, p. 13.
 ^ "It Is The Law / Arthur Hohl [motion picture]". American Silent Feature Film Database. Library of Congress. 2015-06-22. Retrieved 2015-09-25.
 ^ Solomon 2011, p. 1.
 ^ Brownlow 1976, p. 35.
 ^ Langman 1998, pp. 36, 40.
 Bordman, Gerald (1995). American Theatre: A Chronicle of Comedy and Drama, 1914–1930. Oxford University Press. ISBN 978-0-19-509078-9. Brownlow, Kevin (1976) [1968]. The Parade's Gone By... University of California Press. ISBN 978-0-520-03068-8. Clapp, Nicholas (2002). Sheba: Through the Desert in Search of the Legendary Queen. Houghton Mifflin. ISBN 978-0-395-95283-2. Hischak, Thomas S. (2009). Broadway Plays and Musicals: Descriptions and Essential Facts of More than 14,000 Shows through 2007. McFarland. ISBN 978-0-7864-3448-0. Koszarski, Richard (2008). Hollywood on the Hudson: Film and Television in New York from Griffith to Sarnoff. Rutgers University Press. ISBN 978-0-8135-4293-5. Langman, Larry (1998). American Film Cycles: The Silent Era. Greenwood Press. ISBN 978-0-313-30657-0. Liebman, Roy (1998). From Silents to Sound: A Biographical Encyclopedia of Performers who Made the Transition to Talking Pictures. McFarland. ISBN 978-0-7864-0382-0. Palmieri, Anthony F. R. (1980). Elmer Rice: A Playwright's Vision of America. Associated University Presses. ISBN 978-0-8386-2333-6. Slide, Anthony (2000). Nitrate Won't Wait: A History of Film Preservation in the United States. McFarland. ISBN 978-0-7864-0836-8. Solomon, Aubrey (2011). The Fox Film Corporation, 1915–1935: A History and Filmography. McFarland. ISBN 978-0-7864-6286-5. It Is the Law at AllMovie It Is the Law on IMDb v t e St. Elmo (1914) Life's Shop Window (1914) The Celebrated Scandal (1915) Anna Karenina (1915) A Woman's Resurrection (1915) Should a Mother Tell? (1915) The Song of Hate (1915) Blindness of Devotion (1915) The Galley Slave (1915) The Unfaithful Wife (1915) The Green-Eyed Monster (1916) A Wife's Sacrifice (1916) The Spider and the Fly (1916) Under Two Flags (1916) Her Double Life (1916) Romeo and Juliet (1916) The Vixen (1916) The Darling of Paris (1917) The Tiger Woman (1917) Her Greatest Love (1917) Tangled Lives (1917) Heart and Soul (1917) Camille (1917) The Rose of Blood (1917) Madame Du Barry (1917) The Forbidden Path (1918) The Soul of Buddha (1918) Under the Yoke (1918) Cleopatra (1917) When a Woman Sins (1918) The She-Devil (1918) The Light (1919) Salomé (1919) When Men Desire (1919) The Siren's Song (1919) A Woman There Was (1919) The Lone Star Ranger (1919) Wolves of the Night (1919) The Last of the Duanes (1919) Wings of the Morning (1919) Heart Strings (1920) The Adventurer (1920) The Orphan (1920) The Joyous Troublemaker (1920) If I Were King (1920) Drag Harlan (1920) The Scuttlers (1920) His Greatest Sacrifice (1921) The Queen of Sheba (1921) Nero (1922) The Silent Command (1923) The Shepherd King (1923) The Net (1923) It Is the Law (1924) Blindness of Devotion (1915) A Wife's Sacrifice (1916) The She-Devil (1918) The Queen of Sheba (1921) A Daughter of the Gods (1916) Filmography Blake Edwards 1924 films 1920s mystery films American silent feature films American films American black-and-white films American films based on plays Films directed by J. Gordon Edwards Lost American films Fox Film films American mystery films CS1: Julian–Gregorian uncertainty Articles with short description Featured articles Commons category link is on Wikidata Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version  This page was last edited on 14 October 2019, at 00:05 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  It Is the Law ^ a b c d e a b c d e 76 ^ a b c a b 26 a b a b c d e 29 ^ ^ a b c d ^ ^ 66 ^ ^ 66 ^ a b a b ^ 70 ^ 21 ^ 5 ^ 70 ^ 74 ^ ^ 28 ^ 26 ^ 15 ^ ^ ^ ^ ^ It Is the Law Lobby cards 
J. Gordon Edwards
 Curtis Benton It Is the Lawby Elmer Rice and Hayden Talbot 
Arthur Hohl
Herbert Heyes
Mona Palma
 George W. Lane Fox Film  Fox Film 
August 31, 1924 (1924-08-31)
 7 reels United States Silent (English intertitles)  Wikimedia Commons has media related to It Is the Law. 
St. Elmo (1914)
Life's Shop Window (1914)
The Celebrated Scandal (1915)
Anna Karenina (1915)
A Woman's Resurrection (1915)
Should a Mother Tell? (1915)
The Song of Hate (1915)
Blindness of Devotion (1915)
The Galley Slave (1915)
The Unfaithful Wife (1915)
The Green-Eyed Monster (1916)
A Wife's Sacrifice (1916)
The Spider and the Fly (1916)
Under Two Flags (1916)
Her Double Life (1916)
Romeo and Juliet (1916)
The Vixen (1916)
The Darling of Paris (1917)
The Tiger Woman (1917)
Her Greatest Love (1917)
Tangled Lives (1917)
Heart and Soul (1917)
Camille (1917)
The Rose of Blood (1917)
Madame Du Barry (1917)
The Forbidden Path (1918)
The Soul of Buddha (1918)
Under the Yoke (1918)
Cleopatra (1917)
When a Woman Sins (1918)
The She-Devil (1918)
The Light (1919)
Salomé (1919)
When Men Desire (1919)
The Siren's Song (1919)
A Woman There Was (1919)
The Lone Star Ranger (1919)
Wolves of the Night (1919)
The Last of the Duanes (1919)
Wings of the Morning (1919)
Heart Strings (1920)
The Adventurer (1920)
The Orphan (1920)
The Joyous Troublemaker (1920)
If I Were King (1920)
Drag Harlan (1920)
The Scuttlers (1920)
His Greatest Sacrifice (1921)
The Queen of Sheba (1921)
Nero (1922)
The Silent Command (1923)
The Shepherd King (1923)
The Net (1923)
It Is the Law (1924)
 
Blindness of Devotion (1915)
A Wife's Sacrifice (1916)
The She-Devil (1918)
The Queen of Sheba (1921)
 
A Daughter of the Gods (1916)
 
Filmography
Blake Edwards
 