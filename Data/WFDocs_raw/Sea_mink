
 The sea mink (Neovison macrodon) is a recently extinct species of mink that lived on the eastern coast of North America in the family Mustelidae, the largest family in the order Carnivora. It was most closely related to the American mink (Neovison vison), with debate about whether or not the sea mink should be considered a subspecies of the American mink (making it Neovison vison macrodon) or a species of its own. The main justification for a separate species designation is the size difference between the two minks, but other distinctions have been made, such as its redder fur. The only known remains are fragments unearthed in Native American shell middens. Its actual size is speculative, based largely on tooth-remains.
 The sea mink was first described in 1903, after its extinction; information regarding its external appearance and habits stem from speculation and from accounts made by fur traders and Native Americans. It may have exhibited behavior similar to the American mink, in that it probably maintained home ranges, was polygynandrous, and had a similar diet, though more seaward-oriented. It was probably found on the New England coast and the Maritime Provinces, though its range may have stretched further south during the last glacial period. Conversely, its range may have been restricted solely to the New England coast, specifically the Gulf of Maine, or just to nearby islands. The largest of the minks, the sea mink was more desirable to fur traders and became extinct in the late 19th or early 20th century.
 The sea mink was first described as Lutreola macrodon, distinct from the American mink, by Daniel Webster Prentiss, a medical doctor and ornithologist, in 1903 after it became extinct. Prentiss based his description on skull fragments recovered from Native American shell middens in New England. Most sea mink remains, nearly all of them skull fragments, have come from shell middens, but a complete specimen has never been found.[4][5]
 Debate has occurred regarding whether the sea mink was its own species, or a subspecies of the American mink. Those who argue that the sea mink was a subspecies often refer to it as Neovison vison macrodon.[6][7] A study in 1911 by Frederic Brewster Loomis, an American paleontologist, concluded that the differences between the American mink and the sea mink were too minute to justify the latter's classification as a separate species, and he named it Lutreola vison antiquus.[8] A study conducted in 2000 by Mead et al. refuted Loomis by claiming that the size range for the largest sea mink specimen was beyond that of the American mink, thereby making it a separate species.[9] But a 2001 study by Graham concluded that this size difference was insufficient evidence to classify the sea mink as its own species and that it should be considered a subspecies. Graham supposed that the size difference was caused by environmental factors. Furthermore, Graham reported that Mead assumed the smaller mink specimens to be the American mink, and the larger mink specimens outside the range of the American mink to be sea minks; this may have been a case of sexual dimorphism wherein all specimens were sea minks, the larger being males and the smaller being females.[6] A 2007 study compared the dental makeup of the sea mink to the American mink, and concluded that they were distinct enough to be considered separate species.[4]
 Mustela africana
 Mustela felipei
 Mustela frenata
 Neovison vison
 Neovison macrodon
 The taxonomy of minks was revised in 2000, resulting in the formation of a new genus, Neovison, which includes only the sea mink and the American mink. Formerly, both minks were classified in the genus Mustela.[11] The species name macrodon translates to "large teeth".[12] According to Richard Manville, a naturalist who maintains that the sea mink is not its own species, its closest relative is the common mink (N. v. mink), which also inhabits the New England area.[13]
 Fur traders who hunted it gave the sea mink various names, including water marten, red otter, and fisher cat. Possibly the first description of this species was made by Sir Humphrey Gilbert
in the late 1500s as "a fish like a greyhound", which was a reference to its affinity for the sea and its body shape and gait, which were apparently similar to that of a greyhound. It is possible that the fisher (Pekania pennanti) got its name from being mistakenly identified as the sea mink, which was also known as the fisher by fur traders.[14] The Abnaki Indians referred to it as the "mousebeysoo", which means "wet thing".[13] It was named "sea mink" because it was always found near the coast by fur traders, and subsequently the American mink was often referred to as the "wood mink".[13][15]
 The sea mink was a marine mammal that lived around the rocky coasts of New England and the southernmost Maritime Provinces until hunted to extinction in the late 19th century. Most sea mink remains are unearthed on the coast of Maine.[16] Though it is speculated that they at one point inhabited Connecticut and Rhode Island, they were commonly trapped along the coast of the Bay of Fundy (in the Gulf of Maine), and it is said that they formerly existed on the southwestern coast of Nova Scotia.[13] There were reports of unusually large mink furs being collected from Nova Scotia regularly.[14][15] The bones of a specimen unearthed in Middleboro, Massachusetts, were dated to be around 4,300±300 years old, 19 kilometers (12 mi) from salt water.[16] The sea mink may have reached that area by traveling up rivers, or may have been brought there by Native Americans. The latter is most likely as no other mink remains have been discovered between Casco Bay in Maine and southeastern Massachusetts.[17] Sea mink bones have been unearthed in Canada, although these may have been carried there by Native Americans from the Gulf of Maine. The rugged shorelines of the Down East region of Maine may have represented a northernmost barrier in their range.[16] Mead concluded that only American minks inhabited the mainland and that sea minks were restricted to islands off the coast. If this is the case, then all remains found on the mainland were carried there.[9] Graham challenged that hypothesis, stating that it is unlikely that all sea mink specimens originate from one population.[6]
 During the last glacial period, ending 12,000 years ago, the sea mink's range may have extended south of the Gulf of Maine. It may have even evolved there, as Maine at that time would have been covered in glaciers, although the oldest known specimen only dates back to around 5,000 years; this could be due to the rising sea levels—older sea mink remains may be submerged underwater. Alternately, the sea mink may have evolved after the last glacial period and filled a new ecological niche.[4]
 Since the sea mink has only been described by fragmentary remains, its appearance and behaviors are not well-documented. Its relatives, as well as descriptions by fur traders and Native Americans, give a general idea of this animal's appearance and its ecological roles. Accounts from Native Americans in the New England/Atlantic Canadian regions reported that the sea mink had a fatter body than the American mink. The sea mink produced a distinctive fishy odor, and had fur that was said to be coarser and redder than that of the American mink.[4][18][19] It is thought that naturalist Joseph Banks encountered this animal in 1776 in the Strait of Belle Isle, and he described it as being slightly larger than a fox, having long legs, and a tail that was long and tapered towards the end, similar to a greyhound.[14]
 The sea mink was the largest of the minks. As only fragmentary skeletal remains of the sea mink exist, most of its external measurements are speculative and rely only on dental measurements.[4][13][20] In 1929, Ernest Thompson Seton, a wildlife artist, concluded that the probable dimensions for this animal are 91.4 centimeters (36 in) from head to tail, with the tail being 25.4 centimeters (10 in) long.[21]  A possible mounted sea mink specimen collected in 1894 in Connecticut measured 72 centimeters (28 in) from head to tail and the tail was 25.4 centimeters (10 in) in length; a 1996 study found this to be either a large American mink or possibly a hybrid. The specimen was described as having coarse fur that was reddish-tan in color, though much of it was likely faded from age. It was darkest at the tail and the hind limbs, with a 5-by-1.5-centimeter (2 by 0.6 in) white patch between the forearms. There were also white spots on the left forearm and the groin region.[13]
 The type specimen was collected by Prentiss and Frederick True, a biologist, in 1897 in Brooklin, Maine, the remains of which consist of a maxilla, parts of the nasal bone, and the palate. The teeth are all present on the right side of the palate, and the left side consists of the incisors and one premolar. Other than a chipped canine, all the teeth are in good condition. The specimen is apparently larger than the Alaskan mink, as the average distance between the last incisor to the first molar is 2.8 centimeters (1.1 in) in the Alaskan mink, whereas that distance is 3 centimeters (1.2 in) in the type specimen. The nasal bone has an abrupter ascension, and the carnassial teeth make a more acute angle with the gums than those of the common mink.[5][13]
 These minks were large and heavily built, with a low sagittal crest and short, wide postorbital processes (projections on the frontal bone behind the eye sockets).[13] In fact, the most notable characteristic of the skull was its size, in that it was clearly larger than that of other mink species, having a wide rostrum, large nostril openings, large antorbital fenestrae (openings in the skull in front of the eye socket), and large teeth.[15] Their large size was probably in response to their coastal environment, as the largest extant subspecies of American mink, the Alaskan mink (N. v. nesolestes), inhabits the Alexander Archipelago in Alaska, an area with a habitat similar to the Gulf of Maine. Mead, concluding that the mink was restricted to nearshore islands, suggested that the large size was due to insular gigantism.[9] Since almost all members of the subfamily Mustelinae exhibit sexual dimorphism, male sea minks were probably larger than female sea minks. The sea mink's wider carnassial teeth and blunter carnassial blades suggest that they crushed hard shells more often than did the teeth of the American mink.[4]
 As marine mammal species often play a large part in their ecosystems, it is possible that the sea mink was an important intertidal predator. It may have been similar in diet to the American mink, and may have consumed seabirds, seabird eggs, and hard-bodied marine invertebrates, though in greater proportions.[4] Fur traders reported that sea mink dens had two entrances, and were made in the rocks piled up by the waves. Remains of toad sculpins and ocean pout were the most common around their dens, and garden banded snails were also reported to have been part of their diet.[13] Their seafood-oriented diet may have increased their size.[6] According to fur traders, the sea mink was nocturnal and resided in caves and rock crevices during the day.[14] Due to the overlap of American mink and sea mink ranges, it is possible that they hybridized with each other. Although not a truly marine species, being confined to coastal waters, the sea mink was unusually aquatic compared to other members of Musteloidea, being, next to otters, the most aquatic member of the taxon.[4]
 Like other minks, individual sea minks may have maintained home ranges, and, since the males were larger and required more food, males would have had larger territorial claims. Likewise, their larger size may have allowed the males to target larger prey than the females, and they may have had to defend females during mating seasons. Like other weasels, the sea mink was probably polygynandrous, with both sexes mating with multiple individuals.[4]
 The sea mink was pursued by fur traders due to its large size; this made it more desirable than other mink species further inland. The unregulated trade eventually led to its extinction, which is thought to have occurred between 1860 and 1920.[14][16] The sea mink was seldom sighted after 1860. The last two recorded kills of a sea mink were made in Maine in 1880 near Jonesport, Maine, and Campobello Island, New Brunswick, in 1894,[14] although the 1894 kill is speculated to be of large American minks.[13] Fur traders made traps to catch sea minks and also pursued them with dogs, although they were rarely trapped. If a sea mink escaped into a small hole on the rocky ledges, it was dug out by hunters using shovels and crowbars. If it was out of reach of the hunters, it was shot and then retrieved using an iron rod with a screw on the far end. If it was hiding, it was smoked out and suffocated.[13][15][19] The minks' nocturnal behavior may have been caused from pressure by fur traders who hunted them in daylight.[14]
 Since the remains of brain cases found in shell middens are broken and many of the bones found exhibit cut marks, it is assumed that the sea mink was hunted by Native Americans for food, and possibly for exchange and ceremonial purposes.[8][13][16] One study looking at the remains in shell middens in Penobscot Bay reported that sea mink craniums were intact, more so than that of other animals found, implying that they were specifically placed there.[22] Males were more often collected than females.[4]
 
†Lutreola vison antiquus Loomis, 1911
†Mustela macrodon
†Mustela vison macrodon
†Neovison vison macrodon
 †Lutreola vison antiquus Loomis, 1911 †Mustela macrodon †Mustela vison macrodon †Neovison vison macrodon 1 Taxonomy and etymology 2 Range 3 Description 4 Behavior 5 Exploitation and extinction 6 References 7 External links ^ Helgen, K.; Turvey, S.T. (2016). "Neovison macrodon". IUCN Red List of Threatened Species. Version 2016. International Union for Conservation of Nature. Retrieved 6 November 2017.CS1 maint: uses authors parameter (link).mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ "Neovison macrodon". NatureServe Explorer. Retrieved 13 July 2017.
 ^ Wozencraft, W.C. (2005). "Order Carnivora".  In Wilson, D.E.; Reeder, D.M (eds.). Mammal Species of the World: A Taxonomic and Geographic Reference (3rd ed.). Johns Hopkins University Press. p. 619. ISBN 978-0-8018-8221-0. OCLC 62265494.
 ^ a b c d e f g h i j Sealfon, R. A. (2007). "Dental divergence supports species status of the extinct sea mink (Neovison macrodon)". Journal of Mammalogy. 88 (2): 371–383. doi:10.1644/06-MMM-A-227R1.1. JSTOR 4498666.
 ^ a b Prentiss, D. W. (1903). "Description of an extinct mink from the shell-heaps of the Maine coast" (PDF). Proceedings of the United States National Museum. 26 (1336): 887–888. doi:10.5479/si.00963801.26-1336.887.
 ^ a b c d Graham, R. (2001). "Comment on "Skeleton of extinct North American sea mink (Mustela macrodon)" by Mead et al.". Quaternary Research. 56 (3): 419–421. Bibcode:2001QuRes..56..419G. doi:10.1006/qres.2001.2266.
 ^ Kays, R. W.; Wilson, D. E. (2009). Mammals of North America (2nd ed.). Boston, Massachusetts: Princeton University Press. p. 180. ISBN 978-0-6911-4092-6. OCLC 880833145.
 ^ a b Loomis, F. B. (1911). "A new mink from the shell-heaps of Maine". American Journal of Science. 31 (183): 227–229. doi:10.2475/ajs.s4-31.183.227.
 ^ a b c Mead, J.; Spiess, A. E.; Sobolik, K. D. (2000). "Skeleton of extinct North American sea mink (Mustela macrodon)". Quaternary Research. 53 (2): 247–262. Bibcode:2000QuRes..53..247M. doi:10.1006/qres.1999.2109.
 ^ Nyakatura, K.; Bininda-Emonds, O. R. P. (2012). "Updating the evolutionary history of Carnivora (Mammalia): a new species-level supertree complete with divergence time estimates". BMC Biology. 10 (12): 12. doi:10.1186/1741-7007-10-12. PMC 3307490. PMID 22369503.
 ^ Abramov, A. V. (2000). "A taxonomic review of the genus Mustela (Mammalia, Carnivora)" (PDF). Zoosystematica Rossica. 8: 357–364.
 ^ "Etymology pages "M"". www.scotcat.com. Retrieved 16 August 2017.
 ^ a b c d e f g h i j k l Manville, R. H. (1966). "The extinct sea mink, with taxonomic notes" (PDF). Proceedings of the United States National Museum. 122 (3584): 1–12. doi:10.5479/si.00963801.122-3584.1.
 ^ a b c d e f g Mowat, F. (2012) [1984]. Sea of slaughter. Vancouver, British Columbia: Douglas and McIntyre. pp. 160–164. ISBN 978-1-77100-046-8. OCLC 879632158.
 ^ a b c d Hollister, N. (1965). "A synopsis of the American minks". Proceedings of the United States National Museum. 44 (1965): 478–479. doi:10.5479/si.00963801.44-1965.471.
 ^ a b c d e Black, D. W.; Reading, J. E.; Savage, H. G. (1998). "Archaeological records of the extinct sea mink, Mustela macrodon (Carnivora: Mustelidae), from Canada" (PDF). Canadian Field-Naturalist. 112 (1): 45–49. Archived from the original (PDF) on 2017-10-11. Retrieved 2017-07-13.
 ^ Waters, J. H.; Ray, C. E. (1961). "Former range of the sea mink". Journal of Mammalogy. 42 (3): 380–383. doi:10.2307/1377035. JSTOR 1377035.
 ^ Day, David (1981). The encyclopedia of vanished species. London: Universal Books. p. 220. ISBN 978-0-947889-30-2.
 ^ a b Hardy, M. (1903). "The extinct mink from the Maine shell heaps". Forest and Stream. LXI (I): 125.
 ^ Mead, J. I.; Spiess, A. E. (2001). "Reply to Russell Graham about Mustela macrodon". Quaternary Research. 56 (3): 422–423. Bibcode:2001QuRes..56..422M. doi:10.1006/qres.2001.2268.
 ^ Seton, E. T. (1929). Lives of game animals. 2. Doubleday, Doran. p. 562. OCLC 872457192.
 ^ Bourque, B. J. (1995). Diversity and complexity in prehistoric maritime societies: a Gulf Of Maine perspective. New York, New York: Plenum Press. p. 341. ISBN 978-0-306-44874-4. The Mustela macrodon cranium is much more complete than those from the rest of the midden, adding to the impression that these bones were placed especially in the cache.
  Data related to sea minks at Wikispecies Arkive.org v t e Kingdom: Animalia Phylum: Chordata Class: Mammalia Order: Carnivora Suborder: Caniformia African clawless otter (Aonyx capensis) Oriental small-clawed otter (Aonyx cinerea) Sea otter (Enhydra lutris) Northern river otter (Lontra canadensis) Southern river otter (Lontra provocax) Neotropical river otter (Lontra longicaudis) Marine otter (Lontra felina) Eurasian otter (Lutra lutra) Hairy-nosed otter (Lutra sumatrana) Speckle-throated otter (Hydrictis maculicollis) Smooth-coated otter (Lutrogale perspicillata) Giant otter (Pteronura brasiliensis) Hog badger (Arctonyx collaris) Eurasian badger (Meles meles) Everett's ferret badger (Melogale everetti) Chinese ferret badger (Melogale moschata) Javan ferret-badger (Melogale orientalis) Burmese ferret badger (Melogale personata) Palawan stink badger (Mydaus marchei) Javan stink badger (Mydaus javanensis) Honey badger (Mellivora capensis) American badger (Taxidea taxus) Tayra (Eira barbara) Lesser grison (Galictis cuja) Greater grison (Galictis vittata) Wolverine (Gulo gulo) Saharan striped polecat (Ictonyx libyca) Striped polecat (Ictonyx striatus) Patagonian weasel (Lyncodon patagonicus) American marten (Martes americana) Yellow-throated marten (Martes flavigula) Beech marten (Martes foina) Nilgiri marten (Martes gwatkinsii) Pine marten (Martes martes) Japanese marten (Martes melampus) Fisher (Martes pennanti) Sable (Martes zibellina) Tropical weasel (Mustela africana) Mountain weasel (Mustela altaica) Stoat (Mustela erminea) Steppe polecat (Mustela eversmannii) Colombian weasel (Mustela felipei) Long-tailed weasel (Mustela frenata) Japanese weasel (Mustela itatsi) Yellow-bellied weasel (Mustela kathiah) European mink (Mustela lutreola) Indonesian mountain weasel (Mustela lutreolina) Black-footed ferret (Mustela nigripes) Least weasel (Mustela nivalis) Malayan weasel (Mustela nudipes) European polecat (Mustela putorius) Siberian weasel (Mustela sibirica) Black-striped weasel (Mustela strigidorsa) Egyptian weasel (Mustela subpalmata) American mink (Neovison vison) Sea mink (Neovison macrodon) African striped weasel (Poecilogale albinucha) Marbled polecat (Vormela peregusna) Mammals portal Marine life portal Wikidata: Q583627 Wikispecies: Neovison macrodon ADW: Neovison_macrodon ARKive: neovison-macrodon EoL: 926019 Fossilworks: 367292 GBIF: 2433651 iNaturalist: 74757 IRMNG: 11701550 ITIS: 726283 IUCN: 40784 MSW: 14001483 SeaLifeBase: 100091 WoRMS: 404093 IUCN Red List extinct species Mammals described in 1903 Extinct animals of the United States Extinct mammals of North America Mammal extinctions since 1500 Marine mammals Mustelinae Species made extinct by human activities Species endangered by use in wearables CS1 maint: uses authors parameter Articles with short description Featured articles Articles with 'species' microformats Commons category link is on Wikidata Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Wikispecies Create a book Download as PDF Printable version العربية Български Brezhoneg Català Cebuano Čeština Deutsch Español Euskara فارسی Français Interlingua Italiano Latviešu Magyar Nederlands 日本語 پنجابی Polski Português Русский Svenska Українська Tiếng Việt Winaray 中文  This page was last edited on 16 October 2019, at 16:44 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  N. macrodon †Neovison macrodon sea mink Neovison macrodon ^ ^ ^ a b c d e f g h i j 88 a b 26 a b c d 56 ^ a b 31 a b c 53 ^ 10 ^ 8 ^ a b c d e f g h i j k l 122 a b c d e f g a b c d 44 a b c d e 112 ^ 42 ^ a b LXI ^ 56 ^ 2 ^ sea minks Category 
 Drawing of a sea mink published by the Canadian Field-Naturalist in 1988
 Extinct  (1894) (IUCN 3.1)[1]
 Kingdom:
 Animalia
 Phylum:
 Chordata
 Class:
 Mammalia
 Order:
 Carnivora
 Family:
 Mustelidae
 Genus:
 Neovison
 Species:
 †N. macrodon
 †Neovison macrodon(Prentiss, 1903)
 
 The sea mink was found in the Gulf of Maine area.
 

List

†Lutreola vison antiquus Loomis, 1911
†Mustela macrodon
†Mustela vison macrodon
†Neovison vison macrodon


 New World weasels
 


Mustelinae




 




 


.mw-parser-output table.clade{border-spacing:0;margin:0;font-size:100%;line-height:100%;border-collapse:separate;width:auto}.mw-parser-output table.clade table.clade{width:100%}.mw-parser-output table.clade td.clade-label{width:0.7em;padding:0 0.2em;vertical-align:bottom;text-align:center;border-left:1px solid;border-bottom:1px solid}.mw-parser-output table.clade td.clade-label.first{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-label.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-slabel{padding:0 0.2em;vertical-align:top;text-align:center;border-left:1px solid}.mw-parser-output table.clade td.clade-slabel.last{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-slabel.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-bar{vertical-align:middle;text-align:left;padding:0 0.5em;position:relative}.mw-parser-output table.clade td.clade-bar.reverse{text-align:right}.mw-parser-output table.clade td.clade-leaf{border:0;padding:0;text-align:left}.mw-parser-output table.clade td.clade-leafR{border:0;padding:0;text-align:right}.mw-parser-output table.clade td.clade-leaf.reverse{text-align:right}


 


Mustela africana



 


 


Mustela felipei



 



 


 


Mustela frenata



 



 


 





 


Neovison vison



 


 


Neovison macrodon



 



 



 

 Mustelinae
 


 




 


.mw-parser-output table.clade{border-spacing:0;margin:0;font-size:100%;line-height:100%;border-collapse:separate;width:auto}.mw-parser-output table.clade table.clade{width:100%}.mw-parser-output table.clade td.clade-label{width:0.7em;padding:0 0.2em;vertical-align:bottom;text-align:center;border-left:1px solid;border-bottom:1px solid}.mw-parser-output table.clade td.clade-label.first{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-label.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-slabel{padding:0 0.2em;vertical-align:top;text-align:center;border-left:1px solid}.mw-parser-output table.clade td.clade-slabel.last{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-slabel.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-bar{vertical-align:middle;text-align:left;padding:0 0.5em;position:relative}.mw-parser-output table.clade td.clade-bar.reverse{text-align:right}.mw-parser-output table.clade td.clade-leaf{border:0;padding:0;text-align:left}.mw-parser-output table.clade td.clade-leafR{border:0;padding:0;text-align:right}.mw-parser-output table.clade td.clade-leaf.reverse{text-align:right}


 


Mustela africana



 


 


Mustela felipei



 



 


 


Mustela frenata



 



 


 





 


Neovison vison



 


 


Neovison macrodon



 



 

  
 


 


.mw-parser-output table.clade{border-spacing:0;margin:0;font-size:100%;line-height:100%;border-collapse:separate;width:auto}.mw-parser-output table.clade table.clade{width:100%}.mw-parser-output table.clade td.clade-label{width:0.7em;padding:0 0.2em;vertical-align:bottom;text-align:center;border-left:1px solid;border-bottom:1px solid}.mw-parser-output table.clade td.clade-label.first{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-label.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-slabel{padding:0 0.2em;vertical-align:top;text-align:center;border-left:1px solid}.mw-parser-output table.clade td.clade-slabel.last{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-slabel.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-bar{vertical-align:middle;text-align:left;padding:0 0.5em;position:relative}.mw-parser-output table.clade td.clade-bar.reverse{text-align:right}.mw-parser-output table.clade td.clade-leaf{border:0;padding:0;text-align:left}.mw-parser-output table.clade td.clade-leafR{border:0;padding:0;text-align:right}.mw-parser-output table.clade td.clade-leaf.reverse{text-align:right}


 


Mustela africana



 


 


Mustela felipei



 



 


 


Mustela frenata



 

  
 
.mw-parser-output table.clade{border-spacing:0;margin:0;font-size:100%;line-height:100%;border-collapse:separate;width:auto}.mw-parser-output table.clade table.clade{width:100%}.mw-parser-output table.clade td.clade-label{width:0.7em;padding:0 0.2em;vertical-align:bottom;text-align:center;border-left:1px solid;border-bottom:1px solid}.mw-parser-output table.clade td.clade-label.first{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-label.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-slabel{padding:0 0.2em;vertical-align:top;text-align:center;border-left:1px solid}.mw-parser-output table.clade td.clade-slabel.last{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-slabel.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-bar{vertical-align:middle;text-align:left;padding:0 0.5em;position:relative}.mw-parser-output table.clade td.clade-bar.reverse{text-align:right}.mw-parser-output table.clade td.clade-leaf{border:0;padding:0;text-align:left}.mw-parser-output table.clade td.clade-leafR{border:0;padding:0;text-align:right}.mw-parser-output table.clade td.clade-leaf.reverse{text-align:right}


 


Mustela africana



 


 


Mustela felipei



 

  
 
Mustela africana

  
  
 
Mustela felipei

  
  
  
 
Mustela frenata

  
  
  
 



 


Neovison vison



 


 


Neovison macrodon



 

  
 
Neovison vison

  
  
 
Neovison macrodon

  
  
  
 Relations of the sea mink within Mustelinae[10]
  Wikimedia Commons has media related to sea minks. 
Kingdom: Animalia
Phylum: Chordata
Class: Mammalia
Order: Carnivora
Suborder: Caniformia
 Aonyx
African clawless otter (Aonyx capensis)
Oriental small-clawed otter (Aonyx cinerea)
Enhydra
Sea otter (Enhydra lutris)
Lontra
Northern river otter (Lontra canadensis)
Southern river otter (Lontra provocax)
Neotropical river otter (Lontra longicaudis)
Marine otter (Lontra felina)
Lutra
Eurasian otter (Lutra lutra)
Hairy-nosed otter (Lutra sumatrana)
Hydrictis
Speckle-throated otter (Hydrictis maculicollis)
Lutrogale
Smooth-coated otter (Lutrogale perspicillata)
Pteronura
Giant otter (Pteronura brasiliensis)
 
African clawless otter (Aonyx capensis)
Oriental small-clawed otter (Aonyx cinerea)
 
Sea otter (Enhydra lutris)
 
Northern river otter (Lontra canadensis)
Southern river otter (Lontra provocax)
Neotropical river otter (Lontra longicaudis)
Marine otter (Lontra felina)
 
Eurasian otter (Lutra lutra)
Hairy-nosed otter (Lutra sumatrana)
 
Speckle-throated otter (Hydrictis maculicollis)
 
Smooth-coated otter (Lutrogale perspicillata)
 
Giant otter (Pteronura brasiliensis)
 Arctonyx
Hog badger (Arctonyx collaris)
Meles
Eurasian badger (Meles meles)
Melogale
Everett's ferret badger (Melogale everetti)
Chinese ferret badger (Melogale moschata)
Javan ferret-badger (Melogale orientalis)
Burmese ferret badger (Melogale personata)
Mydaus
Palawan stink badger (Mydaus marchei)
Javan stink badger (Mydaus javanensis)
 
Hog badger (Arctonyx collaris)
 
Eurasian badger (Meles meles)
 
Everett's ferret badger (Melogale everetti)
Chinese ferret badger (Melogale moschata)
Javan ferret-badger (Melogale orientalis)
Burmese ferret badger (Melogale personata)
 
Palawan stink badger (Mydaus marchei)
Javan stink badger (Mydaus javanensis)
 Mellivora
Honey badger (Mellivora capensis)
 
Honey badger (Mellivora capensis)
 Taxidea
American badger (Taxidea taxus)
 
American badger (Taxidea taxus)
 Eira
Tayra (Eira barbara)
Galictis
Lesser grison (Galictis cuja)
Greater grison (Galictis vittata)
Gulo
Wolverine (Gulo gulo)
Ictonyx
Saharan striped polecat (Ictonyx libyca)
Striped polecat (Ictonyx striatus)
Lyncodon
Patagonian weasel (Lyncodon patagonicus)
Martes
American marten (Martes americana)
Yellow-throated marten (Martes flavigula)
Beech marten (Martes foina)
Nilgiri marten (Martes gwatkinsii)
Pine marten (Martes martes)
Japanese marten (Martes melampus)
Fisher (Martes pennanti)
Sable (Martes zibellina)
Mustela
Tropical weasel (Mustela africana)
Mountain weasel (Mustela altaica)
Stoat (Mustela erminea)
Steppe polecat (Mustela eversmannii)
Colombian weasel (Mustela felipei)
Long-tailed weasel (Mustela frenata)
Japanese weasel (Mustela itatsi)
Yellow-bellied weasel (Mustela kathiah)
European mink (Mustela lutreola)
Indonesian mountain weasel (Mustela lutreolina)
Black-footed ferret (Mustela nigripes)
Least weasel (Mustela nivalis)
Malayan weasel (Mustela nudipes)
European polecat (Mustela putorius)
Siberian weasel (Mustela sibirica)
Black-striped weasel (Mustela strigidorsa)
Egyptian weasel (Mustela subpalmata)
Neovison
American mink (Neovison vison)
Sea mink (Neovison macrodon)
Poecilogale
African striped weasel (Poecilogale albinucha)
Vormela
Marbled polecat (Vormela peregusna)
 
Tayra (Eira barbara)
 
Lesser grison (Galictis cuja)
Greater grison (Galictis vittata)
 
Wolverine (Gulo gulo)
 
Saharan striped polecat (Ictonyx libyca)
Striped polecat (Ictonyx striatus)
 
Patagonian weasel (Lyncodon patagonicus)
 
American marten (Martes americana)
Yellow-throated marten (Martes flavigula)
Beech marten (Martes foina)
Nilgiri marten (Martes gwatkinsii)
Pine marten (Martes martes)
Japanese marten (Martes melampus)
Fisher (Martes pennanti)
Sable (Martes zibellina)
 
Tropical weasel (Mustela africana)
Mountain weasel (Mustela altaica)
Stoat (Mustela erminea)
Steppe polecat (Mustela eversmannii)
Colombian weasel (Mustela felipei)
Long-tailed weasel (Mustela frenata)
Japanese weasel (Mustela itatsi)
Yellow-bellied weasel (Mustela kathiah)
European mink (Mustela lutreola)
Indonesian mountain weasel (Mustela lutreolina)
Black-footed ferret (Mustela nigripes)
Least weasel (Mustela nivalis)
Malayan weasel (Mustela nudipes)
European polecat (Mustela putorius)
Siberian weasel (Mustela sibirica)
Black-striped weasel (Mustela strigidorsa)
Egyptian weasel (Mustela subpalmata)
 
American mink (Neovison vison)
Sea mink (Neovison macrodon)
 
African striped weasel (Poecilogale albinucha)
 
Marbled polecat (Vormela peregusna)
 Category 
Wikidata: Q583627
Wikispecies: Neovison macrodon
ADW: Neovison_macrodon
ARKive: neovison-macrodon
EoL: 926019
Fossilworks: 367292
GBIF: 2433651
iNaturalist: 74757
IRMNG: 11701550
ITIS: 726283
IUCN: 40784
MSW: 14001483
SeaLifeBase: 100091
WoRMS: 404093
 