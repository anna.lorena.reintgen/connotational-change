
 
 No. 450 Squadron was a unit of the Royal Australian Air Force (RAAF) that operated during World War II. Established at RAAF Station Williamtown, New South Wales, in February 1941, it was the first Australian Article XV squadron formed under the Empire Air Training Scheme.
 The squadron embarked for the Middle East in April 1941; its personnel initially consisted solely of ground crew, who joined the pilots of No. 260 Squadron RAF to form No. 260/450 Squadron, which briefly operated Hawker Hurricane fighters in Syria. It was not until February 1942 that No. 450 Squadron, now with its own pilots and equipped with Curtiss P-40 Kittyhawk fighters, commenced operations in earnest. Over the next 15 months, it fought in the North African and Tunisian Campaigns in both fighter and fighter-bomber roles, claiming 49 German and Italian aircraft destroyed in the air and earning the nickname "The Desert Harassers".
 Beginning in July 1943, No. 450 Squadron took part in the Allied invasion of Sicily and the Italian campaign, primarily in the close support role. Its aircraft attacked targets in Yugoslavia as well as in Sicily and Italy. The squadron began converting from Kittyhawks to North American P-51 Mustang fighters in May 1945, but never saw action with its new aircraft. It was disbanded in August 1945 following the conclusion of hostilities, having suffered 63 fatal casualties during the war. Today, by agreement with the RAAF, the squadron's number is carried by a Royal Canadian Air Force unit, 450 Tactical Helicopter Squadron.
 No. 450 Squadron was formed at RAAF Station Williamtown, near Newcastle, New South Wales, on 7 February 1941. Raised a week before No. 451 Squadron, it was the first Australian squadron established for service with the British military under the Article XV of the Empire Air Training Scheme (EATS).[6][7] No. 450 Squadron was intended to be an "infiltration" squadron, which would consist initially only of ground crew and would receive a nucleus of experienced pilots after arriving in its designated theatre of operations.[8]
 Inaugurated at Ottawa, Canada, in October 1939, EATS was a plan to expand the Royal Air Force's (RAF) capacity to train aircrews by creating a pool of personnel from various Commonwealth countries—Australia, Canada, Britain and New Zealand—through the establishment of a common training system consisting of a series of initial, elementary, and advanced training schools. At the conclusion of advanced training, personnel were posted as required to RAF-controlled squadrons in Britain or the Middle East. These squadrons were designated as either RAF, RAAF, Royal Canadian Air Force or Royal New Zealand Air Force squadrons, but were paid for and administered by the British government, and personnel could be drawn from any Commonwealth nation. Seventeen RAAF squadrons were formed during the war under Article XV of the agreement.[9][10]
 Under the temporary command of Flight Lieutenant Bruce Shepherd,[11][12] No. 450 Squadron left Australia on 11 April 1941, embarking on the troopship Queen Elizabeth at Sydney, and arriving in Egypt on 3 May.[13][14] At RAF Abu Sueir, Squadron Leader Gordon Steege took command of the squadron before it was combined with the pilots and Hawker Hurricanes of No. 260 Squadron RAF, which had been established without ground crew, to form an operational squadron.[13][15] The combined unit, known as No. 260/450 (Hurricane) Squadron, then relocated to Amman in Transjordan. Its first operation was on 29 June 1941, when the Hurricanes attacked Vichy French airfields and infrastructure during the invasion of Syria.[16] No. 260/450 Squadron operated for ten days only and flew 61 sorties against airfields, 20 on offensive patrols and six on bomber-escort duties during the Syrian campaign.[17]
 In August 1941, No. 450 Squadron personnel were separated from No. 260 Squadron, when the latter received its own ground crew. No. 450 Squadron moved to Rayak airfield, Lebanon, where it was allocated Hurricanes and Miles Magister trainers.[13] A batch of 20 trainee Australian, British and Canadian pilots—the majority being Australian—were posted to the squadron in early October, and it began duties as an Operational Training Unit.[15][18] A fortnight later these pilots were posted out and, lacking trained pilots, the squadron's aircraft were re-allocated. On 20 October, the squadron moved to Burg El Arab, Egypt, and began operating as an advanced repair, salvage and service unit, taking part in the North African Campaign.[3][19]
 By December 1941, the squadron was receiving pilots and it began taking delivery of Curtiss P-40 Kittyhawk fighters.[19][20] On 19 December, RAF Middle East Command issued an administrative instruction declaring that although manned primarily by Australians, Nos. 450 and 451 Squadrons were "paid by and loaned to the Royal Air Force under the Empire Air Training Scheme and for all practical purposes they should be regarded as R.A.F. squadrons in every way". Confusion reigned for a time, but after intervention by the British Air Ministry, a further communique on 23 January 1942 announced that "450 and 451 E.A.T.S. Squadrons are to be regarded as R.A.A.F. squadrons".[21] Training began the same month, and No. 450 Squadron commenced operations from RAF Gambut on 19 February 1942, with an uneventful patrol near Tobruk. Three days later Sergeant Raymond Shaw became the first pilot from the squadron to claim an aerial victory, after he intercepted a Junkers Ju 88 near Gazala.[15][16]
 The squadron became part of the Desert Air Force's newly formed No. 239 Wing on 1 March 1942, serving alongside one Australian squadron, No. 3, and two RAF squadrons, Nos. 112 and 250 Squadrons.[22] No. 450 Squadron's main roles—escorting daylight raids by Douglas Boston bombers, and ground-attack missions in support of the Eighth Army—were hazardous and resulted in relatively heavy losses.[23] From 26 May, as Rommel launched an assault on the Gazala–Bir Hacheim line, all Kittyhawk units began to focus on the fighter-bomber role rather than air-to-air combat, to support retreating Commonwealth forces.[24][25] On 29 May, No. 450 Squadron claimed two Junkers Ju 87s and a Messerschmitt Bf 109, for the loss of three pilots killed, including Shaw.[26][27] Flight Sergeant Don McBurnie, the squadron's highest-scoring pilot with five solo victories and one shared, claimed his final "kill" on 4 July 1942 when he shot a Messerschmitt Bf 110 into the sea following a bombing mission on airfields west of Daba.[28][29]
 No. 450 Squadron took part in the decisive Second Battle of El Alamein, during October and November 1942, attacking enemy airfields and claiming three German and Italian fighters destroyed in the air.[30]  It suffered several losses during this time, including one of its leading scorers, Squadron Leader John Williams, who was shot down and taken prisoner on 31 October 1942, three days after he had been appointed commanding officer.[31][32] The squadron was frequently on the move as the Allies advanced following Second El Alamein, changing locations six times during November.[15][30] It often found itself using captured or hastily constructed airfields; one Kittyhawk was destroyed and several ground personnel killed or wounded by land mines at Marble Arch, Libya, in December 1942.[33][34]
 From late 1942, No. 450 Squadron was engaged in the Tunisian Campaign. By March 1943, as the Allied forces advanced, the squadron was operating from Medinne along the Mareth Line. That month it flew over 300 sorties. Further moves occurred, and by mid-April it was based at Karouan. Throughout April and early May, 350 sorties were flown, including attacks on Axis shipping in Cape Bon and in the Gulf of Tunis.[35] The campaign came to an end in mid-May,[36] but the squadron continued defensive patrols into June.[35] Between February 1942 and May 1943, it claimed 49 German and Italian aircraft destroyed in aerial combat, for the loss of 31 pilots, four to accidents.[37] As a result of its involvement in the North African fighting, the squadron received the nickname, "The Desert Harassers",[15] and adopted the motto, "Harass", both of which were derived from a comment by the Nazi propaganda broadcaster "Lord Haw Haw", who described the unit as "Australian mercenaries whose harassing tactics were easily beaten off by the Luftwaffe."[3]
 Following the conclusion of the fighting in the desert, No. 450 Squadron was allocated a ground-attack role during the Allied invasion of Sicily. Moving to Malta on 13 July 1943,[36] the squadron staged out of RAF Luqa, and undertook its first attack in Sicily against Carlentini. Four days later, on 17 July, No. 450 Squadron relocated to Pachino, Sicily, from where it continued ground-attack missions. A further move came on 1 August, when Nos. 450 and 3 Squadrons relocated to Agnone, near Catania, where they commenced close air support operations on 11 August, working closely with Allied ground units around Mount Etna.[35] On the night of 11 August, the airfield was attacked by Ju 88 bombers, which dropped incendiary, anti-personnel and high-explosive bombs for more than an hour. No. 450 Squadron's personnel had been located some distance from the airfield and only one Australian was wounded, although casualties among other units amounted to twelve killed and 60 wounded.[38] Eighteen RAAF Kittyhawks were destroyed, including eleven belonging to No. 450 Squadron.[39][40] Despite this, the two RAAF squadrons flew 22 sorties the following day.[41]
 During the early stages of the Allied campaign on the Italian mainland, which commenced in early September 1943, the squadron undertook bomber escort missions in support of the Eighth Army's landings. In the middle of September, it reverted to the close air support role, operating from Grottaglie, although it also undertook anti-shipping operations, including an attack on Manfredonia on 21 September, during which its aircraft sunk two vessels.[42] The following month, No. 450 Squadron was transferred to Foggia, and then to Mileni, where it was briefly withdrawn from operations to convert to newer model Kittyhawk IVs before rejoining the campaign in late November.[42]  In December, the squadron moved to Cutella, near Termoli, on the central Adriatic coast of Italy. There it encountered problems with severe winter weather restricting operations. Cutella airfield was located close to the beach; heavy rains caused a storm surge on 1 January 1944 and flooded the airfield, damaging some aircraft.[43]
 Meanwhile, Williams and another prisoner of war from No. 450 Squadron, Flight Lieutenant Reginald Kierath, were incarcerated with other Allied POWs at Stalag Luft III, in eastern Germany. In March 1944, both took part in "The Great Escape" and were among 50 POWs murdered by the Gestapo, after being recaptured.[44] Williams, who was 27 years old and from Sydney, was officially an RAF officer, as he had joined the British service under a short service commission in 1938.[44][45] Kierath, who was 29 and from Narromine, New South Wales, was an RAAF officer.[46]
 Throughout January 1944, No. 450 Squadron undertook anti-shipping operations as well as ground support tasks. It flew missions against targets off Dalmatia and in the harbour at Sibenik, as well as around the ports of Velaluka and Zera. In March, the squadron's attention returned to Italy, launching strikes against rolling stock; the following month saw it heavily tasked, flying a total of 430 sorties.[35] On 29 April 1944, a USAAF Republic P-47 Thunderbolt pilot strafed Cutella by mistake. No. 450 Squadron suffered no fatalities or aircraft destroyed but the pilot of a float plane belonging to an air-sea rescue unit was killed, some ground personnel were wounded, a Kittyhawk of No. 3 Squadron was destroyed and several others were damaged.[47][48] The following month, No. 450 Squadron moved to San Angelo, mounting a series of attacks against a 200-vehicle convoy near Subiaco in concert with other Kittyhawk squadrons that claimed 123 vehicles destroyed or damaged.[42][49] No. 450 Squadron later operated from several airfields in central and northern Italy, under the "cab rank" system, whereby patrolling fighter-bombers would attack as requested by army air liaison officers.[3][50] It flew over 1,100 sorties in June and July. The Australian Kittyhawk units were regularly lauded for the accuracy of their assaults; following a mission by No. 450 Squadron on 12 July, the Eighth Army wired No. 239 Wing headquarters: "Excellent bombing. Good show and thank you. No further attacks required."[51]
 No. 450 Squadron took part in the major offensive against the Gothic Line in August–September 1944.[52] Its first attack in early August was a strike on an artillery battery, during which three Kittyhawks were shot down; subsequent attacks throughout the following months were made against various targets including rolling stock, armour, and troop concentrations.[42] From November, after it had moved to Fano on the Italian north-east coast, the squadron also began attacking German forces in Yugoslavia.[53] Its average complement of pilots during the second half of 1944 was 25 from the RAAF, seven from the RAF and five from the South African Air Force.[54] No. 450 Squadron commenced operations from Cervia in February 1945; that month, it lost three pilots to prematurely detonating bombs.[36][55] On 21 March, it took part in Operation Bowler, a major air raid on Venice harbour.[42] The attack resulted in the sinking of a merchant ship, a torpedo boat, and a coastal steamer, as well as the destruction of five warehouses and other harbour infrastructure.[56]
 In May 1945, following the end of the war in Europe, No. 450 Squadron transferred to Lavariano, a few miles south of Udine in north-eastern Italy.[11][56] It also began replacing its Kittyhawks with North American P-51 Mustangs.[3][36] The squadron was disbanded at Lavariano on 20 August 1945.[11][56] During the war, it had lost 63 personnel killed in action, of whom 49 were Australian.[3]
 The squadron was not re-raised by the RAAF after the war, although the numerical designation of "450" was assumed by a Canadian helicopter unit, 450 Tactical Helicopter Squadron, in March 1968. The use of the "450" designation was the result of an administrative error, as the Canadian 400 series squadrons formed during World War II had been numbered between 400 and 449. An agreement was subsequently reached between the RCAF and RAAF and the squadron kept the designation. It is based at Petawawa, in Ontario, and operates Boeing CH-47 Chinook helicopters.[57][58]
 Gordon Steege, No. 450 Squadron RAAF's first commanding officer, became patron of the squadron association in April 2008; he died in September 2013, aged 95.[59][60]
 No. 450 Squadron operated the following aircraft:[61][62][63]
 No. 450 Squadron operated from the following bases and airfields:[61][63][64]
 No. 450 Squadron was commanded by the following officers:[66][67]
 
 1 History

1.1 Middle East and North Africa
1.2 Europe

 1.1 Middle East and North Africa 1.2 Europe 2 Legacy 3 Aircraft operated 4 Squadron bases 5 Commanding officers 6 See also 7 References

7.1 Notes
7.2 Bibliography

 7.1 Notes 7.2 Bibliography 8 Further reading RAAF units under RAF operational control ^ a b c Rawlings 1978, p. 441.
 ^ a b Halley 1988, p. 473.
 ^ a b c d e f "450 Squadron RAAF". Second World War, 1939–1945 units. Australian War Memorial. Archived from the original on 31 October 2013. Retrieved 13 October 2013..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ Flintham & Thomas 2003, p. 68.
 ^ Shores & Williams 1994, p. 68.
 ^ Barnes 2000, pp. 250, 255.
 ^ Eather 1995, pp. 103, 105.
 ^ RAAF Historical Section 1995, p. 105.
 ^ Gillison 1962, pp. 79–89.
 ^ Barnes 2000, p. 3.
 ^ a b c Barnes 2000, p. 254.
 ^ "Squadron Leader Bruce McRae Shepherd". Australian War Memorial. Retrieved 22 October 2013.
 ^ a b c Barnes 2000, p. 250.
 ^ RAAF Historical Section 1995, pp. 105–106.
 ^ a b c d e Eather 1995, p. 103.
 ^ a b RAAF Historical Section 1995, p. 106.
 ^ Herington 1954, p. 95.
 ^ Barnes 2000, pp. 250–251.
 ^ a b Barnes 2000, p. 251.
 ^ Herington 1954, p. 218.
 ^ Herington 1954, pp. 120–121.
 ^ Thomas 2005, p. 46.
 ^ Brown 1983, p. 259.
 ^ Brown 1983, p. 115.
 ^ Shores & Ring 1969, pp. 114–115.
 ^ Shores & Ring 1969, p. 116.
 ^ Brown 1983, pp. 117–118.
 ^ Brown 1983, pp. 139–140, 263.
 ^ Shores & Ring 1969, pp. 140, 240.
 ^ a b RAAF Historical Section 1995, p. 108.
 ^ "Squadron Leader John Edwin Ashley Williams, DFC". Australian War Memorial. Retrieved 6 February 2008.
 ^ Barnes 2000, pp. 251–252.
 ^ Eather 1995, pp. 103–104.
 ^ "No 450 Squadron". RAAF Museum Point Cook. Archived from the original on 1 October 2009. Retrieved 13 October 2013.
 ^ a b c d Barnes 2000, p. 252.
 ^ a b c d Eather 1995, p. 104.
 ^ Brown 1983, pp. 293–294, 299–300.
 ^ Turner 1999, p. 88.
 ^ RAAF Historical Section 1995, p. 109.
 ^ Wilson 2005, pp. 100–101.
 ^ Herington 1954, p. 578.
 ^ a b c d e Barnes 2000, p. 253.
 ^ Herington 1963, p. 70.
 ^ a b Edlington, David (8 April 2004). "The Great Crime: Aussies Among Murder Victims". Air Force News. 46 (5). Archived from the original on 20 November 2007. Retrieved 6 February 2008.
 ^ "No. 34501". The London Gazette. 12 April 1938. p. 2458.
 ^ "Kierath, Reginald Victor". World War II Nominal Roll. Commonwealth of Australia. Retrieved 6 February 2008.
 ^ Franks 2003, p. 108.
 ^ Herington 1963, p. 111.
 ^ RAAF Historical Section 1995, p. 110.
 ^ Herington 1963, pp. 344–345.
 ^ Herington 1963, p. 345.
 ^ Herington 1963, pp. 346–349.
 ^ Herington 1963, p. 353.
 ^ Herington 1963, p. 350.
 ^ RAAF Historical Section 1995, pp. 110–111.
 ^ a b c RAAF Historical Section 1995, p. 111.
 ^ "450 Tactical Helicopter Squadron". Royal Canadian Air Force. Archived from the original on 5 September 2014. Retrieved 8 December 2014.
 ^ "450 Tactical Helicopter Squadron reborn". Royal Canadian Air Force. 18 May 2012. Archived from the original on 4 March 2016. Retrieved 8 December 2014.
 ^ "First commanding officer – now 450's patron". The Harasser: 1. November 2009. Archived from the original on 15 August 2011. Retrieved 5 September 2013.
 ^ "Tributes and celebrations: Gordon Steege". The Sydney Morning Herald. 4 September 2013. Retrieved 24 November 2014.
 ^ a b Rawlings 1978, p. 442.
 ^ Halley 1988, p. 474.
 ^ a b Jefford 2001, p. 94.
 ^ Halley 1988, pp. 473–474.
 ^ "Item MEB0284". Collection. Australian War Memorial. Retrieved 26 July 2015.
 ^ Barnes 2000, p. 264.
 ^ RAAF Historical Section 1995, pp. 107, 111.
 Barnes, Norman (2000). The RAAF and the Flying Squadrons. St Leonards, New South Wales: Allen & Unwin. ISBN 1-86508-130-2. Brown, Russell (1983). Desert Warriors: Australian P-40 Pilots at War in the Middle East and North Africa, 1941–1943. Maryborough, Queensland: Banner Books. ISBN 1-875593-22-5. Eather, Steve (1995). Flying Squadrons of the Australian Defence Force. Weston Creek, Australian Capital Territory: Aerospace Publications. ISBN 1-875671-15-3. Flintham, Vic; Thomas, Andrew (2003). Combat Codes: A Full Explanation and Listing of British, Commonwealth and Allied Air Force Unit Codes Since 1938. Shrewsbury, UK: Airlife Publishing. ISBN 1-84037-281-8. Franks, Norman (2003). Beyond Courage: Air Sea Rescue by Walrus Squadrons in the Adriatic, Mediterranean and Tyrrhenian Seas 1942–1945. London: Grub Street. ISBN 9781904010302. Gillison, Douglas (1962). Royal Australian Air Force, 1939–1942. Australia in the War of 1939–1945. Series 3 – Air. I (1st ed.). Canberra: Australian War Memorial. OCLC 2000369. Halley, James J. (1988). The Squadrons of the Royal Air Force & Commonwealth 1918–1988. Tonbridge, UK: Air Britain (Historians). ISBN 0-85130-164-9. Herington, John (1954). Air War Against Germany and Italy, 1939–1943. Australia in the War of 1939–1945. Series 3 – Air. III (1st ed.). Canberra: Australian War Memorial. OCLC 3633363. Herington, John (1963). Air Power Over Europe, 1944–1945. Australia in the War of 1939–1945. Series 3 – Air. IV (1st ed.). Canberra: Australian War Memorial. OCLC 3633419. Jefford, C.G. (2001) [1988]. RAF Squadrons: A Comprehensive Record of the Movement and Equipment of all RAF Squadrons and Their Antecedents Since 1912 (2nd ed.). Shrewsbury, UK: Airlife Publishing. ISBN 1-85310-053-6. RAAF Historical Section (1995). Units of the Royal Australian Air Force: A Concise History. Volume 2: Fighter Units. Canberra: Australian Government Publishing Service. ISBN 9780644427944. Rawlings, John D. R. (1978) [1976]. Fighter Squadrons of the RAF and Their Aircraft (2nd ed.). London: Macdonald & Jane's. ISBN 0-354-01028-X. Shores, Christopher; Ring, Hans (1969). Fighters Over the Desert: The Air Battles in the Western Desert June 1940 to December 1942. London: Neville Spearman. OCLC 164897156. Shores, Christopher; Williams, Clive (1994) [1966]. Aces High: A Tribute to the Most Notable Fighter Pilots of the British and Commonwealth Air Forces in World War II. London: Grub Street. ISBN 1-898697-00-0. Thomas, Andrew (2005). Tomahawk and Kittyhawk Aces of the RAF and Commonwealth. Oxford: Osprey. ISBN 978-1-84176-083-4. Turner, Jim (1999). The RAAF at War: World War II, Korea, Malaya & Vietnam. East Roseville, New South Wales: Kangaroo Press. ISBN 0-86417-889-1. Wilson, David (2005). Brotherhood of Airmen: The Men and Women of the RAAF in Action. Crows Nest, New South Wales: Allen & Unwin. ISBN 1-74114-333-0. Barton, Leonard L. (1991). The Desert Harassers: Being Memoirs of 450 (RAAF) Squadron 1941–1945. Mosman, New South Wales: Astor. ISBN 9780646034829. James, George A. (ed.) (1996). OK: Recollections of the Desert Harassers. Illawong, New South Wales: 450 Squadron (RAAF) Association. ISBN 9780646278636.CS1 maint: extra text: authors list (link) Officer, George John 'Gus' (ed.) (2008). Six O'Clock Diamond:The Story of a Desert Harasser. Victoria: David and John Officer. ISBN 9780646502502.CS1 maint: extra text: authors list (link) Williams, Louise (ed.) (2015). A True Story of the Great Escape. Crows Nest, New South Wales: Allen & Unwin. ISBN 9781743313893.CS1 maint: extra text: authors list (link) Nipperess, Sandra G. (ed.) (2017). OK: Recollections of the Desert Harassers, Edition 2. Cessnock, New South Wales: 450 Squadron RAAF Association Inc. ISBN 9780648087205.CS1 maint: extra text: authors list (link) v t e 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 40 41 42 43 60 66 67 71 73 75 76 77 78 79 80 82 83 84 85 86 87 92 93 94 99 100 102 107 292 Fighter Rescue and Communication Seaplane Berlin Air Lift 450 451 452 453 454 455 456 457 458 459 460 461 462 463 464 466 467 No. 18 (NEI) No. 19 (NEI) No. 119 (NEI) No. 120 (NEI) Australian Article XV squadrons of World War II Military units and formations established in 1941 Military units and formations disestablished in 1945 Military units and formations in Mandatory Palestine in World War II Articles with short description Featured articles Use Australian English from May 2015 All Wikipedia articles written in Australian English Use dmy dates from November 2014 Pages using deprecated image syntax CS1: long volume value Commons category link is on Wikidata CS1 maint: extra text: authors list Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Português  This page was last edited on 25 October 2019, at 05:42 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  DJ OK No. 450 Squadron a b c a b a b c d e f ^ ^ ^ ^ ^ ^ ^ a b c ^ a b c ^ a b c d e a b ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ a b c d a b c d ^ ^ ^ ^ ^ a b c d e ^ a b 46 ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b c ^ ^ ^ ^ a b ^ a b ^ ^ ^ ^ I III IV No. 450 Squadron RAAF Official crest of No. 450 Squadron 1941–1945 Australia Royal Australian Air Force Fighter, fighter-bomber Desert Air Force "The Desert Harassers"[1] Harass[1][2] World War II Syria, 1941South-East Europe, 1942–1945Egypt & Libya, 1940–1943El AlameinEl HammaNorth Africa, 1942–1943Sicily, 1943Italy, 1943–1945Gustav LineGothic Line The Desert Harassers Gordon Steege (1941–1942)John Williams (1942) A jaguar's head couped, pierced by a rapier in hand[1][2] DJ (December 1941 – April 1942)[4] OK (April 1942 – August 1945)[5] Hawker HurricaneCurtiss P-40 KittyhawkNorth American P-51 Mustang May 1941 December 1941 Hawker Hurricane Mk.I
 December 1941 September 1942 Curtiss P-40 Kittyhawk Mk.I
 1942 September 1942 Curtiss P-40 Kittyhawk Mk.Ia
 September 1942 October 1943 Curtiss P-40 Kittyhawk Mk.III
 October 1943 August 1945 Curtiss P-40 Kittyhawk Mk.IV
 May 1945 August 1945 North American P-51 Mustang Mk.III
 16 February 1941 9 April 1941 RAAF Station Williamtown, New South Wales 
 9 April 1941 12 May 1941 en route to Middle East 
 12 May 1941 23 June 1941 RAF Abu Sueir, Egypt 
 23 June 1941 29 June 1941 RAF Aqir, Palestine 
 29 June 1941 11 July 1941 RAF Amman, Jordan 
 11 July 1941 18 July 1941 Damascus, Syria 
 18 July 1941 4 August 1941 RAF Haifa, Palestine 
 4 August 1941 19 August 1941 RAF El Bassa, Palestine 
 19 August 1941 25 October 1941 Rayak airfield, Lebanon 
 25 October 1941 12 December 1941 RAF Burg El Arab, Egypt 
 12 December 1941 30 January 1942 LG.207/LG 'Y' (Qassassin), Egypt 
 30 January 1942 16 February 1942 LG.12 (Sidi Haneish North), Egypt 
 16 February 1942 22 February 1942 LG.139 (Gambut Main), Libya Det. at RAF El Adem, Libya
 22 February 1942 9 March 1942 LG.142/143 (Gambut Satellite), Libya 
 9 March 1942 16 April 1942 LG.139 (Gambut Main), Libya 
 16 April 1942 17 June 1942 LG.142/143 (Gambut Satellite), Libya 
 17 June 1942 18 June 1942 LG.148/Sidi Azeiz Airfield, Libya 
 18 June 1942 24 June 1942 LG.75, Egypt 
 24 June 1942 27 June 1942 LG.102, Egypt 
 27 June 1942 30 June 1942 LG.106, Egypt 
 30 June 1942 2 October 1942 LG.91, Egypt 
 2 October 1942 14 October 1942 LG.224/Cairo West, Egypt 
 14 October 1942 6 November 1942 LG.175, Egypt 
 6 November 1942 9 November 1942 LG.106, Egypt 
 9 November 1942 11 November 1942 LG.101, Egypt 
 11 November 1942 14 November 1942 LG.76, Egypt 
 14 November 1942 15 November 1942 LG.139 (Gambut 1), Libya 
 15 November 1942 19 November 1942 Gazala Airfield, Libya 
 19 November 1942 8 December 1942 Martuba Airfield, Libya Det. at Antelat Airfield, Libya
 8 December 1942 18 December 1942 Belandah Airfield, Libya 
 18 December 1942 1 January 1943 Marble Arch Airfield, Libya 
 1 January 1943 9 January 1943 Alem el Chel Airfield,  Libya 
 9 January 1943 18 January 1943 Hamraiet 3 Airfield, Libya 
 18 January 1943 24 January 1943 Sedadah Airfield, Libya 
 24 January 1943 14 February 1943 RAF Castel Benito, Libya 
 14 February 1943 8 March 1943 El Assa Airfield, Libya Det. at Ben Gardane Airfield, Tunisia
 8 March 1943 21 March 1943 Nefatia Airfield, Tunisia 
 21 March 1943 6 April 1943 Medenine Airfield, Tunisia 
 6 April 1943 14 April 1943 El Hamma Airfield, Tunisia 
 14 April 1943 18 April 1943 El Djem Airfield, Tunisia 
 18 April 1943 18 May 1943 Alem East Airfield, Tunisia 
 18 May 1943 13 July 1943 Zuwara Airfield, Libya 
 13 July 1943 18 July 1943 RAF Luqa, Malta 
 18 July 1943 2 August 1943 Pachino Airfield, Sicily, Italy 
 2 August 1943 16 September 1943 Agnone Airfield, Sicily, Italy 
 16 September 1943 23 September 1943 Grottaglie Airfield, Italy 
 23 September 1943 3 October 1943 Bari Airfield, Italy 
 3 October 1943 27 October 1943 Foggia Main Airfield, Italy 
 27 October 1943 28 December 1943 Mileni Airfield, Italy 
 28 December 1943 22 May 1944 Cutella Airfield, Italy 
 22 May 1944 12 June 1944 San Angelo Airfield, Italy 
 12 June 1944 23 June 1944 Guidonia Airfield, Italy 
 23 June 1944 9 July 1944 Falerium Airfield, Italy 
 9 July 1944 28 August 1944 Creti Airfield, Italy 
 28 August 1944 11 September 1944 Iesi Airfield, Italy 
 11 September 1944 20 September 1944 Foiano Airfield, Italy 
 20 September 1944 17 November 1944 Iesi Airfield, Italy 
 17 November 1944 25 February 1945 Fano Airfield, Italy 
 25 February 1945 19 May 1945 Cervia Airfield, Italy 
 19 May 1945 20 August 1945 Lavariano, Italy 
 25 March 1941 Flight Lieutenant Bruce McRae Shepherd (temp)
 31 May 1941 Squadron Leader Gordon Henry Steege
 7 May 1942 Squadron Leader Alan Douglas Ferguson
 18 October 1942 Squadron Leader John Edwin Ashley Williams
 2 November 1942 Squadron Leader M.H.C. Barber
 16 March 1943 Squadron Leader John Phillip Bartle
 6 November 1943 Squadron Leader Sydney George Welshman
 6 December 1943 Squadron Leader Kenneth Royce Sands
 7 April 1944 Squadron Leader Ray Trevor Hudson
 15 June 1944 Squadron Leader John Dennis Gleeson
 25 October 1944 Squadron Leader Jack Carlisle Doyle
  Wikimedia Commons has media related to No. 450 Squadron RAAF. 
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
40
41
42
43
60
66
67
71
73
75
76
77
78
79
80
82
83
84
85
86
87
92
93
94
99
100
102
107
292
Fighter
Rescue and Communication
Seaplane
Berlin Air Lift
 
450
451
452
453
454
455
456
457
458
459
460
461
462
463
464
466
467
 
No. 18 (NEI)
No. 19 (NEI)
No. 119 (NEI)
No. 120 (NEI)
 