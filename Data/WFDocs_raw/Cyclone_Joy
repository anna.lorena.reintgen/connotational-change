
 Severe Tropical Cyclone Joy struck Australia in late 1990, causing the third highest floods on record in Rockhampton, Queensland. This cyclone began as a weak tropical low near the Solomon Islands, and initially moved westward. On 18 December, it was named Joy, becoming the 2nd named storm of the 1990–91 Australian region cyclone season.  After turning southwest, Joy developed a well-defined eye and strengthened to  maximum sustained winds of 165 km/h (103 mph) while approaching Cairns in Far North Queensland. Brushing the city with strong winds, the cyclone soon weakened and turned southeast. Joy later curved back southwest, making landfall near Townsville, Queensland on 26 December. It dissipated the next day; remnant moisture continued as torrential rainfall over Queensland for two weeks.
 While drifting offshore northeastern Australia, the cyclone produced wind gusts as high as 124 km/h (77 mph) in Cairns, strong enough to cause power outages. In Mackay, a tornado spawned by Joy damaged 40 homes, while torrential rainfall just south of the city peaked at over 2 metres (6.6 ft). Most storm-associated damage was wrought by severe flooding, which persisted for weeks in  hardest-hit locations. Rains significantly increased water levels on 10 rivers, among them the Fitzroy River, which discharged about 18 trillion litres (4.8×1012 US gallons) of freshwater into Keppel Bay over 25 days. In turn, the Great Barrier Reef suffered biological damage from coral bleaching and decreased salinity. The Fitzroy River rose to a 9.30-metre (30.5 ft) peak at Rockhampton, forcing thousands to evacuate homes; some stranded individuals could only obtain food by helicopter. Elsewhere in Australia, storm moisture alleviated drought conditions and diminished fires near Sydney. Overall, Joy killed six people and caused A$300 million in damage ($234 million USD).[nb 1] Afterwards, the Queensland government issued a disaster declaration for about 30% of the state, and the name Joy was  retired from the list of tropical cyclone names.
 In mid-December, a monsoon trough persisted along the west Pacific Ocean, spawning a pair of tropical disturbances both north and south of the equator. In the northwestern Pacific Ocean, the system became Typhoon Russ.[1] On 15 December, a tropical low formed east of the Solomon Islands. During the next few days, the system passed south of the country while slowly organizing.[2] On 18 December, the Australia Bureau of Meteorology began tracking it, naming the system "Joy" after it upgraded the low to tropical cyclone status.[2][3] A meteorologist from Darwin later apologised that the name was used so close to Christmas, although "Joy" was predetermined by a rotating list of list of tropical cyclone names.[4] Also on 18 December, the Joint Typhoon Warning Center (JTWC)[nb 2] began issuing warnings on the storm, labeling it as Tropical Cyclone 06P.[5] With a ridge to the south, Joy continued generally west-southwestward.[1]
 While in its origins, Joy was experiencing upper-level wind shear, but as it approached the jet stream while turning to the southwest, conditions became more favourable for intensification. The storm quickly intensified,[2] reaching the equivalent of a minimal hurricane on 21 December.[3] At 00:00 UTC on 23 December, the Bureau estimated Joy reached peak 10-minute sustained winds of 165 km/h (103 mph), which made the system a category 4 severe tropical cyclone on the Australian tropical cyclone intensity scale.[2] Around the same time, the JTWC also estimated the same peak winds, but sustained over one minute.[3] Joy developed an eye about 50 km (31 mi) in diameter with concentric eyewalls.[6] The storm began moving slowly off the northeast coast of Australia, passing within 100 km (62 mi) of Cairns, and the motion shifted to a southeast drift.[2] A building high pressure area to the south caused the change in movement, and there were initial concerns the storm would loop to the west and affect Cairns again.[7] Drier air caused Joy to weaken gradually from its peak to the equivalent of a strong tropical storm. At 06:00 UTC on 26 December, after turning back to the southwest, the storm made landfall near Townsville, Queensland,[2] with winds estimated at 95 km/h (59 mph).[3] That day, the JTWC discontinued advisories,[5] and on 27 December, Joy dissipated inland over Queensland.[3] A remnant system persisted into early January, producing continued rainfall across Queensland.[1]
 Before Joy struck Australia, residents evacuated from resorts on Fitzroy and Green islands by boat or plane. Officials set up evacuation centres on the mainland and put the Australian Army on standby.[8] The military evacuated its fleet of Blackhawk helicopters inland from RAAF Base Townsville.[9] A man required rescue from Hope Island by helicopter in advance of the storm.[10] The threat of the storm caused shopping malls and the airport near Cairns to close just before Christmas.[11] Several flights were diverted or delayed, stranding about 1,000 travellers,[12] many of whom spent Christmas in the airport.[13] Road travel was banned in some areas of northeastern Queensland, and residents in Port Douglas were forced to evacuate.[14] The Flood Warning Centre in Brisbane issued 192 flood warnings related to Cyclone Joy in December and January, beginning on 23 December. Most of the warnings were related to increased water levels along rivers.[1]
 Joy passed within 80 km (50 mi) of Green Island off the coast of Queensland, generating a wind gust of 180 km/h (110 mph). Heavy damage occurred on other islands,[2] and one person drowned while surfing in Mackay.[6] A boat became disabled during the storm, forcing its four occupants to ride out the storm for four days on Cockermouth Island until they were rescued by helicopter.[15] Several boats were damaged in the Whitsunday Islands.[6]
 The storm and its remnants dropped heavy rainfall throughout Queensland for about two weeks,[2] totaling over 2 m (6.6 ft) south of Mackay and over 1 m (3.3 ft) between Bowen and St. Lawrence. The highest daily total was 458 mm (18.0 in) about 30 km (19 mi) west of Sarina.[1] Three day rainfall totals around when Joy made landfall included 831 mm (32.7 in) in Blue Mountain and 506 mm (19.9 in) in Waitara.[16] Rainfall continued through the region through March 1991, resulting in the third largest flood in the region in over 100 years.[17]
 Overall, Cyclone Joy killed six people, including five in river flooding, and caused about A$300 million in damage ($234 million USD).[nb 3][1] While stalling off the northeast Australia coast, Joy produced widespread gale force winds,[8] with gusts to 124 km/h (77 mph) recorded at Cairns.[6] After the winds knocked over trees, causing power and phone outages, storm damage cut the water supply and briefly isolated Cairns due to debris blocking roads.[8] An outer rainband struck Mackay as the storm moved ashore, spawning a tornado that damaged 40 houses, destroyed two others, and damaged a caravan park.[2] The windstorm was unexpected there, and damage in Mackay was estimated at A$10 million.[20] Flooding from rainfall affected about 90% of the city, which restricted train travel,[21] causing three trains to be canceled and stranding hundreds of travelers.[22] In Port Douglas, Joy produced an inconsequential storm surge of 0.5 m (1.6 ft).[23] In Innisfail, the cyclone ruined 90% of the town's crops,[24] and over 20 houses sustained wind damage.[25] Banana farmers in the Cairns region lost a combined total of 1.2 million bunches of the fruit. Collectively, crop damage totalled over A$70 million, mostly to sugar cane and banana.[13][26] About 30,000 head of livestock were killed in the region.[27]
 Rain from Cyclone Joy caused rampant flooding across the region,[2] significantly raising water levels along 10 rivers.[28] The Fitzroy River alone swelled to inundate about 4,000 km2 (1,500 sq mi) of terrain. Heightened discharge caused extensive erosion along river channels that removed about 18 million tonnes (20,000,000 short tons) of soil and vegetation.[29] In late December, the Pioneer River at Mackay peaked at 7.6 m (25 ft), safely within the confines of its levee system.[1] The town of Giru endured flooding of streets and houses, which would reoccur several times through February.[1][16] High water levels along the Tully River flooded a portion of the Bruce Highway. In the second week of January, the Herbert River peaked at 11.32 m (37.1 ft), causing residential flooding in Ingham.[1] Elsewhere in Australia, moisture from the storm eased ongoing bushfires near Sydney.[30]
 At Rockhampton, the Fitzroy River rose to an initial peak of 9.15 m (30.0 ft), temporarily dropped, and rose to a final peak of 9.30 m (30.5 ft) in early January 1991, the third highest since records began in 1860,[16] after floods in 1918 and 1954.[29] Inflow from several tributaries ensured the Fitzroy River near Rockhampton remained over 8 m (26 ft) for 13 days. The river entered 350 houses in what was the city's most damaging flood since 1954.[1][16] The town was isolated for about three weeks after flooding covered roads, railways, and the airport.[1][16][31] Before the worst of the flooding, Acting Premier Tom Burns declared a state of disaster for Rockhampton, giving local police the authority to force individuals living in flood zones to leave their homes.[32] Ultimately, over 1,000 people sought higher ground,[33] staying mainly at the houses of friends or relatives, or at nearby schools.[34] In an attempt to mitigate damage, 150 volunteers filled 43,000 sandbags to protect properties. Nearby, residents rescued about 100 dogs from an affected kennel.[35]
 Storm-related flooding damaged portions of the Great Barrier Reef through coral bleaching.[17] Over 25 days, the Fitzroy River discharged about 18 trillion litres (4.8×1012 US gallons) of water into Keppel Bay,[29] reaching the Great Barrier Reef in early January and causing a drop in salinity levels. On Great Keppel Island, about 85% of shallow-water reefs died. Reef damage also occurred in the Whitsunday Islands.[17] The discharge from the Fitzroy River affected various islands and coral groups, depending on the offshore wind direction and ocean current. The water flow washed a group of freshwater turtles from the mainland to North West Island, though they were later returned to their native habitat. The influx of freshwater reduced commercial fishing productivity by 30%.[29]
 Workers in Cairns had restored power and water supplies within a few days of Joy's passage.[7] After the initial flooding from Joy ended, there was additional rainfall in February 1991 that caused flooding across the region.[36] Residual flooding cost the coal industry about A$60 million due to loss of production and hindered exporting.[37] While Rockhampton was still isolated by flooding, a helicopter airdropped food to hundreds of stranded families,[38] after the town experienced food and water shortages.[39] Transportation to Rockhampton was not normalised until 20 January, when the airport and incoming roads were reopened.[27] The city of Rockhampton later created a flood plain management policy as a result of the effects from Joy.[40] The floods helped fill the drainage basin of the Peter Faust Dam within a few weeks, speeding up a process which would have otherwise taken several years.[41] Months after the storm, the Queensland National Parks and Wildlife Service collected reports on environmental impacts from the flooding; they included some beneficial aspects such as new locations for animal breeding,[29] and alleviation of drought conditions.[42] The Queensland government upgraded a portion of the Bruce Highway near Rockhampton to reduce flooding in similar storms.[31]
 Officials declared about a third of the state of Queensland as a disaster area following Cyclone Joy,[43] directing state funds toward recovery efforts.[13] The Queensland government provided monetary assistance to eligible families in the disaster zone and offered special loans to farmers affected by the storm. The federal government agreed to contribute 75% of overall relief costs.[44] Following the cyclone's impacts in Australia, the name Joy was later retired by the World Meteorological Organization.[45]
 1 Meteorological history 2 Preparations 3 Impact 4 Aftermath 5 See also 6 Notes 7 References 8 External links Tropical cyclones portal Book: Cyclone Joy and Places It Affected List of retired Australian cyclone names Cyclone Justin—brought severe flooding to Queensland and Papua New Guinea Cyclone Aivu (1989)—caused considerable damage in areas near Ayr, Queensland Cyclone Ita (2014)—followed a similar path to Joy, and had a similar satellite presentation at peak intensity ^ All damage totals are listed in 1990 values of their respective currencies.
 ^ The Joint Typhoon Warning Center is a joint United States Navy – United States Air Force task force that issues tropical cyclone warnings for the western Pacific Ocean and other regions.[5]
 ^ The damage total was originally reported in 1990 Australian dollars.[18] The exchange rate in 1990 was 1.281 Australian dollars to 1 United States dollar.[19]
 ^ a b c d e f g h i j k Darwin Regional Specialised Meteorological Centre (December 1990). "Darwin Tropical Diagnostic Statement" (PDF). 9 (1). Australian Bureau of Meteorology: 2. ISSN 1321-4233. Retrieved 2013-11-14. Cite journal requires |journal= (help).mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c d e f g h i j Anthony J. Bannister; K. J. Smith (1993-12-04). "The South Pacific and Southeast Indian Ocean Tropical Cyclone Season 1990–1991" (PDF). Australian Meteorological Magazine. Australian Bureau of Meteorology. 42 (4): 179. Retrieved 2013-11-14.
 ^ a b c d e Kenneth R. Knapp; Michael C. Kruk; David H. Levinson; Howard J. Diamond; Charles J. Neumann (2010). 1991 Joy (1990350S11165). The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data (Report). Bulletin of the American Meteorological Society. Retrieved 2013-11-14.
 ^ "Shamed by Joy". Herald Sun. 1990-12-24. – via Lexis Nexis (subscription required)
 ^ a b c Joint Typhoon Warning Center (1992). 1991 Annual Tropical Cyclone Report (PDF) (Report). United States Navy. Retrieved 2013-11-14.
 ^ a b c d Jeff Callaghan (2011-08-05). East Coast Impacts of Tropical Cyclones 1858–2008 (PDF) (Report). Green Cross International. Retrieved 2013-11-15.
 ^ a b "Joy Danger Holds – Fear of winds doubling back". Herald Sun. 1990-12-26. – via Lexis Nexis (subscription required)
 ^ a b c Cathy Johnson (1990-12-24). "Cyclone Joy Heads Towards Cairns". Sydney Morning Herald. Archived from the original on 2013-12-28. Retrieved 2013-11-15.
 ^ "Cyclone lurks off Qld coast And bushfires threaten to flare; in NSW". The Advertiser. Australian Associated Press. 1990-12-27. – via Lexis Nexis (subscription required)
 ^ "Qld braces for cyclone Joy". The Advertiser. Australian Associated Press. 1990-12-22. – via Lexis Nexis (subscription required)
 ^ "North Braced for the Floods of Joy". Sunday Mail. 1990-12-23. – via Lexis Nexis (subscription required)
 ^ "No Joy for Travellers as Cyclone Approaches". Courier Mail. 1990-12-24. – via Lexis Nexis (subscription required)
 ^ a b c "Australian Disaster Zone Declared as Cyclone Weakens". Associated Press. 1990-12-25. – via Lexis Nexis (subscription required)
 ^ Paul Whittaker (1990-12-24). "Qld in fear as cyclone hovers". The Advertiser. – via Lexis Nexis (subscription required)
 ^ "Cyclone fishermen found – 'We're lucky to be alive'". Herald Sun. 1990-12-31. – via Lexis Nexis (subscription required)
 ^ a b c d e Queensland Flood Summary 1990 – 1999 (Report). Australian Bureau of Meteorology. November 2010. Archived from the original on 2013-11-20. Retrieved 2013-11-16.
 ^ a b c R. Van Woesikl; L.M. De Vantier; J.S. Glazebrook (1995-11-23). "Effects of Cyclone 'Joy' on nearshore coral communities of the Great Barrier Reef" (PDF). Marine Ecology Progress Series. 128. Bibcode:1995MEPS..128..261V. doi:10.3354/meps128261. Retrieved 2013-11-15.
 ^ Floods Associated With Severe Tropical Cyclone Joy (PDF) (Report). Australian Bureau of Meteorology. Retrieved 2013-11-18.
 ^ Lawrence H. Officer (2014). "Exchange Rates Between the United States Dollar and Forty-one Currencies". MeasuringWorth. Retrieved 2014-03-09.
 ^ "Mackay Cyclone Damage is $10M". Courier Mail. 1990-12-29. – via Lexis Nexis (subscription required)
 ^ "Soaked City Braces for Increased Floods". Sunday Mail. 1990-12-29. – via Lexis Nexis (subscription required)
 ^ "Four fisherman found safe as floods maroon Mackay". The Canberra Times. 1990-12-31. p. 1. Retrieved 2014-03-02.
 ^ Cairns Cyclone History (PDF) (Report). Geoscience Australia. p. 68. Archived from the original (PDF) on 2009-10-19. Retrieved 2013-11-29.
 ^ "Cyclone Crushes and Creates Crops". Courier Mail. 1991-01-02. – via Lexis Nexis (subscription required)
 ^ Neale Prior (1990-12-26). "Joy's Damage Bill Will Run to Millions, But the Worst Is Over". Sydney Morning Herald. – via Lexis Nexis (subscription required)
 ^ "Joy's crop damage may hit $70m". Hobart Mercury. Australian Associated Press. 1991-01-09. – via Lexis Nexis (subscription required)
 ^ a b "Emergency food for flood towns". Sunday Mail. Australian Associated Press. 1991-01-20. – via Lexis Nexis (subscription required)
 ^ Flood – Gulf Country 23 December 1990 (Report). Australian Emergency Management. Archived from the original on 10 December 2013. Retrieved 2013-11-28.
 ^ a b c d e G.T. Byron (1991-09-27). Workshop on the Impacts of Flooding (PDF) (Report). Queensland Department of Environment and Heritage. Retrieved 2013-11-28.
 ^ Cathy Johnson (1991-01-02). "Extreme Fire Alert for 95% of State". Sydney Morning Herald. – via Lexis Nexis (subscription required)
 ^ a b Fitzroy River Floodplain and Road Planning Study (PDF) (Report). Queensland Department of Main Roads. 2009-07-02. Archived from the original (PDF) on December 30, 2011. Retrieved 2013-11-29.
 ^ John Lehmann (1991-01-03). "Evict Powers to Police in Disaster Area". Courier Mail. – via Lexis Nexis (subscription required)
 ^ Mark Riley (1991-01-05). "1,000 Flee Floods, Worse to Come". Sydney Morning Herald. – via Lexis Nexis (subscription required)
 ^ "Rockhampton as ready as it is going to be". The Canberra Times. 1991-01-06. p. 2. Retrieved 2014-03-02.
 ^ "Floods isolate Rockhampton Evacuations continue as river rises". The Advertiser. Australian Associated Press. 1991-01-07. – via Lexis Nexis (subscription required)
 ^ "Man swept away in Qld flooding". The Canberra Times. 1991-02-04. p. 3. Retrieved 2014-03-02.
 ^ "Coal Output Halved". Courier Mail. 1991-01-10. – via Lexis Nexis (subscription required)
 ^ "Food Drops to Stranded Hundreds". Courier Mail. 1990-12-31. – via Lexis Nexis (subscription required)
 ^ Mike Seccomb (1991-01-10). "Cost of Joy: $70M and Rising as Rain Keeps Falling". Sydney Morning Herald. – via Lexis Nexis (subscription required)
 ^ "Planning Scheme for the City of Rockhampton". Rockhampton City Plan (PDF) (Report). Rockingham City Council. 2005-08-30. Archived from the original (PDF) on 2012-04-26. Retrieved 2013-11-29.
 ^ "Cyclone Joy not such an ill wind..." The Canberra Times. 1991-03-20. p. 22. Retrieved 2014-03-02.
 ^ Cathy Johnson (1991-01-01). "Joy Spells Disaster for Rockhampton". Sydney Morning Herald. – via Lexis Nexis (subscription required)
 ^ "Third of Qld disaster area". Herald Sun. 1991-01-17.
 ^ Mark Riley; Peter Hartcher (1991-01-09). "Aid Pledged as Looters Strike in Flood Areas". Sydney Morning Herald. – via Lexis Nexis (subscription required)
 ^ RA V Tropical Cyclone Committee (October 11, 2018). Tropical Cyclone Operational Plan for the South-East Indian Ocean and the Southern Pacific Ocean 2018 (PDF) (Report). World Meteorological Organization. pp. I-4–II-9 (9–21). Archived from the original on October 12, 2018. Retrieved October 12, 2018.
 Joint Typhoon Warning Centre (JTWC). Australian Bureau of Meteorology (TCWC's Perth, Darwin & Brisbane). v t e  Book  Category  Portal v t e Flora Dinah Ada Dora Fiona/Gertie Althea Daisy Emily Madge Wanda Tracy Trixie Joan David Beth Ted Alby Kerry Simon Cliff Elinor Kathy Lance Sandy Margot Nigel Winifred Connie Jason Elsie Charlie Herbie Ilona Delilah Ned Orson Pedro Felicity Tina Ivor Joy Mark Ian Nina Oliver Polly Roger Naomi Pearl Sharon Annette Bobby Violet Warren Agnes Gertie Barry Celeste Ethel Kirsty Olivia Fergus Rachel Justin Rhonda Katrina Sid Thelma Elaine Gwenda John Rona Vance Steve Tessi Rosita Sam Abigail Chris Erica Inigo Monty Fay Harvey Ingrid Clare Larry Glenda Monica George Guba Helen Hamish Laurence Magda Carlos Heidi Jasmine Lua Oswald Rusty Ita Lam Marcia Debbie Marcus  Book  Category  Portal  WikiProject v t e Sally (1971) Althea (1971) Emily (1972) Gail (1972) Kerry (1973) Madge (1973) Tracy (1974) Vanessa (1976) Watorea (1976) Ted (1976) Leo (1977) Trudy (1978) Winnie (1978) Kerry (1979) Hazel (1979) Idylle (1979) Brian (1980) Fred (1980) Simon (1980) Doris-Gloria (1980) Alice-Adelaide (1980) Felix (1980) Neil (1981) Olga (1981) Chris-Damia (1982) Bernie (1982) Jane (1983) Quenton (1983) Bobby (1984) Chloe (1984) Daryl (1984) Odette (1985) Sandy (1985) Victor (1986) Billy-Lila (1986) Elsie (1987) Gwenda-Ezenina (1988) Ned (1989) Ivor (1990) Joy (1990) Errol (1991) Marian (1991) Harriet-Heather (1992) Ian (1992) Esau (1992) Jane-Irna (1992) Oliver (1993) Sharon (1994) Annette (1994) Bobby (1995) Agnes (1995) Frank (1995) Barry (1996) Kristy (1996) Olivia (1996) Drena (1997) Rhonda (1997) Katrina (1998) Tiffany (1998) Elaine (1999) Norman (2000) Fiona (2003) Monty (2004) Oscar-Itseng (2004) Bertie-Alvin (2005) Larry (2006) Floyd (2006) Pancho (2008) Billy (2008) Ilsa (2009) Ului (2010) Zelia (2011) Bianca (2011) Narelle (2013) Rusty (2013) Christine (2013–14) Kate (2014) Lam (2015) Nathan (2015) Ikola (2015) Quang (2015) Debbie (2017) Savannah (2019) Trevor (2019) Veronica (2019)  Category  Tropical cyclones portal 1990 in Australia Retired Australian region cyclones 1990–91 South Pacific cyclone season 1990–91 Australian region cyclone season Category 4 Australian region cyclones South Pacific tropical depressions CS1 errors: missing periodical Subscription required using via Pages containing links to subscription-only content Featured articles Commons category link from Wikidata Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Français 中文  This page was last edited on 17 September 2019, at 10:48 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  1990–91 South Pacific andthe Australian region cyclone seasons Severe Tropical Cyclone Joy ^ ^ ^ a b c d e f g h i j k 9 a b c d e f g h i j 42 a b c d e ^ a b c a b c d a b a b c ^ ^ ^ ^ a b c ^ ^ a b c d e a b c 128 ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ a b c d e ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ Cyclone Joy  Book  Category  Portal  Book  Category  Portal  WikiProject Category 3 tropical cyclone (SSHWS) Cyclone Joy approaching Australia 15 December 1990 (1990-12-15) 27 December 1990 (1990-12-27)   
 10-minute sustained: 165 km/h (105 mph) 1-minute sustained: 205 km/h (125 mph)  940 hPa (mbar); 27.76 inHg   
 6 direct $234 million (1990 USD) Queensland, Australia  Part of the 1990–91 South Pacific andthe Australian region cyclone seasons 
  
Book: Cyclone Joy and Places It Affected
  Wikimedia Commons has media related to Cyclone Joy.  

3Sina
1Unnamed
TDJoy
TD16P
2Lisa

 3 Sina 1 Unnamed TD Joy TD 16P 2 Lisa 
 Book
 Category
 Portal
 
Flora
Dinah
 
Ada
Dora
Fiona/Gertie
Althea
Daisy
Emily
Madge
Wanda
Tracy
Trixie
Joan
David
Beth
Ted
Alby
Kerry
 
Simon
Cliff
Elinor
Kathy
Lance
Sandy
Margot
Nigel
Winifred
Connie
Jason
Elsie
Charlie
Herbie
Ilona
Delilah
Ned
Orson
Pedro
Felicity
 
Tina
Ivor
Joy
Mark
Ian
Nina
Oliver
Polly
Roger
Naomi
Pearl
Sharon
Annette
Bobby
Violet
Warren
Agnes
Gertie
Barry
Celeste
Ethel
Kirsty
Olivia
Fergus
Rachel
Justin
Rhonda
Katrina
Sid
Thelma
Elaine
Gwenda
John
Rona
Vance
 
Steve
Tessi
Rosita
Sam
Abigail
Chris
Erica
Inigo
Monty
Fay
Harvey
Ingrid
Clare
Larry
Glenda
Monica
George
Guba
Helen
Hamish
Laurence
 
Magda
Carlos
Heidi
Jasmine
Lua
Oswald
Rusty
Ita
Lam
Marcia
Debbie
Marcus
 
 Book
 Category
 Portal
 WikiProject
 
Sally (1971)
Althea (1971)
Emily (1972)
Gail (1972)
Kerry (1973)
Madge (1973)
Tracy (1974)
Vanessa (1976)
Watorea (1976)
Ted (1976)
Leo (1977)
Trudy (1978)
Winnie (1978)
Kerry (1979)
Hazel (1979)
Idylle (1979)
 
Brian (1980)
Fred (1980)
Simon (1980)
Doris-Gloria (1980)
Alice-Adelaide (1980)
Felix (1980)
Neil (1981)
Olga (1981)
Chris-Damia (1982)
Bernie (1982)
Jane (1983)
Quenton (1983)
Bobby (1984)
Chloe (1984)
Daryl (1984)
Odette (1985)
Sandy (1985)
Victor (1986)
Billy-Lila (1986)
Elsie (1987)
Gwenda-Ezenina (1988)
Ned (1989)
 
Ivor (1990)
Joy (1990)
Errol (1991)
Marian (1991)
Harriet-Heather (1992)
Ian (1992)
Esau (1992)
Jane-Irna (1992)
Oliver (1993)
Sharon (1994)
Annette (1994)
Bobby (1995)
Agnes (1995)
Frank (1995)
Barry (1996)
Kristy (1996)
Olivia (1996)
Drena (1997)
Rhonda (1997)
Katrina (1998)
Tiffany (1998)
Elaine (1999)
 
Norman (2000)
Fiona (2003)
Monty (2004)
Oscar-Itseng (2004)
Bertie-Alvin (2005)
Larry (2006)
Floyd (2006)
Pancho (2008)
Billy (2008)
Ilsa (2009)
 
Ului (2010)
Zelia (2011)
Bianca (2011)
Narelle (2013)
Rusty (2013)
Christine (2013–14)
Kate (2014)
Lam (2015)
Nathan (2015)
Ikola (2015)
Quang (2015)
Debbie (2017)
Savannah (2019)
Trevor (2019)
Veronica (2019)
 
 Category
 Tropical cyclones portal
 