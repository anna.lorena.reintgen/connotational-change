Misterioso is a 1958 live album by American jazz ensemble the Thelonious Monk Quartet. By the time of its recording, pianist and bandleader Thelonious Monk had overcome an extended period of career difficulties and achieved stardom with his residency at New York's Five Spot Café, beginning in 1957. He returned there the following year for a second stint with his quartet, featuring drummer Roy Haynes, bassist Ahmed Abdul-Malik, and tenor saxophonist Johnny Griffin. Along with Thelonious in Action (1958), Misterioso captured portions of the ensemble's August 7 show at the venue.
 The album was one of the first successful live recordings of Monk's music and was produced by Orrin Keepnews of Riverside Records. According to Keepnews, the pianist played more distinctly here than on his studio albums in response to the audience's enthusiasm during the performance. Misterioso's title was meant to evoke Monk's reputation as an enigmatic, challenging performer, while its cover art was part of Riverside's attempt to capitalize on his popularity with intellectual and bohemian audiences; it appropriated Giorgio de Chirico's 1915 painting The Seer.
 Misterioso was originally met with a mixed critical reaction; reviewers applauded Monk's performance but were critical of Griffin, whose playing they felt was out of place with the quartet. The album was remastered and reissued in 1989 and 2012 by Original Jazz Classics, and has since received retrospective acclaim from critics, some of whom viewed Griffin's playing as the record's highlight. The saxophonist's solo during the performance of "In Walked Bud" in particular has developed renown among critics and jazz musicians.
 In 1951, Thelonious Monk was convicted of narcotics possession after refusing to betray his friend, pianist Bud Powell, to the police; a police search of the car belonging to Powell's female companion had discovered his glassine envelope of heroin laying beside Monk's feet. The conviction resulted in the suspension of Monk's cabaret card, the permit required by performers in New York for work in nightclubs.[1] Although the loss limited him professionally, he recorded several albums of original music and received positive press during the 1950s. Monk's manager, Harry Colomby, led an appeal on the pianist's behalf in front of the State Liquor Authority (SLA) to have his card restored. Colomby argued to the SLA that Monk was "a drug-free, law-abiding citizen, whose productivity and growing popularity as a recording artist demonstrates his standing as a responsible working musician".[2]
 In May 1957, the SLA said Monk needed to get a club owner to hire him first, prompting Colomby to consider the Five Spot Café in New York City's East Village. "I wanted to find a place that was small", he later said. "I once drove past this place in the Village and there was a bar and I heard music ... A place where poets hung out."[3] Joe Termini, who co-owned the venue with his brother Iggy, testified at Monk's police hearing, which resulted in the reinstatement of his cabaret card and his employment at the Five Spot Café.[3] In his first stable job in years, Monk helped transform the small bar into one of the city's most popular venues, as it attracted bohemians, hipsters, and devout fans of the pianist's music. With the residency, he had finally found jazz stardom after twenty years of career struggles and obscurity.[4]
 Monk began his first stint at the venue in July 1957, with saxophonist John Coltrane, bassist Ahmed Abdul-Malik, and drummer Shadow Wilson in his group.[5] However, by the time it ended in December, he had lost Wilson to poor health, while Coltrane left in pursuit of a solo career and a return to Miles Davis's group.[6] Monk returned to New York's club scene in 1958 with a new quartet and received an eight-week offer from Joe and Iggy Termini to play the venue again, beginning on June 12.[7] He played most nights during the weekend to capacity crowds with Abdul-Malik, drummer Roy Haynes, and tenor saxophonist Johnny Griffin, who had performed with Monk before.[8] Griffin was unfamiliar with all of his repertoire and, like Coltrane, found it difficult to solo over Monk's comping during their first few weeks. During their performances, Monk often left the stage for a drink at the bar or danced around, which gave Griffin an opportunity to play with more space. However, the quartet eventually developed a sufficient rapport and grasp of the set list.[9]
 Orrin Keepnews attempted to record the quartet live at the Five Spot Café on two different occasions in 1958 for his label, Riverside Records. His first recording of the ensemble was of two sets during their July 9 show. Monk was disappointed with the recording and did not allow Riverside to release it, although it was released later after his death.[9]
 Keepnews returned to the venue on August 7 when Monk performed an evening show in the club's overcrowded room, which the producer had set up with recording equipment.[10] It yielded both Misterioso and Thelonious in Action; the latter was released first in 1958.[11] The show was believed to be the first successful live recording of Monk's music, until the recording of his 1957 concert with Coltrane at Carnegie Hall was discovered and released in 2005.[12] The two live albums from the Five Spot Café are the only recordings that document Monk's time with Griffin.[13]
 According to jazz critic Gary Giddins, Misterioso is a hard bop record.[14] The songs performed for the album were arranged by Monk, who reworked four of his earlier compositions.[15] In the album's liner notes, Keepnews wrote of Monk's approach to arrangements: "It should be axiomatic that Monk is a constantly self-renewing composer-arranger-musician, that each new recording of an 'old' number, particularly with different personnel, represents a fresh view of it—almost a new composition."[16] In the producer's opinion, Monk played the piano more vividly and less introspectively than on his studio recordings in response to the enthusiastic crowds he drew nightly to the venue.[16]
 On "Nutty", Griffin incorporated lines from "The Surrey with the Fringe on Top" and exhibited a frenetic swing that was complemented by counterplay from Haynes and Monk.[17] "Blues Five Spot", a new composition by Monk for the album, is a twelve-bar blues homage to the Five Spot Café and featured solos from each player.[18] Griffin and Monk transfigured chord structures and melodies throughout the performance.[17] Griffin's solo vamp maintained the rhythm while quoting lines from other pieces, including the theme song for the animated Popeye theatrical shorts; he played "The Sailor's Hornpipe" at the end of "Blues Five Spot".[19]
 The quartet began "In Walked Bud" with an eight-bar piano intro and thirty-two-bar form. Griffin began his solo a minute into the song with saxophone wails. In the third minute, Monk did not play, while Griffin played fast phrases at the top of his register with intermittently slower R&B and free jazz elements. Monk shouted approvingly throughout Griffin's solo before he resumed piano and played a two-minute theme.[13] "Just a Gigolo", a standard, was the only song on the album not composed by Monk, who performed it in a brief, unaccompanied version.[21] It was played as a single chorus repeated at length.[22]
 The title track—first recorded for Blue Note Records in 1948 with vibraphonist Milt Jackson—is one of Monk's most influential recordings and is based on a series of minor second clusters.[23] His performance of the song at the Five Spot Café showcased his idiosyncratic playing of one blue note next to another. Monk superimposed musical ideas that deviated from the song's original tonal center, adding a C blue note to the D-flat blue note.[16] Haynes' subdued drumming backed Griffin's aggressive bop playing and extended solo on "Misterioso".[17]
 According to Keepnews, the album and its title track were named as a slight play on the words "mist" and "mystery", meant to evoke the perception of Monk's music as enigmatic and challenging at the time.[16] Jazz critic Neil Tesser said that the word, which is Latin for "in a mysterious manner", was "used most often as a musical direction in classical music scores. But by the time Monk's quartet recorded this music [in 1958] 'Misterioso' had largely come to identify Monk himself."[24]
 To capitalize on Monk's popularity with intellectual and bohemian fans from venues such as the Five Spot Café, Riverside released Misterioso and reissues of his older records with designs that appropriated 20th century works of art.[25] The album's cover art is a reproduction of Giorgio de Chirico's 1915 painting The Seer, which was originally painted as a tribute to French poet Arthur Rimbaud.[26]
 According to Monk biographer Robin Kelley, Rimbaud had "called on the artist to be a seer in order to plumb the depths of the unconscious in the quest for clairvoyance". This led Kelley to believe the painting was the best choice for the album cover. "The one-eyed figure represented the visionary", he explained. "The architectural forms and the placement of the chalkboard evoked the unity of art and science—a perfect symbol for an artist whose music has been called 'mathematical.'"[25] In the opinion of musicologist Robert G. O'Meally, the cover reflected "the mysterious violations of convention of perspective, the silences, and oddly attractive angles (the overall futuristic quality) in Monk's music".[27]
 Misterioso was released on LP in 1958 by Riverside and was Monk's eighth album for the label.[31] The following March, Monk was voted pianist of the year in an annual poll of international jazz critics from Down Beat magazine, who said he was heard "at his challenging, consistently creative best" on Misterioso.[32] Jazz critic Nat Hentoff appraised the record that May for Hi Fi Review, in which he said it was not one of the pianist's best albums. He observed "too little space for Monk's soloing and somewhat too much" for Griffin, whose saxophone cry and timing were more impressive than his solos. Hentoff also believed Haynes and Abdul-Malik did not support Monk as creatively as Wilbur Ware and Art Blakey had on his previous Riverside albums, where he said Monk was in more compelling form.[33] When Misterioso was released in 1964 in the United Kingdom, Charles Fox gave it a positive review in Gramophone. He found its music on-par with Monk's usual standards and highlighted by exceptional playing by him and the rhythm section, particularly Haynes, who showed "once again what a great drummer he was then—and, indeed, still is today". However, Fox felt Griffin did not fit in with the quartet and overshadowed Monk's compositions, finding the saxophonist's solos diffuse and characterized by trivial quotations rather than musical development.[22]
 In the All Music Guide to Jazz (2002), Lindsay Planer wrote that Monk's quartet "continually reinvented" their strong, cohesive sound with "overwhelming and instinctual capacities" throughout Misterioso. He especially praised Griffin, saying the saxophonist "consistently liberated the performances".[17] Monk biographer Robin Kelley felt because he had mastered Monk's songs at that point, his solos on Misterioso and Thelonious in Action were excursive and spirited.[9] Jazz critic Scott Yanow found Misterioso to be the superior record between the two because of what he said was Griffin's unforgettable solo on a passionate rendition of "In Walked Bud", while music historian Ted Gioia listed Monk and Griffin's "freewheeling" performance on the title track as one of his recommended recordings of the composition.[34] According to Robert Christgau, both this record and Brilliant Corners (1957) represented Monk's artistic peak.[13] He cited Misterioso as his favorite album and, in a 2009 article for The Barnes & Noble Review, wrote that Griffin's tenor solo during "In Walked Bud" remained his "favorite five minutes of recorded music".[35] Liam McManus from PopMatters was less enthusiastic about Griffin's playing, which he believed was occasionally heavy-handed and detracted from the music, but still recommended Misterioso as an exceptional Monk record featuring the pianist in a casual performance with his quartet.[30]
 —Robert Christgau, 2019[36]
 In 1989, Misterioso was digitally remastered by mastering engineer Joe Tarantino for the album's CD reissue. Tarantino used 20-bit K2 Super Coding System technology at Fantasy Studios in Berkeley, California.[37] On May 15, 2012, Concord Music Group reissued the album as part of the company's Original Jazz Classics Remasters series, along with Jazz at Massey Hall (1953) and Bill Evans' 1962 record Moon Beams. The re-release featured 24-bit remastering by Tarantino and three bonus tracks, including a medley of "Bye-Ya" and "Epistrophy" performed with drummer Art Blakey. Concord vice president Nick Phillips, who produced the reissue series, said Misterioso was "an all-time classic live Thelonious Monk record" and "an indelible snapshot of Monk live in the late '50s."[38] McManus said that as with most reissues of jazz albums, the bonus tracks on Misterioso were valuable and showcased uninhibited performances of Monk's past compositions.[30]
 All songs were composed by Thelonious Monk, except where noted.[16]
 1958 LP[16]
 
 2012 reissue[24]
 
 
 1 Background 2 Recording 3 Composition and performance 4 Title and packaging 5 Release and reception 6 Track listing 7 Personnel 8 Release history 9 See also 10 References 11 Bibliography 12 External links Ahmed Abdul-Malik – bass Paul Bacon – cover design Giorgio de Chirico – cover painting Ray Fowler – engineer Johnny Griffin – tenor saxophone Roy Haynes – drums Orrin Keepnews – liner notes, producer Thelonious Monk – piano Abbey Anna – project assistant Art Blakey – drums (track 9) Chris Clough – project assistant Andrew Pham – design Nick Phillips – reissue producer Joe Tarantino – digital remastering (1989) Neil Tesser – liner notes Michelle Tremblay – project assistant Jazz portal 1950s in jazz Thelonious Monk discography ^ Kelley 2009, pp. 144, 155–56.
 ^ Kelley 2009, pp. 158, 225.
 ^ a b Kelley 2009, p. 225.
 ^ Kelley 2009, p. 1.
 ^ Kelley 2009, p. 225; Kelley 2009, p. 1
 ^ Kelley 2009, p. 239.
 ^ Anon. 1995, p. 70; Kelley 2009, p. 242
 ^ Kelley 2009, p. 242; Planer 2002, p. 895; Kelley 2009, p. 243
 ^ a b c Kelley 2009, p. 243.
 ^ Kelley 2009, p. 243; Anon. 1995, p. 70
 ^ Anon. 1995, p. 70.
 ^ Anon. 1995, p. 70; Siegel 2005
 ^ a b c Christgau 2009.
 ^ Giddins 1976, p. 105.
 ^ Planer 2002, p. 895; Keepnews 1958
 ^ a b c d e f Keepnews 1958.
 ^ a b c d e Planer 2002, p. 895.
 ^ Keepnews 1958; Fox 1964, p. 118
 ^ Planer 2002, p. 895; Fox 1964, p. 118
 ^ Christgau 2009; McKnight 2010.
 ^ Planer 2002, p. 895; Keepnews 1958
 ^ a b c Fox 1964, p. 118.
 ^ Gioia 2012, p. 267; Schuller 1958
 ^ a b Tesser 2012.
 ^ a b Kelley 2009, p. 249.
 ^ O'Meally 1997, p. 39; Kelley 2009, p. 249
 ^ O'Meally 1997, p. 39.
 ^ a b c d Anon.(a) n.d.
 ^ Holtje & Lee 1998.
 ^ a b c McManus 2012.
 ^ Anon. 1958, p. 41; Keepnews 1958.
 ^ Anon. 1959, p. 56.
 ^ Hentoff 1959, p. 115.
 ^ Yanow 2001, p. 1334; Gioia 2012, p. 267
 ^ Christgau 2005; Christgau 2009
 ^ Christgau 2019.
 ^ Anon.(b) n.d.
 ^ Anon. 2012.
 ^ Anon. 1958, p. 41.
 ^ a b Anon. 2001, p. 304.
 ^ Anon.(c) n.d.
 ^ Anon.(d) n.d.
 Anon. (December 1, 1958). "LP New Releases". Billboard. New York..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} Anon. (March 19, 1959). "Thelonious Monk". Down Beat. 26 (16). Anon. (1995). Original Jazz Classics Collector's Guide. Fantasy, Inc. ISBN 0963742132. Anon. (2001). Brilliant Corners: A Bio-discography of Thelonious Monk. Greenwood Publishing Group. ISBN 0313302391. Anon. (2012). "Original Jazz Classics Add Bill Evans, Thelonious Monk and the Quartet Titles". All About Jazz. Archived from the original on May 27, 2013. Retrieved March 29, 2013. Anon.[a] (n.d.). "Misterioso". Acclaimed Music. Archived from the original on March 5, 2016. Retrieved August 23, 2015. Anon.[b] (n.d.). "Thelonious Monk / Thelonious Quartet Monk – Misterioso CD Album". CD Universe. Muze. Archived from the original on January 17, 2016. Retrieved March 29, 2013. Anon.[c] (n.d.). "Misterioso OJCCD 206 2 : Thelonious Monk Quartet". Concord Music Group. Archived from the original on March 10, 2014. Retrieved March 29, 2013. Anon.[d] (n.d.). "Misterioso (Original Jazz Classics Remasters)". Concord Music Group. Archived from the original on October 31, 2013. Retrieved March 29, 2013. Christgau, Robert (February 22, 2005). "Noise on Music Central". The Village Voice. Archived from the original on March 30, 2013. Retrieved April 11, 2013. Christgau, Robert (2009). "Not So Misterioso". The Barnes & Noble Review. Archived from the original on May 11, 2013. Retrieved April 11, 2013. Christgau, Robert (April 16, 2019). "Xgau Sez". robertchristgau.com. Archived from the original on April 20, 2019. Retrieved April 23, 2019. Gioia, Ted (2012). The Jazz Standards: A Guide to the Repertoire. Oxford University Press. ISBN 0199937397. Fox, Charles (August 1964). "Jazz and Swing". Gramophone. 42. Giddins, Gary (September 20, 1976). "Two Labels Pull Out the Old Bottles". The Village Voice. Hentoff, Nat (May 1959). "Thelonious Monk Quartet – Misterioso". Hi Fi Review. 2. Holtje, Steve; Lee, Nancy Ann, eds. (1998). "Thelonious Monk". MusicHound Jazz: The Essential Album Guide. Music Sales Corporation. ISBN 0825672538. Keepnews, Orrin (1958). Misterioso (LP liner notes). Thelonious Monk Quartet. Riverside Records. RLP 1133. Kelley, Robin (2009). Thelonious Monk: The Life and Times of an American Original. Simon & Schuster. ISBN 1439190496. McKnight, Terrance (2010). "Remembering Harlem's Finest: Bud Powell". WNYC News. Archived from the original on March 4, 2016. Retrieved October 27, 2015. McManus, Liam (2012). "Thelonius Monk Quartet: Misterioso". PopMatters. Archived from the original on March 25, 2014. Retrieved March 24, 2014. O'Meally, Robert G. (1997). "Jazz Albums as Art: Some Reflections". The International Review of African American Art. Hampton University Museum. 14 (1). Planer, Lindsay (2002). "Thelonious Monk: Misterioso".  In Bogdanov, Vladimir; Woodstra, Chris; Erlewine, Stephen Thomas (eds.). All Music Guide to Jazz: The Definitive Guide to Jazz Music (4th ed.). Backbeat Books. ISBN 087930717X. Schuller, Gunther (November 1958). "Thelonious Monk". The Jazz Review. Siegel, Robert (2005). "Unearthing Unknown Monk, Coltrane Recording". NPR. Archived from the original on March 6, 2014. Retrieved March 10, 2014. Tesser, Neil (2012). Misterioso (CD reissue liner notes). Thelonious Monk Quartet. Original Jazz Classics. OJC-33725-02. Yanow, Scott (2001). "Thelonious Monk: Misterioso".  In Bogdanov, Vladimir; Woodstra, Chris; Erlewine, Stephen Thomas (eds.). All Music Guide: The Definitive Guide to Popular Music (4th ed.). Hal Leonard Corporation. ISBN 0879306270. Misterioso at Discogs (list of releases) v t e Discography Compositions Genius of Modern Music: Volume 1 Wizard of the Vibes Genius of Modern Music: Volume 2 Thelonious Monk Trio Monk Thelonious Monk and Sonny Rollins Piano Solo Thelonious Monk Plays Duke Ellington The Unique Thelonious Monk Brilliant Corners Thelonious Himself Art Blakey's Jazz Messengers with Thelonious Monk Monk's Music Thelonious Monk with John Coltrane Mulligan Meets Monk Thelonious Monk Quartet with John Coltrane at Carnegie Hall The Complete 1957 Riverside Recordings Thelonious in Action Misterioso The Thelonious Monk Orchestra at Town Hall 5 by Monk by 5 Thelonious Alone in San Francisco Thelonious Monk at the Blackhawk Monk in France Thelonious Monk in Italy Monk's Dream Criss-Cross Monk in Tokyo Miles & Monk at Newport Big Band and Quartet in Concert It's Monk's Time Monk Solo Monk Live at the It Club Live at the Jazz Workshop Misterioso (Recorded on Tour) Straight, No Chaser Thelonious Monk Nonet Live in Paris 1967 Underground Monk's Blues Midnight at Minton's Bird and Diz Bags' Groove Miles Davis and the Modern Jazz Giants Moving Out Sonny Rollins, Vol. 2 Nica's Tempo In Orbit "In Walked Bud" "'Round Midnight" Blue Note Sessions Thelonious Monk Institute of Jazz Thelonious Monk: Straight, No Chaser 1958 in New York (state) 1958 live albums Albums produced by Orrin Keepnews Albums recorded at the Five Spot Café Albums with cover art by Paul Bacon Live instrumental albums Original Jazz Classics live albums Riverside Records live albums Thelonious Monk live albums Articles with short description Articles with hAudio microformats Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Français Svenska  This page was last edited on 15 October 2019, at 23:23 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Misterioso Misterioso 1958 LP 2012 reissue ^ ^ a b ^ ^ ^ ^ ^ a b c ^ ^ ^ a b c ^ ^ a b c d e f a b c d e ^ ^ ^ ^ a b c ^ a b a b ^ ^ a b c d ^ a b c ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ 26 42 2 14  1958 August 7, 1958 Five Spot Café in New York City Hard bop 47:08 Riverside Orrin Keepnews 


Thelonious in Action(1958)

Misterioso(1958)

The Thelonious Monk Orchestra at Town Hall(1959)
 Thelonious in Action(1958)
 Misterioso(1958)
 The Thelonious Monk Orchestra at Town Hall(1959)
  
"In Walked Bud"

Monk resumed his piano playing after Griffin's fast-to-moderate saxophone solo during "In Walked Bud", a piece dedicated to Bud Powell.[20] Problems playing this file? See media help. All Music Guide to Jazz [17] Encyclopedia of Popular Music [28] Music Story [28] MusicHound Jazz 5/5[29] The Penguin Guide to Jazz [28] PopMatters 9/10[30] The Rolling Stone Album Guide [28] 1. "Nutty" 5:22 2. "Blues Five Spot" 8:11 3. "Let's Cool One" 9:16 1. "In Walked Bud" 11:20 2. "Just a Gigolo" (composed by Irving Caesar and Leonello Casucci) 2:07 3. "Misterioso" 10:52 1. "Nutty" 5:25 2. "Blues Five Spot" 8:17 3. "Let's Cool One" 9:16 4. "In Walked Bud" 11:23 5. "Just a Gigolo" (composed by Irving Caesar and Leonello Casucci) 2:09 6. "Misterioso" 10:54 7. "'Round Midnight" 6:15 8. "Evidence" 10:14 9. "Bye-Ya" / "Epistrophy (Theme)" 11:54 
1958 LP[16]

Ahmed Abdul-Malik – bass
Paul Bacon – cover design
Giorgio de Chirico – cover painting
Ray Fowler – engineer
Johnny Griffin – tenor saxophone
Roy Haynes – drums
Orrin Keepnews – liner notes, producer
Thelonious Monk – piano


 
2012 reissue[24]

Abbey Anna – project assistant
Art Blakey – drums (track 9)
Chris Clough – project assistant
Andrew Pham – design
Nick Phillips – reissue producer
Joe Tarantino – digital remastering (1989)
Neil Tesser – liner notes
Michelle Tremblay – project assistant


 United States
 1958[39]
 Riverside Records
 stereo LP
 RLP 1133[40]
 mono LP
 RLP 12–279[40]
 United Kingdom
 1964[22]
 RLP 279
 United States
 April 7, 1989[41]
 Original Jazz Classics
 CD
 OJCCD-206-25
 May 15, 2012[42]
 Original Jazz Classics, Concord Music Group
 CD
 OJC-33725-02
 
Discography
Compositions
 
Genius of Modern Music: Volume 1
Wizard of the Vibes
Genius of Modern Music: Volume 2
Thelonious Monk Trio
Monk
Thelonious Monk and Sonny Rollins
Piano Solo
Thelonious Monk Plays Duke Ellington
The Unique Thelonious Monk
Brilliant Corners
Thelonious Himself
Art Blakey's Jazz Messengers with Thelonious Monk
Monk's Music
Thelonious Monk with John Coltrane
Mulligan Meets Monk
Thelonious Monk Quartet with John Coltrane at Carnegie Hall
The Complete 1957 Riverside Recordings
Thelonious in Action
Misterioso
The Thelonious Monk Orchestra at Town Hall
5 by Monk by 5
Thelonious Alone in San Francisco
Thelonious Monk at the Blackhawk
Monk in France
Thelonious Monk in Italy
Monk's Dream
Criss-Cross
Monk in Tokyo
Miles & Monk at Newport
Big Band and Quartet in Concert
It's Monk's Time
Monk
Solo Monk
Live at the It Club
Live at the Jazz Workshop
Misterioso (Recorded on Tour)
Straight, No Chaser
Thelonious Monk Nonet Live in Paris 1967
Underground
Monk's Blues
 
Midnight at Minton's
Bird and Diz
Bags' Groove
Miles Davis and the Modern Jazz Giants
Moving Out
Sonny Rollins, Vol. 2
Nica's Tempo
In Orbit
 
"In Walked Bud"
"'Round Midnight"
 
Blue Note Sessions
Thelonious Monk Institute of Jazz
Thelonious Monk: Straight, No Chaser
 