
 The 2014 tour by American alternative rock band the Breeders comprised thirteen concerts in the central and western United States in September 2014. The group's lineup for their 1993 album Last Splash consisted of Josephine Wiggs, Jim Macpherson, Kim Deal, and Kelley Deal. Wiggs and Macpherson were not members for the group's next albums, Title TK (2002) and Mountain Battles (2008). In 2013, the 1993 lineup reunited for a tour to commemorate Last Splash's 20th anniversary, and the following year began working on new material together. Invited to open for Neutral Milk Hotel's September 18 concert at the Hollywood Bowl, they planned a tour to lead up to this show, using the opportunity to practice their recent compositions.
 Between September 2 and September 17, the Breeders performed in eleven cities, including St. Louis, Denver, Seattle, Portland, San Francisco, and Las Vegas. Support bands the Funs and the Neptunas opened for them at five and six of these shows, respectively. The group then played at the Hollywood Bowl concert, and finished the tour on September 20 at the Goose Island 312 Urban Block Party event in Chicago. As well as their new songs, they performed numerous selections from Last Splash and Pod (1990). The tour received good reviews from critics; appraisal included comments that the performances were rousing, and that the band was as good as—or better than—in its heyday.
 In 1993, the Breeders released their second album, Last Splash.[1] At this time, the group's lineup  consisted of sisters Kim and Kelley Deal on guitar and vocals, Josephine Wiggs on bass and vocals, and Jim Macpherson on drums.[2] Last Splash was successful in various countries worldwide,[n 1] and the group toured extensively and played at Lollapalooza 1994.[1] In November 1994, Kelley Deal was arrested on drug-related charges, and Wiggs left the band in the mid-1990s and got involved in other musical projects.[1] Macpherson continued playing with Kim Deal in her side-project group, the Amps, and then in the 1996 incarnation of the Breeders,[8][9] but quit the band in 1997.[10] The Breeders' lineups for their next two albums, Title TK (2002) and Mountain Battles (2008), included the Deal sisters, Mando Lopez, and Jose Medeles, as well as Richard Presley on Title TK.[11][12] In 2013, Wiggs and Macpherson rejoined the Deals to tour the 20th anniversary of Last Splash[13][14]—the LSXX Tour.[15][n 2]
 On December 31, 2013, the Breeders performed their final concert on the 60-date tour in Austin, Texas.[15] The group enjoyed the LSXX concerts, and decided they would like to record new music together.[13] Throughout 2014, Wiggs traveled from her home in Brooklyn, New York to Dayton, Ohio, near where Macpherson and both Deals lived.[13][17] The group began practicing new material in Kim Deal's basement,[13] including compositions by her and one by Wiggs.[13][18] By August, there were three new songs they could play well, two less so, and others they had not yet practiced.[13] Reported titles were "Skinhead #2", "Simone", "All Nerve", and "Launched".[13] The band Neutral Milk Hotel asked the Breeders to open for them at a Hollywood Bowl concert to be held on September 18.[13][19] The latter decided to go on tour leading up to this show and to perform some new compositions in preparation for their eventual recording.[13][19][n 3]
 The September 2014 tour comprised thirteen American dates, all in western and central states.[15] At various shows—between which the Breeders traveled by van[23]—the Funs,[24][25] the Neptunas,[22][26] and Kelley Stoltz[27] were the opening acts.[n 4] The tour began in early September with dates in St. Louis and Kansas City, Missouri, and then continued west to Denver, Salt Lake City, and Garden City, Idaho.[15] On September 10, the Breeders started a short Pacific Coast stretch, performing in Seattle, Portland, and San Francisco.[15][n 5] These were followed by Las Vegas and Phoenix, then west again to San Diego,[15] leading up to their concert on September 18 at the Hollywood Bowl with Neutral Milk Hotel and Daniel Johnston.[32] Breeder's Digest, their official website, announced Carrie Bradley's reunion with the band for this show.[15] Following the Hollywood concert, the Breeders finished their tour at the Goose Island 312 Urban Block Party event in Chicago on September 20, with groups such as Unknown Mortal Orchestra and Thao & the Get Down Stay Down.[33]
 In addition to the new compositions "Simone", "Skinhead #2", "All Nerve", and "Launched", the Breeders performed many songs from their albums Pod (1990) and Last Splash.[34][35] Among these were "Saints", "Cannonball", "No Aloha", and "Divine Hammer" from Last Splash, as well as "Doe", "Happiness Is a Warm Gun", and "Iris" from Pod.[35][36] They also played "Off You" from Title TK and the title track from the Safari EP (1992).[34][37] Another composition they performed was "Walking with a Killer",[36][37] which had originally been released in 2012[38]—with the B-side "Dirty Hessians"—as the first in a series of solo 7" singles by Kim Deal,[39][40] and which the Breeders had played in 2013 on their LSXX Tour.[13][n 6]
 The band's performances on the 2014 tour were generally well received. Regarding their September 3 concert in Kansas City, Danny Phillips of Blurt wrote that the Breeders "like wine, seem to improve with age", adding that nothing about the show could have been better;[37] The Kansas City Star's Timothy Finn likewise summed the night up as "an evening that exceeded its promise".[34] Critic Lissa Townsend Rodgers of the Vegas Seven website commended their rousing performances of "New Year" and "Cannonball" at their Las Vegas concert on September 16[35]—a show also enjoyed by Leslie Ventura of Las Vegas Weekly, who noted the confidence with which the band played.[36] Reviewing the September 17 date in San Diego, critic Alex Packard of Listensd.com opined that the Breeders "deliver[ed] the classics like they wrote them yesterday and new material in no less of a moving way".[42] Both Keith Plocek of LA Weekly and Philip Cosores of Consequence of Sound liked the September 18 concert at the Hollywood Bowl;[41][43] Cosores rated a few of the performances as impeccable, and commented that Kim Deal's "rock and roll soul is still as strong as ever".[41]
 Tim Hinely, also of Blurt, wrote that although the Breeders’ showing in Denver on September 5 could by no means be considered first rate, it was nonetheless enjoyable;[44] in Portland on September 11, 94/7's Yume Delegato heard the group's performance as appealingly unpolished and heartfelt, but asserted that some of the new songs did not leave a strong impression.[45]
  LSXX Tour (2013)  Tour 2014  Tour 2017 1 Background 2 Performances and reception 3 Dates 4 Notes 5 Footnotes 6 References ^ It went platinum in the United States;[3] gold in Australia,[4] Canada,[5] and France;[6] and silver in the United Kingdom.[7]
 ^ According to Kelly Deal, both Lopez and Medeles ceded their spots in the group amicably, and were supportive of the Deals' wish to work with Wiggs and Macpherson again.[16]
 ^ By November 2014, the Breeders had recorded two of the compositions—"All Nerve" and "Skinhead #2"—with engineer Steve Albini.[20] Their album of new material All Nerve was released in March 2018, containing these songs but not "Simone" or "Launched".[21]
 ^ The Funs were devotees of the Breeders, and became involved in the tour when member Philip Jerome Lesicko sent a video of a Funs' live performance to Kim Deal; she liked it and invited them to be an opening act.[28][29] Deal was also an admirer of the Neptunas,[30] who had been inactive for several years but reunited when the Breeders asked them to join the 2014 tour.[31]
 ^ Originally, they had not planned to perform in San Francisco, and were to play at the CS2V Festival in San Jose instead; when it became uncertain whether the musical part of this festival would be held, the Breeders redirected their tour to San Francisco.[15]
 ^ Full set lists of a few of the concerts have been reported. In Kansas City, the Breeders played "Off You""Saints""Hag""Skinhead #2""SOS""Limehouse""Simone""Doe""Hellbound""Walking with a Killer""Fortunately Gone""Happiness is a Warm Gun""Launched""New Year""Cannonball""All Nerve""No Aloha""Divine Hammer" and during the encore "Glorious""Safari""Iris".[34][37] At their Las Vegas show, the set list was almost the same, with some variation in the order, and with the inclusion of Last Splash's "I Just Wanna Get Along" and "Do You Love Me Now?" instead of "Launched".[35][36] During their shorter Hollywood set, they performed this album's "Drivin' on 9", as well as twelve of the songs from the Kansas City and Las Vegas concerts.[41]
 "Off You" "Saints" "Hag" "Skinhead #2" "SOS" "Limehouse" "Simone" "Doe" "Hellbound" "Walking with a Killer" "Fortunately Gone" "Happiness is a Warm Gun" "Launched" "New Year" "Cannonball" "All Nerve" "No Aloha" "Divine Hammer" and during the encore "Glorious" "Safari" "Iris".[34][37] ^ a b c Petrusich 2013, p. 2
 ^ Last Splash (CD booklet)
 ^ American certifications – Breeders, The
 ^ Ryan 2011, p. 41
 ^ Gold Platinum Database
 ^ Les Certifications
 ^ Certified Awards Search
 ^ Erlewine
 ^ Phares
 ^ Aston 2013, p. 556
 ^ Title TK (CD booklet)
 ^ Mountain Battles (CD booklet)
 ^ a b c d e f g h i j Hopper 2014
 ^ Brooks 2014
 ^ a b c d e f g h i j k l m 2014 Tour Dates and New Music / 2013 LSXX Tour Dates
 ^ Power 2013
 ^ McDonald 2014
 ^ Cole 2014, p. 2
 ^ a b Silvers 2014, p. 3
 ^ Jones 2014
 ^ The Breeders: 'All Nerve'
 ^ a b c The Breeders with The Neptunas (Showbox)
 ^ Wenzel 2014
 ^ a b The Breeders is Completely SOLD OUT
 ^ a b The Breeders, The Funs (Record Bar)
 ^ a b The Breeders, The Neptunas (The Portland Mercury)
 ^ a b The Breeders with Kelley Stoltz
 ^ Lees 2014
 ^ Galil 2016
 ^ The Deal On The New Breeders Album
 ^ Russo 2017
 ^ a b Neutral Milk Hotel, The Breeders, Daniel Johnston
 ^ a b Bernot 2014
 ^ a b c d Finn 2014
 ^ a b c d Rodgers 2014
 ^ a b c d Ventura 2014
 ^ a b c d Phillips 2014
 ^ Ōtaka 2018
 ^ Pelly 2013
 ^ Belhan 2013
 ^ a b c Cosores 2014
 ^ Packard 2014
 ^ Plocek 2014
 ^ a b Hinely 2014
 ^ Delegato 2014
 ^ The Breeders, The Funs (Urban Lounge)
 ^ Atkins 2014
 ^ The Breeders (Time Out)
 ^ The Breeders, The Neptunas (Bunkhouse Saloon)
 ^ The Breeders, The Neptunas (Crescent Ballroom)
 ^ The Breeders, The Neptunas (Casbah)
 "2014 Tour Dates and New Music / 2013 LSXX Tour Dates". Breeder's Digest. Archived from the original on October 22, 2014. Retrieved October 24, 2017..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} "American certifications – Breeders, The". Recording Industry Association of America. Archived from the original on October 24, 2017. Retrieved October 24, 2017. Atkins, Amy (September 3, 2014). "The Breeders, Sept. 8, Visual Arts Collective". Boise Weekly. Archived from the original on December 22, 2014. Retrieved October 24, 2017. Aston, Martin (2013). Facing the Other Way: The Story of 4AD. The Friday Project. ISBN 978-0-00-748961-9. Belhan, Tom (January 15, 2013). "Kim Deal – "Walking With A Killer" & "Dirty Hessians"". Stereogum. Archived from the original on December 31, 2014. Retrieved October 24, 2017. Bernot, Kate (August 13, 2014). "2014 Chicago fall beer festival guide". Chicago Tribune. Archived from the original on November 17, 2017. Retrieved November 17, 2017. "The Breeders". Time Out. Archived from the original on October 13, 2017. Retrieved October 24, 2017. "The Breeders: 'All Nerve'". 4AD. Archived from the original on January 11, 2018. Retrieved January 11, 2018.CS1 maint: extra punctuation (link) "The Breeders is Completely SOLD OUT". Off Broadway. Archived from the original on December 22, 2014. Retrieved October 24, 2017. "The Breeders, The Funs". Record Bar. Archived from the original on December 22, 2014. Retrieved October 24, 2017. "The Breeders, The Funs". Urban Lounge. Archived from the original on December 22, 2014. Retrieved October 24, 2017. "The Breeders, The Neptunas". Bunkhouse Saloon. Archived from the original on April 2, 2015. Retrieved November 12, 2017. "The Breeders, The Neptunas". Casbah. Archived from the original on December 22, 2014. Retrieved October 24, 2017. "The Breeders, The Neptunas". Crescent Ballroom. Archived from the original on December 22, 2014. Retrieved October 24, 2017. "The Breeders, The Neptunas". The Portland Mercury. Archived from the original on December 22, 2014. Retrieved October 24, 2017. "The Breeders with Kelley Stoltz". Fillmore. Archived from the original on December 22, 2014. Retrieved October 24, 2017. "The Breeders with The Neptunas". Showbox. Archived from the original on November 18, 2014. Retrieved October 24, 2017. Brooks, Miranda (September 1, 2014). "The Breeders get set to debut new material on the road". The Marquee. Archived from the original on December 21, 2014. Retrieved October 24, 2017. "Certified Awards Search" (To access, enter the search parameter "Breeders"). British Phonographic Industry. Retrieved October 24, 2017. Cole, Rachel (April 7, 2014). "Q&A: Kim Deal On Her Solo 7" Series, Making New Breeders Music, And Her Lifelong Obsession With Gear". Stereogum. Archived from the original on December 22, 2014. Retrieved October 24, 2017. Cosores, Philip (September 19, 2014). "Live Review: Neutral Milk Hotel at the Hollywood Bowl (9/18)". Consequence of Sound. Archived from the original on September 20, 2014. Retrieved October 24, 2017. "The Deal On The New Breeders Album". Stereogum. December 4, 2007. Archived from the original on November 26, 2017. Retrieved November 26, 2017. Delegato, Yume (September 16, 2014). "The Breeders :: Wonder Ballroom :: 09.11.14". 94/7. Archived from the original on January 3, 2015. Retrieved October 24, 2017. Erlewine, Stephen Thomas. "The Breeders Biography". AllMusic. Archived from the original on February 23, 2015. Retrieved October 25, 2017. Finn, Timothy (September 4, 2014). "Breeders give a RecordBar crowd a steady volley of favorites and new songs". The Kansas City Star. Archived from the original on September 13, 2014. Retrieved October 25, 2017. Galil, Leor (February 10, 2016). "The Funs make a complicated Valentine's Day mix". Chicago Reader. Archived from the original on March 19, 2017. Retrieved November 26, 2017. "Gold Platinum Database". Music Canada. Archived from the original on August 18, 2016. Retrieved October 25, 2017. Hinely, Tim (September 5, 2014). "The Breeders 9/5/14, Denver". Blurt. Archived from the original on September 16, 2014. Retrieved October 25, 2017. Hopper, Jessica (August 28, 2014). "Kim and Kelley Deal Talk New Breeders Songs: There Are 'Five That We Can Play'". Rolling Stone. Archived from the original on April 9, 2016. Retrieved October 25, 2017. Jones, Ross (November 19, 2014). "Kelley Deal and Slint Drummer Britt Walford appear on new single". DIY Magazine. Archived from the original on March 4, 2016. Retrieved October 25, 2017. Last Splash (CD booklet). The Breeders. Canada: PolyGram. 1993.CS1 maint: others (link) Lees, Jaime (September 2, 2014). "The Funs Kick Off Tour with the Breeders Tonight at Off Broadway". The Riverfront Times. Archived from the original on October 1, 2015. Retrieved November 26, 2017. "Les Certifications". Syndicat National de l'Édition Phonographique (in French). November 2013. Archived from the original on December 11, 2017. Retrieved December 11, 2017. McDonald, Scott (September 10, 2014). "The Breeders make another splash". San Diego CityBeat. Archived from the original on December 22, 2015. Retrieved October 25, 2017. Mountain Battles (CD booklet). The Breeders. Japan: 4AD. 2008.CS1 maint: others (link) "Neutral Milk Hotel, The Breeders, Daniel Johnston". Hollywood Bowl. Archived from the original on October 3, 2014. Retrieved October 25, 2017. Ōtaka, Shunichi (2018). All Nerve (CD booklet) (in Japanese). The Breeders. Japan: Beat Records. Packard, Alex (September 14, 2014). "The Breeders at the Casbah". Listensd.com. Archived from the original on January 3, 2015. Retrieved October 25, 2017. Pelly, Jenn (January 3, 2013). "Kim Deal Launches Solo 7" Singles Series". Pitchfork. Archived from the original on March 6, 2016. Retrieved October 25, 2017. Petrusich, Amanda (May 15, 2013). "Splashdown! The Breeders' Cannonball-like Re-entry". Spin. Archived from the original on July 3, 2017. Retrieved October 25, 2017. Phares, Heather. "The Amps Biography". AllMusic. Archived from the original on September 8, 2015. Retrieved October 25, 2017. Phillips, Danny R. (September 3, 2014). "The Breeders 9/3/14, Kansas City, MO". Blurt. Archived from the original on September 16, 2014. Retrieved October 25, 2017. Plocek, Keith (September 19, 2014). "Neutral Milk Hotel – Hollywood Bowl – September 18, 2014". LA Weekly. Archived from the original on July 5, 2015. Retrieved October 25, 2017. Power, Bobby (May 13, 2013). "The Breeders' 'Last Splash' at 20: An interview with Kelley Deal". Creative Loafing. Archived from the original on November 4, 2017. Retrieved November 4, 2017. Rodgers, Lissa Townsend (September 16, 2014). "The Breeders Are Still Virile After All These Years". Vegas Seven. Archived from the original on December 24, 2014. Retrieved October 25, 2017. Russo, Stacy (2017). We Were Going to Change the World: Interviews with Women from the 1970s and 1980s Southern California Punk Rock Scene. Santa Monica Press. ISBN 978-1-59580-795-3. Retrieved November 27, 2017. Ryan, Gavin (2011). Australia's Music Charts 1988–2010 (PDF ed.). Moonlight Publishing. Silvers, Emma (September 9, 2014). "The Breeders barrel on". San Francisco Bay Guardian. Archived from the original on September 15, 2014. Retrieved October 25, 2017. Title TK (CD booklet). The Breeders. Japan: P-Vine Records. 2002.CS1 maint: others (link) Ventura, Leslie (September 17, 2014). "Concert Review: The Breeders Zip in for a Decisive Bunkhouse Performance". Las Vegas Weekly. Archived from the original on October 6, 2014. Retrieved October 25, 2017. Wenzel, John (September 13, 2014). "Kim Deal's Breeders jump back into "Last Splash" with extended tour". The Denver Post. Archived from the original on November 17, 2017. Retrieved November 17, 2017. v t e Kim Deal Kelley Deal Josephine Wiggs Jim Macpherson Carrie Bradley Tanya Donelly Jose Medeles Britt Walford Pod Last Splash Title TK Mountain Battles All Nerve Live in Stockholm 1994 Safari Head to Toe Fate to Fatal "Cannonball" "Divine Hammer" "Saints" "Off You" "Son of Three" "Wait in the Car" Discography Tour 2009 Tour 2014 Tour 2017 The Amps Belly The Kelley Deal 6000 Pixies Throwing Muses The Last Hard Men 2014 concert tours 2014 in American music Concert tours of the United States September 2014 events in the United States The Breeders Featured articles Use American English from August 2019 All Wikipedia articles written in American English Use mdy dates from August 2019 Articles with short description CS1 maint: extra punctuation CS1 maint: others CS1 French-language sources (fr) CS1 Japanese-language sources (ja) Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version فارسی  This page was last edited on 13 October 2019, at 03:00 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Tour 2014 ^ ^ ^ ^ ^ ^ a b c ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b c d e f g h i j ^ a b c d e f g h i j k l m ^ ^ ^ a b ^ ^ a b c ^ a b a b a b a b ^ ^ ^ ^ a b a b a b c d a b c d a b c d a b c d ^ ^ ^ a b c ^ ^ a b ^ ^ ^ ^ ^ ^ ^ Kim Deal Kelley Deal Josephine Wiggs Jim Macpherson Tour by The Breeders September 2, 2014 (2014-09-02) September 20, 2014 (2014-09-20) 13 
LSXX Tour (2013)
Tour 2014
Tour 2017
 September 2
 St. Louis
 Missouri
 United States
 Off Broadway
 The Funs[24]
 September 3
 Kansas City
 Record Bar
 The Funs[25]
 September 5
 Denver
 Colorado
 Summit Music Hall
 The Funs[44]
 September 7
 Salt Lake City
 Utah
 Urban Lounge
 The Funs[46]
 September 8
 Garden City
 Idaho
 Visual Arts Collective
 The Funs[47]
 September 10
 Seattle
 Washington
 The Showbox
 The Neptunas[22]
 September 11
 Portland
 Oregon
 Wonder Ballroom
 The Neptunas[26]
 September 13
 San Francisco
 California
 The Fillmore
 Kelley Stoltz,[27] The Neptunas[48]
 September 15
 Las Vegas
 Nevada
 Bunkhouse Saloon
 The Neptunas[49]
 September 16
 Phoenix
 Arizona
 Crescent Ballroom
 The Neptunas[50]
 September 17
 San Diego
 California
 The Casbah
 The Neptunas[51]
 September 18
 Los Angeles
 Hollywood Bowl
 Neutral Milk Hotel (headliner), Daniel Johnston[32]
 September 20
 Chicago
 Illinois
 Goose Island 312 Urban Block Party
 Thao & the Get Down Stay Down, Unknown Mortal Orchestra, Cayucas[33]
 
Kim Deal
Kelley Deal
Josephine Wiggs
Jim Macpherson
Carrie Bradley
Tanya Donelly
Jose Medeles
Britt Walford
 
Pod
Last Splash
Title TK
Mountain Battles
All Nerve
 
Live in Stockholm 1994
 
Safari
Head to Toe
Fate to Fatal
 
"Cannonball"
"Divine Hammer"
"Saints"
"Off You"
"Son of Three"
"Wait in the Car"
 
Discography
Tour 2009
Tour 2014
Tour 2017
The Amps
Belly
The Kelley Deal 6000
Pixies
Throwing Muses
The Last Hard Men
 