Hurricane Guillermo was the ninth-most intense Pacific hurricane on record, attaining peak winds of 160 mph (260 km/h) and a barometric pressure of 919 hPa (27.14 inHg). Forming out of a tropical wave on July 30, 1997, roughly 345 mi (555 km) south of Salina Cruz, Mexico, Guillermo tracked in a steady west-northwestward direction while intensifying. The system reached hurricane status by August 1 before undergoing rapid intensification the following day. At the end of this phase, the storm attained its peak intensity as a powerful Category 5 hurricane. The storm began to weaken during the afternoon of August 5 and was downgraded to a tropical storm on August 8. Once entering the Central Pacific Hurricane Center's area of responsibility, Guillermo briefly weakened to a tropical depression before re-attaining tropical storm status. On August 15, the storm reached an unusually high latitude of 41.8°N before transitioning into an extratropical cyclone. The remnants persisted for more than a week as they tracked towards the northeast and later south and east before being absorbed by a larger extratropical system off the coast of California on August 24.
 Throughout Guillermo's track, the storm never threatened any major landmass, resulting in little impact on land. However, because of its extreme intensity, it produced large swells across the Pacific Ocean, affecting areas from Hawaii to coastal Mexico. Along the American Pacific coast, three people drowned amid high waves, two in Baja California and one in California. At its peak, Guillermo was the second strongest known Pacific hurricane on record; however, it has since been surpassed by seven other storms, including Linda later that year. The effects of Guillermo were not deemed severe enough to justify retirement of its name.
 Hurricane Guillermo began its extensive track as a tropical wave that moved off the coast of Africa on July 16, 1997. Initially disorganized and weak, the system tracked westward across the Atlantic Ocean for several weeks. The National Hurricane Center (NHC) stated that they had issues monitoring the system as it moved through the Caribbean; however, they interpreted through satellite data that the wave crossed Central America and entered the Pacific Ocean between July 27 and 28. Once in the Pacific, convection, areas of thunderstorm activity, and banding features began to form. Additionally, a low pressure system developed within the disturbance by July 29. The following day, the system became sufficiently organized for the NHC to classify it as Tropical Depression Nine-E; at this time the depression was situated roughly 345 mi (555 km) south of Salina Cruz, Mexico. In response to a deep-layer ridge to the north, the depression tracked at a steady pace towards the west-northwest, and this motion persisted through the first week of August. Within a day of being classified, the system intensified into Tropical Storm Guillermo, the seventh named storm of the 1997 season.[1]
 By the afternoon of August 1, a strong central dense overcast, an area of high, thick clouds, developed over the center of circulation, prompting the NHC to upgrade the system to a Category 1 hurricane on the Saffir–Simpson Hurricane Scale. Throughout the next day, the system gradually developed an eye within the central dense overcast, leading to further strengthening.[1] Operationally, Guillermo was thought to have briefly leveled out in intensity on August 2;[2] however, in post-storm analysis, the NHC discovered that a steady period of rapid intensification took place.[1] Unlike most hurricanes in the eastern Pacific, Guillermo was investigated by Hurricane Hunters reconnaissance aircraft during its rapid intensification stage. The aircraft released several dropsondes into the storm to gather meteorological data.[3] This mission marked the first time the Hurricane Hunters recorded high-resolution wind data from flight level to within several meters of the ocean surface inside the eyewall of a major hurricane.[4]
 Radar was also employed to determine the size of the hurricane's eye, stated to be roughly 13 mi (20 km) in diameter,[3] following a 6-mile (10 km) decrease due to the strengthening.[5] Light wind shear surrounding the hurricane allowed further strengthening to take place.[3] Late on August 2, the system attained winds of 135 mph (215 km/h), making it a Category 4 storm. During the afternoon of August 3, Guillermo nearly attained Category 5 intensity as it reached its initial peak intensity with winds of 155 mph (250 km/h) along with a barometric pressure estimated at 925 mbar (hPa; 27.32 inHg). A brief period of weakening took place later that day before further strengthening ensued.[1] Late on August 4, Guillermo intensified into a Category 5 hurricane, attaining peak winds of 160 mph (260 km/h).[1] Operational analysis of satellite information indicated a minimum pressure of 921 mbar (hPa; 27.2 inHg);[2] however, post-storm studies stated that the pressure was more likely around 919 mbar (hPa; 27.14 inHg).[1]
 At its peak, cloud temperatures within the eyewall were estimated to be as low as −79 °C (−110 °F). Using the Dvorak technique, a method used to estimate the intensity of tropical cyclones, a value of 7.6 was obtained. This indicated that maximum winds at the surface could have been as high as 181 mph (291 km/h); however, this was not used as the reported intensity as six- to twelve-hour averages indicated sustained winds around 160 mph (260 km/h).[6] After maintaining this intensity for roughly 18 hours, the system began to weaken as it moved into a less favorable environment with moderate wind shear.[1][3] Cloud temperatures within the eyewall also began to increase, indicating that the hurricane was losing intensity.[1]
 Steady weakening took place over the following several days, and the storm dropped below major hurricane status on August 6. By August 8, Guillermo moved over colder waters and was downgraded to a tropical storm as sustained winds dropped below 75 mph (120 km/h). Around this time, the storm started to move along the western edge of the ridge that previously steered it towards the west-northwest, causing Guillermo to turn northwest. On August 9, the storm crossed longitude 140°W, denoting a shift in warning responsibility from the NHC to the Central Pacific Hurricane Center (CPHC).[1] Not long after crossing into the CPHC's area of responsibility, the storm further weakened to a tropical depression as it moved over 24 °C (75 °F) waters.[7]
 Although most tropical cyclones generally weaken as they increase in latitude, warm waters of 26 °C (79 °F) allowed Guillermo to re-intensify into a tropical storm on August 11. Gradually, the storm attained winds of 65 mph (100 km/h) before succumbing to cooler waters further north. The storm weakened to a tropical depression once more on August 15, situated well to the north of the Hawaiian islands.[7] Later that day, Guillermo transitioned into an extratropical cyclone at an unusually high latitude of 41.8°N,[1] roughly 850 mi (1,370 km) south-southeast of Unalaska, Alaska.[8] Over the following days, the remnants of the hurricane turned northeastward towards the Pacific coast of North America. On August 19, the system moved within 575 mi (925 km) of Vancouver Island, British Columbia, before Guillermo tracked southward. Over the following few days, the system slowed significantly and turned towards the east.[1] By August 20, moisture from the remnants of Tropical Storm Ignacio were entrained into the eastern portion of Guillermo's circulation.[9] On August 24, Guillermo's remnant low was finally absorbed by a larger extratropical system, while situated roughly 345 mi (555 km) off of the northern California coastline.[1]
 Along the Pacific coast of Mexico, 12-foot (3.7 m) surf produced by Hurricane Guillermo affected numerous beaches. From Cabo San Lucas to San José del Cabo, the storm sent waves from the Gulf of California over the grounds of beachside resorts. Tourists were driven off some of Cabo's most popular beaches by the rough ocean conditions which flooded homes situated along the coast. Two sightseers were killed after being swept out to sea.[11]
 Due to accurate forecasts, emergency officials across California were able to close off swimming zones and warn the public about dangerous rip currents prior to their arrival.[12] Guillermo generated heavy surf across the beaches of Southern California. Heights from the surf averaged 6 to 8 feet (1.8 to 2.4 m) with local areas reaching above 10 feet (3.0 m),[13] favorable to around 500 surfers at an annual competition at Huntington Beach.[14] According to the Los Angeles Times, some waves reached heights of 15 to 18 ft (4.6 to 5.5 m).[15] Over 100 rescues were reported by local lifeguards.[13] Waves ranging from 6 to 12 feet (1.8 to 3.7 m) also affected Orange County. Hundreds of people were rescued, but rip currents were blamed for three injuries and one death.[16] On August 5, about a mile north of Huntington Beach pier, a 19-year-old man was swept away. His body was recovered several days later. On August 6, a teenage boy and girl were injured on a beach in Corona Del Mar, while an 18-year-old was pulled ashore and sent to the hospital with neck injuries. In Newport Beach, lifeguards made nearly 300 rescues on August 5 and 6 alone.[16]
 In addition to coastal impacts, the system brought a surge of warm, moist air into Southern California, bringing temperatures in some areas as high as 110 °F (43 °C).[17] Between August 15 and 17, large waves generated while Guillermo was at peak intensity came ashore in Hawaii. The swells caused no damage and reached heights of 10 feet (3.0 m) in eastern-facing shores of the state.[7]
 At the time, Hurricane Guillermo's central pressure of 919 mbar (hPa; 27.14 inHg) established it as the second-most intense Pacific hurricane on record, behind Hurricane Ava of 1973. However, later in the 1997 season, Hurricane Linda set a new intensity record, and in subsequent years, hurricanes Kenna, Ioke, Rick, Marie, Odile, and Patricia all achieved lower minimum pressures, leaving Guillermo in ninth place. Guillermo persisted for 16.5 days from its classification as a tropical depression to its transition into an extratropical storm, making it the sixth longest-lasting storm in the basin.[8]
 
 1 Meteorological history 2 Impact and records 3 See also 4 References 5 External links Tropical cyclones portal Other storms of the same name List of Category 5 Pacific hurricanes List of Pacific hurricanes Timeline of the 1997 Pacific hurricane season ^ a b c d e f g h i j k l Britt Max Mayfield (October 2, 1997). "Hurricane Guillermo Preliminary Report". National Hurricane Center. Retrieved July 6, 2010..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b National Hurricane Center (1997). "Operational Track for Hurricane Guillermo". Unisys Weather. Archived from the original on October 2, 2012. Retrieved July 6, 2010.
 ^ a b c d Paul D. Reasor; Matthew D. Eastin; John F. Gamache (June 30, 2008). "Rapidly Intensifying Hurricane Guillermo (1997). Part I: Low-Wavenumber Structure and Evolution". American Meteorological Society. Retrieved July 6, 2010.
 ^ Miles B. Lawrence (October 20, 1998). "Eastern North Pacific Hurricane Season of 1997" (PDF). National Hurricane Center. Retrieved July 7, 2010.
 ^ Matthew Sitkowski; Klaus Dolling; Gary Barnes (2005). "The Rapid Intensification of Hurricane Guillermo (1997) As Viewed With GPS Dropwindsondes" (PDF). Department of Meteorology, University of Hawaii. American Meteorological Society. Retrieved July 6, 2010.
 ^ Regional And Mesoscale Meteorology Team (August 7, 1997). "Daily Satellite Discussion: Thursday August 7, 1997". National Oceanic and Atmospheric Administration.
 ^ a b c Benjamin C. Hablutzel; Hans E. Rosendal; James C. Weyman; Jonathan D. Hoag (1997). "Hurricane Guillermo Preliminary Report". Central Pacific Hurricane Center. Retrieved July 6, 2010.
 ^ a b 
 ^ Edward N. Rappaport (August 27, 1997). "Tropical Storm Ignacio Preliminary Report". National Hurricane Center. Retrieved July 26, 2010.
 ^ National Hurricane Center; Hurricane Research Division; Central Pacific Hurricane Center. "The Northeast and North Central Pacific hurricane database 1949–2018". United States National Oceanic and Atmospheric Administration's National Weather Service. A guide on how to read the database is available here.
 ^ Cabo Bob (2007). "Hurricanes in Cabo San Lucas". Unknown. Archived from the original on February 6, 2012. Retrieved March 6, 2007.
 ^ Scott Hadly (August 10, 1997). "Area Surfers Catch Wave of Forecast Data". Los Angeles Times. Retrieved July 8, 2010.
 ^ a b "California Event Report: High Surf". National Climatic Data Center. 1997. Archived from the original on July 6, 2010. Retrieved March 6, 2007.
 ^ Steve Carney (August 8, 1997). "Board Meeting; Competition Draws 500 Surfers, Big Crowds and Storm-Stoked Waves". Los Angeles Times. p. 3. Retrieved July 8, 2010.
 ^ Jim Benning (August 30, 2005). "A swell season takes shape". Los Angeles Times. Retrieved July 8, 2010.
 ^ a b "California Event Report: Rip Currents". National Climatic Data Center. 1997. Archived from the original on July 6, 2010. Retrieved March 6, 2007.
 ^ Ed Jahn (August 6, 1997). "As summer turns up heat, SDG&E says `cool it' Temperatures might retreat by weekend". The San Diego Union – Tribune. p. A.1. Retrieved July 8, 2010.
 NHC 1997 Pacific hurricane season archive HPC 1997 Tropical Cyclone Rainfall Pages Central Pacific Hurricane Center archive v t e Patsy (1959) Ava (1973) Emilia (1994) Gilma (1994) John (1994) Guillermo (1997) Linda (1997) Elida (2002) Hernan (2002) Kenna (2002) Ioke (2006) Rick (2009) Celia (2010) Marie (2014) Patricia (2015) Lane (2018) Walaka (2018) Willa (2018)  Book  Category  Tropical cyclones portal v t e  Book  Category  Portal 1997 Pacific hurricane season Category 5 Pacific hurricanes Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Deutsch Français Português Tagalog 中文  This page was last edited on 22 September 2019, at 12:43 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  1997 Pacific hurricane season Hurricane Guillermo a b c d e f g h i j k l a b a b c d ^ ^ ^ a b c a b ^ ^ ^ ^ a b ^ ^ a b ^  Book  Category  Portal Category 5 major hurricane (SSHWS/NWS) Hurricane Guillermo at peak intensity on August 4 July 30, 1997 August 24, 1997 (Extratropical after August 15)   
 1-minute sustained: 160 mph (260 km/h)  919 mbar (hPa); 27.14 inHg(Ninth–most intense Pacific hurricane on record)   
 3 total None Pacific coast of Mexico, Hawaii, California  Part of the 1997 Pacific hurricane season 
 Patricia
 2015
 872
 25.75
 Linda
 1997
 902
 26.64
 Rick
 2009
 906
 26.76
 Kenna
 2002
 913
 26.96
 Ava
 1973
 915
 27.02
 Ioke
 2006
 Marie
 2014
 918
 27.11
 Odile
 Guillermo
 1997
 919
 27.14
 Gilma
 1994
 920
 27.17
 Walaka
 2018
 
Patsy (1959)
 
Ava (1973)
 
Emilia (1994)
Gilma (1994)
John (1994)
Guillermo (1997)
Linda (1997)
 
Elida (2002)
Hernan (2002)
Kenna (2002)
Ioke (2006)
Rick (2009)
 
Celia (2010)
Marie (2014)
Patricia (2015)
Lane (2018)
Walaka (2018)
Willa (2018)
 
 Book
 Category
 Tropical cyclones portal
  

TSAndres
TSBlanca
TDThree-E
TSCarlos
TDFive-E
1Dolores
3Enrique
4Felicia

TDOne-C
5Guillermo
TSHilda
TSIgnacio
4Jimena
TSOliwa
TSKevin
5Linda

TSMarty
4Nora
TSOlaf
4Pauline
TDThree-C
TDFour-C
2Rick
TSPaka

 TS Andres TS Blanca TD Three-E TS Carlos TD Five-E 1 Dolores 3 Enrique 4 Felicia TD One-C 5 Guillermo TS Hilda TS Ignacio 4 Jimena TS Oliwa TS Kevin 5 Linda TS Marty 4 Nora TS Olaf 4 Pauline TD Three-C TD Four-C 2 Rick TS Paka 
 Book
 Category
 Portal
 