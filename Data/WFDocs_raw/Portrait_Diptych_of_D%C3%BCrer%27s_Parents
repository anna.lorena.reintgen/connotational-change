
 Portrait Diptych of Dürer's Parents (or Dürer's Parents with Rosaries) is the collective name for two late-15th century portrait panels by the German painter and printmaker Albrecht Dürer. They show the artist's parents, Barbara Holper (c. 1451–1514) and Albrecht Dürer the Elder (c. 1427–1502), when she was around 39 and he was 63 years. The portraits are unflinching records of the physical and emotional effects of ageing. The Dürer family was close, and Dürer may have intended the panels either to display his skill to his parents[1] or as keepsakes while he travelled soon after as a journeyman painter.
 They were created either as pendants, that is conceived as a pair and intended to hang alongside each other,[2] or diptych wings. However, this formation may have been a later conception; Barbara's portrait seems to have been executed some time after her husband's and it is unusual for a husband to be placed to the viewer's right in paired panels. His father's panel is considered the superior work and has been described as one of Dürer's most exact and honest portraits.[3] They are among four paintings or drawings Dürer made of his parents,[A] each of which unsentimentally examines the deteriorating effects of age. His later writings contain eulogies for both parents, from which the love and respect he felt toward them are evident.
 Each panel measured 47.5 cm x 39.5 cm (18.7 in x 15.6 in), but the left hand panel has been cut down. They have been separated since at least 1628, until Barbara's portrait—long considered lost—was reattributed in 1977.[B] The panels were reunited in the Germanisches Nationalmuseum's 2012 exhibition "The Early Dürer".[7]
 The three-quarter view was widely used in southern German portraiture of the late-15th century. Rosary beads were often included to indicate the piety and modesty of the sitters, although by the 16th century religious motifs and sentiments like this were falling out of fashion. Dürer distinguishes himself from his contemporaries through his tight and detailed focus on his parents' faces, a technique that draws comparison to the work of the first generation of Early Netherlandish painters active 50 years earlier. Albrecht the Elder had travelled to Flanders and from working with Netherlandish artists had acquired a strong appreciation for the work of both Jan van Eyck and Rogier van der Weyden.[8] That he passed on this influence to his son is evident from the early use of silverpoint, a medium which according to Erwin Panofsky requires "an exceptional degree of confidence, accuracy and sensitive feeling for its successful handling".[9]
 Dürer would have been aware of Hans Pleydenwurff's portrait of the ageing Count Georg von Lowenstein, through his teacher Michael Wolgemut. Pleydenwurff's portrait was in turn likely influenced by van Eyck's 1438 Portrait of Cardinal Niccolò Albergati.[10][11] Juliane von Fircks [de] believes the portrait of Dürer's father took from Pleydenwurff's portrait, which she describes as a "highly detailed representation of [a] white haired old man, who defies the pains of growing old with an alert mind and an inner animation".[10]
 Von Fircks notes that Dürer's 1484 self-portrait was created with the use of a mirror while his most iconic work is a self-portrait; the 1500 Self-Portrait at Twenty-Eight. From these she concludes that the "accurate observation and documentary recording" of both his own and his parents' appearances over time was not just a compulsion, but that is indicative of a deeper interest in the effects of time and age on human appearance.[10] Although Dürer was fascinated by the effects of ageing on others, he seems to have had some hesitancy at examining how it might affect him personally. The self-portraits tend to be idealised and the 1500 portrait was his last. Later self-portraits are far more understated and executed in a 'secondary' media, such as his drawings of the Man of Sorrows and nude drawing of 1505, which depicted an emaciated body during the time of the plague.[3]
 Albrecht the Elder was 62 or 63[C]  when his panel was painted early in 1490. Barbara's portrait may have been completed soon after, when she would have been around 39. Their son was around 18 and had just completed his apprenticeship with Michael Wolgemut, and would soon leave for Nuremberg to travel as a journeyman painter. The father panel was painted first; for aesthetic reasons Dürer may have waited a year or two until his mother looked older.[13]
 The sitters are presented in three-quarter view before flat, nondescript lacquer-like green backgrounds, which although lushly coloured, are thinly layered.[14] Each has a white ground and light red imprimatura with lead content.[15] The sitter's form and pose echo and in many ways counterbalance each other.[16] Brand Philip draws attention to the similarities of the panels' linear construction, especially the manner in which the folds and lines of their clothing form triangular shapes. The train of Barbara's headdress across her chest corresponds with the opening of Albrecht's fur-lined coat.[13] It was more usual in pendant portraits of this type for the male to be on the left-hand side; the positioning here may be an indication that Dürer originally intended his father's panel to stand alone, given that Barbara's was painted some time later.[7]
 Albrecht the Elder's panel is regarded as the superior of the two works. This may in part be attributed to differing contemporary treatments of male and female portraits; men were allowed more individuality, while female portraits were bound by stereotypes and were not as daring, for example showing evidence of ageing. In either case, the father painting is far more closely detailed, especially in the lining of clothing, which is summary in Barbara's panel, compared to the long seam of her husband's gown. This contrast in detail can also be seen in the treatment of the rosaries, which are given prominence and a glowing red colour in his panel, but are small and relegated in hers.[17]
 Albrecht the Elder's panel is usually, but not always, thought to be the first of the two to be executed and, if so, is the earliest extant example of his son's painting. In contrast, a number of art historians have noted that his mother's portrait contains bland passages, especially around the eyes and may be a near-contemporary copy of a lost original.[18] Lotte Brand Philip believes the clumsiness in the mother panel indicated that Dürer painted it first, as a youthful attempt at portraiture, and that Albrecht the Elder might have later "commissioned" his own portrait to pair with Barbara's.[19] Recent technical examination of the two panels confirms that Barbara's portrait was painted later than her husband's.[17]
 Barbara Holper was the daughter of Hieronymus Holper, under whom Albrecht the Elder served his apprenticeship as a goldsmith.[20] The two men became friends, and when she came of age Holper gave his daughter into marriage when Dürer senior was 40 and she was 15.[18] The couple were compatible, well-matched and fond of each other. Yet their son's writings detail their difficult lives and many setbacks; three of their 18 children survived into adulthood – 17 of whom had been born by the time of this portrait.[21] After her husband died Barbara was destitute and went to live with her son.[22] After she in turn died in 1514, her son wrote "This my pious Mother ... often had the plague and many other severe and strange illnesses, and she suffered great poverty, scorn, contempt, mocking words, terrors, and great adversities. Yet she bore no malice. Also she died hard ... I felt so grieved for her that I cannot express it."[23]
 Barbara is shown wearing a red dress and a matte white bonnet which fully covers her hair, indicating her marital status.[14] Her headdress is draped with a long scarf or train which stretches down her neck and across her left shoulder, contrasting in colour and shape against the black head-wear of her husband. The lines of her face contain touches of white paint to give a highlighting and enlivening effect; they are especially evident around her eyes, the bridge of her nose and around her upper lip.[17] Barbara was attractive in her youth; her son described her as having been "comely and of erect bearing".[24][D] However, by the time of this portrait the effects of time and losing so many children weigh heavily on her face.[13] The panel was grounded with white paint, while the composition seems to have changed significantly from the imprimatura. Faint traces of the original figuration are visible in parts of the background and in the darkened areas of her hood.[7] At some point the panel was cut down at the left side, shifting the compositional balance and removing a portion of her shoulder and headdress.[13]
 The panel closely resembles Dürer's 1514 drawing Portrait of the artist's mother at the age of 63.[26] David Price writes of the drawing's "rough depiction of her flesh emaciated by old age", and the "existential piety in the cast of Barbara Dürer's right eye, which, almost unnaturally, directs her vision heavenward". Although Barbara is some 25 years older in the later drawing and by then terminally ill, the facial resemblance and pose are unmistakable.[16] However, there is a dramatic difference between the relatively young looking 39-year-old woman in the diptych and the aged and terminally ill widow in the 1514 drawing. A family resemblance can be detected between both works and Dürer's Vienna self-portrait; most evidently around the mouth. Kemperdick concludes that these passages are so closely modeled that it is reasonable to presume they were drawn by the same hand, but is conservative in saying definitively that the sitters were related by blood, as Dürer was very young, and though precociously talented, to some extent still reliant on facial "types".[17]
 Dürer's father wears a dark shirt, russet coat and a black hat lined with fur. His skin is slack at the mouth and chin, and he has small, intelligent eyes,[1] which Von Fircks describes as "dark and serious". Their curves echo those of the heavy lids beneath.[10] His eyes are lined with crow's feet and shadowed with brown hatched paint. His facial features are built from brush strokes more typical of drawing than painting—at this young age Dürer was a far more skilled draughtsman than painter.[27] Technical analysis carried out in 2013 by Dagmar Hirschfelder revealed a detailed background which was over-painted by the artist.[28] The abandoned interior space consisted of a corner of a room with an arched window looking out to a countryside view. This type of interior can be traced to the Netherlandish tradition, and is rare in German portraiture of the period.[29]
 Albrecht the Elder's lips are thin and tightly pursed and his mouth is broad and down-turned, yet his features are those of a handsome man.[27] Marcel Brion described him as appearing "mild and thoughtful", an impression reinforced by the uncomplicated design of the painting.[30] This view is reinforced by the relative drabness or simplicity of his clothes, which seem intended to convey a reserved, ascetic piousness. Dürer presents his father more like a low-ranking ecclesiastic than a tradesman: a calm, considerate and straightforward man dressed up in his best, albeit modest, clothes.[31] After his father's death in 1502, Dürer wrote that Albrecht the Elder "passed his life in great toil and stern hard labour, having nothing for his support save what he earned with his hand for himself, his wife and his children ... He underwent manifold afflictions, trials and adversities. But he won just praise from all who knew him ... he was also of few words, and was a God-fearing man."[32]
 Martin Conway describes the portrayal of a dignified man marked by a grave expression and deep "furrows ploughed by seventy years of labour and sorrow".[33] Conway believed the strength of the portrait is in part achieved through Dürer's ability to convey this hardship, while at the same time presenting a man still imbued with traces of pride, and possessing "a kind old face". Noting the obvious affection between the father and son as well as the half smile of the older man, he wonders if that grin might have been born of Albrecht the Elder's satisfaction that his toil has been rewarded by a son of such talent, who was now about to set out on the world for his wanderjahr.[33] A contributing factor to this pride is that Albrecht the Elder trained his son in his own profession, as a jeweller, but at one point came to regret the choice of apprenticeship as the younger man was so obviously suited to drawing and painting. However, his son learned many skills during that period and it gave him a discipline with his hands that became a defining factor of his work, especially in his ability as an engraver.[34]
 Conway described the panel as indicating "an astonishing depth of psychological insight" for an artist not yet 20 years old.[31] In its simple design and detail it shares many of the characteristics of, and is a key precursor to, Dürer's mature work. He portrayed his father again in 1497, when the older man would have been around 70. He has aged noticeably in the intervening seven years: his skin is saggier, the wrinkles deeper and more pronounced. In this later portrait Dürer seems at pains to convey these effects of ageing, which are all too evident on his father's face.[35] According to Brion, his eyes have lost their "distant, mystic" appearance and now seem less contented.[30] Conway agrees, seeing traces of haggardly agitation, but holds that the two paintings convey a similar overall highly favourable and compassionate impression of the man.[33]
 Each canvas is mounted on two boards of equal width, which have been cut vertically and appear have come from the same tree. Tree-ring dating of the wood suggests it was felled around 1482. Wood intended for use in panel painting was generally allowed to mature for around 10 years, giving an approximate dates of 1490–92 for the works. The panels are covered with canvas made of fine but loosely woven linen, and underpainted in white paint. Whether Dürer prepared the panels himself or purchased them already prepared is unknown.[7]
 The father panel is signed and dated on both sides, in what may be the oldest extant instance of Dürer's signature monogram of a large open A and small d.[27] However this inscription and the 1490 date are later additions.[36][37] The Florence canvas is in relatively poor condition: retouching has largely removed Dürer's top layer of brushwork and left the paint hard and dry.[27] Barbara's panel underwent a restoration in 1974, when the surface was cleaned and partially revarnished, with repairs to damage on the reverse sustained from woodboring.[7]
 The reverse of Albrecht's panel contains a rendition of the allied Dürer and Holper families' coats of arms,[38] which are shown beneath a winged Moor wearing a red dress.[39] The Dürer family are represented by a crest showing an open door, a pun on the word Dürer (Thürer meaning 'doormaker'). Albrecht the Elder was born in the Hungarian village of Ajtó. Ajtos is Hungarian for "door"—when his parents moved to Germany their name changed from Tür to Düre.[40][E][20] The Holper crest features a stag, but its significance is lost.[42] The back of Barbara's panel contains imagery of a dragon in a lightning storm set against a rocky landscape or cliff.[7]
 After the painter's death in 1528, the portraits were held by his brother, and then his brother's widow before they passed into the collection of Willibald Imhoff, a grandson of Dürer's friend Willibald Pirckheimer. Inventories from the Imhoff collection from 1573 to 1574, 1580 and 1588 list both panels. The next surviving Imhoff inventory, of 1628, again lists the mother's portrait, but it disappears after a mention in the 1633–58 account books of Hans Hieronymus Imhoff, after which its whereabouts became unknown.[43] Dürer expert Matthias Mende described the missing portrait of Barbara Holper as "among the most severe losses in the Dürer oeuvre".[44]
 In 1977, art historian Lotte Brand Philip proposed that Unknown Woman in a Coif, held by the Germanisches Nationalmuseum in Nuremberg, was the original portrait of Barbara Holper. The Nuremberg panel was previously thought to have originated from a member of Wolgemut's workshop, a Franconian artist in his circle, or the anonymous Mainz painter Master W. B.[22] Brand Philip's attribution was based on striking similarities in composition and its shared tone, theme, and size with the father panel at the Uffizi. In both works the sitters are holding rosary beads, and Dürer attentively describes their hands. Both portraits show the sitter in the same pose, against a similarly coloured background.[45] Both are lit from the upper left. The boards are identically cut in width and depth, although 3 cm was removed from the left edge of Barbara's panel.[44] Brand Philip noted the similarities between the panel and Dürer's 1514 charcoal drawing Portrait of the Artist's Mother at the Age of 63.[21] Fedja Anzelewsky agreed with the attribution, noting that both portraits bear, on their reverse, the catalogue number recorded in the Imhoff inventories, as well as "precisely the same design of masses of dark clouds".[24]
 Anzelewsky speculated that the father's portrait, which was not listed in the 1628 Imhoff inventory, had been broken off and sold to Rudolph II of Austria.[15] Hans Hieronymus Imhoff's lukewarm description of Barbara's portrait—"the mother of Albrecht Dürer in oil colors on wood, [but] there are many who do not believe it to be a work of Dürer"—led Brand Philip to conclude that Albrecht's panel was likely sold individually as the more accomplished and marketable of the two.[46] The attribution is widely accepted today. In 2013 Stephan Kemperdick noted the sophistication of the Nuremberg portrait and that its three-dimensional modeling of the head displays a level of skill beyond Wolgemut and his circle.[17]
 The two panels were reunited in 2012 during a Dürer exhibition in Nuremberg having been separated since sometime between 1588 and 1628.[7]
 1 Sources and influences 2 Description

2.1 Barbara Holper
2.2 Albrecht Dürer the Elder
2.3 The panels

 2.1 Barbara Holper 2.2 Albrecht Dürer the Elder 2.3 The panels 3 Provenance and attribution 4 References

4.1 Notes
4.2 Citations
4.3 Sources

 4.1 Notes 4.2 Citations 4.3 Sources 5 External links ^ Dürer portrayed his father again in 1497,[4] and drew his mother in 1514.[5]
 ^ The Nuremberg panel, however, was thought to be a near contemporary copy of a lost original.[6]
 ^ Albrecht the Elder was born in 1427, but the exact date is not known; he would have been 63 if born early in the year.[12]
 ^ Or as a "pretty upright girl" depending on the translation.[25]
 ^ The signboard on Albrecht the Elder's workshop showed an open door.[41]
 ^ a b Thausing (2003), 45
 ^ Campbell Hutchison (2000), 186
 ^ a b Brand Philip & Anzelewsky (1978–79), 14
 ^ Conway (1918), 142
 ^ Brion (1960), 16
 ^ Bailey (1995), 36
 ^ a b c d e f g "Albrecht Dürer, Barbara Dürer, geb. Holper (recto) " (in German). Germanisches Nationalmuseum. Retrieved 31 October 2013.
 ^ Brion, 17, 45. Brion (1960) describes Dürer's early "excessive devotion" to van der Weyden as delaying his "inauguration [of] a new era in German painting".
 ^ Panofsky, Erwin, 1943; quoted in Brion, 17
 ^ a b c d Von Fircks (2011), 419
 ^ Hunter (1993), 207–218
 ^ Bailey (1995), 36
 ^ a b c d Brand Philip & Anzelewsky (1978–79), 10
 ^ a b Bartl (1999), 26–31
 ^ a b Brand Philip & Anzelewsky (1978–79), 5–18
 ^ a b Price (2003), 22
 ^ a b c d e Kemperdick (2013), 95
 ^ a b Brand Philip & Anzelewsky (1978–79), 5
 ^ Brand Philip & Anzelewsky (1978–79), 17
 ^ a b Brion (1960), 16
 ^ a b Brand Philip & Anzelewsky (1978–79), 11
 ^ a b Kemperdick (2013), 94
 ^ Sturge Moore (1905), 71
 ^ a b Brand Philip & Anzelewsky (1978–79), 12
 ^ Brand Philip (1978–79) , 12
 ^ Tatlock (2010), 116
 ^ a b c d Allen (2005), 21
 ^ Sander (2013), 18
 ^ Kemperdick (2013), 98
 ^ a b Brion (1960), 20
 ^ a b Brion (1960), 19
 ^ Sturge Moore (1905), 36
 ^ a b c Conway (1889), 35
 ^ Conway (1889), 36
 ^ Mills (1991), 747
 ^ Conway (1918), 142
 ^ Kemperdick (2013), 99
 ^ Campbell Hutchison (2002), 238
 ^ Thausing (2003), 46
 ^ Bailey (1995), 36
 ^ Brion (1960), 16
 ^ Allen (2005), 22
 ^ Campbell Hutchison (2000), 209
 ^ a b Brand Philip & Anzelewsky (1978–79), 7
 ^ Campbell Hutchison (1990), 26
 ^ Brand Philip & Anzelewsky (1978–79), 6
 Allen, L. Jessie. Albrecht Dürer. Whitefish, MT: Kessinger Publishing, 2005. .mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}ISBN 0-7661-9475-2 Anzelewsky, Fedja. Albrecht Dürer, das Malerische Werk. Berlin: Deutscher Verlag für Kunstwissenschaft, 1971 Bailey, Martin. Dürer. London: Phaidon Press, 1995. ISBN 0-7148-3334-7 Bartl, Anna.  "Ein Original von Albrecht Dürer? Technologische Untersuchung eines in der Forschung umstrittenen Gemäldes". Restauro: Zeitschrift für Kunsttechniken, Restaurierung und Museumsfragen, Volume 105, No 1, 1999. 26–31. ISSN 0933-4017 Brand Philip, Lotte; Anzelewsky, Fedja. "The Portrait Diptych of Dürer's parents". Simiolus: Netherlands Quarterly for the History of Art, Volume 10, No. 1, 1978–79. 5–18 Brion, Marcel. Dürer. London: Thames and Hudson, 1960 Campbell Hutchison, Jane. Albrecht Dürer: A Guide to Research. New York: Garland, 2000. ISBN 0-8153-2114-7 Conway, Martin. Literary remains of Albrecht Dürer. Cambridge: Cambridge University Press, 1889. Conway, Martin. "Dürer Portraits, Notes". The Burlington Magazine for Connoisseurs, Volume 33, No. 187, October 1918. 142–143 Hunter, John. "Who Is Jan van Eyck's 'Cardinal Nicolo Albergati'?". The Art Bulletin, Volume 75, No. 2, June 1993. 207–218 Kemperdick, Stephan. "Rendered Accouring to My Own Image". In: Dürer: His Art in Context. Sander, Jochen (ed). Frankfurt: Städel Museum & Prestel, 2013. ISBN 3-7913-5317-9 Mills, John FitzMaurice. "The Scope of Albrecht Dürer's Work". RSA Journal, Volume 139, No. 5423, 1991. 745–752 Price, David. Albrecht Dürer's Renaissance: Humanism, Reformation and the Art of Faith. Ann Arbor: University of Michigan Press, 2003. ISBN 0-472-11343-7 Sander, Jochen. "Dürer in Frankfurt". In: Dürer: His Art in Context. Frankfurt: Städel Museum & Prestel, 2013. ISBN 3-7913-5317-9 Stumpel, Jeroen. "Dürer. Vienna". The Burlington Magazine, Volume 146, No. 1210, 2004. 63–65 Sturge Moore, Thomas. Albert Dürer. 1905. Whitefish, MT: Kessinger Publishing, 2004. ISBN 1-4191-0533-7 Tatlock, Lynne. Enduring Loss in Early Modern Germany. Brill Academic Publishers, 2010. ISBN 90-04-18454-6 Thausing, Moritz. Albert Dürer: His Life and Work, Part 1. 1882. Whitefish, MT: Kessinger Publishing, 2003. ISBN 0-7661-5416-5 Von Fircks, Juliane. "Albrecht Dürer the Elder with a Rosary". In: Van Eyck to Dürer. Borchert, Till-Holger (ed). London: Thames & Hudson, 2011. ISBN 978-0-500-23883-7 Albrecht Dürer the Elder at the Uffizi Gallery. v t e List of paintings Portrait Diptych of Dürer's Parents (1490) Portrait of Frederick III of Saxony (1496) St. Jerome in the Wilderness (c. 1496) Portrait of Dürer's Father at 70 (1497) Haller Madonna/Lot and His Daughters (c. 1498) Lamentation of Christ (attributed, c. 1498) Portrait of Oswolt Krell (1499) Portrait of Elsbeth Tucker (1499) Seven Sorrows Polyptych (c. 1500) Lamentation of Christ (c. 1500) Adoration of the Magi (1504) Bagnacavallo Madonna (before 1505) Christ among the Doctors (1506) Portrait of a Young Venetian Woman (1505) Feast of the Rosary (1506) Avarice (1507) Adam and Eve (1507) Martyrdom of the Ten Thousand (1508) Life of the Virgin (1510–1511) The Suicide of Lucretia (1518) Portrait of Emperor Maximilian I (1519) Portrait of Jakob Fugger (c. 1520) Portrait of a Man (1521 or 1524) Portrait of Bernhart von Reesen (1521) Saint Jerome in His Study (1521) The Four Apostles (1526) Portrait of Hieronymus Holzschuher (1526) Portrait of Jakob Muffel (1526) Virgin of the Pear (1526) Self-Portrait at the Age of 13 (drawing, 1484) Self-Portrait c 1492 (drawing, 1492) Portrait of the Artist Holding a Thistle (1493) Self-Portrait at 26 (1498) Self-Portrait at 28 (1500) List of woodcuts, engravings The Holy Family with the Dragonfly (1495) The Holy Family with Three Hares (1496) Apocalypse (1498) Saint Michael Fighting the Dragon (1498) The Four Witches (1497) The Sea Monster (1498–1500) Saint Sebastian at the Column (1500) Visitation (1503) Adam and Eve (1504) Joachim and Anne Meeting at the Golden Gate (1504) Knight, Death and the Devil (1513) Melencolia I (1514) Saint Jerome in His Study (1514) Triumphal Arch (1515) Rhinoceros (1515) Large Triumphal Carriage (1522) Portrait of Erasmus (1526) Young Hare (1502) Great Piece of Turf (1503) Madonna of the Animals (c. 1503) Praying Hands (c. 1508) Wing of a European Roller (1512) Portrait of the Artist's Mother at the Age of 63 (1514) Head of a Walrus (1514) Dresden Altarpiece (c. 1496–1497/1503–1504) Paumgartner Altarpiece (c. 1500) Jabach Altarpiece (c. 1503–1504) Heller Altarpiece (c. 1508) Adoration of the Trinity (1511) Albrecht Dürer's House Agnes Dürer (wife) Albrecht Dürer the Elder (father) Hans Dürer (brother) Anton Koberger (godfather) Diptychs Portraits by Albrecht Dürer 1490s paintings Paintings of the Uffizi Collections of the Germanisches Nationalmuseum Articles with short description Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Español Français Italiano Polski Русский  This page was last edited on 9 August 2019, at 16:45 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Portrait Diptych of Dürer's Parents Dürer's Parents with Rosaries ^ ^ ^ ^ ^ a b ^ a b ^ ^ ^ a b c d e f g ^ ^ a b c d ^ ^ a b c d a b a b a b a b c d e a b ^ a b a b a b ^ a b ^ ^ a b c d ^ ^ a b a b ^ a b c ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ 
List of paintings
Portrait Diptych of Dürer's Parents (1490)
Portrait of Frederick III of Saxony (1496)
St. Jerome in the Wilderness (c. 1496)
Portrait of Dürer's Father at 70 (1497)
Haller Madonna/Lot and His Daughters (c. 1498)
Lamentation of Christ (attributed, c. 1498)
Portrait of Oswolt Krell (1499)
Portrait of Elsbeth Tucker (1499)
Seven Sorrows Polyptych (c. 1500)
Lamentation of Christ (c. 1500)
Adoration of the Magi (1504)
Bagnacavallo Madonna (before 1505)
Christ among the Doctors (1506)
Portrait of a Young Venetian Woman (1505)
Feast of the Rosary (1506)
Avarice (1507)
Adam and Eve (1507)
Martyrdom of the Ten Thousand (1508)
Life of the Virgin (1510–1511)
The Suicide of Lucretia (1518)
Portrait of Emperor Maximilian I (1519)
Portrait of Jakob Fugger (c. 1520)
Portrait of a Man (1521 or 1524)
Portrait of Bernhart von Reesen (1521)
Saint Jerome in His Study (1521)
The Four Apostles (1526)
Portrait of Hieronymus Holzschuher (1526)
Portrait of Jakob Muffel (1526)
Virgin of the Pear (1526)
  
Self-Portrait at the Age of 13 (drawing, 1484)
Self-Portrait c 1492 (drawing, 1492)
Portrait of the Artist Holding a Thistle (1493)
Self-Portrait at 26 (1498)
Self-Portrait at 28 (1500)
 
List of woodcuts, engravings
The Holy Family with the Dragonfly (1495)
The Holy Family with Three Hares (1496)
Apocalypse (1498)
Saint Michael Fighting the Dragon (1498)
The Four Witches (1497)
The Sea Monster (1498–1500)
Saint Sebastian at the Column (1500)
Visitation (1503)
Adam and Eve (1504)
Joachim and Anne Meeting at the Golden Gate (1504)
Knight, Death and the Devil (1513)
Melencolia I (1514)
Saint Jerome in His Study (1514)
Triumphal Arch (1515)
Rhinoceros (1515)
Large Triumphal Carriage (1522)
Portrait of Erasmus (1526)
 
Young Hare (1502)
Great Piece of Turf (1503)
Madonna of the Animals (c. 1503)
Praying Hands (c. 1508)
Wing of a European Roller (1512)
Portrait of the Artist's Mother at the Age of 63 (1514)
Head of a Walrus (1514)
 
Dresden Altarpiece (c. 1496–1497/1503–1504)
Paumgartner Altarpiece (c. 1500)
Jabach Altarpiece (c. 1503–1504)
Heller Altarpiece (c. 1508)
Adoration of the Trinity (1511)
 
Albrecht Dürer's House
 
Agnes Dürer (wife)
Albrecht Dürer the Elder (father)
Hans Dürer (brother)
Anton Koberger (godfather)
 