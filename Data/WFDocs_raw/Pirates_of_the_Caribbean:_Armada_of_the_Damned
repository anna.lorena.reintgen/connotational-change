
 Pirates of the Caribbean: Armada of the Damned is a cancelled action-adventure role-playing video game that was being developed by Propaganda Games for the Xbox 360, PlayStation 3, and Microsoft Windows platforms. Originally set to be published by Disney Interactive Studios, it was the first attempt to create an open world game based on the Pirates of the Caribbean film franchise. The project was cancelled in October 2010, soon before the closure of Propaganda Games.
 Set before events of The Curse of the Black Pearl, the game was to follow James Sterling, a pirate captain whose main mission was to travel across the Caribbean Sea to make a reputation for himself. Although little was unveiled about the story, it was intended to be independent from the films' main arc and include new characters. Gameplay was to have emphasized role-playing elements, including real-time combat and weapon customization.
 After its announcement at 2009's Electronic Entertainment Expo, the game received positive responses from most video game journalists. Daemon Hatfield from IGN thought it "a promising action RPG",[1] while GamesRadar praised the gameplay, comparing it to the Assassin's Creed franchise. Journalists were disappointed when Disney cancelled the game several months before its planned release date. Ubisoft, the publisher of Assassin's Creed, later released a similar game called Assassin's Creed IV: Black Flag. 
 Armada of the Damned was conceived as an action-adventure role-playing video game played from a third-person perspective and set in an open world environment based on the Pirates of the Caribbean universe. The player was to take the role of James Sterling,[2] a pirate captain whose main mission was to travel across the Caribbean Sea and make a reputation for himself. Some of Sterling's features could be directly customized by the player, although Armada of the Damned was centered on a choice system that would affect the character's appearance, personality, weapons, attacks, quests, and story developments.[3]
 Although most of the character's specifications could be customized, some elements were to be determined by the game's two character types, Legendary and Dreaded. Choosing a type at the beginning of the game affected several gameplay and story elements. Each character type had unique features that shaped the way the game was experienced, including how the environment and non-playable characters interacted with the player.[3]
 Combat in Armada of the Damned was split between land and sea. Sterling had a light and a heavy attack, which could be combined to create combos that increased the damage he inflicted. Correct timing lead to a bonus attack at the end of the combo that improved its strength. Sterling could also curse his opponents. The curse was a spell that weakened all surrounding enemies when it was used as a finishing move. All attacks, special moves, and combinations varied depending of the pirate type chosen and could be upgraded at will.[3]
 In the game, the player was given a customizable ship named the Nemesis, commanded by Sterling and his crew.[4] The player's choices in the game would determine the crew they could hire, which in turn affected the ship's attributes. The vessel could be used to explore the Caribbean Sea and battle other crafts. During combat, the player could maneuver the ship, fire the cannons, or board the enemy vessel for hand-to-hand combat. If the enemy craft was boarded and the enemy crew was eliminated, the player received more loot than if it was destroyed. Loot salvaged in these battles could be sold in markets.[5]
 Little information was revealed about Armada of the Damned's plot. Sterling had no connection with the film series' characters and story arcs. The game and the films shared the same universe, but the former was to take place before the events of the latter.[3] Sterling, raised in a poor family, was a young adventurer with dreams of fame and fortune who decided to become a pirate. Although he was killed on his first voyage, Sterling was revived by supernatural forces which gave him a second chance.[3] After these events, the player had the option to follow two paths: become either a Legendary or a Dreaded pirate. On the Legendary path, the player would be a well-respected pirate who acted in good faith. On the Dreaded path, the player would be mysterious and feared.[1]
 Propaganda Games, a studio founded in 2005 and owned by Disney Interactive Studios, developed Armada of the Damned. The studio was established by former employees of EA Canada and focused on the development of action-adventure games for Disney.[7] Propaganda's first game, Turok (2008), became a commercial success, selling more than one million copies.[8] After Turok's release, the studio began working on a project for the Pirates of the Caribbean franchise. Propaganda's vice president and general manager Dan Tudge said that this was because "it is a universe fans will be dying to explore".[6] The studio revealed that they worked on several scripts to improve the gameplay focus. Armada of the Damned included an interactive choice system to shape the game, making choices an important feature.[5][9]
 According to game director Alex Peters, Armada of the Damned's characters were developed unattached from those that appear in the film series. "We were very clear that we didn't want to be associated with being a movie game", he commented. This desire led to the creation of James Sterling, a character that would fit the studio's needs and feel familiar to the characters featured in the films.[2] An original music score was written for Armada of the Damned, while the musical themes from the films were "treated tastefully and only used on occasion."[3] After the game's cancellation, the score was used in Lego Pirates of the Caribbean: The Video Game, which was released in 2011.[10]
 During Armada of the Damned's development, Propaganda Games also worked on Tron: Evolution, which was released in December 2010.[11]  In October 2010, Disney Interactive Studios announced that Armada of the Damned's development team would be laid off as part of a restructuring program. However, Propaganda would finish development of Tron: Evolution and its post-launch downloadable content.[12]
 The restructuring reduced the studio's staff by more than 100 people and led to the cancellation of Armada of the Damned. The remaining development team worked to finish Tron: Evolution, whose team was also affected by the lay offs.[12] However, after the game failed to attain critical or commercial success, Disney cancelled its planned downloadable content and closed Propaganda Games.[13]
 After its announcement at 2009's Electronic Entertainment Expo,[14] Armada of the Damned was met with positive reactions from most video game journalists. Daemon Hatfield from IGN named it "a promising action RPG," elaborating that "even though Armada of the Damned uses the Pirates of the Caribbean license ... [it] is its own game, an adventure that lets players create their own pirate and wander the seas of the Caribbean seeking fame and fortune."[1] Chris Antista from GamesRadar praised its gameplay, comparing it with Assassin’s Creed. He stated that "the game has spectacularly preserved the spirit of the films, and they’ve done it without parasitically clinging to moments you’ve already seen on the big screen."[5]
 An editor from the Official Xbox Magazine speculated that since the first Pirates of the Caribbean film was "followed by a progressive descent into mediocrity and Krakens with twirly tentacles in the sequels, any game that's set before the first movie is likely to be amazing."[15] Matt Miller from Game Informer was impressed with the game, praising its Mass Effect-like speech and combat systems. He commented that "we didn't go into our meeting for Pirates with great expectations, but came out pleasantly amazed at the potential of the game."[4] Joystiq's Mike Schramm compared the game to Fable, noting that "what is there looks good – the combat was solid, if a little shallow, and the graphics and polish are well on their way."[16]
 Journalists expressed disappointment when Disney reduced the staff at Propaganda Games and cancelled development of the game several months before its planned release date. IGN's Ryan Clements stated that "it's a shame that [Armada of the Damned's] potential won't be realized at this point in time."[17] Justin Towell, writing for GamesRadar, was frustrated by Disney's decision: "It makes no sense to completely abandon work on a game that's clearly not that far off completion."[18] Game Informer's Jeff Marchiafava also expressed sadness about the cancellation, saying "What kind of noise does a depressed pirate make? Because we would totally be making that noise right now."[19]
 1 Gameplay 2 Plot 3 Development 4 Cancellation 5 Reception 6 References ^ a b c Hatfield, Daemon (May 18, 2010). "E3 2010: Pirates of the Caribbean – Armada of the Damned Update". IGN. Ziff Davis. Archived from the original on July 9, 2013. Retrieved June 1, 2017..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b Talbot, Ben (February 11, 2010). "Pirates of the Caribbean: Armada of the Damned". GamesRadar. Future plc. Archived from the original on July 9, 2013. Retrieved June 27, 2013.
 ^ a b c d e f Clements, Ryan (May 10, 2010). "First Look at Pirates of the Caribbean: Armada of the Damned". IGN. Ziff Davis. Archived from the original on August 31, 2012. Retrieved June 29, 2013.
 ^ a b Miller, Matt (June 22, 2010). "Pirates Offers More Than Expected". Game Informer. GameStop. Archived from the original on July 9, 2013. Retrieved June 29, 2013.
 ^ a b c Antista, Chris (May 13, 2010). "5 ways Pirates of the Caribbean: Armada of the Damned is avoiding the movie-licensed shitstorm". GamesRadar. Future plc. Archived from the original on July 9, 2013. Retrieved June 27, 2013.
 ^ a b Pfister, Andrew. "Pirates of the Caribbean: Armada of the Damned Interview: Propaganda Games' Alex Peters". G4. G4 Media, LLC. Archived from the original on January 12, 2013. Retrieved June 27, 2013.
 ^ Kawamoto, Dawn (April 19, 2005). "Disney scoops up Avalanche, founds new studio". GameSpot. CBS Interactive. Archived from the original on July 9, 2013. Retrieved June 27, 2013.
 ^ "Turok's Dinosaurs Run Rampant with More Than 1 Million Units Shipped Worldwide". IGN. Ziff Davis. March 26, 2008. Archived from the original on October 16, 2012. Retrieved June 27, 2013.
 ^ Goldstein, Hilary (May 26, 2009). "Pre-E3 2009: Pirates of the Caribbean: Armada of the Damned Unveiled". IGN. Ziff Davis. Archived from the original on October 25, 2012. Retrieved June 27, 2013.
 ^ Citizen, Jessica (May 13, 2011). "LEGO Pirates uses music from axed Armada of the Damned". VG247. Videogaming247 Ltd. Archived from the original on May 16, 2011. Retrieved June 29, 2013.
 ^ Magrino, Tom. "Tron: Evolution begins Dec. 7". GameSpot. CBS Interactive. Archived from the original on November 7, 2012. Retrieved June 27, 2013.
 ^ a b Crecente, Brian. "Pirates of the Caribbean Game Canned as Layoffs Hit Propaganda Confirmed". Kotaku. Gawker Media. Archived from the original on June 15, 2013. Retrieved June 27, 2013.
 ^ Gilbert, Ben. "Propaganda Games shuttered". Joystiq. AOL Inc. Archived from the original on November 18, 2012. Retrieved June 27, 2013.
 ^ "Disney Interactive Studios Announces Pirates of the Caribbean: Armada of the Damned". IGN. Ziff Davis. May 27, 2009. Archived from the original on July 9, 2013. Retrieved June 29, 2013.
 ^ "Pirates of the Caribbean: Armada of the Damned". Official Xbox Magazine. Future plc. Archived from the original on July 9, 2013. Retrieved June 29, 2013.
 ^ Schramm, Mike (June 20, 2010). "Preview: Pirates of the Caribbean: Armada of the Damned". Joystiq. AOL Inc. Archived from the original on June 25, 2010. Retrieved June 29, 2013.
 ^ Clements, Ryan (October 14, 2010). "Pirates of the Caribbean: Armada of the Damned Cancelled". IGN. Ziff Davis. Archived from the original on July 9, 2013. Retrieved June 29, 2013.
 ^ Towell, Justin (October 15, 2010). "It's a crying shame that the new Pirates of the Caribbean game has been cancelled. Seriously". GamesRadar. Future plc. Archived from the original on July 9, 2013. Retrieved June 29, 2013.
 ^ Marchiafava, Jeff (October 14, 2010). "Disney Confirms Armada Of The Damned Cancellation". Game Informer. GameStop. Archived from the original on July 9, 2013. Retrieved June 29, 2013.
 v t e The Curse of the Black Pearl Dead Man's Chest At World's End On Stranger Tides Dead Men Tell No Tales‎ Cast Accolades Jack Sparrow Hector Barbossa Will Turner Elizabeth Swann Joshamee Gibbs James Norrington Davy Jones Pintel and Ragetti Bootstrap Bill Cutler Beckett Tia Dalma Weatherby Swann Black Pearl Flying Dutchman Queen Anne's Revenge Kraken Locations 1966 soundtrack The Curse of the Black Pearl Dead Man's Chest Swashbuckling Sea Songs Soundtrack Treasures Collection At World's End
Remixes Remixes On Stranger Tides Dead Men Tell No Tales List of songs in the film series "Yo Ho (A Pirate's Life for Me)" "He's a Pirate" Pirates of the Caribbean Battle for the Sunken Treasure The Legend of Captain Jack Sparrow Pirate's Lair on Tom Sawyer Island Mickey's Pirate and Princess Party  The Curse of the Black Pearl Pirates of the Caribbean Pirates of the Caribbean Multiplayer Mobile Dead Man's Chest The Legend of Jack Sparrow At World's End Pirates of the Caribbean Online Armada of the Damned Lego Pirates of the Caribbean: The Video Game Jack Sparrow Legends of the Brethren Court The Price of Freedom Trading Card Game Pinball machine Lego Pirates of the Caribbean Rob Kidd  Book  Category Cancelled PlayStation 3 games Cancelled Windows games Cancelled Xbox 360 games Open world video games Pirates of the Caribbean video games Video games developed in Canada Featured articles Articles using Infobox video game using locally defined parameters Articles using Wikidata infoboxes with locally defined images Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Azərbaycanca Français Hrvatski Русский  This page was last edited on 25 September 2019, at 20:31 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Pirates of the Caribbean: Armada of the Damned a b c a b a b c d e f a b a b c a b ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ —Dan Tudge, Propaganda Games' vice president and general manager.[6] —Dan Tudge, Propaganda Games' vice president and general manager.[6]  Propaganda Games Disney Interactive Studios Pirates of the Caribbean Microsoft Windows, PlayStation 3, Xbox 360 Cancelled Action-adventure, role-playing Single-player 
The Curse of the Black Pearl
Dead Man's Chest
At World's End
On Stranger Tides
Dead Men Tell No Tales‎
Cast
Accolades
  
Jack Sparrow
Hector Barbossa
Will Turner
Elizabeth Swann
Joshamee Gibbs
James Norrington
Davy Jones
Pintel and Ragetti
Bootstrap Bill
Cutler Beckett
Tia Dalma
Weatherby Swann
 
Black Pearl
Flying Dutchman
Queen Anne's Revenge
Kraken
Locations
 Albums
1966 soundtrack
The Curse of the Black Pearl
Dead Man's Chest
Swashbuckling Sea Songs
Soundtrack Treasures Collection
At World's End
Remixes
On Stranger Tides
Dead Men Tell No Tales
Songs
List of songs in the film series
"Yo Ho (A Pirate's Life for Me)"
"He's a Pirate"
 
1966 soundtrack
The Curse of the Black Pearl
Dead Man's Chest
Swashbuckling Sea Songs
Soundtrack Treasures Collection
At World's End
Remixes
On Stranger Tides
Dead Men Tell No Tales
 
List of songs in the film series
"Yo Ho (A Pirate's Life for Me)"
"He's a Pirate"
 
Pirates of the Caribbean
Battle for the Sunken Treasure
The Legend of Captain Jack Sparrow
Pirate's Lair on Tom Sawyer Island
Mickey's Pirate and Princess Party
 
 The Curse of the Black Pearl
Pirates of the Caribbean
Pirates of the Caribbean Multiplayer Mobile
Dead Man's Chest
The Legend of Jack Sparrow
At World's End
Pirates of the Caribbean Online
Armada of the Damned
Lego Pirates of the Caribbean: The Video Game
 
Jack Sparrow
Legends of the Brethren Court
The Price of Freedom
 
Trading Card Game
Pinball machine
Lego Pirates of the Caribbean
Rob Kidd
 
 Book
 Category
 