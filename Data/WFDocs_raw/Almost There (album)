
 Almost There is the first studio album by the American Christian rock band MercyMe. Produced by Pete Kipley, it was released on August 14, 2001 by INO Records. After releasing six albums as an unsigned band, they decided to pursue a record contract because it became too difficult to sell albums, book shows, and manage themselves. The band was assigned to work with Kipley, who had not produced a major project before. Four songs on the album had previously appeared on their self-released albums; the rest were newly recorded songs. Critics have characterized the music on the album as contemporary worship and pop rock, with a more radio-friendly sound than the band's self-released albums.
 Almost There received critical acclaim from music critics, who praised the album's songwriting; "I Can Only Imagine" received particular compliments. Critics were more divided on the album's sound. Some felt the album was "innovative" or "fresh", while others felt it was middle–of–the–road or derivative. CCM Magazine listed it in their 25th anniversary edition as one of '100 Albums You Need to Own'.
 "Bless Me Indeed (Jabez's Song)" was released as the album's lead single; however, it underperformed on the charts, leading initially to poorer than expected sales for the album. The second single, "I Can Only Imagine", peaked at number one on the Radio & Records Christian AC chart in 2002. Its success contributed to a sharp increase in sales, and the song stayed on the Christian charts so long that plans for a third single from the album were scrapped. After the song crossed over to mainstream radio in 2003, the album peaked at number 39 on the Billboard 200 and number one on the Billboard Christian Albums chart. Billboard ranked it as the fourth best-selling Christian album of the 2000s in the United States. Almost There has been certified triple platinum by the Recording Industry Association of America (RIAA) and has sold over 3 million copies in the United States.
 MercyMe was formed in 1994 by vocalist Bart Millard, guitarist Mike Scheuchzer, and keyboardist Jim Bryson.[1] Bassist Nathan Cochran joined the band in 1997, with drummer Robby Shaffer joining the following year.[2] In October 1999, they issued their fifth self-released album, The Worship Project.[3] The album proved successful, selling over 60,000 copies within a year, but the difficulty of selling albums directly, in addition to having to book and manage for themselves, led the band to pursue a contract with a record label. Millard was directed by a friend to contact Jeff Moseley, who had connections in the Christian music industry, for advice. After being contacted by Millard, Moseley expressed interest in the band, and within a week MercyMe was officially signed to INO Records, a new record label Moseley was helming.[4] Moseley introduced the band to Pete Kipley, who would produce the album. Although Kipley had been involved on some minor projects like radio mixes, Almost There was his first major project. Millard called Kipley an "amazing guy" and credited him with improving the band's songwriting skills and teaching them about the music industry.[5]
 Four of the songs on Almost There ("Call to Worship", "Cannot Say Enough", "I Can Only Imagine", and "In You") had previously appeared on the band's self-released albums.[2] All of the other songs on the album were new, and had not appeared on any of their previous albums.[6] Millard and MercyMe wrote every song on the album, with the exception of "I Worship You", which was written by Kipley and Reggie Hamm.[7] Although the band wanted to write their own material, they said they liked this song so much that they wanted it to appear as the first track on the album.[8] "Bless Me Indeed (Jabez's Song)" was written at the request of the label, who wanted to capitalize off of the success of the popular Bruce Wilkinson book The Prayer of Jabez (2000). The band did not want to record the song, but eventually acquiesced.[9] According to Millard, the band had to "kind of fight" the label to have "House of God" included on the album; they insisted on including the song because they considered themselves a rock band and felt the label was pushing them too far towards the adult contemporary genre.[10]
 Almost There was recorded at Ivy Park, The Indigo Room, Paradise Sound, and IBC Studios. Kipley produced and programmed the record, while Skye McCaskey and Julian Kindred engineered the album. Salvo mixed a majority of the songs on the album at Cool Springs Studio with the exception of "In You", which was mixed by Shane Wilson. Strings were recorded by the Paltrow Performance Group.[7]
 Almost There has been described by critics as being a worship[11] and pop rock album.[12] The album was noted as being stylistically similar to contemporary Christian bands like FFH.[13] In contrast to the band's self-released albums, which had an "organic" feel, Almost There adopts a more radio-friendly musical style,[14] although the rock style of the band's self-released albums does occasionally resurface.[15] Steve Losey of Allmusic compared Scheuchzer's "guitar nuances" to U2's guitarist the Edge and described Bryson's keyboards as being "intense but subtle".[16]
 Losey described the album's first song "I Worship You" as "falling somewhere between adult contemporary and rock", and it utilizes acoustic guitars and "swirling" synthesizers.[16] "Here Am I" relates the story of people who are not being reached by Christians,[17] and "challenges the listener to go out into the world and stand up for their King".[13] "On My Way to You" is a worship song, requesting "wisdom, purity, and humility in our pursuit of holiness".[18] "How Great is Your Love" incorporates both string and electronic instruments;[13] Millard's vocals in the song utilize "effect–laden delays".[16] "I Can Only Imagine" is a ballad,[16][18] opening with just a piano before building to include drums and guitar.[19] Lyrically, it asks what it will be like in Heaven, standing before God.[16]
 "Bless Me Indeed (Jabez's Song)" is one of the fastest songs on the album. Lyrically, the song parallels the prayer of the Biblical character Jabez in 1 Chronicles 4:10, asking God for blessing and protection from evil.[20] "Cannot Say Enough" was described as "ambient" and compared to Third Day's "Your Love Oh Lord".[17] "House of God" was noted as being one of the album's more rock–oriented songs.[13][21][17] The song utilizes "driving" guitars[22] and a "nasty" guitar riff,[21] and invites the listener to enter the house of god.[13] "Call to Worship" is a mid–tempo song led by guitar; the song was compared to the work of the Cure.[17] The final two songs on the album, "All Fall Down" and "In You", are slower–paced songs, with the latter being led by piano and strings.[13]
 Almost There was released in the United States on August 14, 2001.[23] "Bless Me Indeed (Jabez's Song)" was released in 2001[24] as the album's lead single.[17] The label aimed to give the band a wider appeal by capitalizing on the success of the popular book The Prayer of Jabez.[25] The song debuted on the Radio & Records Christian AC chart on August 31, 2001 at number 29,[26] and spent four weeks on the chart, peaking at number 27.[27] The poor chart performance of the song led to album sales that were lower than anticipated.[25] The album debuted at number 12 on the Billboard Christian Albums chart on September 1, 2001,[28] and a week later entered the Billboard Heatseekers Albums chart, which ranks the top albums from artists who have not had an album enter the top 100 on the Billboard 200 chart, at number 39.[29]
 "I Can Only Imagine" was released to radio on October 12, 2001.[30] The song debuted on the Christian AC chart on November 2, 2001;[31] it reached the number one position on February 22, 2002[32] and spent two weeks at the top spot. It also peaked at number 15 on the Radio & Records Christian CHR chart.[31] As a result of the single's airplay, Almost There experienced a "surge" in sales.[33] The album debuted on the Billboard 200 at number 184 on December 22, 2001,[34] and entered the top ten on the Christian Albums chart on January 19, 2002, charting at number eight.[35] The album reached the peak of the Heatseekers Albums chart on February 2, 2002, and the following week entered the top 100 on the Billboard 200 chart, charting at number 98.[33] "How Great Is Your Love" was announced as the album's third single in an interview with Billboard magazine on February 12, 2002.[33] Millard had heavily pushed INO Records to release it as a single. However, "I Can Only Imagine" stayed on the Christian charts so long that by the time it fell off, the band had to begin work on their next record, and the song ultimately was not released to radio.[36] Almost There was certified Gold by the Recording Industry Association of America (RIAA) on June 13, 2002,[23] signifying shipments of over 500,000 copies.[37] More than a year after the album's release and following the release of the band's second studio album, Spoken For (2002), Almost There remained near the top of the Christian charts.[38] Almost There ranked as the 6th best–selling Christian album[39] and the 128th best–selling album of 2002 in the United States.[40]
 Sales of the album increased throughout 2003 as "I Can Only Imagine" received airplay on mainstream radio formats.[41] The song peaked at number five on the Billboard Adult Contemporary chart and number 71 on the Hot 100, also crossing over to the Mainstream Top 40, Adult Top 40, and Hot Country Songs charts.[31] The album was certified Platinum on July 14, 2003 by the RIAA; a month later, Almost There reached the top spot of the Christian Albums chart for the first time after 107 weeks on the chart.[42] It reached its peak of number 37 on the Billboard 200 on September 20, 2003.[43] It ranked as the 2nd–best selling Christian album[39] and 128th best–selling album of 2004.[44] By May 2004, Almost There had sold over 1.5 million copies,[45] and as of April 2006 it has sold over 2.2 million copies.[46] Almost There was certified double platinum on January 20, 2005 by the RIAA;[23] as of 2012, it is one of only eight Christian albums to have reached that milestone, with others including P.O.D.'s Satellite, Switchfoot's The Beautiful Letdown, and Casting Crowns' self–titled debut album.[47] In its 2000s decade–end charts, Billboard ranked Almost There as the fourth best–selling Christian album of the 2000s in the United States, behind only Satellite, The Beautiful Letdown, and Alan Jackson's Precious Memories.[48]
 Millard's story and the story of "I Can Only Imagine" were adapted into a film of the same name, which was released in March 2018. "I Can Only Imagine" returned to the charts following the film's release, with the song debuting at number two on the Billboard Christian Songs chart and spending a third week at number one on the Billboard Christian Digital Songs chart; the song had spent 425 weeks on the chart at that point, the longest run of any song in the chart's history.[49] The song later peaked at number one on the Christian Songs chart on March 31, 2018,[50] and spent three weeks at the top spot.[51][52][53] It also peaked at number 10 on the Billboard Digital Songs chart.[54] Almost There was certified triple platinum on June 15, 2018,[55] and "I Can Only Imagine" was certified quadruple platinum on September 27, 2019.[23] As of April 2019, it is best-selling Christian single of all-time.[56]
 Almost There received critical acclaim from music critics.[58] Critics praised the album's lyrical content, with particular compliments being given to "I Can Only Imagine".[59] Losey gave the album 4 out of 5 stars, praising it as "a disc that holds power" and being an "exception" rather than a "norm" in comparison to most other praise and worship releases. Losey also praised Scheuchzer and Bryson for their guitar work and keyboard performances, respectively.[16] Adam Woodroof of CCM Magazine gave it 3.5 out of 5 stars. While Woodroof felt the album did not deliver an innovative sound, he complemented it as offering "a breath of fresh air—and a sincerity sure to hold other artists wishing to dive into the genre accountable".[18] In a review for Charisma, Margaret Feinburg praised the album for incorporating songs from their previous self-released albums. She said that "MercyMe was stripped of much of its wonderful, organic, out–of–the box sound but given new life for radio airplay", and that "Overall, MercyMe is a band that deserves to be heard".[14] Kevin McNeese of New Release Today gave the album 5 out of 5 stars, calling it "another defining album" in praise and worship music. He particularly praised "I Can Only Imagine", calling it the album's highlight, but noted the other songs on the album were "penned with the same passion".[19]
 The J Man of Crosswalk.com gave Almost There a B, and said that "In the ever-growing genre of modern worship, MercyMe steps up to the plate and drives a home run over the fence". He praised the album as having a "fresh sound", but felt that much of the album was "somewhat low-key".[22] Kevin Chamberlain of Jesus Freak Hideout gave the album 4 out of 5 stars. Chamberlain praised the album as being "lyrically one of the best albums out there" and said that "Every song is based on some sort of Scripture or Spiritual truth seldom found in some Christian music" but felt the album's sound was average, comparing it to "FFH or any typical Contemporary Christian artist".[13]
 Megumi Nakamura of Cross Rhythms gave the album 7 out of 10 stars. She praised "I Can Only Imagine", but said that "little else on the album that matches the huge impact of that one song".[57] In a later review for the album's "Platinum Edition", Allan Clare gave it an 8 out of 10, saying the rest of the album aside from "I Can Only Imagine" was "bland".[21] Russ Breimeier of Christianity Today felt the album was "something of a mixed bag" and described the band's sound was "a little too mellow to be rock, and a little too heavy to be pop... their particular style doesn't stray from a middle of the road sound". Breimier described the album as not being particularly good or bad and offered a weak recommendation to fans of worship bands like By the Tree and Delirious?.[17]
 In its 25th anniversary edition, CCM Magazine listed Almost There as one of '100 Albums You Need to Own'.[60] In 2004, the magazine ranked "I Can Only Imagine" as the fourth-greatest song in Christian music.[61] At the 33rd GMA Dove Awards, "I Can Only Imagine" won the awards for Song of the Year and Pop/Contemporary Recorded Song of the Year.[62]
 (Credits and track list from the album liner notes)[7]
 (Credits from the album liner notes)[7]
 MercyMe
 Additional performers
 
 Technical and miscellaneous
 
 
 
 
 "Bless Me Indeed (Jabez's Song)"Released: 2001 "I Can Only Imagine"Released: October 12, 2001 1 Background and recording 2 Composition 3 Release and commercial performance 4 Critical reception and accolades 5 Track listing 6 Credits and personnel 7 Charts 8 References Bart Millard – lead vocals Mike Scheuchzer – guitars Jim Bryson – keys Nathan Cochran – bass guitar, backing vocals Robby Shaffer – drums Paltrow Performance Group – strings Peter Kipley – producer, programming Steve McCaskey – engineer Julian Kindred – engineer Salvo – mixing Shane Wilson – mixing Elizabeth Workman – design Shawn Sanders – band photos ^ Narrator (November 22, 2008). Faith & Fame: MercyMe (Television production). Gospel Music Channel.  Event occurs at 7:35. By 1994, friends Bart Millard and Mike Scheuchzer made up their minds to pursue music wholeheartedly, and with the help of Jim Bryson, they packed their bags and moved to Oklahoma to form MercyMe..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b "Meet MercyMe". ChristianBook.com. Archived from the original on April 9, 2016. Retrieved March 26, 2016.
 ^ "WisdomWorks Resources". WisdomWorks. October 7, 1999. Archived from the original on October 7, 1999. Retrieved March 17, 2016.
 ^ Millard, Bart (August 29, 2006). "Interview with Bart, Pt. 3". Almost There (Platinum Edition) (CD). INO Records.  Event occurs at 0:22.
 ^ Millard, Bart (August 29, 2006). "Interview with Bart, Pt. 4". Almost There (Platinum Edition) (CD). INO Records.  Event occurs at 0:20.
 ^ Millard, Bart (August 29, 2006). "Interview with Bart, Pt. 6". Almost There (Platinum Edition) (CD). INO Records.  Event occurs at 0:02.
 ^ a b c d Almost There (Media notes). MercyMe. INO Records. 2001. p. 5, 7.CS1 maint: others (link)
 ^ Millard, Bart (August 29, 2006). "Song By Song". Almost There (Platinum Edition) (CD). INO Records.  Event occurs at 0:36.
 ^ Harold, Zack (May 7, 2014). "Meet the new MercyMe". Charleston Gazette–Mail. Archived from the original on April 15, 2019. Retrieved April 23, 2016.
 ^ Millard, Bart (August 29, 2006). "Song By Song". Almost There (Platinum Edition) (CD). INO Records.  Event occurs at 6:58.
 ^ 
Chamberlin, Kevin (September 1, 2002). "MercyMe, "Almost There" Review". Jesus Freak Hideout. Archived from the original on November 1, 2011. Retrieved August 30, 2011.
Woodroof, Adam (October 2001). "Almost There – Mercy Me" (PDF). CCM Magazine. p. 55. Archived from the original (PDF) on May 10, 2012. Retrieved October 22, 2012.
Feinburg, Margaret (September 30, 2001). "Sight & Sound". Charisma. Archived from the original on June 25, 2016. Retrieved April 10, 2016.
 Chamberlin, Kevin (September 1, 2002). "MercyMe, "Almost There" Review". Jesus Freak Hideout. Archived from the original on November 1, 2011. Retrieved August 30, 2011. Woodroof, Adam (October 2001). "Almost There – Mercy Me" (PDF). CCM Magazine. p. 55. Archived from the original (PDF) on May 10, 2012. Retrieved October 22, 2012. Feinburg, Margaret (September 30, 2001). "Sight & Sound". Charisma. Archived from the original on June 25, 2016. Retrieved April 10, 2016. ^ 
Clare, Allan (December 12, 2016). "Review: Almost There Platinum Edition – MercyMe". Cross Rhythms. Archived from the original on March 24, 2016. Retrieved April 9, 2016.
Woodroof, Adam (October 2001). "Almost There – Mercy Me" (PDF). CCM Magazine. p. 55. Archived from the original (PDF) on May 10, 2012. Retrieved October 22, 2012.
"Almost There". Christianity Today. Archived from the original on December 12, 2007. Retrieved October 22, 2012.
McNeese, Kevin (January 30, 2008). "Almost There by MercyMe – CD Reviews and Information". New Release Today. Archived from the original on April 24, 2016. Retrieved September 1, 2011.
 Clare, Allan (December 12, 2016). "Review: Almost There Platinum Edition – MercyMe". Cross Rhythms. Archived from the original on March 24, 2016. Retrieved April 9, 2016. Woodroof, Adam (October 2001). "Almost There – Mercy Me" (PDF). CCM Magazine. p. 55. Archived from the original (PDF) on May 10, 2012. Retrieved October 22, 2012. "Almost There". Christianity Today. Archived from the original on December 12, 2007. Retrieved October 22, 2012. McNeese, Kevin (January 30, 2008). "Almost There by MercyMe – CD Reviews and Information". New Release Today. Archived from the original on April 24, 2016. Retrieved September 1, 2011. ^ a b c d e f g h Chamberlin, Kevin (September 1, 2002). "MercyMe, "Almost There" Review". Jesus Freak Hideout. Archived from the original on November 1, 2011. Retrieved August 30, 2011.
 ^ a b Feinburg, Margaret (September 30, 2001). "Sight & Sound". Charisma. Archived from the original on June 25, 2016. Retrieved April 10, 2016.
 ^ Bartenhagen, Marcia (November 2002). "In Review Music" (PDF). CCM Magazine. p. 52. Archived from the original (PDF) on March 4, 2016. Retrieved April 16, 2016.
 ^ a b c d e f g Losey, Steve. "Almost There – MercyMe". AllMusic. Archived from the original on November 23, 2015. Retrieved April 10, 2016.
 ^ a b c d e f "Almost There". Christianity Today. Archived from the original on December 12, 2007. Retrieved October 22, 2012.
 ^ a b c d Woodroof, Adam (October 2001). "Almost There – Mercy Me" (PDF). CCM Magazine. p. 55. Archived from the original (PDF) on May 10, 2012. Retrieved October 22, 2012.
 ^ a b c McNeese, Kevin (January 30, 2008). "Almost There by MercyMe – CD Reviews and Information". New Release Today. Archived from the original on April 24, 2016. Retrieved September 1, 2011.
 ^ Duncan, Ryan (March 3, 2014). "My Prayer Playlist". Crosswalk.com. Salem Web Network. Archived from the original on April 25, 2016. Retrieved April 27, 2016.
 ^ a b c d Clare, Allan (December 12, 2016). "Review: Almost There Platinum Edition – MercyMe". Cross Rhythms. Archived from the original on March 24, 2016. Retrieved April 9, 2016.
 ^ a b c "MercyMe – Almost There". Crosswalk.com. Salem Web Network. October 3, 2001. Archived from the original on April 3, 2012. Retrieved August 30, 2011.
 ^ a b c d "Gold & Platinum". Recording Industry Association of America (RIAA). Retrieved April 9, 2016.
 ^ "MercyMe enters SoundScan at No. 5 first week out". INOTOF. August 22, 2001. Archived from the original on June 9, 2002. Retrieved April 10, 2016.
 ^ a b Millard, Bart (August 29, 2006). "Song By Song". Almost There: Platinum Edition (CD). INO Records.  Event occurs at 4:52.
 ^ "Christian" (PDF). Radio & Records (1, 417): 107. August 31, 2001. Retrieved April 11, 2016.
 ^ "Christian" (PDF). Radio & Records (1, 420): 90. September 21, 2001. Retrieved April 11, 2016.
 ^ "Christian Albums". Billboard. September 1, 2001. Archived from the original on November 9, 2016. Retrieved April 14, 2016.
 ^ "Heatseekers" (PDF). Billboard. 113 (36): 89. September 8, 2001. Retrieved April 14, 2016.
 ^ Millard, Bart (February 13, 2018). I Can Only Imagine: A Memoir. W Publishing Group. p. 145. ISBN 978-0785216735.
 ^ a b c "Billboard Charted Singles" (PDF). Mikecurb.com. Curb Records. Archived (PDF) from the original on October 31, 2011. Retrieved August 24, 2011.
 ^ The following sources confirm peak positions for each single on the Radio & Records Christian AC chart:
"Christian" (PDF). Radio & Records (1, 420): 90. September 21, 2001. Retrieved April 11, 2016.
"Christian" (PDF). Radio & Records (1, 441): 117. February 22, 2002. Retrieved April 11, 2016.
 "Christian" (PDF). Radio & Records (1, 420): 90. September 21, 2001. Retrieved April 11, 2016. "Christian" (PDF). Radio & Records (1, 441): 117. February 22, 2002. Retrieved April 11, 2016. ^ a b c Hay, Carla (February 16, 2002). "After seven years, INO/Word's MercyMe is 'Almost There'" (PDF). Billboard. 114 (07): 61. Retrieved April 10, 2016.
 ^ "Billboard 200". Billboard. December 22, 2001. Archived from the original on April 3, 2017. Retrieved April 14, 2016.
 ^ "Christian Albums". Billboard. January 19, 2002. Archived from the original on November 6, 2016. Retrieved April 14, 2016.
 ^ Millard, Bart (August 29, 2006). "Song By Song". Almost There: Platinum Edition (CD). INO Records.  Event occurs at 3:03. I was pushing really hard for it ['How Great Is Your Love'] to be a single off the first record. But, if you know anything about MercyMe, you know we only had two singles. The first was 'Bless Me Indeed', and the second was 'I Can Only Imagine', which stayed on the charts for a year and a half or something crazy like that, so we didn't get the chance to have any more singles cause it was time to make the next record.
 ^ "RIAA – Certification Criteria". Recording Industry Association of America. Archived from the original on January 9, 2016. Retrieved June 11, 2012.
 ^ Lloyd, James (October 11, 2002). "Recordings on review". Dayton Daily News  – via HighBeam Research (subscription required). Archived from the original on September 11, 2016. Retrieved April 14, 2016 – via Highbeam Research;.CS1 maint: extra punctuation (link)
 ^ a b c "Top Contemporary Christian Titles". Billboard. December 28, 2002. Archived from the original on June 20, 2011. Retrieved April 10, 2016.
 ^ a b "Top Billboard 200 Albums". Billboard. December 28, 2002. Archived from the original on May 5, 2010. Retrieved April 10, 2016.
 ^ Bronson, Fred (August 16, 2003). "Have Mercy" (PDF). Billboard. 114 (07): 58. Retrieved April 16, 2016.
 ^ a b "Finally 'There'" (PDF). Billboard. 115 (34): 55. August 23, 2003. Retrieved April 10, 2016.
 ^ a b "The Billboard 200" (PDF). Billboard. 115 (38): 68. September 20, 2003. Retrieved April 9, 2016.
 ^ a b "Top Billboard 200 Albums". Billboard. December 27, 2003. Archived from the original on May 5, 2010. Retrieved April 10, 2016.
 ^ Price, Deborah Evans (May 5, 2004). "It Only Gets Better For Gospel Group" (PDF). Billboard. 116 (20): 3, 83. Retrieved April 14, 2016.
 ^ Price, Deborah Evans (April 29, 2006). "Rocking Harder while Keeping the Faith" (PDF). Billboard. 118 (17): 36–37. Retrieved May 18, 2012.
 ^ "Casting Crowns claims first double platinum album with debut". Jesus Freak Hideout. August 30, 2012. Archived from the original on October 17, 2012. Retrieved November 4, 2012.
 ^ a b "Decade-end Christian Albums (2000s)". Billboard. 2009. Archived from the original on August 25, 2011. Retrieved June 11, 2012.
 ^ Asker, Jim (March 16, 2018). "MercyMe's 'I Can Only Imagine' Shines on Christian Charts After Movie Premiere". Billboard. Archived from the original on February 8, 2019. Retrieved April 15, 2019.
 ^ Asker, Jim (March 28, 2018). "MercyMe's 'I Can Only Imagine' Crowns Hot Christian Songs Chart". Billboard. Archived from the original on March 29, 2018. Retrieved April 15, 2019.
 ^ "Hot Christian Songs (March 31, 2018)". Billboard. Archived from the original on May 22, 2018. Retrieved April 15, 2019.
 ^ "Hot Christian Songs (April 7, 2018)". Billboard. Retrieved April 15, 2019.
 ^ "Hot Christian Songs (April 14, 2018)". Billboard. Archived from the original on January 19, 2019. Retrieved April 15, 2019.
 ^ "Digital Song Sales (March 31, 2018)". Billboard. Retrieved April 15, 2019.
 ^ Moser, John (September 28, 2018). "MercyMe, one of Christian music's biggest acts, to play Allentown's PPL Center". The Morning Call. Archived from the original on September 28, 2018. Retrieved February 19, 2019.
 ^ Tyler Aquilina; Lexi Vollero (April 14, 2019). "Here are the 10 highest-grossing Christian movies". Entertainment Weekly. Archived from the original on April 14, 2019. Retrieved April 15, 2019.
 ^ a b Nakamura, Megumi (November 14, 2001). "Review: Almost There – MercyMe". Cross Rhythms. Archived from the original on December 25, 2013. Retrieved April 18, 2014.
 ^ Harms, Tim (January 8, 2003). "First-Person : MercyMe, Nichole Nordeman, Bebo Norman stood out in 2002". Baptist Press. Archived from the original on February 21, 2013. Retrieved November 12, 2011.
 ^ 
Chamberlin, Kevin (September 1, 2002). "MercyMe, "Almost There" Review". Jesus Freak Hideout. Archived from the original on November 1, 2011. Retrieved August 30, 2011.
Clare, Allan (December 12, 2016). "Review: Almost There Platinum Edition – MercyMe". Cross Rhythms. Archived from the original on March 24, 2016. Retrieved April 9, 2016.
Woodroof, Adam (October 2001). "Almost There – Mercy Me" (PDF). CCM Magazine. p. 55. Archived from the original (PDF) on May 10, 2012. Retrieved October 22, 2012.
 Chamberlin, Kevin (September 1, 2002). "MercyMe, "Almost There" Review". Jesus Freak Hideout. Archived from the original on November 1, 2011. Retrieved August 30, 2011. Clare, Allan (December 12, 2016). "Review: Almost There Platinum Edition – MercyMe". Cross Rhythms. Archived from the original on March 24, 2016. Retrieved April 9, 2016. Woodroof, Adam (October 2001). "Almost There – Mercy Me" (PDF). CCM Magazine. p. 55. Archived from the original (PDF) on May 10, 2012. Retrieved October 22, 2012. ^ "100 Albums You Need to Own" (PDF). CCM Magazine. July 2003. p. 34. Archived from the original (PDF) on May 10, 2012. Retrieved April 14, 2016.
 ^ Taff, Tori (2004). 100 Greatest Songs in Christian Music: The Stories Behind the Music that Changed Our Lives Forever. CCM Magazine, Integrity Publishers. p. 13. ISBN 1-59145-210-4.
 ^ Price, Deborah Evans (May 2, 2002). "Michael W. Smith, Third Day Fly High at Doves" (PDF). Billboard. 114 (18): 6. Retrieved April 20, 2016.
 ^ "Heatseekers" (PDF). Billboard. 114 (05): 83. February 2, 2002. Retrieved April 10, 2016.
 ^ "Top Pop Catalog" (PDF). Billboard. 116 (7): 61. February 14, 2004. Retrieved April 9, 2016.
 ^ "Top Contemporary Christian Titles". Billboard. December 27, 2003. Archived from the original on October 9, 2012. Retrieved April 10, 2016.CS1 maint: unfit url (link)
 ^ "Top Pop Catalog Albums". Billboard. December 25, 2004. Archived from the original on May 2, 2006. Retrieved April 10, 2016.
 ^ "Top Contemporary Christian Titles". Billboard. December 25, 2004. Archived from the original on October 9, 2012. Retrieved April 10, 2016.CS1 maint: unfit url (link)
 v t e Bart Millard Nathan Cochran Mike Scheuchzer Robby Shaffer Barry Graul The Worship Project Almost There Spoken For Undone Coming Up to Breathe All That Is Within Me The Generous Mr. Lovewell The Hurt & the Healer Welcome to the New Lifer The Christmas Sessions MercyMe, It's Christmas! Coming Up to Breathe: Acoustic iTunes Originals: MercyMe The Worship Sessions 10 Discography Awards and nominations I Can Only Imagine 2001 albums INO Records albums MercyMe albums CS1 maint: others Subscription required using via Pages containing links to subscription-only content CS1 maint: extra punctuation CS1 maint: unfit url Use mdy dates from April 2012 Articles with short description Articles with hAudio microformats Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Português  This page was last edited on 18 October 2019, at 17:10 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Almost There Almost There MercyMe Additional performers Technical and miscellaneous ^ a b ^ ^ ^ ^ a b c d ^ ^ ^ ^ ^ a b c d e f g h a b ^ a b c d e f g a b c d e f a b c d a b c ^ a b c d a b c a b c d ^ a b ^ ^ ^ ^ 113 ^ a b c ^ a b c 114 ^ ^ ^ ^ ^ a b c a b ^ 114 a b 115 a b 115 a b ^ 116 ^ 118 ^ a b ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ 114 ^ 114 ^ 116 ^ ^ ^ Bart Millard Nathan Cochran Mike Scheuchzer Robby Shaffer Barry Graul  August 14, 2001 (2001-08-14) Ivy Park, The Indigo Room, Paradise Sound, IBC Studios Worship, pop rock 43:42 INO Pete Kipley 


Look(2000)

Almost There(2001)

Spoken For(2002)
 Look(2000)
 Almost There(2001)
 Spoken For(2002)
  
"Bless Me Indeed (Jabez's Song)"Released: 2001
"I Can Only Imagine"Released: October 12, 2001
 
 AllMusic [16] CCM Magazine [18] Cross Rhythms [57][21] Crosswalk B[22] Jesus Freak Hideout [13] New Release Today [19] 1. "I Worship You" Pete Kipely, Reggie Hamm 3:07 2. "Here Am I" Bart Millard, Kipely 4:20 3. "On My Way to You" Millard, Nathan Cochran, Mike Scheuchzer, Jim Bryson, Robby Shaffer 4:06 4. "How Great Is Your Love" Millard 4:14 5. "I Can Only Imagine" Millard 4:08 6. "Bless Me Indeed (Jabez's Song)" Millard, Cochran, Scheuchzer, Bryson, Shaffer 4:14 7. "Cannot Say Enough" Millard, Cochran, Scheuchzer, Bryson, Shaffer 4:47 8. "House of God" Millard, Cochran, Scheuchzer, Bryson, Shaffer, Kipely 3:13 9. "Call to Worship" Millard, Cochran, Scheuchzer, Bryson, Shaffer 4:51 10. "All Fall Down" Millard, Cochran, Scheuchzer, Bryson, Shaffer 3:45 11. "In You" Millard 4:17 
MercyMe

Bart Millard – lead vocals
Mike Scheuchzer – guitars
Jim Bryson – keys
Nathan Cochran – bass guitar, backing vocals
Robby Shaffer – drums
Additional performers

Paltrow Performance Group – strings


 
Technical and miscellaneous

Peter Kipley – producer, programming
Steve McCaskey – engineer
Julian Kindred – engineer
Salvo – mixing
Shane Wilson – mixing
Elizabeth Workman – design
Shawn Sanders – band photos


 

Weekly charts


Chart (2002)

Peakposition


US Heatseekers Albums (Billboard)[63]

1



Chart (2003)

Peakposition


US Billboard 200[43]

37


US Christian Albums (Billboard)[42]

1



Chart (2004)

Peakposition


US Catalog Albums (Billboard)[64]

3



 1
 37
 1
 3
 

Year-end charts


Chart (2002)

Peakposition


US Billboard 200[40]

128


US Christian Albums (Billboard)[39]

6



Chart (2003)

Peakposition


US Billboard 200[44]

128


US Christian Albums (Billboard)[65]

2



Chart (2004)

Peakposition


US Catalog Albums (Billboard)[66]

18


US Christian Albums (Billboard)[67]

15


Decade-end charts


Chart (2000s)

Peakposition


US Christian Albums (Billboard)[48]

4



 128
 6
 128
 2
 18
 15
 4
 
Bart Millard
Nathan Cochran
Mike Scheuchzer
Robby Shaffer
Barry Graul
 
The Worship Project
 
Almost There
Spoken For
Undone
Coming Up to Breathe
All That Is Within Me
The Generous Mr. Lovewell
The Hurt & the Healer
Welcome to the New
Lifer
 
The Christmas Sessions
MercyMe, It's Christmas!
 
Coming Up to Breathe: Acoustic
iTunes Originals: MercyMe
The Worship Sessions
 
10
 
Discography
Awards and nominations
I Can Only Imagine
 