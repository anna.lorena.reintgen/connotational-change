
 
 Lophopsittacus bensoni Holyoak, 1973
 The Mascarene grey parakeet or Thirioux's grey parrot (Psittacula bensoni), is an extinct species of parrot which was endemic to the Mascarene Islands of Mauritius and Réunion in the western Indian Ocean. It has been classified as a member of the tribe Psittaculini, along with other parrots from the Islands.
 Subfossil bones of the Mascarene grey parakeet found on Mauritius were first described in 1973 as belonging to a smaller relative of the broad-billed parrot in the genus Lophopsittacus. Apart from their size, the bones were very similar to those of other Mascarene parrots. The subfossils were later connected with 17th- and 18th-century descriptions of small grey parrots on Mauritius and Réunion, together with a single illustration published in a journal describing a voyage in 1602, and the species was instead reassigned to the genus Psittacula.
 The Mascarene grey parakeet was grey, had a long tail, and was larger than other species of the genus Psittacula, which are usually green. The grey parrots were said to be easy to hunt, as the capture of one would result in its calling out to summon the whole flock. They were also considered to be crop pests, and being such easy prey meant that they were extensively hunted. Coupled with deforestation, this pushed them into extinction. This had happened by the 1730s on Réunion and by the 1760s on Mauritius.
 In 1973, English ornithologist Daniel T. Holyoak described some small parrot bones that he had discovered among a collection of broad-billed parrot (Lophopsittacus mauritianus) subfossils in the Zoology Museum of Cambridge University. These remains had been collected  in the early 20th century by French amateur naturalist Louis Etienne Thirioux, who had found them in a cave on Le Pouce mountain, on the Mascarene Island of Mauritius. They were placed in the zoology museum by 1908. Apart from their size and robustness, Holyoak did not find the bones to be distinct from those of the Mascarene parrot genera Lophopsittacus, Mascarinus (the Mascarene parrot), Necropsittacus (the Rodrigues parrot), and Psittacula (which had two or three other species inhabiting the Mascarene Islands). Because of their similarities, Holyoak considered all these genera to be closely related.[2]
 Holyoak provisionally placed the new species in the same genus as the broad-billed parrot, naming it Lophopsittacus bensoni; the name honours English ornithologist Constantine W. Benson, for his work on birds from the Indian Ocean, and in classifying bird collections at Cambridge. Holyoak also mentioned the possibility that the remains could represent a small subspecies of Necropsittacus or a wide-beaked form of Mascarinus, but maintained that they were best considered as belonging to a distinct species. The holotype specimen is a mandibular symphysis, with the specimen number UMZC 577a. Other known remains include upper mandibles, a palatine bone, and tarsometatarsi.[2] The species has since been excavated from the Mare aux Songes swamp on Mauritius, from which subfossils of most of the other endemic bird species have been identified as well.[3]
 Old, vague accounts of several different now-extinct Mascarene parrots have created much confusion for the scientists who subsequently examined them.[4] In 1967, American ornithologist James Greenway speculated that 17th- and 18th-century reports of then-unidentified grey parrots on Mauritius referred to the broad-billed parrot.[5] In 1987, English ecologist Anthony S. Cheke correlated the L. bensoni subfossils with the grey parrots reported from Mauritius and Réunion, which had previously been ignored, or considered references to broad-billed parrots.[6] Further study of contemporary accounts indicates that the broad-billed parrot was not grey, but had multiple colours.[7]
 In 2007, the English palaeontologist Julian P. Hume reclassified L. bensoni as a member of the genus Psittacula, as he found it to be generically distinct from Lophopsittacus, but morphologically similar to the Alexandrine parakeet (Psittacula eupatria). Hume also pointed out that an engraving accompanying the 1648 published version of Dutch captain Willem van West-Zanen's journal may be the only definite depiction of this species. The engraving shows the killing of dodos (depicted as penguin-like), a dugong, and parrots on Mauritius in 1602; the depicted method of catching parrots matches that used on Mascarene grey parakeets according to contemporary accounts. Hume coined the new common name "Thirioux's grey parrot" in honour of the original collector.[7][8] The IOC World Bird List instead used the common name "Mascarene grey parakeet".[9]
 The population of grey parrots described from the island of Réunion (referred to as Psittacula cf. bensoni by Hume) is thought to have been conspecific with that on Mauritius.[7] Until subfossils of P. bensoni are found on Réunion, it cannot be confirmed whether the grey parrots of the two islands belonged to the same species.[10] In the 1860s, French naturalists Charles Coquerel and Auguste Vinson suggested these could have been parrots of the genus Coracopsis, but fossils of neither that genus nor Psittacula have ever been found on Réunion. Whilst Coracopsis parrots are known to have been introduced to that island in the 1700s, a population did not become established. While no live or dead Mascarene grey parakeets are known with certainty to have been exported, Hume has suggested that a brown parrot specimen—once housed in Cabinet du Roi but now lost—may have been a discoloured old Mascarene grey parakeet, or perhaps a lesser vasa parrot (Coracopsis nigra). This specimen was described by French naturalist Comte de Buffon in 1779.[7][11] Cheke and Hume suggested in 2008 that Mascarene grey parakeets did not reach Europe because they were considered unimpressive or had too specialised a diet.[12]
 Based on morphological features, the Alexandrine parakeet has been proposed as the founder population for all Psittacula species on Indian Ocean islands, with new populations settling during the species' southwards colonisation from its native South Asia. Features of that species gradually disappear in species further away from its range. Many endemic Mascarene birds, including the dodo, are descended from South Asian ancestors, and Hume has proposed that this may also be the case for all the parrots there. Sea levels were lower during the Pleistocene, so it was possible for species to colonise some of these less isolated islands.[12]
 Although most extinct parrot species of the Mascarenes are poorly known, subfossil remains show that they shared common features such as enlarged heads and jaws, reduced pectoral bones, and robust leg bones. Hume has suggested that they all have a common origin in the radiation of the Psittaculini tribe, basing this theory on morphological features and the fact that Psittacula parrots have managed to colonise many isolated islands in the Indian Ocean.[7] The Psittaculini could have invaded the area several times, as many of the species were so specialised that they may have evolved significantly on hotspot islands before the Mascarenes emerged from the sea. Other members of the Psittacula from the Mascarenes include the extant echo parakeet (Psittacula eques echo) of Mauritius, as well as the extinct Réunion parakeet (Psittacula eques eques), and Newton's parakeet (Psittacula exsul) of Rodrigues.[12] A 2015 genetic study found the other Mascarene Psittacula taxa to group within a clade of rose-ringed parakeet (Psittacula krameri) subspecies from Asia and Africa.[13]
 Contemporary accounts describe the Mascarene grey parakeet as a grey, long-tailed parrot. Subfossils show that its beak was about 30% longer than that of the sympatric echo parakeet, and that it had a comparatively broad beak. Members of the Psittacula commonly have large, red beaks, and long tail feathers, with the central ones being the longest. It also differed from its congeners in other osteological details. It was skeletally similar to the Alexandrine parakeet, but some of its bones were larger and more robust. Its colouration also separated it from all other members of Psittacula, the majority of which are green or partially green.[7]
 Based on subfossils, the Mascarene grey parakeet was smaller than the broad-billed parrot and the Rodrigues parrot, but similar in size to the Mascarene parrot, though with a wider beak. The mandibular symphysis (central jaw ridge) was 2.7–2.9 mm (0.11–0.11 in) thick along the mid-line, the palatine (part of the palate) was 31.1 mm (1.22 in), and the tarsometatarsus (bone in the lower leg) was 22–22.5 mm (0.87–0.89 in).[2] The grey parrots from Réunion were described as being larger than the sympatric Réunion parakeet.[7]
 According to Cheke and Hume, the anatomy of the Mascarene grey parakeet suggests that its habits were largely terrestrial, and it may have eaten the fruits of the hurricane palm and the bottle palm, due to their abundance.[12] Like the extinct Mauritian duck and the Mascarene coot, it appears that the Mascarene grey parakeet inhabited both Mauritius and Réunion. Both populations were said to be easy to hunt by capturing one individual and making it call out, which would summon an entire flock.[7]
 Van West-Zanen, who visited Mauritius in 1602, was the first to mention grey parrots there, and he also described the hunting methods used:
 ... some of the people went bird hunting. They could grab as many birds as they wished and could catch them by hand. It was an entertaining sight to see. The grey parrots are especially tame and if one is caught and made to cry out, soon hundreds of the birds fly around ones’ ears, which were then hit to the ground with little sticks.[7]
 Dutch sailor Willem Ysbrandtszoon Bontekoe was on Réunion in 1618, and described the same behaviour, in the first account of the grey parrots there:
 Coming further inland we found [a] great number of geese, doves, grey parrots and other birds, also many land-turtles... And what we most did marvel at, when we held one of the parrots and other birds and squeezed it till it screamed, there came all the others from thereabout as if they would free it and let themselves be caught as well, so we had enough of them to eat.[7]
 In 1705, French pilot engineer Jean Feuilley gave a more detailed description of the parrots of Réunion and their ecology:
 There are several sorts of parrot, of different sizes and colours. Some are the size of a hen, grey, the beak red [Mascarene parrot]; others the same colour the size of a pigeon [Mascarene grey parakeet], and yet others, smaller, are green [Réunion parakeet]. There are great quantities, especially in the Sainte-Suzanne area and on the mountainsides. They are very good to eat, especially when they are fat, which is from the month of June until the month of September, because at that time the trees produce a certain wild seed that these birds eat.[7]
 Many other endemic species of Mauritius and Réunion were lost after the arrival of humans, so that the ecosystems of these islands are severely damaged and hard to reconstruct. Before humans arrived, the islands were entirely covered in forests, very little of which remains today, because of deforestation.[14] The surviving endemic fauna is still seriously threatened.[15] On Mauritius, the Mascarene grey parakeet lived alongside other recently extinct birds such as the dodo, the red rail, the broad-billed parrot, the Mauritius blue pigeon, the Mauritius owl, the Mascarene coot, the Mauritian shelduck, the Mauritian duck, and the Mauritius night heron. On Réunion, it lived alongside the Réunion ibis, the hoopoe starling, the Mascarene parrot, the Réunion parakeet, the Réunion swamphen, the Réunion owl, the Réunion night heron, and the Réunion pink pigeon.[12]
 To the sailors who visited the Mascarene Islands from 1598 onwards, the fauna was mainly interesting from a culinary standpoint.[4] Of the eight or so parrot species endemic to the Mascarenes, only the echo parakeet has survived. The others likely all vanished due to a combination of extensive hunting and deforestation. Being easily caught, the Mascarene grey parakeet was often hunted in abundance by early visitors to Mauritius and Réunion. As the parrots fattened themselves from June to September, they were particularly sought after at this time of the year. An account by Dutch admiral Steven van der Hagen from 1606 even suggests that the grey parrots of Mauritius were sometimes killed for amusement.[7]
 In the 1720s, French traveller Sieur Dubois stated that the grey parrots on Réunion were especially sought after during their fat season, and also claimed they were crop-pests:
 Grey parrots, as good [to eat] as the pigeons... All the birds of this island have their season at different times, being six months in the low country and six months in the mountains when returning, they are very fat and good to eat... The sparrows [Foudia], grey parrots, pigeons and other birds, bats [Pteropus sp.], cause plenty of damage, some to cereals others to fruit.[7]
 That these birds were said to damage crops probably contributed to their being hunted. The French settlers began to clear forests using the slash-and-burn technique in the 1730s, which in itself would have had a large effect on the population of parrots and other animals that nest in tree cavities.[7]
 The grey parrots appear to have been common on Mauritius until the 1750s in spite of the pressure from humans, but since they were last mentioned by French colonist Charpentier de Cossigny in 1759 (published in 1764), they probably became extinct shortly after this time.[11] The grey parrots of Réunion were last mentioned in 1732, also by Cossigny. This final account gives an insight as to how he regarded the culinary quality of parrots from Réunion:
 The woods are full of parrots, either completely grey [Mascarene grey parrot] or completely green [Réunion parakeet]. They were eaten a lot formerly, the grey especially, but both are always lean and very tough whatever sauce one puts on them.[7]
 The 1648 engraving possibly depicting this species was captioned with a Dutch poem, here in English naturalist Hugh Strickland's 1848 translation:
 For food the seamen hunt the flesh of feathered fowl,
They tap the palms, and round-rumped dodos they destroy,
The parrot's life they spare that he may peep and howl,
And thus his fellows to imprisonment decoy.[16]
 1 Taxonomy

1.1 Evolution

 1.1 Evolution 2 Description 3 Behaviour and ecology 4 Extinction 5 References 6 External links ^ BirdLife International (2012). "Lophopsittacus bensoni". IUCN Red List of Threatened Species. Version 2012.1. International Union for Conservation of Nature. Retrieved 16 July 2012..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c Holyoak, D. T. (1973). "An undescribed extinct parrot from Mauritius". Ibis. 115 (3): 417–419. doi:10.1111/j.1474-919X.1973.tb01980.x.
 ^ Rijsdijk, K. F.; Hume, J. P.; Bunnik, F.; Florens, F. B. V.; Baider, C.; Shapiro, B.; van der Plicht, H.; Janoo, A.;  et al. (January 2009). "Mid-Holocene vertebrate bone Concentration-Lagerstätte on oceanic island Mauritius provides a window into the ecosystem of the dodo (Raphus cucullatus)". Quaternary Science Reviews. 28 (1–2): 14–24. Bibcode:2009QSRv...28...14R. doi:10.1016/j.quascirev.2008.09.018.
 ^ a b Fuller, E. (2001). Extinct Birds (revised ed.). New York: Comstock. pp. 230–231. ISBN 978-0-8014-3954-4.
 ^ Greenway, J. C. (1967). Extinct and Vanishing Birds of the World. New York: American Committee for International Wild Life Protection 13. p. 126. ISBN 978-0-486-21869-4.
 ^ Cheke, A. S. (1987). "An ecological history of the Mascarene Islands, with particular reference to extinctions and introductions of land vertebrates".  In Diamond, A. W. (ed.). Studies of Mascarene Island Birds. Cambridge: Cambridge University Press. pp. 5–89. doi:10.1017/CBO9780511735769.003. ISBN 978-0521113311.
 ^ a b c d e f g h i j k l m n o Hume, J. P. (2007). Reappraisal of the parrots (Aves: Psittacidae) from the Mascarene Islands, with comments on their ecology, morphology, and affinities (PDF). Zootaxa. 1513. pp. 4–21. doi:10.11646/zootaxa.1513.1.1. ISBN 978-1-86977-124-9.
 ^ Hume, J. P. (2006). "The History of the Dodo Raphus cucullatus and the Penguin of Mauritius" (PDF). Historical Biology. 18 (2): 69–93. CiteSeerX 10.1.1.695.6929. doi:10.1080/08912960600639400. ISSN 0891-2963.
 ^ Gill, F.; Donsker, D. (2015). "New Zealand parrots, cockatoos & parrots". IOC World Bird List. Retrieved May 31, 2015.
 ^ Hume, J. P. (2013).  Göhlich, U. B.; Kroh, A. (eds.). "A synopsis of the pre-human avifauna of the Mascarene Islands" (PDF). Proceedings of the 8th International Meeting of Society of Avian Paleontology and Evolution: 195–237.
 ^ a b Hume, J. P.; Walters, M. (2012). Extinct Birds. London: A & C Black. p. 174. ISBN 978-1-4081-5725-1.
 ^ a b c d e Cheke, A. S.; Hume, J. P. (2008). Lost Land of the Dodo: an Ecological History of Mauritius, Réunion & Rodrigues. New Haven and London: T. & A. D. Poyser. pp. 37–56. ISBN 978-0-7136-6544-4.
 ^ Jackson, H.; Jones, C. G.; Agapow, P. M.; Tatayah, V.; Groombridge, J. J. (2015). "Micro-evolutionary diversification among Indian Ocean parrots: temporal and spatial changes in phylogenetic diversity as a consequence of extinction and invasion" (PDF). Ibis. 157 (3): 496–510. doi:10.1111/ibi.12275.
 ^ Cheke, A. S. (1987). "The legacy of the dodo—conservation in Mauritius". Oryx. 21 (1): 29–36. doi:10.1017/S0030605300020457.
 ^ Temple, S. A. (1974). "Wildlife in Mauritius today". Oryx. 12 (5): 584–590. doi:10.1017/S0030605300012643.
 ^ Strickland, H. E.; Melville, A. G. (1848). "The Dodo and its Kindred; or, The History, Affinities, and Osteology of the Dodo, Solitaire, and other Extinct Birds of the Islands Mauritius, Rodriguez and Bourbon": 8–29. doi:10.5962/bhl.title.43794. Cite journal requires |journal= (help)
  Media related to Psittacula bensoni at Wikimedia Commons  Data related to Psittacula at Wikispecies v t e Blue-rumped parrot Brehm's tiger parrot Painted tiger parrot Modest tiger parrot Madarasz's tiger parrot Red-cheeked parrot (or singing parrot) Blue-collared parrot Song parrot Montane racket-tail Mindanao racket-tail Blue-headed racket-tail Green racket-tail Blue-crowned racket-tail Mindoro racket-tail Blue-winged racket-tail (or Sulu racket-tail) Yellow-breasted racket-tail Golden-mantled racket-tail Buru racket-tail Great-billed parrot Blue-naped parrot Blue-backed parrot Black-lored parrot Eclectus parrot Oceanic eclectus parrot † ₴ (extinct or prehistoric) Alexandrine parakeet (or Alexandrine parrot) Seychelles parakeet † Rose-ringed parakeet (or ringnecked parakeet) Echo parakeet Newton's parakeet † Slaty-headed parakeet Grey-headed parakeet Plum-headed parakeet Blossom-headed parakeet Lord Derby's parakeet Red-breasted parakeet Nicobar parakeet Long-tailed parakeet Mascarene grey parakeet † Broad-billed parrot † (genus: Lophopsittacus) Rodrigues parrot † (genus: Necropsittacus) v t e Outline Bird anatomy Flight Eggs Feathers Plumage Beak Vision Dactyly Preen gland Singing Intelligence Migration Sexual selection Lek mating Seabird breeding Incubation Brood parasites Nesting Hybrids Origin of birds
Theropoda
dinosaurs Theropoda dinosaurs Origin of flight Evolution of birds Darwin's finches Seabirds Archaeopteryx Omnivoropterygiformes Confuciusornithiformes Enantiornithes Chaoyangiiformes Patagopterygiformes Ambiortiformes Songlingornithiformes Gansuiformes Ichthyornithiformes Hesperornithes Lithornithiformes Dinornithiformes Aepyornithiformes Gastornithiformes Ringing Ornithology Ornithomancy Bird collections Birdwatching Bird feeding Conservation Aviculture Waterfowl hunting Cockfighting Pigeon racing Falconry Pheasantry Egg collecting Families and orders Genera Glossary of bird terms List by population Lists by region Recently extinct birds Late Quaternary prehistoric birds Notable birds
Individuals
Fictional Individuals Fictional Struthioniformes (ostriches) Rheiformes (rheas) Tinamiformes (tinamous) Apterygiformes (kiwis) Casuariiformes (emus and cassowaries) Anatinae
Aythyini
Mergini
Oxyurini Aythyini Mergini Oxyurini Anserinae
swans
true geese swans true geese Dendrocygninae Stictonettinae Tadorninae Anhima Chauna Anatalavis Anseranas Cracinae Oreophasinae Penelopinae Aepypodius Alectura Eulipoa Leipoa Macrocephalon Megapodius Talegalla Acryllium Agelastes Guttera Numida Callipepla Colinus Cyrtonyx Dactylortyx Dendrortyx Odontophorus Oreortyx Philortyx Rhynchortyx Meleagridinae (turkeys) Perdicinae Phasianinae (pheasants and relatives) Tetraoninae Columbiformes (doves and pigeons) Mesitornithiformes (mesites) Pterocliformes (sandgrouses) Phoenicopteriformes (flamingos) Podicipediformes (grebes) Cuculiformes (cuckoos) Musophagiformes (turacos) Otidiformes (bustards) Caprimulgiformes (nightjars and relatives) Steatornithiformes Podargiformes Apodiformes (swifts and hummingbirds) Opisthocomiformes (hoatzin) Charadriiformes (gulls and relatives) Gruiformes (cranes and relatives) Phaethontiformes (tropicbirds) Eurypygiformes (kagu and sunbittern) Gaviiformes (loons or divers) Sphenisciformes (penguins) Procellariiformes (albatrosses and petrels) Ciconiiformes (storks) Suliformes (cormorants and relatives) Pelecaniformes (pelicans and relatives) Cariamiformes (seriemas and relatives) Falconiformes (falcons and relatives) Psittaciformes (parrots) Passeriformes (perching birds) Cathartiformes (New World vultures and condors) Accipitriformes (eagles and hawks) Strigiformes (owls) Coliiformes (mousebirds) Trogoniformes (trogons and quetzals) Leptosomiformes (cuckoo roller) Bucerotiformes (hornbills and hoopoes) Coraciiformes (kingfishers and rollers) Piciformes (woodpeckers and relatives) Category  Commons  Portal  WikiProject Birds portal Animals portal Biology portal Africa portal Madagascar portal Paleontology portal Wikidata: Q1814708 Wikispecies: Psittacula bensoni EoL: 38651773 Fossilworks: 371626 uBio: 5986583 IUCN Red List extinct species Psittacula Parrots of Africa Birds of Mauritius Birds of Réunion Birds described in 1973 Extinct animals of Africa Extinct birds of Indian Ocean islands Extinct animals of Mauritius Bird extinctions since 1500 CS1 errors: missing periodical Articles with short description Featured articles Articles with 'species' microformats Commons category link is on Wikidata Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Wikispecies Create a book Download as PDF Printable version Deutsch Español Euskara Français Bahasa Indonesia Italiano ქართული Кырык мары Nederlands Polski Português Svenska Tiếng Việt  This page was last edited on 23 August 2019, at 16:03 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  ... some of the people went bird hunting. They could grab as many birds as they wished and could catch them by hand. It was an entertaining sight to see. The grey parrots are especially tame and if one is caught and made to cry out, soon hundreds of the birds fly around ones’ ears, which were then hit to the ground with little sticks.[7]
 Coming further inland we found [a] great number of geese, doves, grey parrots and other birds, also many land-turtles... And what we most did marvel at, when we held one of the parrots and other birds and squeezed it till it screamed, there came all the others from thereabout as if they would free it and let themselves be caught as well, so we had enough of them to eat.[7]
 There are several sorts of parrot, of different sizes and colours. Some are the size of a hen, grey, the beak red [Mascarene parrot]; others the same colour the size of a pigeon [Mascarene grey parakeet], and yet others, smaller, are green [Réunion parakeet]. There are great quantities, especially in the Sainte-Suzanne area and on the mountainsides. They are very good to eat, especially when they are fat, which is from the month of June until the month of September, because at that time the trees produce a certain wild seed that these birds eat.[7]
 Grey parrots, as good [to eat] as the pigeons... All the birds of this island have their season at different times, being six months in the low country and six months in the mountains when returning, they are very fat and good to eat... The sparrows [Foudia], grey parrots, pigeons and other birds, bats [Pteropus sp.], cause plenty of damage, some to cereals others to fruit.[7]
 The woods are full of parrots, either completely grey [Mascarene grey parrot] or completely green [Réunion parakeet]. They were eaten a lot formerly, the grey especially, but both are always lean and very tough whatever sauce one puts on them.[7]
 
For food the seamen hunt the flesh of feathered fowl,
They tap the palms, and round-rumped dodos they destroy,
The parrot's life they spare that he may peep and howl,
And thus his fellows to imprisonment decoy.[16]


 P. bensoni †Psittacula bensoni Mascarene grey parakeet Thirioux's grey parrot ^ a b c 115 ^ 28 a b ^ ^ a b c d e f g h i j k l m n o 1513 ^ 18 ^ ^ a b a b c d e ^ 157 ^ 21 ^ 12 ^ Genus Category Commons Portal WikiProject 
 Hypothetical restoration based on contemporary descriptions, subfossil remains, and relatives
 Extinct  (1764) (IUCN 3.1)[1]
 Kingdom:
 Animalia
 Phylum:
 Chordata
 Class:
 Aves
 Order:
 Psittaciformes
 Family:
 Psittaculidae
 Genus:
 Psittacula
 Species:
 †P. bensoni
 †Psittacula bensoni(Holyoak, 1973)
 
 Location of Mauritius, where subfossils of this species have been found
 
Lophopsittacus bensoni Holyoak, 1973

 (extinctions: † indicates a species confirmed to be extinct, ₴ indicates evidence only from sub-fossils) 
Blue-rumped parrot
 
Brehm's tiger parrot
Painted tiger parrot
Modest tiger parrot
Madarasz's tiger parrot
 
Red-cheeked parrot (or singing parrot)
Blue-collared parrot
Song parrot
 
Montane racket-tail
Mindanao racket-tail
Blue-headed racket-tail
Green racket-tail
Blue-crowned racket-tail
Mindoro racket-tail
Blue-winged racket-tail (or Sulu racket-tail)
Yellow-breasted racket-tail
Golden-mantled racket-tail
Buru racket-tail
 
Great-billed parrot
Blue-naped parrot
Blue-backed parrot
Black-lored parrot
 
Eclectus parrot
Oceanic eclectus parrot † ₴ (extinct or prehistoric)
 
Alexandrine parakeet (or Alexandrine parrot)
Seychelles parakeet †
Rose-ringed parakeet (or ringnecked parakeet)
Echo parakeet
Newton's parakeet †
Slaty-headed parakeet
Grey-headed parakeet
Plum-headed parakeet
Blossom-headed parakeet
Lord Derby's parakeet
Red-breasted parakeet
Nicobar parakeet
Long-tailed parakeet
Mascarene grey parakeet †
 
Broad-billed parrot † (genus: Lophopsittacus)
Rodrigues parrot † (genus: Necropsittacus)
 
Outline
 
Bird anatomy
Flight
Eggs
Feathers
Plumage
Beak
Vision
Dactyly
Preen gland
 
Singing
Intelligence
Migration
Sexual selection
Lek mating
Seabird breeding
Incubation
Brood parasites
Nesting
Hybrids
 
Origin of birds
Theropoda
dinosaurs
Origin of flight
Evolution of birds
Darwin's finches
Seabirds
 
Archaeopteryx
Omnivoropterygiformes
Confuciusornithiformes
Enantiornithes
Chaoyangiiformes
Patagopterygiformes
Ambiortiformes
Songlingornithiformes
Gansuiformes
Ichthyornithiformes
Hesperornithes
Lithornithiformes
Dinornithiformes
Aepyornithiformes
Gastornithiformes
 
Ringing
Ornithology
Ornithomancy
Bird collections
Birdwatching
Bird feeding
Conservation
Aviculture
Waterfowl hunting
Cockfighting
Pigeon racing
Falconry
Pheasantry
Egg collecting
 
Families and orders
Genera
Glossary of bird terms
List by population
Lists by region
Recently extinct birds
Late Quaternary prehistoric birds
Notable birds
Individuals
Fictional
 Palaeognathae
Struthioniformes (ostriches)
Rheiformes (rheas)
Tinamiformes (tinamous)
Apterygiformes (kiwis)
Casuariiformes (emus and cassowaries)
NeognathaeGalloanserae  (fowls)Anseriformes  (waterfowls)Anatidae  (ducks)
Anatinae
Aythyini
Mergini
Oxyurini
Anserinae
swans
true geese
Dendrocygninae
Stictonettinae
Tadorninae
Anhimidae
Anhima
Chauna
Anseranatidae
Anatalavis
Anseranas
Galliformes  (landfowls-  gamebirds)Cracidae
Cracinae
Oreophasinae
Penelopinae
Megapodidae
Aepypodius
Alectura
Eulipoa
Leipoa
Macrocephalon
Megapodius
Talegalla
Numididae
Acryllium
Agelastes
Guttera
Numida
Odontophoridae
Callipepla
Colinus
Cyrtonyx
Dactylortyx
Dendrortyx
Odontophorus
Oreortyx
Philortyx
Rhynchortyx
Phasianidae
Meleagridinae (turkeys)
Perdicinae
Phasianinae (pheasants and relatives)
Tetraoninae
NeoavesColumbeaColumbimorphae
Columbiformes (doves and pigeons)
Mesitornithiformes (mesites)
Pterocliformes (sandgrouses)
Mirandornithes
Phoenicopteriformes (flamingos)
Podicipediformes (grebes)
PassereaOtidimorphae
Cuculiformes (cuckoos)
Musophagiformes (turacos)
Otidiformes (bustards)
Strisores
Caprimulgiformes (nightjars and relatives)
Steatornithiformes
Podargiformes
Apodiformes (swifts and hummingbirds)
Opisthocomiformes
Opisthocomiformes (hoatzin)
Cursorimorphae
Charadriiformes (gulls and relatives)
Gruiformes (cranes and relatives)
Phaethontimorphae
Phaethontiformes (tropicbirds)
Eurypygiformes (kagu and sunbittern)
Aequornithes
Gaviiformes (loons or divers)
Sphenisciformes (penguins)
Procellariiformes (albatrosses and petrels)
Ciconiiformes (storks)
Suliformes (cormorants and relatives)
Pelecaniformes (pelicans and relatives)
Australaves
Cariamiformes (seriemas and relatives)
Falconiformes (falcons and relatives)
Psittaciformes (parrots)
Passeriformes (perching birds)
Afroaves
Cathartiformes (New World vultures and condors)
Accipitriformes (eagles and hawks)
Strigiformes (owls)
Coliiformes (mousebirds)
Trogoniformes (trogons and quetzals)
Leptosomiformes (cuckoo roller)
Bucerotiformes (hornbills and hoopoes)
Coraciiformes (kingfishers and rollers)
Piciformes (woodpeckers and relatives)
 
Struthioniformes (ostriches)
Rheiformes (rheas)
Tinamiformes (tinamous)
Apterygiformes (kiwis)
Casuariiformes (emus and cassowaries)
 Galloanserae  (fowls)Anseriformes  (waterfowls)Anatidae  (ducks)
Anatinae
Aythyini
Mergini
Oxyurini
Anserinae
swans
true geese
Dendrocygninae
Stictonettinae
Tadorninae
Anhimidae
Anhima
Chauna
Anseranatidae
Anatalavis
Anseranas
Galliformes  (landfowls-  gamebirds)Cracidae
Cracinae
Oreophasinae
Penelopinae
Megapodidae
Aepypodius
Alectura
Eulipoa
Leipoa
Macrocephalon
Megapodius
Talegalla
Numididae
Acryllium
Agelastes
Guttera
Numida
Odontophoridae
Callipepla
Colinus
Cyrtonyx
Dactylortyx
Dendrortyx
Odontophorus
Oreortyx
Philortyx
Rhynchortyx
Phasianidae
Meleagridinae (turkeys)
Perdicinae
Phasianinae (pheasants and relatives)
Tetraoninae
NeoavesColumbeaColumbimorphae
Columbiformes (doves and pigeons)
Mesitornithiformes (mesites)
Pterocliformes (sandgrouses)
Mirandornithes
Phoenicopteriformes (flamingos)
Podicipediformes (grebes)
PassereaOtidimorphae
Cuculiformes (cuckoos)
Musophagiformes (turacos)
Otidiformes (bustards)
Strisores
Caprimulgiformes (nightjars and relatives)
Steatornithiformes
Podargiformes
Apodiformes (swifts and hummingbirds)
Opisthocomiformes
Opisthocomiformes (hoatzin)
Cursorimorphae
Charadriiformes (gulls and relatives)
Gruiformes (cranes and relatives)
Phaethontimorphae
Phaethontiformes (tropicbirds)
Eurypygiformes (kagu and sunbittern)
Aequornithes
Gaviiformes (loons or divers)
Sphenisciformes (penguins)
Procellariiformes (albatrosses and petrels)
Ciconiiformes (storks)
Suliformes (cormorants and relatives)
Pelecaniformes (pelicans and relatives)
Australaves
Cariamiformes (seriemas and relatives)
Falconiformes (falcons and relatives)
Psittaciformes (parrots)
Passeriformes (perching birds)
Afroaves
Cathartiformes (New World vultures and condors)
Accipitriformes (eagles and hawks)
Strigiformes (owls)
Coliiformes (mousebirds)
Trogoniformes (trogons and quetzals)
Leptosomiformes (cuckoo roller)
Bucerotiformes (hornbills and hoopoes)
Coraciiformes (kingfishers and rollers)
Piciformes (woodpeckers and relatives)
 Anseriformes  (waterfowls)Anatidae  (ducks)
Anatinae
Aythyini
Mergini
Oxyurini
Anserinae
swans
true geese
Dendrocygninae
Stictonettinae
Tadorninae
Anhimidae
Anhima
Chauna
Anseranatidae
Anatalavis
Anseranas
Galliformes  (landfowls-  gamebirds)Cracidae
Cracinae
Oreophasinae
Penelopinae
Megapodidae
Aepypodius
Alectura
Eulipoa
Leipoa
Macrocephalon
Megapodius
Talegalla
Numididae
Acryllium
Agelastes
Guttera
Numida
Odontophoridae
Callipepla
Colinus
Cyrtonyx
Dactylortyx
Dendrortyx
Odontophorus
Oreortyx
Philortyx
Rhynchortyx
Phasianidae
Meleagridinae (turkeys)
Perdicinae
Phasianinae (pheasants and relatives)
Tetraoninae
 Anatidae  (ducks)
Anatinae
Aythyini
Mergini
Oxyurini
Anserinae
swans
true geese
Dendrocygninae
Stictonettinae
Tadorninae
Anhimidae
Anhima
Chauna
Anseranatidae
Anatalavis
Anseranas
 
Anatinae
Aythyini
Mergini
Oxyurini
Anserinae
swans
true geese
Dendrocygninae
Stictonettinae
Tadorninae
 
Anhima
Chauna
 
Anatalavis
Anseranas
 Cracidae
Cracinae
Oreophasinae
Penelopinae
Megapodidae
Aepypodius
Alectura
Eulipoa
Leipoa
Macrocephalon
Megapodius
Talegalla
Numididae
Acryllium
Agelastes
Guttera
Numida
Odontophoridae
Callipepla
Colinus
Cyrtonyx
Dactylortyx
Dendrortyx
Odontophorus
Oreortyx
Philortyx
Rhynchortyx
Phasianidae
Meleagridinae (turkeys)
Perdicinae
Phasianinae (pheasants and relatives)
Tetraoninae
 
Cracinae
Oreophasinae
Penelopinae
 
Aepypodius
Alectura
Eulipoa
Leipoa
Macrocephalon
Megapodius
Talegalla
 
Acryllium
Agelastes
Guttera
Numida
 
Callipepla
Colinus
Cyrtonyx
Dactylortyx
Dendrortyx
Odontophorus
Oreortyx
Philortyx
Rhynchortyx
 
Meleagridinae (turkeys)
Perdicinae
Phasianinae (pheasants and relatives)
Tetraoninae
 ColumbeaColumbimorphae
Columbiformes (doves and pigeons)
Mesitornithiformes (mesites)
Pterocliformes (sandgrouses)
Mirandornithes
Phoenicopteriformes (flamingos)
Podicipediformes (grebes)
PassereaOtidimorphae
Cuculiformes (cuckoos)
Musophagiformes (turacos)
Otidiformes (bustards)
Strisores
Caprimulgiformes (nightjars and relatives)
Steatornithiformes
Podargiformes
Apodiformes (swifts and hummingbirds)
Opisthocomiformes
Opisthocomiformes (hoatzin)
Cursorimorphae
Charadriiformes (gulls and relatives)
Gruiformes (cranes and relatives)
Phaethontimorphae
Phaethontiformes (tropicbirds)
Eurypygiformes (kagu and sunbittern)
Aequornithes
Gaviiformes (loons or divers)
Sphenisciformes (penguins)
Procellariiformes (albatrosses and petrels)
Ciconiiformes (storks)
Suliformes (cormorants and relatives)
Pelecaniformes (pelicans and relatives)
Australaves
Cariamiformes (seriemas and relatives)
Falconiformes (falcons and relatives)
Psittaciformes (parrots)
Passeriformes (perching birds)
Afroaves
Cathartiformes (New World vultures and condors)
Accipitriformes (eagles and hawks)
Strigiformes (owls)
Coliiformes (mousebirds)
Trogoniformes (trogons and quetzals)
Leptosomiformes (cuckoo roller)
Bucerotiformes (hornbills and hoopoes)
Coraciiformes (kingfishers and rollers)
Piciformes (woodpeckers and relatives)
 Columbimorphae
Columbiformes (doves and pigeons)
Mesitornithiformes (mesites)
Pterocliformes (sandgrouses)
Mirandornithes
Phoenicopteriformes (flamingos)
Podicipediformes (grebes)
 
Columbiformes (doves and pigeons)
Mesitornithiformes (mesites)
Pterocliformes (sandgrouses)
 
Phoenicopteriformes (flamingos)
Podicipediformes (grebes)
 Otidimorphae
Cuculiformes (cuckoos)
Musophagiformes (turacos)
Otidiformes (bustards)
Strisores
Caprimulgiformes (nightjars and relatives)
Steatornithiformes
Podargiformes
Apodiformes (swifts and hummingbirds)
Opisthocomiformes
Opisthocomiformes (hoatzin)
Cursorimorphae
Charadriiformes (gulls and relatives)
Gruiformes (cranes and relatives)
Phaethontimorphae
Phaethontiformes (tropicbirds)
Eurypygiformes (kagu and sunbittern)
Aequornithes
Gaviiformes (loons or divers)
Sphenisciformes (penguins)
Procellariiformes (albatrosses and petrels)
Ciconiiformes (storks)
Suliformes (cormorants and relatives)
Pelecaniformes (pelicans and relatives)
Australaves
Cariamiformes (seriemas and relatives)
Falconiformes (falcons and relatives)
Psittaciformes (parrots)
Passeriformes (perching birds)
Afroaves
Cathartiformes (New World vultures and condors)
Accipitriformes (eagles and hawks)
Strigiformes (owls)
Coliiformes (mousebirds)
Trogoniformes (trogons and quetzals)
Leptosomiformes (cuckoo roller)
Bucerotiformes (hornbills and hoopoes)
Coraciiformes (kingfishers and rollers)
Piciformes (woodpeckers and relatives)
 
Cuculiformes (cuckoos)
Musophagiformes (turacos)
Otidiformes (bustards)
 
Caprimulgiformes (nightjars and relatives)
Steatornithiformes
Podargiformes
Apodiformes (swifts and hummingbirds)
 
Opisthocomiformes (hoatzin)
 
Charadriiformes (gulls and relatives)
Gruiformes (cranes and relatives)
 
Phaethontiformes (tropicbirds)
Eurypygiformes (kagu and sunbittern)
 
Gaviiformes (loons or divers)
Sphenisciformes (penguins)
Procellariiformes (albatrosses and petrels)
Ciconiiformes (storks)
Suliformes (cormorants and relatives)
Pelecaniformes (pelicans and relatives)
 
Cariamiformes (seriemas and relatives)
Falconiformes (falcons and relatives)
Psittaciformes (parrots)
Passeriformes (perching birds)
 
Cathartiformes (New World vultures and condors)
Accipitriformes (eagles and hawks)
Strigiformes (owls)
Coliiformes (mousebirds)
Trogoniformes (trogons and quetzals)
Leptosomiformes (cuckoo roller)
Bucerotiformes (hornbills and hoopoes)
Coraciiformes (kingfishers and rollers)
Piciformes (woodpeckers and relatives)
 
Category
 Commons
 Portal
 WikiProject
 
Wikidata: Q1814708
Wikispecies: Psittacula bensoni
EoL: 38651773
Fossilworks: 371626
uBio: 5986583
 