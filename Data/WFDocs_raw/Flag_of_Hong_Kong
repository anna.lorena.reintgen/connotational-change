
 The flag of Hong Kong, officially the regional flag of the Hong Kong Special Administrative Region of the People's Republic of China, depicts a white stylised five-petal Hong Kong orchid tree (Bauhinia blakeana) flower in the centre of a Chinese red field. Its design was adopted on 4 April 1990 at the Third Session of the Seventh National People's Congress.[1] The precise use of the flag is regulated by laws passed by the 58th executive meeting of the State Council held in Beijing.[2] The design of the flag is enshrined in Hong Kong's Basic Law, the territory's constitutional document,[3] and regulations regarding the use, prohibition of use, desecration, and manufacture of the flag are stated in the Regional Flag and Regional Emblem Ordinance.[4] The flag of Hong Kong was first officially hoisted on 1 July 1997, during the handover ceremony marking the transfer of sovereignty from Britain to China.[5]
 The design of the flag comes with cultural, political, and regional meanings. The colour itself is significant; red is a festive colour for the Chinese people, used to convey a sense of celebration and nationalism.[6] Moreover, the red colour is identical to that used in the national PRC flag,[7] chosen to signify the link re-established between post-colonial Hong Kong and China. The position of red and white on the flag symbolises the "one country two systems" political principle applied to the region. The stylised rendering of the Bauhinia blakeana flower, a flower discovered in Hong Kong, is meant to serve as a harmonising symbol for this dichotomy.[6] The five stars of the Chinese national flag are replicated on the petals of the flower. The Chinese name of Bauhinia × blakeana is most commonly rendered as "洋紫荊", but is often shortened to 紫荊/紫荆 in official uses since  "洋" (yáng) means "foreign" in Chinese, notwithstanding 紫荊/紫荆 refers to another genus called Cercis. A sculpture of the plant has been erected in Golden Bauhinia Square in Hong Kong. The representation of the flower in white runs contrary to the fact they are bright pinkish purple.[citation needed]
 Before the adoption of the flag, the Chairman of the Hong Kong Basic Law Drafting Committee explained the significance of the flag's design to the National People's Congress:
 The Hong Kong government has specified sizes, colours, and manufacturing parameters in which the flag is to be made.  The background of the rectangular flag is red, the same shade of red as that used for the national PRC flag.  The ratio of its length to breadth is 3:2.  In its centre is a five-petal stylised rendering of a white Bauhinia blakeana flower.  If a circle circumscribes the flower, it should have a diameter 0.6 times the entire height of the flag.  The petals are uniformly spread around the centre point of the flag, radiating outward and pointing in a clockwise direction. Each of the flower's petals bears a five-pointed red star, a communist and socialist symbolism, with a red trace, suggestive of a flower stamen. The heading that is used to allow a flag to be slid or raised onto a pole is white.[7]
 This table lists all the official sizes for the flag. Sizes deviating from this list are considered non-standard.  If a flag is not of official size, it must be a scaled-down or scaled-up version of one of the official sizes.[7]
 The following are the approximate colours of the Hong Kong flag in different colour models. It is listed by web colours in hexadecimal notation, CMYK equivalents*, dye colours, HSL equivalents, and Pantone equivalents.
 *CMYK equivalents based on official downloadable files from Hong Kong's Protocol website (see 2nd external link).
 The Regional Flag and Regional Emblem Ordinance stipulates that the Hong Kong flag must be manufactured according to specifications laid out in the ordinance.  If flags are not produced in design according to the ordinance, the Secretary for Justice may petition the District Court for an injunction to prohibit the person or company from manufacturing the flags.  If the District Court agrees that the flags are not in compliance, it may issue an injunction and order that the flags and the materials that were used to make the flags to be seized by the government.[9]
 The Hong Kong flag is flown daily from the Chief Executive's official residence, the Government House, the Hong Kong International Airport, and at all border crossings and points of entry into Hong Kong.[10] At major government offices and buildings, such as the Office of the Chief Executive, the Executive Council, the Court of Final Appeal, the High Court, the Legislative Council, and the Hong Kong Economic and Trade Offices overseas, the flag is displayed during days when these offices are working. Other government offices and buildings, such as hospitals, schools, departmental headquarters, sports grounds, and cultural venues should fly the flag on occasions such as the National Day of the PRC (1 October), the Hong Kong Special Administrative Region Establishment Day (1 July), and New Year's Day.[10] The flag should be raised at 8:00 a.m. and lowered at 6:00 p.m.  The raising and lowering of the flag should be done slowly; it must reach the peak of the flag staff when it is raised, and it may not touch the ground when it is lowered.  The flag may not be raised in severe weather conditions.[11] A Hong Kong flag that is either damaged, defaced, faded or substandard must not be displayed or used.[12]
 Whenever the PRC national flag is flown together with the regional Hong Kong flag, the national flag must be flown at the centre, above the regional flag, or otherwise in a more prominent position than that of the regional flag. The regional flag must be smaller in size than the national flag, and it must be displayed to the left of the national flag.  When the flags are displayed inside a building, the left and right sides of a person looking at the flags, and with his or her back toward the wall, are used as reference points for the left and right sides of a flag.  When the flags are displayed outside a building, the left and right sides of a person standing in front of the building and looking towards the front entrance are used as reference points for the left and right sides of a flag.  The national flag should be raised before the regional flag is raised, and it should be lowered after the regional flag is lowered.[11]
 An exception to this rule occurs during medal presentation ceremonies at multi-sport events such as the Olympics and Asian Games. As Hong Kong competes separately from mainland China, should an athlete from Hong Kong win the gold medal, and an athlete from mainland China win the silver and/or bronze medal(s) in the same event, the regional flag of Hong Kong would be raised in the centre above the national flag(s) during the medal presentation ceremony.
 The Hong Kong flag must be lowered to half-mast as a token of mourning when any of the following people die:[12]
 The flag may also be flown at half-mast when the Central People's Government advises the Chief Executive to do so, or when the Chief Executive considers it appropriate to do so, on occurrences of unfortunate events causing especially serious casualties, or when serious natural calamities have caused heavy casualties.[12] When raising a flag to be flown at half-mast, it should first be raised to the top of the pole and then lowered to a point where the distance between the top of the flag and the top of the pole is one third of the length of the pole. When lowering the flag from half-mast, it should first be raised to the peak of the pole before it is lowered.[11]
 The Regional Flag and Regional Emblem Ordinance states what manner of use of the Hong Kong flag is prohibited and that desecration of the flag is prohibited; it also states that it is a punishable offence for a person to use the flag in a prohibited manner or desecrate the flag.  According to the ordinance, a flag may not be used in advertisements or trademarks,[13] and that "publicly and wilfully burning, mutilating, scrawling on, defiling or trampling" the flag is considered flag desecration.[14] Similarly, the National Flag and National Emblem Ordinance extends the same prohibition toward the national PRC flag.[15][16] The ordinances also allow for the Chief Executive to make stipulations regarding the use of the flag.  In stipulations made in 1997, the Chief Executive further specified that the use of the flag in "any trade, calling or profession, or the logo, seal or badge of any non-governmental organisation" is also prohibited unless prior permission was obtained.[10]
 The first conviction of flag desecration occurred in 1999.  Protesters Ng Kung Siu and Lee Kin Yun wrote the word "Shame" on both the national PRC flag and the Hong Kong flag, and were convicted of violating the National Flag and National Emblem Ordinance and the Regional Flag and Regional Emblem Ordinance.  The Court of Appeal overturned the verdict, ruling that the ordinances were unnecessary restrictions on the freedom of expression and in violation of both the Basic Law and the International Covenant on Civil and Political Rights.  Upon further appeal, however, the Court of Final Appeal maintained the original guilty verdict, holding that this restriction on the freedom of expression was justifiable in that the protection of the flags played a role in national unity and territorial integrity and constituted a restriction on the mode of expressing one's message but did not interfere with one's freedom to express the same message in other ways.[17]
 Leung Kwok-hung, a member of the Legislative Council and a political activist in Hong Kong, was penalised in February 2001, before he became a member of the Legislative Council, for defiling the flag.  He was convicted of three counts of desecrating the flag—for two incidents on 1 July 2000 during the third anniversary of Hong Kong's handover to China and for one incident on 9 July of the same year during a protest against elections to choose the Election Committee, the electoral college which chooses the Chief Executive of Hong Kong.  Leung was placed on a good-behaviour bond for 12 months in the sum of HK$3,000.[18]
 Zhu Rongchang, a mainland Chinese farmer has been jailed for three weeks after setting fire to a Chinese flag in Hong Kong. Zhu was charged for "publicly and wilfully" burning the Chinese flag at Golden Bauhinia Square in central Hong Kong. The 74-year-old man is reportedly the third person charged for desecrating the Chinese national flag, but he is first to be jailed under the law.[19]
 In early 2013, protestors went to the streets flying the old colonial flag demanding more democracy and resignation of Chief Executive Leung Chun Ying. The use of the flag has created concerns from Chinese authorities and request from Leung to stop flying the flag.[20][21] Despite the calls from Leung the old flags are not subject to use restrictions beyond not being allowed to be placed on flagpoles and are freely sold and manufactured in the territory.
 Prior to the secession of Hong Kong to the United Kingdom following the Opium War, Hong Kong fell under the jurisdiction of the government of China and flew the flag and ensign of the Chinese government of the time. Prior to the establishment of the crown colony of Hong Kong, the ruling dynasty in China was the Qing dynasty. Despite being established in 1644, the Qing Empire had no official flags until 1862. Prior to 1898, when the Second Convention of Peking was signed between the Qing Court and the government of the United Kingdom, the New Territories was still Qing land. The flag itself features the "Azure Dragon" on a plain yellow field with the red flaming pearl of the three-legged crow in the upper left corner.[22]
 Prior to Hong Kong's transfer of sovereignty, the flag of Hong Kong was a colonial Blue Ensign flag.[23] The flag of colonial Hong Kong underwent several changes from then until 1997.
 In 1843, a seal representing Hong Kong was instituted.  The design was based on a local waterfront scene; three local merchants with their commercial goods are shown on the foreground, a square-rigged ship and a junk occupy the middle ground, while the background consists of conical hills and clouds. In 1868, a Hong Kong flag was produced, a Blue Ensign flag with a badge based on this "local scene", but the design was rejected by Hong Kong Governor Richard Graves MacDonnell.[23]
 On 3 July 1869, a new design for the Hong Kong flag was commissioned at a cost of £3, which featured a "gentleman in an evening coat who is purchasing tea on the beach at Kowloon". After a brief discussion in the executive council, it was determined that the new design was very problematic and it was not adopted.[24]
 In 1870, a "white crown over HK" badge for the Blue Ensign flag was proposed by the Colonial Secretary. The letters "HK" were omitted and the crown became full-colour three years later.[23] It is unclear exactly what the badge looked like during that period of time, but it was unlikely to be the "local scene".  It should have been a crown of some sort, which may, or may not, have had the letters "HK" below it.  In 1876, the "local scene" badge (Chinese: 阿群帶路圖 Picture of "Ar Kwan" Guiding the British soldier) was re-adopted to the Blue Ensign flag with the Admiralty's approval.[23]
 During a government meeting, held in 1911, it was suggested that the name of the colony appear on the flag in both Latin and Chinese scripts. However, this was dismissed as it would "look absurd" to both Chinese and Europeans.[25] The flag which was eventually adopted featured the Blue Ensign together with a "local scene" of traders in the foreground and both European-style and Chinese-style trading ships in the background.
 During the Second World War, Hong Kong was seized and occupied by the Empire of Japan from 1941 to 1945. During the occupation, the Japanese military government used the flag of Japan in its official works in Hong Kong.[26]
 The flag was similar in design to that previously used. It featured a British Blue Ensign with a local waterfront scene.
 A coat of arms for Hong Kong was granted on 21 January 1959 by the College of Arms in London. The Hong Kong flag was revised in the same year to feature the coat of arms in the Blue Ensign flag. This design was used officially from 1959 until Hong Kong's transfer of sovereignty in 1997.[23] Since then, the colonial flag has been appropriated by protestors, such as on the annual 1 July marches for universal suffrage, as a "symbol of antagonism towards the mainland",[27] along with a blue flag featuring the coat of arms, used by those advocating independence. The flag features a British Blue Ensign with the coat of arms of Hong Kong (1959–1997).
 Portuguese navy occupied Tamão (present-day Tuen Mun, Lantau, and Kwai Chung) in 1514. During this time, the flag of the Kingdom of Portugal was used to represent the territory.[citation needed]
 The flag of the Regional Council represented the governmental body which oversaw matters related to the outlying areas of the territory during the colonial period. The flag itself featured a stylised white R at a 45-degree angle on a dark green background.
 The flag of the Urban Council represented the governmental body which was responsible for matters pertaining to the urban areas of the territory during the colonial period. The flag itself features a simplified white Bauhinia blakeana on a magenta background.
 Before Hong Kong's transfer of sovereignty, between 20 May 1987 and 31 March 1988, a contest was held amongst Hong Kong residents to help choose a flag for post-colonial Hong Kong, with 7,147 design submissions, in which 4,489 submissions were about flag designs.[28] Architect Tao Ho was chosen as one of the panel judges to pick Hong Kong's new flag. He recalled that some of the designs had been rather funny and with political twists: "One had a hammer and sickle on one side and a dollar sign on the other."[29] Some designs were rejected because they contained copyrighted materials, for example, the emblem of Urban Council, Hong Kong Arts Festival and Hong Kong Tourism Board.[28] Six designs were chosen as finalists by the judges, but were all later rejected by the PRC. Ho and two others were then asked by the PRC to submit new proposals.[6]
 Looking for inspiration, Ho wandered into a garden and picked up a Bauhinia blakeana flower. He observed the symmetry of the five petals, and how their winding pattern conveyed to him a dynamic feeling. This led him to incorporate the flower into the flag to represent Hong Kong.[6] The design was adopted on 4 April 1990 at the Third Session of the Seventh National People's Congress,[1] and the flag was first officially hoisted seconds after midnight on 1 July 1997 in the handover ceremony marking the transfer of sovereignty. It was hoisted together with the national PRC flag, while the Chinese national anthem, "March of the Volunteers", was played. The Union Flag and the colonial Hong Kong flag were lowered seconds before midnight.[5]
 In 2019, in conjunction with the anti-extradition bill protests, a variation of the flag, in which the flower appears on a black background and, in some versions, with wilted and blood-stained petals, emerged as a symbol of rebellion and protest. It was widely identified as the Black Bauhinia.[30]
 Badiucao, a Chinese cartoonist and political dissident, designed the Lennon Wall Flag, a symbol of Hong Kong's pro-democracy movement.[31] According to Badiucao, the flag was inspired directly by the Lennon Wall in Hong Kong. It consists of 96 coloured squares which symbolize the post-it notes on the walls: "Every colour on the flag is a different voice. And every individual voice deserves its place in Hong Kong." The number 96 represents the year 1996, the year before the handover of Hong Kong.[31]
 
 1 Current design

1.1 Symbolism
1.2 Construction
1.3 Size specifications
1.4 Colour specifications
1.5 Manufacture regulations

 1.1 Symbolism 1.2 Construction 1.3 Size specifications 1.4 Colour specifications 1.5 Manufacture regulations 2 Protocol

2.1 Display

2.1.1 Half-mast


2.2 Prohibition of use and desecration

 2.1 Display

2.1.1 Half-mast

 2.1.1 Half-mast 2.2 Prohibition of use and desecration 3 Previous Flags of Hong Kong

3.1 Pre-colonial period

3.1.1 Qing dynasty (1862–1895)


3.2 Colonial flags

3.2.1 Use of Union Flag (1843–1871)
3.2.2 First colonial flag (1871–1876)
3.2.3 Second colonial flag (1876–1955)
3.2.4 Japanese occupation period (1941–1945)
3.2.5 Third colonial flag (1955–1959)
3.2.6 Fourth colonial flag (1959–1997)


3.3 Regional flags

3.3.1 Flag of Portuguese territory (1514–1521)



 3.1 Pre-colonial period

3.1.1 Qing dynasty (1862–1895)

 3.1.1 Qing dynasty (1862–1895) 3.2 Colonial flags

3.2.1 Use of Union Flag (1843–1871)
3.2.2 First colonial flag (1871–1876)
3.2.3 Second colonial flag (1876–1955)
3.2.4 Japanese occupation period (1941–1945)
3.2.5 Third colonial flag (1955–1959)
3.2.6 Fourth colonial flag (1959–1997)

 3.2.1 Use of Union Flag (1843–1871) 3.2.2 First colonial flag (1871–1876) 3.2.3 Second colonial flag (1876–1955) 3.2.4 Japanese occupation period (1941–1945) 3.2.5 Third colonial flag (1955–1959) 3.2.6 Fourth colonial flag (1959–1997) 3.3 Regional flags

3.3.1 Flag of Portuguese territory (1514–1521)

 3.3.1 Flag of Portuguese territory (1514–1521) 4 Flags used by government departments

4.1 Council flags

4.1.1 Hong Kong Regional Council
4.1.2 Hong Kong Urban Council


4.2 Potential alternatives

 4.1 Council flags

4.1.1 Hong Kong Regional Council
4.1.2 Hong Kong Urban Council

 4.1.1 Hong Kong Regional Council 4.1.2 Hong Kong Urban Council 4.2 Potential alternatives 5 Flags used at the 2019 Hong Kong protests

5.1 Black Bauhinia
5.2 Lennon Wall Flag

 5.1 Black Bauhinia 5.2 Lennon Wall Flag 6 See also 7 References 8 External links President of the People's Republic of China Chairman of the Standing Committee of the National People's Congress Premier of the State Council Chairman of the Central Military Commission Chairman of the National Committee of the Chinese People's Political Consultative Conference Persons who have made outstanding contributions to the People's Republic of China as the Central People's Government advises the Chief Executive. Persons who have made outstanding contributions to world peace or the cause of human progress as the Central People's Government advises the Chief Executive. Persons whom the Chief Executive considers to have made outstanding contributions to the Hong Kong Special Administrative Region or for whom he or she considers it appropriate to fly the flag at half-mast. 



 



 



 



 



 



 



 



 



 



 



 



 



 



 



 



 Hong Kong portal List of Hong Kong flags Emblem of Hong Kong List of Chinese flags List of British flags Flag of Macau ^ a b "Decision of the National People's Congress on the Basic Law of the Hong Kong Special Administration Region of the People's Republic of China". Government of Hong Kong. 4 April 1990. Retrieved 1 November 2009..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ Laws and Regulations of the People's Republic of China. China Legal Publishing House. 2001. p. iv. ISBN 7-80083-759-9.
 ^ "Basic Law Full Text". Hong Kong Special Administrative Region. Retrieved 20 March 2009.
 ^ "Regional Flag and Regional Emblem Ordinance" (PDF). Bilingual Laws Information System. 1 July 1997. Retrieved 25 July 2009.
 ^ a b Jeffrey Aaronson. "Schedule of Events". Time. Archived from the original on 5 October 2009. Retrieved 1 November 2009.
 ^ a b c d 忆香港区旗区徽的诞生（上） [Reflecting on the Creation of the Hong Kong SAR Flag and Emblem – Part 1] (in Chinese). Wenhui-xinmin United Press Group. 24 May 2007. Retrieved 20 March 2009.[dead link] and 忆香港区旗区徽的诞生（下） [Reflecting on the Creation of the Hong Kong SAR Flag and Emblem – Part 2] (in Chinese). Wenhui-xinmin United Press Group. 25 May 2007. Retrieved 20 March 2009.[dead link]
 ^ a b c (Schedule 1 of the Regional Flag and Regional Emblem Ordinance) "Regional Flag and Regional Emblem Ordinance" (PDF). Bilingual Laws Information System. 1 July 1997. Retrieved 25 July 2009.
 ^ Elihu Lauterpacht, C. J. Greenwood, A. G. Oppenheimer (2002). International Law Reports. 122. Cambridge University Press. p. 582. ISBN 978-0-521-80775-3.CS1 maint: uses authors parameter (link)
 ^ (Schedule 5 of the Regional Flag and Regional Emblem Ordinance) "Regional Flag and Regional Emblem Ordinance" (PDF). Bilingual Laws Information System. 1 July 1997. Retrieved 25 July 2009.
 ^ a b c "Stipulations for the Display and Use of The National Flag and National Emblem and The Regional Flag and Regional Emblem" (PDF). Protocol Division Government Secretariat of the Hong Kong SAR. Retrieved 1 November 2009.
 ^ a b c "Display of the Flags and Emblems". Protocol Division Government Secretariat of the Hong Kong SAR. 6 September 2005. Retrieved 21 March 2009.
 ^ a b c (Schedule 4 of the Regional Flag and Regional Emblem Ordinance) "Regional Flag and Regional Emblem Ordinance" (PDF). Bilingual Laws Information System. 1 July 1997. Retrieved 25 July 2009.
 ^ (Section 6 of the Regional Flag and Regional Emblem Ordinance) "Regional Flag and Regional Emblem Ordinance" (PDF). Bilingual Laws Information System. 1 July 1997. Retrieved 25 July 2009.
 ^ (Section 7 of the Regional Flag and Regional Emblem Ordinance) "Regional Flag and Regional Emblem Ordinance" (PDF). Bilingual Laws Information System. 1 July 1997. Retrieved 25 July 2009.
 ^ "CAP 2401, Section 6 – Prohibition on certain uses of national flag and national emblem". Bilingual Laws Information System. 1 July 1997. Retrieved 25 July 2009.
 ^ "CAP 2401, Section 7 – Protection of the national flag and national emblem". Bilingual Laws Information System. 1 July 1997. Retrieved 25 July 2009.
 ^ "FINAL APPEAL NO. 4 OF 1999 (CRIMINAL)". Court of Final Appeal. 15 December 1999. Retrieved 25 July 2009.
 ^ "Annual Report 2001". Hong Kong Journalists Association. 9 August 2001. Archived from the original on 11 October 2014. Retrieved 25 July 2009.
 ^ "Hong Kong Jails Chinese Farmer For Flag-Burning". Arab Times. 20 September 2011. Retrieved 23 September 2011.
 ^ Alex Lo (5 November 2012). "Flag-wavers have right to be ridiculous". South China Morning Post. Retrieved 21 October 2016.
 ^ Joshua But; Tony Cheung (2 November 2012). "Hong Kong chief executive urges people not to wave colonial flag". South China Morning Post. Retrieved 21 October 2016.
 ^ Mierzejewski, Dominik; Kowalski, Bartosz (2019). China’s Selective Identities: State, Ideology and Culture. Global Political Transitions. Singapore: Springer Singapore. doi:10.1007/978-981-13-0164-3. ISBN 9789811301636.
 ^ a b c d e "Colonial Hong Kong". Flags of the World. 18 August 2007. Retrieved 20 March 2009.
 ^ "Minutes of Meeting: LegCo 15th April 1912" (PDF). Hong Kong LegCo Archives. The Hong Kong Legislative Council. Retrieved 10 May 2019.
 ^ "Minutes of Meeting: 28th December 1911" (PDF). Hong Kong Legislative Council Archives. Hongkong Legislative Council. Retrieved 10 May 2019.
 ^ Carroll, John M. (2007). A Concise History of Hong Kong. Rowman & Littlefield Publishers. p. 124. ISBN 9780742574694.
 ^ A.T. (4 July 2012). "Free speech in Hong Kong: Show of strength". Analects. Hong Kong. Retrieved 24 July 2012.
 ^ a b 香港区旗区徽诞生记. China Art News. 1 January 2007. Archived from the original on 30 December 2013. Retrieved 29 December 2013.
 ^ Andrea Hamilton. "Bringing You The Handover: Meet some of the most important men and women working behind the scenes". Asiaweek. Archived from the original on 7 November 2007. Retrieved 20 March 2009.
 ^ "Enraged Protesters Storm the Legislature on the Anniversary of Hong Kong's Return to Chinese Sovereignty". Time. 1 July 2019. Retrieved 4 July 2019.
 ^ a b Holmes, Chan (10 September 2019). "Political cartoonist Badiucao unveils new 'Lennon Wall flag' for Hong Kong democracy movement". Hong Kong Free Press HKFP. Retrieved 14 September 2019.
 Hong Kong at Flags of the World Flags and Emblems of Hong Kong CAP 2602 Regional Flag and Regional Emblem Ordinance v t e Afghanistan Armenia Azerbaijan Bahrain Bangladesh Bhutan Brunei Cambodia China Cyprus East Timor (Timor-Leste) Egypt Georgia India Indonesia Iran Iraq Israel Japan Jordan Kazakhstan North Korea South Korea Kuwait Kyrgyzstan Laos Lebanon Malaysia Maldives Mongolia Myanmar Nepal Oman Pakistan Philippines Qatar Russia Saudi Arabia Singapore Sri Lanka Syria Tajikistan Thailand Turkey Turkmenistan United Arab Emirates Uzbekistan Vietnam Yemen Abkhazia Artsakh Northern Cyprus Palestine South Ossetia Taiwan British Indian Ocean Territory Christmas Island Cocos (Keeling) Islands Hong Kong Macau  Book  Category  Asia portal v t e Prehistoric Imperial China
Bao'an County and Xin'an County Bao'an County and Xin'an County British colony 1800s–1930s Battle of Hong Kong Japanese occupation 1950s 1960s 1970s 1980s 1990s 2000s Administrative divisions Areas Bays Beaches Buildings and structures Channels Cities and towns Country parks and conservation Ecology Land reclamation Harbours Islands and peninsulas Lakes Marine parks Mountains and peaks Public parks and gardens Reservoirs Rivers Villages National anthem Basic Law Chief Executive Districts Elections Flag Foreign relations Government Independence Judiciary Legal system Legislative Council LGBT rights Military One country, two systems Political parties Protests Sino-British Joint Declaration Special administrative regions of China Education Fire services Food and Health Bureau Healthcare Police Royal Hong Kong Regiment Banks Companies Dollar (currency) Employment Hawkers Port Stock Exchange Communications The Hongs Tourism Airlines Airport Buses Cycling Ferries List of roads Rail Taxis Architecture Cinema Comics Cheung Chau Bun Festival Cuisine Gambling Graffiti Hong Kong orchid Honours system Languages Lion Rock Spirit Literature McDull Media Museums Music Ngai jong People
Demographics
Women
Youth Demographics Women Youth Public holidays Religion RTHK Shopping Sport Television Government of Hong Kong Regional symbols of Hong Kong Flags of Hong Kong Flags introduced in 1997 Red and white flags CS1 uses Chinese-language script (zh) CS1 Chinese-language sources (zh) All articles with dead external links Articles with dead external links from March 2012 CS1 maint: uses authors parameter Use dmy dates from November 2018 EngvarB from November 2018 Articles containing Chinese-language text All articles with unsourced statements Articles with unsourced statements from October 2019 Pages using multiple image with auto scaled images Wikipedia articles that may have off-topic sections from October 2019 All articles that may have off-topic sections Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version العربية Azərbaycanca Bosanski Català Čeština Cymraeg Dansk Deutsch Eesti Ελληνικά Español Euskara فارسی Français 한국어 Hrvatski Ido Bahasa Indonesia Italiano עברית Latviešu Lietuvių Magyar Македонски മലയാളം Bahasa Melayu Nederlands 日本語 Norsk Polski Português Русский Scots Slovenčina Српски / srpski Srpskohrvatski / српскохрватски Suomi Svenska ไทย Türkçe Українська Tiếng Việt Yorùbá 粵語 中文  This page was last edited on 9 November 2019, at 17:49 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  flag of Hong Kong regional flag of the Hong Kong Special Administrative Region of the People's Republic of China A selection of proposals during the 1987–1988 contest is shown below: may stray from the topic of the article a b ^ ^ ^ a b a b c d a b c ^ 122 ^ a b c a b c a b c ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b c d e ^ ^ ^ ^ a b ^ ^ a b Flags of Hong Kong   Civil and state flag, civil and state ensign normal 2:3 Approved on 4 April 1990Used since 1 July 1997 A stylised, white, five-petal Bauhinia blakeana flower in the centre of a red field Tao Ho 
  中華人民共和國香港特別行政區區旗 中华人民共和国香港特别行政区区旗 TranscriptionsStandard MandarinHanyu PinyinZhōnghuá Rénmín Gònghéguó Xiānggǎng Tèbié Xíngzhèngqū QūqíYue: CantoneseYale RomanizationJūngwàh Yàhnmàhn Guhngwòhgwok Hēunggóng Dahkbiht Hàhngjing Kēui KēuikèihJyutpingZung1waa4 Jan4man4 Gung6wo4gwok3 Hoeng1gong2 Dak6bit6 Hang4zing3 Keoi1 Keoi1kei4 Zhōnghuá Rénmín Gònghéguó Xiānggǎng Tèbié Xíngzhèngqū Qūqí Jūngwàh Yàhnmàhn Guhngwòhgwok Hēunggóng Dahkbiht Hàhngjing Kēui Kēuikèih Zung1waa4 Jan4man4 Gung6wo4gwok3 Hoeng1gong2 Dak6bit6 Hang4zing3 Keoi1 Keoi1kei4 
 “
 The regional flag carries a design of five bauhinia petals, each with a star in the middle, on a red background.  The red flag represents the motherland and the bauhinia represents Hong Kong.  The design implies that Hong Kong is an inalienable part of China and prospers in the embrace of the motherland.  The five stars on the flower symbolise the fact that all Hong Kong compatriots love their motherland, while the red and white colours embody the principle of "one country, two systems".[8]
 ”
 1
 288 × 192
 2
 240 × 160
 3
 192 × 128
 4
 144 × 96
 5
 96 × 64
 Car flag
 30 × 20
 Flag for signing ceremonies
 21 × 14
 Desktop flag
 15 × 10
 Red
 
 #FF0000
 0-100-90-0
 Chinese red
 0°,100%,50%
 186
 White
 
 #FFFFFF
 0-0-0-0
 White
 0°,100%,100%
 
  This section may stray from the topic of the article. Please help improve this section or discuss this issue on the talk page. (October 2019)  Wikimedia Commons has media related to Flags of Hong Kong. 
Afghanistan
Armenia
Azerbaijan
Bahrain
Bangladesh
Bhutan
Brunei
Cambodia
China
Cyprus
East Timor (Timor-Leste)
Egypt
Georgia
India
Indonesia
Iran
Iraq
Israel
Japan
Jordan
Kazakhstan
North Korea
South Korea
Kuwait
Kyrgyzstan
Laos
Lebanon
Malaysia
Maldives
Mongolia
Myanmar
Nepal
Oman
Pakistan
Philippines
Qatar
Russia
Saudi Arabia
Singapore
Sri Lanka
Syria
Tajikistan
Thailand
Turkey
Turkmenistan
United Arab Emirates
Uzbekistan
Vietnam
Yemen
 
Abkhazia
Artsakh
Northern Cyprus
Palestine
South Ossetia
Taiwan
 
British Indian Ocean Territory
Christmas Island
Cocos (Keeling) Islands
Hong Kong
Macau
 
 Book
 Category
 Asia portal
 
Prehistoric
Imperial China
Bao'an County and Xin'an County
British colony
1800s–1930s
Battle of Hong Kong
Japanese occupation
1950s
1960s
1970s
1980s
1990s
2000s
 
Administrative divisions
Areas
Bays
Beaches
Buildings and structures
Channels
Cities and towns
Country parks and conservation
Ecology
Land reclamation
Harbours
Islands and peninsulas
Lakes
Marine parks
Mountains and peaks
Public parks and gardens
Reservoirs
Rivers
Villages
 
National anthem
Basic Law
Chief Executive
Districts
Elections
Flag
Foreign relations
Government
Independence
Judiciary
Legal system
Legislative Council
LGBT rights
Military
One country, two systems
Political parties
Protests
Sino-British Joint Declaration
Special administrative regions of China
 
Education
Fire services
Food and Health Bureau
Healthcare
Police
Royal Hong Kong Regiment
 
Banks
Companies
Dollar (currency)
Employment
Hawkers
Port
Stock Exchange
Communications
The Hongs
Tourism
 
Airlines
Airport
Buses
Cycling
Ferries
List of roads
Rail
Taxis
 
Architecture
Cinema
Comics
Cheung Chau Bun Festival
Cuisine
Gambling
Graffiti
Hong Kong orchid
Honours system
Languages
Lion Rock Spirit
Literature
McDull
Media
Museums
Music
Ngai jong
People
Demographics
Women
Youth
Public holidays
Religion
RTHK
Shopping
Sport
Television
 