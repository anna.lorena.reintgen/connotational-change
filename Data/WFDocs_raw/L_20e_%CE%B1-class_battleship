L 20e α was a design for a class of battleships to be built in 1918 for the German Kaiserliche Marine (Imperial Navy) during World War I. Design work on the class of battleship to succeed the Bayern-class battleships began in 1914, but the outbreak of World War I in July 1914 led to these plans being shelved. Work resumed in early 1916 and lessons from the Battle of Jutland, fought later that year, were incorporated into the design. Reinhard Scheer, the commander of the fleet, wanted larger main guns and a higher top speed than earlier vessels, to combat the latest ships in the British Royal Navy. A variety of proposals were submitted, with armament ranging from the same eight 38 cm (15 in) guns of the Bayern class to eight 42 cm (16.5 in) guns.
 Work on the design was completed by September 1918, but by then there was no chance for them to be built. Germany's declining war situation and the reallocation of resources to support the U-boat campaign meant the ships would never be built. The ships would have been significantly larger than the preceding Bayern-class battleships, at 238 m (780 ft 10 in) long, compared to 180 m (590 ft 7 in) for the preceding ships. The L 20e α class would have been significantly faster, with a top speed of 26 knots (48 km/h; 30 mph), compared to the 21-knot (39 km/h; 24 mph) maximum of the Bayerns and would have been the first German warships to have mounted guns larger than 38 cm.
 Just before the start of the 20th century, Germany embarked on a naval expansion to challenge British control of the seas, under the direction of Vizeadmiral (Vice Admiral) Alfred von Tirpitz. Over the following decade, Germany built some two dozen pre-dreadnought battleships over the Brandenburg, Kaiser Friedrich III, Wittelsbach, Braunschweig and Deutschland classes. The dreadnought revolution disrupted German plans but Tirpitz nevertheless continued his program, securing the construction of a further twenty-one dreadnought battleships by 1914, with the Nassau, Helgoland, Kaiser, König, and Bayern classes.[1][2]
 Beginning before World War I broke out in July 1914, the German Kaiserliche Marine began planning for the battleship design for the 1916 construction program, which would follow the Bayern-class battleships that were then under construction. The Bayerns were armed with a main battery of 38-centimeter (15 in) guns in four twin-gun turrets. The British had begun building the similarly-armed Queen Elizabeth and Revenge-class battleships and the Germans intended the 1916 battleship design to be superior to these and designs were drawn up with an armament of ten or twelve 38 cm guns. The designs included versions with the standard twin-gun turrets favored by the German navy, along with variants with both twin and quadruple turrets similar to the French Normandie-class battleships that had been laid down in 1913. The outbreak of war led to the abandonment of the plans.[3]
 By 1916, work had resumed on new battleship designs and, in April, the first three proposals were submitted: the L 1, L 2 and L 3 designs, which were similar to the Ersatz Yorck-class battlecruisers then also under development. The battleships were the same size as the battlecruisers and L 1 and L 3 had the same armament of eight 38 cm guns (L 2 would have mounted ten of those guns) but they would have had a top speed of 25 to 26 knots (46 to 48 km/h; 29 to 30 mph) compared to the 29 to 29.5 knots (53.7 to 54.6 km/h; 33.4 to 33.9 mph) speeds of the Ersatz Yorcks and heavier armor. Work on the designs continued at a slow pace, with thought given to armament alternatives, including batteries of eight or ten 38 cm or eight 42 cm (17 in) guns.[3]
 In January 1916, Vizeadmiral Reinhard Scheer became commander in chief of the High Seas Fleet.[5] Following the Battle of Jutland on 31 May – 1 June 1916, Scheer pushed for new, more powerful battleships, which were in concert with Kaiser Wilhelm II's call for what he referred to as the "Einheitsschiff" (unified ship) that combined the armor and firepower of battleships and the high speed of battlecruisers. Another faction in the naval command, led by Admiral Eduard von Capelle, the State Secretary of the Reichsmarineamt (RMA—Imperial Navy Office), opposed the idea and favored traditional, differentiated capital ship designs. Scheer demanded that the new ships should have guns of 42 cm caliber, an armored belt 350 mm (14 in) thick and be capable of speeds of up to 32 knots (59 km/h; 37 mph), all on a displacement of up to 50,000 metric tons (49,000 long tons).[6] The new 42 cm gun was designed by 29 December 1916 and was approved on 11 September 1918, though none were built.[7][8]
 By the end of 1916, design work on three proposals to meet Scheer's specifications was complete, all of which displaced around 42,000 metric tons (41,000 long tons). L 20b, L 21b and L 22c; L 20b would have eight 42 cm guns, L 21b and L 22c ten or eight 38 cm guns, respectively.[3] After the beginning of unrestricted submarine warfare in February 1917, Capelle argued that capital ship construction should not be halted in favor of U-boat construction.[9] Work on L 20b continued, as the naval command preferred the 42 cm gun variant, with a refined version submitted on 21 August 1917 as L 20e; a new design, L 24, was also submitted, which was similar to L 20e but was slightly longer, faster by 1.5 knots (2.8 km/h; 1.7 mph), had two extra boilers and a correspondingly wider funnel. It also differed in the placement of the torpedo armament. The L 20 design placed them in the hull below the waterline, while the L 24 proposal used above-water launchers.[3][9] Displacement for the designs was fixed at 45,000 t (44,000 long tons).[10] Both ships had a top speed of only 23 knots (43 km/h; 26 mph), which was unacceptable to Scheer.[11]
 By October 1917, the L 20e and L 24e designs were refined into the L 20e α and L 24e α versions; these displaced 44,500 t (43,800 long tons) and 45,000 t respectively. Secondary batteries were reduced to twelve guns, compared to the sixteen guns of the Bayern class.[3][12] L 24e α also had an additional pair of torpedo tubes, mounted above the waterline, compared to L 20e α. The armor layout for both designs was similar to that of the Bayern class. The proposals were submitted to the naval command in January 1918; Wilhelm II continued to stress the importance of the "Einheitsschiff" concept and he suggested that the speed of the design might be significantly increased by removing the forward superfiring turret and the submerged torpedo tubes. For his part, Scheer asked whether triple or quadruple turrets might be used to save enough weight for speed to be increased to 30 knots (56 km/h; 35 mph), which delayed completion of the design until mid-1918. By that time, the studies that had been completed suggested that the weight savings would be minimal and that the more crowded triple or quadruple turrets would reduce the rate of fire too much.[3]
 Two more proposals were completed in mid-1918; the first was almost the same as the L 20e α variant and the second was similar but had only six main battery guns and a top speed of 28 knots (52 km/h; 32 mph). By 11 September 1918, the L 20e α variant was selected as the basis for the next battleship to be built.[3] During the design process, it was decided that the utmost concern was that the ships could be built and placed into service quickly. The ships were to discard the use of broadside belt armor below the waterline, the attachment of which was an extremely time-consuming process. It was believed that the higher speed of the class—26 knots (48 km/h; 30 mph)—would make up for the vulnerability to torpedo attack and make the armor unnecessary.[13][a]
 The ships were never built, primarily because the shipyard capacity available that late in the war had largely been diverted to support the U-boat campaign. The work that would have been necessary to design and test the new 42 cm turret clashed with U-boat construction, which had become the priority of the Navy. Krupp, the firm that had been awarded the contract to conduct the testing, informed the RMA that design work on the new turret would have to wait and Capelle accepted the news without much objection. The RMA filed a report dated 1 February 1918, which stated that capital ship construction had stopped, primarily due to the shifting priorities to the U-boat war.[13] Though the ships of the class were never built, the naval historian Timothy Mulligan notes that with "the unresolved dilemma of conflicting design concepts and overly ambitious demands in battleship characteristics ..." that the L 20 α design represented, "... the Imperial Navy bequeathed a dubious legacy to its successors".[8]
 The L 20e α design was 238 m (781 ft) long at the waterline, with a beam of 33.5 m (110 ft) and a draft of 9 m (30 ft). Displacement was to be approximately 44,500 metric tons (43,800 long tons) as designed and up to 49,500 metric tons (48,700 long tons) fully loaded. The ships were intended to have the typical single tripod foremast mounted atop the large, forward superstructure and a lighter pole main mast aft of the funnel. They to have been powered by either two or four sets of steam turbines driving four shafts, which were to have a combined output of 100,000 shaft horsepower (75,000 kW). The steam plant consisted of six oil-fired and sixteen coal-fired boilers trunked into a large funnel. Bunkerage was 3,000 metric tons (2,953 long tons) of coal and 2,000 metric tons (1,968 long tons) of fuel oil.[15][16] Externally, the ships were similar to the Ersatz Yorck-class battlecruisers.[17]
 The main battery was arranged in four twin-gun turrets, as in the preceding Bayern class, in a superfiring arrangement on the center line; the aft pair of turrets were separated by engine rooms. The four turrets each mounted two 42 cm SK L/45 guns,[b] for a total of eight guns on the broadside.[16][19] The 42 cm gun fired a 1,000-kilogram (2,200 lb) shell out to 33,000 m (36,000 yd) at the maximum elevation of 30 degrees. The estimated muzzle velocity was 800 meters per second (2,600 ft/s)[7] The ships were to have been armed with a secondary battery of twelve 15 cm (5.9 in) SK L/45 guns mounted in casemates in the main deck around the superstructure. The anti-aircraft battery was to have consisted of either eight 8.8 cm (3.5 in) SK L/45 guns or eight 10.5 cm (4.1 in) SK L/45 guns. Four of these would have been mounted on either side of the forward conning tower on the upper deck and the other four would have been abreast of the rear superfiring turret on the main deck. The design was to have been equipped with three submerged torpedo tubes, either 60 or 70 cm (23.6 or 27.6 in) in diameter. One tube was placed in the bow, the other two on either beam to the rear of the engine rooms.[15][16]
 The ships had a 350 mm (13.8 in) armored belt running from slightly forward of the fore barbette to slightly aft of the fourth barbette. Aft of the rearmost turret the belt was reduced to 300 mm (11.8 in), though it did not extend all the way to the stern. In the forward part of the ship, the belt was reduced to 250 mm (9.8 in) and the bow received only splinter protection in the form of 30 mm (1.2 in) thick plate. The belt began 35 cm (13.8 in) below the waterline and extended to 195 cm (76.8 in) above it.[16] Directly above the main belt was a 250 mm thick strake of armor plating which extended up to the upper deck.[20] The ships' armored deck was to have been 50 mm (2 in) thick forward, increased to 50–60 mm (2.4 in) amidships and 50 to 120 mm (4.7 in) aft. Additional horizontal protection forward consisted of a forecastle deck that was 20 to 40 mm (0.8 to 1.6 in) thick. The ships were also protected by a torpedo bulkhead that was 50–60 mm thick. A sloped 30 mm thick splinter bulkhead to protect against shell fragments, extended from the top of the torpedo bulkhead up to the upper deck.[15][16]
 The barbettes were also 350 mm thick on the front and sides, decreasing to 250 mm on the rear. Their lower portions, which were protected by the belt armor, were significantly reduced to 100 mm (3.9 in). The main gun turrets had 350 mm faces, 250 mm sides, 305 mm (12 in) rears, and 150 to 250 mm (5.9 to 9.8 in) roofs. The secondary guns were protected with 170 mm (6.7 in) of armor plate. The forward conning tower had 350 to 400 mm (13.8 to 15.7 in) of armor protection and the aft conning tower received just 250 mm of side protection.[15][16][20]
 See also: List of ships of the Imperial German Navy
 
 Normal: 43,800 t (43,100 long tons) Full load: 48,700 t (47,900 long tons) Normal: 9 m (29 ft 6 in) Full load: 9.90 m (32 ft 6 in) 100,000 shp (75,000 kW) 22 Schulz-Thornycroft boilers 8 × 42 cm (16.5 in) SK L/45 guns 12 × 15 cm (5.9 in) SK L/45 guns 8 × 8.8 cm (3.5 in) SK L/45 or 10.5 cm (4.1 in) SK L/45 guns 3 × 60 cm (23.6 in) or 70 cm (27.6 in) torpedo tubes Belt: 30 to 350 mm (1.2 to 13.8 in) Bulkheads: 60 to 250 mm (2.4 to 9.8 in) Casemate: 170 mm (6.7 in) Barbettes: 100 to 350 mm (3.9 to 13.8 in) Turrets: 150 to 350 mm (5.9 to 13.8 in) Conning tower: 350 to 400 mm (13.8 to 15.7 in) 1 Background 2 Development and cancellation 3 Characteristics

3.1 General characteristics and machinery
3.2 Armament
3.3 Armor

 3.1 General characteristics and machinery 3.2 Armament 3.3 Armor 4 Notes

4.1 Footnotes
4.2 Citations

 4.1 Footnotes 4.2 Citations 5 References ^  In previous battleship designs, such as the Bayern class, the main belt armor extended to 35 cm (14 in) below the waterline and then tapered to 17.2 cm (6.8 in) at the bottom edge. The L 20e α design discarded the use of the lower section of belt armor.[14]
 ^  In Imperial German Navy gun nomenclature, "SK" (Schnelladekanone) denotes that the gun is quick loading, while the L/45 denotes the length of the gun. In this case, the L/45 gun is 45 calibers, meaning that the gun is 45 times as long as it is in bore diameter.[18]
 ^ Herwig, pp. 33–81.
 ^ Gardiner & Gray, pp. 134–149.
 ^ a b c d e f g Dodson, p. 126.
 ^ a b c d e f g h i j k l m n Dodson, p. 230.
 ^ Herwig, p. 161.
 ^ Herwig, pp. 223–224.
 ^ a b Friedman, p. 131.
 ^ a b Mulligan, p. 1017.
 ^ a b Forstmeier & Breyer, p. 44.
 ^ Forstmeier & Breyer, p. 45.
 ^ Forstmeier & Breyer, p. 46.
 ^ Gröner, p. 30.
 ^ a b Weir, p. 179.
 ^ Gardiner & Gray, pp. 149–159.
 ^ a b c d Dodson, p. 232.
 ^ a b c d e f Gardiner & Gray, p. 150.
 ^ Forstmeier & Breyer, p. 83.
 ^ Grießmer, p. 177.
 ^ Campbell, p. 13.
 ^ a b Campbell, p. 20.
 Battleships portal Campbell, N. J. M. (1977).  Preston, Antony (ed.). "German Dreadnoughts and Their Protection". Warship. London: Conway Maritime Press. I (4): 12–20. ISSN 0142-6222..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} Dodson, Aidan (2016). The Kaiser's Battlefleet: German Capital Ships 1871–1918. Barnsley: Seaforth Publishing. ISBN 978-1-84832-229-5. Forstmeier, Friedrich & Breyer, Siegfried (2002). Deutsche Großkampfschiffe 1915 bis 1918 – Die Entwicklung der Typenfrage im Ersten Weltkrieg [German Large Warships 1915 to 1918 –The Development of the Type in the First World War] (in German). Bonn: Bernard & Graefe. ISBN 978-3-7637-6230-9. Friedman, Norman (2011). Naval Weapons of World War One. Annapolis: Naval Institute Press. ISBN 978-1-84832-100-7. Gardiner, Robert; Gray, Randal, eds. (1985). Conway's All the World's Fighting Ships: 1906–1921. Annapolis: Naval Institute Press. ISBN 978-0-87021-907-8. Grießmer, Axel (1999). Die Linienschiffe der Kaiserlichen Marine [The Battleships of the Imperial Navy] (in German). Bonn: Bernard & Graefe Verlag. ISBN 978-3-7637-5985-9. Gröner, Erich (1990). German Warships: 1815–1945. Vol. I: Major Surface Vessels. Annapolis: Naval Institute Press. ISBN 978-0-87021-790-6. Herwig, Holger (1998) [1980]. "Luxury" Fleet: The Imperial German Navy 1888–1918. Amherst: Humanity Books. ISBN 978-1-57392-286-9. Mulligan, Timothy P. (October 2005). "Ship-of-the-Line or Atlantic Raider? Battleship "Bismarck" between Design Limitations and Naval Strategy". The Journal of Military History. 69 (4): 1013–1044. ISSN 1543-7795. Weir, Gary E. (1992). Building the Kaiser's Navy: The Imperial Navy Office and German Industry in the Tirpitz Era, 1890–1919. Annapolis: Naval Institute Press. ISBN 978-1-55750-929-1. v t e None built Preceded by: Bayern class Followed by: Scharnhorst class List of battleships of Germany v t e Nassau Helgoland Kaiser König Bayern L 20e αX Brandenburg Kaiser Friedrich III Wittelsbach Braunschweig Deutschland SMS Von der TannS Moltke SMS SeydlitzS Derfflinger MackensenX Ersatz YorckX SMS Fürst BismarckS SMS Prinz HeinrichS Prinz Adalbert Roon Scharnhorst SMS BlücherS SMS HelaS Gazelle Bremen Königsberg Dresden Nautilus Kolberg Magdeburg Karlsruhe Graudenz Pillau Wiesbaden Königsberg Brummer Cöln FK proposalsX SMS Kaiserin AugustaS Victoria Louise 1898 1906 1911 1913 1914 1916 1916 1917 1918X "I"X U 1 U 2 U 3 U 5 U 9 U 13 U 16 U 17 U 19 U 23 U 27 U 31 U 43 U 51 U 57 U 63 U 66 U 81 U 87 U 93 U 115X U 127 U 131X U 135 U 139 U 142 U 151 UA UB I UB II UB III UC I UC II UC III UD 1X UE I UE II UFX UGX World War I battleships of Germany Battleship classes Proposed ships of Germany Battleships of the Imperial German Navy CS1 German-language sources (de) CS1: long volume value Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Deutsch فارسی Français Русский 中文  This page was last edited on 15 September 2019, at 03:58 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  L 20e α ^ ^ ^ ^ a b c d e f g a b c d e f g h i j k l m n ^ ^ a b a b a b ^ ^ ^ a b ^ a b c d a b c d e f ^ ^ ^ a b I 69 Single ship of class Cancelled Conversions S
Single ship of class
X
Cancelled
V
Conversions Line-drawing of the L 20e α design
 Name: 
L 20e α class Operators: 
 Imperial German Navy Preceded by: 
Bayern class Succeeded by: 
Scharnhorst class Planned: 
Unknown Completed: 
None Type: 
Super-dreadnought battleship Displacement: 

Normal: 43,800 t (43,100 long tons)
Full load: 48,700 t (47,900 long tons)
 Length: 
238 m (781 ft) Beam: 
33.5 m (110 ft) Draft: 

Normal: 9 m (29 ft 6 in)
Full load: 9.90 m (32 ft 6 in)
 Installed power: 

100,000 shp (75,000 kW)
22 Schulz-Thornycroft boilers
 Propulsion: 
4 shafts, 2 or 4 sets of steam turbines Speed: 
26 knots (48 km/h; 30 mph) Armament: 

8 × 42 cm (16.5 in) SK L/45 guns
12 × 15 cm (5.9 in) SK L/45 guns
8 × 8.8 cm (3.5 in) SK L/45 or 10.5 cm (4.1 in) SK L/45 guns
3 × 60 cm (23.6 in) or 70 cm (27.6 in) torpedo tubes
 Armor: 

Belt: 30 to 350 mm (1.2 to 13.8 in)
Bulkheads: 60 to 250 mm (2.4 to 9.8 in)
Casemate: 170 mm (6.7 in)
Barbettes: 100 to 350 mm (3.9 to 13.8 in)
Turrets: 150 to 350 mm (5.9 to 13.8 in)
Conning tower: 350 to 400 mm (13.8 to 15.7 in)
 L 1
 34,000 t (33,463 long tons)
 8 × 38 cm guns
 26 knots (48 km/h; 30 mph)
 220 m (721 ft 9 in)
 30 m (98 ft 5 in)
 8.6 m (28 ft 3 in)
 L 2
 25 knots (46 km/h; 29 mph)
 L 3
 38,000 t (37,400 long tons)
 26 knots (48 km/h; 30 mph)
 230 m (754 ft 7 in)
 30.4 m (99 ft 9 in)
 8.8 m (28 ft 10 in)
 L 20b
 42,000 t (41,337 long tons)
 8 × 42 cm guns
 22.5 knots (41.7 km/h; 25.9 mph)
 235 m (771 ft)
 32 m (105 ft)
 9 m (29 ft 6 in)
 L 21a
 10 × 38 cm guns
 Unknown
 L 22c
 41,700 t (41,041 long tons)
 8 × 38 cm guns
 L 20e
 42,000 t (41,337 long tons)
 8 × 42 cm guns
 25 knots (46 km/h; 29 mph)
 235 to 237 m (777 ft 7 in)
 L 24
 43,000 t (42,321 long tons)
 26.5 knots (49.1 km/h; 30.5 mph)
 240 m (787 ft 5 in)
 L 24e α
 45,000 t (44,289 long tons)
 27.5 knots (50.9 km/h; 31.6 mph)
 33.5 m (109 ft 11 in)
 
None built
 
Preceded by: Bayern class
Followed by: Scharnhorst class
 
List of battleships of Germany
 
Nassau
Helgoland
Kaiser
König
Bayern
L 20e αX
 
Brandenburg
Kaiser Friedrich III
Wittelsbach
Braunschweig
Deutschland
 
SMS Von der TannS
Moltke
SMS SeydlitzS
Derfflinger
MackensenX
Ersatz YorckX
 
SMS Fürst BismarckS
SMS Prinz HeinrichS
Prinz Adalbert
Roon
Scharnhorst
SMS BlücherS
 
SMS HelaS
Gazelle
Bremen
Königsberg
Dresden
Nautilus
Kolberg
Magdeburg
Karlsruhe
Graudenz
Pillau
Wiesbaden
Königsberg
Brummer
Cöln
FK proposalsX
 
SMS Kaiserin AugustaS
Victoria Louise
 
1898
1906
1911
1913
1914
1916
1916
1917
1918X
 
"I"X
 
U 1
U 2
U 3
U 5
U 9
U 13
U 16
U 17
U 19
U 23
U 27
U 31
U 43
U 51
U 57
U 63
U 66
U 81
U 87
U 93
U 115X
U 127
U 131X
U 135
U 139
U 142
U 151
UA
UB I
UB II
UB III
UC I
UC II
UC III
UD 1X
UE I
UE II
UFX
UGX
 
S
Single ship of class
X
Cancelled
V
Conversions
See also: List of ships of the Imperial German Navy

 