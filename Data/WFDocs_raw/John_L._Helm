
 John LaRue Helm (July 4, 1802 – September 8, 1867) was the 18th and 24th governor of the U.S. Commonwealth of Kentucky, although his service in that office totaled less than fourteen months. He also represented Hardin County in both houses of the Kentucky General Assembly and was chosen to be the Speaker of the Kentucky House of Representatives four times. In 1838 his sole bid for federal office ended in defeat when his opponent, Willis Green, was elected to the U.S. House of Representatives.
 Helm was first elected to the Kentucky House of Representatives in 1826; between 1826 and 1843 he served eleven one-year terms in the state house. In 1844 he was elected to the state senate, where he served continuously until he was chosen as the Whig Party nominee for lieutenant governor on a ticket with John J. Crittenden, famous for the Crittenden Compromise. The Whigs won the general election and Helm was elevated to governor on July 31, 1850, when Crittenden resigned to accept an appointment as United States Attorney General in President Millard Fillmore's cabinet. After his service as governor Helm became president of the struggling Louisville and Nashville Railroad. He invested thousands of dollars of his own money in the project and convinced residents along the line's main route to buy stock in the company. In 1859 the line was completed, but the next year Helm resigned over of differences with the board of directors regarding a proposed branch that would extend the line to Memphis, Tennessee.
 Although he openly opposed secession during the American Civil War, federal military forces labeled Helm a Confederate sympathizer. In September 1862, he was arrested for this alleged sympathy, but Governor James F. Robinson recognized him as he was being transported to a prison in Louisville and had him released. After the war Helm identified with the Democratic Party, and in 1865 Hardin County voters returned him to the state senate. In 1867 he was the state's Democratic candidate for governor. Despite his failing health, Helm made a vigorous canvass of the state and won the general election. He was too weak to travel to Frankfort for his inauguration, so state officials administered the oath of office at his home on September 3, 1867. He died five days later.
 In 1780 Helm's grandfather, Thomas Helm, emigrated to Kentucky from Prince William County, Virginia and founded the settlement of Helm Station near Elizabethtown, Kentucky in Hardin County, where John L. Helm was born on July 4, 1802.[1][2][3] He was the eldest of nine children born to George B. Helm, a farmer and politician, and Rebecca LaRue Helm, a descendant of a prominent local pioneer family.[2][4]
 Helm attended the area's public schools and studied with noted educator Duff Green.[5] When Helm was 14 his father fell on hard financial times and Helm returned to work on the family farm.[6] In 1818 he took a better-paying job in the office of Samuel Haycraft, the circuit court clerk of Hardin County.[6] While there he read law with Haycraft, then entered the law office of Ben Tobin in 1821.[1]
 At about this time Helm's father traveled to Texas to enter into business and rebuild his finances, but he died there in 1822,[7] leaving Helm responsible for his mother and siblings.[8] He was admitted to the bar in 1823, the same year Meade County, Kentucky was formed.[3][9] There were no lawyers in the county yet, so although Helm continued living in Hardin County he was made Meade's county attorney.[8] His practice grew rapidly and he was soon able to pay off his father's debts and purchase the Helm homestead.[1] Between 1832 and 1840 he built  "Helm Place" on this land and it remained his home for the rest of his life.[1][10]
 In 1823 Helm called on Representative Benjamin Hardin.[11] While Hardin and Helm discussed business, Hardin's 14-year-old daughter, Lucinda, entered the room to show her father a map she had drawn.[11] Helm later claimed it was love at first sight, and began to pursue Lucinda's affections.[11] They courted for seven years, married in 1830 and had six daughters and five sons together.[4][12] One of his sons, Benjamin Hardin Helm, was a Confederate general in the Civil War and was killed at the Battle of Chickamauga.[1]
 The major political issue in Kentucky during Helm's legal training was the Old Court-New Court controversy. Reeling from the  panic of 1819, Kentuckians had demanded debt relief. In response, the Kentucky General Assembly passed an act that granted debtors a grace period of two years in repaying their debts unless their creditors would accept payment in the devalued notes of the Bank of the Commonwealth. The Kentucky Court of Appeals struck down the law, claiming it was in violation of the Contract Clause of the U.S. Constitution. The angered legislature attempted to impeach the justices on the Court of Appeals, but lacked the necessary two-thirds majority. Instead, they abolished the Court of Appeals and replaced it with a new court, which was stocked with more sympathetic justices by pro-relief governor John Adair. Both courts claimed to be Kentucky's court of last resort.[13]
 Throughout 1825 Helm made speeches and distributed pamphlets in Hardin and surrounding counties, espousing the Old Court position.[12] In 1826 he campaigned as a Whig for a seat in the Kentucky House of Representatives.[14] Helm won the election, and at the age of twenty-four became one of the youngest members to serve in the Kentucky General Assembly.[9] An Old Court majority was elected to both houses of the General Assembly in 1826, which then passed legislation abolishing the New Court.[15]
 Helm was re-elected to the state House in 1827 and 1830, and was re-elected every year from 1833 to 1837.[3] He served as Speaker of the House in 1835 and 1836.[3] In 1837 there was a three-way race for speaker between Helm, James Turner Morehead and Robert P. Letcher.[16] After nine ballots Helm withdrew, and Letcher was elected speaker.[16]
 Helm made his only run for federal office in 1838 and was defeated by Willis Green for a seat in the United States House of Representatives.[1] He returned to the Kentucky House in 1839 and was re-elected in 1842 and 1843, serving as Speaker of the House both years.[3] In 1843, the Kentucky General Assembly proposed to create a new county from part of Hardin County and name it Helm County in honor of John L. Helm.[17] Because of the few dissenting votes on this question, Helm declined the honor and proposed instead that the county be called LaRue County after his mother's family, many of whom still lived in the proposed county. Helm's suggestion was unanimously adopted.[17]
 In 1844 Helm was elected to the Kentucky Senate, where he served until 1848.[3] That year he was the Whig candidate for lieutenant governor on a ticket with John J. Crittenden.[1] Helm defeated Democrat John Preston Martin in the general election.[18] The major political question in the state during Helm's time as lieutenant governor was whether to adopt a new state constitution.[17] As a state senator in 1848, Helm had voted to allow the state's citizens to decide the matter in a referendum, but after seeing the document produced by the constitutional convention, he opposed its ratification.[17] In an address to the state senate in 1850 he declared, "I was for reform, and not for revolution. I was for amending the Constitution, and not for obliterating every vital principle in contained."[19] He especially opposed creating an elective judiciary.[20] His antagonism to the constitution put him at odds with his father-in-law, Benjamin Hardin.[19] The two did not reconcile until 1852, as Hardin lay on his deathbed.[19] The new constitution was adopted in 1850, and in June of that year Helm encouraged the people to accept it.[21]
 Governor Crittenden resigned on July 31, 1850 to accept President Millard Fillmore's appointment as attorney general, and Helm ascended to the governorship.[1] As governor, Helm vetoed a legislative plan to cover deficits in the public school fund by drawing money from the state's sinking fund, but the General Assembly overrode the veto.[14] He urged the legislature to fund a survey of the state's mineral reserves and a census of the state's agricultural and manufacturing resources.[14] He called for spending on internal improvements and for raising judges' salaries to attract more qualified jurists to the bench.[14][18] He also sought a ban on the carrying of concealed deadly weapons.[14] The legislature did not act on any of these proposed reforms.[14] The only part of Helm's agenda that did progress through the General Assembly was election reform.[14]
 Helm was a presidential elector for Winfield Scott in the 1852 presidential election.[1] After this he took twelve years off from politics.[1] As early as 1836, Helm had advocated the construction of the Louisville and Nashville Railroad.[22] On October 2, 1854, he became the railroad's second president.[23] The previous president had been forced out of that position after a disagreement with Louisville's board of aldermen, and construction of the line had almost been abandoned.[24]
 Helm worked diligently to convince residents along the line's main route of the economic benefits it would bring.[25] He persuaded many of them to help clear and grade land for the line and accept company stock as payment, and succeeded in selling stock subscriptions to people in the same area.[14][25] Rising labor costs and troubles transporting materials raised expenses far above the projected budget, and at one point Helm personally redeemed $20,000 ($560 thousand as of 2019) of the company's bonds.[26] Meanwhile, some observers accused Helm of mismanaging the company.[26] The company's fortunes improved in 1857 when the city of Louisville provided $300,000 ($8.07 million as of 2019) in financial aid and the line was completed on October 18, 1859.[27] Due to Helm's influence, the railroad's charter required all trains traveling through Elizabethtown to stop there.[28]
 By the time the line was finished there were public calls from inside and outside the company for Helm to resign, mostly because of his support for a proposed Memphis branch of the railroad.[29] To complete the branch, the Louisville and Nashville would have to complete a line from Bowling Green to Guthrie, Kentucky.[30] There it would join a line owned by the Memphis and Ohio Railroad that began across the state line at Clarksville, Tennessee and extended to Memphis.[30] Supporters believed the branch would economically help both Louisville and Memphis and would lessen their dependence on trade along the Mississippi and Ohio Rivers.[31] Opponents argued that the project was simply a ploy to whip up new support for the struggling railroad.[31] Helm endorsed the Memphis branch in his annual report in 1857.[30]
 On February 4, 1860, two members of the company's board of directors wrote a letter requesting Helm's resignation; they claimed they had voted for his re-election as president of the company with the understanding that he would resign when the main line between Louisville and Nashville was finished.[32] Helm maintained that he felt an obligation to the citizens of Logan County – many of whom he had personally sold stock to – to remain president until the Memphis branch through their county was built.[32] The rift between Helm and the directors continued to widen, however. Helm resigned on February 21, 1860, and was replaced by James Guthrie.[32] The Memphis branch was completed on September 24, 1860.[33]
 On January 8, 1861 Helm chaired a meeting in Louisville that advocated for Kentucky's neutrality in the Civil War.[1][9] Helm was an outspoken opponent of secession, but also denounced the election of Abraham Lincoln and his use of military force to subdue the southern states.[9] Because Helm did not condemn his son, Benjamin, for joining the Confederate Army, federal authorities classified him as a southern sympathizer.[34]
 After learning of the arrest of former governor Charles S. Morehead by federal authorities, Helm fled to Bowling Green, fearing his own arrest. Through the intervention of Warner Underwood he was able to return home on the condition that he swear an oath of allegiance to the Constitution. Nevertheless, federal soldiers repeatedly entered his home, encouraging his slaves to abandon him, and consuming or destroying his crops. Because the state's courts were closed on account of the war, he was unable to earn a living by practicing law. In short order his once-substantial fortune was expended, and he resorted to borrowing money to support his family.[35]
 In September 1862 Helm and several other citizens from Hardin County were arrested by Colonel Knox. After several days of confinement in Elizabethtown the prisoners were conducted to Louisville. By chance, Kentucky governor James F. Robinson recognized Helm in the group and negotiated with General Jeremiah Boyle to get him released. Shortly after returning home Helm learned of Benjamin's death at the Battle of Chickamauga.[36]
 After the war Helm identified with the Democratic Party, and he returned to the state senate in 1865.[3] During his tenure he chaired the Committee on Federal Relations and fought against punitive and restrictive laws against ex-Confederates.[14] On January 22, 1866 he presented to the state senate a protest against the actions of the United States Congress during the Civil War.[37] It denounced the Reconstruction Amendments on the grounds that they granted powers to the federal government that were reserved for the states, and that they were passed while many southern states were not represented in Congress.[38] He also decried the creation and operation of the Freedmen's Bureau.[39] On January 29, 1867, Helm introduced legislation to organize a meeting in Louisville to rally support for President Andrew Johnson and his efforts to restore the Union.[40]
 The state Democratic Convention met on February 22, 1867 in Frankfort and chose Helm and John W. Stevenson as the party's candidates for governor and lieutenant governor, respectively.[40] Helm resigned his seat in the state senate to accept the nomination.[18] Though his health was frail, he determined to canvass the entire state.[41] He continued his call for an end to Civil War bitterness and proscriptions against those who had sided with the Confederacy.[18] He won the general election over Republican Sidney Barnes and a third party candidate, Judge William B. Kinkead.[18]
 The strenuous campaign took a decisive toll on Helm's already weakened body. He was too weak to travel to Frankfort for his inauguration, so the oath of office was administered at his home on September 3, 1867.[2] Helm's secretary of state read the governor's inaugural address at the Hardin County Courthouse.[14] In it Helm repeated his intent to remove political disabilities from ex-Confederates.[42] He also charged that Congress was meddling in the affairs of the states.[42] Though he promised protections for blacks, he opposed the idea of black suffrage.[42]
 Helm died on September 8, 1867, just five days after his inauguration.[3] He was buried in a family graveyard at Helm Place.[3] Helm Place was listed on the National Register of Historic Places on November 9, 1976.[43]
 
 1 Early life 2 Political career

2.1 Lieutenant governor and governor

 2.1 Lieutenant governor and governor 3 President of the Louisville and Nashville Railroad 4 Civil War and second term as governor 5 References

5.1 Bibliography

 5.1 Bibliography 6 Further reading 7 External links ^ a b c d e f g h i j k Powell, p. 46
 ^ a b c Allen, p. 107
 ^ a b c d e f g h i NGA Bio
 ^ a b Owen, p. 68
 ^ Biographical Sketch, pp. 16–17
 ^ a b Biographical Sketch, p. 18
 ^ Biographical Sketch, pp. 15–16
 ^ a b Allen, p. 108
 ^ a b c d Johnson, p. 929
 ^ Coleman, p. 51
 ^ a b c Alexander, p. 18
 ^ a b Biographical Sketch, p. 23
 ^ Biographical Sketch, pp. 21–23
 ^ a b c d e f g h i j Owen, p. 69
 ^ Biographical Sketch, pp. 23–24
 ^ a b Owen, p. 56
 ^ a b c d NRHP nomination form, p. 10
 ^ a b c d e Harrison, p. 422
 ^ a b c Biographical Sketch, p. 34
 ^ Green, p. 217
 ^ NRHP nomination form, p. 11
 ^ NRHP nomination form, pp. 11–12
 ^ Klein, p. 9
 ^ Klein, pp. 8–9
 ^ a b Klein, p. 11
 ^ a b Klein, p. 12
 ^ Klein, pp. 12, 14
 ^ NRHP nomination form, p. 12
 ^ Herr, pp. 23, 26
 ^ a b c Klein, p. 20
 ^ a b Klein, p. 18
 ^ a b c Herr, p. 26
 ^ Klein, p. 22
 ^ Biographical Sketch, p. 73
 ^ Biographical Sketch, pp. 73–75
 ^ Biographical Sketch, pp. 75–77
 ^ Biographical Sketch, p. 79
 ^ Biographical Sketch, pp. 79–80
 ^ Biographical Sketch, p. 82
 ^ a b Biographical Sketch, p. 84
 ^ Biographical Sketch, p. 86
 ^ a b c Owen, p. 70
 ^ NRHP nomination form, p. 1
 Alexander, Arabel Wilbur (1898). The life and work of Lucinda B. Helm, founder of the Woman's parsonage and Home mission society of the M.E. church, South (PDF). Nashville, Tennessee: Publishing House of the Methodist Episcopal church, South. Retrieved February 23, 2009..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} Allen, William B. (1872). A History of Kentucky: Embracing Gleanings, Reminiscences, Antiquities, Natural Curiosities, Statistics, and Biographical Sketches of Pioneers, Soldiers, Jurists, Lawyers, Statesmen, Divines, Mechanics, Farmers, Merchants, and Other Leading Men, of All Occupations and Pursuits. Bradley & Gilbert. Retrieved November 10, 2008. Biographical sketch of the Hon. John L. Helm, late governor of Kentucky. published by direction of the General Assembly of Kentucky. Frankfort, Kentucky: Kentucky Yeoman Office. 1868. Archived from the original on March 8, 2005. Retrieved February 11, 2009.CS1 maint: others (link) Coleman, John Winston (1968). Historic Kentucky. Henry Clay Press. ISBN 0-87642-000-5. Green, Thomas Marshall (1889). Historic Families of Kentucky. R. Clark and Company. Retrieved February 16, 2009. Harrison, Lowell H. (1992).  Kleber, John E. (ed.). The Kentucky Encyclopedia. Associate editors: Thomas D. Clark, Lowell H. Harrison, and James C. Klotter. Lexington, Kentucky: The University Press of Kentucky. ISBN 0-8131-1772-0. Herr, Kincaid A. (2000). The Louisville and Nashville Railroad 1850–1963. The University Press of Kentucky. ISBN 0-8131-2184-1. Johnson, E. Polk (1912). A History of Kentucky and Kentuckians: The Leaders and Representative Men in Commerce, Industry and Modern Activities. Lewis Publishing Company. Retrieved November 10, 2008. "Kentucky Governor John Larue Helm". National Governors Association. Archived from the original on January 17, 2012. Retrieved April 2, 2012. Klein, Maury (2002). History of the Louisville & Nashville Railroad. The University Press of Kentucky. ISBN 0-8131-2263-5. Owen, Tom (2004).  Lowell H. Harrison (ed.). Kentucky's Governors. Lexington, Kentucky: The University Press of Kentucky. ISBN 0-8131-2326-7. Powell, Robert A. (1976). Kentucky Governors. Frankfort, Kentucky: Kentucky Images. OCLC 2690774. Terry, Mrs. Wilbur; Mrs. Edmund S. Richerson (1976). "NRHP Nomination Form" (PDF). Retrieved February 12, 2009. Morton, Jennie Chinn (September 1905). "Sketch of Governor John L. Helm". The Register of the Kentucky Historical Society. 3: 11–14.  "Helm, John Larue" . Appletons' Cyclopædia of American Biography. 1892. v t e Shelby Garrard Greenup Scott Shelby Madison Slaughter Adair Desha Metcalfe J. Breathitt J. Morehead Clark Wickliffe Letcher Owsley Crittenden Helm Powell C. Morehead Magoffin G.W. Johnson Robinson Hawes Bramlette Helm Stevenson Leslie McCreary Blackburn Knott Buckner Brown Bradley Taylor Goebel Beckham Willson McCreary Stanley Black Morrow Fields Sampson Laffoon Chandler K. Johnson Willis Clements Wetherby Chandler Combs N. Breathitt Nunn Ford Carroll Brown Jr. Collins Wilkinson Jones Patton Fletcher S. Beshear Bevin A. Beshear (elect)  Book:Governors of Kentucky v t e Bullitt Caldwell Posey Slaughter (1st time) Hickman Slaughter (2nd time) Barry McAfee Breathitt J. Morehead Wickliffe M. Thomson Dixon Helm J. Thompson Hardy Boyd Jacob Stevenson Carlisle Underwood Cantrill Hindman Bryan Alford Worthington Marshall Beckham Thorne Cox McDermott Black Ballard Denhardt Breathitt Jr. Chandler Johnson Myers Tuggle Wetherby Beauchamp Waterfield Wyatt Waterfield Ford Carroll Stovall Collins Beshear Jones Patton Henry Pence Mongiardo Abramson Luallen Hampton Coleman (elect) v t e Frankfort and Cincinnati Railroad Louisville and Nashville Railroad Basil W. Duke Big South Fork Scenic Railway Bluegrass Railroad and Museum Elkhorn City Railroad Museum Historic Railpark and Train Museum Kentucky Railway Museum Railway Museum of Greater Cincinnati Ashland Coal and Iron Railroad Office Ashland Coal and Iron Railroad Store Chesapeake and Ohio Railroad Passenger and Baggage Depots Henderson, Louisville and Nashville Railroad Depot Hopkinsville L & N Railroad Depot Illinois Central Railroad Station and Freight Depot Louisville and Nashville Railroad Depot Louisville and Nashville Railroad Passenger Depot Louisville, Henderson, and St. Louis Railroad Depot Old L & N Station Paris Railroad Depot Shelbyville L & N Railroad Depot Stanford L&N Railroad Depot Union Station (Louisville) Union Station (Owensboro) Frankfort and Cincinnati Model 55 Rail Car L & N Steam Locomotive No. 152 Louisville and Nashville Combine Car Number 665 Mt. Broderick Pullman Car Barren River L & N Railroad Bridge Big Four Bridge Cairo Rail Bridge C&O Railroad Bridge Cincinnati Southern Bridge Fourteenth Street Bridge (Ohio River) Henderson Bridge (Ohio River) High Bridge of Kentucky Kentucky & Indiana Terminal Bridge Metropolis Bridge Newport Southbank Bridge Sciotoville Bridge Young's High Bridge Cincinnati Southern Railroad Culvert--CSRR Lexington Extension of the Louisville Southern Railroad Louisville and Nashville Railroad Office Building Mitchellsburg Louisville and Nashville Railroad Culvert Russell Railroad YMCA Tank Pond Railroad Underpass LCCN: no2014085663 VIAF: 310520952  WorldCat Identities (via VIAF): 310520952 1802 births 1867 deaths Governors of Kentucky Kentucky Democrats Kentucky lawyers Kentucky state senators Kentucky Whigs Lieutenant Governors of Kentucky Louisville and Nashville Railroad people 19th-century American railroad executives People of Kentucky in the American Civil War People from Hardin County, Kentucky People who died in office Politicians from Louisville, Kentucky Speakers of the Kentucky House of Representatives Whig Party state governors of the United States 19th-century American politicians Democratic Party state governors of the United States LaRue family American lawyers admitted to the practice of law by reading law Use mdy dates from October 2011 CS1 maint: others Commons category link from Wikidata Featured articles Wikipedia articles with LCCN identifiers Wikipedia articles with VIAF identifiers Wikipedia articles with WorldCat-VIAF identifiers Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version العربية Deutsch Français Latina 日本語 Português Русский Svenska 中文  This page was last edited on 30 October 2019, at 19:48 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  In office In office In office In office In office John LaRue Helm a b c d e f g h i j k a b c a b c d e f g h i a b ^ a b ^ a b a b c d ^ a b c a b ^ a b c d e f g h i j ^ a b a b c d a b c d e a b c ^ ^ ^ ^ ^ a b a b ^ ^ ^ a b c a b a b c ^ ^ ^ ^ ^ ^ ^ a b ^ a b c ^ 3 John L. Helm  Lieutenant Governor of Kentucky  Governor of Kentucky  Governor of Kentucky  Book:Governors of Kentucky John L. Helm photographed by Klauber, Louisville, Kentucky, undated.  In officeSeptember 3, 1867 – September 8, 1867 John W. Stevenson Thomas E. Bramlette John W. Stevenson In officeJuly 31, 1850 – September 2, 1851 John J. Crittenden Lazarus W. Powell In officeSeptember 6, 1848 – July 31, 1850 John J. Crittenden Archibald Dixon John Burton Thompson In office1844 In office1826–1843 
 (1802-07-04)July 4, 1802Hardin County, Kentucky September 8, 1867(1867-09-08) (aged 65)Elizabethtown, Kentucky DemocratWhig Lucinda Barbour (Hardin) Helm Son-in-law of Benjamin HardinFather of Benjamin Hardin Helm and Lucinda Barbour Helm Helm Place Lawyer   Wikimedia Commons has media related to John L. Helm. Preceded byArchibald Dixon
  Lieutenant Governor of Kentucky1848–1850
 Succeeded byJohn Burton Thompson
 Preceded byJohn J. Crittenden
  Governor of Kentucky1850–1851
 Succeeded byLazarus W. Powell
 Preceded byThomas E. Bramlette
  Governor of Kentucky1867
 Succeeded byJohn W. Stevenson
 Italics indicate Confederate governors 
Shelby
Garrard
Greenup
Scott
Shelby
Madison
Slaughter
Adair
Desha
Metcalfe
J. Breathitt
J. Morehead
Clark
Wickliffe
Letcher
Owsley
Crittenden
Helm
Powell
C. Morehead
Magoffin
G.W. Johnson
Robinson
Hawes
Bramlette
Helm
Stevenson
Leslie
McCreary
Blackburn
Knott
Buckner
Brown
Bradley
Taylor
Goebel
Beckham
Willson
McCreary
Stanley
Black
Morrow
Fields
Sampson
Laffoon
Chandler
K. Johnson
Willis
Clements
Wetherby
Chandler
Combs
N. Breathitt
Nunn
Ford
Carroll
Brown Jr.
Collins
Wilkinson
Jones
Patton
Fletcher
S. Beshear
Bevin
A. Beshear (elect)
  
 Book:Governors of Kentucky
 
Bullitt
Caldwell
Posey
Slaughter (1st time)
Hickman
Slaughter (2nd time)
Barry
McAfee
Breathitt
J. Morehead
Wickliffe
M. Thomson
Dixon
Helm
J. Thompson
Hardy
Boyd
Jacob
Stevenson
Carlisle
Underwood
Cantrill
Hindman
Bryan
Alford
Worthington
Marshall
Beckham
Thorne
Cox
McDermott
Black
Ballard
Denhardt
Breathitt Jr.
Chandler
Johnson
Myers
Tuggle
Wetherby
Beauchamp
Waterfield
Wyatt
Waterfield
Ford
Carroll
Stovall
Collins
Beshear
Jones
Patton
Henry
Pence
Mongiardo
Abramson
Luallen
Hampton
Coleman (elect)
  
Frankfort and Cincinnati Railroad
Louisville and Nashville Railroad
 
Basil W. Duke
 
Big South Fork Scenic Railway
Bluegrass Railroad and Museum
Elkhorn City Railroad Museum
Historic Railpark and Train Museum
Kentucky Railway Museum
Railway Museum of Greater Cincinnati
 
Ashland Coal and Iron Railroad Office
Ashland Coal and Iron Railroad Store
Chesapeake and Ohio Railroad Passenger and Baggage Depots
Henderson, Louisville and Nashville Railroad Depot
Hopkinsville L & N Railroad Depot
Illinois Central Railroad Station and Freight Depot
Louisville and Nashville Railroad Depot
Louisville and Nashville Railroad Passenger Depot
Louisville, Henderson, and St. Louis Railroad Depot
Old L & N Station
Paris Railroad Depot
Shelbyville L & N Railroad Depot
Stanford L&N Railroad Depot
Union Station (Louisville)
Union Station (Owensboro)
 
Frankfort and Cincinnati Model 55 Rail Car
L & N Steam Locomotive No. 152
Louisville and Nashville Combine Car Number 665
Mt. Broderick Pullman Car
 
Barren River L & N Railroad Bridge
Big Four Bridge
Cairo Rail Bridge
C&O Railroad Bridge
Cincinnati Southern Bridge
Fourteenth Street Bridge (Ohio River)
Henderson Bridge (Ohio River)
High Bridge of Kentucky
Kentucky & Indiana Terminal Bridge
Metropolis Bridge
Newport Southbank Bridge
Sciotoville Bridge
Young's High Bridge
 
Cincinnati Southern Railroad Culvert--CSRR
Lexington Extension of the Louisville Southern Railroad
Louisville and Nashville Railroad Office Building
Mitchellsburg Louisville and Nashville Railroad Culvert
Russell Railroad YMCA
Tank Pond Railroad Underpass
 
LCCN: no2014085663
VIAF: 310520952
 WorldCat Identities (via VIAF): 310520952
 