"Speechless" is a song by the American recording artist Michael Jackson, included on his tenth studio album, Invincible (2001). It was only released as a promotional single in South Korea. The singer was inspired to write the ballad after a water balloon fight with children in Germany. Jackson collaborated on the production with musicians such as Jeremy Lubbock, Brad Buxer, Novi Novoq, Stuart Bradley and Bruce Swedien. Andraé Crouch and his gospel choir provided backing vocals.
 Executives at Jackson's record label, Epic Records, responded positively to the track when given a preview several months before Invincible's release. "Speechless" was issued as a promotional single. Music critics focused on the track's a cappellas, lyrics and music. A clip of Jackson singing "Speechless" was included in the 2009 documentary-concert film Michael Jackson's This Is It.
 Michael Jackson wrote "Speechless" after a water balloon fight with children in Germany. It took him 45 minutes.[1] In an interview with  Vibe magazine, the musician commented, "I was so happy after the fight that I ran upstairs in their house and wrote 'Speechless'. Fun inspires me. I hate to say that, because it's such a romantic song." He added, "But it was the fight that did it. I was happy, and I wrote it in its entirety right there. I felt it would be good enough for the album. Out of the bliss comes magic, wonderment, and creativity."[2][3] Jackson would consider "Speechless" to be one of his favorite songs on Invincible.[4]
 "Speechless" was one of only two songs from Invincible to be written solely by Jackson (the second song being "The Lost Children").[5] Jeremy Lubbock worked with the musician in arranging and conducting an orchestra. Instrumentalists on the track included Brad Buxer on keyboards, and Novi Novog and Thomas Tally on violas. The violinists consisted of Peter Kent, Gina Kronstadt, Robin Lorentz, Kirstin Fife and John Wittenberg. The track featured backing vocals from Andraé Crouch and his gospel choir, The Andraé Crouch Singers. "Speechless" was digitally edited by Buxer and Stuart Brawley, and was mixed by Bruce Swedien,[6] who later said, "Everything with Michael is a stand-out moment but an absolutely gorgeous piece of music called 'Speechless' was really an event. Michael sings the first eight bars a cappella. At the end, he closes it off a cappella – it was Michael's idea to add the a cappella parts."[7]
 The lyrics to "Speechless" deal with being lost for words because of love.[8] The song opens with Jackson's singing a cappella: "Your love is magical, that's how I feel, but I have not the words here to explain", which Rick de Yampert of The Daytona Beach News-Journal felt the singer "[crooned] sweetly".[6][9] The chorus includes the lines, "Speechless, speechless, that's how you make me feel. Though I'm with you, I am far away and nothing is for real."[6] A second a cappella verse bookends the track.[10]
 "Speechless" is a ballad,[11][12] and labeled it as "neo-gospel".[10] According to Musicnotes.com by Alfred Music Publishing. It adds that the track was performed in common time, with a tempo of 80 beats per minute. The song starts in the key of B♭ major and transitions to C major. After the bridge the song transitions to D Major as a choir starts singing, the last two choruses in E Major, ending with a solo a cappella ending by Jackson.  The song's vocal range is from F4 to B5.[13]
 In June 2001, several months before the release of Invincible, "Speechless" was among several songs showcased from the album exclusively to executives of Jackson's music label, Epic Records (a subsidiary of Sony Music Entertainment). Other songs previewed included "Unbreakable", "The Lost Children", "Whatever Happens", "Break of Dawn", "Heaven Can Wait" and "Privacy", all of which featured on Invincible's track listing.[14][15] Roger Friedman of Fox News reported that the executives who listened to the previews liked what they heard. Epic Records' president, Dave Glew, said of the tracks, "It's wonderful and amazing. Michael is singing better than ever." He added, "The ballads! The ballads are beautiful, and they're all there."[16] "Speechless" was later released as a promotional single in 2001. A remixed version of "You Rock My World", featuring rapper Jay-Z, served as the single's B-side.[17] After Jackson's death, a clip of the entertainer singing "Speechless" was included in Michael Jackson's This Is It, a commercially successful documentary-concert film of the singer's rehearsals for his London concert series.[18]
 "Speechless" was considered to be performed on cancelled This Is It concerts that planned to run from July 2009 to March 2010. However, this song was ultimately off the final set list. Later members of band shared set lists for the shows. "Speechless" was marked as an additional material. That set list was dated May 22, 2009.
 Craig Seymour of The Buffalo News felt that the song was the only one from the album in which Jackson successfully revisited his past. The journalist said the song was reminiscent of the 1995 chart-topper "You Are Not Alone", as it sounded to him like a track that could have been written by R. Kelly, who penned the number one hit.[19] Writing for the Chicago Sun-Times, Jim DeRogatis described "Speechless" as a "beautifully minimal, heartfelt romantic ballad".[20] Music journalist Roger Catlin stated that the song leaned toward "neo-gospel".[10] The New York Post said that "Speechless" was "lullaby-like" and the best song on Invincible, and Jon Pareles of The New York Times praised Jackson's "long lines and creamy overdubbed choruses [sailing] weightlessly" in the ballad, that the journalist felt it was a love song to God.[21][22]
 Pop music critic Robert Hilburn described "Speechless", and another song from Invincible ("Butterflies"), as being "as woefully generic as their titles".[8] Ben Rayner of the Toronto Star contested that the a cappellas in "Speechless" were enough to make a person wish that Jackson actually was unable to make a sound.[23] Michigan Daily writer Dustin J. Seibert wrote that the song was a "shining [example] of what happens when The Gloved One gets beside himself and writes smarmy crap that should be reserved for a CD changer somewhere in a preschool".[24] The Fort Worth Star-Telegram said that "Speechless" was one of the weaker tracks from Invincible.[25] Elliot Sylvester of The Independent felt that the song was "pure Jackson – almost to a formulaic fault".[26] The Dallas Morning News' Thor Christensen said that "Speechless" was "produced by Mr. Jackson in bombastic style à la Celine Dion". He added that as the track ended with an emotional Jackson, it drew a parallel with the singer's 1972 ode to a rat, "Ben".[27]
 Vaughn Watson of The Providence Journal hailed "Speechless" as Invincible's "best song, and one of Jackson's finest of any album". He added that with the song, the musician acknowledged the pain that accompanies isolation.[28] In a review of Invincible, The Wichita Eagle stated that "Speechless", "Don't Walk Away" and "Cry" were among the "sincere ballads" in which Jackson was exemplary.[29] Ada Anderson of The Ball State Daily News expressed the view that "Speechless" would become a popular song, and writers for the South Florida Sun-Sentinel stated that the ballad would take time to get used to.[30][31] The Dayton Daily News' Ron Rollins described the track as a "pretty love song".[32] Music critic Kevin C. Johnson thought that "Speechless" was "one of [Jackson's] typical, whispery ballads that swells as it moves along".[33] A journalist for The Olympian stated that the song was "gorgeous".[34]
 On June 21, 2010, six performers in the West End of London show Thriller – Live released "Speechless" as a single with the official name "Speechless – The Tribute to Michael Jackson" to commemorate the one-year anniversary of Jackson's death. All proceeds from the recording were donated to the charity War Child.[35]
 
 Neo-soul gospel 1 Writing and recording 2 Composition 3 Post-production and release 4 Plans to be performed on This Is It 5 Critical reception 6 Track listing 7 Personnel 8 Thriller – Live cast version

8.1 Personnel
8.2 Track listing

 8.1 Personnel 8.2 Track listing 9 Notes 10 References 11 External links Promotional CD single: "Speechless" – 3:18 "You Rock My World" (Track Masters Mix) (featuring Jay-Z) – 3:28 Written, composed, produced and lead vocal by Michael Jackson Orchestra arranged and conducted by Michael Jackson and Jeremy Lubbock Keyboards performed by Brad Buxer Viola performed by Novi Novog and Thomas Tally Violin performed by Peter Kent, Gina Kronstadt, Robin Lorentz, Kirstin Fife and John Wittenberg Written and originally produced by Michael Jackson Produced, edited and mixed by Dave Loughran Executive producer: Adrian Grant for Key Concerts & Entertainment Vocal arrangement by John Maher Music arranged and performed by Dave Loughran Hammond organ performed by John Maher Lead vocals: James Anderson, Jean-Mikhael Baque, Kieran Alleyne, Kuan Frye, Mitchell Zhangazha, MJ Mytton-Sanneh Choir: Britt Quentin, Hope Lyndsey Plumb, J Rome, Jenessa Qua, Linda John-Pierre, Olamide Oshinowo, Paul Clancy, Terrence Ryan, Wayne Anthony-Cole Mastered by Steve Kitch[36] Digital download: "Speechless (A Tribute to Michael Jackson)" – single version – 4:25[37] ^ Reiter, Amy (January 29, 2002). "Jacko inspired by balloon battle". Salon.com. Archived from the original on October 2, 2002. Retrieved April 17, 2010..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ Good, Karen R. (March 2002). Black skin, white mask. Vibe. Retrieved March 14, 2010.
 ^ Jones, p. 283
 ^ Michael Jackson's interview with Getmusic.com. Youtube. October 26, 2001
 ^ Taraborrelli, p. 612
 ^ a b c Liner notes of Invincible by Michael Jackson
 ^ Grant, p. 296
 ^ a b Hilburn, Robert (October 28, 2001). "Michael Jackson's 'Invincible'". Los Angeles Times. Retrieved April 22, 2010.
 ^ de Yampert, Rick (November 13, 2001). "Jackson trying to be new kid on the block". The Daytona Beach News-Journal. Retrieved April 19, 2010.
 ^ a b c Catlin, Roger (October 31, 2001). "'Invincible': new material, echoes of past". The Hartford Courant. Retrieved April 22, 2010.
 ^ Beaumont, Mark (October 30, 2001). "Michael Jackson : Invincible". NME. Retrieved April 16, 2010.
 ^ Reid, Shaheem (October 26, 2001). "Michael Jackson Revisits Old Self, Rails At Press, Tries Bounce On Invincible". MTV. Retrieved April 22, 2010.
 ^ "Speechless – Michael Jackson Digital Sheet Music (Digital Download)". MusicNotes.com. Alfred Publishing Co. Inc.
 ^ Vineyard, Jennifer (June 14, 2001). "Michael Jackson Previews 15 Invincible Tracks". MTV. Retrieved April 17, 2010.
 ^ Turner, Megan (June 19, 2001). "Action Jackson – will the King of Pop reign again?". New York Post. Archived from the original (Payment required to access full article.) on March 7, 2016. Retrieved April 17, 2010.
 ^ Friedman, Roger (June 15, 2001). "Michael Jackson Presents the 'Invincible' Album, Record Execs Go Crazy". Fox News. Archived from the original on 2014-02-03. Retrieved April 17, 2010.
 ^ Packaging of the "Speechless" promotional single by Michael Jackson
 ^ Ditzian, Eric (October 28, 2009). "Inside 'Michael Jackson's This Is It' From The New York Premiere". MTV. Retrieved April 24, 2010.
 ^ Seymour, Craig (October 26, 2001). "Same old Jacko". The Buffalo News. Retrieved April 22, 2010.
 ^ DeRogatis, Jim (October 29, 2001). "Whiny dancer". Chicago Sun-Times. Retrieved April 22, 2010.
 ^ "Hands down, not out". New York Post. October 30, 2001. Archived from the original on March 7, 2016. Retrieved April 22, 2010.
 ^ Pareles, Jon (October 28, 2001). "Music; To Regain Glory, The New Michael Imitates the Old". The New York Times. Retrieved April 22, 2010.
 ^ Rayner, Ben (October 30, 2001). "Invincible incredibly insipid ; New album is not the come-back Jackson has been hoping for". Toronto Star. Retrieved April 22, 2010.
 ^ Seibert, Dustin J. (October 30, 2001). "CD review: Jacko moonwalks toward old form". Michigan Daily. Retrieved April 22, 2010.[dead link]
 ^ "The thriller is gone Jackson's latest shows how far he's fallen". Fort Worth Star-Telegram. November 2, 2001. Retrieved April 24, 2010.
 ^ Sylvester, Elliot (January 14, 2002). "Invincible shows that Jacko is not". The Independent. Archived from the original on January 15, 2005. Retrieved April 24, 2010.
 ^ Christensen, Thor (November 4, 2001). "Off the wall, out of touch Michael Jackson leans too much on the past for his comeback album". The Dallas Morning News. Retrieved April 22, 2010.
 ^ Watson, Vaughn (November 11, 2001). "Spears and Jackson: Mostly stuck in old grooves". The Providence Journal. Retrieved April 22, 2010.
 ^ "Still the king". The Wichita Eagle. November 11, 2001. Retrieved April 22, 2010.
 ^ Anderson, Ada (November 1, 2001). "CD Review: Pop king's new album average". The Ball State Daily News. Retrieved April 22, 2010.[dead link]
 ^ Roache, Dwight (November 30, 2001). "The Jackson legend". South Florida Sun-Sentinel. Retrieved April 22, 2010.
 ^ Rollins, Ron (November 2, 2001). "Recordings on review". Dayton Daily News. Retrieved April 24, 2010.
 ^ Johnson, Kevin C. (November 2, 2001). "Jackson's 'Invincible' isn't 'bad', just 'old'". St. Louis Post-Dispatch. Retrieved April 24, 2010.
 ^ "Ross Raihala sound affects". The Olympian. November 9, 2001. Retrieved April 24, 2010.
 ^ "Thriller Live marks Jackson death with memorial and single | The Official London Theatre Guide". Officiallondontheatre.co.uk. Retrieved 2011-07-24.
 ^ "Speechless – A tribute to Michael Jackson". Speechlessmjtribute.com. Archived from the original on 2011-07-16. Retrieved 2011-07-24.
 ^ "Speechless (A Tribute to Michael Jackson) – Single by Thriller Live Cast – Download Speechless (A Tribute to Michael Jackson) – Single on iTunes". Itunes.apple.com. 2010-06-21. Archived from the original on 2012-11-09. Retrieved 2011-07-24.
 Grant, Adrian (2009). Michael Jackson: The Visual Documentary. Omnibus Press. ISBN 978-1-84938-261-8. Jones, Jel (2005). Michael Jackson, the King of Pop: The Big Picture: the Music! the Man! the Legend! the Interviews!. Amber Books Publishing. ISBN 0-9749779-0-X. Taraborrelli, J. Randy (2004). The Magic and the Madness. Headline. ISBN 0-330-42005-4. "Speechless" at Discogs v t e Discography "Got to Be There" "Ain't No Sunshine" "I Wanna Be Where You Are" "Rockin' Robin" "Love Is Here and Now You're Gone" "You've Got a Friend" "Ben" "Everybody's Somebody's Fool" "My Girl" "Shoo-Be-Doo-Be-Doo-Da-Day" "We've Got a Good Thing Going" "With a Child's Heart" "Morning Glow" "All the Things You Are" "Happy" "Too Young" "Doggin' Around" "Music and Me" "We're Almost There" "Just a Little Bit of You" "You Can't Win" "Ease on Down the Road" "A Brand New Day" "Don't Stop 'Til You Get Enough" "Rock with You" "Working Day and Night" "It's the Falling in Love" "Off the Wall" "Girlfriend" "She's Out of My Life" "One Day in Your Life" "The Girl Is Mine" "Billie Jean" "Beat It" "Wanna Be Startin' Somethin'" "Human Nature" "P.Y.T. (Pretty Young Thing)" "Say Say Say" "Thriller" "You've Really Got a Hold on Me" "Here I Am (Come and Take Me)" "I Hear a Symphony" "Lonely Teardrops" "That's What Love Is Made Of" "I Was Made to Love Her" "Farewell My Summer Love" "Girl You're So Together" "I Just Can't Stop Loving You" "Bad" "The Way You Make Me Feel" "Speed Demon" "Liberian Girl" "Just Good Friends" "Another Part of Me" "Man in the Mirror" "Dirty Diana" "Smooth Criminal" "Leave Me Alone" "Twenty-Five Miles" "Black or White" "Jam" "In the Closet" "Remember the Time" "Can't Let Her Get Away" "Heal the World" "Who Is It" "Give In to Me" "Will You Be There" "Gone Too Soon" "Dangerous" "Come Together" "Someone Put Your Hand Out" "Scream" "Childhood" "They Don't Care About Us" "Stranger in Moscow" "This Time Around" "Earth Song" "D.S." "You Are Not Alone" "Tabloid Junkie" "HIStory" "Smile" "Mind Is the Magic" "Blood on the Dance Floor" "Ghosts" "Is It Scary" "On the Line" "Speechless" "You Rock My World" "Heaven Can Wait" "Butterflies" "Cry" "One More Chance" "Cheater" "The Way You Love Me" "Fall Again" "This Is It" "Hold My Hand" "Hollywood Tonight" "(I Can't Make It) Another Day" "Behind the Mask" "Don't Be Messin' 'Round" "Price of Fame" "Love Never Felt So Good" "Chicago" "Loving You" "A Place with No Name" "Slave to the Rhythm" "Blue Gangsta" "Diamonds Are Invincible" "We Are the World" "What More Can I Give/Todo Para Ti" "We Are the World 25 for Haiti" "She's Trouble" "Night Time Lover" "Papa Was a Rollin' Stone" "State of Independence" "Muscles" "Somebody's Watching Me" "Don't Stand Another Chance" "Centipede" "Tell Me I'm Not Dreamin' (Too Good to Be True)" "Eaten Alive" "Get It" "2300 Jackson Street" "Do the Bartman" "Whatzupwitu" "Why" "I Need You" "Girls, Girls, Girls (Part 2)" "All Eyez on Me" "All in Your Name" "There Must Be More to Life Than This" "Low" "Don't Matter to Me"  Book  Category 2000s ballads 2001 singles Michael Jackson songs Gospel songs Song recordings produced by Michael Jackson Songs written by Michael Jackson Soul ballads 2001 songs All articles with dead external links Articles with dead external links from February 2019 Articles with hAudio microformats Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Español فارسی Français Galego Magyar Nederlands Polski Português Српски / srpski Suomi ไทย Türkçe Tiếng Việt 中文  This page was last edited on 18 October 2019, at 19:11 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Speechless Speechless Promotional CD single Digital download ^ ^ ^ ^ ^ a b c ^ a b ^ a b c ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ Promotional single cover June 21, 2001 (2001-06-21) CD single 2000 Neo-soulgospel 3:18 Epic Michael Jackson Michael Jackson 


"Stranger in Moscow" (1998)

"Speechless" (2001)

"You Rock My World" (2001)
 "Stranger in Moscow" (1998)
 "Speechless" (2001)
 "You Rock My World" (2001)
  "Speechless" on YouTube 
  June 21, 2010 (2010-06-21) Digital Download 2010 Pop 4:25 Motivation Records Michael Jackson Dave Loughran 
Discography
 
"Got to Be There"
"Ain't No Sunshine"
"I Wanna Be Where You Are"
"Rockin' Robin"
"Love Is Here and Now You're Gone"
"You've Got a Friend"
"Ben"
"Everybody's Somebody's Fool"
"My Girl"
"Shoo-Be-Doo-Be-Doo-Da-Day"
"We've Got a Good Thing Going"
"With a Child's Heart"
"Morning Glow"
"All the Things You Are"
"Happy"
"Too Young"
"Doggin' Around"
"Music and Me"
"We're Almost There"
"Just a Little Bit of You"
"You Can't Win"
"Ease on Down the Road"
"A Brand New Day"
"Don't Stop 'Til You Get Enough"
"Rock with You"
"Working Day and Night"
"It's the Falling in Love"
 
"Off the Wall"
"Girlfriend"
"She's Out of My Life"
"One Day in Your Life"
"The Girl Is Mine"
"Billie Jean"
"Beat It"
"Wanna Be Startin' Somethin'"
"Human Nature"
"P.Y.T. (Pretty Young Thing)"
"Say Say Say"
"Thriller"
"You've Really Got a Hold on Me"
"Here I Am (Come and Take Me)"
"I Hear a Symphony"
"Lonely Teardrops"
"That's What Love Is Made Of"
"I Was Made to Love Her"
"Farewell My Summer Love"
"Girl You're So Together"
"I Just Can't Stop Loving You"
"Bad"
"The Way You Make Me Feel"
"Speed Demon"
"Liberian Girl"
"Just Good Friends"
"Another Part of Me"
"Man in the Mirror"
"Dirty Diana"
"Smooth Criminal"
"Leave Me Alone"
"Twenty-Five Miles"
 
"Black or White"
"Jam"
"In the Closet"
"Remember the Time"
"Can't Let Her Get Away"
"Heal the World"
"Who Is It"
"Give In to Me"
"Will You Be There"
"Gone Too Soon"
"Dangerous"
"Come Together"
"Someone Put Your Hand Out"
"Scream"
"Childhood"
"They Don't Care About Us"
"Stranger in Moscow"
"This Time Around"
"Earth Song"
"D.S."
"You Are Not Alone"
"Tabloid Junkie"
"HIStory"
"Smile"
"Mind Is the Magic"
"Blood on the Dance Floor"
"Ghosts"
"Is It Scary"
"On the Line"
 
"Speechless"
"You Rock My World"
"Heaven Can Wait"
"Butterflies"
"Cry"
"One More Chance"
"Cheater"
"The Way You Love Me"
"Fall Again"
"This Is It"
 
"Hold My Hand"
"Hollywood Tonight"
"(I Can't Make It) Another Day"
"Behind the Mask"
"Don't Be Messin' 'Round"
"Price of Fame"
"Love Never Felt So Good"
"Chicago"
"Loving You"
"A Place with No Name"
"Slave to the Rhythm"
"Blue Gangsta"
"Diamonds Are Invincible"
 
"We Are the World"
"What More Can I Give/Todo Para Ti"
"We Are the World 25 for Haiti"
"She's Trouble"
 
"Night Time Lover"
"Papa Was a Rollin' Stone"
"State of Independence"
"Muscles"
"Somebody's Watching Me"
"Don't Stand Another Chance"
"Centipede"
"Tell Me I'm Not Dreamin' (Too Good to Be True)"
"Eaten Alive"
"Get It"
"2300 Jackson Street"
"Do the Bartman"
"Whatzupwitu"
"Why"
"I Need You"
"Girls, Girls, Girls (Part 2)"
"All Eyez on Me"
"All in Your Name"
"There Must Be More to Life Than This"
"Low"
"Don't Matter to Me"
 
 Book
 Category
 