
 
 Abu Muhammad Hasan al-Kharrat (Arabic: حسن الخراط‎ 
Ḥassan al-Kharrāṭ; 1861[note 1]–25 December 1925) was one of the principal Syrian rebel commanders of the Great Syrian Revolt against the French Mandate. His main area of operations was in Damascus and its Ghouta countryside. He was killed in the struggle and is considered a hero by Syrians.[4]
 As the qabaday (local youths boss) of the al-Shaghour quarter of Damascus, al-Kharrat was connected with Nasib al-Bakri, a nationalist from the quarter's most influential family. At al-Bakri's invitation, al-Kharrat joined the revolt in August 1925 and formed a group of fighters from al-Shaghour and other neighborhoods in the vicinity. He led the rebel assault against Damascus, briefly capturing the residence of French High-Commissioner Maurice Sarrail before withdrawing amid heavy French bombardment.
 Towards the end of 1925, relations grew tense between al-Kharrat and other rebel leaders, particularly Sa'id al-'As and Ramadan al-Shallash, as they traded accusations of plundering villages or extorting local inhabitants. Al-Kharrat continued to lead operations in the Ghouta, where he was ultimately killed in a French ambush. The revolt dissipated by 1927, but he gained a lasting reputation as a martyr of the Syrian resistance to French rule.
 Al-Kharrat was born to a Sunni Muslim family in Damascus in 1861, during Ottoman rule in Syria.[5][6] He served as the night watchman of the city's al-Shaghour quarter and as a guard for the neighborhood's orchards.[6][7][8] Damascus was captured by Arab rebels during World War I in October 1918. Afterward, the Arab Club, an Arab nationalist organization, emerged in the city to raise support for the rebels.[9] The club assisted the rebels' leader, Emir Faisal, who formed a rudimentary government.[9] Al-Kharrat became an affiliate of the Arab Club and raised support for Faisal in al-Shaghour.[10] In July 1920, Faisal's government collapsed after its motley forces were defeated by the French at the Battle of Maysalun.[11] Afterward, the French ruled Syria under the aegis of their League of Nations mandate.
 In the early years of French rule, al-Kharrat was al-Shaghour's qabaday (pl. qabadayat),[7][12] the traditional leader of a neighborhood's local toughs.[13] The qabaday was informally charged with redressing grievances and defending a neighborhood's honor against local criminals or the encroachments of qabadayat from other neighborhoods.[13] He was popularly characterized as an honorable man, noted for his personal strength,[13] and protection of minorities and the poor.[14] The qabaday was considered an "upholder of Arab traditions and customs, the guardian of popular culture", according to historian Philip S. Khoury.[13][14] Khoury asserts that al-Kharrat was "probably the most respected and esteemed qabaday of his day".[12] Qabadayat normally shunned formal education,[13] and historian Michael Provence maintains that al-Kharrat was likely illiterate.[15] Qabadayat were normally linked with particular city notables and could secure them political support in their neighborhoods.[13] Al-Kharrat was allied with Nasib al-Bakri, a Damascene politician and landowner.[8] The al-Bakri family was the most influential in al-Shaghour, and al-Kharrat served as the family's principal connection and enforcer in the quarter.[16]
 A revolt against French rule was launched in mid-1925 by the Druze sheikh (chieftain), Sultan Pasha al-Atrash, in the southern mountains of Jabal al-Druze.[8] As al-Atrash's men scored decisive victories against the French Army of the Levant, Syrian nationalists were inspired and the revolt spread northward to the countryside of Damascus and beyond.[8] Al-Bakri was the chief liaison between al-Atrash and the emerging rebel movement in Damascus and the Ghouta.[8] The Ghouta is the fertile plain surrounding Damascus,[17][18] and its orchard groves and extensive waterways provided cover for the rebels and a base from which they could raid Damascus.[19] In August, al-Bakri convinced al-Kharrat to join the uprising.[6][16] According to Provence, al-Kharrat was "ideal" for the job, possessing "a local following of young men, notoriety outside the quarter, good connections and a reputation for toughness".[16] The group of fighters he commanded was known as ′isabat al-Shawaghirah (the band of al-Shaghour).[8] Though named after al-Kharrat's quarter, the band included twenty qabadayat and their armed retinues from other Damascus neighborhoods and nearby villages.[20][note 2] His main areas of operation were in the vicinity of al-Shaghour and the al-Zur forest in the eastern Ghouta.[15] Through his alliance with a Sufi religious leader, al-Kharrat brought an Islamic holy war dimension to the largely secular revolt, something that was not welcomed by some involved.[16]
 Al-Kharrat commenced guerrilla operations in September, targeting French forces posted in the eastern and southern Ghouta.[20] His prominence rose as he led nighttime raids against the French in Damascus, during which he disarmed army patrols and took soldiers hostage.[6] In al-Shaghour, Souk Saruja and Jazmatiyya, al-Kharrat and his band burnt down all French-held buildings.[6] In the first week of October, sixty French gendarmes were dispatched to the Ghouta to apprehend al-Kharrat and his fighters.[3] The gendarmes were quartered in the home of al-Malihah's mukhtar (village headman).[3] In the evening, the rebels attacked the residence, killing one gendarme and capturing the rest; the prisoners were eventually all returned unharmed.[3]
 On 12 October, French troops backed by tanks, artillery and aerial support launched an operation to surround and eliminate al-Kharrat's rebels in the al-Zur forest.[21] Al-Kharrat's men were forewarned of the French deployment by the peasants of al-Malihah.[21] Positioned among the trees, the rebels used sniper fire against the French troops.[21] The latter were unable to lure the rebels out and retreated.[21]
 As the French withdrew toward al-Malihah, they looted the village and set it on fire.[21] French intelligence officials justified the collective punishment of al-Malihah as retaliation for the rebels' capture and humiliation of the gendarmes during the previous week; the French claimed a young boy from al-Malihah had notified al-Kharrat's men of the French presence in the village.[22] Though they were unable to engage al-Kharrat and his forces directly, French troops executed around 100 civilians from Ghouta villages.[22] Their corpses were brought to Damascus, and the bodies of sixteen men described by the French as "brigands" were put on display.[22]
 Spurred by French army actions in the Ghouta, al-Bakri planned to capture the Citadel of Damascus, where French forces were concentrated, and the Azm Palace, where General Maurice Sarrail, the French high commissioner of Syria, would be residing on 17–18 October (Sarrail was typically headquartered in Beirut).[22] The high commissioner functioned as the overall administrator of Syria on behalf of France and exercised practically absolute power.[23] The rebel units active in Damascus at the time were al-Kharrat's ′isabat and a mixed force of Druze fighters and rebels from the al-Midan quarter and the Ghouta.[24] To compensate for the lack of rebel strength, al-Bakri sent a letter to Sultan al-Atrash requesting reinforcements.[22] Al-Atrash replied that he was currently occupied with operations in the Hauran, but would dispatch his entire force to back the Damascus rebels as soon as affairs there were settled.[22] Before he received al-Atrash's reply, al-Bakri decided to move ahead with the operation.[24]
 On 18 October, al-Kharrat led forty rebels into al-Shaghour from the old cemeteries adjacent to the southern gate of Damascus, announcing that the Druze had arrived to relieve the city from French occupation.[24] Crowds of residents enthusiastically welcomed the rebels, and many took up arms alongside them. Al-Kharrat's men captured the quarter's police station, disarming its garrison.[24] They were joined by Ramadan al-Shallash, a rebel commander from Deir ez-Zor, and twenty of his Bedouin fighters. The joint forces proceeded to the Hamidiyya Market and captured the Azm Palace,[24][25] but Sarrail was not present, having already left to attend a meeting in the Hauran town of Daraa.[24] The rebels plundered the palace and set it on fire.[24] Provence asserts that capturing the palace without Sarrail "held no tactical importance" but was a highly symbolic achievement for the rebels because of the Azm Palace's "importance as the historical seat of economic and political power in Damascus, now usurped by the French and totally undefended".[24]
 While al-Kharrat captured the Azm Palace, al-Bakri and 200 rebels under his command rode through the city and were joined by civilians in increasing numbers.[24] After sealing the Old City to prevent the entry of enemy reinforcements, al-Kharrat issued an order to kill anyone linked to the French army.[25] About 180 French soldiers were killed.[25] Sarrail ordered the shelling and aerial bombardment of the city, which lasted two days and killed about 1,500 people.[26] Chaos and scattered fighting ensued as whole neighborhoods, mosques and churches were leveled, French forces moved in, and hundreds of leading figures in the Syrian national movement were arrested,[25] including al-Kharrat's son Fakhri.[8] The latter was captured on 22 October during a botched nighttime raid by the rebels against the French, who had by then retaken Damascus.[15] Al-Kharrat was offered the release of his son in exchange for his own surrender, but refused.[27]
 The rebels withdrew from Damascus as a meeting was held between French army commander Maurice Gamelin and a delegation of Damascene notables.[28] As a result of the meeting, the French agreed to end their bombardment in return for a payment of 100,000 Turkish gold liras by 24 October.[26] The fine was not paid by the French deadline, but the bombardment was not renewed, likely as a result of orders from the French government in Paris.[29] International condemnation of Sarrail's bombardment of Damascus and growing criticism in France of his mishandling of the revolt led to his dismissal on 30 October.[30] He was replaced by politician Henry de Jouvenel,[31] who arrived in Syria in December.[32] On 22 November, al-Kharrat commanded 700 rebels in a battle with about 500 French soldiers outside of Damascus.[33] Al-Kharrat's men inflicted "trifling" losses on the French, but experienced heavy casualties themselves, with thirty dead and forty wounded according to Reuters.[33] On 5 December, al-Kharrat was one of the commanders of a 2,000-strong force uniting rebels from disparate backgrounds, which assaulted the French Army barracks in al-Qadam, south of Damascus. The French claimed to have inflicted significant casualties, but rebel activity continued.[34]
 Centralized order and oversight among the revolt's armed participants was difficult to establish because of the diversity and independence of the rebel factions. A meeting of rebel leaders was held in the Ghouta village of Saqba on 26 November.[35] Sa'id al-'As accused al-Kharrat and others of plundering in the Ghouta,[36] while al-Kharrat alleged that al-Shallash extorted the residents of al-Midan and the Ghouta town of Douma.[37] The meeting concluded with an agreement to elect a government to replace the French authorities, increase recruitment of the Ghouta's inhabitants, coordinate military operations under a central command, and establish a revolutionary court to execute spies.[36] The meeting also designated the area between the village of Zabdin and north of the Douma-Damascus road as being part of al-Kharrat's zone of operations.[36] Despite his leading role in the rebels' military efforts, al-Kharrat was not included in the newly formed rebel leadership council, nor were any of al-Bakri's allies.[36] Instead, al-'As served as the rebels' overall head.[36]
 Sharp divisions among rebel factions became apparent during a second meeting in Saqba on 5 December. According to Syrian journalist Munir al-Rayyes, hostility between al-Kharrat and al-Shallash was well known among the rebels.[35] Because al-Shallash had levied war taxes on the major landlords and city elites of the Ghouta, al-Kharrat's benefactor al-Bakri viewed him as a threat to the traditional landowning class to which al-Bakri belonged.[38] Al-Rayyis claimed the meeting was called for by al-Kharrat,[39] who ordered his fighters to capture and bring al-Shallash to Saqba.[40] However, according to al-'As, the summit was called by al-Shallash, and once the latter arrived in the village, al-Kharrat personally detained him and confiscated his horse, weapons and money.[40]
 After his detention, al-Shallash was given a brief trial during which al-Kharrat accused him of making "impositions and ransoms and financial collections in the name of the revolt", while al-Bakri condemned him specifically for extorting the residents of Douma for 1,000 giney (Ottoman pounds),[35] and imposing large fines on the inhabitants of Harran al-Awamid, al-Qisa and Maydaa for his own personal enrichment.[41] Al-Kharrat and al-Bakri decided al-Shallash's verdict, and dismissed him from the revolt.[38] While many rebels with officer backgrounds similar to al-Shallash disapproved of the judgement, they did not intervene.[35] In his account of the meeting, al-Rayyis condemned the rebel commanders for complacency in the "ridiculous trial" and accused al-Kharrat of being motivated solely by personal animosity.[39] Al-Shallash was able to escape—or was released by al-'As—when French planes bombed the meeting.[40] Al-Shallash would later surrender to Jouvenel and collaborate with French authorities.[42]
 Al-Kharrat was killed in an ambush by French troops in the Ghouta on 25 December 1925.[39] He was succeeded as qabaday of al-Shaghour and commander of the ′isabat al-Shawaghirah by Mahmud Khaddam al-Srija.[12] Al-Kharrat's men continued to fight the French until the revolt ended in 1927,[43] though historian Thomas Philipp states that al-Kharrat's group dissipated after his death.[44] In January 1926, al-Kharrat's son Fakhri was sentenced to death and publicly executed with two other rebels in Marjeh Square, Damascus.[45] The French had previously implored Fakhri to persuade his father to surrender in return for his release, but Fakhri refused.[15]
 Abd al-Rahman Shahbandar, a prominent Syrian nationalist leader, described al-Kharrat as having played "the preeminent role" in the battle against the French in the Ghouta and Damascus.[7] Historian Daniel Neep wrote that al-Kharrat was the "best-known" of all of the Damascus-based rebel leaders,[8] although other leaders of the rebel movement attributed the publicity and praise of al-Kharrat to the efforts of the Cairo-based Syrian-Palestinian Committee, with which al-Bakri was closely affiliated. Al-Kharrat and his son Fakhri are today considered "martyred heroes" by Syrians for their nationalist efforts and their deaths in the Syrian struggle for independence from France.[16][39]
 
 
 
 
 Ibrahim Hananu
 Abd Al-Rahman Shahbandar
 Ayyash Al-Haj
 Sultan al-Atrash
 Saleh Al-Ali
 Fawzi al-Qawuqji
 1 Early life and career 2 Commander in the Great Syrian Revolt

2.1 Recruitment and early confrontations
2.2 Battle of Damascus and operations in Ghouta
2.3 Tensions with rebel leaders

 2.1 Recruitment and early confrontations 2.2 Battle of Damascus and operations in Ghouta 2.3 Tensions with rebel leaders 3 Death and legacy 4 Notes 5 See also 6 References 7 Bibliography ^ Syrian historian Sami Moubayed and Palestinian historian Hanna Batatu list al-Kharrat's birth year as 1861[1][2] while American historian Michael Provence asserts that al-Kharrat was 50 years old in late 1925, suggesting that he was born in 1875.[3]
 ^ Among the other villages and Damascus neighborhoods that contributed fighters to al-Kharrat's rebel band were Jaramana, Kafr Batna, Beit Sahem, al-Malihah, Sidi Amud, Suq Saruja and al-Amara.[8]
 Great Syrian Revolt Ayyash Al-Haj Ibrahim Hananu Yusuf al-'Azma Abd Al-Rahman Shahbandar Sultan al-Atrash Mandate for Syria and the Lebanon Henri Gouraud Syria Adham Khanjar Saleh Al-Ali Fawzi al-Qawuqji 


Ibrahim Hananu


 


Abd Al-Rahman Shahbandar


 


Ayyash Al-Haj


 


Sultan al-Atrash


 


Saleh Al-Ali


 


Fawzi al-Qawuqji


 ^ Moubayed 2006, p. 381
 ^ Batatu 1999, p. 368.
 ^ a b c d Provence 2005, p. 100.
 ^ Provence 2005, p. 119.
 ^ "Syria Opposition Leader Interview Transcript". The Wall Street Journal. 2 December 2011. Retrieved 7 April 2013..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c d e Moubayed 2006, p. 381.
 ^ a b c Batatu 1999, p. 117.
 ^ a b c d e f g h i Neep 2012, pp. 79–80.
 ^ a b Gelvin 1998, pp. 69–70.
 ^ Gelvin 1998, p. 75.
 ^ Khoury 1987, p. 97.
 ^ a b c Khoury 2006, p. 157.
 ^ a b c d e f Khoury 2006, p. 152.
 ^ a b Khoury 2006, p. 154.
 ^ a b c d Provence 2005, p. 118.
 ^ a b c d e Provence 2005, p. 101.
 ^ Neep 2012, p. 131.
 ^ Glassé, Cyril (1989). "Damascus". The New Encyclopedia of Islam. London: Stacey International. p. 110. ISBN 0-7591-0190-6.
 ^ Baer, Gabriel (1982). Fellah and Townsman in the Middle East: Studies in Social History. Abingdon: Frank Cass and Company Limited. p. 302. ISBN 0-7146-3126-4.
 ^ a b Khoury 1987, p. 174.
 ^ a b c d e Provence 2005, pp. 101–102.
 ^ a b c d e f Provence 2005, p. 102.
 ^ Peretz, Don (1994). The Middle East Today (Sixth ed.). Westport: Greenwood Publishing Group. pp. 365–366. ISBN 0-275-94575-8.
 ^ a b c d e f g h i Provence 2005, p. 103.
 ^ a b c d Moubayed 2006, p. 382.
 ^ a b Provence 2005, p. 104.
 ^ MacCallum, Elizabeth Pauline (1928). The Nationalist Crusade in Syria. New York: The Foreign Policy Association. p. 132. OCLC 234199.
 ^ Khoury 1987, p. 177.
 ^ Provence 2005, pp. 104–105.
 ^ Provence 2005, p. 109.
 ^ Khoury 1987, pp. 181–182.
 ^ Provence 2005, p. 126.
 ^ a b Reuters (1 January 1926). "Syrian Revolt: Hassan Kharrat Killed". The Advocate. Retrieved 7 April 2013.
 ^ Provence 2005, p. 116.
 ^ a b c d Provence 2005, pp. 134–135.
 ^ a b c d e Neep 2012, p. 81.
 ^ Neep 2012, p. 83.
 ^ a b Provence 2005, p. 134.
 ^ a b c d Provence 2005, p. 135.
 ^ a b c Provence 2005, p. 137.
 ^ Provence 2005, p. 136.
 ^ Provence 2005, pp. 138–139.
 ^ Provence 2005, p. 138.
 ^ Philipp, Thomas; Schumann, Christoph, eds. (2004). From the Syrian Land to the States of Syria and Lebanon. Würzburg: Orient Institut der DMG Beirut. p. 281. ISBN 3-89913-353-6.
 ^ Neep 2012, p. 54.
 Batatu, Hanna (1999). Syria's Peasantry, the Descendants of Its Lesser Rural Notables, and Their Politics. Princeton: Princeton University Press. ISBN 0-691-00254-1. Khoury, Philip S. (1987). Syria and the French Mandate: The Politics of Arab Nationalism, 1920–1945. Princeton University Press. ISBN 0-691-05486-X. Khoury, Philip S. (2006). "Abu Ali al-Kilawi, A Damascus Qabaday".  In Burke, Edmund III; Yaghoubian, David N. (eds.). Struggle And Survival in the Modern Middle East (Second ed.). London: University of California Press. ISBN 0-520-24661-6. Gelvin, James L. (1998). Divided Loyalties: Nationalism and Mass Politics in Syria at the Close of Empire. London: University of California Press. ISBN 0-520-21069-7. Moubayed, Sami (2006). Steel and Silk: Men and Women who Shaped Syria 1900–2000. Seattle: Cune Press. ISBN 1-885942-41-9. Neep, Daniel (2012). Occupying Syria Under the French Mandate: Insurgency, Space and State Formation. New York: Cambridge University Press. ISBN 1-107-00006-8. Provence, Michael (2005). The Great Syrian Revolt and the Rise of Arab Nationalism (First ed.). Austin: University of Texas Press. ISBN 0-292-70635-9. v t e State of Syria
State of Aleppo
State of Damascus
Al-Jazira Province State of Aleppo State of Damascus Al-Jazira Province Jabal Druze State Alawite State Sanjak of Alexandretta Greater Lebanon 1919 revolt Franco-Syrian War
Battle of Maysalun Battle of Maysalun 1925–1927 revolt
The epic of Ain Albu Gomaa
Capture of Salkhad
Battle of al-Kafr
Battle of al-Mazraa
Battle of al-Musayfirah
1925 Hama uprising The epic of Ain Albu Gomaa Capture of Salkhad Battle of al-Kafr Battle of al-Mazraa Battle of al-Musayfirah 1925 Hama uprising 1936 general strike Syria–Lebanon Campaign Levant Crisis Sykes–Picot Agreement (1916) Paulet–Newcombe Agreement (1920) Franco-Syrian Treaty of Independence (1936) Hashim al-Atassi Shukri al-Quwatli Khalid al-Azm Jamil Mardam Bey Sultan al-Atrash Yusuf al-'Azma Ayyash Al-Haj Ibrahim Hananu Abd al-Rahman Shahbandar French High Commissioner Charles de Gaulle Henri Gouraud Asia portal 1861 births 1925 deaths Syrian Arab nationalists Ottoman Arab nationalists People of the Great Syrian Revolt Syrian Sunni Muslims People from Damascus Guerrillas killed in action Featured articles Use dmy dates from July 2019 Articles containing Arabic-language text Articles with hCards Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version العربية বাংলা Deutsch Español فارسی Français Bahasa Indonesia Latina Bahasa Melayu Português اردو  This page was last edited on 13 October 2019, at 05:24 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Abu Muhammad Hasan al-Kharrat ^ ^ ^ ^ a b c d ^ ^ a b c d e a b c a b c d e f g h i a b ^ ^ a b c a b c d e f a b a b c d a b c d e ^ ^ ^ a b a b c d e a b c d e f ^ a b c d e f g h i a b c d a b ^ ^ ^ ^ ^ ^ a b ^ a b c d a b c d e ^ a b a b c d a b c ^ ^ ^ ^ ^ Portrait of al-Kharrat 1861Damascus, Syria Vilayet, Ottoman Empire 25 December 1925 (aged 63–64)Damascus, French Mandate of Syria Syrian Night watchman and qabaday (local youths boss) of al-Shaghour Commander of Damascus and Ghouta rebels during the Great Syrian Revolt Fakhri 
Great Syrian Revolt
Ayyash Al-Haj
Ibrahim Hananu


 
Yusuf al-'Azma
Abd Al-Rahman Shahbandar
Sultan al-Atrash


 
Mandate for Syria and the Lebanon
Henri Gouraud
Syria


 
Adham Khanjar
Saleh Al-Ali
Fawzi al-Qawuqji


 
State of Syria
State of Aleppo
State of Damascus
Al-Jazira Province
Jabal Druze State
Alawite State
Sanjak of Alexandretta
Greater Lebanon
  
1919 revolt
Franco-Syrian War
Battle of Maysalun
1925–1927 revolt
The epic of Ain Albu Gomaa
Capture of Salkhad
Battle of al-Kafr
Battle of al-Mazraa
Battle of al-Musayfirah
1925 Hama uprising
1936 general strike
Syria–Lebanon Campaign
Levant Crisis
 
Sykes–Picot Agreement (1916)
Paulet–Newcombe Agreement (1920)
Franco-Syrian Treaty of Independence (1936)
 Syrian
Hashim al-Atassi
Shukri al-Quwatli
Khalid al-Azm
Jamil Mardam Bey
Sultan al-Atrash
Yusuf al-'Azma
Ayyash Al-Haj
Ibrahim Hananu
Abd al-Rahman Shahbandar
French
French High Commissioner
Charles de Gaulle
Henri Gouraud
 
Hashim al-Atassi
Shukri al-Quwatli
Khalid al-Azm
Jamil Mardam Bey
Sultan al-Atrash
Yusuf al-'Azma
Ayyash Al-Haj
Ibrahim Hananu
Abd al-Rahman Shahbandar
 
French High Commissioner
Charles de Gaulle
Henri Gouraud
 