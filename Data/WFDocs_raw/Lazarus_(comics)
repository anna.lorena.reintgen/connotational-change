
 Lazarus is a dystopian science fiction comic book series created by writer Greg Rucka and artist Michael Lark. The two began developing the idea in 2012 and partnered with colorist Santi Arcas to finish the art. Image Comics has been publishing the book since the first issue was released on June 23, 2013. Other creators were brought in later to assist with lettering and inking. A six-issue spin-off limited series, Lazarus: X+66, was released monthly in 2017 between issues 26 and 27 of the regular series. Rucka initially said the series could run for up to 150 issues, but later reduced the estimate by half. Lazarus is being collected into paperback and hardcover editions, which sell better than the monthly issues.
 In the series, the world has been divided among sixteen rival families, who run their territories in a feudal system. The main character is Forever Carlyle, the military leader of the Carlyle family. The major themes of Lazarus are the meaning of "family" and nature versus nurture. Critics have given it mostly positive reviews and have praised its worldbuilding, although it is sometimes criticized for slow pace. It has received particular attention for its political themes, and comparisons have been drawn between elements of the story and the presidency of Donald Trump.
 Lazarus is being adapted into other media. Green Ronin Publishing is using the plot as a campaign setting for their Modern AGE role-playing game in 2018. A television adaptation is in development with Legendary Television and Amazon Studios.
 American writer Greg Rucka and artist Michael Lark had previously collaborated on the comic series Gotham Central for DC Comics between 2002 and 2004 and various small projects for Marvel Comics in the years following.[1] Lark wanted to work with Rucka on a creator-owned comic because he felt he was at his best drawing the kind of stories Rucka writes.[2]
 In June 2012, Rucka was in Dallas as part of a book-signing tour. He had dinner with Lark, who lived nearby, and shared an idea for a scene involving a woman who had been shot rising from the dead and pursuing her attackers. Lark liked the story and committed to drawing the comic as soon as a full script was ready.[1][3] Although Rucka had previously published his creator-owned material through Oni Press, his friend Ed Brubaker had been pushing him to work with Image Comics.[1] When they contacted Image's Eric Stephenson and pitched the project as "The Godfather meets Children of Men", he immediately expressed interest.[3] The project, titled Lazarus, was officially announced at the San Diego Comic Con on July 14, 2012.[4][5] The announcement was accompanied by promotional artwork colored by American Elizabeth Breitweiser and featured a prototype logo design and typeface.[4]
 Image Comics provided David Brothers to serve as the series' editor.[6] Unlike traditional comic editors who focus on coordinating schedules and pushing deadlines, Brothers only reviews the work and provides responses that help the team create better work with more internal consistency.[7] Eric Trautmann, who had previously edited two of Rucka's novels, was recruited to help with research, timelines, and design work.[2][8] Lark wanted to work with a European colorist to provide a look distinct from traditional American comics. Rucka suggested Santi Arcas, a Spanish colorist he had worked with in the past, and Lark particularly liked Arcas' skies and textures.[3][9]
 Rucka and Lark developed the setting for Lazarus by looking at the Occupy movement and the underlying economics, then asking themselves "What happens if it goes horribly wrong?"[10] They decided how the story would end before work began on the first issue.[11] They initially gave their lead character the name Endeavor, but Rucka changed the name to Forever to avoid a conflict with a different comic being developed at the same time about a young Inspector Morse.[1][10] Lark based her body type on the soccer player Hope Solo.[1]
 Lark was disappointed by the first script as he felt none of the characters were likable, and the scene described to him over dinner was not included. In response, Rucka wrote a new draft restoring the missing opening scene.[1][12] Lark began drawing the first issue in January 2013, basing the opening scene on the reconstruction sequence in 1997 film The Fifth Element.[6][13]
 When writing a new script, Rucka tries to follow the world-building model used by William Gibson in his 1984 novel Neuromancer and provide information about the environment through context instead of exposition.[14] His biggest struggle is delivering details while maintaining a proper narrative pace.[15] He sometimes self-censors "exceptionally dark" material because he does not want to make Lark draw it.[2] After Lark receives a new script, the collaboration between them is "immediate and constant".[1] Lark questions Rucka about characterization and the direction of the story, leading Rucka to rewrite scripts resulting in what he believes is a better final product.[2] Lark refuses to read scripts in advance so he will stay focused on what is in front of him, not what he will be drawing next.[16] Rucka says Lark intuitively knows what is happening in the story even when it isn't clearly scripted.[2]
 Rucka and Lark have an ongoing conversation about how to show the injustice in the way Forever is treated without being complicit in it themselves. For example, medics must remove Forever's clothes to treat her wounds. Lark wanted to avoid sexualizing the images, but also avoid being "coy" by simply blocking parts of her body with another character's arm.[2] The script gives Lark no direction for aspects like architecture, clothing, or vehicle design. Designing these technical details involves research into prototype technology and takes almost as long as drawing the actual pages for the comics. The time required to create the sets is the primary reason Lark sometimes falls behind schedule.[2][12]
 Lark works on Lazarus ten or more hours per day.[12] He uses photo references and digital tools in the early stages of his art, but the layouts and drawing are done with traditional tools. He is more involved with the coloring on Lazarus than any other comic he has illustrated.[6] The logo design was finalized by Trautmann and Lark.[17] Lark initially did all the lettering and inking for Lazarus, but doing so made it impossible to release new issues on a regular schedule.[3][7] To give him more time to focus on drawing, some of the smaller tasks like logo and type design were given to other people.[6] Brian Level assisted with inking on issues three through ten, when he was replaced by Tyler Boss.[7][18] Beginning with issue ten, Jodi Wynne took over the lettering duties and Owen Freeman started creating the cover art.[8][19] Fake advertisements found on the back covers and many of the computer screens and holographic images in the artwork are created by Trautmann.[7][8] Lark and Rucka often discuss whether to use sound effects in scenes or limit their use. Lark does not want to rely on them to convey information because they may become a "crutch" in place of including important details in the art.[9]
 Issue fifteen features a silent, thirteen-page fight between two characters.[20][21] Rucka, who used to be a choreographer, filmed himself acting out the battle with a friend. Lark used the film for reference as he drew.[15]
 Following the 2016 United States presidential election, the creators' vision for the comic shifted. Rucka, who had used the letter columns in the series to discuss his concerns about then-candidate Donald Trump, told Oregon Public Broadcasting that after the election results Lazarus had changed from a dystopian science fiction story to a documentary.[22] During a discussion panel at the 2017 Chicago Comic & Entertainment Expo, Rucka described Lazarus as being "about the blood red rage that leads to a Trump administration" before joking that he had "tried to warn you three years ago!"[23] Although the overall plan for Lazarus did not change, Rucka said he had a growing interest in writing about a brighter future instead.[22]
 Brubaker advised Rucka to create a four-page "trailer" to promote the book, a strategy Brubaker had used with The Fade Out. Rucka was not initially interested, but Lark liked the idea. The trailer debuted at the 2013 Emerald City Comic Con before appearing online and in Previews, the catalog for Diamond Distribution. The scene was not reproduced in any issue of the series.[1][24]
 Most comics sold to specialty stores in the direct market are non-returnable. To reduce the financial risk for retailers who were uncertain about its sales potential, unsold copies from qualifying orders of the first three issues of Lazarus could be returned to the publisher.[25][26] The first issue went on sale on June 26, 2013 and sold out of its approximately 48,000 copy print run at the distributor level.[27][28] A second printing was announced to coincide with the release of issue two. After a second sellout, it was added to the "Image Firsts" program, a line of discounted first-issue reprints continuously available for retailers to order.[29] By the end of 2013, the first issue had sold an estimated 50,200 copies.[30] The second issue, which also went through multiple printings, sold an estimated 30,600 copies.[26][30] Over the next two years, sales fell steadily to about 14,500 copies.[31]
 Because of scheduling issues, Rucka and Lark were behind on the series from the start.[12] The problems were exacerbated by illness and poor communication during the "Lift" arc, causing issue 9 to be delayed by more than a month.[32] Further late issues led retailers to reduce their orders for new issues.[32] In fall 2015, the team announced a four-month hiatus between issues 21 and 22 to allow Lark to get ahead of schedule.[33] They said they would not solicit any more issues until the next story arc was completed, and the hiatus actually lasted six months, in part because of miscommunication between Image Comics and Diamond Distribution.[12][33][34] During the hiatus, they released a sourcebook providing additional, non-essential background on the Carlyle family.[2] The sourcebook was created with input from Robert Mackenzie and David Walker, who had been providing annotations for the series at NerdSpan.[35] Despite the break, five months passed between the fourth and fifth chapters of the Cull arc. A second sourcebook detailing the Hock Family was released in April 2017.[36]
 In the letter column of issue 26, Rucka announced a six-issue limited series titled Lazarus: X+66 would be released monthly beginning July 2017. The series was written by Rucka and Trautmann, and each issue focused on different supporting characters from the main series. Lark was involved as a consultant, but each issue was drawn by a new artist. This decision gave Lark time to work on something unrelated to Lazarus, which had been his only project since the series began.[37] A four-page preview of the first installment, drawn by Steve Lieber, was included with the book's solicitation in Image Plus #16.[38] A third source book, this time covering the Vassalovka family, was released one week after the limited series ended. Lark returned to Lazarus in April 2018 with issue 27.[39]
 In the letter column of issue 27, Rucka announced the series would change to a 64-page quarterly format beginning with issue 29 in September 2018. He said it would feature 44 pages of comic story and 20 pages will be a variety of prose material.[40] When the proposed issue 29 was solicited, it was retitled and renumbered as Lazarus: Risen #1.[41]
 At the start of the Lazarus, Rucka estimated it would take between 100 and 150 issues to reach the ending.[33][42] In May 2016, he revised his estimate downward, saying Lazarus was "25–30% complete at issue 21".[15]
 The series has been compiled in six trade paperbacks and three hardcovers.[43] The first paperback collection appeared on the New York Times Best Seller List for Paperback Graphic Books in eighth position for two weeks in November 2013.[44] The second appeared in the tenth spot for one week in August 2014.[45] The hardcovers include introductions from notable comic creators like Warren Ellis and behind-the-scenes material not otherwise available. Rucka and Lark take the extra content in them "very seriously" because hardcovers are expensive.[14][46] In 2015, Rucka said sales of single issues "aren't great", but went on to say the series is selling better in a collected format.[42][47] That year, the first paperback collection sold close to the same number of copies to comic specialty shops as it did in 2013, the year it was released.[30][48] Lazarus has been translated into several European languages by Italian publisher Panini Comics and released in hardcover formats containing the same material as the English paperbacks.[49][50]
 Lazarus is a coming of age story for a young woman named Forever Carlyle who is questioning her identity. Its major themes are the meaning of "family" and nature versus nurture.[14] It is set in a bleak future where the world has been divided into sixteen large land areas, each led by one family. Each family rules their region in a feudal system that divides people into three tiers: "family", "serfs" (skilled labors), and "waste" (everyone else).[51] The families have formed alliances to protect themselves from other families, and each family has a chosen warrior known as a "Lazarus" who represents them in combat. Forever is the Lazarus for the Carlyle family, which controls the western half of North America.[52] She obeys the family patriarch, Malcolm Carlyle, and has four siblings: Steven, Beth, and twins Jonah and Johanna. The original source of the Carlyle's fortune and power is from their various developments in genetic technology. Among other advancements, their modified seeds provide food for most of the world. The Carlyles have also altered their own genetics, which has allowed all of them to grow old without suffering the consequences of age.
 Lazarus has received positive reviews since its debut.[53] According to review aggregator Comic Book Roundup, critics gave the first issue an average score of 8.7/10 based on 32 reviews.[54] The series as a whole averages 8.6/10 based on 284 reviews.[55] Critics and fans often praise the world-building in Lazarus, but Lark and Rucka see it as secondary and think it receives too much focus.[14] Publishers Weekly said Forever's "fascinating complexity" made Lazarus stand out from other graphic novels.[46] Writing for Comics Alliance, KM Bezner said every character, including the diabolical ones, displayed humanity and "[blurred] the lines between shades of morality".[56] On Broken Frontier, Tyler Chin-Tanner described "Lift", the series' second story arc, as "a moving tale of family sacrifice".[57] The series has appeared on many comic critics' "best of" lists.[58] Since the series debut, the slow pace has been a common point of criticism.[59]
 Many critics compared Lazarus with other genre works. The timeliness of Rucka's premise made the series stand out among dystopian fiction for IGN reviewer Melissa Grey.[60] Garrett Martin wrote in Paste Magazine that the series was unlike other contemporary class warfare genre fiction like the novels Hunger Games or Blackacre because it is told from the oppressors' point of view.[61] Oliver Sava reviewed the series for The A.V. Club and said it stood out from Image's other science fiction comics "because it's more grounded in current political and economic trends".[20] Rucka specifically addressed fan-drawn parallels to the television series Game of Thrones, saying he had not read the books and purposely avoids watching the show to avoid unintentionally borrowing ideas from it. Lark thinks the comparison to Game of Thrones works to some extent, but points out that Lazarus concentrates more on a single character.[12]
 Lark was praised for being equally good at depicting violence and introspection, and Martin said it was Lark's finest work.[46][61] According to Lark, the characters in Lazarus rarely say what they mean, and some vital story beats are depicted by wordless art.[6][62] Arcas received notice for adding texture and depth to Lark's art and using pallette changes to help tell the story.[7][20]
 Because of its economic themes, Bezner warned that the political elements of Lazarus would not be for everyone.[56] In The Jersey Journal, critic William Kulesa believed the "deeply considered speculation on society, technology, and the future" is what made the series high-quality science fiction.[63] While Newsarama reviewer Vanessa Gabriel felt Lazarus "engages the reader with plausibility",[64] Chin-Tanner found it to be a character driven story even though it dealt with political and scientific issues.[57] Following the election of Donald Trump as President of the United States in 2016, Salon writer Mark Peters called the series "newly relevant" and compared Trump to the Carlyle family.[65]
 In an in-depth review of the series for the Los Angeles Review of Books in 2017, Evan McGarvey praised the research and thought that went into Lazarus, but expressed concern that the visual requirements of the art conflicted with the political themes. He specifically noted the ruling families and their soldiers "simply look cooler" than the waste with whom the audience is meant to identify, and concluded that this dissonance may skew the real message Rucka and Lark want to send. McGarvey went on to compare the Carlyles to the Mercer family and the lift to China's Gaokao.[66]
 Legendary Television bought the rights to adapt Lazarus following a competitive bidding war in March 2015.[53][67] Rucka and Lark will be executive producers along with David Manpearl and Matt Tolmach.[53][68] A pilot script written by Rucka entered its final draft in late 2015 and Legendary began looking for a network willing to purchase it.[42][67] During the hiatus between issues 21 and 22, Rucka and Lark were able to devote more time to developing the adaptation.[12] Rucka said the development process for Lazarus has been better than any of his previous Hollywood experiences, and that he hopes the show will be able to explore characters more deeply using scenes cut from the book.[12][42]
 In September 2017, Deadline Hollywood reported the adaptation was being developed as a potential series for Amazon Studios, who made a "significant production investment" in it.[69] In the letter column of Lazarus X+66 #4 (November 2017), Rucka said this announcement included some inaccuracies, and emphasized the show is still a long way from being released. He said the casting process had not yet begun.[70]
 In the Spring of 2017, Green Ronin Publishing announced The World of Lazarus, a campaign setting in their Modern AGE role-playing game.[71] Although initially planned for a November 2017 release, it was delayed until 2018 to allow more time for development.[72] Rucka said role playing games had an important part in his development as a writer, and that having one of his ideas turned into one "might just possibly be the greatest compliment I could ever receive."[71]
 1 Publication history

1.1 Early development
1.2 Production
1.3 Publication
1.4 Collected editions

 1.1 Early development 1.2 Production 1.3 Publication 1.4 Collected editions 2 Plot

2.1 Synopsis
2.2 Plot

 2.1 Synopsis 2.2 Plot 3 Critical reception

3.1 Political themes

 3.1 Political themes 4 Adaptations in other media

4.1 Television
4.2 Role playing

 4.1 Television 4.2 Role playing 5 External links 6 References Robert Mackenzie and David Walker provide annotations for each issue at Nerdspan.com ^ a b c d e f g h Harper, David (May 8, 2013), "Rucka and Lark Talk the Beautiful, Dark, Twisted Future of “Lazarus” – Interview," Multiversity Comics. Retrieved August 28, 2016
 ^ a b c d e f g h Sims, Chris (April 14, 2016), "The Lesser Of Fourteen Evils: Greg Rucka And Michael Lark On 'Lazarus'," Comics Alliance. Retrieved August 26, 2016
 ^ a b c d Arrant, Chris (February 3, 2014), "Artist Michael Lark Brings Rucka's LAZARUS to Life at Image," Newsarama. Retrieved September 3, 2016
 ^ a b Rucka, Greg (July 14, 2012), "Lazarus Rising," GregRucka.com. Retrieved August 26, 2016
 ^ Johnston, Rich (July 14, 2012), "Image Comics Announces Greg Rucka And Michael Lark’s Lazarus," Bleeding Cool. Retrieved September 3, 2016
 ^ a b c d e Harper, David (August 5, 2013), "Artist August: Michael Lark – Interview," Multiversity Comics. Retrieved September 3, 2016
 ^ a b c d e Harper, David (October 21, 2013), "The Thrills and the Waste of the World of "Lazarus" with Greg Rucka and Michael Lark – Interview," Multiversity Comics. Retrieved September 3, 2016
 ^ a b c "Lazarus: Going From 0 to X +65 Interview," Image Comics. Retrieved September 3, 2016
 ^ a b Walker, David and McKenzie, Robert (2014), "Michael Lark Lazarus #7 Interview, Part 2," NerdSpan. Retrieved September 3, 2016
 ^ a b Glendening, Daniel (August 22, 2012), "Rucka and Lark Reunite for Dystopian “Lazarus”," Comic Book Resources. Retrieved September 3, 2016
 ^ O'Keefe, Matt (June 7, 2016), "Interview: Greg Rucka and Michael Lark on Lazarus and What Makes Them Click," Comics Beat. Retrieved August 26, 2016
 ^ a b c d e f g h Helvie, Forrest (June 14, 2016), "Rucka & Lark on Lazarus' Slow Burn, The Upcoming 'Cull,' & Possible TV Series," Newsarama. Retrieved August 28, 2016
 ^ Burlingame, Russ (July 28, 2013), "Lazarus Creators Greg Rucka and Michael Lark Discuss Their Sold-Out First Issue," ComicBook.com. Retrieved September 8, 2016
 ^ a b c d Santori-Griffith, Matt (April 21, 2016), "Interview: Greg Rucka and Michael Lark Dig Deeper Into Lazarus," Comicosity. Retrieved August 28, 2016
 ^ a b c Lovett, Jamie (May 16, 2016), "Story of a Daughter: Rucka and Lark on Lazarus," ComicBook.com. Retrieved August 28, 2016
 ^ Walker, David and McKenzie, Robert (2013) "Interview: Greg Rucka Talks Lazarus And More (Part 2)," NerdSpan. Retrieved November 14, 2017
 ^ Lazarus vol 1. (w)Greg Rucka (a)Michael Lark (p)Image Comics
 ^ Lazarus #3 (August 2013), (w)Rucka, Greg (a)Lark, Michael (p)Image Comics
 ^ Lazarus #11 (August 2014), (w)Rucka, Greg (a)Lark, Michael (p)Image Comics
 ^ a b c Sava, Oliver (February 20, 2015), "Rucka & Lark's Lazarus goes to war with a brutal action-centric issue", The A.V. Club. Retrieved September 3, 2016
 ^ Lazarus #15 (February 2015), (w)Rucka, Greg (a)Lark, Michael (p)Image Comics
 ^ a b Baer, April (May 26, 2017), "Comics Writer Greg Rucka On Wonder Woman, Stumptown And More," OPB. Retrieved June 22, 2017
 ^ (April 24, 2017), "Rucka, Young, Bellaire & More Reveal Their Storytelling Process," Comic Book Resources. Retrieved June 22, 2017
 ^ Hughes, Joseph (March 4, 2013), "First Look: Image Comics Release Four Page Story From The World Of Rucka And Lark’s ‘Lazarus’," Comics Alliance. Retrieved September 3, 2016
 ^ Johnston, Rich (June 25, 2013), "Speculator Corner – Lazarus To Moombas (Uncanny Update)," Bleeding Cool. Retrieved September 3, 2016
 ^ a b Johnston, Rich (July 30, 2013), "Lazarus #2 Sells Out Of 35,000 Print Run," Bleeding Cool. Retrieved September 8, 2016
 ^ Miller, John Jackson, "Comichron: June 2013 Comic Book Sales to Comics Shops," Comichron. Retrieved May 29, 2018
 ^ (June 27, 2013), "Lazarus #1 Sells Out, Going To Second Printing," ComicBook.com. Retrieved May 29, 2018
 ^ Press Release, (April 4, 2014), "What can you get for a dollar? New Image Firsts," Image Comics. Retrieved September 15, 2016
 ^ a b c Miller, John Jackson, "2013 Comic Book Sales Figures," Comichron. Retrieved September 10, 2016
 ^ Carter, Dave (September 4, 2015), "Image Comics Month-to Month Sales: July 2015 – The Walking On Guard Saga," Comics Beat. Retrieved September 8, 2016
 ^ a b Mackenzie, Robert and Walker, David (2014), "Interview: Greg Rucka and Michael Lark," NerdSpan. Retrieved November 14, 2017
 ^ a b c Weiland, Jonah (November 11, 2015), "Rucka Drops the Mic in "Lazarus," Conjures "Black Magick" at Image & Can't Resist Star Wars," Comic Book Resources. Retrieved August 26, 2016
 ^ Lazarus #22 (June 2016), (w)Rucka, Greg (a)Lark, Michael (p)Image Comics
 ^ Mackenzie, Robert and Walker, David (2015), "Lazarus and the Art of the Season Finale," NerdSpan. Retrieved November 14, 2017
 ^ "Lazarus Sourcebook #2: Hock," Image Comics. Retrieved February 7, 2017
 ^ Lazarus #26 letter column (March 29, 2017). (w)Rucka, Greg & Lark, Michael (p)Image Comics
 ^ Image Plus #16 (April 26, 2017). (p)Image Comics
 ^ Lazarus X+66 #6 letter column (February, 2018). (w)Rucka, Greg & Lark, Michael (p)Image Comics
 ^ Rucka, Greg. Lazarus #27. Letters column: Image Comics.
 ^ "Lazarus: Risen #1". Image Comics. Retrieved January 26, 2019.
 ^ a b c d Kennedy, G.D. (November 8, 2015), "The Greg Rucka Interviews: Talking Lazarus and Black Magick," 13th Dimension. Retrieved September 3, 2016
 ^ "Lazarus," Image Comics. Retrieved August 26, 2016
 ^ (November 17, 2013), "Paperback Graphic Books," New York Times. Retrieved June 23, 2017
 ^ (August 31, 2014), "Paperback Graphic Books," New York Times. Retrieved June 23, 2017
 ^ a b c "Lazarus, Book 1," Publishers Weekly. Retrieved August 28, 2016
 ^ Evans, William (September 15, 2015), "The Carcosa Interview: An Extended Sit-Down with Greg Rucka," Black Nerd Problems. Retrieved September 3, 2016
 ^ Miller, John Jackson, "2015 Comic Book Sales Figures," Comichron. Retrieved September 10, 2016
 ^ Lucaroni, Manuel (September 16, 2017), "Panini Comics, le uscite di ottobre – novembre 2017" (Italian), Manga Forever. Retrieved September 27, 2017. English translation
 ^ "Famiglia. Lazarus," Book Depository. Retrieved September 27, 2017
 ^ de Guzman, Jennifer (October 8, 2014), "Lazarus by Rucka and Lark Gets Deluxe Hardcover in November," Image Comics. Retrieved August 30, 2016
 ^ Peters, Mark (October 24, 2016), "Why aren't you reading Lazarus by Greg Rucka & Michael Lark?," Paste Magazine. Retrieved February 2, 2017
 ^ a b c Han, Angie (March 25, 2015), "Greg Rucka and Michael Lark’s ‘Lazarus’ Is Being Turned Into a TV Series," Slash Film. Retrieved August 28, 2016
 ^ "Lazarus #1 Reviews (2013) Archived 2016-09-15 at the Wayback Machine," ComicBookRoundUp. Retrieved September 2, 2016
 ^ "Lazarus Comic Series Reviews Archived 2016-08-20 at the Wayback Machine," ComicBookRoundUp. Retrieved September 2, 2016
 ^ a b Bezner, KM (September 17, 2015), "Money, Sex, Power: Should You Be Reading 'Lazarus'?," Comics Alliance. Retrieved September 8, 2016
 ^ a b Chin-Tanner, Tyler (December 29, 2014), "BF Awards 2014 – Best Ongoing Series: Lazarus by Greg Rucka & Michael Lark," Broken Frontier. Retrieved August 28, 2016
 ^ 
"2013 Best New Series," Broken Frontier. Retrieved August 28, 2016
"2014 Best Ongoing Series," Broken Frontier. Retrieved August 28, 2016
Harper, David (July 20, 2015), "A Very Scientific Ranking of the Ten Best Image Comics," SKTCHD. Retrieved September 8, 2016
Staff (December 12, 2013), "2013 in Review: Best New Series," Multiversity Comics. Retrieved September 15, 2016
 "2013 Best New Series," Broken Frontier. Retrieved August 28, 2016 "2014 Best Ongoing Series," Broken Frontier. Retrieved August 28, 2016 Harper, David (July 20, 2015), "A Very Scientific Ranking of the Ten Best Image Comics," SKTCHD. Retrieved September 8, 2016 Staff (December 12, 2013), "2013 in Review: Best New Series," Multiversity Comics. Retrieved September 15, 2016 ^ Katayama, Draven (June 18, 2015), "Best Shots Reviews: Prez #1, Runaways #1, Wonder Woman #41, Southern Bastards #9, JLA #1, More," Newsarama. Retrieved September 10, 2016
 ^ Grey, Melissa (June 26, 2013), "Lazarus #1 Review," IGN. Retrieved November 7, 2017
 ^ a b Martin, Garrett (July 5, 2013), "Lazarus #1 by Greg Rucka & Michael Lark," Paste Magazine. Retrieved August 28, 2016
 ^ (January 1, 2016), "Best of 2015: Best Ongoing Series," Comic Bastards. Retrieved September 8, 2016
 ^ Kulesa, William (July 19, 2013), "Greg Rucka and Michael Lark present compelling science-fiction world in 'Lazarus'," The Jersey Journal. Retrieved September 3, 2016
 ^ Gabriel, Vanessa (June 25, 2013), "Best Shots Advance Reviews: Lazarus #1, Uncanny #1, More," Newsarama. Retrieved September 10, 2016
 ^ Peters, Mark (January 6, 2017), "The Comic Book "Lazarus" is a Frightening Vision of Where America Could Be Headed," Salon. Retrieved June 22, 2017
 ^ McGarvey, Evan (October 14, 2017), "Cogs of War: "Lazarus" and the Limits of Dystopia," Los Angeles Review of Books. Retrieved November 8, 2017
 ^ a b Ford, Rebecca (March 24, 2015), "Legendary TV, Matt Tolmach Adapting Comic Book 'Lazarus' (Exclusive)," The Hollywood Reporter. Retrieved September 3, 2016
 ^ Bailey, Benjamin (March 24, 2015), "Greg Rucka Michael Lark's Lazarus is coming to TV Archived 2017-02-15 at the Wayback Machine," Nerdist. Retrieved August 28, 2016
 ^ "Amazon Increases Production Spending for 2018". Variety. 28 September 2017. Retrieved 2 October 2017.
 ^ Lazarus X+66 #4 (w)Greg Rucka (a)Michael Lark (p)Image Comics November 1, 2017
 ^ a b Niebling, William (May 2, 2017), "Greg Rucka's 'Lazarus' converted to RPG", ICv2. Retrieved May 18, 2017
 ^ Pramas, Chris (August 22, 2017), "Ronin Roundtable: Upcoming Releases!," Green Ronin Publishing. Retrieved November 8, 2017
 2013 comics debuts Dystopian comics Image Comics titles Science fiction comics Webarchive template wayback links Featured articles Title pop Pages using multiple image with manual scaled images Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Polski  This page was last edited on 13 October 2019, at 01:50 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Lazarus a b c d e f g h a b c d e f g h a b c d a b ^ a b c d e a b c d e a b c a b a b ^ a b c d e f g h ^ a b c d a b c ^ ^ ^ ^ a b c ^ a b ^ ^ ^ a b ^ ^ ^ a b c ^ a b a b c ^ ^ ^ ^ ^ ^ ^ ^ a b c d ^ ^ ^ a b c ^ ^ ^ ^ ^ ^ a b c ^ ^ a b a b ^ ^ ^ a b ^ ^ ^ ^ ^ a b ^ ^ ^ a b ^ Cover to Lazarus #2. Art by Michael Lark. Image Comics (US, Canada)Panini Comics (International) Monthly (Jun 2013 – May 2018)Quarterly (Mar 2019 – present) Ongoing series Science fiction June 2013 Greg Rucka Michael Lark Jodi Wynne (#1–26)Simon Bowland (#27–28, Risen #1–present) Santi Arcas .mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}ISBN 9781607068099 ISBN 9781607068716 ISBN 9781632152251 ISBN 9781632155238 ISBN 9781534300248 ISBN 9781534304888 ISBN 9781534305151 ISBN 9781632151834 ISBN 9781632157225 ISBN 9781534313347 
 First Collection(11/19/2014)ISBN 9781632151834
 Volume 1: Family(10/9/2013)ISBN 9781607068099
 #1 (6/26/2013)
 When Jonah and Johanna plot a coup against Malcolm, one of their first steps is to kill Forever. When she is returning from a diplomatic mission in Morray territory (South America), Carlyle airships fire on Forever and the Morray Lazarus, Joacquim. Neither are killed, and Forever is told by a pilot he was sent by Jonah. Realizing their coup has failed, Johanna arranges for Jonah to appear to have orchestrated it alone. Jonah flees, and Forever receives an anonymous message saying the Carlyles are not her family.
 #2 (7/24/2013)
 #3 (8/28/2013)
 #4 (10/2/2013)
 
 Volume 2: Lift(7/30/2014)ISBN 9781607068716
 #5 (12/11/2013)
 Members of the waste class are given the opportunity to be tested for "lifting", and applicants with suitable skills or abilities become serfs. Two youths, Michael Barrett and Casey Solomon, are lifted to become a doctor and soldier respectively. During the lift event in Denver that is attended by thousands, a resistance group who hate the Carlyle family plan to set off a bomb. Forever discovers the plot, but Casey is instrumental in stopping it. Meanwhile, Jonah is captured by the Hock family, a Carlyle rival and ruler of Eastern North America.
 #6 (2/5/2014)
 #7 (3/19/2014)
 #8 (4/23/2014)
 #9 (7/2/2014)
 
 Second Collection(5/18/16)ISBN 9781632157225
 Volume 3: Conclave(3/18/15)ISBN 9781632152251
 #10 (8/6/2014)
 When Malcolm learns about Jonah's imprisonment by Hock, he calls for a conclave between the families to settle the dispute. Adhering to the Macau Accords that divided the world amongst the families, it takes place within the territory of a neutral family and all the families and their Lazari are present. Forever attends a friendly poker game with the other Lazari and begins a romantic relationship with Joacquim. Malcolm orders Forever to secretly locate Jonah and kill him, but she helps him escape instead. When the conclave begins, Hock invokes a rule allowing his honor to be defended in combat between Forever and another Lazari. Since Hock does not have a Lazarus of his own, he selects Sonia Bittner, whose family is an ally of his. Despite their friendship, Forever and Sonia battle fiercely. Forever wins, and Malcolm spares Sonia's life. Hock refuses to accept the results and spits poison into Malcolm's face. As the poison begins to take effect, Hock and his allies leave.
 #11 (9/10/14)
 #12 (10/22/14)
 #13 (11/26/14)
 #14 (1/14/15)
 #15 (2/18/15)
 
 Volume 4: Poison(1/27/16)ISBN 9781632155238
 #16 (4/22/15)
 War breaks out along the Carlyle/Hock border, and Forever joins Casey Solomon's squad in an effort to control Duluth, Minnesota. During the battle, Forever is incapacitated and Casey leads the squad to complete the mission. Meanwhile, Michael Barrett works with Beth Carlyle and her partner James to counteract the poison in Malcolm's system, which has left him in a coma. Michael makes the key deduction needed to create an antidote, having realized the poison was designed to attack Carlyle-specific parts of Malcolm's genome, although Malcolm is still bedridden. During this time, Stephen leads the family and the war effort, although he and others are aware he is unfit for the task. Johanna schemes to replace him peacefully. Sonia Bittner, after her defeat at the conclave, was left under the control of the Carlyles. She is kept at a training facility where she accidentally discovers a younger clone of Forever. Sonia learns Forever is also a clone; the seventh one to serve as the Carlyle Lazarus. Neither of the clones is aware of the other, and Sonia is sworn to secrecy.
 #17 (6/17/15)
 #18 (7/29/15)
 #19 (9/2/15)
 #20 (11/4/15)
 #21 (12/30/15)
 
 Third Collection(11/9/19)ISBN 9781534313347
 Volume 5: Cull(5/31/17)ISBN 9781534300248
 #22 (6/15/16)
 The war between Carlyle and Hock grows to include their allies, and Sonia and Joacquim are deployed to fight for Carlyle in Europe. Healed from the wounds she received in Duluth, Forever joins them. During a battle with the Vassalovka family Lazarus, Morray betrays Carlyle and use the cybernetic implants in Joacquim to force him to fight Sonia and Forever against his will. Unable to win, Forever and Sonia retreat. Meanwhile, Stephen transfers power of the Carlyle family to Johanna with Malcolm's consent.
 #23 (7/20/16)
 #24 (8/31/16)
 #25 (10/12/16)
 #26 (3/29/17)
 
 X+66(4/11/18)ISBN 9781534304888
 X+66 #1 (7/17/17)
 A series of stand-alone issues focusing on supporting characters. Each issue was illustrated by a different artist.
 X+66 #2 (8/23/17)
 X+66 #3 (9/27/17)
 X+66 #4 (11/1/17)
 X+66 #5 (11/29/17)
 X+66 #6 (2/14/18)
 
 N/A
 Fracture
 #27 (4/18/18)
 A flashback shows Jonah being rescued by a fishing boat in Bittner territory. He is nursed to health by the captain's family and gives them a false name. He falls in love with the captain's daughter and they have a child together. During the war, a disease kills everyone in his new family, but Jonah is unaffected by it.
 #28 (5/30/18)
 Risen #1 (3/20/19)
 Risen #2 (7/24/19)
 Risen #3 (11/27/19)
 
 Sourcebook Collection Volume One(4/25/18)ISBN 9781534305151
 #1 (4/20/16)
 Background information on the Carlyle family
 #2 (4/26/17)
 Background information on the Hock family
 #3 (2/21/18)
 Background information on the Vassalovka family
 