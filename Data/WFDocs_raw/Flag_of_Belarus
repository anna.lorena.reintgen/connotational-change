The national flag of Belarus (Belarusian: Сцяг Беларусі, Sciah Biełarusi; Russian: Флаг Белоруссии, Flag Belorussii) is a red and green flag with a white and red ornament pattern placed at the staff (hoist) end. The current design was introduced in 2012 by the State Committee for Standardisation of the Republic of Belarus, and is adapted from a design approved in a referendum in May 1995. It is a modification of the 1951 flag used while the country was a republic of the Soviet Union. Changes made to the Soviet-era flag were the removal of symbols of communism (such as the hammer and sickle and the red star) and the reversal of the colours of the ornament pattern, from white-on-red to red-on-white. Since the 1995 referendum, several flags used by Belarusian government officials and agencies have been modelled on this national flag.
 This design replaced the historical white-red-white flag used by the Belarusian Democratic Republic in 1918, before Belarus became a Soviet Republic, and again after it regained its independence in 1991. Opposition groups have continued to use this flag, though its display in Belarus has been restricted by the government of Belarus, which claims it was linked with Nazi collaboration during the Second World War. The white-red-white flag is used in protests against the government and by the Belarusian diaspora.
 The basic design of the national flag of Belarus was first described in Presidential Decree No.214 of 7 June 1995. The flag is a rectangular cloth consisting of two horizontal stripes: a red upper stripe covering two-thirds of the flag's height, and green lower stripe covering one-third. A vertical red-on-white Belarusian decorative pattern, which occupies one-ninth of the flag's length, is placed against the flagstaff. The flag's ratio of width to length is 1:2.[2]
 The flag does not differ significantly from the flag of the Byelorussian SSR, other than the removal of the hammer and sickle and the red star, and the reversal of red and white in the hoist pattern.[4] While there is no official interpretation for the colours of the flag, an explanation given by President Alyaksandr Lukashenka is that red represents freedom and the sacrifice of the nation's forefathers, while green represents life.[5]
 In addition to the 1995 decree, "STB 911–2008: National Flag of the Republic of Belarus" was published by the State Committee for Standardisation of the Republic of Belarus in 2008. It gives the technical specifications of the national flag, such as the details of the colours and the ornament pattern. The red ornament design on the national flag was, until 2012, ​1⁄12 the width of the flag, and ​1⁄9 with the white margin. As of 2012, the red pattern has occupied the whole of the white margin (which stayed at ​1⁄9).[1]
 The colours of the national flag are regulated in "STB 911–2008: National Flag of the Republic of Belarus" and are listed in the CIE Standard illuminant D65.[1]
 A decorative pattern, designed in 1917 by Matrona Markevich, is displayed on the hoist of the flag (as it was previously, on the 1951 flag).[6] The pattern, derived from local plants and flowers, is a traditional type commonly used in Belarus. These patterns are sometimes used in woven garments, most importantly in the traditional ruchnik, a woven cloth used for ceremonial events like religious services, funerals, and other more mundane social functions, such as a host offering guests bread and salt served on a rushnyk.[7]
 The husband of Matrona Markevich was arrested for anti-Soviet propaganda and executed during Soviet repression in Belarus in 1937, after which the family was dekulakised. The original ruchnik has not survived and was either confiscated by the NKVD in 1937 or destroyed during the Second World War. The brother of Matrona Markevich, Mikhail Katsar, head of the ethnography and folklore department at the Academy of Sciences of Belarus, was included into the commission that was ordered to create a new flag for the Belarusian SSR in 1951.[8][9]
 A monument to Matrona Markevich was erected in Sianno in 2015.[8]
 By law, the Belarusian flag should be flown daily, weather permitting, from the following locations:
 The Belarusian flag is also officially flown on the sites of special occasions:
 Belarusian diplomats and various government officials (such as the President and the Prime Minister) display the flag on vehicles. The law allows for the flag to be used for special occasions, such as memorial services and family holidays, and it can be used by various groups of people, such as public organisations, companies, and NGOs. The regulations were issued in the same decree that defined the Belarusian flag. 15 May has been declared the Day of the National Emblem and Flag of Belarus (Belarusian: Дзень дзяржаўнага гербу і дзяржаўнага сцягу Рэспублікі Беларусь, Russian: День Государственного герба и Государственного флага Республики Беларусь).[11] The national flag itself has been incorporated into the badge of the guard units in the Belarusian armed forces.[12] The pole should be three times longer than the width of the flag.[13]
 According to the 1995 presidential decree, the national flag is to be used on a staff that is coloured gold (ochre).[2] Other parts of the protocol specify the finial (the metal ornament on a flag pole) as diamond-shaped and coloured in a yellow metal. In this diamond there is a five-pointed star (similar to that used in national emblem).[14] The diamond-pattern represents another continuation of Soviet flag-traditions.[15]
 The flag of the Byelorussian Soviet Socialist Republic was adopted by decree on 25 December 1951.[16] The flag was slightly modified in 1956 when construction details were added for the red star, and golden hammer and sickle. The final specifications of the flag was set in Article 120 of the Constitution of the Byelorussian SSR and are very similar to the current Belarusian flag. The flag had a length-to-width ratio of one to two (1:2), just like the flag of the Soviet Union (and the other fourteen union republics).[17] The main portion of the flag was red (representing the Revolution), with the rest being green (representing the Belarusian forests). A pattern of white drawn on red decorated the hoist portion of the flag; this design is often used on Belarusian traditional costumes. In the upper corner of the flag, in the red portion, a gold hammer and sickle was added, with a red star outlined in gold above it. The hammer represented the worker, and the sickle the peasant; according to Soviet ideology, these two symbols crossed together symbolised co-operation between the two classes. The red star, a symbol commonly used by Communist parties, was said to stand either for the five social groups (workers, youth, peasants, military, and academics), the five known continents, or the five fingers of the worker's hand. The hammer, sickle and star were sometimes not displayed on the reverse of the flag. The purpose for this design was that the Byelorussian SSR, along with the Soviet Union and the Ukrainian SSR, were admitted to the United Nations in 1945 as founding members and needed distinct flags for each other. The designer of the flag was Milkahil Gusyev.[6]
 Before 1951, several different flags had been in use since the Revolution. The earliest flag was plain red, and was used in 1919 during the existence of the Lithuanian-Byelorussian SSR. After the formation of the Byelorussian SSR, the lettering ССРБ (SSRB) was added in gold to the top hoist. This design was established with the passage of the first Constitution of the Byelorussian SSR.[18] It was later modified in the 1927 Constitution where the letters were changed to БССР (BSSR) but kept the overall design the same.[19] This design was changed in 1937, when a hammer and sickle and red star were placed above the letters. The flag dimensions were also formally established as 1:2 for the first time.[20] This flag remained in use until the adoption of the 1951 flag, which did away with the letters.
 Flag of the Lithuanian-Byelorussian SSR.svg
1919
 Flag of the Byelorussian Soviet Socialist Republic (1919-1927).svg
1919–1927
 Flag of the Byelorussian Soviet Socialist Republic (1927-1937).svg
1927–1937
 Flag of the Byelorussian Soviet Socialist Republic (1937-1951).svg
1937–1951
 The design of the flag used between 19 September 1991 and 5 June 1995 had originally been devised by the Belarusian Democratic Republic (March to December 1918).[21] The original person behind the design of the flag is believed to have been Klaudzi Duzh-Dusheuski before 1917 and this design is known in Belarusian as the byel-chyrvona-byely s'tsyah (Бел-чырвона-белы сьцяг; literally "white-red-white flag").[22] Red and white have traditionally been used in state heraldry of the Grand Duchy of Lithuania and the Polish–Lithuanian Commonwealth. The colours are also based on those of the coat of arms Pahonia that was a traditional coat of arms of Belarusian lands and had a white horseman on a red background.[23] There are several other theories explaining the flag's origin. One theory speaks of an allusion to the name of the country: White Ruthenia.[24]
 Between 1921 and 1939 the white-red-white flag was used by the Belarusian national movement in West Belarus (part of the Second Polish Republic), both by political organisations like the Belarusian Peasants' and Workers' Union or the Belarusian Christian Democracy, and non-political organisations like the Belarusian Schools Society.[25] The flag was also used by the Belarusian Special Battalion in the army of the Republic of Lithuania. After the invasion of Poland and the annexation of modern-day West Belarus in 1939 the flag was forbidden by the Soviet administration in the newly acquired territories as well.[24]
 In 1941 the flag was allowed for usage by the Nazi occupation administration, and it appeared on arm patches of Belarusian volunteers in the German Army and Waffen SS (same as the Russian tricolour) and was used by the Belarusian Central Rada, the anti-Soviet government of Belarus in 1943–1944. After the end of World War II the flag was used by Belarusian diaspora in the West and by small groups of anti-Soviet resistance in Belarus itself. In late 1980s the flag was again used as a symbol of national revival and democratic changes in Belarus. By proposal of the Belarusian Popular Front the flag became state symbol of Belarus upon its regaining of independence in 1991.[24]
 After 1995 the white-red-white flag is used as a symbol of the opposition to the regime of Alexander Lukashenko, most notably at the protests after the 2006 and 2010 presidential elections and at mass rallies on Freedom Day celebrations as well as Dziady memorial marches. The flag is not officially banned from public usage, but is treated by the authorities as an unregistered symbol which means that demonstration of it by political activists or sports fans can lead to arrests and confiscation of the flags.[26][27] In early 2010, the political activist Siarhei Kavalenka was arrested for placing the white-red-white flag atop a Christmas tree on the central square of Vitsebsk. The court gave Kavalenka three years of suspended sentence which was followed by a second arrest and Kavalenka's several weeks long hunger strike. The hunger strike was interrupted by force-feeding on 16 January 2012.[28]
 The referendum that was held to adopt the state symbols took place on 14 May 1995. With a voter turnout of 64.7%, the new flag was approved by a majority in the ratio of three to one (75.1% to 24.9%). The other three questions were also passed by the voters.[29] The way of carrying out the referendum as well as the legality of questioning the national symbols on a referendum was heavily criticised by the opposition.[30][31] Opposition parties claimed that only 48.7% of the entire voting population (75.1% of the 64.7% who showed at the polling stations) supported the adoption of the new flag, but Belarusian law (as in many other countries) states that only a majority of voters is needed to decide on a referendum issue.[32][33] Upon the results going in favor of President Lukashenko, he proclaimed that the return of the Soviet-style flag brought a sense of youth and pleasant memories to the nation.[34]
 Lukashenko had tried to hold a similar referendum before, in 1993, but failed to get parliamentary support. Two months before the May 1995 referendum, Lukashenko proposed a flag design that consisted of two small bars of green and one wide bar of red. While it is not known what became of this suggestion, new designs (called "projects" in Belarus) were suggested a few days later, which were then put up to vote in the 1995 referendum.[35]
 Since the introduction of the 1995 flag, several other flags adopted by government agencies or bodies have been modelled on it.
 The presidential standard, which has been in use since 1997, was adopted by a decree called "Concerning the Standard of the President of Republic of Belarus". The standard's design is an exact copy of the national flag, with the addition of the Belarusian national emblem in gold and red. The standard's ratio of 5:6 differs from that of the national flag, making the standard almost square. It is used at buildings and on vehicles to denote the presence of the president.[36]
 In 2001, President Lukashenko issued a decree granting a flag to the Armed Forces of Belarus. The flag, which has a ratio of 1:1.7, has the national ornamental pattern along the length of the hoist side of the flag. On the front of the flag is the Belarusian coat of arms, with the wording УЗБРОЕНЫЯ СІЛЫ ("Armed Forces") arched over it, and РЭСПУБЛІКІ БЕЛАРУСЬ ("of Republic of Belarus") written below; the text of both is in gold. On the reverse of the flag, the centre contains the symbol of the armed forces, which is a red star surrounded by a wreath of oak and laurel. Above the symbol is the phrase ЗА НАШУ РАДЗІМУ ("For our Motherland"), while below is the full name of the military unit.[37]
 
 1 Design

1.1 Colours
1.2 Hoist ornament pattern

 1.1 Colours 1.2 Hoist ornament pattern 2 Flag protocol 3 Historical flags

3.1 Soviet flag of 1951
3.2 Previous flags of the Soviet era
3.3 White-red-white flag

3.3.1 1995 referendum



 3.1 Soviet flag of 1951 3.2 Previous flags of the Soviet era 3.3 White-red-white flag

3.3.1 1995 referendum

 3.3.1 1995 referendum 4 Other related flags 5 See also 6 References 7 External links the National Assembly of Belarus the Council of Ministers of Belarus courts of Belarus, and offices of local executive and administrative bodies above buildings in which sessions of local Councils of deputies take place military bases or military ships owned by the government buildings used by Belarusian diplomats sessions of local executive and administrative bodies voting/polling places sports arenas during competitions (note that the IOC has its own rules on flag display) [10] 


Flag of the Lithuanian-Byelorussian SSR.svg
1919


 


Flag of the Byelorussian Soviet Socialist Republic (1919-1927).svg
1919–1927


 


Flag of the Byelorussian Soviet Socialist Republic (1927-1937).svg
1927–1937


 


Flag of the Byelorussian Soviet Socialist Republic (1937-1951).svg
1937–1951


 List of Belarusian flags National emblem of Belarus Flag of the Byelorussian Soviet Socialist Republic Emblem of the Byelorussian Soviet Socialist Republic ^ a b c d e СТБ 911-2008 Государственный флаг Республики Беларусь. Общие технические условия [STB 911–2008: National Flag of the Republic of Belarus. Technical Specifications.] (in Russian). State Committee for Standardization of the Republic of Belarus. 2008. Retrieved 2010-08-05..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c "Указ Президента Республики Беларусь Об утверждении Положения о Государственном флаге Республики Беларусь | Геральдика.ру". Geraldika.ru. Retrieved 2012-08-15.
 ^ "- The Washington Post". Washington Post.
 ^ "National Symbols" page on the official website of the President of Belarus
 ^ "The official internet-portal of the President of the Republic of Belarus/2010". President.gov.by. 1998-05-11. Archived from the original on 2010-12-22. Retrieved 2012-08-26.
 ^ a b Басаў А. Н., Куркоў І. М. Флагі Беларусі ўчора і сёння / Пер. А. Н. Найдовіч. — Мн.: Полымя, 1994. С. 24.
 ^ "Belarusian Textiles" and "Belarusian Ruchnik" pages on the Virtual Guide to Belarus website
 ^ a b "У Сянне адкрылі помнік жанчыне, якая вышыла арнамент з дзяржаўнага сцяга" (in Belarusian). Budzma. 6 July 2015. Retrieved 20 January 2018.
 ^ "Адкрыты спіс - Маркевіч Аляксей Захаравіч (1885)" (in Belarusian). Openlist. Retrieved 20 January 2018.
 ^ Flag Manual. Beijing, China: Beijing Organizing Committee for the Games of the XXIX Olympiad – Protocol Division; 2008. p. 4.
 ^ (in Russian) BelTA's page about the national flag day Archived 2006-10-11 at the Wayback Machine
 ^ (in Russian) Badges of the Armed Forces of Belarus Archived 2007-03-11 at the Wayback Machine Note: Works in Internet Explorer only
 ^ Государственные символы Республики Беларусь (in Russian). Национальный центр правовой информации Республики Беларусь. Retrieved 2010-04-30.
 ^ "Embassy of the Republic of Belarus in Korea — Republic of Belarus — National Symbols". Korea.mfa.gov.by. 2008-12-20. Archived from the original on 2012-11-29. Retrieved 2012-08-15.
 ^ 
"ГОРДО РЕЮТ БОЕВЫЕ СТЯГИ!. РВО, № 9 (44) сентябрь 2007". Grinchevskiy.ru. Retrieved 2012-08-15.
 ^  Указ Президиума ВС БССР от 25.12.1951 О государственном флаге Белорусской ССР (in Russian) on the Russian Wikisource ("The decree of the Presidium of the Supreme Belorussian 25.12.1951 on the state flag of the Byelorussian SSR")
 ^  Указ Президиума ВС БССР от 8.05.1956 об утверждении Положения о Государственном флаге Белорусской ССР (in Russian) on the Russian Wikisource ("The decree of the Presidium of the Supreme Belorussian 05.08.1956 approving the Regulation on the State Flag of the Byelorussian SSR")
 ^ Article 32, КОНСТИТУЦИЯ СОЦИАЛИСТИЧЕСКОЙ СОВЕТСКОЙ РЕСПУБЛИКИ БЕЛОРУССИИ (1919) Archived 2010-09-27 at the Wayback Machine. Pravo.by. Retrieved on 2011-05-27.
 ^ Article 75, Конституции (Основного Закона) Белорусской Социалистической Советской Республики (1927) Archived 2010-11-29 at the Wayback Machine. Pravo.by. Retrieved on 2011-05-27.
 ^ Конституции Беларуси – Национальный правовой портал Республики Беларусь Archived 2010-09-29 at the Wayback Machine. Pravo.by. Retrieved on 2011-05-27.
 ^ Understanding Belarus and How Western Foreign Policy Misses the Mark — Grigoriĭ Viktorovich Ioffe — Google Books. Books.google.com. Retrieved 2012-08-26.
 ^ "Наша Ніва". Nn.by. Archived from the original on 2012-02-26. Retrieved 2012-08-26.
 ^ Belarus: The Last European Dictatorship — Andrew Wilson — Google Books. Books.google.com. Retrieved 2012-08-26.
 ^ a b c "Пытаньне дзяржаўнай сымболікі ў Беларусі: гісторыя і сучасны стан". Pahonia-plakat.narod.ru. Retrieved 2012-08-26.
 ^ (in Belarusian)Андрэй Вашкевіч. Нашы сьцягі над Заходняй // Arche, №4 (55) – 2007 Archived 2008-11-21 at the Wayback Machine
 ^ "Polskie Radio Esperanto — Затрыманьні на рыцарскім фэсьце". .polskieradio.pl. Retrieved 2012-06-28.
 ^ Congressional Record — Google Books. Books.google.com. Retrieved 2012-08-26.
 ^ World Report 2011 - Human Rights Watch — Google Books. Books.google.com. Retrieved 2012-08-26.
 ^ "Internet Archive Wayback Machine". Web.archive.org. 2011-07-20. Archived from the original on 2011-07-20. Retrieved 2012-08-15.
 ^ United Nations High Commissioner for Refugees. "Refworld | Chronology for Poles in Belarus". UNHCR. Archived from the original on 2013-04-16. Retrieved 2012-08-15.
 ^ One Europe, Many Nations: A Historical Dictionary of European National Groups — James Minahan — Google Books. Books.google.com. Retrieved 2012-08-15.
 ^ Belarus: A Denationalized Nation — David Marples — Google Books. Books.google.com. Retrieved 2012-08-15.
 ^ "Electoral Code Of The Republic Of Belarus*". Ncpi.gov.by. Archived from the original on 2011-08-23. Retrieved 2012-08-15.
 ^ Nationalisms Today — Tomasz Kamusella, Krzysztof Jaskułowski — Google Books. Books.google.com. 2009-06-15. Retrieved 2012-08-26.
 ^ (in Russian) Vexillographia page "Государственный флаг Республики Беларусь"
 ^ (in Russian) Decree dated March 27, 1997, creating the presidential standard
 ^ Flags of the World page "Belarus – Military Flags" Archived 2005-11-22 at the Wayback Machine, and (in Russian) Vexillographia page "Флаги армии Беларуси"
 Belarus at Flags of the World Vexillographia – Flags of Belarus (in Russian) Official site of the President of the Republic of Belarus – Official description of the National Symbols v t e Flag Emblem Anthem Presidential symbols
Flag Flag Belarus portal Cross of Saint Euphrosyne White-red-white flag Pahonia Long Live Belarus! Vyshyvanka European bison White stork Cornflower Potato Vajacki marš Belarusian Marseillaise Rahvalod Vseslav of Polotsk Euphrosyne of Polotsk Vitaut Francysk Skaryna Lew Sapieha Tadeusz Kościuszko Kastus Kalinouski Yanka Kupala Yakub Kolas Maksim Bahdanovič Belavezhskaya Pushcha Saint Sophia Cathedral Mir Castle Niasvizh Castle v t e Albania Andorra Armenia Austria Azerbaijan Belarus Belgium Bosnia and Herzegovina Bulgaria Croatia Cyprus Czech Republic Denmark Estonia Finland France Georgia Germany Greece Hungary Iceland Ireland  Italy Kazakhstan Latvia Liechtenstein Lithuania Luxembourg Malta Moldova Monaco Montenegro Netherlands North Macedonia Norway Poland Portugal Romania Russia San Marino Serbia Slovakia Slovenia Spain Sweden Switzerland Turkey Ukraine United Kingdom
England
Northern Ireland
Scotland
Wales England Northern Ireland Scotland Wales Vatican City Abkhazia Artsakh Kosovo Northern Cyprus South Ossetia Transnistria Åland Faroe Islands Gibraltar Guernsey Isle of Man Jersey Svalbard European Union Sovereign Military Order of Malta v t e Sovereign states Dependent territories Timeline Sovereign states Dependent territories Flags of Belarus National flags National symbols of Belarus Flags introduced in 1995 Flags introduced in 2012 CS1 uses Russian-language script (ru) CS1 Russian-language sources (ru) CS1 Belarusian-language sources (be) Articles with Russian-language external links Webarchive template wayback links Articles with Belarusian-language external links Articles containing Belarusian-language text Articles containing Russian-language text Commons category link is on Wikidata Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version አማርኛ العربية Azərbaycanca Беларуская Беларуская (тарашкевіца)‎ Български Bosanski Català Čeština Cymraeg Dansk Deutsch Eesti Ελληνικά Español Esperanto Euskara فارسی Français Galego ગુજરાતી 한국어 Հայերեն हिन्दी Hrvatski বিষ্ণুপ্রিয়া মণিপুরী Bahasa Indonesia Italiano עברית ქართული Latviešu Lietuvių Lumbaart Magyar Македонски മലയാളം მარგალური Nederlands 日本語 Norsk Polski Português Română Русский Scots Shqip Sicilianu Simple English Slovenčina Српски / srpski Srpskohrvatski / српскохрватски Suomi Svenska Tagalog ไทย Türkçe Українська Tiếng Việt Yorùbá 中文  This page was last edited on 1 November 2019, at 19:20 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  flag of Belarus a b c d e a b c ^ ^ ^ a b ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b c ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ Flag of Belarus National flag of Belarus 
Standard Colour Sample of the National Flag[1]


Colour

Colour coordinate

Y10


x10
y10


Red

0.553 ± 0.010
0.318 ± 0.010
14.8 ± 1.0


Green

0.297 ± 0.010
0.481 ± 0.010
29.6 ± 1.0
 
Standard Colour Sample of the National Flag[1]


Colour

Colour coordinate

Y10


x10
y10


Red

0.553 ± 0.010
0.318 ± 0.010
14.8 ± 1.0


Green

0.297 ± 0.010
0.481 ± 0.010
29.6 ± 1.0
   National flag and ensign 1:2[1] 7 June 1995 (original design with a thinner ornament pattern)[2]10 February 2012 (current (above) design with a thicker ornament pattern)[1] A horizontal bicolour of red over green in a 2:1 ratio, with a red ornamental pattern on a white vertical stripe at the hoist. Mikhail Husyev 
  Variant flag of  Belarus Presidential Standard The national flag shortened to 5:6 ratio with the national emblem in gold. 
  Variant flag of  Belarus Бел-чырвона-белы сцяг ("The White Red and White Flag") 1918 (Belarusian Democratic Republic and Belarusian government in exile),Unofficially in West Belarus until 1939,unofficially between 1942 and 1944 (during German occupation),Officially from 1991 to 1995Still often used by government opposition groups.[3] 1:2 19189 September 1991 A horizontal triband of white, red and white Kłaŭdzi Duž-Dušeŭski 
 0.553 ± 0.010 0.318 ± 0.010 14.8 ± 1.0
 0.297 ± 0.010 0.481 ± 0.010 29.6 ± 1.0
  Wikisource has original text related to this article:
Flag of Belarus  Wikimedia Commons has media related to National flag of Belarus.  
Flag
Emblem
Anthem
Presidential symbols
Flag
 

Belarus portal 
Cross of Saint Euphrosyne
White-red-white flag
Pahonia
Long Live Belarus!
Vyshyvanka
European bison
White stork
Cornflower
Potato
 
Vajacki marš
Belarusian Marseillaise
 
Rahvalod
Vseslav of Polotsk
Euphrosyne of Polotsk
Vitaut
Francysk Skaryna
Lew Sapieha
Tadeusz Kościuszko
Kastus Kalinouski
Yanka Kupala
Yakub Kolas
Maksim Bahdanovič
 
Belavezhskaya Pushcha
Saint Sophia Cathedral
Mir Castle
Niasvizh Castle
 
Albania
Andorra
Armenia
Austria
Azerbaijan
Belarus
Belgium
Bosnia and Herzegovina
Bulgaria
Croatia
Cyprus
Czech Republic
Denmark
Estonia
Finland
France
Georgia
Germany
Greece
Hungary
Iceland
Ireland

Italy
Kazakhstan
Latvia
Liechtenstein
Lithuania
Luxembourg
Malta
Moldova
Monaco
Montenegro
Netherlands
North Macedonia
Norway
Poland
Portugal
Romania
Russia
San Marino
Serbia
Slovakia
Slovenia
Spain
Sweden
Switzerland
Turkey
Ukraine
United Kingdom
England
Northern Ireland
Scotland
Wales
Vatican City
  
Abkhazia
Artsakh
Kosovo
Northern Cyprus
South Ossetia
Transnistria
 
Åland
Faroe Islands
Gibraltar
Guernsey
Isle of Man
Jersey
Svalbard
 
European Union
Sovereign Military Order of Malta
 
Sovereign states
Dependent territories
Timeline
 
Sovereign states
Dependent territories
  Heraldry portal 