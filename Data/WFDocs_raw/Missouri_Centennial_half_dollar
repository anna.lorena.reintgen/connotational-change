
 The Missouri Centennial half dollar is a commemorative fifty-cent piece struck by the United States Bureau of the Mint in 1921. It was designed by Robert Ingersoll Aitken. The U.S. state of Missouri wanted a commemorative coin to mark its centennial that year. Legislation for such a coin passed through Congress without opposition, and was signed on March 4, 1921 by President Warren G. Harding on his inauguration day. The federal Commission of Fine Arts hired Aitken to design the coin, which depicted Daniel Boone on both sides.  The reverse design, showing Boone with a Native American, was likely intended to symbolize the displacement of the Indians by white settlers.
 To increase sales, a portion of the issue was produced with the mark 2★4, symbolic of Missouri being the 24th state. Although admired for the design, the coins did not sell as well as hoped, and almost 60 percent were returned to the Philadelphia Mint for melting. There are fewer coins with 2★4 than without, but they remain near-equal in value.
 The State of Missouri, admitted to the Union in 1821 as part of the Missouri Compromise, wanted a commemorative coin for sale at the Missouri Centennial Exposition and State Fair, to be held in Sedalia from August 8 to 20, 1921.[1] Legislation for such a half dollar was introduced in the Senate by that state's Selden P. Spencer on January 20, 1921, the bill being designated S. 4893.[2][3] It was referred to the Committee on Banking and Currency.[3] The bill was reported to the full Senate by Connecticut's George P. McLean on January 25 with an amendment reducing the authorized mintage from 500,000 to 250,000 and a recommendation that it pass. Vice President Thomas R. Marshall, who was presiding over the Senate, asked if there was objection to its passage.  The Minority Leader, Democrat Oscar Underwood of Alabama, said he did not think there would be objection, but that the bill should be read first.  Idaho Senator William E. Borah stated he would object if there was to be discussion of it, as the Senate had not had morning business in ten days. Borah asked McLean if he expected there would be objection to the bill, and McLean reassured the Idahoan. The bill, as amended, passed the Senate without recorded opposition.[4]
 An identical bill, H. R. 15767,[5] had been introduced in the House of Representatives on January 17, 1921.[2] On February 10, Indiana Representative Albert Vestal, chairman of the House Committee on Coinage, Weights and Measures (to which the bill had been referred) reported it back to the House with an amendment identical to that approved by the Senate, and with a recommendation that the bill pass.[6] S. 4983 had been referred to that committee after its receipt by the House, and on February 24 the Coinage Committee recommended the Senate bill pass the House, noting that the two bills were identical.[5]
 S. 4893 was considered by the House of Representatives on March 2, 1921, with two days remaining in the congressional session. Ohio's Warren Gard had asked  questions about previous coin bills when they passed through the House.[7] Gard enquired of Vestal whether the Missouri bill was identical to the earlier bills, and whether it contained the same safeguards, and Vestal confirmed these. The bill was passed by the House without recorded objection,[8] and was enacted into law with the signature of the new president, Warren G. Harding, on his inauguration day, March 4, 1921.[2][9]
 While the legislation was still pending in Congress, on January 29, 1921, Senator Spencer wrote to Charles Moore, chairman of the Commission of Fine Arts, regarding the bill.  Moore replied on February 2, advising that one of the few American sculptors capable of designing a coin be hired, and that the resulting work be subjected to the friendly criticism of other artists.  Spencer put James Montgomery, chairman of the Missouri Centennial Commission, in touch with Moore, and on February 9, Moore made several proposals for the design, including a "prairie-schooner ... second, the heads of [pioneer Daniel] Boone and [early Missouri politician Thomas Hart] Benton; third, the state seal of Missouri; and fourth, the Indian pointing westward".[10] The hired sculptor could choose among these concepts, Moore suggested, and decide which could be used to best effect, with Montgomery's approval and that of the Fine Arts Commission. Montgomery replied on February 16, proposing a "standing figure of Daniel Boone, coon skin cap, deer skin clothes, with an Indian sitting at his feet, the Missouri River flowing in front, with the bluffs on opposite side, with one star with the figures '24' in center of star. This would signify first, that the white man had supplanted the Indian in Missouri Territory."[11]
 Montgomery also proposed that a star with the number "24" (Missouri being the 24th state) be included in the design, but be removed from the die after 5,000 coins were struck.  These would boost proceeds, helping to pay the sculptor's fee and the cost of the die, an expense estimated at $1,750.[12] Montgomery wrote again on March 22, proposing that the words "MISSOURI CENTENNIAL, SEDALIA, 1921" appear on the coins, and if possible the centennial date of August 10 as well.[12]
 The Fine Arts Commission hired Robert Ingersoll Aitken, a sculptor best known in numismatic circles for his design for the $50 Panama Pacific gold pieces.[1] This was apparently not communicated to the Missouri commission as on May 17, James Earle Fraser, a member of the Fine Arts Commission, told Moore that he had heard that someone associated with the Missouri Centennial had made inquiries with the Medallic Art Company of New York, aimed at hiring a sculptor.[12]  Whether Moore wrote to Montgomery is unclear, but he wrote to Spencer on May 26, advising the senator that the designs had been preliminarily approved.  Commission members had suggested several changes; these were implemented and, on June 9, the Fine Arts Commission approved the design, a decision communicated by letter that day to the Director of the Mint, Raymond T. Baker.[13] To speed production, the Medallic Art Company produced hubs from which working dies to produce the coins could be made.[14] These hubs were sent to the Philadelphia Mint on June 29, 1921.[15]
 The bust of frontiersman Daniel Boone, who lived in Missouri for the final quarter century of his life, appears on the obverse.[16] Anthony Swiatek and Walter Breen, in their 1988 book on commemorative coins, note speculation that the obverse may have been inspired by Albin Polasek's sculptured bust of Boone in the Hall of Fame for Great Americans in New York City. Boone wears a deerskin jacket and a coonskin cap. The centennial dates, the name of the country and the coin's denomination surround the bust. The reverse depicts Boone with rifle and powder horn, and a Native American—Swiatek and Breen noted that the frontiersman appears to be sending away the Indian, who bears a shield and peace pipe, apparently dramatizing Montgomery's desire to show the white man supplanting the Indian in Missouri; Breen stated "as though this was something to brag about".[17] The 1921 Report of the Director of the Mint describes the interaction as "Daniel Boone, with powder and rifle, directing the attention of an Indian to the westward course of the white man".[16] The Missouri Centennial half dollar, which shows Boone on either side, is one of the few coins in United States numismatic history to have the same individual depicted on both sides; other such depictions include Boone himself on the 1934–1938 Daniel Boone Bicentennial half dollar, Lafayette on the 1900-dated Lafayette dollar and the frontiersman on the 1936 Elgin, Illinois, Centennial half dollar.[18]
 The 24 stars on the reverse convey the same message that the 2★4 on the obverse of some specimens does—that Missouri was the 24th state to enter the Union. The name SEDALIA, the site of the centennial exposition, appears in exergue. Aitken's stylized monogram, RA, is near the rifle butt.[9] The Missouri piece, and the Columbian half dollar of 1892–1893, are the only U.S. commemorative half dollars to bear none of the patriotic mottoes usually found on U.S. coinage, that is LIBERTY, IN GOD WE TRUST and E PLURIBUS UNUM.[19] The design was liked at the time of issue; several specimens were shown and admired at the 1921 American Numismatic Association convention at Boston in August, soon after the coin's release.[20]
 Art historian Cornelius Vermeule, in his volume on U.S. coins and medals, admired Aitken's Missouri piece, writing that Aitken "became the first American medalist to apply the principles of Renaissance medallic design to a coin of the United States and the first such artist to make a frontiersman look like a Medici prince".[21] He suggested that the figures on the reverse stand "like Roman soldiers in an Antonine relief on the Arch of Constantine or Renaissance condottieri in a large fresco of court ceremonials."[21] Mermeule noted that "the lettering on the obverse follows the forms and system of Pisanello" and that "the coin as a whole is a work of art rather than just another way to market a silver fifty-cent piece because all three of the mottoes that usually burden and constrict America's attempts of numismatic art are omitted."[21] The author concluded of Aitken's works, "his imagination in selecting from the past to rephrase the present worked very well in the United States commemorative coinage".[22]
 A total of 50,028 Missouri Centennial half dollars was struck at the Philadelphia Mint in July 1921, 28 being reserved for inspection and testing at the 1922 meeting of the annual Assay Commission.[23] The first coins displayed the 2★4 on the obverse; afterwards, this was ground off the dies and the remainder were struck without it.  The Sedalia Trust Company distributed the coins at a price of $1 on behalf of the centennial commission, selling both varieties by mail and the plain ones at the exposition in August.  There had been little advance publicity, and the exposition took place during the Recession of 1921. All of the 2★4 coins sold, but when sales slowed of the plain variety, 29,600 were returned to the mint for melting.[9][15] Early advertisements for the coin contained sketches prepared by Aitken, showing the obverse as issued, but depicting a reverse bearing the state Seal of Missouri, that Aitken had abandoned when he found it did not work well.[24] Aitken, in a letter published in part in the December 1921 issue of The Numismatist (the journal of the American Numismatic Association) wrote that "The illustration that you published was made from one of several drawings which I submitted to the Federal Art Commission. The Missouri committee was informed that I would work along these lines, though I was given full latitude for any change I might advise. The seal of the State did not work out well, so I developed the reverse with the two standing figures, which met with the instant approval of the Commission in Washington."[24]
 The number of coins bearing the 2★4 is uncertain. Swiatek and Breen wrote that 5,000 were struck and were sold, meaning that 15,400 of the plain ones were issued.[9] Swiatek, in his 2012 volume on commemoratives, adhered to these figures.[15] Coin dealer David Bullowa wrote in 1937 that 10,000 of the 2★4 were issued, with which numismatic writer Q. David Bowers concurred, noting that the two varieties are about equal in rarity.[25] The deluxe edition of R. S. Yeoman's A Guide Book of United States Coins, published in 2018, estimates that 9,400 of the 2★4 were issued, and 11,400 of the plain.[26] Both varieties sold at a premium above the issue price by 1925, and at the height of the first commemorative coin boom in 1936, the 2★4 sold for $25 and the plain for $28.  By 1940, the 2★4 sold for more than the plain, and at the height of the second boom in 1980, the 2★4 brought $2,600 to the plain's $2,400. Yeoman's book lists the 2★4 for between $625 and $7,250, and the plain for between $400 and $6,650, each depending on condition.[27] An exceptional specimen of the plain variety sold at auction in 2015 for $70,500.[28] At least one specimen in proof condition is known, sold at auction in 1992.[29]
 90.0% silver 10.0% copper 1 Legislation 2 Preparation 3 Design 4 Production, distribution, and collecting 5 References 6 Sources 7 External links ^ a b Bowers, p. 153.
 ^ a b c "Missouri 100th Anniversary 50-Cent Piece". ProQuest Congressional. Retrieved March 6, 2017..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b "Missouri 100th Anniversary 50-Cent Piece". United States Senate.
 ^ 1921 Congressional Record, Vol. 67, Page 9995 (January 25, 1921)
 ^ a b "Coinage of 50-Cent Piece in Commemoration of Admission of Missouri into the Union". United States House of Representatives. February 24, 1921.
 ^ "Coinage of 50-Cent Piece in Commemoration of Admission of Missouri into the Union". United States House of Representatives. February 10, 1921.
 ^ 1920 Congressional Record, Vol. 66, Page 5947–5950 (April 21, 1920)
 ^ 1921 Congressional Record, Vol. 67, Page 4357 (March 2, 1921)
 ^ a b c d Swiatek & Breen, p. 164.
 ^ Taxay, pp. 52–53.
 ^ Taxay, pp. 53, 56.
 ^ a b c Taxay, p. 56.
 ^ Taxay, pp. 56–58.
 ^ Bowers, p. 154.
 ^ a b c Swiatek, p. 130.
 ^ a b Flynn, p. 123.
 ^ Swiatek & Breen, pp. 163–164.
 ^ Bowers, p. 156.
 ^ Slabaugh, p. 46.
 ^ "The Missouri Centennial Half Dollar a Popular Coin". The Numismatist: 484–485. October 1921.
 ^ a b c Vermeule, p. 162.
 ^ Vermeule, p. 163.
 ^ Flynn, p. 124.
 ^ a b "A 'Variety' of the Missouri Centennial Half Dollar". The Numismatist: 575. December 1921.
 ^ Bowers, pp. 155–156.
 ^ Yeoman, p. 1058.
 ^ Yeoman, p. 1059.
 ^ "1921 Missouri 50C MS". Numismatic Guaranty Corporation. Retrieved July 29, 2018.
 ^ Stevenson, Jed (August 2, 1992). "COINS; Mix-Up and Mystery: The 1921 Missouri Proof". The New York Times. Retrieved December 21, 2016.
 Bowers, Q. David (1992). Commemorative Coins of the United States: A Complete Encyclopedia. Wolfeboro, NH: Bowers and Merena Galleries, Inc. ISBN 978-0-943161-35-8. Flynn, Kevin (2008). The Authoritative Reference on Commemorative Coins 1892–1954. Roswell, GA: Kyle Vick. OCLC 711779330. Slabaugh, Arlie R. (1975). United States Commemorative Coinage (second ed.). Racine, WI: Whitman Publishing. ISBN 978-0-307-09377-6. Swiatek, Anthony (2012). Encyclopedia of the Commemorative Coins of the United States. Chicago, IL: KWS Publishers. ISBN 978-0-9817736-7-4. Swiatek, Anthony; Breen, Walter (1981). The Encyclopedia of United States Silver & Gold Commemorative Coins, 1892 to 1954. New York, NY: Arco Publishing. ISBN 978-0-668-04765-4. Taxay, Don (1967). An Illustrated History of U.S. Commemorative Coinage. New York, NY: Arco Publishing. ISBN 978-0-668-01536-3. Vermeule, Cornelius (1971). Numismatic Art in America. Cambridge, MA: The Belknap Press of Harvard University Press. ISBN 978-0-674-62840-3. Yeoman, R. S. (2018). A Guide Book of United States Coins (Mega Red 4th ed.). Atlanta, GA: Whitman Publishing, LLC. ISBN 978-0-7948-4580-3.  Media related to Missouri Centennial half dollar at Wikimedia Commons v t e 1¢ 5¢ 10¢ 25¢ 50¢ $1 5₥ 1¢ (large size) 2¢ 3¢ (silver) 3¢ (nickel) 5¢ (silver) 20¢ $1 (gold) $2.5 $3 $5 $10 $20 2¢ (billon) 2.5¢ 3¢ (bronze) $2 $4 $50 $100 1800s 1900s 1910s 1920s 1930s 1940s 1950s 1970s 1980s 1990s 2000s 2010s 2020s Silver Eagle (1986–present) Gold Eagle (1986–present) Platinum Eagle (1997–present) Gold Buffalo (2006–present) First Spouse (gold) (2007–2016) Palladium Eagle (2017–present) America the Beautiful (silver) (2010–present) Proof Set (1936–present) Mint Set (1947–present) Special Mint Set (1964–1967) Souvenir Set (1972–1998) Silver Proof Set (1976, 1992–present) Prestige Set (1983–1997) v t e ← 1910s United States commemorative coins (1920s) 1930s → Maine Centennial half dollar Pilgrim Tercentenary half dollar Pilgrim Tercentenary half dollar Missouri Centennial half dollar Alabama Centennial half dollar Peace dollar Grant Memorial coinage Monroe Doctrine Centennial half dollar Huguenot-Walloon half dollar Lexington-Concord Sesquicentennial half dollar Stone Mountain Memorial half dollar California Diamond Jubilee half dollar Fort Vancouver Centennial half dollar United States Sesquicentennial coinage Oregon Trail Memorial half dollar Vermont Sesquicentennial half dollar Oregon Trail Memorial half dollar Hawaii Sesquicentennial half dollar Arts portal Business and economics portal Numismatics portal United States portal Visual arts portal 1921 establishments in the United States Cultural depictions of Daniel Boone Currencies introduced in 1921 Early United States commemorative coins Fifty-cent coins Missouri Native Americans on coins CS1: Julian–Gregorian uncertainty Featured articles Commons category link is on Wikidata Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version  This page was last edited on 11 November 2019, at 23:29 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Missouri Centennial half dollar a b a b c a b ^ a b ^ ^ ^ a b c d ^ ^ a b c ^ ^ a b c a b ^ ^ ^ ^ a b c ^ ^ a b ^ ^ ^ ^ ^ United States 50 cents (0.50 US dollars) 12.5 g 30.61 mm 2.15 mm (0.08 in) Reeded 
90.0% silver
10.0% copper
 0.36169 troy oz 1921 50,028 with 28 pieces for the Assay Commission (29,600 melted) None, all pieces struck at the Philadelphia Mint without mint mark  Bust of Daniel Boone Robert Ingersoll Aitken 1921  Boone with a Native American Robert Ingersoll Aitken 1921 
1¢
5¢
10¢
25¢
50¢
$1
 
5₥
1¢ (large size)
2¢
3¢ (silver)
3¢ (nickel)
5¢ (silver)
20¢
$1 (gold)
$2.5
$3
$5
$10
$20
 
2¢ (billon)
2.5¢
3¢ (bronze)
$2
$4
$50
$100
 
1800s
1900s
1910s
1920s
1930s
1940s
1950s
1970s
1980s
1990s
2000s
2010s
2020s
 
Silver Eagle (1986–present)
Gold Eagle (1986–present)
Platinum Eagle (1997–present)
Gold Buffalo (2006–present)
First Spouse (gold) (2007–2016)
Palladium Eagle (2017–present)
America the Beautiful (silver) (2010–present)
 
Proof Set (1936–present)
Mint Set (1947–present)
Special Mint Set (1964–1967)
Souvenir Set (1972–1998)
Silver Proof Set (1976, 1992–present)
Prestige Set (1983–1997)
 
Maine Centennial half dollar
Pilgrim Tercentenary half dollar
 
Pilgrim Tercentenary half dollar
Missouri Centennial half dollar
Alabama Centennial half dollar
Peace dollar
 
Grant Memorial coinage
 
Monroe Doctrine Centennial half dollar
 
Huguenot-Walloon half dollar
 
Lexington-Concord Sesquicentennial half dollar
Stone Mountain Memorial half dollar
California Diamond Jubilee half dollar
Fort Vancouver Centennial half dollar
 
United States Sesquicentennial coinage
Oregon Trail Memorial half dollar
 
Vermont Sesquicentennial half dollar
 
Oregon Trail Memorial half dollar
Hawaii Sesquicentennial half dollar
 