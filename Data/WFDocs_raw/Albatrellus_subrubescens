
 
 Albatrellus subrubescens is a species of polypore fungus in the family Albatrellaceae. The fruit bodies (mushrooms) of the fungus have whitish to pale buff-colored caps that can reach up to 14.5 cm (5.7 in) in diameter, and stems up to 7 cm (2.8 in) long and 2 cm (0.8 in) thick. On the underside of the caps are tiny light yellow to pale greenish-yellow pores, the site of spore production. When the fruit bodies are fresh, the cap and pores stain yellow where exposed, handled, or bruised.
 The species is found in Asia, Europe, and North America, where it grows on the ground in deciduous or mixed woods, usually in association with pine trees. It is closely related, and physically similar, to the more common Albatrellus ovinus, from which it may be distinguished macroscopically by differences in the color when bruised, and microscopically by the amyloid (staining bluish-black to black with Melzer's reagent) walls of the spores. The fruit bodies of A. subrubescens contain scutigeral, a bioactive chemical that has antibiotic activity. A. subrubescens mushrooms are mildly poisonous, and consuming them will result in a short-term gastrointestinal illness.
 The species was first described as Scutiger subrubescens by American mycologist William Murrill in 1940, based on collections that he found growing under oak near Gainesville, Florida, in November 1938.[4] In 1947 he transferred it to the genus Polyporus.[5] Josiah Lincoln Lowe examined Murrill's type material and thought that it did not differ from Albatrellus confluens.[6] In 1965, Zdeněk Pouzar made collections from Bohemia (now the Czech Republic), and described it as a new species (Albatrellus similis), unaware of the similarity to Murrill's Florida specimens.[7] Further study revealed that A. similis was identical to Murrill's Scutiger subrubescens, and Pouzar transferred the latter epithet to Albatrellus.[8] In 1974, Pouzar recognized that Lowe's species Albatrellus confluens was distinct from A. subrubescens.[9] The specific epithet subrubescens, "tinted reddish", is derived from the Latin words sub ("less than") and rubescens ("growing red").[10]
 A. ovinus
 A. citrinus
 A. subrubescens
 A. tianschanicus
 Four Albatrellus species were included in a large-scale phylogenetic analysis of the order Russulales published in 2003. Based on their ribosomal DNA sequences, the four form a clade, or monophyletic group (that is, they derived from a single ancestor). Of the four tested species, A. ovinus was most closely related to A. subrubescens. The polypore Wrightoporia lenta (type species of the genus Wrightoporia) occurred on a single branch basal to the albatrellus clade, implying that it shared with the Albatrellus species a common ancestor from which both were descended.[12] In a more recent (2010) molecular analysis by Canadian mycologist Serge Audet aimed at clarifying relationships among species formerly placed in Scutiger, A. subrubescens grouped in a clade with A. ovinus and A. citrinus. According to Audet, these species, in addition to A. avellaneus and A. piceiphilus, are the constituents of an Albatrellus with limits defined by molecular genetics. Other Albatrellus species were transferred to segregate genera: A. fletti and A. confluens to Albatrellopsis; A. caeruleoporus and A. yasudae to Neoalbatrellus; A. pes-caprae and A. ellisii to an amended Scutiger.[11]
 The cap of A. subrubescens is between 6 to 14.5 cm (2.4 to 5.7 in) in diameter, with a central, eccentric (away from the center), or rarely lateral (attached to the edge of cap) stem. Initially, the cap is convex with an involute margin, flattening out with age. The cap margin may be folded or flat. The cap surface in young specimens is smooth but soon forms appressed scale-like spots, which may transform into scales in age. Initially, the cap  has white margins and a brownish-violet center with scale-like spots; the center later becomes orange-brownish or ochraceous brown. According to Canadian mycologist James Ginns, who described North American Albatrellus species in 1997, some North American specimens may be covered with blackish-gray to purple-gray fibrils,[13] but this characteristic is not seen in European collections.[14] The cap discolors yellowish when bruised.[13]
 The stem is 1.6 to 7 cm (0.6 to 2.8 in) long and 1 to 2 cm (0.4 to 0.8 in) thick, cylindrical, irregular, and its base may be somewhat pointed, or bulbous. Initially white, the stem develops orange/violet spots and later brownish orange spots; in old specimens the stem may be brownish brick red. The tubes on the pore surface (underside of the cap) are about 2.5–3 mm long and decurrent in attachment. The pores are small, measuring about 2–3 per millimeter. They are initially greenish-white, but later turn dark brown; dried specimens can have pores that are tinted green.[14] Fruit bodies have a "faintly fragrant, pleasant" odor;[13] in his original report on the species, Murrill noted that specimens left to dry in an oven developed a strong odor of burnt sugar.[4] The taste of the mushroom has been described variously as indistinct,[13] or "distinctly bitter".[15] The type material was noted by Murrill to taste bitter, an observation later corroborated by Pouzar with European collections.[9] A. subrubescens mushrooms are mildly toxic: consumption causes a gastrointestinal illness that usually subsides one to four hours after ingestion.[16]
 In deposit, the spores are white.[17] The spores are 3.4–4.7 by 2.2–3.4 µm, ellipsoid to ovoid in shape, and amyloid (absorbing iodine when stained with Melzer's reagent). Most have a single large oil drop. The spore-bearing cells (the basidia) are club-shaped, 12–16 µm long by 5.7–7.7 µm thick, and have four thin, slightly curved sterigmata that are 3.4–4.3 µm long.[14] The hymenium lacks any cystidia.[15] The hyphal system is monomitic, meaning that the context is made of thin-walled generative hyphae. These hyphae have lengths in the range of 3.5–30 µm (most commonly 6–17 µm), with thin walls (up to 1 µm thick), and are hyaline (translucent). Although they are inamyloid, some hyphae have internal masses that are colored pale bluish-gray to black, which makes them appear collectively grayish-black under the microscope.[13] Gloeoporous hyphae (wide and thin-walled with refractive contents) are also scattered throughout the context, as well as some hyphae with expanded tips that are thick-walled and amyloid.[15]
 In general, A. subrubescens can be distinguished from other Albatrellus species by its white cap that becomes orange when bruised, its simple-septate hyphae, small amyloid spores, and habitat under pines.[18] In the field, Albatrellus ovinus is difficult to differentiate from A. subrubescens due to its similar appearance. A. ovinus usually lacks the violet color often seen in the cap and stem of A. subrubescens. Microscopic characteristics can be used to reliably distinguish the two species: the spores of A. subrubescens are amyloid, in contrast to those of A. ovinus,[19] and A. ovinus spores are smaller, typically 3.8–4.6 by 3.3–3.5 µm.[8] Other similar species include A. tianschanicus, described from the Altai Mountains in East-Central Asia, and the Japanese species A. cantharellus. Unlike A. subrubescens, these species have hairy scales on the surface of their caps, and the scales are darker than the spaces between the scales. Also, the scales of A. subrubescens are not much darker than the area between the scales.[8] Both of these Asian species have larger spores than A. subrubescens: those of A. cantharellus are 4.5–7 by 4–5.5 µm, while those of A. tianschanicus are 5–7 by 4–5 µm.[20]
 Albatrellopsis confluens has caps that are pinkish-buff to pale orange, and white flesh that dries to a pinkish-buff; it has a taste that is bitter, or like cabbage. The spores of A. confluens are weakly amyloid.[17] Additional differences distinguishing Albatrellopsis confluens from A. subrubescens include the presence of clamp connections in the context hyphae, and mycelium on the base of the stem.[9] The European fungus A. citrinus, originally considered a morphotype of A. subrubescens, was described as a new species in 2003. It is distinguished from A. subrubescens morphologically by its smaller caps (up to 7 cm (2.8 in) in diameter), the yellowish bruising of the caps with age or after handling, and the absence of violet spots on the cap. A. citrinus associates with spruce rather than pine, and requires calcareous (lime-rich) soil.[20]
 Fruit bodies of A. subrubescens are usually solitary, but sometimes several (typically between two and eight) are stuck together by the stem bases or on the sides of their caps.[13] A strictly terrestrial species, it is not found fruiting on wood. It prefers to grow in pine woods, but has occasionally been associated with silver fir in Europe; fruit bodies associated with the latter tree species tend to be less robust than those found growing with pine.[9] It is suspected that A. subrubescens may be mycorrhizal with two- and three-needle pines (i.e., those species that have their needles attached in bundles of two or three), although its ecological preferences are not known with certainty. Ginns, relating a personal communication with David Arora, wrote that Arora encountered several clumps of fruit bodies in an area in California containing mostly knobcone pine (a three-needle pine), manzanita, huckleberry and a few mandrones.[13]
 The species has been reported from a variety of locations in temperate regions of Asia, Europe, and North America.[21][22] In North America, its distribution includes Alberta, Quebec, and the Northwest Territories in Canada. In the United States, it is found in Alabama, Arizona, California, Florida, New York, Texas, Washington, and Wisconsin.[9][13][17] The North American distribution extends south to Mexico, in Chiapas.[23][24] In Asia, the fungus has been recorded from Yunnan in southwest China, and Tibet.[18] In Europe, collections have been reported from Austria, Czechoslovakia, Finland, Germany, Italy, Poland, Sweden, Switzerland, Yugoslavia, and the Ukraine.[9]
 Albatrellus subrubescens contains the bioactive compound scutigeral, which has antibiotic activity. This chemical—also found in the related species A. ovinus[25]—may contribute to the mushroom's toxicity by disturbing the body's intestinal flora.[26] Scutigeral interacts selectively to the dopamine receptor D1 subfamily (the most abundant dopamine receptor in the central nervous system, regulating neuronal growth and development, and mediating some behavioral responses).[27] A 1999 publication suggested that scutigeral has agonistic activity at vanilloid receptors (a receptor found on sensory nerves in mammals); specifically, that it affects the uptake of calcium in the neurons of rat dorsal root ganglia.[28] Later reports failed to corroborate this pharmacological activity. One 2003 study reported that scutigeral acts as a weak antagonist on the human vanilloid receptor VR1,[29] while another study published that year did not find any activity.[30]
 Scutiger subrubescens Murrill (1940) Polyporus subrubescens (Murrill) Murrill (1947) Albatrellus similis Pouz. (1965) Scutiger ovinus var. subrubescens (Murrill) L.G.Krieglst. (1992)[1] Albatrellus ovinus var. subrubescens (Murrill) L.G.Krieglst. (2000)[2] 1 Taxonomy and phylogeny 2 Description

2.1 Similar species

 2.1 Similar species 3 Habitat and distribution 4 Bioactive compounds 5 References ^ Krieglsteiner GJ. (1992). Anmerkungen, Korrekturen und Nachträge zum Verbreitungsatlas de Grosspilze Deutschlands (West), Band 1 (1991), Teilbände A und B [Comments, Corrections and Additions to the Distribution Atlas of Macrofungi of West Germany]. Beiträge zur Kenntnis der Pilze Mitteleuropas (in German). 8. Dietenberger: Schwäbisch Gmünd: Einhorn-Verlage. pp. 173–204. ISBN 978-3-927654-28-0..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ Krieglsteiner GJ. (2003). Die Großpilze Baden-Württembergs [The Large Mushrooms in Baden-Württemberg] (in German). 4. Stuttgart: Eugen Ulmer. p. 475. ISBN 978-3-8001-3531-8.
 ^ "Albatrellus subrubescens (Murrill) Pouzar :196, 1972". MycoBank. International Mycological Association. Retrieved 2013-01-27.
 ^ a b Murrill WA. (1940). "Additions to Florida fungi 5". Bulletin of the Torrey Botanical Club. 67 (4): 275–81. doi:10.2307/2481174. JSTOR 2481174.
 ^ Murrill WA. (1947). "Florida polypores". Lloydia. 10: 242–80.
 ^ Overholts LO. (1953). The Polyporaceae of the United States, Alaska, and Canada. University of Michigan Studies. Scientific Series. 19. Ann Arbor: University of Michigan Press. p. 428.
 ^ Pouzar Z. (1966). "A new species of the genus Albatrellus (Polyporaceae)". Folia Geobotanica & Phytotaxonomica. 1 (3): 274–6. JSTOR 4179435.
 ^ a b c Pouzar Z. (1972). "Contribution to the knowledge of the genus Albatrellus (Polyporaceae). I. A conspectus of species of north temperate zone". Česká Mykologie. 26 (4): 194–200.
 ^ a b c d e f Pouzar Z. (1974). "An observation on Albatrellus subrubescens (Polyporaceae)". Folia Geobotanica & Phytotaxonomica. 9 (1): 87–94. doi:10.1007/BF02851403. JSTOR 4179779.
 ^ Headrick D, Gordh G (2001). A Dictionary of Entomology. Wallingford: CAB International. p. 792. ISBN 978-0-85199-655-4.
 ^ a b Audet S. (2010). "Essai de découpage systématique du genre Scutiger (Basidiomycota): Albatrellopsis, Albatrellus, Polyporoletus, Scutiger et description de six nouveaux genres" [Essay on systematic cutting of the genus Scutiger (Basidiomycota): Albatrellopsis, Albatrellus, Polyporoletus, Scutiger and description of six new genera] (abstract). Mycotaxon (in French). 111: 431–64. doi:10.5248/111.431.
 ^ Larsson E, Larsson K-H (2003). "Phylogenetic relationships of russuloid Basidiomycetes with emphasis on aphyllophoralean taxa". Mycologia. 95 (6): 1037–65. doi:10.2307/3761912. JSTOR 3761912. PMID 21149013.
 ^ a b c d e f g h Ginns J. (1997). "The taxonomy and distribution of rare or uncommon species of Albatrellus in western North America". Canadian Journal of Botany. 75 (2): 261–73. doi:10.1139/b97-028.
 ^ a b c Pouzar Z. (1975). "Two rare Japanese species of the genus Albatrellus (Polyporaceae)". Folia Geobotanica et Phytotaxonomica. 10 (2): 197–203. doi:10.1007/bf02852862. JSTOR 4179859.
 ^ a b c Ryvarden L. (1993). European Polypores. Part 1. Abortiporus–Lindtneria. Oslo: Lubrecht & Cramer. p. 95. ISBN 978-82-90724-12-7.
 ^ Angeli P, Lazzarini E, Para R (2009). I Funghi Tossici e Velenosi [Toxic and Poisonous Mushrooms] (in Italian). Milan: Ulrico Hoepli Editore. p. 43. ISBN 978-88-203-4291-3.
 ^ a b c Bessette AE, Roody WC, Bessette AR (2007). Mushrooms of the Southeastern United States. Syracuse: Syracuse University Press. p. 249. ISBN 978-0-8156-3112-5.
 ^ a b Zheng HD, Liu PG (2008). "Additions to our knowledge of the genus Albatrellus (Basidiomycota) in China" (PDF). Fungal Diversity. 32: 157–70. ISSN 1560-2745.
 ^ Jülich W. (1984). Die Nichtblatterpilze, Gallertpilze und Bauchpilze [Aphyllophorales, Heterobasidiomycetes, Gastromycetes]. Kleine Kryptogamenflora (in German). 2b/1. Stuttgart: Gustav Fischer Verlag.
 ^ a b Ryman S, Fransson P, Johannesson H, Danell E (2003). "Albatrellus citrinus sp. nov., connected to Picea abies on lime rich soils". Mycological Research. 107 (10): 1243–6. doi:10.1017/S0953756203008359. PMID 14635772.
 ^ Ginns J. (2006). "Annotated Key to Pacific Northwest Polypores". Vancouver Mycological Society. Archived from the original on 8 October 2010. Retrieved 2010-10-13.
 ^ Canfield ER, Gilbertson RL (1971). "Notes on the genus Albatrellus in Arizona". Mycologia. 63 (5): 964–71. doi:10.2307/3757898. JSTOR 3757898.
 ^ Valenzuela R, Nava R, Cifuentes J (1994). "El género Albatrellus en México. I" [The genus Albatrellus from Mexico]. Revista Mexicana de Micología (in Spanish). 10: 113–52. ISSN 0187-3180.
 ^ González-Espinosa M, Ramírez-Marcial N, Ruiz-Montoya L (2005). Diversidad biológica en Chiapas [Biodiversity in Chiapas] (in Spanish). Plaza y Valdes. p. 68. ISBN 978-970-722-399-8.
 ^ Cole RJ. (2003). Handbook of Secondary Fungal Metabolites. 2. Boston: Academic Press. p. 607. ISBN 978-0-12-179462-0.
 ^ Bresinsky A, Besl H (1989). A Colour Atlas of Poisonous Fungi: A Handbook for Pharmacists, Doctors, and Biologists. London: Manson Publishing. p. 181. ISBN 978-0-7234-1576-3.
 ^ Dekermendjian K, Shan R, Nielsen M, Stadler M, Sterner O, Witt MR (1997). "The affinity to the brain dopamine D1 receptor in vitro of triphenyl phenols isolated from the fruit bodies of Albatrellus ovinus". European Journal of Medicinal Chemistry. 32 (4): 351–6. doi:10.1016/S0223-5234(97)89088-5.
 ^ Szallasi A, Biro T, Szabó T, Modarres S, Petersen M, Klusch A, Blumberg PM, Krause JE, Sterner O (1999). "A non-pungent triprenyl phenol of fungal origin, scutigeral, stimulates rat dorsal root ganglion neurons via interaction at vanilloid receptors". British Journal of Pharmacology. 126 (6): 1351–8. doi:10.1038/sj.bjp.0702440. PMC 1565912. PMID 10217528.
 ^ Hellwig V, Nopper R, Mauler F, Ji-Kai L, Zhi-Hui D, Stadler M (2003). "Activities of prenylphenol derivatives from fruitbodies of Albatrellus spp. on the human and rat vanilloid receptor 1 (VR1) and characterisation of the novel natural product, confluentin". Archiv der Pharmazie. 336 (2): 119–26. doi:10.1002/ardp.200390008. PMID 12761765.
 ^ Ralevic V, Jerman JC, Brough SJ, Davis JB, Egerton J, Smart D (2002). "Pharmacology of vanilloids at recombinant and endogenous rat vanilloid receptors". Biochemical Pharmacology. 65 (1): 143–51. doi:10.1016/S0006-2952(02)01451-X. PMID 12473388.
  Media related to Albatrellus subrubescens at Wikimedia Commons Wikidata: Q80705 BioLib: 59892 EoL: 6763334 Fungorum: 308435 GBIF: 3357733 iNaturalist: 350020 MycoBank: 308435 NCBI: 205785 Wikidata: Q59419077 Fungorum: 290829 MycoBank: 290829 Fungi described in 1940 Fungi of Asia Fungi of Europe Fungi of North America Poisonous fungi Russulales Taxa named by William Alphonso Murrill CS1 German-language sources (de) CS1 French-language sources (fr) CS1 Italian-language sources (it) CS1 Spanish-language sources (es) Articles with short description Featured articles Articles with 'species' microformats Commons category link from Wikidata Taxonbars with automatically added basionyms Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Asturianu Español Italiano Piemontèis Português Suomi Svenska  This page was last edited on 11 September 2019, at 06:15 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  A. subrubescens Albatrellus subrubescens pores convex flat decurrent bare white mycorrhizal poisonous Albatrellus subrubescens A. subrubescens ^ 8 ^ 4 ^ a b 67 ^ 10 ^ 19 ^ 1 a b c 26 a b c d e f 9 ^ a b 111 ^ 95 a b c d e f g h 75 a b c 10 a b c ^ a b c a b 32 ^ 2b/1 a b 107 ^ ^ 63 ^ 10 ^ ^ 2 ^ ^ 32 ^ 126 ^ 336 ^ 65 
 Kingdom:
 Fungi
 Division:
 Basidiomycota
 Class:
 Agaricomycetes
 Order:
 Russulales
 Family:
 Albatrellaceae
 Genus:
 Albatrellus
 Species:
 A. subrubescens
 Albatrellus subrubescens(Murrill) Pouz. (1972)
 

Scutiger subrubescens Murrill (1940)
Polyporus subrubescens (Murrill) Murrill (1947)
Albatrellus similis Pouz. (1965)
Scutiger ovinus var. subrubescens (Murrill) L.G.Krieglst. (1992)[1]
Albatrellus ovinus var. subrubescens (Murrill) L.G.Krieglst. (2000)[2]

  Mycological characteristics pores on hymenium cap is convex
   or flat hymenium is decurrent stipe is bare spore print is white ecology is mycorrhizal edibility: poisonous 


 


.mw-parser-output table.clade{border-spacing:0;margin:0;font-size:100%;line-height:100%;border-collapse:separate;width:auto}.mw-parser-output table.clade table.clade{width:100%}.mw-parser-output table.clade td.clade-label{width:0.7em;padding:0 0.2em;vertical-align:bottom;text-align:center;border-left:1px solid;border-bottom:1px solid}.mw-parser-output table.clade td.clade-label.first{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-label.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-slabel{padding:0 0.2em;vertical-align:top;text-align:center;border-left:1px solid}.mw-parser-output table.clade td.clade-slabel.last{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-slabel.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-bar{vertical-align:middle;text-align:left;padding:0 0.5em;position:relative}.mw-parser-output table.clade td.clade-bar.reverse{text-align:right}.mw-parser-output table.clade td.clade-leaf{border:0;padding:0;text-align:left}.mw-parser-output table.clade td.clade-leafR{border:0;padding:0;text-align:right}.mw-parser-output table.clade td.clade-leaf.reverse{text-align:right}


 


A. ovinus



 


 


A. citrinus



 


 


A. subrubescens



 



 


 





 


A. tianschanicus



 



 

  
 
.mw-parser-output table.clade{border-spacing:0;margin:0;font-size:100%;line-height:100%;border-collapse:separate;width:auto}.mw-parser-output table.clade table.clade{width:100%}.mw-parser-output table.clade td.clade-label{width:0.7em;padding:0 0.2em;vertical-align:bottom;text-align:center;border-left:1px solid;border-bottom:1px solid}.mw-parser-output table.clade td.clade-label.first{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-label.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-slabel{padding:0 0.2em;vertical-align:top;text-align:center;border-left:1px solid}.mw-parser-output table.clade td.clade-slabel.last{border-left:none;border-right:none}.mw-parser-output table.clade td.clade-slabel.reverse{border-left:none;border-right:1px solid}.mw-parser-output table.clade td.clade-bar{vertical-align:middle;text-align:left;padding:0 0.5em;position:relative}.mw-parser-output table.clade td.clade-bar.reverse{text-align:right}.mw-parser-output table.clade td.clade-leaf{border:0;padding:0;text-align:left}.mw-parser-output table.clade td.clade-leafR{border:0;padding:0;text-align:right}.mw-parser-output table.clade td.clade-leaf.reverse{text-align:right}


 


A. ovinus



 


 


A. citrinus



 


 


A. subrubescens



 

  
 
A. ovinus

  
  
 
A. citrinus

  
  
 
A. subrubescens

  
  
  
 



 


A. tianschanicus



 

  
 
A. tianschanicus

  
  
 Phylogeny of A. subrubescens and selected related species based on ribosomal DNA sequences.[11]
 
Wikidata: Q80705
BioLib: 59892
EoL: 6763334
Fungorum: 308435
GBIF: 3357733
iNaturalist: 350020
MycoBank: 308435
NCBI: 205785
 
Wikidata: Q59419077
Fungorum: 290829
MycoBank: 290829
 