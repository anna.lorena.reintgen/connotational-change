
 Valley View is a mid-19th-century Greek Revival residence and farm overlooking the South Branch Potomac River northwest of Romney, West Virginia. Valley View is atop a promontory where Depot Valley joins the South Branch Potomac River valley.
 The Valley View property was part of the South Branch Survey of the Northern Neck Proprietary, a large tract that was inherited by Thomas Fairfax, 6th Lord Fairfax of Cameron, in 1719. It was settled by John Collins and his family in 1749, and acquired by the Parsons family before 1772. The Valley View house was built by James Parsons Jr. in 1855. After the Civil War, Parsons' widow sold the farm to Charles Harmison. His wife, Elizabeth Harmison, inspired by her childhood Virginia home, Western View, and the scenic South Branch Potomac River views, named the farm Valley View. The most recent of a series of owners, the Mayhew family, bought the property in 1979. Valley View's current proprietors, Robert and Kim Mayhew, have restored the historic residence and grounds.
 The house at Valley View is a two-story brick structure with a rectangular architectural plan. The front entrance is covered by a small portico, topped with a pediment supported by wooden Doric columns. The rear of the house, with a two-story wood porch stretching across it, faces the South Branch Potomac River valley and Mill Creek Mountain. Each of the original eight large rooms of the 1855 structure contains a fireplace framed by a wooden trabeated mantelpiece with classical elements. The original windows, wooden trim, and materials in the main section of the house are intact. The house was listed on the National Register of Historic Places in 2012 as a locally significant example of Greek Revival architecture.
 The Valley View house is about 1 mile (1.6 km) northwest of downtown Romney, atop a promontory (known locally as the Yellow Banks) where Depot Valley joins the South Branch Potomac River valley. Depot Valley runs 0.5 miles (0.8 km) from West Sioux Lane in Romney to Valley View, and an unnamed tributary of Big Run flows north along its bottom. Depot Valley Road parallels the stream.[2]
 Depot Valley is named for Romney Depot, located at the end of a former spur of the Baltimore and Ohio Railroad (B&O) South Branch line near the intersection of present-day West Sioux Lane and Depot Valley Road.[2] The unincorporated area around the depot was once known as Valley.[3][4] A post office operated there from 1928 until 1937, when its mail was routed through Romney; it is assumed that Valley View farm used it, since it was 0.5 miles (0.8 km) south of the house.[4]
 The Valley View farm property adjoins the Wappocomo farm on the northeast, the corporate limits of Romney on the east and south and the Yellow Banks on the west. As well as Valley View's 6.63-acre (2.68 ha) tract, the Mayhew family owns agricultural land rich in alluvial soils along the South Branch Potomac River west of the house. The South Branch Valley Railroad bisects this farmland, crossing the South Branch Potomac River via a wooden trestle.[2]
 Valley View Island, an island in the South Branch Potomac River just north of the mouth of Sulphur Spring Run, is approximately 0.5 miles (0.8 km) southwest of the Valley View house.[2] Both the house and the island are owned by the Mayhew family.[5] The island is ringed by forests, with agricultural fields in its center.[2] When Lots Number 17 and 19 of the Northern Neck Proprietary South Branch Survey were surveyed in 1749 and resurveyed in 1788, the island belonged to Lot Number 19. At that time, the river flowed east of the island, along the base of the Yellow Banks; its course later changed to run around the west side of the island.[6]
 Mill Creek Mountain, a narrow anticlinal mountain ridge, rises westward from the South Branch Potomac River across from Valley View. The western foothills of South Branch Mountain rise to the east.[2][7][8] Both mountains are covered with Appalachian – Blue Ridge forests of hardwoods and pine.[9]
 The land upon which Valley View is located was originally part of the Northern Neck Proprietary, a land grant that the exiled Charles II awarded to seven of his supporters in 1649 during the English Interregnum.[10][11][12] Following the Restoration of Charles II to the throne in 1660,[13] he renewed the Northern Neck Proprietary grant in 1662, revised it in 1669, and again renewed the original grant favoring the original grantees Thomas Colepeper, 2nd Baron Colepeper and Henry Bennet, 1st Earl of Arlington in 1672.[14] In 1681, Bennet sold his share to Lord Colepeper, and Lord Colepeper received a new charter for the entire land grant from James II in 1688.[10][15][16]
 Following the deaths of Lord Colepeper, his wife Margaret, and his daughter Katherine, the Northern Neck Proprietary passed to Katherine's son Thomas Fairfax, 6th Lord Fairfax of Cameron in 1719,[10][17][18] who selected a portion of it for his manor. This tract, known as the South Branch Survey of the proprietary, extended from the north end of the Trough to the junction of the North and South Branches of the Potomac River.[15] In 1748, Fairfax commissioned James Genn to survey the South Branch Potomac River lowlands for sale and lease, with lots ranging in size from 300 to 400 acres (120 to 160 ha).[15]
 In 1749, the tract on which Valley View stands was purchased from Lord Fairfax by John Collins.[19] The 425-acre (172 ha) lot was Lot Number 20 on the South Branch Survey.[19] Collins also owned a large tract of land spanning present-day Hampshire and Hardy counties.[20] His son Thomas Collins is thought to have inherited his father's landholdings as an "heir at law", since there is no record of a will by John Collins dispensing of his properties.[a] By 1772, Thomas Collins acquired Lot Number 20, where he lived with his wife Elizabeth.[19] In 1816, Collins was serving as a magistrate when the town of Romney held a Virginia state election for the Electoral College.[21] One representative from each of Virginia's 25 counties traveled to Romney to cast his vote.[21] Collins and county commissioner William Donaldson certified the convention's election results.[21]
 In 1817, Thomas Collins sold Lot Number 20 to James Gregg Parsons. It is unknown whether the Collinses moved from the tract or continued living on it after the sale. Thomas Collins died in 1822, and Elizabeth Collins in 1823.[19]
 The Parsons family members were among the first English settlers in the Thirteen Colonies in 1635; around 1740, they settled in Hampshire County.[19] By 1778, Isaac Parsons (1752–1796), a member of the Virginia House of Delegates, owned 161 acres (65 ha) of Lot Number 16 and all of Lot Number 17 in the Proprietary.[6] James Gregg Parsons, his eldest son,[22] was born in Hampshire County in 1773. In 1795, he married Mary Catherine Casey (1773–1846), whose family owned the adjoining Lot Number 21.[19] After their marriage, they lived in the main house at Wappocomo, which had been built by Mary Catherine's father Nicholas Casey. The couple inherited the house after Nicholas Casey's death in 1833.[19]
 James Gregg Parsons died on January 25, 1847, leaving most of his land to his three sons: James (Big Jim) Parsons Jr. (1798–1858), David C. Parsons (1803–1860), and Isaac Parsons (1814–1862).[19][23] James, his eldest son, inherited Lot Number 20 (known as the Collins Tract);[20] his second son David inherited Lot Number 13 south of Romney (on which Hickory Grove was later located);[24][25] and his youngest son Isaac inherited Lot Number 21 (which included Wappocomo).[19][23] His sons also inherited the nearby "Jake Sugar Rum Tract, the McGuire Tract, and five town lots in Romney".[23] According to historian William K. Rice, by 1846 Parsons' sons and their families were all living on the tracts they would eventually inherit. Rice determined that James Parsons Jr. moved to the Collins Tract, around 1826, and was living there when his father died.[19]
 James Parsons Jr. was a farmer and cattleman who was born in Hampshire County.[19][20][26] Parsons family genealogist Virginia Parsons MacCabe wrote the following description of James Parsons Jr. in her book Parsons' Family History and Record (1913): "He was square and honorable in business, and had a large circle of friends; he had the urbanity and the gentility of manner which characterizes the true gentleman".[26] Parsons married Elizabeth Miller on January 8, 1829.[20][26] The couple had eleven children, several of whom attended college.[19][20]
 In 1855, Parsons began building the present-day Valley View house on the Collins Tract.[20] Although he wrote many letters to his sister Mary Gregg Parsons Stump about farming, cattle, family, health and community events, no letters are known to remain from the time of the house's construction.[19] The Parsons family owned several slaves who are thought to have assisted with construction.[19]
 After living in his new house for three years, Big Jim died of tuberculosis on October 14, 1858.[19][26][27] His widow, Elizabeth, lived in the house until after the Civil War.[19][28] In 1867 or 1869,[b] she sold the house, the Collins Tract and the remainder of Lot Number 20 to Charles Harmison (1823–1896) for $8,500, moving with her remaining children to Missouri (where she died in 1883).[19][26][28] The cost of building the house financially strained the Parsons family;[19][27] historian Catherine Snider Long suggests that Elizabeth Miller Parsons sold the house as a result of further, war-related, financial stress from which the family could not recover.[28]
 Charles Harmison was born in Franklin County, Illinois, to Nathaniel and Lydia Harmison, and married Bettie Ann Smith (1827–1903) on May 4, 1854, in Taylor County, West Virginia.[29] Bettie, the daughter of C. C. and Martha W. Smith,[29] was raised at Western View (their Fauquier County, Virginia, home).[19][28][30] By 1867, Harmison and his family were living in Harrison County.[28] His older brother had moved to Romney, where he established and operated the Virginia House hotel.[28] In 1867, Charles Harmison's brother learned that the Parsons farm on the Collins Tract was for sale, and he advised Charles to buy it.[28] Charles' wife, who wanted to live nearer to Virginia, also urged Charles to buy the property.[28] Charles purchased the farm and he, his wife, their seven children, and a young African American boy named Snoden moved from Harrison County to Hampshire County in three days.[19][28] They traveled on the Northwestern Turnpike in an ambulance Charles had bought after the war.[28] Elizabeth Harmison named their new house and farm Valley View, which was influenced by the name of her childhood home, Western View, and the view of the South Branch Potomac River valley from their property.[19][28][30]
 Harmison prospered in Hampshire County, acquiring adjacent properties and enlarging his Valley View estate.[28][31] He later gave his acquired lands to his children to establish their own homes when they married.[28] His farm was further changed in 1884, when the B&O Railroad completed its South Branch line between the main B&O line at Green Spring and Romney Depot.[32] The South Branch line bisected the small valley to the immediate east of the house, which became known as Depot Valley.[32]
 Charles Harmison died on October 31, 1896 after being thrown from a buggy.[33][34] His son George Edward Harmison (1863–1916) inherited Valley View around 1903 and brought his wife, Carrie Belle Fox (1870–1953), there after their marriage on October 4, 1905.[19][28][35] George demolished the old log kitchen at Valley View, replacing it with a contemporary one.[19]
 In June 1909, construction commenced on the Hampshire Southern Railroad between its northern terminus on the B&O Railroad's Romney Depot spur and the South Branch Potomac River within the bottomlands of George Harmison's farm.[36] In October 1909, the first train on the Hampshire Southern line passed over Harmison's bottomlands and crossed the river on an unfinished trestle across the South Branch Potomac River.[37][38] By 1910, the 18-mile (29 km) line from the Romney spur terminus at Valley View to McNeill was in operation.[32] Later that year, freight and passenger service between Romney and Moorefield began, providing a direct rail link between Moorefield and the B&O Railroad main line at Green Spring.[32][39] The Hampshire Southern Railroad Company operated this line until 1911, when it was purchased by the Moorefield and Virginia Railroad Company.[32][39] Moorefield and Virginia transferred the rail line to the B&O Railroad Company in 1913, when it became part of the B&O's South Branch line.[39]
 In 1911, George Harmison subdivided the Valley View fields on the Yellow Banks overlooking the South Branch Potomac River. The new development, known as the Valley View Addition to Romney, was south of the Valley View house and west of Romney Depot. Twenty-one lots were sold at public auction on September 27, 1911, and several more were sold privately.[40]
 Harmison died in 1916, and Carrie continued to live at Valley View until her death on February 8, 1953.[28][41][42] Harmison's nephew, Paul Cresap Harmison (1893–1972, a grandson of Charles Harmison's brother Jonathan Harmison), and his wife Nancy Parker Harmison (1896–1981) had moved to Valley View to live with her. After Carrie's death, Paul and Nancy Parker Harmison inherited the house and farm.[31] Paul and Nancy's daughter Virginia Helen Harmison was married to Robert Esler in front of the fireplace in the home's living room on May 5, 1957.[43] Valley View remained in the Harmison family until 1963, when it was sold to Philip Newell and his wife Martha.[19][31]
 During its changes in ownership, the original Lot Number 20 of the South Branch Survey was repeatedly partitioned and sold. By 1976, the original property was divided into five farms and other parcels, including the Valley View Addition.[20] The Valley View residence lies on a 6.63-acre (2.68 ha) tract.[19]
 Valley View was purchased by Robert Mayhew's father and a business associate in 1979.[19] Mayhew later bought the house from his father, and he and his wife Kim restored the residence and its grounds.[5] In 1991, the Potomac Eagle Scenic Railroad began operating on the old B&O South Branch line, which bisects the bottomlands below Valley View.[39][5]
 After surveys of historic properties in the county, in 2008 the Hampshire County Historic Landmarks Commission and the Hampshire County Commission began an initiative to place structures and districts on the National Register of Historic Places (NRHP).[44] The county received funding from the State Historic Preservation Office of the West Virginia Division of Culture and History to survey and document the structure's architecture and history.[44] As a result of this initiative, Valley View was one of the first eight historic properties to be considered for placement on the register.[44] The other seven were Capon Chapel, Fort Kuykendall, Hickory Grove, Hook Tavern, North River Mills Historic District, Old Pine Church and Springfield Brick House.[44] The house at Valley View was listed on the NRHP on December 12, 2012.[1][45]
 The house at Valley View is significant for its Greek Revival architecture.[45] According to architectural historian Courtney Fint Zimmerman, "Valley View is a characteristic example of the Greek Revival style for more practical residential applications in outlying areas".[19] The Valley View house has several Greek Revival design characteristics, including a symmetrical architectural plan and elevations and "substantial, formal" mass.[45] Zimmerman (who prepared Valley View's registration form for the NRHP) said, "Valley View's applied details in the Greek Revival style, including the front entrance entablature and portico, are more limited, yet the variations that can be seen on Valley View and other estates in the South Branch Valley illustrate the flexibility inherent in the style".[45] According to Zimmerman, large houses like Valley View served as the "centers" of the plantations that formed the "basis of the local economy and social life" in Hampshire County.[46] Valley View was added to the NRHP as a locally significant example of Greek Revival architecture.[47]
 Valley View's house consists of the original 1855 brick section and a board-and-batten 1961–1962 kitchen addition.[30] The grounds contain a smokehouse, a water well, the foundations of an ice house, and a summer kitchen.[48]
 The bricks from which the house was built were fired in the immediate vicinity (along the banks of the South Branch Potomac River), and the brick walls were reinforced with hand-wrought structural iron angles.[19][49] The nails used in its construction were fabricated by a local blacksmith, and the wooden sills and joists were sawn by hand.[19][50]
 Valley View's house is a two-story brick structure with a rectangular architectural plan and exterior dimensions measuring about 49 by 32 feet (14.9 by 9.8 m).[30] The house's exterior brick walls are 9 inches (23 cm) thick and laid in an American bond.[19][30][49] The house is topped with a steep metal gabled roof with standing seam profiles.[30][50] Two sets of double inside chimneys extend above the steep roofline on the northwestern and southeastern ends.[30][50]
 The front façade of the house faces a hill to the southwest. It is five bays wide, with the front entrance at the first floor's center bay. Wide double-hung sash windows are uniformly placed on the house's front façade, with four nine-over-six double-hung wooden sashes on the first story and five six-over-six double-hung wooden sash windows on the second.[c] Each window is surrounded by green-painted wooden shutters and white-painted wooden lintels and sills.[30][50]
 The front entrance is covered by a small Greek Revival portico measuring about 12 by 12 feet (3.7 by 3.7 m), topped with a pediment supported by wooden Doric columns and engaged columns at the wall.[30][50][54] The front porch is flanked by modest wooden handrails and balusters on its left and right sides.[30] The front entrance is post and lintel (trabeated) construction, with a six-pane transom and two three-pane sidelight windows around the doorway.[30][50] Zimmerman suggests that "Big Jim" Parsons embellished his home's front entrance to assert his "wealth and status" and provide "an honored welcome to visitors".[55]
 The rear façade of the house faces northeast, across the South Branch Potomac River valley toward Mill Creek Mountain.[30][50] A two-story (double) wooden porch about 9 feet (2.7 m) deep extends across the rear of the house, topped by a shed roof extending from the main gabled roof at a shallower pitch.[30][50] The first-story porch supports are brown wooden turned posts with no handrail or balusters, and the porch's second story has white painted square wood posts and vertical railings.[30] Like the front façade, the rear façade is five bays wide; access to the double porch is through a door in the central bay on both levels.[30] The other four bays have nine-over-six double-hung wooden sash windows on the first story and six-over-six double-hung wooden sash windows on the second story.[30] The northwestern and southeastern sides of the house have one small square window at attic level, between each pair of inside chimneys.[30][50]
 The interior of the Valley View house has a two-room-deep, central-hallway floor plan.[48][50] Its wide central hallway contains a staircase from the first floor to the attic, with a wooden handrail supported with square balusters and a modest wooden turned newel post.[48] The ceilings are 10 feet (3.0 m) high.[48][50] Although the house's foundation is low, the height of the interior walls and the full-sized attic make the house appear tall from the outside.[50]
 The original house has eight large rooms, each with a fireplace framed by a wooden geometric trabeated mantelpiece with classical elements.[48][50][55] The four large rooms on the first floor open from either side of the center hallway. They contain simple wide wood trim, including skirting boards and door frame moldings with "subtly demarcated corners".[48][50] The house's living and dining rooms have wide, wooden dado rails.[48] Most of the wooden decorative trim is painted white, and the walls are plaster.[48] The lone exception is the room serving as an office and den, which has dark stained wooden trim and interior brick structural walls (exposed by the removal of its plaster during the 1960s).[48] All rooms have the original wide plank wooden floors.[48] The second floor has four bedrooms, with closets on either side of a fireplace and simple wood skirting boards and door frames.[48] Parsons family members painted signatures and graffiti in the attic around 1856, which remain visible on the stairwell wall.[19]
 A one-story kitchen addition, built in 1961–1962 and measuring about 21 by 14 feet (6.4 by 4.3 m), extends from the northwest side of the original 1855 house. The addition has a gabled standing seam metal roof, and its exterior is covered in white-painted board-and-batten siding. It has a vinyl bay window on the southwest side, a one-over-one double-hung vinyl window on the northeast side and a door (adjoining the wall of the 1855 house) on the southeast side. An enclosed board-and-batten porch, measuring about 14 by 10 feet (4.3 by 3.0 m), and a shed roof extend from the front (southwest) of the kitchen addition. The original basement under the 1855 house is accessible through this porch extension. A ghost building outline on the northwest side of the 1855 house indicates an earlier structure where the present kitchen addition stands.[30]
 There are several ancillary structures near the house at Valley View, including a smokehouse and a water well, and the foundations of an ice house and a summer kitchen.[48] Although the smokehouse, the summer kitchen and the ice house are believed to have been built by the Collinses before Big Jim Parsons built Valley View, the dates of construction are uncertain.[19]
 The smokehouse, measuring about 15 by 20 feet (4.6 by 6.1 m), is adjacent to the kitchen addition. It is set into a hillside, allowing at-grade entry to its two levels. Built of square-cut logs with white chinking atop a rubble masonry foundation, the smokehouse is topped with a standing seam metal gabled roof.[48]
 South of the smokehouse is the brick foundation of an ice house measuring about 15 by 20 feet (4.6 by 6.1 m) and topped by modern wooden pergola and patio structures. The 15-by-20-foot (4.6 by 6.1 m) brick foundation of Valley View's summer kitchen is north of the smokehouse and topped by a contemporary wooden pavilion with a gabled roof.[48]
 In the rear yard of the house is a water well, enclosed by a brick building about 7 by 7 feet (2.1 by 2.1 m) in area and 3.5 feet (1.1 m) in height. In the center of the well cap is a metal hand pump. Although the well cap's bricks are similar to those used in the construction of the main house, the well may date from an earlier residence on the site.[48]
 1 Geography and setting 2 History

2.1 Royal land grant and Collins family ownership
2.2 Parsons family
2.3 Harmison family
2.4 Mayhew family

 2.1 Royal land grant and Collins family ownership 2.2 Parsons family 2.3 Harmison family 2.4 Mayhew family 3 Architecture

3.1 Exterior
3.2 Interior
3.3 Kitchen addition
3.4 Ancillary structures

 3.1 Exterior 3.2 Interior 3.3 Kitchen addition 3.4 Ancillary structures 4 See also 5 References

5.1 Explanatory notes
5.2 Citations

 5.1 Explanatory notes 5.2 Citations 6 Bibliography 7 External links Architecture portal Geography portal National Register of Historic Places portal List of historic sites in Hampshire County, West Virginia List of plantations in West Virginia National Register of Historic Places listings in Hampshire County, West Virginia ^ Selden Brannon cites the suggestion of historian Catherine Snider Long that this is confirmed in the deed of Lot Number 20, which states that the tract was deeded to John Collins on August 8, 1749, and was subsequently deeded to James Gregg Parsons by Thomas Collins.[20]
 ^ While Brannon states 1867 as the year when Elizabeth Miller Parsons sold the farm to Charles Harmison, Courtney Fint Zimmerman cites Catherine Snider Long's date of sale in 1869.[19][28]
 ^ A sash is a frame consisting of panes of glass. "Nine-over-six" describes a window with two sashes; the top sash contains nine glass panes, and the bottom sash contains six glass panes.[51][52][53]
 ^ a b "National Register of Historic Places Listings". Weekly List of Actions Taken on Properties: 12/10/12 through 12/14/12. National Park Service. December 21, 2012. Archived from the original on July 9, 2016. Retrieved July 9, 2016..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c d e f United States Geological Survey (1973). Romney Quadrangle–West Virginia (Topographic map). 1:24,000. 7.5 Minute Series. Reston, VA: United States Geological Survey. OCLC 36344599.
 ^ "Geographic Names Information System: Feature Detail Report for Valley Post Office (historical) (Feature ID: 1718729)". Geographic Names Information System. United States Geological Survey. Retrieved July 9, 2016.
 ^ a b McMaster 2010, p. 47.
 ^ a b c "Owner of Mayhew Chevrolet marks three decades at wheel". Cumberland Times-News. Cumberland, Maryland. March 30, 2008. p. 25. Archived from the original on October 21, 2018. Retrieved October 21, 2018 – via NewspaperArchive.com.
 ^ a b Munske & Kerns 2004, p. 144.
 ^ "Geographic Names Information System: Feature Detail Report for Mill Creek Mountain (Feature ID: 1543330)". Geographic Names Information System. United States Geological Survey. Retrieved July 9, 2016.
 ^ "Geographic Names Information System: Feature Detail Report for South Branch Mountain (Feature ID: 1552967)". Geographic Names Information System. United States Geological Survey. Retrieved July 9, 2016.
 ^ "District 2 Wildlife Management Areas". West Virginia Division of Natural Resources Wildlife Resources Section. Archived from the original on July 9, 2016. Retrieved July 9, 2016.
 ^ a b c Munske & Kerns 2004, p. 9.
 ^ Coleman 1951, p. 246.
 ^ Rose 1976, p. 25.
 ^ William and Mary Quarterly 1898, p. 222.
 ^ William and Mary Quarterly 1898, pp. 222–3.
 ^ a b c Brannon 1976, p. 286.
 ^ William and Mary Quarterly 1898, p. 224.
 ^ William and Mary Quarterly 1898, pp. 224–6.
 ^ Rice 2015, p. 23.
 ^ a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag Zimmerman 2012, p. 9 of the PDF file.
 ^ a b c d e f g h Brannon 1976, p. 309.
 ^ a b c Munske & Kerns 2004, p. 79.
 ^ MacCabe 1913, p. 254.
 ^ a b c MacCabe 1913, p. 255.
 ^ Baker & Riebe 2010, p. 7 of the PDF file.
 ^ Brannon 1976, p. 298.
 ^ a b c d e MacCabe 1913, p. 260.
 ^ a b Brannon 1976, pp. 310–1.
 ^ a b c d e f g h i j k l m n o p Brannon 1976, p. 311.
 ^ a b "Marriage Record Detail: Charles Harmison". West Virginia Vital Research Records. West Virginia Division of Culture and History. Archived from the original on July 9, 2016. Retrieved July 9, 2016.
 ^ a b c d e f g h i j k l m n o p q r Zimmerman 2012, p. 5 of the PDF file.
 ^ a b c Brannon 1976, p. 312.
 ^ a b c d e Brannon 1976, p. 19.
 ^ "Death Record Detail: Charles Harmison, Sr". West Virginia Vital Research Records. West Virginia Division of Culture and History. Archived from the original on July 9, 2016. Retrieved July 9, 2016.
 ^ "Sad Accident" (PDF). The Clarksburg Telegram. Clarksburg, West Virginia. November 27, 1896. p. 2. Archived from the original (PDF) on October 21, 2018. Retrieved October 21, 2018 – via Chronicling America.
 ^ "Marriage Record Detail: George Edward Harmison". West Virginia Vital Research Records. West Virginia Division of Culture and History. Archived from the original on July 9, 2016. Retrieved July 9, 2016.
 ^ "Money Raised for Forty Mile Line". Highland Recorder. 31 (22). Monterey, Virginia. June 4, 1909. p. 2. Retrieved October 21, 2018 – via Library of Virginia.
 ^ "First Train On The New Road". Cumberland Evening Times. Cumberland, Maryland. October 21, 1909. p. 8. Archived from the original on October 21, 2018. Retrieved October 21, 2018 – via NewspaperArchive.com.
 ^ "Virginia Briefs". The Baltimore Sun. Baltimore. October 21, 1909. p. 11. Archived from the original on October 21, 2018. Retrieved October 21, 2018 – via Newspapers.com.
 ^ a b c d "History of the Potomac Eagle Scenic Railroad". Potomac Eagle Scenic Railroad. February 28, 2016. Archived from the original on July 9, 2016. Retrieved July 9, 2016.
 ^ "Back in Time: 100 Years Ago". Hampshire Review. Romney, West Virginia. September 28, 2011.
 ^ "Death Record Detail: Carrie B. Harmison (1)". West Virginia Vital Research Records. West Virginia Division of Culture and History. Archived from the original on July 9, 2016. Retrieved July 9, 2016.
 ^ "Death Record Detail: Carrie B. Harmison (2)". West Virginia Vital Research Records. West Virginia Division of Culture and History. Archived from the original on July 9, 2016. Retrieved July 9, 2016.
 ^ "Esler-Harmison Nuptials Take Place at Bride's Home". Cumberland Evening Times. Cumberland, Maryland. May 21, 1957. p. 6. Archived from the original on October 21, 2018. Retrieved October 21, 2018 – via NewspaperArchive.com.
 ^ a b c d Pisciotta, Marla (May 11, 2011). "Preserving Our History". Hampshire Review. Romney, West Virginia. p. 1B.
 ^ a b c d Zimmerman 2012, p. 8 of the PDF file.
 ^ Zimmerman 2012, p. 11 of the PDF file.
 ^ Zimmerman 2012, p. 3 of the PDF file.
 ^ a b c d e f g h i j k l m n o Zimmerman 2012, p. 6 of the PDF file.
 ^ a b Brannon 1976, pp. 309–310.
 ^ a b c d e f g h i j k l m n Brannon 1976, p. 310.
 ^ Ching 2012, pp. 289–290.
 ^ Historic England 2017, pp. 14–15.
 ^ "The Window Glossary". Window and Door Manufacturers Association (WDMA) website. Window and Door Manufacturers Association (WDMA). Archived from the original on December 31, 2017. Retrieved December 31, 2017.
 ^ Zimmerman 2012, pp. 9–10 of the PDF file.
 ^ a b Zimmerman 2012, p. 10 of the PDF file.
 Baker, Charles; Riebe, Erin; Hampshire County Landmarks Commission, West Virginia State Historic Preservation Office (May 2010). National Register of Historic Places Registration Form: Hickory Grove (PDF). National Park Service. Archived from the original (PDF) on July 9, 2016. Retrieved July 9, 2016. Brannon, Selden W., ed. (1976). Historic Hampshire: A Symposium of Hampshire County and Its People, Past and Present. Parsons, West Virginia: McClain Printing Company. ISBN 978-0-87012-236-1. OCLC 3121468. Ching, Francis D. K. (2012). A Visual Dictionary of Architecture. Hoboken, New Jersey: John Wiley & Sons. ISBN 978-0-470-64885-8. OCLC 780495852 – via Google Books. Coleman, Roy V. (1951). Liberty and Property. New York City: Scribner. OCLC 1020487 – via Google Books. Historic England (February 2017). Traditional Windows: Their Care, Repair and Upgrading. Swindon: Historic England. ISBN 978-1-84802-462-5. OCLC 1014451933. Archived from the original on December 31, 2017. Retrieved December 31, 2017 – via Historic England. MacCabe, Virginia Parsons (1913). Parsons' Family History and Record. Decatur, Illinois: Charles W. Nickey. OCLC 8590919. Retrieved July 9, 2016 – via Internet Archive. McMaster, Len (2010). "Hampshire County West Virginia Post Offices, Part 2" (PDF). LaPosta: A Journal of American Postal History. 41: 38–48. Archived from the original (PDF) on July 9, 2016. Retrieved July 9, 2016. Munske, Roberta R.; Kerns, Wilmer L., eds. (2004). Hampshire County, West Virginia, 1754–2004. Romney, West Virginia: The Hampshire County 250th Anniversary Committee. ISBN 978-0-9715738-2-6. OCLC 55983178. Rice, Otis K. (2015). The Allegheny Frontier: West Virginia Beginnings, 1730–1830. Lexington, Kentucky: University Press of Kentucky. ISBN 978-0-8131-6438-0. OCLC 900345296 – via Google Books. Rose, Cornelia Bruère (1976). Arlington County, Virginia: A History. Arlington County, Virginia: Arlington Historical Society. OCLC 2401541 – via Google Books. William and Mary Quarterly (April 1898). "The Northern Neck of Virginia". William and Mary Quarterly. 6 (4): 222–226. ISSN 0043-5597. JSTOR 1915885. OCLC 1607858. Zimmerman, Courtney Fint (August 24, 2012). National Register of Historic Places Registration Form: Valley View (PDF). National Park Service. Archived from the original (PDF) on July 9, 2016. Retrieved July 9, 2016.  Media related to Valley View (Romney, West Virginia) at Wikimedia Commons v t e Capon Springs North River Mills Historic District Washington Bottom Farm Brill Octagon House Capon Chapel Fort Van Meter French's Mill Hampshire County Courthouse Hebron Church Hickory Grove Hook's Tavern Kuykendall Polygonal Barn Nathaniel and Isaac Kuykendall House Literary Hall Old District Parsonage Old Pine Church Captain David Pugh House Scanlon Farm Sloan–Parker House Springfield Brick House Sycamore Dale Valley View Yellow Spring Mill Wilson-Wodrow-Mytinger House Fort Mill Ridge Civil War Trenches Capon Lake Whipple Truss Bridge Pin Oak Fountain v t e Contributing property Keeper of the Register Historic district History of the National Register of Historic Places National Park Service Property types Barbour Berkeley Boone Braxton Brooke Cabell Calhoun Clay Doddridge Fayette Gilmer Grant Greenbrier Hampshire Hancock Hardy Harrison Jackson Jefferson Kanawha Lewis Lincoln Logan Marion Marshall Mason McDowell Mercer Mineral Mingo Monongalia Monroe Morgan Nicholas Ohio Pendleton Pleasants Pocahontas Preston Putnam Raleigh Randolph Ritchie Roane Summers Taylor Tucker Tyler Upshur Wayne Webster Wetzel Wirt Wood Wyoming Bridges National Historic Landmarks  Category:National Register of Historic Places in West Virginia  Portal:National Register of Historic Places 1855 establishments in Virginia Buildings and structures in Romney, West Virginia Farms on the National Register of Historic Places in West Virginia Greek Revival houses in West Virginia Houses completed in 1855 Houses in Hampshire County, West Virginia Houses on the National Register of Historic Places in West Virginia National Register of Historic Places in Hampshire County, West Virginia Parsons family of West Virginia Plantation houses in West Virginia South Branch Valley Railroad CS1: Julian–Gregorian uncertainty Articles with short description Featured articles Use mdy dates from November 2018 Use American English from July 2016 All Wikipedia articles written in American English Coordinates on Wikidata Commons category link is on Wikidata Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version  This page was last edited on 29 September 2019, at 07:59 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Valley View ^ ^ ^ a b a b c d e f ^ a b a b c a b ^ ^ ^ a b c ^ ^ ^ ^ a b c ^ ^ ^ a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag a b c d e f g h a b c ^ a b c ^ ^ a b c d e a b a b c d e f g h i j k l m n o p a b a b c d e f g h i j k l m n o p q r a b c a b c d e ^ ^ ^ ^ 31 ^ ^ a b c d ^ ^ ^ ^ a b c d a b c d ^ ^ a b c d e f g h i j k l m n o a b a b c d e f g h i j k l m n ^ ^ ^ ^ a b 41 6 See also:  Valley View U.S. National Register of Historic Places 
 Valley View from the southeast, with Mill Creek Mountain in the background Valley ViewShow map of Eastern Panhandle of West VirginiaValley ViewShow map of West VirginiaValley ViewShow map of the United States Depot Valley RoadRomney, West Virginia, United States 39°21′23.4″N 78°45′35.25″W﻿ / ﻿39.356500°N 78.7597917°W﻿ / 39.356500; -78.7597917Coordinates: 39°21′23.4″N 78°45′35.25″W﻿ / ﻿39.356500°N 78.7597917°W﻿ / 39.356500; -78.7597917 6.63 acres (2.68 ha) 1855 Greek Revival 12001050[1] December 12, 2012 
Capon Springs
North River Mills Historic District
Washington Bottom Farm
  
Brill Octagon House
Capon Chapel
Fort Van Meter
French's Mill
Hampshire County Courthouse
Hebron Church
Hickory Grove
Hook's Tavern
Kuykendall Polygonal Barn
Nathaniel and Isaac Kuykendall House
Literary Hall
Old District Parsonage
Old Pine Church
Captain David Pugh House
Scanlon Farm
Sloan–Parker House
Springfield Brick House
Sycamore Dale
Valley View
Yellow Spring Mill
Wilson-Wodrow-Mytinger House
 
Fort Mill Ridge Civil War Trenches
 
Capon Lake Whipple Truss Bridge
Pin Oak Fountain
 See also: National Register of Historic Places listings in Hampshire County, West Virginia and List of National Historic Landmarks in West Virginia 
Contributing property
Keeper of the Register
Historic district
History of the National Register of Historic Places
National Park Service
Property types
  
Barbour
Berkeley
Boone
Braxton
Brooke
Cabell
Calhoun
Clay
Doddridge
Fayette
Gilmer
Grant
Greenbrier
Hampshire
Hancock
Hardy
Harrison
Jackson
Jefferson
Kanawha
Lewis
Lincoln
Logan
Marion
Marshall
Mason
McDowell
Mercer
Mineral
Mingo
Monongalia
Monroe
Morgan
Nicholas
Ohio
Pendleton
Pleasants
Pocahontas
Preston
Putnam
Raleigh
Randolph
Ritchie
Roane
Summers
Taylor
Tucker
Tyler
Upshur
Wayne
Webster
Wetzel
Wirt
Wood
Wyoming
 
Bridges
National Historic Landmarks
 
 Category:National Register of Historic Places in West Virginia
 Portal:National Register of Historic Places
 