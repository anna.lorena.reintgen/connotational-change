
 
 John Doubleday (about 1798[note 1] – 25 January 1856) was a British craftsperson, restorer, and dealer in antiquities who was employed by the British Museum for the last 20 years of his life. He undertook several duties for the museum, not least as a witness in criminal trials, but was primarily their specialist restorer, perhaps the first person to hold the position. He is best known for his 1845 restoration of the severely-damaged Roman Portland Vase, an accomplishment that places him at the forefront of his profession at the time.
 While at the British Museum, Doubleday also dealt in copies of coins, medals, and ancient seals. His casts in coloured sulphur and in white metal of works in both national and private collections, allowed smaller collections to hold copies at a fraction of the price that the originals would command. Thousands of his copies entered the collections of institutions and individuals. Yet the accuracy he achieved led to confusion with the originals; after his death he was labelled a forger, but with the caveat that "[w]hether he did copies with the intention of deceiving collectors or not is open to doubt".[6]
 Little is known about Doubleday's upbringing or personal life. Several sources describe him as an American, including the 1851 United Kingdom census, which records him as a New York-born British subject. An obituary noted that he worked at a printer's shop for more than 20 years during his youth, which gave him the experience of casting type that he would employ in his later career as a copyist. Doubleday's early life, family, and education are otherwise unknown. He died in 1856, leaving a wife and five daughters, all English; the eldest child was born around 1833.
 From 1836 to 1856 Doubleday worked in the Department of Antiquities at the British Museum.[7] He appears to have been employed as a freelancer who also occasionally acted as an agent in sales to the museum.[4] At times he presented the museum with items including coins, medals, and Egyptian objects.[8] Among other donations, his 1830 gift of 2,433 casts of medieval seals was the only significant donation recorded by the museum that year,[9][10] he offered several coins and another 750 casts the following year,[11] and in 1836, he presented the museum with a Henry Corbould lithograph of himself.[8] A further presentation in 1837 was still considered, in 1996, to be one of the museum's most important collections of casts of seals.[12] He seems to have been the museum's primary, and perhaps its first, dedicated restorer;[13][4] his death was described as leaving the post vacant.[14] At his death, it was noted that he was "chiefly employed in the reparation of innumerable works of art, which could not have been intrusted to more skilful or more patient hands",[2] and that he "was well known as one of the most valuable servants of that department".[1]
 The highlight of Doubleday's career came after 7 February 1845 when a young man, who later admitted having spent the prior week "indulging in intemperance", smashed the Portland Vase, an example of Roman cameo glass and among the most famous glass items in the world,[15] into hundreds of pieces.[16] After his selection for the restoration, Doubleday commissioned a watercolour painting of the fragments by Thomas H. Shepherd.[17] No account of his restoration survives,[18] but on 1 May he discussed it in front of the Society of Antiquaries of London,[19][20] and by 10 September he had glued the vase whole again.[17] Only 37 small splinters, most from the interior or thickness of the vase, were left out; the cameo base disc, which was found to be a modern replacement, was set aside for separate display.[21][22] A new base disc of plain glass, with a polished exterior and matte interior, was diamond-engraved "Broke Feby 7th 1845 Restored Sept 10th 1845 By John Doubleday".[17] The British Museum awarded Doubleday an additional £25 (equivalent to £2,500 in 2016) for his work.[23]
 At the time the restoration was termed "masterly"[24] and Doubleday was lauded by The Gentleman's Magazine for demonstrating "skilful ingenuity" and "cleverness ... sufficient to establish his immortality as the prince of restorers".[22] In 2006 William Andrew Oddy, a former keeper of conservation at the museum, noted that the achievement "must rank him in the forefront of the craftsmen-restorers of his time."[25] Doubleday's restoration would remain for more than 100 years until the adhesive grew increasingly discoloured.[26][27] The vase was next restored by J. W. R. Axtell in 1948–1949, and then by Nigel Williams in 1988–1989.[26][28][note 2]
 Beyond his work on the Portland Vase, several other of Doubleday's responsibilities at the British Museum have been recorded.[4] In 1851 he successfully undid damaging restoration work by William Thomas Brande of the Royal Mint, who in using acid to clean bronze bowls from Nimrud had caused extreme oxidation.[32] Doubleday's method, described at the time only as "a very simple process and without employing acids", is unknown, but may have used warm water with soap.[33]
 Doubleday was again called upon when, between 1850 and 1855, the museum received clay tablets from excavations in Babylonia and Assyria.[34] Some were poorly packaged and had developed crystalline deposits rendering the writing illegible.[34] Under the direction of Samuel Birch, then the keeper of the Department of Oriental Antiquities, Doubleday attempted to remove the deposits.[35] The results were described by E. A. Wallis Budge, former keeper of Egyptian and Assyrian antiquities at the museum, as "disastrous",[36] but by modern reasoning as "prescient", for though unsuccessful, the underlying methods were subsequently refined by others.[37] Doubleday first attempted to harden the tablets by firing them, but this resulted in the flaking of the surfaces, destroying the inscriptions.[38] His second attempt, submerging the tablets in solutions, also resulted in disintegration, at which point Birch suspended the efforts entirely.[39] Later attempts by other conservators in firing similar tablets were more successful; mainstream acceptance today is more tempered by concerns about reversibility than by concerns about efficacy.[40] Doubleday is regarded as the inventor of this method, and his failures may have been caused by raising or lowering the temperature too quickly.[41]
 Doubleday twice served as a witness in criminal matters.[4][42] In 1841 he testified about his analysis of a gold medal during a trial concerning its theft.[42][43] Eight years later Doubleday again testified, in March and April 1849, in a matter concerning the theft of coins from the museum.[4][44] Early in February, Timolean Vlasto, a fashionable twenty-four-year-old from Vienna whose late father, Count Vlasto, had been a diplomat,[45][46][47] had been introduced to Charles Newton (later Sir Charles) by a friend, who described Vlasto as a person interested in coins.[44][46][note 3] Vlasto was given unfettered access to the museum's collection.[44][46] Suspicions were aroused on 24 March, and on Monday the 26th a label was found on the floor; the coin that it described was missing.[44] Upon inspection many more coins could not be found, but some were recovered when a search warrant for Vlasto's lodgings was obtained on Thursday.[44][49] Doubleday was called to testify on Thursday or Friday; he stated that some of the coins exactly matched sulphur casts which he had made before the theft, and that the market value was between £3,000 and £4,000.[44] Vlasto, who was remanded without bail, claimed that the majority of the coins discovered were not the museum's.[44][50] On 17 April Doubleday again testified, identifying two more coins as belonging to the museum.[47][51][52] In early May Vlasto pleaded guilty to the theft of 266 coins from the museum, valued at £500, and another 71, valued at £150, from the house of General Charles Richard Fox.[46][53] Vlasto's lawyer termed him a monomaniac who was only interested in collecting, not selling.[46] The pleas met little sympathy.[54] Vlasto was sentenced by the Central Criminal Court to seven years transportation to Australia,[46][55][56] and in early 1851 was placed on board the Lady Kennaway for the journey.[57]
 Apart from his work at the British Museum, Doubleday was a dealer and a copyist of coins, medals, and ancient seals.[2] He sold sulphur and white metal casts, the former coloured in different hues, at his establishment,[58] which, located near the British Museum, may have helped facilitate his employment there.[2] He also sold curiosities,[4] such as cabinets, snuff boxes, and lead seals purportedly made from materials taken from the charred ruins of the Palace of Westminster with the consent of the Commissioners of Woods and Forests,[59][60][61][62][63] and pieces of wood said to be from a tree planted by Shakespeare.[64] In 1835 Doubleday advertised for sale copies of 6000 Greek coins, 2050 bronze, 1000 silver, and 500 gold Roman coins, and 300 Roman medallions,[58] in addition to other antiquities and what Doubleday termed "the most extensive Collection of Casts in Sulphur of ancient seals ever formed".[65] By 1851 he had casts of more than 10,000 seals,[66] and at his death it was said that he "possessed the largest collection of casts of seals in England, probably in the world."[67] This comprehensiveness led to his contribution to the 1848 Monumenta Historica Britannica of a descriptive catalogue of Roman coins relating to Britain.[68] More unique pieces he sometimes exhibited, either himself or by loan to Sir Henry Ellis, to the Society of Antiquaries of London.[69][70][71][72][73] Doubleday's casts came from a range of places;[74][75][76] on good terms with a variety of institutions and collectors, he was permitted to take casts at will from the collections of the British Museum and the Bibliothèque nationale in Paris.[74][77][78]
 Doubleday's casts were inexpensive, and sold widely.[58][79] He was well known among collectors,[80][81][82] and also sold to lyceums; University College London filled out their collection with his casts, finding them cost-effective substitutes for study.[58] This same appearance of realism saw some of Doubleday's copies passed off as real.[83][84] Doubleday was cast as a forger in Leonard Forrer's 1904 Biographical Dictionary of Medallists, though with the caveat that "[w]hether he did copies with the intention of deceiving collectors or not is open to doubt".[6][note 4]
 Little is known about Doubleday's personal life,[4] and nothing about his upbringing or education.[25] An 1859 edition of The English Cyclopædia described him as American,[86][note 6] and the 1851 census as a New York-born "artist" who was nonetheless a British subject, married to one Elizabeth and father of five daughters,[4] all Londoners.[5] His eldest daughter, also an Elizabeth, was born around 1833, suggesting that Doubleday and his wife had married by then.[4]
 Doubleday worked at a printer's shop in his youth for more than 20 years, according to his obituary in The Athenæum, giving him experience through making type in the casting of metal and other materials.[1] Subsequently, he began copying medals, ancient seals, and coins,[2][4] occasionally devising new methods of doing so;[87][66] he also prepared castings for the Royal Mint, and become a founding member of the Royal Numismatic Society.[4][2][88] By 1832 he was listed in directories under the heading "Curiosity, shell & picture dealers", and as a dealer in ancient seals.[4] As well as his work at the British Museum, he may have been a collector.[7]
 According to the obituary in The Athenæum, Doubleday died "after a long illness" on 25 January 1856, "in the fifty-seventh year of his age".[1] The illness was termed "extreme" by colleagues, such that he was unreachable for months.[89] Obituaries were published in The Athenæum and The Gentleman's Magazine,[1][2][note 7] and he was buried in Kensal Green Cemetery.[4] His will was made only six days before his death.[4] His entire estate was left to Elizabeth Bewsey, the daughter of a deceased bookkeeper; she was apparently not the Elizabeth to whom Doubleday was married, making it a bequest that seemingly left nothing for his wife or daughters.[4] His library was sold by Sotheby's that April.[4][91] The 322 lots combined to fetch £228 2s6d (equivalent to £22,800 in 2016).[92]
 To the memory
of
John Doubleday
of the parish of Bloomsbury
who departed this life
25th January 1856
aged 59 years

Also of Louisa Doubleday
fourth daughter of the above
who departed this life 30th Nov 1858
aged 11 years

Also of Julia Bewsey
youngest daughter of the above
who died January 19th 1886
aged 36 years

Also Elizabeth Doubleday
widow of the above
who died May 21st 1889
aged 83 years

Also of Eliza Doubleday
eldest daughter of the above
who died January 25th 1929
aged 90 years
 1 At the British Museum

1.1 Portland Vase
1.2 Other works

 1.1 Portland Vase 1.2 Other works 2 As a dealer 3 Personal life 4 Notes 5 References 6 Bibliography ^ Doubleday's year of birth, let alone date, is uncertain. Contemporary obituaries in The Athenæum and The Gentleman's Magazine describe his death as occurring in his fifty-seventh year,[1][2] which would indicate a birthday between January 1799 and January 1800. The 1851 census, recorded on 30 March,[3] puts his age at 53,[4][5] consistent with a birthday between March 1797 and March 1798. Cemetery records from his burial record his age as 59,[4] as does his headstone, indicating a birthday between January 1796 and January 1797.
 ^ A second reason for the second reconstruction was the rediscovery of the 37 pieces unincorporated by Doubleday.[29] Three of these, from the inside of the vase, were placed by Axtell.[30] By the end of the Williams reconstruction, only 17 fragments remained.[31]
 ^ Sixteen years later, while writing about catching a servant in the act of stealing from him, Newton would declare that "I have not seen so livid and hideous a complexion since the day when Timoleon Pericles Vlastò was detected stealing coins from the British Museum."[48]
 ^ By the 1923 edition, references to Doubleday being a forger were removed from the dictionary.[85]
 ^ The inscription reads:

To the memory
of
John Doubleday
of the parish of Bloomsbury
who departed this life
25th January 1856
aged 59 years

Also of Louisa Doubleday
fourth daughter of the above
who departed this life 30th Nov 1858
aged 11 years

Also of Julia Bewsey
youngest daughter of the above
who died January 19th 1886
aged 36 years

Also Elizabeth Doubleday
widow of the above
who died May 21st 1889
aged 83 years

Also of Eliza Doubleday
eldest daughter of the above
who died January 25th 1929
aged 90 years


 ^ Doubleday was also noted as being one of two with the same surname employed at the British Museum at the same time; the other, Edward Doubleday, was an unrelated English entomologist.[86]
 ^ The latter was republished in The Annual Register the following year.[90]
 ^ a b c d e Athenæum 1856a, p. 140.
 ^ a b c d e f g Gentleman's Magazine 1856, p. 431.
 ^ National Archives 2010.
 ^ a b c d e f g h i j k l m n o p q r Simon 2018.
 ^ a b English Census 1851.
 ^ a b Forrer 1904.
 ^ a b British Museum Doubleday.
 ^ a b British Museum acquisitions 1848.
 ^ British Museum benefactors 1883, p. xxvii.
 ^ Fennell 1842, p. 56.
 ^ British Museum acquisitions 1833, pp. 155, 212.
 ^ Harvey & McGuinness 1996, p. 25.
 ^ Oddy 1993, pp. 10–11.
 ^ Budge 1925, p. 150.
 ^ Journal of Glass Studies Foreword 1990.
 ^ Painter & Whitehouse 1990, p. 65.
 ^ a b c Painter & Whitehouse 1990, p. 69.
 ^ Williams 1989, pp. 5–6.
 ^ Archaeologia Appendix 1846, p. 500.
 ^ Gentleman's Magazine 1846b, p. 409.
 ^ Painter & Whitehouse 1990, pp. 69–71, 82–83.
 ^ a b Gentleman's Magazine 1846a, p. 41.
 ^ Wilson 2002, p. 112.
 ^ Morning Post 1845.
 ^ a b Oddy 2006.
 ^ a b Painter & Whitehouse 1990, pp. 82–84.
 ^ Williams 1989, pp. 5–6, 29.
 ^ Williams 1989.
 ^ Painter & Whitehouse 1990, p. 82.
 ^ Painter & Whitehouse 1990, p. 83.
 ^ Williams 1989, p. 19.
 ^ Reade 2008, pp. 19–20.
 ^ Reade 2008, p. 20.
 ^ a b Budge 1925, pp. 147–148.
 ^ Budge 1925, pp. xviii, 148.
 ^ Budge 1925, pp. ii, 148.
 ^ Reade 2017, p. 182.
 ^ Budge 1925, p. 148.
 ^ Budge 1925, pp. 148–149.
 ^ Reade 2017, p. 197.
 ^ Reade 2017, pp. 182, 184.
 ^ a b Westgarth 2009, p. 89.
 ^ Old Bailey Proceedings 1841.
 ^ a b c d e f g Morning Post 1849.
 ^ Pembrokeshire Herald 1849.
 ^ a b c d e f Akerman 1850.
 ^ a b The Era 1849.
 ^ Newton 1865, p. 17.
 ^ The Spectator 1849a.
 ^ Morning Chronicle 1849.
 ^ Bury and Norwich Post 1849.
 ^ The Spectator 1849b.
 ^ The Examiner 1849.
 ^ Macaulay 2008, p. 64.
 ^ Maitland Mercury 1849.
 ^ Old Bailey Proceedings 1849.
 ^ Pickup 2017.
 ^ a b c d Silliman 1835, p. 75.
 ^ Sotheby's catalogue 1839, p. 26.
 ^ Archaeologia Presents 1836, p. 490.
 ^ Shenton 2015.
 ^ Millett snuff box.
 ^ Millett lead seal.
 ^ Millett Shakespeare's tree.
 ^ Akerman 1834.
 ^ a b Winkles & Winkles 1851, p. vii n.14.
 ^ Sims 1861, p. 299.
 ^ Petrie, Sharpe & Hardy 1848, pp. clii–clxxiii.
 ^ Archaeologia Appendix 1836, pp. 460–461.
 ^ Vincent 2015, p. 10 n.23.
 ^ Gentleman's Magazine 1836, p. 81.
 ^ Gentleman's Magazine 1845, p. 176.
 ^ Archaeologia Appendix 1847, p. 408.
 ^ a b Silliman 1835, pp. 75–77.
 ^ Green 1857, p. 148 n.3.
 ^ Madden 1856, p. 367 n.5.
 ^ British Museum Select Committee 1836, p. 490.
 ^ Mechanics' Magazine 1837.
 ^ Rose 1850–1851, pp. 158–159, 165.
 ^ H. 1837, p. 338.
 ^ Brown 1845, p. 30.
 ^ Notes and Queries 1858.
 ^ Numismatic Chronicle 1849, p. 136.
 ^ Williams 1993.
 ^ Forrer 1923.
 ^ a b Knight 1859, p. 378.
 ^ Burgon 1841.
 ^ Carson 1986, pp. 3, 5–6, 59.
 ^ Walford & Way 1856, pp. 70–71.
 ^ The Annual Register 1857, p. 235.
 ^ Athenæum 1856b.
 ^ Sotheby's catalogue 1856, p. 20.
 Akerman, John Yonge (1834). "Impressions of Roman and Greek Coins, Seals, &c.". A Descriptive Catalogue of Rare and Unedited Roman Coins. London: Effingham Wilson. p. 513..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}  Akerman, John Yonge, ed. (January 1850). "The Robbery at the British Museum". The Numismatic Chronicle and Journal of the Royal Numismatic Society. XII: 51. JSTOR 42680699.  Cave, Edward; Nichols, John (July 1836). "Antiquarian Researches: Society of Antiquaries". The Gentleman's Magazine. VI: 81–82.  "Antiquarian Researches: Society of Antiquaries". The Gentleman's Magazine. XXIV: 175–178. August 1845.  Sylvanus Urban, Gent (April 1846). "Antiquarian Researches: Society of Antiquaries". The Gentleman's Magazine. XXV: 406–410.  "Appendix". Archaeologia. XXVI: 455–485. 1836.  "Appendix". Archaeologia. XXXI: 455–521. 1846.  London, Society of Antiquaries of (1847). "Appendix". Archaeologia. XXXII: 389–457.  "Appendix: Deaths". The Annual Register, or a View of the History and Politics of the Year 1856. 98. London: F. & J. Rivington. 1857. pp. 226–290.  "The British Museum". Fine Arts. The Morning Post. London. 11 July 1845. p. 6.  "The British Museum". Police Intelligence. The Morning Post. London. 31 March 1849. p. 8.  Brown, Abner W. (1845). "Account of Researches for the Discovery of the Ancient Seal of the Archdeaconry of Northampton; with Remarks on the Subject of Ecclesiastical Seals in General". Report of the Architectural Society of the Archdeaconry of Northampton. 2: 27–66.  Budge, E. A. Wallis (1925). The Rise & Progress of Assyriology. London: Martin Hopkinson & Co.  Burgon, John William (January 1841). "On a New Method of Obtaining Representations of Coins". The Numismatic Chronicle. III: 190–192.  Carson, Robert (1986). A History of the Royal Numismatic Society 1836-1986 (PDF). London: Royal Numismatic Society. ISBN 978-0-901405-24-1.  Catalogue of the First Portion of the Very Extensive, Highly Valuable and Important Collection of Coins and Medals, in Gold, Silver, and Copper, of the Late Eminent Numismatist, Mr. Matthew Young. London: S. Leigh Sotheby. 1839.  Catalogue of the Miscellaneous Library of the Late Mr. John Doubleday. London: S. Leigh Sotheby & John Wilkinson. 1856. OCLC 82189166. "Census Records". National Archives. 14 December 2010. Retrieved 6 April 2018.  Fennell, James H. (22 January 1842). "A Familiar Description of the British Museum and its Contents". The Mirror of Literature, Amusement, and Instruction. I (4): 55–59.  "Foreword". Journal of Glass Studies. 32: 12. 1990. JSTOR 24188027. "Forged and Imitation Coins". The Numismatic Chronicle and Journal of the Royal Numismatic Society. XI: 185–187. January 1849. JSTOR 42686171.  Forrer, Leonard, ed. (1904). "Doubleday (Brit.)". Biographical Dictionary of Medallists. I. London: Spink & Son. p. 612.  Forrer, Leonard, ed. (1923). "Doubleday, J.". Biographical Dictionary of Medallists. VII. London: Spink & Son. p. 231.  "The Greek Criminal". British Extracts. The Maitland Mercury and Hunter River General Advertiser. Maitland, New South Wales. 13 October 1849. p. 1.  Green, Mary Anne Everett (1857). Lives of the Princesses of England. I. London: Lonman, Brown, Green, Longman, & Roberts.  H., D. (April 1837). "Minor Correspondence". The Gentleman's Magazine. VII: 338.  Harvey, Paul D. A.; McGuinness, Andrew (1996). A Guide to British Medieval Seals. Toronto: University of Toronto Press. ISBN 978-0-8020-0867-1. "James Stephens". The Proceedings of the Old Bailey. Retrieved 29 March 2018.  "John Doubleday (Biographical details)". The British Museum. Retrieved 20 March 2018. "John Doublday: England and Wales Census, 1851". FamilySearch. Retrieved 29 March 2018. Knight, Charles, ed. (1859). "British Museum, The". The English Cyclopædia: Arts and Sciences. II. London: Bradbury & Evans. pp. 368–403.  List of Additions in the Natural History, Antiquities, and Prints of the British Museum in the Years MDCCCXXXVI – MDCCCXXXIX. London: George Woodfall and Son. 1848.  List of Additions Made to the Collections in the British Museum in the Year MDCCCXXXI. London: George Woodfall and Son. 1833.  "List of Benefactors to the British Museum from Whom Donations of Magnitude Have Been Received". A Guide to the Exhibition Galleries of the British Museum, Bloomsbury. London: Trustees of the British Museum. 1883. pp. xxi–xxxvi.  Macaulay, Thomas Babington (2008).  Thomas, William (ed.). The Journals of Thomas Babington Macaulay. 2. London: Pickering & Chatto. ISBN 978-1-85196-903-6. Madden, Frederic (1856). "Remarks on the Anglo-Saxon Charters Granted to the Abbey of St. Denis, in France, and on the Seals Attached to them". The Archaeological Journal. XIII: 355–371.  "Mediæval Seals (2nd S. v. 274.)". Notes and Queries. 2nd. V (122): 367. 1 May 1858.  "The Metropolis". The Spectator (1084): 313. 7 April 1849.  "The Metropolis". The Spectator (1086): 364. 21 April 1849.  Millett, Timothy. "David Garrick / Shakespeare's Mulberry Tree". Timothy Millett Limited. Retrieved 2 April 2018.  Millett, Timothy. "Fire at the Palace of Westminster, Turned Wood Snuff Box". Timothy Millett Limited. Retrieved 2 April 2018.  Millett, Timothy. "St. Stephens Westminster". Timothy Millett Limited. Retrieved 2 April 2018.  "Miscellaneous and Numismatic Library of the late Mr. John Doubleday, also some Etruscan Vases and Miscellaneous Articles". The Athenæum (1487): 507. 26 April 1856.  "Mr. John Doubleday". Mechanics' Magazine. XXVII (704): 339. 4 February 1837.  Newton, Charles Thomas (1865). Travels & Discoveries in the Levant. II. London: Day & Son.  "Obituary: Mr. John Doubleday". The Gentleman's Magazine. XLV: 431–432. 1856.  Oddy, William Andrew (1993). "The History of and Prospects for the Conservation of Metals in Europe". Current Problems in the Conservation of Metal Antiquities. Tokyo: Tokyo National Research Institute of Cultural Properties. pp. 1–27. Oddy, William Andrew (2006). "Doubleday, John".  In Panzeri, Matteo & Gimondi, Cinzia (eds.). Amplius Vetusta Servare: First Results of the European Project. Saonara, Italy: Il Prato. p. 109. ISBN 978-88-900741-7-2. "Our Weekly Gossip". The Athenæum (1475): 139–140. 2 February 1856.  Painter, Kenneth & Whitehouse, David (1990). "The History of the Portland Vase". Journal of Glass Studies. 32: 24–84. JSTOR 24188030.  Petrie, Henry; Sharpe, John & Hardy, Thomas Duffus (1848). Monumenta Historica Britannica, or Materials for the History of Britain. I.  Pickup, David (November 2017). "The Strange Tale of Timoleon Vlasto". Coin News. 54 (11): 53–54. "The Portland Vase and the Sarcophagus in which it was Found". The Gentleman's Magazine. XXV: 41–44. January 1846.  "Presents to the Society". Archaeologia. XXVI: 487–499. 1836.  Reade, Julian Edgeworth (2008). "Nineteenth-Century Nimrud: Motivation, Orientation, Conservation".  In Curtis, John E.; McCall, Henrietta; Collon, Dominique & al-Gailani Werr, Lamia (eds.). New Light on Nimrud: Proceedings of the Nimrud Conference, 11th–13th March 2002 (PDF). London: British Institute for the Study of Iraq. pp. 1–21. ISBN 978-0-903472-24-1.  Reade, Julian Edgeworth (December 2017). "The Manufacture, Evaluation and Conservation of Clay Tablets Inscribed in Cuneiform: Traditional Problems and Solutions". Iraq. LXXIX: 163–202. doi:10.1017/irq.2016.10.  Report from the Select Committee on British Museum. London: House of Commons. 1836.  "The Robbery of Coins at the British Museum". Domestic Intelligence. The Bury and Norwich Post. Bury, Suffolk. 25 April 1849. p. 1 – via Newspapers.com.  "The Robbery of Coins at the British Museum, &c". Bow-Street. The Era. London. 22 April 1849. p. 14 – via Newspapers.com.  "Robbery of Coins at the British Museum". Central Criminal Court. The Examiner (2, 154). London. 12 May 1849. p. 300 – via Newspapers.com.  Rose, Henry John (1850–1851). "Introductory Memoir on some Seals of Bedfordshire". Reports and Papers of the Architectural and Archaeological Societies of the Counties of Lincoln and Northampton. I: 154–165.  Shenton, Caroline (13 January 2015). "Snuff's Enough". Caroline Shenton. Retrieved 2 April 2018.  Silliman, Benjamin, ed. (January 1835). "Miscellaneous Communications from an American Naval Officer, travelling in Europe; forwarded from the Mediterranean, May, 1834". The American Journal of Science and Arts. XXVII (I): 74–84.  Simon, Jacob (March 2018). "British Picture Restorers, 1600–1950 – D". National Portrait Gallery. Retrieved 14 September 2017.  Sims, Richard (1861). A Manual for the Genealogist, Topographer, Antiquary, and Legal Professor. London: John Russell Smith.  "Timoleon Vlasto". Police Intelligence – Friday. The Morning Chronicle (24, 787). London. 31 March 1849. p. 8 – via Newspapers.com.  "Timoleon Vlasto". Miscellaneous. The Pembrokeshire Herald and General Advertiser. VI (CCLXXVI). Pembrokeshire, Wales. 13 April 1849. p. 3.  "Timonion Ulasto". The Proceedings of the Old Bailey. Retrieved 28 March 2018.  Vincent, Nicholas (2015). "The Seals of King Henry II and his Court".  In Schofield, Phillipp R. (ed.). Seals and their Context in the Middle Ages. Oxford: Oxbow Books. pp. 7–33. ISBN 978-1-78297-817-6.  Walford, Weston S. & Way, Albert (March 1856). "Examples of Mediæval Seals". The Archaeological Journal. XIII: 62–76.  Westgarth, Mark (2009). "A Biographical Dictionary of Nineteenth Century Antique & Curiosity Dealers" (PDF). Regional Furniture. XXIII: 1–204.  Williams, David H. (1993). Catalogue of Seals in the National Museum of Wales. I. Cardiff: National Museum of Wales. ISBN 978-0-7200-0381-9. Williams, Nigel (1989). The Breaking and Remaking of the Portland Vase. London: British Museum Publications. ISBN 978-0-7141-1291-6. Wilson, David M. (2002). The British Museum: A History. London: British Museum Press. ISBN 978-0-7141-2764-4. Winkles, Henry & Winkles, Benjamin (1851). "Introduction". Winkles's Architectural and Picturesque Illustrations of the Cathedral Churches of England and Wales. I. London: David Bogue. pp. i–xx.  GND: 1196383936 VIAF: 2699157040166267040006  WorldCat Identities (via VIAF): 2699157040166267040006 1790s births 1856 deaths American emigrants to the United Kingdom Antiquarians Coin designers Conservator-restorers Employees of the British Museum Medallists Naturalised citizens of the United Kingdom People from New York (state) Typesetters Featured articles Use dmy dates from March 2018 Use British English from March 2018 Biography with signature Articles with hCards CS1: Julian–Gregorian uncertainty Wikipedia articles with GND identifiers Wikipedia articles with VIAF identifiers Wikipedia articles with WorldCat-VIAF identifiers Date of birth unknown Year of birth unknown Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Deutsch  This page was last edited on 22 February 2019, at 20:30 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  
To the memory
of
John Doubleday
of the parish of Bloomsbury
who departed this life
25th January 1856
aged 59 years

Also of Louisa Doubleday
fourth daughter of the above
who departed this life 30th Nov 1858
aged 11 years

Also of Julia Bewsey
youngest daughter of the above
who died January 19th 1886
aged 36 years

Also Elizabeth Doubleday
widow of the above
who died May 21st 1889
aged 83 years

Also of Eliza Doubleday
eldest daughter of the above
who died January 25th 1929
aged 90 years

 John Doubleday ^ ^ ^ ^ ^ ^ ^ a b c d e a b c d e f g ^ a b c d e f g h i j k l m n o p q r a b a b a b a b ^ ^ ^ ^ ^ ^ ^ ^ a b c ^ ^ ^ ^ a b ^ ^ a b a b ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ a b ^ a b c d e f g ^ a b c d e f a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b c d ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ XII VI XXIV XXV XXVI XXXI XXXII 98 2 III I 32 XI I VII I VII II 2 XIII V XXVII II XLV 32 I 54 XXV XXVI LXXIX I XXVII VI XIII XXIII I I John Doubleday around 1845 with his restoration of the Portland Vase, and a watercolour of the shattered fragments About  1798New York, U.S. (1856-01-25)25 January 1856 British Restorer, dealer 1836–1856 Reconstructing the Portland Vase  
GND: 1196383936
VIAF: 2699157040166267040006
 WorldCat Identities (via VIAF): 2699157040166267040006
 