
 Ernest Edward Mills Joyce AM (c. 1875 – 2 May 1940) was a Royal Naval seaman and explorer who participated in four Antarctic expeditions during the Heroic Age of Antarctic Exploration, in the early 20th century. He served under both Robert Falcon Scott and Ernest Shackleton. As a member of the Ross Sea party in Shackleton's Imperial Trans-Antarctic Expedition, Joyce earned an Albert Medal for his actions in bringing the stricken party to safety, after a traumatic journey on the Great Ice Barrier. He was awarded the Polar Medal with four bars, one of only two men to be so honoured, the other being his contemporary, Frank Wild.
 Joyce came from a humble seafaring background and began his naval career as a boy seaman in 1891. His Antarctic experiences began 10 years later, when he joined Scott's Discovery Expedition as an Able Seaman. In 1907 Shackleton recruited Joyce to take charge of dogs and sledges on the Nimrod Expedition. Subsequently Joyce was engaged in a similar capacity for Douglas Mawson's Australasian Antarctic Expedition in 1911, but left the expedition before it departed for the Antarctic. In 1914 Shackleton recruited Joyce for the Ross Sea party; despite his heroics this expedition marked the end of Joyce's association with the Antarctic, and of his exploring career, although he made repeated attempts to join other expeditions.
 Throughout his career Joyce was known as an abrasive personality who attracted adverse as well as positive comments. His effectiveness in the field was widely acknowledged by many of his colleagues, but other aspects of his character were less appreciated – his capacity for bearing grudges, his boastfulness and his distortions of the truth. Joyce's diaries, and the book he wrote based on them, have been condemned as self-serving and the work of a fabulist. He made no significant material gains from his expeditions, living out his post-Antarctic life in humble circumstances before dying in 1940.
 Joyce's Naval Service Record show his place and date of birth at Feltham, Sussex, 22 December 1875. [1]. Kelly Tyler-Lewis, in her account of the Ross Sea party, quotes a newspaper report giving Joyce's age as 64 in 1939, indicating birth year as 1875 – although she also gives his age as 29 in 1901, suggesting an earlier birth year.[2] Joyce's father and grandfather had both been sailors, his father probably within the coastguard service.[3] After the father's early death his widow, with three children to support on her limited earnings as a seamstress, sent the young Ernest to the Lower School of Greenwich Royal Hospital School for Navy Orphans at Greenwich. Here, in austere surroundings, he received a vocational education that would prepare him for a lower-deck career in the Royal Navy. After leaving the school in 1891, he joined the navy as a boy seaman, progressing over the next ten years to Ordinary Seaman and then Able Seaman.[4] Joyce had blue eyes and a fair complexion, with a tattoo on his left forearm and a scar on his right cheek. He was not a tall man, only 5' 7" in height. [5].
 British Naval Archive Records at Portsmouth - No: 160823 - provide full details on Joyce’s early naval service. This commenced in May 1891 as a Boy Second Class on the St. Vincent, and over the following ten years he served on a number of ships; the Boscawen, Alexandra, Victory 1, Duke of Wellington, etc. [6]. In 1891 he was serving on HMS Gibraltar in Cape Town where, in September, Scott's expedition ship Discovery stopped on the way to the Antarctic. Scott was short-handed, and requested volunteers; from a response of several hundreds, Joyce was one of four seamen chosen to join Discovery. He sailed south with her on 14 October 1901.[4][7]
 The Discovery expedition was Joyce's Antarctic baptism, although for the next three years he kept a relatively low profile; Scott scarcely mentions him in The Voyage of the Discovery, and Edward Wilson's diaries not at all. It seems that he took readily to Antarctic life,[8] gaining experience in sledging and dog-driving techniques and other aspects of Antarctic exploration. He did not figure in the main journeys of the expedition, although towards the end he joined Arthur Pilbeam and Frank Wild in an attempt to climb Mount Erebus, ascending to some 3,000 feet (910 m).[9] Joyce was at times badly affected by frostbite; on one occasion two officers, Michael Barne and George Mulock, held Joyce's frostbitten foot against the pits of their stomachs and kneaded the ankle for several hours to save it from amputation.[10] However, such experiences left Joyce undaunted; the polar historian Beau Riffenburgh writes that Joyce was repeatedly drawn to the Antarctic by "a curious combination of affection and antipathy" that "impelled [him] to return again and again".[11]
 During the expedition Joyce encountered several men who would feature prominently in Antarctic polar history during the following years, including Scott, Wilson, Frank Wild, Tom Crean, William Lashly, Edgar Evans and, most significantly, Ernest Shackleton. Joyce made several sledging trips with Shackleton[12] and created an impression of competence and reliability. He also impressed Scott as "sober, honest, loyal and intelligent",[4] and expedition organiser Sir Clements Markham later described him as "an honest and trustworthy man".[4] His reward, at the conclusion of the expedition, was promotion to Petty Officer 1st Class on Scott's recommendation.[4] However, he had been bitten by the bug of Antarctic exploration,[13] and ordinary naval duty no longer appealed. He left the navy in 1905 but found shore life unsatisfying and re-enlisted in 1906. When the chance came a year later to join Shackleton's Nimrod Expedition, he took it immediately.[4]
 When Shackleton was selecting the crew for his Antarctic expedition in Nimrod, Joyce was one of his earliest recruits. Most accounts tell the story that Shackleton saw Joyce on a bus that was passing his expedition offices, sent someone out to fetch him, and recruited him on the spot.[12][14][15] To join the expedition, Joyce bought his release from the Navy; in later years he would claim that Shackleton had failed to recompense him for this, despite a promise to do so, one of several disputes over money and recognition which would strain his relations with Shackleton.[11][16] Joyce, Shackleton and Frank Wild were the only members of the expedition with previous Antarctic experience, and on the basis of his Discovery exploits, Joyce was put in charge of the new expedition's general stores, sledges and dogs. Before departure in August 1907, he and Wild took a crash course in printing at Sir Joseph Causton's printing firm in Hampshire, as Shackleton intended to publish a book or magazine while in the Antarctic.[17]
 Nimrod left New Zealand on 1 January 1908, and as a fuel-saving measure was towed towards the Antarctic pack ice by the tug Koonya.[18] On 23 January, by now under her own power, she reached the Ross Ice Shelf (then known as the "Great Ice Barrier", or "Barrier"), where Shackleton planned to base his headquarters in an inlet discovered during the Discovery voyage. This proved impossible; the inlet, where Scott and Shackleton had taken balloon flights in February 1902, had greatly expanded to become an open bay, christened the "Bay of Whales".[19] Shackleton was convinced that the ice was not secure enough as a landing ground, and could find no feasible alternative site on nearby King Edward VII Land. Before leaving for the Antarctic Shackleton had promised Scott that he would not base his expedition in or near Scott's former headquarters in McMurdo Sound. Shackleton was now forced to break this agreement, and take Nimrod to the safer waters of McMurdo Sound.[20] The site finally chosen as a base was at Cape Royds, some 20 miles (32 km) north of Scott's old Discovery headquarters at Hut Point. During the extended and often difficult process of unloading the ship Joyce remained ashore, looking after the dogs and ponies, and helping to build the expedition hut.[21] Joyce was witness to an incident during unloading, where a crate hook attached to a barrel swung across and struck one of the watching officers - Aeneas Mackintosh - on the face.[22] (Mackintosh's right eye was virtually destroyed and later that day the expedition doctor operated to remove the eye.) In March Joyce assisted the party that made the first successful ascent of Mount Erebus, although he did not make the climb himself.[23]
 During the following winter Joyce, with Wild's help, printed copies of the expedition book Aurora Australis, edited by Shackleton. About 25 or 30 copies of the book were printed, sewn and bound.[24] Otherwise Joyce was busy preparing equipment and stores for the next season's journey to the Pole in which, in view of his experience, he fully expected to be included. However, various mishaps had reduced the number of ponies to four, so Shackleton cut the southern party to that number. One of those dropped was Joyce, on advice from expedition doctor Eric Marshall, who noted that Joyce had a liver problem and the early stages of heart disease.[25] Frank Wild, who along with Marshall and Jameson Adams was selected for the southern journey, wrote in his diary after the party's bid to reach the Pole had fallen short: "If we only had Joyce and Marston here instead of these two useless grub-scoffing beggars"—Marshall and Adams—"we would have done it easily."[26] Joyce showed no particular resentment at his exclusion; he assisted the preparatory work and accompanied the polar party on the southward march for the first seven days.[27] In the following months he took charge of enhancing the depots, to ensure adequate supplies for the returning southern party. He deposited a special cache of luxuries at Minna Bluff, together with life-saving food and fuel, earning Wild's spontaneous praise when the cache was discovered.[28]
 Shackleton and his party returned safely from their polar journey, on Nimrod's last feasible date for sailing home. They had established a new Farthest South at 88°23′S, only 97 nautical miles (180 km; 112 mi) from the South Pole. Joyce had been ready to remain at the base with a rearguard, to wait for the party or to establish its fate if it did not return in time to catch the ship.[29] Nimrod finally reached London in September 1909 and was prepared, under Joyce's direction, as a floating exhibition of polar artefacts. Shackleton paid him a salary of £250 a year, equivalent to £26,000 in 2018, a generous amount for the time.[4]
 Joyce was not invited to join Scott's Terra Nova Expedition, although several of Shackleton's men were, including Frank Wild who declined. Instead, Joyce and Wild both signed up for Douglas Mawson's Australasian Antarctic Expedition. In 1911 Joyce travelled to Denmark to acquire dogs for this expedition, and took them on to Tasmania. Joyce did not subsequently sail with Mawson. According to one account he was "dismissed" before the expedition left Australia,[30] while another suggests that Joyce was dropped when Mawson reduced his expedition from three shore parties to two.[31] Whatever the reason, it appears that there was a falling-out; Mawson reportedly distrusted Joyce, saying that "he spent too much time in hotels", which suggests that drink was an issue.[32] Joyce remained in Australia, obtaining work with the Sydney Harbour Trust.[33]
 In February 1914 Joyce, still in Australia, was contacted by Shackleton. who outlined plans for his Imperial Trans-Antarctic Expedition. Shackleton wanted Joyce in the expedition's supporting Ross Sea party; should the plans change to a one-ship format, Shackleton promised to find a different role for Joyce within the expedition.[34] Joyce would later claim without justification that Shackleton had offered him a place on the main transcontinental party.[35] In his subsequent book, The South Polar Trail published in 1929, Joyce also misrepresented the nature of his appointment to the Ross Sea party, omitting Shackleton's order that placed him under an officer and claiming that he had been given sole authority over dogs and sledging.[35]
 The task of the Ross Sea party, under the command of another Nimrod veteran, Aeneas Mackintosh, was to establish a base in McMurdo Sound and then lay a series of supply depots across the Ross Ice Shelf to assist the transcontinental party. Shackleton saw this task as routine; he wrote: "I had not anticipated that the work would present any great difficulties".[36] However, the party had been assembled rather hurriedly, and was inexperienced.[37] Only Joyce and Mackintosh had been to the Antarctic before, and Mackintosh's participation in polar work had been brief; he had been invalided from the Nimrod Expedition before the initial landing, after an accident led to the loss of his right eye and had returned only for the final stages of the expedition[38]
 Aurora's departure from Australia was delayed by a series of organisational and financial setbacks,[39] and the party did not arrive in McMurdo Sound until 16 January 1915—very late in the season for depot-laying work. Mackintosh, who believed that Shackleton might attempt to cross the continent in that first season, insisted that sledging work should begin without delay, with a view to laying down supply depots at 79° and 80°S.[40] Joyce opposed this; more time, he maintained, should be set aside to acclimatise and train men and dogs.[41] However he was over-ruled by Mackintosh, who was unaware that Shackleton had ruled out a crossing that season.[42] Joyce's diary notes of January the 24th detail his frustrations: 
 "After breakfast Skipper + I discussed several details.  I could not get him to see that we were jeopardizing the dogs + I cannot quite understand why Shacks should alter his plan of campaign. As for wintering the ship - this to my mind is the silliest damn rot that could have possibly occurred.  The wintering of the Discovery was quite alright in its way, but then we had no experience of Antarctic conditions.  If I had Shacks here I would make him see my way of arguing.
 Anyway Mack is my Boss + I must uphold him until I find that he is not fit to carry out the hard tedious work that is in front of us.  Having one eye will play merry hell with him in the extreme temperatures.  As he will not take my advice about the dogs I must let him have his way.''" [43]
 Mackintosh further vexed Joyce by deciding to lead this depot-laying party himself, unmoved by Joyce's claim to have independent authority over this area.[44] The party was divided into two teams, and the journey began on 24 January, in an atmosphere of muddle. Initial attempts at travelling on the Barrier were thwarted by the condition of the surface, and Mackintosh's team got lost on the sea ice between Cape Evans and Hut Point. Joyce privately gloated over this evidence of the captain's inexperience.[45] The teams eventually reached the 79° mark, and laid the "Bluff depot" there (Minna Bluff was a prominent visible landmark at this latitude) on 9 February. It seemed that Joyce's party had enjoyed the easier journey.[46] Mackintosh's plan to take the dogs on to the 80° mark led to more words between him and Joyce,[47] who argued that several dogs had already died and that the remainder needed to be kept for future journeys, but again he was over-ruled. On 20 February the party reached the 80° latitude and laid their depot there.[48] The outcome of this journey was 105 lb (48 kg) of provisions and fuel at 80°S and 158 lb (72 kg) at 79°S. But a further 450 lb (200 kg), intended for the depots, had been dumped on the journey, to save weight.[49]
 By this time men and dogs were worn out. On the return journey, in appalling Barrier weather,[n 1] all the dogs perished, as Joyce had predicted, and the party returned to Hut Point on 24 March exhausted and severely frostbitten.[51] After being delayed for ten weeks at Hut Point by the condition of the sea ice, the party finally got back to their base at Cape Evans on 2 June. They then learned that Aurora, with most of the shore party's stores and equipment still aboard, had been torn from its moorings in a gale, and blown far out to sea with no prospect of swift return. Fortunately, the rations for the next season's depot-laying had been landed before the ship's involuntary departure.[52] However, the shore party's own food, fuel, clothing and equipment had been largely carried away; replacements would have to be improvised from supplies left at Cape Evans after Scott's 1910–13 Terra Nova expedition, augmented by seal meat and blubber.[52] In these circumstances Joyce proved his worth as a "master scavenger" and improviser,[53] unearthing from Scott's abandoned stores, among other treasures, a large canvas tent from which he fashioned roughly tailored clothing. He also set about stitching 500 calico bags, to hold the depot rations.[54]
 The party set out on 1 September 1915. The men were under-trained and half-fit, in primitive clothing and with home-made equipment.[55] With only five dogs remaining from the previous season's debacle,[56] the task would mostly be one of manhauling. Before beginning the march south—a return distance of 800 nautical miles (1,500 km; 920 mi)—approximately 3,800 pounds (1,700 kg) of stores had to be taken to the base depot at Minna Bluff.[55] This phase of the task lasted until 28 December. Mackintosh had divided his forces into two parties, himself in charge of one and Joyce of the other. The two men continued to disagree over methods; finally, Joyce confronted Mackintosh with incontrovertible evidence that his party's methods were much the more effective, and Mackintosh capitulated. "I never came across such an idiot in charge of men", Joyce wrote in his diary.[57]
 The weaker members of the party—Arnold Spencer-Smith and Mackintosh himself—were by this time showing signs of physical breakdown,[58] as the long march south began from Bluff Depot towards Mount Hope at 83°30′S, where the final depot was to be laid. The party was reduced to six when three men were forced to turn back because of a Primus stove failure.[59] With Mackintosh and Joyce in the final party were Spencer-Smith, Ernest Wild (younger brother of Frank), Dick Richards and Victor Hayward. With four dogs they trekked southward, increasingly afflicted by frostbite, snow blindness and, eventually, scurvy. Spencer-Smith collapsed, and thereafter had to be carried on the sledge.[60] Mackintosh, barely able to walk, fought on until the final depot was laid at Mount Hope. On the homeward journey the effective leadership of the party fell increasingly to Joyce, as Mackintosh's condition deteriorated until, like Spencer-Smith, he had to be carried on the sledge.[61] The journey became a protracted struggle which eventually cost the life of Spencer-Smith and took the others to the limits of their endurance. Mackintosh suffered further physical and mental collapse, and had to be left in the tent while Joyce, himself suffering from severe snow blindness,[62] led the rest to the safety of Hut Point. He, Dick Richards and Ernest Wild then returned for Mackintosh, reaching his tent on March the 16th. Joyce wrote that evening: 
 "Good going passed Smith’s grave 10.45 + had lunch at Depot. Saw Skippers camp just after + looking through the glasses found him outside the tent much to the joy of all hands as we expected him to be down." [63] The five survivors were all back at Hut Point on 18 March 1916.[64]
 All five men were showing symptoms of scurvy with varying severity. However, a diet of fresh seal meat, rich in Vitamin C, enabled them to recover slowly. By mid-April they were ready to consider travelling the final 13 miles (21 km) across the frozen sea to the base at Cape Evans.[65]
 Joyce tested the sea-ice on 18 April and found it firm, but the following day a blizzard from the south swept all the ice away.[66] The ambience at Hut Point was gloomy, and the unrelieved diet of seal was depressing. This seemed particularly to affect Mackintosh, and on 8 May, despite the urgent pleadings of Joyce, Richards and Ernest Wild, he decided to risk the re-formed ice and walk to Cape Evans.[65] Victor Hayward volunteered to accompany him. Joyce recorded in his diary: "I fail to understand how these people are so anxious to risk their lives again".[65] Shortly after their departure a blizzard descended, and the two were never seen again.[65]
 Joyce and the others learned the fate of Mackintosh and Hayward only when they were finally able to reach Cape Evans in July. Joyce immediately set about organising searches for traces of the missing men; in the subsequent months parties were sent to search the coasts and the islands in McMurdo Sound, but to no avail.[67] Joyce also organised journeys to recover geological samples left on the Barrier and to visit the grave of Spencer-Smith, where a large cross was erected.[68] In the absence of the ship, the seven remaining survivors lived quietly, until on 10 January 1917, the refitted Aurora arrived with Shackleton aboard to take them home. They learned then that their depot-laying efforts had been futile, Shackleton's ship Endurance having been crushed by the Weddell Sea ice nearly two years previously.[69]
 After his return to New Zealand Joyce was hospitalised, mainly from the effects of snow blindness, and according to his own account had to wear dark glasses for a further 18 months.[70] During this period he married Beatrice Curtlett from Christchurch.[71] He was now probably unfit for further polar work, although he attempted, unsuccessfully, to rejoin the Navy in 1918.[72] In September 1919 he was seriously injured in a car accident, which led to months of convalescence followed by a return to England.[73] In 1920 he signed up for a new Antarctic expedition to be led by John Cope of the Ross Sea party, but this venture proved abortive.[74] He continued to maintain his claims to financial compensation from Shackleton, which caused a breach between them,[75] and he was not invited to join Shackleton's Quest expedition which departed in 1921. He applied to join the British Mount Everest expedition of 1921–22, but was rejected.[76]
 He was in the public eye again in 1923 when he was awarded the Albert Medal for his efforts to save the lives of Mackintosh and Spencer-Smith during the 1916 depot-laying journey. Richards received the same award; Hayward, and Ernest Wild, who had died of typhoid during naval service in the Mediterranean in 1918, received the award posthumously.[76] In 1929 Joyce published a contentious version of his diaries under the title The South Polar Trail,[77] in which he boosted his own role, played down the contributions of others, and incorporated fictitious colourful details.[78] Thereafter he indulged in various schemes for further expeditions, and wrote numerous articles and stories based on his exploits before settling into a quiet life as a hotel porter in London. Bickel's assertion that Joyce lived into his eighties, beyond the date (1958) of the first Antarctic crossing by Vivian Fuchs and his party,[79] is not supported by any other source. Joyce died from natural causes, aged about 65, on 2 May 1940.[80] He is commemorated in Antarctica by Mount Joyce at 75°36′S 160°38′E﻿ / ﻿75.600°S 160.633°E﻿ / -75.600; 160.633﻿ (Mount Joyce).[81]
 The polar historian Roland Huntford sums up Joyce as a "strange mixture of fraud, flamboyance and ability".[15] This mixed assessment is endorsed in the assortment of views expressed by those associated with him. Dick Richards of the Ross Sea party described him as "a kindly soul and a good pal",[82] and others shared the favourable opinions expressed by Scott and Markham, confirming Joyce as a "jolly good sort", though unsuited for command.[83] On the other hand, Eric Marshall of the Nimrod Expedition had found him "of limited intelligence, resentful and incompatible",[84] while John King Davis, when refusing to join the Imperial Trans-Antarctic Expedition, told Shackleton: "I absolutely decline to be associated with any enterprise with which people of the Joyce type are connected".[85]
 Joyce's versions of events recorded in his published diaries have been described as unreliable and sometimes as outright invention—a "self-aggrandizing epic".[78] Specific examples of this "fabulism" include his self-designation as "Captain" after the Ross Sea expedition;[76] his invented claim to have seen Scott's death tent on the Barrier; the misrepresentation of his instructions from Shackleton regarding his sledging role, and his assertion of independence in the field; his claim to have been offered a place on the transcontinental party when Shackleton had made it clear he did not want him there;[35] and his habit, late in life, of writing anonymously to the press praising "the famous Polar Explorer Ernest Mills Joyce".[86] This self-promotion neither surprised nor upset his former comrades. "It is what I would have expected", said Richards. "He was bombastic [...] but true-hearted and a staunch friend".[87] Alexander Stevens, the party's chief scientist, concurred. They knew that Joyce, for all his swaggering style, had the will and determination to "drag men back from certain death".[35] Lord Shackleton, the explorer's son, named Joyce (with Mackintosh and Richards) as "one of those who emerge from the (Ross Sea party) story as heroes".[88]
 Notes
 References
 
 1 Early years 2 Discovery Expedition, 1901–04 3 British Antarctic Expedition (Nimrod) 1907–09 4 Australasian Antarctic Expedition, 1911 5 Imperial Trans-Antarctic Expedition, 1914–17

5.1 Membership of Ross Sea party
5.2 Major setbacks
5.3 Depot-laying journey
5.4 Rescue

 5.1 Membership of Ross Sea party 5.2 Major setbacks 5.3 Depot-laying journey 5.4 Rescue 6 Later life

6.1 Post-expedition career
6.2 Assessment

 6.1 Post-expedition career 6.2 Assessment 7 See also 8 Notes and references 9 Sources 10 External links List of Antarctic expeditions ^ The Ross Ice Shelf tends to experience extreme weather conditions during the March–April period, such as to make travel difficult. "Barrier weather" was a vital factor in the loss of Captain Scott's party in 1912.[50]
 ^ McOrist, p 10 
 ^ Tyler-Lewis, pp. 262 and p. 56
 ^ Huxley, p. 101
 ^ a b c d e f g Tyler-Lewis, p. 55–57
 ^ McOrist, p 11 
 ^ McOrist, p 11 
 ^ Wilson, p. 59 and p. 401
 ^ Fisher, p. 127
 ^ Riffenburgh, p. 173.
 ^ Huxley, p. 115
 ^ a b Riffenburgh, p. 126
 ^ a b Riffenburgh, p. 125
 ^ Mills, p. 41
 ^ Fisher, p. 127 (citing the story to Hugh Robert Mill)
 ^ a b Huntford, p. 194
 ^ Tyler-Lewis, pp. 253–58
 ^ Fisher, p. 121
 ^ Riffenburgh, pp. 143–45
 ^ Riffenburgh, pp. 151–52
 ^ Riffenburgh, pp. 110–16
 ^ Riffenburgh, pp. 157–67
 ^ McOrist p 8
 ^ Mills, p. 62
 ^ Mills, p. 65
 ^ Riffenburgh, p. 191
 ^ Mills, p. 96
 ^ Riffenburgh, p. 201
 ^ Riffenburgh, pp. 216–18
 ^ Riffenburgh, p. 274
 ^ Riffenburgh, p. 303
 ^ Mills, pp. 127–28
 ^ Mills, p. 128
 ^ Tyler-Lewis, p. 57
 ^ Fisher, p. 315
 ^ a b c d Tyler-Lewis, p. 260
 ^ South, p. 242
 ^ Tyler-Lewis, pp. 52–53
 ^ Tyler-Lewis, pp. 22–23
 ^ Fisher, pp. 398–99
 ^ Tyler-Lewis, p. 69
 ^ Bickel, p. 47
 ^ Tyler-Lewis, pp. 214–15
 ^ McOrist, p. 42
 ^ Tyler-Lewis, p. 67
 ^ Tyler-Lewis, pp. 69–74
 ^ Tyler-Lewis, p. 83–92
 ^ Tyler-Lewis, p. 83
 ^ Tyler-Lewis, p. 92
 ^ Tyler-Lewis, pp. 104–05
 ^ Huxley, pp. 253–56.
 ^ Tyler-Lewis, pp. 99–102
 ^ a b Tyler-Lewis, pp. 128–131
 ^ Bickel, p. 80
 ^ Bickel, p. 82
 ^ a b Tyler-Lewis, p. 148
 ^ Tyler-Lewis, p. 146
 ^ Tyler-Lewis, pp. 158–59
 ^ Bickel, p. 127
 ^ Tyler-Lewis, p. 163
 ^ Tyler-Lewis, p. 171
 ^ Bickel, p. 147
 ^ Tyler-Lewis, p, 190–93
 ^ McOrist, p 298
 ^ Tyler-Lewis, p. 193
 ^ a b c d Bickel, pp. 208–09
 ^ Bickel, p. 209
 ^ "Trans-Antarctic Expedition 1914–17: SY Aurora and the Ross Sea Party". south-pole.com. Retrieved 9 July 2008..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ Tyler-Lewis, p. 237
 ^ Bickel, pp. 231–33
 ^ Bickel, p. 237
 ^ Tyler-Lewis, p. 249
 ^ Tyler-Lewis, p. 253
 ^ Tyler-Lewis, pp. 254–55
 ^ Tyler-Lewis, p. 256
 ^ Fisher, p. 440
 ^ a b c Tyler-Lewis, pp. 257–58
 ^ Published by Duckworth's, London 1929
 ^ a b Tyler-Lewis, p. 259
 ^ Bickel, p. 236
 ^ Tyler-Lewis, p. 263
 ^ "Mount Joyce". Geographic Names Information System. United States Geological Survey. Retrieved 8 April 2013.
 ^ Huntford, p. 450
 ^ Tyler-Lewis, p. 28
 ^ Huntford, p. 234
 ^ Tyler-Lewis, p. 49
 ^ Tyler-Lewis, p. 262
 ^ Quoted by Tyler-Lewis, p. 260
 ^ Bickel, p. vii
 Bickel, Lennard: Shackleton's Forgotten Men Random House, London, 2000 ISBN 0-7126-6807-1 Fisher, M and J: Shackleton (biography) James Barrie Books, London, 1957 Huxley, Elspeth: Scott of the Antarctic Weidenfeld and Nicolson, London, 1977 ISBN 0-297-77433-6 Huntford, Roland: Shackleton (biography) Hodder & Stoughton, London, 1985 ISBN 0-340-25007-0 "Measuring Worth". Institute for the Measurement of Worth. Retrieved 21 June 2008. McOrist, Wilson Shackleton's Heroes The Robson Press, an imprint of Biteback Publishing, London, 2015 ISBN 978-1-84954-815-1 Riffenburgh, Beau: Nimrod Bloomsbury Publishing, London, 2004 ISBN 0-7475-7253-4 Scott, Robert Falcon:The Voyage of the Discovery Smith, Elder & Co, London, 1905 Shackleton, Ernest: South Century Limited edition, ed. Peter King, London, 1991 ISBN 0-7126-3927-6 Tyler-Lewis, Kelly: The Lost Men Bloomsbury Publishing, London, 2007 ISBN 978-0-7475-7972-4 Wilson, Edward: Diary of the Discovery Expedition Blandford Press, London, 1975 ISBN 0-7137-0431-4 "Trans-Antarctic Expedition 1914–17: SY Aurora and the Ross Sea Party". south-pole.com. Retrieved 4 April 2008. Cool Antarctica, pictures of Antarctica, information and travel guide at www.coolantarctica.com (History and Ships of the Antarctic) v t e Drift of the Aurora Voyage of the James Caird Rescue by the Yelcho
Luis Pardo Luis Pardo Caird Coast Cave Cove Elephant Island Fortuna Glacier King Haakon Bay McDonald Ice Rumples Mount Worsley Peggotty Bluff Stromness Stromness Bay Atkin d'Anglade Donnelly Downing Glidden Grady Hooke Kavanagh Larkman Maugher Mugridge Ninnis Paton Shaw Stenhouse Thompson Warren Wise Shore party: Cope Gaze Hayward Jack Joyce Mackintosh Richards Spencer-Smith Stevens Wild Bakewell Blackborow Cheetham Clark Crean Green Greenstreet Holness How Hudson Hurley Hussey James Kerr Macklin Marston McCarthy McIlroy McLeod McNish
Mrs Chippy Mrs Chippy Orde-Lees Rickinson Shackleton Stephenson Vincent Wild Wordie Worsley  Category  Commons ISNI: 0000 0000 7907 8548 LCCN: no00017954 SNAC: w61h5rxd SUDOC: 064814041 TePapa: 6865 Trove: 993760 VIAF: 11845713  WorldCat Identities (via VIAF): 11845713 English explorers Explorers of Antarctica Imperial Trans-Antarctic Expedition Recipients of the Polar Medal Recipients of the Albert Medal (lifesaving) Royal Navy sailors People from Bognor Regis 1875 births 1940 deaths People educated at the Royal Hospital School Articles with short description EngvarB from November 2017 Use dmy dates from November 2017 Featured articles Wikipedia articles with ISNI identifiers Wikipedia articles with LCCN identifiers Wikipedia articles with SNAC-ID identifiers Wikipedia articles with SUDOC identifiers Wikipedia articles with TePapa identifiers Wikipedia articles with Trove identifiers Wikipedia articles with VIAF identifiers Wikipedia articles with WorldCat-VIAF identifiers Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version العربية Deutsch Español Euskara Français Italiano עברית Nederlands 日本語 Norsk Português Русский  This page was last edited on 25 October 2019, at 11:22 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Ernest Edward Mills Joyce Notes ^ References ^ ^ ^ a b c d e f g ^ ^ ^ ^ ^ ^ a b a b ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b c d ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ a b c d ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b c ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ Shore party: 
Drift of the Aurora
Voyage of the James Caird
Rescue by the Yelcho
Luis Pardo
 
Caird Coast
Cave Cove
Elephant Island
Fortuna Glacier
King Haakon Bay
McDonald Ice Rumples
Mount Worsley
Peggotty Bluff
Stromness
Stromness Bay
 Aurora
Atkin
d'Anglade
Donnelly
Downing
Glidden
Grady
Hooke
Kavanagh
Larkman
Maugher
Mugridge
Ninnis
Paton
Shaw
Stenhouse
Thompson
Warren
Wise
Shore party: Cope
Gaze
Hayward
Jack
Joyce
Mackintosh
Richards
Spencer-Smith
Stevens
Wild
Endurance
Bakewell
Blackborow
Cheetham
Clark
Crean
Green
Greenstreet
Holness
How
Hudson
Hurley
Hussey
James
Kerr
Macklin
Marston
McCarthy
McIlroy
McLeod
McNish
Mrs Chippy
Orde-Lees
Rickinson
Shackleton
Stephenson
Vincent
Wild
Wordie
Worsley
 
Atkin
d'Anglade
Donnelly
Downing
Glidden
Grady
Hooke
Kavanagh
Larkman
Maugher
Mugridge
Ninnis
Paton
Shaw
Stenhouse
Thompson
Warren
Wise
Shore party: Cope
Gaze
Hayward
Jack
Joyce
Mackintosh
Richards
Spencer-Smith
Stevens
Wild
 
Bakewell
Blackborow
Cheetham
Clark
Crean
Green
Greenstreet
Holness
How
Hudson
Hurley
Hussey
James
Kerr
Macklin
Marston
McCarthy
McIlroy
McLeod
McNish
Mrs Chippy
Orde-Lees
Rickinson
Shackleton
Stephenson
Vincent
Wild
Wordie
Worsley
 
 Category
 Commons
 
ISNI: 0000 0000 7907 8548
LCCN: no00017954
SNAC: w61h5rxd
SUDOC: 064814041
TePapa: 6865
Trove: 993760
VIAF: 11845713
 WorldCat Identities (via VIAF): 11845713
 