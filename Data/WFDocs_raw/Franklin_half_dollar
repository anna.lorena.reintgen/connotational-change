The Franklin half dollar is a coin that was struck by the United States Mint from 1948 to 1963.  The fifty-cent piece pictures Founding Father Benjamin Franklin on the obverse and the Liberty Bell on the reverse.  A small eagle was placed to the right of the bell to fulfill the legal requirement that half dollars depict the figure of an eagle.  Produced in 90 percent silver with a reeded edge, the coin was struck at the Philadelphia, Denver, and San Francisco mints. As of April 26, 2019, the melt value of the $0.50 coin was approximately $5.46.
 Mint director Nellie Tayloe Ross had long admired Franklin, and wanted him to be depicted on a coin.  In 1947, she instructed her chief engraver, John R. Sinnock, to prepare designs for a Franklin half dollar. Sinnock's designs were based on his earlier work, but he died before their completion. The designs were completed by Sinnock's successor, Gilroy Roberts.  The Mint submitted the new designs to the Commission of Fine Arts ("Commission") for its advisory opinion. The Commission disliked the small eagle and felt that depicting the crack in the Liberty Bell would expose the coinage to jokes and ridicule.  Despite the Commission's disapproval, the Mint proceeded with Sinnock's designs.
 After the coins were released in April 1948, the Mint received accusations that Sinnock's initials "JRS" on the cutoff at Franklin's shoulder were a tribute to Soviet dictator Joseph Stalin (Stalin did not have a middle  name that began with an 'R').  No change was made, with the Mint responding that the letters were simply the artist's initials (The same accusation was made after the release of the Sinnock designed Roosevelt Dime in 1946).  The coin was struck regularly until 1963. Beginning in 1964 it was replaced by the Kennedy half dollar, issued in honor of the assassinated President John F. Kennedy.  Though the coin is still legal tender, its value to collectors or as silver (intrinsic value) both greatly exceed its face value.
 Mint Director Nellie Tayloe Ross had long been an admirer of Benjamin Franklin, and wished to see him on a coin.[2] In 1933, Sinnock had designed a medal featuring Franklin, which may have given her the idea.[2]  Franklin had opposed putting portraits on coins;[3] he advocated proverbs about which the holder could profit through reflection.[4] In a 1948 interview, Ross noted that Franklin only knew of living royalty on coins, and presumably would feel differently about a republic honoring a deceased founder.[3]  Indeed, Franklin might have been more upset at the reverse design: as numismatic writer Jonathan Tepper noted, "Had Benjamin Franklin known that he would be appearing on a half dollar with an eagle, he most likely would have been quite upset.  He detested the eagle, and numismatic lore has it that he often referred to it as a scavenger. Given the practical man that he was, Franklin proposed the wild turkey as our national bird."[5]
 An 1890 statute forbade the replacement of a coin design without congressional action, unless it had been in service for 25 years, counting the year of first issuance.  The Walking Liberty half dollar and Mercury dime had been first issued in 1916; they could be replaced without congressional action from and after 1940.[6]  Mint officials considered putting Franklin on the dime in 1941, but the project was shelved due to heavy demands on the Mint for coins as the United States entered World War II.[7]  During the war, the Mint contemplated adding one or more new denominations of coinage; Sinnock prepared a Franklin design in anticipation of a new issue, which did not occur.[8]  The dime was redesigned in 1946 to depict fallen President Franklin Roosevelt, who had been closely associated with the March of Dimes.[7]  The Walking Liberty design seemed old-fashioned to Mint officials, and the only other coin being struck which was eligible for replacement was the Lincoln cent.  Abraham Lincoln remained a beloved figure, and Ross did not want to be responsible for removing him from the coinage.[9]
 In 1947, Ross asked Sinnock to produce a design for a half dollar featuring Franklin.  The chief engraver adapted his earlier work for the obverse.[2]  He had designed the medal from a bust of Franklin by Jean-Antoine Houdon.[7] Sinnock based his design for the reverse on the 1926 commemorative half dollar for the sesquicentennial (150th anniversary) of American Independence.[2] Numismatic writer Don Taxay later discovered that Sinnock had based his Liberty Bell (as depicted on both the Sesquicentennial half dollar and the Franklin half) on a sketch by John Frederick Lewis.[10] Sinnock died in May 1947, before finishing the reverse design, which was completed by the new chief engraver, Gilroy Roberts.[7]  Similar to Sinnock's work for the Roosevelt dime, the portrait is designed along simple lines, with Franklin depicted wearing a period suit.  The small eagle on the reverse was added as an afterthought, when Mint officials realized that the Coinage Act of 1873 required one to be displayed on all coins of greater value than the dime.[9]
 The Mint sought comments on the designs from the Commission of Fine Arts, which was provided with a lead striking of the obverse and a view of the reverse; Taxay suggests they were shown a plaster model.  On December 1, 1947, Commission chairman Gilmore Clarke wrote to Ross saying that they had no objection to the obverse, in which they recognized Sinnock's "good workmanship".[11]  As for the reverse,
 The eagle shown on the model is so small as to be insignificant and hardly discernible when the model is reduced to the size of a coin.  The Commission hesitate to approve the Liberty Bell as shown with the crack in the bell visible; to show this might lead to puns and to statements derogatory to United States coinage.
 The Commission disapprove the designs.[11]
 Numismatist Paul Green later noted, "Over the years there would probably have been even more puns and derogatory statements if there had been an attempt to depict the bell without a crack."[12]  The Commission suggested a design competition under its auspices.[13]  Its recommendations, which were only advisory,[5] were rejected by the Treasury Department and the coin was approved by Treasury Secretary John W. Snyder, which Taxay ascribes to an unwillingness to dishonor Sinnock.[13]
 On January 7, 1948, the Treasury issued a press release announcing the new half dollar.  The Commission's disapproval went unreported; instead the release noted that the design had been Ross's idea and had received Secretary Snyder's "enthusiastic approval".[14]  The release noted Franklin's reputation for thrift, and expressed hope that the half dollar would serve as a reminder that spare cash should be used to purchase savings bonds and savings stamps.  Franklin became the fifth person and first non-president to be honored by the issuance of a regular-issue US coin, after Lincoln, Roosevelt, George Washington and Thomas Jefferson.[14]
 In a speech given when she unveiled the design in January 1948, Ross indicated that she had been urged to put Franklin on the cent because of his association with the adage "a penny saved is a penny earned" (in Franklin's original, "A penny saved is twopence dear".)[10]  Ross stated, "You will agree, I believe, that the fifty-cent piece, being larger and of silver, lends itself much better to the production of an impressive effect."[10]  On April 29, 1948, the day before the coin's public release, Ross held a dinner party for 200 at the Franklin Institute in Philadelphia; each guest received a Franklin half dollar in a card signed by Ross.[4][15]
 The new half dollars first went on sale at noon on April 30, 1948, the anniversary of George Washington's 1789 inauguration as President.  They were sold from a booth on the steps of the Sub-Treasury Building in New York, by employees of the Franklin Savings Bank dressed in Revolutionary-era garb.[16]
 The Roosevelt dime had been designed by Sinnock, and had provoked complaints by citizens viewing Sinnock's initials "JS" on the coin as those of Joseph Stalin, placed there by some Kremlin infiltrator within the Mint. Even though Sinnock's initials (placed on the cutoff of Franklin's bust) were expressed "JRS", the Mint still received similar complaints, to which they responded with what numismatic historian Walter Breen termed "outraged official denials".[7][15]  According to The New York Times, "People wrote in demanding to know how the Bureau of the Mint had discovered that Joe Stalin had a middle name."[17] Another rumor was that the small "o" in "of" was an error, and that the coins would be recalled.  This claim died more quickly than the Stalin rumor.[15]
 After the assassination of John F. Kennedy on November 22, 1963, Congress and the Mint moved with great speed to authorize and produce a half dollar in tribute to him.  With the authorization of the Kennedy half dollar on December 30, 1963, the Franklin half dollar series came to an end.[18]  Breen reports rumors of 1964 Franklin half dollars, produced possibly as trial strikes to test 1964-dated dies, but none has ever come to light.[15]  A total of 465,814,455 Franklin half dollars were struck for circulation; in addition, 15,886,955 were struck in proof.[19]
 The Franklin half dollar was struck in relatively small numbers in its first years,[9] as there was limited demand due to a glut of Walking Liberty halves.  No half dollars were struck at Denver in 1955 and 1956 due to a lack of demand for additional pieces.[20]  The San Francisco Mint closed in 1955; it did not reopen until 1965.[21]  In 1957, with improved economic conditions, demand for the pieces began to rise.[20]  They were struck in much greater numbers beginning in 1962, which saw the start of the greatly increased demand for coins which would culminate in the great coin shortage of 1964.[9]  No Franklin half dollar is rare today, as even low-mintage dates were widely saved.  Proof coins were struck at the Philadelphia Mint from 1950.  "Cameo proofs", with frosted surfaces and mirror-like fields, were struck in small numbers and carry a premium.  Just under 498 million Franklin half dollars, including proofs, were struck.[10]
 There are only 35 different dates and mintmarks in the series, making it a relatively inexpensive collecting project.[20]  A widely known variety is the 1955 "Bugs Bunny" half.  This variety was caused by a die clash between an obverse die and a reverse die.  The impact of the eagle's wings on the other die caused a marking outside of Franklin's mouth which, according to some, resembles buck teeth.[22]  The quality of half dollars struck by the Mint decreased in the late 1950s, caused by deterioration of the master die from which working dies were made for coinage.[23]
 In an initial attempt to improve the quality of the pieces, the Mint made slight modifications to the designs, though both the old (Type I) and new (Type II) were struck in 1958 and 1959.  One obvious difference between the types is the number of long tail feathers on the eagle—Type I half dollars have four tail feathers, Type II only three.  Approximately 20% of the 1958 Philadelphia coinage is Type II, struck from dies which were first used to strike the 1958 proofs.  About 70% of the 1959 half dollars struck at Philadelphia are Type II; all 1958-D and 1959-D half dollars are Type I.[5] The Mint recut the master die before beginning the 1960 coinage, improving quality.[23]
 An especially well-struck Franklin half dollar is said to have full bell lines. To qualify, the seven parallel lines making up the bottom of the bell must be fully visible, and the three wisps of hair to the right of Franklin's ear on the obverse must also fully show, and not blend together.[1] Many Franklins have been damaged by "roll friction": the tendency of pieces in a loose coin roll to rub together repeatedly, causing steel-gray abrasions, usually on Franklin's cheek and on the center of the Liberty Bell.[24]
 By mintages, the key dates in this series are the 1948, 1949-S, 1953 and 1955. Franklin half dollars have been extensively melted for their silver, and many dates are rarer than the mintage figures indicate.[1] For example, although more than nine million 1962 halves were struck for circulation, and an additional three million in proof, the coin was more valuable as bullion than in any condition when silver prices reached record levels in 1979–1980. In 2010, the 1962 half in MS-65 condition sold for about US$145, second only to the 1953-S in price in that grade.[25]
 Note: Numbers in parentheses represent coins which were distributed in proof sets, which are also included in the totals.
 Other sources
 
 90% Ag 10% Cu[1] 1 Background and selection 2 Release and production 3 Collecting

3.1 Mintage figures

 3.1 Mintage figures 4 References 5 Bibliography 6 External links ^ a b c d e f g h i Coin World, Franklin half dollar.
 ^ a b c d Taxay 1983, p. 376.
 ^ a b AP/The Portsmouth Times & 1948-05-13.
 ^ a b Tomaska 2002, p. 3.
 ^ a b c Tepper & April 1990.
 ^ Breen 1988, pp. 413, 416.
 ^ a b c d e Tomaska 2011, p. 21.
 ^ Tomaska 2002, pp. 4–5.
 ^ a b c d Lange 2006, p. 172.
 ^ a b c d Coin Community Family.
 ^ a b Taxay 1983, pp. 376–377.
 ^ Tomaska 2002, p. xi.
 ^ a b Taxay 1983, p. 377.
 ^ a b United States Department of the Treasury & 1948-01-07.
 ^ a b c d Breen 1988, p. 416.
 ^ The New York Times & 1948-05-01.
 ^ The New York Times & 1959-02-15.
 ^ Breen 1988, p. 418.
 ^ Guth & Garrett 2005, p. 96.
 ^ a b c Daytona Beach Sunday News-Journal & 1975-03-16.
 ^ AP/Ocala Star-Banner & 1965-08-22.
 ^ Tomaska 2011, p. 68.
 ^ a b Tomaska 2011, p. 84.
 ^ Tepper & May 1990.
 ^ Green & 2010-07-14.
 ^ a b c Breen 1988, pp. 417–418.
 Breen, Walter (1988). Walter Breen's Complete Encyclopedia of U.S. and Colonial Coins. New York: Doubleday. ISBN 978-0-385-14207-6..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} Guth, Ron; Garrett, Jeff (2005). United States Coinage: A Study by Type. Atlanta, Ga.: Whitman Publishing. ISBN 978-0-7948-1782-4. Lange, David W. (2006). History of the United States Mint and its Coinage. Atlanta: Whitman Publishing. ISBN 978-0-7948-1972-9. Taxay, Don (1983) [1966]. The U.S. Mint and Coinage (reprint ed.). New York: Sanford J. Durst Numismatic Publications. ISBN 978-0-915262-68-7. Tomaska, Rick (2002). A Complete Guide to Franklin Half Dollars. Virginia Beach, Va.: DLRC Press. ISBN 978-1-880731-68-0. Tomaska, Rick (2011). A Guide Book of Franklin & Kennedy Half Dollars. Atlanta: Whitman Publishing. ISBN 978-0-7948-3243-8. "Benjamin Franklin opposed putting portraits on coins". The Portsmouth (Ohio) Times. Associated Press. May 13, 1948. p. 28. Retrieved April 4, 2011. "Minting of coins with less silver to start on Monday". Ocala Star-Banner. Associated Press. August 22, 1965. p. 24. Retrieved April 25, 2011. "1948–63 Half Dollar Franklin History". Coin Community Family. Retrieved April 4, 2011. "Franklin half dollar". Coin World. Amos Press. Retrieved November 10, 2014. Green, Paul M. (July 14, 2010). "1962 Franklin Half Dollars Melted, Now Valuable". Coin Collecting News. Archived from the original on October 6, 2011. Retrieved April 22, 2011.CS1 maint: BOT: original-url status unknown (link) "New half dollar for museum archives". The New York Times. May 1, 1948. Retrieved April 5, 2011. (subscription required) Restifo, Harriett (March 16, 1975). "The Franklin half". Daytona Beach Sunday News-Journal. p. 10C. Retrieved April 25, 2011. Shuster, Alvin (February 15, 1959). "Change of a penny". The New York Times. p. 15 (Magazine). Retrieved April 5, 2011. (subscription required) Tepper, Jonathan (April 1990). "Circulation strike Franklin half dollars, Part I". The Numismatist. American Numismatic Association: 544–546, 617. Tepper, Jonathan (May 1990). "Circulation strike Franklin half dollars, Part II". The Numismatist. American Numismatic Association: 736–739, 771–772. "Press service no. S-589" (DOC). United States Department of the Treasury. January 7, 1948. Retrieved April 5, 2011. Money portal Numismatics portal United States portal US Franklin Half Dollar by year. Histories, photos, and more. Franklin Half Dollar Pictures v t e Join, or Die. (1754 political cartoon) Albany Plan of Union
Albany Congress Albany Congress Hutchinson Letters Affair Committee of Secret Correspondence Committee of Five Declaration of Independence Model Treaty
Franco-American alliance
Treaty of Amity and Commerce
Treaty of Alliance Franco-American alliance Treaty of Amity and Commerce Treaty of Alliance Staten Island Peace Conference Treaty of Paris, 1783 Delegate, 1787 Constitutional Convention Pennsylvania Provincial Assembly Postmaster General Founding Fathers Franklin's electrostatic machine Bifocals Franklin stove Glass armonica Gulf Stream exploration, naming, and chart Lightning rod Kite experiment Pay it forward Associators
111th Infantry Regiment 111th Infantry Regiment Junto club American Philosophical Society Library Company of Philadelphia Pennsylvania Hospital Academy and College of Philadelphia
University of Pennsylvania University of Pennsylvania Philadelphia Contributionship Union Fire Company Early American currency Continental Currency dollar coin Fugio cent United States Postal Service President, Pennsylvania Abolition Society Master, Les Neuf Sœurs Gravesite Silence Dogood letters (1722) A Dissertation on Liberty and Necessity, Pleasure and Pain (1725) The Busy-Body letters (1729) Pennsylvania Gazette (1729–1790) Poor Richard's Almanack (1732–1758) The Drinker's Dictionary (1737) "Advice to a Friend on Choosing a Mistress" (1745) "The Speech of Polly Baker" (1747) Observations Concerning the Increase of Mankind, Peopling of Countries, etc. (1751) Experiments and Observations on Electricity (1751) Birch letters (1755) The Way to Wealth (1758) Pennsylvania Chronicle (1767) Rules by Which a Great Empire May Be Reduced to a Small One (1773) Proposed alliance with the Iroquois (1775) A Letter To A Royal Academy (1781) Remarks Concerning the Savages of North America (1784) The Morals of Chess (1786) An Address to the Public (1789) A Plan for Improving the Condition of the Free Blacks (1789) The Autobiography of Benjamin Franklin (1771–1790, pub. 1791) Bagatelles and Satires (pub. 1845) Franklin as a journalist Franklin Court Benjamin Franklin House Benjamin Franklin Institute of Technology Benjamin Franklin National Memorial Franklin Institute
awards
medal awards medal Benjamin Franklin Medal Royal Society of Arts medal Depicted in The Apotheosis of Washington Boston statue Washington D.C. statue Jefferson Memorial pediment In popular culture
Ben and Me (1953 short)
Ben Franklin in Paris (1964 musical play)
1776 (1969 musical
1972 film)
Benjamin Franklin (1974 miniseries)
A More Perfect Union (1989 film)
Liberty! (1997 documentary series)
Liberty's Kids (2002 animated series)
Benjamin Franklin (2002 documentary series)
John Adams (2008 miniseries)
Sons of Liberty (2015 miniseries) Ben and Me (1953 short) Ben Franklin in Paris (1964 musical play) 1776 (1969 musical 1972 film) Benjamin Franklin (1974 miniseries) A More Perfect Union (1989 film) Liberty! (1997 documentary series) Liberty's Kids (2002 animated series) Benjamin Franklin (2002 documentary series) John Adams (2008 miniseries) Sons of Liberty (2015 miniseries) Refunding Certificate Franklin half dollar One-hundred dollar bill Franklin silver dollar Washington–Franklin stamps
other stamps other stamps Cities, counties, schools named for Franklin Franklin Field State of Franklin Sons of Ben (Philadelphia Union) Ships named USS Franklin Ben Franklin effect Age of Enlightenment American Enlightenment The New-England Courant The American Museum magazine American Revolution
patriots patriots Syng inkstand Deborah Read (wife) Sarah Franklin Bache (daughter) Francis Franklin (son) William Franklin (son) Richard Bache Jr. (grandson) Benjamin F. Bache (grandson) Louis F. Bache (grandson) William Franklin (grandson) Andrew Harwood (great-grandson) Alexander Bache (great-grandson) Josiah Franklin (father) Jane Mecom (sister) James Franklin (brother) Mary Morrell Folger (grandmother) Peter Folger (grandfather) Richard Bache (son-in-law) Ann Smith Franklin (sister-in-law) Category Commons v t e Nova Constellatiox (1783) Liberty Cap (1793–1797) Draped Bust (1800–1808) Classic Head (1809–1836) Braided Hair (1840–1857) Fugio (Franklin) (1787) Silver centerx (1792) Chain (1793) Wreath (1793) Liberty Cap (1793–1796) Draped Bust (1796–1807) Classic Head (1808–1814) Matron Head (1816–1839) Braided Hair (1839–1857, 1868) Ringx (1850–1851, 1853, 1884–1885) Flying Eagle (1856–1858, patterns struck in 1854–1855) Indian Head (1859–1909, patterns struck in 1858) Lincoln (1909–present) Billonx (1836) Bronze (1864–1873, patterns struck in 1863) Silver (1851–1873) Bronze (Coronet Head)x (1863) Nickel (Liberty Head) (1865–1889) Half disme (1792) Flowing Hair (1794–1795) Draped Bust (1796–1797, 1800–1805) Capped Bust (1829–1837) Washingtonx (1866; 1909–1910) Seated Liberty (1837–1873) Shield (1866–1883) Liberty Head (1883–1913) Buffalo (Indian Head) (1913–1938) Jefferson (1938–present) Nova Constellatiox (1783) Dismex (1792) Draped Bust (1796–1807) Capped Bust (1809–1837) Seated Liberty (1837–1891) Barber (1892–1916) Mercury (1916–1945) Roosevelt (1946–present) Seated Liberty (1875–1878) Draped Bust (1796–1807) Capped Bust (1815–1838) Seated Liberty (1838–1891) Barber (1892–1916) Standing Liberty (1916–1930) Washington (eagle reverse) (1932–1998) Washington (bicentennial) (1975–1976) Washington (50 State) (1999–2008) Washington (D.C. and U.S. Territories) (2009) Washington (America the Beautiful) (2010–present) Nova Constellatiox (1783) Flowing Hair (1794–1795) Draped Bust (1796–1807) Capped Bust (1807–1839) Seated Liberty (1839–1891) Barber (1892–1915) Walking Liberty (1916–1947) Franklin (1948–1963) Kennedyc (1964–present) Kennedy (bicentennial) (1975–1976) Continental Currency (Fugio or Franklin)x (1776) Nova Constellatiox (1783) Flowing Hair (1794–1795) Draped Bust (1795–1804) Gobrechtx (1836–1839) Seated Liberty (1840–1873) Trade (1873–1885) Morgan (1878–1904; 1921) Peace (1921–1935, patterns struck in 1964) Eisenhower (1971–1978) Eisenhower (bicentennial) (1975–1976) Susan B. Anthony (1979–1981; 1999) Sacagaweac (2000–present) Presidential (2007–2016) American Innovationc (2018–present) Liberty Head (1849–1854) Indian Princess (1854–1889) Draped Bust (1796–1807) Capped Bust (1808–1834) Classic Head (1834–1839) Liberty Head (1840–1907) Indian Head (1908–1929) Indian Princess (1854–1889) Draped Bust (1795–1807) Capped Bust (1808–1834) Classic Head (1834–1838) Liberty Head (1839–1908) Indian Head (1908–1929) Capped Bust (1795–1804) Liberty Head (1838–1907) Indian Head (1907–1933) Liberty Head (1850–1907, pattern struck in 1849) Quintuple Stellax (1879) Saint-Gaudens (1907–1933) Two and a half cent piece (2.5¢)x (not minted) Two dollar piece ($2)x (not minted) Stella ($4)x (1879–1880) Half union ($50)x (1877) Union ($100)x (not minted) v t e 1¢ 5¢ 10¢ 25¢ 50¢ $1 5₥ 1¢ (large size) 2¢ 3¢ (silver) 3¢ (nickel) 5¢ (silver) 20¢ $1 (gold) $2.5 $3 $5 $10 $20 2¢ (billon) 2.5¢ 3¢ (bronze) $2 $4 $50 $100 1800s 1900s 1910s 1920s 1930s 1940s 1950s 1970s 1980s 1990s 2000s 2010s 2020s Silver Eagle (1986–present) Gold Eagle (1986–present) Platinum Eagle (1997–present) Gold Buffalo (2006–present) First Spouse (gold) (2007–2016) Palladium Eagle (2017–present) America the Beautiful (silver) (2010–present) Proof Set (1936–present) Mint Set (1947–present) Special Mint Set (1964–1967) Souvenir Set (1972–1998) Silver Proof Set (1976, 1992–present) Prestige Set (1983–1997) Birds on coins Currencies introduced in 1948 Coins of the United States United States silver coins Fifty-cent coins Works about Benjamin Franklin CS1 maint: BOT: original-url status unknown Pages containing links to subscription-only content Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Deutsch Français Italiano Português Русский Українська 中文  This page was last edited on 12 November 2019, at 20:18 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  The eagle shown on the model is so small as to be insignificant and hardly discernible when the model is reduced to the size of a coin.  The Commission hesitate to approve the Liberty Bell as shown with the crack in the bell visible; to show this might lead to puns and to statements derogatory to United States coinage.
The Commission disapprove the designs.[11]

 Franklin half dollar Note: a b c d e f g h i a b c d a b a b a b c ^ a b c d e ^ a b c d a b c d a b ^ a b a b a b c d ^ ^ ^ ^ a b c ^ ^ a b ^ ^ a b c Other sources  Half dollar (United States coin) (1948–1963) Lincoln Jefferson Roosevelt Washington (America the Beautiful) Kennedy Sacagawea American Innovation bold United States 50 cents (0.50 US dollars) 12.50[1] g 30.61[1] mm (1.21[1] in) 1.8 mm reeded[1] 
90% Ag
10% Cu[1]
 0.36169[1] troy oz 1948–1963[1] D, S  on reverse, above yoke of bell and below letter E in STATES.  Philadelphia Mint coins struck without mint mark.  Benjamin Franklin John R. Sinnock 1948  Liberty Bell Sinnock with participation by Gilroy Roberts 1948 3,006,814
 4,028,500
 
 5,614,000
 4,120,500
 3,744,000
 7,793,509 (51,386)
 8,031,600
 
 16,859,602 (57,500)
 9,475,200
 13,696,000
 21,274,073 (81,980)
 25,395,600
 5,526,000
 2,796,820 (128,800)
 20,900,400
 4,148,000
 13,421,502 (233,300)
 25,445,580
 4,993,400
 2,876,381 (378,200)
 
 
 4,701,384 (669,384)
 
 
 6,361,952 (1,247,952)
 19,966,850
 
 4,917,652 (875,652)
 23,962,412
 
 7,349,291 (1,149,291)
 13,053,750
 
 7,715,602 (1,691,602)
 18,215,812
 
 11,318,244 (3,028,244)
 20,276,442
 
 12,932,019 (3,218,019)
 35,473,281
 
 25,239,645 (3,075,645)
 67,069,292
 
 Preceded byWalking Liberty half dollar
  Half dollar (United States coin) (1948–1963)
 Succeeded byKennedy half dollar
 President of Pennsylvania (1785–1788), Ambassador to France (1779–1785), Second Continental Congress (1775–1776) 
Join, or Die. (1754 political cartoon)
Albany Plan of Union
Albany Congress
Hutchinson Letters Affair
Committee of Secret Correspondence
Committee of Five
Declaration of Independence
Model Treaty
Franco-American alliance
Treaty of Amity and Commerce
Treaty of Alliance
Staten Island Peace Conference
Treaty of Paris, 1783
Delegate, 1787 Constitutional Convention
Pennsylvania Provincial Assembly
Postmaster General
Founding Fathers
 
Franklin's electrostatic machine
Bifocals
Franklin stove
Glass armonica
Gulf Stream exploration, naming, and chart
Lightning rod
Kite experiment
Pay it forward
Associators
111th Infantry Regiment
Junto club
American Philosophical Society
Library Company of Philadelphia
Pennsylvania Hospital
Academy and College of Philadelphia
University of Pennsylvania
Philadelphia Contributionship
Union Fire Company
Early American currency
Continental Currency dollar coin
Fugio cent
United States Postal Service
President, Pennsylvania Abolition Society
Master, Les Neuf Sœurs
Gravesite
 
Silence Dogood letters (1722)
A Dissertation on Liberty and Necessity, Pleasure and Pain (1725)
The Busy-Body letters (1729)
Pennsylvania Gazette (1729–1790)
Poor Richard's Almanack (1732–1758)
The Drinker's Dictionary (1737)
"Advice to a Friend on Choosing a Mistress" (1745)
"The Speech of Polly Baker" (1747)
Observations Concerning the Increase of Mankind, Peopling of Countries, etc. (1751)
Experiments and Observations on Electricity (1751)
Birch letters (1755)
The Way to Wealth (1758)
Pennsylvania Chronicle (1767)
Rules by Which a Great Empire May Be Reduced to a Small One (1773)
Proposed alliance with the Iroquois (1775)
A Letter To A Royal Academy (1781)
Remarks Concerning the Savages of North America (1784)
The Morals of Chess (1786)
An Address to the Public (1789)
A Plan for Improving the Condition of the Free Blacks (1789)
The Autobiography of Benjamin Franklin (1771–1790, pub. 1791)
Bagatelles and Satires (pub. 1845)
Franklin as a journalist
 
Franklin Court
Benjamin Franklin House
Benjamin Franklin Institute of Technology
Benjamin Franklin National Memorial
Franklin Institute
awards
medal
Benjamin Franklin Medal
Royal Society of Arts medal
Depicted in The Apotheosis of Washington
Boston statue
Washington D.C. statue
Jefferson Memorial pediment
In popular culture
Ben and Me (1953 short)
Ben Franklin in Paris (1964 musical play)
1776 (1969 musical
1972 film)
Benjamin Franklin (1974 miniseries)
A More Perfect Union (1989 film)
Liberty! (1997 documentary series)
Liberty's Kids (2002 animated series)
Benjamin Franklin (2002 documentary series)
John Adams (2008 miniseries)
Sons of Liberty (2015 miniseries)
Refunding Certificate
Franklin half dollar
One-hundred dollar bill
Franklin silver dollar
Washington–Franklin stamps
other stamps
Cities, counties, schools named for Franklin
Franklin Field
State of Franklin
Sons of Ben (Philadelphia Union)
Ships named USS Franklin
Ben Franklin effect
 
Age of Enlightenment
American Enlightenment
The New-England Courant
The American Museum magazine
American Revolution
patriots
Syng inkstand
 
Deborah Read (wife)
Sarah Franklin Bache (daughter)
Francis Franklin (son)
William Franklin (son)
Richard Bache Jr. (grandson)
Benjamin F. Bache (grandson)
Louis F. Bache (grandson)
William Franklin (grandson)
Andrew Harwood (great-grandson)
Alexander Bache (great-grandson)
Josiah Franklin (father)
Jane Mecom (sister)
James Franklin (brother)
Mary Morrell Folger (grandmother)
Peter Folger (grandfather)
Richard Bache (son-in-law)
Ann Smith Franklin (sister-in-law)
 
Category
Commons
 
Nova Constellatiox (1783)
Liberty Cap (1793–1797)
Draped Bust (1800–1808)
Classic Head (1809–1836)
Braided Hair (1840–1857)
 
Fugio (Franklin) (1787)
Silver centerx (1792)
Chain (1793)
Wreath (1793)
Liberty Cap (1793–1796)
Draped Bust (1796–1807)
Classic Head (1808–1814)
Matron Head (1816–1839)
Braided Hair (1839–1857, 1868)
Ringx (1850–1851, 1853, 1884–1885)
Flying Eagle (1856–1858, patterns struck in 1854–1855)
Indian Head (1859–1909, patterns struck in 1858)
Lincoln (1909–present)
 
Billonx (1836)
Bronze (1864–1873, patterns struck in 1863)
 
Silver (1851–1873)
Bronze (Coronet Head)x (1863)
Nickel (Liberty Head) (1865–1889)
 
Half disme (1792)
Flowing Hair (1794–1795)
Draped Bust (1796–1797, 1800–1805)
Capped Bust (1829–1837)
Washingtonx (1866; 1909–1910)
Seated Liberty (1837–1873)
Shield (1866–1883)
Liberty Head (1883–1913)
Buffalo (Indian Head) (1913–1938)
Jefferson (1938–present)
 
Nova Constellatiox (1783)
Dismex (1792)
Draped Bust (1796–1807)
Capped Bust (1809–1837)
Seated Liberty (1837–1891)
Barber (1892–1916)
Mercury (1916–1945)
Roosevelt (1946–present)
 
Seated Liberty (1875–1878)
 
Draped Bust (1796–1807)
Capped Bust (1815–1838)
Seated Liberty (1838–1891)
Barber (1892–1916)
Standing Liberty (1916–1930)
Washington (eagle reverse) (1932–1998)
Washington (bicentennial) (1975–1976)
Washington (50 State) (1999–2008)
Washington (D.C. and U.S. Territories) (2009)
Washington (America the Beautiful) (2010–present)
 
Nova Constellatiox (1783)
Flowing Hair (1794–1795)
Draped Bust (1796–1807)
Capped Bust (1807–1839)
Seated Liberty (1839–1891)
Barber (1892–1915)
Walking Liberty (1916–1947)
Franklin (1948–1963)
Kennedyc (1964–present)
Kennedy (bicentennial) (1975–1976)
 
Continental Currency (Fugio or Franklin)x (1776)
Nova Constellatiox (1783)
Flowing Hair (1794–1795)
Draped Bust (1795–1804)
Gobrechtx (1836–1839)
Seated Liberty (1840–1873)
Trade (1873–1885)
Morgan (1878–1904; 1921)
Peace (1921–1935, patterns struck in 1964)
Eisenhower (1971–1978)
Eisenhower (bicentennial) (1975–1976)
Susan B. Anthony (1979–1981; 1999)
Sacagaweac (2000–present)
Presidential (2007–2016)
American Innovationc (2018–present)
 Gold dollar ($1)
Liberty Head (1849–1854)
Indian Princess (1854–1889)
Quarter eagle ($2.50)
Draped Bust (1796–1807)
Capped Bust (1808–1834)
Classic Head (1834–1839)
Liberty Head (1840–1907)
Indian Head (1908–1929)
Three dollars ($3)
Indian Princess (1854–1889)
Half eagle ($5)
Draped Bust (1795–1807)
Capped Bust (1808–1834)
Classic Head (1834–1838)
Liberty Head (1839–1908)
Indian Head (1908–1929)
Eagle ($10)
Capped Bust (1795–1804)
Liberty Head (1838–1907)
Indian Head (1907–1933)
Double eagle ($20)
Liberty Head (1850–1907, pattern struck in 1849)
Quintuple Stellax (1879)
Saint-Gaudens (1907–1933)
 
Liberty Head (1849–1854)
Indian Princess (1854–1889)
 
Draped Bust (1796–1807)
Capped Bust (1808–1834)
Classic Head (1834–1839)
Liberty Head (1840–1907)
Indian Head (1908–1929)
 
Indian Princess (1854–1889)
 
Draped Bust (1795–1807)
Capped Bust (1808–1834)
Classic Head (1834–1838)
Liberty Head (1839–1908)
Indian Head (1908–1929)
 
Capped Bust (1795–1804)
Liberty Head (1838–1907)
Indian Head (1907–1933)
 
Liberty Head (1850–1907, pattern struck in 1849)
Quintuple Stellax (1879)
Saint-Gaudens (1907–1933)
 
Two and a half cent piece (2.5¢)x (not minted)
Two dollar piece ($2)x (not minted)
Stella ($4)x (1879–1880)
Half union ($50)x (1877)
Union ($100)x (not minted)
 (italics) Obsolete  •  x Canceled  •  (bold) Currently in production  •  c Currently produced for collectors only 
1¢
5¢
10¢
25¢
50¢
$1
 
5₥
1¢ (large size)
2¢
3¢ (silver)
3¢ (nickel)
5¢ (silver)
20¢
$1 (gold)
$2.5
$3
$5
$10
$20
 
2¢ (billon)
2.5¢
3¢ (bronze)
$2
$4
$50
$100
 
1800s
1900s
1910s
1920s
1930s
1940s
1950s
1970s
1980s
1990s
2000s
2010s
2020s
 
Silver Eagle (1986–present)
Gold Eagle (1986–present)
Platinum Eagle (1997–present)
Gold Buffalo (2006–present)
First Spouse (gold) (2007–2016)
Palladium Eagle (2017–present)
America the Beautiful (silver) (2010–present)
 
Proof Set (1936–present)
Mint Set (1947–present)
Special Mint Set (1964–1967)
Souvenir Set (1972–1998)
Silver Proof Set (1976, 1992–present)
Prestige Set (1983–1997)
 