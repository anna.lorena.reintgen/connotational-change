
 The Louisiana Purchase Sesquicentennial half dollar was a proposed United States commemorative coin, legislation for which passed both houses of Congress, but was vetoed in 1954 by President Dwight Eisenhower. Intended to celebrate the 150th anniversary of the Louisiana Purchase (1803), the coin was lobbied for by both the Missouri Historical Society (MHS) and the Louisiana Purchase 150th Anniversary Association of New Orleans, who hoped to be able to buy the entire coin issue from the government and sell it at a profit.
 Numismatist Eric P. Newman led the MHS's efforts, and corresponded with Congressman Thomas B. Curtis of Missouri, who helped push the bill forward with officials of the Louisiana group, such as Clay Shaw. Although many commemorative coins had been authorized by Congress in the 1930s, legislators passed few after that; the Treasury Department was strongly against their issue. When the House of Representatives held a hearing on the Louisiana Purchase Sesquicentennial half dollar, the bill was opposed by Assistant Director of the Mint  F. Leland Howard. The House passed the bill in April 1953, but the Senate was slow to act, passing it in January 1954, and after the House concurred with the Senate amendments, the bill was sent to Eisenhower later that month.
 Eisenhower vetoed the bill, as well as two other commemorative coin bills, on February 3, 1954. Congress made no attempt to override his vetoes. No commemorative coins were authorized or issued by the United States after 1954 until a new issue was struck in 1982.
 The Louisiana Purchase gave the United States over a million square miles of previously French territory for the price of $15 million. The Purchase was ratified by the U.S. Senate on October 20, 1803, and the new land subsequently doubled the size of the United States and opened the door to a new period of westward expansion.[1] In 1902, President Theodore Roosevelt signed a bill to subsidize the Louisiana Purchase Exposition, which would become known as the St. Louis World Fair of 1904. Of the $5 million paid to the fair by the government, $250,000 was in the form of commemorative gold dollar coins.[2]
 Beginning in the 1920s, the Treasury Department began to oppose the growing number of commemorative coins being authorized by the U.S. Congress. Many commemorative coin bills passed Congress in the mid-1930s. Some of these issues were deemed abusive, with coin dealers given an exclusive right to buy all the coins, or issues continuing for years, such as the Oregon Trail Memorial half dollar, first struck in 1926, last struck in 1939.[3] One such bill in 1938 was vetoed by President Franklin D. Roosevelt.[4] In 1939, Congress put an end to commemoratives for the time being, ordering an end to the multi-year series, such as the Oregon Trail issue.[5] President Harry S. Truman reluctantly signed bills for two issues in 1946,[6] but later also vetoed two.[7]
 Nearing the 150th anniversary of the Louisiana Purchase, unrelated attempts began by groups in Missouri and in Louisiana to get a commemorative half dollar authorized for that sesquicentennial.[8] On April 24, 1952, Eric Newman, a numismatist and a director of the Missouri Historical Society (MHS), wrote to George H. Moore, a federal judge and president of the society, proposing a commemorative coin for the anniversary. Newman told the judge the federal government had issued such coins in the past for similar occasions, and that such an issue could provide the $10,000 the society needed for its sesquicentennial activities.[9] By September 1952, this led to a regular correspondence with Thomas B. Curtis, the Republican congressman for Missouri's 12th district.[10] The MHS had learned of similar efforts by Louisianans, with a bill introduced by Congressman Hale Boggs, who had failed to get any action on his bill during the 88th Congress but who planned to try again in January 1953.[11] Both congressmen introduced bills in early 1953,[12][13] and agreed to work together to get a coin bill through Congress; their states' groups were urged to work together as well.[14]
 A hearing was held on the Boggs and Curtis bills before the House Banking and Currency Committee on March 3, 1953. Curtis spoke briefly before yielding to Boggs, a former member of the committee; before those present got down to business, there were reminiscences by Boggs and joking exchanges with members of the committee. Then, Boggs addressed the committee on the history of the Louisiana Purchase, stating that its great historic importance deserved the issuance of a coin.[15] William H. Semsrott, president of the Associated Retailers of St. Louis, a trade association, who was a director of the Missouri Historical Society, spoke next, followed by retired admiral Thomas J. Ryan, representing the Louisiana commission celebrating the sesquicentennial. Both urged the passage of a bill for commemorative coins. Semsrott told of the upcoming commemorations in St. Louis; Ryan mentioned an observance held during the Sugar Bowl in January, and that President Dwight D. Eisenhower would be coming to New Orleans later in the year to join the festivities.[16]
 Assistant Director of the Mint F. Leland Howard testified in opposition to the bills, stating he did not doubt the importance of the commemoration, but it was Treasury Department policy to oppose commemorative coin bills. He noted that this policy went back to before President Hoover's veto of the Gadsden Purchase half dollar bill in 1930, and had been adhered to by administrations of both parties. Howard told the committee that 250,000 gold dollars had been minted for the Louisiana Purchase centennial in 1903, but only 34,750 had been sold and the remainder melted, and that millions of the recent Booker T. Washington Memorial half dollar and Carver-Washington half dollar issues remained at the Mint, and might be melted. Howard stated that commemorative half dollars cost more to produce than the ordinary sort, due to the smaller amounts coined. He offered the Mint's assistance in the production of a non-legal tender medal, that could be authorized by Congress but would not cause confusion in the coinage.[17]
 Boggs spoke in rebuttal, stating that the Post Office Department was to issue a special stamp for the anniversary, which was in his view deserving of a coin. Howard noted that a special stamp in the hands of the public would be put on an envelope, mailed, and thereafter be handled by people familiar with stamps, something not true for a commemorative coin. The committee adjourned, to meet again in executive session to consider the bill.[18] On the evening of March 3, Semsrott sent a telegram to Newman, noting that the atmosphere of the hearing had been cordial, "but opposition from Treasury and Mint was very strong which in itself may defeat us".[19]
 On March 10, 1953, the House Banking Committee issued a report bearing the name of Jesse P. Wolcott of Michigan, the chairman. It proposed amendments to the Boggs bill, allowing both the Missouri and Louisiana groups to purchase coins at face value from the government for resale, as could any nonprofit group from a state that included Louisiana Purchase land, if authorized by the state's legislature. A maximum of 2,500,000 coins were to be struck, all at the Philadelphia Mint and dated 1953, with an initial minting of not less than 200,000 coins.[20] The report noted the Treasury Department objections to commemorative coins, and stated that the bill's provisions were intended to address them.[21] The bill, as amended, was called up on the House floor on April 13, 1953 and passed without discussion or dissent.[22]
 Efforts to reach an agreement between the two state societies had continued, and on March 20, Clay Shaw, managing director of the New Orleans-based Louisiana Purchase 150th Anniversary Association, wrote to Newman, stating that Seymour Weiss would negotiate for the association,[23] but when Weiss wrote to Newman on April 8, he stated he could see no point in working out a deal until the bill for a sesquicentennial coin was passed through Congress.[24] Newman telephoned Weiss long distance and convinced him arrangements needed to be worked out in advance how to divide the proceeds.[25]
 In the Senate, the bill was referred to the Committee on Banking and Commerce. The discussions between the two state committees were sidetracked on June 9 when the committee announced it would pass no commemorative coin bills that year. Weiss wrote, "apparently the Treasury Department opposition is fixed and most powerful."[26] Nevertheless, James G. Beall of Maryland reported back to the Senate on behalf of the Banking Committee on July 30, recommending passage. The bill called for the issuance of a maximum of 2,500,000 half dollars. Beall noted the Treasury Department objections, and stated,
 The possible small additional cost to the United States in the issuance of the commemorative coin which this bill authorizes and which from time to time the Congress may authorize is, in your committee's opinion, far outweighed by the benefits that redound to us as a people and a nation. Our history, our traditions, our institutions, those historic benchmarks in the development of this Nation—their commemoration are symbols of the spiritual and political development of our Nation, and they serve, as does our flag, to instill in the minds of our people that patriotic and spiritual fervor without which we, as a nation, could not survive. We must be just as vigilant, in fact more vigilant, about maintaining and encouraging the spiritual resources of our Nation as we are about the preservation and development of our physical and economic resources. The material resources of a nation can be dissipated or destroyed; the spirit, tradition, and sacred history of our Nation, if reasonably protected and developed, will not only never die but will also serve to make us strong physically and economically.[27]
 In September, Newman wrote to Curtis asking if there was any hope of getting the bill through, especially since Curtis' chief ally in the Senate, Robert A. Taft of Ohio, had recently died. Curtis replied that his plan had been to use Taft, but that he had almost gotten the bill passed through the efforts of Senator Russell Long of Louisiana, it being sidetracked at the last minute.[28][29]
 The bill was called up in the Senate by William Knowland of California on January 12, 1954, following that body passing bills for coins honoring New York City and Northampton, Massachusetts, on their 300th anniversaries. As neither senator from Louisiana was present, Knowland put the bill aside temporarily.[30] The bill was called up again after the arrival of Senator Long. He proposed several amendments, including that the coins be dated 1954 rather than 1953 as in the original bill, and addressed the Senate briefly. The Senate adopted the amendments and passed the bill without further discussion.[31] Charles van Ravenswaay, director of the Missouri Historical Society, wrote to Newman on January 18, calling the Senate passage "a nice surprise. And where do we go from here?"[32] Newman wrote to Curtis the following day, stating that he would order a copy of the bill and send any proposed amendments. Since the year of the sesquicentennial had by then passed, Newman thought it might be best to honor the Lewis and Clark Expedition on the coin, as it left St. Louis in 1804.[33]
 As the two houses had passed versions of the bill that were not identical, it returned to the House of Representatives, where on January 21, 1954, Wolcott called up the bill. Jacob Javits of New York asked if the consideration of the Louisiana bill meant there might be commemorative coins for New York City; Wolcott suggested Javits wait three minutes. The House agreed to the Senate amendments on the Louisiana bill, then passed it as well as the Northampton and New York City bills—the latter bills had originated in the Senate.[34] On January 25, the enrolled Louisiana bill was signed by the Speaker of the House, Joseph W. Martin Jr., and by the president of the Senate, Vice President Richard M. Nixon; the bill was then presented to President Eisenhower.[35]
 On February 3, 1954, Eisenhower vetoed the Louisiana Purchase bill, returning it unsigned to the House of Representatives where it originated, and listing his objections. He also vetoed the New York City and Northampton commemorative coin bills, similarly returning them to the Senate where they began. In the near-identical veto messages,[36] he noted that there was often not as much interest in such coins as expected, and that they opened a door for confusion and counterfeiting. Eisenhower stated, "I fully recognize the importance to the country of the event which this coin would commemorate. I recognize, too, that the authorization of 1 or 2 or 3 of such issues of coins would not do major harm. However, experience has demonstrated that the authorization of even a single commemorative issue brings forth a flood of other authorizations to commemorate events or anniversaries of local or national importance. In the administration of President Hoover these authorizations multiplied to the point where he felt compelled to exercise his veto."[37] No attempt was made to override any of Eisenhower's three vetoes.[38]
 Curtis sent a copy of the press release which included Eisenhower's veto message to Newman on February 10, regretting the outcome,[39] as did Newman in his reply, speculating that Eisenhower "probably was under the impression that these coins circulate and did not realize that they end up as souvenirs".[40]
 Wayne Homren, editor of the numismatic publication The E-Sylum, wrote in 2017, "What a shame—so much effort for nought. Yet the commemorative half program by that point was indeed getting bogged down and bloated, and something had to give."[10] After Eisenhower's vetoes, no commemorative coins were authorized until 1981, when a bill for the George Washington 250th Anniversary half dollar was passed with Treasury support, to be issued the following year. These coins were sold by the government, not issued to a private group at face value for resale at a profit.[41]
 
 1 Background 2 Origins and development 3 Hearing 4 Passage by Congress 5 Veto 6 References 7 Sources ^ Slabaugh, p. 23.
 ^ Swiatek & Breen, p. 120.
 ^ Bowers, pp. 24–27.
 ^ 1963 hearings, pp. 57–58.
 ^ Bowers, pp. 27, 431.
 ^ 1963 hearings, p. 57.
 ^ House hearings, p. 20.
 ^ House hearings, p. 2.
 ^ Newman, Eric P. (April 24, 1952). "Letter from Eric P. Newman to George H. Moore" (pdf). Newman Numismatic Portal..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b Augsburger, Len. "The Louisiana Purchase Half Dollar That Wasn't". E-Sylum. Numismatic Bibliomania Society. Archived from the original on August 4, 2019. Retrieved May 4, 2019.
 ^ Curtis, Thomas B. (October 1, 1952). "Letter from Thomas B. Curtis to Eric P. Newman" (pdf). Newman Numismatic Portal.
 ^ Curtis, Thomas B. (January 21, 1953). "Letter from Thomas B. Curtis to Eric P. Newman" (pdf). Newman Numismatic Portal.
 ^ Curtis, Thomas B. (February 4, 1953). "Letter from Thomas B. Curtis to Eric P. Newman" (pdf). Newman Numismatic Portal.
 ^ Curtis, Thomas B. (February 19, 1953). "Letter from Thomas B. Curtis to Eric P. Newman" (pdf). Newman Numismatic Portal.
 ^ House hearings, pp. 1–6.
 ^ House hearings, pp. 6–12.
 ^ House hearings, pp. 13–21.
 ^ House hearings, pp. 22–28.
 ^ Semsrott, William H. (March 3, 1953). "Telegram from William H. Semsrott to Eric P. Newman" (pdf). Newman Numismatic Portal.
 ^ House report, pp. 1–2.
 ^ House report, pp. 2–4.
 ^ 1953 Congressional Record, Vol. 100, p. 3015 Archived July 11, 2019, at the Wayback Machine (April 13, 1953)
 ^ Shaw, Clay (March 20, 1953). "Letter from Clay Shaw to Eric P. Newman". Newman Numismatic Portal.
 ^ Weiss, Seymour (April 8, 1953). "Letter from Seymour Weiss to Eric P. Newman". Newman Numismatic Portal.
 ^ Newman, Eric P. (April 13, 1953). "Letter from Eric P. Newman to Seymour Weiss". Newman Numismatic Portal.
 ^ Weiss, Seymour (June 16, 1953). "Letter from Seymour Weiss to Eric P. Newman". Newman Numismatic Portal.
 ^ Senate report, pp. 1–2.
 ^ Newman, Eric P. (September 23, 1953). "Letter from Eric P. Newman to Thomas B. Curtis" (pdf). Newman Numismatic Portal.
 ^ Curtis, Thomas B. (September 28, 1953). "Letter from Thomas B. Curtis to Eric P. Newman" (pdf). Newman Numismatic Portal.
 ^ 1954 Congressional Record, Vol. 100, pp. 160–163 (January 12, 1954)
 ^ 1954 Congressional Record, Vol. 100, pp. 188–189 (January 12, 1954)
 ^ Curtis, Thomas B. (September 28, 1953). "Letter from Thomas B. Curtis to Eric P. Newman" (pdf). Newman Numismatic Portal.
 ^ Newman, Eric P. (January 19, 1954). "Letter from Eric P. Newman to Thomas B. Curtis" (pdf). Newman Numismatic Portal.
 ^ 1954 Congressional Record, Vol. 100, Page 607–608 (January 21, 1954)
 ^ 1954 Congressional Record, Vol. 100, pp. 673, 762 Archived May 5, 2019, at the Wayback Machine (January 25, 1954)
 ^ Public Papers of the Presidents of the United States: 1954 Dwight D. Eisenhower. Washington, DC: General Printing Office. pp. 236–239. ISBN 978-1-62376-828-7.
 ^ "President vetoes commemorative bills". The Numismatist: 253–254. March 1954. Archived from the original on May 5, 2019. Retrieved May 5, 2019.
 ^ "Vetoes by President Dwight D. Eisenhower". United States Senate. Archived from the original on May 5, 2019. Retrieved May 5, 2019.
 ^ Curtis, Thomas B. (February 10, 1954). "Letter from Thomas B. Curtis to Eric P. Newman" (pdf). Newman Numismatic Portal.
 ^ Newman, Eric P. (February 15, 1954). "Letter from Eric P. Newman to Thomas B. Curtis" (pdf). Newman Numismatic Portal.
 ^ Ganz, pp. 1552–1555.
 Bowers, Q. David (1992). Commemorative Coins of the United States: A Complete Encyclopedia. Wolfeboro, NH: Bowers and Merena Galleries, Inc. ISBN 978-0-943161-35-8. Ganz, David L. (October 1991). "Commemorative coinage enters a new golden age". The Numismatist. American Numismatic Association: 1550–1557. (subscription required) Slabaugh, Arlie R. (1975). United States Commemorative Coinage (second ed.). Racine, WI: Whitman Publishing. ISBN 978-0-307-09377-6. Swiatek, Anthony; Breen, Walter (1981). The Encyclopedia of United States Silver & Gold Commemorative Coins, 1892 to 1954. New York: Arco Publishing. ISBN 978-0-668-04765-4. United States House of Representatives Committee on Banking and Currency (March 3, 1953). Coin to commemorate Louisiana Purchase. United States Government Printing Office. (subscription required) United States House of Representatives Committee on Banking and Currency (March 10, 1953). Louisiana Purchase sesquicentennial commemorative coin. United States Government Printing Office. (subscription required) United States House of Representatives Committee on Banking and Currency, Subcommittee on Consumer Affairs (May 20, 1963). Commemorative Medals and Coins Legislation. United States Government Printing Office. (subscription required) United States Senate Committee on Banking and Currency (July 30, 1953). Louisiana Purchase sesquicentennial commemorative coin. United States Government Printing Office. (subscription required) v t e ← 1940s United States commemorative coins (1950s) 1970s and 1980s → Booker T. Washington Memorial half dollar Booker T. Washington Memorial half dollar Carver-Washington half dollar Carver-Washington half dollar Carver-Washington half dollar Northampton, Massachusetts Tercentennial half dollar (cancelled) City of New York Tercentennial half dollar (cancelled) Louisiana Purchase Sesquicentennial half dollar (cancelled) v t e 1¢ 5¢ 10¢ 25¢ 50¢ $1 5₥ 1¢ (large size) 2¢ 3¢ (silver) 3¢ (nickel) 5¢ (silver) 20¢ $1 (gold) $2.5 $3 $5 $10 $20 2¢ (billon) 2.5¢ 3¢ (bronze) $2 $4 $50 $100 1800s 1900s 1910s 1920s 1930s 1940s 1950s 1970s 1980s 1990s 2000s 2010s 2020s Silver Eagle (1986–present) Gold Eagle (1986–present) Platinum Eagle (1997–present) Gold Buffalo (2006–present) First Spouse (gold) (2007–2016) Palladium Eagle (2017–present) America the Beautiful (silver) (2010–present) Proof Set (1936–present) Mint Set (1947–present) Special Mint Set (1964–1967) Souvenir Set (1972–1998) Silver Proof Set (1976, 1992–present) Prestige Set (1983–1997) United States portal Money portal Numismatics portal Early United States commemorative coins Webarchive template wayback links Use American English from September 2019 All Wikipedia articles written in American English Use mdy dates from September 2019 Articles with short description Pages containing links to subscription-only content Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version  This page was last edited on 12 November 2019, at 17:15 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  The possible small additional cost to the United States in the issuance of the commemorative coin which this bill authorizes and which from time to time the Congress may authorize is, in your committee's opinion, far outweighed by the benefits that redound to us as a people and a nation. Our history, our traditions, our institutions, those historic benchmarks in the development of this Nation—their commemoration are symbols of the spiritual and political development of our Nation, and they serve, as does our flag, to instill in the minds of our people that patriotic and spiritual fervor without which we, as a nation, could not survive. We must be just as vigilant, in fact more vigilant, about maintaining and encouraging the spiritual resources of our Nation as we are about the preservation and development of our physical and economic resources. The material resources of a nation can be dissipated or destroyed; the spirit, tradition, and sacred history of our Nation, if reasonably protected and developed, will not only never die but will also serve to make us strong physically and economically.[27]
 Louisiana Purchase Sesquicentennial half dollar ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ 
Booker T. Washington Memorial half dollar
 
Booker T. Washington Memorial half dollar
Carver-Washington half dollar
 
Carver-Washington half dollar
 
Carver-Washington half dollar
Northampton, Massachusetts Tercentennial half dollar (cancelled)
City of New York Tercentennial half dollar (cancelled)
Louisiana Purchase Sesquicentennial half dollar (cancelled)
 
1¢
5¢
10¢
25¢
50¢
$1
 
5₥
1¢ (large size)
2¢
3¢ (silver)
3¢ (nickel)
5¢ (silver)
20¢
$1 (gold)
$2.5
$3
$5
$10
$20
 
2¢ (billon)
2.5¢
3¢ (bronze)
$2
$4
$50
$100
 
1800s
1900s
1910s
1920s
1930s
1940s
1950s
1970s
1980s
1990s
2000s
2010s
2020s
 
Silver Eagle (1986–present)
Gold Eagle (1986–present)
Platinum Eagle (1997–present)
Gold Buffalo (2006–present)
First Spouse (gold) (2007–2016)
Palladium Eagle (2017–present)
America the Beautiful (silver) (2010–present)
 
Proof Set (1936–present)
Mint Set (1947–present)
Special Mint Set (1964–1967)
Souvenir Set (1972–1998)
Silver Proof Set (1976, 1992–present)
Prestige Set (1983–1997)
 