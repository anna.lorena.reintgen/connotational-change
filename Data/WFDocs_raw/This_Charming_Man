
 "This Charming Man" is a song by the English rock band the Smiths, written by guitarist Johnny Marr and singer Morrissey. Released as the group's second single in October 1983 on the independent record label Rough Trade, it is defined by Marr's jangle pop guitar riff and Morrissey's characteristically morose lyrics, which revolve around the recurrent Smiths themes of sexual ambiguity and lust.[1]
 Feeling detached from the early 1980s mainstream gay culture, Morrissey wrote "This Charming Man" to evoke an older, more coded and self-aware underground scene. The singer said of the song's lyrics: "I really like the idea of the male voice being quite vulnerable, of it being taken and slightly manipulated, rather than there being always this heavy machismo thing that just bores everybody."[2]
 Although only moderately successful on first release—the single peaked at number 25 on the UK Singles Chart, "This Charming Man" has been widely praised in both the music and mainstream press. Re-issued in 1992, it reached number 8 on the UK Singles Chart (making it the Smiths' biggest UK hit by chart position). In 2004, BBC Radio 2 listeners voted it number 97 on the station's "Sold on Song Top 100" poll.[3] Mojo magazine journalists placed the track at number 1 on their 2008 "50 Greatest UK Indie Records of All Time" feature.[2] It was certified Silver by the British Phonographic Industry in 2015.[4]
 By early 1983, the Smiths had gained a large following on the UK live circuit and had signed a record deal with the indie label Rough Trade. The deal, along with positive concert reviews in the weekly music press and an upcoming session on John Peel's radio show on BBC Radio 1, generated a large media buzz for the band. In a music scene dominated by corporate and video-driven acts, the Smiths' camp and bookish image stood out, and many expected the band to be the breakthrough act of the UK post-punk movement.[2] The previous October Frankie Goes to Hollywood released their iconic track "Relax", which was seen as an anthem to an out alpha male self-assertiveness, and alien to many UK homosexuals. However, the Smiths' May 1983 debut single "Hand in Glove" failed to live up to critical and commercial expectations, mostly due to its perceived low production values. When Rough Trade label mates Aztec Camera began to receive day-time national radio-play with their track "Walk out to Winter", Marr admitted to "feeling a little jealous, my competitive urges kicked in". The guitarist believed the Smiths needed an up-beat song, "in a major key", to gain a chart positioning that would live up to expectations.[2]
 Marr wrote the music to "This Charming Man" especially for the Peel session[5] on the same night that he wrote "Still Ill" and "Pretty Girls Make Graves".[6] Based on the Peel performance, Rough Trade label head Geoff Travis suggested that the band release the song as a single instead of the slated release "Reel Around the Fountain", which had gathered notoriety in the press due to what were seen as lyrical references to pedophilia.[7][8] The Smiths entered Matrix Studios in London on September 1983 to record a second studio version of the song for release as a single.[7] However, the result—known as the 'London version'—was unsatisfactory and soon after, the band travelled to Strawberry Studios in Stockport to try again. Here, they recorded the more widely heard A-side.[9]
 The lyrics of "This Charming Man" comprise a first person narrative in which the male protagonist punctures one of his bicycle's wheels on a remote hillside. A passing "charming man" in a luxury car stops to offer the cyclist a lift, and although the protagonist is at first hesitant, after much deliberation he accepts the offer. While driving together the pair flirt, although the protagonist finds it difficult to overcome his reluctance: "I would go out tonight, but I haven't got a stitch to wear". The motorist tells the cyclist: "it's gruesome that someone so handsome should care".[2]
 Morrissey deliberately used archaic language when composing the voice-over style lyrics for "This Charming Man". His use of phrases and words such as 'hillside desolate', 'stitch to wear', 'handsome' and 'charming' are used to convey a more courtly world than the mid-Eighties north of England, and evoke a style that has, in the words of the music critic Mat Snow, "nothing to do with fashion".[2] Morrissey had already used the word 'handsome' in a song title—in "Handsome Devil", the B-side to "Hand in Glove"—and observed in a 1983 interview with Barney Hoskyns that he used the word to "try and revive some involvement with language people no longer use. In the daily scheme of things, people's language is so frighteningly limited, and if you use a word with more than 10 letters it's absolute snobbery."[2] Snow puts forward the case that through the use of the dated word 'charming', Morrissey sought to rebel against the then mainstream gay culture from which he felt alienated. Morrissey told Hoskyns: "I hate this 'festive faggot' thing ... People listen to "This Charming Man" and think no further than what anyone would presume. I hate that angle, and it's surprising that the gay press have harped on more than anyone else. I hate it when people talk to me about sex in a trivial way."[2]
 As with many of Morrissey's compositions, the song's lyrics features dialogue borrowed from a cult film. The line "A jumped-up pantry boy who never knew his place" is borrowed from the 1972 film adaptation of Anthony Shaffer's 1970 homoerotic play Sleuth, in which Laurence Olivier plays a cuckolded author to Michael Caine's 'bit of rough'.[2][9]
 Both studio versions begin with an introductory guitar riff, joined by the rhythm section. Morrissey's vocals are first heard eight seconds into the track. His vocal melodies are diatonic, and consciously avoid blues inflections.[10] The chorus is played twice; the first time it is followed by a brief pause, the second by the closing of the song. The rhythm section of Andy Rourke and Mike Joyce provide a beat more danceable than usual for a Smiths track. The drums were originally programmed on a Linn Drum Computer, under the direction of producer John Porter. Porter used the programme to trigger the sampled sounds of the live drum kit, featuring a Motownesque bassline.[11] Marr's guitar part consists of single notes and thirds as opposed to strummed bar chords, and his guitar serves to creates a counter-melody throughout the song. Marr overdubbed numerous guitar parts onto the song,[10] and in a December 1993 interview, told Guitar Player magazine:
 I'll try any trick. With the Smiths, I'd take this really loud Telecaster of mine, lay it on top of a Fender Twin Reverb with the vibrato on, and tune it to an open chord. Then I'd drop a knife with a metal handle on it, hitting random strings. I used it on "This Charming Man", buried beneath about 15 tracks of guitar ... it was the first record where I used those highlife-sounding runs in 3rds. I'm tuned up to F# and I finger it in G, so it comes out in A. There are about 15 tracks of guitar. People thought the main guitar part was a Rickenbacker, but it's really a '54 Tele. There are three tracks of acoustic, a backwards guitar with a really long reverb, and the effect of dropping knives on the guitar – that comes in at the end of the chorus.[12]
 The chord progression for the song, from the instrumental intro to the lyric "Will nature make a man of me" is: A | Asus4 | A | E | Bm7 | D7 | C#m | E | A | E/A | Asus4 | E[13]
 On release, the song received near unanimous critical praise. Paul Morley of the NME wrote, "'This Charming Man' is an accessible bliss, and seriously moving. This group fully understand that the casual is not enough ... This is one of the greatest singles of the year, a poor compliment. Unique and indispensable, like 'Blue Monday' and 'Karma Chameleon' – that's better!"[14] A contemporary review in The Face asked, "Where has all the wildness and daring got to? Some of it has found its way onto the Smiths' record, 'This Charming Man'. It jangles and crashes and Morrissey jumps in the middle with his mutant choir-boy voice, sounding jolly and angst-ridden at the same time. It should be given out on street corners to unsuspecting passers-by of all ages."[15] Another contemporary review by Treble magazine described the song as a "stellar jangle-pop track," based on one of Marr's first truly iconic guitar licks.[16] While the band was little-known in the United States at the time, Robert Palmer of The New York Times described the song as "sparkling, soaring, superlative pop-rock, and proof that the guitar-band format pioneered by the Beatles is still viable for groups with something to say".[17] The following year, Palmer chose the song as the second best single of 1984.[18]
 AllMusic's Ned Raggett noted that "Early Elvis would have approved of the music, Wilde of the words", and described the track as "an audacious end result by any standard".[19] Tim DiGravina, of the same organisation, wrote that "Debating the merits of the track here would be a bit pointless, as it's a classic song from one of the last great classic bands. It might as well be called 'This Charming Song'."[20] In 2007, Oasis songwriter Noel Gallagher described the first time he heard the track: "The second I heard 'This Charming Man' everything made sense. The sound of that guitar intro was incredible. The lyrics are fuckin' amazing, too. People say Morrissey's a miserable cunt, but I knew straight away what he was on about."[21] In 2006, Liz Hoggard from The Independent said that "This Charming Man ... is about age-gap, gay sex".[22]
 During an appearance on Top of the Pops, Morrissey appeared waving gladioli.[1] A 2004 BBC Radio 2 feature on the song noted that the performance was most people's introduction to The Smiths and, "therefore, to the weird, wordy world of Morrissey and the music of Johnny Marr".[3] Uncut magazine, commentating on the nationally televised debut, wrote that "Thursday evening when Manchester's feyest first appeared on Top of the Pops would be an unexpected pivotal cultural event in the lives of a million serious English boys. His very English, camp glumness was a revolt into Sixties kitchen-sink greyness against the gaudiness of the Eighties new wave music, as exemplified by Culture Club and their ilk. The Smiths' subject matter may have been 'squalid' but there was a purity of purpose about them that you messed with at your peril."[15] Noel Gallagher said of the performance: "None of my mates liked them — they were more hooligan types. They came into work and said 'Fuckin' hell, did you see that poof on "Top of the Pops" with the bush in his back pocket?' But I thought it was life-changing."[21]
 The earliest version of "This Charming Man" was recorded on 14 September 1983, in Maida Vale Studio 4, for John Peel's radio programme (first broadcast: 21 September 1983).[23] Produced by Roger Pusey, and assisted by Ted De Bono, this version of the song was first included on the 1984 compilation Hatful of Hollow. On 28 October 1983, the "Manchester" version was released in the UK in 7" and 12" formats, reaching number 25 in the UK charts.[24] The record sleeve uses a still frame from Jean Cocteau's 1949 film Orphée, featuring French actor Jean Marais.[2] The song was later included as a bonus track on the cassette version of the band's debut album The Smiths in the UK,[25] and subsequently on all American versions.
 Following the 1989 bankruptcy of Rough Trade,[26] WEA Records purchased the Smiths' back catalogue.[27] In 1992 WEA re-issued the band's catalogue, and all subsequent pressings of The Smiths have incorporated "This Charming Man". WEA re-released the single itself in 1992 to support the Best... I compilation album. The reissued single reached number 8 on the British singles chart, the band's highest chart placing.[28][29]
 In December 1983, DJ François Kevorkian released a "New York" mix of the single on Megadisc records.[30] Kevorkian geared the song for nightclub dancefloors. The track was intended to be pressed in limited numbers for New York club DJs. However, Rough Trade boss Geoff Travis liked the mix and gave the release wide distribution in the UK.[31] Morrissey publicly disowned the mix, and urged fans not to purchase copies.[30] Travis later claimed, "it was my idea, but they agreed. They said 'Go ahead', then didn't like it so it was withdrawn." He also said, "Nothing that ever happened in the Smiths occurred without Morrissey's guidance; there's not one Smiths record that went out that Morrissey didn't ask to do, so there's nothing on my conscience."[31]
 Death Cab for Cutie covered "This Charming Man" for their 1997 demo You Can Play These Songs with Chords.[32] In 2001, Canadian indie pop band Stars covered the song for their debut album Nightsongs.[33]
 "Jeane" "Accept Yourself" "Wonderful Woman" 7" vinyl 12" vinyl Jangle pop pop rock Johnny Marr Morrissey Roger Pusey (Peel version) John Porter (Single version) 1 Background 2 Music and lyrics 3 Reception 4 Versions and release history 5 Cover versions 6 Track listing 7 Personnel 8 Chart positions 9 References

9.1 Bibliography

 9.1 Bibliography 10 External links Morrissey – lead vocals Johnny Marr – electric and acoustic guitar Andy Rourke – bass guitar Mike Joyce – drums ^ a b Strong 2000, p. 901.
 ^ a b c d e f g h i j Snow 2008.
 ^ a b "BBC – Radio 2 – Sold On Song Top 100: 'This Charming Man' – The Smiths". BBC Radio 2. 2004. Retrieved 23 November 2005..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ "Certified Awards". British Phonographic Industry. Retrieved 28 August 2016.
 ^ "John Peel Biography" (PDF). BBC Online. 2005. p. 15. Retrieved 22 March 2008.
 ^ Maconie, Stuart (December 1993). "The Secret History of ...". Select. ISSN 0959-8367.
 ^ a b Rogan 1993, p. 178.
 ^ a b Young 2006, p. 102.
 ^ a b Goddard 2004, p. 50.
 ^ a b Rooksby 2001, p. 107.
 ^ Mulholland, Garry (2002). This Is Uncool: The 500 Greatest Singles Since Punk and Disco. Cassell. ISBN 0-304-36186-0.
 ^ Gore, Joe (January 1990). "Guitar Anti-hero". Guitar Player. ISSN 0017-5463.
 ^ "This Charming Man By The Smiths – Digital Sheet Music". MusicNotes.com. Universal Music Publishing Group. Retrieved 27 April 2016.
 ^ Morley, Paul (12 November 1983). "This Charming Man". NME. ISSN 0028-6362.
 ^ a b "This Charming Man". The Face. Archived from the original on 24 May 2011.
 ^ "The Smiths: The Complete Songs". Treble. 20 March 2014. Retrieved 11 May 2015.
 ^ Palmer, Robert (22 February 1984). "The Pop Life; Rap and Hip-Hop Music in 'Wild Style'". The New York Times. Retrieved 8 March 2008.
 ^ Palmer, Robert (9 January 1985). "Prince Leads Critic's List of Top 10". The New York Times. Retrieved 8 March 2008.
 ^ Raggett, Ned. "This Charming Man – Song Review". AllMusic. All Media Network. Retrieved 8 March 2008.
 ^ DiGravina, Tim. "The Smiths – This Charming Man". AllMusic. All Media Network. Retrieved 8 March 2008.
 ^ a b "Last night I dreamed that somebody loved me". Uncut. March 2007. ISSN 1368-0722.
 ^ Hoggard, Liz (4 June 2006). "Morrissey: The Alan Bennett of pop". The Independent. Retrieved 24 July 2014.
 ^ "14/09/1983 – The Smiths". BBC Online. Retrieved 22 March 2008.
 ^ "Charm offensive". Record Collector.
 ^ "Smiths, The – The Smiths (Cassette, Album)". Discogs. Retrieved 6 May 2011.
 ^ Sinclair, David (8 September 2006). "Rough with the smooth". The Independent. p. 14.
 ^ Gennoe, Dan (2006). "Second Coming" (PDF). British Council. Archived from the original (PDF) on 2 June 2014.
 ^ Rogan, Johnny (November–December 1992). "Johnny Marr's View". Record Collector. ISSN 0261-250X.
 ^ a b "Official Singles Chart Top 100". Official Charts Company.  Retrieved 24 July 2014.
 ^ a b "Man about the house". The Times. 13 March 2004. Archived from the original on 17 May 2011.
 ^ a b Rogan 1993, p. 182.
 ^ Eliscu, Jenny (14 November 2002). "Death Cab For Cutie – You Can Play These Songs With Chords". Rolling Stone. Archived from the original on 3 May 2009. Retrieved 10 March 2007.
 ^ Studarus, Laura (14 October 2014). "Stars: Gang of Losers". Paste. Retrieved 11 May 2015.
 ^ "Official Singles Chart Top 100". Official Charts Company.  Retrieved 24 July 2014.
 ^ Kent, David (1993). Australian Chart Book 1970-1992. St Ives, N.S.W.: Australian Chart Book. ISBN 0-646-11917-6.
 ^ "Charts.nz – The Smiths – This Charming Man". Top 40 Singles.  Retrieved 24 July 2014.
 ^ "The Irish Charts – Search Results – This Charming Man". Irish Singles Chart.  Retrieved 24 July 2014.
 Goddard, Simon (2004). The Smiths: Songs That Saved Your Life. Reynolds & Hearn. ISBN 1-903111-84-6. Rogan, Johnny (December 1993). Morrissey & Marr: The Severed Alliance. London: Omnibus Press. ISBN 0-7119-3000-7. Rooksby, Rikky (September 2001). Inside Classic Rock Tracks. Backbeat Books. ISBN 0-87930-654-8. Snow, Mat (March 2008). "Ello 'Andsome!". Mojo. ISSN 1351-0193. Strong, Martin Charles (2000). The Great Rock Discography. Edinburg: Canongate. ISBN 978-1-84195-017-4. Young, Rob (2006). Rough Trade – Labels Unlimited. London: Black Dog Publishing. ISBN 1-904772-47-1. The Smiths discography at Discogs Lyrics of this song at MetroLyrics v t e Morrissey Johnny Marr Mike Joyce Andy Rourke Craig Gannon The Smiths Meat Is Murder The Queen Is Dead Strangeways, Here We Come Rank Hatful of Hollow The World Won't Listen Louder Than Bombs Stop Me Best... I ...Best II Singles The Very Best of The Smiths The Sound of The Smiths The Smiths Singles Box Complete The Peel Sessions "Hand in Glove" "This Charming Man" "What Difference Does It Make?" "Heaven Knows I'm Miserable Now" "William, It Was Really Nothing" "How Soon Is Now?" "Shakespeare's Sister" "Barbarism Begins at Home" "That Joke Isn't Funny Anymore" "The Boy with the Thorn in His Side" "Bigmouth Strikes Again" "Panic" "Ask" "Shoplifters of the World Unite" "Sheila Take a Bow" "Girlfriend in a Coma" "I Started Something I Couldn't Finish" "Last Night I Dreamt That Somebody Loved Me" "Stop Me If You Think You've Heard This One Before" "There Is a Light That Never Goes Out" "Sweet and Tender Hooligan" "Suffer Little Children" "Please, Please, Please, Let Me Get What I Want" "Some Girls Are Bigger Than Others" Discography Songs List of live performances The Smiths Is Dead 1983 singles 1983 songs 1992 singles LGBT-related songs Rough Trade Records singles The Smiths songs Songs written by Johnny Marr Songs written by Morrissey Featured articles EngvarB from June 2014 Use dmy dates from June 2014 Articles with hAudio microformats Singlechart usages for UK Singlechart called without artist Singlechart called without song Singlechart usages for New Zealand Singlechart usages for Ireland2 Singlechart usages for United Kingdom Singlechart making named ref Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Español 한국어 Italiano Nederlands Polski Русский Svenska Українська  This page was last edited on 9 July 2019, at 09:01 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  I'll try any trick. With the Smiths, I'd take this really loud Telecaster of mine, lay it on top of a Fender Twin Reverb with the vibrato on, and tune it to an open chord. Then I'd drop a knife with a metal handle on it, hitting random strings. I used it on "This Charming Man", buried beneath about 15 tracks of guitar ... it was the first record where I used those highlife-sounding runs in 3rds. I'm tuned up to F# and I finger it in G, so it comes out in A. There are about 15 tracks of guitar. People thought the main guitar part was a Rickenbacker, but it's really a '54 Tele. There are three tracks of acoustic, a backwards guitar with a really long reverb, and the effect of dropping knives on the guitar – that comes in at the end of the chorus.[12]
 This Charming Man This Charming Man a b a b c d e f g h i j a b ^ ^ ^ a b a b a b a b ^ ^ ^ ^ a b ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ a b a b a b ^ ^ ^ ^ ^ ^ Morrissey Johnny Marr Mike Joyce Andy Rourke  "Jeane""Accept Yourself""Wonderful Woman" 31 October 1983 7" vinyl12" vinyl October 1983, at Strawberry Studios in Stockport, England Jangle poppop rock 2:41 Rough Trade Johnny MarrMorrissey Roger Pusey (Peel version)John Porter (Single version) 


"Hand in Glove" (1983)

"This Charming Man" (1983)

"What Difference Does It Make?" (1984)
 "Hand in Glove" (1983)
 "This Charming Man" (1983)
 "What Difference Does It Make?" (1984)
 1. "This Charming Man" 2:41 2. "Jeane" 3:02 1. "This Charming Man (Manchester)" (Same as Original Single Version) 2:41 2. "This Charming Man (London)" 2:47 3. "Accept Yourself" 3:55 4. "Wonderful Woman" 3:08 1. "This Charming Man (New York) Vocal" (Remixed by François Kevorkian) 5:35 2. "This Charming Man (New York) Instrumental" (Remixed by François Kevorkian) 4:18 1. "This Charming Man (Manchester)" (Same as Original Single Version) 2:41 2. "Jeane" 3:02 3. "Wonderful Woman" 3:08 4. "Accept Yourself" 3:55 1. "This Charming Man (Manchester)" (Same as Original Single Version) 2:41 2. "This Charming Man (London)" 2:47 3. "This Charming Man (New York Vocal)" 5:33 4. "This Charming Man (New York Instrumental)" 4:19 5. "This Charming Man (Peel Session from 21 September 1983)" 2:43 6. "This Charming Man (Single Remix)" 2:46 7. "This Charming Man (Original Single Version)" 2:41 1. "This Charming Man (Manchester)" (Same as Original Single Version) 2:41 2. "Jeane" 3:02 3. "Accept Yourself" 3:55 1. "This Charming Man (Manchester)" (Same as Original Single Version) 2:41 2. "This Charming Man (London)" 2:47 3. "This Charming Man (New York Vocal)" 5:33 4. "This Charming Man (New York Instrumental)" 4:19 5. "This Charming Man (Peel Session from 21 September 1983)" 2:43 6. "This Charming Man (Single Remix)" 2:46 7. "This Charming Man (Original Single Version)" 2:41 8. "Wonderful Woman" 3:08 1. "This Charming Man (Manchester)" (Same as Original Single Version) 2:41 2. "This Charming Man (London)" 2:47 3. "This Charming Man (New York Vocal)" 5:33 4. "This Charming Man (New York Instrumental)" 4:19 5. "This Charming Man (Peel Session from 21 September 1983)" 2:43 6. "This Charming Man (Single Remix)" 2:46 7. "This Charming Man (Original Single Version)" 2:41 8. "Jeane" 3:02 9. "Wonderful Woman" 3:08 10. "Accept Yourself" 3:55 1983
 UK Indie Chart (Official Charts Company)[8]
 1
 UK Singles (Official Charts Company)[34]
 25
 1984
 Australia (Kent Music Report)[35]
 52
 New Zealand (Recorded Music NZ)[36]
 15
 1992
 Ireland (IRMA)[37]
 9
 UK Singles (Official Charts Company)[29]
 8
 
Morrissey
Johnny Marr
Mike Joyce
Andy Rourke
Craig Gannon
 
The Smiths
Meat Is Murder
The Queen Is Dead
Strangeways, Here We Come
 
Rank
 
Hatful of Hollow
The World Won't Listen
Louder Than Bombs
Stop Me
Best... I
...Best II
Singles
The Very Best of The Smiths
The Sound of The Smiths
The Smiths Singles Box
Complete
 
The Peel Sessions
 
"Hand in Glove"
"This Charming Man"
"What Difference Does It Make?"
"Heaven Knows I'm Miserable Now"
"William, It Was Really Nothing"
"How Soon Is Now?"
"Shakespeare's Sister"
"Barbarism Begins at Home"
"That Joke Isn't Funny Anymore"
"The Boy with the Thorn in His Side"
"Bigmouth Strikes Again"
"Panic"
"Ask"
"Shoplifters of the World Unite"
"Sheila Take a Bow"
"Girlfriend in a Coma"
"I Started Something I Couldn't Finish"
"Last Night I Dreamt That Somebody Loved Me"
"Stop Me If You Think You've Heard This One Before"
"There Is a Light That Never Goes Out"
"Sweet and Tender Hooligan"
 
"Suffer Little Children"
"Please, Please, Please, Let Me Get What I Want"
"Some Girls Are Bigger Than Others"
 
Discography
Songs
List of live performances
The Smiths Is Dead
 