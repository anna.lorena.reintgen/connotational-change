The Michigan State Capitol is the building that houses the legislative branch of the government of the U.S. state of Michigan. It is in the portion of the state capital of Lansing which lies in Ingham County. The present structure, at the intersection of Capitol and Michigan Avenues, is a National Historic Landmark that houses the chambers and offices of the Michigan Legislature as well as the ceremonial offices of the Governor of Michigan and Lieutenant Governor. Historically, this is the third building to house the Michigan government.[3]
 The first state capitol was in Detroit, the original capital of Michigan, and was relocated to Lansing in 1847, due to the need to develop the state's western portion and for better defense from British troops stationed in Windsor, Ontario.
 On July 13, 1787, the Second Continental Congress passed the Northwest Ordinance, creating the Northwest Territory which included Michigan. In 1805, the U.S. Congress created the Michigan Territory, with Detroit as its territorial capital.[4] Michigan first applied for statehood as early as 1832, though it was rebuffed due to a dispute with Ohio over the Toledo Strip, a 468-square mile (1,210 km²) area that included the important port city of Toledo. By 1835, Michigan had formed a state government without receiving authorization from Congress to do so. The state's boundaries included the contested area.
 The dispute culminated in what has become known as the Toledo War, as Michigan and Ohio militia took up arms in the area. As a condition for entering the Union, Michigan was forced to accept the eastern three-quarters of the Upper Peninsula in exchange for ceding its claim to the Toledo Strip. After a state convention first rejected this condition, a second convention, assembled under some duress in December 1836, reluctantly accepted the terms and Michigan became the 26th state on January 26, 1837, with Detroit as its first capital.
 The first building to serve as the State Capitol was built in 1832 as the Territorial Courthouse. The court house was on the corner of Griswold Street and State Street. This brick structure was one of Michigan's earliest Greek revival buildings, with a portico of Ionic columns and a central tower of 140 feet (42 m). Built at a cost of $24,500 ($610,000 in 2019), the building housed the territorial government and state legislatures until 1848, when a hastily erected wood building was constructed in Lansing following a decision made March 17, 1847, to move the capital from Detroit to Lansing. The Detroit building then became a public school (the Union School, at one time the city's only high school) and library until it burned in 1893.[3]
 The 1835 Michigan Constitution[5] provided that:
 Prior to 1847, Detroit fought to maintain the Capitol within its jurisdiction, but communities in the growing western part of the state had many reasons for wanting a move inland, including the need to increase defensibility by moving the state government away from the Canada–US border. Detroit had been occupied during the War of 1812, and the border area of Michigan of less than a mile between Detroit and Windsor, Ontario, at the Detroit River continued to be occupied by British troops on both banks. Proponents of moving the capitol also sought to promote settlement and the economy in the interior, as well as making the government more accessible to the people throughout the state.[3]
 Contenders seeking designation as the new Capitol included Ann Arbor, Jackson, Grand Rapids, and Shiawassee Township in Shiawassee County. At one point during the debate, Marshall officials were so certain of its selection that they built a governor's mansion.[3] After extensive debate, State Senator Joseph H. Kilbourne of Ingham County proposed that the nearly uninhabited Lansing Township be made the seat of government. The legislature agreed, with the location north of Ann Arbor, west of Detroit, and east of Grand Rapids being deemed a suitable compromise. The legislature renamed it as the Town of Michigan, though by 1848 the original name of Lansing was restored.[4]
 Construction began in 1847 on the state capitol building in Lansing, a temporary structure on the block bordered by Washington Avenue, Capitol Avenue, Allegan Street, and Washtenaw Street. It was a simple two-story wood frame structure, painted white with green wooden shutters and topped by a tin cupola. The total cost for construction was $22,952.01 ($620,000 in 2019). The building was sold when the permanent capitol building opened in 1879. It was then used as a factory until, like the first capitol, it was destroyed by a fire in 1882.[3]
 In the early 1870s, Governor Henry P. Baldwin urged the legislature to fund a new, permanent capitol. On March 31, 1871, a bill was adopted "for the erection of a new state capitol, and a building for the temporary use of the state officers." The new capitol was to cost $1.2 million ($25,000,000 in 2019), to be raised by a six-year state income tax.[3]
 In 1872, architect Elijah E. Myers of Springfield, Illinois, was commissioned to design the new capitol building and the design committee selected his design named Tuebor, which means I will defend.[4] Myers used the central dome and wing design found in the United States Capitol in his design and subsequently went on to design two other state capitol buildings, the statehouses of Colorado and Texas, as well as the former territorial capitol building of Idaho, the most by any architect.[6] The cornerstone was laid on October 2, 1873, with about 7,000 Lansing residents and some 30,000 to 50,000 visitors attending.[3] Construction and finishing work were completed by late 1878. The new capitol, with 139 rooms, was dedicated at the same time as the inauguration of Governor Charles Croswell January 1, 1879.[3]
 The Lansing capitol building inspired a national trend after the American Civil War for fireproof buildings, large enough to house expanding government as well as serving as a durable repository for artifacts of the war,[7] including battle flags that were moved to the Michigan Historical Museum in 1990. Over the years the dome, which at first matched the light tan of the building, was repainted a bright white. The legislature funded an extensive historical restoration starting in 1989 which was completed in 1992.[4] The restoration returned the dome to a creamy-white shade, upgraded mechanical systems, and improved accessibility as well as restoring many of the original design elements. One of the restoration's largest phases entailed removal of "half-floors" that were installed in 1969 to create 50,000 square feet (4,600 m2) of additional office space. The floors were created by dividing the 16-foot (4.9 m)-high rooms horizontally and creating a level of rooms which was accessed from the stairway landing.[8]  The Capitol Building was listed on the National Register of Historic Places January 25, 1971 (NRHP Reference #71000396), and was designated as a National Historic Landmark October 5, 1992.[9]
 The Michigan State Capitol is 267 ft (81 m) from the ground to the tip of finial/spire above the dome. The building is 420 ft 2 in (128.07 m) long and 273 ft 11 in (83.49 m) wide (including approaches). The capitol occupies 1.16 acres (4,700 m2), has a perimeter of 1,520 ft (460 m).[7] The structure contains  four stories, with public entrances on the ground floor. Two grand staircases in the north and south corridors go up to the top floor.[4]  The rotunda measures 44.5 feet (13.6 m) in diameter and 160 feet (49 m) in height measured from the floor to the oculus.[10]
 When it opened, the Capitol structure was large enough to host all the state agencies and departments. Due to the growth of state government, however, only the offices of Senate and House leadership and ceremonial offices for the governor and lieutenant governor remain in the capitol. The ground floor corridors led to "store rooms" designed by the architect in the original building plans. This includes an armory in the southwest corner of the south corridor. The original wood floor has been replaced by gray tiles. The rooms were originally lit with gas fixtures, though by 1900, the building had been refitted with electric lights. Today, the ground floor is home to several offices, including the Secretary of the Senate, the Clerk of the House, and the Capitol Tours and Information Service.
 Starting with the first floor and continuing to the third, black and white floor tiling is made of Vermont marble and limestone. The exception is the floor of the rotunda which is composed of 976 blocks of translucent glass. The blocks vary in size so that when viewed from the upper floors, they appear to form a bowl which mirrors the dome above. The doorknobs are mostly made of a brass and bronze alloy (most of the original brass doorknobs have disappeared over time). The present doorknobs and hinges, locked for protection, display the state coat of arms. Though the building appears to have walnut woodwork, the wood is Michigan white pine that has been wood-grained to give the appearance of walnut.
 The first floor gives visitors their first view of the interior of the rotunda. Below the cast-iron dome, the ceiling displays eight muses painted in 1886. For more than a century, the muses' artist remained anonymous; it is now known that it was Tommaso Juglaris, who created them in his Boston studio and never came to Michigan.  In the east wing of the first floor is a large clock, called a long-drop clock. It was once the building's master clock and is at least as old as the Capitol. The clock was restored in 1990 and is in working condition.
 The second floor, in addition to housing the gubernatorial offices, features the Gallery of Governors with portraits of former Michigan Governors on the walls of the rotunda; the gallery extends up to the third floor. The governor's offices were among the most extensively restored during the 1989–1992 restoration. The office features a suite of original furnishings manufactured in 1876 by the Feige Brothers Company of Saginaw. The former chambers of the Michigan Supreme Court are in the south wing of the building. The court vacated its chambers in 1970 for larger quarters, eventually moving to its current location in the Michigan Hall of Justice.[4] The room is now used by the Senate Appropriations Committee and named for longtime chairman Harry Gast.[11]
 Public access to Michigan's legislative bodies is through the third floor. The capitol building holds the chambers and offices of the bicameral state legislature, which is composed of the Michigan House of Representatives and Michigan Senate. Public galleries are at both ends of the third floor. The Senate, with 38 members, has its chambers on the south side of the building, while the House of Representatives, with 110 members, has its chambers in the north wing. House sessions are normally held on Tuesdays and Wednesdays at 1:00 PM and Thursdays at 10:30 AM, while Senate sessions begin at 10:00 AM on Tuesdays, Wednesdays, and Thursdays. Both houses occasionally convene on Mondays and Fridays.[12] Senate and House sessions are taped by Michigan Government Television, a public service body transmitted to local cable television systems' government-access television channels. Similar to C-SPAN, MGTV has made live coverage of the legislative proceedings available since July 15, 1996.[13]
 Although having the same floor plan, the House and Senate chambers are decorated very differently, with the former in terra cotta and teal colors and the latter in blue and gold. An oval cartouche in the carpet at the entrance to the House chamber features the state flower, the apple blossom. Presiding over the house is the speaker, whose chair is behind a desk in the center with a plaster and paint version of the state coat of arms. The Senate chamber is somewhat smaller than the House. The president of the Senate is the lieutenant governor, who presides over sessions from a walnut rostrum at the front of the chamber. Both the House and the Senate use computerized voting systems, including wall-mounted screens that allow visitors to follow the voting and both also contain glass tiled ceilings that allow natural light to shine through etched glass panels to better light the room. These ceiling tiles feature the coats-of-arms of each state in the United States.[4]
 In summer 2013, the structure underwent additional work to replace carpeting installed in the 1989–1992 renovation that had become worn and frayed in many areas. Work also upgraded wiring for communications systems, repaired roof leaks and upgraded a handicapped parking area and entrance on the north side.[14]
 The Capitol Pediment, located above the main front entrance to the building, is entitled "The Rise and Progress of Michigan." It depicts a central figure, Michigan, who is dressed as a Native American. She offers a book and globe to the people of her state, promising a bright future. She is surrounded by symbols of Michigan's economy, including a plow, cornucopia, and a laurel wreath to represent agriculture. Also included are symbols representing shipping, mining, and lumbering.
 The cornerstone, located at the northeast corner of the building, bears the dates 1872, the start of construction, and 1878, the year of completion. The stone was opened during ceremonies November 15, 1978, the centennial of the building's completion. Various documents enclosed within the stone had been damaged by moisture, although several coins from the era remained intact. Officials closed and resealed with stone with 38 new items depicting Michigan's history, people, and lifestyles. At the lawn's southeast corner is the Grand Army of the Republic Memorial, a 1924-bronze plaque attached to a boulder as a memorial to American Civil War veterans who fought for the Union.[4]
 The grounds have several notable trees. An Eastern White Pine, the state tree of Michigan, is located at the east front of the building. The Dr. Martin Luther King, Jr. Tree was planted in memory of the slain leader in 1984 to the north of the Austin Blair Monument. The oldest tree on the grounds is a catalpa on the southeast lawn present when the Capitol was dedicated in 1873. The American Forestry Association has certified this catalpa is the largest living tree of its kind in the United States.[7][15] The most recently dedicated tree is a blue spruce called "the Freedom Tree," planted in 1973 as a memorial to the Vietnam War's missing-in-action and prisoners of war.[4]
 
 1 History

1.1 First state capitol
1.2 Second state capitol
1.3 Third state capitol

 1.1 First state capitol 1.2 Second state capitol 1.3 Third state capitol 2 Today

2.1 Building
2.2 Grounds

 2.1 Building 2.2 Grounds 3 See also 4 Notes 5 References 6 External links List of state and territorial capitols in the United States ^ "National Register Information System". National Register of Historic Places. National Park Service. April 15, 2008..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} 
 ^ "Michigan State Capitol". National Historic Landmark summary listing. National Park Service. Archived from the original on 2012-02-06. Retrieved 2008-06-27.
 ^ a b c d e f g h i "The Michigan State Capitol". Michigan Senate (original material from Michigan Department of Management and Budget). 1988. Archived from the original on 2007-09-29. Retrieved 2007-10-13.
 ^ a b c d e f g h i "Your State Capitol, A Walking Tour" (PDF). Michigan Historical Center. 1999. Retrieved 2007-10-13.
 ^ "Constitution of the State of Michigan of 1835". Archives of Michigan, Michigan Library and Historical Center. 2005. Archived from the original on October 12, 2007. Retrieved 2007-10-13.
 ^ "Elijah E. Myers". Michigan Senate (original material from Michigan Department of Management and Budget). 1988. Archived from the original on 2007-08-24. Retrieved 2007-10-13.
 ^ a b c Chartkoff, Kerry (February 28, 1992). "National Historic Landmark Nomination—Michigan State Capitol" (PDF). National Park Service. Cite journal requires |journal= (help) and Accompanying 13 photos, exterior and interior, from c.1885, 1990, 1992, and undated. (1.66 MB)
 ^ "Homes of the Court: The Capitol at Lansing". Michigan Supreme Court Historical Society. Retrieved 2014-10-22.
 ^ "Michigan State Capitol". National Historic Landmark summary listing. National Park Service. 2007-09-18. Archived from the original on 2012-02-06. Retrieved 2007-10-13.
 ^ "First Floor". Your State Capitol. Michigan Legislative Council. Archived from the original on May 27, 2010. Retrieved 28 November 2012.
 ^ SCR 16 (2015): A concurrent resolution of tribute offered as a memorial for Harry T. Gast, former member of the House of Representatives and the Senate.
 ^ "Citizen's Guide to State Government" (PDF). Michigan Capitol Committee. 2005. Retrieved 2007-10-13.
 ^ "The MGTV Story". Michigan Government Television. 1999. Archived from the original on September 29, 2007. Retrieved 2007-10-13.
 ^ Daum, Kristen (21 August 2013). "Michigan Capitol renovations to wrap up this week". Detroit Free Press. Retrieved 2014-10-22.
 ^ Gangloff, Deborah (January–February 1992). "Introduction: the 1992 edition, national register of big trees". American Forests. 98 (1–2). Retrieved 2007-10-13.
 Cupolas of Capitalism - State Capitol Building Histories/States from MI to MO (2005). Cupolas.com. Accessed December 8, 2005. Seale, William (1995). Michigan's Capitol. University of Michigan Press. ISBN 0-472-06573-4. Seale, William (1995). Michigan's Capitol: Construction and Restoration. University of Michigan Press. ISBN 0-472-09573-0. Michigan State Capitol homepage Michigan State Capitol Commission Virtual tour of the State of Michigan Capitol Rotunda Virtual tour of the State of Michigan Capitol Michigan's Civil War Flags (displayed in rotunda) v t e Congressional delegation Fauna Geography Government Highways History Index Islands Law Lighthouses Municipalities Museums National Historic Landmarks National Register of Historic Places listings People State Historic Sites State parks Tallest buildings Timeline Tourist attractions Culture Crime Demographics Economy Education Politics Upper Peninsula
Copper Country
Gogebic Range
Keweenaw Peninsula Copper Country Gogebic Range Keweenaw Peninsula Lower Peninsula
Central Michigan
Metro Detroit
Michiana
Northern Michigan
Southeast Michigan
The Thumb
West Michigan Central Michigan Metro Detroit Michiana Northern Michigan Southeast Michigan The Thumb West Michigan Ann Arbor Battle Creek Bay City Bloomfield Township Canton Chesterfield Township Clinton Township Commerce Township Dearborn Dearborn Heights Detroit East Lansing Farmington Hills Flint Georgetown Township Grand Blanc Township Grand Rapids Holland Charter Township Independence Township Kalamazoo Kentwood Lansing Lincoln Park Livonia Macomb Township Meridian Charter Township Midland Muskegon Novi Orion Township Pontiac Portage Redford Rochester Hills Roseville Royal Oak Saginaw Saginaw Township St. Clair Shores Shelby Charter Township Southfield Sterling Heights Taylor Troy Warren Waterford Township West Bloomfield Westland Wyoming Ypsilanti Charter Township Alcona Alger Allegan Alpena Antrim Arenac Baraga Barry Bay Benzie Berrien Branch Calhoun Cass Charlevoix Cheboygan Chippewa Clare Clinton Crawford Delta Dickinson Eaton Emmet Genesee Gladwin Gogebic Grand Traverse Gratiot Hillsdale Houghton Huron Ingham Ionia Iosco Iron Isabella Jackson Kalamazoo Kalkaska Kent Keweenaw Lake Lapeer Leelanau Lenawee Livingston Luce Mackinac Macomb Manistee Marquette Mason Mecosta Menominee Midland Missaukee Monroe Montcalm Montmorency Muskegon Newaygo Oakland Oceana Ogemaw Ontonagon Osceola Oscoda Otsego Ottawa Presque Isle Roscommon Saginaw Sanilac Schoolcraft Shiawassee St. Clair St. Joseph Tuscola Van Buren Washtenaw Wayne Wexford v t e Alabama Alaska Arizona Arkansas California Colorado Connecticut Delaware Florida Georgia Hawaii Idaho Illinois Indiana Iowa Kansas Kentucky Louisiana Maine Maryland Massachusetts Michigan Minnesota Mississippi Missouri Montana Nebraska Nevada New Hampshire New Jersey New Mexico New York North Carolina North Dakota Ohio Oklahoma Oregon Pennsylvania Rhode Island South Carolina South Dakota Tennessee Texas Utah Vermont Virginia Washington West Virginia Wisconsin Wyoming American Samoa District of Columbia Guam Northern Mariana Islands Puerto Rico United States Virgin Islands v t e Alcona Alger Allegan Alpena Antrim Arenac Baraga Barry Bay Benzie Berrien Branch Calhoun Cass Charlevoix Cheboygan Chippewa Clare Clinton Crawford Delta Dickinson Eaton Emmet Genesee Gladwin Gogebic Grand Traverse Gratiot Hillsdale Houghton Huron Ingham Ionia Iosco Iron Isabella Jackson Kalamazoo Kalkaska Kent Keweenaw Lake Lapeer Leelanau Lenawee Livingston Luce Mackinac Macomb Manistee Marquette Mason Mecosta Menominee Midland Missaukee Monroe Montcalm Montmorency Muskegon Newaygo Oakland Oceana Ogemaw Ontonagon Osceola Oscoda Otsego Ottawa Presque Isle Roscommon Saginaw Sanilac Schoolcraft Shiawassee St. Clair St. Joseph Tuscola Van Buren Washtenaw Wayne (Detroit) Wexford Bridges Historic districts Houses Michigan State Historic Sites (listings) National Historic Landmarks Properties of religious function Railway stations v t e Lansing East Lansing Bath Township Delhi Township Delta Township DeWitt Township Dimondale Edgemont Park Grand Ledge Haslett Holt Lansing Township Mason Meridian Township Okemos Waverly Williamston Clinton County Eaton County Ingham County Colonial Village Downtown Lansing Genesee Old Town REO Town Tamarisk Davenport University Great Lakes Christian College Lansing Community College Michigan State University Michigan State University College of Law Western Michigan University Cooley Law School East Lansing Public Schools
East Lansing High School East Lansing High School Lansing School District
Lansing Everett High School
J. W. Sexton High School
Lansing Eastern High School Lansing Everett High School J. W. Sexton High School Lansing Eastern High School Waverly Community Schools
Waverly Senior High School Waverly Senior High School Lansing Catholic High School Capital Area District Library Eli and Edythe Broad Art Museum Impression 5 Science Center Library of Michigan and Historical Center MSU Library Michigan Women's Hall of Fame R. E. Olds Transportation Museum Turner-Dodge House McLaren–Greater Lansing Hospital Sparrow Hospital Common Ground Music Festival Cooley Law School Stadium East Lansing Film Festival Grand River Lake Lansing Lake Lansing Park North Lansing Center Lansing JazzFest Lansing Lugnuts Lansing River Trail Lansing Symphony Orchestra Michigan Hall of Justice Michigan State Capitol MSU Pavilion MSU Spartans Michigan Supreme Court Michigan Walk of Fame Old Town BluesFest Potter Park Zoo Red Cedar River St. Mary Cathedral Summit at the Capital Centre Wharton Center Capital Area Transportation Authority Capital Region Airport Authority Capital Region International Airport Michigan Flyer Multimodal Gateway Port Lansing Union Depot City Pulse Lansing State Journal MLive Lansing Radio REVUE Mid Michigan The State News Television Accident Fund Insurance Company Auto-Owners Insurance Biggby Coffee Dart Container Déjà Vu Consulting Delta Dental of Michigan Jackson National Life Lansing Board of Water & Light Lake Trust Credit Union Michigan Farm Bureau Michigan High School Athletic Association MSU Federal Credit Union Roman Catholic Diocese of Lansing Quality Dairy Company TechSmith Two Men and a Truck Eastwood Towne Center Frandor Shopping Center Lansing City Market Lansing Mall Meridian Mall  Michigan Central Michigan  United States LCCN: sh94006940 Government of Michigan State capitols in the United States Buildings and structures in Lansing, Michigan National Register of Historic Places in Lansing, Michigan Government buildings on the National Register of Historic Places in Michigan National Historic Landmarks in Michigan Michigan State Historic Sites in Ingham County Government buildings completed in 1879 Tourist attractions in Lansing, Michigan Neoclassical architecture in Michigan Italianate architecture in Michigan 1879 establishments in Michigan Government buildings with domes CS1 errors: missing periodical Articles with short description Infobox mapframe without OSM relation ID on Wikidata Coordinates on Wikidata Commons category link is on Wikidata Featured articles Wikipedia articles with LCCN identifiers Pages using the Kartographer extension Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version العربية Asturianu Deutsch Español فارسی Français Italiano Nederlands 日本語 پنجابی Русский Suomi 中文  This page was last edited on 22 September 2019, at 19:42 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Cornerstone laid: Michigan State Capitol ^ ^ a b c d e f g h i a b c d e f g h i ^ ^ a b c ^ ^ ^ ^ ^ ^ ^ ^ 98 Michigan State Capitol Lansing Lansing East Lansing The seat of government for this state shall be at Detroit, or at such other place or places as may be prescribed by law until the year eighteen hundred and forty-seven when it shall be permanently located by the legislature.[3] The seat of government for this state shall be at Detroit, or at such other place or places as may be prescribed by law until the year eighteen hundred and forty-seven when it shall be permanently located by the legislature.[3] A statue of Austin Blair stands in the foreground  Neoclassical/Italianate/Renaissance revival 100 N Capitol Ave, Lansing, Michigan, U.S. 42°44′01″N 84°33′20″W﻿ / ﻿42.733601°N 84.555470°W﻿ / 42.733601; -84.555470Coordinates: 42°44′01″N 84°33′20″W﻿ / ﻿42.733601°N 84.555470°W﻿ / 42.733601; -84.555470 1872; 147 years ago (1872) Cornerstone laid:October 2, 1873; 146 years ago (October 2, 1873) September 26, 1878; 141 years ago (September 26, 1878) January 1, 1879; 140 years ago (January 1, 1879) 1989–1992; 2010–2014 $1,427,738.78 267 feet (81 meters) 128 m × 83.5 m (420 ft × 274 ft) 2 Elijah E. Myers Capitol BuildingYour State Capitol  Michigan State Capitol U.S. National Register of Historic Places U.S. National Historic Landmark Michigan State Historic Site 
 1871; 148 years ago (1871) Myers, Elijah E. Renaissance Downtown Lansing MRA (AD) 71000396[1] January 25, 1971; 48 years ago (January 25, 1971) October 5, 1992; 27 years ago (October 5, 1992)[2] February 18, 1956  Wikimedia Commons has media related to Michigan State Capitol. Lansing (capital) 
Congressional delegation
Fauna
Geography
Government
Highways
History
Index
Islands
Law
Lighthouses
Municipalities
Museums
National Historic Landmarks
National Register of Historic Places listings
People
State Historic Sites
State parks
Tallest buildings
Timeline
Tourist attractions
  
Culture
Crime
Demographics
Economy
Education
Politics
 
Upper Peninsula
Copper Country
Gogebic Range
Keweenaw Peninsula
Lower Peninsula
Central Michigan
Metro Detroit
Michiana
Northern Michigan
Southeast Michigan
The Thumb
West Michigan
 
Ann Arbor
Battle Creek
Bay City
Bloomfield Township
Canton
Chesterfield Township
Clinton Township
Commerce Township
Dearborn
Dearborn Heights
Detroit
East Lansing
Farmington Hills
Flint
Georgetown Township
Grand Blanc Township
Grand Rapids
Holland Charter Township
Independence Township
Kalamazoo
Kentwood
Lansing
Lincoln Park
Livonia
Macomb Township
Meridian Charter Township
Midland
Muskegon
Novi
Orion Township
Pontiac
Portage
Redford
Rochester Hills
Roseville
Royal Oak
Saginaw
Saginaw Township
St. Clair Shores
Shelby Charter Township
Southfield
Sterling Heights
Taylor
Troy
Warren
Waterford Township
West Bloomfield
Westland
Wyoming
Ypsilanti Charter Township
 
Alcona
Alger
Allegan
Alpena
Antrim
Arenac
Baraga
Barry
Bay
Benzie
Berrien
Branch
Calhoun
Cass
Charlevoix
Cheboygan
Chippewa
Clare
Clinton
Crawford
Delta
Dickinson
Eaton
Emmet
Genesee
Gladwin
Gogebic
Grand Traverse
Gratiot
Hillsdale
Houghton
Huron
Ingham
Ionia
Iosco
Iron
Isabella
Jackson
Kalamazoo
Kalkaska
Kent
Keweenaw
Lake
Lapeer
Leelanau
Lenawee
Livingston
Luce
Mackinac
Macomb
Manistee
Marquette
Mason
Mecosta
Menominee
Midland
Missaukee
Monroe
Montcalm
Montmorency
Muskegon
Newaygo
Oakland
Oceana
Ogemaw
Ontonagon
Osceola
Oscoda
Otsego
Ottawa
Presque Isle
Roscommon
Saginaw
Sanilac
Schoolcraft
Shiawassee
St. Clair
St. Joseph
Tuscola
Van Buren
Washtenaw
Wayne
Wexford
 
Alabama
Alaska
Arizona
Arkansas
California
Colorado
Connecticut
Delaware
Florida
Georgia
Hawaii
Idaho
Illinois
Indiana
Iowa
Kansas
Kentucky
Louisiana
Maine
Maryland
Massachusetts
Michigan
Minnesota
Mississippi
Missouri
Montana
Nebraska
Nevada
New Hampshire
New Jersey
New Mexico
New York
North Carolina
North Dakota
Ohio
Oklahoma
Oregon
Pennsylvania
Rhode Island
South Carolina
South Dakota
Tennessee
Texas
Utah
Vermont
Virginia
Washington
West Virginia
Wisconsin
Wyoming
 
American Samoa
District of Columbia
Guam
Northern Mariana Islands
Puerto Rico
United States Virgin Islands
 
Alcona
Alger
Allegan
Alpena
Antrim
Arenac
Baraga
Barry
Bay
Benzie
Berrien
Branch
Calhoun
Cass
Charlevoix
Cheboygan
Chippewa
Clare
Clinton
Crawford
Delta
Dickinson
Eaton
Emmet
Genesee
Gladwin
Gogebic
Grand Traverse
Gratiot
Hillsdale
Houghton
Huron
Ingham
Ionia
Iosco
Iron
Isabella
Jackson
Kalamazoo
Kalkaska
Kent
Keweenaw
Lake
Lapeer
Leelanau
Lenawee
Livingston
Luce
Mackinac
Macomb
Manistee
Marquette
Mason
Mecosta
Menominee
Midland
Missaukee
Monroe
Montcalm
Montmorency
Muskegon
Newaygo
Oakland
Oceana
Ogemaw
Ontonagon
Osceola
Oscoda
Otsego
Ottawa
Presque Isle
Roscommon
Saginaw
Sanilac
Schoolcraft
Shiawassee
St. Clair
St. Joseph
Tuscola
Van Buren
Washtenaw
Wayne (Detroit)
Wexford
  
Bridges
Historic districts
Houses
Michigan State Historic Sites (listings)
National Historic Landmarks
Properties of religious function
Railway stations
 
Lansing
East Lansing
Bath Township
Delhi Township
Delta Township
DeWitt Township
Dimondale
Edgemont Park
Grand Ledge
Haslett
Holt
Lansing Township
Mason
Meridian Township
Okemos
Waverly
Williamston
 
Clinton County
Eaton County
Ingham County
 
Colonial Village
Downtown Lansing
Genesee
Old Town
REO Town
Tamarisk
 Colleges and universities
Davenport University
Great Lakes Christian College
Lansing Community College
Michigan State University
Michigan State University College of Law
Western Michigan University Cooley Law School
School districts
East Lansing Public Schools
East Lansing High School
Lansing School District
Lansing Everett High School
J. W. Sexton High School
Lansing Eastern High School
Waverly Community Schools
Waverly Senior High School
Private schools
Lansing Catholic High School
Museums and libraries
Capital Area District Library
Eli and Edythe Broad Art Museum
Impression 5 Science Center
Library of Michigan and Historical Center
MSU Library
Michigan Women's Hall of Fame
R. E. Olds Transportation Museum
Turner-Dodge House
 
Davenport University
Great Lakes Christian College
Lansing Community College
Michigan State University
Michigan State University College of Law
Western Michigan University Cooley Law School
 
East Lansing Public Schools
East Lansing High School
Lansing School District
Lansing Everett High School
J. W. Sexton High School
Lansing Eastern High School
Waverly Community Schools
Waverly Senior High School
 
Lansing Catholic High School
 
Capital Area District Library
Eli and Edythe Broad Art Museum
Impression 5 Science Center
Library of Michigan and Historical Center
MSU Library
Michigan Women's Hall of Fame
R. E. Olds Transportation Museum
Turner-Dodge House
 
McLaren–Greater Lansing Hospital
Sparrow Hospital
 
Common Ground Music Festival
Cooley Law School Stadium
East Lansing Film Festival
Grand River
Lake Lansing
Lake Lansing Park North
Lansing Center
Lansing JazzFest
Lansing Lugnuts
Lansing River Trail
Lansing Symphony Orchestra
Michigan Hall of Justice
Michigan State Capitol
MSU Pavilion
MSU Spartans
Michigan Supreme Court
Michigan Walk of Fame
Old Town BluesFest
Potter Park Zoo
Red Cedar River
St. Mary Cathedral
Summit at the Capital Centre
Wharton Center
 
Capital Area Transportation Authority
Capital Region Airport Authority
Capital Region International Airport
Michigan Flyer
Multimodal Gateway
Port Lansing
Union Depot
 
City Pulse
Lansing State Journal
MLive Lansing
Radio
REVUE Mid Michigan
The State News
Television
 
Accident Fund Insurance Company
Auto-Owners Insurance
Biggby Coffee
Dart Container
Déjà Vu Consulting
Delta Dental of Michigan
Jackson National Life
Lansing Board of Water & Light
Lake Trust Credit Union
Michigan Farm Bureau
Michigan High School Athletic Association
MSU Federal Credit Union
Roman Catholic Diocese of Lansing
Quality Dairy Company
TechSmith
Two Men and a Truck
 
Eastwood Towne Center
Frandor Shopping Center
Lansing City Market
Lansing Mall
Meridian Mall
 
 Michigan
Central Michigan
 United States
 
LCCN: sh94006940
 