
 Susianna "Susi" Levonovna Kentikian (Armenian: Սյուզի Կենտիկյան; born Syuzanna Kentikyan on 11 September 1987) is an Armenian-German professional boxer.[3] She was born in Yerevan, Armenian SSR, but left the country with her family at the age of five because of the Nagorno-Karabakh War. Kentikian has lived in Hamburg since 1996 and began boxing at the age of twelve. Following a successful amateur career, she turned professional in 2005 upon signing with the Hamburg-based Spotlight Boxing promotion.
 Kentikian is a two-time flyweight world champion, having held the World Boxing Association (WBA) female title from 2007 to 2012, and from 2013 to 2017. Additionally, she was the World Boxing Organization (WBO) flyweight champion from 2009 to 2012, and held the Women's International Boxing Federation (WIBF) title from 2007 to 2012 and 2015 to 2017.
During the 2009 WBA convention in Colombia she was named the first ever WBA female Super Champion. It was announced that this belt would be called "Susi Kentikian belt" for all other future Super Champions.[4]
 Until 2012, Kentikian remained undefeated as a professional, winning 16 of her first 30 fights by knockout or stoppage. The German television station ZDF has broadcast her fights since July 2009. She had previously headlined fight cards for the television station ProSieben from 2007 to 2009. Kentikian has gained minor celebrity status in Germany and she hopes to reach a popularity similar to that of the retired German female boxing star Regina Halmich.
 Susianna Kentikian was born in Yerevan, Armenian SSR, the daughter of veterinary doctor Levon Kentikian and his wife Makruhi.[5] At the age of five, she left Armenia with her parents and her nine-year-old brother, Mikael, because her father was called up to serve in the military during the Nagorno-Karabakh War.[6] In 1992, the family first moved to Berlin, Germany and stayed at asylum seekers' homes. However, due to the violence at these facilities and their poor knowledge of the German language, they left Berlin and moved to Moldova and later to Russia, where Kentikian went to school for a short period of time.[7] The family returned to Germany in 1996 and relocated in Hamburg, again living in government facilities for asylum seekers.[2] Kentikian's residence status remained uncertain for almost a decade. Several times, she and her family were taken to the airport for deportation, but the intervention of local friends such as her amateur trainer, Frank Rieth, who called lawyers, the media and local politicians, prevented their final expulsion.[8] Her family received a permanent residence permit in 2005 when she signed a three-year professional boxing contract that established a stable income.[9]
 At the age of 16, Kentikian began working as a cleaner in a local fitness center to help her family financially.[7] She graduated from high school (Realschule) in the summer of 2006[10] and she eventually became a German citizen in June 2008.[11] She applied for dispensation to be allowed to retain her Armenian citizenship.[1] Kentikian lived with her family in an apartment near her Hamburg boxing gym.[2]
 Kentikian discovered her enthusiasm for boxing when she was twelve years old after accompanying her brother to his boxing training.[2] She started with regular training herself and stated that boxing had allowed her to forget the difficulties of her life for a short time: "I could let everything out, my whole energy. If you have so many problems like our family, you need something like that."[6]
 Kentikian won the Hamburg Championships for juniors from 2001 to 2004.[12] She also won the Northern German Championships for juniors in 2003 and 2004, and in October 2004, she had her biggest amateur success by winning the International German Women's Amateur Championships in the featherweight division for juniors.[12] Kentikian found it increasingly difficult to find opponents in the amateur ranks, as few boxers wanted to face her, and her status as an asylum seeker did not allow her to box outside Hamburg.[13] Kentikian's final amateur record stood at 24 wins and one loss. She later blamed overeagerness for her single loss, having fought despite health problems at the time.[14] Her aggressive style and fast combinations, and her ambition to always attack until she knocked out the opponent earned her the nickname "Killer Queen"; she has often used the identically-named song by the English rock band Queen as her entrance music.[2]
 Kentikian was discovered as a professional boxer at an exhibition fight during qualifications for the World Amateur Boxing Championships. At the beginning of 2005, she signed a three-year contract with the Hamburg boxing promoter Spotlight Boxing, a joint venture of Universum Box-Promotion, focusing on young athletes.[9] In future years, she was coached by Universum trainer Magomed Schaburow.[5] Kentikian started her professional career on 15 January 2005, with a win by unanimous decision over Iliana Boneva of Bulgaria on the undercard of German female boxing star Regina Halmich. Over the next 14 months, Kentikian won nine of her eleven fights by knockout. Her unusually high knockout percentage, rarely seen in lower female weight classes, began to draw attention.[8] On 25 July 2006, she won her first belt, the International German flyweight title, against Daniela Graf by unanimous decision. In her first international title fight on 9 September 2006, Kentikian beat Maribel Zurita from the United States with a fourth round technical knockout for the WIBF InterContinental Flyweight title; the fight was stopped when Zurita was cut over the left eyebrow.[8]
 In her 15th professional bout, Kentikian fought for her first world championship in Cologne, Germany, on 16 February 2007; it was also her first time headlining a fight card.[8] She won by a ninth-round technical knockout against Carolina Alvarez of Venezuela, thereby winning the vacant WBA Flyweight title. Alvarez took unanswered punches in most of the rounds and was bleeding heavily from her nose, and the referee eventually stopped the fight in round nine in concern for Alvarez's health.[15] Six weeks later, on 30 March 2007, Kentikian made her first title defense. Before a crowd of 19,500 in the Kölnarena, she fought on the undercard of an exhibition bout between German comedian Stefan Raab and WIBF World Champion Halmich. Kentikian beat María José Núñez from Uruguay with a third-round technical knockout. Núñez was knocked down in round two and Kentikian finished the fight one round later with a right cross followed up by combinations that left Núñez defenseless on the ropes, causing the referee to step in.[16]
 Kentikian next faced Nadia Hokmi of France on 25 May 2007, in her second title defense. Hokmi, using her height and reach advantage, proved to be the first test of Kentikian's professional career and both boxers fought a competitive bout. While Hokmi started out slower, she managed to win several of the later rounds by landing repeated combinations. Kentikian won through a split decision for the first time in her career.[17] The fight was voted among the five "Top Fights of the Year" by WomenBoxing.com.[18] On 7 September 2007, Kentikian defended her title against Shanee Martin from the United Kingdom, winning by a third-round technical knockout. Kentikian controlled her opponent from the opening bell and the referee stopped the fight after Martin was knocked down from a straight right hand in round three.[19]
 Following the retirement of long-standing WIBF belt holder Halmich, Kentikian unified the WBA and vacant WIBF Flyweight titles in her hometown of Hamburg on 7 December 2007. She met Hokmi in a rematch of their contest six months earlier. The French boxer again proved to be a tough opponent and the fight developed similarly to their first encounter. Once more, Kentikian had the better start, but Hokmi scored during the second half of the fight, again making the bout close. This time however, Kentikian was ahead on all three of the judges' scorecards, winning by unanimous decision.[20] After the end of the year, Kentikian was chosen as Hamburg's sportswoman of the year.[21]
 Kentikian successfully defended her titles against Sarah Goodson of the Philippines by a third-round technical knockout on 29 February 2008. Goodson, who had fought almost exclusively in lower weight classes before, was overpowered by Kentikian and the referee ended the fight after a series of body punches in round three.[22] In her next title defense on 10 May 2008, Kentikian beat Mary Ortega from the United States with a first-round technical knockout. Ortega, who had previously fought against well-known opponents such as Elena Reid and Hollie Dunaway, was knocked down twice by straight right hands during the first 90 seconds of the fight. When Kentikian had Ortega pinned against the ropes again, the referee stepped in shortly before the end of round one. The quick stoppage came as a surprise to many, including television commentator Halmich, who had expected a hard-fought bout.[23]
 In her next title defense on 29 August 2008, Kentikian met Hager Finer of Israel, Halmich's last opponent before retiring. Following a close opening round, the boxing match turned into a brawl and Finer scored during the first half of the fight. From round five onwards, Kentikian managed to take over the bout by landing the cleaner punches and she won by unanimous decision.[24] On 5 December 2008, Kentikian faced Anastasia Toktaulova of Russia, the reigning GBU Flyweight Champion, although the GBU title was not on the line. During the uncharacteristic tactical fight, Kentikian managed to control her opponent from the middle of the ring in most of the rounds. The three judges all scored the bout in favor of Kentikian.[25] In December 2008, she was named Germany's female boxer of the year for the first time.[26] In addition, she won the WBA Best Female Boxer award for 2007–08.[27]
 Kentikian retained her WIBF and WBA belts with a unanimous decision win over Elena Reid from the United States on 20 March 2009. Reid, who was well known in Germany after two controversial bouts with Halmich in 2004 and 2005, remained largely passive from the opening bell and Kentikian controlled her through the majority of the fight. Reid did not win a single round on the official scorecards.[28] On 4 July 2009, Kentikian fought the Interim WBA Super Flyweight Champion Carolina Gutierrez Gaite of Argentina. Kentikian used her speed and combinations to dominate her opponent through the ten rounds, winning every round on the judges' scores.[29] Kentikian ended 2009 by fighting the undefeated Turkish-German Julia Sahin (20–0) on 10 October for the vacant WBO Female Flyweight title. Kentikian overwhelmed Sahin with her higher work rate early on. Sahin spent most of the fight covering up from Kentikian's many flurries. Kentikian won a unanimous decision to become the new WBO Female Flyweight Champion.[30]
 Kentikian, now the WIBF, WBA and WBO Female Flyweight Champion, defended all of her titles against undefeated Women's International Boxing Association titleholder Nadia Raoui on 24 April 2010 in Hamburg.[31] The bout was very close, as Kentikian was outlanded in most rounds by Raoui. After ten rounds, Kentikian held narrow advantages on two of the three judges' scorecards and won via split decision.[32] On 17 July, Kentikian defended her championships once again against Arely Muciño. In the first few rounds, she used combinations and quick offensive attacks to score against Mucino, before suffering a cut due to an accidental clash of heads. Kentikian was unable to continue; the fight was declared a no contest and she retained her titles.[33]
 Kentikian again defended her WIBF Flyweight title on 26 March 2011 against Ana Arrazola. She won at least seven rounds on each of the judges' scorecards on her way to a unanimous decision victory. Arrazola received a one-point deduction in the sixth round, as Kentikian remained the WIBF champion.[34] Later in the year, Kentikian scored another unanimous decision win on 21 October, this time against Teeraporn Pannimit of Thailand, to retain her WIBF, WBA and WBO Female Flyweight titles. She maintained control of the fight throughout en route to a lopsided victory.[35]
 Undefeated with 29 wins (including 16 by knockout), Kentikian next attempted to defend her WIBF and WBO Female Flyweight titles on 16 May 2012 in Frankfurt, Germany against Melissa McMorrow.[36][37] Kentikian lost via majority decision with scores of 95–95, 94–96 and 94–96, the first time she was defeated as a professional.[38] The WBA Female title was not defended in that fight, so Kentikian remained that organization's champion. On 1 December, American Carina Moreno challenged her for the WBA title in Düsseldorf.[37] Kentikian sought to utilize a brawling style against Moreno, but was frequently hit by Moreno's punches while moving forward. The fight was decided by majority decision after 10 rounds: one judge gave Kentikian a four-point advantage, but the other two had Moreno ahead by identical 96–94 margins as she claimed the WBA Female Flyweight championship. Kentikian suffered her second consecutive loss.[39]
 On 1 February 2013, Kentikian fought Sanae Jah for the Interim WBA Female Flyweight championship, which was vacant at the time. She established command of the fight by regularly moving close to Jah and applying pressure. In the seventh round, Kentikian scored a knockdown with a right-hand punch. Despite suffering multiple cuts by her right eye, including one from a clash of heads during the 10th and final round, Kentikian was able to finish the fight and earn a unanimous decision victory.[40] Kentikian followed that bout with a rematch against Moreno on 6 July.[41] With a win by unanimous decision, she regained the WBA Female Flyweight title.[42] Kentikian made a title defense against Simona Galassi on 7 December in Stuttgart, under a scoring system in which judges could use half-points in their tallies. Each judge scored the fight in Kentikian's favor by at least 2.5 points, as she successfully defended her WBA championship.[43]
 Kentikian's opponent in her next title defense was Dan-Bi Kim. In the bout, which was held on 31 May 2014, Kentikian scored a technical knockout in the ninth round.[44] On 8 November 2014, Kentikian fought Naoko Fujioka and earned the judges' decision after 10 rounds.[45] After founding her own boxing promotion company, Kentikian Promotions, she next fought in October 2015, following an 11-month absence. Against Mexican boxer Susana Cruz Perez, Kentikian was cut in the second round by a clash of heads, but won most rounds on the judges' scorecards. The fight went the scheduled 10 rounds, and Kentikian earned a unanimous decision, although Halmich said that she had not shown "her old form".[46] The victory earned Kentikian the WIBF Flyweight championship. On 30 July 2016, Kentikian fought in a WIBF title defense against Nevenka Mikulic and won by unanimous decision. As of 2018, that was her most recent fight.[47] By 2017, Kentikian's WBA Female Flyweight championship had been vacated.[48]
 At the beginning of her professional career, Kentikian was primarily featured in the local media in Hamburg and occasionally in national German newspapers; in particular, her difficult childhood and her long-time uncertain asylum status sparked interest in the press and led to comparisons with the boxing film Million Dollar Baby.[2] Her height of 1.54 m (5 ft 1 in) also drew attention, and she was dubbed "Germany's smallest professional boxer".[49] Early on, Kentikian was considered one of the big talents in German boxing[6] and the media mentioned her as the potential successor of record world champion Halmich, a goal she had also set out for herself.[7]
 In 2007, Kentikian was introduced to a much larger audience due to cooperation between German television station ProSieben and her promoter Spotlight Boxing.[10] In addition to live broadcasts of her fights during so-called "ProSieben Fight Nights,"[50] she appeared several times on the popular television show TV total. She took part in a four-round sparring session with the show's host, Stefan Raab,[51] and participated in the competitive entertainment event World Wok Championships, where she teamed with Sven Hannawald, Christina Surer and Markus Beyer to win the four-person competition.[52] Her first world title defense, fighting María José Núñez on the undercard of a Raab vs. Halmich exhibition bout, was seen by 4.69 million television viewers—her most watched fight to date.[53] A camera crew visited her for one year prior to her first world championship fight against Carolina Alvarez; the documentary aired in June 2007 on the German public broadcaster Das Erste. A shortened version with commentary in English was aired by the German international broadcaster Deutsche Welle in October 2007.[54] By 2010, Kentikian became a spokesperson for the Armenian charitable organization Lebensbaum Armenien (Armenia's Tree of Life).[55]
 On August 1, 2019, she was revealed to be the Monster on The Masked Singer.[56]
 Source:[47]
 
 Armenian German 1 Early life 2 Amateur career 3 Professional career

3.1 2005–2007
3.2 2008–2010
3.3 2011–2016

 3.1 2005–2007 3.2 2008–2010 3.3 2011–2016 4 In the media 5 Professional boxing record 6 References 7 External links ^ a b Endlich passt die Hymne. Hamburger Abendblatt. 26 August 2008. Accessed 26 August 2008. (in German)
 ^ a b c d e f Krohn, Anne-Dore. Durchgeboxt. Die Zeit. 15 February 2007. Accessed 1 May 2007. (in German)
 ^ Professional boxing record for Susi Kentikian from BoxRec. Retrieved 4 October 2016.
 ^ http://www.guardian.co.tt/article-6.2.325443.e80c61bb0a
 ^ a b Jensen, Björn. "Killer Queen" krönt sich mit WM-Titel. Hamburger Abendblatt. 19 February 2007. Accessed 26 May 2008. (in German)
 ^ a b c Bösecke, Ina. "Ich bin ein Killer, ich kämpfe wie ein Mann". Spiegel Online. 16 February 2007. Accessed 1 May 2007. (in German)
 ^ a b c Treptow, Julia. Ein Fliegengewicht boxt sich nach oben. Spiegel Online. 18 January 2006. Accessed 1 May 2007. (in German)
 ^ a b c d Monheim, Gert. Die starke Susi – Boxen zum Überleben. Das Erste. 27 June 2007. Accessed 16 April 2008. (in German)
 ^ a b Hardt, Andreas. Susi Kentikian – Hamburgs "Million Dollar Baby". Die Welt. 31 March 2005. Accessed 1 May 2007.
 ^ a b "Ich wollte schon als Siebenjährige Großes schaffen". Hamburger Abendblatt. 7 April 2007. Accessed 1 May 2007. (in German)
 ^ Hamburg. Hamburger Abendblatt. 16 June 2008. Accessed 26 June 2008. (in German)
 ^ a b Susianna Kentikian | Portrait. Archived 6 March 2010 at the Wayback Machine Kentikian.de. Accessed 17 April 2008. (in German)
 ^ Susi Kentikian. taz Hamburg. 2 January 2003. Accessed 21 June 2008. (in German)
 ^ Kötter, Andreas. ProSieben Fight Night – Interview mit Susi Kentikian. Archived 2 March 2008 at the Wayback Machine ProSieben.de. Accessed 13 April 2008. (in German)
 ^ Drexel, Fritz. Kentikian Stops Alvarez. Archived 27 November 2007 at the Wayback Machine East Side Boxing. 17 February 2007. Accessed 21 June 2008.
 ^ Drexel, Fritz. Kentikian Destroys Anchorena. East Side Boxing. 30 March 2007. Accessed 21 June 2008.
 ^ Drexel, Fritz. Kentikian struggles to split decision victory over Hockmi. East Side Boxing. 25 May 2007. Accessed 13 April 2008.
 ^ Womens Boxing – WBAN Awards 2007. WomenBoxing.com. Accessed 18 March 2008.
 ^ Drexel, Fritz. Kentikian Destroys Martin! Archived 18 September 2007 at the Wayback Machine East Side Boxing. 7 September 2007. Accessed 13 April 2008.
 ^ Susi Kentikian wins Halmich's belt. Archived 8 February 2008 at the Wayback Machine East Side Boxing. 8 December 2007. Accessed 21 June 2008.
 ^ Hamburgs Sportler des Jahres. Die Welt. 5 February 2008. Accessed 13 April 2008. (in German)
 ^ Dower, Jim. Kentikian Destroys Goodson. Boxing News 24. 1 March 2008. Accessed 21 June 2008.
 ^ Schmidt, Erik. Kentikian Destroys Ortega. Boxing News 24. 11 May 2008. Accessed 21 June 2008.
 ^ Swiecznik, Sebastian. Zbik schools Carvalho, Kentikian beats Shmoulefeld Finer! Archived 22 May 2011 at the Wayback Machine East Side Boxing. 29 August 2008. Accessed 30 August 2008.
 ^ Schmidt, Erik. Kentikian Defeats Toktaulova. Boxing News 24. 6 December 2008. Accessed 5 January 2009.
 ^ "König Arthur" sticht Klitschkos aus. FOCUS Online. 18. Dezember 2008. Accessed 5 January 2009. (in German)
 ^ The WBA congratulates 2007/2008 Award Winners. WBAOnline.com. Accessed 26 July 2008. Archived 3 August 2008 at the Wayback Machine
 ^ Persson, Ake. Kentikian Dominates Reid. Archived 6 March 2012 at the Wayback Machine East Side Boxing. 21 March 2009. Accessed 31 May 2009.
 ^ Schmidt, Erik. Chambers Upsets Dimitrenko, Alekseev, Kentikian and Graf Also Victorious. Boxing News 24. 4 July 2009. Accessed 5 July 2009.
 ^ Kentikian defeats Sahin. Boxing News 24. 15 October 2009. Accessed 30 April 2018.
 ^ Video: Kentikian vs. Raoui, Brähmer vs. Plotinsky Weigh-In. East Side Boxing. 23 April 2010. Accessed 1 February 2018.
 ^ Video: Kentikian Wins Controversial Decision Over Raoui; Brähmer Dominates Plotinsky. East Side Boxing. 25 April 2010. Accessed 1 February 2018.
 ^ Schmidt, Erik. Kentikian and Mucino bout ends in technical draw. Boxing News 24. 17 July 2010. Accessed 2 January 2018.
 ^ Dimitrenko Flattens Sosnowski. Seconds Out. 26 March 2011. Accessed 4 February 2018.
 ^ Susi Kentikian defends titles in clash vs. Teeraporn Pannimit. PanArmenian.net News Agency. 22 October 2011. Accessed 5 March 2018.
 ^ Persson, Per Ake. Susi Kentikian Defends Her Titles on May 16 in Frankfurt. Boxing Scene. 16 April 2012. Accessed 8 May 2018.
 ^ a b Local roundup: Watsonville's Carina Moreno set to fight for WBA flyweight title in December. Santa Cruz Sentinel. 7 November 2012. Accessed 11 February 2018.
 ^ Dower, Jim. Pianeta decisions 47-year-old McCall; Kentikian loses to McMorrow. Boxing News 24. 16 May 2012. Accessed 11 February 2018.
 ^ Dower, Jim. Moreno defeats Kentikian. Boxing News 24. 1 December 2012. Accessed 11 February 2018.
 ^ Dower, Jim. Soliman defeats Sturm; Kentikian decisions Jah. Boxing News 24. 1 February 2013. Accessed 14 February 2018.
 ^ Susi Kentikian vs. Carina Moreno on July 6th in Germany. Boxing News 24. 12 June 2013. Accessed 18 February 2018.
 ^ Susi Kentikian vs. Simona Galassi on December 7th. Boxing News 24. 6 November 2013. Accessed 18 February 2018.
 ^ Erazo, Hilmar Rojas. Susi Kentikian retained her flyweight belt. World Boxing Association. 7 December 2013. Accessed 2 March 2018.
 ^ News: Kentikian–Fujioka; Langford; Stulginskas Update. Boxing Scene. 22 October 2014. Accessed 2 March 2018.
 ^ Results from Germany: Stieglitz and Sturm draw; Kentikian beats Fujioka. Boxing News 24. 8 November 2014. Accessed 7 March 2018.
 ^ Ecksel, Robert. "Killer Queen" Retains WBA World Flyweight Title. World Boxing Association. 4 October 2015. Retrieved 10 March 2018.
 ^ a b Susi Kentikian. BoxRec. Accessed 21 March 2018.
 ^ Titles: World Boxing Association World Female: flyweight. BoxRec. Accessed 2 June 2018.
 ^ Jensen, Björn. Die kleinste Profiboxerin Deutschlands. Hamburger Abendblatt. 10 January 2005. Accessed 1 May 2007. (in German)
 ^ ProSieben setzt Kooperation mit Profiboxstall spotlight boxing fort. ProSieben Television GmbH. 7 July 2007. Accessed 16 April 2008. (in German)
 ^ TV total – 29.03.2007. ProSieben.de. Accessed 16 April 2008. (in German) Archived 12 December 2008 at the Wayback Machine
 ^ TV total – WOK WM 2007. ProSieben.de. Accessed 16 April 2008. (in German) Archived 14 March 2008 at the Wayback Machine
 ^ Reifeprüfung für die "Killer Queen". ProSieben Television GmbH. 18 April 2007. Accessed 1 May 2007. (in German)
 ^ Hungerland, Enno. Die starke Susi – Boxen zum Überleben. WDR. 24 May 2007. Accessed 26 May 2007. (in German)
 ^ Hranush Hakobyan met with Susi Kentikian. PanArmenian.net News Agency. 14 August 2010. Accessed 5 February 2018.
 ^ https://www.prosieben.de/tv/the-masked-singer/news/the-masked-singer-das-monster-ist-susi-kentikian-105697
 Official website (in German) Professional boxing record for Susi Kentikian from BoxRec Susianna Kentikian profile at wban.org Lebensbaum für Armenien (in German) GND: 143858548 ISNI: 0000 0001 1812 7454 VIAF: 169107464  WorldCat Identities (via VIAF): 169107464 1987 births Living people Sportspeople from Yerevan Armenian women boxers German Armenians German women boxers Armenian emigrants to Germany Naturalized citizens of Germany World flyweight boxing champions World Boxing Association champions World Boxing Organization champions Articles with German-language external links Webarchive template wayback links Articles with short description Use dmy dates from September 2018 Articles containing Armenian-language text Featured articles Wikipedia articles with GND identifiers Wikipedia articles with ISNI identifiers Wikipedia articles with VIAF identifiers Wikipedia articles with WorldCat-VIAF identifiers Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version العربية Башҡортса Dansk Deutsch Ελληνικά Español فارسی Français Հայերեն Արեւմտահայերէն Bahasa Indonesia Italiano עברית ქართული 日本語 Norsk Polski Русский Shqip Simple English Svenska Tagalog Українська  This page was last edited on 2 August 2019, at 05:14 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Susianna Susi Levonovna Kentikian Syuzanna Kentikyan Professional record summary 39 fights 36 wins 2 losses By knockout By decision No contests a b a b c d e f ^ ^ a b a b c a b c a b c d a b a b ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ Susi Kentikian Vacant Regina Halmich  WIBF flyweight champion Vacant Eva Voraberger  WIBF flyweight champion Incumbent Vacant Eileen Olszewski  GBU female flyweight champion Inaugural champion  WBA female flyweight champion  WBO female flyweight champion Vacant Arely Muciño  WBA female flyweight championInterim title Vacant  WBA female flyweight champion Kentikian in 2018 Susianna Levonovna Kentikian[1] Killer Queen[2] Flyweight 1.55 m (5 ft 1 in) 
Armenian
German
  (1987-09-11) 11 September 1987 (age 32)Yerevan, Armenian SSR, Soviet Union (now Armenia) Orthodox 39 36 17 2 1 Professional record summary
 39 fights
 36 wins
 2 losses
 By knockout
 17
 0
 By decision
 19
 2
 No contests
 1
 39
 Win
 36–2 (1)
  Nevenka Mikulic
 UD
 10
 30 Jul 2016
  Alsterdorfer Sporthalle, Hamburg, Germany
 Retained WIBF and GBU female flyweight titles
 38
 Win
 35–2 (1)
  Susana Cruz Perez
 UD
 10
 2 Oct 2015
  Inselparkhalle, Hamburg, Germany
 Retained WBA female flyweight title;Won vacant WIBF and GBU female flyweight titles
 37
 Win
 34–2 (1)
  Naoko Fujioka
 UD
 10
 8 Nov 2014
  Porsche-Arena, Stuttgart, Germany
 Retained WBA female flyweight title
 36
 Win
 33–2 (1)
  Dan-Bi Kim
 TKO
 9 (10), 1:39
 31 May 2014
  König Palast, Krefeld, Germany
 Retained WBA female flyweight title
 35
 Win
 32–2 (1)
  Simona Galassi
 UD
 10
 7 Dec 2013
  Porsche-Arena, Stuttgart, Germany
 Retained WBA female flyweight title
 34
 Win
 31–2 (1)
  Carina Moreno
 UD
 10
 6 Jul 2013
  Westfalenhalle, Dortmund, Germany
 Won WBA female flyweight title
 33
 Win
 30–2 (1)
  Sanae Jah
 UD
 10
 1 Feb 2013
  ISS Dome, Düsseldorf, Germany
 Won vacant WBA female interim flyweight title
 32
 Loss
 29–2 (1)
  Carina Moreno
 SD
 10
 1 Dec 2012
  Burg-Wächter Castello, Düsseldorf, Germany
 Lost WBA female flyweight title
 31
 Loss
 29–1 (1)
  Melissa McMorrow
 MD
 10
 16 May 2012
  Brandenburg-Halle, Frankfurt, Germany
 Lost WBO, and WIBF female flyweight titles
 30
 Win
 29–0 (1)
  Teeraporn Pannimit
 UD
 10
 21 Oct 2011
  Brandenburg-Halle, Frankfurt, Germany
 Retained WBA, WBO, and WIBF female flyweight titles
 29
 Win
 28–0 (1)
  Ana Arrazola
 UD
 10
 26 Mar 2011
  Universum Gym, Hamburg, Germany
 Retained WIBF flyweight title
 28
 NC
 27–0 (1)
  Arely Muciño
 NC
 3 (10), 0:27
 17 Jul 2010
  Sport- und Kongresshalle, Schwerin, Germany
 Retained WBA, WBO, and WIBF female flyweight titles;NC after Kentikian was cut from an accidental head clash
 27
 Win
 27–0
  Nadia Raoui
 SD
 10
 24 Apr 2010
  Alsterdorfer Sporthalle, Hamburg, Germany
 Retained WBA, WBO, and WIBF female flyweight titles
 26
 Win
 26–0
  Julia Sahin
 UD
 10
 10 Oct 2009
  StadtHalle, Rostock, Germany
 Retained WBA and WIBF female flyweight titles;Won vacant WBO female flyweight title
 25
 Win
 25–0
  Carolina Marcela Gutierrez Gaite
 UD
 10
 4 Jul 2009
  Color Line Arena, Hamburg, Germany
 Retained WBA and WIBF female flyweight titles
 24
 Win
 24–0
  Elena Reid
 UD
 10
 20 Mar 2009
  Alsterdorfer Sporthalle, Hamburg, Germany
 Retained WBA and WIBF female flyweight titles
 23
 Win
 23–0
  Anastasia Toktaulova
 UD
 10
 5 Dec 2008
  Sporthalle Brandberge, Halle, Germany
 Retained WBA and WIBF female flyweight titles
 22
 Win
 22–0
  Hagar Shmoulefeld Finer
 UD
 10
 29 Aug 2008
  Burg-Wächter Castello, Düsseldorf, Germany
 Retained WBA and WIBF female flyweight titles
 21
 Win
 21–0
  Mary Ortega
 TKO
 1 (10), 1:53
 10 May 2008
  Sporthalle Brandberge, Halle, Germany
 Retained WBA and WIBF female flyweight titles
 20
 Win
 20–0
  Sarah Goodson
 TKO
 3 (10), 0:57
 29 Feb 2008
  Alsterdorfer Sporthalle, Hamburg, Germany
 Retained WBA and WIBF female flyweight titles
 19
 Win
 19–0
  Nadia Hokmi
 UD
 10
 7 Dec 2007
  Alsterdorfer Sporthalle, Hamburg, Germany
 Retained WBA female flyweight title;Won vacant WIBF female flyweight title
 18
 Win
 18–0
  Shanee Martin
 TKO
 3 (10), 1:14
 7 Sep 2007
  Burg-Wächter Castello, Düsseldorf, Germany
 Retained WBA female flyweight title
 17
 Win
 17–0
  Nadia Hokmi
 SD
 10
 25 May 2007
  Fight Night Arena, Cologne, Germany
 Retained WBA female flyweight title
 16
 Win
 16–0
  Maria Jose Nunez
 TKO
 3 (10)
 30 Mar 2007
  Kölnarena, Cologne, Germany
 Retained WBA female flyweight title
 15
 Win
 15–0
  Carolina Alvarez
 TKO
 9 (10), 0:27
 16 Feb 2007
  Fight Night Arena, Cologne, Germany
 Won vacant WBA female flyweight title
 14
 Win
 14–0
  Maja Frenzel
 TKO
 4 (10), 1:35
 21 Nov 2006
  Universum Gym, Hamburg, Germany
 Retained WIBF Inter-Continental flyweight title
 13
 Win
 13–0
  Maribel Zurita
 TKO
 4 (10)
 9 Sep 2006
  Bördelandhalle, Magdeburg, Germany
 Won vacant WIBF Inter-Continental flyweight title
 12
 Win
 12–0
  Daniela Graf
 UD
 10
 25 Jul 2006
  Sportschule Sachsenwald, Hamburg, Germany
 Won vacant International German female flyweight title
 11
 Win
 11–0
  Evgeniya Zablotskaya
 TKO
 2 (6), 1:32
 15 Apr 2006
  Maritim Hotel, Magdeburg, Germany
 
 10
 Win
 10–0
  Emilina Metodieva
 TKO
 4 (6), 1:33
 14 Jan 2006
  Ballhaus, Aschersleben, Germany
 
 9
 Win
 9–0
  Maria Krivoshapkina
 UD
 6
 13 Dec 2005
  Freizeit Arena, Sölden, Austria
 
 8
 Win
 8–0
  Svetla Taskova
 TKO
 2 (6), 1:08
 29 Oct 2005
  TURM ErlebnisCity, Brandenburg, Germany
 
 7
 Win
 7–0
  Renata Vesecka
 TKO
 4 (6), 0:58
 17 Sep 2005
  Harzlandhalle, Ilsenburg, Germany
 
 6
 Win
 6–0
  Simona Pencakova
 TKO
 2 (4), 1:14
 2 Jul 2005
  Color Line Arena, Hamburg, Germany
 
 5
 Win
 5–0
  Albena Atseva
 TKO
 2 (6)
 4 Jun 2005
  Ballhaus, Aschersleben, Germany
 
 4
 Win
 4–0
  Juliia Vlasenko
 TKO
 3 (4), 1:50
 7 May 2005
  Volkswagen Halle, Braunschweig, Germany
 
 3
 Win
 3–0
  Lucie Sovijusova
 TKO
 1 (4)
 9 Mar 2005
  Sporthalle Wandsbek, Hamburg, Germany
 
 2
 Win
 2–0
  Debbie Lohmaier
 KO
 1 (4), 1:23
 26 Feb 2005
  Color Line Arena, Hamburg, Germany
 
 1
 Win
 1–0
  Iliana Boneva
 UD
 4
 15 Jan 2005
  Bördelandhalle, Magdeburg, Germany
 
  Wikimedia Commons has media related to Susi Kentikian. Minor world boxing titles
 VacantTitle last held byRegina Halmich
  WIBF flyweight champion7 December 2007 – 16 May 2012
 Succeeded byMelissa McMorrow
 VacantTitle last held byEva Voraberger
  WIBF flyweight champion2 October 2015 – present
 Incumbent
 VacantTitle last held byEileen Olszewski
  GBU female flyweight champion2 October 2015 – present
 Major world boxing titles
 Inaugural champion
  WBA female flyweight champion16 February 2007 – 1 December 2012
 Succeeded byCarina Moreno
  WBO female flyweight champion10 October 2009 – 16 May 2012
 Succeeded byMelissa McMorrow
 VacantTitle last held byArely Muciño
  WBA female flyweight championInterim title1 February 2013 – 6 July 2013Won full title
 Vacant
 Preceded byCarina Moreno
  WBA female flyweight champion6 July 2013 – 2017
 Succeeded byNaoko Fujioka
 
GND: 143858548
ISNI: 0000 0001 1812 7454
VIAF: 169107464
 WorldCat Identities (via VIAF): 169107464
 