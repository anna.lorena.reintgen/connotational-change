
 Portman Road is a football stadium in Ipswich, Suffolk, England, which has been the home ground of Ipswich Town F.C. since 1884. The stadium has also hosted many England youth international matches, and one senior England friendly international match, against Croatia in 2003. It  staged several other sporting events, including athletics meetings and international hockey matches, musical concerts and Christian events.
 The stadium underwent significant redevelopments in the early 2000s, which increased the capacity from 22,600 to a current figure of 30,311, making it the largest-capacity football ground in East Anglia. Its four stands have since been converted to all-seater, following the recommendations of the Taylor Report.
 Ipswich played their early matches at Broomhill Park,[4] but in 1884, the club moved to Portman Road and have played there ever since.[1] The ground was also used as a cricket pitch during the summer by the East Suffolk Cricket Club who had played there since 1855.[5] The cricket club had erected a pavilion, the first fixed building at the ground. More substantial elements of ground development did not begin for a further 11 years, though Ipswich became one of the first clubs to implement the use of goal nets in 1890.[1] At this time, Ipswich were an amateur side (the team became professional in 1936) and the first visit of a professional club came in 1892, when Preston North End played a Suffolk County Football Association team. This was followed six years later by a visit from Aston Villa, a game which was so popular that a temporary stand was erected in order to accommodate a crowd of around 5,000.[6] In 1901, a tobacco processing plant was built along the south edge of the ground by the Churchman brothers,[1] a name which would later become synonymous with the south stand located there.[7]
 The first permanent stand, a wooden structure known affectionately as the "Chicken Run",[8][9] was built on the Portman Road side of the ground in 1906.[6] This structure was sold in 1971 to the local speedway team, the Ipswich Witches, who installed it at Foxhall Stadium.[6] In 1914, the ground was commandeered by the British Army for use as a training camp for the duration of the First World War. Control of Portman Road was not returned to the club until two years after the end of the war and significant work was required to repair damage to the ground caused by heavy machinery.[6]
 For a short period during the 1920s, Portman Road was host to a number of whippet races in an attempt to increase revenue,[6] and in 1928 a small stand was built on the west side of the ground. The football club turned professional in 1936 and the cricket club were forced to move out,[5] so work began on the first bank of terracing at the north end of the pitch. The following year, on the back of winning the Southern League, a similar terrace was built at the southern "Churchmans" end and 650 tip-up seats, bought from Arsenal, were installed.[6] Portman Road was home to Ipswich Town's first Football League match on 27 August 1938, a 4–2 victory against Southend United in Division Three (South) witnessed by more than 19,000 spectators.[10]
 The Supporters' Association funded a number of improvements at Portman Road; in 1952, concrete terracing replaced the wooden terraces at the cost of £3,000 and another £3,000 was used to re-terrace the North Stand in 1954, bringing the capacity of the ground to approximately 29,000. In 1957, the association raised £30,500 towards the building of a new West Stand, increasing ground capacity to around 31,000. Floodlights were installed two years later; the result of £15,000 raised by the association.[1] The floodlights were switched on by club president Lady Blanche Cobbold for the first floodlit match at the ground, a friendly against Arsenal, in February 1960.[11][12]
 Television cameras made their debut at Portman Road in 1962 as Anglia Television arrived for Match of the Week;[13] it was another six years before the BBC televised a match at the ground, Match of the Day visiting Portman Road for the first time in 1968 to witness Ipswich's league fixture against Birmingham City.[6] Meanwhile, ground development continued with roofing enhancements to the North Stand and an increase in capacity to 31,500 by 1963. Dressing rooms were constructed in 1965 and new turnstiles were introduced two years later, including a separate entrance for juveniles at the Churchmans end. In 1968 the club agreed to a new 99-year lease on the ground with owners Ipswich Borough Council.[6]
 The two-tier propped cantilever Portman Stand was built along the east side of the ground in place of the existing terraces in 1971, providing 3,500 additional seats and increasing the capacity of the ground to approximately 37,000. Advertising appeared around the perimeter of the ground in the same year, while the following year saw the construction of the "Centre Spot" restaurant underneath the Portman Stand. Additional seating was added to the Portman Stand in 1974 and the ground saw its record attendance of 38,010 the following year in an FA Cup tie against Leeds United. Following success in the 1978 FA Cup, the club invested in 24 executive boxes in front of the Portman Stand and, as a result of the Safety of Sports Ground Act (1975), reduced the capacity in front by introducing seats, taking the overall capacity down to 34,600.[1][6]
 Plastic seats replaced wooden benches in the West Stand in 1980 and in the following year, the club announced a deal with electronics company Pioneer Corporation with the stand expanded at a cost of around £1.3m, renamed as the Pioneer Stand and re-opened in 1983. However, the cost of building the stand meant the club had to sell players, and led to a decline in fortunes on the pitch.[14] Safety barriers were removed from the North Stand in 1989 following the Hillsborough disaster and following the recommendations of the Taylor Report, the terraces in both the North and South stands were also converted to all-seating. The Pioneer Stand was renamed as the Britannia Stand following a new sponsorship deal with the building society in 1999,[1][6] and in the following year a statue of Sir Alf Ramsey was unveiled at the corner of Portman Road and Sir Alf Ramsey Way.[15]
 Success for Ipswich Town in promotion to the Premier League in 2000 led to further investment in the infrastructure, with the club spending around £22 million on redeveloping both the North and South stands.[1] The complete renovation of the South Stand into a two-tier stand added 4,000 seats to the stadium.  The subsequent demolition and reconstruction of a two-tier North Stand added a further 4,000 seats and brought the total capacity of the ground to more than 30,000.[1] In 2001, local brewery Greene King took on the sponsorship of the updated South Stand and as such, the stand was renamed the Greene King stand until 2009, when the sponsorship deal ended and the name changed back to the original 'South Stand'.[16]
 Following the death of former manager Bobby Robson in 2009, the club announced that the North Stand would be renamed as the Sir Bobby Robson Stand.  The official unveiling took place at half time during the league match hosting Newcastle United, another of Robson's former clubs, on 26 September 2009.[17] On 31 March 2012, the South Stand was renamed the Sir Alf Ramsey Stand in memory of Sir Alf Ramsey who guided Ipswich Town to the Old First Division title. The season was the 50th anniversary of Ipswich Town winning the old First Division title.[18] On 10 July 2012, the Britannia Stand was renamed East of England Co-operative Stand following a sponsorship deal with the East of England Co-operative Society.[19]
 The pitch is surrounded by four all-seater stands, the Sir Bobby Robson Stand, the Cobbold Stand, the Sir Alf Ramsey Stand and the East of England Co-operative Stand.  All stands are covered and are multi-tiered.[20]
 The Cobbold Stand was constructed in 1971, and originally known as the Portman Stand.[21] With two tiers and a cantilever roof, it is used to accommodate away fans, with an allocation of up to 3,000 seats per game and for family seating.  It also contains a number of executive boxes as well as regular seating for home fans.[22] The Sir Alf Ramsey Stand is a two-tiered stand which has a match-day capacity of approximately 7,000.  It also houses the "Sir Bobby Robson Suite" restaurant and "Legends Bar".  The tunnel, from which the players emerge onto the pitch from their dressing rooms, is unusually located in the corner of the stadium between the Sir Alf Ramsey Stand and the East of England Co-operative Stand.[21]
 The East of England Co-operative Stand, constructed in 1957,[20] was updated to an all-seater stand in 1990 and currently has three tiers consisting of home fan seating and an additional family area.  It also contains the directors' box, further executive boxes and the press area.  Behind the stand is a full-size AstroTurf pitch which is often used on a casual basis by fans before home games.[23] The Sir Bobby Robson Stand was completely rebuilt in 2001 and has a capacity of around 7,500.[24] It is a two-tier cantilever stand which is divided into an adults-only lower tier "...traditionally for the 'hard core' Town fan..."  and a mixed upper tier.[16][25]
 There are nine areas throughout the ground designated for disabled supporters, in the lower East of England Co-operative Stand, the Sir Alf Ramsey Stand and the Sir Bobby Robson Stand.  These provide over 300 spaces to accommodate wheelchair users and ambulant disabled, together with their carers.  The ground also provides 12 seats in the East of England Co-operative Stand for visually impaired spectators with commentary via individual radio headsets in each seat, provided by local radio station BBC Radio Suffolk.[26]
 The former groundsman, Alan Ferguson, received a number of accolades, including both Premiership and Championship Groundsman of the Year,[27][28] and the pitch was voted the best in the Championship for three consecutive seasons in 2003, 2004 and 2005.[29]
 There are statues of Sir Alf Ramsey and Sir Bobby Robson, both former Ipswich Town and England managers, outside the ground.[30][31] Nearby Portman's Walk was renamed Sir Alf Ramsey Way in 1999.[6]
 On 20 August 2003, Portman Road hosted its first and thus far only senior England fixture, a friendly against Croatia,[32] the match finishing 3–1 to England in front of 28,700 spectators.[33] The stadium has been used by England youth teams on a number of occasions, the first on 24 November 1971, saw the England U23 team draw 1–1 with Switzerland.[34] England U21s drew in a UEFA European U21 Championship qualifying match there on 18 August 2006 against Moldova, in front of 13,556 spectators.[35]
 During the years when Ipswich were members of the Amateur Football Alliance (AFA), Portman Road was a regular venue for showpiece matches, staging AFA Senior Cup Finals and matches against the Corinthians. It also hosted two AFA international representative matches in 1909 and 1910, with an England XI beating teams representing Bohemia (10–1) and France (20–0).[36]
 In addition, a variety of other sports have been hosted at Portman Road, including athletics in 1927,[37] an American football match in 1953,[38] and several international hockey matches in the 1960s and 1970s.[39][40][41]
 The stadium has also hosted several music concerts, including performances by Elton John,[42] R.E.M.,[43] Red Hot Chili Peppers,[44] Pink,[45] Neil Diamond,[46] Tina Turner,[47] and Rod Stewart,[48] among others.
 In March 2005, around 8,000 Christians attended a gathering at the stadium, the largest act of Christian worship in Suffolk, since American evangelist Billy Graham used Portman Road on part of his Mission England Tour in 1984.[49]
 The highest attendance recorded at Portman Road is 38,010 for a match against Leeds United in the FA Cup sixth round on 8 March 1975.[50] The record modern (all-seated) attendance is 30,152, set on 21 December 2003 against local rivals Norwich City in Football League Division One.[50] The largest crowd for a non-competitive game at the ground was over 23,000 for Bobby Robson's testimonial where Ipswich, including George Best, played against an England XI.[51]
 The highest seasonal average at the stadium since Ipswich turned professional was 26,431 in the 1976–77 season while Ipswich were playing in the First Division.  The lowest average attendance at Portman Road was 8,741 in the club's inaugural league season, the 1936–37 season in Division Three (South). The highest total seasonal attendance was recorded during the 1980–81 season when the aggregate was more than 814,000 during a season in which Ipswich won the UEFA Cup Final and finished second in the First Division.[52]
 Portman Road hosted Ipswich Town's first appearance in European football competition when they defeated Floriana of Malta 10–0, still a club record, in the European Cup in 1962.[53] Since then, Ipswich Town remain undefeated at Portman Road in all European competitions, a total of 31 matches spanning 40 years,[54] a record until it was surpassed by AZ Alkmaar in December 2007.[55]
 The stadium is approximately 450 yards (410 m) from Ipswich railway station, which lies on the Great Eastern Main Line from London Liverpool Street to Norwich. The stadium has parking nearby for supporters, and the streets around the ground are subject to a residents-only permit parking scheme, but there are several pay and display or park and ride car parks within a short distance of the ground.[21][22][56]
  Media related to Portman Road at Wikimedia Commons
 
 1 History 2 Structure and facilities 3 Other uses 4 Records 5 Transport 6 References 7 External links ^ a b c d e f g h i j "History of the Stadium". Ipswich Town F.C. Archived from the original on 3 July 2008. Retrieved 4 February 2013..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ "Ipswich Town". Championship Clubs. The Football League. 1 August 2010. Archived from the original on 21 April 2012. Retrieved 20 March 2012.
 ^ "Portman Road". Soccerbase. Archived from the original on 7 January 2008. Retrieved 19 January 2008.
 ^ "Why save Broomhill outdoor swimming pool". Broomhill Pool Trust. Archived from the original on 11 March 2012. Retrieved 21 March 2012.
 ^ a b "About us – The Early Years ..." Ipswich and East Suffolk Cricket Club. Archived from the original on 12 November 2004. Retrieved 23 January 2008.
 ^ a b c d e f g h i j k "The History of Portman Road". Pride of Anglia. Archived from the original on 15 May 2008. Retrieved 19 January 2008.
 ^ Paul Geater (21 December 2007). "Another nail in industry's coffin". Ipswich Evening Star. Archived from the original on 20 April 2013. Retrieved 21 March 2012.
 ^ "Heaven and Hell – the signings". East Anglian Daily Times. Archived from the original on 16 September 2007. Retrieved 24 January 2008.
 ^ "North Stand, The Making of the". Pride of Anglia. Retrieved 24 January 2008. (subscription required)
 ^ "Match Report – Ipswich Town (2) 4–2 (0) Southend United". Soccerbase. Archived from the original on 13 February 2012. Retrieved 19 January 2008.
 ^ Inglis, Simon (1984) [1983]. "Chapter 10. East Anglia and Essex". The Football Grounds of England and Wales. Collins Willow. pp. 117–118.
 ^ "Anglia News: Floodlights Switched On At Ipswich: Ipswich Beat Arsenal". Anglia Television news report. East Anglian Film Archive. Archived from the original on 30 July 2012. Retrieved 21 March 2012.
 ^ John Bourn. "History of football on ITV". Archived from the original on 12 January 2005. Note that the reference says Match of the Week started in 1963; however according to Soccerbase Archived 2 October 2007 at the Wayback Machine, Ipswich's 3–2 loss to Wolves actually occurred in 1962.
 ^ "Q&A Redevelopment Questions". Cambridge United F.C. Archived from the original on 9 August 2011. Retrieved 4 February 2013.
 ^ "Statue of Sir Alf unveiled – Part One". Ipswich Town F.C. Archived from the original on 8 September 2005. Retrieved 16 March 2007.
 ^ a b "Sponsors". Ipswich Town F.C. 26 November 2007. Archived from the original on 9 May 2008. Retrieved 20 January 2008.
 ^ "Ipswich to unveil Sir Bobby stand". BBC Sport. 18 August 2009. Retrieved 20 August 2009.
 ^ "Ipswich Town renames stand after Sir Alf Ramsey". BBC News. 30 March 2012. Archived from the original on 3 April 2012. Retrieved 31 March 2012.
 ^ Gooderham, Dave (11 July 2012). "Updated: Britannia Stand becomes the East of England Co-operative Stand". East Anglian Daily Times. Retrieved 28 October 2017.
 ^ a b Halls, Ross. "Stadium History". Ipswich Town Talk. Archived from the original on 28 October 2017. Retrieved 28 October 2017.
 ^ a b c Duncan Adams (21 February 2012). "What's Portman Road Like?". Ipswich Town: Portman Road. Football Ground Guide. Archived from the original on 5 June 2012. Retrieved 22 March 2012.
 ^ a b "Portman Road". Ipswich Town F.C. 11 January 2012. Archived from the original on 7 August 2011. Retrieved 4 February 2013.
 ^ "Behind enemy lines!". vitalfootball.co.uk. Archived from the original on 10 February 2012. Retrieved 25 January 2008.
 ^ 
John Cutlack (1 April 2002). "Ipswich Town Football Club: The north stand". High Beam research. Concrete. Archived from the original on 9 April 2016. Retrieved 22 March 2012. (subscription required to view full article)
 ^ "Seating plan at Portman Road". Ipswich Town F.C. 14 May 2007. Archived from the original on 7 August 2011. Retrieved 4 February 2013.
 ^ "Facilities for disabled at Ipswich Town". 8 October 2007. Archived from the original on 10 February 2012. Retrieved 4 February 2013.
 ^ Tyldesley, Clive (15 April 2001). "Understated Ipswich begin to betray their excitement". The Daily Telegraph. London. Archived from the original on 14 October 2007. Retrieved 19 January 2008.
 ^ "Groundsman admits mistakes made". Ipswich Evening Star. 3 January 2007. Archived from the original on 7 April 2008. Retrieved 19 January 2008.
 ^ "Ipswich scoop pitch award again". BBC Sport. 22 April 2005. Archived from the original on 13 January 2016. Retrieved 19 January 2008.
 ^ "Sir Bobby Robson statue unveiled". BBC Sport. 16 July 2002. Archived from the original on 2 July 2004. Retrieved 19 January 2008.
 ^ "Sir Alf tribute unveiled". BBC Sport. 22 August 2000. Retrieved 19 January 2008.
 ^ "FA chooses Portman Road". BBC Sport. 18 June 2003. Archived from the original on 12 October 2003. Retrieved 12 October 2007.
 ^ "England 3 Croatia 1". The Football Association. Archived from the original on 10 January 2008. Retrieved 12 October 2007.
 ^ "Portman Road ready for England". The Football Association. 17 June 2003. Archived from the original on 8 May 2005. Retrieved 19 January 2008.
 ^ Derek Davis (16 August 2006). "Walcott underlines promise with goal". Green'un24. Archived from the original on 4 March 2016. Retrieved 19 March 2012.
 ^ Mills, Richard. "An Exception in War and Peace: Ipswich Town Football Club, c. 1907–1945" (pdf). University of East Anglia. Retrieved 14 December 2017.
 ^ "1927 County Championships". Pride of Anglia. 18 June 1927. Archived from the original on 25 February 2009. Retrieved 19 January 2008.
 ^ "Bentwaters vs Sculthorpe". Pride of Anglia. 29 September 1953. Archived from the original on 25 February 2009. Retrieved 19 January 2008.
 ^ "Hockey International – Great Britain vs West Germany". Pride of Anglia. 29 June 1968. Archived from the original on 25 February 2009. Retrieved 19 January 2008.
 ^ "International Hockey – Great Britain vs France". Pride of Anglia. 24 June 1972. Archived from the original on 25 February 2009. Retrieved 19 January 2008.
 ^ "International Hockey – Great Britain vs Malaysia (men) & England vs Holland (women)". Pride of Anglia. 26 June 1976. Archived from the original on 25 February 2009. Retrieved 19 January 2007.
 ^ "Elton makes Ipswich date". BBC Suffolk. 20 December 2003. Retrieved 19 January 2008.
 ^ "R.E.M playing at Portman Road". BBC Suffolk. Archived from the original on 30 August 2005. Retrieved 19 January 2008.
 ^ Sullivan, Caroline (3 July 2006). "Red Hot Chili Peppers". The Guardian. London. Archived from the original on 8 January 2008. Retrieved 19 January 2008.
 ^ "Ipswich UK, Portman Road". pinkspage.com. Archived from the original on 10 November 2009. Retrieved 20 March 2012.
 ^ "Neil Diamond, Portman Road Football Ground, Thursday 26 May 2005". BBC Suffolk. Archived from the original on 28 May 2008. Retrieved 19 January 2008.
 ^ Haugh, Richard (20 May 2010). "Johnny Cash: Remembering the Ipswich gig that never was". BBC Suffolk. Archived from the original on 25 May 2010. Retrieved 31 March 2012.
 ^ "Rod Stewart is set to rock Ipswich". Ipswich Evening Star. 7 November 2006. Archived from the original on 7 January 2008. Retrieved 19 January 2008.
 ^ "Church organises stadium worship". BBC News. 29 March 2005. Retrieved 19 January 2008.
 ^ a b Duncan Adams (21 February 2012). "Record & Average Attendance". Ipswich Town: Portman Road. Football Ground Guide. Archived from the original on 5 June 2012. Retrieved 22 March 2012.
 ^ "GEORGE BEST TURNS OUT FOR IPSWICH TOWN". Colorspot Images Ltd. Archived from the original on 26 October 2011. Retrieved 19 March 2012.
 ^ "Attendances since 1936 (all competitions)". Pride of Anglia. Archived from the original on 27 September 2007. Retrieved 18 January 2008. (subscription required)
 ^ "Ipswich Town". fchd.info. Archived from the original on 14 April 2010. Retrieved 23 January 2008.
 ^ "Ipswich edge out Liberec". BBC Sport. 31 October 2002. Archived from the original on 6 May 2004. Retrieved 23 January 2008.
 ^ Caroline Cheese (20 December 2007). "AZ Alkmaar 2–3 Everton". BBC Sport. Archived from the original on 21 December 2007. Retrieved 23 January 2008.  AZ Alkmaar were defeated in the match after breaking the record total; this could allow the record to be regained by Ipswich Town should they qualify for Europe in the future and remain undefeated in their next two home games.
 ^ "Ipswich". Multimap. Retrieved 19 January 2008.
 v t e Ipswich Town F.C. Managers  Players Records Seasons History of Ipswich Town F.C. Portman Road Players of the Year Hall of Fame U23s and Academy East Anglian derby Pride of Anglia All articles v t e Adams Park Bloomfield Road Crown Ground Fratton Park Gigg Lane Highbury Stadium Kassam Stadium Keepmoat Stadium Kingsmeadow London Road Memorial Stadium New Meadow New York Stadium Pirelli Stadium Portman Road Prenton Park Priestfield Stadium Roots Hall Sincil Bank Spotland Stadium St Andrew's Stadium MK Stadium of Light University of Bolton Stadium v t e Anfield Bramall Lane Carrow Road City of Manchester Stadium Dean Court Emirates Stadium Falmer Stadium Goodison Park King Power Stadium London Stadium Molineux Old Trafford St James' Park St Mary's Stadium Selhurst Park Stamford Bridge Tottenham Hotspur Stadium Turf Moor Vicarage Road Villa Park Bet365 Stadium Bloomfield Road Boundary Park Cardiff City Stadium City Ground County Ground Craven Cottage DW Stadium Elland Road Ewood Park Fratton Park The Hawthorns Hillsborough Stadium KCOM Stadium Kirklees Stadium Liberty Stadium Loftus Road Madejski Stadium Oakwell Portman Road Pride Park Riverside Stadium Stadium of Light St Andrew's University of Bolton Stadium The Valley Valley Parade Wembley Stadium Ayresome Park Baseball Ground Boleyn Ground Burnden Park The Dell Filbert Street Highbury Highfield Road Maine Road Roker Park White Hart Lane Ipswich Town F.C. Sports venues in Ipswich Football venues in Suffolk Football venues in England Premier League venues Sports venues completed in 1884 English Football League venues 1884 establishments in England Pages containing links to subscription-only content Webarchive template wayback links Articles with short description Use British English from September 2012 Use dmy dates from December 2017 Pages using deprecated image syntax Coordinates on Wikidata Commons category link from Wikidata Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version العربية Azərbaycanca تۆرکجه Bosanski Dansk Deutsch Ελληνικά Español فارسی Français Gaeilge 한국어 Bahasa Indonesia Italiano עברית Lietuvių Nederlands 日本語 Norsk ਪੰਜਾਬੀ Polski Português Русский Simple English Svenska Türkçe Українська 中文  This page was last edited on 23 September 2019, at 16:30 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Portman Road a b c d e f g h i j ^ ^ ^ a b a b c d e f g h i j k ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ a b a b c a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ Aerial view of Portman Road, 2015 Portman Road Stadium Ipswich, Suffolk, England 52°3′18.22″N 1°8′41.39″E﻿ / ﻿52.0550611°N 1.1448306°E﻿ / 52.0550611; 1.1448306Coordinates: 52°3′18.22″N 1°8′41.39″E﻿ / ﻿52.0550611°N 1.1448306°E﻿ / 52.0550611; 1.1448306 Ipswich Borough Council[1] 30,311[2] 112 by 82 yards (102 m × 75 m)[3] Grass 1884[1] 2002 Ipswich Town F.C. 
Ipswich Town F.C.
Managers
 Players
Records
Seasons
 
History of Ipswich Town F.C.
Portman Road
Players of the Year
Hall of Fame
U23s and Academy
 
East Anglian derby
Pride of Anglia
All articles
 
Adams Park
Bloomfield Road
Crown Ground
Fratton Park
Gigg Lane
Highbury Stadium
Kassam Stadium
Keepmoat Stadium
Kingsmeadow
London Road
Memorial Stadium
New Meadow
New York Stadium
Pirelli Stadium
Portman Road
Prenton Park
Priestfield Stadium
Roots Hall
Sincil Bank
Spotland Stadium
St Andrew's
Stadium MK
Stadium of Light
University of Bolton Stadium
 
Anfield
Bramall Lane
Carrow Road
City of Manchester Stadium
Dean Court
Emirates Stadium
Falmer Stadium
Goodison Park
King Power Stadium
London Stadium
Molineux
Old Trafford
St James' Park
St Mary's Stadium
Selhurst Park
Stamford Bridge
Tottenham Hotspur Stadium
Turf Moor
Vicarage Road
Villa Park
 
Bet365 Stadium
Bloomfield Road
Boundary Park
Cardiff City Stadium
City Ground
County Ground
Craven Cottage
DW Stadium
Elland Road
Ewood Park
Fratton Park
The Hawthorns
Hillsborough Stadium
KCOM Stadium
Kirklees Stadium
Liberty Stadium
Loftus Road
Madejski Stadium
Oakwell
Portman Road
Pride Park
Riverside Stadium
Stadium of Light
St Andrew's
University of Bolton Stadium
The Valley
Valley Parade
Wembley Stadium
 
Ayresome Park
Baseball Ground
Boleyn Ground
Burnden Park
The Dell
Filbert Street
Highbury
Highfield Road
Maine Road
Roker Park
White Hart Lane
 