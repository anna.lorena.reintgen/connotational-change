
 Yarralumla is a large inner south suburb of Canberra, the capital city of Australia. Located approximately 3.5 kilometres (2.2 mi) south-west of the city, Yarralumla extends along the south-west bank of Lake Burley Griffin. (The lake was created after the Second World War through the blocking, with a dam, of the Molonglo River.)
 In 1828, Henry Donnison, a Sydney merchant, was granted a lease on the western side of Stirling Ridge. Donnison's land was named Yarralumla in a survey of the area conducted in 1834, apparently after the indigenous people's term for the area. It was also spelt Yarrolumla in other documents. In 1881, the estate was bought by Frederick Campbell, grandson of Robert Campbell who built nearby "Duntroon". He completed the construction of a large, gabled, brick house on his property in 1891 that now serves as the site of Government House, the official residence of the Governor-General of Australia. Campbell's house replaced an elegant, Georgian-style homestead, the main portions of which were erected from local stone in the 1830s. Among the old Yarralumla homestead's most notable occupants were Sir Terence Aubrey Murray, who owned Yarralumla sheep station from 1837 to 1859, Augustus Onslow Manby Gibbes, who owned the property from 1859 to 1881, and Augustus' father Colonel John George Nathaniel Gibbes (1787–1873). (Augustus "Gussie" Gibbes was Murray's brother-in-law; he also advanced money to Frederick Campbell to assist with the construction, in 1890–1891, of Campbell's grand new family house at Yarralumla.)
 The modern suburb of Yarralumla was gazetted by the government in 1928 and as of 2011[update] was home to approximately 3,000 people and many diplomatic missions. In recent years, it has become one of Canberra's most desirable and expensive residential suburbs because of its wide leafy streets, attractive lakeside setting and central location.
 Yarralumla is located in the central Canberra district of South Canberra. It is bordered by Lake Burley Griffin to the north, Commonwealth Avenue and Capital Hill to the east, Adelaide Avenue and the Cotter Road to the south, and Scrivener Dam, Lady Denman Drive and part of the Molonglo River to the west.[2]
 Although Yarralumla is one of the largest suburbs in Canberra by area,[3] its population remains quite small because more than half of its area consists of open space or non-residential development, including Weston and Stirling Parks, the Royal Canberra Golf Club, and the grounds of Government House.[2] A relatively high proportion of houses are occupied by diplomatic missions.[2]
 The embassy area of Yarralumla is located towards the eastern end of the suburb next to Stirling Park.[2] It is the hilliest area of Yarralumla; Parliament House and the Parliamentary Triangle are located nearby.[2]
 The streets in Yarralumla are named after Australian governors and botanists.[5] Most of the older streets in the suburb are laid out on an approximately rectangular grid with some curved sections, while the more hilly eastern end of the suburb, including the embassy district, is set out with contour-guided roads.[2] Major roads in Yarralumla include Banks Street, Novar Street and Hopetoun Circuit in a north-south direction and Schlich Street, Loftus Street and Weston Street running east-west. Being a dormitory suburb, there are no major through roads.[2] Access to the rest of the city can be made from Adelaide Avenue, Commonwealth Avenue, Lady Denman Drive and Cotter Road, all of which run along the borders of the suburb.[2] From these roads, entry to the suburb can be gained by turning into roads such as Coronation Drive, Hopetoun Circuit and Novar Street.[2]
 Yarralumla is located on the Yarralumla Formation which is a mudstone/siltstone formation that was formed around 425 million years ago during the Silurian Period. The formation extends from Red Hill and Woden in the South to Lake Burley Griffin in to the north, passing under the suburb of Yarralumla. The formation is evidence of the last major marine sedimentary period when eastern Australia was still covered by shallow seas. It shows fossil evidence of trilobites, coral and primitive crinoids. The Yarralumla brickworks quarry and the Deakin anticline are places where the formation is exposed and easily studied.[6]
 The area now called Yarralumla is part of two original land grants, which were granted to free settlers for the establishment of farms. In 1828 Henry Donnison, a Sydney merchant who had arrived with his wife and family on the brig Ellen on 29–30 July 1828, was granted an allotment on the western side of Stirling Ridge. A second grant was made to William Klensendorlffe (a German who had served in the British Navy and arrived free in the Colony in 1818), who had bought the land from John Stephen, on 7 March 1839. Donnison's land was named Yarralumla in a survey of the area conducted in 1834. Yarralumla was a name for the area used by the local people, apparently meaning "echo".[5] An area to the west of what is now the suburb was the Yarrolumla parish.[7]
 The prominent New South Wales parliamentarian Sir Terence Aubrey Murray (1810–1873) purchased Yarralumla in 1837. He lived there with his wife Mary Murray (née Gibbes, 1817–1858), the second daughter of the Collector of Customs for NSW, Colonel John George Nathaniel Gibbes (1787–1873), MLC. In 1859, Murray sold Yarralumla to his brother-in-law, Augustus Onslow Manby Gibbes (1828–1897). Later that same year, Augustus' parents came to live with him at Yarralumla homestead.
 Augustus Gibbes improved the estate and acquired additional land by purchase and lease. However, In 1881, he sold Yarralumla for 40,000 pounds to Frederick Campbell, a descendant of Robert Campbell, in order to travel overseas. Frederick Campbell erected a new, three-storey, brick house on the site of the former Yarralumla homestead at the beginning of the 1890s. Campbell's house would later form the basis of what is now the Governor-General of Australia's official Canberra residence, known colloquially as "Yarralumla" or "Government House". Campbell also built a large wooden woolshed nearby in 1904. It remains standing to this day.[8]
 In 1908, the Limestone Plains area, including Yarralumla, was selected as the site for the capital city of the newly established Commonwealth of Australia. Soon afterwards in 1913, the Commonwealth Government purchased the property. Tenant farmers were allowed to stay on the land on annual leases, some remaining until 1963 when the Molonglo River was dammed to form Lake Burley Griffin.[8]
 With the construction of Australia's capital city underway, the Yarralumla brickworks were established in 1913 to supply building material. The bricks were used for many of Canberra's buildings, including the provisional Parliament House. In 1917, Walter Burley Griffin named the area surrounding the brickworks "Westridge".[9]
A narrow gauge goods railway was constructed for the transportation of bricks to some of the major building sites in central Canberra. This linked the brickworks to places such as Parliament House, and the Kingston Power House.[6][10]
 Construction on the Commonwealth nursery and Westbourne Woods arboretum was started in 1914, and a temporary camp was built near the brickworks to accommodate the workers. Thomas Charles Weston was Officer-in-Charge (Afforestation Branch) in the years 1913 to 1926, and later became Director of City Planting and the Superintendent of Parks and Gardens. Weston was responsible for testing and selecting plant species at the arboretum for their suitability to Canberra's environment; from 1913 through to 1924 Weston oversaw the propagation of more than two million trees which were then planted in the Canberra area.[11] Most of the original Westbourne Woods arboretum is now leased to the Royal Canberra Golf Club, with the remainder forming part of Weston Park. The Yarralumla nursery is still active, albeit on a smaller scale and functioning as a retail nursery selling both wholesale and direct to the public.[12]
 In 1922, a workers' tent camp was erected on the eastern side of Stirling Ridge to house the men working on the main intercepting sewer. The following year saw the start of the construction of 62 small, four-room, unlined timber cottages, to be used as housing for the married tradesmen involved in the construction of the provisional Parliament House. Other camps were established at the eastern end of Stirling Park on the hills opposite modern Lotus Bay. The first of these was contractor John Howie's settlement (1922–30), consisting of 25 timber cottages for his married men and timber barracks (Hostel Camp) for his single men. Two other single men's tent camps were established nearby—Old Tradesmen's Camp (1923–27) and No 1 Labourers Camp (1924–27). The men from Howie's worked on the Hotel Canberra and the others on the construction of the provisional Parliament House and nearby administrative buildings.[13]
 The Stirling Park camps were known as Westlake to their new inhabitants, and previously "Gura Bung Dhaura" (stony ground) to the local Aboriginal people.[14] In 1925, the population of this temporary suburb was 700. This represented roughly one-fifth of the total population of the Federal Capital Territory at the time;[13] in the region, only Molonglo Settlement had a larger population, at 750. The site was chosen so that it was near to Parliament House but hidden from direct line of sight from anywhere "important". The small cottages at Westlake were removed starting in the mid-1950s, with the last one removed in 1965. Many of the Westlake workers' cottages were moved to Queanbeyan and are still used as housing today. The Stirling Park near the embassy area of Yarralumla now covers the historic Westlake settlement area.
 The Commonwealth Forestry School was established in Westridge near the brickworks and Westbourne Woods in 1926. It opened with its first intake of students in the following year. Today the heritage-listed Forestry School and the associated principal's residence Westridge House are located on Banks Street, Yarralumla. The Commonwealth Scientific and Industrial Research Organisation (CSIRO) Forestry and Forestry Products subsumed the school in 1975. Westridge House, an impressive Tudor-style structure, underwent a A$500,000 refurbishment and is used as a residence for the chief officer of the CSIRO.[15]
 By 1928, there were over 130 people on the electoral roll for Westridge.[13] The majority of the population consisted of men working at the brickworks and nursery. Westridge was gazetted as a Canberra suburb in 1928.
 Westridge was renamed Yarralumla in the 1950s. In 1963, Lake Burley Griffin was filled and Yarralumla was expanded to include Westlake, which had up until then been part of Acton.[13]
 After the Second World War, the suburb began to expand rapidly with the construction of many private homes. Yarralumla's image as a lower-class suburb would persist into the 1960s and 1970s. This general perception began to alter once Lake Burley Griffin had been created and its surrounds landscaped into parklands; the area soon gained a reputation for its attractive lakeside location.[16] During the 1980s, house prices began to rise coincident with a rejuvenation of the suburb. Many of the original government-built monocrete, brick, and weatherboard houses have been demolished and replaced by larger dwellings of a variety of more modern styles and materials.
 The population of the Westridge area on the 1928 electoral roll numbered over 130.[9] At the 2016 census, Yarralumla had a population of 2,890 people.[1] Of these 46.9% were male. The suburb had only 1.0% indigenous Australians, substantially below the national average of 2.8%.[1] The percentage of married people in the suburb was nine points higher than the national average, and the proportion of residents who had never married was 9 points lower. Despite the higher level of marriage, this did not translate into a higher level of children; 49.6% of families consisted of a couple without children in the household, compared to the national average of 37.8%. Yarralumla residents had a median age of 49, compared to a Canberra average of 35. The suburb had an older population for a city of young people; the median national age is 38 and 41.7% of Yarralumla residents were 55 or over, compared to the national figure of 27.6%.[1]
 Yarralumla is a comparatively wealthy suburb with a 2016 median weekly personal income of $1,347; this compares to an ACT-wide figure of $998 and an Australia-wide figure of $662. The public service or defence force employed around 21.7% of the workforce, somewhat less than the ACT average of 23.6%. This compared to 1.9% for Australia as a whole.[1] The higher incomes were derived from the suburb's white-collar base; 63.5% of Yarralumla's workforce was employed as professionals or in managerial posts, compared to 35.2% nationally. In contrast, only 3.4% were engaged in blue-collar occupations, compared with 15.8% for the nation as a whole. The proportion of the population working as tradesmen and technicians was almost four times lower than average across Australia.[1]
 The median monthly housing loan repayments in Yarralumla in 2016 were $2,700, compared to the ACT-wide figure of $2,058 and a federal average of $1,755. At $450, the weekly rent was more than 50% higher than the national average.[1] Yarralumla's median house price was $1m in 2011 versus $530,000 for the whole of the ACT in 2013.[17][18] The rate of home ownership in the suburb was 46.9%—much higher than the national average of 31.0%. 25.4% of the households rented.[1] Accommodation was mostly separate houses (64.2%),[1] although the number of residences in the suburb has been increasing through conversion of blocks to dual occupancy and other medium-density-type developments. Despite this, only 5.9% of residences were apartments or units, slightly less than half of the national average.[1]
 The population of Yarralumla in 2016 was predominantly Australian-born, with some 69.3% of its residents being born in Australia. The second most prevalent birthplace was England at 6.0%, followed by New Zealand and United States with 1.6 and 1.5% respectively.[1] The suburb was more oriented towards the Anglo-Celtic majority than the rest of Australia; English was spoken at home by 80.3% of the population, compared to the national average of 72.7%. Every other language was spoken by less than 2% of the population.[1] Italian, Mandarin, Greek, German and French were all spoken by at least 0.8% of the population.[1] The most popular religious affiliations in descending order were no religion, Roman Catholic, Anglican, Uniting and Presbyterian and Reformed. The proportion of the population professing to having no religion was 34.6%, higher than the national average of 29.6%.[1]
 The Yarralumla local shopping centre is located on the corner of Bentham and Novar Streets. The centre contains a supermarket, dry-cleaners, chemist, gift shop as well as several restaurants and speciality shops.
 Weston Park is situated on a peninsula near the western end of Lake Burley Griffin.[2] The park includes swimming areas, children's play equipment and wading pools, and a miniature railway, and is a popular barbecue spot on weekends.[2][19] Weston Park forms part of a string of parks that line the southern shore of Lake Burley Griffin; other parks include Yarralumla Bay, Lennox Gardens (incorporating a Japanese garden named Canberra Nara Park) and Stirling Park.[2][4]
 Like most of Canberra, Yarralumla's only scheduled public transport is provided by ACTION buses. Route 57 provides a service from Yarralumla to Woden and City Interchanges along Novar Street, Schlich Street and Hopetoun Circuit.[20]
 Yarralumla's first school, the Catholic St Peter Chanel's Primary School, opened in 1956; it closed in the 1990s. Yarralumla Primary School, a public school, opened a year after St Peter Chanel's in 1957. Half of the original primary school is now used as a behavioural centre catering to problem students.[21] Preschools in Yarralumla include the Montessori preschool on Loftus Street. There is also a day care facility called Little Lodge on Macgillivray Street.[22]
 The Canberra Japanese Supplementary School Inc., a Japanese weekend educational programme, has its school office in the Japanese Embassy Consular Section in Yarralumla, while it holds its classes at Deakin High School in Deakin. It was established on 1 August 1988.[23]
 Yarralumla is located within the federal electorate of Canberra, which is currently represented by Alicia Payne in the House of Representatives. In the ACT Legislative Assembly, Yarralumla is part of the electorate of Kurrajong, which elects five members on the basis of proportional representation, currently two Labor, two Liberal and one Greens.[26] Polling place statistics are shown to the right for the Yarralumla polling place at Yarralumla Primary School in the 2019 federal[24] and 2016 ACT[25] elections.
 The Yarralumla Residents Association (YRA) is a registered organisation  formed to represent the views of Yarralumla residents and business. The group stands against government plans for urban consolidation, supporting open space and the conservation of low-density housing. The organisation has been very vocal in opposing government plans for further development of Yarralumla and is also vocal about any plans for the Yarralumla brickworks site.[27]
  
Yarralumla is notable among Canberra suburbs for its large number of landmarks and places of historical interest. The Governor-General's residence Government House, which shares the name Yarralumla, is located at the western end of the suburb in 53 hectares (130 acres) of parkland. It sits alongside Lake Burley Griffin, next to the Royal Canberra Golf Club and Scrivener Dam. The house was built in 1891 as the headquarters for the Yarralumla property.[2][8] Also located alongside Scrivener Dam is the National Zoo & Aquarium.[2] The nearby Yarralumla woolshed is available for event hire, often playing host to parties and bush dances.[28] The land surrounding the woolshed has been developed as an equestrian park, including areas for showjumping, eventing and endurance riding.[2]  The Yarralumla brickworks are notable as the first industrial manufacturing facility in the ACT. The brickworks were closed temporarily several times due to the Great Depression and both World Wars. Proposals to modernise the brickworks were rejected by the National Capital Development Commission in the early 1970s and they closed permanently in 1976. Presently the site is closed to the public and is in a state of disrepair. The unfenced parkland around the brickworks is a popular recreation area for Yarralumla residents.[6][10] Residential development is proposed for the 42-hectare site, initially including 1,600 new dwellings, although this is now likely to be significantly reduced.[29]
 The eastern end of Yarralumla is home to many of the diplomatic missions in Canberra,[2] many of which are built in a traditional style reflecting that of their respective home countries. Examples of regionally styled chanceries include the embassies of Saudi Arabia and Thailand,[30][31] and the High Commissions of India and Papua New Guinea.[32][33] The United States embassy was the first embassy built in Canberra, with the foundation stone laid on the Fourth of July, 1942. The embassy is an impressive compound of buildings built in a Georgian style, inspired by several buildings designed by Christopher Wren for Virginia at the beginning of the 18th century.[34] The French embassy includes the French-Australian War Memorial opened in 1961, which has a sculpture by André Bizette-Lindet called Winged Victory.[35] Canberra tourist drive six takes tourists on a car-based tour past many of Canberra's embassies including those located in Yarralumla. It zig-zags through the eastern side of Yarralumla past many of the missions.[2][4]
 Also located in the eastern end of the suburb are Lennox Gardens, the Yarralumla Yacht Club, the Albert Hall and the Hotel Canberra.[4] The Hotel Canberra opened in 1924 to accommodate politicians when Parliament was in session. The hotel was closed in 1974 and the buildings served as an annexe for Parliament House between 1976 and 1984. The Hyatt Hotel Group re-opened the hotel in 1987.[36]
 Coordinates: 35°18′18″S 149°05′53″E﻿ / ﻿35.305092°S 149.098098°E﻿ / -35.305092; 149.098098
 1 Geography 2 History

2.1 Settlement
2.2 Development
2.3 After World War II

 2.1 Settlement 2.2 Development 2.3 After World War II 3 Demographics 4 Suburb amenities 5 Education 6 Politics 7 Notable places 8 See also 9 Footnotes 10 Further reading 11 External links History of Canberra List of diplomatic missions in Australia ^ a b c d e f g h i j k l m n Australian Bureau of Statistics (27 June 2017). "Yarralumla (State Suburb)". 2016 Census QuickStats. Retrieved 28 June 2017..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} 
 ^ a b c d e f g h i j k l m n o p q r s UBD Canberra, pp. 58–59.
 ^ UBD Canberra, pp. 1–100.
 ^ a b c d e UBD Canberra, p. 3.
 ^ a b "Suburb Name search results". ACT Planning & Land Authority. Retrieved 5 January 2014.
 ^ a b c "Yarralumla Brickworks Entry 2004" (PDF). Environment ACT Heritage Register. Archived from the original (PDF) on 26 July 2008. Retrieved 5 January 2014.
 ^ "Parish of Yarrolumla". New South Wales. Department of Lands (National Library of Australia). 2004. Retrieved 5 January 2014.
 ^ a b c "Official Residence – Government House". Governor-General's Office. Archived from the original on 19 July 2008. Retrieved 5 January 2014.
 ^ a b "1928 Electoral Roll for Westridge". transcribed by Ann Gugler. Archived from the original on 21 August 2008. Retrieved 5 October 2008.
 ^ a b "Yarralumla Brickworks Planning Review" (PDF). ACT Planning and Land Authority (March 2005). Retrieved 4 October 2008.
 ^ "Charles Weston and the Greening of Canberra". National Capital Authority, Dr John Gray. Archived from the original on 20 July 2008. Retrieved 4 October 2008.
 ^ "Yarralumla Nursery Description". National Trust of Australia. Archived from the original on 29 October 2004. Retrieved 1 November 2004.
 ^ a b c d Gugler, Ann (1997). Westlake One of the Vanished Suburbs of Canberra. Canberra: Ann Gugler. ISBN 0-646-30075-X.
 ^ "Stirling Park and Scrivener's Hut". National Capital Authority. Retrieved 5 January 2014.
 ^ "Official Committee Hansard – Consideration of Budget Estimates" (PDF). Australian Senate – Employment, Workplace Relations and Education Legislation Committee (6 June 2002). Archived from the original (PDF) on 18 February 2006. Retrieved 8 November 2005.
 ^ "Yarralumla Bay". Australian Capital Territory Department of Territory and Municipal Services. Archived from the original on 5 January 2014. Retrieved 5 January 2013.
 ^ "Suburb Information". Allhomes. Archived from the original on 4 January 2014. Retrieved 5 January 2014.
 ^ "ACT Real Estate Market Trends". Allhomes. Retrieved 5 January 2014.
 ^ David Curry (22 March 2009). "Weston Park tourist train's future derailed". The Canberra Times. Archived from the original on 28 March 2009. Retrieved 23 April 2010.
 ^ Route 57 Transport Canberra
 ^ "Yarralumla Primary School webpage". ACT Department of Education. Retrieved 17 November 2004.
 ^ "Yarralumla Campus". Only About Children. Archived from the original on 6 January 2014. Retrieved 5 January 2014.
 ^ "学校概要" (Archive). Canberra Japanese Supplementary School Inc. Retrieved on 7 April 2015. "借用校 Alfred Deakin High School (Denison St., Deakin, 2600, ACT) ディーキンハイスクール（写真）校舎・2Fにて授業を実施" and "日本大使館領事部：112 Empire Circuit, YARRALUMLA, ACT 2600, AUSTRALIA"
 ^ a b "Federal Election 2019 - Yarralumla Polling Place". Australian Electoral Commission. 18 May 2019. Retrieved 21 June 2019.
 ^ a b "2016 Results for Kurrajong Candidates at Yarralumla Polling Place". ACT Electoral Commission. 13 November 2016. Archived from the original on 14 November 2016. Retrieved 13 November 2016.
 ^ "Current members". ACT Legislative Assembly. Retrieved 13 November 2016.
 ^ "YRA website". Yarralumla Residents Association. Archived from the original on 17 March 2012. Retrieved 2 November 2005.
 ^ "ACT Heritage Library Image Library: Image: Exterior of the Yarralumla Woolshed". Images.act.gov.au. Retrieved 23 April 2010.
 ^ "Yarralumla brickworks plans set to be slashed". Domain, Fairfax Media. 15 November 2014. Retrieved 21 December 2014.
 ^ "Royal Embassy of Saudi Arabia Canberra". Retrieved 5 January 2014.
 ^ "The Royal Thai Embassy – Canberra". Canberra.thaiembassy.org. Retrieved 23 April 2010.
 ^ "High Commission of India". Hcindia-au.org. Archived from the original on 17 April 2010. Retrieved 23 April 2010.
 ^ "PNG High Commission". Pngcanberra.org. Retrieved 23 April 2010.
 ^ "History of the US Embassy". U.S. Department of State. Archived from the original on 30 December 2005. Retrieved 4 November 2005.
 ^ "French-Australian War Memorial". Embassy of France in Canberra. Retrieved 1 February 2014.
 ^ "MS 7302 – Records of Hotel Canberra". National Library of Australia (13 November 2003). Retrieved 22 November 2004.
 UBD Canberra. North Ryde, New South Wales: Universal Publishers. 2007. ISBN 0-7319-1882-7. St. Peter Chanel's School, St. Peter Chanel's School, Yarralumla, A.C.T.: Silver Jubilee 1956–1981. Yarralumla, A.C.T., 1981. ISBN 0-9593244-0-2. Coulthard-Clark, C.D., Gables, Ghosts & Governors-General: the Story of the Historic House at Yarralumla, Allen & Unwin, North Sydney, 1988. ISBN 0-04-320232-2. Wilson, Gwendoline, Murray of Yarralumla, Oxford University Press, Melbourne, 1968, ISBN 0-9587049-5-3. Royal Canberra Golf Club: the First Seventy-five years, 1926–2001, Canberra, A.C.T., 2001, published by the Royal Canberra Golf Club. ISBN 0-9596415-1-3, ISBN 0-9596415-2-1. Gugler, Ann, Builders of Canberra 1909–1929, Canberra, A.C.T., 1994. ISBN 978-0-646-15387-2. Gugler, Ann, Westlake: One of the Vanished 'suburbs' of Canberra, Canberra, A.C.T., 1997. ISBN 978-0-646-30075-7. Gugler, Ann, True tales from Canberra's vanished suburbs of Westlake, Westridge and Acton, Canberra, GPN Publications, 1999. ISBN 978-0-646-35950-2. Proctor, Peter, Biographical Register of Canberra and Queanbeyan, Canberra, The Heraldry & Genealogical Society of Canberra Inc, 2001. ISBN 978-1-876393-10-6. Yarralumla Residents Association – Yarralumla Page v t e Barton Capital Hill Deakin Forrest Fyshwick Griffith Kingston Narrabundah Parkes Pialligo Red Hill Yarralumla Suburbs of Canberra Diplomatic districts Australian Statistical Geography Standard 2016 ID same as Wikidata Use dmy dates from October 2015 Use Australian English from August 2011 All Wikipedia articles written in Australian English Featured articles Articles with short description Articles containing potentially dated statements from 2011 All articles containing potentially dated statements Commons category link is on Wikidata Coordinates on Wikidata Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Cebuano Deutsch فارسی Français Italiano Magyar  This page was last edited on 18 September 2019, at 23:30 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Suburbs around Yarralumla: Yarralumla a b c d e f g h i j k l m n a b c d e f g h i j k l m n o p q r s ^ a b c d e a b a b c ^ a b c a b a b ^ ^ a b c d ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ Yarralumla, Australian Capital Territory Kingston Manuka Belconnen Booth Canberra Central (including North and South Canberra) Coree Cotter River Gungahlin Hall Jerrabomberra Kowen Majura Molonglo Valley Mount Clear Paddys River Rendezvous Creek Stromlo Tennent Tuggeranong Weston Creek Woden Valley Suburbs of Canberra List of Canberra suburbs Group Centres
Kingston
Manuka Districts .mw-parser-output .nobold{font-weight:normal}in Canberra
Belconnen
Booth
Canberra Central (including North and South Canberra)
Coree
Cotter River
Gungahlin
Hall
Jerrabomberra
Kowen
Majura
Molonglo Valley
Mount Clear
Paddys River
Rendezvous Creek
Stromlo
Tennent
Tuggeranong
Weston Creek
Woden Valley See also
Suburbs of Canberra
List of Canberra suburbs  2,890 (2016 census)[1] 328.0/km2 (849.6/sq mi) 1922 20 September 1928 2600 8.81 km2 (3.4 sq mi) South Canberra Kurrajong Canberra 
Suburbs around Yarralumla:
Black Mountain
Acton
City

Stromlo Forest
Yarralumla
Parkes

Curtin
Deakin
Capital Hill

 Suburbs around Yarralumla: Black Mountain Acton City Stromlo Forest Yarralumla Parkes Curtin Deakin Capital Hill 
 


2019 Federal Election[24]


 

Labor

35.90%


 

Greens

16.76%


 

Liberal

39.06%


 

Independent

5.32%


2016 ACT Election[25]


 

Liberal

42.2%


 

Labor

29.4%


 

Independent

13.9%


 

Greens

10.6%


 

Canberra Community Voters

1.7%

  
 Labor
 35.90%
  
 Greens
 16.76%
  
 Liberal
 39.06%
  
 Independent
 5.32%
  
 Liberal
 42.2%
  
 Labor
 29.4%
  
 Independent
 13.9%
  
 Greens
 10.6%
  
 Canberra Community Voters
 1.7%
  Belgium
  Brazil
  Canada
  China
  Egypt
  Estonia
  Finland
  France
  Germany
  Greece
  India
  Indonesia
  Ireland
  Israel
  Japan
  South Korea
  Malaysia
  Mexico
  Myanmar
  Netherlands
  New Zealand
  Nigeria
  Norway
  Pakistan
  Papua New Guinea
  Philippines
  Poland
  Saudi Arabia
  Singapore
  South Africa
  Spain
  Sri Lanka
  Sweden
  Thailand
  Turkey
  United Kingdom
  United States
  Wikimedia Commons has media related to Yarralumla, Australian Capital Territory. 
Barton
Capital Hill
Deakin
Forrest
Fyshwick
Griffith
Kingston
Narrabundah
Parkes
Pialligo
Red Hill
Yarralumla
Group Centres
Kingston
Manuka
 
Districts .mw-parser-output .nobold{font-weight:normal}in Canberra
Belconnen
Booth
Canberra Central (including North and South Canberra)
Coree
Cotter River
Gungahlin
Hall
Jerrabomberra
Kowen
Majura
Molonglo Valley
Mount Clear
Paddys River
Rendezvous Creek
Stromlo
Tennent
Tuggeranong
Weston Creek
Woden Valley
See also
Suburbs of Canberra
List of Canberra suburbs
 