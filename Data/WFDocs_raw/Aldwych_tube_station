
 Aldwych is a closed station on the London Underground, located in the City of Westminster in Central London. It was opened in 1907 with the name Strand, after the street on which it is located, and was the terminus of the short Piccadilly line branch from Holborn that was a relic of the merger of two railway schemes. The station building is close to the Strand's junction with Surrey Street, near Aldwych. During its lifetime, the branch was the subject of a number of unrealised extension proposals that would have seen the tunnels through the station extended southwards, usually to Waterloo.
 Served mostly by a shuttle train and having low passenger numbers, the station and branch were considered for closure several times. Service was offered only during weekday peak hours from 1962 and discontinued in 1994, when the cost of replacing the lifts was considered too high for the income generated.
 Disused parts of the station and the running tunnels were used during both world wars to shelter artworks from London's public galleries and museums from bombing. The station has long been popular as a filming location and has appeared as itself and as other London Underground stations in a number of films. In recognition of its historical significance, the station is a Grade II listed building.
 The Great Northern and Strand Railway (GN&SR) first proposed a station in the Strand area in a private bill presented to Parliament in November 1898.[3] The station was to be the southern terminus of an underground railway line planned to run from Wood Green station (now Alexandra Palace) via Finsbury Park and King's Cross and was originally to be located at the corner of Stanhope Street and Holles Street, north of the Strand. When the two streets were scheduled for demolition as part of the London County Council's plans for the construction of Kingsway and Aldwych, the GN&SR moved the location to the junction of the two new roads.[4] Royal Assent to the bill was given and the Great Northern and Strand Railway Act 1899 was enacted on 1 August.[5]
 In September 1901, the GN&SR was taken over by the Brompton and Piccadilly Circus Railway (B&PCR), which planned to build an underground line from South Kensington to Piccadilly Circus via Knightsbridge. Both were under the control of Charles Yerkes through his Metropolitan District Electric Traction Company and, in June 1902, were transferred to Yerkes' new holding company, the Underground Electric Railways Company of London (UERL).[6] Neither of the railways had carried out any construction, but the UERL obtained permission for new tunnels between Piccadilly Circus and Holborn to connect the two routes. The companies were formally merged as the Great Northern, Piccadilly and Brompton Railway (GNP&BR) following parliamentary approval in November 1902.[7][8][9] Prior to confirmation of the merger, the GN&SR had sought permission to extend its line southwards from the future junction of Kingsway and Aldwych, under Norfolk Street to a new interchange under the Metropolitan District Railway's station at Temple. The extension was rejected following objections from the Duke of Norfolk under whose land the last part of the proposed tunnels would have run.[10]
 In 1903, the GNP&BR sought permission for a branch from Piccadilly Circus to run under Leicester Square, Strand, and Fleet Street and into the City of London. The branch would have passed and interchanged with the already approved Strand station,[11] allowing travel on the GNP&BR from Strand in three directions. The deliberations of a Royal Commission on traffic in London prevented parliamentary consideration of the proposal, which was withdrawn.[12]
 In 1905, with the Royal Commission's report about to be published, the GNP&BR returned to Parliament with two bills for consideration. The first bill revived the 1903 proposal for a branch from Piccadilly Circus to the City of London, passing and interchanging with Strand station. The second proposed an extension and relocation of Strand station to the junction of Strand and Surrey Street. From there the line was to continue as a single tunnel under the River Thames to Waterloo. The first bill was again delayed and withdrawn. Of the second, only the relocation of Strand station was permitted.[13]
 The linking of the GN&SR and B&PCR routes meant that the section of the GN&SR south of Holborn became a branch from the main route. The UERL began constructing the main route in July 1902. Progress was rapid, so that it was largely complete by the Autumn of 1906.[14] Construction of the Holborn to Strand section was delayed while the London County Council constructed Kingsway and the tramway subway running beneath it and while the UERL decided how the junction between the main route and the branch would be arranged at Holborn.[15][note 1]
 Strand station was built on the site of the Royal Strand Theatre, which had closed on 13 May 1905 and been demolished. Construction of the station began on 21 October 1905,[17] to a design by the UERL's architect Leslie Green in the UERL house style of a two-storey steel-framed building faced with red glazed terracotta blocks, with wide semi-circular windows on the upper floor.[18] The station building is L-shaped, with two façades separated by the building on the corner of Strand and Surrey Street. The Strand façade is narrow with a single semi-circular window above the entrance. The façade in Surrey Street is wider with a separate entrance and exit and a shop unit. In anticipation of a revival of the extension to Waterloo and the City route, the station was built with three circular lift shafts able to accommodate six trapezium-shaped lifts. Only one of the shafts was fitted out, with two lifts.[19] The other two shafts rose from the lower concourse to the basement of the station, but could have been extended upwards into the space of the shop unit when required. A fourth smaller-diameter shaft accommodated an emergency spiral stair.[20]
 The platforms are 92 feet 6 inches (28.19 m) below street level and are 250 feet (76 m) long;[17] shorter than the GNP&BR's standard length of 350 feet (110 m).[21] As with other UERL stations, the platform walls were tiled with distinctive patterns, in this case cream and dark green. Only parts of the platform walls were decorated because it was planned to operate the branch with short trains.[17] Owing to the reduced lift provision, a second route between the platforms and lifts was never brought into use and was left in an unfinished condition without tiling.[22]
 The GNP&BR's main route opened on 15 December 1906, but the Strand branch was not opened until 30 November 1907.[23] Initially, shuttle trains operated to Holborn from the eastern platform into the through platform at Holborn. At peak times, an additional train operated alternately in the branch's western tunnel into the bay platform at Holborn. During the first year of operation, a train for theatregoers operated late on Monday to Saturday evenings from Strand through Holborn and northbound to Finsbury Park; this was discontinued in October 1908.[17]
 In March 1908, the off-peak shuttle service began to use the western platform at Strand and the through platform at Holborn, crossing between the two branch tunnels south of Holborn. Low usage led to the withdrawal of the second peak-hour shuttle and the eastern tunnel was taken out of use in 1914.[24][25] On 9 May 1915, three of the Underground stations in the area were renamed and Strand station became Aldwych.[23][note 2] Sunday services ended in April 1917 and, in August of the same year, the eastern tunnel and platform at Aldwych and the bay platform at Holborn were formally closed.[26] A German bombing campaign in September 1917 led to the disused platform being used as storage for around 300 paintings from the National Gallery from then until December 1918.[27]
 In October 1922, the ticket office was replaced by a facility in the lifts.[26] Passenger numbers remained low: when the station was one of a number on the network considered for closure in 1929, its annual usage was 1,069,650 and takings were £4,500.[28][note 3] The branch was again considered for closure in 1933, but remained open.[26]
 Wartime efficiency measures led to the branch being closed temporarily on 22 September 1940, shortly after the start of The Blitz, and it was partly fitted out by the City of Westminster as an air-raid shelter. The tunnels between Aldwych and Holborn were used to store items from the British Museum, including the Elgin Marbles. The branch reopened on 1 July 1946, but patronage did not increase.[29] In 1958, the station was one of three that London Transport announced would be closed. Again it survived, but the service was reduced in June 1958 to run during Monday to Friday peak hours and Saturday morning and early afternoons.[30][note 4] The Saturday service was withdrawn in June 1962.[30]
 After operating only during peak hours for more than 30 years, the closure announcement came on 4 January 1993. The original 1907 lifts required replacement at a cost of £3 million. This was not considered justifiable in relation to the passenger numbers using the station and it was losing London Regional Transport £150,000 per year. The Secretary of State for Transport granted permission on 1 September 1994 to close the station and the branch closed on 30 September.[31][note 5] Recognising the station's historical significance as a mostly unaltered station from the early 20th century, the station was given Grade II listed building status in 2011.[2] Office floors above the station are used by the Classics department of King's College London.[32]
 Although the Piccadilly Circus to City of London branch proposal of 1905 was never revisited after its withdrawal, the early plan to extend the branch south to Waterloo was revived a number of times during the station's life. The extension was considered in 1919 and 1948, but no progress towards constructing the link was made.[29]
 In the years after the Second World War, a series of preliminary plans for relieving congestion on the London Underground had considered various east-west routes through the Aldwych area, although other priorities meant that these were never proceeded with. In March 1965, a British Rail and London Transport joint planning committee published "A Railway Plan for London", which proposed a new tube railway, the Fleet line (later renamed the Jubilee line), to join the Bakerloo line at Baker Street then run via Bond Street, Green Park, Charing Cross, Aldwych and into the City of London via Ludgate Circus, Cannon Street and Fenchurch Street before heading into south-east London. An interchange was proposed at Aldwych and a second recommendation of the report was the revival of the link from Aldwych to Waterloo.[33][34] London Transport had already sought parliamentary approval to construct tunnels from Aldwych to Waterloo in November 1964,[35] and in August 1965, parliamentary powers were granted. Detailed planning took place, although public spending cuts led to postponement of the scheme in 1967 before tenders were invited.[30]
 Planning of the Fleet line continued and parliamentary approval was given in July 1969 for the first phase of the line, from Baker Street to Charing Cross.[36] Tunnelling began on the £35 million route in February 1972 and the Jubilee line opened north from Charing Cross in May 1979.[37] The tunnels of the approved section continued east of Charing Cross under Strand almost as far as Aldwych station, but no work at Aldwych was undertaken and they were used only as sidings.[38] Funding for the second phase of the work was delayed throughout the 1970s whilst the route beyond Charing Cross was reviewed to consider options for serving anticipated development in the London Docklands area. By 1979, the cost was estimated as £325 million, a six-fold increase from the £51 million estimated in 1970.[39] A further review of alternatives for the Jubilee line was carried out in 1980, which led to a change of priorities and the postponement of any further effort on the line.[40] When the extension was eventually constructed in the late 1990s it took a different route, south of the River Thames via Westminster, Waterloo and London Bridge to provide a rapid link to Canary Wharf, leaving the tunnels between Green Park and Aldwych redundant.[41]
 In July 2005, Ove Arup & Partners produced a report, DLR Horizon 2020 Study, for the Docklands Light Railway (DLR) examining "pragmatic development schemes" to expand and improve the DLR network between 2012 and 2020. One of the proposals was an extension of the DLR from Bank to Charing Cross via City Thameslink and Aldwych. The disused Jubilee line tunnels would be enlarged to accommodate the larger DLR trains and Aldwych station would form the basis for a new station on the line, although requiring considerable reconstruction to accommodate escalators. The estimated cost in 2005 was £232 million for the infrastructure works and the scheme was described as "strongly beneficial" as it was expected to attract passengers from the London Underground's existing east-west routes and from local buses and reduce overcrowding at Bank station. The business case assessment was that the proposal offered high value, although similar values were calculated for other extension proposals from Bank. Further detailed studies were proposed.[42]
 In 2015, a scheme was proposed by the design firm Gensler to convert disused London Underground tunnels into subterranean rail trails, enabling the disused branches of the Piccadilly line and Jubilee line to be used as cycle paths. The scheme, which would involve re-opening Aldwych station as an access point for cyclists, has not been officially approved.[43]
 Because it was a self-contained section of the London Underground that was closed outside weekday peak hours, Aldwych station and the branch line from Holborn were popular locations for filming scenes set on the Tube even before their closure. Since the branch's closure in 1994, its use in film productions has continued, with the station appearing as itself and, with appropriate signage, as other stations on the network.[30] The track and infrastructure are maintained in operational condition, and a train of ex-Northern line 1972 tube stock is permanently stabled on the branch. This train can be driven up and down the branch for filming. The physical connection with the Piccadilly line northbound tracks remains, but requires manual operation.[44]
 Films and television productions that have been shot at Aldwych include:
 The pre-war operation of the station features in a pivotal scene in Geoffrey Household's novel Rogue Male, when the pursuit of the protagonist by an enemy agent sees them repeatedly using the shuttle service on the branch line. A chase through Aldwych station ends with the agent's death by electrocution on the track.[53] A much modified and expanded version of the station appears as a level in the video game Tomb Raider III.[54] The music video for The Prodigy's song "Firestarter" was filmed in the disused eastern tunnel and one of the unused lift shafts.[55] The station was the subject of an episode of Most Haunted in 2002.[56]
  London transport portal
 
 2 (1907–1917) 1 (1917–1994) 1 History

1.1 Planning
1.2 Construction
1.3 Operation
1.4 Proposals for extension and new connections

 1.1 Planning 1.2 Construction 1.3 Operation 1.4 Proposals for extension and new connections 2 Use in media 3 See also 4 Notes and references

4.1 Notes
4.2 References
4.3 Bibliography

 4.1 Notes 4.2 References 4.3 Bibliography 5 External links The Gentle Gunman (1952)[30] Battle of Britain (1969)[45] Death Line (1972)[30] Take It or Leave It (1981)[46] Superman IV: The Quest for Peace (1986)[30] The Krays (1990)[47] Patriot Games (1992)[47] Creep (2004)[45] V for Vendetta (2006)[45] The Good Shepherd (2006)[45] Atonement (2007)[45] 28 Weeks Later (2007)[45] The Bank Job (2008)[48] The Edge of Love (2008)[45] Mr Selfridge (2013)[49] Fast & Furious 6 (2013)[50] Sherlock (2014)[51] Darkest Hour (2017)[52] List of former and unopened London Underground stations ^ When originally planned, Holborn station was to have just two platforms, shared by trains on the main route and by the shuttle service from Aldwych. The junctions between the routes would have been south of the station. The interference that shuttle trains would have caused to the main route led to a redesign so that two northbound platforms were provided, one for the main line and one for the branch line, with a single southbound platform. The junctions between the two northbound tunnels would have been north of the platforms. When powers were sought to build the junction in 1905, the layout was changed again so that four platforms were to be provided. The southbound tunnel of the main route no longer connected to the branch, which was provided with an additional platform in a dead-end tunnel accessed from a cross tunnel from the northbound branch tunnel. As built, the dead-end platform tunnel was adjacent to the northbound main line platform, for ease of access.[16]
 ^ The Strand name was transferred to Charing Cross (Strand) station, now the Northern line platforms of Charing Cross. The other station renamed was Charing Cross (Embankment), which became Charing Cross, and in 1979, Embankment.[23]
 ^ The other stations considered for closure were Down Street (closed 1932), York Road (closed 1932), Brompton Road (closed 1934), Regent's Park, Mornington Crescent, Hyde Park Corner, Gillespie Road (now Arsenal), Gloucester Road and Covent Garden.[28]
 ^ Mornington Crescent and South Acton were the other stations that were to be closed. Only the latter did, on 28 February 1959.[23][30]
 ^ The Epping to Ongar section of the Central line also closed on 30 September.[23]
 ^ a b c d e "Multi-year station entry-and-exit figures" (XLSX). London Underground station passenger usage data. Transport for London. January 2018. Retrieved 22 July 2018..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b Historic England. "Aldwych Underground Station (1401034)". National Heritage List for England. Retrieved 27 July 2011.
 ^ "No. 27025". The London Gazette. 22 November 1898. pp. 7040–7043.
 ^ Badsey-Ellis 2005, p. 77.
 ^ "No. 27105". The London Gazette. 4 August 1899. pp. 4833–4834.
 ^ Badsey-Ellis 2005, p. 118.
 ^ Badsey-Ellis 2005, pp. 152–53.
 ^ "No. 27464". The London Gazette. 12 August 1902. pp. 5247–5248.
 ^ "No. 27497". The London Gazette. 21 November 1902. p. 7533.
 ^ Badsey-Ellis 2005, p. 138.
 ^ Badsey-Ellis 2005, p. 215.
 ^ Badsey-Ellis 2005, p. 222.
 ^ Badsey-Ellis 2005, pp. 241–42.
 ^ Wolmar 2005, p. 181.
 ^ Badsey-Ellis 2005, p. 239.
 ^ Badsey-Ellis 2005, pp. 239–41.
 ^ a b c d Connor 2001, p. 94.
 ^ Wolmar 2005, p. 175.
 ^ Badsey-Ellis 2005, pp. 242–43.
 ^ Architect's drawing of station – Connor 2001, p. 95.
 ^ Badsey-Ellis 2005, p. 238.
 ^ Badsey-Ellis 2005, p. 243.
 ^ a b c d e Rose 1999.
 ^ Connor 2001, p. 95.
 ^ Aldwych Underground Station Open Day. London Transport Museum/Transport for London. 2011. p. 5.
 ^ a b c Connor 2001, p. 98.
 ^ Wolmar 2005, pp. 212 & 214.
 ^ a b Connor 2001, p. 31.
 ^ a b Connor 2001, pp. 98–99.
 ^ a b c d e f g h Connor 2001, p. 99.
 ^ Connor 2001, pp. 100–101.
 ^ "Classics on the Strand". www.kcl.ac.uk. King's College London. Retrieved 11 May 2019.
 ^ Horne 2000, pp. 31–33.
 ^ British Railways Board/London Transport Board (March 1965). A Railway Plan for London (PDF). p. 23.
 ^ "Parliamentary Notices". The Times (56185): 2. 3 December 1964. Archived from the original on 27 September 2012.
 ^ Horne 2000, p. 34.
 ^ Horne 2000, pp. 35, 38 & 45.
 ^ Horne 2000, pp. 35, 38 & 42.
 ^ Horne 2000, pp. 35 & 52.
 ^ Horne 2000, p. 53.
 ^ Horne 2000, p. 57.
 ^ Ove Arup & Partners (July 2005). DLR Horizon 2020 Study (PDF). pp. 34–38 & 66. Archived (PDF) from the original on 8 November 2011. As of August 2011 no decision has been taken about the future of Aldwych station as an element of an expanded DLR.
 ^ O'Sullivan, Feargus. "Bike paths in abandoned tube tunnels: is the London Underline serious?". The Guardian. Retrieved 7 February 2015.
 ^ "Filming at Aldwych July 2002". Cravens Heritage Trains. 6 December 2006. Archived from the original on 30 December 2005. Retrieved 26 June 2008.
 ^ a b c d e f g "London Underground Film Office". Transport for London. Archived from the original on 3 August 2010.
 ^ UnionSquareMusic (31 December 2013), Madness - Take It Or Leave It - full movie, retrieved 11 April 2019
 ^ a b Badsey-Ellis & Horne 2009, p. 104.
 ^ "The London Underground in Films & TV". www.nickcooper.org.uk. Retrieved 14 January 2019.
 ^ "Mr Selfridge: Production Notes" (PDF). ITV Studios. Archived (PDF) from the original on 11 February 2014.
 ^ "Fast And Furious 6". Movie-locations.com. Retrieved 11 May 2019.
 ^ "The allure of abandoned Tube stations". BBC News. 2 January 2014. Archived from the original on 2 January 2014.
 ^ "Aldwych - London Transport Museum". www.ltmuseum.co.uk. Retrieved 13 October 2019.
 ^ Household 1977, pp. 62–63.
 ^ "Tomb Raider III – Aldwych in 4:18". Internet Archive. Retrieved 21 August 2011.
 ^ "Site Records: Aldwych — Holborn branch". Subterranea Britannica. Archived from the original on 27 January 2003.
 ^ "Most Haunted – Season 1 – Episode 15". TV.com. Retrieved 28 February 2015.
 Badsey-Ellis, Antony (2005). London's Lost Tube Schemes. Capital Transport. ISBN 1-85414-293-3. Badsey-Ellis, Antony; Horne, Mike (2009). The Aldwych Branch. Capital Transport. ISBN 978-1-85414-321-1. Connor, J.E. (2001) [1999]. London's Disused Underground Stations. Capital Transport. ISBN 1-85414-250-X. Horne, Mike (2000). The Jubilee Line. Capital Transport. ISBN 1-85414-220-8. Household, Geoffrey (1977) [1939]. Rogue Male. Penguin Books. ISBN 0-14-000695-8. Rose, Douglas (1999). The London Underground, A Diagrammatic History. Douglas Rose/Capital Transport. ISBN 1-85414-219-4. Wolmar, Christian (2005) [2004]. The Subterranean Railway: How the London Underground Was Built and How It Changed the City Forever. Atlantic Books. ISBN 1-84354-023-1. "Aldwych Station". Underground History. Archived from the original on 10 December 2005. "Aldwych". London's Abandoned Tube Stations. Archived from the original on 17 August 2008. "Aldwych-Holborn Branch". Disused stations. Subterranea Britannica. Archived from the original on 27 January 2003. London Transport Museum Photographic Archive
Strand station, mid-1907
Surrey Street elevation, mid-1907
Surrey Street elevation after vertical extension, 1958 Strand station, mid-1907 Surrey Street elevation, mid-1907 Surrey Street elevation after vertical extension, 1958 "An ENSA concert party entertains shelterers in Aldwych station". The Daily Telegraph. 3 May 2014. Retrieved 29 May 2014. Oliver, Luke (12 June 2010). The Secret Station – Aldwych. YouTube. Retrieved 29 June 2014. v t e Cockfosters Oakwood Southgate Arnos Grove Bounds Green Wood Green Turnpike Lane Manor House Finsbury Park  Arsenal Holloway Road Caledonian Road King's Cross St Pancras  Russell Square Holborn Covent Garden Leicester Square Piccadilly Circus Green Park Hyde Park Corner Knightsbridge South Kensington Gloucester Road Earl's Court Barons Court Hammersmith Turnham Green Acton Town South Ealing Northfields Boston Manor Osterley Hounslow East Hounslow Central Hounslow West Hatton Cross Heathrow Terminal 4  Heathrow Terminals 2 & 3  Heathrow Terminal 5  Ealing Common North Ealing Park Royal Alperton Sudbury Town Sudbury Hill South Harrow Rayners Lane Eastcote Ruislip Manor Ruislip Ickenham Hillingdon Uxbridge 1973 Stock Siemens Inspiro 1906 Stock 1938 Stock 1956 Stock 1959 Stock Standard Stock Brompton & Piccadilly Circus Railway Railway Great Northern, Piccadilly & Brompton Railway Great Northern & Strand Railway District Railway Underground Electric Railways Company of London Aldwych Brompton Road Down Street Osterley & Spring Grove York Road Angel Aldgate Alexandra Palace, Hornsey and Harringay Goldhawk Road and Acton Parsons Green Shepherd's Bush Waterloo London Underground Transport for London v t e Stanmore Canons Park Queensbury Kingsbury Wembley Park Neasden Dollis Hill Willesden Green Kilburn West Hampstead   Finchley Road Swiss Cottage St. John's Wood Baker Street Bond Street Green Park Westminster Waterloo  Southwark ( Waterloo East) London Bridge  Bermondsey Canada Water  Canary Wharf  North Greenwich Canning Town  West Ham  Stratford     1996 Stock Metropolitan line Bakerloo line Charing Cross Aldwych Ludgate Circus Cannon Street Fenchurch Street Surrey Docks New Cross Gate New Cross Lewisham St Katharine Docks Wapping Surrey Docks North Millwall North Greenwich Custom House Silvertown Woolwich Arsenal Beckton London Underground 1972 Stock 1983 Stock London Underground Night Tube Transport for London  London transport portal v t e Blake Hall British Museum North Weald Ongar Wood Lane Hounslow Town Mark Lane Osterley & Spring Grove Park Royal & Twyford Abbey South Acton St. Mary's (Whitechapel Road) Tower of London Shoreditch Charing Cross Brill Granborough Road Hammersmith (Grove Road) Lord's Marlborough Road Quainton Road St. Mary's (Whitechapel Road) Swiss Cottage Uxbridge Road Verney Junction Waddesdon Waddesdon Road Westcott Winslow Road Wood Lane Wood Siding Wotton City Road South Kentish Town King William Street Aldwych (pictured) Brompton Road Down Street Osterley & Spring Grove York Road Bushey Carpenders Park Hatch End Headstone Lane Watford High Street Watford Junction Castle Hill (Ealing Dean) Hanwell Hayes Langley Leigh-on-Sea Shoeburyness Slough Southend Central Southall West Drayton Windsor New Cross New Cross Gate Rotherhithe Shadwell Surrey Quays Wapping Aylesbury Great Missenden Stoke Mandeville Wendover Drayton Park Essex Road Extension to Camberwell Denham Emlyn Road Harefield Road Heathfield Terrace Paddenswick Road Rylett Road The Grove Cannon Hill Cheam Collingwood Road Elm Farm Elm Grove Merton Park Morden Sutton Sutton Common Aldwych Ludgate Circus Cannon Street Fenchurch Street Surrey Docks New Cross Gate New Cross Lewisham St Katharine Docks Wapping Surrey Docks North Millwall North Greenwich Custom House Silvertown Woolwich Arsenal Beckton Cassiobridge Clerkenwell Hampstead Watford Central Watford High Street Watford Junction Watford Vicarage Road Alexandra Palace Brockley Hill Bushey Heath Cranley Gardens Crouch End Elstree South Highgate Mill Hill (The Hale) Muswell Hill North End Stroud Green Aldwych branch extension to Waterloo Walthamstow Wood Street Lothbury Harringay Hornsey Wood Green Charing Cross & Waterloo Electric Railway City & Brixton Railway London Central Railway Edgware Road Tube schemes Waterloo & Whitehall Railway Disused London Underground stations Disused railway stations in the City of Westminster Former Great Northern, Piccadilly and Brompton Railway stations Railway stations opened in 1907 Railway stations closed in 1994 Former single platform tube stations Grade II listed buildings in the City of Westminster Grade II listed railway stations King's College London Aldwych Leslie Green railway stations Railway stations located underground in the United Kingdom CS1: Julian–Gregorian uncertainty EngvarB from October 2017 Use dmy dates from October 2017 Coordinates on Wikidata Commons category link is on Wikidata Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Deutsch Español Français Italiano Magyar Nederlands Polski Português Русский Suomi Svenska 中文  This page was last edited on 13 October 2019, at 14:46 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Aldwych Strand ^ ^ ^ ^ ^ a b c d e a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b c d ^ ^ ^ ^ ^ a b c d e ^ ^ a b c ^ a b a b a b c d e f g h ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b c d e f g ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ Aldwych tube station Former Abandoned Plans Main entrance to Aldwych station (formerly Strand station) AldwychLocation of Aldwych in Central London Aldwych City of Westminster Great Northern, Piccadilly and Brompton Railway 
2 (1907–1917)
1 (1917–1994)
 No Data[1] No Data[1] No Data[1] No Data[1] No Data[1] Opened Closed Reopened Closed II 1401034[2] 20 July 2011 51°30′44″N 0°06′57″W﻿ / ﻿51.51215°N 0.11595°W﻿ / 51.51215; -0.11595Coordinates: 51°30′44″N 0°06′57″W﻿ / ﻿51.51215°N 0.11595°W﻿ / 51.51215; -0.11595  London transport portal  Wikimedia Commons has media related to Aldwych tube station.  
 Former
  
 HolbornTerminus
  
 Piccadilly lineAldwych branch(1907–1994)
  
 Terminus
  
 Abandoned Plans
  
 Charing Crosstowards Stanmore  Jubilee linePhase 2 (never constructed)  Ludgate Circustowards New Cross Gate or Lewisham HolbornTerminus
  
 Piccadilly lineProposed extension to Waterloo(never constructed)
  
 WaterlooTerminus
 Mainroute
Cockfosters
Oakwood
Southgate
Arnos Grove
Bounds Green
Wood Green
Turnpike Lane
Manor House
Finsbury Park 
Arsenal
Holloway Road
Caledonian Road
King's Cross St Pancras 
Russell Square
Holborn
Covent Garden
Leicester Square
Piccadilly Circus
Green Park
Hyde Park Corner
Knightsbridge
South Kensington
Gloucester Road
Earl's Court
Barons Court
Hammersmith
Turnham Green
Acton Town
Heathrowbranch
South Ealing
Northfields
Boston Manor
Osterley
Hounslow East
Hounslow Central
Hounslow West
Hatton Cross
Heathrow Terminal 4 
Heathrow Terminals 2 & 3 
Heathrow Terminal 5 
Uxbridgebranch
Ealing Common
North Ealing
Park Royal
Alperton
Sudbury Town
Sudbury Hill
South Harrow
Rayners Lane
Eastcote
Ruislip Manor
Ruislip
Ickenham
Hillingdon
Uxbridge
 
Cockfosters
Oakwood
Southgate
Arnos Grove
Bounds Green
Wood Green
Turnpike Lane
Manor House
Finsbury Park 
Arsenal
Holloway Road
Caledonian Road
King's Cross St Pancras 
Russell Square
Holborn
Covent Garden
Leicester Square
Piccadilly Circus
Green Park
Hyde Park Corner
Knightsbridge
South Kensington
Gloucester Road
Earl's Court
Barons Court
Hammersmith
Turnham Green
Acton Town
 
South Ealing
Northfields
Boston Manor
Osterley
Hounslow East
Hounslow Central
Hounslow West
Hatton Cross
Heathrow Terminal 4 
Heathrow Terminals 2 & 3 
Heathrow Terminal 5 
 
Ealing Common
North Ealing
Park Royal
Alperton
Sudbury Town
Sudbury Hill
South Harrow
Rayners Lane
Eastcote
Ruislip Manor
Ruislip
Ickenham
Hillingdon
Uxbridge
 Current
1973 Stock
Future
Siemens Inspiro
Former
1906 Stock
1938 Stock
1956 Stock
1959 Stock
Standard Stock
 
1973 Stock
 
Siemens Inspiro
 
1906 Stock
1938 Stock
1956 Stock
1959 Stock
Standard Stock
 Former companies
Brompton & Piccadilly Circus Railway Railway
Great Northern, Piccadilly & Brompton Railway
Great Northern & Strand Railway
District Railway
Underground Electric Railways Company of London
Former stations
Aldwych
Brompton Road
Down Street
Osterley & Spring Grove
York Road
Rejected plans
Angel
Aldgate
Alexandra Palace, Hornsey and Harringay
Goldhawk Road and Acton
Parsons Green
Shepherd's Bush
Waterloo
 
Brompton & Piccadilly Circus Railway Railway
Great Northern, Piccadilly & Brompton Railway
Great Northern & Strand Railway
District Railway
Underground Electric Railways Company of London
 
Aldwych
Brompton Road
Down Street
Osterley & Spring Grove
York Road
 
Angel
Aldgate
Alexandra Palace, Hornsey and Harringay
Goldhawk Road and Acton
Parsons Green
Shepherd's Bush
Waterloo
 
London Underground
Transport for London
 London transport portal

 
Stanmore
Canons Park
Queensbury
Kingsbury
Wembley Park
Neasden
Dollis Hill
Willesden Green
Kilburn
West Hampstead  
Finchley Road
Swiss Cottage
St. John's Wood
Baker Street
Bond Street
Green Park
Westminster
Waterloo 
Southwark ( Waterloo East)
London Bridge 
Bermondsey
Canada Water 
Canary Wharf 
North Greenwich
Canning Town 
West Ham 
Stratford    
 
1996 Stock
 Former lines
Metropolitan line
Bakerloo line
Former stations
Charing Cross
Abandoned plansPhase 2 (1971)
Aldwych
Ludgate Circus
Cannon Street
Fenchurch Street
Phase 3 (1971/72)
Surrey Docks
New Cross Gate
New Cross
Lewisham
Phase 3 (1980)
St Katharine Docks
Wapping
Surrey Docks North
Millwall
North Greenwich
Custom House
Silvertown
Woolwich Arsenal
Beckton
Former rolling stock
London Underground 1972 Stock
1983 Stock
 
Metropolitan line
Bakerloo line
 
Charing Cross
 Phase 2 (1971)
Aldwych
Ludgate Circus
Cannon Street
Fenchurch Street
Phase 3 (1971/72)
Surrey Docks
New Cross Gate
New Cross
Lewisham
Phase 3 (1980)
St Katharine Docks
Wapping
Surrey Docks North
Millwall
North Greenwich
Custom House
Silvertown
Woolwich Arsenal
Beckton
 
Aldwych
Ludgate Circus
Cannon Street
Fenchurch Street
 
Surrey Docks
New Cross Gate
New Cross
Lewisham
 
St Katharine Docks
Wapping
Surrey Docks North
Millwall
North Greenwich
Custom House
Silvertown
Woolwich Arsenal
Beckton
 
London Underground 1972 Stock
1983 Stock
 
London Underground
Night Tube
Transport for London
 London transport portal
 Central
Blake Hall
British Museum
North Weald
Ongar
Wood Lane
District
Hounslow Town
Mark Lane
Osterley & Spring Grove
Park Royal & Twyford Abbey
South Acton
St. Mary's (Whitechapel Road)
Tower of London
East London
Shoreditch
Jubilee
Charing Cross
Metropolitan
Brill
Granborough Road
Hammersmith (Grove Road)
Lord's
Marlborough Road
Quainton Road
St. Mary's (Whitechapel Road)
Swiss Cottage
Uxbridge Road
Verney Junction
Waddesdon
Waddesdon Road
Westcott
Winslow Road
Wood Lane
Wood Siding
Wotton
Northern
City Road
South Kentish Town
King William Street
Piccadilly
Aldwych (pictured)
Brompton Road
Down Street
Osterley & Spring Grove
York Road
 
Blake Hall
British Museum
North Weald
Ongar
Wood Lane
 
Hounslow Town
Mark Lane
Osterley & Spring Grove
Park Royal & Twyford Abbey
South Acton
St. Mary's (Whitechapel Road)
Tower of London
 
Shoreditch
 
Charing Cross
 
Brill
Granborough Road
Hammersmith (Grove Road)
Lord's
Marlborough Road
Quainton Road
St. Mary's (Whitechapel Road)
Swiss Cottage
Uxbridge Road
Verney Junction
Waddesdon
Waddesdon Road
Westcott
Winslow Road
Wood Lane
Wood Siding
Wotton
 
City Road
South Kentish Town
King William Street
 
Aldwych (pictured)
Brompton Road
Down Street
Osterley & Spring Grove
York Road
  Bakerloo
Bushey
Carpenders Park
Hatch End
Headstone Lane
Watford High Street
Watford Junction
District
Castle Hill (Ealing Dean)
Hanwell
Hayes
Langley
Leigh-on-Sea
Shoeburyness
Slough
Southend Central
Southall
West Drayton
Windsor
East London
New Cross
New Cross Gate
Rotherhithe
Shadwell
Surrey Quays
Wapping
Metropolitan
Aylesbury
Great Missenden
Stoke Mandeville
Wendover
Northern
Drayton Park
Essex Road
 
Bushey
Carpenders Park
Hatch End
Headstone Lane
Watford High Street
Watford Junction
 
Castle Hill (Ealing Dean)
Hanwell
Hayes
Langley
Leigh-on-Sea
Shoeburyness
Slough
Southend Central
Southall
West Drayton
Windsor
 
New Cross
New Cross Gate
Rotherhithe
Shadwell
Surrey Quays
Wapping
 
Aylesbury
Great Missenden
Stoke Mandeville
Wendover
 
Drayton Park
Essex Road
 Bakerloo
Extension to Camberwell
Central
Denham
Emlyn Road
Harefield Road
Heathfield Terrace
Paddenswick Road
Rylett Road
The Grove
District
Cannon Hill
Cheam
Collingwood Road
Elm Farm
Elm Grove
Merton Park
Morden
Sutton
Sutton Common
Jubilee
Aldwych
Ludgate Circus
Cannon Street
Fenchurch Street
Surrey Docks
New Cross Gate
New Cross
Lewisham
St Katharine Docks
Wapping
Surrey Docks North
Millwall
North Greenwich
Custom House
Silvertown
Woolwich Arsenal
Beckton
Metropolitan
Cassiobridge
Clerkenwell
Hampstead
Watford Central
Watford High Street
Watford Junction
Watford Vicarage Road
Northern
Alexandra Palace
Brockley Hill
Bushey Heath
Cranley Gardens
Crouch End
Elstree South
Highgate
Mill Hill (The Hale)
Muswell Hill
North End
Stroud Green
Piccadilly
Aldwych branch extension to Waterloo
Victoria
Walthamstow Wood Street
Great Northern & City
Lothbury
Great Northern & Strand
Harringay
Hornsey
Wood Green
Other
Charing Cross & Waterloo Electric Railway
City & Brixton Railway
London Central Railway
Edgware Road Tube schemes
Waterloo & Whitehall Railway
 
Extension to Camberwell
 
Denham
Emlyn Road
Harefield Road
Heathfield Terrace
Paddenswick Road
Rylett Road
The Grove
 
Cannon Hill
Cheam
Collingwood Road
Elm Farm
Elm Grove
Merton Park
Morden
Sutton
Sutton Common
 
Aldwych
Ludgate Circus
Cannon Street
Fenchurch Street
Surrey Docks
New Cross Gate
New Cross
Lewisham
St Katharine Docks
Wapping
Surrey Docks North
Millwall
North Greenwich
Custom House
Silvertown
Woolwich Arsenal
Beckton
 
Cassiobridge
Clerkenwell
Hampstead
Watford Central
Watford High Street
Watford Junction
Watford Vicarage Road
 
Alexandra Palace
Brockley Hill
Bushey Heath
Cranley Gardens
Crouch End
Elstree South
Highgate
Mill Hill (The Hale)
Muswell Hill
North End
Stroud Green
 
Aldwych branch extension to Waterloo
 
Walthamstow Wood Street
 
Lothbury
 
Harringay
Hornsey
Wood Green
 
Charing Cross & Waterloo Electric Railway
City & Brixton Railway
London Central Railway
Edgware Road Tube schemes
Waterloo & Whitehall Railway
  London transport portal 