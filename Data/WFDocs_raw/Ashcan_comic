
 An ashcan comic is an American comic book originally created solely to establish trademarks on potential titles and not intended for sale. The practice was common in the 1930s and 1940s when the comic book industry was in its infancy, but was phased out after updates to US trademark law. The term was revived in the 1980s by Bob Burden, who applied it to prototypes of his self-published comic book. Since the 1990s, the term has been used to describe promotional materials produced in large print runs and made available for mass consumption. In the film and television industries, the term has been adopted for low-grade material created to preserve a claim to licensed property rights.
 The modern comic book was created in the 1930s, and rapidly grew in popularity.[1] In the competition to secure trademarks on titles intended to sound thrilling, publishers including All-American Publications and Fawcett Comics developed the ashcan edition,[2] which was the same size as regular comics and usually had a black and white cover.[2] Typically, cover art was recycled from previous publications with a new title pasted to it.[3] Interior artwork ranged from previously published material in full color[2][3] to unfinished pencils without word balloons.[4] Some ashcans were only covers with no interior pages.[5] Production quality on these works range from being hand-stapled with untrimmed pages to machine-stapled and machine trimmed.[5] Once the practice was established, DC Comics used ashcans more frequently than any other publisher.[5] Not all the titles secured through ashcan editions were actually used for regular publications.[6]
 The purpose of the ashcan editions was to fool the US Patent & Trademark Office into believing the book had actually been published.[7] Clerks at the office would accept the hastily produced material as legitimate, granting the submitting publisher a trademark to the title.[8][9] Since the ashcans had no other use, publishers printed as few as two copies; one was sent to the Trademark Office, the other was kept for their files.[10] Occasionally, publishers would send copies to distributors or wholesalers by registered mail to further establish publication dates,[3] but nearly all ashcan comic editions were limited to five copies or fewer.[2]
 At the time, garbage cans were commonly called "ash cans" because they were used to hold soot and ash from wood and coal heating systems.[4] The term was applied to these editions of comics because they had no value and were meant to be thrown away after being accepted by the Trademark Office.[4][7] Some spare copies were given to editors, employees, and visitors to keep as souvenirs.[5][7] Changes to the United States trademark law in 1946 allowed publishers to register a trademark with an intent to use instead of a finished product,[5] and the practice of creating and submitting ashcans was abandoned when publishers began to consider it an unnecessary effort lawyers used to justify a fee.[6] Because of their rarity, ashcans from this era are desired by collectors and often fetch a high price.[4]
 In 1984, Golden Age comic book collector and dealer Bob Burden created Flaming Carrot Comics, which was published by Aardvark-Vanaheim.[2][7][11] For each issue, Burden had magazine-sized prototype editions printed and shared them with friends and people who assisted with production.[2] Some were also sent to retailers as advance previews to generate interest in his comic.[7] Fewer than forty copies of each prototype were made, and Burden described them as ashcans.[2]
 In 1992, comic creator Rob Liefeld applied the term to two digest-sized prototype versions of Youngblood #1, but this ashcan was created for mass release. Instead of denoting the material as worthless, Liefeld's usage implied rarity and collectability.[2] This ashcan was the first publication from Image Comics, a publisher started by popular artists during a boom period for comic books. The sales success of the Youngblood ashcans prompted imitation, and for the next year nearly every new Image series had a corresponding ashcan.[7] The typical print run for Image ashcans was between 500 and 5,000. Soon, other publishers began releasing ashcans in a variety of sizes and formats.[2] In 1993, Triumphant Comics advertised ashcan editions of new properties with print runs of 50,000.[12]
 Following the collapse of the speculation market in comics in the mid-1990s, the term has been used by publishers to describe booklets promoting upcoming comics.[8] Established publishers such as Dark Horse Comics, IDW Publishing, and DC Comics continue to use ashcan copies as part of their marketing plan for new titles.[13][14][15] Aspiring creators also apply the term to hand-stapled photocopied books they use to demonstrate their abilities to hiring editors at comic book conventions or as part of a submissions package.[16]
 The term has been appropriated by the film and television industries to refer to low-quality material made specifically to preserve rights to a licensed character, which often expire if unused for a set period of time. One of the earliest examples of this practice is the 1966 animated adaptation of The Hobbit.[17] Other prominent examples include the 2011 Hellraiser: Revelations,[17] a 2015 adaptation of The Wheel of Time,[17] and the unreleased Fantastic Four film from 1994.[18]
 
 1 Original use 2 Later use 3 Film and television 4 See also 5 References Burning off, the airing of otherwise-abandoned television programs in less desirable time slots or on sister networks, often for contractual or legal reasons ^ Ramsey, Taylor (February 5, 2013). "The History of Comics: Decade by decade". The Artifice. Retrieved June 23, 2018..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c d e f g h i Christensen, William A; Seifert, Mark (February 1994). "Evolution of the Ashcan". Wizard. No. 30. New York City: Wizard Entertainment. p. 89.
 ^ a b c Seifert, Mark (August 1, 2014). "75 Years Ago Today: DC Comics Wins The Race For Flash". Bleeding Cool. Avatar Press. Retrieved June 19, 2018.
 ^ a b c d Malito, Alessandra (October 9, 2017). "As fans throng to New York Comic Con, these comic books sell for millions of dollars". MarketWatch. Retrieved December 11, 2017.
 ^ a b c d e Colabuono, Gary (September 1999). "Absolutely Amazing DC Ashcans". Comic Book Marketplace. Vol. 2 no. 71. Coronado, CA: Gemstone Publishing, Inc. p. 24.
 ^ a b Hamerlinck, Paul; Daigh, Ralph (2001). Fawcett Companion: The Best of FCA. Raleigh, NC: TwoMorrows Publishing. p. 107. ISBN 9781893905108.
 ^ a b c d e f Colabuono, Gary (July 1993). "Ashcan Comics". Hero Illustrated. No. 1. Lombard, Illinois: Warrior Publications. p. 56.
 ^ a b "Historic DC Ashcan Comic Books in Heritage New York Auction". CGC Comics. January 31, 2012. Archived from the original on June 26, 2018. Retrieved June 19, 2018.
 ^ "Historic DC Ashcan comic books in Heritage Auctions February 22 New York event". Art Daily. Retrieved June 15, 2019.
 ^ Cox, Brian L (August 13, 2011). "Rare 'ashcan' comic books on display at Chicago Comic Con". Chicago Tribune. Retrieved December 11, 2017.
 ^ Mallette, Jack (November 1986). "Bob Burden (part 1)". Comics Interview. No. 40. Fictioneer Books. pp. 22–41.
 ^ "Advertisement". Hero Illustrated. No. 2. Lombard, Illinois: Warrior Publications. August 1993. p. 125.
 ^ Dietsch, TJ (April 12, 2012). "C2E2: Bobby Curnow Unleashes "Battle Beasts" at IDW". Comic Book Resources. Retrieved July 2, 2018.
 ^ Johnston, Rich (April 20, 2017). "Let's All Read The Dark Matter/Master Class Ashcan From DC Comics". Bleeding Cool. Avatar Press. Retrieved July 2, 2018.
 ^ Johnston, Rich (March 12, 2015). "Dark Horse Sends Fight Club 2, Rebels And Archie Vs. Predator". Bleeding Cool. Avatar Press. Retrieved June 25, 2018.
 ^ Hart, Christopher (2014). Drawing Cutting Edge Anatomy. Potter/Ten Speed/Harmony/Rodale. ISBN 9780770434861.
 ^ a b c Wirestone, Clay (March 13, 2015). "The Weird History of the "Ashcan Copy"". Mental Floss. Retrieved June 19, 2018.
 ^ Ito, Robert (March 2005). "Fantastic Faux!". Los Angeles. p. 108. Retrieved January 1, 2012.
 v t e Comic book
Ashcan comic
Minicomic Ashcan comic Minicomic Comic strip
Comic strip formats
Daily comic strip
Lianhuanhua
Sunday comics
Sunday strip
Topper
Yonkoma Comic strip formats Daily comic strip Lianhuanhua Sunday comics Sunday strip Topper Yonkoma Digital comics
Mobile comic
Webcomic
Webtoon Mobile comic Webcomic Webtoon Editorial cartoon Graphic novel
Trade paperback Trade paperback Film comic Motion comic Photo comics Text comics Cartoonists
list list Artists Writers Inkers Colorists Letterers Editors Publishing companies Editorial
list list Minicomics Webcomics American Australian Canadian Cuban Macedonian Japanese (manga) Female comics creators
list list Jewish American cartoonists Years in comics American
Golden Age
Silver Age
Bronze Age
Modern Age
events Golden Age Silver Age Bronze Age Modern Age
events events Japanese (manga) Webcomics Abstract Adult Alternative Autobiographical Celebrity comics Crime Dystopian Erotic Fantasy
list list Furry Gekiga Horror Romance
list list Science fiction Superhero Teen humor Tijuana Bible Underground War Western Wrestling Antihero Funny animal Superhero Supervillain Masking Rogue Widescreen comics Ethnic stereotypes Portrayal of black people
African characters African characters Portrayal of women
Women in Refrigerators
The Hawkeye Initiative Women in Refrigerators The Hawkeye Initiative Feminist comic books LGBT
American mainstream comics American mainstream comics Gender and webcomics South Africa Argentina Brazil Canada
Quebec Quebec Mexico United States
list list China and Taiwan
list
Hong Kong list Hong Kong India
list list Japan
lists lists Korea
list list Philippines Thailand Vietnam Czech Republic France and Belgium
list
Belgium list Belgium Germany Hungary Italy
list list Netherlands Poland Portugal Serbia Spain
list list United Kingdom
Wales Wales Australia Comic books Comic strips Manga magazines Webcomics Based on fiction Based on films Based on video games Based on television programs Awards Best-selling comic series
manga series
manga magazines manga series manga magazines Limited series Comic books on CD/DVD Comics solicited but never published Comics and comic strips made into feature films Belgian Comic Strip Center Billy Ireland Cartoon Library & Museum
National Cartoon Museum National Cartoon Museum British Cartoon Archive Caricature & Cartoon Museum Basel Cartoon Art Museum The Cartoon Museum Fred Waring Cartoon Collection Gibiteca Antonio Gobbo Michigan State University Comic Art Collection Museum of Comic and Cartoon Art ToonSeum Words & Pictures Museum Center for Cartoon Studies The Kubert School Academy of Comic Book Arts Association of Canadian Cartoonists Association of Comics Magazine Publishers Australian Cartoonists' Association National Cartoonists Society Samahang Kartunista ng Pilipinas Association des Critiques et des journalistes de Bande Dessinée Canadian Comic Book Creator Awards Association Comic & Fantasy Art Amateur Press Association Sequart Organization Svenska Serieakademien Academy of Comic-Book Fans and Collectors British Amateur Press Association (comics) Comic Book Legal Defense Fund Comic Legends Legal Defense Fund Finnish Comics Society Friends of Lulu The Hero Initiative Xeric Foundation  Portal  Category Comics terminology Publications Trademark law Use American English from September 2019 All Wikipedia articles written in American English Use mdy dates from September 2019 Articles with short description Pages using multiple image with auto scaled images Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version  This page was last edited on 28 October 2019, at 01:00 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  ashcan comic ^ a b c d e f g h i a b c a b c d a b c d e a b a b c d e f a b ^ ^ ^ ^ ^ ^ ^ ^ a b c ^ Glossary of comics terminology 
Comic book
Ashcan comic
Minicomic
Comic strip
Comic strip formats
Daily comic strip
Lianhuanhua
Sunday comics
Sunday strip
Topper
Yonkoma
Digital comics
Mobile comic
Webcomic
Webtoon
Editorial cartoon
Graphic novel
Trade paperback
  
Film comic
Motion comic
Photo comics
Text comics
 
Cartoonists
list
Artists
Writers
Inkers
Colorists
Letterers
Editors
Publishing companies
By format
Editorial
list
Minicomics
Webcomics
By country
American
Australian
Canadian
Cuban
Macedonian
Japanese (manga)
Other
Female comics creators
list
Jewish American cartoonists

 
Editorial
list
Minicomics
Webcomics
 
American
Australian
Canadian
Cuban
Macedonian
Japanese (manga)
 
Female comics creators
list
Jewish American cartoonists
 
Years in comics
American
Golden Age
Silver Age
Bronze Age
Modern Age
events
Japanese (manga)
Webcomics
 Genres
Abstract
Adult
Alternative
Autobiographical
Celebrity comics
Crime
Dystopian
Erotic
Fantasy
list
Furry
Gekiga
Horror
Romance
list
Science fiction
Superhero
Teen humor
Tijuana Bible
Underground
War
Western
Wrestling
Tropes
Antihero
Funny animal
Superhero
Supervillain
Masking
Rogue
Widescreen comics
Themes
Ethnic stereotypes
Portrayal of black people
African characters
Portrayal of women
Women in Refrigerators
The Hawkeye Initiative
Feminist comic books
LGBT
American mainstream comics
Gender and webcomics
 
Abstract
Adult
Alternative
Autobiographical
Celebrity comics
Crime
Dystopian
Erotic
Fantasy
list
Furry
Gekiga
Horror
Romance
list
Science fiction
Superhero
Teen humor
Tijuana Bible
Underground
War
Western
Wrestling
 
Antihero
Funny animal
Superhero
Supervillain
Masking
Rogue
Widescreen comics
 
Ethnic stereotypes
Portrayal of black people
African characters
Portrayal of women
Women in Refrigerators
The Hawkeye Initiative
Feminist comic books
LGBT
American mainstream comics
Gender and webcomics
 Africa
South Africa
Americas
Argentina
Brazil
Canada
Quebec
Mexico
United States
list
Asia
China and Taiwan
list
Hong Kong
India
list
Japan
lists
Korea
list
Philippines
Thailand
Vietnam
Europe
Czech Republic
France and Belgium
list
Belgium
Germany
Hungary
Italy
list
Netherlands
Poland
Portugal
Serbia
Spain
list
United Kingdom
Wales
Oceania
Australia
 
South Africa
 
Argentina
Brazil
Canada
Quebec
Mexico
United States
list
 
China and Taiwan
list
Hong Kong
India
list
Japan
lists
Korea
list
Philippines
Thailand
Vietnam
 
Czech Republic
France and Belgium
list
Belgium
Germany
Hungary
Italy
list
Netherlands
Poland
Portugal
Serbia
Spain
list
United Kingdom
Wales
 
Australia
 By format
Comic books
Comic strips
Manga magazines
Webcomics
By source
Based on fiction
Based on films
Based on video games
Based on television programs
Other lists
Awards
Best-selling comic series
manga series
manga magazines
Limited series
Comic books on CD/DVD
Comics solicited but never published
Comics and comic strips made into feature films
 
Comic books
Comic strips
Manga magazines
Webcomics
 
Based on fiction
Based on films
Based on video games
Based on television programs
 
Awards
Best-selling comic series
manga series
manga magazines
Limited series
Comic books on CD/DVD
Comics solicited but never published
Comics and comic strips made into feature films
 
Belgian Comic Strip Center
Billy Ireland Cartoon Library & Museum
National Cartoon Museum
British Cartoon Archive
Caricature & Cartoon Museum Basel
Cartoon Art Museum
The Cartoon Museum
Fred Waring Cartoon Collection
Gibiteca Antonio Gobbo
Michigan State University Comic Art Collection
Museum of Comic and Cartoon Art
ToonSeum
Words & Pictures Museum
 
Center for Cartoon Studies
The Kubert School
 Professional
Academy of Comic Book Arts
Association of Canadian Cartoonists
Association of Comics Magazine Publishers
Australian Cartoonists' Association
National Cartoonists Society
Samahang Kartunista ng Pilipinas
Critical andacademic
Association des Critiques et des journalistes de Bande Dessinée
Canadian Comic Book Creator Awards Association
Comic & Fantasy Art Amateur Press Association
Sequart Organization
Svenska Serieakademien
Charitableand outreach
Academy of Comic-Book Fans and Collectors
British Amateur Press Association (comics)
Comic Book Legal Defense Fund
Comic Legends Legal Defense Fund
Finnish Comics Society
Friends of Lulu
The Hero Initiative
Xeric Foundation
 
Academy of Comic Book Arts
Association of Canadian Cartoonists
Association of Comics Magazine Publishers
Australian Cartoonists' Association
National Cartoonists Society
Samahang Kartunista ng Pilipinas
 
Association des Critiques et des journalistes de Bande Dessinée
Canadian Comic Book Creator Awards Association
Comic & Fantasy Art Amateur Press Association
Sequart Organization
Svenska Serieakademien
 
Academy of Comic-Book Fans and Collectors
British Amateur Press Association (comics)
Comic Book Legal Defense Fund
Comic Legends Legal Defense Fund
Finnish Comics Society
Friends of Lulu
The Hero Initiative
Xeric Foundation
 
 Portal
 Category
 