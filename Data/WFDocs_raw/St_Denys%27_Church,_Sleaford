
 
 St Denys' Church, is a medieval Anglican parish church in Sleaford, Lincolnshire, England. While a church and a priest have probably been present in the settlement since approximately 1086, the oldest parts of the present building are the tower and spire, which date to the late 12th and early 13th centuries; the stone broach spire is one of the earliest examples of its kind in England. The Decorated Gothic nave, aisles and north transept were built in the 14th century. The church was altered in the 19th century: the north aisle was rebuilt by the local builders Kirk and Parry in 1853 and the tower and spire were largely rebuilt in 1884 after being struck by lightning. St Denys' remains in use for worship by the Church of England.
 The church is a Grade I listed building, a national designation given to "buildings of exceptional interest".[1] It is a prime example of Decorated Gothic church architecture in England, with the architectural historians Sir Nikolaus Pevsner and John Harris noting that "it is a prolonged delight to follow the mason's inventiveness".[2] The church's tracery has attracted special praise, with Simon Jenkins arguing that its Decorated windows are "works of infinite complexity".[3] Built out of Ancaster stone with a lead roof, St Denys' is furnished with a medieval rood screen and a communion rail, possibly by Sir Christopher Wren, and has a peal of eight bells, dating to 1796. The church also houses several memorials, including two altar tombs commemorating members of the Carre family, Sleaford's lords of the manor in the 17th century.
 St Denys' Church is the parish church of the benefice of Sleaford (formerly called New Sleaford), which encompasses most of the market town of Sleaford in the English non-metropolitan county of Lincolnshire. The benefice is a vicarage and falls within the deanery of Lafford and the archdeaconry and diocese of Lincoln;[4] as of 2015, the vicar is Rev. Philip Anthony Johnson, who was appointed in 2013.[5][6] The church is located next to (and faces onto) the market place at the town centre.[4] It is dedicated to St Denys; the Victorian clergyman and local historian Edward Trollope stated that this is a medieval form of St Dionysius, but does not elaborate on which of the several saints called Dionysius this refers to.[7] According to a pamphlet published by the parochial church council, St Denys is a medieval composite of Dionysius of Paris, Dionysius the Areopagite and Pseudo-Dionysius the Areopagite.[8]
 As of 2015, regular church services were scheduled for Sundays and Wednesdays. Holy Communion was conducted weekly at 8:00 am on Sundays, followed by Sunday School and an all-age family worship at 10:00 am. A parent and toddler group was scheduled for Wednesdays at 9:30 am.[4]
 The Sleaford area has been inhabited since the late Iron Age; people settled around the ford where a prehistoric track running northwards from Bourne crossed the River Slea.[9] A large hoard of coin moulds belonging to the Corieltauvi tribe have been uncovered in this area and dated to the late Iron Age.[9] It was occupied by the Romans,[10] and then by the Anglo-Saxons.[11] The place-name Slioford first appears in 852, meaning "crossing over a muddy stream", in reference to the Slea.[12] The settlement around the crossing came to be known as "Old" Sleaford in 13th-century sources to distinguish it from developments further west, around the present-day market place, which came to be known as "New" Sleaford.[13] The origins of New Sleaford are not clear, leading to a theory that it was planted by the Bishop of Lincoln in the 12th century as a means of increasing his income, hence the epithet "New". The town's compass-point layout, the 12th-century date of St Denys' stonework and other topographical features offer evidence for this theory.[14][15]
 A speculative reassessment of Domesday Book (1086) material suggests that St Denys' origins may be earlier. Two manors called Eslaforde (Sleaford) were recorded in the Domesday Book, one held by Remigius, Bishop of Lincoln, the other by Ramsey Abbey. The Bishop succeeded a Saxon thegn, Bardi, and held 11 carucates with 29 villeins, 11 bordars, 6 sokemen, a church and priest, as well as 8 mills, 1 acre of woodland, 320 acres (130 ha) of meadow and 330 acres (130 ha) of marsh. Ramsey Abbey had been granted land in Sleaford and surrounding villages before the Norman Conquest of England; in Domesday its fee consisted of 1 carucate, 1 sokeman, 2 villeins and 27 acres of meadow. It was sokeland of the abbot of Ramsey's manor of Quarrington, where he held two churches.[16] There is no evidence for a second church at Quarrington, which suggests that the record is alluding to one in another of the abbot's manors for which Quarrington was an estate centre. The local historians David Roffe and Christine Mahany ruled out the possibility that this referred to Cranwell, another of the abbey's fees, and concluded that it is probably a reference to the church at Old Sleaford, which was granted by a knight of Ramsey to Haverholme Priory in c. 1165. Hence, the church possessed by the bishop in the other manor must have been a second church in Sleaford, and therefore could only have been St Denys' in what would become New Sleaford.[17]
 Sleaford and its church were altered considerably in the 12th century, especially under Bishop Alexander of Lincoln; a castle was constructed to the west of the town during his episcopate and work on the earliest surviving parts of the church may date to this period.[18] Facing onto the market place, the tower is the oldest part of the present church building and dates to the late 12th century, probably c. 1180.[19] Its broach spire has been dated to the early 13th century, possibly c. 1220.[19] A prebendary of Sleaford is recorded in the late 13th century whose office was probably founded by one of the post-Conquest Bishops, who were its patrons. The vicarage of Sleaford was founded and endowed in 1274; the record has survived and shows Henry de Sinderby being presented to the vicarage by the Treasurer of Lincoln and Prebendary of Sleaford, Richard de Belleau; the Bishop instituted him that March. The vicar could profit from tithes and oblations, and was given a house formerly occupied by one Roger the chaplain, but he had to pay £15 to the prebendary at the feasts of the Nativity and St John the Baptist. The prebendary otherwise retained his jurisdiction over the parish.[20]
 A period of rebuilding and remodelling occurred in the late Middle Ages. A chantry chapel, dedicated to the Virgin Mary, was founded in 1271 by the merchants Thomas Blount and John de Bucham, who endowed it with lands around Old and New Sleaford, and several surrounding villages. The chapel is located on the north aisle, and the chaplain was instructed to pray there for the founders at his daily mass.[21] The chantry priest's house is recorded in the 1440s as one of the oldest buildings in Sleaford; located in the churchyard, it became the Vicarage.[22] The tower was probably accompanied by a nave of a similar date, which was rebuilt in the Decorated Gothic style in the mid to late 14th century; the transept followed twenty or thirty years later, according to Trollope.[19][23] A clerestory was added in around 1430 and the chancel was remodelled at this time.[19]
 A diocesan return of 1563 recorded 145 households in the parish of New Sleaford,[24] while the Compton Census (1676) reveals that New Sleaford had a Conformist population of 576 people, no "Papists", and 6 Non-conformists.[25] There is a widely held local tradition that St Denys' was used during the English Civil War (1642–51) as a barracks for parliamentary troops  who destroyed the interior furnishings.[26] The local historian Trollope stated that the soldiers looted the brass eagle lectern (last recorded in 1622),[27] broke the stained glass windows and the organ, and stole valuables.[28] Whether this damage occurred or not, repairs to the windows and roof were carried out in 1657, paid for by public subscription. Galleries were also added to the church in the 18th century: the south in 1758, west in 1772, and north in 1783–84.[29] In 1772, Edward Evans, a ship's surgeon on HMS Egmont, donated £300 to replace the organ with one built by Samuel Green of London.[21]
 For most of the 19th century, the Anglican community dominated Sleaford's civic bodies, including the Board of Guardians, who oversaw the workhouse, and the Local Board of Health. Dr Richard Yerburgh and his son, Richard, were vicars in 1809–51 and 1851–82 respectively and had family connections with the local builders Kirk and Parry; Yerburgh and Thomas Parry (one half of the firm) were on the Board of Guardians and were labelled members of a "family party" by opponents during the Board's 1870 elections (they were nonetheless re-elected).[30][31] They and other local clergymen were key players in the establishment of National schools in Sleaford and Quarrington, which Kirk and Parry built.[30] The Anglican congregation, at an estimated 700 to 800 people in 1851 (St Denys' had enough space for 743 people), was less than half of the size of the nonconformist community, which was probably larger than 2,000 and tended to flourish in poorer parts of the town.[32][33]
 The 19th century also witnessed two major restorations to St Denys'. As the congregation expanded, the need for greater space was met with the addition of a new north aisle in 1853. This coincided with a wider restoration project carried out at the cost of £3,500 by Kirk and Parry, which included the demolition of the galleries, the addition of a strainer arch and the relocation of the organ.[29][34] The church was damaged by an electrical storm in 1884 and parts, including the stone broach spire—one of the oldest in England[35]—were rebuilt by Kirk and Parry in 1885–86.[29] The old organ was sold in 1891 and St Hugh's Chapel and the choir vestry were dedicated to the memory of a local solicitor, Henry Snow, in 1906.[29][36] Electric lighting was introduced in 1951–53 and extensive restoration work was carried out in 1966, when the organ was rebuilt, and in 1988.[29] Fifty-four solar panels were added in 2008, at the cost of £70,000, and by 2011 were able to cover the church's electricity bill.[37]
 St Denys' is constructed in Ancaster stone across four periods: the earliest sections in a transitional style between Early English Gothic and Decorated Gothic; the late medieval nave, aisles and chancel in Decorated Gothic; the later Perpendicular Gothic clerestory and chancel; and the Victorian neo-Gothic restorations. The earliest parts consist of the late 12th or early 13th-century tower and spire on the west side of the church, which have a combined height of 144 feet (44 m). Its arched entrance exhibits features of both the Early English and Decorated Gothic styles. During the restoration, a 15th-century window was removed, placed in the churchyard and replaced by arcading and three circlets, deemed "somewhat absurd" by the architectural historian Sir Nikolaus Pevsner.[35][19]
 With the exception of the tower and spire, much of the church was rebuilt in the Decorated Gothic style during the late 14th century; Pevsner argued that Sleaford was among those Lincolnshire churches which demonstrate that "For the decorated style, Lincolnshire is the best county of all".[38] The nave and aisles extend eastwards from the tower. Outside, parts of the aisles are highly decorated; the north doorway includes shafts, mouldings and finials, while the simpler south doorway has niches and monster carvings. The northern doorway has a gable which encroaches up into a five-light window.[39] Pevsner remarks that St Denys' is particularly notable for its tracery,[35] adding that "it is a prolonged delight to follow the mason's inventiveness along the building".[2] The church's entry on the Statutory List of Buildings of Special Architectural or Historic Interest also notes its "particularly good mid [14th-century] tracery and ornament".[19] The window above the north doorway is a good example; it contains elements shared by others of the Decorated style around the church, including reticulated ogee arches of varying complexity.[39] The north transept includes a six-light window—"one of the great flowing designs of the country", according to Pevsner, Harris and Antram.[40] Simon Jenkins, in his 2012 England's Thousand Best Churches, awards St Denys three stars and says of the window tracery: "This is Lincolnshire at its most curvaceous, best displayed in the north transept north window. Words can barely do justice to this work... This is a work of infinite complexity, with variations repeated throughout the church."[3]
 A Perpendicular clerestory adorns the aisles with three-light windows; it dates to the early 15th century, possibly c. 1430. The north aisle was extended in 1853, but the architects, Kirk and Parry, reused the windows.[19][40]
 Inside, the tower ceiling has a tierceron vault and is connected to the nave and aisles by arches. The nave's arcading spans four bays, the last of which on the north side acts as an entrance to the transept. The north aisle was extended in 1853 by Kirk and Parry, who added a strainer arch to support the tower and join it with the north aisle.[41] The reredos and altar were built in 1922 by L.T. Moore;[n 1] the rood screen was restored by Ninian Comper in 1918 and the Communion rail was taken from Lincoln Cathedral during a restoration;[41] it has been attributed to Sir Christopher Wren, but Pevsner makes no mention of this.[41][42] The rood loft is also by Comper, who is described by Simon Jenkins as "inventive as ever. His wood is unusually dark and unpainted, as are the rood figures and angels. The loft blooms out over the crossing."[3] The screen and altar rails in St Hugh's Chapel are the work of C.H. Fowler; E. Stanley Watkins completed the reredos in 1906.[41] In the 1640s, the stained glass included the armorial bearings of Sir William Hussey impaling his wife's Barkley arms, and the arms of Markham and John Russell, Bishop of Lincoln. These have not survived,[44] but the current stained glass includes a "very Gothic" window by Hardman dating to c. 1853, three by Holland of Warwick from the late 1880s, one of Ward and Hughes (1885) and one by Morris & Co. from 1900.[41]  In 2006 Glenn Carter completed a stained glass window dedicated to Yvonne Double which had been commissioned by her widower, Eddy Double.[45][46][n 2]
 The earliest peal recorded at St Denys' had six bells: three dating from 1600, one from 1707 and two undated. In 1796 a new peal of eight bells were cast by Thomas Osborn of Downham, Norfolk; the tenor is in the E key and weighs 19 long cwt 3 qr 6 lb (2,218 lb or 1,006 kg).[48][49] Samuel Green's organ of 1772 was rebuilt by Holdich in 1852[50] and replaced by the present organ in 1891, which was built by Forster and Andrews of Hull; rebuilt by Harrison & Harrison in 1966, it was restored by A. Carter in 1999 and has three manuals and a pedalboard.[51][52] St Hugh's Chapel has its own organ, with one manual, installed by Cousans and Sons in 1912.[52][53] The church also houses a collection of fifteen antique, chained books in an oak reading desk; the oldest items date to the early 17th century and include tracts on divinity. Other items of furniture include two old chests and a dole cupboard, while a 16th-century tapestry is in the church's possession.[41][54] The octagonal font is in a Decorated Gothic style, but has been altered considerably.[41]
 The churchyard around St Denys' has been expanded several times: first in 1391, when the Bishop of Lincoln, John Bokingham, was granted a piece of land 150 by 8 feet (45.7 m × 2.4 m) to one side of the church. Land to the north was also added in 1796. The grounds were enclosed by a dwarf wall, which was replaced by a more substantial stone wall and iron fence in 1837; the railings were removed during the First World War.[8][55] In a report on the town's health in 1850, William Ranger criticised the overcrowding of the churchyard; in 1855, burials in the grounds ceased and the vestry elected a burial board to produce a solution.[n 3] They purchased 2 acres, 3 roods and 31 poles (0.92 ha) of land to the north of Eastgate at a cost of £1,500; this was converted into a cemetery and a further 2 roods and 17 poles (0.063 ha) were bought for an access road. These grounds were extended in 1862 by an acquisition of 3 acres and 39 poles (1.51 ha) of land to the west of the cemetery; they are now managed by Sleaford Town Council.[56][57]
 The doorway in the tower of the church, showing three orders of shafts and zigzag mouldings in the arch
 The five-light window with reticulated ogee arches above the doorway of the north aisle
 The rood screen, restored by Sir Ninian Comper in 1918[41]
 View into the north transept and the chantry chapel
 East window of the chancel
 Yvonne Double memorial window, by Glenn Carter, 2006[45][46]
 The pulpit
 15th-century window removed from the tower during restoration work in c. 1884
 According to Edward Trollope, the oldest tombstone in the church was from the 13th century; it was faded and illegible when he recorded it in or before 1872. A 14th-century slab, originally for a now-lost effigy, is in the church, and brass plate from the same period was discovered during the 1853 restoration. Richard Dokke, along with his wife Joanna and son John, are commemorated in a plaque dating to the 1430s, and a plaque to William Harebeter and his wife Elizabeth also dates to the 15th century.[58]
 Although Gervase Holles recorded many 16th-century monuments when he visited Sleaford, most have disappeared.[59] Amongst those which remain are the tombs and plaques commemorating the first members of the Carre family to settle in Sleaford. The Carres hailed from Northumberland, but George Carre (d. 1521), a wool merchant, established himself in the town and is commemorated in St Denys' by a brass. On the northern side of the chancel is an alabaster monument dedicated to George's eldest surviving son Robert Carre (d. 1590), his three wives and some of their children; he became lord of the manors of Old and New Sleaford. Opposite, on the southern side, is an alabaster altar tomb by Maximilian Colt dedicated to Robert's fourth son and eventual heir, Sir Edward Carre, 1st Baronet (d. 1618), which carries the effigies of Edward and one of his two wives, probably his second, Anne Dyer; according to Trollope, it was "said to have been mutilated during the Civil War".[60][61] Further plaques commemorate Sir Edward Carre's grandson, Sir Robert Carre, 3rd Baronet (d. 1682), and his son, Sir Edward (d. 1683), who is also commemorated by a bust in the church.[62]
 There are numerous other memorials to prominent Sleafordians. Early examples are plaques to John Walpoole (d. 1591, monument dated 1631), the draper Richard Warsope (d. 1609, erected by Robert Camock), and Rev. Theophilus Brittaine (d. 1696).[63] Later monuments include those of Richard Moore (d. 1771) and Elizabeth Cooper (d. 1792), as well as a slab for Eleanor (d. 1725), wife of John Peart.[61][64] The novelist Frances Brooke (d. 1789) is buried in the church.[65] Clergymen include William Seller, Joseph Francis (d. 1749) and Anthony Skepper (d. 1773). A window is dedicated to a local solicitor, Henry Snow (d. 1905), and memorials on the north wall include George Jeudwine (d. 1952), another solicitor, and the local historian William Hosford (d. 1987).[66] The monument to Ann Bankes (d. 1834) incorporates a statue of a woman sinking into the ground, which Pevsner called "remarkably tender", while the tablet to Ann Moore (d. 1830) in the transept is noted as "good Grecian".[61]
 From 1 January 2019 Lee Rooke is the new organist and choirmaster.[67]
 1 Description 2 History

2.1 Background and origins
2.2 Expansion
2.3 Early modern and later

 2.1 Background and origins 2.2 Expansion 2.3 Early modern and later 3 Architecture, fittings and grounds 4 Memorials 5 Music 6 References

6.1 Notes
6.2 Citations
6.3 Bibliography

 6.1 Notes 6.2 Citations 6.3 Bibliography 7 External links 


The doorway in the tower of the church, showing three orders of shafts and zigzag mouldings in the arch


 


The five-light window with reticulated ogee arches above the doorway of the north aisle


 


The rood screen, restored by Sir Ninian Comper in 1918[41]


 


View into the north transept and the chantry chapel


 


East window of the chancel


 


Yvonne Double memorial window, by Glenn Carter, 2006[45][46]


 


The pulpit


 


15th-century window removed from the tower during restoration work in c. 1884


 ^ An earlier reredos, designed in "fine Gothic" style by Charles Kirk, was erected in memory of M.P. Moore (d. 1866).[42][43]
 ^ Other windows have been dedicated to prominent parishioners, including Dr Richard Yerburgh, Charles Kirk, Francis and Benjamin Handley, Caroline E. Moore, Robert George Bankes, John Bissill, John Caparn, Mrs. Lucy Ashington, Henrietta Bankes, John Pearson, and William Hoster. Thomas Parry, C. Drake Newton and Mrs. Warwick.[47]
 ^ The first members were M.P. Moore, John Warwick, Edward Newbatt, William Foster, Thomas Simpson, Edward Allen, J.T. Marston, Charles Kirk and Thomas Parry. Newbatt and Kirk declined the offer and so W.H. Holdich and Rev. Richard Yerburgh were elected in their place.[56]
 ^ "Listed buildings". Historic England. Retrieved 7 July 2015..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b Pevsner & Harris 1973, p. 634.
 ^ a b c Jenkins 2012, pp. 450–451.
 ^ a b c "Sleaford Parish Church – St Denys', Sleaford". A Church Near You. Church of England. Retrieved 6 July 2015.
 ^ "Contact us". St Denys' Church, Sleaford. Archived from the original on 5 October 2015. Retrieved 3 October 2015.
 ^ "New vicar at St Denys' Church". Sleaford Standard. 23 October 2013. Retrieved 6 July 2015.
 ^ Trollope 1872, p. 147.
 ^ a b Hoare 1988, p. 12.
 ^ a b Mahany & Roffe 1979, p. 6.
 ^ Mahany & Roffe 1979, pp. 8–10.
 ^ Mahany & Roffe 1979, p. 10.
 ^ Ekwall 1977, p. 462.
 ^ "Settlement of Old Sleaford (Reference Name MLI91636)". Lincs to the Past. Lincolnshire Archives. Retrieved 29 November 2014.
 ^ Mahany & Roffe 1979, pp. 11–16.
 ^ Pawley 1996, pp. 15–16.
 ^ Mahany & Roffe 1979, p. 11.
 ^ Mahany & Roffe 1979, pp. 13–14.
 ^ Pawley 1996, p. 25.
 ^ a b c d e f g Historic England. "Parish Church of St Denys (1062157)". National Heritage List for England. Retrieved 18 February 2015.
 ^ Trollope 1872, pp. 140–141.
 ^ a b Trollope 1872, p. 152.
 ^ Pawley 1996, p. 33.
 ^ Trollope 1872, pp. 147–148.
 ^ "General Settlement Record for New Sleaford" Heritage Gateway. Retrieved 1 December 2015.
 ^ Whiteman & Clapinson 1987, pp. 363.
 ^ Pawley 1996, p. 42.
 ^ Trollope 1872, p. 164.
 ^ Trollope 1872, p. 151.
 ^ a b c d e Hoare 1988, p. 1.
 ^ a b Pawley 1996, p. 88.
 ^ Ellis 1981, p. 101.
 ^ Pawley 1996, pp. 88–89.
 ^ Ellis 1981, p. 98.
 ^ Trollope 1872, p. 149.
 ^ a b c Pevsner, Harris & Antram 2002, p. 650.
 ^ "Dedication of a Chapel". Grantham Journal. 14 April 1906. p. 6. Retrieved 6 July 2015 – via British Newspaper Archive.
 ^ "Profit for St Denys' solar panel project in Sleaford". BBC News. 26 October 2011. Retrieved 6 July 2015.
 ^ Pevsner & Harris 1973, p. 38.
 ^ a b Pevsner, Harris & Antram 2002, pp. 650–651.
 ^ a b Pevsner, Harris & Antram 2002, p. 651.
 ^ a b c d e f g h Pevsner, Harris & Antram 2002, p. 652.
 ^ a b Trollope 1872, p. 154.
 ^ "Chronicle for the Year 1866". Lincolnshire Chronicle. 5 January 1867. p. 8. Retrieved 26 March 2015 – via British Newspaper Archive.
 ^ Trollope 1872, p. 163.
 ^ a b "BEM honour for services to the community". Sleaford Standard. 13 June 2014. Retrieved 7 July 2015.
 ^ a b "Commissions". Glenn Carter Glass. Retrieved 7 July 2015.
 ^ Trollope 1872, pp. 163–164.
 ^ Trollope 1872, pp. 150–151.
 ^ "Sleaford, S. Denys". Dove's Guide for Church Bell Ringers. Retrieved 3 September 2015.
 ^ "Lincolnshire Sleaford, St. Denys, Market Place [N14356]". National Pipe Organ Register. British Institute of Organ Studies. Retrieved 3 September 2015.
 ^ "Lincolnshire Sleaford, St. Denys, Market Place [N14357]". National Pipe Organ Register. British Institute of Organ Studies. Retrieved 3 September 2015.
 ^ a b "Lincolnshire Sleaford, St. Denys, Market Place [N14358]". National Pipe Organ Register. British Institute of Organ Studies. Retrieved 3 September 2015.
 ^ "Lincolnshire Sleaford, St. Denys, Market Place [D00199]". National Pipe Organ Register. British Institute of Organ Studies. Retrieved 3 September 2015.
 ^ Trollope 1872, pp. 165–166.
 ^ Trollope 1872, pp. 166–167.
 ^ a b Ellis 1981, p. 77.
 ^ "Sleaford Cemetery". Sleaford Town Council. 3 July 2015. Retrieved 6 July 2015.
 ^ Trollope 1872, p. 155.
 ^ Trollope 1872, pp. 155–156.
 ^ Trollope 1872, pp. 131, 135, 156–158.
 ^ a b c Pevsner, Harris & Antram 2002, p. 653.
 ^ Trollope 1872, pp. 159–160.
 ^ Trollope 1872, pp. 161–162.
 ^ Trollope 1872, p. 162.
 ^ Edwards, Mary Jane (2008). "Brooke [née Moore], Frances (bap. 1724, d. 1789)". Oxford Dictionary of National Biography. Oxford University Press.
 ^ Hoare 1988, p. 8.
 ^ https://www.sleafordparishchurch.co.uk/STB%20Dec%202018.pdf
 Ekwall, Eilert (1977) [1960], The Concise Oxford Dictionary of English Place-names (4th ed.), Oxford: Clarendon Press, ISBN 978-0-19-869103-7 Ellis, Charles (1981), Mid-Victorian Sleaford: 1851 – 1871, Lincoln: Lincolnshire Library Service, ISBN 978-0-86111-102-2 Hoare, Douglas (1988), St Denys' Church, Sleaford, Sleaford: St Denys', Sleaford, Parochial Church Council, OCLC 877129833 Jenkins, Simon (2012), England's Thousand Best Churches, London: Penguin Books, ISBN 978-1-84614-664-0 Mahany, Christine; Roffe, David (1979), Sleaford, Stamford: South Lincolnshire Archaeological Unit, ISBN 978-0-906295-02-1 Pawley, Simon (1996), The Book of Sleaford, Finmere: Baron Birch for Quotes Ltd., ISBN 978-0-86023-559-0 Pevsner, Nikolaus; Harris, John (1973) [1964], Lincolnshire, The Buildings of England (1st ed.), Harmondsworth: Penguin Pevsner, Nikolaus; Harris, John; Antram, Nicholas (2002) [1989], Lincolnshire, The Buildings of England (2nd ed.), New Haven and London: Yale University Press, ISBN 978-0-300-09620-0 Trollope, Edward (1872), Sleaford, and the wapentakes of Flaxwell and Aswardhurn, London: W. Kent & Co., OCLC 228661584 Whiteman, A.; Clapinson, M. (1987), The Compton Census of 1676: A Critical Edition, Oxford University Press, ISBN 0197260411 Official website v t e St. Helen's, Brant Broughton St Barbara's, Haceby (redundant) St Botolph's, Quarrington St Denys', Sleaford St Peter's, Threekingham St Martin's, Ancaster St James' Church, Aslackby All Saints, Barrowby St Andrew's, Billingborough Bourne Abbey St Vincent's, Caythorpe St John the Evangelist, Corby Glen St Andrew's, Folkingham St Nicholas', Fulbeck St Wulfram's, Grantham St Mary and St Peter's, Harlaxton St Medardus and St Gildardus, Little Bytham St Guthlac's, Market Deeping St Peter's, Ropsley All Saints', Stamford St George's, Stamford St John the Baptist's, Stamford (redundant) St Martin's, Stamford St Mary's, Stamford St Paul's, Stamford St Andrew and St Mary's, Stoke Rochford St Bartholomew's, Welby Crowland Abbey St Mary Magdalene, Gedney St Mary and St Nicolas, Spalding St Botolph's, Boston St Wilfrid's, Alford St Michael's, Burwell St Mary's, North Cockerington St Benedict's, Haltham-on-Bain (redundant) St Mary's, Horncastle St James', Louth All Saints, Saltfleetby St Botolph's, Skidbrooke St Peter's, South Somercotes All Saints, Theddlethorpe St Martin's, Waithe St John the Baptist's, Yarburgh St Chad's, Harpswell St Peter's, Normanby by Spital (redundant) St Lawrence's, Snarford (redundant) Stow Minster St Mary's, Barnetby Holy Trinity, Barrow upon Humber St Mary's, Barton-upon-Humber St Peter's, Barton-upon-Humber (redundant) St Maurice's, Horkstow St Andrew's, Redbourne Grimsby Minster v t e St Edith's Church, Anwick St Andrew's Church, Asgarby St Mary's Church, Bloxholm St Denys' Church, Aswarby Aubourn Hall All Saints' Church, Beckingham St Michael's Church, Billinghay Somerton Castle (and outbuildings) St Helen's Church, Brant Broughton St Michael's Church, Stragglethorpe Quaker Meeting House, Brant Broughton All Saints' Church, Canwick St Mary's Church, Carlton-le-Moorland All Saints' Church, Coleby The Temple, Coleby Hall St Andrew's Church, Cranwell Culverthorpe Hall St Andrew's Church, Kelby St Thomas à Becket's Church, Digby St Peter's Church, Doddington Doddington Hall St James and St John's Church, Dorrington St Andrew's Church, Ewerby St John the Baptist's Church, Great Hale St Andrew's Church, Heckington Heckington Windmill St Andrew's Church, Helpringham St Swithin's Church, Leadenham St Andrew's Church, Leasingham St Peter's Church, Navenby St Barbara's Church, Haceby St Botolph's Church, Newton St Peter's Church, North Rauceby All Saints' Church, North Scarle St Peter's Church, Norton Disney St Peter and St Paul's Church, Osbournby St Clement's Church, Rowston All Saints' Church, Ruskington St Denys' Church, Silk Willoughby St Denys' Church, Sleaford Kyme Tower St Michael's Church, Swaton Temple Bruer Preceptory St Peter's Church, Threekngham St Nicholas's Church, Walcot (and churchyard cross) St Chad's Church, Welbourn All Saints' Church, Wellingore St Mary's Church, Wilsford Grade I listed churches in Lincolnshire Church of England church buildings in Lincolnshire Sleaford CS1: Julian–Gregorian uncertainty Featured articles Use British English from November 2015 Use dmy dates from June 2011 Articles with short description Coordinates on Wikidata Articles with OS grid coordinates Commons category link is on Wikidata Official website different in Wikidata and Wikipedia Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Deutsch Français Shqip Simple English  This page was last edited on 28 September 2019, at 01:41 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  St Denys' Church ^ ^ ^ ^ a b a b c a b c ^ ^ ^ a b a b ^ ^ ^ ^ ^ ^ ^ ^ ^ a b c d e f g ^ a b ^ ^ ^ ^ ^ ^ ^ a b c d e a b ^ ^ ^ ^ a b c ^ ^ ^ a b a b a b c d e f g h a b ^ ^ a b a b ^ ^ ^ ^ ^ a b ^ ^ ^ a b ^ ^ ^ ^ a b c ^ ^ ^ ^ ^ ^ St Denys', Sleaford The church's west façade, facingthe market place St Denys' Church, SleafordLocation in Lincolnshire 52°59′59″N 0°24′32″W﻿ / ﻿52.99972°N 0.40889°W﻿ / 52.99972; -0.40889Coordinates: 52°59′59″N 0°24′32″W﻿ / ﻿52.99972°N 0.40889°W﻿ / 52.99972; -0.40889 TF 06883 45893 England Church of England sleafordparishchurch.co.uk St Denys New Sleaford Lafford Lincoln Canterbury Rev. Philip Johnson Lee Rooke Richard ClashPhilip Starks  Wikimedia Commons has media related to St Denys', Sleaford. 
St. Helen's, Brant Broughton
St Barbara's, Haceby (redundant)
St Botolph's, Quarrington
St Denys', Sleaford
St Peter's, Threekingham
 
St Martin's, Ancaster
St James' Church, Aslackby
All Saints, Barrowby
St Andrew's, Billingborough
Bourne Abbey
St Vincent's, Caythorpe
St John the Evangelist, Corby Glen
St Andrew's, Folkingham
St Nicholas', Fulbeck
St Wulfram's, Grantham
St Mary and St Peter's, Harlaxton
St Medardus and St Gildardus, Little Bytham
St Guthlac's, Market Deeping
St Peter's, Ropsley
All Saints', Stamford
St George's, Stamford
St John the Baptist's, Stamford (redundant)
St Martin's, Stamford
St Mary's, Stamford
St Paul's, Stamford
St Andrew and St Mary's, Stoke Rochford
St Bartholomew's, Welby
 
Crowland Abbey
St Mary Magdalene, Gedney
St Mary and St Nicolas, Spalding
 
St Botolph's, Boston
 
St Wilfrid's, Alford
St Michael's, Burwell
St Mary's, North Cockerington
St Benedict's, Haltham-on-Bain (redundant)
St Mary's, Horncastle
St James', Louth
All Saints, Saltfleetby
St Botolph's, Skidbrooke
St Peter's, South Somercotes
All Saints, Theddlethorpe
St Martin's, Waithe
St John the Baptist's, Yarburgh
 
St Chad's, Harpswell
St Peter's, Normanby by Spital (redundant)
St Lawrence's, Snarford (redundant)
Stow Minster
 
St Mary's, Barnetby
Holy Trinity, Barrow upon Humber
St Mary's, Barton-upon-Humber
St Peter's, Barton-upon-Humber (redundant)
St Maurice's, Horkstow
St Andrew's, Redbourne
 
Grimsby Minster
 
St Edith's Church, Anwick
St Andrew's Church, Asgarby
St Mary's Church, Bloxholm
St Denys' Church, Aswarby
Aubourn Hall
All Saints' Church, Beckingham
St Michael's Church, Billinghay
Somerton Castle (and outbuildings)
St Helen's Church, Brant Broughton
St Michael's Church, Stragglethorpe
Quaker Meeting House, Brant Broughton
All Saints' Church, Canwick
St Mary's Church, Carlton-le-Moorland
All Saints' Church, Coleby
The Temple, Coleby Hall
St Andrew's Church, Cranwell
Culverthorpe Hall
St Andrew's Church, Kelby
St Thomas à Becket's Church, Digby
St Peter's Church, Doddington
Doddington Hall
St James and St John's Church, Dorrington
St Andrew's Church, Ewerby
St John the Baptist's Church, Great Hale
St Andrew's Church, Heckington
Heckington Windmill
St Andrew's Church, Helpringham
St Swithin's Church, Leadenham
St Andrew's Church, Leasingham
St Peter's Church, Navenby
St Barbara's Church, Haceby
St Botolph's Church, Newton
St Peter's Church, North Rauceby
All Saints' Church, North Scarle
St Peter's Church, Norton Disney
St Peter and St Paul's Church, Osbournby
St Clement's Church, Rowston
All Saints' Church, Ruskington
St Denys' Church, Silk Willoughby
St Denys' Church, Sleaford
Kyme Tower
St Michael's Church, Swaton
Temple Bruer Preceptory
St Peter's Church, Threekngham
St Nicholas's Church, Walcot (and churchyard cross)
St Chad's Church, Welbourn
All Saints' Church, Wellingore
St Mary's Church, Wilsford
 