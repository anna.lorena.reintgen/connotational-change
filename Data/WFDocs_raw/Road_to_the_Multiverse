Kei Ogawa as Japanese Lois and MegKotaro Watanabe as Japanese Brian and QuagmireJamison Yang as Japanese Chris, Stewie and Peter
 "Road to the Multiverse" is the first episode of the eighth season of the animated comedy series Family Guy. Directed by Greg Colton and written by Wellesley Wild, the episode originally aired on Fox in the United States on September 27, 2009, along with the series premiere of The Cleveland Show. In "Road to the Multiverse", two of the show's main characters, baby genius Stewie and anthropomorphic dog Brian, both voiced by series creator Seth MacFarlane, use an "out-of-this-world" remote control to travel through a series of various parallel universes. They eventually end up in a world where dogs rule and humans obey. Brian becomes reluctant to return to his own universe, and he ultimately ends up breaking the remote, much to the dismay of Stewie, who soon seeks a replacement. The "Road to" episodes which have aired throughout various seasons of Family Guy were inspired by the Road to ... comedy films starring Bing Crosby, Bob Hope and Dorothy Lamour, though this episode was not originally conceived as a "Road to" show.
 During the sixth season, episodes of Family Guy were delayed from regular broadcast due to the 2007–08 Writers Guild of America strike. MacFarlane, the series creator and executive producer, sided with the Writers Guild and participated in the strike until its conclusion. As a result, the seventh season consisted entirely of hold-overs. "Road to the Multiverse" was the first episode to be produced and aired after the strike ended. It was first announced at the 2008 San Diego Comic-Con International.
 Responses to the episode were highly positive; critics praised its storyline, numerous cultural references, and use of various animation styles. According to Nielsen ratings, it was watched by 10.17 million people during its original airing in the United States. The episode featured guest performances by Kei Ogawa, Kotaro Watanabe and Jamison Yang, along with several recurring guest voice actors for the series. Greg Colton won a Primetime Emmy Award for Individual Achievement in Animation, for storyboarding the episode, at the 62nd Primetime Emmy Awards. "Road to the Multiverse" was released on DVD along with seven other episodes from the season on June 15, 2010.
 As the Griffin family attend the county fair, Stewie announces that he has bred a winning pedigree pig for the local Quahog Clam Day. Revealing to Brian that he got the pig from a parallel universe, he shows him a remote control that allows access to the various parallel universes. Each universe depicts Quahog in the same time and place but under different conditions. Deciding to test the device, they both visit a universe where Christianity never existed, so the Dark Ages never occurred and thus humanity is 1000 years more technologically advanced (despite the existence of the Sistine Chapel in that universe). This leads an impressed Brian to ask whether the remote can take them to other alternative realities. Stewie guides them both through several more parallel universes, about half of which have their own portrayals of the Griffin family. As time passes, Brian begins to lose his amazement for the device and eventually comes to realize that Stewie has no idea how to return home.
 Continuing their explorations, they reach a universe where humans are subservient to dogs. Stewie finally figures out how to modify the remote device so that they can return home; but Brian, overwhelmed by the thought of a world run by dogs like himself, is reluctant to leave and steals the remote. Stewie and Brian fight over the device, ultimately breaking it, which traps them in the alternative universe. In desperation, the two go to the universe's version of the Griffin family – who are all dogs except for their pet Brian, who is human – hoping to find a solution. The dog version of Stewie quickly confronts the two, revealing that he has also developed a universe-traveling device that would allow them to return to their own universe. Before Dog Stewie can fetch them his remote control, Human Stewie bites the dog version of his father, Peter, out of anger for being treated like an animal and is sent to the pound where he is to be euthanized later that day. The two Brians and Dog Stewie go to the human pound to free him, and both Stewie and Brian are sent back to their original universe. As they are being transported, human Brian, dreaming of a better life in a world of intelligent inhabitants, leaps into the inter-universe portal at the last moment and successfully makes it to the original universe with the other two. Excited about his new prospects in life, human Brian begins his optimistic adventure in a brand new universe but is quickly struck by a car.
 The episode was first announced at the 2008 San Diego Comic-Con International in San Diego, California, on July 26, 2008.[2][3] It was written by series regular Wellesley Wild and directed by Greg Colton[4] shortly after the conclusion of the seventh production season, which consisted entirely of held-over episodes due to the 2007–2008 Writers Guild of America strike.[5][6] "Road to the Multiverse" is the fifth episode of the "Road to" hallmarks of the series, which have aired in various seasons of the show, and the second to be directed by Colton. The episodes are a parody of the seven Road to... comedy films starring Bing Crosby, Bob Hope and Dorothy Lamour.[7] Though it was not originally intended to be a "Road to" episode, Greg Colton convinced series creator and executive producer Seth MacFarlane and "Spies Reminiscent of Us" director Cyndi Tang to change the episode's title from "Sliders",[1] parodying the science fiction television series Sliders.[2] Colton's suggestion of the new title "Road to the Multiverse" was accepted, as was altering the premise of "Spies Reminiscent of Us", the season's original "Road to" episode.[8] Executive producer and former Star Trek: Enterprise writer[9] David A. Goodman, a fan of science fiction and the series Sliders, played a key role in the episode's original development. The production staff of Family Guy, including Wellesley Wild, watched an episode of Sliders before writing the show.[10] Series regulars Peter Shin and James Purdum served as supervising directors, with Andrew Goldberg and Alex Carter working as staff writers for the episode.[4] Composer Walter Murphy, who has worked on the series since its inception, returned to compose the music for "Road to the Multiverse".[4][11] Ron Jones and MacFarlane also contributed to the music and lyrics featured in the episode.[11]
 The episode features several examples of animation styles that differ greatly from the series' customary appearance. One such example involves the Disney universe, where the characters are drawn in the style of classic Walt Disney animated films. The sequence was animated entirely in Los Angeles by Main Street Productions, which were approached and recruited by series producer Kara Vallow to create the sequence,[12] rather than in South Korea where the show is normally animated.[13] MacFarlane described the scene as "a bit of challenge" and "kind of an experiment" since every character had to be completely redesigned based on the style of such films as "Pinocchio, Beauty and the Beast and Snow White and the Seven Dwarfs."[12][13] Another difference occurs in the dog universe, where the human characters are redrawn as dogs and Brian is redrawn as a human. MacFarlane found redesigning Brian easiest, simply giving him "a big nose and a collar."[13] In addition to traditional animation, the episode included a parody by Sarah E. Meyer, Eileen Kohlhepp, Kelly Mazurowskiof of Robot Chicken,[14] a stop motion series created by Family Guy cast member Seth Green for the Cartoon Network animation block Adult Swim.[15] Green did not take part in the making of the parody; it was instead animated by the Los Angeles company Screen Novelties, which had worked on the early seasons of Robot Chicken.[12]
 "Road to the Multiverse", along with the seven other episodes from Family Guy's eighth season, was released on a three-disc DVD set in the United States on June 15, 2010. The DVDs included brief audio commentaries by Seth MacFarlane, various crew and cast members from several episodes,[16] a collection of deleted scenes, a special mini-feature that discussed the process behind animating "Road to the Multiverse" and a mini-feature entitled Family Guy Karaoke.[17][18] The set also includes a reprint of the script for the episode.[19][20]
 In addition to the regular cast, Japanese actors Kei Ogawa, Kotaro Watanabe and Jamison Yang guest starred in the episode as Japanese-inspired versions of the Griffin family and Glenn Quagmire.[4] Recurring guest voice actor John G. Brennan reprised his recurring role as Mort Goldman and Adam West reprised his role as Mayor Adam West, who appears as an anthropomorphic mouse in the Disney universe. Minor appearances were made by writer and showrunner Steve Callaghan, actor Ralph Garman, writer and showrunner Mark Hentemann and writers Patrick Meighan, Danny Smith, Alec Sulkin and John Viener.[4]
 The episode opens with Stewie revealing his ability to travel across parallel universes to Brian. The first universe that they decide to visit, after having questioned the origin of Stewie's pedigree pig, is said to exist in a world where Christianity is absent.[21] In this universe, everything is seemingly years in advance of the 21st century; Quagmire is able to take a single pill and be instantly cured of the AIDS virus, and flying cars and buildings surround them.[22] As the two travel through the universe, they come upon Stewie's older sister Meg, who has become significantly more attractive. While they watch her walk down the street, the 1984 single "Drop Dead Legs" by Van Halen plays.[23] Playing on the nonexistence of Christianity, Brian and Stewie visit the Sistine Chapel and discover that a large collection of photos of American actress Jodie Foster has been substituted for The Creation of Adam painting by Michelangelo, who was fired and replaced by John Hinckley.[21]
 Seeking to explore more alternative realities, Stewie takes Brian to a universe resembling the 1960 animated sitcom The Flintstones.[24] Peter and his wife Lois are shown dressed in a manner similar to Fred Flintstone and Wilma Flintstone respectively.[24] Becoming tired of this universe, the two then transport themselves to a universe where the atomic bombing of Japan never occurred, allowing Japan to conquer the United States in World War II.[25]
 Another universe references many works by Walt Disney.[21] Meg appears as Ursula from the 1989 film The Little Mermaid and Herbert appears as the Queen from the 1937 film Snow White and the Seven Dwarfs. The rumor about Walt Disney being an anti-Semite is also referenced[26] by having the universe's occupants attack the Disney version of Mort Goldman when he enters a room, brutally beating him to death off-screen.[13][27] Discouraged, Brian and Stewie transport themselves to a universe resembling the Adult Swim series Robot Chicken,[14] a show co-created by Family Guy cast member Seth Green.[27] The sequence reveals several action figures of cartoon characters: He-Man, Optimus Prime, Lion-O and Duke from G.I. Joe.[28]
 Continuing their travels, the two come across a universe where singer and performer Frank Sinatra was never born,[29] resulting in the loss of the 1960 presidential election by President John F. Kennedy to then-Vice President Richard Nixon, which causes World War III.[21] Brian questions whether Lee Harvey Oswald shot Kennedy, and Stewie responds that he shot Mayor McCheese instead.[30] A sequence similar to the Zapruder film, which shows the assassination of Kennedy, is shown, with Jacqueline Kennedy also appearing.[30] Brian and Stewie next discover a universe completely depicted as a political cartoon.[21] The next reference occurs in the dog universe when Stewie says, "Take your stinking paws off me you damn, dirty dog!" which is a reference to the famous quote "Take your stinking paws off me you damn, dirty ape!" from the first Planet of the Apes movie. The final reference of the episode also occurs in the dog universe when Stewie mentions, "Gosh, Brian, I sure hope this next leap, will be the leap home," a nod to the opening narration of the time travel series Quantum Leap.[8]
 Ramsey Isler, IGN.[31]
 "Road to the Multiverse" was broadcast on September 27, 2009, as a part of the Animation Domination block on Fox, and was preceded by an episode of The Simpsons and the pilot episode of MacFarlane's new show The Cleveland Show.[32] It was followed by the season premiere of MacFarlane's other show American Dad!.[33] It was watched by 10.17 million viewers in its original airing, according to Nielsen ratings, despite being aired simultaneously with the season premiere of Desperate Housewives on ABC, the season premiere of The Amazing Race on CBS and Sunday Night Football on NBC. The episode also acquired a 5.2 rating in the 18–49 demographic, beating The Simpsons, The Cleveland Show and American Dad!, in addition to edging out all three shows in total viewership.[34] The episode's ratings were Family Guy's highest since the airing of the season six episode "McStroke".[35] The episode's first broadcast in Canada, on Global TV, was watched by 1.29 million viewers, making it first for its timeslot in the week it was broadcast.[36]
 "Road to the Multiverse" received critical acclaim, with one calling the storyline "right up there with the best of the early episodes we've seen on the series."[25] In a simultaneous review of the episodes of The Simpsons and American Dad! that preceded and followed the episode respectively and The Cleveland Show pilot, The A.V. Club's Emily VanDerWerff commented that she felt "essentially predisposed to like" the episode, adding that she enjoyed the entire theme of the show, in addition the fact that it was more than just science fiction. In the conclusion of her review VanDerWerff called the episode a "solid start to the eighth season" and rated it as a B+, the best rating between The Simpsons episode "Homer the Whopper", the American Dad! episode "In Country...Club" and The Cleveland Show's series premiere.[21] Ahsan Haque of IGN gave the episode a 9.6 out of 10, saying that the episode featured "plenty of memorable lines, some truly stunning animation ... and a relentless non-stop barrage of witty jokes."[25] In a subsequent review in January 2010 of "Stewie and Brian's Greatest Adventures", Haque called the episode "creative, visually impressive, and features some of the best random gags we've seen on the show in a long time."[37] In 2019, to celebrate the show's 20th anniversary, IGN published a list of the 20 best Family Guy episodes, with "Road to the Multiverse" ranked the fourth best.[38] Television critic Alex Rocha of TV Guide also found the episode to have "great laughs," saying that the show is "definitely off to a great start" to a new season.[27] Tom Eames of entertainment website Digital Spy placed the episode at number one on his listing of the best Family Guy episodes in order of "yukyukyuks" and described the episode as "another crazy Brian and Stewie adventure".[39] He noted that the story did not have "the most interesting plot", but got "more and more entertaining and so much fun" with every new world they featured in. He concluded that "coupled with Brian and Stewie front and centre, it made for the best Family Guy episode ever."[39] The director of "Road to the Multiverse", Greg Colton, was awarded the Primetime Emmy Award for Outstanding Individual Achievement in Animation, for storyboarding the episode, on August 21, 2010, at the 62nd Primetime Emmy Awards's Creative Arts Awards.[40]
 Although the Parents Television Council, a frequent Family Guy critic, did not name Family Guy its "Worst TV Show of the Week" for "Road to the Multiverse", it did refer to this episode in its negative review of the following episode, "Family Goy". The review noted that the appearance of the Griffin family's Jewish neighbor, Mort Goldman, in "Multiverse" was notable since, in the Disney parody, Mort was beaten to a bloody pulp by Disney-inspired versions of the cast – a reference to Walt Disney's purported antisemitism. The review goes on to state, "apparently, in Seth MacFarlane's mind, the best way to fight anti-Semitism is with more anti-Semitism. One must wonder what young, angry, disaffected bigots tuning into the show must think. All they see is a nebbish stereotype getting his teeth knocked out of his skull and a blood-soaked Star of David tumbling to the floor."[41]
 In a 2012 interview, Seth MacFarlane stated: "As far as the all-around best episode, "Road to the Multiverse" would have to be up there."[42]
 A video game sequel called Family Guy: Back to the Multiverse was made. It is also a continuation of the season 9 episode "The Big Bang Theory".
 
 1 Plot 2 Production and development 3 Cultural references 4 Reception 5 Sequel 6 References 7 External links ^ a b "20th Century Fox – Fox In Flight – Family Guy". 20th Century Fox. Archived from the original on 2011-07-11. Retrieved 2010-04-26..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b Keller, Richard (2008-07-26). "American Dad and Family Guy – Comic-Con Report". TV Squad. AOL, Inc. Retrieved 2010-07-05.
 ^ "A Look Back at Comic Con 2008". Comic-Con International: San Diego. Archived from the original on 2009-07-25. Retrieved 2010-07-05.
 ^ a b c d e "Family Guy – Road to the Multiverse – Cast and Crew". Yahoo!. Archived from the original on 2011-06-15. Retrieved 2010-04-04.
 ^ "Pencils Down". Writers Guild of America, West. Archived from the original on 2007-12-19. Retrieved 2007-12-20.
 ^ Haque, Ahsan (2008-05-13). "Family Guy: Season 6 Review". IGN. Retrieved 2010-06-28.
 ^ P., Ken. "Interview with Seth MacFarlane". IGN. Retrieved 2009-12-09.
 ^ a b MacFarlane, Seth (2010-06-15). Family Guy Volume Eight Audio Commentary (DVD). 20th Century Fox.
 ^ Spelling, Ian (2009-03-26). "How the Trek: Next Generation cast beams into Family Guy". Sci Fi Wire. Syfy. Archived from the original on 2010-01-28. Retrieved 2010-06-30.
 ^ Goodman, David (2010-06-15). Family Guy Volume Eight Audio Commentary (DVD). 20th Century Fox.
 ^ a b Brandquist, Lisa (2009-10-12). "Sundays Go To "Family Guy" Creator Seth MacFarlane". The Excelsior. Retrieved 2010-07-11.
 ^ a b c Colton, Greg (2010-06-15). Road to "Road to the Multiverse" (DVD). 20th Century Fox.
 ^ a b c d Turner, John (2009-09-22). "You won't believe where Family Guy goes in its sci-fi premiere". Sci-Fi Wire. Syfy. Archived from the original on 2010-01-04. Retrieved 2009-12-27.
 ^ a b Wild, Wellesley; Steve Callaghan; David A. Goodman; Mark Hentemann; Seth MacFarlane; Chris Sheridan; Danny Smith (2010). Family Guy – Road to the Multiverse script. 20th Century Fox. p. 22. Stewie and Brian reappear in a stop-motion "Robot Chicken" world. Action figures of the Griffins sit on the couch. Stewie and Brian are also action figures.
 ^ Firecloud, Johnny (2010-01-29). "Adult Swim's 'Robot Chicken' Renewed For Two More Seasons". CraveOnline. AtomicOnline. Archived from the original on 2010-02-02. Retrieved 2010-07-29.
 ^ Lambert, David (2010-03-24). "Family Guy – This Just In: Volume 8 DVD Announced to Retailers, with Complete Details". TVShowsonDVD.com. Archived from the original on 2010-03-29. Retrieved 2010-04-03.
 ^ Lieberman, Joe (2010-06-16). "Family Guy – Volume Eight DVD Review". IGN. Retrieved 2010-06-30.
 ^ McCutcheon, David (2010-05-19). "Family Guy V8 Drops In". IGN. Retrieved 2010-06-30.
 ^ Lambert, David (2010-05-18). "Family Guy – Fox Provides Press Release with Complete Volume 8 DVD Details". 20th Century Fox. TVShowsonDVD.com. Archived from the original on 2010-06-23. Retrieved 2010-06-26.
 ^ Kirkland, Bruce (2010-06-17). "MacFarlane 'toons jump to DVD". Toronto Sun. Retrieved 2010-06-30.
 ^ a b c d e f VanDerWerff, Emily (September 28, 2009). ""Homer the Whopper"/"Pilot"/"Road to the Multiverse"/"In Country...Club"". The A.V. Club. The Onion, Inc. Retrieved 16 September 2019.
 ^ Wild, Wellesley; Steve Callaghan; David A. Goodman; Mark Hentemann; Seth MacFarlane; Chris Sheridan; Danny Smith (2010). Family Guy – Road to the Multiverse script. 20th Century Fox. p. 6. Stewie and Brian suddenly reappear in a futuristic-looking version of Quahog. There are flying cars, people with jet packs, etc. [...] Quagmire (cont'd) Ohp, I got AIDS again. Better take my "Nyquil Cold, Flu and AIDS." (Takes a pill) All gone.
 ^ Wild, Wellesley; Steve Callaghan; David A. Goodman; Mark Hentemann; Seth MacFarlane; Chris Sheridan; Danny Smith (2010). Family Guy – Road to the Multiverse script. 20th Century Fox. p. 7. Brian turns and looks and his eyes widen. Angle on attractive female legs walking in high heels on the sidewalk as "Drop Dead Legs" by Van Halen plays.
 ^ a b Wild, Wellesley; Steve Callaghan; David A. Goodman; Mark Hentemann; Seth MacFarlane; Chris Sheridan; Danny Smith (2010). Family Guy – Road to the Multiverse script. 20th Century Fox. p. 9. Stewie and Brian reappear in a world drawn in the Hanna-Barbera style of "The Flintstones." [...] A Flintstone version of Peter stands talking to a Flintstone version of Lois.
 ^ a b c Haque, Ahsan (2009-09-25). "Family Guy: "Road to the Multiverse" Review". IGN. Retrieved 2009-09-28.
 ^ Dakss, Brian (2006-11-01). "Walt Disney: More Than 'Toons, Theme Parks". CBS News. Retrieved 2006-06-29.
 ^ a b c Rocha, Alex (2009-09-28). "Family Guy Episode Recap: "Road to Multiverse"". TV Guide. Archived from the original on 2010-03-05. Retrieved 2009-11-20.
 ^ Wild, Wellesley; Steve Callaghan; David A. Goodman; Mark Hentemann; Seth MacFarlane; Chris Sheridan; Danny Smith (2010). Family Guy – Road to the Multiverse script. 20th Century Fox. p. 23. Duke from "G.I. Joe", Optimus Prime from "Transformers", Lion-O from "Thundercats", and He-Man from "He-Man" all shuffle in.
 ^ Wild, Wellesley; Steve Callaghan; David A. Goodman; Mark Hentemann; Seth MacFarlane; Chris Sheridan; Danny Smith (2010). Family Guy – Road to the Multiverse script. 20th Century Fox. p. 24. It says that in this universe, Frank Sinatra was never born, and therefore he was unable to use his influence to get Kennedy elected. So, Nixon won the 1960 election, and totally botched the Cuban Missile Crisis, causing World War Three.
 ^ a b Wild, Wellesley; Steve Callaghan; David A. Goodman; Mark Hentemann; Seth MacFarlane; Chris Sheridan; Danny Smith (2010). Family Guy – Road to the Multiverse script. 20th Century Fox. p. 24. Mayor McCheese and Jackie Onassis ride in an open Lincoln.
 ^ Isler, Ramsey (2010-06-02). "Family Guy: Season 8 Review". IGN. Retrieved 2010-06-29.
 ^ Weiss, Joanna (2009-09-26). "For 'Family' fans, 'Cleveland' rocks". The Boston Globe. Retrieved 2010-06-30.
 ^ Goldman, Eric (2009-06-15). "Fringe, Cleveland Show Fall Debut Dates". IGN. Retrieved 2010-06-30.
 ^ Gorman, Bill (2009-09-28). "Updated TV Ratings: Sunday Night Football Wins; Cleveland Show Large; Housewives Down". TV by the Numbers. Archived from the original on 2009-10-04. Retrieved 2009-12-15.
 ^ Calabria, Rosario T. (2008-01-14). "Broadcast TV Ratings for Sunday, January 13, 2008". Your Entertainment Now. Retrieved 2010-03-21.
 ^ "Top Programs – Total Canada September 21 – September 27, 2009" (PDF). BBM Canada. 2009. Archived from the original (PDF) on July 6, 2011. Retrieved 2010-06-29.
 ^ Haque, Ahsan (2010-01-11). "Family Guy: Stewie and Brian's Greatest Adventures". IGN. Retrieved 2010-08-14.
 ^ "Top 20 Family Guy episodes". IGN. January 31, 2019. Retrieved September 21, 2019.
 ^ a b Eames, Tom (19 March 2017). "The 16 best ever Family Guy episodes in order of yukyukyuks". Digital Spy. Retrieved 19 March 2017.
 ^ "2010 Creative Arts Emmy Winners Press Release" (PDF). Academy of Motion Picture Arts and Sciences. 2010-08-22. Retrieved 2010-08-22.
 ^ "Parents Television Council – "Family Guy" on Fox". Worst TV Show of the Week. Parents Television Council. 2009-10-09. Archived from the original on 2010-01-06. Retrieved 2009-10-13.
 ^ "Seth MacFarlane on 'Family Guy's' Future, Jon Stewart's Advice and the Man Who Stormed the Writers Room (Q&A)". The Hollywood Reporter. 2012-11-09. Retrieved 2012-11-11.
 Television portal "Road to the Multiverse" on IMDb "Road to the Multiverse" at TV.com v t e "Road to the Multiverse" "Family Goy" "Spies Reminiscent of Us" "Brian's Got a Brand New Bag" "Hannah Banana" "Quagmire's Baby" "Jerome Is the New Black" "Dog Gone" "Business Guy" "Big Man on Hippocampus" "Dial Meg for Murder" "Extra Large Medium" "Go, Stewie, Go!" "Peter-assment" "Brian Griffin's House of Payne" "April in Quahog" "Brian & Stewie" "Quagmire's Dad" "The Splendid Source" "Something, Something, Something, Dark Side" "Partial Terms of Endearment" Road to... Star Wars episodes  Category  Book v t e "Road to Rhode Island" "Road to Europe" "Road to Rupert" "Road to Germany" "Road to the Multiverse" "Road to the North Pole" "Roads to Vegas" "Road to India" 2009 American television episodes Family Guy (season 8) episodes Road to... (Family Guy) Television episodes about parallel universes Disney parodies Television episodes with live action and animation Robot Chicken Articles with short description Television episode articles with short description for single episodes Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Español Русский  This page was last edited on 29 October 2019, at 08:42 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Road to the Multiverse Previous Next Road to the Multiverse a b a b ^ a b c d e ^ ^ ^ a b ^ ^ a b a b c a b c d a b ^ ^ ^ ^ ^ ^ a b c d e f ^ ^ a b a b c ^ a b c ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ "Road to the Multiverse" Category Book 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 Seasons
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18 Family Guy episode From left to right: Brian, Peter, Stewie and Quagmire as a bird in the Disney universe sequence. Season 8Episode 1 Greg Colton Wellesley Wild 7ACX06[1] September 27, 2009 (2009-09-27) 
Kei Ogawa as Japanese Lois and MegKotaro Watanabe as Japanese Brian and QuagmireJamison Yang as Japanese Chris, Stewie and Peter

 


← Previous"Peter's Progress"

Next →"Family Goy"
 ← Previous"Peter's Progress"
 Next →"Family Goy"
 Family Guy (season 8) List of Family Guy episodes  Wikiquote has quotations related to: "Road to the Multiverse" 
Seasons
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
 
"Road to the Multiverse"
"Family Goy"
"Spies Reminiscent of Us"
"Brian's Got a Brand New Bag"
"Hannah Banana"
"Quagmire's Baby"
"Jerome Is the New Black"
"Dog Gone"
"Business Guy"
"Big Man on Hippocampus"
"Dial Meg for Murder"
"Extra Large Medium"
"Go, Stewie, Go!"
"Peter-assment"
"Brian Griffin's House of Payne"
"April in Quahog"
"Brian & Stewie"
"Quagmire's Dad"
"The Splendid Source"
"Something, Something, Something, Dark Side"
"Partial Terms of Endearment"
 
Road to...
Star Wars episodes
 
 Category
 Book
 
"Road to Rhode Island"
"Road to Europe"
"Road to Rupert"
"Road to Germany"
"Road to the Multiverse"
"Road to the North Pole"
"Roads to Vegas"
"Road to India"
 