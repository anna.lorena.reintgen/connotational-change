
 
 The Yugoslav monitor Drava was a river monitor operated by the Royal Yugoslav Navy between 1921 and 1941. She was originally built for the Austro-Hungarian Navy as the name ship of the Enns-class river monitors. As SMS Enns, she was part of the Danube Flotilla during World War I, and fought against the Serbian and Romanian armies from Belgrade to the lower Danube. In October 1915, she was covering an amphibious assault on Belgrade when she was holed below the waterline by a direct hit, and had to be towed to Budapest for repairs. After brief service with the Hungarian People's Republic at the end of the war, she was transferred to the newly created Kingdom of Serbs, Croats and Slovenes (later Yugoslavia), and renamed Drava. She remained in service throughout the interwar period, but was not always in full commission due to budget restrictions.
 During the German-led Axis invasion of Yugoslavia in April 1941, Drava spent six days shelling airfields near Mohács in Hungary and fought off a small flotilla of Hungarian gunboats. On 12 April, she was attacked by Junkers Ju 87 Stuka dive bombers of the Luftwaffe. The anti-aircraft gunners on the ship claimed three enemy aircraft, but nine of the Stukas scored hits. Most of these had little effect, but the last bomb dropped straight down Drava's funnel and exploded in her engine room, killing 54 of the crew, including her captain, Aleksandar Berić. Only 13 of the crew survived. She was raised and scrapped by Hungary during their occupation of parts of Yugoslavia. Berić was posthumously awarded the Order of Karađorđe's Star for his sacrifice, and the base of the Serbian River Flotilla at Novi Sad is named after him.
 SMS Enns was constructed for the Austro-Hungarian Navy as the name ship of the Enns-class river monitors by Schiffswerft Linz and Stabilimento Tecnico Triestino (STT). She was laid down by Schiffswerft Linz at Linz on 21 November 1913,[1] as part of the Austro-Hungarian 1912 Naval Program.[2] She was named after the River Enns, a tributary of the Danube. When World War I broke out in July 1914, construction of Enns was well advanced, but after Schiffswerft Linz was taken over by STT, her machinery was ordered from the STT works near Trieste. On 2 August, the machinery was dispatched by train to Linz and installation began without delay.[1] She was launched in September 1914, and completed on 17 October of that year.[3] Despite the requirement that Enns and SMS Inn be constructed as sister ships, and the fact that their size and armament were identical, there were significant design differences between the two vessels, as they were constructed by completely independent shipbuilding companies. Enns had an overall length of 60.2 m (197 ft 6 in),[a] a beam of 10.3 m (33 ft 10 in), and a normal draught of 1.3 m (4 ft 3 in). Her standard displacement was 536 tonnes (528 long tons), and her crew consisted of 95 officers and enlisted men.[5] She had two triple-expansion steam engines, each driving a single propeller shaft. Steam for the engines was provided by two Yarrow water-tube boilers,[1] and her engines were rated at 1,500 indicated horsepower (1,100 kW), with a maximum of 1,700 ihp (1,300 kW). She was designed to reach a top speed of 13 knots (24 km/h; 15 mph),[4] and carried 70 tonnes (69 long tons) of fuel oil.[2]
 Enns was armed with one twin gun turret of 120 mm (4.7 in)L/45[b] guns mounted forward, and three single 120 mm (4.7 in) L/10 howitzer turrets mounted on the aft deck. On the upper deck there were two single 66 mm (2.6 in) L/50 anti-aircraft guns, one on the port side forward of the funnel, and one on the starboard side to the rear of the funnel. She was also equipped with six 8 mm (0.31 in) machine guns.[6] The maximum range of her Škoda 120 mm (4.7 in) L/45 guns was 15 km (9.3 mi), and her howitzers could fire their 20 kg (44 lb) shells a maximum of 6.2 kilometres (3.9 mi).[7] Her armour consisted of belt and bulkheads  40 mm (1.6 in) thick and deck armour 25 mm (0.98 in) thick, and her conning tower and gun turrets were 50 mm (2.0 in) thick.[3]
 The original plans called for open anti-aircraft mounts, but the experience of the existing monitors in the first battles on the Danube against Serbia demonstrated that the mounts needed protection from small arms fire, so armoured barbettes were added. These modifications prevented the crew in the conning tower from viewing directly aft of the ship, so a 1.3 m (4 ft 3 in) high rectangular platform was placed on top of the conning tower that afforded a view aft. To avoid any increase in her draught from these modifications, her hull was lengthened. Due to the urgent need for the ship to be put into service, the planned telescopic mast was not installed, and an alternative mast was constructed using angle iron lattice.[1] Enns was launched on 29 July 1914 and commissioned on 17 October 1914.[1]
 Soon after Enns was commissioned into the Danube Flotilla she was in action against Serbian forces at Belgrade,[8] under the command of Linienschiffsleutnant[c] (LSL) Richard Funk.[10] French artillery support arrived in Belgrade in November, endangering the monitor's anchorage, with Enns being the only monitor in the flotilla with the range to match the French guns.[8] On 21 November, Enns engaged the French battery in an artillery duel at a range of 10 km (6.2 mi).[10] This stalemate continued until the following month when the Serbs briefly evacuated Belgrade in the face of an Austro-Hungarian assault. After less than two weeks, the Austrians had to withdraw from Belgrade, and it was soon re-occupied by the Serbs, reinforced by the Russians and French. Enns continued in action against Serbia and her allies at Belgrade until late December, when the monitor base was withdrawn to Petrovaradin for the winter.[11]
 In January 1915, British artillery arrived in Belgrade, further bolstering its defences.[12] In mid-February, the monitors redeployed to an anchorage at Zemun.[10] Following the commencement of the Gallipoli campaign, munitions supply to the Ottomans became critical, and despite the failure of an earlier attempt to ship arms and ammunition down the Danube, another attempt was planned. On 30 March, the steamer Belgrad left Zemun, escorted by Enns and the monitor SMS Bodrog. The convoy was undetected as it sailed past Belgrade at night during a storm, but after the monitors returned to base, the steamer struck a mine near Vinča, and after coming under heavy artillery fire, exploded near Ritopek.[13] On 22 April 1915, a British picket boat that had been brought overland by rail from Salonika was used to attack the Danube Flotilla anchorage at Zemun, firing two torpedoes without success.[14] In September 1915, the Central Powers were joined by Bulgaria, and the Serbian Army soon faced overwhelming Austrian and German ground troops. In early October, the Austro-Hungarian Third Army attacked Belgrade, and Enns, along with the majority of the flotilla, was heavily engaged in support of the crossings near the Belgrade Fortress and Ada Ciganlija island.[15] During the final river crossing and support of the resulting bridgehead, Enns was near Grosser Krieg Island on 8 October when she received a direct hit below the waterline and her 120 mm (4.7 in) magazine flooded. She was towed out of danger by the armed steamer Almos, and was eventually hauled to Budapest where she was repaired.[16] During repairs, the barbettes on her upper deck were replaced with turrets for the anti-aircraft guns. Although the howitzers had not been particularly successful, a plan to replace the aft howitzer mount with a turret similar to that used on SMS Bodrog was not carried out.[1]
 When she returned to the flotilla after repairs, she saw action at Rjahovo in early October 1916, where she contributed to the defeat of the Romanian Flămânda Offensive.  The Romanian attempt to cross the Danube to attack the rear of Generalfeldmarschall[d] August von Mackensen's Austro-Hungarian Third Army was thwarted,[18] and a force consisting of Enns, the monitors SMS Leitha, SMS Temes (II) and SMS Szamos, the patrol boat Viza and the armed steamer Balaton destroyed a pontoon bridge near Rjahovo. After forays against Giurgiu to secure trains loaded with coal and oil, in November Enns and other ships supported the crossing of the Danube by von Mackensen's army at Sistow. The following month, Enns bombarded Căscioarele, driving enemy troops out of the village. From late December 1916 to mid-March 1917, Enns and other ships of the flotilla wintered at Turnu Severin.[10]
 In March 1917, Enns relocated to Brăila in eastern Romania, where it remained until July 1918. Sent to Linz and Budapest for an overhaul in dry dock, Enns then returned to eastern Romania and was stationed at Reni where she met a group of monitors and patrol boats that had been operating against Russia in the Black Sea.[10] In October 1918, the Danube Flotilla was under serious threat of being cut off in the lower Danube by French forces after the Bulgarians concluded an armistice with the British and French. After the steamer Croatia was fired on by the French as it tried to get past Lom, she cut her tow line, releasing seven lighters, which ran aground on a sandbank. Croatia was hit, suffered casualties and grounded on the Romanian side of the river. The French retrieved three of the lighters and towed them into the anchorage at Lom. The following day, Enns and two other monitors managed to release three of the remaining lighters while under heavy French fire, and towed them upstream.[19] The flotilla continued to retreat up the Danube, running the gauntlet of French and Serbian forces. With the dissolution of Austria-Hungary in early November, the South Slavs went ashore at Vukovar. The Austrian, Hungarian and Czech crew members of the flotilla continued on their journey, arriving in Budapest on 6 November,[20] and Enns began flying the Hungarian flag,[10] as part of the navy of the Hungarian People's Republic.[21] On 8 December, the monitors were seized by the Allies, and less than two weeks later Enns was towed to Belgrade where she was handed over to Serbia to be maintained on behalf of the new Kingdom of Serbs, Croats and Slovenes (KSCS, later the Kingdom of Yugoslavia).[10]
 Immediately after the armistice, Enns was crewed by sailors of the KSCS in 1918–19. Under the terms of the Treaty of Saint-Germain-en-Laye concluded in September 1919, Enns was transferred to the KSCS along with a range of other vessels, including three other river monitors,[22] and was officially handed over to the KSCS navy and renamed Drava in 1920.[23] Her sister ship Inn was transferred to Romania and renamed Besarabia.[3] In 1925–26, Drava was refitted, but by the following year only two of the four river monitors of the KSCS Navy were being retained in full commission at any time.[24] In 1932, the British naval attaché reported that Yugoslav ships were engaging in little gunnery training, and few exercises or manoeuvres, due to reduced budgets.[25]
 Drava was based at Bezdan under the command of Aleksandar Berić,[e] when the German-led Axis invasion of Yugoslavia began on 6 April 1941. She was assigned as flagship of the 1st Mine Barrage Division,[27] and was responsible for the Hungarian border on the Danube, under the operational control of the 30th Infantry Division Osiječka,[28] which was part of the 2nd Army.[29] Drava steamed upstream to Mohács in Hungary to shell the airfield there on 6 and 8 April,[30] but was subjected to daily attacks by the Luftwaffe.[31] On 10 April, Drava and her fellow monitor Morava were ordered to sail downstream to conform with the withdrawals of the 1st and 2nd Army's from Bačka and Baranja.[29] About 14:00 the following day, a Yugoslav lookout near Batina signalled Drava that a group of four Hungarian patrol boats, armed with 70 mm (2.8 in) guns, was coming down the Danube from the direction of Mohács. Drava engaged the patrol boats at a range of 6–7 km (3.7–4.3 mi), and drove the small Hungarian flotilla north again. Berić followed this up at 16:00 by once again shelling the airfield at Mohács. Morale on the ship was good, but when Berić met with Army elements later that day he became aware of the situation elsewhere, and nine crew deserted.[32]
 Early on 12 April, with the other three monitors having been scuttled the night before, Drava was attacked by Junkers Ju 87 Stuka dive bombers of Sturzkampfgeschwader 77 flying from Arad, Romania. The anti-aircraft gunners on the ship claimed three enemy aircraft, and nine of the Stukas scored hits on her, most of which had little effect. However, the last bomb dropped straight down her funnel and exploded in her engine room, killing 54 of the crew. Only 13 survived,[30] and she sank off Čib.[31] Having ordered the burning of codes before she sank, Berić and his first officers were among the dead, but two of the successful anti-aircraft gunners, Rade Milojević and Miroslav Šurdilović, survived.[33] During their occupation of parts of Yugoslavia, Drava was raised and then scrapped by Hungary.[34] Berić was posthumously awarded the Order of Karađorđe's Star for his sacrifice. In April 2015, a bust of Berić was unveiled at the village of Belegiš, near Stara Pazova.[35] The barracks of the Serbian River Flotilla in Novi Sad are also named after him.[33]
 1,700 ihp (1,300 kW) Yarrow boilers 2 × 120 mm (4.7 in) L/45 guns (1 × 2) 3 × 120 mm (4.7 in) L/10 howitzers (3 × 1) 2 × 66 mm (2.6 in) L/50 anti-aircraft guns (2 × 1) 6 × 8 mm (0.31 in) machine guns Belt and bulkheads: 40 mm (1.6 in) Deck: 25 mm (0.98 in) Conning tower and gun turrets: 50 mm (2.0 in) 1 Description and construction 2 Career

2.1 World War I
2.2 Interwar period and World War II

 2.1 World War I 2.2 Interwar period and World War II 3 Notes 4 Footnotes 5 References

5.1 Books
5.2 News
5.3 Websites

 5.1 Books 5.2 News 5.3 Websites ^ According to Greger, her overall length was 57.9 m (190 ft).[4]
 ^ L/45 denotes the length of the gun. In this case, the L/45 gun is 45 calibre, meaning that the gun was 45 times as long as the diameter of its bore.
 ^ Equivalent to a Austro-Hungarian Army Hauptman (captain).[9]
 ^ Equivalent to a British Army field marshal.[17]
 ^ His rank was Poručnik bojnog broda, equivalent to a United States Navy lieutenant commander.[26]
 ^ a b c d e f Pawlik, Christ & Winkler 1989, p. 66.
 ^ a b Jane's Information Group 1989, p. 315.
 ^ a b c Greger 1976, p. 142.
 ^ a b Greger 1976, p. 141.
 ^ Pawlik, Christ & Winkler 1989, pp. 66–67.
 ^ Pawlik, Christ & Winkler 1989, pp. 67 & 69.
 ^ Greger 1976, pp. 9–10.
 ^ a b Halpern 2012, p. 265.
 ^ Deak 1990, Introduction.
 ^ a b c d e f g Pawlik, Christ & Winkler 1989, p. 67.
 ^ Halpern 2012, pp. 265–266.
 ^ Halpern 2012, p. 266.
 ^ Halpern 2012, p. 267.
 ^ Halpern 2012, pp. 270–271.
 ^ Halpern 2012, p. 272.
 ^ Halpern 2012, p. 273.
 ^ Mombauer 2001, p. xv.
 ^ Halpern 2012, p. 281.
 ^ Halpern 2012, p. 285.
 ^ Halpern 2012, pp. 285–286.
 ^ Csonkaréti & Benczúr 1992, p. 123.
 ^ Gardiner 1985, p. 422.
 ^ Gardiner 1985, p. 426.
 ^ Jarman 1997a, p. 732.
 ^ Jarman 1997b, p. 451.
 ^ Niehorster 2013b.
 ^ Niehorster 2013a.
 ^ Terzić 1982, p. 168.
 ^ a b Terzić 1982, p. 375.
 ^ a b Shores, Cull & Malizia 1987, p. 224.
 ^ a b Fitzsimons 1977, p. 843.
 ^ Terzić 1982, p. 392.
 ^ a b Vujičić & 15 August 2014.
 ^ Chesneau 1980, p. 357.
 ^ Radio Television of Serbia & 6 April 2015.
 Chesneau, Roger, ed. (1980). Conway's All the World's Fighting Ships, 1922–1946. London, England: Conway Maritime Press. ISBN 978-0-85177-146-5..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} Csonkaréti, Károly; Benczúr, László (1992). Haditengerészek és folyamőrök a Dunán: a császari (és) királyi haditengerészet dunaflottillájától a magyar királyi honvéd folyamerőkig (1870–1945) [Naval Guards on the Danube River and the Danube Flotilla of the Royal Hungarian Navy (1870–1945)] (in Hungarian). Budapest, Hungary: Zrínyi Kiadó. ISBN 978-963-327-153-7. Deak, Istvan (1990). Beyond Nationalism: A Social and Political History of the Habsburg Officer Corps, 1848–1918. New York, New York: Oxford University Press. ISBN 978-0-19-992328-1. Fitzsimons, Bernard, ed. (1977). The Illustrated Encyclopedia of 20th Century Weapons and Warfare. 8. New York, New York: Columbia House. OCLC 732716343. Gardiner, Robert, ed. (1985). Conway's All the World's Fighting Ships, 1906–1921. London, England: Conway Maritime Press. ISBN 978-0-85177-245-5. Greger, René (1976). Austro-Hungarian Warships of World War I. London, England: Allan. ISBN 978-0-7110-0623-2. Halpern, Paul G. (2012). A Naval History of World War I. Annapolis, Maryland: Naval Institute Press. ISBN 978-0-87021-266-6. Jane's Information Group (1989) [1946/47]. Jane's Fighting Ships of World War II. London, England: Studio Editions. ISBN 978-1-85170-194-0. Jarman, Robert L., ed. (1997a). Yugoslavia Political Diaries 1918–1965. 1. Slough, Berkshire: Archives Edition. ISBN 978-1-85207-950-5. Jarman, Robert L., ed. (1997b). Yugoslavia Political Diaries 1918–1965. 2. Slough, Berkshire: Archives Edition. ISBN 978-1-85207-950-5. Mombauer, Annika (2001). Helmuth von Moltke and the Origins of the First World War. Cambridge, New York: Cambridge University Press. ISBN 978-0-521-79101-4. Pawlik, Georg; Christ, Heinz; Winkler, Herbert (1989). Die K.u.K. Donauflottille 1870–1918 [The K.u.K. Danube Flotilla 1870–1918] (in German). Graz, Austria: H. Weishaupt Verlag. ISBN 978-3-900310-45-5. Shores, Christopher F.; Cull, Brian; Malizia, Nicola (1987). Air War for Yugoslavia, Greece, and Crete, 1940–41. London, England: Grub Street. ISBN 978-0-948817-07-6. Terzić, Velimir (1982). Slom Kraljevine Jugoslavije 1941: Uzroci i posledice poraza [The Collapse of the Kingdom of Yugoslavia in 1941: Causes and Consequences of Defeat] (in Serbo-Croatian). 2. Belgrade, Yugoslavia: Narodna knjiga. OCLC 10276738. Vujičić, Dragan (15 August 2014). "Poslednje zbogom poručnika Berića" [Last Farewell to Lieutenant Berić]. Novosti. "Otkriven spomenik Aleksandru Beriću" [Monument to Aleksandar Berić Unveiled]. Radio Television of Serbia. 6 April 2015. Niehorster, Leo (2013a). "Royal Yugoslav Armed Forces Ranks". Leo Niehorster. Retrieved 14 May 2015. Niehorster, Leo (2013b). "Balkan Operations Order of Battle Royal Yugoslavian Navy River Flotilla 6th April 1941". Leo Niehorster. Retrieved 14 May 2015. v t e Drava Morava Sava Vardar v t e 4 Apr: Lichtenfels, Liebenfels, Proussa 5 Apr: U-76 7 Apr: Clan Fraser 12 Apr: Drava, Morava, Sava, Vardar 13 Apr: Corinthic, HMS Rajputana 16 Apr: HMS Mohawk 17 Apr: Zagreb 20 Apr: Empire Endurance, Ithaki, Psara, Vasilefs Georgios 21 Apr: Thyella 22 Apr: Frinton, Hydra, Sea-Serpent 23 Apr: Alberta, Kilkis/ex-USS Mississippi, Kios, Lemnos 25 Apr: Kyzikos, Pergamos, Pittsburgh 26 Apr: Kydoniai 27 Apr: HMS Diamond, Slamat (disaster), HMS Wryneck 28 Apr: U-65 29 Apr: City of Nagpur Unknown date: HMS Usk 12 Apr: Siboney 24 Apr: HMS Rover 30 Apr: HMS Fermoy 1914 ships Ships built in Austria-Hungary World War I naval ships of Austria-Hungary World War I monitors World War II monitors Maritime incidents in April 1941 Ships sunk by German aircraft World War II naval ships of Yugoslavia Ships of the Royal Yugoslav Navy Riverine warfare Articles with short description Featured articles Use dmy dates from August 2014 Pages using deprecated image syntax CS1 Hungarian-language sources (hu) CS1 German-language sources (de) CS1 Serbo-Croatian-language sources (sh) Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Magyar Српски / srpski Українська  This page was last edited on 9 April 2019, at 15:47 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Yugoslav monitor Drava SMS Enns ^ ^ ^ ^ ^ a b c d e f a b a b c a b ^ ^ ^ a b ^ a b c d e f g ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b a b a b ^ a b ^ ^ 8 1 2 2 Sister ship to Enns, SMS Inn
 Name: 
Enns Namesake: 
Enns River Builder: 
Schiffswerft Linz / Stabilimento Tecnico Triestino Linz Laid down: 
21 November 1913 Launched: 
29 July 1914 In service: 
17 October 1914 Out of service: 
6 November 1918 Fate: 
Transferred to the Hungarian People's Republic Notes: 
Sister ship Inn was ceded to Romania and renamed Besarabia Name: 
Enns Namesake: 
Enns River Acquired: 
6 November 1918 Out of service: 
8 December 1918 Fate: 
Assigned to the Kingdom of Serbs, Croats and Slovenes (KSCS) Name: 
Drava Namesake: 
Drava River Acquired: 
1920 Fate: 
Sunk by Luftwaffe dive bombers on 12 April 1941 Class and type: 
Enns-class river monitor Displacement: 
536 tonnes (528 long tons) Length: 
60.2 m (197 ft 6 in) Beam: 
10.3 m (33 ft 10 in) Draught: 
1.3 m (4 ft 3 in) Installed power: 

1,700 ihp (1,300 kW)
Yarrow boilers
 Propulsion: 
2 Triple-expansion steam engines Speed: 
13 knots (24 km/h; 15 mph) Complement: 
95 officers and enlisted Armament: 

2 × 120 mm (4.7 in) L/45 guns (1 × 2)
3 × 120 mm (4.7 in) L/10 howitzers (3 × 1)
2 × 66 mm (2.6 in) L/50 anti-aircraft guns (2 × 1)
6 × 8 mm (0.31 in) machine guns
 Armour: 

Belt and bulkheads: 40 mm (1.6 in)
Deck: 25 mm (0.98 in)
Conning tower and gun turrets: 50 mm (2.0 in)
 
Drava
Morava
Sava
Vardar
 
4 Apr: Lichtenfels, Liebenfels, Proussa
5 Apr: U-76
7 Apr: Clan Fraser
12 Apr: Drava, Morava, Sava, Vardar
13 Apr: Corinthic, HMS Rajputana
16 Apr: HMS Mohawk
17 Apr: Zagreb
20 Apr: Empire Endurance, Ithaki, Psara, Vasilefs Georgios
21 Apr: Thyella
22 Apr: Frinton, Hydra, Sea-Serpent
23 Apr: Alberta, Kilkis/ex-USS Mississippi, Kios, Lemnos
25 Apr: Kyzikos, Pergamos, Pittsburgh
26 Apr: Kydoniai
27 Apr: HMS Diamond, Slamat (disaster), HMS Wryneck
28 Apr: U-65
29 Apr: City of Nagpur
Unknown date: HMS Usk
 
12 Apr: Siboney
24 Apr: HMS Rover
30 Apr: HMS Fermoy
 1940  1941  1942 March 1941   May 1941 