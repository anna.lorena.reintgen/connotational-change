
 HMS Princess Royal was the second of two Lion-class battlecruisers built for the Royal Navy before the First World War. Designed in response to the Moltke-class battlecruisers of the Imperial German Navy, the ships significantly improved on the speed, armament, and armour of the preceding Indefatigable class. The ship was named for The Princess Royal, a title occasionally granted to the Monarch's eldest daughter.[3]
 Laid down in 1912 and commissioned in 1913, Princess Royal served in the Battle of Heligoland Bight a month after the war began. She was then sent to the Caribbean to prevent the German East Asia Squadron from using the Panama Canal. After the East Asia Squadron was sunk at the Battle of the Falkland Islands in December 1914, Princess Royal rejoined the 1st Battlecruiser Squadron. During the Battle of Dogger Bank, Princess Royal scored only a few hits, although one crippled the German armoured cruiser Blücher. Shortly afterward, she became the flagship of the 1st Battlecruiser Squadron, under the command of Rear-Admiral Osmond Brock.
 Princess Royal was moderately damaged during the Battle of Jutland and required a month and a half of repairs. Apart from providing distant support during the Second Battle of Heligoland Bight in 1917, the ship spent the rest of the war on uneventful patrols of the North Sea. Princess Royal was placed into reserve in 1920, then was sold for breaking up as scrap in 1922 to meet the terms of the Washington Naval Treaty.
 The Lion-class battlecruisers, nicknamed the "Splendid Cats",[4] were designed by Philip Watts, the Director of Naval Construction, to be as superior to the new German battlecruisers of the Moltke class as the German ships were to the Invincible class. The increase in speed, armour and gun size forced a 70% increase in size over the Indefatigable class and made them the largest warships in the world.[5]
 Princess Royal was significantly larger than her predecessors. She had an overall length of 700 feet (213.4 m), a beam of 88 feet 6.75 inches (27.0 m), and a draught of 32 feet 5 inches (9.9 m) at deep load. The ship normally displaced 26,270 long tons (26,690 t) and 30,820 long tons (31,310 t) at deep load, over 8,000 long tons (8,100 t) more than the earlier ships. She had a metacentric height of 5.95 feet (1.8 m) at deep load.[6]
 Princess Royal had two paired sets of Parsons direct-drive steam turbines housed in separate engine-rooms. Each set consisted of a high-pressure ahead-and-astern turbine driving an outboard shaft, and a low-pressure ahead-and-astern turbine driving an inner shaft. Designed power was 70,000 shaft horsepower (52,199 kW) for a speed of 28 knots (52 km/h; 32 mph).[7] In September 1912, Princess Royal began her sea trials and developed 78,803 shp (58,763 kW) for a speed of 28.5 knots (52.8 km/h; 32.8 mph).[8] During maximum power trials in July 1913, the battlecruiser achieved 96,238 shp (71,765 kW) for a speed of 27.97 knots (51.80 km/h; 32.19 mph) while at the unusually high displacement of 29,660 long tons (30,140 t).[9]
 The steam plant consisted of 42 Yarrow large-tube boilers arranged in seven boiler rooms.[10] Maximum bunkerage was 3,500 long tons (3,600 t) of coal and an additional 1,135 long tons (1,153 t) of fuel oil to be sprayed on the coal to increase its burn rate.[11] At 10 knots (19 km/h; 12 mph), the ship's range was 5,610 nautical miles (10,390 km; 6,460 mi).[4]
 Princess Royal was armed with eight BL 13.5-inch Mk V guns ("BL" for breech-loading) in four twin hydraulically powered turrets, designated 'A', 'B', 'Q' and 'X' from bow to stern. Her secondary armament consisted of 16 BL 4-inch Mk VII guns, most of which were mounted in casemates in the superstructure.[12] The two guns mounted on the deck above the forward group of casemates were fitted with gun shields in 1913 and 1914 to better protect their crews from enemy fire.
 The battlecruiser was built without anti-aircraft (AA) guns, but from October 1914 to December 1916 she was fitted with a single QF 6 pounder Hotchkiss gun ("QF" for quick-firing) on a high-angle mount, which had a maximum depression of 8° and a maximum elevation of 60°.[12] Its 6-pound (2.7 kg) shell was fired at a muzzle velocity of 1,765 ft/s (538 m/s) and a rate of fire of 20 rounds per minute. It had a maximum ceiling of 10,000 feet (3,000 m), but an effective range of only 3,600 feet (1,100 m).[13] A single QF 3-inch 20 cwt[a] AA gun was added in January 1915 and carried until April 1917. Its high-angle mount had a maximum depression of 10° and a maximum elevation of 90°. The gun fired a 12.5-pound (5.7 kg) shell at a muzzle velocity of 2,500 ft/s (760 m/s) at a rate of 12 to 14 rounds per minute. It had a maximum effective ceiling of 23,500 ft (7,200 m).[14]
 Princess Royal received a fire-control director between mid-1915 and May 1916 that centralised fire-control under the gunnery officer who now fired the guns. To align their guns on the target, the turret crewmen had to follow pointers whose position was transmitted from the director. This greatly increased accuracy as it was easier for the director to spot the fall of shells and eliminated the shell spread caused by the ship's roll as the turrets fired individually.[15]
 By early 1918, Princess Royal carried a Sopwith Pup and a Sopwith 1½ Strutter on flying-off ramps fitted on top of 'Q' and 'X' turrets. The Pup was intended to shoot down Zeppelins while the 1½ Strutter was used for spotting and reconnaissance.[16] Each platform had a canvas hangar to protect the aircraft during inclement weather.[17]
 The armour protection given to Lion and Princess Royal was heavier than on the Indefatigables. The waterline belt of Krupp cemented armour measured 9 inches (229 mm) thick amidships; this thinned to 4 inches towards the ships' ends, and did not reach the bow or stern. The upper armour strake had a maximum thickness of 6 inches over the same length as the thickest part of the waterline armour and thinned to 5 inches (127 mm) abreast of the end turrets. The gun turrets and barbettes were protected by 8 to 9 inches (203 to 229 mm) of armour, except for the turret roofs which used 2.5 to 3.25 inches (64 to 83 mm). The thickness of the nickel steel deck ranged from 1 to 2.5 inches (25 to 64 mm). Nickel-steel torpedo bulkheads 2.5 inches (64 mm) thick were fitted abreast of the magazines and shell rooms. The sides of the conning tower were 10 inches (254 mm) thick.[18] After the Battle of Jutland revealed a vulnerability to plunging shellfire, 1 inch of additional armour, weighing approximately 100 long tons (102 t), was added to the magazine crowns and turret roofs.[19]
 Princess Royal was laid down at the Vickers Shipyard in Barrow-in-Furness on 2 May 1910. She was launched on 29 April 1911 by Princess Louise, The Princess Royal, and commissioned into the Royal Navy on 14 November 1912.[20] She cost £1,955,922 plus an additional £120,300 for her armament.[21] Upon commissioning, Princess Royal joined the 1st Cruiser Squadron, which was renamed the 1st Battlecruiser Squadron (BCS) in January 1913. Rear-Admiral Beatty assumed command of the 1st BCS on 1 March 1913. The squadron, including Princess Royal, visited Brest in February 1914 and ports in Russia during June.[22]
 Princess Royal first saw combat during the Battle of Heligoland Bight on 28 August 1914. She was part of Admiral Beatty's battlecruiser force, which was originally to provide distant support to the British cruisers and destroyers closer to the German coast if the German High Seas Fleet sortied in response. The battlecruisers headed south at full speed at 11:35,[b] when the British light forces failed to disengage on schedule, as the rising tide meant that German capital ships would be able to clear the bar at the mouth of the Jade Estuary. The British light cruiser Arethusa had been crippled earlier in the battle and was under fire from the German light cruisers Strassburg and Cöln when Beatty's battlecruisers appeared out of the mist at 12:37. Strassburg was able to duck into the mists and evade fire, but Cöln was quickly crippled by the squadron's guns. Before Cöln could be sunk, Beatty was distracted by the sudden appearance of the German light cruiser Ariadne directly to his front, and ordered pursuit. Ariadne was reduced to a burning hulk after only three salvos at less than 6,000 yards (5,500 m).[23]
 Princess Royal sailed from Cromarty on 28 September to rendezvous with a Canadian troop convoy bound for the United Kingdom. She rejoined the 1st BCS on 26 October, but was detached again a few days later to reinforce the North Atlantic and Caribbean Squadrons in the search for Admiral Graf Spee's German East Asia Squadron after it destroyed the West Indies Squadron on 1 November 1914. Princess Royal arrived at Halifax on 21 November, then spent several days off New York City before she steamed down to the Caribbean to guard against the possibility that Graf Spee would use the Panama Canal. The East Asia Squadron was sunk off the Falkland Islands on 7 December, and Princess Royal left Kingston, Jamaica to sail to the UK on 19 December.[24]
 On 23 January 1915, a force of German battlecruisers under the command of Admiral Franz von Hipper sortied to clear the Dogger Bank of any British vessels that might be collecting intelligence on German movements. The British were reading the German coded messages, and a large battlecruiser force under Admiral Beatty – aboard Lion – sailed to intercept. Contact was initiated at 07:20 on the 24th, when the British light cruiser Arethusa spotted the German light cruiser Kolberg. By 07:35, the Germans had seen Beatty's force; Hipper – aboard Seydlitz – ordered his ships south at 20 knots (37 km/h; 23 mph), thinking he could outpace any British battleships, and could increase to Blücher's maximum speed of 23 knots (43 km/h; 26 mph) if the pursuing ships were battlecruisers.[25]
 Beatty ordered his battlecruisers to catch the Germans before they could escape. The leading ships – Lion, Princess Royal and Tiger – pursued at 27 knots (50 km/h; 31 mph), and Lion opened fire at 08:52 at a range of 20,000 yards (18,000 m). The other ships followed a few minutes later, but the extreme range and decreasing visibility meant they did not start scoring hits until 09:09. The German battlecruisers opened fire two minutes later at a range of 18,000 yards (16,000 m) and concentrated their fire on Lion, hitting her once. At 09:35, Beatty signalled to "engage the corresponding ships in the enemy's line", but Tiger's captain – believing that Indomitable was already engaging Blücher – joined Lion in attacking Seydlitz, which left Moltke unengaged and able to fire on Lion without risk. Moltke and Derfflinger combined their fire to badly damage Lion over the next hour, even with Princess Royal attacking Derfflinger.[26]
 Meanwhile, Blücher had been heavily damaged; her speed had dropped to 17 knots (31 km/h; 20 mph), and her steering gear was jammed. Beatty ordered Indomitable to attack her at 10:48. Six minutes later, he spotted what he thought was a submarine periscope on the starboard bow and ordered an immediate 90° turn to port to avoid the submarine, although the submarine warning flag was not raised because most of Lion's signal halyards had been shot away. Soon afterward, Lion lost her remaining dynamo to the rising water, which knocked out all remaining light and power. At 11:02, Beatty had flags hoisted signalling "course north-east", to bring his ships back to their pursuit of Hipper, and "attack the rear of the enemy". Rear-Admiral Sir Gordon Moore – temporarily commanding the squadron from New Zealand – thought the signals meant to attack Blücher, which was about 8,000 yards (7,300 m) to the north-east, and ordered the four other battlecruisers away from the pursuit of Hipper's main force to engage. Beatty tried to correct the mistake, but he was so far behind the leading battlecruisers that his signals could not be read in the smoke and haze.[27]
 Beatty transferred to the destroyer Attack at 11:50 and set off in pursuit of his battlecruisers, reaching them shortly before Blücher sank. He boarded Princess Royal at 12:20 and ordered the ships to pursue the main German force. This order was rescinded when it became clear that the time wasted in sinking Blücher meant the rest of Hipper's battlecruisers would reach friendly waters before they could be caught. Beatty's battlecruisers turned for home, catching up to Lion, which was limping along at 10 knots (19 km/h; 12 mph).[28]
 Princess Royal hit Derfflinger once, but only damaged two armour plates and caused a coal bunker to flood.[29] She hit Blücher at least twice, including the shot that crippled her, out of a total of 271 13.5-inch (343 mm) shells fired during the battle, a hit rate of only 0.7%. By way of contrast, her sister Lion made four hits out of 243 shells fired, a rate of 1.6%. She also fired two 13.5-inch shrapnel shells at the German airship L5 as its crew attempted to bomb the sinking Blücher, mistaking it for a British ship,[30] despite the fact that the maximum elevation of those guns was only 20°.[31] Princess Royal was not damaged during the battle.[32]
 On 31 May 1916, Princess Royal was the flagship of Rear-Admiral Osmond Brock and the 1st BCS under Beatty's overall command;[30] they had put to sea with the rest of the Battlecruiser Fleet to intercept a sortie by the High Seas Fleet into the North Sea. The British had decoded the German radio messages, and left their bases before the Germans put to sea. Hipper's battlecruisers spotted the Battlecruiser Fleet to their west at 15:20, but Beatty's ships did not see the Germans to their east until 15:30. Two minutes later, Beatty ordered a course change to east south-east, positioning the British ships to cut off the Germans' line of retreat, and signalled action stations. Hipper ordered his ships to turn to starboard, away from the British, to assume a south-easterly course, and reduced speed to 18 knots (33 km/h; 21 mph) to allow three light cruisers of the 2nd Scouting Group to catch up. With this turn, Hipper was falling back on the High Seas Fleet, 60 miles (97 km) behind him. Beatty altered course to the east, as he was still too far north to cut Hipper off.[33]
 This began what was to be called the "Run to the South" as Beatty changed course to steer east south-east at 15:45, now paralleling Hipper's course less than 18,000 yards (16,000 m) away. The Germans opened fire first at 15:48, followed by the British. The British ships were still in the process of making their turn as only the two leading ships – Lion and Princess Royal – had steadied on their course when the Germans opened fire. The two battlecruisers engaged Lützow, the leading German ship, while Derfflinger targeted Princess Royal. The German fire was accurate from the start, with two hits on Princess Royal within the first three minutes.[34] British gunnery was less effective; the range was incorrectly estimated as the German ships blended into the haze. Princess Royal's 'A' turret stopped working effectively early in the battle: the left gun was rendered inoperable when the breech pinion gear sheared, and the right gun misfired frequently.[30] By 15:54, the range was down to 12,900 yards (11,800 m); Beatty ordered a course change two points[c] to starboard to open up the range at 15:57.[35]
 At 16:11, a torpedo fired by Moltke passed under Princess Royal. Those aboard the British ship saw the torpedo's track, but incorrectly concluded that a U-boat was positioned on the opposite side of the British line – away from the German battlecruisers – and was firing toward both groups of ships. This false impression was compounded by reports of a periscope sighting by the destroyer Landrail.[36] By this time, the distance between the British and German ships was too great for accurate fire, so Beatty altered course four points to port between 16:12 and 16:15, closing the range. This manoeuvre exposed Lion to the fire of the German battlecruisers, and the smoke from multiple successful hits caused Derfflinger to lose sight of Princess Royal and switch targets to Queen Mary at 16:16. By 16:25, the range was down to 14,400 yards (13,200 m) and Beatty turned two points to starboard to open the range again. Around this time, Queen Mary was hit multiple times in quick succession and her forward magazines exploded.[37] At 16:30, the light cruiser Southampton, scouting in front of Beatty's ships, spotted the lead elements of the High Seas Fleet charging north at top speed. Three minutes later, they sighted the topmasts of Vice-Admiral Reinhard Scheer's battleships, but did not report this to the fleet for another five minutes. Beatty continued south for another two minutes to confirm the sighting before ordering his force to turn north.[38]
 The German battlecruisers made their own turn north in pursuit,[39] but Beatty's ships maintained full speed, and gradually moved out of range. The British battlecruisers turned north, then north-east, to try to rendezvous with the main body of the Grand Fleet, and at 17:40 opened fire again on their German counterparts. Facing the setting sun, the Germans could not make out the British ships and turned away to the north-east at 17:47.[40] Beatty gradually turned towards the east so his ships could cover the Grand Fleet as it deployed into battle formation, but he mistimed his manoeuvre and forced the leading British division to manoeuvre away from the Germans.[41] About 18:22, Princess Royal was hit by two 12.0-inch (305 mm) shells fired by the battleship Markgraf; one of these disabled 'X' turret and the other penetrated the ship's side armour.[42] By 18:35, Beatty was following the 3rd BCS as they were leading the Grand Fleet east-southeast, and continuing to engage Hipper's battlecruisers to their south-west. A few minutes earlier, Scheer had ordered a simultaneous 180° starboard turn, and Beatty lost sight of them in the haze.[41] At 18:44, Beatty turned his ships south-east, then south-southeast four minutes later, to find Hipper's force. He then ordered the two surviving ships of the 3rd BCS to take position astern of New Zealand, while slowing to 18 knots (33 km/h; 21 mph) and altering course to the south.[43] Beatty then ordered his ships to make a complete circle to stay within visual range of the Grand Fleet.[44] At 18:55, Scheer ordered another 180° turn, which put the German ships on a converging course again with the Grand Fleet. However, the British had altered course to the south, allowing the Grand Fleet to cross Scheer's "T" and inflict damage on the leading German ships. Scheer ordered yet another 180° turn at 19:13, and successfully extricated the High Seas Fleet from the danger precipitated by his previous turn.[45] About this time, Princess Royal fired at the leading German battlecruiser for three minutes without result.[46]
 The British lost sight of the Germans until Castor spotted smoke to the west-northwest at 20:05, then identified and engaged several German torpedo boats. Hearing the sound of gunfire, Beatty ordered his ships west, and spotted the German battlecruisers only 8,500 yards (7,800 m) away. Inflexible opened fire at 20:20, followed almost immediately by the rest of the battlecruisers.[47] Shortly after 20:30, the pre-dreadnought battleships of Rear-Admiral Mauve's II Battle Squadron were spotted. The British battlecruisers and German pre-dreadnoughts exchanged fire; the Germans fired only a few times before turning away to the west because of poor visibility and the more accurate British gunnery, and disappeared into the mist around 20:40.[48] Beatty's battlecruisers sailed south-southeast, ahead of both the Grand Fleet and the High Seas Fleet, until the order to reverse course for home was given at 02:55.[49]
 Along with the rest of the battlecruisers, Princess Royal reached Rosyth Dockyard in Scotland on the morning of 2 June, and she immediately received temporary repairs over the next eight days. She then sailed for Plymouth, where permanent repairs were completed on 15 July, and returned to Rosyth by 21 July. Princess Royal was hit nine times during the battle — six times by Derfflinger during the "Run to the South", twice by Markgraf during the "Run to the North", and once by Posen just after II Battle Squadron appeared — with 22 killed and 81 injured. The battlecruiser fired only 230 13.5-inch shells during the battle, as her visibility was often impaired by the funnel smoke and fires aboard Lion. She was credited with three hits on Lützow and two on Seydlitz. A torpedo fired at the German pre-dreadnoughts failed to hit.[30]
 As the German High Seas Fleet was forbidden from taking any unnecessary risks, the majority of Princess Royal's post-Jutland activities consisted of uneventful patrols of the North Sea. On the evening of 18 August, the Grand Fleet put to sea in response to a deciphered message that the High Seas Fleet, minus the II Squadron, would be leaving harbour that night. The Germans planned to bombard the port of Sunderland on the 19th, with extensive reconnaissance provided by airships and submarines. The Grand Fleet sailed with 29 dreadnought battleships and 6 battlecruisers to their anticipated rendezvous in the North Sea;[d] for fear that they had entered a minefield after Nottingham was torpedoed, they turned north before turning south again. Scheer steered south-eastward pursuing a lone British battle squadron reported by an airship, which was in fact the Harwich Force under Commodore Tyrwhitt. Realising their mistake, the Germans then set course for home. The only contact came in the evening when Tyrwhitt sighted the High Seas Fleet but was unable to achieve an advantageous attack position before dark, and broke off. Both the British and German fleets returned home; the light cruisers Nottingham and Falmouth had been sunk by submarines, and the German dreadnought Westfalen had been damaged by a torpedo.[50]
 Princess Royal provided support for British light forces involved in the Second Battle of Heligoland Bight on 17 November 1917, but never came within range of any German ships. She sailed with the 1st BCS on 12 December after German destroyers sank seven ships of a Norway-bound convoy, including Partridge, four naval trawlers and four cargo ships earlier that day, but the British were unable to intercept and returned to base the following day.[51] Princess Royal, along with the rest of the Grand Fleet, sortied on the afternoon of 23 April 1918 after radio transmissions revealed that the High Seas Fleet was at sea after a failed attempt to intercept the regular British convoy to Norway. However, the Germans were too far ahead of the British, and no shots were fired.[52] Starting in July 1918, the Grand Fleet was affected by the 1918 flu pandemic; at one point, Princess Royal lacked sufficient healthy crewmen to sail.[53]
 Following the surrender of the High Seas Fleet at the end of the war, Princess Royal and the 1st BCS made up part of the guard force at Scapa Flow.[54] Princess Royal was reassigned to the Atlantic Fleet in April 1919.[24] The battlecruiser was placed in reserve in 1920, and an attempt to sell her to Chile later that year was unsuccessful.[32] She became the flagship of the Commander-in-Chief of the Scottish Coast on 22 February 1922.[24] She was sold for scrap in December 1922[55] to meet the tonnage limitations set on the Royal Navy by the Washington Naval Treaty,[4] and arrived at the breakers on 13 August 1923.[24]
 
 26,270 long tons (26,690 t) 30,820 long tons (31,310 t) (deep load) (70,000 shp (52,000 kW) 42 × Yarrow boilers 4 × Direct-drive steam turbine sets 4 × shafts 4 × twin BL 13.5 in (343 mm) guns 16 × single BL 4 in (102 mm) guns 2 × 1 – 21 in (533 mm) torpedo tubes Belt: 9–4 in (229–102 mm) Bulkheads: 4 in (102 mm) Barbettes: 9–8 in (229–203 mm) Turrets: 9 in (229 mm) Decks: 2.5 in (64 mm) Conning tower: 10 in (254 mm) 1 Design

1.1 General characteristics
1.2 Propulsion
1.3 Armament
1.4 Armour

 1.1 General characteristics 1.2 Propulsion 1.3 Armament 1.4 Armour 2 Construction and career

2.1 First World War

2.1.1 Battle of Heligoland Bight
2.1.2 Battle of Dogger Bank
2.1.3 Battle of Jutland
2.1.4 Post-Jutland career



 2.1 First World War

2.1.1 Battle of Heligoland Bight
2.1.2 Battle of Dogger Bank
2.1.3 Battle of Jutland
2.1.4 Post-Jutland career

 2.1.1 Battle of Heligoland Bight 2.1.2 Battle of Dogger Bank 2.1.3 Battle of Jutland 2.1.4 Post-Jutland career 3 Notes 4 References 5 Bibliography 6 External links Battleships portal ^ 
"cwt" is the abbreviation for hundredweight, 30 cwt referring to the weight of the gun.
 ^ 
The times used in this article are in UTC, which is one hour behind CET, which is often used in German works.
 ^ 
The compass can be divided into 32 points, each corresponding to 11.25 degrees. A two-point turn to port would alter the ships' course by 22.5 degrees.
 ^ 
While no sources explicitly state that Princess Royal was part of the fleet at this time, of the seven Royal Navy battlecruisers then in commission, Indomitable was under refit through August and the only one unavailable for action. See Roberts, p. 122.
 ^ The Times (London), 2 May 1911, p. 8.
 ^ The Times (London), 29 April 1911, p. 8.
 ^ Silverstone, p. 258.
 ^ a b c Gardiner & Gray, p. 29.
 ^ Burt, p. 151.
 ^ Roberts, pp. 43–45.
 ^ Roberts, pp. 33, 76.
 ^ Roberts, pp. 76, 78, 80.
 ^ Roberts, p. 79.
 ^ Roberts, pp. 70–76.
 ^ Roberts, p. 76.
 ^ a b Roberts, p. 83.
 ^ Buxton, p. 235.
 ^ Campbell 1985, p. 61.
 ^ Roberts, pp. 92–93.
 ^ Layman, pp. 114–15.
 ^ Roberts, p. 92.
 ^ Roberts, pp. 109, 112.
 ^ Roberts, p. 113.
 ^ Roberts, p. 41.
 ^ Burt, p. 154.
 ^ Burt, p. 161.
 ^ Massie, pp. 109–13.
 ^ a b c d Roberts, p. 123.
 ^ Massie, pp. 376–84.
 ^ Massie, pp. 386–96.
 ^ Massie, pp. 398–402.
 ^ Tarrant, p. 38.
 ^ Tarrant, pp. 35–39.
 ^ a b c d Campbell 1978, pp. 29, 32.
 ^ Campbell 1978, p. 27.
 ^ a b Burt, p. 162.
 ^ Tarrant, pp. 69, 71, 75.
 ^ Tarrant, pp. 80–82.
 ^ Tarrant, p. 83.
 ^ Tarrant, p. 85.
 ^ Tarrant, pp. 89–91.
 ^ Massie, pp. 598–600.
 ^ Massie, p. 601.
 ^ Tarrant, p. 109.
 ^ a b Tarrant, pp. 130–38.
 ^ Campbell 1985, pp. 153, 170–71.
 ^ Tarrant, p. 145.
 ^ Brooks, p. 263.
 ^ Tarrant, pp. 149, 157.
 ^ Campbell 1985, p. 208.
 ^ Tarrant, p. 175.
 ^ Tarrant, pp. 177–78.
 ^ Tarrant, pp. 178, 224.
 ^ Marder, III, pp. 238–39, 287–96.
 ^ Newbolt, pp. 169, 193.
 ^ Massie, p. 748.
 ^ Stevens & Goldrick, p. 186.
 ^ Marder, V, p. 273.
 ^ Campbell 1978, p. 29.
 Brooks, John (2005). Dreadnought Gunnery and the Battle of Jutland: The Question of Fire Control. Naval Policy and History. 32. Abingdon, UK: Routledge. ISBN 978-0-415-40788-5..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} Burt, R. A. (1986). British Battleships of World War One. Annapolis, Maryland: Naval Institute Press. ISBN 978-0-87021-863-7. Buxton, Ian (2008). Big Gun Monitors: Design, Construction and Operations 1914–1945 (2nd, revised and expanded ed.). Annapolis, Maryland: Naval Institute Press. ISBN 978-1-59114-045-0. Campbell, John (1985). Naval Weapons of World War II. Annapolis, Maryland: Naval Institute Press. ISBN 978-0-87021-459-2. Campbell, N. J. M. (1978). Battle Cruisers. Warship Special. 1. Greenwich, UK: Conway Maritime Press. ISBN 978-0-85177-130-4. Gardiner, Robert; Gray, Randal, eds. (1985). Conway's All the World's Fighting Ships: 1906–1921. Annapolis, Maryland: Naval Institute Press. ISBN 978-0-85177-245-5. Layman, R. D. (1996). Naval Aviation in the First World War. Annapolis, Maryland: Naval Institute Press. ISBN 978-1-55750-617-7. Marder, Arthur J. (1978). From the Dreadnought to Scapa Flow, The Royal Navy in the Fisher Era, 1904–1919. III: Jutland and After, May 1916 – December 1916 (Second ed.). London: Oxford University Press. ISBN 978-0-19-215841-3. Marder, Arthur J. (1970). From Dreadnought to Scapa Flow: The Royal Navy in the Fisher Era, 1904–1919. V: Victory and Aftermath (January 1918 – June 1919). London: Oxford University Press. ISBN 978-0-19-215187-2. Massie, Robert (2004). Castles of Steel: Britain, Germany and the Winning of the Great War. New York: Random House. ISBN 978-0-679-45671-1. Newbolt, Henry (1996). Naval Operations. History of the Great War: Based on Official Documents. V (reprint of the 1931 ed.). London and Nashville, Tennessee: Imperial War Museum and Battery Press. ISBN 978-1-870423-72-4. Roberts, John (1997). Battlecruisers. Annapolis, Maryland: Naval Institute Press. ISBN 978-1-55750-068-7. Silverstone, Paul H. (1984). Directory of the World's Capital Ships. New York: Hippocrene Books. ISBN 978-0-88254-979-8. Stevens, David; Goldrick, James (2010). 1918 Year of Victory. Auckland, NZ: Exisle. ISBN 978-1-921497-42-1. Tarrant, V. E. (1999). Jutland: The German Perspective: A New View of the Great Battle, 31 May 1916 (reprint of the 1995 ed.). London: Brockhampton Press. ISBN 978-1-86019-917-2. Imperial War Museums: Lives of the First World War: HMS Princess Royal at the Battle of Jutland (Crew List) Battle of Jutland Crew Lists Project - HMS Princess Royal Crew List v t e Lion Princess Royal Preceded by: Indefatigable class Followed by: HMS Queen Mary 1911 ships Ships built in Barrow-in-Furness Lion-class battlecruisers World War I battlecruisers of the United Kingdom Use dmy dates from January 2017 Use British English from January 2017 Commons category link is on Wikidata CS1: long volume value Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Български Català Čeština Deutsch Français Italiano Magyar 日本語 Polski Русский Suomi Tiếng Việt  This page was last edited on 4 October 2019, at 10:42 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  HMS Princess Royal ^ ^ ^ ^ ^ ^ ^ a b c ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ a b c d ^ ^ ^ ^ ^ a b c d ^ a b ^ ^ ^ ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ HMS Princess Royal (ship, 1912) 32 1 V Princess Royal at anchor
 Name: 
Princess Royal Namesake: 
The Princess Royal Ordered: 
1909–10 Contingent Programme Builder: 
Vickers, Barrow-in-Furness Cost: 
£2,076,222 (including armament) Laid down: 
2 May 1910[1] Launched: 
29 April 1911[2] Sponsored by: 
Princess Louise, The Princess Royal Commissioned: 
14 November 1912 Fate: 
Sold for scrap, 19 December 1922 Class and type: 
Lion-class battlecruiser Displacement: 

26,270 long tons (26,690 t)
30,820 long tons (31,310 t) (deep load)
 Length: 
700 ft (213.4 m) Beam: 
88 ft 7 in (27 m) Draught: 
32 ft 5 in (9.9 m) at deep load Installed power: 

(70,000 shp (52,000 kW)
42 × Yarrow boilers
 Propulsion: 

4 × Direct-drive steam turbine sets
4 × shafts
 Speed: 
28 knots (52 km/h; 32 mph) Range: 
5,610 nmi (10,390 km; 6,460 mi) at 10 knots (19 km/h; 12 mph) Complement: 
985 (in 1912) Armament: 

4 × twin BL 13.5 in (343 mm) guns
16 × single BL 4 in (102 mm) guns
2 × 1 – 21 in (533 mm) torpedo tubes
 Armour: 

Belt: 9–4 in (229–102 mm)
Bulkheads: 4 in (102 mm)
Barbettes: 9–8 in (229–203 mm)
Turrets: 9 in (229 mm)
Decks: 2.5 in (64 mm)
Conning tower: 10 in (254 mm)
  Wikimedia Commons has media related to HMS Princess Royal (ship, 1912). 
Lion
Princess Royal
 
Preceded by: Indefatigable class
Followed by: HMS Queen Mary
 List of battlecruisers of the Royal Navy 