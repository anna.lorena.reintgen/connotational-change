
 James Park Woods VC (4 January 1886 – 18 January 1963) was an Australian recipient of the Victoria Cross during World War I; the Victoria Cross was the highest award for gallantry in the face of the enemy that could be awarded to members of the Australian armed forces at the time. Woods enlisted in the Australian Imperial Force in September 1916 and, after training in Australia and the United Kingdom, joined his unit, the 48th Battalion, in France in September 1917. Along with the rest of his battalion, he participated in the First Battle of Passchendaele the following month. In early 1918, Woods was hospitalised for several months before rejoining his unit in May. He again reported sick in July, and did not return to the 48th Battalion until mid-August.
 On 18 September 1918, the 48th Battalion was involved in the attack on the Hindenburg Outpost Line during the Hundred Days Offensive. After the first phase of the attack, some elements of Woods' unit were tasked to support another battalion as it conducted a further assault. When it was suspected that flanking British troops had not gone forward to their objective as reported, a four-man patrol including Woods attempted to make contact with them. Instead of finding British troops, they encountered a strong German post and, after calling for reinforcements, attacked it, driving more than thirty Germans from the position. Woods' actions during this assault and subsequent defence of the captured post resulted in him being awarded the Victoria Cross. Woods survived the war, returned to Australia and operated a vineyard and orchard in Western Australia. He retired early due to ill health and died in 1963. His medals are displayed in the Hall of Valour at the Australian War Memorial.
 James Park "Jimmy" Woods was born at Two Wells, South Australia, on 4 January 1886,[a] the son of a blacksmith, James Woods, and his wife Ester née Johnson. After his mother's death when he was seven, Woods was raised by a stepsister and, after completing his schooling, worked in a vineyard alongside his brothers. Not long after World War I broke out in 1914, he attempted to enlist in the Australian Imperial Force (AIF), but was rejected due to his height of 5 feet 4 inches (163 cm). He and his brother Will then moved to Western Australia and for the next two years worked in cartage and fencing in the Kantanning district before James became a viticulturist at Caversham in the Swan Valley wine region near Perth. He was also an enthusiastic cricketer in his younger years. After further unsuccessful attempts to join the AIF, he was eventually successful on 29 September 1916, after height requirements had been lowered.[1][2][4]
 Following his enlistment as a private, Woods was allotted as a reinforcement to the 48th Battalion, a mixed South Australian-Western Australian unit, part of the 12th Brigade, 4th Division. It was already serving on the Western Front in France and Belgium. After two months training at Blackboy Hill, Woods embarked with the rest of his reinforcement draft on HMAS Berrima at Fremantle on 23 December 1916. He disembarked at Devonport in the UK on 16 February 1917, and joined the 12th Training Battalion at Codford on Salisbury Plain. During this training period, he was hospitalised with mumps and then with bronchitis and pneumonia. Recovered, he completed his training, embarked at Southampton for France, and joined the 48th Battalion on 12 September.[2][4][5]
 At the time Woods joined the battalion, it was undergoing training and absorbing reinforcements in a rest area at Zuytpeene. On 21 September, the battalion was collected by buses and transported over several days via Steenvoorde to the vicinity of Ypres in Belgium. There the 12th Brigade relieved other units of the 4th Division that had participated in the Battle of Polygon Wood and took up support positions. During this period its main tasks were battlefield salvage and digging graves for recently killed Germans and Australians. Despite these non-combat tasks, the battalion suffered several casualties from German artillery fire. The 48th Battalion was relieved from these positions on 1 October, and occupied trenches at Westhoek Ridge overnight before returning to Steenvoorde.[6]
 After a week at Steenvoorde, Woods' battalion marched to Abeele on 10 October, then entrained for Ypres. That evening it marched to trenches on Westhoek Ridge, where it received orders for a major attack by the division: the First Battle of Passchendaele, which was fought on 12 October. Despite initial success and the capture of more than 200 Germans, the main attack failed, leaving the left flank of the battalion exposed. The first German counterattack was beaten off, but with its left flank unprotected, the 48th Battalion was pushed back to its start line by a second counterattack. During the fighting, the unit suffered 370 casualties from its original complement of 621. Woods and the rest of the battalion were withdrawn from the line and they were re-united with the nucleus troops, the one-third of each unit that was retained out of the line when a battalion went into action. Thus reinforced, the battalion then went briefly into a support position on Anzac Ridge on 19 October, before being relieved and marching away from Passchendaele for a long period of rest and recuperation. This time was spent first at Cuhem, then Friancourt before the battalion spent a few weeks at a camp near Péronne, where Woods and the rest of the unit celebrated Christmas Day.[7][8]
 On 8 January 1918, the 48th Battalion marched to Péronne and, travelling by train and on foot, arrived in Belgium where they entered the front line trenches near Hollebeke on 11 January. The battalion remained in the quiet trenches in cold winter conditions for ten days before being relieved, being mainly engaged on improving the dilapidated defences. On the day after the battalion was relieved and went into the rear area, Woods reported sick. He was diagnosed with either bronchitis or pericarditis, and was evacuated to a hospital in Birmingham, UK. He did not return to his battalion until 30 May,[9][10] when the battalion was in a rest area at Rivery. On 2 June the unit moved forward again, taking up positions as part of the divisional reserve. Two weeks later, the 48th Battalion moved into the front line near Sailly-le-Sec, from which it was relieved on 4 July. It then went back into a rest area near Allonville with the remainder of the 4th Division. This rest period continued to the end of the month, but on 23 July, Woods reported sick with dysentery. He was evacuated and admitted to a casualty clearing station to recover, and did not return to his unit until 16 August. At this time, the battalion was in the front line near Lihons, where it had a quiet time until relieved on 24 August. The unit was then withdrawn to a rest area near Saint-Vaast-en-Chaussée where it remained for nearly two weeks.[11][12]
 By September 1918, the Allies were in the midst of the Hundred Days Offensive, a series of unanswered attacks that had commenced with the Battle of Amiens on 8 August. As part of the attack on the Hindenburg Outpost Line on 18 September, the 48th Battalion led the assault in the first phase, capturing 480 Germans, and they were followed by the 45th and 46th Battalions which were responsible for the second and third phases respectively.[13] The Australian attack was near Le Verguier, north-west of St. Quentin. On the right flank of the 12th Brigade, the British 1st Division delayed its assault on the third objective line.[14] The commander of the 12th Brigade, Brigadier General Raymond Leane, therefore ordered two companies of the 48th Battalion to protect the flank of the 46th Battalion as it conducted the third phase of the attack. After the 46th Battalion's successful assault, the two companies of the 48th Battalion were tasked to assist the 46th Battalion in consolidating their position. The British 1st Division on the right flank repeatedly reported that they had also captured the third objective, and Leane sent patrols to confirm.[15]
 Woods was a member of a four-man patrol sent on Leane's orders to make contact with the British. Instead of finding British troops, they located a strongly defended German position with excellent fields of fire. While an attack in force on the position was being organised, Woods led the patrol against the German defenders. Woods killed one German, and at least thirty others fled, leaving behind four heavy and two light machine guns. One of the patrol was wounded in the assault, but they managed to hold off several determined counterattacks until reinforcements arrived. Woods climbed onto the parapet, lay down, and threw hand grenades passed to him. Reinforcements arrived when he was down to his last few rifle cartridges, and the post was secured by dawn. During the overall brigade attack, the 48th Battalion suffered just 65 casualties. For his work, Woods was recommended for the Victoria Cross (VC).[1][16][17][18]
 The 4th Division was then relieved and went to the rear to rest,[19] and the brigade did not return to combat before the Armistice of 11 November 1918.[20] Woods' VC citation was published on Christmas Eve 1918, and read:[21]
 For conspicuous bravery and devotion to duty near Le Verguier, north-west of St. Quentin, on the 18th September, 1918, when, with a weak patrol, he attacked and captured a very formidable enemy post, and subsequently, with two comrades, held the same against heavy enemy counterattacks. Although exposed to heavy fire of all descriptions, he fearlessly jumped on the parapet and opened fire on the attacking enemy, inflicting severe casualties. He kept up his fire and held up the enemy until help arrived, and throughout the operations displayed a splendid example of valour, determination and initiative. Woods' VC was the only one awarded to a member of the 48th Battalion.[22] He received the decoration from King George V at Buckingham Palace on 31 May 1919.[23] Woods embarked aboard the troop ship SS Königin Luise on 21 June, arrived back in Fremantle in early August, and was discharged from the AIF the following month.[24][25]
 On his return to civilian life, Woods bought and operated a vineyard and orchard in the Swan Valley. He married Olive Adeline Wilson on 30 April 1921. The couple had seven children: four sons and three daughters. James suffered from poor health as a result of his service and in 1937 retired on a full pension, after which the family moved to Claremont. His sons Gordon and Norman served in the Royal Australian Air Force during World War II; Gordon, the eldest, was killed in a flying training accident near Newcastle, New South Wales, in October 1943. In retirement, Woods was involved with the Returned Sailors', Soldiers' and Airmen's Imperial League of Australia, and was a keen fisherman. In 1956, he went to the UK to attend the VC centenary.[1][2][26]
 Woods died on 18 January 1963 in the Repatriation General Hospital, Hollywood, in Nedlands, aged 77, and was buried in Karrakatta Cemetery. He was survived by his wife and six children.[2] A ward at the Repatriation General Hospital, now known as the Hollywood Private Hospital, is named in his honour.[27]
 As well as the Victoria Cross, British War Medal and Victory Medal for his service in World War I,[28] Woods was later awarded the King George VI Coronation Medal and Queen Elizabeth II Coronation Medal. His medal set, including his Victoria Cross, was presented to the Australian War Memorial in Canberra, and is displayed in the Hall of Valour.[29] The Victoria Cross was the highest award possible at the time, and is considered the equivalent of the Victoria Cross for Australia, which was created in 1991.[30]
 WW1 British War Medal
 Victory Medal of the United Kingdom
 King George VI Coronation Medal
 Queen Elizabeth II Coronation Medal
 
 Western Front
First Battle of Passchendaele
Hundred Days Offensive First Battle of Passchendaele Hundred Days Offensive 1 Early life 2 World War I

2.1 1916–1917
2.2 1918

 2.1 1916–1917 2.2 1918 3 Later life 4 Gallery 5 Notes 6 Footnotes 7 References

7.1 Books
7.2 News and gazettes
7.3 Websites

 7.1 Books 7.2 News and gazettes 7.3 Websites 


WW1 British War Medal


 


Victory Medal of the United Kingdom


 


King George VI Coronation Medal


 


Queen Elizabeth II Coronation Medal


 ^ Staunton gives his date of birth as 2 January 1891,[1] but Higgins and South Australian Births, Deaths and Marriages data state he was born on 4 January 1886.[2][3]
 ^ a b c d Staunton 2005, p. 185.
 ^ a b c d e Higgins 1990.
 ^ Genealogy SA 2018.
 ^ a b Blanch & Pegram 2018, p. 298.
 ^ National Archives 2018, pp. 5, 7–9.
 ^ Devine 1919, p. 93.
 ^ Devine 1919, pp. 95–104.
 ^ Bean 1937, pp. 923–926.
 ^ Devine 1919, pp. 105–106.
 ^ National Archives 2018, pp. 7 & 15.
 ^ Devine 1919, pp. 133 & 142–144.
 ^ National Archives 2018, p. 15.
 ^ Bean 1942, pp. 904–909.
 ^ Bean 1942, pp. 917–918 & 921–924.
 ^ Bean 1942, pp. 925–928.
 ^ Blanch & Pegram 2018, pp. 298–299.
 ^ Wigmore & Harding 1986, pp. 130–131.
 ^ Bean 1942, pp. 930–931.
 ^ Bean 1942, p. 935.
 ^ Devine 1919, pp. 153–157.
 ^ The London Gazette 24 December 1918.
 ^ Australian War Memorial 2018a.
 ^ Wigmore & Harding 1986, p. 131.
 ^ National Archives 2018, pp. 16 & 30.
 ^ Jarrett 2014.
 ^ Blanch & Pegram 2018, p. 299.
 ^ Hollywood Private Hospital 2018.
 ^ National Archives 2018, p. 35.
 ^ Australian War Memorial 2018b.
 ^ "The Victoria Cross for Australia". Department of Defence. Australian Government. Retrieved 1 October 2019..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 Bean, C. E. W. (1937). The Australian Imperial Force in France, 1917. Official History of Australia in the War of 1914–1918. 4 (5 ed.). Sydney, New South Wales: Angus & Robertson. OCLC 216975066. Bean, C. E. W. (1942). The Australian Imperial Force in France: May 1918 – The Armistice. Official History of Australia in the War of 1914–1918. 6 (1 ed.). Sydney, New South Wales: Angus & Robertson. OCLC 830564565. Blanch, Craig; Pegram, Aaron (2018). For Valour: Australians Awarded the Victoria Cross. Sydney, New South Wales: NewSouth Publishing. ISBN 978-1-74223-542-4. Devine, W. (1919). The Story of a Battalion: Being a Record of the 48th Battalion, A.I.F. Melbourne, Victoria: Melville & Mullen. OCLC 3854185. Staunton, Anthony (2005). Victoria Cross. Prahran, Victoria: Hardie Grant. ISBN 978-1-74273-486-6. Wigmore, Lionel; Harding, Bruce A. (1986).  Williams, Jeff; Staunton, Anthony (eds.). They Dared Mightily (2 ed.). Canberra, Australian Capital Territory: Australian War Memorial. ISBN 978-0-642-99471-4. Jarrett, Ian (21 April 2014). "Tale of two ships". The West Australian. Retrieved 31 December 2015. "No. 31082". The London Gazette (Supplement). 24 December 1918. p. 15119. "48th Australian Infantry Battalion". Australian War Memorial. Retrieved 4 October 2018. "A History of Caring". Hollywood Private Hospital. Retrieved 12 December 2018. Higgins, Matthew (1990). "Woods, James Park (1886–1963)". Australian Dictionary of Biography. Retrieved 4 October 2018. "NAA: B2455, Woods J P". National Archives of Australia. 1914–1920. Retrieved 4 October 2018. "Victoria Cross: Private J P Woods, 48 Battalion, AIF". Australian War Memorial. Retrieved 13 December 2018. "Woods, James Parks". Genealogy SA. Retrieved 26 October 2018. ADB: woods-james-park-9178 Trove: 1464939 1886 births 1963 deaths Australian World War I recipients of the Victoria Cross Australian Army soldiers People from Two Wells, South Australia Burials at Karrakatta Cemetery Use dmy dates from April 2018 Use Australian English from April 2017 All Wikipedia articles written in Australian English Articles with short description CS1: Julian–Gregorian uncertainty Pages containing London Gazette template with parameter supp set to y Wikipedia articles with ADB identifiers Wikipedia articles with Trove identifiers Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version العربية  This page was last edited on 10 November 2019, at 10:14 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  For conspicuous bravery and devotion to duty near Le Verguier, north-west of St. Quentin, on the 18th September, 1918, when, with a weak patrol, he attacked and captured a very formidable enemy post, and subsequently, with two comrades, held the same against heavy enemy counterattacks. Although exposed to heavy fire of all descriptions, he fearlessly jumped on the parapet and opened fire on the attacking enemy, inflicting severe casualties. He kept up his fire and held up the enemy until help arrived, and throughout the operations displayed a splendid example of valour, determination and initiative.— The London Gazette – 24 December 1918
 James Park Woods ^ a b c d a b c d e ^ a b ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ 4 6 Woods c. 1918–1919 Jimmy (1886-01-04)4 January 1886Two Wells, South Australia 18 January 1963(1963-01-18) (aged 77)Nedlands, Western Australia Karrakatta Cemetery Australia Australian Imperial Force 1916–1919 Private 48th Battalion World War I
Western Front
First Battle of Passchendaele
Hundred Days Offensive
 Victoria Cross Vigneron 
ADB: woods-james-park-9178
Trove: 1464939
 