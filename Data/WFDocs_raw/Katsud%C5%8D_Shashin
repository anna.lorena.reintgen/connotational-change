
 
 Katsudō Shashin (活動写真, "motion picture"), sometimes called the Matsumoto fragment, is a 1907 Japanese animated filmstrip that is the oldest known work of animation from Japan. Its creator is unknown. Evidence suggests it was made before 1912, so it may predate the earliest displays of Western animated films in Japan. It was discovered in a collection of films and projectors in Kyoto in 2005.
 The three-second filmstrip depicts a boy who writes "活動写真", removes his hat, and bows. The frames were stencilled in red and black using a device for making magic lantern slides, and the filmstrip was fastened in a loop for continuous play.
 Katsudō Shashin consists of a series of cartoon images on fifty frames of a celluloid strip and lasts three seconds at sixteen frames per second.[1] It depicts a young boy in a sailor suit who writes the kanji characters "活動写真" (katsudō shashin, "moving picture") from right to left, then turns to the viewer, removes his hat, and bows.[1] Katsudō Shashin is a provisional title for the film, whose actual title is unknown.[2]
 Unlike in traditional animation, the frames were not produced by photographing the images, but rather were impressed onto film using a stencil.[3] This was done with a kappa-ban,[a] a device for stencilling magic lantern slides. The images were in red and black on a strip of 35 mm film[b][4] whose ends were fastened in a loop for continuous viewing.[5]
 Early printed animation films for optical toys such as the zoetrope predated projected film animation. German toy manufacturer Gebrüder Bing presented a cinematograph at a toy festival in Nuremberg in 1898; soon other toy manufacturers sold similar devices.[6] Live-action films for these devices were expensive to make; possibly as early as 1898 animated films for these devices were on sale, and could be fastened in loops for continuous viewing.[7] Imports of these German devices appeared in Japan at least as early as 1904;[8] films for them likely included animation loops.[9]
 Projected film technology arrived in Japan from the West in 1896–97.[10] The earliest display of foreign animation in Japanese theatres that can be dated with certainty is of the French animator Émile Cohl's The Nipper's Transformations[c] (1911), which premièred in Tokyo on 15 April 1912. Works by Ōten Shimokawa, Seitarō Kitayama, and Jun'ichi Kōuchi in 1917 were the first Japanese animated films to reach theatre screens.[11] The films are lost, but a few have been discovered in "toy movie"[d] versions for viewing at home on hand-cranked projectors; the oldest to survive is Hanawa Hekonai meitō no maki[e] (1917), titled Namakura-gatana in its home version.[12]
 In December 2004, a secondhand dealer in Kyoto contacted Natsuki Matsumoto,[f][3] an expert in iconography at the Osaka University of Arts.[13] The dealer had obtained a collection of films and projectors from an old Kyoto family, and Matsumoto arrived the next month to fetch them.[3] The collection included three projectors, eleven 35 mm films, and thirteen glass magic lantern slides.[3]
 When Matsumoto found Katsudō Shashin in the collection,[13] the filmstrip was in poor condition.[14] The collection included three Western animated filmstrips;[15] Katsudō Shashin may have been made in imitation of such examples of German or other Western animation.[15] Based on evidence such as the likely manufacture dates of the projectors in the collection, Matsumoto and animation historian Nobuyuki Tsugata [ja][g] determined the film was most likely made in the late Meiji period, which ended in 1912;[h][16] historian Frederick S. Litten has suggested c. 1907 as a likely date,[2] and that "a production date before 1905 or after 1912 is unlikely".[9] At the time, movie theatres were rare in Japan;[5] evidence suggests Katsudō Shashin was mass-produced to be sold to wealthy owners of home projectors.[17] The creator of the filmstrip remains unknown;[13] to Matsumoto, the relatively poor quality and low-tech printing technique indicate it was likely from a smaller company.[9]
 The discovery was widely covered in Japanese media.[3] Given its speculated date of creation, the film would have been contemporary to—or even have predated—early animated works by Cohl and the American animators J. Stuart Blackton and Winsor McCay. The newspaper Asahi Shimbun acknowledged the importance of the discovery of Meiji-period animation, but expressed reservations about placing the film in the genealogy of Japanese animation, writing that it is "controversial that [Katsudō Shashin] should even be called animation in the contemporary sense".[14]
 1 Description 2 Background 3 Rediscovery 4 See also 5 Notes 6 References

6.1 Works cited

 6.1 Works cited 7 External links Cinema of Japan History of animation History of anime List of rediscovered films List of anime by release date (pre-1939) Animation portal Anime and manga portal Film portal Japan portal ^ 合羽版 kappa-ban; the printing process was called kappa-zuri (合羽刷り) 
 ^ The filmstrip has since shrunk to 33.5 mm.[2] 
 ^ French: Les Exploits de Feu Follet; Japanese: ニッパルの変形 Nipparu no Henkei 
 ^ 玩具 gangu 
 ^ 塙凹内名刀之巻 Hanawa Hekonai meitō no maki, "Filmreel of Hanawa Hekonai's famous sword"
 ^ 松本 夏樹 Matsumoto Natsuki, b. 1952
 ^ 津堅 信之 Tsugata Nobuyuki, b. 1968
 ^ The Meiji period lasted from 1868 to 1912.
 ^ a b Anime News Network staff 2005.
 ^ a b c Litten 2014, p. 13.
 ^ a b c d e Matsumoto 2011, p. 98.
 ^ Matsumoto 2011, p. 116.
 ^ a b Asahi Shimbun staff 2005.
 ^ Litten 2014, p. 9.
 ^ Litten 2014, p. 10.
 ^ Litten 2014, p. 14.
 ^ a b c Litten 2014, p. 15.
 ^ Matsumoto 2011, p. 112.
 ^ Litten 2013, p. 27.
 ^ Matsumoto 2011, pp. 96–97.
 ^ a b c Clements & McCarthy 2006, p. 169.
 ^ a b López 2012, p. 584.
 ^ a b Litten 2014, p. 12.
 ^ Matsumoto & Tsugata 2006, p. 101; Matsumoto 2011, p. 115.
 ^ Matsumoto 2011, pp. 116–117.
 Anime News Network staff (7 August 2005). "Oldest Anime Found". Anime News Network. Archived from the original on 2 February 2007. Retrieved 12 February 2014..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} Asahi Shimbun staff (1 August 2005). "Nihon saiko? Meiji jidai no anime firumu, Kyōto de hakken" 日本最古？明治時代のアニメフィルム、京都で発見 [Oldest in Japan? Meiji-period animated film discovered in Kyoto]. China People's Daily Online (Japanese Edition) (in Japanese). Archived from the original on 11 October 2007. Retrieved 11 June 2014. Clements, Jonathan; McCarthy, Helen (2006). The Anime Encyclopedia: A Guide to Japanese Animation Since 1917. Stone Bridge Press. ISBN 978-1-84576-500-2. Litten, Frederick S. (2013). "Shōtai kenkyū nōto: Nihon no eigakan de jōei sareta saisho no (kaigai) animēshon eiga ni tsuite" 招待研究ノート：日本の映画館で上映された最初の（海外）アニメーション映画について [On the Earliest (Foreign) Animation Shown in Japanese Cinemas]. The Japanese Journal of Animation Studies (in Japanese). 15 (1A): 27–32. Litten, Frederick S. (17 June 2014). "Japanese color animation from ca. 1907 to 1945" (PDF). litten.de. Archived from the original (PDF) on 14 July 2014. Retrieved 18 June 2014. López, Antonio (2012). "A New Perspective on the First Japanese Animation". Published proceedings‚ Confia‚ (International Conference on Illustration and Animation)‚ 29–30 Nov 2012. IPCA. pp. 579–586. ISBN 978-989-97567-6-2. Matsumoto, Natsuki; Tsugata, Nobuyuki (2006). "Kokusan saikō to kangaerareru animēshon firumu no hakken ni tsuite" 国産最古と考えられるアニメーションフィルムの発見について [The discovery of supposedly oldest Japanese animation films]. Eizōgaku (in Japanese) (76): 86–105. ISSN 0286-0279. Matsumoto, Natsuki (2011). "映画渡来前後の家庭用映像機器" [Home movie equipment from the earliest days of film in Japan].  In Iwamoto, Kenji (ed.). Nihon eiga no tanjō 日本映画の誕生 [Birth of Japanese film] (in Japanese). Shinwa-sha. pp. 95–128. ISBN 978-4-86405-029-6.  Media related to Katsudō Shashin (1907 film) at Wikimedia Commons Katsudō Shashin on IMDb v t e History of anime 3Hz A.C.G.T A.P.P.P. AIC Ajia-do Animation Works Aniplex
A-1 Pictures
CloverWorks A-1 Pictures CloverWorks Arms Asahi Broadcasting Corporation
DLE DLE Asahi Production Asatsu-DK
Eiken
Gonzo
NAS Eiken Gonzo NAS Ashi Productions Asread AXsiZ Bandai Namco Holdings
Bandai Namco Arts
Actas
Sunrise
Bandai Namco Pictures
Happinet Bandai Namco Arts
Actas Actas Sunrise
Bandai Namco Pictures Bandai Namco Pictures Happinet Bee Train Production Bones Brain's Base Bridge C-Station C2C Chaos Project CoMix Wave Films Creators in Pack Cygames
CygamesPictures CygamesPictures Daume Dentsu Digital Frontier
GEMBA GEMBA Diomedéa Doga Kobo Egg Firm Eight Bit Emon Ekachi Epilka EMT Squared Encourage Films Ezo'la Feel Fuji TV
David Production David Production Frontier Works G&G Direction Gaina Gainax Gallop Geek Toys Genco GoHands Graphinica
Yumeta Company Yumeta Company GRIZZLY Hoods Entertainment IG Port
Production I.G
Signal.MD
Wit Studio Production I.G Signal.MD Wit Studio Imagin J.C.Staff Kadokawa Corporation
Dwango
ENGI Dwango ENGI Khara Kinema Citrus Knack Productions Kyoto Animation Lay-duce Magic Bus Maho Film MAPPA Marvelous
Artland Artland Millepensee Mook Animation Movic Mushi Production NAZ Nexus Nippon Animation Nippon TV
Madhouse
Tatsunoko Production Madhouse Tatsunoko Production Nomad NUT Odessa Entertainment Oh! Production OLM Orange Passione P.A.Works Pierrot Pine Jam Polygon Pictures Project No.9 Remic Robot Communications Satelight Science Saru Sega Sammy Holdings
TMS Entertainment
Marza Animation Planet TMS Entertainment
Marza Animation Planet Marza Animation Planet Seven Shaft Shirogumi Shuka Silver Link
Connect Connect Square Enix
Visual Works Visual Works Studio 4°C Studio A-Cat Studio Bind Studio Chizu Studio Comet Studio Deen Studio Ghibli Studio Gokumi Studio Hibari
Larx Entertainment
Lerche Larx Entertainment Lerche Studio Nue Studio Ponoc Studio Puyukai Studio VOLN SynergySP Tear Studio Tezuka Productions TNK Tokyo Broadcasting System
Seven Arcs (Seven Arcs Pictures) Seven Arcs (Seven Arcs Pictures) Toei Company
Toei Animation Toei Animation Troyca TV Asahi
Shin-Ei Animation Shin-Ei Animation TV Tokyo
AT-X AT-X Twin Engine
Geno Studio
Studio Colorido Geno Studio Studio Colorido Typhoon Graphics Ufotable Ultra Super Pictures
Liden Films
Ordet
Sanzigen
Trigger Liden Films Ordet Sanzigen Trigger White Fox Yaoyorozu Zero-G Zexcs Artmic Bandai Visual Group TAC Hal Film Maker J2 Communications Jetlag Productions Kitayama Eiga Seisakujo Kitty Films (Mitaka Studio) Kokusai Eiga-sha Manglobe Palm Studio Production IMS Radix Ace Entertainment Spectrum Animation Studio Fantasia Topcraft Triangle Staff Tsuchida Production Walt Disney Animation Japan Xebec The Association of Japanese Animations Japanese Animation Creators Association Animation Kobe Awards Animax Anison Grand Prix Anime Grand Prix Japan Academy Prize for Animation of the Year Japan Media Arts Awards Mainichi Film Award for Best Animation Film Newtype Anime Award Ōfuji Noburō Award Seiyu Awards Sugoi Japan Award Tokyo Anime Award Original net animation (ONA) Original video animation (OVA) Television
Late night
UHF Late night UHF Ecchi Girls with guns Harem Hentai
Tentacle erotica Tentacle erotica Isekai Lolicon Kaitō Magical girl Mecha Shotacon Yaoi Yuri Animeism Noitamina +Ultra Animage Animedia Katsudō Shashin Kinema Junpo Manga Newtype  Portal 1907 films 1907 animated films 1900s animated short films 1900s anime films 1910s animated short films 1910s anime films Anime short films Japanese films Rediscovered films Works of unknown authorship Japanese silent films Silent films in color Articles containing Japanese-language text Articles containing Turkish-language text Articles containing French-language text Featured articles Use Canadian English from June 2014 All Wikipedia articles written in Canadian English Use dmy dates from April 2019 Articles with short description CS1 uses Japanese-language script (ja) CS1 Japanese-language sources (ja) Commons category link is on Wikidata Articles containing video clips Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version العربية Azərbaycanca Deutsch Ελληνικά Español فارسی Français Bahasa Indonesia ქართული Magyar Bahasa Melayu 日本語 پنجابی Português Русский Shqip Tagalog Українська اردو Tiếng Việt 中文  This page was last edited on 11 September 2019, at 19:53 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Katsudō Shashin Matsumoto fragment ^ ^ ^ ^ ^ ^ ^ ^ a b a b c a b c d e ^ a b ^ ^ ^ a b c ^ ^ ^ a b c a b a b ^ ^ 15 
History of anime
 Active
3Hz
A.C.G.T
A.P.P.P.
AIC
Ajia-do Animation Works
Aniplex
A-1 Pictures
CloverWorks
Arms
Asahi Broadcasting Corporation
DLE
Asahi Production
Asatsu-DK
Eiken
Gonzo
NAS
Ashi Productions
Asread
AXsiZ
Bandai Namco Holdings
Bandai Namco Arts
Actas
Sunrise
Bandai Namco Pictures
Happinet
Bee Train Production
Bones
Brain's Base
Bridge
C-Station
C2C
Chaos Project
CoMix Wave Films
Creators in Pack
Cygames
CygamesPictures
Daume
Dentsu
Digital Frontier
GEMBA
Diomedéa
Doga Kobo
Egg Firm
Eight Bit
Emon
Ekachi Epilka
EMT Squared
Encourage Films
Ezo'la
Feel
Fuji TV
David Production
Frontier Works
G&G Direction
Gaina
Gainax
Gallop
Geek Toys
Genco
GoHands
Graphinica
Yumeta Company
GRIZZLY
Hoods Entertainment
IG Port
Production I.G
Signal.MD
Wit Studio
Imagin
J.C.Staff
Kadokawa Corporation
Dwango
ENGI
Khara
Kinema Citrus
Knack Productions
Kyoto Animation
Lay-duce
Magic Bus
Maho Film
MAPPA
Marvelous
Artland
Millepensee
Mook Animation
Movic
Mushi Production
NAZ
Nexus
Nippon Animation
Nippon TV
Madhouse
Tatsunoko Production
Nomad
NUT
Odessa Entertainment
Oh! Production
OLM
Orange
Passione
P.A.Works
Pierrot
Pine Jam
Polygon Pictures
Project No.9
Remic
Robot Communications
Satelight
Science Saru
Sega Sammy Holdings
TMS Entertainment
Marza Animation Planet
Seven
Shaft
Shirogumi
Shuka
Silver Link
Connect
Square Enix
Visual Works
Studio 4°C
Studio A-Cat
Studio Bind
Studio Chizu
Studio Comet
Studio Deen
Studio Ghibli
Studio Gokumi
Studio Hibari
Larx Entertainment
Lerche
Studio Nue
Studio Ponoc
Studio Puyukai
Studio VOLN
SynergySP
Tear Studio
Tezuka Productions
TNK
Tokyo Broadcasting System
Seven Arcs (Seven Arcs Pictures)
Toei Company
Toei Animation
Troyca
TV Asahi
Shin-Ei Animation
TV Tokyo
AT-X
Twin Engine
Geno Studio
Studio Colorido
Typhoon Graphics
Ufotable
Ultra Super Pictures
Liden Films
Ordet
Sanzigen
Trigger
White Fox
Yaoyorozu
Zero-G
Zexcs
Defunct
Artmic
Bandai Visual
Group TAC
Hal Film Maker
J2 Communications
Jetlag Productions
Kitayama Eiga Seisakujo
Kitty Films (Mitaka Studio)
Kokusai Eiga-sha
Manglobe
Palm Studio
Production IMS
Radix Ace Entertainment
Spectrum Animation
Studio Fantasia
Topcraft
Triangle Staff
Tsuchida Production
Walt Disney Animation Japan
Xebec
 
3Hz
A.C.G.T
A.P.P.P.
AIC
Ajia-do Animation Works
Aniplex
A-1 Pictures
CloverWorks
Arms
Asahi Broadcasting Corporation
DLE
Asahi Production
Asatsu-DK
Eiken
Gonzo
NAS
Ashi Productions
Asread
AXsiZ
Bandai Namco Holdings
Bandai Namco Arts
Actas
Sunrise
Bandai Namco Pictures
Happinet
Bee Train Production
Bones
Brain's Base
Bridge
C-Station
C2C
Chaos Project
CoMix Wave Films
Creators in Pack
Cygames
CygamesPictures
Daume
Dentsu
Digital Frontier
GEMBA
Diomedéa
Doga Kobo
Egg Firm
Eight Bit
Emon
Ekachi Epilka
EMT Squared
Encourage Films
Ezo'la
Feel
Fuji TV
David Production
Frontier Works
G&G Direction
Gaina
Gainax
Gallop
Geek Toys
Genco
GoHands
Graphinica
Yumeta Company
GRIZZLY
Hoods Entertainment
IG Port
Production I.G
Signal.MD
Wit Studio
Imagin
J.C.Staff
Kadokawa Corporation
Dwango
ENGI
Khara
Kinema Citrus
Knack Productions
Kyoto Animation
Lay-duce
Magic Bus
Maho Film
MAPPA
Marvelous
Artland
Millepensee
Mook Animation
Movic
Mushi Production
NAZ
Nexus
Nippon Animation
Nippon TV
Madhouse
Tatsunoko Production
Nomad
NUT
Odessa Entertainment
Oh! Production
OLM
Orange
Passione
P.A.Works
Pierrot
Pine Jam
Polygon Pictures
Project No.9
Remic
Robot Communications
Satelight
Science Saru
Sega Sammy Holdings
TMS Entertainment
Marza Animation Planet
Seven
Shaft
Shirogumi
Shuka
Silver Link
Connect
Square Enix
Visual Works
Studio 4°C
Studio A-Cat
Studio Bind
Studio Chizu
Studio Comet
Studio Deen
Studio Ghibli
Studio Gokumi
Studio Hibari
Larx Entertainment
Lerche
Studio Nue
Studio Ponoc
Studio Puyukai
Studio VOLN
SynergySP
Tear Studio
Tezuka Productions
TNK
Tokyo Broadcasting System
Seven Arcs (Seven Arcs Pictures)
Toei Company
Toei Animation
Troyca
TV Asahi
Shin-Ei Animation
TV Tokyo
AT-X
Twin Engine
Geno Studio
Studio Colorido
Typhoon Graphics
Ufotable
Ultra Super Pictures
Liden Films
Ordet
Sanzigen
Trigger
White Fox
Yaoyorozu
Zero-G
Zexcs
 
Artmic
Bandai Visual
Group TAC
Hal Film Maker
J2 Communications
Jetlag Productions
Kitayama Eiga Seisakujo
Kitty Films (Mitaka Studio)
Kokusai Eiga-sha
Manglobe
Palm Studio
Production IMS
Radix Ace Entertainment
Spectrum Animation
Studio Fantasia
Topcraft
Triangle Staff
Tsuchida Production
Walt Disney Animation Japan
Xebec
 
The Association of Japanese Animations
Japanese Animation Creators Association
 
Animation Kobe Awards
Animax Anison Grand Prix
Anime Grand Prix
Japan Academy Prize for Animation of the Year
Japan Media Arts Awards
Mainichi Film Award for Best Animation Film
Newtype Anime Award
Ōfuji Noburō Award
Seiyu Awards
Sugoi Japan Award
Tokyo Anime Award
 
Original net animation (ONA)
Original video animation (OVA)
Television
Late night
UHF
 
Ecchi
Girls with guns
Harem
Hentai
Tentacle erotica
Isekai
Lolicon
Kaitō
Magical girl
Mecha
Shotacon
Yaoi
Yuri
 
Animeism
Noitamina
+Ultra
 
Animage
Animedia
Katsudō Shashin
Kinema Junpo
Manga
Newtype
 
 Portal
 