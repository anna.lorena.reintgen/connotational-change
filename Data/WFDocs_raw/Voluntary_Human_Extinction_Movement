The Voluntary Human Extinction Movement (VHEMT[A]) is an environmental movement that calls for all people to abstain from reproduction to cause the gradual voluntary extinction of humankind. VHEMT supports human extinction primarily because, in the group's view, it would prevent environmental degradation. The group states that a decrease in the human population would prevent a significant amount of human-caused suffering. The extinctions of non-human species and the scarcity of resources required by humans are frequently cited by the group as evidence of the harm caused by human overpopulation.
 VHEMT was founded in 1991 by Les U. Knight  , an American activist who became involved in the environmental movement in the 1970s and thereafter concluded that human extinction was the best solution to the problems facing the Earth's biosphere and humanity. Knight publishes the group's newsletter and serves as its spokesman. Although the group is promoted by a website and represented at some environmental events, it relies heavily on coverage from outside media to spread its message. Many commentators view its platform as unacceptably extreme, though other writers have applauded VHEMT's perspective. In response to VHEMT, some journalists and academics have argued that humans can develop sustainable lifestyles or can reduce their population to sustainable levels. Others maintain that, whatever the merits of the idea, the human reproductive drive will prevent humankind from ever voluntarily seeking extinction.
 The Voluntary Human Extinction Movement was founded by Les U. Knight,[2][3][B] a high school substitute teacher living in Portland, Oregon.[2] After becoming involved in the environmental movement as a college student in the 1970s, Knight attributed most of the dangers faced by the planet to human overpopulation.[2][5] He joined the Zero Population Growth organization,[2] and chose to be vasectomised at age 25.[5] He later concluded that the extinction of humanity would be the best solution to the Earth's environmental problems.[2] He believes that this idea has also been held by some people throughout human history.[6]
 In 1991, Knight began publishing VHEMT's newsletter,[2] known as These Exit Times.[3] In the newsletter, he asked readers to further human extinction by not procreating.[2] VHEMT has also published cartoons,[7] including a comic strip titled Bonobo Baby, featuring a woman who forgoes childbearing in favor of adopting a bonobo.[3] In 1996, Knight created a website for VHEMT;[8] it was available in 11 languages by 2010.[9] VHEMT's logo features the letter "V" (for voluntary) and a picture of the Earth with north at the bottom.[10][C]
 VHEMT functions as a loose network rather than a formal organization,[11] and does not compile a list of members. Daniel Metz of Willamette University stated in 1995 that VHEMT's mailing list had just under 400 subscribers.[2] Six years later, Fox News said the list had only 230 subscribers.[12] Knight says that anyone who agrees with his ideology is a member of the movement;[2] and that this includes "millions of people".[13][D]
 Knight serves as the spokesman for VHEMT.[2] He attends environmental conferences and events, where he publicizes information about population growth.[9] VHEMT's message has, however, primarily been spread through coverage by media outlets, rather than events and its newsletter.[8] VHEMT sells buttons and T-shirts,[8] as well as bumper stickers that read "Thank you for not breeding".[3]
 In 2018, a supporter of the movement appeared on the popular YouTube channel LAHWF in a video called, "Chatting with a Supporter of the Voluntary Human Extinction Movement".[15]
 —VHEMT Website[16]
 Knight argues that the human population is far greater than the Earth can handle, and that the best thing for Earth's biosphere is for humans to voluntarily cease reproducing.[17] He says that humans are "incompatible with the biosphere"[3] and that human existence is causing environmental damage which will eventually bring about the extinction of humans (as well as other organisms).[18] According to Knight, the vast majority of human societies have not lived sustainable lifestyles,[5] and attempts to live environmentally friendly lifestyles do not change the fact that human existence has ultimately been destructive to the Earth and many of its non-human organisms.[3] Voluntary human extinction is promoted on the grounds that it will prevent human suffering and the extinction of other species; Knight points out that many species are threatened by the increasing human population.[2][12][17]
 James Ormrod, a psychologist who profiled the group in the journal Psychoanalysis, Culture & Society, notes that the "most fundamental belief" of VHEMT is that "human beings should stop reproducing", and that some people consider themselves members of the group but do not actually support human extinction.[8] Knight, however, believes that even if humans become more environmentally friendly, they could still return to environmentally destructive lifestyles and hence should eliminate themselves.[5] Residents of First World countries bear the most responsibility to change, according to Knight, as they consume the largest proportion of resources.[19]
 Knight believes that Earth's non-human organisms have a higher overall value than humans and their accomplishments, such as art: "The plays of Shakespeare and the work of Einstein can't hold a candle to a tiger".[3] He argues that species higher in the food chain are less important than lower species.[3] His ideology is drawn in part from deep ecology, and he sometimes refers to the Earth as Gaia.[20] He notes that human extinction is unavoidable, and that it is better to become extinct soon to avoid causing the extinction of other animals.[17] The potential for evolution of other organisms is also cited as a benefit.[8]
 Knight sees abstinence from reproduction as an altruistic choice[5] – a way to prevent involuntary human suffering[21] – and cites the deaths of children from preventable causes as an example of needless suffering.[5] Knight claims that non-reproduction would eventually allow humans to lead idyllic lifestyles in an environment comparable to the Garden of Eden,[22] and maintains that the last remaining humans would be proud of their accomplishment.[23] Other benefits of ceasing human reproduction that he cites include the end of abortion, war, and starvation.[22] Knight argues that "procreation today is de facto child abuse".[20] He maintains that the standard of human life will worsen if resources are consumed by a growing population rather than spent solving existing issues.[20] He speculates that if people ceased to reproduce, they would use their energy for other pursuits,[3] and suggests adoption and foster care as outlets for people who desire children.[5]
 VHEMT rejects government-mandated human population control programs in favor of voluntary population reduction,[2] supporting only the use of birth control and willpower to prevent pregnancies.[3] Knight states that coercive tactics are unlikely to permanently lower the human population, citing the fact that humanity has survived catastrophic wars, famines, and viruses.[9] Though their newsletter's name recalls the suicide manual Final Exit,[18] the idea of mass suicide is rejected,[19] and they have adopted the slogan "May we live long and die out".[5] A 1995 survey of VHEMT members found that a majority of them felt a strong moral obligation to protect the Earth, distrusted the ability of political processes to prevent harm to the environment, and were willing to surrender some of their rights for their cause. VHEMT members who strongly believed that "Civilization [is] headed for collapse" were most likely to embrace these views.[24] However, VHEMT does not take any overt political stances.[8]
 VHEMT promotes a more extreme ideology than Population Action International, a group that argues humanity should reduce—but not eliminate—its population to care for the Earth. However, the VHEMT platform is more moderate and serious than the Church of Euthanasia, which advocates population reduction by suicide and cannibalism.[12][22] The 1995 survey found that 36% considered themselves members of Earth First! or had donated to the group in the previous five years.[25]
 Knight states his group's ideology runs counter to contemporary society's natalism. He believes this pressure has stopped many people from supporting, or even discussing, population control.[5] He admits that his group is unlikely to succeed, but contends that attempting to reduce the Earth's population is the only moral option.[3]
 Reception of Knight's idea in the mainstream media has been mixed. Writing in the San Francisco Chronicle, Gregory Dicum states that there is an "undeniable logic" to VHEMT's arguments, but he doubts whether Knight's ideas can succeed, arguing that many people desire to have children and cannot be dissuaded.[5] Stephen Jarvis echoes this skepticism in The Independent, noting that VHEMT faces great difficulty owing to the basic human reproductive drive.[3] At The Guardian's website, Guy Dammann applauds the movement's aim as "in many ways laudable", but argues that it is absurd to believe that humans will voluntarily seek extinction.[26] Freelance writer Abby O'Reilly writes that since having children is frequently viewed as a measure of success, VHEMT's goal is difficult to attain.[27] Knight contends in response to these arguments that though sexual desire is natural, human desire for children is a product of enculturation.[3]
 The Roman Catholic Archdiocese of New York has criticized Knight's platform, arguing that the existence of humanity is divinely ordained.[12] Ormrod claims that Knight "arguably abandons deep ecology in favour of straightforward misanthropy". He notes that Knight's claim that the last humans in an extinction scenario would have an abundance of resources promotes his cause based on "benefits accruing to humans". Ormrod sees this type of argument as counter-intuitive, arguing that it borrows the language of "late-modern consumer societies". He faults Knight for what he sees as a failure to develop a consistent and unambiguous ideology.[20] The Economist characterizes Knight's claim that voluntary human extinction is advisable due to limited resources as "Malthusian bosh". The paper further states that compassion for the planet does not necessarily require the pursuit of human extinction.[2] Sociologist Frank Furedi also deems VHEMT to be a Malthusian group, classifying them as a type of environmental organization that "[thinks] the worst about the human species".[28] Writing in Spiked, Josie Appleton argues that the group is indifferent to humanity, rather than "anti-human".[29]
 Brian Bethune writes in Maclean's that Knight's logic is "as absurd as it's unassailable". However, he doubts Knight's claim that the last survivors of the human race would have pleasant lives and suspects that a "collective loss of the will to live" would prevail.[22] In response to Knight's platform, journalist Sheldon Richman argues that humans are "active agents" and can change their behavior. He contends that people are capable of solving the problems facing Earth.[17] Alan Weisman, author of The World Without Us, suggests a limit of one child per family as a preferable alternative to abstinence from reproduction.[22]
 Katharine Mieszkowski of Salon.com recommends that childless people adopt VHEMT's arguments when facing "probing questions" about their childlessness.[30] Writing in the Journal for Critical Animal Studies, Carmen Dell'Aversano notes that VHEMT seeks to renounce children as a symbol of perpetual human progress. She casts the movement as a form of "queer oppositional politics" because it rejects perpetual reproduction as a form of motivation. She argues that the movement seeks to come to a new definition of "civil order", as Lee Edelman suggested that queer theory should. Dell'Aversano believes that VHEMT fulfills Edelman's mandate because they embody the death drive rather than ideas that focus on the reproduction of the past.[31]
 Although Knight's organization has been featured in a book titled Kooks: A Guide to the Outer Limits of Human Belief,[2] The Guardian journalist Oliver Burkeman notes that in a phone conversation Knight seems "rather sane and self-deprecating".[32] Weisman echoes this sentiment, characterizing Knight as "thoughtful, soft-spoken, articulate, and quite serious".[29] Philosophers Steven Best and Douglas Kellner view VHEMT's stance as extreme, but they note that the movement formed in response to extreme stances found in "modern humanism".[33]
 
 
 1 History 2 Organization and promotion 3 Ideology 4 Reception 5 See also 6 Notes 7 References 8 Bibliography 9 Further reading 10 External links Environment portal Anti-environmentalism Antinatalism Carrying capacity Childfree David Benatar, an advocate of antinatalism Deep ecology Earth liberation Green anarchy Nature worship Negative Population Growth Radical environmentalism ^ VHEMT is pronounced "vehement",[1] because, according to Knight, that is what they are.[2]
 ^ Knight denies that he is the founder, saying that "I'm not the founder of VHEMT, I just gave it a name".[4]
 ^ VHEMT states that the inverted Earth represents the radical shift in human direction the movement seeks, and notes that upside down emblems are often used as symbols of distress.[10]
 ^ On its website, VHEMT characterizes the participants in its movement as "volunteer", "supporter", or "undecided", each of whom share an interest in a reduction in the rate of human births.[14]
 ^ Pesca, Mike (May 12, 2006). "All Choked Up". NPR. Retrieved January 7, 2012..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}
 ^ a b c d e f g h i j k l m n o "Sui genocide". The Economist. December 17, 1998. Retrieved January 7, 2012.
 ^ a b c d e f g h i j k l m Jarvis, Stephen (April 24, 1994). "Live long and die out: Stephen Jarvis encounters the Voluntary Human Extinction Movement". The Independent. Retrieved January 7, 2012.
 ^ "Personal information about Les U. Knight". The Voluntary Human Extinction Movement. Retrieved March 23, 2012.
 ^ a b c d e f g h i j Dicum, Gregory (November 16, 2005). "Maybe None". San Francisco Chronicle. Retrieved January 7, 2012.
 ^ Ormrod 2011, p. 142.
 ^ "Sites answer 300 million questions". Barre Montpelier Times Argus. October 22, 2006. Archived from the original on October 19, 2013. Retrieved March 9, 2012.
 ^ a b c d e f Ormrod 2011, p. 143.
 ^ a b c Weisman 2010, p. 310.
 ^ a b "Symbolism of the logo for the Voluntary Human Extinction Movement". Voluntary Human Extinction Movement. Retrieved January 22, 2012.
 ^ Ormrod 2011, pp. 142–3.
 ^ a b c d Park, Michael Y. (July 29, 2001). "Anti-People Group Pushes for Man's Extinction". Fox News. Archived from the original on April 29, 2011. Retrieved February 19, 2012.
 ^ 
"About The Movement — When and how did VHEMT start?". The Voluntary Human Extinction Movement. Retrieved January 23, 2012.
 ^ 
"How to join VHEMT". The Voluntary Human Extinction Movement. Retrieved January 28, 2012.
 ^ LAHWF (2018-11-17), Chatting with a Supporter of the Voluntary Human Extinction Movement, retrieved 2019-07-06
 ^ "SUCCESS". Voluntary Human Extinction Movement Website. vhemt.org.
 ^ a b c d 
Keck, Kristi (October 5, 2007). "Earth a gracious host to billions, but can she take many more?". CNN Technology. Retrieved January 27, 2012.
 ^ a b Savory, Eve (September 4, 2008). "VHEMT: The case against humans". CBC News. Retrieved January 7, 2012.
 ^ a b Buarque, Daniel (October 31, 2011). "Cada pessoa nova é um fardo para o planeta, diz movimento da extinção" [Every new person is a burden on the planet, says an extinction movement]. G1 (in Portuguese). Retrieved January 7, 2012.
 ^ a b c d Ormrod 2011, p. 158.
 ^ "Breeding to Death". New Scientist. May 15, 1999. p. 19. (subscription required)
 ^ a b c d e Bethune, Brian (August 6, 2007). "Please refrain from procreating". Maclean's. Archived from the original on June 25, 2013. Retrieved January 7, 2012.
 ^ Weisman 2010, p. 312.
 ^ Ellis 1998, p. 267.
 ^ Ellis 1998, p. 382–3.
 ^ Dammann, Guy (December 28, 2008). "Am I fit to breed?". The Guardian. Retrieved January 7, 2012.
 ^ O'Reilly, Abby (November 24, 2007). "No more babies, please". The Guardian. Retrieved January 7, 2012.
 ^ Furedi, Frank (September 12, 2007). "Environmentalism". Spiked. Retrieved March 9, 2012.
 ^ a b Appleton, Josie (July 20, 2007). "Unleashing nature's terror". Spiked. Archived from the original on May 21, 2013. Retrieved March 9, 2012.
 ^ Mieszkowski, Katharine (November 16, 2005). "No need to breed?". Salon. Archived from the original on September 24, 2015. Retrieved January 7, 2012.
 ^ 
Carmen Dell'Aversano (2010). "The Love Whose Name Cannot be Spoken: Queering the Human–Animal Bond" (PDF). Journal for Critical Animal Studies. VIII (1/2): 73–126. Retrieved February 25, 2012.
 ^ Burkeman, Oliver (February 12, 2010). "Climate change: calling planet birth". The Guardian. Retrieved January 7, 2012.
 ^ Best & Kellner 2001, p. 268–9.
 Best, Steven; Kellner, Douglas (2001). The Postmodern Adventure: Science, Technology, and Cultural Studies at the Third Millennium. Guilford Press. ISBN 978-1-57230-665-3. Ellis, Richard J. (1998). The Dark Side of the Left: Illiberal Egalitarianism in America. University Press of Kansas. ISBN 978-0-7006-1030-3. Ormrod, James S. (2011). "'Making room for the tigers and the polar bears': Biography, phantasy and ideology in the Voluntary Human Extinction Movement". Psychoanalysis, Culture & Society. 16 (2): 142–61. doi:10.1057/pcs.2009.30. Weisman, Alan (2010). The World Without Us. HarperCollins. ISBN 978-1-4434-0008-4. Adams, Guy (April 19, 2007), "How to save the planet: According to some eco-extremists, the only way to really make a difference is to stop breeding and let the human race die out.", The Independent Hymas, Lisa (July 19, 2010), "Want to join the Voluntary Human Extinction Movement?", Grist Anti-People Group Pushes for Man's Extinction, Fox News, July 29, 2001, archived from the original on 2013-05-17, retrieved 2014-12-17 Official website Voluntary Human Extinction Movement blog (U.S.), (India) Les U. Knight's profile at Blogger.com Focus Earth: No More Children. Planet Green Videos. Discovery Communications. (An interview with Les U. Knight) Taking on the Voluntary Human Extinction Movement. msnbc.com. v t e Biocapacity Demographics of the world Optimum population Overpopulation
Malthusian catastrophe Malthusian catastrophe Population Population ethics Population momentum Sustainable development Women's reproductive rights Zero population growth Family planning
Pledge two or fewer Pledge two or fewer Human population planning
One-child policy
Two-child policy One-child policy Two-child policy Population biology Population decline Population density
Physiological density Physiological density Population dynamics Population growth Population model Population pyramid Projections of population growth Deforestation Desalination Desertification Environmental impact
of agriculture
of aviation
of biodiesel
of concrete
of electricity generation
of the energy industry
of fishing
of irrigation
of mining
of off-roading
of oil shale industry
of palm oil
of paper
of the petroleum industry
of reservoirs
of shipping
of war of agriculture of aviation of biodiesel of concrete of electricity generation of the energy industry of fishing of irrigation of mining of off-roading of oil shale industry of palm oil of paper of the petroleum industry of reservoirs of shipping of war Industrialisation Land degradation Land reclamation Overconsumption Pollution Quarrying Urbanization
Loss of green belts
Urban sprawl Loss of green belts Urban sprawl Waste Water scarcity
Overdrafting Overdrafting Carrying capacity Deep ecology Earth's energy budget Food security Habitat destruction I = P × A  × T Kaya identity Malthusian growth model Overshoot (population) World energy consumption World energy resources World3 model A Modest Proposal Observations Concerning the Increase of Mankind, Peopling of Countries, etc. An Essay on the Principle of Population "How Much Land Does a Man Need?" Operating Manual for Spaceship Earth Population Control: Real Costs, Illusory Benefits The Limits to Growth The Population Bomb The Skeptical Environmentalist The Ultimate Resource Population and Environment Population and Development Review Population and housing censuses by country Metropolitan areas by population Population milestone babies 7 Billion Actions Extinction Rebellion International Conference on Population and Development Population Action International Population Connection Population Matters Population Research Institute United Nations Population Fund Voluntary Human Extinction Movement World Population Day World Population Foundation Classic Maya collapse Fertility and intelligence Green Revolution Holocene extinction Migration  Commons  Human overpopulation Human activities with impact on the environment Human migration v t e Background extinction rate Coextinction De-extinction Ecological extinction Extinct in the wild Functional extinction Genetic pollution Lazarus taxon Local extinction Pseudoextinction Extinction vortex Genetic erosion Habitat destruction Human overpopulation Muller's ratchet Mutational meltdown Overexploitation Paradox of enrichment Overabundant species Extinction debt Extinction risk from global warming Extinction threshold Field of Bullets Hypothetical species Latent extinction risk Ordovician–Silurian Late Devonian Permian–Triassic Triassic–Jurassic Cretaceous–Paleogene Holocene
Timeline Timeline Great Oxidation End-Ediacaran End-Botomian Dresbachian Cambrian–Ordovician Ireviken Mulde Lau Carboniferous Olson's End-Capitanian Carnian Pluvial Toarcian End-Jurassic or Tithonian Aptian Cenomanian-Turonian Eocene–Oligocene Middle Miocene Pliocene–Pleistocene Quaternary Lists of extinct species
Lists of extinct animals
List of extinct plants Lists of extinct animals List of extinct plants IUCN Red List extinct species International Union for Conservation of Nature IUCN Species Survival Commission Voluntary Human Extinction Movement Decline in amphibian populations Decline in insect populations Human extinction  Category  Commons 1991 establishments in Oregon Antinatalism Deep ecology Human overpopulation Environmental movements Radical environmentalism Population organizations Organizations based in Portland, Oregon Human extinction CS1 Portuguese-language sources (pt) Pages containing links to subscription-only content Spoken articles Articles with hAudio microformats Official website different in Wikidata and Wikipedia Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version العربية Asturianu Беларуская Català Čeština Deutsch Español Esperanto فارسی Français 한국어 Italiano עברית Latina Lietuvių Nederlands 日本語 Norsk Oʻzbekcha/ўзбекча Polski Português Русский Simple English Suomi Українська Tiếng Việt 中文  This page was last edited on 8 November 2019, at 01:24 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Voluntary Human Extinction Movement VHEMT ^ ^ ^ ^ ^ a b c d e f g h i j k l m n o a b c d e f g h i j k l m ^ a b c d e f g h i j ^ ^ a b c d e f a b c a b ^ a b c d ^ ^ ^ ^ a b c d a b a b a b c d ^ a b c d e ^ ^ ^ ^ ^ ^ a b ^ ^ VIII ^ ^ 16 Listen to this article More spoken articles Commons  May we live long and die out 1991 NGO Les U. Knight vhemt.org 
Biocapacity
Demographics of the world
Optimum population
Overpopulation
Malthusian catastrophe
Population
Population ethics
Population momentum
Sustainable development
Women's reproductive rights
Zero population growth
 
Family planning
Pledge two or fewer
Human population planning
One-child policy
Two-child policy
Population biology
Population decline
Population density
Physiological density
Population dynamics
Population growth
Population model
Population pyramid
Projections of population growth
 
Deforestation
Desalination
Desertification
Environmental impact
of agriculture
of aviation
of biodiesel
of concrete
of electricity generation
of the energy industry
of fishing
of irrigation
of mining
of off-roading
of oil shale industry
of palm oil
of paper
of the petroleum industry
of reservoirs
of shipping
of war
Industrialisation
Land degradation
Land reclamation
Overconsumption
Pollution
Quarrying
Urbanization
Loss of green belts
Urban sprawl
Waste
Water scarcity
Overdrafting
 
Carrying capacity
Deep ecology
Earth's energy budget
Food security
Habitat destruction
I = P × A  × T
Kaya identity
Malthusian growth model
Overshoot (population)
World energy consumption
World energy resources
World3 model
 
A Modest Proposal
Observations Concerning the Increase of Mankind, Peopling of Countries, etc.
An Essay on the Principle of Population
"How Much Land Does a Man Need?"
Operating Manual for Spaceship Earth
Population Control: Real Costs, Illusory Benefits
The Limits to Growth
The Population Bomb
The Skeptical Environmentalist
The Ultimate Resource
 
Population and Environment
Population and Development Review
 
Population and housing censuses by country
Metropolitan areas by population
Population milestone babies
 
7 Billion Actions
Extinction Rebellion
International Conference on Population and Development
Population Action International
Population Connection
Population Matters
Population Research Institute
United Nations Population Fund
Voluntary Human Extinction Movement
World Population Day
World Population Foundation
 
Classic Maya collapse
Fertility and intelligence
Green Revolution
Holocene extinction
Migration
 
 Commons
 Human overpopulation
Human activities with impact on the environment
Human migration
 
Background extinction rate
Coextinction
De-extinction
Ecological extinction
Extinct in the wild
Functional extinction
Genetic pollution
Lazarus taxon
Local extinction
Pseudoextinction
  
Extinction vortex
 
Genetic erosion
Habitat destruction
Human overpopulation
Muller's ratchet
Mutational meltdown
Overexploitation
Paradox of enrichment
Overabundant species
 
Extinction debt
Extinction risk from global warming
Extinction threshold
Field of Bullets
Hypothetical species
Latent extinction risk
 Major
Ordovician–Silurian
Late Devonian
Permian–Triassic
Triassic–Jurassic
Cretaceous–Paleogene
Holocene
Timeline
Other
Great Oxidation
End-Ediacaran
End-Botomian
Dresbachian
Cambrian–Ordovician
Ireviken
Mulde
Lau
Carboniferous
Olson's
End-Capitanian
Carnian Pluvial
Toarcian
End-Jurassic or Tithonian
Aptian
Cenomanian-Turonian
Eocene–Oligocene
Middle Miocene
Pliocene–Pleistocene
Quaternary
 
Ordovician–Silurian
Late Devonian
Permian–Triassic
Triassic–Jurassic
Cretaceous–Paleogene
Holocene
Timeline
 
Great Oxidation
End-Ediacaran
End-Botomian
Dresbachian
Cambrian–Ordovician
Ireviken
Mulde
Lau
Carboniferous
Olson's
End-Capitanian
Carnian Pluvial
Toarcian
End-Jurassic or Tithonian
Aptian
Cenomanian-Turonian
Eocene–Oligocene
Middle Miocene
Pliocene–Pleistocene
Quaternary
 
Lists of extinct species
Lists of extinct animals
List of extinct plants
IUCN Red List extinct species
 
International Union for Conservation of Nature
IUCN Species Survival Commission
Voluntary Human Extinction Movement
 
Decline in amphibian populations
Decline in insect populations
Human extinction
 
 Category
 Commons
 