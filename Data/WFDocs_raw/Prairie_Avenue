Prairie Avenue is a north–south street on the South Side of Chicago, which historically extended from 16th Street in the Near South Side community area of Chicago in Cook County, Illinois, United States, to the city's southern limits and beyond. The street has a rich history from its origins as a major trail for horseback riders and carriages. During the last three decades of the 19th century, a six-block section of the street served as the residence of many of Chicago's elite families and an additional four-block section was also known for grand homes. The upper six-block section includes part of the historic Prairie Avenue District, which was declared a Chicago Landmark and added to the National Register of Historic Places.
 Several of Chicago's most important historical figures have lived on the street. This is especially true of the period of recovery from the Great Chicago Fire of 1871 when many of the most important families in the city moved to the street. Residents of the street have influenced the evolution of the city and have played prominent national and international roles. They have influenced the political history, the architecture, the culture, the economy, as well as the law and government of Chicago. The street has over time been influenced by the demographics of Chicago.
 The importance of the street declined, but it still has landmark buildings and is the backbone of a historic district. Preservation battles regarding various properties on the street have been notable with one having been chronicled on the front page of The New York Times. In the early 21st century, parts of the street were redeveloped to host townhouses and condominiums. In the late 20th century and early 21st century the street was extended north to accommodate new high-rise condominiums, such as One Museum Park, along Roosevelt Road (12th Street).  The redevelopment extended the street so that it has prominent buildings bordering Grant Park with Prairie Avenue addresses.
 Prairie Avenue once served as an Indian trail linking Fort Dearborn to Fort Wayne in Indiana and thus derived its name from the vast midwestern prairie land between the two endpoints.[1] In 1812, the Battle of Fort Dearborn occurred in the area that is now the northern section of the street, in what is known as the Near South Side community area.[2] Casualties of the battle, such as William Wells and George Ronan, were struck down here.[3]
 Over time, the district has evolved from an upscale neighborhood to a factory district and back to an upscale neighborhood. Zoning in 1853 anticipated residential development, although only one grand villa existed at the time. By 1877 the eleven-block area of Prairie Avenue as well as Calumet Avenue housed elite residences. By 1886 the finest mansions in the city, each equipped with its own carriage house, stood on Prairie Avenue.[4] In the 1880s and 1890s, mansions for George Pullman, Marshall Field, John J. Glessner and Philip Armour anchored a neighborhood of over fifty mansions known as "Millionaire's Row".[1] Many of the leading architects of the day, such as Richard Morris Hunt, Henry Hobson Richardson and Daniel Burnham designed mansions on the street. At the time of the 1893 World's Columbian Exposition, guidebooks described the street as "the most expensive street west of Fifth Avenue".[5][6] However, after Bertha Palmer, society wife of Potter Palmer, built the Palmer Mansion that anchored the Gold Coast along Lake Shore Drive, the elite residents began to move north.[1]
 By 1911, warehouses and factories cramped the Prairie Avenue District. Large industry overtook the district by 1950. Early 21st century deindustrialisation, urban congestion, and historic preservation have brought the return of trendy buildings, and restored as well as renovated structures. Simultaneously new infill housing is resuscitating the district.[4] Now, the historic northern section of the street is part of the Chicago Landmark Prairie Avenue District that is listed on the National Register of Historic Places.[7][8] It was declared a Chicago Landmark on December 27, 1979, and added to the National Register of Historic Places on November 15, 1972.[7][9] The historic district includes the 1800 and 1900-blocks of South Prairie, the 1800 block of South Indiana and 211 through 217 East Cullerton.[10]
 In the 1850s, railroad related industries prospered near the lumber district along the South Branch of the Chicago River. Thus, the business district began to supplant the elegant residences along Michigan and Wabash Avenues south of Jackson Boulevard.[11] Shortly after the Civil War, the city's wealthy residents settled on Prairie Avenue due to its proximity to the Loop less than a mile away and the fact that traveling there did not involve crossing the Chicago River. In 1870, Daniel Thompson erected the first large upper-Prairie Avenue home.  Marshall Field followed in 1871 with a Richard Morris Hunt design.[12]  Prairie Avenue was the most posh Chicago address by the time of the Great Chicago Fire of 1871.[11]
 Many of South Michigan Avenue's elegant villas were destroyed in the Great Chicago Fire of 1871.[13]  The post-fire South Side of Chicago grew rapidly as all economic classes left the city's center. Many of Chicago's elite families settled along Prairie Avenue. By the 1870s and 1880s, Prairie Avenue was the location of elaborate houses between 16th Street and 22nd Street (now Cermak Road).[14] In 1886, the urban elite, including George Pullman, Marshall Field, Philip Armour and John B. Sherman all owned family homes in this area that created an opulent Prairie Avenue streetscape reminiscent of European city streets;[13] as such, it was widely regarded as the city's most fashionable neighborhood. Businesses, such as the Pullman Company, Armour & Company and D.H. Burnham & Company, with ties to Prairie Avenue had national and international reach and impact.[15]  Additional grand homes (including many Queen Anne style architecture in the United States and Richardsonian Romanesque) were located on Prairie between 26th and 30th Streets starting in the mid-1880s.[12]  The last mansion, a three-story Georgian Revival residence with 21 rooms, was built in the district at 2126 Prairie Avenue in 1905.[5]
 However, as the start of the 20th century came, industry's pervasive reach, increased railroad soot, and an encroaching vice district, caused the area to become less desirable, and the social elite vacated the region for quieter neighborhoods such as Kenwood, the Gold Coast and more commonly the suburban North Shore.[13]  The Chicago Tribune highlighted 1898 Prairie Avenue as a place that was undesirable to those for whom it was affordable, and unaffordable to those for whom it was desirable.[16]  Light industry and vacant lots overtook Prairie Avenue during the second half of the 20th century. The elegant mansions were mostly torn down or fell into extreme disrepair.[12]  By the 1970s, most of the residential buildings had been replaced by factories and parking lots.[6]  Starting in the late 1990s, the downtown housing market flourished in Chicago and the resulting boom that has transformed many neighborhoods revived Prairie Avenue, causing most of the factories to be demolished or converted to loft condominiums.  The factories have been replaced by condominium projects and most of the remaining mansions have been renovated.[6]
 During the 1870s, 1880s and 1890s, upper Prairie Avenue residents were central to cultural and social fabric of the city. The economy was supported by the thousands of jobs created by the Pullman Car Company and Armour and Company. Chicago's richest man, Marshall Field, changed the buying habits of the city. John Shorthall saved the property from total chaos after the Great Chicago Fire by saving property records. At one point in the 1880s, sixteen of the 60 members of the Commercial Club of Chicago lived on Prairie Avenue.  George Armour headed the Chicago Academy of Fine Arts, which became the Art Institute of Chicago.[17]  1801 South Prairie resident, William Wallace Kimball, employed about 1500 people around the start of the 20th century in his organ and piano manufacturing company.[18]  John Glessner, a founder of International Harvester, built what has been described as the centerpiece of the historic district.[5]
 As a home to many of Chicago's leading families, Prairie Avenue became the base of many important political movements. Woman's suffrage had activists, such as Illinois Women Suffrage Association President Jane Jones, on Prairie Avenue.  Illinois Central Railroad Co. v. State of Illinois, 146 U.S. 387 (1892), pitted the public welfare of the city against the railroad industry and was the foundation for the public trust doctrine which facilitated the city's reclamation of much of the lakefront. Prairie Avenue residents bolstered other efforts to fight against the railroads. The concentration of wealth also made Prairie Avenue the target of complaints about taxation inequities.[19]
 Many of these leading families also took part in philanthropy.  John Shorthall, founder of Chicago Title & Trust and Prairie Avenue resident, created the Illinois Society for the Prevention of Cruelty to Animals and convened local and state societies to unite under a national organization (American Society for the Prevention of Cruelty to Animals) that could combine its political strength and lobby Congress. The Illinois Institute of Technology was a successor entity of the Armour Institute of Technology, which was an outgrowth of the generosity of Philip and Joseph Armour.[20]
 Historic preservation in Chicago has saved some of the city's architectural heritage. The efforts of the Chicago Architecture Foundation and the Landmarks and Preservation Council of Illinois have been at the forefront of these efforts. The Commission on Chicago Landmarks (now part of the city's Department of Planning and Development) has designated the Prairie Avenue Historic District as a city landmark.[21]
 A few of the mansions of the heyday still remain in the 1800-block including the National Historic landmark designated John J. Glessner House designed in 1886 by architect Henry H. Richardson for Glessner;[11][22]  these provide a sense of the street's former character.  Glessner House, which was the subject of a notable preservation battle in the 1960s,[6] is considered to be Richardson's finest urban residence.[5] This district includes the Henry B. Clarke House, which although twice relocated is purported to be the city's oldest standing house.  In addition to the Clarke House and the Glessner House, nine other houses from the late-19th century remain in the historic district portion of Prairie Avenue.[5][21]  Both the Glessner House and the Clarke House are on the National Historic Register and now serve as museums.[21]  Most of the Prairie Avenue families worshiped at the Chicago Landmark Second Presbyterian Church of Chicago, which is listed on the National Register of Historic Places.[23]
 Marshall Field lived at 1905 South Prairie and purchased 1919 South Prairie for Marshall Field, Jr.  It is believed that Solon Spencer Beman had contributed to the design of what is now known as the Marshall Field, Jr. Mansion. Then, Field hired Daniel Burnham to design extensions and additions to the property after purchasing it 1890.[10]  In 2007, the Commission on Chicago Landmarks announced the rehabilitation of the Marshall Field Jr. Mansion, which had been vacant for 40 years and which was renovated as six private residences, won a Preservation Award.[24]
 Today, Prairie Avenue has buildings indexed in the Chicago Historic Resources Survey in the Near South Side, Douglas, Grand Boulevard, Washington Park and Chatham community areas.[25]  Among the properties listed is a simple two-flat used by Al Capone in the 1920s at 7244 South Prairie in Greater Grand Crossing.[26]  Other current prominent addresses are the Kimball House at 1801 South Prairie (Near South Side),[23] 2801, 3564, 3566, and 3600 South Prairie (Douglas),[27] and 4919 South Prairie (Grand Boulevard).[28]
 The William Wallace Kimball House, which is a three-story turreted chateau, was designed by Solon Beman, who is best known for his work in the Pullman District of the Pullman community area.[5]  Adjacent to the Kimball House and across from the Glessner House is the Coleman-Ames mansion at 1811 South Prairie.[5]  These two houses were formerly owned by R.R. Donnelley & Sons Company and now jointly serve as the national headquarters for the United States Soccer Federation (USSF), which leased them from 1991 until 1998 when it purchased them from the Chicago Architectural Foundation.[5]  The Kimball house, which has been the product of a $1 million renovation in the 1990s by the USSF was featured in Richard Gere's Primal Fear as well as several television shows.[5]
 Al Capone and his family lived in the two-story red brick duplex at 7244 South Prairie Avenue from 1923, which is shortly after he moved to Chicago, until 1931, when he was sent off to prison for income tax fraud.[29]  The Capone family kept the home until his mother's death in 1952. In 1988, the privately owned house was nominated for the National Register of Historic Places by historians as the home of one of Chicago's most famous citizens.[30] The nomination was withdrawn after local politicians and members of Italian-American groups sharply argued that it would appear to validate the life of a murderer and hoodlum.  The house retains the security bars on the basement windows and the brick garage out back, which the Capone built for his bullet-proof Cadillac limousine.[29]
 In 2000, the Howard Van Doren Shaw-designed 1907 Georgian Revival Platt Luggage Building at 2301 South Prairie was the subject of preservation debates when McCormick Place attempted to tear it down to build a parking garage.  The conflict, which was not settled before wreckers had knocked a hole in a corner of the building and which included protests and a petition to the Illinois Supreme Court, was described on the front page of The New York Times.[31] Preservationists, including the Landmarks Preservation Council of Illinois and the National Trust for Historic Preservation, eventually dropped their appeals once the Metropolitan Pier and Exposition Authority committed to incorporating the original facade of the building into the exterior of the parking garage at an additional cost of $2.5 million to the project.[32][33] The Harriet F. Rees House at 2110 South Prairie was spared demolition in 2014 and moved one block north to 2017 South Prairie.[34]
 A book on the history of the street, entitled Chicago's Historic Prairie Avenue, was published on June 2, 2008, as part of Arcadia Publishing Co.'s Images of America series.  William H. Tyre is the author.[35] In 2006, the Prairie District Neighborhood Alliance, a non-profit organization, was formed to provide representation for thousands of South Loop residents, including the Prairie District, Central Station and Museum Park, Motor Row, the South Michigan Ave Corridor, as well as other areas of the Near South Side.[36]
 In 2003, the area redevelopment was well underway. Deindustrialization and urbanization had pushed out manufacturing. As a result, factories were generally demolished, or converted to loft apartment buildings. Some neglected mansions survive as restored or renovated properties in the historic district.[4] Today, Prairie Avenue is undergoing a redevelopment that includes One Museum Park (1215 South Prairie Avenue) and One Museum Park West (1201 South Prairie Avenue).  These Prairie Avenue addresses border the Roosevelt Road side of Grant Park.  One Museum Park is the tallest building on Chicago's South Side and among the tallest buildings in Chicago.[37] It surpassed 340 on the Park as the tallest all-residential building in Chicago,[37] and it is second to the Trump World Tower in the United States.
 
 1 History 2 Background 3 Influence 4 Preservation 5 Today 6 See also 7 References 8 External links  Chicago portal  U.S. Roads portal ^ a b c d Hayner, Don and Tom McNamee, Streetwise Chicago, "Prairie Avenue", p. 105, Loyola University Press, 1988, .mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}ISBN 0-8294-0597-6
 ^ Chicago School of Architecture Foundation; Prairie Avenue Historic District Committee (June 1975). Prairie Avenue Historic District.
 ^ Kinzie Gordon, Nelly (1912). The Fort Dearborn Massacre. Chicago: Rand McNally & Co. p. 56.
 ^ a b c Conzen, Michael P, Douglas Knox and Dennis McClendon (2005). "Neighborhood Change: Prairie Avenue, 1853-2003". The Electronic Encyclopedia of Chicago. Chicago Historical Society. Retrieved 2007-10-18.CS1 maint: multiple names: authors list (link)
 ^ a b c d e f g h i Sharoff, Robert (1998-09-20). "Saving the Grand Relics Of Chicago's Prairie Ave". The New York Times. p. 4, Section 11. Retrieved 2009-09-24.
 ^ a b c d Sharoff, Robert (2005-09-04). "Restoring the Legacy of a Historic Chicago Neighborhood". The New York Times. Retrieved 2009-09-24.
 ^ a b "Prairie Avenue District". City of Chicago Department of Planning and Development, Landmarks Division. 2003. Archived from the original on 2007-08-27. Retrieved 2007-10-18.
 ^ "National Register of Historic Places: Illinois - Cook County". National Register of Historic Places.com. Retrieved 2007-12-06.
 ^ "National Register Information System". National Register of Historic Places. National Park Service. January 23, 2007. 
 ^ a b "About The Mansion". marshallfieldjrmansion.com. Archived from the original on 2008-08-29. Retrieved 2008-04-07.
 ^ a b c McClendon, Dennis (2005). "Near South Side". The Electronic Encyclopedia of Chicago. Chicago Historical Society. Retrieved 2007-10-18.
 ^ a b c Carey, Heidi Pawlowski (2005). "Prairie Avenue". The Electronic Encyclopedia of Chicago. Chicago Historical Society. Retrieved 2007-10-18.
 ^ a b c Conzen, Michael P. & Douglas Knox (2005). "Chicago's Prairie Avenue Elite in 1886". The Electronic Encyclopedia of Chicago. Chicago Historical Society. Archived from the original on 2007-11-05. Retrieved 2007-10-18.
 ^ Pacyga, Dominic A. (2005). "South Side". The Electronic Encyclopedia of Chicago. Chicago Historical Society. Retrieved 2007-10-18.
 ^ "Prairie Ave Gallery: The Reach of Prairie Avenue Businesses". The Electronic Encyclopedia of Chicago. Chicago Historical Society. 2005. Archived from the original on 2007-12-18. Retrieved 2007-10-18.
 ^ "Prairie Ave Gallery: Representations". The Electronic Encyclopedia of Chicago. Chicago Historical Society. 2005. Archived from the original on 2007-12-18. Retrieved 2007-10-18.
 ^ "The Worlds of Prairie Avenue". The Electronic Encyclopedia of Chicago. Chicago Historical Society. 2005. Archived from the original on 2008-04-19. Retrieved 2007-10-19.
 ^ Wilson, Mark R. (2005). "Kimball (W. W.) Co". The Electronic Encyclopedia of Chicago. Chicago Historical Society. Archived from the original on 2008-08-29. Retrieved 2008-05-12.
 ^ "Prairie Ave Gallery: Prairie Avenue Politics". The Electronic Encyclopedia of Chicago. Chicago Historical Society. 2005. Archived from the original on 2007-12-03. Retrieved 2007-10-18.
 ^ "Prairie Ave Gallery: Philanthropies". The Electronic Encyclopedia of Chicago. Chicago Historical Society. 2005. Archived from the original on 2007-12-03. Retrieved 2007-10-18.
 ^ a b c Sciacchitano, Barbara (2005). "Historic Preservation". The Electronic Encyclopedia of Chicago. Chicago Historical Society. Retrieved 2007-10-18.
 ^ McCauley, Stephen (2002-03-03). "Celebration; Chicago". The New York Times. p. 53, Section 6. Retrieved 2009-09-23.
 ^ a b Commission on Chicago Landmarks and the Chicago Department of Planning and Development (1996). "Community Area #33: Near South Side". Chicago Historic Resources Survey: An inventory of Architecturally and historically significant structures. pp. 258–264.
 ^ "Chicago Commission Honors 21 Preservation Landmarks". AIArchitect. American Institute of Architects. 2007-09-21. Retrieved 2008-04-07.
 ^ Commission on Chicago Landmarks and the Chicago Department of Planning and Development (1996). "Chapter 2: Address Index". Chicago Historic Resources Survey: An inventory of Architecturally and historically significant structures. pp. II–32 and II–33.
 ^ Commission on Chicago Landmarks and the Chicago Department of Planning and Development (1996). "Community Area #69: Greater Grand Crossing". Chicago Historic Resources Survey: An inventory of Architecturally and historically significant structures. pp. 444–447.
 ^ Commission on Chicago Landmarks and the Chicago Department of Planning and Development (1996). "Community Area #35: Douglas". Chicago Historic Resources Survey: An inventory of Architecturally and historically significant structures. pp. 268–277.
 ^ Commission on Chicago Landmarks and the Chicago Department of Planning and Development (1996). "Community Area #38: Grand Boulevard". Chicago Historic Resources Survey: An inventory of Architecturally and historically significant structures. pp. 288–295.
 ^ a b Schmidt, William E. (1989-11-19). "On the Lam in Chicago". The New York Times. p. 8, Section 5. Retrieved 2009-09-23.
 ^ Schmidt, William E. (1989-03-22). "Is Honor Due House That Was Home to Capone?". The New York Times. p. A16. Retrieved 2009-09-23.
 ^ Rozhon, Tracie (2000-11-12). "Chicago Girds for Big Battle Over Its Skyline". The New York Times. p. 1. Retrieved 2009-09-24.
 ^ Bey, Lee (2000-10-06). "Platt's preservation is sweet, but there's a bitter aftertaste". Chicago Sun-Times. p. 16. Retrieved 2009-09-24.
 ^ Mendell, David (2000-10-06). "Platt Building's Western Facade Only To Be Spared Demolition". Chicago Tribune. p. 3. Retrieved 2009-09-24.
 ^ "Historic South Loop House Moving To Make Way For McCormick Redevelopment". CBS. October 1, 2014. Retrieved October 15, 2014.
 ^ Tyre, William H. (2008). Chicago's Historic Prairie Avenue. Google Book Search. Arcadia Publishing. ISBN 0-7385-5212-7.
 ^ "About Us". www.pdnachicago.com. Retrieved 2009-03-02.
 ^ a b "One Museum Park". Emporis.com. Retrieved 2008-05-08.
 Glessner House Museum Prairie District Neighborhood Alliance Website v t e Columbus Drive (300 E) Jeffery Boulevard (2000 E) Magnificent Mile, Michigan Avenue (100 E) Prairie Avenue (300 E) Stony Island Avenue (1600 E) Torrence Avenue (2628 E) 1st Avenue (Cumberland Avenue) (8400 W) Cicero Avenue (Skokie Boulevard) (4800 W) Clark Street (100 W) Halsted Street (800 W) Harlem Avenue (7200 W) Kedzie Avenue (3200 W) LaSalle Street (140 W) Loomis Street (Noble Street, Southport Avenue) (1400 W) Mannheim Road (La Grange Road) (10400 W) Pulaski Road (4000 W) Sheffield Avenue (1000 W) Wells Street (200 W) Western Avenue (2400 W) Addison Street (3600 N) Armitage Avenue (2000 N) Belmont Avenue (3200 N) Chicago Avenue (800 N) Dempster Street (8800 N) Devon Avenue (6400 N) Diversey Parkway (2800 N) Division Street (1200 N) Foster Avenue (5200 N) Fullerton Avenue (2400 N) Howard Street (7600 N) Hubbard Street (430 N) Irving Park Road (4000 N) North Avenue (1600 N) Oak Street (1000 N) Ohio Street (600 N) Randolph Street (150 N) Touhy Avenue (7200 N) 95th Street (9500 S) 159th Street (15900 S) Cermak Road (22nd Street) (2200 S) Ida B. Wells Drive (Congress Parkway) (500 S) DeKoven Street (1120 S) Garfield Boulevard (55th Street) (5500 S) Maxwell Street (1330 S) Pershing Road (39th Street) (3900 S) Roosevelt Road (1200 S) Archer Avenue Blue Island Avenue Broadway Elston Avenue Grand Avenue Lake Street Lake Shore Drive Lincoln Avenue Milwaukee Avenue Munger Road North Clark Street Ogden Avenue Ridge Avenue Rush Street Sheridan Road Stearns Road Wacker Drive Chicago park and boulevard system Chicago placename etymologies Multilevel streets in Chicago Six Corners U.S. Route 66 in Illinois v t e Architectural style categories Contributing property Historic district History of the National Register of Historic Places Keeper of the Register National Park Service Property types Alabama Alaska Arizona Arkansas California Colorado Connecticut Delaware Florida Georgia Hawaii Idaho Illinois Indiana Iowa Kansas Kentucky Louisiana Maine Maryland Massachusetts Michigan Minnesota Mississippi Missouri Montana Nebraska Nevada New Hampshire New Jersey New Mexico New York North Carolina North Dakota Ohio Oklahoma Oregon Pennsylvania Rhode Island South Carolina South Dakota Tennessee Texas Utah Vermont Virginia Washington West Virginia Wisconsin Wyoming American Samoa Guam Minor Outlying Islands Northern Mariana Islands Puerto Rico Virgin Islands Federated States of Micronesia Marshall Islands Palau District of Columbia Morocco National Historic Preservation Act of 1966
Historic Preservation Fund Historic Preservation Fund  Portal  Category Streets in Chicago Historic districts in Chicago CS1 maint: multiple names: authors list Commons category link is on Wikidata Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Create a book Download as PDF Printable version Español Français 中文  This page was last edited on 1 October 2019, at 04:50 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Prairie Avenue a b c d ^ ^ a b c a b c d e f g h i a b c d a b ^ ^ a b a b c a b c a b c ^ ^ ^ ^ ^ ^ ^ a b c ^ a b ^ ^ ^ ^ ^ a b ^ ^ ^ ^ ^ ^ ^ a b Prairie Avenue 300 East The National Historic Landmark John J. Glessner House at 1800 South Prairie Avenue Department of Streets & Sanitation Chicago 134th Street (13400 S)[1] Roosevelt Road (1200 S) George Pullman residence: 1729 S. Prairie Ave. (c. 1900)Interior of Pullman Residence (1922)Al Capone residence: 7244 S. Prairie Ave. (1929)
 Marshall Field residence: 1905 S. Prairie Ave. (c. 1905)Mercy Hospital: 2537 S. Prairie Ave. (1910) (where Theodore Roosevelt went after 1912-10-14 shooting)
  Wikimedia Commons has media related to Prairie Avenue. East of StateStreet (0 E/W)
Columbus Drive (300 E)
Jeffery Boulevard (2000 E)
Magnificent Mile, Michigan Avenue (100 E)
Prairie Avenue (300 E)
Stony Island Avenue (1600 E)
Torrence Avenue (2628 E)
West ofState Street
1st Avenue (Cumberland Avenue) (8400 W)
Cicero Avenue (Skokie Boulevard) (4800 W)
Clark Street (100 W)
Halsted Street (800 W)
Harlem Avenue (7200 W)
Kedzie Avenue (3200 W)
LaSalle Street (140 W)
Loomis Street (Noble Street, Southport Avenue) (1400 W)
Mannheim Road (La Grange Road) (10400 W)
Pulaski Road (4000 W)
Sheffield Avenue (1000 W)
Wells Street (200 W)
Western Avenue (2400 W)
 
Columbus Drive (300 E)
Jeffery Boulevard (2000 E)
Magnificent Mile, Michigan Avenue (100 E)
Prairie Avenue (300 E)
Stony Island Avenue (1600 E)
Torrence Avenue (2628 E)
 
1st Avenue (Cumberland Avenue) (8400 W)
Cicero Avenue (Skokie Boulevard) (4800 W)
Clark Street (100 W)
Halsted Street (800 W)
Harlem Avenue (7200 W)
Kedzie Avenue (3200 W)
LaSalle Street (140 W)
Loomis Street (Noble Street, Southport Avenue) (1400 W)
Mannheim Road (La Grange Road) (10400 W)
Pulaski Road (4000 W)
Sheffield Avenue (1000 W)
Wells Street (200 W)
Western Avenue (2400 W)
 North of MadisonStreet (0 N/S)
Addison Street (3600 N)
Armitage Avenue (2000 N)
Belmont Avenue (3200 N)
Chicago Avenue (800 N)
Dempster Street (8800 N)
Devon Avenue (6400 N)
Diversey Parkway (2800 N)
Division Street (1200 N)
Foster Avenue (5200 N)
Fullerton Avenue (2400 N)
Howard Street (7600 N)
Hubbard Street (430 N)
Irving Park Road (4000 N)
North Avenue (1600 N)
Oak Street (1000 N)
Ohio Street (600 N)
Randolph Street (150 N)
Touhy Avenue (7200 N)
South ofMadison Street
95th Street (9500 S)
159th Street (15900 S)
Cermak Road (22nd Street) (2200 S)
Ida B. Wells Drive (Congress Parkway) (500 S)
DeKoven Street (1120 S)
Garfield Boulevard (55th Street) (5500 S)
Maxwell Street (1330 S)
Pershing Road (39th Street) (3900 S)
Roosevelt Road (1200 S)
 
Addison Street (3600 N)
Armitage Avenue (2000 N)
Belmont Avenue (3200 N)
Chicago Avenue (800 N)
Dempster Street (8800 N)
Devon Avenue (6400 N)
Diversey Parkway (2800 N)
Division Street (1200 N)
Foster Avenue (5200 N)
Fullerton Avenue (2400 N)
Howard Street (7600 N)
Hubbard Street (430 N)
Irving Park Road (4000 N)
North Avenue (1600 N)
Oak Street (1000 N)
Ohio Street (600 N)
Randolph Street (150 N)
Touhy Avenue (7200 N)
 
95th Street (9500 S)
159th Street (15900 S)
Cermak Road (22nd Street) (2200 S)
Ida B. Wells Drive (Congress Parkway) (500 S)
DeKoven Street (1120 S)
Garfield Boulevard (55th Street) (5500 S)
Maxwell Street (1330 S)
Pershing Road (39th Street) (3900 S)
Roosevelt Road (1200 S)
 
Archer Avenue
Blue Island Avenue
Broadway
Elston Avenue
Grand Avenue
Lake Street
Lake Shore Drive
Lincoln Avenue
Milwaukee Avenue
Munger Road
North Clark Street
Ogden Avenue
Ridge Avenue
Rush Street
Sheridan Road
Stearns Road
Wacker Drive
 
Chicago park and boulevard system
Chicago placename etymologies
Multilevel streets in Chicago
Six Corners
U.S. Route 66 in Illinois
 
Architectural style categories
Contributing property
Historic district
History of the National Register of Historic Places
Keeper of the Register
National Park Service
Property types
 
Alabama
Alaska
Arizona
Arkansas
California
Colorado
Connecticut
Delaware
Florida
Georgia
Hawaii
Idaho
Illinois
Indiana
Iowa
Kansas
Kentucky
Louisiana
Maine
Maryland
Massachusetts
Michigan
Minnesota
Mississippi
Missouri
Montana
Nebraska
Nevada
New Hampshire
New Jersey
New Mexico
New York
North Carolina
North Dakota
Ohio
Oklahoma
Oregon
Pennsylvania
Rhode Island
South Carolina
South Dakota
Tennessee
Texas
Utah
Vermont
Virginia
Washington
West Virginia
Wisconsin
Wyoming
 
American Samoa
Guam
Minor Outlying Islands
Northern Mariana Islands
Puerto Rico
Virgin Islands
 
Federated States of Micronesia
Marshall Islands
Palau
 
District of Columbia
Morocco
 
National Historic Preservation Act of 1966
Historic Preservation Fund
 
 Portal
 Category
 