
 Euryoryzomys emmonsae, also known as Emmons' rice rat[1] or Emmons' oryzomys,[5] is a rodent from the Amazon rainforest of Brazil in the genus Euryoryzomys of the family Cricetidae. Initially misidentified as E. macconnelli or E. nitidus, it was formally described in 1998. A rainforest species, it may be scansorial, climbing but also spending time on the ground. It lives only in a limited area south of the Amazon River in the state of Pará, a distribution that is apparently unique among the muroid rodents of the region.
 Euryoryzomys emmonsae is a relatively large rice rat, weighing 46 to 78 g (1.6 to 2.8 oz), with a distinctly long tail and relatively long, tawny brown fur. The skull is slender and the incisive foramina (openings in the bone of the palate) are broad. The animal has 80 chromosomes and its karyotype is similar to that of other Euryoryzomys. Its conservation status is assessed as "Data Deficient", but deforestation may pose a threat to this species.
 In 1998, Guy Musser, Michael Carleton, Eric Brothers, and Alfred Gardner reviewed the taxonomy of species previously lumped under "Oryzomys capito" (now classified in the genera Hylaeamys, Euryoryzomys, and Transandinomys). They described the new species Oryzomys emmonsae on the basis of 17 specimens from three locations in the state of Pará in northern Brazil; these animals had been previously identified as Oryzomys macconnelli (now Euryoryzomys macconnelli) and then as Oryzomys nitidus (now Euryoryzomys nitidus).[3] The specific name honors Louise H. Emmons, who, among other contributions to Neotropical mammalogy, collected three of the known examples of the species in 1986, including the holotype.[3][6] The new species was placed in what they termed the "Oryzomys nitidus group", which also included O. macconelli, O. nitidus, and O. russatus.[7]
 In 2000, James Patton, Maria da Silva, and Jay Malcolm reported on mammals collected at the Rio Juruá in western Brazil. In this report, they provided further information on the Oryzomys species reviewed by Musser and colleagues, including sequence data from the mitochondrial cytochrome b gene. Their analysis reaffirmed that O. emmonsae was a distinct species and found that it was closest to O. macconnelli and O. russatus, differing from both by about 12% in the cytochrome b sequence; O. nitidus was more distantly related, differing by 14.7%. The average sequence difference between the three O. emmonsae individuals studied was 0.8%.[8]
 In 2006, an extensive morphological and molecular phylogenetic analysis by Marcelo Weksler showed that species then placed in the genus Oryzomys did not form a single, cohesive (monophyletic) group; for example, O. macconnelli, O. lamia (placed under O. russatus by Musser and colleagues) and O. russatus clustered together in a single natural group (clade), but were not closely related to the type species of Oryzomys, the marsh rice rat (O. palustris).[9] Later in 2006, Weksler and colleagues described several new genera to accommodate species previously placed in Oryzomys, among which was Euryoryzomys for the "O. nitidus complex", including O. emmonsae.[4]
 Thus, the species is now known as Euryoryzomys emmonsae.[1] As a species of Euryoryzomys, it is classified within the tribe Oryzomyini ("rice rats"), which includes over a hundred species, mainly from South and Central America.[10][5] Oryzomyini in turn is part of the subfamily Sigmodontinae of family Cricetidae, along with hundreds of other species of mainly small rodents.[5]
 Euryoryzomys emmonsae is a fairly large,[11] long-tailed rice rat with long, soft fur. The hairs on the back are 8 to 10 mm (0.31 to 0.39 in) long.[12] It generally resembles E. nitidus in these and other characters, but has a longer tail.[3] E. macconnelli is slightly larger and has longer and duller fur. In E. emmonsae, the upperparts are tawny brown, but a bit darker on the head because many hairs have black tips. The hairs of the underparts are gray at the bases and white at the tips; overall, the fur appears mostly white. In most specimens, there is a patch on the chest where the gray bases are absent. The longest of the vibrissae (whiskers) of the face extend slightly beyond the ears. The eyelids are black. The ears are covered with small, yellowish brown hairs and appear dark brown overall. The feet are covered with white hairs above and brown below. There are six pads on the plantar surface, but the hypothenar is reduced. The ungual tufts, tufts of hair which surround the bases of the claws, are well-developed. The tail is like the body in color above, and mostly white below, but in the 10 mm (0.39 in) nearest the tail tip it is brown below.[12]
 Compared to E. nitidus[3] and E. macconnelli, the skull is relatively small and slender. It has broad and short incisive foramina (perforations of the palate between the incisors and the molars) and lacks sphenopalatine vacuities which perforate the mesopterygoid fossa, the gap behind the end of the palate. The animal is similar to other members of the genus in the pattern of the arteries of the head.[12] The alisphenoid strut, an extension of the alisphenoid bone which separates two foramina (openings) in the skull (the masticatory-buccinator foramen and the foramen ovale accessorium) is rarely present; its presence is more frequent in E. nitidus.[13] The capsular process, a raising of the bone of the mandible (lower jaw) behind the third molar, houses the back end of the lower incisor in most Euryoryzomys, but is absent in E. emmonsae and E. macconnelli.[14] Traits of the teeth are similar to those of E. nitidus and other Euryoryzomys.[12][14]
 The karyotype includes 80 chromosomes with a total of 86 major arms (2n = 80; FN = 86). The X chromosome is subtelocentric (with one pair of long arms and one pair of short arms) and the Y chromosome is acrocentric (with only one pair of arms, or with a minute second pair). Among the autosomes (non-sex chromosomes), the four metacentric or submetacentric (with two pairs of arms as long as or not much shorter than the other) pairs of chromosomes are small, and the 35 pairs of acrocentrics range from large to small. Some of those have a minute second pair of arms and could also be classified as subtelocentric, which would raise FN to 90. This karyotype is similar to other known karyotypes of members of Euryoryzomys.[12]
 In thirteen specimens measured by Musser, head and body length ranges from 120 to 142 mm (4.7 to 5.6 in), tail length (12 specimens only) from 130 to 160 mm (5.1 to 6.3 in), hindfoot length from 32 to 35 mm (1.3 to 1.4 in), ear length (three specimens only) from 23 to 24 mm (0.91 to 0.94 in), and body mass from 46 to 78 g (1.6 to 2.8 oz).[15]
 The known distribution of Euryoryzomys emmonsae is limited to a portion of the Amazon rainforest south of the Amazon River in the state of Pará, between the Xingu and Tocantins rivers, but the limits of its range remain inadequately known.[12][5] No other South American rainforest muroid rodent is known to have a similar distribution.[16][6] Musser and colleagues reported it from three locations and Patton and others added a fourth; in some of those it occurs together with E. macconnelli or Hylaeamys megacephalus.[17][16][18]
 Specimens of E. emmonsae for which detailed habitat data are available were caught in "viny forest", a microhabitat that often included much bamboo. All were captured on the ground, some in bamboo thickets and another under a log.[16] Musser and colleagues speculated that E. emmonsae may be scansorial, spending time both on the ground and climbing in vegetation, like the similarly long-tailed rice rat Cerradomys subflavus.[6]
 The IUCN currently lists Euryoryzomys emmonsae as "Data Deficient" because it is so poorly known. It may be threatened by deforestation and logging, but occurs in at least one protected area, the Tapirapé-Aquiri National Forest.[1]
 
 Oryzomys emmonsae Musser et al., 1998[3] [Euryoryzomys] emmonsae: Weksler, Percequillo, and Voss, 2006[4] 1 Taxonomy 2 Description 3 Distribution and ecology 4 Conservation status 5 References 6 Literature cited ^ a b c d Percequillo, Weksler & Patton 2019.
 ^ Musser et al. 1998, p. 177.
 ^ a b c d e Musser et al. 1998, p. 233.
 ^ a b Weksler et al., 2006, p. 11
 ^ a b c d Musser & Carleton 2005, p. 1148.
 ^ a b c Musser et al. 1998, p. 239.
 ^ Musser et al. 1998, p. 175.
 ^ Patton et al., 2000, fig. 99; table 37
 ^ Weksler, 2006, figs. 34–39
 ^ Weksler et al., 2006, table 1
 ^ Musser et al. 1998, p. 233-234.
 ^ a b c d e f Musser et al. 1998, p. 234.
 ^ Musser et al. 1998, p. 236.
 ^ a b Weksler et al., 2006, p. 12
 ^ Musser et al., 1998, table 37
 ^ a b c Musser et al. 1998, p. 237.
 ^ Musser et al. 1998, p. 232.
 ^ Patton et al., 2000, p. 146
 Musser, Guy G.; Carleton, Michael D.; Brothers, Eric M.; Gardner, Alfred L. (1998-03-16). "Systematic studies of oryzomyine rodents (Muridae, Sigmodontinae) : diagnoses and distributions of species formerly assigned to Oryzomys "capito"". Bulletin of the American Museum of Natural History. 236. hdl:2246/1630..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} Musser, G.G.; Carleton, M.D. (2005). "Oryzomys emmonsae".  In Wilson, D.E.; Reeder, D.M (eds.). Mammal Species of the World: A Taxonomic and Geographic Reference (3rd ed.). Johns Hopkins University Press. p. 1148. ISBN 978-0-8018-8221-0. OCLC 62265494. Patton, J.L.; da Silva, M.N.F.; Malcolm, J.R. (2000). "Mammals of the Rio Juruá and the evolutionary and ecological diversification of Amazonia". Bulletin of the American Museum of Natural History. 244: 1–306. doi:10.1206/0003-0090(2000)244<0001:MOTRJA>2.0.CO;2. hdl:2246/1593. Percequillo, A.; Weksler, M.; Patton, J. (2019). "Euryoryzomys emmonsae". IUCN Red List of Threatened Species. IUCN. 2019: e.T29402A22328999. doi:10.2305/IUCN.UK.2019-1.RLTS.T29402A22328999.en. Retrieved 19 September 2019. Weksler, M. (2006). "Phylogenetic relationships of oryzomyine rodents (Muroidea: Sigmodontinae): separate and combined analyses of morphological and molecular data". Bulletin of the American Museum of Natural History. 296: 1–149. doi:10.1206/0003-0090(2006)296[0001:PROORM]2.0.CO;2. hdl:2246/5777. Weksler, M.; Percequillo, A. R.; Voss, R. S. (2006-10-19). "Ten new genera of oryzomyine rodents (Cricetidae: Sigmodontinae)" (PDF). American Museum Novitates. American Museum of Natural History. 3537: 1–29. doi:10.1206/0003-0082(2006)3537[1:TNGOOR]2.0.CO;2. hdl:2246/5815. v t e Kingdom: Animalia Phylum: Chordata Class: Mammalia Order: Rodentia Family: Cricetidae Subfamily: Sigmodontinae A. galapagoensis A. xanthaeolus †A. donovani †A. praeuniversitatis A. savamis †C. cailoi C. goytaca C. langguthi C. maracajuensis C. marinhus C. scotti C. subflavus C. vivoi D. albimaculatus E. polius E. emmonsae E. lamia E. legatus E. macconnelli E. nitidus E. russatus H. alfaroi H. chapmani H. fuscatus H. intectus H. melanotis H. rhabdops H. rostratus H. saturatior H. brasiliensis H. chacarius H. sciureus H. acritus H. laticeps H. megacephalus H. oniscus H. perenensis H. tatei H. yunganus L. molitor †M. audreyae †M. curazensis †M. desmarestii †M. georginae †M. luciae M. caliginosus M. robustulus M. zunigae M. transitorius M. altissimus M. minutus M. hammondi N. dubosti N. guianae N. minutus N. musseri N. paracou N. pictus N. spinosus N. tenuipes N. apicalis N. magdalenae N. palmipes N. rattus N. squamipes N. albigularis N. auriventer N. caracolus N. childi N. devius N. keaysi N. levipes N. maculiventer N. meridensis N. moerex N. nimbosus N. pectoralis N. pirrensis †N. darwini N. fernandinae N. indefessus N. swarthi †N. vespuccii O. auyantepui O. bicolor O. catherinae O. cleberi O. concolor O. flavicans O. mamorae O. paricola O. phaeotis O. rex O. roberti O. rutilus O. speciosus O. superans O. sydandersoni O. trinitatis O. andinus O. arenalis O. brendae O. chacoensis O. destructor O. flavescens O. fornesi O. fulvescens O. griseolus O. longicaudatus O. magellanicus O. microtis O. moojeni O. nigripes O. rupestris O. stramineus O. vegetus †O. victus O. balneator O. albiventer †O. antillarum O. couesi O. dimidiatus O. gorgasi †O. nelsoni Marsh rice rat (O.  palustris) (parasites) O. peninsulae †P. nivalis P. simplex †R. primigenus S. melanops S. ucayalensis S. alfari S. aphrastus S. angouya T. bolivaris T. talamancae Z. brevicauda Z. brunneus †"Ekbletomys hypenemus" Wikidata: Q1763310 Wikispecies: Euryoryzomys emmonsae EoL: 4468340 GBIF: 5786862 iNaturalist: 74331 ITIS: 972468 IUCN: 29402 NCBI: 123940 Wikidata: Q27334322 GBIF: 4265041 ITIS: 970705 MSW: 13000797 IUCN Red List data deficient species Euryoryzomys Rodents of South America Mammals of Brazil Endemic fauna of Brazil Mammals described in 1998 Taxa named by Guy Musser Articles with short description Articles with 'species' microformats Taxonbars with automatically added original combinations Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Wikimedia Commons Wikispecies Create a book Download as PDF Printable version العربية Català Español Euskara 한국어 मराठी Nederlands Polski Português Svenska Tagalog Tiếng Việt  This page was last edited on 29 October 2019, at 17:09 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  E. emmonsae Euryoryzomys emmonsae Euryoryzomys emmonsae Emmons' rice rat Emmons' oryzomys a b c d ^ a b c d e a b a b c d a b c ^ ^ ^ ^ ^ a b c d e f ^ a b ^ a b c ^ ^ 236 244 2019 296 3537 Data Deficient (IUCN 3.1)[1]
 Kingdom:
 Animalia
 Phylum:
 Chordata
 Class:
 Mammalia
 Order:
 Rodentia
 Family:
 Cricetidae
 Subfamily:
 Sigmodontinae
 Genus:
 Euryoryzomys
 Species:
 E. emmonsae
 Euryoryzomys emmonsae(Musser, Carleton, Brothers, and Gardner, 1998)
 
 Range (in green).[2]
 
Oryzomys emmonsae Musser et al., 1998[3]
[Euryoryzomys] emmonsae: Weksler, Percequillo, and Voss, 2006[4]
 
Kingdom: Animalia
Phylum: Chordata
Class: Mammalia
Order: Rodentia
Family: Cricetidae
Subfamily: Sigmodontinae
 
A. galapagoensis
A. xanthaeolus
 
†A. donovani
†A. praeuniversitatis
 
A. savamis
 
†C. cailoi
 
C. goytaca
C. langguthi
C. maracajuensis
C. marinhus
C. scotti
C. subflavus
C. vivoi
 
D. albimaculatus
 
E. polius
 
E. emmonsae
E. lamia
E. legatus
E. macconnelli
E. nitidus
E. russatus
 
H. alfaroi
H. chapmani
H. fuscatus
H. intectus
H. melanotis
H. rhabdops
H. rostratus
H. saturatior
 
H. brasiliensis
H. chacarius
H. sciureus
 
H. acritus
H. laticeps
H. megacephalus
H. oniscus
H. perenensis
H. tatei
H. yunganus
 
L. molitor
 
†M. audreyae
†M. curazensis
†M. desmarestii
†M. georginae
†M. luciae
 
M. caliginosus
M. robustulus
M. zunigae
 
M. transitorius
 
M. altissimus
M. minutus
 
M. hammondi
 
N. dubosti
N. guianae
N. minutus
N. musseri
N. paracou
N. pictus
N. spinosus
N. tenuipes
 
N. apicalis
N. magdalenae
N. palmipes
N. rattus
N. squamipes
 
N. albigularis
N. auriventer
N. caracolus
N. childi
N. devius
N. keaysi
N. levipes
N. maculiventer
N. meridensis
N. moerex
N. nimbosus
N. pectoralis
N. pirrensis
 
†N. darwini
N. fernandinae
N. indefessus
N. swarthi
 
†N. vespuccii
 
O. auyantepui
O. bicolor
O. catherinae
O. cleberi
O. concolor
O. flavicans
O. mamorae
O. paricola
O. phaeotis
O. rex
O. roberti
O. rutilus
O. speciosus
O. superans
O. sydandersoni
O. trinitatis
 
O. andinus
O. arenalis
O. brendae
O. chacoensis
O. destructor
O. flavescens
O. fornesi
O. fulvescens
O. griseolus
O. longicaudatus
O. magellanicus
O. microtis
O. moojeni
O. nigripes
O. rupestris
O. stramineus
O. vegetus
†O. victus
 
O. balneator
 
O. albiventer
†O. antillarum
O. couesi
O. dimidiatus
O. gorgasi
†O. nelsoni
Marsh rice rat (O.  palustris) (parasites)
O. peninsulae
 
†P. nivalis
 
P. simplex
 
†R. primigenus
 
S. melanops
S. ucayalensis
 
S. alfari
S. aphrastus
 
S. angouya
 
T. bolivaris
T. talamancae
 
Z. brevicauda
Z. brunneus
 
†"Ekbletomys hypenemus"
 
Wikidata: Q1763310
Wikispecies: Euryoryzomys emmonsae
EoL: 4468340
GBIF: 5786862
iNaturalist: 74331
ITIS: 972468
IUCN: 29402
NCBI: 123940
 
Wikidata: Q27334322
GBIF: 4265041
ITIS: 970705
MSW: 13000797
 