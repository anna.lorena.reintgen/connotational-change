Coordinates: 59°5′40″N 10°47′30″E﻿ / ﻿59.09444°N 10.79167°E﻿ / 59.09444; 10.79167
 Albatros was the fourth of six Type 23 torpedo boats built for the German Navy (initially called the Reichsmarine and renamed the Kriegsmarine in 1935). Completed in 1927, Albatros often served as a flagship of torpedo boat units. The ship made multiple non-intervention patrols during the Spanish Civil War in the late 1930s. After an attack by aircraft of the Spanish Republican Air Force killed German sailors in 1937, she participated in the retaliatory bombardment of Almería.
 At the beginning of World War II in 1939, Albatros helped to lay minefields and made anti-shipping patrols before participating in Operation Weserübung, the German invasion of Norway in April 1940. The ship fired the first shots of the campaign when she encountered and crippled a Norwegian patrol boat. She was lightly damaged during the Battle of Horten Harbor. Albatros then ran aground and was wrecked while maneuvering in an attempt to avoid Norwegian coastal artillery.
 Derived from the World War I-era torpedo boat SMS H145,[Note 1] the Type 23 torpedo boat was slightly larger, but had a similar armament and speed.[1] The Type 23 had an overall length of 87.7 meters (287 ft 9 in) and was 85.7 meters (281 ft 2 in) long at the waterline.[2] The ships had a beam of 8.25 meters (27 ft 1 in), and a mean draft of 3.65 meters (12 ft). They displaced 923 long tons (938 t) at standard load and 1,290 long tons (1,310 t) at deep load.[3] The pair of Schichau geared steam turbine sets, each driving one propeller, were designed to produce 23,000 shaft horsepower (17,000 kW) using steam from three water-tube boilers, which propelled the ship at 33 knots (61 km/h; 38 mph).[4] Albatros carried a maximum of 321 metric tons (316 long tons) of fuel oil which was intended to give a range of 3,600 nautical miles (6,700 km; 4,100 mi) at 17 knots (31 km/h; 20 mph).[1] The effective range proved to be only 1,800 nmi (3,300 km; 2,100 mi) at that speed. Their crews consisted of 4 officers and 116 sailors.[3]
 As built, the Type 23s mounted three 10.5-centimeter (4.1 in) SK L/45[Note 2] guns, one forward and two aft of the superstructure; the aft superfiring gun was on an open mount while the others were protected by gun shields.[6] They carried six rotating 500-millimeter (19.7 in) torpedo tubes in two triple mounts amidships and could also carry up to 30 mines.[2] After 1931, the torpedo tubes were replaced by 533-millimeter (21 in) tubes and a pair of 2-centimeter (0.8 in) C/30 anti-aircraft guns were added.[Note 3][1][3] At least some of the ships were fitted with depth charges, but details are lacking.[8]
 Albatros was laid down at the Reichsmarinewerft Wilhelmshaven (Wilhelmshaven Navy Yard) on 5 October 1925[4] as yard number 105, launched on 15 July 1926 and commissioned on 15 May 1927.[9] After working up, Albatros became the flagship of the 4th Torpedo Boat Half-Flotilla, which also consisted of her sister ships Falke, Greif and Möwe. The half-flotilla was under the command of Korvettenkapitän Karl Dönitz, who later became grand admiral of the Kriegsmarine of Nazi Germany.[10]
 In the spring of 1929, Albatros was departing Wilhelmshaven to take part in a fleet cruise in Spanish waters, and collided with Möwe at the exit from the harbor. Both ships followed the fleet four days later after repairs. In 1931, the 4th Torpedo Boat Half-Flotilla and the light cruiser Königsberg were present during the celebrations of the 10th anniversary of the Latvian Navy in Libau. The following year, Albatros, again with her sisters and Königsberg, represented Germany at the celebration of the betrothal of the Swedish Crown Prince Gustav Adolf to the German princess Sibylla of Saxe-Coburg and Gotha. On 7 December 1932, Albatros was decommissioned and replaced by Greif as flagship of the 4th Half-Flotilla.[10]
 On 5 October 1933, Albatros, commanded by Kapitänleutnant (Lieutenant) Werner Hartmann, was put back into service, replacing the pre-war boat T151 in the 2nd Torpedo Boat Half-Flotilla based at Swinemünde (now Świnoujście, Poland). Albatros became the flagship of the 2nd Half-Flotilla, which consisted of Möwe and the two Type 24 torpedo boats Jaguar and Leopard, on 1 October 1934.[10]
 From July 1936 to October 1937, Albatros carried out three non-intervention patrols in Spanish waters which were intended to prevent men and material from reaching the participants the Spanish Civil War. On the first mission from 28 July to 27 August 1936, the four ships of the 2nd Half-Flotilla escorted the light cruiser Köln and the heavy cruisers Deutschland and Admiral Scheer to the north Spanish coast where they evacuated Germans and other refugees to France. The warships not only transported refugees, but also escorted the many merchant ships that were chartered by Germany for the repatriation of their citizens. The half-flotilla returned to Spain with Albatros from 28 September to 29 November.[10] Her sister Seeadler ran aground while leaving Cadiz harbor that same month and had to return to Germany on one turbine, escorted by Albatros.[11]
 The 2nd Half-Flotilla returned to Spain for the third time from May to June 1937. On 24 May, Republican aircraft attacked the town and harbor of Palma de Mallorca causing Deutschland to depart for Ibiza, although Albatros's captain chose to remain in port. During subsequent attacks later that day, several bombs fell near the ship and she steamed to join the cruiser in Ibiza. Five days later, another attack was carried out on Deutschland which killed several dozen crewmen. As retaliation, Adolf Hitler ordered Admiral Scheer to bombard the Republican-held city of Almería. The four boats of the 2nd Half-Flotilla[11] escorted the ship as she did so on 31 May, targeting Republican coastal artillery, naval buildings and ships in the harbor, which killed 19 people.[12] On 24 June Albatros was replaced by Möwe, and returned to Germany escorting the light cruisers Köln and Leipzig.[10]
 In fall 1937, the 2nd Half-Flotilla was disbanded, and Albatros served as a training ship until she was decommissioned on 16 February 1938. The boat was placed back into service on 1 July 1938 and was assigned to the 6th Torpedo Boat Flotilla. She was transferred four months later to the 5th Torpedo Boat Flotilla, which included her sisters Greif, Möwe, Kondor, and Falke.[10]
 At the start of World War II, Albatros was used in the defensive mining operations in the North Sea that began on 3 September 1939 that were intended to prevent the Royal Navy from entering the German Bight. From 3 to 5 October Albatros, together with three destroyers and her sisters Greif and Falke, was tasked with anti-shipping patrols in the Kattegat and Skaggerak that captured four ships.[13]
 During Operation Weserübung, Albatros was assigned to Group 5 under Konteradmiral Oskar Kummetz on the heavy cruiser Blücher, tasked to capture Oslo. Albatros transported about 100 men of the invasion force and was one of the cruiser's escorts through the Baltic and Kattegat. While passing Skagen, Denmark, on 8 April the British submarine HMS Triton unsuccessfully attacked the cruisers of the group with torpedoes. Albatros spotted their tracks and unsuccessfully depth charged the submarine. Later that night the group encountered the Norwegian patrol boat HNoMS Pol III in heavy fog at 23:00. After firing a warning shot and realizing that Albatros would not turn away, and was going to violate Norwegian neutrality, Pol III fired flares to alert Norwegian coastal batteries and rammed Albatros in the side. From Albatros it was clear that the guns on Pol III were manned, and that the Norwegians intended to fight. Despite clear orders from Kummetz to fire only if fired upon, the torpedo boat's captain, Kapitänleutnant Siegfried Strelow, opened fire, hitting Pol III with at least two 10.5 cm shells and raking her with machine guns, thus firing the opening shots of the campaign. The Norwegian ship's crew attempted to abandon ship in the only intact boat remaining, but it capsized and they were taken aboard Albatros. Albatros's crew set the patrol boat on fire and abandoned it, proceeding up the foggy Oslofjord independently. The torpedo boat was finally able to get a bearing from her sister Kondor and followed her towards the naval base at Karljohansvern, in the town of Horten. En route, she was spotted by the lightly armed Norwegian minesweeper Otra which sheered off after radioing a report at 04:03 on 9 April.[14]
 The German force tasked to occupy Karljohansvern was scheduled to do so at dawn on 9 April, but Kondor's captain, the commander of the force, decided to assault the harbor directly since the Norwegians had already been alerted. About 140 soldiers were transferred to the small motor minesweepers R17 and R21 and the former was in the lead as they steamed through the harbor entrance at 04:35 at high speed, slowly followed by Albatros, while Kondor was transferring her embarked troops to another ship. The minelayer HNoMS Olav Tryggvason engaged R17 ten minutes later and set her on fire, but not before she unloaded her troops. The minelayer was only able to get a few shots off at R21 before she steamed behind an island in the harbor. About this time, Albatros was approaching the harbor mouth and exchanged fire with Olav Tryggvason without effect. Strelow, with only a single gun able to bear on the minelayer, withdrew behind one of the outer islands and started blindly bombarding the harbor. The minelayer was occasionally struck by shrapnel, but she hit the torpedo boat with one shell at 06:30, killing two and wounding another pair of sailors. Albatros withdrew not long afterward [Note 4] and the German troops that had made it ashore bluffed the Norwegians into surrendering at 07:35.[16]
 Later that morning, Kondor and Albatros were ordered to land their troops at Son and then, reinforced by R21, she was ordered to secure the submarine base at Teie. On the morning of 10 April, Kondor and Albatros were engaged by coastal batteries on the island of Bolærne and forced to turn away. Later that day, Albatros was escorting the merchant ship SS Curityba while also landing men on the island of Rauøy; Strelow decided to steam east of the island to avoid any further attention from the guns on Bolærne. Unbeknownst to him, sea ice had removed the marker for the Gyren shoal a few weeks earlier and Albatros struck it at a speed of 20 knots (37 km/h; 23 mph). The impact ripped open at least one fuel tank and holed the turbine and boiler rooms. The ship settled with a pronounced list to port and her stern in the air. She quickly lost all electrical power and several fires were ignited that caused several small explosions; Albatros was declared a total loss. The crew was rescued by the auxiliary V707 Arthur Dunker and was later assigned to Olav Tryggvason after the Norwegian surrender. The minelayer was initially renamed Albatros II and then became Brummer.[17]
 
 923 long tons (938 t) (standard) 1,290 long tons (1,310 t) (deep load) 3 × water-tube boilers 23,000 shp (17,000 kW) 2 × shafts 2 × geared steam turbine sets 3 × single 10.5 cm (4.1 in) guns 2 × triple 500 mm (19.7 in) torpedo tubes 30 mines 1 Design and armament 2 Construction and career

2.1 Spanish Civil War
2.2 Interwar
2.3 Second World War

 2.1 Spanish Civil War 2.2 Interwar 2.3 Second World War 3 Notes 4 Citations 5 Bibliography ^ "SMS" stands for "Seiner Majestät Schiff" (German: His Majesty's Ship).
 ^ In Imperial German Navy gun nomenclature, "SK" (Schnelladekanone) denotes that the gun is quick firing, while the L/45 denotes the length of the gun. In this case, the L/45 gun is 45 caliber, meaning that the gun is 45 times as long as it is in diameter.[5]
 ^ In Kriegsmarine gun nomenclature, SK stands for Schiffskanone (ship's gun), C/30 stands for Constructionjahr (construction year) 1930.[7]
 ^ Naval historian Vincent O'Hara says that the boat's forward gun malfunctioned after firing eight rounds, but this is not mentioned by naval historian Geir Haarr.[15]
 ^ a b c Gröner, p. 191
 ^ a b Gardiner & Chesneau, p. 237
 ^ a b c Whitley 1991, p. 202
 ^ a b Whitley 2000, p. 57
 ^ Friedman, pp. 130–131
 ^ Whitley 1991, p. 45
 ^ Campbell, p. 219
 ^ Haarr 2009, p. 377
 ^ Gröner, p. 192
 ^ a b c d e f Hildebrand, Röhr & Steinmetz, p. 86
 ^ a b Whitley 1991, p. 79
 ^ Haarr 2013, pp. 32–33
 ^ Rohwer, pp. 2, 6; Whitley 1991, p. 84
 ^ Haarr 2009, pp. 83–84, 119–123, 129
 ^ O'Hara, p. 28
 ^ Haarr 2009, pp. 147–151
 ^ Haarr 2009, pp. 153, 155, 163, 380, fn. 11, p. 458
 Campbell, John (1985). Naval Weapons of World War II. Annapolis, Maryland: Naval Institute Press. ISBN 978-0-87021-459-2..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em} Friedman, Norman (2011). Naval Weapons of World War One. Barnsley, South Yorkshire, UK: Seaforth. ISBN 978-1-84832-100-7. Gardiner, Robert & Chesneau, Roger, eds. (1980). Conway's All the World's Fighting Ships 1922–1946. London: Conway Maritime Press. ISBN 0-85177-146-7. Gröner, Erich (1990). German Warships: 1815–1945. Volume 1: Major Surface Warships. Annapolis, Maryland: Naval Institute Press. ISBN 978-0-87021-790-6. Haarr, Geirr H. (2009). The German Invasion of Norway, April 1940. Annapolis, Maryland: Naval Institute Press. ISBN 978-1-59114-310-9. Haarr, Geirr H. (2013). The Gathering Storm: The Naval War in Northern Europe September 1939 – April 1940. Annapolis: Naval Institute Press. ISBN 978-1-59114-331-4. Hildebrand, Hans H.; Röhr, Albert & Steinmetz, Hans-Otto (1993). Die Deutschen Kriegsschiffe (Volume 1) [Warships of Germany] (in German). Ratingen: Mundus Verlag. ISBN 978-3-78220-237-4. O'Hara, Vincent (2004). "2 The Invasion of Norway, April–June 1940". The German Fleet at War, 1939–1945. Annapolis, Maryland: Naval Institute Press. ISBN 978-1-61251-397-3. Rohwer, Jürgen (2005). Chronology of the War at Sea 1939–1945: The Naval History of World War Two (Third Revised ed.). Annapolis, Maryland: Naval Institute Press. ISBN 978-1-59114-119-8. Whitley, M. J. (1991). German Destroyers of World War Two. Annapolis, Maryland: Naval Institute Press. ISBN 978-1-55750-302-2. Whitley, M. J. (2000). Destroyers of World War Two: An International Encyclopedia. London: Cassell & Co. ISBN 978-1-85409-521-3. v t e Möwe Seeadler Greif Albatros Kondor Falke Followed by: Type 24 German torpedo boats of World War II v t e 6 Apr: U-50 8 Apr: HMS Glowworm, Rio de Janeiro 9 Apr: HNoMS Æger, Blücher, HNoMS Eidsvold, HMS Gurkha, Karlsruhe, HNoMS Norge, Seattle, Sørland, HNoMS Tor 10 Apr: Albatros, Anton Schmitt, Friedenau, HMS Hardy, HMS Hunter, Königsberg, HMS Tarpon, HMS Thistle, Wilhelm Heidkamp 13 Apr: Bernd von Arnim, Diether von Roeder, Erich Giese, Erich Koellner, HNoMS Frøya, Georg Thiele, Hans Lüdemann, Hermann Künne, HNoMS Storm, U-64, Wolfgang Zenker 15 Apr: Brummer, U-49 18 Apr: HNoMS Sæl, HMS Sterlet 20 Apr: HNoMS Stegg 25 Apr: HMS Bradman, HNoMS Trygg 26 Apr: HNoMS Garm 29 Apr: HMS Unity 30 Apr: HMS Bittern, HMS Dunoon, Maillé Brézé Unknown date: U-1 8 Apr: HNoMS Pol III 10 Apr: Alster, HMS Hotspur 11 Apr: Alster, HNoMS Brand 12 Apr: HNoMS Storm 13 Apr: Malangen 16 Apr: HNoMS Brand Type 23 torpedo boats 1926 ships Ships built in Wilhelmshaven Military units and formations of Nazi Germany in the Spanish Civil War Maritime incidents in April 1940 Articles containing German-language text Coordinates on Wikidata CS1: long volume value CS1 German-language sources (de) Featured articles Not logged in Talk Contributions Create account Log in Article Talk Read Edit View history Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store Help About Wikipedia Community portal Recent changes Contact page What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page Create a book Download as PDF Printable version Deutsch Français 日本語 Norsk Suomi  This page was last edited on 19 September 2019, at 00:32 (UTC). Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization. Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Statistics Cookie statement Mobile view 
  
  Albatros ^ ^ ^ ^ a b c a b a b c a b ^ ^ ^ ^ ^ a b c d e f a b ^ ^ ^ ^ ^ ^ Right elevation and plan of the Type 23
 Name: 
Albatros Namesake: 
Albatross Builder: 
Reichsmarinewerft Wilhelmshaven Yard number: 
105 Laid down: 
5 October 1925 Launched: 
15 July 1926 Completed: 
15 May 1927 Fate: 
Beached, 10 April 1940 Class and type: 
Type 23 torpedo boat Displacement: 

923 long tons (938 t) (standard)
1,290 long tons (1,310 t) (deep load)
 Length: 
87.7 m (287 ft 9 in) o/a Beam: 
8.25 m (27 ft 1 in) Draft: 
3.65 m (12 ft) Installed power: 

3 × water-tube boilers
23,000 shp (17,000 kW)
 Propulsion: 

2 × shafts
2 × geared steam turbine sets
 Speed: 
33 knots (61 km/h; 38 mph) Range: 
1,800 nmi (3,300 km; 2,100 mi) at 17 knots (31 km/h; 20 mph) Complement: 
120 Armament: 

3 × single 10.5 cm (4.1 in) guns
2 × triple 500 mm (19.7 in) torpedo tubes
30 mines
 
Möwe
Seeadler
Greif
Albatros
Kondor
Falke
 
Followed by: Type 24
 
German torpedo boats of World War II
 
6 Apr: U-50
8 Apr: HMS Glowworm, Rio de Janeiro
9 Apr: HNoMS Æger, Blücher, HNoMS Eidsvold, HMS Gurkha, Karlsruhe, HNoMS Norge, Seattle, Sørland, HNoMS Tor
10 Apr: Albatros, Anton Schmitt, Friedenau, HMS Hardy, HMS Hunter, Königsberg, HMS Tarpon, HMS Thistle, Wilhelm Heidkamp
13 Apr: Bernd von Arnim, Diether von Roeder, Erich Giese, Erich Koellner, HNoMS Frøya, Georg Thiele, Hans Lüdemann, Hermann Künne, HNoMS Storm, U-64, Wolfgang Zenker
15 Apr: Brummer, U-49
18 Apr: HNoMS Sæl, HMS Sterlet
20 Apr: HNoMS Stegg
25 Apr: HMS Bradman, HNoMS Trygg
26 Apr: HNoMS Garm
29 Apr: HMS Unity
30 Apr: HMS Bittern, HMS Dunoon, Maillé Brézé
Unknown date: U-1
 
8 Apr: HNoMS Pol III
10 Apr: Alster, HMS Hotspur
11 Apr: Alster, HNoMS Brand
12 Apr: HNoMS Storm
13 Apr: Malangen
16 Apr: HNoMS Brand
 1939  1940  1941 March 1940   May 1940 