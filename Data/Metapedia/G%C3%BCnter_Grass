Nobel Prize in Literature  1999 
 Günter Wilhelm Grass (born 16 October 1927 – 13 April 2015) is a Nobel Prize-winning German author, poet, playwright, sculptor and artist.
 Grass was born in Danzig. In 1945, he became a refugee in West Germany, but in his fiction he frequently returns to the Danzig of his childhood.
 He is best known for his first novel, The Tin Drum, a key text in European magic realism and the first part of his Danzig Trilogy. His works frequently have a left wing political dimension and Grass has been an active supporter of the Social Democratic Party of Germany. In 2006, Grass acknowledged that at the age of 17 he had volunteered for submarine service, was not accepted and instead drafted into a Waffen-SS Panzer Division.
 Grass was born in Danzig on October 16, 1927, to Wilhelm Grass (1899–1979), a Protestant ethnic German, and Helene Grass (née Knoff, 1898–1954), a Roman Catholic of Kashubian origin.[1][2] Grass was raised a Catholic. His parents had a grocery store with an attached apartment in Danzig-Langfuhr. He has one sister, who was born in 1930.
 Grass attended the Danzig Gymnasium Conradinum. In 1943 he became a Luftwaffenhelfer, then he was drafted into the Reichsarbeitsdienst. He volunteered for submarine service with the Kriegsmarine "to get out of the confinement he felt as a teenager in his parents' house" which he considered - in a negative way - civic Catholic lower middle class.[3][4] in November 1944, shortly after his seventeenth birthday, but was not accepted and instead was drafted into the 10th SS Panzer Division Frundsberg [5]. With the Panzer Division he saw active combat from February 1945 until he was wounded on 20 April 1945, captured in Marienbad and sent to an American prisoner-of-war camp. His home city was taken over by Soviet and Polish administration and he thus was not able to return home, finding refuge in western Germany.
 In 1946 and 1947 he worked in a mine and received an education in stonemasonry. For many years he studied sculpture and graphics, first at the Kunstakademie Düsseldorf, then at the Berlin University of the Arts. Grass worked as an author, graphic designer, and sculptor, traveling frequently. He married in 1954 and since 1960 has lived in Berlin as well as part-time in Schleswig-Holstein. Divorced in 1978, he remarried in 1979. From 1983 to 1986 he held the presidency of the Berlin Academy of the Arts.
 English-speaking readers probably know Grass best as the author of The Tin Drum (Die Blechtrommel), published in 1959 (and subsequently filmed by director Volker Schlöndorff in 1979). It was followed in 1961 by the novella Cat and Mouse (Katz und Maus) and in 1963 by the novel Dog Years (Hundejahre), which together with The Tin Drum form what is known as The Danzig Trilogy. All three works deal with the rise of National Socialism and with the war experience in the unique cultural setting of Danzig and the delta of the Vistula River. Dog Years, in many respects a sequel to The Tin Drum, portrays the area's mixed ethnicities and complex historical background in lyrical prose that is highly evocative.[who?]
 He was elected in 1993 an Honorary Fellow of the Royal Society of Literature [6]
 As a trained graphic artist, he has also created the distinctive cover art for his novels.
 Grass has received dozens of international awards and in 1999 achieved the highest literary honor: the Nobel Prize in Literature. His literature is commonly categorised as part of the artistic movement of Vergangenheitsbewältigung, roughly translated as "coming to terms with the past."
 In 2002 Grass returned to the forefront of world literature with Crabwalk (Im Krebsgang). This novel, one of whose main characters first appeared in Cat and Mouse, was Grass' most successful work in decades, and is about the Soviet sinking of the refugee ship Wilhelm Gustloff in 1945 murdering 9000 people.
 Representatives of the City of Bremen joined together to establish the Günter Grass Foundation, with the aim of establishing a centralized collection of his numerous works, especially his many personal readings, videos and films. The Günter Grass House in Lübeck houses exhibitions of his drawings and sculptures, an archive and a library.
 Grass took an active role in the Social Democratic Party of Germany (SPD) and supported Willy Brandt's election campaign. Grass criticised left-wing radicals and instead argued in favour of the "snail's pace", as he put it, of democratic reform (Aus dem Tagebuch einer Schnecke). Books containing his speeches and essays have been released throughout his literary career.
 In the 1980s, he became active in the peace movement and visited Calcutta for six months. A diary with drawings was published as Zunge zeigen, an allusion to Kali's tongue.
 During the events leading up to the reunification of Germany in 1989-90, Grass argued for the continued separation of the two German states, asserting that a unified Germany would necessarily resume its role as belligerent nation-state.
 In 2001, Grass proposed the creation of a German-Polish museum for art lost during the War. The Hague Convention of 1907 requires the return of art that had been evacuated, stolen or seized. Unlike many countries[citation needed] that have cooperated with Germany, some countries refuse to repatriate some of the looted art.[7][8]
 On 12 August 2006, in an interview [9] about his forthcoming book Peeling the Onion, Grass stated that he had been a member of the Waffen-SS. Before this interview, Grass was seen as someone who had been a typical member of the "Flakhelfer generation," one of those too young to see much fighting or to be involved with the Nazi regime in any way beyond its youth organizations.
 On 15 August 2006, the online edition of Der Spiegel, Spiegel Online, published three documents from U.S. forces dating from 1946, verifying Grass's Waffen-SS membership.[10]
 After an unsuccessful attempt to volunteer for the U-boat fleet at age 15, Grass was conscripted into the Reichsarbeitsdienst (Reich Labor Service), and was then called up for the Waffen-SS in 1944. At that point of the war, youths could be conscripted into the Waffen-SS instead of the Armed Forces (Wehrmacht).
 
Grass was trained as a tank gunner and fought with the 10th SS Panzer Division Frundsberg until its surrender to U.S. forces at Marienbad. In 2007, Grass published an account of his wartime experience in The New Yorker, including an attempt to "string together the circumstances that probably triggered and nourished my decision to enlist.".[11] To the BBC, Grass said in 2006: Joachim Fest, German journalist, historian and biographer of Adolf Hitler, told the German weekly Der Spiegel about Grass's disclosure: As Grass has for many decades been an outspoken left-leaning critic of Germany's treatment of its Nazi past, his statement caused a great stir in the press.
 Rolf Hochhuth said it was "disgusting" that this same "politically correct" Grass had publicly criticized Helmut Kohl and Ronald Reagan's visit to a military cemetery at Bitburg in 1985, because it also contained graves of Waffen-SS soldiers. In the same vein, the historian Michael Wolffsohn has accused Grass of hypocrisy in not earlier disclosing his SS membership. Many have come to Grass' defense based upon the fact the involuntary Waffen-SS membership was very early in Grass' life, starting when he was drafted shortly after his seventeenth birthday, and also precisely because he has always been publicly critical of Germany's Nazi past, unlike many of his conservative critics. For example, novelist John Irving has criticised those who would dismiss the achievements of a lifetime because of a mistake made as a teenager.[14] However, Pat Buchanan, Reagan's White House Communication's director at the time, has claimed that the very point that Reagan was seeking to emphasize in his own decision to visit Bitburg was that many of the Waffen-SS were either very young or had been drafted into the Nazi forces.[15]
 
Grass's biographer Michael Jürgs spoke of "the end of a moral institution".[16]
Lech Wałęsa had initially criticized Grass for keeping silent about his SS membership for 60 years, but after a few days had publicly withdrawn his criticism after reading the letter of Grass to the mayor of Gdańsk, and admitted that Grass "set the good example for the others."[17]
On 14 August 2006, the ruling party of Poland, Law and Justice, called on Grass to relinquish his honorary citizenship of Gdańsk. A 'Law and Justice' politician Jacek Kurski stated, "It is unacceptable for a city where the first blood was shed, where World War II began, to have a Waffen-SS member as an honorary citizen." However, according to a 2010 poll[18][19] ordered by city's authorities, the vast majority of Gdańsk citizens did not support Kurski's position. The mayor of Gdańsk, Paweł Adamowicz, said that he opposed submitting the affair to the municipal council because it was not for the council to judge history.[20]
 April 4, 2012, Grass published a poem in the Süddeutsche Zeitung, entitled Was gesagt werden muss ("What must be said"), where he expressed concern about an Israeli military strike against Iran. The poem states: “Why do I only say now, aged and with my last ink: the atomic power Israel is endangering the already fragile world peace?” His answer to this rhetorical question is that the "incomparable" crimes of Nazis against Jews, and the duty of Germans to avoid anti-Semitism, have kept him silent. But he is no longer willing to stay quiet: “I admit: I will be silent no longer, because I am sick of the hypocrisy of the West.”[21]
 The poem generated controversy in Germany and Israel,[22] and he was declared persona non grata by Eli Yishai, the Israeli Minister for the Interior, who remarked:"Grass' poems are an attempt to guide the fire of hate toward the State of Israel and the Israeli people, and to advance the ideas of which he was a public partner in the past, when he wore the uniform of the SS." Foreign Minister Avigdor Lieberman commented on the:'egoism of so-called Western intellectuals, who are willing to sacrifice the Jewish people on the altar of crazy anti-Semites for a second time, just to sell a few more books or gain recognition." Grass remarked that his poem criticised the present government of Israel for its policy that keeps building settlements despite a UN resolution.[23]
 Collections in English translation
 
Interviews
 
 1 Life 2 Works

2.1 Nobel Prize

 2.1 Nobel Prize 3 Political activism 4 2006 Waffen-SS revelations 5 2012 poem regarding Israel and Iran 6 Novels and collections 7 References 8 External links Günter Grass' poem about Israel Die Vorzüge der Windhühner (poems, 1956) Die bösen Köche. Ein Drama (play, 1956) translated as The Wicked Cooks in Four Plays (1967) Hochwasser. Ein Stück in zwei Akten (play, 1957) The Flood Onkel, Onkel. Ein Spiel in vier Akten (play, 1958) Mister, Mister Danziger Trilogie
Die Blechtrommel (1959) trans. The Tin Drum (1959)
Katz und Maus (1961) trans. Cat and Mouse (1963)
Hundejahre (1963) trans. Dog Years (1965) Die Blechtrommel (1959) trans. The Tin Drum (1959) Katz und Maus (1961) trans. Cat and Mouse (1963) Hundejahre (1963) trans. Dog Years (1965) Gleisdreieck (poems, 1960) Die Plebejer proben den Aufstand (play, 1966) trans. The Plebeians Rehearse the Uprising (1966) Ausgefragt (poems, 1967) Über das Selbstverständliche. Reden - Aufsätze - Offene Briefe - Kommentare (speeches, essays, 1968) trans. Speak out! Speeches, Open Letters, Commentaries (1969) with 3 additional pieces Örtlich betäubt (1969) trans. Local Anaesthetic (1970) Davor (play, 1970) trans. Max (1972) on a plot from Local Anaesthetic Aus dem Tagebuch einer Schnecke (1972) trans. From the Diary of a Snail (1973) Der Bürger und seine Stimme. Reden Aufsätze Kommentare (speeches, essays, 1974) Denkzettel. Politische Reden und Aufsätze 1965-1976 (political essays and speeches, 1978) Der Butt (1977) trans. The Flounder (1978) Das Treffen in Telgte (1979) trans. The Meeting at Telgte (1981) Kopfgeburten oder Die Deutschen sterben aus (1980) trans. Headbirths, or, the Germans are Dying Out (1982) Widerstand lernen. Politische Gegenreden 1980–1983 (political speeches, 1984) Die Rättin (1986) trans. The Rat (1987) Zunge zeigen. Ein Tagebuch in Zeichnungen ("A Diary in Drawings", 1988) trans. Show Your Tongue (1989) Unkenrufe (1992) trans. The Call of the Toad (1992) Ein weites Feld (1995) trans. Too Far Afield (2000) Mein Jahrhundert (1999) trans. My Century (1999) Im Krebsgang (2002) trans. Crabwalk (2002) Letzte Tänze (poems, 2003) Beim Häuten der Zwiebel (2006) trans. Peeling the Onion (2007) First volume of memoir. Dummer August (poems, 2007) Die Box (2008) trans. The Box (2010) Second volume of memoir. Grimms Wörter (2010) Third volume of memoir. Four Plays (1967) including Ten Minutes to Buffalo In the Egg and Other Poems (1977) Two States One Nation? (1990) Jump up ↑ Garland, The Oxford Companion to German Literature, p. 302.
 Jump up ↑ "The Literary Encyclopedia", Günter Grass (b. 1927). Retrieved on 16 August 2006.
 Jump up ↑ "Katholischen Mief". Source: http://www.zeit.de/2006/34/Leiter-1-34
 Jump up ↑ Nobel prize winner Grass admits serving in SS Reuters, 11 Aug 2006
 Jump up ↑ Spiegel on G. Grass volunteering for submarine service
 Jump up ↑ Royal Society of Literature All Fellows. Royal Society of Literature. Retrieved on 8 August 2010.
 Jump up ↑ Rückgabe von Beutekunst: Die letzten deutschen Kriegsgefangenen. Feuilleton (2010-10-26). Retrieved on 2010-10-31.
 Jump up ↑ http://www.spiegel.de/kultur/gesellschaft/0,1518,498915,00.html
 Jump up ↑ Günter Grass im Interview: Warum ich nach sechzig Jahren mein Schweigen breche. Feuilleton. Retrieved on 2010-10-31.
 Jump up ↑ Grass räumte als Kriegsgefangener Waffen-SS-Mitgliedschaft ein. Spiegel.de. Retrieved on 2010-10-31.
 Jump up ↑ Grass, Günter (2007-06-04). "How I Spent the War : A recruit in the Waffen S.S.". The New Yorker. http://www.newyorker.com/reporting/2007/06/04/070604fa_fact_grass?currentPage=all. Retrieved 2007-05-24. 
 Jump up ↑ "Guenter Grass served in Waffen SS". BBC News. 11 August 2006. http://news.bbc.co.uk/1/hi/entertainment/4785851.stm. 
 Jump up ↑ 
"Grass admits serving in Waffen SS". Reuters. 2006-08-13. Archived from the original on 2006-08-22. http://web.archive.org/web/20060822003712/http://www.iht.com/articles/2006/08/13/news/grass.php. Retrieved 2006-08-13. 
 Jump up ↑ Irving, John (2006-08-19). "Günter Grass is my hero, as a writer and a moral compass". London: The Guardian. http://www.guardian.co.uk/commentisfree/story/0,,1853745,00.html. Retrieved 2006-08-19. 
 Jump up ↑ Pat Buchanan'S Response To Norman Podhoretz'S Op-Ed - Buchanan Campaign Press Releases - T H E I N T E R N E T B R I G A D E - Official Web Site
 Jump up ↑ Echo auf Grass' SS-Vergangenheit: "Ende einer moralischen Instanz" - SPIEGEL ONLINE - Nachrichten - Kultur. Spiegel.de (2006-08-12). Retrieved on 2010-10-31.
 Jump up ↑ SS-Vergangenheit: Walesa macht Grass Ehrenbürgerwürde streitig - SPIEGEL ONLINE - Nachrichten - Kultur. Spiegel.de. Retrieved on 2010-10-31.
 Jump up ↑ Kraj - Gazeta.pl. Serwisy.gazeta.pl. Retrieved on 2010-10-31.
 Jump up ↑ http://bi.gazeta.pl/im/4/3561/m3561294.jpg
 Jump up ↑ 
Rakowiec, Małgorzata (2006-08-14). "Grass asked to give up Polish title". Reuters. http://news.scotsman.com/latest.cfm?id=1185192006. Retrieved 2006-08-14.  [dead link]
 Jump up ↑ 
German Nobel laureate accuses Israel of plotting to ‘wipe out the Iranian people’. Retrieved on 2012-04-05.
 Jump up ↑ Deborah Cole (2012-04-04). "Gunter Grass accuses Israel of plotting to 'wipe out' Iran". Associated Press. Archived from the original on 2012-04-07. http://web.archive.org/web/20120407062947/http://www.google.com/hostednews/afp/article/ALeqM5gMlVY2OQxp-ESLm0CKYOCvNbdjfg?docId=CNG.a6164a792a6ceaa28c4f10c558ab597d.881. 
 Jump up ↑ Ophir Bar-Zohar and Barak Ravid
Minister declares Gunter Grass persona non grata in Israel,' at Haaretz, 8 April, 2012.
 Works by or about Günter Grass in libraries (WorldCat catalog) Günter Grass in the German National Library catalogue (German) Nobel prize Biobibliographical notes (in English, also available in French, German, and Swedish) Biographical timeline (German) Concise biographical information, essays (German) Grass' special links to Gdansk Less concise biographical information (German) Grass admits serving with Waffen-SS The Guardian article Survey of reactions to Grass's disclosure of his time in the Waffen-SS from the German and international press (English) Grass' "Shame" Over SS Service Günter Grass 'Bookweb' on literary website The Ledge, with suggestions for further reading. Detailed article on his Waffen-SS membership (English) Elizabeth Gaffney (Summer 1991). "Gunter Grass, The Art of Fiction No. 124". Gunter Grass discusses The Tin Drum on the BBC World Service programme World Book Club Video interview on PBS with Günter Grass by Charlie Rose 2007 Real Audio interview at NYPL with Günter Grass and Norman Mailer by Andrew O'Hagan Portrait on rosenthalusa.com Vicente Aleixandre (1977) Isaac Bashevis Singer (1978) Odysseas Elytis (1979) Czesław Miłosz (1980) Elias Canetti (1981) Gabriel García Márquez (1982) William Golding (1983) Jaroslav Seifert (1984) Claude Simon (1985) Wole Soyinka (1986) Joseph Brodsky (1987) Naguib Mahfouz (1988) Camilo José Cela (1989) Octavio Paz (1990) Nadine Gordimer (1991) Derek Walcott (1992) Toni Morrison (1993) Kenzaburō Ōe (1994) Seamus Heaney (1995) Wisława Szymborska (1996) Dario Fo (1997) José Saramago (1998) Günter Grass (1999) Gao Xingjian (2000) Complete list (1901–1925) (1926–1950) (1951–1975) (1976–2000) (2001–2025) History of Germany Medieval German literature Sturm und Drang Weimar classicism Romanticism Literary realism Weimar culture Exilliteratur German studies Reinmar der Alte Hartmann von Aue Walter von der Vogelweide Wolfram von Eschenbach Albrecht von Johansdorf Heinrich von Morungen Das Nibelungenlied Gottfried von Strassburg Paul Fleming Hans Jakob Christoffel von Grimmelshausen Andreas Gryphius Christian Hoffmann von Hoffmannswaldau Johann Michael Moscherosch Martin Opitz Hans Sachs Angelus Silesius Anthony Ulrich, Duke of Brunswick-Wolfenbüttel Georg Wickram Christian Gellert Johann Wolfgang von Goethe Johann Christoph Gottsched Johann Christian Günther Friedrich Hölderlin Friedrich Gottlieb Klopstock Jakob Michael Reinhold Lenz Gotthold Ephraim Lessing Novalis (Friedrich von Hardenberg) Jean Paul Friedrich Schiller Johann Gottfried Schnabel Christoph Martin Wieland Ludwig Achim von Arnim Clemens Brentano Georg Büchner Adalbert von Chamisso Annette von Droste-Hülshoff Marie von Ebner-Eschenbach Joseph von Eichendorff Theodor Fontane Gustav Freytag Jeremias Gotthelf Franz Grillparzer Jacob Grimm Wilhelm Grimm Gerhart Hauptmann Christian Friedrich Hebbel Johann Peter Hebel Heinrich Heine Georg Herwegh Paul Heyse E.T.A. Hoffmann Friedrich Hölderlin Gottfried Keller Heinrich von Kleist Nikolaus Lenau Conrad Ferdinand Meyer Eduard Mörike Johann Nestroy Wilhelm Raabe Adalbert Stifter Theodor Storm Ludwig Tieck Ludwig Uhland Hermann Bahr Johannes R. Becher Gottfried Benn Thomas Bernhard Heinrich Böll Volker Braun Bertolt Brecht Hermann Broch Arnolt Bronnen Hans Christoph Buch Elias Canetti Paul Celan Alfred Döblin Friedrich Dürrenmatt Lion Feuchtwanger Marieluise Fleißer Erich Fried Max Frisch Stefan George Ernst Glaeser Rainald Goetz Hermann Hesse Georg Heym Hugo von Hoffmansthal Ricarda Huch Peter Huchel Uwe Johnson Ernst Jünger Franz Kafka Erich Kästner Irmgard Keun Sarah Kirsch Karl Kraus Else Lasker-Schüler Heinrich Mann Klaus Mann Thomas Mann Christian Morgenstern Erich Mühsam Heiner Müller Adolf Muschg Robert Musil Ernst Erich Noth Erich Maria Remarque Rainer Maria Rilke Joseph Roth Nelly Sachs Ernst von Salomon Paul Scheerbart Arthur Schnitzler W.G. Sebald Anna Seghers Ernst Toller Georg Trakl Kurt Tucholsky Robert Walser Peter Weiss Christa Wolf Stefan Zweig Thomas Brussig Günther Grass Durs Grünbein Peter Handke Elfriede Jelinek Wladimir Kaminer Alexander Kluge Christian Kracht Siegfried Lenz Monika Maron Terézia Mora Herta Müller Emine Sevgi Özdamar Rafik Schami Zafer Şenocak Botho Strauß Yoko Tawada Uwe Timm Martin Walser Peter Wawerzinek Feridun Zaimoğlu Rudolf Christoph Eucken Paul Heyse Gerhart Hauptmann Carl Spitteler Thomas Mann Nelly Sachs Heinrich Böll Elias Canetti Günter Grass Elfriede Jelinek Herta Müller Georg Büchner Prize Adelbert von Chamisso Prize Hans Fallada Prize Goethe Prize Heinrich Heine Prize Friedrich Hölderlin Prize Kleist Prize Leipzig Book Fair Prize Nelly Sachs Prize 1927 births 2015 deaths People from Gdańsk German novelists Günter Grass Kashubian poets Magic realism writers Nobel laureates in Literature German Nobel laureates German socialists German military personnel of World War II SS personnel German autobiographers Tank personnel Fellows of the Royal Society of Literature Georg Büchner Prize winners Kunstakademie Düsseldorf alumni Writers who illustrated their own writing Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information Deutsch Español Magyar  This page was last edited on 4 November 2019, at 11:22. Privacy policy About Metapedia Disclaimers 
  It happened as it did to many of my age. We were in the labour service and all at once, a year later, the call-up notice lay on the table. And only when I got to Dresden did I learn it was the Waffen-SS.[12]
 After 60 years, this confession comes a bit too late. I can't understand how someone who for decades set himself up as a moral authority, a rather smug one, could pull this off.[13]
 Georg Büchner Prize Nobel Prize in Literature Günter Wilhelm Grass Collections in English translation Günter Grass Interviews Authority control 
 Novelist, Poet, Playwright, Sculptor, Graphic Designer,
 German
 1956–present
 The Tin Drum
 Georg Büchner Prize  1965 
Nobel Prize in Literature  1999 

 
 
Influences
Bocaccio, François Rabelais, Grimmelshausen, Cervantès, Voltaire, Denis Diderot Laurence Sterne, Goethe, Thomas Mann, Friedrich Hölderlin, Friedrich Nietzsche, Georg Trakl, Ivan Goncharov, Theodor Fontane, Rainer Maria Rilke, Franz Kafka, Alfred Döblin, Albert Camus, the Nouveau Roman, Vladimir Nabokov

 
 
Influenced
Gabriel García Márquez, Salman Rushdie, Haruki Murakami[citation needed], John Irving, Philip Roth, Michel Tournier, Jose Saramago, António Lobo Antunes, Patrick Süskind, Graham Swift, Tom Robbins

 
 
   Wikimedia Commons has media related to: Günter Grass  v • d • e
Nobel Laureates in Literature (1976–2000)* Saul Bellow (1976)
Vicente Aleixandre (1977)
Isaac Bashevis Singer (1978)
Odysseas Elytis (1979)
Czesław Miłosz (1980)
Elias Canetti (1981)
Gabriel García Márquez (1982)
William Golding (1983)
Jaroslav Seifert (1984)
Claude Simon (1985)
Wole Soyinka (1986)
Joseph Brodsky (1987)
Naguib Mahfouz (1988)
Camilo José Cela (1989)
Octavio Paz (1990)
Nadine Gordimer (1991)
Derek Walcott (1992)
Toni Morrison (1993)
Kenzaburō Ōe (1994)
Seamus Heaney (1995)
Wisława Szymborska (1996)
Dario Fo (1997)
José Saramago (1998)
Günter Grass (1999)
Gao Xingjian (2000)

Complete list
(1901–1925)
(1926–1950)
(1951–1975)
(1976–2000)
(2001–2025)

  * Saul Bellow (1976)
Vicente Aleixandre (1977)
Isaac Bashevis Singer (1978)
Odysseas Elytis (1979)
Czesław Miłosz (1980)
Elias Canetti (1981)
Gabriel García Márquez (1982)
William Golding (1983)
Jaroslav Seifert (1984)
Claude Simon (1985)
Wole Soyinka (1986)
Joseph Brodsky (1987)
Naguib Mahfouz (1988)
Camilo José Cela (1989)
Octavio Paz (1990)
Nadine Gordimer (1991)
Derek Walcott (1992)
Toni Morrison (1993)
Kenzaburō Ōe (1994)
Seamus Heaney (1995)
Wisława Szymborska (1996)
Dario Fo (1997)
José Saramago (1998)
Günter Grass (1999)
Gao Xingjian (2000)  

Complete list
(1901–1925)
(1926–1950)
(1951–1975)
(1976–2000)
(2001–2025)

 v • d • e
German literatureRelated articles* German language
History of Germany
Medieval German literature
Sturm und Drang
Weimar classicism
Romanticism
Literary realism
Weimar culture
Exilliteratur
German studiesMedieval literature* Dietmar von Aist
Reinmar der Alte
Hartmann von Aue
Walter von der Vogelweide
Wolfram von Eschenbach
Albrecht von Johansdorf
Heinrich von Morungen
Das Nibelungenlied
Gottfried von StrassburgEarly modern literature* Simon Dach
Paul Fleming
Hans Jakob Christoffel von Grimmelshausen
Andreas Gryphius
Christian Hoffmann von Hoffmannswaldau
Johann Michael Moscherosch
Martin Opitz
Hans Sachs
Angelus Silesius
Anthony Ulrich, Duke of Brunswick-Wolfenbüttel
Georg Wickram18th century* Barthold Heinrich Brockes
Christian Gellert
Johann Wolfgang von Goethe
Johann Christoph Gottsched
Johann Christian Günther
Friedrich Hölderlin
Friedrich Gottlieb Klopstock
Jakob Michael Reinhold Lenz
Gotthold Ephraim Lessing
Novalis (Friedrich von Hardenberg)
Jean Paul
Friedrich Schiller
Johann Gottfried Schnabel
Christoph Martin Wieland19th century* Bettina von Arnim
Ludwig Achim von Arnim
Clemens Brentano
Georg Büchner
Adalbert von Chamisso
Annette von Droste-Hülshoff
Marie von Ebner-Eschenbach
Joseph von Eichendorff
Theodor Fontane
Gustav Freytag
Jeremias Gotthelf
Franz Grillparzer
Jacob Grimm
Wilhelm Grimm
Gerhart Hauptmann
Christian Friedrich Hebbel
Johann Peter Hebel
Heinrich Heine
Georg Herwegh
Paul Heyse
E.T.A. Hoffmann
Friedrich Hölderlin
Gottfried Keller
Heinrich von Kleist
Nikolaus Lenau
Conrad Ferdinand Meyer
Eduard Mörike
Johann Nestroy
Wilhelm Raabe
Adalbert Stifter
Theodor Storm
Ludwig Tieck
Ludwig Uhland20th century* Ingeborg Bachmann
Hermann Bahr
Johannes R. Becher
Gottfried Benn
Thomas Bernhard
Heinrich Böll
Volker Braun
Bertolt Brecht
Hermann Broch
Arnolt Bronnen
Hans Christoph Buch
Elias Canetti
Paul Celan
Alfred Döblin
Friedrich Dürrenmatt
Lion Feuchtwanger
Marieluise Fleißer
Erich Fried
Max Frisch
Stefan George
Ernst Glaeser
Rainald Goetz
Hermann Hesse
Georg Heym
Hugo von Hoffmansthal
Ricarda Huch
Peter Huchel
Uwe Johnson
Ernst Jünger
Franz Kafka
Erich Kästner
Irmgard Keun
Sarah Kirsch
Karl Kraus
Else Lasker-Schüler
Heinrich Mann
Klaus Mann
Thomas Mann
Christian Morgenstern
Erich Mühsam
Heiner Müller
Adolf Muschg
Robert Musil
Ernst Erich Noth
Erich Maria Remarque
Rainer Maria Rilke
Joseph Roth
Nelly Sachs
Ernst von Salomon
Paul Scheerbart
Arthur Schnitzler
W.G. Sebald
Anna Seghers
Ernst Toller
Georg Trakl
Kurt Tucholsky
Robert Walser
Peter Weiss
Christa Wolf
Stefan ZweigContemporary writers* Zsuzsa Bánk
Thomas Brussig
Günther Grass
Durs Grünbein
Peter Handke
Elfriede Jelinek
Wladimir Kaminer
Alexander Kluge
Christian Kracht
Siegfried Lenz
Monika Maron
Terézia Mora
Herta Müller
Emine Sevgi Özdamar
Rafik Schami
Zafer Şenocak
Botho Strauß
Yoko Tawada
Uwe Timm
Martin Walser
Peter Wawerzinek
Feridun ZaimoğluNobel laureates who wrote in German* Theodor Mommsen
Rudolf Christoph Eucken
Paul Heyse
Gerhart Hauptmann
Carl Spitteler
Thomas Mann
Nelly Sachs
Heinrich Böll
Elias Canetti
Günter Grass
Elfriede Jelinek
Herta MüllerGerman language literary awards* Ingeborg Bachmann Prize
Georg Büchner Prize
Adelbert von Chamisso Prize
Hans Fallada Prize
Goethe Prize
Heinrich Heine Prize
Friedrich Hölderlin Prize
Kleist Prize
Leipzig Book Fair Prize
Nelly Sachs Prize  Related articles * German language
History of Germany
Medieval German literature
Sturm und Drang
Weimar classicism
Romanticism
Literary realism
Weimar culture
Exilliteratur
German studies  Medieval literature * Dietmar von Aist
Reinmar der Alte
Hartmann von Aue
Walter von der Vogelweide
Wolfram von Eschenbach
Albrecht von Johansdorf
Heinrich von Morungen
Das Nibelungenlied
Gottfried von Strassburg  Early modern literature * Simon Dach
Paul Fleming
Hans Jakob Christoffel von Grimmelshausen
Andreas Gryphius
Christian Hoffmann von Hoffmannswaldau
Johann Michael Moscherosch
Martin Opitz
Hans Sachs
Angelus Silesius
Anthony Ulrich, Duke of Brunswick-Wolfenbüttel
Georg Wickram  18th century * Barthold Heinrich Brockes
Christian Gellert
Johann Wolfgang von Goethe
Johann Christoph Gottsched
Johann Christian Günther
Friedrich Hölderlin
Friedrich Gottlieb Klopstock
Jakob Michael Reinhold Lenz
Gotthold Ephraim Lessing
Novalis (Friedrich von Hardenberg)
Jean Paul
Friedrich Schiller
Johann Gottfried Schnabel
Christoph Martin Wieland  19th century * Bettina von Arnim
Ludwig Achim von Arnim
Clemens Brentano
Georg Büchner
Adalbert von Chamisso
Annette von Droste-Hülshoff
Marie von Ebner-Eschenbach
Joseph von Eichendorff
Theodor Fontane
Gustav Freytag
Jeremias Gotthelf
Franz Grillparzer
Jacob Grimm
Wilhelm Grimm
Gerhart Hauptmann
Christian Friedrich Hebbel
Johann Peter Hebel
Heinrich Heine
Georg Herwegh
Paul Heyse
E.T.A. Hoffmann
Friedrich Hölderlin
Gottfried Keller
Heinrich von Kleist
Nikolaus Lenau
Conrad Ferdinand Meyer
Eduard Mörike
Johann Nestroy
Wilhelm Raabe
Adalbert Stifter
Theodor Storm
Ludwig Tieck
Ludwig Uhland  20th century * Ingeborg Bachmann
Hermann Bahr
Johannes R. Becher
Gottfried Benn
Thomas Bernhard
Heinrich Böll
Volker Braun
Bertolt Brecht
Hermann Broch
Arnolt Bronnen
Hans Christoph Buch
Elias Canetti
Paul Celan
Alfred Döblin
Friedrich Dürrenmatt
Lion Feuchtwanger
Marieluise Fleißer
Erich Fried
Max Frisch
Stefan George
Ernst Glaeser
Rainald Goetz
Hermann Hesse
Georg Heym
Hugo von Hoffmansthal
Ricarda Huch
Peter Huchel
Uwe Johnson
Ernst Jünger
Franz Kafka
Erich Kästner
Irmgard Keun
Sarah Kirsch
Karl Kraus
Else Lasker-Schüler
Heinrich Mann
Klaus Mann
Thomas Mann
Christian Morgenstern
Erich Mühsam
Heiner Müller
Adolf Muschg
Robert Musil
Ernst Erich Noth
Erich Maria Remarque
Rainer Maria Rilke
Joseph Roth
Nelly Sachs
Ernst von Salomon
Paul Scheerbart
Arthur Schnitzler
W.G. Sebald
Anna Seghers
Ernst Toller
Georg Trakl
Kurt Tucholsky
Robert Walser
Peter Weiss
Christa Wolf
Stefan Zweig  Contemporary writers * Zsuzsa Bánk
Thomas Brussig
Günther Grass
Durs Grünbein
Peter Handke
Elfriede Jelinek
Wladimir Kaminer
Alexander Kluge
Christian Kracht
Siegfried Lenz
Monika Maron
Terézia Mora
Herta Müller
Emine Sevgi Özdamar
Rafik Schami
Zafer Şenocak
Botho Strauß
Yoko Tawada
Uwe Timm
Martin Walser
Peter Wawerzinek
Feridun Zaimoğlu  Nobel laureates who wrote in German * Theodor Mommsen
Rudolf Christoph Eucken
Paul Heyse
Gerhart Hauptmann
Carl Spitteler
Thomas Mann
Nelly Sachs
Heinrich Böll
Elias Canetti
Günter Grass
Elfriede Jelinek
Herta Müller  German language literary awards * Ingeborg Bachmann Prize
Georg Büchner Prize
Adelbert von Chamisso Prize
Hans Fallada Prize
Goethe Prize
Heinrich Heine Prize
Friedrich Hölderlin Prize
Kleist Prize
Leipzig Book Fair Prize
Nelly Sachs Prize NAME
 Grass, Günter Wilhelm
 ALTERNATIVE NAMES
 
 SHORT DESCRIPTION
 German novelist
 DATE OF BIRTH
 16 October 1927
 PLACE OF BIRTH
 Danzig-Langfuhr, Free City of Danzig
 DATE OF DEATH
 
 PLACE OF DEATH
 
 Günter Grass Contents Life Works Political activism 2006 Waffen-SS revelations 2012 poem regarding Israel and Iran Novels and collections References External links Navigation menu Nobel Prize Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools In other languages 