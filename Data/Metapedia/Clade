Cladistics is the method of classifying organisms into groups called clades.
 A clade (Greek = branch) is a group of organisms with a common ancestor and all its descendants (and nothing else). Such a clade is monophyletic.[1] The term 'clade' was coined by English biologist Julian Huxley.
 Birds, dinosaurs, crocodiles, and all other descendants (living or extinct) of their most recent common ancestor form a clade.[2] In the terms of biological systematics, a clade is a single branch on the tree of life, a monophyletic group.
Biological classification needs such a natural group of organisms to be put together and given a taxonomic name.
 This brings classification in line with phylogeny (how living things evolved). In cladistics, clades are the only acceptable units.
 Some versions of cladistics have been the subject of controversy.[3]p226[4][5]
 The term clade was introduced in 1958 by Julian Huxley, cladistic by Cain and Harrison in 1960, and cladist (for an adherent of Hennig's school) by Mayr in 1965.[6] Hennig referred to his own approach as phylogenetic systematics.[1] From the time of his original formulation until the end of the 1980s cladistics remained a minority approach to classification.
 In the 1990s it rapidly became the dominant method of classification in evolutionary biology. Computers made it possible to process large quantities of data about organisms and their characteristics (traits). At about the same time the development of effective sequence analysis techniques made it possible to apply cladistic methods of analysis to biochemical and molecular features of organisms as well as to anatomical ones.[7]
 For some decades in the mid to late twentieth century, a commonly used methodology was numerical taxonomy.[3]p221 This made no attempt to resolve phylogeny, only similarities. The weakness of this approach was that it left out the connection between classification and evolution.[5]
 Phylogenetic nomenclature is a way of giving names to the groups (clades) that have been decided on by cladistic methods. It differs in many ways from Linnaean nomenclature.[8] Critics of phylogenetic nomenclature include Ashlock,[9] Mayr,[10] and Williams.[11]
 Monophyly is defined differently in evolutionary biology and cladistics. In evolutionary biology, the term monophyletic refers to a group of organisms descended from its most recent common ancestor.[12] A monophyletic group may include all or only a part of the descendants of the common ancestor. The ancestor may be a taxon of various ranks.
 In cladistics, by contrast, a monophyletic group is a group consisting of all the inferred descendants of an ancestral species.[1][13]
 In cladistics, a group that includes only some of the descendants of the ancestral species is not monophyletic but paraphyletic. The objective of cladistics is to block out monophyletic clades. Therefore the elements of a paraphyletic group must be rearranged so that they do form one or several clades. Degree of similarity is not a criterion in grouping organisms into clades.
  
 1 Examples 2 History of cladistics 3 Phylogenetic nomenclature 4 Monophyly and paraphyly 5 Relevant pages 6 References Monophyly Paraphyly Polyphyly ↑ 1.0 1.1 1.2 Hennig, Willi 1979. Phylogenetic systematics. Urbana: University of Illinois Press. ISBN 0-252-06814-9.
 ↑ Glossary entry "clade" Understanding Evolution. 2010. University of California Museum of Paleontology.
 ↑ 3.0 3.1 Mayr, Ernst 1982. The growth of biological thought: diversity, evolution and inheritance. Cambridge, MA: Harvard University Press. ISBN 0-674-36446-5.
 ↑ Patterson, Colin 1982. Morphological characters and homology. In Joysey, Kenneth A. & Friday A.E. (eds) Problems in phylogenetic reconstruction. Systematics Association Special Volume 21, London: Academic Press. ISBN 0-12-391250-4
 ↑ 5.0 5.1 Ridley, Mark 1986. Evolution and classification: the reformation of cladism. Longman, London. ISBN 0-582-44497-7
 ↑ Dupuis, Claude 1984. Willi Hennig's impact on taxonomic thought. Annual Review of Ecology and Systematics 15: 1–24. ISSN 0066-4162
 ↑ Baron C. & Høeg J.T. 2005. Gould, Scharm and the paleontological perspective in evolutionary biology. In Koenemann S. & Jenner R.A. Crustacea and arthropod relationships. CRC Press, 3–14. ISBN 978-0-8493-3498-6 [1] retrieved 2008-10-15
 ↑ Hennig, Willi (1975), "'Cladistic analysis or cladistic classification': a reply to Ernst Mayr", Systematic Zoology 24 (2): 244–256, doi:10.2307/2412765.  The paper to which he was responding is reprinted in Mayr, Ernst 1976. Evolution and the diversity of life: selected essays. Cambridge, MA: Harvard University Press, ISBN 0-674-27105-X
 ↑ Ashlock, Peter D. (1971), "Monophyly and associated terms", Systematic Zoology 20 (1): 63–69, doi:10.2307/2412223. 
Ashlock, Peter D. (1972), "Monophyly again", Systematic Zoology 21 (4): 430–438, doi:10.2307/2412435. 
Ashlock, Peter D. (1974), "The uses of cladistics", Annual Review of Ecology and Systematics 5: 81–99, doi:10.1146/annurev.es.05.110174.000501, ISSN 0066-4162. 
Ashlock, Peter D. (1979), "An evolutionary systematist’s view of classification", Systematic Zoology 28 (4): 441–450, doi:10.2307/2412559. 
 Ashlock, Peter D. (1972), "Monophyly again", Systematic Zoology 21 (4): 430–438, doi:10.2307/2412435.  Ashlock, Peter D. (1974), "The uses of cladistics", Annual Review of Ecology and Systematics 5: 81–99, doi:10.1146/annurev.es.05.110174.000501, ISSN 0066-4162.  Ashlock, Peter D. (1979), "An evolutionary systematist’s view of classification", Systematic Zoology 28 (4): 441–450, doi:10.2307/2412559.  ↑ Mayr, Ernst (1974), "Cladistic analysis or cladistic classification?", Zeitschrift fűr Zoologische Systematik und Evolutionforschung 12: 94–128, http://courses.cit.cornell.edu/jdv55/teaching/systematics/mayr%2074%20-%20cladistic%20analysis%20or%20cladistic%20classification.pdf, retrieved 2010-12-14 ,
Mayr, Ernst (1978), "Origin and history of some terms in systematic and evolutionary biology", Systematic Zoology 27 (1): 83–88, doi:10.2307/2412818. ,
Mayr, E & Bock, WJ (2002), "Classifications and other ordering systems", Journal of Zoological Systematics and Evolutionary Research 40 (4): 169–194, doi:10.1046/j.1439-0469.2002.00211.x 
 Mayr, Ernst (1978), "Origin and history of some terms in systematic and evolutionary biology", Systematic Zoology 27 (1): 83–88, doi:10.2307/2412818. , Mayr, E & Bock, WJ (2002), "Classifications and other ordering systems", Journal of Zoological Systematics and Evolutionary Research 40 (4): 169–194, doi:10.1046/j.1439-0469.2002.00211.x  ↑ Williams, P.A. (1992), "Confusion in cladism", Synthese 01: 135–132 
 ↑  Mayr E. & Ashlock P.D. 1991. Principles of systematic zoology. 2nd ed, McGraw-Hill. 
 ↑ Judd W.S. et al 2002. Plant systematics; a phylogenetic approach. 2nd ed, Sunderland MA: Sinauer.
 Cladistics Content from Wikipedia Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 29 May 2016, at 19:21. Privacy policy About Metapedia Disclaimers 
  Cladistics clades Phylogenetic Nomenclature Linnaean Nomenclature 15 24 20 21 5 28 12 27 40 01 Phylogenetic Nomenclature
 Linnaean Nomenclature
 Handles arbitrarily deep trees.
 Biased towards trees about 4 to 12 levels deep.
 Primary goal is to reflect the process of evolution, as currently understood
 Primary goal is to group organisms in a clear and useful way
 Assumes that the shape of the tree will change frequently with new discoveries
 New discoveries may require releveling of Genera, Classes, Orders, and Kingdoms
 Limited to entities related by evolution or ancestry
 Supports groupings when evolution or ancestry are not fully known
 Does not include a process for naming species
 Includes a process for giving unique names to species
 Ignores established paraphyletic groups such as reptiles
 Permits well-known groups such as reptiles
 Limited to organisms that evolved by inherited traits; not applicable to hybrid organisms, or when lateral transfer has happened
 Applicable to all organisms, regardless of evolutionary mechanism
 Cladistics Contents Examples History of cladistics Phylogenetic nomenclature Monophyly and paraphyly Relevant pages References Navigation menu Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 