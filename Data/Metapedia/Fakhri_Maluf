Brother Francis Maluf, M.I.C.M.,[1] born Fakhri Boutros Maluf (July 19, 1913 – September 5, 2009) was a Lebanese nationalist political philosopher, Chairman of the Supreme Council of the Syrian Social Nationalist Party, professor of philosophy and mathematics, and later a founding member of Father Leonard Feeney's religious order, the Slaves of the Immaculate Heart of Mary, ultimately serving as Superior of Saint Benedict Center in Richmond, New Hampshire. Broadly speaking, Brother Francis led two entirely distinct public lives, first in politics and worldly affairs, and then in the spiritual realm after his moving to the United States in 1939 and entering the Catholic Church in 1940, united by the thread of his celebrated charity and capacity for deep reflection. Following his death, fellow Syrian patriot Adel Beshara wrote, "In hindsight, Fakhri Maluf was born three times. First in 1913 when he became a member of the Maluf family; second in 1934 when he joined the Syrian national renaissance; and third when he rediscovered God in [1940]."[2]
 Although his religious order is of the Roman Rite of the Catholic Church, Brother Francis was a Melkite Rite Catholic.
 Fakhri Maluf was a member of the Ghassanids, an Arab tribe that enigrated from Yemen to Syria in the 1st century AD and historically remained Christian under Muslim rule. Maluf is a variant of Maalouf, the surname of one extended Ghassanid clan dating back many centuries taken from the appellation Maayuf (Arabic: معيوف), meaning "exempt", as from restrictions to non-Muslims.[3]
 Fakhri was born in Al Mashrah, Lebanon in 1913, then part of the Ottoman Empire. His father, Boutros Maluf, was a Freemason, and did not raise his children in the Catholic faith. An educational pioneer, Boutros ran a school for poor children out of the Maluf home. He died in Fakhri's boyhood, and his mother took over the school, and raised Fakhri as a Presbyterian; he would later follow in his father's steps and joined the Craft as a young adult.[4] There Fakhri took up his first studies, and later it was in the family school that Fakhri began his own long life of teaching. He studied at the American University of Beirut, graduating with a Bachelor’s Degree in mathematics. Upon graduation he joined the faculty of the University, where he taught physics from 1934-1939.
 Fakhri Maluf's involvement with nationalist politics began in 1933. Still a student, he met Autun Saadeh, then teaching German at the American University.[5] Saadeh's rational thinking and philosophical grounding made an immense impression on Maluf. In 1941 Maluf recounted the 1934 exchange between Saadeh and himself which convinced him to take up the political life.
 It was in the summer of 1934. I was relaxing in my green tent near our house in our small village at the foot of Mount Sannine. The sun had tilted toward the West and I was still deeply absorbed, since the morning, in a book on the mysteries of the stars (astrology). Suddenly, the atmosphere around me invigorated as it always did when Saadeh showed up. It was Saadeh himself. He stepped inside the tent and before I could collect myself he asked me: what is in you hands? “A book on astrology’’, I replied. He rejoined with a bewildering tone: “You are studying the stars without knowing anything about who owns and dominates the ground you are standing on.”[6]
 Thereafter, Maluf joined the Syrian Social Nationalist Party when it was still an underground organization. Though Saadeh was almost ten years Maluf's senior, the two collaborated frequently and became close friends, building on their shared patriotism, fondness for their heritage, and common emphasis on the place of ethics in the national life.[7] Becoming one of the philosophical pillars of the SSNP, he was entrusted to lead its Cultural Seminar, awarded the title of Trustee (amin), and elected as Chairman of the Supreme Council. An intellectual above all, Maluf penned at least a score articles defending Saadeh between 1936 and 1944, and was the first to translate the SSNP Program into English.[8]
 In 1939, Maluf received a scholarship to continue his studies at the University of Michigan, and left Lebanon for the United States. He spoke with a Catholic priest on the ship coming to America, and there awakened in him an interest in the Catholic faith. The University chaplain, along a corps of fellow Catholic students interested in philosophy, fostered his spiritual growth, and he entered the Church on the Feast of St. Andrew (November 30) 1940. He received his Master's Degree in philosophy in 1941, and wrote his doctoral dissertation on the philosophy of science, for which he received his Ph.D. in 1942. Harvard University awarded him a fellowship for further study, and so he arrived in Cambridge, Massachusetts in fall 1942.
 Far though he was from his native Lebanon, his interest in nationalist politics did not wain after his conversion, and he continued writing for the SSNP. It was only in 1945 that he announced his final departure the political realm, a hard blow for Saaleh.[10]
 During his time at University of Michigan, he also met "a very dear friend," Mary Healy of Ann Arbor, Michigan, an undergraduate student and a devout Catholic. They married in 1943.[11]
 In winter 1942,[12] not long after coming to Cambridge, Maluf became a lecturer at the fledgling Saint Benedict Center, a Catholic apostotate geared toward students of Harvard and other nearby schools located at the intersection of Bow and Arrow streets. Catherine Clarke, a foundress of the Center, testifies,
 Fakhri had an eager enthusiasm for things mental and spiritual, and this he passed on to others. His quiet, gracious manners, his sincerity, his unobstrusive erudition, his pursuit of a way of holiness were so convincing and so refreshing that he won our hearts almost immediately.[13]
 He taught philosophy and theology at the Center, with his emphasis on the latter strengthening as he grew in appreciation of the Church Fathers. In the meantile, while from 1942 to 1945, Maluf taught mathematics and science at the College of the Holy Cross in Worcester, Massachusetts. From 1945 to 1949, he taught philosophy, theology, and mathematics at Boston College. However, whilst the Center as a whole began to question the strength of Chiistian conviction in America after the Japanese atomic massacres of 1945, he grew disgusted with the liberalism that had seeped into Catholic academia. In particular, he was scandalized by the flippant treatment of the dogma extra Ecclesiam nulla salus (no salvation outside the Church). As he recounted in Sentimental Theology, an article appearing in the Fall 1947 issue of From the Housetops, a journal published by Saint Benedict Center, upon mentioning the dogma before his colleagues, in no time they "had the doctrine so completely covered with reservations and vicious distinctions as to ruin its meaning and destroy the effect of its challenge."[14]
 "Sentimental Theology" proved a seminal piece, starting a controversy over extra Ecclesiam nulla salus and its interpretation that engendered hostility toward the Center. In addition to winning converts, many Harvard and Radcliffe students attending Saint Benedict Center began to see a conflict between their studies and their faith. Some even resigned, penning letters to administrators explaining their reasons for doing so. While Feeney and his associates never pushed students to take so drastic a sacrifice, he celebrated those who took the brave step. The Center itself had inaugurated Saint Benedict Center School, a small liberal arts institution with a choice faculty accredited to offer B.A., M.A., and Ph.D. degrees, and took on many students from the established universities which, understandably, were not happy with the Catholic thorn in their side. Archbishop Richard Cushing and Auxiliary Bishop John Wright, were urged by persons connected with Harvard to do something about the preaching of this impolitic doctrine. Wright and Cushing (who had formerly encouraged Saint Benedict Center, even contributing two articles to From the Housetops) were quick to dine at Harvard as guests, in a show of solidarity with the secular university.[15]
 Following Father Feeney's refusal to leave the Center to teach at Holy Cross in Worcester Maluf, along with fellow BC professors James R. Walsh' and Charles Ewaskio, Boston College High School teacher David Supple, and many others, signed a letter to Provincial Superior John J. McEleney in defense of Feeney's decision on September 9, 1948, the day after the deadline for compliance passed.[16] The four BC signatories continued to encounter what they considered appalling denials of Church doctrine at Boston College; by 1948 an article teaching salvation outside the Church was required reading in some religion courses, and students maintaining the strict teaching on examinations were marked wrong.[17]
 In October Maluf's superior in the Philosophy department bean questioning him about the presence of his signature in the letter. In November he discussed matters before William L. Keleher, S.J., President of the College, and the orthodoxy of Boston College came up. When the President asked, "Do you imply that we are not teaching the complete Catholic truth here?" Maluf simply said, "I do not know that you are teaching that there is no salvation outside the Church."[18]
 With hardships mounting, on January 19, 1949, 55 Center members bound themselves under the leadership of Fr. Feeney and Catherine Clarke as the Slaves of the Immaculate Heart of Mary, following St. Louis de Monfort's charism of True Devotion to Mary.[19][20] He would later, as the order adopted more of the strictures of traditional Catholic religious orders, adopt the religious name Francis, in honor of Saint Francis Xavier,[21] while his wife chose Mary Bernadette. On February 24, 1949 the four teachers sent an appeal to the Very Reverend Jean Baptiste Janssens, S.J., General of the Society of Jesus. The core of it read:
 We are convinced that at Boston College many doctrines are being taught by members of the Society of Jesus which are contrary to defined dogmas of the Faith. They are teaching implicitly and explicitly that there may be salvation outside the Catholic Church, that a man may be saved without admitting that the Roman Church is supreme among all churches, and that a man may be saved without submission to the Pope.[22]
 Fr. Janssens received the letter,[23] but never respond himself, but the Vatican Secretary of State acknowledged the letter.[24]
 On April 13, President Keleher, under orders from Fr. Janssens, summoned Maluf, Walsh, and Ewaskio to his office. He demanded they recant their statements, or be fired from the College, balking whenever the three asked him to show them where they were in heresy and again insisting they retract their statements; Maluf rebutted that the request, that they deny defined doctrines of the Catholic faith by accepting interpretations which completely nullified their substance, was itself heretical. The three refused the President's demands, and were terminated along with Supple, who refused similar demands.[25] The next day, Holy Thursday, the story of the firing appeared in the Boston Post, Boston Evening Globe, and Boston Traveler,[26] with additional coverage in the Boston Herald, Daily Record, and New York Times on April 15, Good Friday.[27] Subsequently, the Center distributed the Spring 1949 From the Housetops, consisting of Raymond Karam's "Reply to a Liberal", a rebuttal to an article by Fr. Philip Donnelly which they saw as watering down extra Ecclesiam nulla salus, about Boston.[28] This proved the beginning of the Slaves' evangelizing tradition of bookselling.
 Only a week later, the Archbishop placed the Center under interdict. On the evening of April 18, less than twelve hours after a delegation from the Center had received Cushing's promise to help them in any way he could, he forbade Center associates from receiving the sacraments and silenced Father Feeney;[29] ecclesiastical authorities even took the unusual step of denying engaged Center members marriages in the Church.[30]
 In Rome, the Holy Office convened in plenary session on July 27, 1949 to decide the Heresy Case. Ruling that the 'heresy' lay with Saint Benedict Center, its decision was "approved by His Holiness, Pope Pius XII, in an audience the following day. On August 8, 1949, it sent an official letter bearing the decision to Archbishop Cushing, including the Holy Office's interpretation of the dogma extra Ecclesiam nulla salus. Parts of the letter were published in The Pilot, the official newspaper of the Archdiocese of Boston (The Pilot would publish the full contents on September 6, 1952).[31] Father Feeney did not accept the letter, and issued a counter-statement.[32] These events became known as "The Boston Heresy Case" after Maluf gave a series of lectures about them under that title in fall 1949.[33]
 In June 1950, the Center was struck a further blow when the commonwealth, under pressure from Harvard,[34] revoked the state authorization of Saint Benedict Center School, where Maluf taught, meaning veterans could not attend under the GI Bill. This, at the suggestion of Maluf, was the beginning of Father Feeney's notorious Sunday orations on Boston Common, delivered every week without fail until 1958.[35]
 Over the succeeding years, the Slaves took on more and more of the aspects of a traditional religious order as they were further marginalized. The Buildings of Saint Benedict Center were sometimes stoned,[36] and a fence was constructed around the premises. The forbidding grounds, embellished by the new religious order's black garments and discipline, earned the nickname "Quaker Village." Maluf, one of Father Feeney's most devoted men, was always near the center of the action, and when Feeney decried and fought the influence of organized Jewry, Maluf, an Arab was the target of choice for the Center's Jewish foes.[37]
 In 1958, the Center left Cambridge for a farm in the quieter community of Still River. By this time, the Slaves had already adopted vows of chastity, and the married couples among them lived as brother and sister in the monastic life. This, however, caused a dilemma over the fate of their children. The twelve married couples in the order had among them 39 children,[38] of whom five, Mariam, Peter, Leonard, Anna Maria, and Agnes, were Maluf's. Eventually, Fr. Feeney became convinced that, after the age of three, children should be raised communally after the age of three, and thenceforth only rarely spend time with their parents. Discipline was rigorous, including corporal punishment; as with the earlier Saint Benedict Center School students, they were given an education of the highest standard.[39] Thus were Maluf's children raised communally for much of their lives. The measure kept the community together, in an interview for Gary Potter's 1995 After the Boston Heresy Case he considered the communal rearing, which had few precedents outside of heretical groups like the Cathars, a mistake.[40]
 Following the death of Sr. Catherine in 1968, divisions appeared in the community. Before Father Feeney's death, most of the brothers had defected to the Benedictines under William Gibbs (Brother Gabriel Gibbs, and later Father after his Benedictine ordination); most of the remaining members followed Brother Hugh MacIsaac, while Mother Teresa Beneway led a community of sisters later to be known as St. Ann's House. Maluf was one of those who fell in with Br. Hugh. Shortly after Feeney's reconciliation with Church authorities in 1973, he issued a statement underscoring that Feeney had been reconciled without having to change his beliefs on extra Ecclesiam nulla salus, a move others thought unwise for creating further tension in a precarious situation.[41]
 Father Feeney died in 1978, and Brother Hugh died the next year. Brother Thomas Augustine Dalton was elected to succeed him. In 1982, Maluf contested Thomas Augustine's leadership, but the majority of the community reaffirmed their support of the superior. Maluf then brought a civil law suit to obtain the superiorship; the suit lasted five trying years, but was a failure. Defeated, he and four others (his chaste spouse among them) left in 1988, and established a house of Saint Benedct Center in Richmond, New Hampshire.[42]
 Brother Francis devoted the time remaining to him to teaching. Saint Benedict Center-Richmond opened Immaculate Heart of Mary, a K-12 school at which he taught into his 90s. He also crafted the syllabus of the Saint Augustine Institute, a Saint Benedict Center program for adults seeking a Catholic liberal arts education, intended for small group discussions with book reports that could be completed by mail.[43]
 As in his professorial days, Maluf's favored lecture subject remained philosophy. An entire course in Perennial Philosophy, meaning Aristoteleo-Thomistic philosophy (not to be confused with Perennial Traditionalism) was, divided into eight parts, was recorded, and is available from the Center's store.[44] The eight parts are:
 I. Logic
II. Cosmology
III. Psychology
IV. Ethics
V. Greek Philosophy
VI. Polemics
VII. Epistemology
VIII. Ontology
 An Introduction[45] and the volume on Cosmology[46] were published in print in 1995 and 2002, respectively (the volume on Logic having proven too large a project to undertake first), but the publication project has apparently stalled. Divine Alchemy,[47] a volume of Maluf's poetry, was released in 2000, followed by The Challenge of Faith,[48] a collection of seventy-two concise meditations in 2004, both by Loreto Publications.
 At 95 years of age in March 2009, Brother Francis was duly presented with "The Richmond 250 Cane", which is presented to the town's oldest citizen every year.[49] After a protracted illness he died on September 5. His Requiem Mass was said by the Very Reverend Father Daniel O. Lamothe at Saint Margaret Mary Church in Keene on September 10. Traditionalist Catholics paid homage to the longtime pedagogue.[50]
 Maluf was also remembered by Syrian patriots. Though he had renounced the political life nearly sixty-five years before his death, fellow expat nationalist Adel Beshara wrote a memorial piece in Al-Mashriq: A Quarterly Journal of Middle Eastern Studies for September 2010;[51] the SSNP site Syrian Social Nationalist Information Network reposted the contents of the issue on its site.[52]
 “Our National Characteristics” (Al-Maarad, 14 April, 1936)
“Our nation is not a Neglected Chapter in History” (Al-Makshouf, 13 October, 1937)
“The Population Density in Syria” (An-Nahda, November 1937)
“A Reply to the Poet Elias Farhat” (Su ria al-Jadidah, 25 March, 1939)
“The National and non-National Man of Letters” (Suria al-Jadidah, 14 April, 1939)
“Our Culture” (Al-Zowba’a, 15 May, 1939)
“Syria’s Future in the Event of War” (Suria al-Jadidah, 14 August, 1939)
“The Concept of Wait and See” (Suria al-Jadidah, 7 Sept, 1939)
“Philosophical Aspect in Saadeh’s Thought” (Suria al-Jadidah, 16 November, 1939) (fragment)
“From Zeno to Saadeh” (Suria al-Jadi dah, 16 February, 1940)
“The New Democracy” (al-Salam, 27 August 1940)
“Syria’s New Message to the World” (Suria al-Jadidah, 31 August, 1940)
“Ideologies and the Philosophy of History” (Suria al-Jadidah, 17 April, 1941)
“An Aspect of the National Renai sance” (Suria al-Jadidah, 19 April, 1941)
“Between Two Homelands” (Al-Nisr, 22 April, 1941)
“The Leader [Saadeh] in his Guiding Work” (Suria al-Jadidah, 19 May, 1941)
“Ideology, Faith, Solidarity” (Suria al-Jadidah, 23 May, 1941)
“The Ideology and the Truth” (al-Zowba’a, 15 February, 1943)
“To the Leader Antun Saadeh” (al-Zowba’a, 15 June, 1943)
“A Letter to Salloum Mukar zil” (Al-Huda, 10 July, 1944)
 Philosophia Perennis: Vol I - Introduction. Richmond, New Hampshire: Saint Benedict Center, 1995.
Philosophia Perennis: Vol III - Cosmology. Fitzwilliam, New Hampshire: Loreto Publications, 2002.
Divine Alchemy. Fitzwilliam, New Hampshire: Loreto Publications: 2000.
The Challenge of Faith. Fitzwilliam, New Hampshire: Loreto Publications, 2004.
 Brother Francis' Obituary | Catholicism.org. September 11, 2009.
Clarke, Catherine. The Loyolas and the Cabots. Saint Benedict Center: Richmond, NH, 1992.
Potter, Gary. After The Boston Heresy Case. Catholic Treasures Books: Monrovia, CA, 1995.
 1 Biography

1.1 Heritage
1.2 Early years
1.3 Syrian Social Nationalist Party
1.4 Move to the United States, Conversion, and Marriage
1.5 Saint Benedict Center
1.6 The Boston Heresy Case
1.7 Religious Order
1.8 Last years

 1.1 Heritage 1.2 Early years 1.3 Syrian Social Nationalist Party 1.4 Move to the United States, Conversion, and Marriage 1.5 Saint Benedict Center 1.6 The Boston Heresy Case 1.7 Religious Order 1.8 Last years 2 Bibliography

2.1 Syrian nationalism
2.2 Philosophy, Theology, and Poetry

 2.1 Syrian nationalism 2.2 Philosophy, Theology, and Poetry 3 Sources 4 References ↑ Br. Francis Maluf, M.I.C.M. | Catholicism.org
 ↑ Adel Beshara. A Great Man Has Died: Fakhri Maluf (Brother Francis Maluf) 1913-2009 Al-Mashriq: A Quarterly Journal of Middle East Studies, Vol. 9, No. 34 (September 2010). Beshara, however, is mistaken about the date of Maluf's rediscovery of God, dated by the Slaves of the Immaculate Heart of Mary, who are certainly not mistaken about this spiritual matter, to 1940.
 ↑ See for instance Maloofs International.
 ↑ Fakhri Maluf. "Sentimental Theology," Note 4. From the Housetops, Volume II, No.1. September 1947. Available at catholicism.org [1]. Note how Maluf refers to his former self as a heretic rather than an unbeliever.
 ↑ Don Peretz. The Middle East Today. Greenwood Publishing Group, 1994 , pg. 384.
 ↑ "A Great Man Has Died."
 ↑ On ethics Saadeh himself held: One of the most important elements in any national revival, after the establishment of the concept of nationhood and identification of the principal goals, is the issue of eth ics or ethical mentality, finding a system of proper ethos capable of changing the state of the people or society. Every political strategy and every military strategy, irrespective of how masterly or complete it might be, can only be realized through ethical principles that can bear the burdens of such a strategy. That is to say solid ethics that embody unyielding resolu tion, unbendable faith, a robust will and the belief that principles are more important than life itself. This is because human life without human values to which man holds fast and with which he can build his personality and sense of existence is worthless: it is no better than animal life. "A Great Man Has Died."
 ↑ "A Great Man Has Died."
 ↑ Franklin D. Roosevelt. "Address to the International Student Assembly." September 3, 1942. Available at The American Presidency Project [2]. Accessed July 18, 2013.
 ↑ "A Great Man Has Died."
 ↑ Catherine G. Clarke. The Loyolas and the Cabots. Richmond, New Hampshire: Saint Benedict Center, 1993, pp. 21-22.
 ↑ Gary Potter. After the Boston Heresy Case. Monrovia, California: Catholic Treasures Books, 1995, pg. 82.
 ↑ Clarke, pg. 22.
 ↑ Fakhri Maluf. "Sentimental Theology." From the Housetops, Volume II, No.1. September 1947. Available at catholicism.org [3].
 ↑ Clarke, pp. 49-50, 79-80.
 ↑ Clarke, pp. 113-115.
 ↑ Clarke, pg.169. The article, Fr. Philip Donnelly's "Observations on the Question of Salvation Outside the Church", is reproduced after Raymond Karam's "Reply to a Liberal" in the link.
 ↑ Clarke, pg. 134.
 ↑ 
Neumann, John. A Latter-Day Athanasius: Father Leonard Feeney. Crusade of Saint Benedict Center, Richmond. Retrieved on 2013-07-01.
 ↑ History of the Slaves of the Immaculate Heart of Mary (part 3/4). Saint Benedict Center (Still River, MA).
 ↑ "Br. Francis Maluf, M.I.C.M."
 ↑ Clarke, pg. 171.
 ↑ Clarke, pg. 179.
 ↑ Clarke, pg. 172.
 ↑ Clarke, pp. 185-190.
 ↑ Clarke, pp. 195-198.
 ↑ Clarke, pg. 200.
 ↑ Clarke, pp. 203-207.
 ↑ Clarke, pp. 213-216.
 ↑ Clarke, pp. 259-266.
 ↑ Letter of the Holy Office to the Archbishop of Boston
 ↑ Potter, pg. 140. Father Feeney's statement reads, It is reported by The Pilot in the letter of the Holy Office... that 'the decisions set forth in this letter were approved by His Holiness, Pope Pius XII, in an audience' (italics ours). St. Benedict Center still knows that it has no answer on its doctrinal crusade and its appeal for an  ex cathedra pronouncement from the Holy Father. IS THERE SALVATION OUTSIDE THE CATHOLIC CHURCH, or IS THERE NOT? If we have said something inconsistent with Catholic doctrine, we would like to know what it is from the Holy See and in a clear, definite statement.The one unmistakable statement from the Holy Office that got through... is the reaffirmation of the fact that the doctrine that there is no salvation outside the Church 'is an incontestable principle.' This is exactly the principle which Archbishop Cushing, Bishop Wright, and Boston College have contested, and the principle for which we have been suffering for a solid year. The Pilot advises us to 'return to the unity of the Church at the peril of our souls.' That we are outside the unity of the Church we deny.
 ↑ Brenton Welling Jr. St. Benedict's Explains Its Doctrine The Harvard Crimson. September 27, 1949. Accessed July 14, 2013.
 ↑ Wallach Will See State Educational Chief on Feeney The Harvard Crimson. December 14, 1949. Accessed July 1, 2013.
 ↑ Potter, pp. 133-134.
 ↑ Potter, 165.
 ↑ Potter, pg. 135.
 ↑ Potter, pg. 133.
 ↑ Potter, pg. 171.
 ↑ Potter, 165.
 ↑ Potter, pg. 180.
 ↑ History of the Slaves of the Immaculate Heart of Mary (page 4) Saint Benedict Center, Still River, MA. Accessed July 23, 2013. This source relates the dispute from the side favorable to Br. Thomas Augustine.
 ↑ Saint Augustine Institute of Catholic Studies store.Catholicism.org
 ↑ Courses on Perennial Philosophy by Brother Francis Maluf, M.I.C.M., Ph.D. store.Catholicism.org
 ↑ Philosophia Perennis: Vol I - Introduction to Philosophy store.Catholicism.org
 ↑ Philosophia Perennis: Vol III - Cosmology store.Catholicism.org
 ↑ Literature :: Divine Alchemy Loreto Publications - Catholic Book Publisher
 ↑ Devotion and Spirituality :: Challenge of Faith, The] Loreto Publications - Catholic Book Publisher
 ↑ Br. Maximilian Maria, M.I.C.M. Brother Francis and The Richmond 250 Cane Catholicism.org. March 28, 2009. Accessed July 23, 2013.
 ↑ TIA Homage to Brother Francis Maluf, MICM Tradition in Action. September 9, 2009. Accessed July 23, 2013.
 ↑ "A Great Man Has Died: Fakhri Maluf (Brother Francis Maluf) 1913-2009."
 ↑ [4] October 23, 2010. Accessed July 23, 2013.
 1913 births 2009 deaths Arab Christians Lebanese expatriates Founders of Roman Catholic religious communities Political philosophers Syrian nationalists Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 4 October 2016, at 15:29. Privacy policy About Metapedia Disclaimers 
  
It was in the summer of 1934. I was relaxing in my green tent near our house in our small village at the foot of Mount Sannine. The sun had tilted toward the West and I was still deeply absorbed, since the morning, in a book on the mysteries of the stars (astrology). Suddenly, the atmosphere around me invigorated as it always did when Saadeh showed up. It was Saadeh himself. He stepped inside the tent and before I could collect myself he asked me: what is in you hands? “A book on astrology’’, I replied. He rejoined with a bewildering tone: “You are studying the stars without knowing anything about who owns and dominates the ground you are standing on.”[6]

 
Fakhri had an eager enthusiasm for things mental and spiritual, and this he passed on to others. His quiet, gracious manners, his sincerity, his unobstrusive erudition, his pursuit of a way of holiness were so convincing and so refreshing that he won our hearts almost immediately.[13]

 
We are convinced that at Boston College many doctrines are being taught by members of the Society of Jesus which are contrary to defined dogmas of the Faith. They are teaching implicitly and explicitly that there may be salvation outside the Catholic Church, that a man may be saved without admitting that the Roman Church is supreme among all churches, and that a man may be saved without submission to the Pope.[22]

 Brother Francis Maluf, M.I.C.M. Fakhri Boutros Maluf Catherine Clarke Charles Ewaskio David Supple William L. Keleher Gabriel Gibbs Hugh MacIsaac Teresa Beneway Thomas Augustine Dalton See also: Leonard Feeney#Saint Benedict Center etc. See also: Leonard Feeney#Saint Benedict Center etc. Francis Maluf Contents Biography Bibliography Sources References Navigation menu Heritage Early years Syrian Social Nationalist Party Move to the United States, Conversion, and Marriage Saint Benedict Center The Boston Heresy Case Religious Order Last years Syrian nationalism Philosophy, Theology, and Poetry Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 