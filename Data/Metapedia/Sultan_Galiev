Sultan Galiev (Tatar: Мирсәет Хәйдәргали улы Солтангалиев, Mirsäyet Xäydärğäli ulı Soltanğäliev, pronounced [ˌmirsæˈjet xæɪˌdærɣæˈli ulɯ sɔlˌtɑnɣæˈliəf]; Russian: Мирсаид Хайдаргалиевич Султан-Галиев Mirsaid Khaydargalievich Sultan-Galiev; 1892–1940), usually known in English as Mirza Sultan-Galiev, was a Tatar Bolshevik who rose to prominence in the Russian Communist Party in the early 1920s. Seen as the architect of Muslim 'national communism', he was later purged from the party.
 Sultan Galiev was the son of a teacher, born in the village of Elembet'evo, Ufa Guberniya, Bashkiria, then part of the Russian Empire on 13 July 1892.[1] At base, he had a difficult and impoverished childhood: first, as a school teacher, his father made very little money (not nearly enough to support his wife and 12 children) and was frequently transferred from place to place; second, there was considerable, lasting tension between his parents, because they came from very different layers of Tatar society. Sultan Galiev later wrote, "My mother was the daughter of a prince – a noblewoman, while my father was a simple "Mishar," and this quite often stung the eyes of my father."[2]
 Though his parents could not afford to send him to a private school, Sultan Galiev was able to learn a great deal from his father and at the latter's maktab, which followed the "New Method" of maktab teaching founded by Ismail Gasprinski (1851–1914). From a young age Sultan Galiev studied the Russian language and read many of the Russian classics from his father's library. At his father's school, he studied from age 8 to 15, learning Tatar and Arabic, history, geography, and mathematics, while also receiving a basic understanding of the Qur'an and Sharia. All this, especially his knowledge of Russian, greatly helped him to gain entrance to the Kazan Teachers College (see Tatar State University of Humanities and Education) in 1907.[3]
 Sultan Galiev was first drawn to revolutionary ideas during the abortive 1905 revolution. Following the revolution's defeat he moved to Baku, where he came to the attention of Nariman Narimanov. He was further drawn to revolutionary ideas while studying to become a teacher at the Tatar Teachers College in Kazan.[4] At this time, he also received his first lessons in socialism. The future bolshevik A. Nasybullin and the future Basmachi (see Basmachi Revolt) A. Ishmurzin gave him books on the theory of socialism and conversed with him about the books.[5]
 Graduating from the Teacher's College in 1911, Sultan Galiev began his career as a "half-starved village school teacher and librarian." In 1912 he also started to publish articles in various newspapers in Russian and Tatar, initially under various pseudonyms, such as "Sukhoi [Dry one]," Syn naroda [Son of the People]," "Uchitel'-tatarin [Teacher-Tatar]," "Karamas-kalinets," and then from 1914 under his own name. Over the same period, he also "secretly distributed anti-government proclamations in the Muslim villages of Ufa province and spoke out against the installation of Russian or Christianized Tatar teachers in Muslim schools.[6]
 As with most people of his generation, World War I played a large role in his personal transformation. With the war's outbreak, Sultan Galiev and his wife Rauza Chanysheva moved to Baku, where Sultan Galiev began to write for a variety of newspapers. He seems to have absorbed amongst the city's diverse population of Azerbaijanis, Armenians, Georgians, Russians, Tatars, and Iranians, a deep and growing dissatisfaction with the tsarist autocracy, its resistance to reform, and handling of the war effort. Baku's political climate in combination with the 1916 anti-conscription uprising of Muslims in Central Asia led him to break with the reform-minded Jadidism of his youth and move towards revolutionary socialism.[5]
 In May 1917 Sultan Galiev participated in the All-Russian Muslim Conference in Moscow and was elected to the All-Russia Muslim Council created by it. In July that year he went to Kazan, where he met Mullanur Waxitov, with whom he helped set up the Muslim Socialist Committee, with a program close to that of the Bolsheviks. In November 1917 he joined the Bolshevik faction of the Russian Social Democratic Labour Party. Following the establishment of Narkomnats in June 1917, Sultan Galiev was asked to become head of the Muslim section. In January 1918 the Central Commissariat of Muslim affairs in Inner Russia and Siberia (Muskom), was set up under the chairmanship of Waxitov, with Sultan Galiev as representative of the Russian Communist Party. He was appointed the chair of the Central Muslim Military Collegium when it was established in June 1918. He wrote for Zhizn' Natsional'nostei (Life of the Nationalities). Mustafa Suphi acted as his secretary.[7]
 He was an avid reader of the Russian Literature. He translated works by Tolstoy and Pushkin into the Tatar language. In 1913 he married Rauza Chanysheva, who became a leading figure in the women's movement. They separated after
personal problems in 1918.
 In December 1917, in response to some Tatars' accusations that he was betraying his own people to the Bolsheviks, Sultan Galiev wrote a revealing explanation for his decision to join the Bolsheviks:
 I now move to my cooperation with the Bolsheviks. I will say the following: I associate with them not from sycophancy. The love for my people, which lies inherently inside me, draws me to them. I go to them not with a goal to betray our nation, not in order to drink its blood. No! No! I go there because with my whole spirit I believe in the rightness of the Bolsheviks’ cause. I know this; it is my conviction. Thus, nothing will remove it from my soul. I realize that only some of the bolsheviks were able to implement what was promised at the beginning of the revolution. [But] only they stopped the war. Only they are striving to pass the nationalities’ fates into their own hands. Only they revealed who started the world war. What does not lead me to them? They also declared war on English imperialism, which oppresses India, Egypt, Afghanistan, Persia and Arabia. They are also the ones who raised arms against French imperialism, which enslaves Morocco, Algiers, and other Arab states of Africa. How could I not go to them? You see, they proclaimed the words, which have never been voiced since creation of the world in the history of the Russian state. Appealing to all muslims of Russia and the East, they announced that Istanbul must be in Muslims’ hands. They did this while English troops, seizing Jerusalem, appealed to Jews with the words: ‘Gather together quickly in Palestine, we will create for you a European state.’[8]
 During the Civil War he was active in organising the defence of Kazan against the Whites in August 1918 and liquidating opposition after they had been driven out. He was also instrumental in ensuring that the Bashkir people, led by Zeki Velidi, joined the Bolshevik side which weakened the military potential of Kolchak's army. His knowledge of national movements in the East won him the trust of Stalin and other highly placed Party and government figures. Sultan-Galiev carried out many tasks on the personal orders of Stalin. In April 1919 he again was rushed to the eastern front to help shore up the moral of the Tatar 21st division at Malmysh after Kolchak's Spring offensive had forced the Red Army to abandon Ishevsk to the Whites. In June 1919 he was sent to Kazan at request of the local Bolshevik administration to help resolve the national question among the Tatars, but he was soon recalled to Moscow by Lenin to work on the nationality issue in the Narkomnats until 1922.[9]
 Mirsaid wanted to give Marxism an Islamic face. He argued that Tsarist Russians had oppressed Muslim society apart from a few big landowners and bourgeois. He was thought of by the Bolsheviks as being excessively tolerant of nationalism and religion[10], and in 1923 he was accused of nationalist, pan-Islamic and pan-Turkic deviations and he was arrested and expelled from the party. He was freed, but with Lenin's death in 1924, he lost his only protector, and remained a political outcast, constantly watched by state security. In these years he spent his time travelling for the Hunting Union and writing occasional reviews and translations. He was accompanied by his second wife Fatima Yerzina, whom he had married in 1918, and their two children. In 1928 he was arrested a second time and sentenced to be shot in July 1930. However, in January 1931 his sentence was commuted to ten years of hard labour for nationalism and anti-Soviet activity. In 1934 he was released and given permission to live in the Saratov Oblast. At the beginning of 1937 he was again arrested, and was forced to make a confession. In December 1939 he received the death sentence which was carried out on 28 January 1940 in Moscow.
 Stalin was not sympathetic to Mirsaid's attempt to synthesise Islam, nationalism and communism for a revolution in the East in general and among Muslim areas there in particular. Stalin, therefore, had Sultan Galiev imprisoned and later executed (in Moscow on 28 January 1940) for being an independent ‘Muslim’ leader.[11] He was killed by the NKVD on the morning of 28 January 1940 at Lefort Prisonreference required by Stalin's orderreference required.
 1 Biography 2 See also 3 References 4 External links National communism ↑ In a very long, autobiographical letter written shortly after his arrest (around 23 May 1923), Sultan Galiev wrote, "I was born in Bashkiria in the Bashkir village of Shipaevo (in Russian it is called, I think, Belembeevo, Sterlitamakskii canton)." Mirsaid Sultan-Galiev: stati, vystupleniia, dokumenty, comp. by I.G. Gizzatullin, D.R. Sharafutdinov (Kazan:Tatarskoe knizhnoe izd-vo, 1992), p. 386.
 ↑ R. G. Landa, “Mirsaid Sultan-Galiev,” Voprosy Istoriia KPSS 1999 (8): 56. Mishar (Mişär) Tatars are an ethnic sub-group of the Volga Tatars, speaking a Western dialect of the Tatar language, originating from Mordovia and living in Bashkiria since the late middle ages; for more see Tatars, and scroll down to Mişär Tatars.
 ↑ Landa, "Mirsaid Sultan-Galiev," pp. 55-56.
 ↑ I.R. Tagirov (ed.), Neizvestnyi Sultan-Galiev: Rassekrechennye dokumenty i materialy (Kazan': Tatarskoe knyzhnoe izdatel'stvo, 2002), p. 11.
 ↑ 5.0 5.1 Landa, "Mirsaid Sultan-Galiev," p. 58.
 ↑ Landa, "Mirsaid Sultan-Galiev," p. 57.
 ↑ Left Wing of the Turkish Communist Party by Enternasyonalist Komunist Sol, October 2008
 ↑ I. G. Gizzatullin, D. R. Sharafutdinov (compilers), Mirsaid Sultan-Galiev. Stat’I, vystupleniia, dokumenty (Kazan’: Tatarskoe knizhskoe izdatel’stvo, 1992), p. 52. Sultan Galiev's letter was published on 19 December 1917 in the newspaper Koyash, the same paper that had published the attack on Sultan Galiev's sympathies two days earlier, a clear sign of the openness of political debate in Kazan at that time.
 ↑ Mirsaid Sultan-Galiev: His Character and Fate, Sh. F. Mukhamedyarov and B. F. Sultanbekov, Central Asian Survey, Vol. 9, No. 2, pp. 109-117, 1990 Society for Central Asian Studies.
 ↑ Stalin, a biography by Robert Service, page 154
 ↑ I.R. Tagirov (ed.), Neizvestnyi Sultan-Galiev: Rassekrechennye dokumenty i materialy (Kazan': Tatarskoe knyzhnoe izdatel'stvo, 2002), doc. 112, p. 384. Document 110 is the actual judgement, in which Sultan-Galiev is convicted of being the "organizer and factual leader of an anti-Soviet nationalistic group," who led an "active struggle against soviet power" and the party "on the basis of pan-Turkism and pan-Islamism, with the goal of tearing away from Soviet Russia Turkic-Tatar regions and establishing in them a bourgeois-democratic Turan state" (pp. 382-383).
 Sultan-Galiyev Mirsait Sultan Galiev - a Forgotten Precursor The Case of Sultan-Galiyev by the Marxist-Leninist Research Bureau, Report #3, 1995. Mykola Bakay Anna Barkova Vasile Bătrânac Yuri Bezmenov Larisa Bogoraz Yelena Bonner Leonid Borodin Alexei Borovoi Vladimir Bougrine Joseph Brodsky Vladimir Bukovsky Valery Chalidze Lev Chernyi Viacheslav Chornovil Lydia Chukovskaya Yuli Daniel Vadim Delaunay David Devdariani Mihai Dolgan Ivan Drach Alexander Esenin-Volpin Eliyahu Essas Benjamin Fain Viktor Fainberg Moysey Fishbein Ilya Gabay Yuri Galanskov Alexander Galich Zviad Gamsakhurdia Alexander Ginzburg Yevgenia Ginzburg Semyon Gluzman Natalya Gorbanevskaya Pyotr Grigorenko Vasily Grossman Paruyr Hayrikyan Oleksa Hirnyk Mykola Horbal Grigory Isayev Boris Kagarlitsky Romas Kalanta Sofiya Kalistratova Ihor Kalynets Vitaliy Kalynychenko Dina Kaminskaya Ivan Kandyba Ephraim Kholmyansky Yuliy Kim Lev Kopelev Boris Korczak Anatoly Koryagin Merab Kostava Lina Kostenko Sergei Kovalev Zoya Krakhmalnikova Jüri Kukk Eduard Kuznetsov Anatoly Kuznetsov Yaroslav Lesiv Eduard Limonov Pavel Litvinov Levko Lukyanenko Kronid Lyubarsky Michail J. Makarenko Guram Mamulia Nadezhda Mandelstam Osip Mandelstam Anatoly Marchenko Valeriy Marchenko Myroslav Marynovych Roy Medvedev Zhores Medvedev Naum Meiman Yosef Mendelevitch Ion Moraru Viktor Muravin Mustafa Abdülcemil Qırımoğlu Lev Navrozov Viktor Nekipelov Alexander Nekrich Valeria Novodvorskaya Vasile Odobescu Yuri Orlov Boris Pasternak Yekaterina Peshkova Leonid Plyushch Alexandr Podrabinek Grigory Pomerants Irina Ratushinskaya Maria Rozanova Mykola Rudenko Ain Saar Andrei Sakharov Dmitri Savitski Shmuel Schneurson Efraim Sevela Igor Shafarevich Varlam Shalamov Vladimir Shelkov Danylo Shumuk Andrei Sinyavsky Victor Sokolov Sergei Soldatov Aleksandr Solzhenitsyn Vladimir Strelnikov Vasyl Stus Sultan Galiev Nadiya Svitlychna Vasyl Symonenko Les Tanyuk Alexander Tarasov Valery Tarsis Enn Tarto Georgi Vins Georgi Vladimov Vladimir Voinovich Michael Voslenski Gleb Yakunin Yevgeny Zamyatin Aleksandr Zinovyev Yosyf Zisels Articles in need of neutralization 1892 births 1940 deaths People from Bashkortostan Bolsheviks Executed Soviet people Executed politicians Great Purge victims Members of the Communist Party of the Soviet Union executed by the Soviet Union Tatar people executed by the Soviet Union Russian Social Democratic Labour Party members Russian Marxists Russian Muslims Russian Tatar people Soviet dissidents Soviet politicians Soviet rehabilitations Tatar revolutionaries Tatar writers Translators from Russian Translators to Tatar Turanism Articles containing non-English language text Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 2 April 2012, at 15:48. Privacy policy About Metapedia Disclaimers 
  
I now move to my cooperation with the Bolsheviks. I will say the following: I associate with them not from sycophancy. The love for my people, which lies inherently inside me, draws me to them. I go to them not with a goal to betray our nation, not in order to drink its blood. No! No! I go there because with my whole spirit I believe in the rightness of the Bolsheviks’ cause. I know this; it is my conviction. Thus, nothing will remove it from my soul. I realize that only some of the bolsheviks were able to implement what was promised at the beginning of the revolution. [But] only they stopped the war. Only they are striving to pass the nationalities’ fates into their own hands. Only they revealed who started the world war. What does not lead me to them? They also declared war on English imperialism, which oppresses India, Egypt, Afghanistan, Persia and Arabia. They are also the ones who raised arms against French imperialism, which enslaves Morocco, Algiers, and other Arab states of Africa. How could I not go to them? You see, they proclaimed the words, which have never been voiced since creation of the world in the history of the Russian state. Appealing to all muslims of Russia and the East, they announced that Istanbul must be in Muslims’ hands. They did this while English troops, seizing Jerusalem, appealed to Jews with the words: ‘Gather together quickly in Palestine, we will create for you a European state.’[8]

 Sultan Galiev Mirza Sultan-Galiev v • d • e
 Soviet dissidents* Andrei Amalrik
Mykola Bakay
Anna Barkova
Vasile Bătrânac
Yuri Bezmenov
Larisa Bogoraz
Yelena Bonner
Leonid Borodin
Alexei Borovoi
Vladimir Bougrine
Joseph Brodsky
Vladimir Bukovsky
Valery Chalidze
Lev Chernyi
Viacheslav Chornovil
Lydia Chukovskaya
Yuli Daniel
Vadim Delaunay
David Devdariani
Mihai Dolgan
Ivan Drach
Alexander Esenin-Volpin
Eliyahu Essas
Benjamin Fain
Viktor Fainberg
Moysey Fishbein
Ilya Gabay
Yuri Galanskov
Alexander Galich
Zviad Gamsakhurdia
Alexander Ginzburg
Yevgenia Ginzburg
Semyon Gluzman
Natalya Gorbanevskaya
Pyotr Grigorenko
Vasily Grossman
Paruyr Hayrikyan
Oleksa Hirnyk
Mykola Horbal
Grigory Isayev
Boris Kagarlitsky
Romas Kalanta
Sofiya Kalistratova
Ihor Kalynets
Vitaliy Kalynychenko
Dina Kaminskaya
Ivan Kandyba
Ephraim Kholmyansky
Yuliy Kim
Lev Kopelev
Boris Korczak
Anatoly Koryagin
Merab Kostava
Lina Kostenko
Sergei Kovalev
Zoya Krakhmalnikova
Jüri Kukk
Eduard Kuznetsov
Anatoly Kuznetsov
Yaroslav Lesiv
Eduard Limonov
Pavel Litvinov
Levko Lukyanenko
Kronid Lyubarsky
Michail J. Makarenko
Guram Mamulia
Nadezhda Mandelstam
Osip Mandelstam
Anatoly Marchenko
Valeriy Marchenko
Myroslav Marynovych
Roy Medvedev
Zhores Medvedev
Naum Meiman
Yosef Mendelevitch
Ion Moraru
Viktor Muravin
Mustafa Abdülcemil Qırımoğlu
Lev Navrozov
Viktor Nekipelov
Alexander Nekrich
Valeria Novodvorskaya
Vasile Odobescu
Yuri Orlov
Boris Pasternak
Yekaterina Peshkova
Leonid Plyushch
Alexandr Podrabinek
Grigory Pomerants
Irina Ratushinskaya
Maria Rozanova
Mykola Rudenko
Ain Saar
Andrei Sakharov
Dmitri Savitski
Shmuel Schneurson
Efraim Sevela
Igor Shafarevich
Varlam Shalamov
Vladimir Shelkov
Danylo Shumuk
Andrei Sinyavsky
Victor Sokolov
Sergei Soldatov
Aleksandr Solzhenitsyn
Vladimir Strelnikov
Vasyl Stus
Sultan Galiev
Nadiya Svitlychna
Vasyl Symonenko
Les Tanyuk
Alexander Tarasov
Valery Tarsis
Enn Tarto
Georgi Vins
Georgi Vladimov
Vladimir Voinovich
Michael Voslenski
Gleb Yakunin
Yevgeny Zamyatin
Aleksandr Zinovyev
Yosyf Zisels  * Andrei Amalrik
Mykola Bakay
Anna Barkova
Vasile Bătrânac
Yuri Bezmenov
Larisa Bogoraz
Yelena Bonner
Leonid Borodin
Alexei Borovoi
Vladimir Bougrine
Joseph Brodsky
Vladimir Bukovsky
Valery Chalidze
Lev Chernyi
Viacheslav Chornovil
Lydia Chukovskaya
Yuli Daniel
Vadim Delaunay
David Devdariani
Mihai Dolgan
Ivan Drach
Alexander Esenin-Volpin
Eliyahu Essas
Benjamin Fain
Viktor Fainberg
Moysey Fishbein
Ilya Gabay
Yuri Galanskov
Alexander Galich
Zviad Gamsakhurdia
Alexander Ginzburg
Yevgenia Ginzburg
Semyon Gluzman
Natalya Gorbanevskaya
Pyotr Grigorenko
Vasily Grossman
Paruyr Hayrikyan
Oleksa Hirnyk
Mykola Horbal
Grigory Isayev
Boris Kagarlitsky
Romas Kalanta
Sofiya Kalistratova
Ihor Kalynets
Vitaliy Kalynychenko
Dina Kaminskaya
Ivan Kandyba
Ephraim Kholmyansky
Yuliy Kim
Lev Kopelev
Boris Korczak
Anatoly Koryagin
Merab Kostava
Lina Kostenko
Sergei Kovalev
Zoya Krakhmalnikova
Jüri Kukk
Eduard Kuznetsov
Anatoly Kuznetsov
Yaroslav Lesiv
Eduard Limonov
Pavel Litvinov
Levko Lukyanenko
Kronid Lyubarsky
Michail J. Makarenko
Guram Mamulia
Nadezhda Mandelstam
Osip Mandelstam
Anatoly Marchenko
Valeriy Marchenko
Myroslav Marynovych
Roy Medvedev
Zhores Medvedev
Naum Meiman
Yosef Mendelevitch
Ion Moraru
Viktor Muravin
Mustafa Abdülcemil Qırımoğlu
Lev Navrozov
Viktor Nekipelov
Alexander Nekrich
Valeria Novodvorskaya
Vasile Odobescu
Yuri Orlov
Boris Pasternak
Yekaterina Peshkova
Leonid Plyushch
Alexandr Podrabinek
Grigory Pomerants
Irina Ratushinskaya
Maria Rozanova
Mykola Rudenko
Ain Saar
Andrei Sakharov
Dmitri Savitski
Shmuel Schneurson
Efraim Sevela
Igor Shafarevich
Varlam Shalamov
Vladimir Shelkov
Danylo Shumuk
Andrei Sinyavsky
Victor Sokolov
Sergei Soldatov
Aleksandr Solzhenitsyn
Vladimir Strelnikov
Vasyl Stus
Sultan Galiev
Nadiya Svitlychna
Vasyl Symonenko
Les Tanyuk
Alexander Tarasov
Valery Tarsis
Enn Tarto
Georgi Vins
Georgi Vladimov
Vladimir Voinovich
Michael Voslenski
Gleb Yakunin
Yevgeny Zamyatin
Aleksandr Zinovyev
Yosyf Zisels  NAME
 Sultangaliev, Mirsaid
 ALTERNATIVE NAMES
 
 SHORT DESCRIPTION
 
 DATE OF BIRTH
 1892
 PLACE OF BIRTH
 
 DATE OF DEATH
 1940
 PLACE OF DEATH
 
 Sultan Galiev Contents Biography See also References External links Navigation menu Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 