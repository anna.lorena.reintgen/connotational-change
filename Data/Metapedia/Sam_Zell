Samuel "Sam" Zell (born September 1941) is a U.S.-born billionaire and real estate entrepreneur. He is co-founder and Chairman of Equity Group Investments, a private investment firm. With an estimated net worth of US$5 billion, he is ranked as the 68th richest American by Forbes.[1] In April 2007, Zell completed a leveraged buyout of the Tribune Company, publisher of the Chicago Tribune and the Los Angeles Times. He is also the owner of the Chicago Cubs.
 Zell was born in Chicago in 1941 to Jewish immigrant parents from Poland who fled the country just before the German invasion in 1939. Shortly after moving from Seattle to Chicago, Zell's father Bernard changed the family name from Zielonka to Zell.[2] He received his BA (1963) from the University of Michigan, where he was a member of the Alpha Epsilon Pi fraternity. He also received his JD (1966) from the University of Michigan Law School.[3]
 Zell, with Robert H. Lurie went on to found the Equity Group Investments, LLC, which spawned three real estate public companies, including: Equity Residential, the largest apartment owner in the United States; Equity Office Properties, the largest office owner in the country; and Manufactured Home Communities, a mobile home company. In addition, Zell has created a number of public and private companies. He also controls SZ Investments LLC as his investment arm.
 Zell is also Chairman of Capital Trust Inc., a finance and investment management company focused on the commercial real estate industry, and Anixter International, the world's largest distributor of communication products and electrical and electronic wire and cable.
 In 2007 the Blackstone Group completed its purchase of Zell's Equity Office Properties Trust for $39 billion,[4]
and sold off many of the portfolio's properties for record amounts.[5] By early 2009 most of the properties sold were "underwater" (worth less than the mortgage).[1]
 Between 1992 and 1999, Zell's Chillmark fund owned Jacor Communications, Inc., a successful radio broadcast group that included a television station. The company was sold to Clear Channel Communications in 1999.
 On April 2, 2007, the Tribune Company announced their acceptance of Zell's offer to buy the Chicago Tribune, the Los Angeles Times, and other media assets. On December 20, 2007, Zell took the company private, and the following day he became the Chairman and CEO. He plans to sell the Chicago Cubs, and sell the company's 25 percent interest in Comcast SportsNet Chicago.
 In a sharply critical June 2008 opinion piece for The Washington Post entitled, "The L.A. Times' Human Wrecking Ball", veteran Los Angeles-based editor and columnist Harold Meyerson took Zell to task for "taking bean counting to a whole new level", asserting that "he's well on his way to... destroying the L.A. Times." Comparing Zell to James McNamara, who was sentenced to life in prison for the notorious 1910 Los Angeles Times bombing (which killed 21 employees), Meyerson concluded his article by opining that "Life in San Quentin sounds about right" for Zell.[6]
 Zell is known for using "salty" language in the newsroom.[7] In February 2008, the website LA Observed reprinted an internal memo that said:
 "Last week you may have encountered some colorful uses of the lexicon from Sam Zell that we are not used to hearing at the Times... But of course we still have the same expectations at the Times of what is correct in the workplace. It's not good judgment to use profane or hostile language and we can't tolerate that... In short, nothing changes; the fundamental rules of decorum and decency apply... Sam is a force of a nature; the rest of us are bound by the normal conventions of society."[8]
 A long-time supporter of the Wharton School of the University of Pennsylvania, he helped fund the Real Estate Department at Wharton, as well as the Zell-Lurie Institute at the Ross School of Business at University of Michigan. Zell also endowed the Zell Center for Risk Research at the Kellogg School of Management at Northwestern University, and the Samuel Zell and Robert Lurie Real Estate Center at the Wharton School. Zell has also donated significantly to his alma mater, the University of Michigan.
 Zell, according to The Forward[9], is also "a major donor to causes in the Middle East. His donations include a $3.1 million donation to the Herzliya Interdisciplinary Center in Israel and separate donations to the Israel Center for Social and Economic Progress, a free market oriented Israeli think tank founded by Daniel Doron. In the United States, he has given major gifts to such Jewish causes as the American Jewish Committee and a Chicago Jewish day school named after his father."
 Zell has donated to both Republican and Democratic candidates — with more money going to the former — as well as to lobbying groups representing the real estate industry. According to an analysis of Federal Election Commission records by the Center for Public Integrity, "Zell has given more than $100,000 in political contributions since the 1998 election cycle, most of it supporting Republican causes."[10]
 Zell was recently implicated in the complaint against Illinois Governor Rod Blagojevich. According to the Associated Press, Zell "got the message and is very sensitive to the issue." The issue in question was the firing of certain editorial staff members in exchange for tax breaks on the sale of Wrigley Field.
[11] Zell has yet to state that he would not have fired the editor mentioned in the federal complaint in exchange for the tax break in question.
 In 2008, Zell announced a plan to place the Chicago Cubs and Wrigley Field up for sale separately in order to maximize profits. He also announced he would consider selling naming rights to Wrigley Field for anyone willing to put up the money. These announcements have been widely unpopular in Chicago[12][13] and a poll taken by the Chicago Sun-Times showed that 53% of 2,000 people who voted said they would no longer attend Cubs games if the field was renamed.[14]
 In April 2008, Zell made a controversial comment about the subprime mortgage crisis at a conference in Los Angeles, where he stated, "This country needs a cleansing. We need to clean out all those people who never should have bought in the first place, and not give them sympathy.” [15]
 In June 2008, Politicker.com editorial cartoonist Rob Tornoe took Sam Zell to task for the changes he has brought to both the Los Angeles Times and the Tribune Company.[16]
 1 Biography

1.1 Early life
1.2 Real estate business
1.3 Media investments

1.3.1 Los Angeles Times


1.4 Philanthropy
1.5 Political contributions
1.6 Controversies

 1.1 Early life 1.2 Real estate business 1.3 Media investments

1.3.1 Los Angeles Times

 1.3.1 Los Angeles Times 1.4 Philanthropy 1.5 Political contributions 1.6 Controversies 2 References 3 External links ↑ The 400 Richest Americans. Forbes (2008-09-16). Retrieved on 2008-09-17.
 ↑ Katharine Q. Seelye, Terry Pristin (2007-03-25). Sam Zell, the 'grave dancer,' sees profit in newspapers. International Herald Tribune. Archived from the original on 2007-03-28. Retrieved on 2007-04-03.
 ↑ Billionaire Investor Sam Zell at Glance. Associated Press (2006-09-21). Retrieved on 2007-04-04.
 ↑ Wilson, David (2007-02-08). Blackstone's Costly Buyout, Navistar's Listing. Bloomberg. Retrieved on 2007-04-05.
 ↑ The Equity Office Triple Flips. Wall Street Journal.
 ↑ Harold Meyerson, "The L.A. Times's Human Wrecking Ball", Washington Post, June 11, 2008
 ↑ Los Angeles Times staffers warned about behaving like Zell
 ↑ Let Sam be Sam, but you be nice
 ↑ Nathaniel Popper, "Billionaire Boychiks Battle for Media Empire: ‘Committed Zionist’ To Buy Papers With Troubled Ties to Community", The Forward, April 13, 2007
 ↑ McGarry, Brendan; Welsh, Ben (April 4, 2007). "In Political Contribution, Zell Leans Right and Wife Leans Left" (in English). Center for Public Integrity. http://www.publicintegrity.org/telecom/report.aspx?aid=822. 
 ↑ {url=http://www.google.com/hostednews/ap/article/ALeqM5iZ7EJA2XWcKb4J9a-3-BoszplwKwD94VIEF81}
 ↑ "Cubs Fans Consider a Wrigley by Any Other Name" (in English). The Wall Street Journal. February 29, 2008. http://blogs.wsj.com/dailyfix/2008/02/29/cubs-fans-consider-a-wrigley-by-any-other-name/. 
 ↑ Wojciechowski, Gene (March 2, 2008). "Cubs' new owner should think again about renaming Wrigley" (in English). ESPN.com. http://sports.espn.go.com/espn/columns/story?columnist=wojciechowski_gene&id=3270817&sportCat=mlb. 
 ↑ Dodd, Mike (March 1, 2008). "For Cubs fans, renaming Wrigley is dealbreaker" (in English). USA Today. http://www.usatoday.com/sports/baseball/nl/cubs/2008-02-27-wrigley-naming-rights_N.htm. 
 ↑ {url=http://www.bloomberg.com/apps/news?pid=20601087&sid=acare3gwtAdI&refer=home}
 ↑ Sam Zell leaves his mark on the Los Angeles Times | PolitickerCA
 Sam Zell Biography The Zell Center for Risk Research at the Kellogg School of Management The Zell Lurie Institute: The University of Michigan The Samuel Zell and Robert Lurie Real Estate Center at Wharton The Zell entrepreneurship program at the Interdisciplinary Center, in Israel Detailed list of Zell's federal campaign contributions since 1979 Zell's Year End Gift "What the Zell?" Chronicles one of the most-quotable men in journalism http://www.guardian.co.uk/business/2008/apr/30/useconomy.usa Content from Wikipedia 1941 births Living people Jews in the United States Jewish billionaires Zionists Jewish media executives Newspaper publishers Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 17 August 2014, at 13:47. Privacy policy About Metapedia Disclaimers 
  
"Last week you may have encountered some colorful uses of the lexicon from Sam Zell that we are not used to hearing at the Times... But of course we still have the same expectations at the Times of what is correct in the workplace. It's not good judgment to use profane or hostile language and we can't tolerate that... In short, nothing changes; the fundamental rules of decorum and decency apply... Sam is a force of a nature; the rest of us are bound by the normal conventions of society."[8]

 Samuel "Sam" Zell Sam Zell Contents Biography References External links Navigation menu Early life Real estate business Media investments Philanthropy Political contributions Controversies Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools Los Angeles Times 