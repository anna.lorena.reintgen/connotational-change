Nina Hagen (born 11 March 1955) is a German singer and actress.
 Hagen was born as Catharina Hagen in the former East Berlin, East Germany, the daughter of Hans Hagen (also known as Hans Oliva-Hagen), a scriptwriter, and Eva-Maria Hagen, an actress and singer. Her paternal grandfather died in the Sachsenhausen concentration camp (one of her great-grandparents was Jewish).[1] Her parents divorced when she was two years old, and growing up she saw her father infrequently. At age four, she began to study ballet, and was considered an opera prodigy by the time she was nine.
 When Hagen was 11, her mother married Wolf Biermann, an anti-establishment singer-songwriter. Biermann's political views later influenced young Hagen.
 Hagen left school at age sixteen, and went to Poland, where she began her career.[2][3] After that, she returned to Germany and joined the cover band Fritzens Dampferband (Fritzen's Steamboat Band, together with Achim Mentzel and others). She added songs by Janis Joplin and Tina Turner to the "allowable" set lists during shows.
 From 1972–73, Hagen enrolled in the crash-course performance program at The Central Studio for Light Music in East Berlin. Upon graduation, she formed the band Automobil.
 In East Germany, she performed with the band Automobil, becoming one of the country's best-known young stars. Her most famous song from the early part of her career was Du hast den Farbfilm vergessen (You forgot the colour film), " a subtle dig mocking the sterile, gray, Communist state,"[4] in 1974. Her musical career in the DDR was cut short, however, when she and her mother left the country in 1976, following the expulsion of her stepfather.
 The circumstances surrounding the family's emigration were exceptional: Biermann was granted permission to perform a televised concert in Cologne, but denied permission to re-cross the border to his home country. Hagen submitted an application to leave the country. In it, she claimed to be Biermann's biological daughter, and threatened to become the next Wolf Biermann if not allowed to rejoin her father. Just four days later her request was granted,[citation needed] and she settled in Hamburg, where she was signed to a CBS-affiliated record label. Her label advised her to acclimate herself to Western culture through travel, and she arrived in London during the height of the punk rock movement. Hagen was quickly taken up by a circle that included The Slits and the Sex Pistols; Johnny Rotten was a particular admirer.[citation needed]
 Back in Germany by mid-1977, Hagen formed the Nina Hagen Band in West Berlin's Kreuzberg district. In 1978 they released their self-titled debut album, which included the single "TV-Glotzer" (a cover of "White Punks on Dope" by The Tubes, though with entirely different German lyrics), and Auf'm Bahnhof Zoo, about West Berlin's then-notorious Berlin Zoologischer Garten station. The album also included a version of "Rangehn" ("Go For It"), a song she had previously recorded in East Germany, but with different music.
 
According to reviewer Fritz Rumler: … she thrusts herself into the music, aggressively, directly, furiously, roars in the most beautiful opera alto, then, through shrieks and squeals, precipitates into luminous soprano heights, she parodies, satirises, and howls on stage like a dervish. The album gained significant attention throughout Germany and abroad, both for its hard rock sound and for Hagen's theatrical vocals, far different from the straightforward singing of her East German recordings. However, relations between Hagen and the other band members deteriorated over the course of the subsequent European tour, and Hagen decided to leave the band in 1979, though she was still under contract to produce a second album. This LP, Unbehagen (which in German also means discomfort or unease), was eventually produced with the band recording their tracks in Berlin and Hagen recording the vocals in Los Angeles, California. It included the single "African Reggae" and a cover of Lene Lovich's "Lucky Number". The other band members sans Hagen, soon developed a successful independent musical career as Spliff.
 Meanwhile, Hagen's public persona was steadily creating media uproar. She became infamous for an appearance on an Austrian evening talk show called Club 2, on 9 August 1979, on the topic of youth culture, when she demonstrated (while clothed, but explicitly) various female masturbation positions and became embroiled in a heated argument with another panelist. The talk show host had to step down following this controversy.[5][6]
 She also acted with Dutch rocker Herman Brood and singer Lene Lovich in the 1979 film Cha Cha.
 A European tour with a new band in 1980 was cancelled, and Hagen turned to the United States. A limited-edition 10-inch EP was released on vinyl that summer in the U.S. Two songs from her first album Nina Hagen Band were on the A side, and two songs from her second album Unbehagen were on the B-side. All four songs were sung in German.
 In late 1980, Hagen discovered she was pregnant, broke up with the father-to-be Ferdinand Karmelk,[7] and moved to Los Angeles. Her daughter, Cosma Shiva Hagen, was born in Santa Monica on 17 May 1981. In 1982, Hagen released her first English-language album: NunSexMonkRock, a dissonant mix of punk, funk, reggae, and opera. She then went on a world tour with the No Problem Orchestra.
 In 1983, she released the album Angstlos and a minor European tour. By this time, Hagen's public appearances were becoming stranger and frequently included discussions of God, UFOs, her social and political beliefs, animal rights and vivisection, and claims of alien sightings. The English version of Angstlos, Fearless, generated two major club hits in America, "Zarah" (a cover of the Zarah Leander (#45 USA) song "Ich weiss, es wird einmal ein Wunder geschehen") and the disco/punk/opera song, "New York New York" (#9 USA). During 1984 Hagen spent a lot of time in London and UK based MusicSzene magazine chief-editor Wilfried Rimensberger, in conjunction with Spree Film, produced a first TV feature on her and what was remaining from London's 70 Punk movement induced by artist and model Frankie Stein.
 Her 1985 album Nina Hagen In Ekstasy fared less well, but did generate club hits with "Universal Radio" (#39 USA) and a cover of "Spirit In The Sky" and also featured a 1979 recording of her hardcore punk take on Frank Sinatra's My Way, which had been one of her signature live tunes in previous years. She performed songs from this album during the 1985 version of Rock in Rio. Wilfried Rimensberger and award-winning film director Lothar Spree produced a TV documentary for German Television ZDF. This was followed by a launch of Nina's UFO fashion underwear at anti-SAFT in Zurich, where again Rimensberger joined her up with New Romantic icon Steve Strange performing on stage. Simultaneously fashion photographer Hannes Schmid produced a Nina Hagen cover for German Cosmopolitan magazine. This also coincided with leading music publications like BRAVO and MusicSzene running cover stories that all put Hagen back on the forefront of something that in retrospective became a final highpoint of what the punk movement was all about. At the end of 1986, her contract with CBS was over and she released the Punk Wedding EP independently in 1987, a celebration of her marriage to a 17-year-old-punk nicknamed 'Iroquois'. It followed an independent 1986 one-off single with Lene Lovich, the anthemic Don't Kill The Animals. In 1989, Hagen released the album Nina Hagen which was backed up by another German tour.
 In 1989 she had a relationship with Frank Chevallier from France, with whom she has a son, Otis Chevallier-Hagen.
 In the 1990s, Hagen lived in Paris with her daughter Cosma Shiva and son Otis. In 1991 she toured Europe in support of her new album Street. In 1992 Hagen became the host of a TV show on RTLplus. Also in the same year (1992) she collaborated with Adamski on the European smash and minor UK hit single "Get Your Body". The following year, she released Revolution Ballroom. In 1994 Laramy Smith recorded Nina at Ferber Studios in Paris, for the "Somebody Help Me" project, a song to aid the homeless worldwide available at laramysmith.com. In 1994, Nina starred in the acclaimed San Francisco Goethe Institut's "The Seven Addictions and Five Professions of Anita Berber", playing the singer version of "Anita" alongside dancer Jennifer Pieren who portrayed the other "professions" of "Anita". Also, her voice was heard on the Freaky Fukin Weirdoz single "Hit Me With Your Rhythm Stick". 1995 brought the German-language album Freud Euch appeared, recorded in English as BeeHappy in 1996. Nina returned to San Francisco to star in another San Francisco Goethe Institut show, "Hannusen, Hitler's Jewish Clarvoyant." Hagen also collaborated with electronic music composer Christopher Franke, along with Rick Palombi (credited as Rick Jude) on "Alchemy of Love", the theme song for the film Tenchi Muyo! in Love. In May 1996, she married David Lynn, who is fifteen years younger, but divorced him in the beginning of 2000. In 1997 she collaborated with German hip hop musician Thomas D.
 In 1998, Hagen became the host of a weekly science fiction show on the British Sci-Fi Channel, in addition to embarking on another tour of Germany. In 1999, she released the devotional album Om Namah Shivay, which was distributed exclusively online and included an unadulterated musical version of the Hare Krishna mantra. She also provided vocals to "Witness" and "Bereit" on KMFDM's Adios.
 Also in 1998 she recorded the official club anthem (Eisern Union !) for FC Union Berlin and four versions were issued on a CD single by G.I.B Music and Distribution GmbH.
 In 1999, she played the role of Celia Peachum in The Threepenny Opera by Kurt Weill and Bertolt Brecht, alongside Max Raabe. She also regularly performs songs by Kurt Weill, Hans Eisler and Paul Dessau set to Brecht's texts.
 In 2000, her song Schön ist die Welt became the official song of Expo 2000. Another cover of a Zarah Leander song "Der Wind hat mir ein Lied erzählt" was a minor hit the same year. The album The Return of the Mother was released in February 2001, accompanied by another German tour. In 2001 she collaborated with Rosenstolz and Marc Almond on the single Total eclipse/Die schwarze Witwe that reached #22 in Germany. On 14 October 2002 Nina hit Moscow by coming there with her concert, while interviews with the eccentric singer were aired on many TV-channels.
 Hagen dubbed the voice of Sally in the German release of Tim Burton's The Nightmare Before Christmas, and she has also done voice work on the movie Hot Dogs by Michael Schoemann. Hagen has been featured on songs by other bands, for instance on Oomph!'s song "Fieber". She did a cover of Rammstein's "Seemann" with Apocalyptica. Later albums include Big Band Explosion, in which she sang numerous swing covers with her then husband, Danish singer and performer, Lucas Alexander. This was followed by Heiß, a greatest hits album. Her most recent album, Journey to The Snow Queen, is more of an audio book—she reads the Snow Queen fairy tale with Tchaikovsky's The Nutcracker in the background. In 2005 Nina Hagen headlined the Drop Dead Festival in New York City. Hagen has been an active protester against the war in Iraq. In 2006 she was a part of the Popstars team. She is a vegetarian.[8] In August 2009 she was baptized in the Protestant Reformed church of Schüttorf.[9] On October 21 after seven years passed she visited Moscow[10] again. Her latest album, Personal Jesus was released July 16, 2010, after a four year lapse.
 See: Nina Hagen discography
 1 Early years 2 Music career

2.1 1970s
2.2 1980s
2.3 1990s
2.4 2000s

 2.1 1970s 2.2 1980s 2.3 1990s 2.4 2000s 3 Discography 4 Acting career

4.1 Filmography

 4.1 Filmography 5 Quotations 6 References 7 External links Both of my parents were atheists, and I found the way to God all alone on my own. You have to invite him, so that he shows up. [11] Asked if she was happy, she replied: Of course, I'm a family member of Christ and I have a Lord. He marched ahead of me and showed me the way.[11] ↑ Scally, Derek (2010-09-18). "'She has calmed down since her baptism'". The Irish Times. Archived from the original on 2013-01-26. https://archive.is/9j8Nt. Retrieved 2010-09-17. 
 ↑ Canal +, interview
 ↑ Concert in Gdańsk, August 8, 2009
 ↑ http://www.dangerousminds.net/comments/pre_punk_nina_hagen_in_east_germany_1974
 ↑ Nina Hagen Scandal on Club 2 1979 (Taken from RTL - "100 Prozent Nina Hagen")
 ↑ Nina Hagen Biography
 ↑ Nina Hagen Archiv
 ↑ Nina Hagen is a vegetarian — Famous Vegetarians — Vegan Celebrities — by HappyCow
 ↑ Focus Online, 2009-08-17. Sängerin wird in Schüttorf getauft. Accessed 2009-11-24. (German)
 ↑ Russian media about Nina in Moscow 
 ↑ 11.0 11.1 In an interview in the German newspaper Zeit Online, April 11, 2006.
 Nina Hagen Mother Of Punk (Russian-English) Seltene Live/Video Aufnahmen von Nina Hagen Nina Hagen discography Nina Hagen Electronic Shrine Trouser Press Nina Hagen Discography Nina Hagen - MySpace page Nina Hagen on Europopmusic.eu (English) Nina Hagen at the Internet Movie Database Nina Hagen: Interview (German) Nina Hagen vertreibt Wissenschaftler aus Talkshow Maischberger (German) [1] Drop Dead Magazine with issue 0 with Nina Hagen on the cover as well as a large interview inside 1979 Club 2 talk show with loose translation in More info Nina Hagen interviewed on Swedish TV during the 80s (English with Swedish subtitles) Nina Hagen 1955 births Living people People from East Berlin Jews Converts to Christianity Female punk rock singers Female New Wave singers Female rock singers Gothic rock musicians German-language singers German Calvinists German vegetarians German autobiographers German female singers German people of Jewish descent German punk rock musicians German New Wave musicians Articles with hCards Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 7 December 2015, at 17:18. Privacy policy About Metapedia Disclaimers 
  
… she thrusts herself into the music, aggressively, directly, furiously, roars in the most beautiful opera alto, then, through shrieks and squeals, precipitates into luminous soprano heights, she parodies, satirises, and howls on stage like a dervish. Nina Hagen Catharina Hagen Nina Hagen Band 1: 1: 1: 2: 3 4: 3: 4: 4: 5 6: 5 6: 6: 6: 7: 7: 2 8: 8: 1: 2: 3: 4: 5: 6: 7: 8: 9: 1: 2: 3: 4: 5: 6: 7: 8: 9: 3: Nina Hagen in 2010 Catharina Hagen 11 March 1955 (1955-03-11) (age 64) East Berlin, German Democratic Republic Punk rockPost-punkNew wave Gothic rockGlam rockNeue Deutsche WelleGospel Singer, Activist 1971–present Columbia Records  Mercury Records Automobil  The Nina Hagen Band  Apocalyptica nina-hagen.com 1976
 Liebesfallen
 Liane Brückner
 
 1976
 Unser stiller Mann
 Regina
 
 1979
  Bildnis einer Trinkerin
 Singer in a taxi drivers' bar
 
 1979
  Cha-Cha
 
 
 1983
 Pankow '95
 Jungfrau Maria
 
 1996
 Lilien in der Bank
 Suzanne
 
 2000
 Vasilisa
 The Witch
 
 2004
 7 Zwerge
 The Evil Queen
 Hagen's daughter Cosma played Snow White
 2006
 7 Zwerge - Der Wald ist nicht genug
 The Evil Queen
 Hagen's daughter Cosma played Snow White
 v • d • e
Popstars (Germany)Seasons1  · 2  · 3  · 4  · 5  · 6  · 7  · 8  · 9Current JudgesDetlef Soost • Marta Jandová • Thomas M. SteinFormer Judges1: Simone Angel • 1: Mario Mendryzcki • 1: Rainer Moslener • 2: Noah Sow • 3-4: Uwe Fahrenkrog-Petersen • 3: Sabrina Setlur • 4: Sandy Mölling • 4: Lukas Hilbert • 5-6: Nina Hagen • 5-6: Dieter Falk • 6: Marusha • 6: Jane Comerford • 7: Loona • 7: Sido • 2,8: Alex Christensen • 8: Michelle LeonardWinners1: No Angels • 2: Bro'Sis • 3: Overground • 4: Nu Pagadi • 5: Monrose • 6: Room 2012 • 7: Queensberry • 8: Some & Any • 9: LaViVe Winners' singles1: "Daylight in Your Eyes" • 2: "I Believe" • 3: "Schick mir 'nen Engel" • 4: "Sweetest Poison" • 5: "Shame" • 6: "Haunted" • 7: "I Can't Stop Feeling" • 8: "Last Man Standing" • 9: "No Time for Sleeping"Runner-ups3: PreludersFresh'n'Juicy · Curly · Yasmin K. · Senad · Johnny MC · Fabrizio Levita · Tryna · Milk & Honey · Bisou · Popstars Dance Company · Mehdi · Marcella McCrae · DiyanaRTL II (1-2) · ProSieben (3-9)  Seasons 1  · 2  · 3  · 4  · 5  · 6  · 7  · 8  · 9  Current Judges Detlef Soost • Marta Jandová • Thomas M. Stein  Former Judges 1: Simone Angel • 1: Mario Mendryzcki • 1: Rainer Moslener • 2: Noah Sow • 3-4: Uwe Fahrenkrog-Petersen • 3: Sabrina Setlur • 4: Sandy Mölling • 4: Lukas Hilbert • 5-6: Nina Hagen • 5-6: Dieter Falk • 6: Marusha • 6: Jane Comerford • 7: Loona • 7: Sido • 2,8: Alex Christensen • 8: Michelle Leonard  Winners 1: No Angels • 2: Bro'Sis • 3: Overground • 4: Nu Pagadi • 5: Monrose • 6: Room 2012 • 7: Queensberry • 8: Some & Any • 9: LaViVe   Winners' singles 1: "Daylight in Your Eyes" • 2: "I Believe" • 3: "Schick mir 'nen Engel" • 4: "Sweetest Poison" • 5: "Shame" • 6: "Haunted" • 7: "I Can't Stop Feeling" • 8: "Last Man Standing" • 9: "No Time for Sleeping"  Runner-ups 3: Preluders  Fresh'n'Juicy · Curly · Yasmin K. · Senad · Johnny MC · Fabrizio Levita · Tryna · Milk & Honey · Bisou · Popstars Dance Company · Mehdi · Marcella McCrae · DiyanaRTL II (1-2) · ProSieben (3-9) NAME
 Hagen, Nina
 ALTERNATIVE NAMES
 Hagen, Catharina
 SHORT DESCRIPTION
 Singer
 DATE OF BIRTH
 11 March 1955
 PLACE OF BIRTH
 East Berlin, Germany (now Berlin, Germany)
 DATE OF DEATH
 
 PLACE OF DEATH
 
 Nina Hagen Contents Early years Music career Discography Acting career Quotations References External links Navigation menu 1970s 1980s 1990s 2000s Filmography Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 