Ole Hanson (January 6, 1874 - July 6, 1940) was an American politician who served as mayor of Seattle, Washington from 1918 to 1919. Hanson became a national figure promoting law and order when he took a hardline position during the 1919 Seattle General Strike. He then resigned as mayor, wrote a book, and toured the lecture circuit, earning tens of thousands of dollars in honoraria lecturing to conservative civic groups about his experiences and views. Hanson later left Washington and founded the city of San Clemente, California in 1925.
 Ole Hanson was born in a log cabin in Union Grove in Racine County, Wisconsin, the son of Thorsten Hanson and Goro Tostofson Hanson.[1] He was the fifth of six children raised by the Norwegian immigrant couple.[2]
 As a teenager, the precocious Hanson worked as a tailor during the day and studied law at night.[2] He passed the Wisconsin bar in 1893, despite being two years too young to practice law.[2] In the end, Hanson never did work in the legal profession, instead going into the grocery business before moving west and going into real estate.[3]
 He worked as a real estate developer and co-founded Lake Forest Park, Washington in 1912 as a rural planned community for professionals in the Seattle area.
 Entering political life, he served in the Washington House of Representatives during 1908 and 1909.[1] In 1912 he supported Theodore Roosevelt for President of the United States.[4] In 1914, Hanson himself ran for the United States Senate as the candidate of the so-called Bull Moose Party.[1] Hanson garnered nearly a quarter of the vote in the five-way race, won by Republican incumbent Wesley Livsey Jones with a 37% plurality.
 In 1918, Hanson was elected the thirty-third mayor of Seattle.
 While in office, he became famous for claiming that he broke the Seattle General Strike of 1919, even though conservative national labor leaders pressured the Seattle unions into ending the general strike and Hanson's intervention mattered little. Nevertheless, he was hailed by some of the press as a defender of public order against labor radicals.
 In April 1919, anarchists made him one of the targets of booby-trap bombs mailed to approximately 30 prominent American officials. Hanson survived the assassination attempt.[4] He resigned as Mayor on August 28, 1919, saying: "I am tired out and am going fishing."[3]
 Following his resignation, Hanson set to work writing a book on what he perceived to be the radical menace to America, published in January 1920 in the immediate aftermath of the so-called "Palmer Raids" as Americanism versus Bolshevism. In this inflammatory tome, Hanson declared:[5]
 With syndicalism — and its youngest child, bolshevism — thrive murder, rape, pillage, arson, free love, poverty, want, starvation, filth, slavery, autocracy, suppression, sorrow and Hell on earth. It is a class government of the unable, the unfit, the untrained; of the scum, of the dregs, of the cruel, and of the failures. Freedom disappears, liberty emigrates, universal suffrage is abolished, progress ceases,...and a militant minority, great only in their self-conceit, reincarnate under the Dictatorship of the Proletariat a greater tyranny than ever existed under czar, emperor, or potentate.
 In Hanson's view, the fact that the 1919 Seattle general strike was peaceful belied its revolutionary nature and intent. He wrote:[6]
 Hanson toured the country giving lectures about the dangers of "domestic bolshevism." He earned $38,000 in 7 months, 5 times his annual salary as mayor.[7]
 In 1925, Hanson put some of his wealth to work by purchasing a 2000 acre tract at the southern tip of Orange County, California. Hanson believed that the area's pleasant climate, beautiful beaches and fertile soil would serve as a haven to Californians who were tired of urban life. He named the city San Clemente after neighboring San Clemente Island, southernmost of California's Channel Islands.
 Hanson envisioned his new project as a Spanish-style coastal resort town. He proclaimed, "I have a clean canvas and I am determined to paint a clean picture. Think of it — a canvas five miles long and one and one-half miles wide!"
 In an unprecedented move, Hanson had a clause added to all deeds requiring that building plans be submitted to an architectural review board in an effort to ensure that future development would retain Spanish Colonial Revival style influence. Red tile roofs became a stylistic signature of the new community. Hanson succeeded in promoting his new venture and selling property to interested buyers. The area was officially incorporated as a city on February 27, 1928.
 Over the years, Hanson built various public structures in San Clemente, including the Beach Club, the Community Center, the pier, and Max Berg Plaza Park, which were later donated to the city. He also had a Spanish Style home built overlooking the San Clemente Pier. This home was later named Casa Romantica.
 The Great Depression did not treat Hanson kindly. Financially leveraged with mortgages on his various property ventures, Hanson lost all his remaining holdings, including his beloved mansion in San Clemente.[2] Hanson eventually moved along to launch a new property development at Twentynine Palms in San Bernardino County, California.[2]
 Ole Hanson died of a heart attack on July 6, 1940.[2] He was 66 years old at the time of his death. Hanson was survived by his widow and ten children.[4]
 The city of San Clemente bears numerous reminders of its founding father, including the Ole Hanson historic pool which overlooks the Pacific Ocean. His home in the center of the city overlooking the historic pier was restored and opened to the community as a cultural center.
  
 1 Biography

1.1 Early years
1.2 Political career
1.3 Founding San Clemente
1.4 Death and legacy

 1.1 Early years 1.2 Political career 1.3 Founding San Clemente 1.4 Death and legacy 2 Footnotes 3 Works 4 Additional reading 5 External links ↑ 1.0 1.1 1.2 Lawrence Kestenbaum, "Ole Hanson," PoliticalGraveyard.com Retrieved September 4, 2010.
 ↑ 2.0 2.1 2.2 2.3 2.4 2.5 Don Kindred, The Legend of Ole Hanson (San Clemente Journal, SanClemente.com/)
 ↑ 3.0 3.1 Ole Hanson Quits as Seattle Mayor (New York Times August 29, 1919)
 ↑ 4.0 4.1 4.2 "Ole Hanson, Once Mayor of Seattle," New York Times, July 8, 1940.
 ↑ Ole Hanson, Americanism versus Bolshevism. New York: Doubleday, Page & Co., 1920; pp. vii-viii.
 ↑ Quoted in Jeremy Brecher, Strike!, 126
 ↑ Robert K. Murray, Red Scare: A Study in National Hysteria, 1919-1920. Minneapolis: University of Minnesota Press, 1955; pp. 65-66.
 Americanism vs. Bolshevism Garden City, New York: Doubleday, Page & Co., 1920. Jeremy Brecher, Strike! Revised edition. Boston: South End Press, 1997. Ann Hagedorn, Savage Peace: Hope and Fear in America, 1919. New York: Simon and Schuster, 2007. Terje I. Leiren, "Ole and the Reds: The 'Americanism' of Seattle Mayor Ole Hanson," Norwegian-American Studies, Volume 30, pg. 75. Robert K. Murray, Red Scare: A Study in National Hysteria, 1919-1920 Minneapolis: University of Minnesota Press, 1955. Cliffe, "Know Your Mayor: Ol' Ole Hanson," VintageSeattle.org, November 27, 2007. Retrieved September 4, 2010. Trevor Williams, Ole Hanson's Fifteen Minutes, Seattle General Strike Project, Harry Bridges Center for Labor Studies at the University of Washington, 1999. Jordan Stone Jordan Maddocks Collins Yesler Gatzert Weed B. Brown Jacobs L. Smith Struve Leary Yesler Shoudy Minor Moran White Hall Ronald Phelps Black Wood Ballinger Moore J. Miller Gill Dilling Cotterill Gill Hanson Fitzgerald Caldwell E. Brown Landes Edwards Harlin Dore C. Smith Dore Langlie Carroll Millikin Devin Pomeroy Clinton Braman F. Miller Uhlman Royer Rice Schell Nickels McGinn 1874 births 1940 deaths American people of Norwegian descent People from Racine County, Wisconsin People from Seattle, Washington People from San Clemente, California People from Orange County, California Members of the Washington House of Representatives Attempted assassination survivors Mayors of Seattle, Washington American anti-communists Conservatism in the United States American political pundits Washington (state) Progressives (1912) Content from Wikipedia Pages using duplicate arguments in template calls Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 22 November 2012, at 14:14. Privacy policy About Metapedia Disclaimers 
  I am tired of reading rhetorical, finely spun, hypocritical, far-fetched excuses for bolshevism, communism, syndicalism, IWWism! Nauseated by the sickly sentimentality of those who would conciliate, pander, and encourage all who would destroy our Government, I have tried to learn the truth and tell it in United States English of one or two syllables....
With syndicalism — and its youngest child, bolshevism — thrive murder, rape, pillage, arson, free love, poverty, want, starvation, filth, slavery, autocracy, suppression, sorrow and Hell on earth. It is a class government of the unable, the unfit, the untrained; of the scum, of the dregs, of the cruel, and of the failures. Freedom disappears, liberty emigrates, universal suffrage is abolished, progress ceases,...and a militant minority, great only in their self-conceit, reincarnate under the Dictatorship of the Proletariat a greater tyranny than ever existed under czar, emperor, or potentate.

 The so-called sympathetic Seattle strike was an attempted revolution. That there was no violence does not alter the fact... The intent, openly and covertly announced, was for the overthrow of the industrial system; here first, then everywhere... True, there were no flashing guns, no bombs, no killings. Revolution, I repeat, doesn't need violence. The general strike, as practised in Seattle, is of itself the weapon of revolution, all the more dangerous because quiet. To succeed, it must suspend everything; stop the entire life stream of a community... That is to say, it puts the government out of operation. And that is all there is to revolt — no matter how achieved.
 Ole Hanson Hiram C. Gill Mayor of Seattle C. B. Fitzgerald Authority control Preceded byHiram C. Gill
 Mayor of Seattle1918–1919
 Succeeded byC. B. Fitzgerald
 v • d • e
Mayors of Seattle, Washington* Atkins
Jordan
Stone
Jordan
Maddocks
Collins
Yesler
Gatzert
Weed
B. Brown
Jacobs
L. Smith
Struve
Leary
Yesler
Shoudy
Minor
Moran
White
Hall
Ronald
Phelps
Black
Wood
Ballinger
Moore
J. Miller
Gill
Dilling
Cotterill
Gill
Hanson
Fitzgerald
Caldwell
E. Brown
Landes
Edwards
Harlin
Dore
C. Smith
Dore
Langlie
Carroll
Millikin
Devin
Pomeroy
Clinton
Braman
F. Miller
Uhlman
Royer
Rice
Schell
Nickels
McGinn  * Atkins
Jordan
Stone
Jordan
Maddocks
Collins
Yesler
Gatzert
Weed
B. Brown
Jacobs
L. Smith
Struve
Leary
Yesler
Shoudy
Minor
Moran
White
Hall
Ronald
Phelps
Black
Wood
Ballinger
Moore
J. Miller
Gill
Dilling
Cotterill
Gill
Hanson
Fitzgerald
Caldwell
E. Brown
Landes
Edwards
Harlin
Dore
C. Smith
Dore
Langlie
Carroll
Millikin
Devin
Pomeroy
Clinton
Braman
F. Miller
Uhlman
Royer
Rice
Schell
Nickels
McGinn NAME
 Hanson
 ALTERNATIVE NAMES
 
 SHORT DESCRIPTION
 American politician
 DATE OF BIRTH
 January 6, 1874
 PLACE OF BIRTH
 Union Grove, Wisconsin
 DATE OF DEATH
 July 6, 1940
 PLACE OF DEATH
 Los Angeles, California
 Ole Hanson Contents Biography Footnotes Works Additional reading External links Navigation menu Early years Political career Founding San Clemente Death and legacy Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 