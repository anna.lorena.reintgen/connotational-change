The Battle of Caporetto (also known as the Twelfth Battle of the Isonzo or the Battle of Karfreit as it was known by the Central Powers , took place from 24 October to 19 November 1917, near the town of Kobarid (now in Slovenia), on the Austro-Italian front of World War I. The battle was named after the Italian name of the town of Kobarid (known as Karfreit in German).
 Austro-Hungarian forces, reinforced by German units, were able to break into the Italian front line and rout the Italian army, which had practically no mobile reserves. The battle was a demonstration of the effectiveness of the use of stormtroopers and the infiltration tactics developed in part by Oskar von Hutier. The use of poison gas by the Germans played a key role in the collapse of the Italian Second Army.[2]
 The German offensive began at approximately 02:00 on 24 October 1917. Due to the bad weather that morning, particularly the mist,[3] the Italians were caught by complete surprise. The battle opened with a German artillery barrage, poison gas, and smoke, and was followed by an all-out assault against the Italian lines.[4] The Italians had primitive gas masks, gave no counter fire and had given the Germans all the weather information they needed over their radio.[5] The defensive line of the Italian Second Army was breached almost immediately. The German forces made extensive use of flamethrowers and hand grenades as a part of their infiltration tactics, and were able to tear gaping holes in the Italian line, especially in the Italian strongholds on Mount Matajur and the Kolovrat Range. By the end of the first night, von Below's men had advanced a remarkable 25 km. German and Austro-Hungarian attacks from either side of von Below's central column were less effective, however. The Italian Army had been able to repel the majority of these attacks, but the success of von Below's central thrust threw the entire Italian Army into disarray. Forces had to be moved along the Italian front in an attempt to stem von Below's breakout, but this only weakened other points along the line and invited further attacks. At this point, the entire Italian position on the Tagliamento River was under threat.
 2nd army commander Luigi Capello was Italy's best general but was bedridden with fever while still retaining command. Realizing his forces were ill-prepared for this attack and were being routed, Capello requested permission to withdraw back to the Tagliamento. He was overruled by Cadorna, however, who believed that the Italian force could regroup and hold out against the attackers. Finally, on 30 October, Cadorna ordered the majority of the Italian force to retreat to the other side of the river. It took the Italians four full days to cross the river, and by this time the German and Austro-Hungarian armies were on their heels. By 2 November, a German division had established a bridgehead on the Tagliamento. About this time, however, the rapid success of the attack caught up with them. The German and Austro-Hungarian supply lines were stretched to breaking point, and as a result, they were not able to launch another concerted attack. Cadorna took advantage of this to retreat further, and by 10 November had established a position on the Piave River.[3]
 Even before the battle, Germany was struggling to feed and supply its armies in the field. Erwin Rommel, who, as a junior officer, won the Pour le Mérite for his exploits in the battle, often bemoaned the demands placed upon his "poorly fed troops".[6] The Allied blockade of the German Empire, which the Kaiserliche Marine had been unable to break, was responsible for food shortages and widespread malnutrition in Germany and the Central Powers in general. When inadequate provisioning was combined with the gruelling night marches preceding the battle of Caporetto (Kobarid), a heavy toll was extracted from the German and Austro-Hungarian forces. Despite these logistical problems, the initial assault was extremely successful. However, as the area controlled by the combined Central Powers forces expanded, an already limited logistical capacity was overstrained. By the time the attack reached the Piave, the soldiers of the Central Powers were running low on supplies and were feeling the physical effects of exhaustion.[6] As the Italians began to counter the pressure put on them by the Central Powers, the German forces lost all momentum and were once again caught up in another round of attrition warfare.
 Italian losses were enormous: 11,000 were killed, 20,000 wounded and 265,000 were taken prisoner – morale was so low among the Italian troops, mainly due to Cadorna's harsh disciplinary regime, that most of these surrendered willingly. Furthermore, roughly 3,000 guns, 3,000 machine guns and 2,000 mortars were captured, along with an untold amount of stores and equipment.[7] Rommel then an Oberleutnant, captured 1,500 men and 43 officers with just 3 riflemen and 2 officers to help.[5] In addition, a large number of Italian soldiers deserted the army following the battle. Austria-Hungarian and German forces advanced more than 100 km in the direction of Venice, but they were not able to cross the Piave River. Although to this point the Italians had been left to fight on their own, after Kobarid (Caporetto) they were reinforced by six French infantry divisions and five British infantry divisions as well as sizeable air contingents. However, these troops played no role in stemming the advancing Germans and Austro-Hungarians, because they were deployed on the Mincio River, some 45 km behind the Piave, as the British and French strategists did not believe the Piave line could be held. The Piave served as a natural barrier where the Italians could establish a new defensive line, which was held during the subsequent Battle of the Piave River and later served as springboard for the Battle of Vittorio Veneto, where the Austro-Hungarian army was finally defeated after 4 days of stiff resistance.
 The battle led to the conference at Rapallo and the creation of a Supreme War Council, with the aim of improving Allied military co-operation and developing a unified strategy.
 Luigi Cadorna was forced to resign after the defeat. The defeat alone was not the sole cause, but rather the breaking point for an accumulation of failures, as perceived by the Italian Prime Minister, Vittorio Emanuele Orlando. Throughout much of his command, including at Kobarid (Caporetto), Cadorna was known to have maintained poor relations with the other generals on his staff.[8] By the start of the battle he had sacked 217 generals, 255 colonels and 355 battalion commanders.[9] In addition, he was detested by his troops as being too harsh.[10] He was replaced by Armando Diaz and Pietro Badoglio.
 This led governments to the realization that fear alone could not adequately motivate a modern army. After the defeat at Kobarid (Caporetto), Italian propaganda offices were established, promising land and social justice to soldiers. Italy also accepted a more cautious military strategy from this point on. General Diaz concentrated his efforts on rebuilding his shattered forces while taking advantage of the national rejuvenation that had been spurred by invasion and defeat.
 After this battle, the term "Caporetto" gained a particular resonance in Italy. It is used to denote a terrible defeat – the failed General Strike of 1922 by the socialists was referred to by Mussolini as the "Caporetto of Italian Socialism". Many years after the war, Caporetto was still being used to destroy the credibility of the liberal state.[8]
 The Battle of Caporetto has been the subject of a number of books. The Swedish author F.J. Nordstedt wrote about the battle in his novel Caporetto. The bloody aftermath of Caporetto was vividly described by Ernest Hemingway in his novel A Farewell to Arms. Curzio Malaparte wrote an excoriation of the battle in his first book, Viva Caporetto published in 1921. It was censored by the state and suppressed, finally published in 1980.
 1 Battle 2 Failures of German Logistics 3 Aftermath 4 Popular culture 5 References ↑ 1.0 1.1  (25 October 2005) World War I: A Student Encyclopedia. United States: ABC-CLIO, 431. ISBN 1-85109-879-8. “By 10 November Italian losses were 10,000 dead, 30,000 wounded, and 265,000 prisoners (about 350,000 stragglers from the Second Army did manage to reach the Piave line). The army had also lost 3,152 artillery pieces of a preoffensive total of 6,918. An additional 1,712 heavy trench mortars and 3,000 machine guns had been captured or abandoned in the retreat, along with vast amounts of other military equipment, especially as the rapid withdrawal had prevedented the removal of heavy weapons and equipment across the Isonzo River. In contrast, the attackers had sustained about 70,000 casualties.” 
 ↑ Seth, Ronald (1965). Caporetto: The Scapegoat Battle. Macdonald. p. 147
 ↑ 3.0 3.1  (2001) The Encyclopedia of World History, 6th, Houghton Mifflin Harcourt, 1243. ISBN 0-395-65237-5. 
 ↑ Dupuy & Dupuy (1970), p. 971
 ↑ 5.0 5.1 Geoffrey Regan, More Military Blunders, page 161
 ↑ 6.0 6.1 Macksey, Kenneth (1997). Rommel: Battles and Campaigns. Da Capo Press, 224. ISBN 0-306-80786-6. 
 ↑  (2003) The First World War. Osprey Publishing, 352. ISBN 1-84176-738-7. 
 ↑ 8.0 8.1  (2002) Mussolini and Italy. Heinemann, 235. ISBN 0-435-32725-9. 
 ↑ Geoffrey Regan, More Military Blunders, page 160
 ↑  (2001) Caporetto, 1917: Victory Or Defeat?. Routledge, 171. ISBN 0-7146-5073-0. 
 Content from Wikipedia Conflicts in 1917 Battles of the Italian Front Battles of World War I involving Austria-Hungary Battles of World War I involving Germany Battles of World War I involving Italy Battles of the Isonzo History of Slovenia Military history of Italy during World War I 1917 in Italy 1917 in Austria Pages using duplicate arguments in template calls Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 12 August 2012, at 11:58. Privacy policy About Metapedia Disclaimers 
  Battle of Caporetto Twelfth Battle of the Isonzo Battle of Karfreit See also: Battle of Caporetto order of battle See also: Battle of Caporetto order of battle Part of the Italian Front (World War I)
 


Date

24 October – 12 November 1917


Location

Kobarid (Caporetto), Matajur, Kolovrat Range, Slovenia


Result

Central Powers victory


 24 October – 12 November 1917
 Kobarid (Caporetto), Matajur, Kolovrat Range, Slovenia
 Central Powers victory
  German Empire Austria-Hungary
  Italy
  Otto von Below  Svetozar Boroević
  Luigi Cadorna Luigi Capello
 400,000 soldiers 2,213 artillery
 856,000 soldiers 2,200 artillery
 70,000 dead or wounded[1]
 10,000 dead, 30,000 wounded,265,000 captured[1]
 Battle of Caporetto Contents Battle Failures of German Logistics Aftermath Popular culture References Navigation menu Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 