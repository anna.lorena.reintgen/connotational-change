This much revised and reorganized edition of Intellectuals and Society is more than half again larger than the first edition. Four new chapters have been added on intellectuals and race, including a chapter on race and intelligence.
 These new chapters show the radically different views of race prevailing among the intelligentsia at the beginning of the twentieth century and at the end-- and yet how each of these opposite views of race had the same dogmatic quality and the same refusal to countenance differing opinions among their contemporaries, much less engage dissenting opinions in serious debate. Moreover, each of these very different views of race produced flourishes of rhetoric and travesties of logic, leading to dire social consequences, though of very different sorts in the two eras.
 Other additions to this edition include a critique of John Rawls' conception or justice and a re-examination of the so-called "trickle-down theory" behind "tax cuts for the rich." There are other revisions, from the preface to the final chapter, the latter being extensively rewritten to bring together and highlight the themes of the other chapters, and to make unmistakably clear what Intellectuals and Society is, and is not, seeking to do.
 Thomas Sowell has taught economic at Cornell, UCLA, Amherst, and other academic institutions, and his book Basic Economics has been translated into six languages. He is currently a scholar in residence at the Hoover Institution, Stanford University. He has published in both academic journals and such popular media as the Wall Street Journal, Investor's Business Daily, Forbes, and Fortune, and he writes a syndicated column that appears in newspapers across the country.
 Preface v
 Acknowledgements x
 Part I:Introduction
 Chapter 1: Intellect and Intellectuals
 Chapter 2: Knowledge and Notions
 Part II: Intellectuals and Economics
 Chapter 3: "Income Distribution"
 Chapter 4: Economic Systems
 Chapter 5: Government and the Economy
 Part III: Intellectuals and Social Visions
 Chapter 6: A Conflict of Visions
 Chapter 7: Abstract People in an Abstract World
 Chapter 8: Arguments without Arguments
 Chapter 9: Patterns of the Annointed
 Part IV: Optional Reality
 Chapter 10: Filtering Reality
 Chapter 11: Subjective Truth
 Part V: Intellectuals and the Law
 Chapter 12: Changing the Law
 Chapter 13: Law and "Results"
 Part VI: Intellectuals and War
 Chapter 14: The World Wars
 Chapter 15: The Cold War and Beyond
 Part VII: Intellectuals and Race
 Chapter 16: Disparities and Their Causes
 Chapter 17: Race and Intelligence
 Chapter 18: Liberalism and Multiculturalism
 Chapter 19: Race and Cosmic Justice
 Part VIII: An Overview
 Chapter 20: Patterns and Visions
 Chapter 21: Incentives and Constraints
 Chapter 22: The Influence of Intellectuals
 Endnotes 547
 Index 648
 Intellectuals and Society, Thomas Sowell, 2011, Basic Books, ISBN-10: 0465025226 ISBN-13: 978-0465025220
 1 Back Cover Text 2 About the Author 3 Table of Contents 4 Books By Same Author 5 Publication Data 6 External Links Intelligence Versus Intellect 4
The Intelligentsia 7 The Intelligentsia 7 Ideas and Accountability 7
Verifiability 8
Accountability 10 Verifiability 8 Accountability 10 Competing Concepts of Knowledge 15
Concentration and Dispersion of Knowledge 17
Experts 25 Concentration and Dispersion of Knowledge 17 Experts 25 The Role of Reason 29
Reason and Justification 29
"One Day at a Time" Rationalism 34 Reason and Justification 29 "One Day at a Time" Rationalism 34 Empirical Evidence 42 Moral Consideration 48 The Poor as Consumers 53 Chaos Versus Competition 59 Zero-Sum Economics 69 Business 75
Management 75
Business "Power" or "Control" 78 Management 75 Business "Power" or "Control" 78 Recessions and Depressions 84 Opposing Social Visions 94 The Left-Right Dichotomy 98 Youth and Age 108 Disparities and Their Causes 116 Intertemporal Abstractions 126 Equality 128 "Simplistic" Arguments 139 Unworthy Opponents 141 Unworthy Arguments 147 The Rhetoric of "Rights" 157 Social Justice 159 "Change" Versus the Status Quo 172 Attitudes Versus Principles 175 A Sealed Bubble 177 Crusades of the Anointed 179 Selective Samples 191 Suppressing Facts 193 Fictitious People 202 Verbal Cleansing 217 Objective Verus Impartiality 219 The Localization of Evil 226 The Invidious and the Dramatic 228
The Invidious 228
The Dramatic 231 The Invidious 228 The Dramatic 231 Methods of Change 224
The Constitution and the Courts 246
Judicial Activism 254
Judicial Restraint and "Original Intent" The Constitution and the Courts 246 Judicial Activism 254 Judicial Restraint and "Original Intent" Burden of Proof 272 Property Rights 277 Crime 282 The First World War 301
The Pre-War Era 301
America at War 303 The Pre-War Era 301 America at War 303 The Second World War 310
Intellectuals Between the World Wars 310
Responses to International Crises 328
The Outbreak of War 333 Intellectuals Between the World Wars 310 Responses to International Crises 328 The Outbreak of War 333 Replaying the 1930's 339
The Vietnam War 341
The Cold War 348 The Vietnam War 341 The Cold War 348 The Iraq Wars 362 Patriotism and National Honor 370 Genetic Determinism 382 The Progressive Era 384 Responses to Disparities 403 Heredity and Environment 415 The Magnitudes in Question 417 Predictive Validity 422 Abstract Questions 423 Test Score Differences 425 Duration of Mental Test Results 429 The Liberal Era 441
The Liberal Vision 443
Racism and Causation 448 The Liberal Vision 443 Racism and Causation 448 The Multiculturalism Era 456
The Multicultural Vision 457
Cultural Changes 461
The Cultural Universe 463 The Multicultural Vision 457 Cultural Changes 461 The Cultural Universe 463 Disparities Versus Injustices 469 Race and Crime 472 Slavery 479 Value Premises 488 Assumptions about Facts 491 Intellectual Standards 495 The Supply of Public Intellectuals 504 The Demand for Public Intellectuals 509
A Sense of Mission 513
Constraints 517 A Sense of Mission 513 Constraints 517 The Nature of Intellectuals Influence 522 The Cognitive Elite 528 Intellectuals and Politicians 531 The Track Record of Intellectuals 534 Social Cohesion 537 Anti-Intellectual Intellectuals 542 Implications 544 Economic Facts and Fallacies Black Rednecks and White Liberals The Vision of the Anointed: Self-Congratulation as a Basis for Social Policy "Trickle Down Theory" and "Tax Cuts For The Rich" A Conflict of Visions: Ideological Origins of Political Struggles Basic Economics: A Common Sense Guide to the Economy Wealth, Poverty and Politics Dismantling America: and other controversial essays The Thomas Sowell Reader Knowledge And Decisions Applied Economics: Thinking Beyond Stage One The Quest for Cosmic Justice Conquests and Cultures: An International History Migrations And Cultures: A World View Barbarians inside the Gates and Other Controversial Essays Is Reality Optional?: And Other Essays Ever Wonder Why? And Other Controversial Essays Controversial Essays Inside American Education: The Decline, The Deception, The Dogma Education: Assumptions versus History: Collected Papers Affirmative Action Around the World: An Empirical Study Ethnic America: A History Civil Rights: Rhetoric or Reality? The Housing Boom and Bust: Revised Edition Discrimination and Disparities Literature Books Sociology Thomas Sowell Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 8 March 2018, at 05:57. Privacy policy About Metapedia Disclaimers 
  Thomas Sowell Part I:Introduction Part II: Intellectuals and Economics Part III: Intellectuals and Social Visions Part IV: Optional Reality Part V: Intellectuals and the Law Part VI: Intellectuals and War Part VII: Intellectuals and Race Part VIII: An Overview Intellectuals and Society Cover of the first English edition Thomas Sowell London English Politics Basic Books 2011 680 0465025226 Intellectuals and Society Contents Back Cover Text About the Author Table of Contents Books By Same Author Publication Data External Links Navigation menu Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 