Coordinates: 10°29′S 105°38′E﻿ / ﻿10.483°S 105.633°E﻿ / -10.483; 105.633
The Territory of Christmas Island is a territory of Australia in the Indian Ocean. It is located 2600 km northwest of the Western Australian city of Perth, 360 km south of the Indonesian capital, Jakarta, and 975 km ENE of the Cocos (Keeling) Islands.
 It has a population of 1,403 residents who live in a number of "settlement areas" on the northern tip of the island: Flying Fish Cove (also known as Kampong), Silver City, Poon Saan, and Drumsite.
 The island’s geographic isolation and history of minimal human disturbance has led to a high level of endemism among its flora and fauna, which is of significant interest to scientists and naturalists.[2] 63% of its 135 km2 is an Australian national park. There exist large areas of primary monsoonal forest.
 Phosphate, deposited originally as dead marine organisms (not guano as often thought), has been mined on the island for many years.
 Captain William Mynors of the Royal Mary, a British East India Company vessel, named the island when he sailed past it on Christmas Day in 1643.[3] The island was included on British and Dutch navigation charts as early as the beginning of the seventeenth century, but it was not until 1666 when a map published by Dutch cartographer Pieter Goos included the island. Goos labelled the island Mony, the meaning of which is unclear.[4] British navigator
William Dampier, aboard the British ship Cygnet, made the earliest recorded visit to sea round the island in March 1688. He found it uninhabited.[5] Dampier gave an account of the visit which can be found in his Voyages.[6] Dampier was trying to reach Cocos from New Holland. His ship was pulled off course in an easterly direction, arriving at Christmas Island 28 days later. Dampier landed at the Dales (on the west coast). Two of his crewmen were the first recorded humans to have set foot on Christmas Island.
 Daniel Beekman made the next recorded visit, chronicled in his 1718 book, A Voyage to and from the Island of Borneo, in the East Indies.
 The first attempt at exploring the island was in 1857 by the crew of the Amethyst. They tried to reach the summit of the island, but found the cliffs impassable.
 During the 1872–76 Challenger expedition to Indonesia, naturalist Dr John Murray carried out extensive surveys.[7]
 In 1887, Captain Maclear of HMS Flying Fish, having discovered an anchorage in a bay that he named Flying Fish Cove, landed a party and made a small but interesting collection of the flora and fauna. In the next year, Pelham Aldrich, on board HMS Egeria, visited it for ten days, accompanied by J. J. Lister, who gathered a larger biological and mineralogical collection.
 Among the rocks then obtained and submitted to Sir John Murray for examination were many of nearly pure phosphate of lime, a discovery which led to annexation of the island by the British Crown on 6 June 1888.[7]
 Soon afterwards, a small settlement was established in Flying Fish Cove by G. Clunies Ross, the owner of the Cocos (Keeling) Islands (some 900 km to the south west) to collect timber and supplies for the growing industry on Cocos.
 Phosphate mining began in the 1890s using indentured workers from Singapore, Malaya and China. John Davis Murray, a mechanical engineer and recent graduate of Purdue University, was sent to supervise the operation on behalf of the Phosphate Mining and Shipping Company. Murray was known as the "King of Christmas Island" until 1910, when he married and settled in London.[8]
 The island was administered jointly by the British Phosphate Commissioners and District Officers from the United Kingdom Colonial Office through the Straits Settlements, and later the Crown Colony of Singapore.
 From the outbreak of war in South East Asia in December 1941, Christmas Island was a target for Japanese occupation because of its rich phosphate deposits.[9] A naval gun was installed under a British officer and four NCOs and 27 Indian soldiers.[9] The first attack was carried out on 20 January 1942 by the Japanese submarine I-59, which torpedoed a Norwegian freighter, the Eidsvold.[10] The vessel drifted and eventually sank off West White Beach. Most of the European and Asian staff and their families were evacuated to Perth. In late February and early March 1942, there were two aerial bombing raids. Shelling from a Japanese naval group on 7 March led the District Officer to hoist the white flag.[9] But after the Japanese naval group sailed away, the British officer raised the Union Flag once more.[9] During the night of 10–11 March a mutiny of the Indian troops, abetted by the Sikh policemen, led to the murder of the five British soldiers and the imprisonment of the remaining 21 Europeans.[9] On 31 March, a Japanese fleet of nine vessels arrived and the island was surrendered. About 850 men of the 21st and 24th Special Base Forces and 102nd Construction Unit came ashore and occupied the island.[9] They rounded up the workforce, most of whom had fled to the jungle. Sabotaged equipment was repaired and preparations were made to resume the mining and export of phosphate. Only 20 men from 21st Special Base Force were left as a garrison.[9]
 Isolated acts of sabotage and the torpedoing of the Nissei Maru at the wharf on 17 November 1942[11] meant that only small amounts of phosphate were exported to Japan during the occupation. In November 1943, over 60% of the Island's population was evacuated to Surabayan prison camps, leaving a total population of just under 500 Chinese and Malays and 15 Japanese to survive as best they could. In October 1945 HMS Rother reoccupied Christmas Island.[12][13][14][15] After the war seven mutineers were traced and prosecuted by the Military Court in Singapore. In 1947 five of them were sentenced to death; however, following representations made by the newly independent governments of India and Pakistan, their sentences were reduced to penal servitude for life.[9]
 At Australia's request, the United Kingdom transferred sovereignty to Australia; in 1957, the Australian government paid the government of Singapore £2.9 million in compensation, a figure based mainly on an estimated value of the phosphate forgone by Singapore.
 Under Commonwealth Cabinet Decision 1573 of 9 September 1958, D. E. Nickels was appointed the first Official Representative of the new Territory. In a Media Statement on 5 August 1960, the Minister for Territories, Paul Hasluck, said, among other things, that "His extensive knowledge of the Malay language and the customs of the Asian people... has proved invaluable in the inauguration of Australian administration... During his two years on the Island he had faced unavoidable difficulties... and constantly sought to advance the Island's interests." John William Stokes succeeded him and served from 1 October 1960 to 12 June 1966. On his departure he was lauded by all sectors of the Island community. In 1968, the Official Secretary was re-titled an Administrator and, since 1997, Christmas Island and the Cocos (Keeling) Islands together are called the Australian Indian Ocean Territories and share a single Administrator resident on Christmas Island. A list and timetable of the Island's leaders since its settlement is at the World Statesmen site[16] and in Neale (1988) and Bosman (1993).
 The 2004 earthquake and tsunami centered off the western shore of Sumatra, Indonesia, resulted in no reported casualties, but some swimmers were swept some 150 m out to sea for a time before being swept back in.[17]
 From the late 1980s and early 1990s, boats carrying asylum seekers and mainly departing from Indonesia landed on the island. In 2001, Christmas Island was the site of the Tampa controversy, in which the Australian government stopped a Norwegian ship, MV Tampa, from disembarking 438 rescued asylum seekers at Christmas Island. The ensuing standoff and the associated political reactions in Australia were a major issue in the 2001 Australian federal election.[18]
 Another boatload of asylum seekers was taken from Christmas Island to Papua New Guinea for processing, after it was claimed that many of the adult asylum seekers threw their children into the water, apparently in protest at being turned away. This was later found to be false by a Senate select committee. Many of the refugees were subsequently accepted by New Zealand.[citation needed]
 The former Howard Government later secured the passage of legislation through the Australian Parliament which excised Christmas Island from Australia's migration zone, meaning that asylum seekers arriving on Christmas Island could not automatically apply to the Australian government for refugee status. This allowed the Royal Australian Navy to relocate them to other countries (Papua New Guinea's Manus Island, and Nauru) as part of the so-called Pacific Solution. In 2006 an Immigration Detention Centre, containing approximately 800 beds, was constructed on the island for the Department of Immigration. Originally estimated to cost $210 million, the final cost was over $400 million.[19]
 In 2007, the Rudd Government announced plans to decommission the Manus Island and Nauru centres; processing would then occur on Christmas Island itself.[20]
 In 2010, around 50 asylum seekers died off the coast of the island as the boat they were travelling on crashed into the rocks off Flying Fish Cove.[21][22]
 As of 2006, the estimated population is 1,493. (The Australian Bureau of Statistics reports a population of 1,508 as of the 2001 Census.)
 The ethnic composition is 70% Chinese 20% European and 10% Malay. According to the CIA World Factbook, religions practised on Christmas Island include Buddhism 36%,[citation needed] Christianity 18%, Islam 25% and others 21%. English is the official language, but Chinese languages and Malay are also spoken.
 A postal agency was opened on the island in 1901 and sold stamps of the Strait Settlements.[23]
 After the Japanese Occupation (1942–45), postage stamps of the British Military Administration in Malaya were in use, then stamps of Singapore.[24]
 In 1958, the island received its own postage stamps after being put under Australian custody. It had a large philatelic and postal independence, managed first by the Phosphate Commission (1958–1969) and then by the Island's Administration (1969–93).[23] This ended on 2 March 1993 when Australia Post became the island's postal operator: stamps of Christmas Island can be used in Australia and Australian stamps in the island.[24]
 Christmas Island is a non-self governing territory of Australia, currently administered by the Department of Regional Australia, Regional Development and Local Government. Administration was carried out by the Attorney-General's Department[25] up until 14 September 2010,[26] and prior to this by the Department of Transport and Regional Services before 29 November 2007.[27] The legal system is under the authority of the Governor-General of Australia and Australian law. An Administrator appointed by the Governor-General represents the monarch and Australia.
 The Australian Government provides Commonwealth-level government services through the Christmas Island Administration and the Department of Regional Australia, Regional Development and Local Government. As an administered Territory, there is no state government; instead, state government type services are provided by contractors, including departments of the Western Australian Government, with the costs met by the Australian (Commonwealth) Government. A unicameral Shire of Christmas Island with nine seats provides local government services and is elected by popular vote to serve four-year terms. Elections are held every two years, with half the members standing for election.
 Christmas Island residents who are Australian citizens also vote in Commonwealth (federal) elections. Christmas Island residents are represented in the House of Representatives through the Northern Territory Division of Lingiari and in the Senate by Northern Territory Senators.
 In early 1986, the Christmas Island Assembly held a design competition for an island flag; the winning design was adopted as the informal flag of the territory for over a decade, and in 2002 it was made the official flag of Christmas Island.
 Phosphate mining had been the only significant economic activity, but in December 1987 the Australian government closed the mine. In 1991, the mine was reopened by a consortium which included many of the former mine workers as shareholders. With the support of the government, the $34 million Christmas Island Casino and Resort opened in 1993, but was closed in 1998. As of 2011, the resort has re-opened without the casino.
 The Australian Government in 2001 agreed to support the creation of a commercial spaceport on the island, however this has not yet been constructed, and appears that it will not proceed in the future. The Howard Government built a temporary immigration detention centre on the island in 2001 and planned to replace it with a larger, modern facility located at North West Point until Howard's defeat in the 2007 elections.
 Located at 10°30′S 105°40′E﻿ / ﻿10.5°S 105.667°E﻿ / -10.5; 105.667, the island is a quadrilateral with hollowed sides, about 19 km in greatest length and 14.5 km in extreme breadth. The total land area is 135 km2, with 138.9 km of coastline. The island is the flat summit of a submarine mountain more than 4500 m,[28] the depth of the platform from which it rises being about 4200 m and its height above the sea being upwards of 300 m.[29] The mountain was originally a volcano, and some basalt is exposed in places such as The Dales and Dolly Beach, but most of the surface rock is limestone accumulated from the growth of coral over millions of years.[30] “The summit of this mountain peak is formed of a succession of tertiary limestones ranging in age from the Eocene (or Oligocene) up to recent reef-deposits, with intercalations in the older beds of volcanic rocks.”[31]
 Steep cliffs along much of the coast rise abruptly to a central plateau. Elevation ranges from sea level to 361 m at Murray Hill. The island is mainly tropical rainforest, of which 63% is National Park.
 The narrow fringing reef surrounding the island can be a maritime hazard.
 Christmas Island is 500 km south of Indonesia and about 2600 km northwest of Perth.
 The climate is tropical, with heat and humidity moderated by trade winds.
 Christmas Island is of immense value as it was uninhabited until the late 19th century, so many unique species of fauna and flora exist which have evolved independently of human interference. Two-thirds of the island has been declared a National Park which is managed by the Australian Department of Environment and Heritage through Parks Australia.
 The dense rainforest has evolved in the deep soils of the plateau and on the terraces. The forests are dominated by 25 tree species. Ferns, orchids and vines grow on the branches in the humid atmosphere beneath the canopy. The 135 plant species include at least eighteen which are found nowhere else.
 Christmas Island’s endemic plants include the trees Arenga listeri, Pandanus elatus and Dendrocnide peltata var. murrayana; the shrubs Abutilon listeri, Colubrina pedunculata, Grewia insularis and Pandanus christmatensis; the vines Hoya aldrichii and Zehneria alba; the herbs Asystasia alba, Dicliptera maclearii and Peperomia rossii; the grass Ischaemum nativitatis; the fern Asplenium listeri; and the orchids Brachypeza archytas, Flickingeria nativitatis, Phreatia listeri and Zeuxine exilis.[33]
 Two species of native rats, the Maclear's and Bulldog Rat, have become extinct since the island was settled. The endemic shrew has not been seen since the mid 1980s and may be already extinct, and the Christmas Island Pipistrelle, a small bat, is critically endangered and possibly also extinct.[34]
 The annual red crab mass migration (around 100 million animals) to the sea to spawn has been called one of the wonders of the natural world[35] and takes place each year around November; after the start of the wet season and in synchronisation with the cycle of the moon.
 The land crabs and sea birds are the most noticeable animals on the island. Twenty terrestrial and intertidal species of crab (of which thirteen are regarded as true land crabs, only dependent on the ocean for larval development) have been described. Robber crabs, known elsewhere as coconut crabs, also exist in large numbers on the island.
 Christmas Island is a focal point for sea birds of various species. Eight species or subspecies of sea birds nest on the island. The most numerous is the Red-footed Booby that nests in colonies, in trees, on many parts of the shore terrace. The widespread Brown Booby nests on the ground near the edge of the seacliff and inland cliffs. Abbott's Booby (listed as endangered) nests on tall emergent trees of the western, northern and southern plateau rainforest. The Christmas Island forest is the only nesting habitat of the Abbott's Booby left in the world. The endemic Christmas Island Frigatebird (listed as endangered) has nesting areas on the north-eastern shore terraces and the more widespread Great Frigatebirds nest in semi-deciduous trees on the shore terrace with the greatest concentrations being in the North West and South Point areas. The Common Noddy and two species of bosuns or tropicbirds, with their brilliant gold or silver plumage and distinctive streamer tail feathers, also nest on the island.
 Of the ten native land birds and shorebirds, seven are endemic species or subspecies. This includes the Christmas Island Thrush, and the Christmas Island Imperial-pigeon. Some 86 migrant bird species have been recorded as visitors to the Island.
 Christmas Island has been identified by BirdLife International as both an Endemic Bird Area and Important Bird Area because it supports five endemic species and five subspecies as well as over 1% of the world populations of five other seabirds.[36]
 Telephone services are provided by Telstra and are a part of the Australian network with the same prefix as Western Australia (08). A GSM mobile telephone system replaced the old analogue network in February 2005. Phone reception is at its best on the roofs of the buildings. Four free-to-air television stations from Australia are broadcast (ABC, SBS, GWN and WIN) in the same time-zone as Perth. Radio broadcasts from Australia include ABC Radio National, ABC Kimberley, Triple J and Red FM. All services are provided by satellite links from the mainland. Broadband internet became available to subscribers in urban areas in mid 2005 through the local internet service provider, CIIA (formerly dotCX).
 Christmas Island, due to its close proximity to Australia's northern neighbours, falls within many of the more 'interesting' satellite footprints throughout the region. This results in ideal conditions for receiving various Asian broadcasts which locals sometimes prefer to the West Australian provided content. Additionally, ionospheric conditions usually bode well for many of the more terrestrial radio transmissions – HF right up through VHF and sometimes in to UHF. The island plays home to a small array of radio equipment that, evidently, spans a good chunk of the usable spectrum. A variety of government owned and operated antenna systems are employed on the island to take advantage of this.
 A container port exists at Flying Fish Cove with an alternative container unloading point to the south of the island at Norris Point for use during the December to March 'swell season" of seasonal rough seas.
 An 18 km standard gauge railway from Flying Fish Cove to the phosphate mine was constructed in 1914. It was closed in December 1987, when the Australian Government closed the mine, but remains largely intact. Because of its very small population size, Christmas Island has the longest railway per capita in the world, more than 100 times of the average length.[37]
 There are four weekly flights with Virgin Australia Airlines into Christmas Island Airport from Perth, Western Australia and a weekly charter flight from Malaysia by Travel Exchange.
 There is a new recreation centre at Phosphate Hill operated by the Shire of Christmas Island. There is also a taxi service. The road network covers most of the island and is generally good quality, although four wheel drive vehicles are needed to access some more distant parts of the rain forest or the more isolated beaches, which are only accessible by rough dirt roads.
 The island-operated crèche is located in the Recreation Centre.[38] Christmas Island District High School, catering for students in grades P-12 is run by the Western Australian Education Department. Students wishing to complete university education must do so on mainland Australia.
 The island has one public library.[39]
 1 History

1.1 Discovery by Europeans
1.2 Exploration and annexation
1.3 Settlement and exploitation
1.4 Japanese invasion
1.5 Transfer to Australia

 1.1 Discovery by Europeans 1.2 Exploration and annexation 1.3 Settlement and exploitation 1.4 Japanese invasion 1.5 Transfer to Australia 2 Refugee and immigration detention 3 Demographics 4 Postage stamps 5 Government 6 Economy 7 Geography

7.1 Climate

 7.1 Climate 8 Flora and fauna

8.1 Flora
8.2 Fauna

 8.1 Flora 8.2 Fauna 9 Communications

9.1 Telecommunications

 9.1 Telecommunications 10 Transport

10.1 Container port
10.2 Railways
10.3 Air travel
10.4 Road transport

 10.1 Container port 10.2 Railways 10.3 Air travel 10.4 Road transport 11 Education 12 See also 13 Notes 14 References 15 Further reading 16 External links ↑ Christmas Island entry at The World Factbook, The World Factbook, CIA. Accessed 14 April 2009.
 ↑ Save Christmas Island – Introduction. The Wilderness Society (19 September 2002). Archived from the original on 9 June 2007. Retrieved on 14 April 2007.
 ↑ Department of the Environment, Water, Heritage and the Arts – Christmas Island History. Australian Government (8 July 2008). Retrieved on 26 April 2009.
 ↑ Digital Collections – Maps – Goos, Pieter, ca. 1616–1675. Paskaerte Zynde t'Oosterdeel Van Oost Indien (cartographic material) : met alle de Eylanden deer ontrendt geleegen van C. Comorin tot aen Iapan. National Library of Australia. Retrieved on 26 April 2009.
 ↑ Carney, Gerard (2006). The constitutional systems of the Australian states and territories. Cambridge University Press, 477. ISBN 0521863058. “The uninhabited island was named on Christmas Day 1643 by Captain William Mynors as he sailed past, leaving to William Dampier the honour of first landing ashore in 1688.” 
 ↑ Dampier, Captain William (1703). A New Voyage Round The World. The Crown in St Paul’s Church-yard, London, England: James Knapton, Contemporary full panelled calf with raised bands to spine and crimson morocco title labels; crimson sprinkled edges; 8vo. ISBN N/A. 
 ↑ 7.0 7.1 History. Welcome to Christmas Island. Christmas Island Tourism Association. Retrieved on 26 April 2009.
 ↑ Walsh, William (1913). A Handy Book of Curious Information. London: Lippincott, 447. 
 ↑ 9.0 9.1 9.2 9.3 9.4 9.5 9.6 9.7 L, Klemen (1999-2000). The Mystery of Christmas Island, March 1942. Forgotten Campaign: The Dutch East Indies Campaign 1941-1942.
 ↑ L, Klemen (1999-2000). Allied Merchant Ship Losses in the Pacific and Southeast Asia. Forgotten Campaign: The Dutch East Indies Campaign 1941-1942.
 ↑ Cressman, Robert J.. The Official Chronology of the U.S. Navy in World War II Chapter IV: 1942. Hyperwar/.
 ↑ Public Record Office, England War Office and Colonial Office Correspondence/Straits Settlements
 ↑ You must specify title =  and url =  when using {{cite web}}.J. Pettigrew. . Australian Territories January 1962.
 ↑ Interviews conduced by J G Hunt with Island residents, 1973–77
 ↑ Correspondence J G Hunt with former Island residents, 1973–79
 ↑  Christmas Island, World Statesmen, http://www.worldstatesmen.org/Christmas_Island.html .
 ↑ Main article: Countries affected by the 2004 Indian Ocean earthquake
 ↑ Fowler, Connie. Karsten Klepsuik, John Howard and the Tampa Crisis: Good Luck or Good Management?. Celsius Centre for Scandinavian Studies.
 ↑ Detention on Christmas Island. Amnesty International (10 March 2009). Retrieved on 26 April 2009.
 ↑ Public release of costing. electioncostings.gov.au (15 November 2007). Archived from the original on 26 July 2008. Retrieved on 26 April 2009.
 ↑ Needham, Kirsty; Stevenson, Andrew; Allard, Tom (15 December 2010). "They had no chance". The Sydney Morning Herald. http://www.smh.com.au/national/they-were-screaming-help-help-help-20101215-18ya5.html. Retrieved 15 December 2010. 
 ↑ , AU: ABC, 2011-02-09, http://www.abc.net.au/news/stories/2011/02/09/3134412.htm?section=justin .
 ↑ 23.0 23.1 Richard Breckon, "Christmas Island's Stamps and Postal History: 50 Years of Australian Administration", Gibbons Stamp Monthly, October 2008, pp. 81–85.
 ↑ 24.0 24.1 Commonwealth Stamp Catalogue Australia, Stanley Gibbons, 4th edition, 2007, pp. 104–112.
 ↑ First Assistant Secretary, Territories Division (30 January 2008). Territories of Australia. Attorney-General's Department. Archived from the original on 12 November 2009. Retrieved on 7 February 2008. “The Federal Government, through the Attorney-General's Department administers Ashmore and Cartier Islands, Christmas Island, the Cocos (Keeling) Islands, the Coral Sea Islands, Jervis Bay, and Norfolk Island as Territories.”
 ↑ First Assistant Secretary, Access to Justice Division (2 February 2011). Territories of Australia. Attorney-General's Department. Retrieved on 28 August 2011. “Under the Administrative Arrangements Order made on 14 September 2010, responsibility for services to Territories was transferred to the Department of Regional Australia, Regional Development and Local Government.”
 ↑ Department of Infrastructure, Transport, Regional Development and Local Government. Territories of Australia. Archived from the original on 16 December 2007. Retrieved on 7 February 2008. “As part of the Machinery of Government Changes following the Federal Election on 29 November 2007, administrative responsibility for Territories has been transferred to the Attorney General's Department.”
 ↑ Submission on Development Potential No. 37. Northern Australia Land and Water Taskforce (16 August 2007). Retrieved on 26 April 2009.
 ↑ Christmas island. World Factbook. CIA (23 April 2009). Retrieved on 26 April 2009.
 ↑ Physical Characteristics. Christmas Island National Park, Parks Australia.. Retrieved on 2007-05-13.
 ↑ [1] II.—A MONOGRAPH OF CHRISTMAS ISLAND (INDIAN OCEAN:PHYSICAL FEATURES AND GEOLOGY). By C. W. ANDREWS.
With descriptions of the Fauna and Flora by numerous contributors. 8vo ; pp. xiii, 337, 22 plates, 1 map, text illustrated.(London : printed by order of the Trustees of the British Museum, 1900.)
 ↑ Climate statistics for Christmas Island. Bureau of Meteorology. Retrieved on 21 September 2011.
 ↑ Christmas Island National Park: Flora.
 ↑ Parks Australia.
 ↑ Geoscience Australia on Christmas Island.
 ↑ BirdLife International. (2011). Important Bird Areas factsheet: Christmas Island. Downloaded from http://www.birdlife.org on 2011-12-23.
 ↑ (Railways) Total (per capita) (most recent) by country. Retrieved on 10 December 2009.
 ↑ Recreation Centre.
 ↑ Public library.
 Flora: Endemic plants. Parks and Reserves: Chritmas Island National Park. Australia Government - Dept of Sustainability, Environment, Water, Population and Communities (Updated 2010-11-11). Retrieved on 2010-11-16.  This article incorporates public domain material from websites or documents of the CIA World Factbook. L, Klemen (1999-2000). Forgotten Campaign: The Dutch East Indies Campaign 1941-1942. Jan Adams and Marg Neale, 1993, Christmas Island – The Early Years – 1888–1958. Published by Bruce Neale. 96 pages including many b&w photographs. ISBN 0-646-14894-X Dr Gerald R. Allen and Roger C. Steene, 1998, Fishes of Christmas Island. Published by the Christmas Island Natural History Association. 197 pages including many photographs and plates. ISBN 0-9591210-1-3. [Note: A second, revised, edition was published 2007 with a third author included, Max Orchard. ISBN 9780959121087] Anonymous, 1984, Christmas Island, Indian Ocean – a Unique Island. Published by a committee of present and former employees of the phosphate mining company. 60 pages including colour photographs. D. Bosman (Editor), 1993, Christmas Island Police – 1958–1983. Published by editor. 112 pages including many photographs. Cyril Ayris, 1993, Tai Ko Seng – Gordon Bennett of Christmas Island. Published by the Gordon Bennett Educational Foundation. 263 pages including photographs. ISBN 0-646-15483-4 CIA World Factbook 2002 Charles. W. Andrews, A Description of Christmas Island (Indian Ocean). Geographical Journal, 13(1), 17–35 (1899). Charles W. Andrews, A Monograph of Christmas Island, London,1900. H.S. Gray, 1981, Christmas Island Naturally. Published by author. 133 pages including many colour photographs. ISBN 0-9594105-0-3 John Hicks, Holger Rumpff and Hugh Yorkston, 1984, Christmas Crabs. Published by the Christmas Island Natural History Association. 76 pages including colour photographs. ISBN 0-9591210-0-5 National Library of Australia, The Indian Ocean: a select bibliography. 1979 ISBN 0-642-99150-2 Margaret Neale, 1988, We were the Christmas Islanders. Published by Bruce Neale. 207 pages including many b&w photographs. ISBN 0-7316-4158-2/0-7316-4157-4. W. J. L. Wharton, Account of Christmas Island, Indian Ocean. Proceedings of the Royal Geographical Society and Monthly Record of Geography, 10 (10), 613–624 (1888). Les Waters, 1983, The Union of Christmas Island Workers. Published by Allen & Unwin (1992 edition), St Leonards, NSW. 170 pages including b&w photographs. Christmas Island Shire – official government website Christmas Island Tourism Association – official tourism website Christmas Island National Park – official website Christmas Island National Park Christmas Island Act 1958  Christmas Island travel guide from Wikitravel Christmas Island at the Open Directory Project Christmas Island entry at The World Factbook Christmas Island Travel Guide from Unearth Travel a creative commons travel wiki "Australia Puts Its Refugee Problem on a Remote Island, Behind Razor Wire" — New York Times, 5 November 2009 Articles with broken citations Wikipedia articles incorporating text from the World Factbook Christmas Island Islands of the Indian Ocean States and territories of Australia British rule in Singapore States and territories established in 1957 Important Bird Areas of Australia Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 26 August 2014, at 21:08. Privacy policy About Metapedia Disclaimers 
  Anthem:  Capital Ethnic groups Christmas Island title =  url =  13(1) 10 (10) Christmas Island Main article: Postage stamps and postal history of Christmas Island See also: Birds of Christmas Island Main article: Postage stamps and postal history of Christmas Island See also: Birds of Christmas Island 




   Anthem: Advance Australia Fair 
 Capitaland largest city Flying Fish Cove ("The Settlement") English (de facto) Ethnic groups  70% Chinese, 20% European, 10% Malay Christmas Islanders Federal constitutional monarchy  -  Queen Elizabeth II  -  Governor-General of the Commonwealth of Australia Quentin Bryce  -  Administrator Brian Lacy  -  Shire President Gordon Thomson  -  Sovereigntytransferred to Australia 1957   -  Total 135 km252 sq mi   -  Water (%) 0  -  2009 estimate 1,402[1] (220th)  -  Density 10.39/km2 (n/a)26.96/sq mi Australian dollar (AUD)  (UTC+7) 61 .cx 30.6(87.1)
 31.5(88.7)
 31.5(88.7)
 31.4(88.5)
 30.0(86)
 29.4(84.9)
 28.8(83.8)
 29.5(85.1)
 30.9(87.6)
 31.4(88.5)
 31.8(89.2)
 31.2(88.2)
 31.8(89.2)
 27.9(82.2)
 27.9(82.2)
 28.2(82.8)
 28.2(82.8)
 27.8(82)
 27.0(80.6)
 26.2(79.2)
 26.0(78.8)
 26.2(79.2)
 26.8(80.2)
 27.2(81)
 27.7(81.9)
 27.3(81.1)
 22.6(72.7)
 22.6(72.7)
 23.0(73.4)
 23.5(74.3)
 23.8(74.8)
 23.2(73.8)
 22.5(72.5)
 22.2(72)
 22.2(72)
 22.6(72.7)
 22.8(73)
 22.5(72.5)
 22.8(73)
 18.8(65.8)
 18.4(65.1)
 18.6(65.5)
 18.3(64.9)
 19.3(66.7)
 14.1(57.4)
 16.2(61.2)
 17.7(63.9)
 16.7(62.1)
 18.2(64.8)
 18.0(64.4)
 18.0(64.4)
 14.1(57.4)
 293.6(11.559)
 348.4(13.717)
 290.9(11.453)
 221.8(8.732)
 176.7(6.957)
 155.8(6.134)
 96.9(3.815)
 43.1(1.697)
 47.0(1.85)
 68.7(2.705)
 147.7(5.815)
 210.7(8.295)
 2,110.5(83.091)
 Source: Australian Bureau of Meteorology [32]
    Wikimedia Commons has media related to: Christmas Island   Australia related{{{list1}}}  {{{list1}}}  Oceania related{{{list1}}}  {{{list1}}}  Asia related{{{list1}}}  {{{list1}}}  Language related{{{list1}}}  {{{list1}}} Christmas Island Contents History Refugee and immigration detention Demographics Postage stamps Government Economy Geography Flora and fauna Communications Transport Education See also Notes References Further reading External links Navigation menu Discovery by Europeans Exploration and annexation Settlement and exploitation Japanese invasion Transfer to Australia Climate Flora Fauna Telecommunications Container port Railways Air travel Road transport Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 