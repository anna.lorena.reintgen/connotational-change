Nadezhda Konstantinovna "Nadya" Krupskaya (Russian: Наде́жда Константи́новна Кру́пская, scientific transliteration Nadežda Konstantinovna Krupskaja) (26 February [O.S. 14 February] 1869 – February 27, 1939)[1] was a Russian Bolshevik revolutionary and politician. She married the Russian revolutionary leader Vladimir Lenin in 1898. She was deputy minister (Comissar) of Education in 1929–1939, Doctor of Education.
 She was born to an upper-class, but impoverished, family. Her father was a Russian military officer, a nobleman of the Russian Empire. Nadya’s father, Konstantin Ignat’evich Krupski, was orphaned in 1847 at nine years of age. He was educated and given a commission as an infantry officer in the Russian Army. Just before leaving for his assignment in Poland he married Nadya’s mother. After six years of service, Krupski lost favor with his supervisors and was charged with “un-Russian activities.” He may have been suspected of being involved with revolutionaries. Following this time, he worked in factories or wherever he could find work until later in life when he was recommissioned just before his death.[2]
 Her mother, Elizaveta Vasilyevna Tistrova was the daughter of landless Russian nobles. Elizaveta’s parents died when she was young and she was enrolled in the Bestuzhev Courses, which happened to be the highest formal education available to women in Russia during this time. After earning her degree Elizaveta went on to work as a governess for noble families until she married Krupski.[3]
 Having parents who were well educated, and of aristocratic descent, combined with firsthand experience with lower class working conditions probably led to the formation of many of Nadya’s ideologies. “From her very childhood Krupskaya was inspired with the spirit of protest against the ugly life around her.”[4]
 One of Nadya’s friends from gymnasium, Ariadne Tyrkova, described Krupskaya as “a tall, quiet girl, who did not flirt with the boys, moved and thought with deliberation, and had already formed strong convictions… She was one of those who are forever committed, once they have been possessed by their thoughts and feelings….”[5] Nadya breifly attended two different secondary schools before finding the perfect fit with Prince A.A. Obolensky's Female Gymnasium, “a distinguished private girls’ secondary school in Petersburg.” This education was probably more liberal than most other gymnasiums since it was noted that some of the staff were former revolutionaries.[6]
 After her father’s death, Krupskaya and her mother gave lessons as a source of income. Krupskaya had expressed an interest in entering the education field from a young age. She was particularly drawn to Tolstoy’s theories on education, which were fluid instead of structured. They focused on the personal development of each individual student and centered on the importance of the teacher-student relationship.[7]
 This led Krupskaya to study many of Tolstoy’s works, including his theories of reformation. These were peaceful, law abiding ideologies, which focused on people abstaining from unneeded luxuries and being self-dependent instead of hiring someone else to tend your house, etc. Tolstoy had a lasting impression on Krupskaya, since it was said she had “a special contempt for stylish clothes and comfort.”[8] She was always modest in dress, as were her furnishings in her home and office.
 As a devoted, lifelong student, Krupskaya began to participate in several discussion circles. These groups got together to study and discuss particular topics for the benefit of everyone involved. It was later, in one of these circles, that Krupskaya was first introduced to the theories of Marx. This piqued her interest as a potential way of making life better for her people. Krupskaya began an in-depth study of the subject. This was difficult since such books had been banned by the Russian government. Consequently, revolutionaries had been collecting such books and keeping them in underground libraries.
 It was at a similar discussion circle that Krupskaya first met Vladimir Ilyich Ulyanov, a dedicated Marxist who later came to be called Lenin. Krupskaya was impressed by his speeches but not his personality, at least at first. It is hard to know very much of the courtship between Lenin and Krupskaya as neither party spoke often of personal matters.
 In October 1896, several months after Lenin was arrested, Krupskaya was also arrested. After some time, Lenin was sentenced to exile in Siberia. They had very little communication while in prison but before leaving for Siberia, Lenin wrote a "secret note" to Krupskaya that was delivered by her mother. It suggested that she could be permitted to join him in Siberia if she told people she was his fiancée. At that time, Krupskaya was still awaiting sentencing in Siberia. Krupskaya was permitted to accompany Lenin but only if they were married as soon as she arrived.
 In her memoirs, Krupskaya notes "with him even such a job as translation was a labour of love".[9] Her relationship with Lenin was more professional than marital, which Kollontai compared to slavery, but she remained loyal, never once considering divorce.
 Krupskaya is believed to have suffered from Graves' disease,[10] an illness affecting the thyroid gland in the neck which causes the eyes to bulge and the neck to tighten. In female sufferers, it can also disrupt the menstrual cycle, which may explain why Lenin and Krupskaya never had children (and the rumors about Lenin having an affair with Inessa Armand[citation needed]).
 Upon his release, Lenin went off to Europe and settled in Munich where Krupskaya later met up with him upon her release (1901). After she had arrived the couple moved to London.
 Krupskaya's political life was active: she was a functionary of the bolshevik faction of the Russian Social Democratic Labour Party from 1903. She became secretary of the Central Committee in 1905; she returned to Russia the same year, but left again after the failed revolution of 1905 and worked as a teacher in France a couple of years.
 After the October Revolution in 1917, she was appointed deputy to Anatoliy Lunacharskiy, the People's Commissar for Education, where she took charge of Vneshkol'nyi Otdel the Adult Education Division; she became chairman of the education committee in 1920 and was deputy commissar (government minister) from 1929 to 1939. She was instrumental in the foundation of Komsomol and the Pioneer movement as well as the Soviet educational system, including the censorship and political indoctrination within it. She was also fundamental in the development of Soviet librarianship.
 Krupskaya became a member of the Central Committee of the Communist Party of the Soviet Union in 1924, a member of its control commission in 1927, a member of the Supreme Soviet in 1931 and an honorary citizen in 1931. She apparently favored Stalin in the great debates between the Left Opposition and the CPSU majority of the 1920s. In 1925, she attacked Lev Trotsky in a polemic that was in response to Trotsky's tract The Lessons of October. In it, she stated that "Marxist analysis was never Comrade Trotsky’s strong point." In relation to the debate around Socialism in one country versus Permanent Revolution, she asserted that Trotsky "under-estimates the role played by the peasantry." Furthermore, she held that Trotsky had misinterpreted the revolutionary situation in post-WWI Germany. During the congress of 1925, she initially supported Grigory Zinoviev and Lev Kamenev, but eventually voted for the process against Nikolai Bukharin and the exclusion of Trotsky, Zinoviev and Kamenev from the party.[11]
 In 1936 she defended restrictions on abortion passed by the Soviet government in that year, arguing that they were part of a consistent policy pursued since 1920 to do away with the reasons to have an abortion.[12]
 Krupskaya is the author of the biography Reminiscences of Lenin,[13] which chronicles the life of her husband. Her biography is the most detailed account of Lenin’s life before coming to power. It ends in 1919, shortly after the Bolsheviks took power.
 Before the revolution, Krupskaya worked five years as an instructor for a factory owner who offered evening classes for his employees. Legally, reading, writing and arithmetic were taught. Illegally, classes with a revolutionary influence were taught for those students who might be ready for them. Krupskaya and other instructors were relieved of duty when nearly 30,000 factory workers in the area went on strike for better wages.[14] Even after the revolution her emphasis was on “the problems of youth organization and education.”[15] In order to become educated they needed better access to books and materials.[16]
 Pre-revolutionary Russian libraries had a tendency to exclude particular members. Some were exclusively for higher classes and some were only for employees of a particular company's "Trade Unions". In addition they also had narrow, Orthodox literature. It was hard to find any books with new ideas, which is exactly why the underground libraries began. Another problem was the low level of literacy of the masses.
 The revolution did not cause an overnight improvement in the libraries. In fact, for a while there were even more problems. The Trade Unions still refused to allow general public use, funds for purchasing books and materials were in short supply and books that were already a part of the libraries were falling apart. In addition there was a low interest in the library career field due to low income and the libraries were sorely in need of re-organization.
 Krupskaya directed a census of the libraries in order to address these issues.[17] She encouraged libraries to collaborate and to open their doors to the general public. She encouraged librarians to use common speech when speaking with patrons. Knowing the workers needs was encouraged; what kind of books should be stocked, the subjects readers were interested in, and organizing the material in a fashion to better serve the readers. Committees were held to improve card catalogs.
 Krupskaya stated at a library conference: “We have a laughable number of libraries, and their book stocks are even more inadequate. Their quality is terrible, the majority of the population does not know how to use them and does not even know what a library is.”[18]
 She also sought better professional schools for librarians. Formal training was scarce in pre-revolutionary Russia for librarians and it only truly began in the 20th century. Krupskaya, therefore, advocated the creation of library “seminaries” where practicing librarians would instruct aspiring librarians in the skills of their profession, similar to those in the West. The pedagogical characteristics were however those of the Soviet revolutionary period. Librarians were trained to determine what materials were suitable to patrons and whether or not they had the ability to appreciate what the resource had to offer. Also, Krupskaya desired that librarians possess greater verbal and writing skills so that they could more clearly explain why certain reading materials were better than others to their patrons. She believed that explaining resource choices to patrons was a courtesy and an opportunity for more education in socialist political values, not something that was required of the librarian. They were to become facilitators of the revolution and, later, those who helped preserve the values of the resulting socialist state.[19]
 Krupskaya was a committed Marxist for whom each element of public education was a step toward improving the life of her people, granting all individuals access to the tools of education and libraries, needed to forge a more fulfilling life. The fulfillment was education and the tools were education and library systems.[20]
 Following her death in 1939 a Leningrad chocolate factory was renamed in her honour. Its chocolate bar product was named Krupskaya and retains that name today.[21]
 The asteroid 2071 Nadezhda discovered in 1971 by Soviet astronomer Tamara Mikhailovna Smirnova was named in her honour.[22]
 Krupsakaya (right) in 1936
 Krupskaya (middle) in the 1930s
 Board at a kindergarten in Berlin-Spandau, Germany
  
 1 Early life 2 Married life 3 Political career 4 Soviet education and libraries 5 Legacy 6 Gallery 7 References 8 Sources 9 External links 


Krupsakaya (right) in 1936


 


Krupskaya (middle) in the 1930s


 


Board at a kindergarten in Berlin-Spandau, Germany


 ↑ McNeal, 13.
 ↑ McNeal, 5–9.
 ↑ McNeal, 11–12
 ↑ C. Bobrovskaija, Lenin and Krupskaja (New York City : Workers Library Publishers, Inc., 1940), 4.
 ↑ McNeal, 19.
 ↑ McNeal, 17–19.
 ↑ Tolstoy, Leo. In Encyclopædia Britannica. Retrieved March 21, 2008, from Encyclopædia Britannica Online
 ↑ McNeal, 23.
 ↑ http://www.marxists.org/archive/krupskaya/works/rol/rol01.htm
 ↑ H. Rappaport, Conspirator (London: Hutchinson, 2009), 200.
 ↑ Nadezhda K. Krupskaya. The Lessons of October Source: The Errors of Trotskyism, Communist Party of Great Britain, May 1925
 ↑ Preface to the pamphlet “The New Law on Mother and Child”, 1936.
 ↑ N. K. Krupskaya's. Reminiscences of Lenin, Written: 1933; First Published: International Publishers, 1970
 ↑ Raymond, 53–55
 ↑ McNeal, 173
 ↑ Raymond, 171.
 ↑ N. K. Krupskaya, Part two : Krupskaia on libraries, ed Sylva Simsova (Hamden : Archon Books, 1968) 45–51.
 ↑ Raymond, 161.
 ↑ Richardson, John (2000). "The Origin of Soviet Education for Librarianship: The Role of Nadezhda Konstantinovna Krupskaya (1869–1939), Lyubov' Borisovna Khavkina-Hamburger (1871–1949) and Genrietta K. Abele-Derman (1882–1954)". Journal of Education for Library and Information Science 41 (Spring 2000): 106–128 (115–117). doi:10.2307/40324059. ISSN 0748-5786.
 ↑ Raymond, 172.
 ↑ Crace, John (2010-01-27). "The Soviet chocolate named after Lenin's widow". The Guardian (London). http://www.guardian.co.uk/lifeandstyle/2010/jan/27/soviet-chocolate-lenin-russia. Retrieved 2010-05-19. 
 ↑ Schmadel, Lutz D. (2003). Dictionary of Minor Planet Names, 5th, New York: Springer Verlag, 168. ISBN 3540002383. 
 McNeal, Robert Hatch (1973). Bride of the Revolution: Krupskaya and Lenin. Gollancz. ISBN 0472616005.  Raymond, Boris The contribution of N. K. Krupskaia to the development of Soviet Russian Librarianship: 1917–1939 (Ann Arbor : The University of Chicago, 1978) Nadezhda Krupskaya Krupskaya Internet Archive Obituary by Leon Trotsky 1869 births 1939 deaths Bolsheviks Marxist writers Old Bolsheviks People buried in the Kremlin Wall Necropolis People from Saint Petersburg Russian Social Democratic Labour Party members Russian communists Russian Marxists Russian revolutionaries Soviet women writers Spouses of Russian and Soviet national leaders Vladimir Lenin Russian women Russian nobility Soviet educators Soviet women in politics Content from Wikipedia Pages using duplicate arguments in template calls Articles with hCards Articles containing non-English language text Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 11 December 2014, at 23:09. Privacy policy About Metapedia Disclaimers 
  Nadezhda Konstantinovna "Nadya" Krupskaya 41 Nadezhda Konstantinovna Krupskaya Nadezhda Krupskaya, c. 1890s 26 February 1869(1869-02-26)St. Petersburg, Russian Empire 27 February 1939 (aged 70)Moscow, Russian SFSR, Soviet Union   Wikimedia Commons has media related to: Nadezhda Konstantinovna Krupskaya  NAME
 Krupskaya, Nadezhda Konstantinova
 ALTERNATIVE NAMES
 
 SHORT DESCRIPTION
 
 DATE OF BIRTH
 26 February 1869
 PLACE OF BIRTH
 St. Petersburg, Russian Empire
 DATE OF DEATH
 27 February 1939
 PLACE OF DEATH
 Moscow, Russian SFSR, Soviet Union
 Nadezhda Krupskaya Contents Early life Married life Political career Soviet education and libraries Legacy Gallery References Sources External links Navigation menu Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 