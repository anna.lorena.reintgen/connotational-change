Edgar Miles Bronfman, Jr. (born May 16, 1955) is an American businessman who served as CEO of Warner Music Group 2004 to 2011 and as Chairman from 2011 to 2012. In May, 2011, the sale of WMG was announced; Bronfman would continue as CEO in the transaction. In August 2011, he became Chairman of the company as Stephen Cooper became CEO.[1] Bronfman previously served as CEO of Seagram and vice-chairman of Vivendi Universal. He is the son of Edgar Miles Bronfman and the grandson of Samuel Bronfman, patriarch of one of the wealthiest and most influential Jewish families in Canada.
 The Bronfman family gained its fortunes through the Seagram Company, an alcohol distilling company, but Edgar Jr. ("Efer" to friends) has gained his reputation by expanding and later divesting ownership of the Seagram Company, as well as for pursuing more creative activities as a Broadway and film producer and songwriter.
 His controversial decision to sell his family stake in DuPont Chemical in order to create the Vivendi Universal entertainment company which turned out to be short-lived, was a source of widespread astonishment in the business community.[citation needed]
 Edgar Jr. is the second of five children of Ann (Loeb) and Edgar Miles Bronfman. He was indicated by his grandfather in 1971 as being suitable as the heir to the Seagram Company.[citation needed]
 From his early days, Bronfman's interest in the arts was apparent. He was particularly active in school theatre, an interest his parents supported by donating to construct The Ann and Edgar Bronfman Theatre during a 1967 expansion at The Collegiate School, the prestigious private school in Manhattan which Edgar Jr. attended. Edgar Jr. and his classmates created a documentary film of the school that spawned the Collegiate Film Festival, an event that gained positive press in The Los Angeles Times and The Village Voice.
 In the summer before his junior year in high school, Bronfman went to London to work on a feature film, arranged partly through his father's connections at MGM.
 Bronfman proceeded to a brief career in entertainment in the 1970s as a film and Broadway producer. The summer before his final year of high school, in 1972, he was a credited producer on the film, The Blockhouse. Despite his inexperience, Bronfman's involvement was accepted because of his connections and access to financing through his family's wealth. In return, he learned many of the tricks of the trade by watching his more experienced peers.
 In 1973, Bronfman began a songwriting career under the pseudonyms Junior Miles and Sam Roman. He often collaborated with Bruce Roberts on songs like "Whisper in the Dark", which he gave to Dionne Warwick to record in thanks for introducing him to his first wife, Sherry.
 His Efer Productions company was signed by Universal Studios in 1977 to a three-year movie production contract. He produced the unsuccessful film The Border, which starred Jack Nicholson.
 In 1982, Bronfman returned to the Seagram Company, spending three months learning the ropes before moving to London to become managing director of Seagram Europe. In 1984, Bronfman returned to New York as President of the House of Seagram, the company's U.S. marketing division. By 1994 he became the Chief Executive Officer, where he began a move away from the traditional liquor business and into entertainment.
 The first step in this diversification was the widely criticized sale of Seagram's stake in DuPont. In 1981, Edgar Bronfman, Sr., had sold Seagram's stake in Conoco to DuPont, in exchange for almost 25% of the chemical giant. This stake in DuPont, by 1995, represented about 70% of Seagram's total earnings. Nevertheless, Bronfman, Jr., acting as Seagram's CEO, approached DuPont about buying back its shares, a deal that DuPont wasted no time in closing.
 With the proceeds of the $9 billion sale, Bronfman, Jr., went on an expansion into the entertainment business, in music through the acquisition of Polygram, and in film entertainment through MCA and Universal Pictures. However, the new entertainment conglomerate he created had a brief life, before needing a strategic partner. Bronfman, Jr., then led Seagram into a controversial all-stock acquisition by French conglomerate Vivendi in 2000. Bronfman, Jr., became chief of the new company, Vivendi Universal, but the Seagram company effectively lost control of its entertainment businesses. Meanwhile, the beverage division—the core of Seagram's business—was acquired by Pernod Ricard and Diageo. Seagram's for all intents and purposes ceased to exist.
 In December 2001, Bronfman announced he was stepping down from an executive capacity at Vivendi Universal, but remaining as vice chair of the board.
 On February 27, 2004, Bronfman finalized the acquisition of Warner Music Group and he has served as Chairman and CEO of the music company since that time. Bronfman has helped to transform WMG by rapidly growing the company's digital music sales, redefining the relationships it has with artists and diversifying its revenue streams through its expansion into growing areas of the music business.[2] WMG held an initial public offering of stock in 2005 (NYSE: WMG), and is now the only standalone major music company to be publicly traded. While the stock has fallen from a high in 2005 of over $30 per share, the company has nonetheless produced double-digit growth in its digital business, increased its market share and delivered stable revenue performance despite a drastic music industry decline during the same period.[3] In 2008, The New York Times reported that WMG's Atlantic Records became the first major record label to generate more than half of its music sales in the U.S. from digital products.[4]
 In May, 2011, WMG and Bronfman announced the company's sale to Access Industries for US$3.3 billion cash. Access is controlled by Russian-born billionaire Len Blavatnik, a former board member and still-substantial shareholder of WMG at the time of the purchase announcement. The sale, coming after a three-month bidding process, "serves the best interests of stockholders as well as the best interests of music fans, our recording artists and songwriters, and the wonderful people of this company," according to a statement released by Bronfman.[5] CEO Bronfman would continue in his post in the transaction, though further job cuts were also foreseen. The investment group which has owned the company since 2004 was said to have received a positive return on its investment.[6]
 In August 2011, Bronfman became Chairman of Warner Music and Stephen Cooper became CEO.[7] He stepped down as Chairman on January 31, 2012.[8][9]
 Bronfman has continued to dabble in songwriting and penned the hit song "To Love You More", which was recorded by Celine Dion, and Barbra Streisand's "If I Didn't Love You".
 He also currently chairs the Board of Directors of Endeavor, an international non-profit development organization that finds and supports high-impact entrepreneurs in emerging markets.
 In 1979. Bronfman married his first wife, Sherry Brewer, an African-American actress, in New Orleans. Bronfman's father did not approve of the marriage. "I very much wanted for him to end the relationship, because I told him, all marriages are difficult enough without the added stress of totally different backgrounds", Bronfman Sr. wrote in his memoirs. "Sherry offered to convert [to Judaism], which though well intentioned, was not the point."[10] Bronfman and Brewer eloped and he and his father remained estranged.[11] The couple had three children before they divorced in 1991:
 In 1993, Bronfman married Clarisa Alcock San Román, the daughter of Frank Alcock Pérez-Matos, a Venezuelan oil executive of half British descent and a Venezuelan mother, Dinorah San Román Strup.[13][14] They have four children: Aaron, Bettina, Erik, and Clarissa.
 On January 21, 2011, Bronfman was found guilty in French court of insider trading and received a 15-month suspended sentence and a €5m fine.[15]
Considering the jail sentences handed out to other executives for similar convictions, BNN reporter Michael Kane told CTV News "The fact that the judge suspended the jail time could be looked at as getting off lightly, perhaps."[16] He has appealed the decision.[17]
 At the height of file sharing service Napster's popularity, Bronfman was a leading opponent of the illegal use of peer-to-peer technology. As CEO of Universal, he helped lead the music industry's opposition to Napster, likening it to slavery and Soviet communism.[18] As Chairman and CEO of WMG, Bronfman has championed the use of digital innovation and is recognized as a leader in the industry gradual but growing embrace of social media, including its work with Cisco to develop a social networking strategy of its own as well as investments and partnerships with digital music services such as Spotify, imeem, MySpace Music, Lala and we7.[19]
 In 2006, WMG was the first major media company to create a business model around user-generated content and, more recently, has been pushing for ways to monetize the popularity of P2P networks on college campuses.[20][21]
 In late 2006 in an interview with Reuters, Bronfman caused a stir by admitting that his children have pirated music.[22] He claims to have punished the child, but wants the punishment to remain within the realm of the family.[23]
 Lately, Bronfman may have revised his judgment. During the GSMA Mobile Asia Congress, he told the audience that mobile operators should not make the same mistakes that the music industry has:[24]
 In 2010 Bronfman changed his philosophy on the music industry's online business models, stating that he does not support free advertising supported models. He said that WMG will focus on promoting services that require payment, that will appeal to the population that already pays for downloads in stores such as iTunes.[25]
 1 Early life 2 Career 3 Personal life 4 Insider trading conviction 5 Music piracy 6 See also 7 References and notes 8 External links Benjamin – Bronfman's eldest son with Sherry is also known as "Ben Brewer", a rock musician. Brewer was the guitar player and vocalist for the New York-based alternative rock band The Exit. He also was engaged to Mathangi "Maya" Arulpragasam, a British recording artist, songwriter, painter and director of Sri Lankan Tamil descent. Her compositions combine elements of electronica, dance, alternative, hip hop and world music. She is better known under the stage name of M.I.A. They have a son, Ikhyd Edgar Arular Bronfman, born on 11 February 2009. Vanessa and Hannah[12] Bronfman family Edgar Bronfman, Sr. Samuel Bronfman ↑ http://www.billboard.biz/bbbiz/industry/record-labels/warner-music-ceo-edgar-bronfman-jr-and-chairman-1005322912.story
 ↑ Fitzgerald, Michael. [1], Fast Company, "How Warner Music and Its Musicians Are Combating Declining Album Sales", 1, July 2010.
 ↑ Cox, Rob. "Warner Music Is Singing Again", The New York Times, 24 May 2009.
 ↑ Arango, Tim. "Digital Sales Surpass CDs at Atlantic", The New York Times, 25 November 2008.
 ↑ Smith, Ethan, "Deal Values Warner Music at $3.3 Billion", Wall Street Journal, May 6, 2011. Retrieved 2011-05-06.
 ↑ "Warner Music Group being sold for $1.3 billion to Access Industries amid industry decline", Associated Press via Washington Post, May 6, 1:38 PM EDT. Retrieved 2011-05-06.
 ↑ http://www.billboard.biz/bbbiz/industry/record-labels/warner-music-ceo-edgar-bronfman-jr-and-chairman-1005322912.story
 ↑ http://www.billboard.biz/bbbiz/industry/record-labels/updated-edgar-bronfman-jr-to-step-down-as-1005616802.story
 ↑ http://www.billboard.biz/bbbiz/industry/record-labels/video-edgar-bronfman-jr-says-warner-music-1006058352.story
 ↑ "Seagram chairman admits he never approved of son's marriage to a Black woman", Jet, 23 March 1998.
 ↑ Auletta, Ken. "Rising Son", The New Yorker, 6 June 1994.
 ↑ Women's Wear Daily: "Hannah Bronfman Saves the World" by Alessandra Codinha August 8, 2011
 ↑ Solo Genealogia: Clarisa Alcock San Román retrieved April 8, 2012
 ↑ Maclean's Magazine quoted by The Canadian Encyclopedia Historica: "Bronfman Sells DuPont" April 17, 1995
 ↑ Ex-Vivendi executives found guilty. Financial Times. Retrieved on 2011-01-21.
 ↑ http://www.ctv.ca/CTVNews/TopStories/20110121/bronfman-financial-case-french-court-110121/
 ↑ http://www.wmg.com/newsdetails/id/8a0af8122d9dba3f012da8f5a5410106
 ↑ Charles C. Mann (2000-09-01). "The Heavenly Jukebox". The Atlantic Monthly. http://www.theatlantic.com/issues/2000/09/mann.htm. 
 ↑ Warner Music adding social networking to websites
 ↑ Warner makes deal with YouTube
 ↑ Three Major Record Labels Join the 'Choruss'
 ↑ "Will The Recording Industry Sue Edgar Bronfman For Downloading?". TechDirt. 4 December 2006. http://www.techdirt.com/articles/20061204/003837.shtml. 
 ↑ Adam Reuters (1 December 2006). "Interview: Warner Music Group boss Edgar Bronfman". Reuters. http://secondlife.reuters.com/stories/2006/12/01/warner-music-boss-edgar-bronfman/. 
 ↑ Duncan Riley (14 November 2007). "Warner Music Boss: We Were Wrong". techcrunch. http://www.techcrunch.com/2007/11/14/warner-music-boss-we-were-wrong/. 
 ↑ Ian Youngs (10 February 2010). "Warner retreats from free music streaming". BBC. http://news.bbc.co.uk/2/hi/entertainment/8507885.stm. 
 McQueen, Rod. The Icarus Factor: The Rise and Fall of Edgar Bronfman Jr., 2004. ISBN 0-385-65995-4 Faith, Nicholas. The Bronfmans: The Rise and Fall of the House of Seagram, 2006. ISBN 0-312-33219-X Edgar Bronfman, Jr.'s biography at WMG Corporate Site Official page for Bronfman's book about Judaism in America Speaking at Stanford "Edgar Bronfman Jr." at hollywood.com Milner, Brian. "The Unmaking of a Dynasty", Cigar Aficionado Plotz, David. "Edgar Bronfman, Edgar Bronfman: Overrated father, misunderstood son", Slate, 26 April 1998 Edgar Bronfman, Jr. biography at Directors of Endeavor Big Beat Records Elektra Records Fueled by Ramen Roadrunner Records East West Records Rykodisc ATCO Records Del-Fi Records Frank Sinatra Enterprises Eleven: A Music Company Maverick Records Nonesuch Records Reprise Records Sire Records Warner Bros. Records Warner Bros. Records Nashville Reprise Records Nashville Word Label Group (Fervent Records Myrrh Records Word Records) Warner Music Vision WEA International (Warner Music Australasia Warner Music Canada) Warner-Tamerlane Publishing Corp. WB Music Corp. Warner Custom Products Warner Music Group Soundtracks WMG Film, Television & Commercial Licensing Warner Strategic Marketing Rhino Entertainment Stephen Cooper Mike Caren Michael Fleisher Lyor Cohen Julie Greenwald Jac Holzman Robert Hurwitz John Janick  Craig Kallman Kevin Liles Todd Moscowitz Scott Pascucci Dairmuid Quinn Seymour Stein Cees Wessels Tom Whalley Articles in need of neutralization Wikipedia articles needing rewrite from February 2010 1955 births Living people American billionaires American people of Canadian-Jewish descent American mass media owners American music industry executives Bronfman family Collegiate School (New York) alumni People from New York City Pages using duplicate arguments in template calls Articles with hCards Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 23 August 2014, at 03:28. Privacy policy About Metapedia Disclaimers 
  may need to be rewritten entirely to comply with Wikipedia's quality standards Edgar Miles Bronfman, Jr. Junior Miles Sam Roman Benjamin Vanessa Hannah – Chief Executive Officer of Warner Music Group Stephen Cooper 
  This article may need to be rewritten entirely to comply with Wikipedia's quality standards. You can help. The discussion page may contain suggestions. Bronfman, CEO of Warner Music Group Edgar Miles Bronfman, Jr.May 16, 1955 (1955-05-16) (age 64)New York City, U.S.A.  American CEO of Warner Music Group from 2004 to 2011  Chairman of Warner Music Group from 2011 to 2012 $2,500,000,000 Stephen Cooper Sherry Brewer (1979–1991)  Clarisa Alcock San Román (1993— ) Benjamin Bronfman  Vanessa  Hannah  Aaron  Bettina  Erik  Clarissa Edgar Bronfman, Sr. Ann Loeb Samuel Bronfman (grandfather) “
 We used to fool ourselves ... We used to think our content was perfect just exactly as it was. We expected our business would remain blissfully unaffected even as the world of interactivity, constant connection and file sharing was exploding. And of course we were wrong. How were we wrong? By standing still or moving at a glacial pace, we inadvertently went to war with consumers by denying them what they wanted and could otherwise find and as a result of course, consumers won.
 ”
 “
 The 'get all your music you want for free, and then maybe with a few bells and whistles we can move you to a premium price' strategy is not the kind of approach to business that we will be supporting in the future.
 ”
 Preceded by–
 Chief Executive Officer of Warner Music Group2004 – August 19, 2011
 Succeeded byStephen Cooper
 v • d • e
Warner Music GroupAtlantic Records Group* Atlantic Records
Big Beat Records
Elektra Records
Fueled by Ramen
Roadrunner RecordsAlternative Distribution Alliance(formerly Independent Label Group)* Cordless Recordings
East West Records
RykodiscRhino Entertainment* Rhino Records
ATCO Records
Del-Fi Records
Frank Sinatra Enterprises
Eleven: A Music CompanyWarner Bros. Records Group* Asylum Records
Maverick Records
Nonesuch Records
Reprise Records
Sire Records
Warner Bros. RecordsWarner Music Nashville* Atlantic Records Nashville
Warner Bros. Records Nashville
Reprise Records Nashville
Word Label Group (Fervent Records
Myrrh Records
Word Records)Distribution* Alternative Distribution Alliance
Warner Music Vision
WEA International (Warner Music Australasia
Warner Music Canada)Music publishing and licensing* Warner/Chappell Music
Warner-Tamerlane Publishing Corp.
WB Music Corp.
Warner Custom Products
Warner Music Group Soundtracks
WMG Film, Television & Commercial Licensing
Warner Strategic Marketing
Rhino EntertainmentManagement* Edgar Bronfman, Jr.
Stephen Cooper
Mike Caren
Michael Fleisher
Lyor Cohen
Julie Greenwald
Jac Holzman
Robert Hurwitz
John Janick  Craig Kallman
Kevin Liles
Todd Moscowitz
Scott Pascucci
Dairmuid Quinn
Seymour Stein
Cees Wessels
Tom Whalley  Atlantic Records Group * Atlantic Records
Big Beat Records
Elektra Records
Fueled by Ramen
Roadrunner Records  Alternative Distribution Alliance(formerly Independent Label Group) * Cordless Recordings
East West Records
Rykodisc  Rhino Entertainment * Rhino Records
ATCO Records
Del-Fi Records
Frank Sinatra Enterprises
Eleven: A Music Company  Warner Bros. Records Group * Asylum Records
Maverick Records
Nonesuch Records
Reprise Records
Sire Records
Warner Bros. Records  Warner Music Nashville * Atlantic Records Nashville
Warner Bros. Records Nashville
Reprise Records Nashville
Word Label Group (Fervent Records
Myrrh Records
Word Records)  Distribution * Alternative Distribution Alliance
Warner Music Vision
WEA International (Warner Music Australasia
Warner Music Canada)  Music publishing and licensing * Warner/Chappell Music
Warner-Tamerlane Publishing Corp.
WB Music Corp.
Warner Custom Products
Warner Music Group Soundtracks
WMG Film, Television & Commercial Licensing
Warner Strategic Marketing
Rhino Entertainment  Management * Edgar Bronfman, Jr.
Stephen Cooper
Mike Caren
Michael Fleisher
Lyor Cohen
Julie Greenwald
Jac Holzman
Robert Hurwitz
John Janick  Craig Kallman
Kevin Liles
Todd Moscowitz
Scott Pascucci
Dairmuid Quinn
Seymour Stein
Cees Wessels
Tom Whalley NAME
 Bronfman, Jr., Edgar
 ALTERNATIVE NAMES
 
 SHORT DESCRIPTION
 
 DATE OF BIRTH
 16 May 1955
 PLACE OF BIRTH
 New York City, U.S.A.
 DATE OF DEATH
 
 PLACE OF DEATH
 
 Edgar Bronfman, Jr. Contents Early life Career Personal life Insider trading conviction Music piracy See also References and notes External links Navigation menu Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 