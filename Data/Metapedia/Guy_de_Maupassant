Medieval
16th century · 17th century
18th century · 19th century
20th century · Contemporary
 Chronological list 
Writers by category 
Novelists ·
Playwrights 
Poets ·
Essayists 
Short story writers
 Henri René Albert Guy de Maupassant[1] (5 August 1850 – 6 July 1893) was a popular 19th-century French writer, considered one of the fathers of the modern short story and one of the form's finest exponents.
 A protégé of Flaubert, Maupassant's stories are characterized by their economy of style and efficient, effortless dénouement. Many of the stories are set during the Franco-Prussian War of the 1870s and several describe the futility of war and the innocent civilians who, caught in the conflict, emerge changed. He also wrote six novels.
 Henri-René-Albert-Guy de Maupassant was born on August 5, 1850 at the château de Miromesnil, near Dieppe in the Seine-Inférieure (now Seine-Maritime) department. He was the first son of Laure Le Poittevin and Gustave de Maupassant, both from prosperous bourgeois families. When Maupassant was eleven and his brother Hervé was five, his mother, an independent-minded woman, risked social disgrace to obtain a legal separation from her husband.
 After the separation, Le Poittevin kept her two sons, the elder Guy and younger Hervé. With the father’s absence, Maupassant’s mother became the most influential figure in the young boy’s life. She was an exceptionally well read woman and was very fond of classical literature, especially Shakespeare. Until the age of thirteen, Guy happily lived with his mother, to whom he was deeply devoted, at Étretat, in the Villa des Verguies, where, between the sea and the luxuriant countryside, he grew very fond of fishing and outdoor activities. At age thirteen, he was sent to a small seminary near Rouen for classical studies.
 In October 1868, at the age of 18, he saved the famous poet Algernon Charles Swinburne from drowning off the coast of Étretat at Normandy.[2] As he entered junior high school, he met the great author Gustave Flaubert.
 He first entered a seminary at Yvetot, but deliberately got himself expelled. From his early education he retained a marked hostility to religion. Then he was sent to the Lycée Pierre-Corneille in Rouen[3] where he proved a good scholar indulging in poetry and taking a prominent part in theatricals.
 The Franco-Prussian War broke out soon after his graduation from college in 1870; he enlisted as a volunteer and fought bravely. Afterwards, in 1871, he left Normandy and moved to Paris where he spent ten years as a clerk in the Navy Department. During these ten tedious years his only recreation and relaxation was canoeing on the Seine on Sundays and holidays.
Gustave Flaubert took him under his protection and acted as a kind of literary guardian to him, guiding his debut in journalism and literature. At Flaubert's home he met Émile Zola and the Russian novelist Ivan Turgenev, as well as many of the proponents of the realist and naturalist schools.
 In 1878 he was transferred to the Ministry of Public Instruction and became a contributing editor of several leading newspapers such as Le Figaro, Gil Blas, Le Gaulois and l'Écho de Paris. He devoted his spare time to writing novels and short stories.
 In 1880 he published what is considered his first masterpiece, "Boule de Suif", which met with an instant and tremendous success. Flaubert characterized it as "a masterpiece that will endure." This was Maupassant's first piece of short fiction set during the Franco-Prussian War, and was followed by short stories such as "Deux Amis", "Mother Savage", and "Mademoiselle Fifi".
 The decade from 1880 to 1891 was the most fertile period of Maupassant's life. Made famous by his first short story, he worked methodically and produced two or sometimes four volumes annually. He combined talent and practical business sense, which made him wealthy.
 In 1881 he published his first volume of short stories under the title of La Maison Tellier; it reached its twelfth edition within two years; in 1883 he finished his first novel, Une Vie (translated into English as A Woman's Life), 25,000 copies of which were sold in less than a year. In his novels, he concentrated all his observations scattered in his short stories. His second novel Bel-Ami, which came out in 1885, had thirty-seven printings in four months.
 His editor, Havard, commissioned him to write new masterpieces and Maupassant continued to produce them without the slightest apparent effort. At this time he wrote what many consider to be his greatest novel, Pierre et Jean.
 With a natural aversion to society, he loved retirement, solitude, and meditation. He traveled extensively in Algeria, Italy, England, Brittany, Sicily, Auvergne, and from each voyage brought back a new volume. He cruised on his private yacht "Bel-Ami," named after his earlier novel. This feverish life did not prevent him from making friends among the literary celebrities of his day: Alexandre Dumas, fils had a paternal affection for him; at Aix-les-Bains he met Hippolyte Taine and fell under the spell of the philosopher-historian.
 Flaubert continued to act as his literary godfather. His friendship with the Goncourts was of short duration; his frank and practical nature reacted against the ambience of gossip, scandal, duplicity, and invidious criticism that the two brothers had created around them in the guise of an 18th-century style salon.
 Maupassant was but one of a fair number of 19th-century Parisians who did not care for the Eiffel tower; indeed, he often ate lunch in the restaurant at its base, not out of any preference for the food, but because it was only there that he could avoid seeing its otherwise unavoidable profile.[4] Moreover, he and forty-six other Parisian literary and artistic notables attached their names to letter of protest, ornate as it was irate, against the tower's construction to the then Minister of Public Works.[5]
 Maupassant also wrote under several pseudonyms such as Joseph Prunier, Guy de Valmont, and Maufrigneuse (which he used from 1881 to 1885).
 In his later years he developed a constant desire for solitude, an obsession for self-preservation, and a fear of death and crazed paranoia of persecution, that came from the syphilis he had contracted in his early days. On January 2, in 1892, Maupassant tried to commit suicide by cutting his throat and was committed to the celebrated private asylum of Dr. Esprit Blanche at Passy, in Paris, where he died on July 6, 1893.
 Guy De Maupassant penned his own epitaph: "I have coveted everything and taken pleasure in nothing." He is buried in Section 26 of the Cimetière du Montparnasse, Paris.
 Maupassant is considered one of the fathers of the modern short story. He delighted in clever plotting, and served as a model for Somerset Maugham and O. Henry in this respect. His stories about expensive jewelry ("The Necklace", "Les Bijoux") are imitated with a twist by Maugham ("Mr Know-All", "A String of Beads") and Henry James.
 Taking his cue from Balzac, Maupassant wrote comfortably in both the high-Realist and fantastic modes; stories and novels such as "L'Héritage" and Bel-Ami aim to recreate Third Republic France in a realistic way, whereas many of the short stories (notably "Le Horla" and "Qui sait ?") describe apparently supernatural phenomena.
 The supernatural in Maupassant, however, is often implicitly a symptom of the protagonists' troubled minds; Maupassant was fascinated by the burgeoning discipline of psychiatry, and attended the public lectures of Jean-Martin Charcot between 1885 and 1886.[6] This interest is reflected in his fiction.
 Maupassant is notable as the subject of one of Leo Tolstoy's essays on art: The Works of Guy de Maupassant.
 Friedrich Nietzsche's autobiography mentions him in the following text:
 "I cannot at all conceive in which century of history one could haul together such inquisitive and at the same time delicate psychologists as one can in contemporary Paris: I can name as a sample – for their number is by no means small, ... or to pick out one of the stronger race, a genuine Latin to whom I am particularly attached, Guy de Maupassant."
 1 Biography 2 Significance 3 Criticism 4 Bibliography

4.1 Novels
4.2 Short-story collections
4.3 Travel writing
4.4 Poetry

 4.1 Novels 4.2 Short-story collections 4.3 Travel writing 4.4 Poetry 5 References 6 External links Une vie (1883) Bel-Ami (1885) Mont-Oriol (1887) Pierre et Jean (1888) Fort comme la mort (1889) Notre Cœur (1890) Les Soirées de Médan (with Zola, Huysmans et al. Contains Boule de Suif by Maupassant) (1880) La Maison Tellier (1881) Mademoiselle Fifi (1882) Contes de la bécasse (1883) A Vendetta (1883) Miss Harriet (1884) Les Sœurs Rondoli (1884) Clair de lune (1884) (contains "Les Bijoux") Yvette (1884) Toine (1885) Contes du jour et de la nuit (1885) (contains "La Parure" or "The Necklace") Monsieur Parent (1886) La Petite Roque (1886) Le Horla (1887) Le Rosier de Madame Husson (1888) La Main gauche (1889) L'Inutile Beauté (1890) La Main d'Ecorche Au soleil (1884) Sur l'eau (1888) La Vie errante (1890) Des vers (1880) ↑ French pronunciation: [ɡi də mopasɑ̃]
 ↑ Clyde K. Hyder, Algernon Swinburne: The Critical Heritage, 1995, p.185
 ↑ Lycée Pierre Corneille de Rouen - History
 ↑ Barthes, Roland. The Eiffel Tower and Other Mythologies. Tr. Howard, Richard. Berkeley: University of California Press. ISBN 978-0-520-20982-4. Page 1.
 ↑ Harriss, Joseph. The Tallest Tower. Unlimited Publishing LLC, 2004. ISBN 1-58832-102-9. Pages 15, 16.
 ↑ Pierre Bayard, Maupassant, juste avant Freud (Paris: Minuit, 1998)
 Maupassantiana, a French Scholar Website on Maupassant and his Works. Works by or about Guy de Maupassant at Internet Archive (scanned books original editions color illustrated) Works by Guy de Maupassant at Project Gutenberg (plain text and HTML) Works by Guy de Maupassant at LibriVox (audiobooks) Université McGill: le roman selon les romanciers Recensement et analyse des écrits non romanesques de Guy de Maupassant Works by Guy de Maupassant at Online Literature (HTML) Works by Guy de Maupassant in Ebooks (French) Works by or about Guy de Maupassant in libraries (WorldCat catalog) Works by Guy de Maupassant (text, concordances and frequency list) Maupassant biography Donald Adamson Bed 29 & Other Stories: an anthology of 26 of Maupassant's short stories (1993) Articles needing additional references from March 2011 All articles needing additional references 1850 births 1893 deaths French novelists French short story writers French fantasy writers 19th-century French writers French military personnel of the Franco-Prussian War Deaths from syphilis Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 5 November 2011, at 08:58. Privacy policy About Metapedia Disclaimers 
  needs additional citations for verification Henri René Albert Guy de Maupassant Guy de Maupassant Guy de Maupassant Guy de Maupassant 
  This article needs additional citations for verification. Please help improve this article by adding reliable references. Unsourced material may be challenged and removed. (March 2011) 
 Montparnasse Cemetery
 Novelist, short story writer, poet
 French-German
 Naturalism, Realism
 
 
Influences
Honoré de Balzac, Gustave Flaubert, Hippolyte Taine, Émile Zola

 
 
Influenced
Raymond Carver, Anton Chekhov, O. Henry, Henry James, H. P. Lovecraft, W. Somerset Maugham

 
 
 By category
 
Medieval
16th century · 17th century
18th century · 19th century
20th century · Contemporary

 
Chronological list 
Writers by category 
Novelists ·
Playwrights 
Poets ·
Essayists 
Short story writers

   Wikiquote has a collection of quotations related to: Guy de Maupassant    Wikimedia Commons has media related to: Guy de Maupassant    Wikisource has original works written by or about: Guy de Maupassant  NAME
 Maupassant, Guy De
 ALTERNATIVE NAMES
 
 SHORT DESCRIPTION
 
 DATE OF BIRTH
 5 August 1850
 PLACE OF BIRTH
 
 DATE OF DEATH
 6 July 1893
 PLACE OF DEATH
 
 Guy de Maupassant Contents Biography Significance Criticism Bibliography References External links Navigation menu Novels Short-story collections Travel writing Poetry Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 