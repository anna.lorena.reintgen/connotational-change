Neofolk is a form of folk music-inspired experimental music that emerged from European ideals and post-industrial music circles. Neofolk can either be solely acoustic folk music or a blend of acoustic folk instrumentation aided by varieties of accompanying sounds such as pianos, strings and elements of industrial music and experimental music. The genre encompasses a wide assortment of themes including traditional music, heathenry, romanticism and occultism. Neofolk musicians often have ties to other genres such as neoclassical and martial industrial, or have links with Heathen circles or other countercultural elements.
 The name and themes have certain similarities with the völkisch movement.
 The term "neofolk" originates from esoteric music circles who started using the term in the late 20th century to describe music influenced by musicians such as Douglas Pearce (Death In June), Tony Wakeford (Sol Invictus) and David Tibet (Current 93) who had collaborated heavily for a period of time. These musicians were part of a post-industrial music circle who later on incorporated folk music based upon traditional and European elements into their sound. Folk musicians such as Vulcan's Hammer, Changes and Comus had created music with similar sounds and themes to neofolk as far back as the 1960s. These musicians could be considered harbingers of the sound that later influenced the neofolk artists. However, the distinction must be made that it was the aforementioned artists who were involved in the dark music scene throughout the 1980s and 1990s that contributed specifically to the emergence of neofolk. Neofolk is seen by many as an extension of post-industrial music into the folk music genre which did not occur until the late 20th century.

 The spirit of neofolk contains parallels to the ideals of American and British folk movements of the 1960s. The basis of this music is built upon principles against commercialization and popular culture. However the themes of neofolk and folk music are drastically different. A majority of artists within the neofolk genre focus on archaic, cultural and literary references. Local traditions and indigenous beliefs tend to be portrayed heavily as well as esoteric and historical topics.
 Of particular mention is Heathenry. This subject plays a large part in the thematic elements touched upon by many modern and original neofolk artists. Runes, heathen European sites and other means of expressing an interest in the ancient and ancestral occurs often in neofolk music. Aesthetically, references to this subject occur within band names, album artwork, clothing and various other means of artistic expression. This has led to some forefathers of the genre and current artists within the genre attributing it to being an aspect of a broader Heathen revival.[1]
 As a descriptor, apocalyptic folk predates neofolk and was used by David Tibet to describe the music of his band Current 93 during a period in the late 1980s and early 1990s. Initially, Tibet did not intend to imply connection with the folk music genre; rather, that Current 93 was "apocalyptic folk music" or music made by apocalyptic people.[1]
 The term was applied to most artists on the now-defunct World Serpent Distribution company and music influenced directly by C93's Thunder Perfect Mind era. Gnostic and Thelemic themes are often featured in the works of these artists, as well as influences from 1960s psychedelic rock and psych folk. It is also sometimes used to describe those of similar musical distinction but not directly influenced or associated, such as Michael Gira.
 Folk Noir was a term originally coined by photographer David Mearns to describe the music of mid-period Sol Invictus. It is generally related to Tursa Records-related bands. It is sometimes found on webzines as a more neutral term, without the specific connotations of "neofolk", but the meaning is largely the same though the usage of the term 'noir' hints at an overall dark subject matter relating to historical, often British, subjects.
 Other vague terms sometimes used to describe artists of this genre include "dark folk" and "pagan folk". These terms are umbrella terms that also describe various other forms of unrelated music.
 Martial Industrial or military pop is a genre that shares a lot in common with neofolk and developed very close to it. A number of artists that could be classified as neofolk also regularly work with and play shows with martial industrial acts or produce martial industrial.
 Other related styles include dark ambient, neoclassical, dark cabaret, folk metal, industrial and post-industrial music or a mixture of all these, such as music created that fits under the heading of martial industrial.
      1 History 2 Culture

2.1 Heathenry

 2.1 Heathenry 3 Related terms and styles

3.1 Apocalyptic folk
3.2 Folk Noir
3.3 Martial Industrial

 3.1 Apocalyptic folk 3.2 Folk Noir 3.3 Martial Industrial 4 Artists 5 Events 6 References

6.1 Footnotes
6.2 Bibliography

 6.1 Footnotes 6.2 Bibliography 7 See also 8 External links 16 Horsepower Agalloch Angels of Light Ataraxia[2] Belborn[3] Blood Axis Camerata Mediolanense Changes[4] Current 93 Darkwood Death in June[5] Empyrium Faun[6] Fire + Ice Forseti[7] Harvest Rain[8] H.E.R.R. Horse Feathers In Gowan Ring Lord Wind Love Is Colder Than Death[9] Luftwaffe Mizar The Moon lay hidden beneath a Cloud[10] Murzik Nature and Organisation Narsilion[11] Nest[12] Neun Welten Of the Wand & the Moon[13] Omnia Ordo Rosarius Equilibrio[14] Orplid[6] Qntal[15] Rome[16] Sol Invictus[17] Sonne Hagal[18] Spiritual Front[19] Sturmpercht Tenhi[20] Unto Ashes Von Thronstahl Werkraum Emily Jane White[21] The Winding Stair Winglord Wovenhand Flammenzauber Mėnuo Juodaragis ↑  "I'm very happy about that because I see Death In June as part of a European cultural revival. I'm pleased that the Old Gods are being resurrected, for want of a better word. Old symbols. I feel very pleased that I am a part of that process and that I have had influence. At this stage in the game, so to speak, it's not false modesty to say that I am content with my influence." Powell, Erin. Interview with Douglas Pearce, 2005.
 ↑ Ataraxia. FluxEuropa.
 ↑ Interview with Belborn. FluxEuropa.
 ↑ Changes. Discogs.
 ↑ Powell, Erin (April 9, 2005). Interview with Douglas Pearce. Death in June.
 ↑ 6.0 6.1 Schurmann, Martin (April 1, 2006). Neofolk – mehr als nur eine Musikrichtung (German).
 ↑ Interview: Ähren im Sturm (German). Ikonen-Magazin.
 ↑ Interviews:Harvest Rain. Heathen Harvest.
 ↑ Love Is Colder Than Death. Darktronica.
 ↑ The Moon lay hidden beneath a Cloud. FluxEuropa.
 ↑ Narsilion. Darktronica.
 ↑ Nest. Darktronica.
 ↑ Of the Wand & the Moon. The Metal Archives.
 ↑ Ordo Rosarius Equilibrio. Darktronica.
 ↑ Qntal. Darktronica.
 ↑ Rome. Darktronica.
 ↑ Sol Invictus - The Devil's Steed. Heathen Harvest (June 3, 2005).
 ↑ Sonne Hagal Interview. Heathen Harvest (January 23, 2006).
 ↑ Spiritual Front. Darktronica.
 ↑ Tenhi. Darktronica.
 ↑ http://www.slantmagazine.com/music/review/emily-jane-white-dark-undercoat/1715
 Diesel, Andreas (2005). Looking for Europe - Neofolk und Hintergründe. Zeltingen-Rachtig. ISBN 3-936878-02-1.  Radical Traditionalism Compulsion Flux Europa Heathen Harvest IRONFLAME.de Judas Kiss Neo-form Webzine Soleilnoir Heimdallr webzine Content from Wikipedia Neofolk Folk music Music genres Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information Deutsch Français Hrvatski Svenska  This page was last edited on 17 October 2018, at 16:09. Privacy policy About Metapedia Disclaimers 
  Neofolk apocalyptic folk Folk Noir military pop Neofolk Main article: Martial industrial Main article: Martial industrial FolkPost-punkExperimentalIndustrial 1980s, Europe Folk instruments Minor, mainly Europe Martial industrial Dark folk Post-industrial 
16 Horsepower
Agalloch
Angels of Light
Ataraxia[2]
Belborn[3]
Blood Axis
Camerata Mediolanense
Changes[4]
Current 93
Darkwood
Death in June[5]
Empyrium

 
Faun[6]
Fire + Ice
Forseti[7]
Harvest Rain[8]
H.E.R.R.
Horse Feathers
In Gowan Ring
Lord Wind
Love Is Colder Than Death[9]
Luftwaffe
Mizar
The Moon lay hidden beneath a Cloud[10]

 
Murzik
Nature and Organisation
Narsilion[11]
Nest[12]
Neun Welten
Of the Wand & the Moon[13]
Omnia
Ordo Rosarius Equilibrio[14]
Orplid[6]
Qntal[15]
Rome[16]

 
Sol Invictus[17]
Sonne Hagal[18]
Spiritual Front[19]
Sturmpercht
Tenhi[20]
Unto Ashes
Von Thronstahl
Werkraum
Emily Jane White[21]
The Winding Stair
Winglord
Wovenhand

 
  This article isa part of the portalNeofolk
 Neofolk Contents History Culture Related terms and styles Artists Events References See also External links Navigation menu Heathenry Apocalyptic folk Folk Noir Martial Industrial Footnotes Bibliography Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools In other languages 