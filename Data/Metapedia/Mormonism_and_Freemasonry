Freemasonry · Grand Lodge · Masonic Lodge · Masonic Lodge Officers · Grand Master · Prince Hall Freemasonry · Regular Masonic jurisdictions
 History of Freemasonry · Liberté chérie · Masonic manuscripts
 The relationship between Mormonism and Freemasonry began early in the life of Mormon founder Joseph Smith, Jr., as his older brother and possibly his father were Freemasons while the family lived near Palmyra, New York. Nevertheless, in the late 1820s, the western New York region was swept with anti-Masonic fervor, and the Book of Mormon, a foundational religious book published by Smith in 1830, is generally considered to reflect that anti-Masonic sentiment by condemning what it portrays as oath-bound conspiratorial organizations.
 By the 1840s, however, Smith and several prominent Mormons had become Freemasons and founded a lodge in Nauvoo, Illinois, in March 1842. Soon after joining Freemasonry, Smith introduced a new temple "Endowment" ceremony including a number of symbolic elements that were essentially identical with their analogues within Freemasonry. Smith remained a Freemason until his death; however, later Mormon leaders distanced themselves from Freemasonry. In modern times, The Church of Jesus Christ of Latter-day Saints (LDS Church), the predominant Mormon organization, holds no position for or against the compatibility of Masonry with LDS Church doctrine.
 A significant numbers of leaders in the early Latter Day Saint movement were Masons prior to their involvement in the movement. Notable examples include Brigham Young, Heber C. Kimball, John C. Bennett, Hyrum Smith and Joseph Smith, Sr.
 In the early 1840s a Masonic Lodge was formed by LDS Church members who were Freemasons. Joseph Smith, Jr. and his brother Hyrum became members of the newly formed Nauvoo lodge. It appears that John C. Bennett had a particularly strong influence in the spread of Freemasonry, and soon over 1,500 Mormon men in the city of Nauvoo were practicing Masons. LDS historian Reed Durham writes:
 "By 1840, John Cook Bennett, a former active leader in Masonry had arrived in Commerce and rapidly exerted his persuasive leadership in all facets of the Church, including Mormon Masonry. ... Joseph and Sidney [Rigdon] were inducted into formal Masonry ... on the same day..." being made "Masons on Sight" by the Illinois Grandmaster.("Is There No Help for the Widow's Son?" by Dr. Reed C. Durham, Jr., as printed in "Joseph Smith and Masonry: No Help for the Widow's Son", Martin Pub. Co., Nauvoo, Ill., 1980, p. 17.) (This freed Joseph from having to complete the ritual and memorization necessary to work one's way through the first three degrees.) Making one "A Mason on Sight" is generally reserved as an honor and is a rarity in occurrence.
 In 1842 Smith became a Master Mason, as indicated by in the History of the Church:
 Tuesday, [March] 15. — I officiated as grand chaplain at the installation of the Nauvoo Lodge of Free Masons, at the Grove near the Temple. Grand Master Jonas, of Columbus, being present, a large number of people assembled on the occasion. The day was exceedingly fine; all things were done in order, and universal satisfaction was manifested. In the evening I received the first degree in Freemasonry in the Nauvoo Lodge, assembled in my general business office. (History of the Church, by Joseph Smith, Deseret Book, 1978, Vol.4, Ch.32, p.550-1)
 Joseph Smith was raised to the third degree of master mason "on sight" by Grand Master Jonas of the Grand Lodge of Illinois. This was fully within Jonas' right of office, but was a fairly rare procedure.[1]
 Wednesday, March 16. — I was with the Masonic Lodge and rose to the sublime degree. (History of the Church, Vol.4, Ch.32, p.552)
 In The Mormon Church and Freemasonry (2001), Terry Chateau writes:
 [The Joseph Smith family] was a Masonic family which lived by and practiced the estimable and admirable tenets of Freemasonry. The father, Joseph Smith, Sr., was a documented member in upstate New York. He was raised to the degree of Master Mason on May 7, 1818 in Ontario Lodge No. 23 of Canandaigua, New York. An older son, Hyrum Smith, was a member of Mount Moriah Lodge No. 112, Palmyra New York.
 It should be noted that Hyrum Smith was not only Joseph's older brother, but succeeded their father as Patriarch to the Church and Oliver Cowdery as assistant president of the Church (they were the only two men who held this office) and was always closely relied on by Joseph Smith.
 Problems arose concerning the special dispensation granted to the Nauvoo Lodge, brought by Bodley Lodge No. 1, and on August 11, 1842 the special dispensation was suspended by the Grand Master until the annual Communication of the Illinois Grand Lodge[2] "During the short period covering its activities, this Lodge initiated 286 candidates and raised almost as many. John C. Bennett reports an instance in which sixty-three persons were elected on a single ballot."[2] This suspension was later lifted and the Mormon Lodges resumed work although several irregularities in their practice were noted. The irregularities centered on mass balloting (voting on more than one candidate at a time) and not requiring proficiency in each degree before proceeding to the next degree (in many cases, initiates were being passed to the Fellowcraft degree and raised to the Master Mason degree within two days of being initiated as an Entered Apprentice).[2] In 1844, the Mormon Lodges (of which there were five at that time) were ordered to cease work by the Grand Lodge,[3] although they ignored the order and continued to function as clandestine lodges until Smith's death.
 Mormon temple worship shares an extensive commonality of symbols, signs, vocabulary and clothing with Freemasonry, including robes, aprons, handshakes, ritualistic raising of the arms, etc.[4] The interpretation of many of these symbols has been adapted to the Mormon narrative from their original meanings in Freemasonry. For example, whereas Masons exchange secret handshakes to identify fellow Freemasons, Mormonism teaches that these handshakes must be given to sentinel angels in order for Mormons to be admitted into the highest kingdom of heaven. Mormon temple garments also bear the Masonic symbols of the Square and Compass, although Mormons have imbued these symbols with religious meaning that exceeds the meaning of the symbols as intended by Freemasonry.
 In the "Temple and Salvation for the Dead" part of Discourses of Brigham Young, Brigham Young gives a quote about the temple which directly relates to the story of Hiram Abiff from Masonic folklore. Although Young changed some of the key masonic aspects about Hiram to fit better with Mormonism's view of the temple, the story is the same.
 It is true that Solomon built a temple for the purpose of giving endowments, but from what we can learn of the history of that time they gave very few if any endowments, and one of the high priests [Hiram Abiff] was murdered by wicked and corrupt men, who had already begun to apostatize, because he would not reveal those things appertaining to the priesthood that were forbidden him to reveal until he came to the proper place. (Discourses of Brigham Young, compiled by John A Widtsoe, Deseret Book Company, 1977)
 When Smith was in the Carthage Jail in 1844, after he fired his last round in a small pepper-box pistol (which had been given to him that morning by Cyrus Wheelock), he held up his arms and may have been giving the Masonic call of distress, hoping Masons in the contingent would honor this call and not fire on him. It is recorded that he ran towards the open window with uplifted hands, and proclaimed, "Oh Lord my God."[5] Most people saw this as only a plea to God for aid, although others suspect otherwise.[6] This phrase, "Oh, Lord, my God, is there no help for the widow's son?" is the sign/token of a Master Mason in distress; a Mason is bound by honor to come to the utterer's aid if there is a greater chance of saving the life of the seeker than on losing his own.[7]
 From 1925 to 1984 the Grand Lodge of Utah prohibited Latter-day Saints from joining, but no other Grand Lodge followed this ban and Mormons were free to join Lodges outside Utah. In 1984 the Grand Lodge of Utah officially dropped its anti-Mormon position and allowed LDS church members to join. Today there is no formal obstacle in Utah or in any other Grand Lodge preventing Mormons from becoming Freemasons.
 The presidency of the LDS Church has not made an official statement as to whether or not Freemasonry is compatible with Mormonism. However Don LeFevre, a past spokesman for the church has said the church "...strongly advises its members not to affiliate with organizations that are secret, oath-bound, or would cause them to lose interest in church activities."[8] There are many LDS Masons in Utah and other Grand Lodges who serve and have served in various leadership positions, including Grand Masters, other Grand Officers, and Worshipful Masters.
 1 Historical connections 2 Similarities in symbology and ritual 3 Modern official LDS Church policy 4 Recent explorations of the issue 5 See also 6 Notes 7 References 8 Further reading 9 External link Clyde R. Forsberg published Equal Rites: The Book of Mormon, Masonry, Gender, and American Culture in 2004 through Columbia University Press.[9] Greg Kearney, an endowed Mormon who is also a Freemason, gave a presentation of the issue of Mormonism and Freemasonry at the 2005 conference of the Foundation for Apologetic Information and Research.[1] In 2009 Matthew B. Brown published Exploring the Connection Between Mormons and Masons.[10] A forthcoming book by Nick Literski, called Method Infinite: Freemasonry and the Mormon Restoration, has been anticipated for some years.[9][11][12][13] Christianity and Freemasonry Walker Lewis Master Mahan Salt Lake Masonic Temple Secret combination (Latter Day Saints) Temple robes: Latter-day Saint tradition ↑ 1.0 1.1 Kearney, Greg (2005), "The Message and the Messenger: Latter-day Saints and Freemasonry", 2005 FAIR Conference (Foundation for Apologetic Information & Research), http://www.fairlds.org/fair-conferences/2005-fair-conference/2005-the-message-and-the-messenger-latter-day-saints-and-freemasonry 
 ↑ 2.0 2.1 2.2 Goodwin (1920).
 ↑ Brodie (1971, p. 367).
 ↑ Goodwin (1920, pp. 54–59).
 ↑ Times and Seasons, vol. 5 no. 13 [July 15, 1844], p. 585
 ↑ Durham, Reed C. (April 20, 1974), Is There No Help For The Widow's Son?, Mormon History Association convention, Nauvoo, Illinois . Unauthorized transcription by Melvin B. Hogan, as found at mormonismi.net.[unreliable source?] Another version of Hogan's transcription as found at CephasMinistry.com.[unreliable source?]
 ↑ Freemasonry Exposed, p. 76[Full citation needed]
 ↑ Salt Lake Tribune Section D1, Monday Feb. 17, 1992[Full citation needed]
 ↑ 9.0 9.1 Literski, Nicholas S. (2005), "Mormonism, Masonry, and Mischief: Clyde Forsberg’s Equal Rites", FARMS Review 17 (1): 1–10, http://mi.byu.edu/publications/review/?vol=17&num=1&id=566, retrieved 2009-12-31. 
 ↑ Literski, Nick (October 29, 2009). Book Review: Exploring the Connection Between Mormons and Masons. Mormon Matters. Retrieved on 2009-12-31.
 ↑ Literski, Nicholas S.. An Introduction to Mormonism and Freemasonry. The Signature Books Library. Signature Books. Retrieved on 2009-12-31.
 ↑ Stack, Peggy Fletcher (January 13, 2006). "Mormon connection to Masons explored ahead of 'Da Vinci Code' sequel". The Salt Lake Tribune. http://www.koffordbooks.com/method/Salt%20Lake%20Tribune%20-%20Search.htm. Retrieved 2009-12-31. 
 ↑ Forthcoming. Greg Kofford Books. Retrieved on 2009-12-31.[not in citation given]
 Anderson, Devery S.; Bergera, Gary James, eds. (2005), Joseph Smith's Quorum of the Anointed, Salt Lake City: Signature Books, ISBN 1-56085-186-4, http://signaturebooks.com/2010/02/quorum-of-the-anointed/ . Brodie, Fawn M. (1971), No Man Knows My History: The Life of Joseph Smith (2nd ed.), New York: Knopf, ISBN 0-394-46967-4 . Brooke, John L. (1994), The Refiner's Fire: The Making of Mormon Cosmology, 1644–1844, Cambridge: Cambridge University Press . Buerger, David John (1987), "The Development of the Mormon Temple Endowment Ceremony", Dialogue: A Journal of Mormon Thought 20 (4): 33–76, http://content.lib.utah.edu/u?/dialogue,20139 . Buerger, David John (2002), The Mysteries of Godliness: A History of Mormon Temple Worship (2nd ed.), Salt Lake City: Signature Books, ISBN 1560851767 . Bullock, Steven C. (1996), Revolutionary Brotherhood: Freemasonry and the Transformation of the American Social Order, 1730-1840, Chapel Hill, NC: University of North Carolina Press . Compton, Todd (1997), In Sacred Loneliness: The Plural Wives of Joseph Smith, Salt Lake City: Signature Books, ISBN 1-56085-085-X . Forsberg, Clyde R. (2004), Equal rites: the Book of Mormon, Masonry, gender, and American culture, New York: Columbia University Press, ISBN 9780231126403 . Goodwin, S.H. (1920), Mormonism and Masonry: Origins, Connections and Coincidences Between Mason and Mormon Temple/Templar Rituals, http://www.phoenixmasonry.org/mormonisn_and_masonry.htm . Homer, Michael W. (1992), "Masonry and Mormonism in Utah, 1847–1984", Journal of Mormon History 18 (2): 57–96, http://content.lib.utah.edu/u?/jmh,16644 . Homer, Michael W., "Similarity of Priesthood in Masonry: The Relationship between Freemasonry and Mormonism", Dialogue: A Journal of Mormon Thought 27 (Fall): 2–113 . Hogan, Mervin B. (1967), "Mormonism and Free Masonry under Covert Masonic Influences", The Royal Arch Mason 9 (Spring): 3–11 . Hoyos, Arturo; Morris, S. Brent (2004), Freemasonry in Context: History, Ritual, Controversy, Lanham, Md.: Lexington Books . Morgan, William (1827), Illustrations of Masonry by One of the Fraternity Who has devoted Thirty Years to the Subject: "God said, Let there be Light, and there was light", Batavia, N.Y.: David C. Miller, http://utlm.org/onlinebooks/captmorgansfreemasonrycontents.htm . Walgren, Kent L. (1982), "James Adams: Early Springfield Mormon and Freemason", Journal of the Illinois State Historical Society 75 (Summer): 121–36 . Ivins, Anthony W. (1934), The Relationship of "Mormonism" and Freemasonry, Salt Lake City, UT: Deseret News Press, OCLC 9638443 . Online reprint at shields-research.org. Lindsay, Jeff, Questions About the LDS Temple Ceremony and Masonry, "LDSFAQ (Mormon Answers)", jefflindsay.com, http://www.jefflindsay.com/LDSFAQ/FQ_masons.shtml . - apologetic discussion of Mormonism and freemasonry Tanner, Jerald and Sandra (1969), "13. Captain Morgan and the Masonic Influence in Mormonism", The Mormon Kingdom Vol. 1, Utah Lighthouse Ministry, OCLC 19836200, http://www.utlm.org/onlineresources/mormonkingdomvol1ch13masonicinfluence.htm . - polemic discussion of Mormonism and freemasonry The Masonic Moroni[dead link] - Exploring the Historical Relationship Between Early Mormonism & Freemasonry. Articles in need of neutralization Christianity and Freemasonry Latter Day Saint movement in the United States Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 23 July 2016, at 16:30. Privacy policy About Metapedia Disclaimers 
  
"By 1840, John Cook Bennett, a former active leader in Masonry had arrived in Commerce and rapidly exerted his persuasive leadership in all facets of the Church, including Mormon Masonry. ... Joseph and Sidney [Rigdon] were inducted into formal Masonry ... on the same day..." being made "Masons on Sight" by the Illinois Grandmaster.("Is There No Help for the Widow's Son?" by Dr. Reed C. Durham, Jr., as printed in "Joseph Smith and Masonry: No Help for the Widow's Son", Martin Pub. Co., Nauvoo, Ill., 1980, p. 17.) (This freed Joseph from having to complete the ritual and memorization necessary to work one's way through the first three degrees.) Making one "A Mason on Sight" is generally reserved as an honor and is a rarity in occurrence.

 
Tuesday, [March] 15. — I officiated as grand chaplain at the installation of the Nauvoo Lodge of Free Masons, at the Grove near the Temple. Grand Master Jonas, of Columbus, being present, a large number of people assembled on the occasion. The day was exceedingly fine; all things were done in order, and universal satisfaction was manifested. In the evening I received the first degree in Freemasonry in the Nauvoo Lodge, assembled in my general business office. (History of the Church, by Joseph Smith, Deseret Book, 1978, Vol.4, Ch.32, p.550-1)

 
Wednesday, March 16. — I was with the Masonic Lodge and rose to the sublime degree. (History of the Church, Vol.4, Ch.32, p.552)

 
[The Joseph Smith family] was a Masonic family which lived by and practiced the estimable and admirable tenets of Freemasonry. The father, Joseph Smith, Sr., was a documented member in upstate New York. He was raised to the degree of Master Mason on May 7, 1818 in Ontario Lodge No. 23 of Canandaigua, New York. An older son, Hyrum Smith, was a member of Mount Moriah Lodge No. 112, Palmyra New York.

 
It is true that Solomon built a temple for the purpose of giving endowments, but from what we can learn of the history of that time they gave very few if any endowments, and one of the high priests [Hiram Abiff] was murdered by wicked and corrupt men, who had already begun to apostatize, because he would not reveal those things appertaining to the priesthood that were forbidden him to reveal until he came to the proper place. (Discourses of Brigham Young, compiled by John A Widtsoe, Deseret Book Company, 1977)

 Freemasonry Core Articles History Masonic Bodies Masonic Masonic groups for women Masonic Youth Organizations Views of Masonry People and Places Other related articles Mormonism and Freemasonry 17 20 18 27 9 75 Part of a series of articles onFreemasonry
 
 
Core Articles
Freemasonry · Grand Lodge · Masonic Lodge · Masonic Lodge Officers · Grand Master · Prince Hall Freemasonry · Regular Masonic jurisdictions

History
History of Freemasonry · Liberté chérie · Masonic manuscripts



 Masonic Bodies 


Masonic 


Masonic bodies · York Rite · Order of Mark Master Masons · Holy Royal Arch · Royal Arch Masonry · Cryptic Masonry · Knights Templar · Red Cross of Constantine · Scottish Rite · Knight Kadosh · The Shrine · Royal Order of Jesters · Tall Cedars of Lebanon · The Grotto · Societas Rosicruciana · Grand College of Rites · Swedish Rite · Order of St. Thomas of Acon · Royal Order of Scotland · Research Lodge


 Masonic groups for women 


Women and Freemasonry · Order of the Amaranth · Order of the Eastern Star · Co-Freemasonry


 Masonic Youth Organizations 


DeMolay · A.J.E.F. · Job's Daughters · International Order of the Rainbow for Girls



 Views of Masonry 


Anti-Masonry · Anti-Masonic Party · Anti-Freemason Exhibition · Christianity and Freemasonry · Catholicism and Freemasonry · Suppression of Freemasonry · Masonic conspiracy theories · Taxil hoax



 People and Places 


Masonic Temple · James Anderson · Albert Mackey · Albert Pike · Prince Hall · John the Evangelist · John the Baptist · William Schaw · Elizabeth Aldworth · List of Freemasons · Lodge Mother Kilwinning · Freemasons' Hall, London · House of the Temple · Solomon's Temple · Detroit Masonic Temple · List of Masonic buildings



 Other related articles 


Great Architect of the Universe · Square and Compasses · Pigpen cipher · Eye of Providence · Hiram Abiff · Sprig of Acacia · Masonic Landmarks · Pike's Morals and Dogma· Propaganda Due · Dermott's Ahiman Rezon

This box: view • talk • edit
 Masonic bodies · York Rite · Order of Mark Master Masons · Holy Royal Arch · Royal Arch Masonry · Cryptic Masonry · Knights Templar · Red Cross of Constantine · Scottish Rite · Knight Kadosh · The Shrine · Royal Order of Jesters · Tall Cedars of Lebanon · The Grotto · Societas Rosicruciana · Grand College of Rites · Swedish Rite · Order of St. Thomas of Acon · Royal Order of Scotland · Research Lodge
 Women and Freemasonry · Order of the Amaranth · Order of the Eastern Star · Co-Freemasonry
 DeMolay · A.J.E.F. · Job's Daughters · International Order of the Rainbow for Girls
 Anti-Masonry · Anti-Masonic Party · Anti-Freemason Exhibition · Christianity and Freemasonry · Catholicism and Freemasonry · Suppression of Freemasonry · Masonic conspiracy theories · Taxil hoax
 Masonic Temple · James Anderson · Albert Mackey · Albert Pike · Prince Hall · John the Evangelist · John the Baptist · William Schaw · Elizabeth Aldworth · List of Freemasons · Lodge Mother Kilwinning · Freemasons' Hall, London · House of the Temple · Solomon's Temple · Detroit Masonic Temple · List of Masonic buildings
 Great Architect of the Universe · Square and Compasses · Pigpen cipher · Eye of Providence · Hiram Abiff · Sprig of Acacia · Masonic Landmarks · Pike's Morals and Dogma· Propaganda Due · Dermott's Ahiman Rezon
 Mormonism and Freemasonry Contents Historical connections Similarities in symbology and ritual Modern official LDS Church policy Recent explorations of the issue See also Notes References Further reading External link Navigation menu Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 