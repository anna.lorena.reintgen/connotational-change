Boasian anthropology is an influential movement in anthropology, originating with the Jewish anthropologist Franz Boas. The movement has also had various influences outside of anthropology. Important aspects include race denialism and attacks on Western culture and civilization.
 Boasian anthropology emerged in the early 20th century, at Columbia University, where Boas was appointed as the head of a newly created department of anthropology.
 Boas and his many influential students have been argued to have been intensely concerned with pushing an ideological and political agenda within anthropology. Already by 1915, the Boasians controlled the American Anthropological Association and held a two-thirds majority on its Executive Board. In 1919, Boas could state that “most of the anthropological work done at the present time in the United States” was done by his students. By 1926, every major department of anthropology was headed by Boas’s students, the majority of whom were Jewish.[1]
 Influential students of Boas included Ashley Montagu (born Israel Ehrenberg), Margaret Mead, Ruth Benedict, Alexander Goldenweiser, Melville Herskovits, Alfred Kroeber, Robert Lowie, Paul Radin, Edward Sapir, and Leslie Spier. Most were Jews and many (including Boas) have been argued to have been deeply concerned with Jewish interests and anti-Semitism.[1]
 Boas was a socialist and many of his followers were political radicals. Kevin MacDonald has argued that "the attraction of Jewish intellectuals to the left is a general phenomenon and has typically co-occurred with a strong Jewish identity and sense of pursuing specifically Jewish interests".[1][2]
 Boasian anthropology has been criticized as (at least while Boas was alive) being a highly authoritarian and cohesive political movement, centered around a charismatic leader, predominantly ethnically Jewish, strongly supporting the interests and careers of one another, intolerant of any dissent or criticism (even from within the movement), and applying a double standard to critics and supporters regarding empirical evidence.[1]
 Mead and Benedict were not (openly) Jews, although Benedict may have been of Jewish descent.[3] They have been argued to have become the most publicly visible of Boas students. Boas, like Freud, have been argued to have deliberately recruited and publicly promoted some gentiles out of concern "that his Jewishness would make his science appear partisan and thus compromised".[1]
 Furthermore, "some of Boaz’s most famous gentile followers were also at odds with American society: Margaret Mead was bisexual and Ruth Benedict was lesbian. Thus, “the Boazians shared an out-group sensibility, a commitment to a common viewpoint and a program to dominate the institutional structure of anthropology.”"[4]
 Kevin MacDonald argues in The Culture of Critique that Boasian anthropology is one manifestation of a Jewish group evolutionary strategy. It is argued to have contributed to a shift from Darwinism and other biological perspectives as a fundamental paradigm of the social science. Generally, this shift is argued to have developed consequent to the entry of certain Jews to these fields.[1]
 Before Boas, the prevalent view was that cultures had evolved in a series of developmental stages. The stages were associated with racial differences and modern European culture was at a high and superior stage. Boasians in various way attacked this view. For example, Boasian anthropology in the 1920s criticized American (European) culture as overly homogeneous, hypocritical, and emotionally and esthetically repressive (especially regarding sexuality). One important part of this campaign was creating ethnographies of exotic cultures, which described them as lacking of such negative traits and as models for Europeans to emulate. Possible problems in such cultures were seen as corruptions due to European influence.[1]
 One consequence of this was a long-term and almost complete censorship of, and lack of research on, warfare and violence among non-European primitive cultures by anthropologists. In reality, studies are argued to have shown that less advanced cultures experience much more violence and warfare than advanced cultures.[1] See also the Noble savage article.
 A common misconception is that all Boasians were biological (genetic) race denialists. Instead, initially, the existence of biological races was formally accepted, but politically sensitive race differences were downplayed and environmental causes emphasized. However, later Boasians started to completely deny the existence of biological races, while still arguing for the existence of races as social constructs.
 Regardless, Boasian influence quickly caused United States anthropological research on racial differences to cease. Eugenicists and racial theorists were completely excluded from the profession.[1]
 One aspect of Boasian anthropology is emphasis on cultural relativism, which again opposes European culture as being better.
 MacDonald writes that "An important technique of the Boasian school was to cast doubt on general theories of human evolution, such as those implying developmental sequences, by emphasizing the vast diversity and chaotic minutiae of human behavior, as well as the relativism of standards of cultural evaluation. The Boasians argued that general theories of cultural evolution must await a detailed cataloguing of cultural diversity, but in fact no general theories emerged from this body of research in the ensuing half century of its dominance of the profession (Stocking 1968, 210). Because of its rejection of fundamental scientific activities such as generalization and classification, Boasian anthropology may thus be characterized more as an anti-theory than a theory of human culture (White 1966, 15). Boas also opposed research on human genetics—what Derek Freeman (1991, 198) terms his “obscurantist antipathy to genetics.""[1]
 Some extremely well-known and influential studies by Boasian anthropologists have been criticized as not only erroneous, but as systematically misrepresenting facts. In a very well-known and influential study, Boas claimed extensive changes in cranial morphology for second-generation US immigrants. Recent studies have re-examined the data used by Boas and stated that Boas' claims are not supported his data. See the Craniometry article. The two most well-known ethnographies by students of Boas (Coming of Age in Samoa and Patterns of Culture) have been accused of hiding many negative aspects of the studied non-European cultures, in order to depict them as idyllic paradises, superior to European culture.[1] See also the article Noble savage.
 Another student of Boas was Ashley Montagu (born Israel Ehrenberg). He was the principal author of the influential 1950 UNESCO declaration "The Race Question‎".
 Although not formally a Boasian, another very influential person is argued to be the Franco-Jewish anthropologist Claude Lévi-Strauss, who influenced many later Cultural Marxist ideologues. Lévi-Strauss interacted extensively with Boas, acknowledged his influence, and has been argued to have had similar concerns with Jewish interests and anti-Semitism. Lévi-Strauss in turn was very influential in France, and has been described as “the common father” of Michel Foucault, Louis Althusser, Roland Barthes, and Jacques Lacan.[1]
 Otto Klineberg was a professor of psychology at Columbia, deeply influenced by Boas, and "made it his business to do for psychology what his friend and colleague at Columbia [Boas]
had done for anthropology: to rid his discipline of racial explanations for human social differences".[1]
 By the mid-1930s, the Boasian view of the cultural determination of human behavior had a strong influence on social scientists generally. Some Boasian anthropologists became influenced by and influenced psychoanalysis, which also influenced the Frankfurt School, with Jews being important persons in all movements. This had effects such as reducing biological viewpoints within psychoanalysis and all movements were used for cultural critique (of European culture).[1]
 There have also been several other argued connections between Boasian anthropology and the increasing influence of genetics denialism on psychology.[5]
 The American Anthropological Association ("AAA") is closely associated with Boasian anthropology. It has been criticized for having become increasingly dominated by cultural anthropologists, with a tendency to reject both biological approaches and science. In 2010, the AAA leadership in a long-range planning document even proposed removing the word "science" at several points from the description of the association's mission and proposed changing the purpose to be "to advance the public understanding of humankind in all its aspects". After severe criticisms, this did not occur with both science and humanities stated to be important. One criticism of the proposed change was that in order to receive funding and to be able to politically influence the general public, it is advantageous to not officially abandon science.[6][7][8]
 The AAA's supposedly scholarly journals (or at least the cultural anthropology ones) are still filled with positive references to discredited pseudosciences such as Marxism and psychoanalysis.
 See also the article on the American Anthropological Association on various related issues.
 1 The Boasians 2 Teachings 3 Other influences 4 Recent increasing genetics denialism and rejection of science 5 See also 6 External links 7 References Cultural relativism - A Boasian concept Cultural Marxism Noble savage Jewish influence Race denialism "The Boasian School of Anthropology and the Decline of Darwinism in the Social Sciences" by Dr. Kevin MacDonald - PDF of the chapter on Boas in Culture of Critique provided at Kevin MacDonald's website. Jews and Race: A Pre-Boasian Perspective, Part 1 Jews and Race: A Pre-Boasian Perspective, Part 2 ↑ 1.00 1.01 1.02 1.03 1.04 1.05 1.06 1.07 1.08 1.09 1.10 1.11 1.12 1.13 Kevin B. MacDonald. The Culture of Critique. 1998, 2002. 1st Books Library.
 ↑ Wesley Critz George (1962) has a discussion on the left-wing political views of most Boasians [1].
 ↑ Modell, Judith Schachter. (1983). Ruth Benedict, Patterns of a Life. University of Pennsylvania Press.
 ↑ Science Strikes Back https://www.amren.com/news/2018/07/race-reality-of-human-differences-sarich-miele/
 ↑  Subversion of Science: How Psychology Lost Darwin http://codoh.com/library/document/3021/
 ↑ Anthropologists Debate Whether 'Science' Is a Part of Their Mission http://www.chronicle.com/article/Anthropologists-Debate-Whether/125571/
 ↑ Anthropology Association Rejecting Science? http://www.chronicle.com/blogs/innovations/anthropology-association-rejecting-science/27936
 ↑ Anthropology, Science, and Public Understanding http://blogs.plos.org/neuroanthropology/2010/12/01/anthropology-science-and-public-understanding/
 Anthropology Jewish intellectual movements Race denialism Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 17 August 2019, at 06:43. Privacy policy About Metapedia Disclaimers 
  Boasian anthropology Race research Race differences Related research areas Boasian anthropology American Anthropological Association
 Boasian anthropology
 Craniometry
 Cultural relativism
 Franz Boas
 Noble savage
 Race denialism
 Recent African origin of modern humans
 Statement on "Race"
 The Race Question
 Race
 Arguments regarding the existence of races
 Race and crime
 Race and health
 Race and intelligence
 Race and intelligence: The genetics or not debate
 Countries and intelligence
 Race and morphology/physiology
 Race and sports
 Differential K theory
 Human Accomplishment
 Other race differences
 Boasian anthropology
 Contact hypothesis
 Dysgenics
 Effects of race mixing ‎
 Ethnic heterogeneity
 Eugenics
 Genetics denialism
 Inbreeding depression and outbreeding depression
 Migration
 Pathological altruism
 Racial genetic interests
 Recent African origin of modern humans
 Smart fraction
 The sociologist's fallacy
 White flight
 White demographics
 Boasian anthropology Contents The Boasians Teachings Other influences Recent increasing genetics denialism and rejection of science See also External links References Navigation menu Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 