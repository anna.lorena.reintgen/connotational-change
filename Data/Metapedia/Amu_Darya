The Amu Darya (Persian: آمودریا, Āmūdaryā; Pashto: د آمو سيند, də Āmu Sind; Arabic: جيحون‎, Jihôn or Jayhoun; Hebrew: גּוֹזָן‎, Gozan ), also called Oxus and Amu River, is a major river in Central Asia. It is formed by the junction of the Vakhsh and Panj rivers. In ancient times, the river was regarded as the boundary between Iran and Tūrān.[2]
 In antiquity, the river was known as Vaksu to Indo-Aryans.
 In ancient Afghanistan, the river was also called Gozan, descriptions of which can be found in the book "The Kingdom of Afghanistan: a historical sketch By George Passman Tate".[3] [4]
 In classical antiquity, the river was known as the Ōxus in Latin and Ὦξος Oxos in Greek — a clear derivative of Vakhsh — the name of the largest tributary of the river. In Middle Persian sources of the Sassanid period the river is known as Wehrōd[2] (lit. "good river").
 The name Amu is said to have come from the medieval city of Āmul, (later, Chahar Joy/Charjunow, and now known as Türkmenabat), in modern Turkmenistan, with Darya being the Persian word for "river".
 Medieval Arabic and Muslim sources call the river Jayhoun (جيحون) which is derived from Gihon, the biblical name for one of the four rivers of the Garden of Eden.[5][6]
 Amu Darya is a river almost in reverse, for long reputed to be sourced by a powerful glacier fed stream high in the Pamir Knot at the eastern end of Afghanistan's Wakhan Corridor, and ending not at the sea but spreading out into the sands of Turkmenistan's Kyzyl Kum desert, well short of its historic terminus of the inland Aral Sea.
 In the traditions of the Islamic prophet Muhammad (ahadith), the river is called by the name Jayhan (arabic form of its ancient name Gozan).[7]
  According to Ibn Hanbal's version of this hadith: 
 "Four rivers gush forth from Paradise: the Euphrates, the Nile, the Sayhan, and the Jayhan" 
(Musnad, II, 260-261).
  – The introductory chapters of Yāqūt's Muʿjam al-buldān, Page 30 Historians tell us that one of the most ancient names for the Oxus or Amu in ancient Afghanistan was Gozan. A name used by Greek, Mongol, Chinese, Persian, Jewish and Afghan historians. However, this name is no longer used.
 The river's total length is 2400 km and its drainage basin totals 534739 km2 in area, providing a mean discharge of around 97.4 km3[1] of water per year. The river is navigable for over 1450 km. All of the water comes from the high mountains in the south where annual precipitation can be over 1000 mm. Even before large-scale irrigation began, high summer evaporation meant that not all of this discharge reached the Aral Sea - though there is some evidence the large Pamir glaciers provided enough melt water for the Aral to overflow during the 13th and 14th centuries A.D.
 One source of the Amu Darya is the Pamir River, which emerges from Lake Zorkul (once also known as Lake Victoria) in the Pamir Mountains (ancient Mount Imeon), and flows west to Qila-e Panja, where it joins the Wakhan River to form the Panj River.
 Another claimed source of the Amu Darya is an ice cave at the end of the Wakhjir valley, in the Wakhan Corridor, in the Pamir Mountains, near the border with Pakistan. A glacier turns into the Wakhan River and joins the Pamir River about 50 km downstream[11]).
 The Panj River forms the border of Afghanistan and Tajikistan. It flows west to Ishkashim where it turns north and then east north-west through the Pamirs passing the Tajik-Afghan Friendship Bridge. It subsequently forms the border of Afghanistan and Uzbekistan for about 200 km, passing Termez and the Afghanistan-Uzbekistan Friendship Bridge. It delineates the border of Afghanistan and Turkmenistan for another 100 km before it flows into Turkmenistan at Atamyrat. As the Amudarya, it flows across Turkmenistan south to north, passing Türkmenabat, and forms the border of Turkmenistan and Uzbekistan from Halkabat. It is then split into many waterways that are used to form the river delta joining the Aral Sea, passing Urgench, Daşoguz and other cities, but it does not reach what is left of the sea anymore and is lost in the desert.
 Use of water from the Amu Darya for irrigation has been a major contributing factor to the shrinking of the Aral Sea since the late 1950s[citation needed].
 Historical records state that in different periods, the river flowed into the Aral Sea (from the south), the Caspian Sea (from the east) or both, similar to the Syr Darya (Jaxartes, in Ancient Greek).
 About 1385045 km2 of land is drained by the Amu Darya into the Aral Sea endorheic basin. This includes most of Tajikistan, the southwest corner of Kyrgyzstan, the northeast corner of Afghanistan, a long narrow portion of western Turkmenistan and about half of Uzbekistan. Part of the Amu Darya's drainage divide in Tajikistan forms that country's border with China (in the east) and Pakistan (to the south). About 61% of the drainage lies within Tajikistan, Uzbekistan and Turkmenistan, while 39% is in Afghanistan.[12]
Of the area drained by the Amu Darya, only about 200000 km2 actively contribute water to the river.[13]
This is because many of the river's major tributaries (especially the Zeravshan River) have been diverted, and much of the river's drainage is dominated by outlying desert and steppe.
 The abundant water flowing in the Amu Darya almost entirely comes from glaciers in the Pamir Mountains and Tian Shan,[14]
which, standing above the surrounding arid plain, collect atmospheric moisture which otherwise would probably have escaped somewhere else. Without its mountain water sources, the Amu Darya would not contain any water because it rarely rains in the lowlands that characterize most of the river. Throughout most of the steppe, the annual rainfall is about 300 mm.[12][15]
 The Amu Darya was called the Oxus by the ancient Greeks. In ancient times, the river was regarded as the boundary between Irān and Tūrān.[2] The river's drainage lies in the area between the former empires of Genghis Khan and Alexander the Great, although they occurred at much different times. One southern route of the Silk Road ran along part of the Amu Darya northwestward from Termez before going westwards to the Caspian Sea.
 It is believed that the Amu Darya's course across the Kara-Kum Desert has gone through several major shifts in the past few thousand years. Much of the time, the most recent period being in the 13th century to the late 16th century, the Amu Darya emptied into both the Aral and the Caspian Seas, the latter via a large distributary called the Uzboy River. The Uzboy splits off from the main channel just south of the Amudarya Delta. Sometimes, the flow through the two branches was more or less equal, but often, most of the Amu Darya's flow split to the west and flowed into the Caspian.
 People began to settle along the lower Amu Darya and the Uzboy in the 5th century A.D., establishing a thriving chain of agricultural lands, towns, and cities. The river was impounded in about 985 A.D. at the bifurcation of the forks by the massive Gurganj Dam, which diverted water to the Aral. The dam was destroyed by Genghis Khan's troops in 1221, and the Amu Darya shifted its flows more or less equally between the main stem and the Uzboy.[16] But in the 18th century, the river again turned north, flowing into the Aral Sea, a path it has taken since. Less and less water flowed down the Uzboy until, in the 1720s, the river's surface flow completely dried up.[17]
 The first British explorer to reach the region in the Great Game period was a naval officer called John Wood. He was sent on an expedition to find the source of the river in 1839. He found modern day Lake Zorkul, called it Lake Victoria and proclaimed he had found the source. [18]
 The Soviet Union became the ruling power in the 20th century. The Soviet Union fell in the 1990s and Central Asia split up into the many smaller countries that lie within or partially within the Amu Darya basin. In the 1960s and 1970s, the Amu Darya and Syr Darya were first used by the Soviets to irrigate extensive cotton fields in the Central Asian plain. Before this time, water from the rivers was already being used for agriculture, but not on this massive scale. The Qaraqum Canal, Karshi Canal, and Bukhara Canal were among the larger of the irrigation diversions built.[19] The Main Turkmen Canal was a proposed project that would have diverted water along the dry Uzboy River bed into central Turkmenistan, but was never built.
  But the majestic River floated on,
 Out of the mist and hum of that low land, 
Into the frosty starlight, and there moved, 
Rejoicing, through the hushed Chorasmian waste,
Under the solitary moon: — he flowed 
Right for the polar star, past Orgunjè,
Brimming, and bright, and large: then sands begin 
To hem his watery march, and dam his streams, 
And split his currents; that for many a league 
The shorn and parcelled Oxus strains along 
Through beds of sand and matted rushy isles — 
Oxus, forgetting the bright speed he had 
In his high mountain-cradle in Pamere, 
A foiled circuitous wanderer: — till at last 
The longed-for dash of waves is heard, and wide 
His luminous home of waters opens, bright 
And tranquil, from whose floor the new-bathed stars 
Emerge, and shine upon the Aral Sea.
  – Matthew Arnold, Sohrab and Rustum The Oxus river, and Arnold's poem, provide a literary background for the 1930s children's book The Far-Distant Oxus.
 
 1 Names 2 In Ahadith 3 As the river Gozan 4 Description 5 Watershed 6 History 7 Literature 8 See also 9 Notes 10 References Transoxiana Mount Imeon Surkhan Darya Zeravshan River Sherabad River ↑ 1.0 1.1 http://www.ce.utexas.edu/prof/mckinney/papers/aral/CentralAsiaWater-McKinney.pdf
 ↑ 2.0 2.1 2.2 B. Spuler, ĀMŪ DARYĀ, in Encyclopædia Iranica, online ed., 2009 Cite error: Invalid <ref> tag; name "Iranica" defined multiple times with different content
 ↑ http://books.google.co.uk/books?id=tGTd9KKwKVwC&pg=PA11&lpg=PA11&dq=allintext:+%3D+gozan+oxus+amu&source=bl&ots=ZDHOgiLp1e&sig=Du0XLu3_E3vhhGiArc3JU_iqJHk&hl=en&ei=CBHhTPm6IYnsuAPGkKjYDg&sa=X&oi=book_result&ct=result&resnum=1&ved=0CBYQ6AEwAA#v=onepage&q=allintext%3A%20%3D%20gozan%20oxus%20amu&f=false
 ↑ http://library.du.ac.in/dspace/bitstream/1/4715/4/Ch.1 The kingdom of Afghanistan (page 1-87).pdf
 ↑ William C. Brice. 1981. Historical Atlas of Islam (Hardcover). Leiden with support and patronage from Encyclopaedia of Islam. ISBN 90-04-06116-9.
 ↑ Encyclopædia Britannica Online: Amu Darya
 ↑ The introductory chapters of Yāqūt's Muʿjam al-buldān, by Yāqūt ibn ʿAbd Allāh al-Ḥamawī, Page 30
 ↑ The Kingdom of Afghanistan: a historical sketch, By George Passman Tate, Page 11.
 ↑ Jews in Islamic countries in the Middle Ages, By Moshe Gil, David Strassler, Page 428.
 ↑ Tamerlane and the Jews, By Michael Shterenshis, Page xxiv.
 ↑ J. Mock and K. O'Neil (2004): Expedition Report
 ↑ 12.0 12.1 
Rakhmatullaev, Shavkat. Groundwater resources use and management in the Amu Darya River Basin (Central Asia). Environmental Earth Sciences. SpringerLink. Retrieved on 2010-02-09.
 ↑ Agaltseva, N.A. (1997). Automated system of runoff forecasting for the Amudarya River basin. Destructive Water: Water-Caused Natural Disasters, their Abatement and Control. International Association of Hydrological Sciences. Retrieved on 2010-02-09.
 ↑ Basin Water Organization “Amudarya”. Interstate Commission for Water Coordination of Central Asia. Retrieved on 2010-02-11.
 ↑ Amudarya River Basin Morphology. Central Asia Water Information. Retrieved on 2010-02-09.
 ↑ Volk, Sylvia (2000-11-11). The Course of the Oxus River. University of Calgary. Retrieved on 2010-02-08.
 ↑ Kozubov, Robert (2007-11). Uzboy. Turkmenistan Analytic Magazine. Retrieved on 2010-02-08.
 ↑ Keay, J. (1983) When Men and Mountains Meet ISBN 0-7126-0196-1 Chapter 9
 ↑ Pavlovskaya, L.P.. Fishery in the Lower Amu Darya Under the Impact of Irrigated Agriculture. Karakalpak Branch. Academy of Sciences of Uzbekistan. Retrieved on 2010-02-09.
 Curzon, George Nathaniel. 1896. The Pamirs and the Source of the Oxus. Royal Geographical Society, London. Reprint: Elibron Classics Series, Adamant Media Corporation. 2005. ISBN 1-4021-5983-8 (pbk; ISBN 1-4021-3090-2 (hbk). Gordon, T. E. 1876. The Roof of the World: Being the Narrative of a Journey over the high plateau of Tibet to the Russian Frontier and the Oxus sources on Pamir. Edinburgh. Edmonston and Douglas. Reprint by Ch'eng Wen Publishing Company. Taipei. 1971. Toynbee, Arnold J. 1961. Between Oxus and Jumna. London. Oxford University Press. Wood, John, 1872. A Journey to the Source of the River Oxus. With an essay on the Geography of the Valley of the Oxus by Colonel Henry Yule. London: John Murray. Pages with reference errors Articles containing Arabic language text Rivers of Afghanistan Rivers of Tajikistan Rivers of Turkmenistan Rivers of Uzbekistan International rivers of Asia Articles containing non-English language text Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 8 March 2012, at 02:54. Privacy policy About Metapedia Disclaimers 
   According to Ibn Hanbal's version of this hadith: 
"Four rivers gush forth from Paradise: the Euphrates, the Nile, the Sayhan, and the Jayhan" 
(Musnad, II, 260-261).

 – The introductory chapters of Yāqūt's Muʿjam al-buldān, Page 30  But the majestic River floated on,
Out of the mist and hum of that low land, 
Into the frosty starlight, and there moved, 
Rejoicing, through the hushed Chorasmian waste,
Under the solitary moon: — he flowed 
Right for the polar star, past Orgunjè,
Brimming, and bright, and large: then sands begin 
To hem his watery march, and dam his streams, 
And split his currents; that for many a league 
The shorn and parcelled Oxus strains along 
Through beds of sand and matted rushy isles — 
Oxus, forgetting the bright speed he had 
In his high mountain-cradle in Pamere, 
A foiled circuitous wanderer: — till at last 
The longed-for dash of waves is heard, and wide 
His luminous home of waters opens, bright 
And tranquil, from whose floor the new-bathed stars 
Emerge, and shine upon the Aral Sea.

 – Matthew Arnold, Sohrab and Rustum Name origin:  Tributaries Amu Darya Oxus Amu River Panj River Amu Darya "Hara (Bokhara) and to the river of Gozan (that is to say, the Amu, (called by Europeans the Oxus)....". [8] "the Gozan River is the River Balkh, i.e. the Oxus or the Amu Darya.....". [9] "... and were brought into Halah (modern day Balkh), and Habor (which is Pesh Habor or Peshawar), and Hara (which is Herat), and to the river Gozan (which is the Ammoo, also called Jehoon)...". [10] "Hara (Bokhara) and to the river of Gozan (that is to say, the Amu, (called by Europeans the Oxus)....". [8] "the Gozan River is the River Balkh, i.e. the Oxus or the Amu Darya.....". [9] "... and were brought into Halah (modern day Balkh), and Habor (which is Pesh Habor or Peshawar), and Hara (which is Herat), and to the river Gozan (which is the Ammoo, also called Jehoon)...". [10] Amu Darya
 Oxus, Jayhoun, də Āmu Sind, Vaksu, Amu River
  Amu Darya Delta from space 
 Name origin: Named for city of Āmul (now Turkmenabat)
 
 Tajikistan, Afghanistan, Turkmenistan, Uzbekistan
 Central Asia
 
 Tributaries
  - left
 Panj River
  - right
 Vakhsh River, Surkhan Darya, Sherabad River, Zeravshan River
 
 
 Pamir River/Panj River
  - location
 Lake Zorkul, Pamir Mountains, Tajikistan
  - elevation
 4,130 m (13,550 ft)
  - coordinates
 37°27′04″N 73°34′21″E﻿ / ﻿37.45111°N 73.5725°E﻿ / 37.45111; 73.5725
 Kyzyl-Suu/Vakhsh River
  - location
 Alay Valley, Pamir Mountains, Kyrgyzstan
  - elevation
 4,525 m (14,846 ft)
  - coordinates
 39°13′27″N 72°55′26″E﻿ / ﻿39.22417°N 72.92389°E﻿ / 39.22417; 72.92389
 Kerki
  - elevation
 326 m (1,070 ft)
  - coordinates
 37°06′35″N 68°18′44″E﻿ / ﻿37.10972°N 68.31222°E﻿ / 37.10972; 68.31222
 Aral Sea
  - location
 Amudarya Delta, Uzbekistan
  - elevation
 28 m (92 ft)
  - coordinates
 44°06′30″N 59°40′52″E﻿ / ﻿44.10833°N 59.68111°E﻿ / 44.10833; 59.68111
 
 2,400 km (1,491 mi)
 534,739 km² (206,464 sq mi)
 
  - average
 2,525 m3/s (89,170 cu ft/s) [1]
 
   Wikimedia Commons has media related to: Amu Darya  v • d • e
 Rivers in TurkmenistanRiversAmu Darya   • Atrek  •  Kushk  •  Morghab (Murgab)   •  Sumber (Sari-su)   •  TejenCanalKarakumRiver (extinct)Uzboy  Rivers Amu Darya   • Atrek  •  Kushk  •  Morghab (Murgab)   •  Sumber (Sari-su)   •  Tejen  Canal Karakum  River (extinct) Uzboy Oxus River Contents Names In Ahadith As the river Gozan Description Watershed History Literature See also Notes References Navigation menu Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 