Wilhelm Emil "Willy" Messerschmitt (/'vĭli 'messer shmĭt/) (June 26, 1898 – September 15, 1978) was a German aircraft designer and manufacturer. He was born in Frankfurt am Main, the son of a wine merchant. His stepfather was the American painter and Munich Academy Professor Carl von Marr.
 Probably Messerschmitt's single most important design was the Messerschmitt Bf 109, designed in 1934 with the collaboration of Walter Rethel. The Bf 109 became the most important fighter in the Luftwaffe as Germany re-armed prior to World War II. To this day, it remains the most-produced fighter in history, with some 35,000 built. Another Messerschmitt aircraft, first called "Bf 109R", purpose-built for record setting, but later re-designated Messerschmitt Me 209, broke the absolute world air-speed record and held the world speed record for propeller-driven aircraft until 1969. His firm also produced the first jet-powered fighter to enter service — the Messerschmitt Me 262, although Messerschmitt himself did not design it.
 As a young man, Messerschmitt befriended German sailplane pioneer Friedrich Harth. Harth joined the German army in 1914 and while he was away at war, Messerschmitt continued work on one of Harth's designs, the S5 glider. In 1917, Messerschmitt himself signed up for military service. Following the war, the two were re-united and continued to work together while Messerschmitt commenced study at the Munich Technical College and Harth built aircraft at the Bayerische Flugzeugwerke (BFW - Bavarian Aircraft Works). The S8 glider they designed and built together in 1921 broke a world duration record (albeit unofficially) and they went into partnership for a while running a flying school. The same year, the first plane entirely designed by Messerschmitt flew — the S9 glider.
 During 1923 Harth and Messerschmitt had a falling out and went their separate ways, with Messerschmitt founding his own aircraft company at Augsburg. At first, Messerschmitt built sailplanes, but within two years had progressed via motor gliders to small powered aircraft - sports and touring types. These culminated in the Messerschmitt M 17 and Messerschmitt M 18 designs, which Messerschmitt sold to BFW in 1927, when the Bavarian state government encouraged a merger between the two companies. These were followed by the Messerschmitt M20 light transport in 1928, which proved a disaster for BFW and Messerschmitt himself. Two Deutsche Luft Hansa M20s were involved in serious crashes very soon after purchase, and this led the airline to cancel their order for the type. This caused a serious cash-flow problem for the company and led to its bankruptcy in 1931. The M20 crashes also created a powerful enemy for Messerschmitt in the person of Erhard Milch, the head of Luft Hansa who had lost a close friend in one of the crashes.
 The establishment of the Reichsluftfahrtministerium ("Reich Aviation Ministry" - RLM) by the National Socialist government in 1933, headed by Milch, led to a resurgence in the German aircraft industry and the resurrection of BFW. Collaborating with Robert Lusser, Messerschmitt designed the flagship product of the relaunched company, a low-wing sports monoplane called the Messerschmitt M37, but better known by its later RLM designation of Bf 108 Taifun. The following year, Messerschmitt would incorporate many design features of this aircraft into the Bf 109 fighter.
 Nevertheless, only the ties that Messerschmitt had formed with leading National Socialists Rudolf Hess and Hermann Göring (through Theo Croneiss) saved him from sharing the fate of Milch's other great enemy, Hugo Junkers. To stay in business in the face of Milch ensuring that he would get no government contracts, Messerschmitt had signed agreements with Romania for sales of the M37 and a transport plane, the Messerschmitt M 36. When Milch learned of this, he publicly denounced Messerschmitt as a traitor, and the Gestapo was sent to question him and other BFW officials. Probably due to Croneiss' intervention, no further action was taken.
 When in 1936, the Messerschmitt Bf 109 won the RLM's single-seat fighter contest to become one of the main Luftwaffe aircraft types, Messerschmitt and his factory took an important role in the RLM's armament plans, increasing in significance even further when Messerschmitt's Bf 110 also won the multi-purpose fighter contest.
 On July 11, 1938, Messerschmitt was appointed chairman and managing director of Bayerische FlugzeugWerke (BFW) and the company was renamed after him to Messerschmitt AG. This same year, the company began work on what would eventually become the Me 262, and of the Messerschmitt Me 210, planned as successor for the Bf 110. The Me 210 turned out to be plagued by massive development problems that were only solved by evolving the type into the Messerschmitt Me 410, and the resulting problems and delays again put the reputation of both Messerschmitt and his namesake company in jeopardy.
 Following World War II, Messerschmitt was tried by a denazification kangaroo court for using slave labor, and in 1948 was convicted of being a "fellow traveller". After two years in prison, he was released and resumed his position as head of his company. Since Germany was forbidden to manufacture aircraft until 1955, he turned his company to manufacturing prefabricated buildings, sewing machines, and small cars — most notably the Messerschmitt Kabinenroller. Exporting his talents, he designed the Hispano HA-200 jet trainer for Hispano Aviación in Spain in 1952 before eventually being allowed to return to aircraft manufacturing in Germany to licence-produce the Fiat G91 and then Lockheed F-104 Starfighter for the West German Luftwaffe. He designed the later Helwan HA-300, a light supersonic interceptor, for the Egyptian air forces. This is his last aircraft design.
 Messerschmitt saw his company through mergers first with Bölkow in 1968 and then Hamburger Flugzeugbau in 1969, at which point it became MBB (Messerschmitt-Bölkow-Blohm, now part of EADS) with Messerschmitt as chairman until 1970 when he retired. He died eight years later, on 15 September 1978 in a Munich hospital in undisclosed circumstances.
 Messerschmitt's designs were characterized by a clear focus on performance, especially by striving for lightweight construction, but also by minimizing parasitic drag from aerodynamic surfaces. His critics accused him of taking this approach too far in some designs. His falling out with Harth had been over designs Harth felt to be dangerously unstable, and the Me 210 displayed instability, too, which could only be cured by enlarging the airframe and the aerodynamic surfaces, increasing drag and weight. Messerschmitt's design philosophy also is evident in his arguments with Alexander Lippisch, who was designing the tailless Me 163 rocket fighter for production at the Messerschmitt works. While Lippisch maintained the tailless design on principle had an advantage with regard to total drag, Messerschmitt pointed out that the design compromises necessary to make a tailless aircraft safely controllable defeated this purpose by increasing drag to the original level and above.
 Messerschmitt was appointed an Honorary Professor by the Munich Technical College in 1930, and the Vice-President of the Deutsche Akademie für Luftfahrtforschnung (German Academy of Aeronautical Research). The German government also awarded him the title of Wehrwirtschaftsführer (defense industry leader). In 1938, Adolf Hitler bestowed upon Messerschmitt the German National Prize for Art and Science.
     
 1 First sailplane designs and WWI 2 Beginning of his career 3 National Socialist Germany and World War II 4 Trial and post-war career 5 Criticism

5.1 Awards

 5.1 Awards 6 Video 7 Bibliography Frank Vann: Willy Messerschmitt. First full biography of an aeronautical genius. Sparkford: Stephens, 1993 Short Biography M 18 M 19 M 20 M 21 M 22 M 23 M 24 M 25 M 26 M 27 M 28 M 29 M 30 M 31 M 33 M 35 M 36 M 37 Bf 109 Bf 110 Bf 161 Bf 162 Bf 163 Me 163 Me 209 Me 210 Me 261 Me 262 Me 263 Me 264 Me 290 Me 309 Me 310 Me 321 Me 323 Me 328 Me 329 Me 409 Me 410 Me 609 P.1095 P.1099 P.1100 P.1101 P.1102 P.1103 P.1104 P.1106 P.1107 P.1108 P.1109 P.1110 P.1111 P.1112 1898 births 1978 deaths People from Frankfurt People from Hesse-Nassau German aerospace engineers Commanders Crosses of the Order of Merit of the Federal Republic of Germany German aircraft World War II Luftwaffe Pages using duplicate arguments in template calls Articles with hCards Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information Magyar  This page was last edited on 9 May 2013, at 01:52. Privacy policy About Metapedia Disclaimers 
  Wilhelm Emil "Willy" Messerschmitt 1958 June 26, 1898 (1898-06-26) September 15, 1978 (1978-09-16) (aged 80) German Aerospace engineer v • d • e
Messerschmitt aircraftCompany designations* M 17
M 18
M 19
M 20
M 21
M 22
M 23
M 24
M 25
M 26
M 27
M 28
M 29
M 30
M 31
M 33
M 35
M 36
M 37RLM designations 1933-1945* Bf 108
Bf 109
Bf 110
Bf 161
Bf 162
Bf 163
Me 163
Me 209
Me 210
Me 261
Me 262
Me 263
Me 264
Me 290
Me 309
Me 310
Me 321
Me 323
Me 328
Me 329
Me 409
Me 410
Me 609Experimental designations* P.1092
P.1095
P.1099
P.1100
P.1101
P.1102
P.1103
P.1104
P.1106
P.1107
P.1108
P.1109
P.1110
P.1111
P.1112USAAC designations* C-44  Company designations * M 17
M 18
M 19
M 20
M 21
M 22
M 23
M 24
M 25
M 26
M 27
M 28
M 29
M 30
M 31
M 33
M 35
M 36
M 37  RLM designations 1933-1945 * Bf 108
Bf 109
Bf 110
Bf 161
Bf 162
Bf 163
Me 163
Me 209
Me 210
Me 261
Me 262
Me 263
Me 264
Me 290
Me 309
Me 310
Me 321
Me 323
Me 328
Me 329
Me 409
Me 410
Me 609  Experimental designations * P.1092
P.1095
P.1099
P.1100
P.1101
P.1102
P.1103
P.1104
P.1106
P.1107
P.1108
P.1109
P.1110
P.1111
P.1112  USAAC designations * C-44 NAME
 Messerschmitt, Willy
 ALTERNATIVE NAMES
 
 SHORT DESCRIPTION
 
 DATE OF BIRTH
 June 26, 1898
 PLACE OF BIRTH
 
 DATE OF DEATH
 September 15, 1978
 PLACE OF DEATH
 
 Willy Messerschmitt Contents First sailplane designs and WWI Beginning of his career National Socialist Germany and World War II Trial and post-war career Criticism Video Bibliography Navigation menu Awards Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools In other languages 