Gerald Prentice Nye (December 19, 1892 – July 17, 1971) was an American politician who represented North Dakota in the United States Senate from 1925 to 1945. He was a Republican and supporter of World War II-era isolationism, chairing the Nye Committee which studied the causes of United States' involvement in World War I.
 Gerald Nye (whose first name was pronounced with a hard "G"), was born in Hortonville, Wisconsin to Irwin Raymond Nye and Phoebe Ella Nye (née Prentice). Both of his grandfathers had served in the Civil War: Freeman James Nye in the 43rd Wisconsin Volunteer Infantry Regiment and George Washington Prentice in the 3rd Wisconsin Volunteer Cavalry Regiment.
 He was the first of four children. In his first year he and his parents moved to Wittenberg, Wisconsin, where his father became owner and editor of a small newspaper. Three more children were born there: Clair Irwin, Donald Oscar, and Marjorie Ella. Nye's father was a staunch supporter of Progressive Robert M. La Follette, and Nye personally remembered his father's taking him to hear Senator La Follette speak and then meet the Senator afterwards. (Years later, Gerald Nye and Robert LaFollette the younger would serve in the U.S. Senate together.) His uncle, Wallace G. Nye, was Mayor of Minneapolis, Minnesota when Gerald was in his teens. [citation needed]
 His mother, Ella, had been diagnosed with tuberculosis. Family history indicates that she may have been asthmatic. She made trips to the South for recuperation, but on October 19, 1906 she died. He was thirteen; his brothers, ten and eight; and his baby sister, six. He was comforted by the presence of his four grandparents at the funeral.
Nye graduated from Wittenberg High School in 1911, at age 18, and moved back to his grandparents' town of Hortonville, Wisconsin. [citation needed]
 Gerald and his brother Clair had grown up helping around their father's newspaper business and learned the trade. Gerald took the editing end and Clair operated the presses. In 1911, after graduation, Nye became editor of The Hortonville Review. Three years later, he was the editor of the Creston Daily Plain Dealer in Iowa. In May 1916, he bought a weekly paper in Fryburg, North Dakota, The Fryburg Pioneer.
 Nye was a supporter of the agrarian reform movement. His editorials lambasted big government and big business. He took the side of the struggling farmers. In 1924, Nye unsuccessfully sought election as a progressive Republican to the U.S. House. When U.S. Senator Edwin F. Ladd died on June 22, 1925, he and others gathered in the office of North Dakota Governor A.G. Sorlie, who told the group that he had decided to appoint "Jerry over here" to fill the seat. [citation needed]
 Nye and his young family moved to Washington in 1925. He became a very active, popular and outspoken Senator, and North Dakotans elected him to three full terms, in 1926, 1932, and 1938. [citation needed]
 He served on the Foreign Relations Committee, the Appropriations Committee, the Defense Committee and the Public Lands Committee. As Chairman of Public Lands, he dealt with the Teapot Dome investigations and the formation of Grand Teton National Park. He was instrumental in passing legislation to protect public access to the sea coasts. He initially supported Democratic President Franklin D. Roosevelt and his New Deal. He supported the political positions of Robert M. La Follette, and legislation for agricultural price supports.
 In the 1920s, as Chairman of the Public Lands Committee, Nye uncovered the fact that Warren G. Harding's interior secretary Albert B. Fall had uncompetitively leased a government oil field to Mammoth Oil Company, in return for contributions to the Republican National Committee. The resulting scandal gave Nye the nickname of "Gerald the Giant-Killer". [citation needed]
 Between 1934 and 1936, Nye headed an investigation of the munitions industry. The Special Committee on Investigation of the Munitions Industry investigated profiteering in the munitions and banking industry and the possibility that greed was a significant factor in leading us into World War I. The Nye Committee as it was commonly known, drew national and international attention. Nye's appointment to the chairmanship of this committee came from Senator George Norris. According to peace activist, Dorothy Detzer, Norris said, "Nye's young, he has inexhaustible energy and he has courage. Those are all important assets. He may be rash in his judgments at times, but it's the rashness of enthusiasm." [1] Senator Norris proposed Nye as "...the only one out of the 96 whom he deemed to have the competence, independence and stature for the task." [2]
 Nye created headlines by drawing connections between the wartime profits of the banking and munitions industries to America's involvement in World War I. Many Americans felt betrayed: perhaps the war hadn't been an epic battle between the forces of good (democracy) and evil (autocracy). This investigation of these "merchants of death" helped to bolster sentiments for isolationism.[3] A leading member of the Nye Committee staff was Alger Hiss.
 
According to the United States Senate website: 
The investigation came to an abrupt end early in 1936. The Senate cut off committee funding after Chairman Nye blundered into an attack on the late Democratic President Woodrow Wilson. Nye suggested that Wilson had withheld essential information from Congress as it considered a declaration of war. Democratic leaders, including Appropriations Committee Chairman Carter Glass of Virginia, unleashed a furious response against Nye for 'dirtdaubing the sepulcher of Woodrow Wilson.' Standing before cheering colleagues in a packed Senate Chamber, Glass slammed his fist onto his desk until blood dripped from his knuckles.[3] Nye was instrumental in the development and adoption of the Neutrality Acts that were passed between 1935 and 1937. To mobilize antiwar sentiments, he helped establish the America First Committee. In 1941, Nye accused Hollywood of attempting to “drug the reason of the American people,“ and “rouse war fever.“ He was hostile to Jewish Hollywood moguls particularly the Warner Brothers.[4]
 The day of the Japanese bombing of Pearl Harbor on 7 December 1941, Nye attended an America First meeting in Pittsburgh. Before his speech a reporter for the Pittsburgh Post-Gazette told him about the attack, but Nye was skeptical and did not mention the news to the audience. The reporter passed him a note during the speech stating that Japan had declared war; Nye read it but continued speaking. He only announced the attack at the end of his one-hour speech, stating that he had received "the worst news that I have encountered in the last 20 years".[5][6] However, the next day Nye joined the rest of the Senate in voting for a unanimous declaration of war.[7]
 In 1943 a confidential report by Isaiah Berlin of the Senate Foreign Relations Committee for the British Foreign Office stated that Nye
 In November 1944, Nye was defeated in his re-election attempt by Governor John Moses, a Democrat. Nye chose to remain in the Washington area. He and his wife had purchased 3 acre of pasture land in Chevy Chase, part of a farm on a hill above Rock Creek Park. Their two sons had been born in 1943 and 1944.
 Nye organized and became president of Records Engineering, Inc., in Washington, D.C. The pre-computer age firm created, organized, and managed records of industrial and government clients. In 1960 he was appointed to the Federal Housing Administration as Assistant to the Commissioner and in charge of housing for the elderly. In 1963, he accepted an appointment to the professional staff of the U.S. Senate Committee on Aging. 1966 saw his grand retirement party at the U.S. Capitol. It was attended by the Senators Robert Kennedy and Ted Kennedy and hosted by Senator Everett Dirksen, who presented Nye with a typewriter and desk lamp and orders to begin his memoirs. Nye became a consultant to churches and private groups desiring government funds for the building of retirement housing.
 Nye was a Freemason and attended Grace Lutheran Church in Washington, D.C.
 On August 16, 1916, he married Anna Margaret Johnson in Iowa where she lived with her maternal grandparents and had taken their name, Munch. In 1919, they moved to Cooperstown, North Dakota where Gerald was the editor and publisher of the Sentinel Courier. Anna and Gerald had three children: Marjorie (born 1917), Robert (born 1921), and James (born 1923). His eldest three children grew up on Grosvenor Street in Washington, D.C. and attended high school there. Every summer, Gerald would take the children to Yellowstone National Park where Marjorie and a young Gerald Ford were teenage friends.
 In March 1940, Nye divorced his first wife, and on December 14, 1940, he remarried, to an Iowa schoolteacher, A. Marguerite Johnson. They had three children, all born in Washington, D.C. – Gerald Jr. (born 1943), Richard (born 1944), and Marguerite (born 1950).
 A lifelong smoker, Nye had arterial disease; the arteries in his legs were surgically replaced with plastic arteries, then state-of-the-art. Close to the end of his life, a blood clot went to his lung. While recovering from that experience, but still weak, a doctor mistakenly prescribed a drug containing penicillin, to which Nye was known to be allergic. As a result, he died on July 17, 1971, at the age of 78.[citation needed]
  
 1 Early life 2 Newspaper years 3 Political years

3.1 Teapot Dome Scandal
3.2 Nye Committee
3.3 Antiwar movement

 3.1 Teapot Dome Scandal 3.2 Nye Committee 3.3 Antiwar movement 4 Post-Senate years in Washington 5 Personal life 6 Death 7 References 8 External links ↑ Wayne S. Cole, Senator Gerald P. Nye and American Foreign Relations, Minneapolis, 1962, p. 68
 ↑ quoting the author: Barbara W. Tuchman, "The March of Folly", Random House, New York, 1984, p. 382
 ↑ 3.0 3.1 Merchants of Death. United States Senate. Retrieved on 17 January 2011.
 ↑ America First: the Anti-War Movement, Charles Lindbergh and the Second World War, 1940–41
 ↑ "Nye Slow Giving News To Firsters". Pittsburgh Post-Gazette: pp. 13. 1941-12-08. http://news.google.com/newspapers?id=lJRRAAAAIBAJ&sjid=LWoDAAAAIBAJ&pg=1539%2C6070938. Retrieved December 8, 2011. 
 ↑ Pitz, Marylynne (2001-12-02). "A decision that lives in infamy". Pittsburgh Post-Gazette. http://www.post-gazette.com/lifestyle/20011202americafirst1202fnp2.asp. Retrieved December 8, 2011. 
 ↑ Current Biography 1941, pgs. 619-21
 ↑ The scholar who edited and analyzed Berlin's report commented, "what is wholly unwarranted is Berlin's accusation that Nye had fascist connections. Nye's isolationism derived from an instinctive agrarian neutrality rather than from any ideological persuasion or anti-British bias." Hachey, Thomas E. (Winter 1973–1974). "American Profiles on Capitol Hill: A Confidential Study for the British Foreign Office in 1943". Wisconsin Magazine of History 57 (2): 141–153.
 Gerald P. Nye at the Biographical Directory of the United States Congress Roach McCumber Frazier Langer Brunsdale Q. Burdick J. Burdick Conrad Heitkamp Hansbrough Johnson Thompson Purcell Gronna Ladd Nye Moses Young Andrews Conrad Dorgan Hoeven 1892 births 1971 deaths Cardiovascular disease deaths in Maryland Deaths from surgical complications People from Outagamie County, Wisconsin American anti-war activists History of United States isolationism United States Senators from North Dakota North Dakota Republicans Appointed United States Senators Republican Party United States Senators Teapot Dome scandal America First Committee Maryland Republicans Content from Wikipedia Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 14 May 2014, at 20:24. Privacy policy About Metapedia Disclaimers 
  
The investigation came to an abrupt end early in 1936. The Senate cut off committee funding after Chairman Nye blundered into an attack on the late Democratic President Woodrow Wilson. Nye suggested that Wilson had withheld essential information from Congress as it considered a declaration of war. Democratic leaders, including Appropriations Committee Chairman Carter Glass of Virginia, unleashed a furious response against Nye for 'dirtdaubing the sepulcher of Woodrow Wilson.' Standing before cheering colleagues in a packed Senate Chamber, Glass slammed his fist onto his desk until blood dripped from his knuckles.[3] is a notorious fire-eating Anglophobe Isolationist. His principal claim to fame rests on his committee which investigated the American armament industry a few years before the war, and much popular anti-British feeling stems from publicity which was accorded to that committee. He is a member of the Farm Bloc, and possesses some influence in the Republican senatorial caucus. He has Fascist connexions, and works closely with Wheeler and Reynolds inside and outside the Senate. His bête noire is [Wendell] Willkie, whom he hates even more than the British Empire; indeed, he recently went to the length of defending the latter against the criticisms of the former, since he evidently regards any stick as good enough to beat Willkie with.[8]
 In office Gerald Prentice Nye 57 Edwin F. Ladd  United States Senator (Class 3) from North Dakota Lynn Frazier, William Langer John Moses Authority control Main article: Teapot Dome Scandal Main article: Nye Committee Main article: Teapot Dome Scandal Main article: Nye Committee Gerald Nye
 
 United States Senatorfrom North Dakota
 In officeNovember 14, 1925 – January 3, 1945
 Edwin F. Ladd
 John Moses
 
 December 19, 1892(1892-12-19)Hortonville, Wisconsin, U.S.
 July 17, 1971 (aged 78)Brentwood, Maryland, U.S.
 Republican
 Preceded byEdwin F. Ladd
  United States Senator (Class 3) from North Dakota1925–1945 Served alongside: Lynn Frazier, William Langer
 Succeeded byJohn Moses
 v • d • e
United States Senators from North DakotaClass 1* Casey
Roach
McCumber
Frazier
Langer
Brunsdale
Q. Burdick
J. Burdick
Conrad
HeitkampClass 3* Pierce
Hansbrough
Johnson
Thompson
Purcell
Gronna
Ladd
Nye
Moses
Young
Andrews
Conrad
Dorgan
Hoeven  Class 1 * Casey
Roach
McCumber
Frazier
Langer
Brunsdale
Q. Burdick
J. Burdick
Conrad
Heitkamp   Class 3 * Pierce
Hansbrough
Johnson
Thompson
Purcell
Gronna
Ladd
Nye
Moses
Young
Andrews
Conrad
Dorgan
Hoeven NAME
 Nye, Gerald
 ALTERNATIVE NAMES
 
 SHORT DESCRIPTION
 American politician and antiwar activist
 DATE OF BIRTH
 December 19, 1892
 PLACE OF BIRTH
 Hortonville, Wisconsin, U.S.
 DATE OF DEATH
 July 17, 1971
 PLACE OF DEATH
 Brentwood, Maryland, U.S.
 Gerald P. Nye Contents Early life Newspaper years Political years Post-Senate years in Washington Personal life Death References External links Navigation menu Teapot Dome Scandal Nye Committee Antiwar movement Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 