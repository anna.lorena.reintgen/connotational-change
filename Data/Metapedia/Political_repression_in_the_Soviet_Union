This article is part of the series:
Politics and government ofthe Soviet Union
 Throughout the history of the Soviet Union millions of people became victims of political repression, which was an instrument of the state since the October Revolution. Culminating during the Stalin era, it still existed during the "Khrushchev Thaw", followed by increased persecution of Soviet dissidents during the Brezhnev stagnation, and didn't cease to exist during Mikhail Gorbachev's perestroika. Its heritage still influences the life of modern Russia and other former Soviet states.[citation needed]
 Ideological repression
 Early on the theoretical basis of the repressions was the Leninist view of the class struggle and the resulting notion of the dictatorship of the proletariat. Its legal basis was formalized into the Article 58 in the code of Russian SFSR and similar articles for other Soviet republics.[citation needed]
 At times, the repressed were called the enemies of the people. Punishments by the state included summary executions, sending innocent people to Gulag, forced resettlement, and stripping of citizen's rights. At certain times, all members of a family, including children, were punished as "traitor of Motherland family members". Repression was conducted by the Cheka and its successors, and other state organs. Periods of the increased repression include Red Terror, Collectivisation, the Great Purges, the Doctor's Plot, and others. The secret police forces conducted massacres of prisoners on numerous occasions. Repression was practiced in the Soviet republics and in the territories liberated by Soviet Army during World War II, including Baltic States and Eastern Europe.[1][unreliable source?].
 State repression led to resistance, which were brutally suppressed by military force, such as the Tambov rebellion, Kronstadt rebellion, and Vorkuta Uprising. During the Tambov rebellion, Bolshevik military forces were allegedly authorized by Tukhachevsky (chief Red Army commander in the area) to use chemical weapons against villages with civilian population and rebels. According to the witnesses accounts, chemical weapons were never actually used.[2] Prominent citizens of villages were often taken as hostages and executed if the resistance fighters did not surrender.[3]
 Red Terror in Soviet Russia was the campaign of mass arrests and executions conducted by the Bolshevik government. The Red Terror was officially announced on September 2, 1918 by Yakov Sverdlov and ended in about October 1918. However Sergei Melgunov applies this term to repressions for the whole period of the Russian Civil War, 1918-1922.[4][5]
 Collectivization in the Soviet Union was a policy, pursued between 1928 and 1933, to consolidate individual land and labour into collective farms (Russian: колхо́з, kolkhoz, plural kolkhozy). The Soviet leaders were confident that the replacement of individual peasant farms by kolkhozy would immediately increase food supplies for the urban population, the supply of raw materials for processing industry, and agricultural exports generally. Collectivization was thus regarded as the solution to the crisis in agricultural distribution (mainly in grain deliveries) that had developed since 1927 and was becoming more acute as the Soviet Union pressed ahead with its ambitious industrialization program.[6] As the peasantry, with the exception of the poorest part, resisted the collectivization policy, the Soviet government resorted to harsh measures to force the farmers to collectivize. In his conversation with Winston Churchill Stalin gave his estimate of the number of "kulaks" who were repressed for resisting collectivization as 10 million, including those forcibly deported.[7][8]
 The Great Purge (Russian: Большая чистка, transliterated Bolshaya chistka) was a series of campaigns of political repression and persecution in the Soviet Union orchestrated by Joseph Stalin in 1937-1938.[9][10] It involved the purge of the Communist Party of the Soviet Union, repression of peasants, deportations of ethnic minorities, and the persecution of unaffiliated persons, characterized by widespread police surveillance, widespread suspicion of "saboteurs", imprisonment, and killings.[9] Estimates of the number of deaths associated with the Great Purge run from the official figure of 681,692 to nearly 2 million.
 In Soviet Union, political repressions targeted not only individual persons, but also whole ethnic, social, religious, and other categories of population.
 Population transfer in the Soviet Union may be classified into the following broad categories: deportations of "anti-Soviet" categories of population, often classified as "enemies of workers"; deportations of nationalities; labor force transfer; and organized migrations in opposite directions to fill the ethnically cleansed territories. In most cases their destinations were underpopulated remote areas (see Involuntary settlements in the Soviet Union).
 Entire nations and ethnic groups were collectively punished by the Soviet Government for alleged collaboration with the enemy during World War II. At least nine distinct ethnic-linguistic groups, including ethnic Germans, ethnic Greeks, ethnic Poles, Crimean Tatars, Balkars, Chechens, and Kalmyks, were deported to remote unpopulated areas of Siberia (see sybirak) and Kazakhstan. Population transfer in the Soviet Union led to millions of deaths from the inflicted hardships.[11] Koreans and Romanians were also deported. Mass operations of the NKVD were needed to deport hundreds of thousands of people.
 The Soviet famine of 1932-1933 was severely aggravated by the actions of the government of the Soviet Union, such as the confiscation of food, the lack of meat, planned delivery limitations that ignored the famine, blocking the migration of its starving population, and the suppression of the information about the famine, all of which prevented any organized relief effort. This led to deaths of millions of people in the affected area.[11] The overall number of the 1932-1933 famine victims Soviet-wide is variously estimated as 6-7 million[12] or 6-8 million.[13]
 Gulag: A History, by Anne Applebaum,[14] explains: "It was the branch of the State Security that operated the penal system of forced labour camps and associated detention and transit camps and prisons. While these camps housed criminals of all types, the Gulag system has become primarily known as a place for political prisoners and as a mechanism for repressing political opposition to the Soviet state.”[citation needed]
 During the early years of World War II Soviet Union annexed several territories in East Europe as the consequence of the German-Soviet Pact and its Secret Additional Protocol.[15]
 In the Baltic countries of Estonia, Latvia and Lithuania, repressions and the mass deportations were carried out by the Soviets. The Serov Instructions, "On the Procedure for carrying out the Deportation of Anti-Soviet Elements from Lithuania, Latvia, and Estonia", contained detailed procedures and protocols to observe in the deportation of Baltic nationals. Public tribunals were also set up to punish "traitors to the people": those who had fallen short of the "political duty" of voting their countries into the USSR. In the first year of Soviet occupation, from June 1940 to June 1941, the number confirmed executed, conscripted, or deported is estimated at a minimum of 124,467: 59,732 in Estonia, 34,250 in Latvia, and 30,485 in Lithuania.[16] This included 8 former heads of state and 38 ministers from Estonia, 3 former heads of state and 15 ministers from Latvia, and the then president, 5 prime ministers and 24 other ministers from Lithuania.[17]
 After Stalin's death, the suppression of dissent was dramatically reduced and took new forms. The internal critics of the system were convicted for anti-Soviet agitation, Anti-Soviet slander, or as "social parasites". Others were labeled as mentally ill, having sluggishly progressing schizophrenia and incarcerated in "psikhushkas", i.e. mental hospitals used by the Soviet authorities as prisons.[18] A number of notable dissidents, including Aleksandr Solzhenitsyn, Vladimir Bukovsky, and Andrei Sakharov, were sent to internal or external exile.
 The exact number of victims may never be known and remains a matter of debate among historians. The published results vary depending on the time when the estimate was made, on the criteria and methods used for the estimates, and sources available for estimates. Some historians attempt to make separate estimates for different periods of the Soviet history. For example, the number of victims under Joseph Stalin's regime vary from 8 to 61 million [5][19][20][21][22][23]
 Russian writer Aleksandr Solzhenitsyn, who deeply studied this problem, considers that 66 million 700 thousand people became victims to the state repression and terrorism from 1917 to 1959.[24]:375 Analogous figure — over 66 million people — was announced by Alexander Nikolaevich Yakovlev, the chairman of the commission for rehabilitation of repressed persons.[24]:375, 376 According to Viktor Luneyev, actual struggle against dissent was manyfold larger than it was registered in sentences, and we do not know how many persons were kept under surveillance of secret services, held criminally liable, arrested, sent to psychiatric hospitals, expelled from their work, restricted in their rights everyway.[24]:373 No objective counting of repressed persons is possible without fundamental analysis of archival documents.[24]:378 The difficulty of this method is that the required data are very diverse and are not in one archive.[24]:378 They are in the State Archive of the Russian Federation, in the archive of the Goskomstat of Russia, in the archives of the MVD of Russia, the FSB of Russia, the General Prosecutor's Office of the Russian Federation, in the Russian Military and Historical Archive, in archives of constituent entities of the Russian Federation, in urban and regional archives, as well as in archives of the former Soviet Republics that now are independent countries of the Commonwealth of Independent States and the Baltics.[24]:378 Where, for example, can one determine the number of people shot in the second half of the 1930s and in the early 1940s in the Kurapaty hole (estimate for them is from 30 to 100 thousand people or, by some estimates, up to 200 thousand)?[24]:378 Only in Belarus.[24]:378 The same can be said of other mass shootings and other forms of repression of victims of Soviet regime.[24]:378
 Day of Remembrance of the Victims of Political Repressions (День памяти жертв политических репрессий - October 30, since 1991), in former Soviet republics (except for Ukraine, which has its own annual Day of Remembrance for the victims of political repressions by the Soviet regime on third Sunday of May). Members of the Memorial society take active part in meetings.[citation needed]
 Constitution Government
Ministries
State Committees
Executive Officer Ministries State Committees Executive Officer Council of People's Commissars Council of Ministers Cabinet of Ministers State Council Presidential Council Communist Party
Congress
History Congress History General Secretary
Politburo
Central Committee
Secretariat
Orgburo Politburo Central Committee Secretariat Orgburo Leaders Premiers
Cabinets Cabinets President (List)
Vice President Vice President Collective leadership Congress of Soviets
Central Executive Committee Central Executive Committee Supreme Soviet
Soviet of the Union
Soviet of Nationalities
Presidium Soviet of the Union Soviet of Nationalities Presidium Congress of People's Deputies
Speaker
1989 Legislative election Speaker 1989 Legislative election Law
Supreme Court
People's Court
Procurator General Supreme Court People's Court Procurator General 1917–1927
Revolution
Civil War Revolution Civil War 1927–1953
World War II World War II 1953–1964
Khrushchev Thaw Khrushchev Thaw 1964–1982
Era of Stagnation Era of Stagnation 1982–1991
Dissolution Dissolution State Ideology
Soviet democracy
Marxism-Leninism
Leninism
Stalinism Soviet democracy Marxism-Leninism Leninism Stalinism Economy
Agriculture
Consumer goods
Five-Year Plan
Kosygin reform
New Economic Policy
Science and technology
Era of Stagnation
Material balance planning Agriculture Consumer goods Five-Year Plan Kosygin reform New Economic Policy Science and technology Era of Stagnation Material balance planning Culture
Demographics
Education
Family
Phraseology
Religion
Transport Demographics Education Family Phraseology Religion Transport Repression
Censorship
Censorship of images
Economic repression
Great purge
Gulag system
Collectivization
Human rights
Mass killings
Ideological repression
Suppressed research
Political abuse of psychiatry
Political repression
Population transfer
Propaganda
Red Terror Censorship Censorship of images Economic repression Great purge Gulag system Collectivization Human rights Mass killings Ideological repression Suppressed research Political abuse of psychiatry Political repression Population transfer Propaganda Red Terror 1 Origins and early Soviet times 2 Red Terror 3 Collectivization 4 Great Purge 5 Genocide, ethnic cleansing, and population transfers 6 Gulag 7 Repressions in annexed territories

7.1 Baltic States
7.2 Poland
7.3 Romania

 7.1 Baltic States 7.2 Poland 7.3 Romania 8 Post-Stalin era (1953-1991) 9 Loss of life 10 Difficulties in counting the repressed 11 Remembering the victims 12 See also 13 References

13.1 Notes
13.2 Bibliography

 13.1 Notes 13.2 Bibliography 14 Further reading Active measures Human rights in the Soviet Union Rehabilitation (Soviet) Soviet political system Soviet law Hitler Youth Conspiracy ↑ Anton Antonov-Ovseenko Beria (Russian) Moscow, AST, 1999. Russian text online[unreliable source?]
 ↑ Химико-политический туман (Chemical Political Fog) by Alexander Shirokorad.
 ↑ Courtois, et al, 1999: [page needed]
 ↑ Serge Petrovich Melgunov, Red Terror in Russia, Hyperion Pr (1975), ISBN 0-883-55187-X
 ↑ 5.0 5.1 Courtois et al., 1999: [page needed]
 ↑ Davies, R.W., The Soviet Collective Farms, 1929-1930, Macmillan, London (1980), p. 1.
 ↑ Valentin Berezhkov, "Kak ya stal perevodchikom Stalina", Moscow, DEM, 1993, ISBN 5-85207-044-0. p. 317
 ↑ Stanislav Kulchytsky, "How many of us perished in Holodomor in 1933", Zerkalo Nedeli, November 23–29, 2002.
 ↑ 9.0 9.1 Figes, 2007: pp. 227-315
 ↑ Lenin, Stalin, and Hitler: The Age of Social Catastrophe. By Robert Gellately. 2007. Knopf. 720 pages ISBN 1400040051
 ↑ 11.0 11.1 Conquest, 1986: [page needed]
 ↑ С. Уиткрофт (Stephen G. Wheatcroft), "О демографических свидетельствах трагедии советской деревни в 1931—1933 гг." (On demographic evidence of the tragedy of the Soviet village in 1931-1833), "Трагедия советской деревни: Коллективизация и раскулачивание 1927-1939 гг.: Документы и материалы. Том 3. Конец 1930-1933 гг.", Российская политическая энциклопедия, 2001, ISBN 5-8243-0225-1, с. 885, Приложение № 2
 ↑ "Ukraine", Encyclopædia Britannica, 2008.
 ↑ Gulag: A History, by Anne Applebaum
 ↑ The Soviet occupation and incorporation at Encyclopædia Britannica
 ↑ Dunsdorfs, Edgars. The Baltic Dilemma. Speller & Sons, New York. 1975
 ↑ Küng, Andres. Communism and Crimes against Humanity in the Baltic States. 1999 [1]
 ↑ The Soviet Case: Prelude to a Global Consensus on Psychiatry and Human Rights. Human Rights Watch. 2005
 ↑ Ponton, G. (1994) The Soviet Era.
 ↑ Tsaplin, V.V. (1989) Statistika zherty naseleniya v 30e gody.
 ↑ Nove, 1993: [page needed]
 ↑ Davies, Norman. Europe: A History, Harper Perennial, 1998. ISBN 0-06-097468-0.
 ↑ Bibliography: Rummel.
 ↑ 24.0 24.1 24.2 24.3 24.4 24.5 24.6 24.7 24.8 Лунеев, Виктор (2005). Преступность XX века: Mировые, региональные и российские тенденции. Wolters Kluwer Russia. ISBN 5466000981. 
 Conquest, Robert (1986). The Harvest of Sorrow: Soviet Collectivization and the Terror-Famine. Oxford University Press. ISBN 0-19-505180-7.   (1999) The Black Book of Communism: Crimes, Terror, Repression. Harvard University Press. ISBN 0-674-07608-7.  Figes, Orlando (2007). The Whisperers: Private Life in Stalin's Russia. Macmillan. ISBN 0-08050-7461-9.   (2001) Beyond invisible walls: the psychological legacy of Soviet trauma, East European therapists, and their patients. Psychology Press. ISBN 9781583913185.  Nove, Alec (1993). "Victims of Stalinism: How Many?", Stalinist Terror: New Perspectives. Cambridge University Press. ISBN 0-521-44670-8.  Brooks, Jeffrey (2000). Thank you, comrade Stalin!: Soviet public culture from revolution to Cold War. Princeton University Press. ISBN 9780691004112.  Leggett, George (1981). The Cheka: Lenin's Political Police. Oxford: Clarendon Press. ISBN 0-19-822862-7.  Medvedev, Roy Aleksandrovich (1985). On Soviet Dissent. Columbia University Press. ISBN 9780231048132.  Rosefielde, Steven (2009). Red Holocaust. Taylor & Francis. ISBN 978-0-415-77757-5.  Shearer, David R. (2009). Policing Stalin's socialism: repression and social order in the Soviet Union, 1924-1953. Yale University Press. ISBN 9780300149258.  Solomon, Peter H. (1996). Soviet criminal justice under Stalin. Cambridge University Press. ISBN 9780521564519.  Wintrobe, Ronald (2000). The Political Economy of Dictatorship. Cambridge University Press. ISBN 9780521794497.  Telephone tapping in the Eastern Bloc Berlin Wall Inner German border People's Commissariat for Internal Affairs (NKVD) State Political Directorate (OGPU) Main Directorate of State Security (GUGB) People's Commissariat for State Security (NKGB) Ministry for State Security (MGB) Committee for State Security (KGB) Security Service (SB) Political repression in the Soviet Union Articles containing non-English language text Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 11 March 2012, at 21:23. Privacy policy About Metapedia Disclaimers 
  Soviet Union Politics and government ofthe Soviet Union Government Communist Party Leadership Legislature Judiciary History Ideology Economy Society USSR Portal political repression Main article: Red Terror Main article: Collectivization in the USSR Main article: Great Purge Main article: Population transfer in the Soviet Union Main article: Gulag Main article: Occupation of Baltic States#Soviet terror Main article: Soviet invasion of Poland Main article: Soviet occupation of Bessarabia and Northern Bukovina Main article: Dissident movement in the Soviet Union Main article: Day of Remembrance of the Victims of Political Repressions Main article: Red Terror Main article: Collectivization in the USSR Main article: Great Purge Main article: Population transfer in the Soviet Union Main article: Gulag Main article: Occupation of Baltic States#Soviet terror Main article: Soviet invasion of Poland Main article: Soviet occupation of Bessarabia and Northern Bukovina Main article: Dissident movement in the Soviet Union Main article: Day of Remembrance of the Victims of Political Repressions Soviet Union
 
This article is part of the series:
Politics and government ofthe Soviet Union

 

 

Government

Constitution
Government
Ministries
State Committees
Executive Officer
Council of People's Commissars
Council of Ministers
Cabinet of Ministers
State Council
Presidential Council



Communist Party

Communist Party
Congress
History
General Secretary
Politburo
Central Committee
Secretariat
Orgburo



Leadership

Leaders
Premiers
Cabinets
President (List)
Vice President
Collective leadership



Legislature

Congress of Soviets
Central Executive Committee
Supreme Soviet
Soviet of the Union
Soviet of Nationalities
Presidium
Congress of People's Deputies
Speaker
1989 Legislative election



Judiciary

Law
Supreme Court
People's Court
Procurator General



History

1917–1927
Revolution
Civil War
1927–1953
World War II
1953–1964
Khrushchev Thaw
1964–1982
Era of Stagnation
1982–1991
Dissolution



Ideology

State Ideology
Soviet democracy
Marxism-Leninism
Leninism
Stalinism



Economy

Economy
Agriculture
Consumer goods
Five-Year Plan
Kosygin reform
New Economic Policy
Science and technology
Era of Stagnation
Material balance planning



Society

Culture
Demographics
Education
Family
Phraseology
Religion
Transport
Repression
Censorship
Censorship of images
Economic repression
Great purge
Gulag system
Collectivization
Human rights
Mass killings
Ideological repression
Suppressed research
Political abuse of psychiatry
Political repression
Population transfer
Propaganda
Red Terror


 

Atlas USSR Portalview • talk • edit
 Political repression • Economic repression • Ideological repression
 Red Terror • Collectivization • Great Purge • Population transfer • Gulag • Holodomor • Mass killings under Communist regimes • Political abuse of psychiatry
 Religion • Suppressed research • Censorship • Censorship of images
 v • d • e
 v • d • e
Secret police agencies in Communist Eastern Europe* Iron Curtain
Telephone tapping in the Eastern Bloc
Berlin Wall
Inner German borderSoviet Union* Extraordinary Commission (Cheka)
People's Commissariat for Internal Affairs (NKVD)
State Political Directorate (OGPU)
Main Directorate of State Security (GUGB)
People's Commissariat for State Security (NKGB)
Ministry for State Security (MGB)
Committee for State Security (KGB)Socialist People's Republic of Albania* Directorate of State Security (Sigurimi)People's Republic of Bulgaria* Committee for State Security (DS)Czechoslovak Socialist Republic* State Security (StB)German Democratic Republic* Ministry for State Security (Stasi)People's Republic of Hungary* State Protection Authority (ÁVH)People's Republic of Poland* Ministry of Public Security (UB)
Security Service (SB)Socialist Republic of Romania* Department of State Security (Securitate)Socialist Federal Republic of Yugoslavia* Department of State Security (UDBA)  * Iron Curtain
Telephone tapping in the Eastern Bloc
Berlin Wall
Inner German border  Soviet Union * Extraordinary Commission (Cheka)
People's Commissariat for Internal Affairs (NKVD)
State Political Directorate (OGPU)
Main Directorate of State Security (GUGB)
People's Commissariat for State Security (NKGB)
Ministry for State Security (MGB)
Committee for State Security (KGB)  Socialist People's Republic of Albania * Directorate of State Security (Sigurimi)  People's Republic of Bulgaria * Committee for State Security (DS)  Czechoslovak Socialist Republic * State Security (StB)  German Democratic Republic * Ministry for State Security (Stasi)  People's Republic of Hungary * State Protection Authority (ÁVH)  People's Republic of Poland * Ministry of Public Security (UB)
Security Service (SB)  Socialist Republic of Romania * Department of State Security (Securitate)  Socialist Federal Republic of Yugoslavia * Department of State Security (UDBA) Political repression in the Soviet Union Contents Origins and early Soviet times Red Terror Collectivization Great Purge Genocide, ethnic cleansing, and population transfers Gulag Repressions in annexed territories Post-Stalin era (1953-1991) Loss of life Difficulties in counting the repressed Remembering the victims See also References Further reading Navigation menu Baltic States Poland Romania Notes Bibliography Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 