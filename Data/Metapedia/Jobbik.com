Jobbik, The Movement for a Better Hungary (Hungarian: Jobbik Magyarországért Mozgalom), commonly known as Jobbik, is a patriotic national liberation movement representing the interests of the Hungarian people. The party describes itself as a principled, conservative and radically patriotic Christian party, whose fundamental purpose is the protection of Hungarian values and interests.[2] Measured according to its representation in the European Parliament and the National Assembly, it is Hungary's third largest party.
 Originally established in 2002 as the Right-Wing Youth Association (Jobboldali Ifjúsági Közösség – JOBBIK) by a group of Catholic and Protestant university students, Jobbik was eventually founded as a political party in October 2003.[3]Template:Third-party-inline Instrumental in this was the person of Gergely Pongrátz, who in a speech to the founding conference made reference to the Hungarian Revolution of 1956.[4]Template:Third-party-inline
 Around Christmas 2003, Jobbik started a nationwide cross-erecting action to remind Hungarians of the "true meaning" of the holiday. The move was disapproved by several Christian intellectual groups, while the churches did not officially object to the political absorption of the religious symbol.[5] The party has adopted the Árpád stripes as a symbol. The stripes have been in use since the 13th century and have traditionally been associated with Hungary, deriving from the coat of arms of the House of Árpád, the dynasty which St. Stephen I of Hungary belonged to.
 Even though the far-right Hungarian Justice and Life Party (MIÉP) and Jobbik had publicly shown their mutual aversion beforehand, both parties entered an electoral alliance for the 2006 national elections, called the MIÉP–Jobbik Third Way Alliance of Parties. Its intention was seen as winning votes from the major conservative Fidesz party.[6]
 In the 2006 Hungarian national elections the alliance won only 2.2% of the votes. Therefore, Jobbik termed the alliance a failure and virtually broke it up. In 2009 the State Audit Office (ÁSZ) reported the alliance for grave breaches of accounting rules. Jobbik blamed MIÉP alone for the irregularities.[7]
 Jobbik was perfectly positioned to capitalize on the mainstream growth in nationalist sentiment associated with the 2006 protests in Hungary. After October 2006 it felt confirmed in the belief that the events of 1989 had essentially been a sham, because “Communists” were still in charge. Subsequent events permitted the party to consolidate its growth.[citation needed]
 In 2007 the party published a policy document[8] which sought to explain and find solutions to the problems of the preceding years. Named after Gábor Bethlen, it claimed that the country would suffer from a “crisis of constitutionality”, depicted all parties as acting to benefit themselves, corrupt, or idle. However, Jobbik saw a lack of support by the media as an obstacle to disseminating its policy approach. As a result the party was compelled to mobilize primarily at the local level, which however, would bring it closer to the immediate concerns of the Hungarian electorate, namely the local effects of the global financial crisis, and the perceived rise of crime.[citation needed]
 The party faced its first electoral test with the coming of the 2009 European parliamentary elections. Once again however, it felt that it was being intentionally excluded by the media from the opportunity of presenting its (symbolically 56 page[9]) manifesto to the public;[10] Consequently, party chairman Vona and Krisztina Morvai (who had agreed to head the party’s list), conducted a nationwide tour to take the party manifesto directly to the electorate. The election’s results surprised Jobbik[11] as much as it shocked their opponents:[12] the party sending three MEPs to Strasbourg; coming close to level with the governing Hungarian Socialist Party (MSZP) while eliminating their liberal coalition partner Alliance of Free Democrats (SZDSZ), to become the nation’s third party.[13] Unsurprisingly, Morvai’s first speech[14] to the European parliament called for the chamber to be conscious of human rights abuses within the community, i.e. Hungary, in order to secure legitimacy about them taking place in other nations, i.e. Iran; she asserted that every major anti-government demonstration had suffered the same fate since 2006.[15]Template:Third-party-inline Vona himself was pepper sprayed, handcuffed and arrested at a seated protest in Budapest on 4 July 2009,[16]Template:Third-party-inline
 Hungary's 2010 parliamentary elections saw Jobbik cement its position as the nation's 3rd largest party, doubling the vote it had received in the previous year and getting just 3 seats less than the previous ruling party MSZP (the Post-Bolshevistic party).
 October 25, 2009 Jobbik founded the Alliance of European National Movements in Budapest together with political parties from four European countries, Nationaldemokraterna (Sweden), Fiamma Tricolore (Italy), Front National (France) and the Front National (Belgium).[17]

 The original programme (in Hungarian) contains 88 pages. There is a shortened English language manifesto: (24 pages) [18]
 The Movement for a Better Hungary more commonly goes under its abbreviated name Jobbik (pronounced [ˈjobbik]), which is in fact a play on words. The word Jobb in Hungarian has two meanings, the adjective for “better” and the direction “right,” the comparative Jobbik therefore means both ‘the more preferable choice’ and ‘more to the right’. This is similar to the English "Right Choice" meaning both Conservative and Proper.[citation needed]
 The meaning of the party’s 2009 election slogan “Hungary belongs to the Hungarians” (Magyarország a Magyaroké!) was also the subject of considerable scrutiny. Some critics thought the slogan essentially tautological,[19] while others were sufficiently concerned to mount a successful complaint at the National Electoral Commission; which ruled it “unconstitutional” on the very eve of the election.[20] Jobbik maintained that the use of the slogan’s wording was justified because Hungary’s postbolshevic political and economic elite had been engaged in enriching themselves through placing the majority of the country’s significant assets into private or foreign ownership.[21]
 Gábor Vona has set out his thoughts in "What do we mean by radicalism?" [22] Jobbik rejects the common classification of the political spectrum in left and right. It prefers a distinction of political parties based on their stance towards globalisation. On this scheme, the party sees itself as patriotic.[23]
 Jobbik's ideology has been described as national ideology, whose strategy “relies on a combination of ethno-nationalism with anti-elitist rhetoric and a radical critique of post-bolshevistic political institutions and theft of production establishments and real-estate by post bolshevistic, liberal Jewish "privatizers" .[24][25] The most conspicuous advocate of the Hungarian expression of this nationalistic trend, radical nationalism, is Gábor Vona.
 In the place of this illusory divide Jobbik substitutes the twin ideologies of, first, a virtually unrestrained capitalism (namely economic Neoliberalism), and second, modern social liberalism (deemed to include Political correctness and Multiculturalism); which it maintains are the two primary forces actually governing the lives of the modern European (and by extension Hungarian) citizen. But it is also mistaken to believe that these two ideologies are opposed, and in conflict. For Jobbik they represent the status quo, to which the majority of established politicians unquestioningly subscribe.[26] Moreover, any questioning would be pointless because these twin ideologies operate at the trans-national rather than national level. They are imposed and enforced by the directives and regulations of the European Union, which in the form of the Commission has already exclusively appropriated the most significant executive powers of national governments; and whose democratic removal the European elector is (by design) incapable of achieving.[27] As a result the Hungarian citizen is forced to endure a succession of day-to-day realities that fly in the face of their own interests;[28] and there is nothing of merit that politicians from the two main parties can do about it, as a result they either spend their time enriching themselves, or manufacturing political controversies over symbolic issues in order to divert attention from their own impotence and corruption.[29] This is the radical element of Jobbik's ideology.
 The nationalist element comes from the suggested solution to this dilemma. As these two currently governing ideologies are considered, by definition, indifferent to the economic fate or cultural survival of the Hungarian people: nationalism is revealed as a sensible, intelligent and worthwhile political choice.[29] And a new and realistic divide emerges, which permits one to make valid and useful distinctions between policy options or Euro-legislation. Namely, does it serve the national interest, or not. As Morvai Krisztina remarked on the 2009 campaign trail, “We have no use in Hungary, for those laws,[30] which in our own land actively disadvantage Hungarians and prioritise the interests of foreigners.”[31] Consequently, Vona has stated that he sees one of the key functions of the party to be compelling those in Hungarian public life to profess which side of this actual political dividing line they occupy. To declare “on which side of the real barricade they stand.”[32] The party’s magazine is entitled “Barricade” (Barikád)[33] for this reason.

 Jobbik's interpretation of Hungarian nationalism also includes the ideology of Turanism which stresses the (alleged) origin of the Magyar peoples in Central Asia, and the links of the Hungarians to Asian, especially Turkic peoples. Therefore Jobbik leader Gábor Vona favors Hungaria turning away from the so called, zionistic West (including the Euro-Atlantic alliances) and towards the "East", to form a "Turanian alliance" whose "Western bastion" Hungary should become.[34]
 Jobbik's Greater Hungarian irredentist claims can be found in pleas for cross-border ethnic self-determination. For example, the party demands "territorial autonomy" for the Székely Land in Romania and desires to make Transcarpathian Ukraine an independent Hungarian district.[35]
 Jobbik tries to clean Hungarian culture from Bolshevistic-liberal pseudo culture and pornography.[36]
 Jobbik rejects the globalised capitalism, and the influence of foreign investors in Hungary.[37]
 Jobbik dedicates itself to supporting the cause of the significant (a quarter of Hungarians live outside Hungary[41]) Hungarian minorities that exist external to the nation's territory in countries bordering Hungary[42] in their campaigns[citation needed] to achieve self-determination and autonomy.[43]
 Of the three victorious powers of World War I it was the United States under Woodrow Wilson which argued for self-determination amongst the ethnic communities of the Austro-Hungarian empire.The city of Sopron was given the opportunity of deciding whether it wished to become part of Hungary or Austria through a plebiscite in 1921 following unrest. It decided on the former, but no other town was again granted the privilege.</ref> France and Britain however, arguably had a broader strategic agenda of ensuring that any future resurgent Germany would not have a European industrial ally to call upon. They therefore sought by their boundary decisions, primarily to economically cripple Austria and Hungary respectively to ensure this objective. Though their intentions are a subject of debate, two consequences of their actions nevertheless followed. First, Austria-Hungary having been an autarky, its successor states were indeed economically crippled; Germany in World War II had no such industrially significant ally.) Second, the Treaty of Trianon (which in 1920 specifically partitioned the Kingdom of Hungary) left large ethnically Hungarian communities (whose residence had spanned centuries) outside the territorial border of modern Hungary; such communities today making up one quarter of Central Europe’s Magyar population.[41] As a consequence the Hungarian constitution states that, “The Republic of Hungary bears a sense of responsibility for what happens to Hungarians living outside of its borders and promotes the fostering of their relations with Hungary.”[44]
 However, according to Jobbik, successive Hungarian governments have found it inconvenient to honour this commitment. The resolution of the Magyar minority issue was sold as a chief benefit of joining the European Community. In Jobbik's view, the EU seems merely to have hoped that accession would have made this issue somehow disappear. Jobbik sees the EU's continued and resolute refusal to get involved in any alleged incident of the infringement of Magyar minority rights as being in stark contrast to frequent and repeated highlighting of the Roma minority issue. In Hungary, Jobbik argues, minority issues are actively promoted and defended (through exclusive multi-ethnic radio and TV times, and dedicated social funding); the deliberate curtailment of Magyar rights is seen as not merely a rhetorical feature of the political extremes of some neighbouring countries, but actually a part of official government policy or of persisting legislation. (e.g. the racist Beneš decrees) Jobbik campaigns for these communities to be given the opportunity of self-determination that was originally denied them. As a result the party has caused concern amongst those whose political or constitutional principles are strictly opposed to even countenancing such a proposal, due to commitments to territorial integrity.
 Jobbik have been victims of a vast and extensive Hungarophobic propaganda campaign in the international press. This largely eminates from decadent liberal "academics"/public policy think-tanks in the West, barely concealed supporters of communism and Jewish supremacists (who support Khazarian ethnocracy in Palestine but work tirelessly against Christian nations). These identity groups and others are opposed to Hungary being a healthy Christian nation which serves the interests of its own people and choose to attack Jobbik for that reason. A broad array of operant conditioning epithets are slung out against Jobbik, including the typical playground name-calling of "fascist", "anti-semitic", "anti-gypsy", "homophobic", etc. This propaganda campaign also includes Wikipedia, which features a sardonic and heavily bias article against the party, violating its own supposed premise of neutral point of view for a completely Judeocentric line.
 The symbol of the Guard, the crest of King Emeric, decorated with lions and Árpád stripes, which is also part of their uniform.
 In June 2007 president Vona, supported by the party, founded and registered the organisation called Magyar Gárda, which says in its deed of foundation that it intends to become “part or core” of the national guard to be set up in accordance with the Bethlen Gábor programme, and it also wishes to participate actively “in strengthening national self-defence” and “maintaining public order” as well as supporting and organising social and charity missions, in disaster prevention and civil defence. The foundation of the Guard was accompanied by sharp political debate.
 On 10 March 2008 three leading figures resigned from the party: Dávid Kovács, the founding president of the party, Ervin Nagy, committee chairman, and Márton Fári, former chairman of the party’s ethical committee. They indicated the Hungarian Guard as the cause of their resignation, stating that "Jobbik has been merged inseparably with the Guard, taking responsibility for something that it cannot really control in the long run".
 On 2 July 2009 the Metropolitan Court of Appeal (Fővárosi Ítélőtábla) disbanded the "Magyar Gárda" Organization because the court held that the activities of the organization were against the human rights of minorities as guaranteed by the Hungarian Constitution. The Guard has attempted to reorganize itself as a civil service association, known as the Magyar Gárda Foundation, engaged in cultural and nation building activities rather than politics. Its renewed activities are opposed by the Hungarian authorities [45] and prosecutors claim that the founding of the new organization is in contempt of previous court rulings.
 Jobbik argues that the national police should be greatly strengthened and -with the FIDESZ- supports introducing a "three strikes law" [46]
The radical nationalism in Hungary, including Jobbik protested against Israeli President Shimon Peres, after he said that "Israel is buying up Hungary".[47]
 For the Hungarian Parliament:
 *In an electoral alliance with MIÉP, under the name of the "MIÉP-Jobbik Third Way Alliance of Parties", joined by Independent Smallholders’ Party (FKgP) organisations from 15 counties.
 For the European Parliament:
 Municipal:
 Mayoral:
 
  
 1 History and development

1.1 Foundation
1.2 Alliances
1.3 Growth and electoral success
1.4 International relations

 1.1 Foundation 1.2 Alliances 1.3 Growth and electoral success 1.4 International relations 2 Manifesto and political programme 3 Issues and ideology

3.1 Linguistic clarifications
3.2 Patriotism
3.3 Cultural conservatism

 3.1 Linguistic clarifications 3.2 Patriotism 3.3 Cultural conservatism 4 Policy position

4.1 The economy
4.2 Magyar irredentism

 4.1 The economy 4.2 Magyar irredentism 5 Hungarophobic campaign 6 The Hungarian Guard 7 Election results 8 Quotes 9 References 10 External links Krisztina Morvai Zoltán Balczó - His seat EP was taken over by Béla Kovács, when he became a member of the Hungarian Parliament in May 2010. Csanád Szegedi Tiszavasvári - Erik Fülöp Hencida - László Szémán Hegyháthodász - Roland Dervalics Gyöngyöspata - Oszkár Juhász ↑  Nationalist Jobbik Party Doubles Voter Base In Hungary, xpatloop.com, 2009-06-25, http://www.xpatloop.com/news/61800 
 ↑ Jobbik confident of winning EP seat, party leader says. politics.hu (source: MTI) (2009-05-13). “Jobbik describes itself as “a principled, conservative and radically patriotic Christian party. Its fundamental purpose is protecting Hungarian values and interests.””
 ↑  Miért alakult meg a Jobbik Magyarországért Mozgalom-párt (Why was the Movement for a Better Hungary founded?), zuglo.jobbik.hu (Hungarian), 2008-06-01, http://zuglo.jobbik.hu/miert_alakult_meg_a_jobbik_magyarorszagert_mozgalom_part 
 ↑ Pongrácz, Gergely (2003-10-24), Pongrátz Gergely megható beszéde a Jobbik alakuló gyűlésén (Gergely Pongrácz’s moving speech to the Jobbik founding conference), youtube.com (Hungarian @ 6:25), http://www.youtube.com/watch?v=5Q7vVCfo-hY, "The torch is now falling from our hands, it is you who must take it up, that spirit, those values, for which so many brother-in-arms died in ’56. It is you who must take it onwards. (A fáklya kiesik a kezünkből, nektek kel átvenni, azt a szellemiséget, azokat az eszméket, amiért 56-ba olyan sok bajtársuk halt meg. Nektek kell tovább vinni.)" 
 ↑ Szilágyi, Tamás (2008). Sacred Characteristics of the Nation: "Hungarianism" as Political Religion? (PDF). revacern.eu. “The other case, which drew the attention of the public, is the country‐wide cross erecting “actions” of JOBBIK Party during Christmas, which started in 2003... Several Christian intellectual groups have disapproved these actions; however, no clear objection appeared from the churches against the political appropriation of the religious symbol.”
 ↑ The nationalist Right Gets Together: "Third way" platform. hvg (2005-10-17).
 ↑  Prosecutors target Jobbik-MIÉP 2006 election vehicle, Politics.hu, 2009-08-19, http://www.politics.hu/20090819/prosecutors-target-jobbikmiep-2006-election-vehicle 
 ↑ Jobbik (2007). Gábor Bethlen Programme (PDF). Jobbik.
 ↑ Jobbik (2009-03). Magyarország a magyaroké! – A Jobbik programja a Magyar érdek védelmében, a Nemzetek Európája megteremtéséért (Hungary belongs to the Hungarians! – Jobbik’s programme for the defence of the Hungarian nation’s interests, and the creation of a Europe of Nations (PDF). Jobbik (Hungarian).
 ↑ Vona, Gábor (2009-05-18). Vona Gábor Balassagyarmaton (Gábor Vona in Balassagyarmat). YouTube @ 5:11. “Hungarian: Ugye nagyon nehéz nekünk elmondani a saját programunkat. Abban a nem megtisztelhető helyzetben vagyunk, hogy az ember megkapcsolja a Tévét és Kolompár Orbán meg Horn Gábor beszél a Jobbik programjáról. Nem Morvai Krisztina, nem én, vagy nem valamelyik másik politikusunk. / English: Of course it is very difficult for us to explain our own programme. We are in an unfortunate predicament, where one switches on the television and Orbán Kolompár (Gypsi activist) or Gábor Horn (SZDSZ politician) are talking about the Jobbik programme. Not Krisztina Morvai, not myself, nor any other politician of ours.”
 ↑ Vona, Gábor (2009-06-12). Vona Gábor az Estében (Gábor Vona on Tonight). YouTube (Hungarian).
 ↑ Balogh, Eva (2009-06-07). European parliamentary elections: Hungary. Hungarian Spectrum. Archived from the original on 2012-07-08.
 ↑ Results of the 2009 European elections: Hungary. [www.europarl.europa.eu] (English) (2009-07-08).
 ↑ Morvai, Krisztina (2009-07-15). Jobbik MEPs first speeches to the European Parliament. YouTube @ 1:32.
 ↑ Human Rights Violations in Hungary. YouTube (2009-07-13).
 ↑ Gábor Vona’s arrest. YouTube (2009-07-04).
 ↑ Foreign nationalists speak, national media silent (2009-10-25).
 ↑ Radical Change: for national self-determination and social justice (PDF). Retrieved on 2010-06-18.
 ↑ Heltai-Hopp, András (2009-06-05), Big players fight domestic battle in EP election, The Budapest Times, http://www.budapesttimes.hu/content/view/12139/219/ 
 ↑  EP elections - Hungary elections committee finds radical Jobbik's slogan unconstitutional, The Budapest Times, 2009-06-04, http://www.budapesttimes.hu/content/view/12155/159/ 
 ↑ Morvai, Krisztina (2009-06-09), Quotation taken from Budapest Sun article, via blog (the Budapest Sun subsequently closed down), Maria Golubeva`s blog, http://www.politika.lv/blogi/index.php?id=61430, "The political and economic elite have placed the whole country in foreign hands. We have, therefore, a particular justification for emphasising that “Hungary belongs to the Hungarians”. We want to get back our national assets, which have been sold abroad and privatised, and want to prevent further national assets from getting into private and/or foreign hands." 
 ↑ The Movement for a Better Hungary - VONA: What do we mean by radicalism?. Jobbik (2009-11-28). Retrieved on 2010-06-18.
 ↑ Leigh Phillips (2010-04-19). EUobserver / A far-right for the Facebook generation: The rise and rise of Jobbik. Euobserver.com. Retrieved on 2010-06-18.
 ↑ Betz, Hans-Georg (1994). Radical Right-Wing Populism in Western Europe (The New Politics of Resentment). Palgrave MacMillan, 4. ISBN 0312083904. “the majority of radical right-wing populist parties are radical in their rejection of the established socio-cultural and socio-political system” 
 ↑ Albertazzi, Daniele (2007). Radical Twenty-First Century Populism: The Spectre of Western European Democracy. Palgrave MacMillan. ISBN 023001349X. 
 ↑ Vona, Gábor (2008-07-21), Vona Gábor: Hazánk két valósága (Gábor Vona: The two differing realities of our homeland), barikad.hu (Hungarian), http://barikad.hu/node/15809 
 ↑ Horvath, John, "Far Right Rising in Hungary", Telepolis 
 ↑ McConkey, Jamie (2009-04-09), Jobbik: Hungary’s far right on the EU, Slovakia and elections, cafebabel.com, http://www.cafebabel.com/eng/article/29499/hungary-slovakia-jobbik-right-european-elections.html 
 ↑ 29.0 29.1 Vona, Gábor (2008-09-02), JobbikTV Különkiadás - Morvai Krisztina az EP listavezető (Jobbik TV Special – Krisztina Morvai will head the EP list), youtube, http://www.youtube.com/watch?v=uUfDA1rcjp4 
 ↑  For example: Jobbik says top priority is Hungarian ownership of farmland, politics.hu (MTI), 2008-05-25, http://www.politics.hu/20090525/jobbik-says-top-priority-is-hungarian-ownership-of-farmland, "Hungarian farmers should immediately receive the same EU subsidies as their foreign competitors, Morvai said." 
 ↑ Morvai, Krisztina (2009-05-01), Speech in Óbuda during 2009 European election campaign, youtube.com (subtitled @ 7:55), http://www.youtube.com/watch?v=4CwilgZBi7Y, "We have no use in Hungary, for those laws, which in our own land actively disadvantage Hungarians and prioritise the interests of foreigners. We want nothing less than the suspension of such legislation at a single stroke." 
 ↑ Vona, Gábor (2008-09-02), JobbikTV Különkiadás - Morvai Krisztina az EP listavezető (Jobbik TV Special – Krisztina Morvai will head the EP list), youtube (@ 1:46), http://www.youtube.com/watch?v=uUfDA1rcjp4, "Hungarian: Itt az ideje végre annak, hogy mindenki kijöjjön a napra, hogy megnézhessük mindannyian azt hogy ki hová tartozik: nem a hamis törésvonalak mentén hanem a valódi törésvonalak mentén;... meg kell mutatnia mindenkinek magát, hogy a valódi barikádnak melyik oldalán ál. / English: The time has at last arrived, for everyone to emerge into the sunlight, so we may all see who belongs where: not with respect to the illusory dividing line but with respect to the real dividing line;... everyone will have to reveal themselves, on which side of the real barricade they stand." 
 ↑ barikad.hu. barikad.hu. Retrieved on 2010-06-18.
 ↑ Vona, Gábor: Turanism Instead of Euro-Atlantic Alliance!, Jobbik.com. Retrieved on 25 Aug 2011.
 ↑ Jobbik MEPs to fight for pre-Trianon borders. Politics.hu (MTI) (2009-06-15). “Jobbik will demand territorial autonomy for Szekler land in Romania and will also press for Transcarpathia in Ukraine to become an independent Hungarian district, Szegedi said.”
 ↑ Varkonyi, Zsolt (2008-11-12). Statement Made by the Foreign Affairs Committee of Jobbik. Jobbik.com. “Since the time the regime changed, the Socialist-Liberal governments in Hungary and the forces in the background that support them have had a share in diminishing the Hungarians’ economic and political strength, their ability to reproduce themselves and their culture, in destroying their image of the future and their faith in themselves, in ridiculing their traditions and their national identity. It is only natural that this policy should in itself have encouraged all the internal and external forces that are up against the historical presence of Hungarians in the Carpathian basin and their growth in the future.”
 ↑  Jobbik stages demonstration against banks, "foreign speculative capital", politics.hu, 2009-08-04, http://www.politics.hu/20090804/jobbik-stages-demonstration-against-banks-foreign-speculative-capital- 
 ↑ Teich, Mikuláš; Dušan Kováč, Martin D. Brown (2011). Slovakia in History. Cambridge University Press. Retrieved on 7 November 2011. 
 ↑ http://books.google.com/books?id=IMYrryZL9K0C&lpg=PA20&dq=census%201910%20hungary%20manipulated&hl=sk&pg=PA20#v=onepage&q=Hungarian%20government%20manipulated&f=false
 ↑ Map is based on the 1910 manipulated census.[38] The Hungarian government manipulated the stats in order to increase the percentage of Magyar population, so these numbers are unreliable.[39]
 ↑ 41.0 41.1 Inder Singh, Anita (2000). Democracy, ethnic diversity, and security in post-communist Europe. Central European University Press, 97. ISBN 0275972585. “[including the nations of the former Soviet Union] Magyar and Russian minorities are the largest minority groups in Europe, about one-tenth of all Russians and a quarter of Magyars live outside Russia and Hungary respectively.” 
 ↑ Molnar, A Concise History of Hungary, p. 262 online; Richard C. Frucht, Eastern Europe: An Introduction to the People, Lands, and Culture p. 359-360 online)
 ↑ Foreign Policy of Jobbik. [1] (English) (2009-05-19). “"...support and advocate the efforts of Hungarian minorities outside the country, historically deprived of their rights of self-determination and governance."”
 ↑ Constitution of the Republic of Hungary (Chapter I, Article 6, Subsection 3). The 'Lectric Law Library (1989-10-23).
 ↑ police-investigate-new-magyar-garda New Magyar Gárda
 ↑ Erősíteni kell a nemzettudatot. Naplo-online.hu. Retrieved on 2010-06-18.
 ↑ Maroz, Sharon (2007-10-22). פרס עורר האנטישמים בהונגריה (Hebrew). Maariv. Retrieved on 2009-07-09.
 ↑ EP results in; Fidesz dominates as Jobbik nears 15% of popular vote. Politics.hu (2009-06-07). Retrieved on 2009-06-08. “The ruling Hungarian Socialist Party (MSZP) won four seats, the radical nationalist Jobbik, a non-parliamentary force, three seats, and the conservative opposition Democratic Forum one seat, OVB head Emilia Rytko said.”
 ↑ 49.0 49.1 49.2 http://www.dailystormer.com/7-great-quotes-from-jobbik/
 Official website (Hungarian) Official website (English) Electoral Manifesto (English) Deed of Foundation (Hungarian) "Third way" platform: The nationalist right gets together (HVG) Hungarian Socialist Party (48) Jobbik – Movement for a Better Hungary (46) Christian Democratic People's Party (36) LMP – Politics Can Be Different (15) Independent (14) Civil Movement (0.89%) Hungarian Communist Workers' Party (0.11%) Hungarian Social Democratic Party (0.08%) Hungarian Socialist Party (4) Jobbik – Movement for a Better Hungary (3) Hungarian Democratic Forum (1) List of political parties Politics of Hungary Articles containing Hebrew language text Articles containing Hungarian language text NPOV disputes Articles to be expanded from August 2009 All articles to be expanded Political parties in Hungary Nationalist parties Political parties established in 2003 2003 establishments in Hungary Eurosceptic parties Content from Wikipedia Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information Dansk Deutsch Español Magyar Português Svenska  This page was last edited on 8 December 2015, at 16:42. Privacy policy About Metapedia Disclaimers 
  Hungarians are victims of a Jewish conspiracy to colonise our land and rob our resources.
—Sandor Porzse, a prominent member of Jobbik, told a French website in a 2012 interview[49] The Land Law has to be amended so that the Solomons cannot buy land in Hungary.
—Gabor Vona, the party leader, 2010[49] So-called proud Hungarian Jews should go back to playing with their tiny little circumcised tails.
—Krisztina Morvai, an MEP for the party and Jobbik’s presidential candidate, [49] Jobbik, The Movement for a Better Hungary Jobbik Jobbik The neutrality of this article is disputed. Style guide For the Hungarian Parliament: For the European Parliament: 2009 Seat winners: Municipal: Mayoral: Hungarian: English: Hungarian: English: Main article: Magyar Gárda † 2009 Seat winners: 
Krisztina Morvai
Zoltán Balczó - His seat EP was taken over by Béla Kovács, when he became a member of the Hungarian Parliament in May 2010.
Csanád Szegedi Krisztina Morvai
Zoltán Balczó - His seat EP was taken over by Béla Kovács, when he became a member of the Hungarian Parliament in May 2010.
Csanád Szegedi Main article: Magyar Gárda † 2009 Seat winners: 
Krisztina Morvai
Zoltán Balczó - His seat EP was taken over by Béla Kovács, when he became a member of the Hungarian Parliament in May 2010.
Csanád Szegedi Krisztina Morvai
Zoltán Balczó - His seat EP was taken over by Béla Kovács, when he became a member of the Hungarian Parliament in May 2010.
Csanád Szegedi  Gábor Vona 24 October 2003 1113 Budapest, Villányi út 20/A Patriotism[1]Pro-Christian civilisation None Alliance of European National Movements Non-Inscrits Red and Silver 46 / 386 3 / 22 www.jobbik.hu (Hungarian)www.jobbik.com (English) 2006*
 119,007
 2.20%
 231
 0.007%
 0
 0%
 extra-parliamentary
 2010
 855,436
 16.67%
 141,323
 12.26%
 47
 12.18%
 opposition
 2009
 427,773
 14.77%
 3rd[48]
 3
 Non-Attached Members
 Non-Attached Members
 
  This section requires expansion. v • d • e
 Political parties in HungaryNational Assembly (386)* Fidesz – Hungarian Civic Union (227)
Hungarian Socialist Party (48)
Jobbik – Movement for a Better Hungary (46)
Christian Democratic People's Party (36)
LMP – Politics Can Be Different (15)
Independent (14)Not represented** Hungarian Democratic Forum (2.67%)
Civil Movement (0.89%)
Hungarian Communist Workers' Party (0.11%)
Hungarian Social Democratic Party (0.08%)
* Limit for parties to join the National Assembly in Hungary is 5 % of popular votesEuropean Parliament (22)* Fidesz – Hungarian Civic Union and Christian Democratic People's Party (14)
Hungarian Socialist Party (4)
Jobbik – Movement for a Better Hungary (3)
Hungarian Democratic Forum (1)* Portal:Politics
List of political parties
Politics of Hungary  National Assembly (386) * Fidesz – Hungarian Civic Union (227)
Hungarian Socialist Party (48)
Jobbik – Movement for a Better Hungary (46)
Christian Democratic People's Party (36)
LMP – Politics Can Be Different (15)
Independent (14)  Not represented* * Hungarian Democratic Forum (2.67%)
Civil Movement (0.89%)
Hungarian Communist Workers' Party (0.11%)
Hungarian Social Democratic Party (0.08%)
* Limit for parties to join the National Assembly in Hungary is 5 % of popular votes  European Parliament (22) * Fidesz – Hungarian Civic Union and Christian Democratic People's Party (14)
Hungarian Socialist Party (4)
Jobbik – Movement for a Better Hungary (3)
Hungarian Democratic Forum (1)  * Portal:Politics
List of political parties
Politics of Hungary Jobbik Contents History and development Manifesto and political programme Issues and ideology Policy position Hungarophobic campaign The Hungarian Guard Election results Quotes References External links Navigation menu Foundation Alliances Growth and electoral success International relations Linguistic clarifications Patriotism Cultural conservatism The economy Magyar irredentism Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools In other languages 