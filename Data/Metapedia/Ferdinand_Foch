Marshal Ferdinand Foch (French pronunciation: [fɔʃ]), GCB, OM, DSO (2 October 1851 – 20 March 1929), was a French soldier, military theorist, and First World War hero credited with possessing "the most original and subtle mind in the French army" in the early 20th century.[1]
 Foch's prestige soared as a result of the 1914 victory at the Marne for which he was widely credited as the chief actor while commanding the French Ninth Army. This led to his promotion to Marshal of France and ultimately "Supreme Commander of the Allied Armies" in 1918, at which time he played a decisive role in halting a renewed German advance on Paris in the Second Battle of the Marne.
 Postwar historians took a less sanguine view of Foch's talents as commander, particularly as that idea took root that his military doctrines had set the stage for the futile and costly offensives of 1914 in which French armies suffered devastating losses. The efficacy of Foch's tactical ideas are debated—the Ninth Army's counterattacks at the Marne generally failed, but his sector resisted determined German attacks while holding the pivot on which the neighbouring French and British forces depended in rolling back the German line. One of his battlefield reports from the Marne—"Hard pressed on my right; center is yielding; impossible to maneuver. Situation excellent, I shall attack!"—won fame as a symbol both of Foch's battlefield leadership and of French determination to resist the invader at any cost.
 On 11 November 1918, Foch accepted the German request for an armistice. Foch advocated peace terms that would make Germany unable to pose a threat to France ever again. His words after the Treaty of Versailles, "This is not a peace. It is an armistice for twenty years" would prove prophetic; the Second World War started twenty years and sixty five days later. In 1919 he was made a Field Marshal in the British Empire, and in 1923 a Marshal of Poland, adding to a long list of military decorations.
 Foch was born in Tarbes, Hautes-Pyrénées as the son of a civil servant from Comminges. He attended school in Tarbes, Rodez, and the Jesuit College in St. Etienne. His brother was later a Jesuit and this may initially have hindered Foch's rise through the ranks of the French Army (since the Republican government of France was anti-clerical).
 Foch enlisted in the French 4th Marine Infantry Regiment, in 1870, during the Franco-Prussian War, and decided to stay in the army after the war. In 1871, Foch entered the École Polytechnique and received his commission as a Lieutenant in the 24th Artillery Regiment, in 1873, despite not having the time to complete his course due to the shortage of junior officers. He rose through the ranks, eventually reaching the rank of Captain before entering the Staff College in 1885. In 1895, he was to return to the College as an instructor and it is for his work here that he was later acclaimed as "the most original military thinker of his generation".[2] Turning to history for inspiration, Foch became known for his critical analyses of the Franco-Prussian and Napoleonic campaigns and of their relevance to the pursuit of military operations in the new century. His re-examination of France's painful defeat in 1870 was among the first of its kind.
 In his career as instructor Foch created renewed interest in French military history, inspired confidence in a new class of French officers, and brought about "the intellectual and moral regeneration of the French Army".[1] His thinking on military doctrine was shaped by the Clausewitzian philosophy, then uncommon in France, that "the will to conquer is the first condition of victory." Collections of his lectures, which reintroduced the concept of the offensive to French military theory, were published in the volumes "Des Principes de la Guerre" ("On the Principles of War") in 1903, and "De la Conduite de la Guerre" ("On the Conduct of War") in 1904. Sadly, while Foch advised "qualification and discernment" in military strategy and cautioned that "recklessness in attack could lead to prohibitive losses and ultimate failure,"[3] his concepts, distorted and misunderstood by contemporaries, became associated with the extreme offensive doctrines (l'offensive à outrance) of his successors. The cult of the offensive came to dominate military circles; that Foch's books were cited in the development of Plan XVII, the disastrous offensive that brought France close to ruin in 1914, proved particularly damaging to his reputation.
 Foch continued his initially slow rise through the ranks, being promoted to Lieutenant-Colonel in 1898. Thereafter, his career accelerated and he returned to command in 1901, when he was posted to a regiment. He was promoted to become a Colonel in 1903. In 1905 Georges Clemenceau, then Prime Minister, determined to make use of his military ability to the full, irrespective of political considerations, and, after a short time spent as deputy chief of the general staff, he was appointed commandant of the École Militaire. Then Brigadier General (Général de Brigade) in 1907, returning to the Staff College as Commandant from 1907–1911. In 1911 he was promoted Major General (Général de Division) and then Lieutenant General (Général de corps d’Armée) in 1913, taking command of XXe Corps at Nancy. He had held this appointment exactly a year when he led the XX Corps into battle. Foch was then the only intellectual master of the Napoleonic school still serving. And the doctrines of the brilliant series of war school commandants, Maillard, Langlois, Bonnal, Foch, had been challenged, not only by the German school, but also since about 1911 by a new school of thought within the French army itself, which, under the inspiration of General Loiseau de Grandmaison, criticized them as lacking in vigour and offensive spirit, and conducing to needless dispersion of force. The younger men carried the day, and the French army took the field in 1914 governed by a new code of practice. But history decided at once and emphatically against the new idea in the first battles of August, and it remained to be seen whether the Napoleonic doctrine would hold its own, give way to doctrines evolved in the war itself, or, incorporating the new moral and technical elements and adapting itself to the war of national masses, reappear in a new outward form within which the spirit of Napoleon remained unaltered. To these questions the war had given an ambiguous answer which provided material for expert controversy.[4]
 On the outbreak of the war, Foch was in command of XX Corps, part of the Second Army of General de Castelnau. On 14 August the corps advanced towards the Sarrebourg-Morhange line, taking heavy casualties in the Battle of the Frontiers. The defeat of XV Corps to its right forced Foch into retreat. Foch acquitted himself well, covering the withdrawal to Nancy and the Charmes Gap, before launching a counter-attack that prevented the Germans from crossing the Meurthe.
 He was then selected to command the newly formed Ninth Army, which he was to command during the First Battle of the Marne and the Race to the Sea. With his Chief of Staff Maxime Weygand, Foch managed to do this while the whole French Army was in full retreat. Only a week after taking command of 9th Army, he was forced to fight a series of defensive actions to prevent a German breakthrough. It was then that he spoke the famous words: "Hard pressed on my right. My center is yielding. Impossible to maneuver. Situation excellent. I attack." His counter-attack was an implementation of the theories he had developed during his staff college days, and succeeded in stopping the German advance. Foch received further reinforcements from the Fifth Army and, following another attack on his forces, counter-attacked again on the Marne. The Germans dug in before eventually retreating. On 12 September Foch regained the Marne at Châlons and liberated the city. The people of Châlons greeted as a hero the man widely believed to have been instrumental in stopping the great retreat and stabilising the Allied position. Receiving thanks from the Bishop of Châlons, Foch piously replied, "non nobis, Domine, non nobis, sed nomini tuo da gloriam." (Not unto us, o Lord, not unto us, but to Your name give glory, Psalm 115:1) [citation needed]
 Foch's successes gained him a further promotion, on 4 October, when he was appointed assistant Commander-in-Chief with responsibility for co-ordinating the activities of the northern French armies, and liaising with the British forces. This was a key appointment as the so-called "Race to the Sea" was then in progress. Joffre had also wanted to nominate Foch as his successor "in case of accident", to make sure the job would not be given to Galliéni, but the French government would not agree to this. When the Germans attacked on 13 October, they narrowly failed to break through the British and French lines. They tried again at the end of the month during the First Battle of Ypres; this time suffering terrible casualties. Foch had again succeeded in co-ordinating a defence and winning against the odds. On 2 December 1914, King George V of the United Kingdom appointed him an honorary Knight Grand Cross of the Order of the Bath.[5] In 1915, his responsibilities by now crystallised into command of the Northern Army Group, he conducted the Artois Offensive, and, in 1916, the French part of the Battle of the Somme. He was strongly criticised for his tactics and the heavy casualties that were suffered by the Allied armies during these battles, and in December 1916 was removed from command, by General Joffre, and sent to command in Italy; Joffre was himself sacked days later.
 Just a few months later, after the failure of General Nivelle, General Pétain was appointed Chief of the General Staff; Foch hoped to succeed Pétain in command of Army Group Centre, but this job was instead given to General Fayolle. The following month General Pétain was appointed Commander-in-Chief in place of Nivelle, and Foch was recalled and promoted to Chief of the General Staff.
 On 26 March 1918, at the Doullens Conference, Foch was given the job of co-ordinating the activities of the Allied armies,[6][7] forming a common reserve and using these divisions to guard the junction of the French and British armies and to plug the potentially fatal gap that would have followed a German breakthrough in the British Fifth Army sector. At a later conference he was give the title Supreme Commander of the Allied Armies with the title of Généralissime ("supreme General") . Despite being surprised by the German offensive on the Chemin des Dames, the Allied armies under Foch's command ultimately held the advance of the German forces during the great Spring Offensive of 1918 and at the Second Battle of Marne in July 1918. The celebrated phrase, "I will fight in front of Paris, I will fight in Paris, I will fight behind Paris," attributed both to Foch and Clemenceau, illustrated the Généralissime's resolve to keep the Allied armies intact, even at the risk of losing the capital. On 6 August 1918, Foch was made Marshal of France.
 Along with the British commander Field Marshal Haig, Foch planned the Grand Offensive, opening on 26 September 1918, which led to the defeat of Germany. After the war, he claimed to have defeated Germany by smoking his pipe.[8] Foch accepted the German cessation of hostilities in November, after which he refused to shake the hand of the German signatory[citation needed]. On the day of the armistice, he was elected to the Académie des Sciences. Ten days later, he was unanimously elected to the Académie française. On 30 November 1918, he was awarded the highest Portuguese decoration the Order of the Tower and Sword, 1st class (Grand Cross).
 In January 1919, at the Paris Peace Conference Foch presented a memorandum to the Allied plenipotentiaries in which he stated:
 In a subsequent memorandum, Foch argued that the Allies should take full advantage of their victory by permanently weakening German power in order to prevent her from threatening France again:
 However the British Prime Minister David Lloyd George and the American President Wilson objected to the detachment of the Rhineland from Germany, but agreed to Allied military occupation for fifteen years, which Foch thought insufficient to protect France.
 Foch considered the Treaty of Versailles to be "a capitulation, a treason" because he believed that only permanent occupation of the Rhineland would grant France sufficient security against a revival of German aggression.[10] As the treaty was being signed Foch said: "This is not peace. It is an armistice for 20 years".[11]
 Foch was made a British Field Marshal in 1919,[12] and, for his advice during the Polish-Bolshevik War of 1920, as well as his pressure on Germany during the Great Poland Uprising, he was awarded with the title of Marshal of Poland in 1923.
 On 1 November 1921 Foch was in Kansas City to take part in the groundbreaking ceremony for the Liberty Memorial that was being constructed there. Also present that day were Lieutenant General Baron Jacques of Belgium, Admiral David Beatty of Great Britain, General Armando Diaz of Italy and General John J. Pershing of the United States. One of the main speakers was Vice President Calvin Coolidge of the United States. In 1935 bas-reliefs of Foch, Jacques, Diaz and Pershing by sculptor Walker Hancock were added to the memorial.
 Foch died on 20 March 1929, and was interred in Les Invalides, next to Napoleon and many other famous French soldiers and officers.
 A statue of Foch was set up at the Compiègne Armistice site when the area was converted into a national memorial. This statue was the one item left undisturbed by the Germans following their defeat of France in June, 1940. Following the signing of France's surrender on 21 June, the Germans ravaged the area surrounding the railway car in which both the 1918 and 1940 surrenders had taken place. The statue was left standing, to view nothing but a wasteland. The Armistice site was restored by German POW labour following the Second World War, with its memorials and monuments either restored or reassembled.
 A heavy cruiser and an aircraft carrier were named in his honour, as well as an early district of Gdynia, Poland. The latter was, however, renamed by the communist government after the Second World War. Nevertheless, one of the major avenues of the town of Bydgoszcz, located then in the Polish corridor, holds his name as sign of gratitude for campaigning for an independent Poland. Avenue Foch, a street in Paris, was named after him. Several other streets have been named in his honour in Lyon, Kraków, Chrzanów,[13] Grenoble, Quito, Beirut, New Orleans, Leuven, Wynnum, Cambridge, Williston Park, Milltown and Foch Road in Singapore. Fochville in South Africa was also named in his honour. A statue of Foch stands near Victoria station in London. Foch also has a grape cultivar named after him.
 Foch received the title of Doctor honoris causa of the Jagiellonian University of Kraków in 1918.
 Jean Baudoin (before 1634) · 
François Charpentier (1650) · 
Jean-François de Chamillart (1702) · 
Claude Louis Hector de Villars (1714) · 
Honoré Armand de Villars (1734) · 
Étienne Charles de Loménie de Brienne (1770) · 
Jean-Gérard Lacuée, count of Cessac (1803) · 
Alexis de Tocqueville (1841) · 
Jean-Baptiste Henri Lacordaire (1860) · 
Albert, 4th duc de Broglie (1862) · 
Charles-Jean-Melchior de Vogüé (1901) · 
Ferdinand Foch (1918) · 
Philippe Pétain (1929) · 
André François-Poncet (1952) · 
Edgar Faure (1978) · 
Michel Serres (1990)
  
 1 Early life 2 Foch and the First World War 3 Paris Peace Conference 4 Post-war career 5 Honours and awards

5.1 France
5.2 Foreign decorations
5.3 Bibliography

 5.1 France 5.2 Foreign decorations 5.3 Bibliography 6 Further reading 7 See also 8 Notes 9 External links Legion of Honour: Medaille Militaire - 21 December 1916. Croix de Guerre 1914-1918 Commemorative Medal of War 1870-1871 Officer of Public Instruction. Order of Merit (United Kingdom) Knight Grand Cross of the Order of the Bath (United Kingdom) Distinguished Service Order (United Kingdom) Order of the White Eagle (Poland) (15 April 1923) Grand Cross of the Order of Virtuti Militari (15 April 1923, Poland) Grand Cross of the Order of Polonia Restituta (Poland) Grand Cross of the Order of Leopold (Belgium) Grand Cross of the Order of Ouissam Alaouite (Morocco) Distinguished Service Medal (United States) Order of Lāčplēsis 3rd Class (Latvia) Order of Saint George Second Class (Орден Святого Георгия, 1916, Russian Empire) Les Principes de la guerre. Conférences faites à l'Ecole supérieure de guerre (On the Principles of War), Berger-Levrault, (1903) La Conduite de la guerre (On the Conduct of War), Berger-Levrault, 1905 Mémoire pour servir à l'histoire de la guerre 1914-1918 (The Memoirs of Marshal Foch,Posthumous), Plon, 1931. Porte, Rémy, and F Cochet. Ferdinand Foch, 1851-1929: Apprenez À Penser : Actes Du Colloque International, École Militaire, Paris, 6-7 Novembre 2008. Paris: Soteca, 2010. ISBN 9782916385433 Doughty, Robert A. Pyrrhic Victory: French Strategy and Operations in the Great War (Harvard U.P. 2005) Greenhalgh, Elizabeth. "Command in a Coalition War: Reassessing Marshal Ferdinand Foch" French History and Civilization. Papers from the George Rudé Seminar. Volume 2 (2009) pp 91–100 online Neiberg, Michael S. Foch: Supreme Allied Commander in the Great War (Brassey’s Inc., 2003), short popular biography Army Manoeuvres of 1912 Foch Line Marshal Foch Professor of French Literature, a chair at the University of Oxford established in Foch's honour in 1918 ↑ Jump up to: 1.0 1.1 Shirer, p. 81
 Jump up ↑ Michael Carver (editor), The War Lords: Military Commanders of the Twentieth Century, (Weidenfeld & Nicolson, 1976), p. 123. ISBN 0-297-77084-5
 Jump up ↑ Shirer, p. 80
 Jump up ↑  Charles Francis Atkinson (1922). "Foch, Ferdinand". Encyclopædia Britannica (12th ed.). 
 Jump up ↑ London Gazette: no. 29044, p. 601, 19 January 1915. Retrieved 30 May 2008.
 Jump up ↑ Keegan, John, "The First World War" (Vintage Books, 1998), p. 403.
 Jump up ↑  Ferdinand Foch at Project Gutenberg
 Jump up ↑ " 'How did I win the war?' Foch will say chaffingly to André de Marincourt, many months later. 'By smoking my pipe. That is to say, by not getting excited, by reducing everything to simple terms, by avoiding useless emotions, and keeping all my strength for the job.' " Frank H. Simonds, History of the World War, Vol. 5, Ch. 3, III. Doubleday, Page & Co., 1920.
 ↑ Jump up to: 9.0 9.1 Ernest R. Troughton, It's Happening Again (John Gifford, 1944), p. 17.
 Jump up ↑ Anthony Adamthwaite, Grandeur and Misery: France's Bid for Power in Europe, 1914-40 (Hodder Arnold, 1995), p. 57.
 Jump up ↑ Ruth Henig, Versailles and After, 1919-33 (Routledge, 1995), p. 52.
 Jump up ↑ London Gazette: (Supplement) no. 31481, p. 9809, 29 July 1919. Retrieved 27 May 2008.
 Jump up ↑ Chrzanovia Patria Parva Street chart of Chrzanów
 Unjustly Accused: Marshal Ferdinand Foch and the French 'Cult of the Offensive' Biography on FirstWorldWar.com Foch's Biography in French on the Immortals page of the Académie française  Foch the Man, by Clara E. Laughlin at Project Gutenberg Ferdinand Foch at Find a Grave Use dmy dates from April 2012 1851 births 1929 deaths Alumni of the École Polytechnique British Field Marshals French military personnel of the Franco-Prussian War French military personnel of World War I French military writers Generalissimos People from Tarbes Grand Croix of the Légion d'honneur Recipients of the Croix de Guerre (France) Honorary Knights Grand Cross of the Order of the Bath Honorary Members of the Order of Merit Marshals of France Marshals of Poland Members of the Académie française Grand Crosses of the Order of Leopold (Belgium) Recipients of the Médaille Militaire Recipients of the Order of Lāčplēsis, 3rd class Recipients of the Order of St. George of the Second Degree Recipients of the Order of the White Eagle (Poland) Grand Crosses of the Virtuti Militari Grand Crosses of the Order of Polonia Restituta Recipients of the Order of Ouissam Alaouite Companions of the Distinguished Service Order Recipients of the Distinguished Service Medal (United States) Content from Wikipedia Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 11 December 2014, at 15:34. Privacy policy About Metapedia Disclaimers 
  Henceforward the Rhine ought to be the Western military frontier of the German countries. Henceforward Germany ought to be deprived of all entrance and assembling ground, that is, of all territorial sovereignty on the left bank of the river, that is, of all facilities for invading quickly, as in 1914, Belgium, Luxembourg, for reaching the coast of the North Sea and threatening the United Kingdom, for outflanking the natural defences of France, the Rhine, Meuse, conquering the Northern Provinces and entering the Parisian area.[9]
 What the people of Germany fear the most is a renewal of hostilities since, this time, Germany would be the field of battle and the scene of the consequent devastation. This makes it impossible for the yet unstable German Government to reject any demand on our part if it is clearly formulated. The Entente, in its present favourable military situation, can obtain acceptance of any peace conditions it may put forward provided that they are presented without much delay. All it is has to do is to decide what they shall be.[9]
 Ferdinand Foch Ferdinand Foch Ferdinand Foch Ferdinand Foch Nicholas Longworth Cover of Time Magazine Eduard Benes · · · · · · · · · · · · · · · Knight - 9 July 1892;
Officer - 11 July 1908;
Commander - 31 December 1913;
Grand Officer - 18 September 1914;
Grand Cross - 8 October 1915. Knight - 9 July 1892; Officer - 11 July 1908; Commander - 31 December 1913; Grand Officer - 18 September 1914; Grand Cross - 8 October 1915. Knight - 9 July 1892;
Officer - 11 July 1908;
Commander - 31 December 1913;
Grand Officer - 18 September 1914;
Grand Cross - 8 October 1915. Knight - 9 July 1892;
Officer - 11 July 1908;
Commander - 31 December 1913;
Grand Officer - 18 September 1914;
Grand Cross - 8 October 1915. 
 General Foch in 1913
  France
 French Army
 1871–1923
 Maréchal de France
 Franco-Prussian WarFirst World War* Battle of the Frontiers* Spring Offensive* Meuse-Argonne Offensive
 Marshal of France (1918) British Field Marshal (1919) Marshal of Poland (1920) Grand Cross of the Légion d'honneurMédaille militaireCroix de guerre 1914-1918Order of Merit (UK) Virtuti Militari (1st Class)Distinguished Service Medal (US)
   Wikisource has original text related to this article:
Ferdinand Foch    Wikimedia Commons has media related to: Ferdinand Foch    Wikiquote has a collection of quotations related to: Ferdinand Foch  Preceded byNicholas Longworth
 Cover of Time Magazine16 March 1925
 Succeeded byEduard Benes
 v • d • e
Marshals of PolandJózef Piłsudski • Ferdinand Foch • Edward Rydz-Śmigły • Michał Rola-Żymierski • Konstanty Rokossowski • Marian Spychalski  Józef Piłsudski • Ferdinand Foch • Edward Rydz-Śmigły • Michał Rola-Żymierski • Konstanty Rokossowski • Marian Spychalski v • d • e
Académie Française Seat 18
Jean Baudoin (before 1634) · 
François Charpentier (1650) · 
Jean-François de Chamillart (1702) · 
Claude Louis Hector de Villars (1714) · 
Honoré Armand de Villars (1734) · 
Étienne Charles de Loménie de Brienne (1770) · 
Jean-Gérard Lacuée, count of Cessac (1803) · 
Alexis de Tocqueville (1841) · 
Jean-Baptiste Henri Lacordaire (1860) · 
Albert, 4th duc de Broglie (1862) · 
Charles-Jean-Melchior de Vogüé (1901) · 
Ferdinand Foch (1918) · 
Philippe Pétain (1929) · 
André François-Poncet (1952) · 
Edgar Faure (1978) · 
Michel Serres (1990)

  
Jean Baudoin (before 1634) · 
François Charpentier (1650) · 
Jean-François de Chamillart (1702) · 
Claude Louis Hector de Villars (1714) · 
Honoré Armand de Villars (1734) · 
Étienne Charles de Loménie de Brienne (1770) · 
Jean-Gérard Lacuée, count of Cessac (1803) · 
Alexis de Tocqueville (1841) · 
Jean-Baptiste Henri Lacordaire (1860) · 
Albert, 4th duc de Broglie (1862) · 
Charles-Jean-Melchior de Vogüé (1901) · 
Ferdinand Foch (1918) · 
Philippe Pétain (1929) · 
André François-Poncet (1952) · 
Edgar Faure (1978) · 
Michel Serres (1990)

 NAME
 Foch, Ferdinand
 ALTERNATIVE NAMES
 
 SHORT DESCRIPTION
 
 DATE OF BIRTH
 2 October 1851
 PLACE OF BIRTH
 Tarbes, France
 DATE OF DEATH
 20 March 1929
 PLACE OF DEATH
 Paris, France
 Ferdinand Foch Contents Early life Foch and the First World War Paris Peace Conference Post-war career Honours and awards Further reading See also Notes External links Navigation menu France Foreign decorations Bibliography Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 