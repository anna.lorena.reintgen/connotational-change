French Polynesia ( /ˈfrɛntʃ pɒlɪˈniːʒə/; French: Polynésie française, pronounced: [pɔlinezi fʁɑ̃sɛz]; Tahitian: Pōrīnetia Farāni) is an overseas country of the French Republic (pays d'outre-mer). It is made up of several groups of Polynesian islands, the most famous island being Tahiti in the Society Islands group, which is also the most populous island and the seat of the capital of the territory (Papeetē). Although not an integral part of its territory, Clipperton Island was administered from French Polynesia until 2007.
 The island groups that make up French Polynesia were not officially united until the establishment of the French protectorate in 1889. The first of these islands to be settled by indigenous Polynesians were the Marquesas Islands in AD 300 and the Society Islands in AD 800. The Polynesians were organized in loose chieftainships.[5]
 European communication began in 1521 when the Portuguese explorer Ferdinand Magellan sighted Pukapuka in the Tuāmotu-Gambier Archipelago. Dutchman Jakob Roggeveen came across Bora Bora in the Society Islands in 1722, and the British explorer Samuel Wallis visited Tahiti in 1767. The French explorer Louis Antoine de Bougainville visited Tahiti in 1768, while the British explorer James Cook visited in 1769. Christian missions began with Spanish priests who stayed in Tahiti for a year from 1774; Protestants from the London Missionary Society settled permanently in Polynesia in 1797.[5][6]
 King Pōmare II of Tahiti was forced to flee to Mo'orea in 1803; he and his subjects were converted to Protestantism in 1812. French Catholic missionaries arrived on Tahiti in 1834; their expulsion in 1836 caused France to send a gunboat in 1838. In 1842, Tahiti and Tahuata were declared a French protectorate, to allow Catholic missionaries to work undisturbed. The capital of Papeetē was founded in 1843. In 1880, France annexed Tahiti, changing the status from that of a protectorate to that of a colony.[7]
 In the 1880s, France claimed the Tuamotu Archipelago, which formerly belonged to the Pōmare Dynasty, without formally annexing it. Having declared a protectorate over Tahuata in 1842, the French regarded the entire Marquesas Islands as French. In 1885, France appointed a governor and established a general council, thus giving it the proper administration for a colony. The islands of Rimatara and Rūrutu unsuccessfully lobbied for British protection in 1888, so in 1889 they were annexed by France. Postage stamps were first issued in the colony in 1892. The first official name for the colony was Établissements de l'Océanie (Settlements in Oceania); in 1903 the general council was changed to an advisory council and the colony's name was changed to Établissements Français de l'Océanie (French Settlements in Oceania).[8]
 In 1940 the administration of French Polynesia recognised the Free French Forces and many Polynesians served in World War II. Unknown at the time to French and Polynesians, the Konoe Cabinet in Imperial Japan on 16 September 1940 included French Polynesia among the many territories which were to become Japanese possessions in the post-war world[9] – though in the course of the war in the Pacific the Japanese were not able to launch an actual invasion of the French islands.
 In 1946, Polynesians were granted French citizenship and the islands' status was changed to an overseas territory; the islands' name was changed in 1957 to Polynésie Française (French Polynesia). In 1962, France's early nuclear testing ground of Algeria became independent and the Maruroa atoll in the Tuamotu Archipelago was selected as the new testing site; tests were conducted underground after 1974.[10] In 1977, French Polynesia was granted partial internal autonomy; in 1984, the autonomy was extended. French Polynesia became a full overseas collectivity of France in 2004.[6][11]
 In September 1995, France stirred up widespread protests by resuming nuclear testing at Fangataufa atoll after a three-year moratorium. The last test was on 27 January 1996. On 29 January 1996, France announced that it would accede to the Comprehensive Test Ban Treaty, and no longer test nuclear weapons.[12]
 Politics of French Polynesia takes place in a framework of a parliamentary representative democratic French overseas collectivity, whereby the President of French Polynesia is the head of government, and of a multi-party system. Executive power is exercised by the government. Legislative power is vested in both the government and the Assembly of French Polynesia (the territorial assembly).
 Political life in French Polynesia has been marked by great instability since the mid-2000s. On 14 September 2007, the pro-independence leader Oscar Temaru, 63, was elected president of French Polynesia for the 3rd time in 3 years (with 27 of 44 votes cast in the territorial assembly).[13] He replaced former President Gaston Tong Sang, opposed to independence, who lost a no-confidence vote in the Assembly of French Polynesia on 31 August after the longtime former president of French Polynesia, Gaston Flosse, hitherto opposed to independence, sided with his long enemy Oscar Temaru to topple the government of Gaston Tong Sang. Oscar Temaru, however, had no stable majority in the Assembly of French Polynesia, and new territorial elections were held in February 2008 to solve the political crisis.
 The party of Gaston Tong Sang won the territorial elections, but that did not solve the political crisis: the two minority parties of Oscar Temaru and Gaston Flosse, who together have one more member in the territorial assembly than the political party of Gaston Tong Sang, allied to prevent Gaston Tong Sang from becoming president of French Polynesia. Gaston Flosse was then elected president of French Polynesia by the territorial assembly on February 23, 2008 with the support of the pro-independence party led by Oscar Temaru, while Oscar Temaru was elected speaker of the territorial assembly with the support of the anti-independence party led by Gaston Flosse. Both formed a coalition cabinet. Many observers doubted that the alliance between the anti-independence Gaston Flosse and the pro-independence Oscar Temaru, designed to prevent Gaston Tong Sang from becoming president of French Polynesia, could last very long.[14]
 At the French municipal elections held in March 2008, several prominent mayors who are member of the Flosse-Temaru coalition lost their offices in key municipalities of French Polynesia, which was interpreted as a disapproval of the way Gaston Tong Sang, whose party French Polynesian voters had placed first in the territorial elections the month before, had been prevented from becoming president of French Polynesia by the last minute alliance between Flosse and Temaru's parties. Eventually, on 15 April 2008 the government of Gaston Flosse was toppled by a constructive vote of no confidence in the territorial assembly when two members of the Flosse-Temaru coalition left the coalition and sided with Tong Sang's party. Gaston Tong Sang was elected president of French Polynesia as a result of this constructive vote of no confidence, but his majority in the territorial assembly is very narrow. He offered posts in his cabinet to Flosse and Temaru's parties which they both refused. Gaston Tong Sang has called all parties to help end the instability in local politics, a prerequisite to attract foreign investors needed to develop the local economy.
 Despite a local assembly and government, French Polynesia is not in a free association with France, like the Cook Islands with New Zealand or the Federated States of Micronesia with the United States. As a French overseas collectivity, the local government has no competence in justice, education, security and defense, directly provided and administered by the French State, the Gendarmerie and the French Military. The highest representative of the State in the territory is the High Commissioner of the Republic in French Polynesia (French: Haut commissaire de la République).
 French Polynesia also sends two deputies to the French National Assembly, one representing the Leeward Islands administrative subdivision, the Austral Islands administrative subdivision, the commune (municipality) of Mo'orea-Mai'ao, and the westernmost part of Tahiti (including the capital Papeetē), and the other representing the central and eastern part of Tahiti, the Tuāmotu-Gambier administrative division, and the Marquesas Islands administrative division. French Polynesia also sends one senator to the French Senate.
 French Polynesians vote in the French presidential elections and at the 2007 French presidential election, in which the pro-independence leader Oscar Temaru openly called to vote for the Socialist candidate Ségolène Royal while the parties opposed to independence generally supported the center-right candidate Nicolas Sarkozy, the turnout in French Polynesia was 69.12% in the first round of the election and 74.67% in the second round. French Polynesians voters placed Nicolas Sarkozy ahead of Ségolène Royal in both rounds of the election (2nd round: Nicolas Sarkozy 51.9%; Ségolène Royal 48.1%).[15]
 Between 1946 and 2003, French Polynesia had the status of an overseas territory (French: territoire d'outre-mer, or TOM). In 2003 it became an overseas collectivity (French: collectivité d'outre-mer, or COM). Its statutory law of 27 February 2004 gives it the particular designation of overseas country inside the Republic (French: pays d'outre-mer au sein de la République, or POM), but without legal modification of its status.
 French Polynesia has five administrative subdivisions (French: subdivisions administratives):
 The islands of French Polynesia have a total land area of 4,167 square kilometres (1,622 sq. mi) scattered over 2,500,000 square kilometres (965,255 sq. mi) of ocean. There are around 130 islands in French Polynesia.[16] The highest point is Mount Orohena on Tahiti.
 It is made up of six groups of islands. The largest and most populated island is Tahiti, in the Society Islands.
 The island groups are:
 Aside from Tahiti, some other important atolls, islands, and island groups in French Polynesia are: Ahē, Pora Pora, Hiva 'Oa, Huahine, Mai'ao, Maupiti, Meheti'a, Mo'orea, Nuku Hiva, Ra'iātea, Taha'a, Te Ti'aroa, Tupua'i, and Tūpai.
 The GDP of French Polynesia in 2006 was 5.65 billion US dollars at market exchange rates, the fifth-largest economy in Oceania after Australia, New Zealand, Hawaii, and New Caledonia.[4] The GDP per capita was 21,999 US dollars in 2006 (at market exchange rates, not at PPP), lower than in Hawai'i, Australia, New Zealand, and New Caledonia, but higher than in all the independent insular states of Oceania.[4]
 French Polynesia has a moderately developed economy, which is dependent on imported goods, tourism, and the financial assistance of mainland France. Tourist facilities are well developed and are available on the major islands. Also, as the noni fruit from these islands is discovered for its medicinal uses, people have been able to find jobs related to this agricultural industry.
 The legal tender of French Polynesia is the CFP Franc.
 Agriculture: coconuts, vanilla, vegetables, fruits.
 Natural resources: timber, fish, cobalt.
 In 2008 French Polynesia's imports amounted to 2.2 billion US dollars and exports amounted to 0.2 billion US dollars.[17] The major export of French Polynesia is their famous black Tahitian pearls which accounted for 55% of exports (in value) in 2008.[17]
 Total population on 1 January 2010 was 267,000 inhabitants,[2] up from 259,596 at the August 2007 census.[3] At the 2007 census, 68.6% of the population of French Polynesia lived on the island of Tahiti alone.[3] The urban area of Papeetē, the capital city, has 131,695 inhabitants (2007 census).
 At the 2007 census, 87.3% of people living in French Polynesia were born in French Polynesia, 9.3% were born in metropolitan France, 1.4% were born in overseas France outside of French Polynesia, and 2.0% were born in foreign countries.[18] At the 1988 census, the last census which asked questions regarding ethnicity, 66.5% of people were ethnically unmixed Polynesians, 7.1 % were Polynesians with light European and/or East Asian mixing, 11.9% were Europeans (mostly French), 9.3% were people of mixed European and Polynesian descent, the so-called Demis (literally meaning "Half"), and 4.7% were East Asians (mainly Chinese).[1]
 The Europeans, the Demis and the East Asians are essentially concentrated on the island of Tahiti, particularly in the urban area of Papeetē, where their share of the population is thus much greater than in French Polynesia overall.[1] Race mixing has been going on for more than a century already in French Polynesia, resulting in a rather mixed society. For example Gaston Flosse, the long-time leader of French Polynesia, is a Demi (European father from Lorraine and Polynesian mother).[19] His main opponent and former president, Gaston Tong Sang is a member of the East Asian (in his case Chinese) community.[20] Oscar Temaru, the current president, is ethnically Polynesian (father from Tahiti, mother from the Cook Islands),[21] but he has admitted to also have Chinese ancestry.[22]
 Despite a long tradition of race mixing, racial tensions have been growing in recent years, with politicians using a xenophobic discourse and fanning the flame of racial tensions.[22][23] The pro-independence politicians have long pointed the finger at the European community (Oscar Temaru, pro-independence leader and former president of French Polynesia, was for example found guilty of "racial discrimination" by the criminal court of Papeetē in 2007 for having referred to the Europeans living in French Polynesia as "trash", "waste").[24] More recently, the Chinese community which controls many businesses in French Polynesia has been targeted in verbal attacks by the newly allied Gaston Flosse and Oscar Temaru in their political fight against Gaston Tong Sang, whose Chinese origins they emphasize in contrast with their Polynesian origins, despite the fact that they both have mixed origins (European and Polynesian for Flosse; Polynesian and Chinese for Temaru).[25]
 In April 2008, after the government of Gaston Flosse was toppled in the Assembly of French Polynesia and Gaston Tong Sang became the new president of French Polynesia, two French Polynesian labor union leaders made anti-Chinese remarks ("I'm not hiding from the fact that I wouldn't like our country to be ruled by someone who's not a Polynesian"; "a Chinese only thinks of the business leaders, because he is a businessman").[26] These anti-Chinese remarks caused a political furor and were widely condemned in French Polynesia.[27]
 French is the only official language of French Polynesia.[31] An organic law of 12 April 1996 states that "French is the official language, Tahitian and other Polynesian languages can be used." At the 2007 census, among the population whose age was 15 and older, 68.5% of people reported that the language they speak the most at home is French, 29.9% reported that the language they speak the most at home is any of the Polynesian languages (four-fifth of which Tahitian), 1.0% reported a Chinese language (half of which Hakka), and 0.6% another language.[32]
 At the same census, 94.7% of people whose age was 15 or older reported that they could speak, read and write French, whereas only 2.0% reported that they had no knowledge of French.[32] 74.6% of people whose age was 15 or older reported that they could speak, read and write one of the Polynesian languages, whereas 13.6% reported that they had no knowledge of any of the Polynesian languages.[32]
 Christianity is the main religion of the islands, a majority (54%) belonging to various Protestant churches and a large minority (30%) being Roman Catholic. Slightly more than 50% of French Polynesia's population belongs to the Maohi Protestant Church, the largest Protestant denomination.[33] The Church of Jesus Christ of Latter-day Saints had 20,282 members as of 2009.[34]
Jehovah's Witnesses -according to the 2009 Yearbook of Jehovah's Witnesses- had 2,248 publishers in Tahiti as of 2009.
 There are 53 airports in French Polynesia; 46 are paved.[35] The Faaa International Airport is the only international airport in French Polynesia. Each island has its own airport that serves flights to other islands. Air Tahiti is the main airline that flies around the islands.
 French Polynesia came to the forefront of the world music scene in 1992,[citation needed] with the release of The Tahitian Choir's recordings of unaccompanied vocal Christian music called himene tārava, recorded by French musicologist Pascal Nabet-Meyer. This form of singing is common in French Polynesia and the Cook Islands, and is distinguished by a unique drop in pitch at the end of the phrases, which is a characteristic formed by several different voices; it is also accompanied by steady grunting of staccato, nonsensical syllables.
 Baie de Cook in Mo'orea
 Mont Otemanu in Bora Bora
 Satellite view of the Iles Gambier
 Black sand beach in Tahiti
 Motu Tiahura, Mo'orea
 Fakarava lagoon
 Baie des traîtres, Hiva `Oa
 Presidence of the Government (Papeetē)
 Mangareva
 Bungalows of Hôtel Hibiscus, Hauru Point, Mo'orea
 Plage de Marita, Bora Bora
 Bora Bora from the air
 High Commissionership of the Republic (Papeetē)
 Tahiti
 Tahitian women on the beach, by Paul Gauguin (1891)
 Bora Bora
 Sunrise over Mo'orea
 Bora Bora, Taha'a and Ra'iātea from space
 Black sand beach, Tahiti
 Flags of French Polynesia and French Republic's Flag
 Coordinates: 17°32′S 149°34′W﻿ / ﻿17.533°S 149.567°W﻿ / -17.533; -149.567
 
 1 History 2 Politics 3 Administration

3.1 Administrative divisions

 3.1 Administrative divisions 4 Geography 5 Economy 6 Demographics

6.1 Historical population
6.2 Languages
6.3 Religion

 6.1 Historical population 6.2 Languages 6.3 Religion 7 Transportation 8 Notable people 9 Music 10 Gallery 11 See also 12 References 13 Bibliography 14 External links Marquesas Islands (French: (les) (Îles) Marquises or officially la subdivision administrative des (Îles) Marquises) Leeward Islands (French: (les) Îles Sous-le-Vent or officially la subdivision administrative des Îles Sous-le-Vent) (the two subdivisions administratives Windward Islands and Leeward Islands are part of the Society Islands) Windward Islands (French: (les) Îles du Vent or officially la subdivision administrative des Îles du Vent) (the two subdivisions administratives Windward Islands and Leeward Islands are part of the Society Islands) Tuāmotu-Gambier (French: (les) (Îles) Tuamotu-Gambier or officially la subdivision administrative des (Îles) Tuamotu-Gambier) (the Tuamotus and the Gambier Islands) Austral Islands (French: (les) (Îles) Australes or officially la subdivision administrative des (Îles) Australes) (including the Bass Islands) Marquesas Islands Society Islands Tuamotu Archipelago Gambier Islands often considered part of the Tuamotu Archipelago Austral Islands Bass Islands often considered part of the Austral Islands Jacques Brel (1929–1978), Belgian musician, lived in French Polynesia near the end of his life Jean Gabilou, singer (born 1944), represented France in the 1981 Eurovision Song Contest Paul Gauguin (1848–1903), French impressionist painter, spent the last years of his life in French Polynesia Maréva Georges, model, former Miss Tahiti 1990 and Miss France 1991 Pouvāna'a 'Ō'opa (1895–1977), Politician and Tahitian nationalist Henri Hiro (1944–1991), Film director and script writer, poet, ecologist, activist Ella Koon, model (born 1979) Marco Namouro, writer (1889–1968) Fabrice Santoro, tennis professional (born 1972) Marama Vahirua, footballer, cousin of Pascal Vahirua (born 1980) Pascal Vahirua, former French international footballer (born 1966) Célestine Hitiura Vaite, writer (born 1966) 


Baie de Cook in Mo'orea


 


Mont Otemanu in Bora Bora


 


Satellite view of the Iles Gambier


 


Black sand beach in Tahiti


 


Motu Tiahura, Mo'orea


 


Fakarava lagoon


 


Baie des traîtres, Hiva `Oa


 


Presidence of the Government (Papeetē)


 


Mangareva


 


Bungalows of Hôtel Hibiscus, Hauru Point, Mo'orea


 


Plage de Marita, Bora Bora


 


Bora Bora from the air


 


High Commissionership of the Republic (Papeetē)


 


Tahiti


 


Tahitian women on the beach, by Paul Gauguin (1891)


 


Bora Bora


 


Sunrise over Mo'orea


 


Bora Bora, Taha'a and Ra'iātea from space


 


Black sand beach, Tahiti


 


Flags of French Polynesia and French Republic's Flag


 Colonial and Departmental Heads of French Polynesia French colonial empire List of French possessions and colonies ↑ Jump up to: 1.0 1.1 1.2 Frontières ethniques et redéfinition du cadre politique à Tahiti (PDF). Retrieved on 31 May 2011.
 ↑ Jump up to: 2.0 2.1 2.2 (French) Institut Statistique de Polynésie Française (ISPF). Bilan, principaux indicateurs et estimations de population (Ensemble Polynésie). Retrieved on 21 January 2011.
 ↑ Jump up to: 3.0 3.1 3.2 3.3 (French) Institut Statistique de Polynésie Française (ISPF). Population légale au 20 août 2007. Retrieved on 13 January 2009.
 ↑ Jump up to: 4.0 4.1 4.2 4.3 (French) Institut Statistique de Polynésie Française (ISPF). La Production Intérieure Brute et le Produit Intérieur Brut. Retrieved on 14 September 2009. [dead link]
 ↑ Jump up to: 5.0 5.1 Ganse, Alexander. History of Polynesia, before 1797. Retrieved on 20 October 2007.
 ↑ Jump up to: 6.0 6.1 History of French Polynesia. History of Nations. Retrieved on 20 October 2007.
 Jump up ↑ Ganse, Alexander. History of French Polynesia, 1797 to 1889. Retrieved on 20 October 2007.
 Jump up ↑ Ganse, Alexander. History of French Polynesia, 1889 to 1918. Retrieved on 20 October 2007.
 Jump up ↑ The Japanese claim to the French Pacific islands, along with many other vast territories, appears in the 16 September 1940 "Sphere of survival for the Establishment of a New Order in Greater East Asia by Imperial Japan", published in 1955 by Japan's Foreign Ministry as part of the two-volume "Chronology and major documents of Diplomacy of Japan 1840–1945" – here quoted from "Interview with Tetsuzo Fuwa: Japan's War: History of Expansionism", Japan Press Service, July 2007
 Jump up ↑ Ganse, Alexander. History of Polynesia, 1939 to 1977. Retrieved on 20 October 2007.
 Jump up ↑ Ganse, Alexander. History of French Polynesia, 1977 to present. Retrieved on 20 October 2007.
 Jump up ↑ Whitney, Craig R (30 January 1996). "France Ending Nuclear Tests That Caused Broad Protests". New York Times. http://query.nytimes.com/gst/fullpage.html?res=950DE4D71639F933A05752C0A960958260&sec=&spon=&pagewanted=all. Retrieved 20 October 2007. 
 Jump up ↑ BBC NEWS, French Polynesia gets new leader. BBC News (14 September 2007). Retrieved on 31 May 2011.
 Jump up ↑ http://www.rfo.fr/article1437.html
 Jump up ↑ (French) Minister of the Interior, Government of France. POLYNESIE FRANCAISE (987) (résultats officiels). Retrieved on 14 September 2007.
 Jump up ↑ Kingfisher Geography Encyclopedia. ISBN 1-85613-582-9. Page 546
 ↑ Jump up to: 17.0 17.1 (French) Institut d'émission d'Outre-Mer (IEOM). La Polynésie française en 2008 (PDF). Retrieved on 14 September 2009. [dead link]
 Jump up ↑ (French) Institut Statistique de Polynésie Française (ISPF). Recensement 2007 – Migrations : Chiffres clés. Retrieved on 15 November 2008. [dead link]
 Jump up ↑ Flosse s'efface après 20 ans de règne sur la Polynésie[dead link]
 Jump up ↑ Victoire de Gaston Tong Sang[dead link]
 Jump up ↑ TUNUI (22 February 1999). Portrait du Président Oscar Manutahi TEMARU. Teaomaohi.pf. Retrieved on 31 May 2011.
 ↑ Jump up to: 22.0 22.1 Logiques " autonomiste " et " indépendantiste " en Polynésie française. Conflits.org. Retrieved on 31 May 2011.
 Jump up ↑ Temaru-Flosse: le rebond du nationalisme tahitien. Rue89.com (19 January 2011). Retrieved on 31 May 2011.
 Jump up ↑ Temaru Found Guilty Of "Racial Discrimination"
 Jump up ↑ Politique : Toujours pas de gouvernement[dead link]
 Jump up ↑ Anti-Chinese Remarks Cause A Political Furor
 Jump up ↑ Propos "anti-chinois": les réactions se multiplient
 Jump up ↑ 2002 census ((French)). Legifrance.gouv.fr. Retrieved on 31 May 2011.
 Jump up ↑ 1971, 1977, 1983, 1988, and 1996 censuses[dead link]
 Jump up ↑ Censuses from 1907 to 1962 in Population, 1972, #4–5, pp. 705–706, published by INED[dead link]
 Jump up ↑ Le tahitien reste interdit à l'assemblée de Polynésie, RFO, 6 October 2010
 ↑ Jump up to: 32.0 32.1 32.2 (French) Institut Statistique de Polynésie Française (ISPF). Recensement 2007 – Langues : Chiffres clés. Retrieved on 15 November 2008. [dead link]
 Jump up ↑ "http://www.webcitation.org/64LUkdnR5". Tahitipresse. 2010-07-26. http://www.webcitation.org/64LUkdnR5. Retrieved 2011-12-31. 
 Jump up ↑ LDS Newsroom Statistical Information
 Jump up ↑ CIA – The World Factbook. Cia.gov. Retrieved on 31 May 2011.
 Danielsson, Bengt (1965). Work and Life on Raroia: An Acculturation Study from the Tuamotu Group, French Oceania. London: G. Allen & Unwin.  Danielsson, Bengt; Marie-Thérèse Danielsson (1986). Poisoned Reign: French Nuclear Colonialism in the Pacific. New York: Penguin Books. ISBN 0-140-08130-5.  Hough, Richard (1995). Captain James Cook. W W Norton. ISBN 0393036804.  Pollock, Nancy J.; Ron Crocombe, eds. (1988). French Polynesia: A Book of Selected Readings. Suva, Fiji: Institute of Pacific Studies of the University of the South Pacific. ISBN 9820200326.  Thompson, Virginia; Richard Adloff (1971). The French Pacific Islands: French Polynesia and New Caledonia. Berkeley: University of California Press.  Aldrich, Robert (1990). The French Presence in the South Pacific, 1842–1940..  Aldrich, Robert (1993). France and the South Pacific since 1940..  James Rogers and Luis Simón. The Status and Location of the Military Installations of the Member States of the European Union and Their Potential Role for the European Security and Defence Policy (ESDP). Brussels: European Parliament, 2009. 25 pp. Government of French Polynesia Presidency of French Polynesia Administrative Subdivisions of French Polynesia (French) Encyclopédie collaborative du patrimoine culturel et naturel polynésien French Polynesia entry at The World Factbook French Polynesia at UCB Libraries GovPubs French Polynesia at the Open Directory Project Wikimedia Atlas of French Polynesia Official Tourism Website  French Polynesia travel guide from Wikitravel South Seas island guide Guadeloupe Martinique Réunion Mayotte2 St. Barthélemy St. Martin St. Pierre and Miquelon Wallis and Futuna Île Saint-Paul Crozet Islands Kerguelen Islands Adélie Land Bassas da India4 Europa Island4 Glorioso Islands3, 4, 5 Juan de Nova Island4 Tromelin Island4, 5 2 Claimed by Comoros 3 Claimed by Madagascar 4 Claimed by Seychelles 5 Claimed by Mauritius Country articles requiring maintenance Articles containing French language text French Polynesia Special territories of the European Union Overseas departments, collectivities and territories of France European colonisation in Oceania Polynesia States and territories established in 1842 Articles containing non-English language text Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 3 August 2016, at 18:00. Privacy policy About Metapedia Disclaimers 
  Motto:  Anthem:  Capital Ethnic groups GDP HDI French Polynesia French Polynesia · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · Main article: Politics of French Polynesia Main article: Administrative divisions of French Polynesia Main article: Geography of French Polynesia Main article: Economy of French Polynesia Main article: Demographics of French Polynesia Main article: Transportation in French Polynesia Main article: Music of French Polynesia Main article: Politics of French Polynesia Main article: Administrative divisions of French Polynesia Main article: Geography of French Polynesia Main article: Economy of French Polynesia Main article: Demographics of French Polynesia Main article: Transportation in French Polynesia Main article: Music of French Polynesia Government General information Travel 






     Motto: "Tahiti Nui Māre'are'a" Anthem: La Marseillaise 
 Capital Papeetē17°34′S 149°36′W﻿ / ﻿17.567°S 149.6°W﻿ / -17.567; -149.6 Fa'a'a French Ethnic groups  (in 1988, last ethnic census)[1]66.5% unmixed Polynesians;7.1% Polynesians with light European and/or East Asian mixing;11.9% Europeans (mostly French);9.3% Demis (mixed European and Polynesian descent);4.7% East Asians (mostly Chinese) French Polynesian Dependent territory  -  President of France Nicolas Sarkozy  -  Presidentof French Polynesia Oscar Temaru  -  High Commissioner Richard Didier  -  Protectorate 1842   -  Overseas territory 1946   -  Overseas collectivity 2004   -  Total 4,167 km2 (173rd)1,609 sq mi   -  Water (%) 12  -  Jan. 1, 2010 estimate 267,000[2] (177th)  -  Aug. 2007 census 259,596[3] (177th)  -  Density 63/km2 (130th)163.2/sq mi GDP (nominal) 2006 estimate  -  Total US$5.65 billion[4] (not ranked)  -  Per capita US$21,999[4] (not ranked) HDI (n/a) n/aError: Invalid HDI value · n/a CFP franc (XPF)  (UTC-10, −9:30, -9) right +689 .pf Faaa Tahiti 29,900
 Papeete Tahiti 26,300
 Mahina Tahiti 14,500
 30,600 31,900 31,600 35,900 40,400 44,000 51,200 58,200 63,300 76,323
 84,551 119,168 137,382 166,753 188,814 219,521 245,516 259,596 267,000
 Official figures from past censuses.[2][3][28][29][30]
  Find more about French Polynesia on Wikipedia's sister projects:
  Definitions from Wiktionary
  Textbooks from Wikibooks
  Quotations from Wikiquote
  Source texts from Wikisource
  Images and media from Commons
  News stories from Wikinews
  Learning resources from Wikiversity
 v • d • e
French Polynesia Society Islands |  Marquesas Islands |  Austral Islands |  Tuamotus |  Gambier IslandsPoliticsHigh Commissioner · President · Parliament · Elections · Political parties · Coat of armsHistoryPolynesians · Popular history · Pomare dynasty · Bombardment of PapeeteGeographyAdministrative divisions · Cities and towns · Volcanoes · Climate · WildlifeEconomyCurrency · Communications · Transport · AirportsSocietyDemographics · Languages · Education · Music · Dance · Sports · Mythology · Religion · FlagOutline · Index   Society Islands |  Marquesas Islands |  Austral Islands |  Tuamotus |  Gambier Islands  Politics High Commissioner · President · Parliament · Elections · Political parties · Coat of arms   History Polynesians · Popular history · Pomare dynasty · Bombardment of Papeete  Geography Administrative divisions · Cities and towns · Volcanoes · Climate · Wildlife  Economy Currency · Communications · Transport · Airports  Society Demographics · Languages · Education · Music · Dance · Sports · Mythology · Religion · Flag  Outline · Index  Geographic locale{{{list1}}}  {{{list1}}} v • d • e
Austronesian-speaking countries and territoriesFormosanTaiwanMalayo-Polynesian American Samoa ·  Brunei ·  Burma (Myanmar) ·  Cambodia ·  Christmas Island ·  Cocos (Keeling) Islands ·  Cook Islands ·  Easter Island ·  East Timor ·  Fiji ·  French Polynesia ·  Guam ·  Hainan ·  Indonesia ·  Kiribati ·  Madagascar ·  Malaysia ·  Marshall Islands ·  FS Micronesia ·  Nauru ·  New Caledonia ·  New Zealand ·  Niue ·  Northern Mariana Islands · Orchid Island ·  Palau ·  Papua New Guinea ·  Philippines ·  Samoa ·  Singapore ·  Solomon Islands ·  Sri Lanka ·  Suriname ·  Tokelau ·  Tonga ·  Tuvalu ·  United States (Hawaii) ·  Vanuatu ·  Vietnam ·  Wallis and Futuna   Formosan Taiwan  Malayo-Polynesian  American Samoa ·  Brunei ·  Burma (Myanmar) ·  Cambodia ·  Christmas Island ·  Cocos (Keeling) Islands ·  Cook Islands ·  Easter Island ·  East Timor ·  Fiji ·  French Polynesia ·  Guam ·  Hainan ·  Indonesia ·  Kiribati ·  Madagascar ·  Malaysia ·  Marshall Islands ·  FS Micronesia ·  Nauru ·  New Caledonia ·  New Zealand ·  Niue ·  Northern Mariana Islands · Orchid Island ·  Palau ·  Papua New Guinea ·  Philippines ·  Samoa ·  Singapore ·  Solomon Islands ·  Sri Lanka ·  Suriname ·  Tokelau ·  Tonga ·  Tuvalu ·  United States (Hawaii) ·  Vanuatu ·  Vietnam ·  Wallis and Futuna  v • d • e
 Overseas departments and territories of France Inhabited areasOverseas departments1* French Guiana
Guadeloupe
Martinique
Réunion
Mayotte2Overseas collectivities* French Polynesia
St. Barthélemy
St. Martin
St. Pierre and Miquelon
Wallis and FutunaSpecial status* New Caledonia Uninhabited areasPacific Ocean* Clipperton IslandFrench Southern and  Antarctic Lands* Île Amsterdam
Île Saint-Paul
Crozet Islands
Kerguelen Islands
Adélie LandScattered islands in  the Indian Ocean* Banc du Geyser4
Bassas da India4
Europa Island4
Glorioso Islands3, 4, 5
Juan de Nova Island4
Tromelin Island4, 5* 1 Also known as overseas regions
2 Claimed by Comoros
3 Claimed by Madagascar
4 Claimed by Seychelles
5 Claimed by Mauritius   Inhabited areasOverseas departments1* French Guiana
Guadeloupe
Martinique
Réunion
Mayotte2Overseas collectivities* French Polynesia
St. Barthélemy
St. Martin
St. Pierre and Miquelon
Wallis and FutunaSpecial status* New Caledonia  Overseas departments1* French Guiana
Guadeloupe
Martinique
Réunion
Mayotte2Overseas collectivities* French Polynesia
St. Barthélemy
St. Martin
St. Pierre and Miquelon
Wallis and FutunaSpecial status* New Caledonia Overseas departments1 * French Guiana
Guadeloupe
Martinique
Réunion
Mayotte2   Overseas collectivities * French Polynesia
St. Barthélemy
St. Martin
St. Pierre and Miquelon
Wallis and Futuna  Special status * New Caledonia   Uninhabited areasPacific Ocean* Clipperton IslandFrench Southern and  Antarctic Lands* Île Amsterdam
Île Saint-Paul
Crozet Islands
Kerguelen Islands
Adélie LandScattered islands in  the Indian Ocean* Banc du Geyser4
Bassas da India4
Europa Island4
Glorioso Islands3, 4, 5
Juan de Nova Island4
Tromelin Island4, 5  Pacific Ocean* Clipperton IslandFrench Southern and  Antarctic Lands* Île Amsterdam
Île Saint-Paul
Crozet Islands
Kerguelen Islands
Adélie LandScattered islands in  the Indian Ocean* Banc du Geyser4
Bassas da India4
Europa Island4
Glorioso Islands3, 4, 5
Juan de Nova Island4
Tromelin Island4, 5 Pacific Ocean * Clipperton Island  French Southern and  Antarctic Lands * Île Amsterdam
Île Saint-Paul
Crozet Islands
Kerguelen Islands
Adélie LandScattered islands in  the Indian Ocean* Banc du Geyser4
Bassas da India4
Europa Island4
Glorioso Islands3, 4, 5
Juan de Nova Island4
Tromelin Island4, 5 * Île Amsterdam
Île Saint-Paul
Crozet Islands
Kerguelen Islands
Adélie Land  Scattered islands in  the Indian Ocean * Banc du Geyser4
Bassas da India4
Europa Island4
Glorioso Islands3, 4, 5
Juan de Nova Island4
Tromelin Island4, 5    * 1 Also known as overseas regions
2 Claimed by Comoros
3 Claimed by Madagascar
4 Claimed by Seychelles
5 Claimed by Mauritius French Polynesia Contents History Politics Administration Geography Economy Demographics Transportation Notable people Music Gallery See also References Bibliography External links Navigation menu Administrative divisions Historical population Languages Religion Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools Government General information Travel 