Peter Karl Ott von Bátorkéz (1738 – 10 May 1809) joined the Austrian army and fought in the wars against the Kingdom of Prussia, Ottoman Turkey, and the First French Republic in the last half of the 18th century. During the French Revolutionary Wars, he rose in rank to general officer and twice campaigned against the army of Napoleon Bonaparte in Italy. He played a key role in the Marengo campaign in 1800. He was Proprietor (Inhaber) of an Austrian Hussar regiment from 1801 to 1809.
 Born in Esztergom (Gran), Hungary in 1738, Ott joined Andlau Infantry Regiment # 57 as a Fahnrich (cadet) in 1757. He was a veteran of the Seven Years War, having fought at the battles of Landshut and Liegnitz, being wounded in the latter action. In the War of the Bavarian Succession he served in the Kálnoky Hussar Regiment # 2 as a Major. While still serving with the Kálnoky Hussars as the Oberst-Leutnant, he led his horsemen in numerous actions during the Austro-Turkish War (1787-1791). After directing the hussars in a successful action at Valje Muliere, he earned promotion to Oberst (colonel) of the regiment. He was awarded the Military Order of Maria Theresa for an action against the Turks at Calafat, Wallachia in modern Romania. In 1791, he was elevated to the noble rank of Freiherr.[1]
 In 1793 Ott served on the upper Rhine in actions at Offenbach, Schaid and Brumath. On 1 January 1794, he received promotion to General-Major. He continued to serve in the same theater in 1794-1795.
 Ott transferred to the Italian campaign against Napoleon Bonaparte in 1796. In the operations that resulted in the Battle of Lonato during the Castiglione campaign, he commanded one of the four brigades under the orders of Peter Quasdanovich. On 29 July he defeated a French brigade near Salò and advanced south along the banks of the Chiese River. On 31 July he attacked and seized Lonato del Garda though he was later driven out of the latter place by superior French forces under Hyacinthe Despinoy and Claude Dallemagne. On 3 August, his reinforced brigade repulsed the attacks of Despinoy and Dallemagne near Paitone and Gavardo. Unfortunately for the Austrians, the battle went badly for them at Lonato and Salò, compelling Quasdanovich to order a retreat.[2]
 After Dagobert von Wurmser's defeat at the Battle of Bassano on 8 September, Ott led the Austrian advance guard with great distinction during Wurmser's dash to Mantua. At Cerea, he held off a French intercepting column long enough for Wurmser to arrive and defeat it.[3] The next day, Ott ambushed the 12th Light Demi-Brigade, killing its commander, Brigadier General Charles-François Charton and capturing 400 men.[1] During the battle under the walls of Mantua on 15 September, he commanded the left flank at La Favorita. His force was the last part of the Austrian army to retreat.[4] He led a sortie on 23 September which suffered heavy losses at Governolo.[5] In February 1797, he surrendered with the garrison at the unsuccessful conclusion of the Siege of Mantua. On 1 March 1797, Emperor Francis II appointed Ott Feldmarschal-Leutnant.[6]
 Ott served under Pál Kray and the Russian Alexander Suvarov during the 1799 liberation of Italy from the French. During this period, he commanded a division at the capture of Brescia on 21 April, the Battle of Cassano on 27 April, and the Battle of the Trebbia on 17–20 June.[7] He led his division at the decisive Battle of Novi on 15 August.[8]
 After the Austrian army of Michael von Melas invested a French army in Genoa during April 1800, Ott was given command of the siege operations. Ordered to raise the Siege of Genoa on 2 June, he disobeyed orders and remained in position.[9] As he hoped, a few hours later, André Masséna asked for terms and formally surrendered to Ott on 4 June. In the bitterly fought Battle of Montebello on 9 June, a French force led by Jean Lannes defeated Ott's corps. He had fought the battle against orders. When the Austrian chief of staff begged him to withdraw at the beginning of the action, Ott replied, "My outposts are attacked. I march to succor them."[10]
 At the Battle of Marengo on 14 June, Ott commanded the Austrian left wing. Because of congestion in the bridgehead, it took a long time for the left wing to deploy. However, once his attack at mid-day got underway, it broke the French right flank and helped convince them to retreat from Marengo.[11] Later in the battle, when French reinforcements defeated the Austrian main body, Ott withdrew in good order and brought his command off in safety.[12] This was his last active command.
 In 1801, he became the proprietor of the Ott Hussar Regiment # 5.[13] He retired from the army in 1805. The Ott Hussars served in Italy and Hungary during the War of the Fifth Coalition in 1809.[14] Ott died in Budapest on 10 May of that year.
 1 Early career 2 French Revolutionary Wars

2.1 Rhine 1793-1795
2.2 Italy 1796-1797
2.3 Italy 1799
2.4 Italy 1800

 2.1 Rhine 1793-1795 2.2 Italy 1796-1797 2.3 Italy 1799 2.4 Italy 1800 3 Retirement 4 References

4.1 External references
4.2 Footnotes

 4.1 External references 4.2 Footnotes Arnold, James R. Marengo & Hohenlinden. Barnsley, South Yorkshire, UK: Pen & Sword, 2005. ISBN 1-84415-279-0 Bowden, Scotty & Tarbox, Charlie. Armies on the Danube 1809. Arlington, Texas: Empire Games Press, 1980. Boycott-Brown, Martin. The Road to Rivoli. London: Cassell & Co., 2001. ISBN 0-304-35305-1 Chandler, David. Dictionary of the Napoleonic Wars. New York: Macmillan, 1979. ISBN 0-02-523670-9 Pivka, Otto von. Armies of the Napoleonic Era. New York: Taplinger Publishing, 1979. ISBN 0-8008-5471-3 Smith, Digby. The Napoleonic Wars Data Book. London: Greenhill, 1998. ISBN 1-85367-2769 Peter Ott by Digby Smith, compiled by Leopold Kudrna ↑ 1.0 1.1 Smith-Kudrna, Ott
 ↑ Boycott-Brown, pp 385 & 392
 ↑ Smith, pp 123-124
 ↑ Boycott-Brown, pp 434-435
 ↑ Boycott-Brown, p 439
 ↑ Chandler, p 323, 465. Chandler incorrectly credits Ott instead of Otto with the victory at the Battle of Villers-en-Cauchies.
 ↑ Smith, pp 151-152, 152, 159
 ↑ Smith, p 163
 ↑ Arnold, p 75
 ↑ Arnold, p 122
 ↑ Arnold, pp 161-162
 ↑ Arnold, p 183
 ↑ Pivka, p 91
 ↑ Bowden, p 107
 Austrian soldiers Austrian generals Military leaders of the French Revolutionary Wars Austrian Empire military leaders of the French Revolutionary Wars Hungarian generals 1738 births 1809 deaths Military Order of Maria Theresa recipients Pages using duplicate arguments in template calls Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 2 February 2012, at 10:37. Privacy policy About Metapedia Disclaimers 
  bátorkézi Ott Károly Péter Peter Karl Ott von Bátorkéz Newly raised in 1798 Proprietor (Inhaber) of Hussar Regiment # 5 Joseph Radetzky von Radetz The native form of this personal name is bátorkézi Ott Károly Péter. This article uses the Western name order. The native form of this personal name is bátorkézi Ott Károly Péter. This article uses the Western name order. Preceded byNewly raised in 1798
 Proprietor (Inhaber) of Hussar Regiment # 51801–1809
 Succeeded byJoseph Radetzky von Radetz
 NAME
 Ott von Batorkez, Peter Karl
 ALTERNATIVE NAMES
 
 SHORT DESCRIPTION
 
 DATE OF BIRTH
 1738
 PLACE OF BIRTH
 
 DATE OF DEATH
 10 May 1809
 PLACE OF DEATH
 
 Peter Karl Ott von Bátorkéz Contents Early career French Revolutionary Wars Retirement References Navigation menu Rhine 1793-1795 Italy 1796-1797 Italy 1799 Italy 1800 External references Footnotes Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 