Sir Isaac Alfred Isaacs GCB GCMG KC (6 August 1855 – 11 February 1948) was an Australian judge and politician, the third Chief Justice of Australia, ninth Governor-General of Australia and the first born in Australia to occupy that post. He is the only person ever to have held both positions of Chief Justice of Australia and Governor-General of Australia. He was also one of only three Governors-General of Australia to live to 92.
 Isaac Isaacs was the son of Alfred Isaacs, a tailor of Jewish ancestry from the town of Mława, Poland. Seeking greater fortune, Alfred left Poland and worked his way across what is now Germany, spending some months in Berlin and Frankfurt. By 1845 he had passed through Paris and arrived to work in London. In London while working as a tailor he met Rebecca Abrahams, a fellow Jew; the two marrying in 1849. After news of the 1851 Victorian gold rush reached England, Australia became a very popular destination for fortune seekers and the Isaacs decided to emigrate. By 1854 they had saved sufficient for the fare, departing from Liverpool in June 1854 and arriving in Melbourne in September.[1] Some time after arriving the Isaacs moved into a cottage and shopfront in Elizabeth Street, Melbourne, where Alfred continued his tailoring. Isaac Alfred Isaacs was born in this cottage on 6 August 1855.[2] His family moved to various locations around Melbourne while he was young, then in 1859 moved to Yackandandah in northern Victoria, where friends of the family already lived.[3] At this time Yackandandah was a large gold mining settlement of 3,000 people.
 Isaacs' three siblings, John, who later became a solicitor and Victorian Member of Parliament, Carolyn and Hannah were born in Yackandandah. Another brother, born in Melbourne, and a sister, born in Yackandandah, both died very young.[4] His first formal schooling was from sometime after 1860 at a small private establishment. At eight he won the school arithmetic prize, winning his photograph by the schoolmaster, who was also a photographer and bootmaker. Yackandandah state school was opened in 1863 and Isaacs enrolled as a pupil. Here he excelled academically, particularly in arithmetic and languages, though he was a frequent truant, walking off to spend time in the nearby mining camps. To help Isaacs gain a better quality education, in 1867, his family moved to nearby Beechworth first enrolling him in the Common school then in the Beechworth Grammar School.[5] He excelled at the Grammar School, becoming dux in his first year and winning many academic prizes.[6] In his second year he was employed part time as an assistant teacher at the school, and took up after school tutoring of fellow students. In September 1870, when Isaacs was just 15 years old, he passed his examination as a pupil teacher and taught at the school from then until 1873. Isaacs was next employed as an assistant teacher at the Beechworth State School, the successor to the Common school.[7]
 While employed at the State School, Isaacs had his first experience of the Law, as an unsuccessful litigant in an 1875 County Court case. He disputed a payment arrangement with the headmaster of his school, resigning as part of the dispute. After returning to teaching, now back at the Grammar School, he expanded his interest in the law; reading law books and attending court sittings.[8]
 As a child Isaacs became fluent in Russian, which his parents spoke frequently, as well as English and some German. Isaacs later gained varying degrees of proficiency in Italian, French, Greek, Hindustani and Chinese.[9]
 In 1875 he moved to Melbourne and found work at the Prothonotary's Office of the Law Department. In 1876, while still working full-time, he studied law at the University of Melbourne. He graduated in 1880 with a Master of Laws degree in 1883. In 1888 he married Deborah Jacobs, then they had two daughters.
 In 1892 Isaacs was elected to the Victorian Legislative Assembly as a liberal. In 1893 he became Solicitor-General. He was the member for Bogong from May 1892 until May 1893 and between June 1893 and May 1901. In 1897 he was elected to the Convention, that drafted the Australian Constitution, where he supported those arguing for a more democratic draft. He took silk as a Queen's Counsel in 1899.[10]
 Isaacs was elected to the first federal Parliament in 1901 to the seat of Indi as a critical supporter of Edmund Barton and his Protectionist government. He was one of a group of backbenchers pushing for more radical policies and he earned the dislike of many of his colleagues through what they saw as his aloofness and rather self-righteous attitude to politics.
 Alfred Deakin appointed Isaacs Attorney-General in 1905 but he was a difficult colleague and in 1906 Deakin was keen to get him out of politics by appointing him to the High Court bench. He was the first serving Minister to resign from the Parliament. On the High Court he joined H. B. Higgins as a radical minority on the Court in opposition to the Chief Justice, Sir Samuel Griffith. He served on the Court for 24 years, acquiring a reputation as a learned radical but uncollegial justice.
 Isaacs was one of only eight justices of the High Court to have served in the Parliament of Australia prior to his appointment to the Court; the others were Edmund Barton, Richard O'Connor, H. B. Higgins, Edward McTiernan, John Latham, Garfield Barwick, and Lionel Murphy. He was also one of two to have served in the Parliament of Victoria, along with Higgins.
 In 1930 the Labor Prime Minister, James Scullin, appointed Isaacs, by this time aged 75, as Chief Justice. Shortly afterwards, however, Scullin decided to appoint an Australian as Governor-General and offered the post to Isaacs. Scullin personally advised King George V to make the appointment, during his 1930 trip to Europe. The King reluctantly agreed to his advice, although his own preferred appointee[11] was Field Marshal Sir William Birdwood (later Lord Birdwood), who had commanded the Australian Imperial Force during World War I.
 Isaacs agreed to a reduction in salary and conducted the office with great frugality. He gave up his official residences in Sydney and Melbourne and most official entertaining. He was the first Governor-General to live permanently at Government House, Canberra. This was well-received with the public as was Isaacs's image of rather austere dignity.
 Although Isaacs was seen as a Labor appointment, the Scullin government fell at the end of 1931, and the rest of Isaacs's term was during the United Australia Party government of Joseph Lyons.
 Isaacs was 81 when his term ended in 1936, but his public life was far from over. He remained active in various causes for another decade and wrote frequently on matters of constitutional law. In the 1940s he became embroiled in controversy with the Jewish community both in Australia and internationally through his outspoken opposition to Zionism. His principal critic was Julius Stone.[12] Isaacs was supported by Rabbi Jacob Danglow and Harold Boas. Isaacs insisted that Judaism was a religious identity and not a national or ethnic one. He opposed the notion of a Jewish homeland in Palestine. Isaacs said "[p]olitical Zionism to which I am irrevocably opposed for the reasons which will be found clearly stated, must be sharply distinguished from religious and cultural Zionism to which I am strongly attached."[13]
 Isaacs opposed Zionism partly because he disliked nationalism of all kinds and saw Zionism as a form of Jewish national chauvinism—and partly because he saw the Zionist agitation in Palestine as disloyalty to the British Empire to which he was devoted. When Zionist terrorists blew up the King David Hotel in 1946 he wrote that "the honour of Jews throughout the world demands the renunciation of political Zionism".
Isaacs' main objections to Political Zionism were:-
 Isaacs said "the Zionist movement as a whole...now places its own unwarranted interpretation on the Balfour Declaration, and makes demands that are arousing the antagonism of the Moslem world of nearly 400 millions, thereby menacing the safety of our Empire, endangering world peace and imperiling some of the most sacred associations of the Jewish, Christian, and Moslem faiths. Besides their inherent injustice to others these demands would, I believe, seriously and detrimentally affect the general position of Jews throughout the world..".[15]
 He died in February 1948 and thus did not live to see the creation of the State of Israel.
 In May 1949 he was honoured with the naming of the Australian Electoral Division of Isaacs (1949–69) in the outer southern suburbs of Melbourne.
 At a redistribution in November 1968 the electorate was abolished and a separate Division of Isaacs was created in the south-eastern suburbs of Melbourne. It exists to this day.
 The Canberra suburb of Isaacs was named after him in 1966.
 In 1973 he was honoured on a postage stamp bearing his portrait issued by Australia Post.[16]
 Lee, Godfrey S. The battle of the scholars: the debate between Sir Isaac Isaacs and Julius Stone over Zionism during World War 2. Australian Journal of Politics and History, v.31, no.1, 1985: 128–134
 Kirby, Michael. Sir Isaac Isaacs – a sesquicentenary reflection [online]. Melbourne University Law Review, v.29, no.3, Dec 2005: 880–904.
 Austin Chapman •
Thomas Ewing •
John Forrest •
Littleton Groom •
Isaac Isaacs •
John Keating •
William Lyne •
Samuel Mauger •
Thomas Playford II
 
 1 Early life 2 Working life 3 Opposition to political Zionism 4 Honours 5 Isaacs's written works 6 Biographies 7 Articles 8 Notes 9 References 10 External links “A negation of Democracy, and an attempt to revert to the Church-State of bygone ages. Provocative anti-Semitism. Unwarranted by the Balfour Declaration, the Mandate, or any other right; contrary to Zionist assurances to Britain and to the Arabs and in present conditions unjust to other Palestinians politically and to other religions. As regards unrestricted immigration, a discriminatory and an undemocratic camouflage for a Jewish State. An obstruction to the consent of the Arabs to the peaceful and prosperous settlement in Palestine of hundreds of thousands of suffering European Jews, the victims of National Socialist atrocities; and provocative of Moslem antagonism within and beyond the Empire, and consequently a danger to its integrity and safety. Inconsistent in demanding on one hand, on a basis of a separate Jewish nationality everywhere Jews are found, Jewish domination in Palestine, and at the same time claiming complete Jewish equality elsewhere than in Palestine, on the basis of a nationality common to the citizens of every faith.”[14]  The new agriculture, 1901, Melbourne : Department of Agriculture Opinion of the Hon. Isaac A. Isaacs, K.C., M.P., re the case of Lieutenant Witton, 1902, Melbourne : [s.n.]  The Riverina Transport case, 1938, Melbourne : Australian Natives' Association, Victorian Board of Directors Australian democracy and our constitutional system, 1939, Melbourne : Horticultural Press An appeal for a greater Australia : the nation must itself take power for its post-war reconstruction; the constitutional issue stated; dynamic democracy, 1943, Melbourne : Horticultural Press Referendum powers : :a stepping stone to greater freedom, 1946, Melbourne : [s.n.] Palestine : peace and prosperity or war and destruction? Political Zionism : undemocratic, unjust, dangerous, 1946, Melbourne : Ramsey Ware Publishing Gordon, Max. Sir Isaac Isaacs: A Life of Service (Heinemann: Melbourne) 1963. Cowen, Sir Zelman. Isaac Isaacs (Oxford University Press) 1967 Cowen, Sir Zelman. Sir Isaac Isaacs (Melbourne University Press) 1979 ↑ Gordon (1963), pp.1–5
 ↑ Gordon (1963), pp.9–10
 ↑ Gordon (1963), pp.12–14
 ↑ Gordon (1963), pp.13,18
 ↑ Gordon (1963), pp.19–20
 ↑ Cowen, Zelman (1983). "Isaacs, Sir Isaac Alfred (1855–1948)". Australian Dictionary of Biography. Australian National University. http://www.adb.online.anu.edu.au/biogs/A090439b.htm. Retrieved 11 November 2008. 
 ↑ Gordon (1963), p.23
 ↑ Gordon (1963), pp.23–25
 ↑ Gordon (1963), pp.12–13,17
 ↑ Sir Isaac Isaacs, Contribution and significance of an individual in the 1930s, Australia between the wars: 1930s, History Year 9, NSW | Online Education Home Schooling Skwirk Australia. Skwirk.com.au. Retrieved on 2011-06-06.
 ↑ Gavin Souter, Acts of Parliament, p. 268
 ↑ Julius Stone, “Stand up and be counted!” An open letter to the Rt Hon Sir Isaac Isaacs on the occasion of the 26th anniversary of the Jewish National Home, 1944.
 ↑ Isaacs, pp. 7–8.
 ↑ Isaacs
 ↑ Isaacs, pp. 8–9.
 ↑ Australian stamp. None. Retrieved on 2011-06-06.
 Gordon, Max (1963). Sir Isaac Isaacs. Adelaide: Heinemann.  Isaacs, Sir Isaac. ‘Palestine: Peace and Prosperity or War and Destruction? Political Zionism: Undemocratic, Unjust, Dangerous’ (Ramsay Ware Publishing) 14 January 1946 University of Melbourne: Isaac Alfred Isaacs includes photograph (1898). Australian Dictionary of Biography: Isaac Alfred Isaacs includes portrait as Chief Justice National Library of Australia: Papers of Sir Isaac Isaacs Isaac Isaacs Victorian Parliamentary Profile Indi Election Results 1901 Tennyson Northcote Dudley Denman Munro Ferguson Forster Stonehaven Isaacs Gowrie Gloucester McKell Slim Dunrossil De L'Isle Casey Hasluck Kerr Cowen Stephen Hayden Deane Hollingworth Jeffery Bryce Knox Isaacs Gavan Duffy Latham Dixon Barwick Gibbs Mason Brennan Gleeson French O'Connor Higgins Powers Piddington Rich Starke Evatt McTiernan Williams Webb Fullagar Kitto Taylor Menzies Windeyer Owen Walsh Stephen Jacobs Murphy Aickin Wilson Deane Dawson Toohey Gaudron McHugh Gummow Kirby Hayne Callinan Heydon Crennan Kiefel Bell Articles in need of neutralization Use dmy dates from October 2011 1855 births 1948 deaths Attorneys General of Australia Australian federationists Australian Jews Australian knights Australian people of Polish descent Chief Justices of Australia Governors-General of Australia Jewish anti-Zionism Justices of the High Court of Australia Knights Grand Cross of the Order of the Bath Knights Grand Cross of the Order of St Michael and St George Members of the Australian House of Representatives Members of the Australian House of Representatives for Indi Members of the Victorian Legislative Assembly Protectionist Party politicians University of Melbourne alumni Queen's Counsel 1901–2000 Australian Queen's Counsel Jewish politicians Attorneys-General of Victoria Politicians from Melbourne Pages using duplicate arguments in template calls Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 10 December 2015, at 18:51. Privacy policy About Metapedia Disclaimers 
  In office In office In office In office In office Sir Isaac Alfred Isaacs Josiah Symon Attorney-General of Australia Littleton Groom Lord Stonehaven Governor-General of Australia Lord Gowrie New division Member for Indi Joseph Brown Sir Adrian Knox Chief Justice of Australia Sir Frank Gavan Duffy Isaac Isaacs Prime Minister: The Right Honourable Sir Isaac Isaacs GCB GCMG KC
 
 In office21 January 1931 – 23 January 1936
 George VEdward VIII
 The Lord Stonehaven
 The Lord Gowrie
 In office2 April 1930 – 21 January 1931
 James Scullin
 The Lord Stonehaven
 Sir Adrian Knox
 Sir Frank Gavan Duffy
 Puisne Justice of the High Court of Australia
 In office12 October 1906 – 2 April 1930
 Alfred Deakin
 Henry Northcote, 1st Baron Northcote
 Sir Frank Gavan Duffy
 Sir Edward McTiernan
 In office6 July 1905 – 10 October 1906
 Josiah Symon
 Littleton Groom
 Member of the Australian Parliamentfor Indi
 In office9 May 1901 – 10 October 1906
 NoneAustralian Federation
 Joseph Brown
 
 6 August 1855(1855-08-06)Melbourne, Victoria Colony
 11 February 1948 (aged 92)Melbourne, VictoriaAustralia
 Barrister, Politician & Judge
 Judaism
 Preceded byJosiah Symon
 Attorney-General of Australia1905–1906
 Succeeded byLittleton Groom
 Preceded byLord Stonehaven
 Governor-General of Australia1931–1936
 Succeeded byLord Gowrie
 New division
 Member for Indi1901–1906
 Succeeded byJoseph Brown
 Preceded bySir Adrian Knox
 Chief Justice of Australia1930–1931
 Succeeded bySir Frank Gavan Duffy
   Wikimedia Commons has media related to: Isaac Isaacs  v • d • e
Governors-General of Australia* Hopetoun
Tennyson
Northcote
Dudley
Denman
Munro Ferguson
Forster
Stonehaven
Isaacs
Gowrie
Gloucester
McKell
Slim
Dunrossil
De L'Isle
Casey
Hasluck
Kerr
Cowen
Stephen
Hayden
Deane
Hollingworth
Jeffery
Bryce   * Hopetoun
Tennyson
Northcote
Dudley
Denman
Munro Ferguson
Forster
Stonehaven
Isaacs
Gowrie
Gloucester
McKell
Slim
Dunrossil
De L'Isle
Casey
Hasluck
Kerr
Cowen
Stephen
Hayden
Deane
Hollingworth
Jeffery
Bryce  v • d • e
Justices of the High Court of AustraliaChief Justices of Australia*Griffith
Knox
Isaacs
Gavan Duffy
Latham
Dixon
Barwick
Gibbs
Mason
Brennan
Gleeson
FrenchPuisne Justices*Barton
O'Connor
Higgins
Powers
Piddington
Rich
Starke
Evatt
McTiernan
Williams
Webb
Fullagar
Kitto
Taylor
Menzies
Windeyer
Owen
Walsh
Stephen
Jacobs
Murphy
Aickin
Wilson
Deane
Dawson
Toohey
Gaudron
McHugh
Gummow
Kirby
Hayne
Callinan
Heydon
Crennan
Kiefel
Bellcurrent Justices are in italics  Chief Justices of Australia *Griffith
Knox
Isaacs
Gavan Duffy
Latham
Dixon
Barwick
Gibbs
Mason
Brennan
Gleeson
French  Puisne Justices *Barton
O'Connor
Higgins
Powers
Piddington
Rich
Starke
Evatt
McTiernan
Williams
Webb
Fullagar
Kitto
Taylor
Menzies
Windeyer
Owen
Walsh
Stephen
Jacobs
Murphy
Aickin
Wilson
Deane
Dawson
Toohey
Gaudron
McHugh
Gummow
Kirby
Hayne
Callinan
Heydon
Crennan
Kiefel
Bell  current Justices are in italics v • d • e
Second Deakin Cabinet (1905–06)Prime Minister: Alfred Deakin
Austin Chapman •
Thomas Ewing •
John Forrest •
Littleton Groom •
Isaac Isaacs •
John Keating •
William Lyne •
Samuel Mauger •
Thomas Playford II

  Prime Minister: Alfred Deakin  
Austin Chapman •
Thomas Ewing •
John Forrest •
Littleton Groom •
Isaac Isaacs •
John Keating •
William Lyne •
Samuel Mauger •
Thomas Playford II

  NAME
 Isaacs, Isaac Alfred
 ALTERNATIVE NAMES
 
 SHORT DESCRIPTION
 Governor-General of Australia
 DATE OF BIRTH
 6 August 1855
 PLACE OF BIRTH
 Melbourne, Victoria, Australia
 DATE OF DEATH
 11 August 1948
 PLACE OF DEATH
 South Yarra, Victoria, Victoria, Australia
 Isaac Isaacs Contents Early life Working life Opposition to political Zionism Honours Isaacs's written works Biographies Articles Notes References External links Navigation menu Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 