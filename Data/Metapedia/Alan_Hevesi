Alan G. Hevesi (born January 31, 1940) is a Democratic politician who
served as a New York State Assemblyman from 1971 to 1993, as Comptroller of the City of New York from 1994 to 2001, and as State Comptroller for the State of New York from 2003 to 2006. Hevesi is originally from Queens, New York, and is of Jewish faith.[1][dead link]
 Hevesi was first elected State Comptroller in 2002 and won re-election in 2006.[2] However, he resigned from office effective December 22, 2006, as part of a plea bargain with the Albany County Court, based on his personal use of state employees to care for his ailing wife, in lieu of a grand jury indictment. In February 2007 Hevesi was sentenced to a $5000 fine and permanently banned from holding elective office again; he received no jail time and no probation.[3] He later pled guilty to corruption charges surrounding a "pay to play" scheme regarding the New York State Pension Fund, and was sentenced to 1–4 years on April 15, 2011.[4]
 Alan Hevesi is the son of the late Dr. Eugene Hevesi, a Hungarian-born leader in the American Jewish community who served as foreign affairs secretary for the American Jewish Committee and as representative to the United Nations for several Jewish NGOs.[5] His brother is Dennis Hevesi, a reporter for The New York Times.[6] His grandfather was Simon Hevesi, Chief Rabbi of Budapest prior to World War II.
 Alan Hevesi received his PhD in Public Law & Government from Columbia University in 1971. He earned a B.A. degree in Political Science from Queens College, CUNY. The title of his doctoral dissertation was Legislative Leadership in New York State. His dissertation has been archived by University Microfilms International [1] and the document number is 7201325. [2]
 Hevesi and his wife Carol have been residents of Forest Hills, Queens.[7]
 From 1971 to 1993, Hevesi represented Queens in the New York State Assembly, rising to chair various committees and be considered a potential Assembly Speaker. Simultaneously (from 1967 to 1993), he was an associate professor of political science at Queens College in Flushing, New York.
 Hevesi claims credit for authoring 108 laws as an assemblyman. He is a close ally of New York State Assembly Speaker Sheldon Silver.
 He first unsuccessfully sought the Democratic Nomination for City Comptroller in 1989; he was defeated by Brooklyn District Attorney and former Congresswoman Elizabeth Holtzman. In 1993, he came back to challenge Holtzman in the primary, following ethics accusations against Holtzman. Holtzman had taken a questionable loan from Fleet Bank, which Hevesi assailed her on during an NY1 debate. Hevesi was also supported by Geraldine Ferraro, a former Congresswoman and Vice Presidential running mate of Walter Mondale. Ferraro, upset over Holtzman's 1992 Senate race attack, even went so far as to encourage her candidacy to oppose Holtzman. (Hevesi and Ferraro would later become estranged.) The politically powerful health care union Local 1199, led by Jennifer Cunningham, gave Hevesi its endorsement. Hevesi defeated Holtzman to secure the Democratic nomination. Hevesi then defeated former Congressman Herman Badillo, the Republican candidate in the general election.
 He served two terms as New York City Comptroller from 1994 to 2002, when he was term-limited out of the office.
 In 2001, Hevesi sought the Democratic nomination for Mayor of New York, running on the platform of "Most Experienced, Best Qualified". He finished fourth, behind Public Advocate Mark J. Green, Bronx Borough President Fernando Ferrer, and New York City Council Speaker Peter Vallone. Hevesi was the Liberal Party nominee for Mayor in the general election, but did not campaign in the race; instead, he endorsed Green for mayor in the general election. Following his defeat in the mayor's race, Hevesi started his campaign for state comptroller, which he won, defeating Republican John Faso, 50% to 46%, in 2002.
 As State Comptroller, Hevesi served as the state's chief fiscal officer and as head of the state Department of Audit and Control. In New York, the comptroller signs state checks, handles state bookkeeping, conducts audits of state and local finances, and issues economic forecasts. The comptroller also serves as the sole trustee of the state pension system, an important role in the investment community based on the value of New York State's investment portfolio.
 On December 22, 2006, CNN reported that "[N]ew York State Comptroller Alan Hevesi entered into a plea agreement which included his immediate resignation Friday to avoid a felony indictment by a New York State grand jury charging him with defrauding the government by having staffers drive his wife and assist her in with personal matters from 2003–2006. Hevesi pleaded guilty to Superior Court Filing of defrauding the government, a Class E felony, and will pay a $5,000 fine. Hevesi admitted the wrongdoing when allegations surfaced in November, and has repaid the state more than $200,000."
 Hevesi was re-elected as New York State comptroller in 2006. Had he not resigned, he would have begun his second term of office on January 1, 2007.
 The New York Post in an editorial questioned Hevesi's use of financial advisers as campaign contributors (Alan Hevesi's advisers, November 11, 1996). As City Comptroller he got to choose the city bond underwriters. According to a New York Post editorial, Hevesi hired a female minority owned firm, Philadelphia based P.G. Corbin & Co. Hevesi used discretionary funds in the amount of $1.58 million. Back in 1995, Hevesi's office suspended Corbin for failing to pay $5,900 in city corporate taxes.
 A Hevesi favored firm, San Francisco-based Grigsby Brandford & Co, was involved in a bribery scandal in 1995. Hevesi, who had been seen as a champion of minority causes, now found himself in a difficult position. He had no comment.
 Bayside, New York, activist Joyce Shepard of the Citizens Action Committee for Change tried to highlight the issue of abused women. She contacted Hevesi's office to investigate. Hevesi issued a critical report on the Giuliani administration. On April 17, 1997, she was at New York City Hall with both Giuliani and Hevesi side by side. They promised more space for victims of domestic abuse. Despite all the promises nothing happened. She went undercover to expose the shortcomings of the shelter system. She criticized both Hevesi and Giuliani. After she was quoted by the Village Voice Hevesi reportedly phoned her and told her: "Don't you ever call my office about anything again!". The comptroller's office didn't deny the call took place. But a spokesman stated the office is open to anyone who thinks something is going on we should know about (Mission Unfullfilled, New York Newsday, Janison, D. July 20, 1997).
 As state comptroller, Hevesi faced a conflict of interest allegation in relation to a private capital fund named "Markstone", according to a report in the New York Sun (Hevesi's Advice Stirs Questions On the Coast, Gerstein, J., May 11, 2006). The opening paragraph stated, The New York State comptroller, Alan Hevesi, encouraged California pension managers to invest in a private capital fund founded by a man whose wife has been a generous donor to his political campaigns. The story originally appeared in the Los Angeles Times. Hevesi met with his California counterpart, comptroller Steve Westly, and Elliot Broidy of Markstone Capital Group. They met on May 19, 2003, in order to "pitch" the California Public Employees' Retirement System (CalPERS) to invest in Markstone. Markstone was a fund that invested in Israeli companies. The Sun reported that in June 2003, Mr. Hevesi invested $200 million in Markstone. Mr. Broidy is a major Republican political contributor. Mr. Broidy did not directly contribute to Hevesi's political campaigns, but his wife, Robin Rosenzweig, had contributed $80,000 since 2002 -- $30,000 before the May 2003 meeting and $50,000 afterwards. She also contributed to Andrew Hevesi's state assembly race. On October 7, 2010, Hevesi pleaded guilty to accepting gratuities for steering the investment funds to California venture capitalist Elliot Broidy. Hevesi had accepted $75,000 in trips for himself and his family and $500,000 in campaign contributions, and benefited from $380,000 given to a lobbyist.[8]
 At a commencement address he delivered at Queens College on June 1, 2006, Hevesi told his audience that Senator Charles Schumer was so tough he would "put a bullet between the President's eyes if he could get away with it." Several hours after his remarks, Hevesi apologized for his comments, calling them "beyond dumb," "remarkably stupid," and "incredibly moronic."[9]
 While attending the New York State Association of Counties conference on September 28, 2006, Hevesi had one of his state employed press aides tape record the speech of his opponent, Christopher Callaghan.[10][dead link]
 On October 2, 2006, allegations arose that Hevesi fired receptionist Alexander McHugh, who filed a charge of sexual harassment. Hevesi's office stated that the receptionist did not cooperate with their investigation into the matter and that "...found no evidence of sexual harassment." McHugh filed a complaint with the federal Equal Employment Opportunity Commission.[11][dead link]
 On September 21, 2006, Alan Hevesi admitted that he used Nicholas Acquafredda as a state employee to drive and aid his ailing wife. Hevesi claims that in 2003 the State Ethics Commission decided that he would pay back the entire cost of driving around his wife unless it is for specific safety purposes. A spokesperson from the State Ethics Commission denies such a decision was made.
 On September 26, 2006, Hevesi said he will pay the state more than $82,000 for having a public employee chauffeur his wife, after his Republican challenger, Christopher Callaghan, asked the Albany County District Attorney's office to investigate. Callaghan first phoned in the complaint to the State Comptroller's own State funds-abuse/fraud hotline. Hevesi had admitted the previous week that he had not reimbursed the state. Callaghan, in addition to the Republican party's nominee for the state governor race, John Faso, also called for Hevesi's resignation. The Attorney General (who was seeking the Democratic party's state governor nomination), Eliot Spitzer, withdrew his endorsement of Hevesi.
 The controversy stimulated interest in the candidacies of Callaghan and minor party candidates Julia Willebrand of the Green Party and John Cain of the Libertarian Party.[12][dead link] Hevesi claimed that the drivers were needed to provide security detail to his wife, though a bipartisan ethics panel concluded that the state police found no threat that justified such an arrangement. The panel also concluded that Hevesi had no intention of repaying the state for the driver services until Callaghan publicly filed a complaint.[13]
 On October 12, 2006, Albany County District Attorney David Soares' office acknowledged that it was officially investigating actions by Hevesi regarding the public employee hired to chauffeur his wife.[14][dead link]
On October 23, 2006, the Ethics Commission deemed that Hevesi's actions involving the chauffeur may have violated state law.[15][dead link]
 On November 3, 2006, Hevesi was ordered by the office of state Attorney General Eliot Spitzer to pay the state $90,000 – in addition to the $83,000 he has already paid – in compensation for what has been deemed an improper use of a state employee.[16][dead link] Hevesi said was just a mistake and apologized in a new TV ad, in which he says, “I'm asking you to weigh my mistake against my 35 years of public service, I’m human...I'm a good comptroller who did a dumb thing"[3][dead link]
 On December 12, 2006, Hevesi agreed to a deal that calls for the $90,000 in escrow money to be turned over to the state and for him to pay an additional $33,605 within 10 days, making his payback total $206,000. It was revealed that Hevesi had actually hired four, not the two employees initially thought for his wife's 'security detail' and said employees were running personal errands according to the report from the Attorney General Office.[17][dead link]
 On December 13, 2006, a poll conducted between December 5 and December 11 by Quinnipiac showed that 45% of people in New York felt that Hevesi should resign and 43% felt he had paid his debt to the state. 1,144 registered voters in New York were polled with a margin of error of +/- 2.9 percent.[18]
 On December 14, 2006, the Albany County District Attorney acknowledged that he had a strong enough case to indict Hevesi (see beginning of page for judicial resolution).[19][dead link]
 The scandal came to a close in February, 2007 when, after pleading guilty in December 2006 to a charge of defrauding the government, Hevesi was sentenced by Judge Stephen Herrick in Albany County Court to a $5,000 fine and barred permanently from elected office. As part of the plea deal, he was given no jail time, and received no probation.[3][dead link] Prior to sentencing, Hevesi paid the state more than $200,000 in restitution. Hevesi expressed remorse for his actions and told the judge: "I'm culpable, I'm responsible and I apologize."
 By December 1997, Hevesi as Comptroller of the City of New York enlisted the weight and soundness of his City's finances in the cause of forcing Swiss banks to meet the demands of the World Jewish Congress and other organizations then suing Swiss banks over National socialist-era bank balances the WJC said was owing to the heirs of victims of the Holocaust, joined eventually by both then-Mayor of New York City Rudolph Giuliani and then-Governor of New York State George Pataki. In his book on the subject, Norman Finkelstein dubs him "the godfather of Holocaust restitution sanctions."[20]
 He recruited the financial officers of many other states and municipalities in the US to similarly place the powers and responsibilities entrusted to them by their employers at the service of this cause, at one point calling them to a conference in his own city at which they discussed ways and means of coordinating their individual actions for maximum effect.[21] Sanctions took the forms, variously, of withdrawing balances from (the US branches of) Swiss banks, disinvestment in Swiss banks and their various investment vehicles, disinvestment in Swiss companies, and in companies of other nationalities with operations or markets in Switzerland. Hevesi and a number of his accomplices further undertook to deny regulatory permissions to Swiss banks seeking to expand their activities in their jurisdictions.
 This effort against Switzerland having seemed successful in securing the $1.25 billion (1999) settlement, Hevesi then brought the power of the ad hoc network he had constructed to bear in subsequent actions against Germany, Austria, and other countries,[22] where its use also was deemed successful in raising the amounts of the settlements.
 On October 6, 2009, Raymond Harding, chairman of the Liberal Party of New York, pleaded guilty to charges that he accepted $800,000 from aides to Hevesi when Hevesi was comptroller of the state of New York.[23] In 2005, Harding had secured employment at the Health Insurance Plan of New York for the incumbent State Assemblyman in District 28 (Queens), who then did not run for re-election so that Hevesi's son, Andrew, could run for State Assemblyman. The younger Hevesi ran successfully and took his seat in the Assembly in 2005. Alan Hevesi thanked Harding for his assistance in his son's election.
 Hevesi pled guilty to corruption charges and was sentenced to one to four years on April 15, 2011.[4][24] He spent his first full day in lockup in the prison infirmary according to the New York Post. State correction officials confirmed that Hevesi was incarcerated at the Mid-State Correctional Facility. He was initially sent to a medical unit at the Ulster Correctional Facility where he was issued a green uniform, a short haircut, and assigned inmate number 11-R-1334.[25] Hevesi went before a parole board on November 14, 2012 and was released on parole on December 19, 2012.[26]
 2002 Democratic Primary for state Comptroller
 2002 Race for state Comptroller
 2006 Race for state Comptroller
 1993 NYC Democratic Ticket
 1997 NYC Democratic Ticket
 2001 NYC Liberal Party Ticket
 2002 NYS Democratic Ticket
 2006 NYS Democratic ticket
 1 Background 2 Political career

2.1 State Assembly
2.2 NYC Comptroller
2.3 State Comptroller

 2.1 State Assembly 2.2 NYC Comptroller 2.3 State Comptroller 3 Controversies

3.1 Controversy regarding a chauffeur for Hevesi's wife
3.2 Holocaust restitution sanctions by states and municipalities
3.3 Payoffs to Raymond Harding for political favors
3.4 Felony Conviction and Sentence

 3.1 Controversy regarding a chauffeur for Hevesi's wife 3.2 Holocaust restitution sanctions by states and municipalities 3.3 Payoffs to Raymond Harding for political favors 3.4 Felony Conviction and Sentence 4 Electoral history 5 Election tickets on which Hevesi has appeared 6 Publications

6.1 Books
6.2 Journal articles
6.3 Newspaper articles

 6.1 Books 6.2 Journal articles 6.3 Newspaper articles 7 See also 8 References 9 External links Alan Hevesi, 63% William Mulrow, 37% Alan Hevesi (D), 50% John Faso (R), 46% Alan Hevesi (D), 56.37% Christopher Callaghan (R), 39.45% Julia Willebrand (Green), 2.78% John Cain (LBT), 0.99% Willie Cotton (SWP), 0.40% Mayor: David Dinkins Public Advocate: Mark J. Green Comptroller: Alan Hevesi Mayor: Ruth Messinger Public Advocate: Mark J. Green Comptroller: Alan Hevesi Mayor: Alan Hevesi Public Advocate: Scott Stringer Comptroller: Herbert Berman Governor: Carl McCall Lieutenant Governor: Dennis Mehiel Comptroller: Alan Hevesi Attorney General: Eliot Spitzer Governor: Eliot Spitzer Lieutenant Governor: David Paterson Comptroller: Alan Hevesi Attorney General: Andrew Cuomo U.S. Senate: Hillary Rodham Clinton Marilyn Gittell and Alan G Hevesi (1969). The Politics of Urban Education. Praeger.  Alan G Hevesi (1975). Legislative politics in New York State : a comparative analysis. Praeger. ISBN 0-275-05520-5.  Leon G. Hoffman, Alan G. Hevesi, Paul E. Lynch, Peter J. Gomes, Nancy J. Chodorow, Ralph Roughton, Barney Frank, Susan Vaughan (2001-01-29). "Homophobia: Analysis of a Permissible Prejudice: A Public Forum of the American Psychoanalytic Association and the American Psychoanalytic Foundation". Journal of Gay & Lesbian Psychotherapy 4 (1): 5–53. doi:10.1300/J236v04n01_02. ISSN 08917140. Alan G. Hevesi (Sep 21, 1995). "The Effectiveness Of The NYC Department Of Juvenile Justice's Aftercare Program". Juvenile Justice Digest 23 (18): 1. ISSN 00942413. Alan G. Hevesi (July 8, 1999). "New lead paint law protects landlords, not children". New York Amsterdam News. ISSN 00287121  Alan G. Hevesi (July 9, 1998). "A stand must be taken". USA Today: pp. 11A. ISSN 07347456  Alan G. Hevesi (April 25, 1987). "When Blacks and Jews Pull Together". New York Times: pp. 1.31. ISSN 03624331  New York State Comptroller New York Comptroller election, 2006 ↑ http://web.archive.org/web/20070102022938/http://www.thejewishweek.com/news/newscontent.php3?artid=13460
 ↑ Cardwell, Diane (November 8, 2006). Despite Accusations, Hevesi Is Re-elected New York’s Comptroller. New York Times.
 ↑ 3.0 3.1 The Chauffeurgate Scandal Finally Ends for Alan Hevesi. News 10 – Albany (February 10, 2007). Retrieved on 2011-05-03. 
 ↑ 4.0 4.1 "Former State Comptroller Hevesi To Face Prison Time". NY1 News. April 15, 2011. http://www.ny1.com/content/137441/former-state-comptroller-hevesi-to-face-prison-time. Retrieved April 15, 2011. 
 ↑ "Dr. Eugene Hevesi, 87, A Jewish Leader, Dies". The New York Times. February 17, 2009. http://www.nytimes.com/1983/02/17/obituaries/dr-eugene-hevesi-87-a-jewish-leader-dies.html. Retrieved March 20, 2009. 
 ↑ Navarro, Mireya (July 15, 1993). "A Comptroller Candidate Fights for Recognition". The New York Times. http://www.nytimes.com/1993/07/15/nyregion/a-comptroller-candidate-fights-for-recognition.html?sec=&spon=&pagewanted=all. Retrieved March 20, 2009. 
 ↑ Navarro, Mireya. " A Comptroller Candidate Fights for Recognition", The New York Times, July 15, 1993. Accessed October 8, 2007. "A native New Yorker, Mr. Hevesi lives in Forest Hills with his wife, Carol."
 ↑ http://www.nbcnewyork.com/news/local-beat/The-Man-Who-Betrayed-His-Trust-Hevesi-Update-Sidebar-104512464.html
 ↑ Hevesi Apologizes for Remarks. 1010 WINS (June 2, 2006).
 ↑ Mahoney, Joe (September 29, 2006). Hevesi aide working on campaign. New York Daily News.
 ↑ Smith, Ben (October 2, 2006). Eliot pick a warning for union: Local 1199's clout may be on wane. New York Daily News.
 ↑ Lucadamo, Kathleen (October 30, 2006). Flap-happy day for underdogs. New York Daily News.
 ↑ http://www.auburnpub.com/articles/2006/12/23/news/nation_world/nation01.txt
 ↑ .. New York Newsday (October 2006). 
 ↑ .. New York Newsday (October 2006). 
 ↑ Comptroller Alan Hevesi ordered to pay more. WHEC-TV (November 3, 2006). 
 ↑ Hammond, Bill (December 12, 2006). Drowning in denial. New York Daily News. 
 ↑ .. Quinnipiac University (December 13, 2006). 
 ↑ Lovett, Kenneth (December 14, 2006). A Hev-y Hitter. New York Post. 
 ↑ Bazyler, Michael J. Holocaust Justice. New York University Press, New York, 2003, p. 21.
 ↑ Rickman, Gregg. Swiss Banks and Jewish Souls.  Transaction Publishers, New Brunswick, N. J., 1999, p. 205.
 ↑ Finkelstein, Norman. The Holocaust Industry. Verso, New York, second paperback edition 2003, p. 121
 ↑ "Ex-Political Boss Pleads Guilty in Pension Case" New York Times. October 6, 2009.
 ↑ "Ex-NY state comptroller Alan Hevesi gets prison in pension fund pay-to-play scandal". Wall Street Journal. April 15, 2011. http://online.wsj.com/article/AP418e81b4ed1f4ff9b6ed477f9e6670ac.html. Retrieved April 15, 2011. 
 ↑ Lovett, kenneth (April 17, 2011). "Former Controller Alan Hevesi begins prison stint in an infirmary ward room". New York Daily News. http://www.nydailynews.com/news/politics/2011/04/17/2011-04-17_hevesis_in_prison_infirmary.html. Retrieved April 18, 2011. 
 ↑ Precious, Tom (15 November 2012). Former state comptroller getting out of prison. The Buffalo News. Retrieved on 15 November 2012.
 New York State: Office of the State Comptroller Grout Metz Pendergast Craig Berry McAneny Cunningham McGoldrick Taylor McGoldrick Joseph Gerosa Beame Procaccino Beame Goldin Holtzman Hevesi Thompson Liu Articles in need of neutralization Wikipedia articles needing style editing from December 2007 All articles needing style editing 1940 births Living people Jewish fraudsters American prisoners and detainees Columbia University alumni Jewish American politicians American people of Hungarian-Jewish descent Members of the New York State Assembly New York City Comptrollers New York State Comptrollers People from Forest Hills, Queens 2004 United States presidential electors Queens College, City University of New York alumni American politicians convicted of fraud Prisoners and detainees of New York Pages using duplicate arguments in template calls Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 5 January 2014, at 18:41. Privacy policy About Metapedia Disclaimers 
  tone or style may not reflect the formal tone used on Wikipedia In office In office Alan G. Hevesi Citizens Action Committee for Change 2002 Democratic Primary for state Comptroller 2002 Race for state Comptroller 2006 Race for state Comptroller 1993 NYC Democratic Ticket 1997 NYC Democratic Ticket 2001 NYC Liberal Party Ticket 2002 NYS Democratic Ticket 2006 NYS Democratic ticket 4 23 Emanuel Gold New York State Assembly, 25th District Vincent Nicolosi Alfred A. DelliBovi New York State Assembly, 28th District Melinda Katz Elizabeth Holtzman New York City Comptroller Bill Thompson Carl McCall New York State Comptroller Thomas Sanzillo Acting Rudolph W. Giuliani Liberal Party Nominee for Mayor of New York City Michael R. Bloomberg Authority control 
  This article's tone or style may not reflect the formal tone used on Wikipedia. Specific concerns may be found on the talk page. See Wikipedia's guide to writing better articles for suggestions. (December 2007) Alan Hevesi
 53rd Comptroller of New York
 In officeJanuary 1, 2003 – December 22, 2006
 George Pataki
 Carl McCall
 Thomas DiNapoliThomas Sanzillo (acting)
 41st Comptroller of New York City
 In officeJanuary 1, 1994 – December 31, 2001
 Elizabeth Holtzman
 Bill Thompson
 
 January 31, 1940 (1940-01-31) (age 79)
 Democratic
 Carol Hevesi
 Jewish
 Preceded byEmanuel Gold
 New York State Assembly, 25th District1971–1972
 Succeeded byVincent Nicolosi
 Preceded byAlfred A. DelliBovi
 New York State Assembly, 28th District1973–1993
 Succeeded byMelinda Katz
 Preceded byElizabeth Holtzman
 New York City Comptroller1994–2001
 Succeeded byBill Thompson
 Preceded byCarl McCall
 New York State Comptroller2003–2006
 Succeeded byThomas Sanzillo Acting
 Preceded byRudolph W. Giuliani
 Liberal Party Nominee for Mayor of New York City2001
 Succeeded byMichael R. Bloomberg
 v • d • e
New York State ComptrollersSands* •  Curtenius** •  Jones •  Henry •  Jenkins •  McIntyre •  Savage •  Marcy •  S Wright •  Flagg •  Cooke •  Collier •  Flagg •  Fillmore •  Hunt •  Fuller •  J Wright •  Cook •  Burrows •  Church •  Denniston •  Robinson •  Hillhouse •  Allen •  Nichols •  Hopkins •  Robinson •  Olcott •  Wadsworth •  Davenport •  Chapin •  Wemple •  Campbell •  Roberts •  Morgan •  Gilman •  Knight •  Miller •  Kelsey •  Wilson •  Glynn •  Gaus •  Kelsey*** •  Williams •  Sohmer •  Travis •  Wendell •  Maier •  Fleming •  Murphy •  Tremaine •  Yates*** •  O'Leary •  Moore •  McGovern •  Levitt •  Regan •  McCall •  Hevesi •  Sanzillo*** •  DiNapoli  * as Auditor General, ** as Auditor, *** Acting  Sands* •  Curtenius** •  Jones •  Henry •  Jenkins •  McIntyre •  Savage •  Marcy •  S Wright •  Flagg •  Cooke •  Collier •  Flagg •  Fillmore •  Hunt •  Fuller •  J Wright •  Cook •  Burrows •  Church •  Denniston •  Robinson •  Hillhouse •  Allen •  Nichols •  Hopkins •  Robinson •  Olcott •  Wadsworth •  Davenport •  Chapin •  Wemple •  Campbell •  Roberts •  Morgan •  Gilman •  Knight •  Miller •  Kelsey •  Wilson •  Glynn •  Gaus •  Kelsey*** •  Williams •  Sohmer •  Travis •  Wendell •  Maier •  Fleming •  Murphy •  Tremaine •  Yates*** •  O'Leary •  Moore •  McGovern •  Levitt •  Regan •  McCall •  Hevesi •  Sanzillo*** •  DiNapoli  * as Auditor General, ** as Auditor, *** Acting v • d • e
New York City Comptrollers since the 1898 Consolidation* Coler
Grout
Metz
Pendergast
Craig
Berry
McAneny
Cunningham
McGoldrick
Taylor
McGoldrick
Joseph
Gerosa
Beame
Procaccino
Beame
Goldin
Holtzman
Hevesi
Thompson
Liu  * Coler
Grout
Metz
Pendergast
Craig
Berry
McAneny
Cunningham
McGoldrick
Taylor
McGoldrick
Joseph
Gerosa
Beame
Procaccino
Beame
Goldin
Holtzman
Hevesi
Thompson
Liu  NAME
 Hevesi, Alan
 ALTERNATIVE NAMES
 
 SHORT DESCRIPTION
 American politician
 DATE OF BIRTH
 January 31, 1940
 PLACE OF BIRTH
 
 DATE OF DEATH
 
 PLACE OF DEATH
 
 Alan Hevesi Contents Background Political career Controversies Electoral history Election tickets on which Hevesi has appeared Publications See also References External links Navigation menu State Assembly NYC Comptroller State Comptroller Controversy regarding a chauffeur for Hevesi's wife Holocaust restitution sanctions by states and municipalities Payoffs to Raymond Harding for political favors Felony Conviction and Sentence Books Journal articles Newspaper articles Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 