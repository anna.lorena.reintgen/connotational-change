Plutarco Elías Calles (Spanish pronunciation: [pluˈtarko eˈli.as ˈkaʎes]; 25 September 1877 – 19 October 1945) was a Mexican general and politician. He was president of Mexico from 1924 to 1928, but he continued to be the de facto ruler from 1928–1935, a period known as the maximato. Calles is most noted for a fierce oppression of Catholics that led to the Cristero War, a civil war between Catholic rebels and government forces, and for founding the Partido Nacional Revolucionario (National Revolutionary Party, or PNR), which eventually became the Institutional Revolutionary Party (PRI) – which governed Mexico for more than 70 years.
 Elías Calles grew up in poverty and deprivation, the son of an alcoholic who was not married to his mother. He adopted the Calles surname from the uncle who raised him after the death of his mother, Maria de Jesús Campuzano. This uncle was a devout atheist and raised his nephew with a fanatical hatred of the Catholic Church.[3] His father, also called Plutarco Elías, was, despite his degenerate nature, descended from a prominent family in the Provincias Internas, most often recorded as Elías González. The first of this line to settle in Mexico was Francisco Elías González de Zayas (1704–1790), a Spaniard of distant Sephardic Jewish ancestry.[4] Elías González emigrated from La Rioja, Spain to Alamos, Sonora, Mexico, in 1729, where, as commander of the presidio of Terrenate, he played a role in the wars against the Yaqui and Apache.
 The younger Plutarco worked many different jobs, from bartender to schoolteacher, and always had a keen sense of political opportunity.[5] He was a supporter of Francisco I. Madero, under whom he became a police commissioner, and his ability to align himself with the political winners of the Mexican Revolution (1910–1920) allowed him to move up the ranks quickly; he attained the rank of general in 1915. He led the Constitutional Army in his home state of Sonora and managed to repel the conventionalists of José María Maytorena and Pancho Villa in the Battle of Agua Prieta in 1915.[6]
 In 1915, Elías Calles became governor of Sonora, known as one of the most reformist politicians of his generation. His radical rhetoric tended to conceal the pragmatic essence of his policy, which was to promote the rapid growth of Mexican national capitalism, whose infrastructure he helped to establish. In particular, he attempted to make Sonora a dry state,[6] promoted legislation giving social security and collective bargaining to workers, and expelled all Catholic priests. In 1919, Venustiano Carranza promoted Calles to Secretary of Commerce, Industry and Labor. In 1920 he aligned himself with Álvaro Obregón to overthrow Carranza, and Obregón named him head of the interior ministry.[6] Calles used his ability to draw in labor class votes to come to power with Obregón. He aligned himself with the Laborist Party and was in 1924 elected president, defeating the agrarianist candidate Ángel Flores and the eccentric perennial candidate Nicolás Zúñiga y Miranda.
 Calles' presidency was supported by labor and peasant unions. The Laborist party which supported his government in reality functioned as the political-electoral branch of the powerful Regional Confederation of Mexican Workers (CROM), led by Luis Napoleón Morones. Shortly before his inauguration he had travelled to Europe to study social democracy and the labor movement, and he tried to implement the things he had learned there in Mexico. Calles supported land reforms and promoted the ejido as a way to emancipate campesinos, but no large tracts of land were redistributed under his presdidency nonetheless. Calles founded several banks in support of campesinos as well as the Banco de México, Mexico's national bank. Calles's finance secretary Alberto J. Pani managed to achieve debt relief of a part of Mexico's foreign debt, but after a conflict with Calles, Pani resigned in 1927.
 Calles changed Mexico's civil code to give illegitimate children the same rights as legitimate, partly as a reaction against the problems he himself often had encountered being a child of unmarried parents. According to false rumours,[7] his parents had been Syrians or Turks, giving him the nickname El Turco (The Turk). His detractors drew comparisons between Calles and the "Grand Turk", the barbarian anti-Christian leaders from the era of the Crusades. In order not to draw too much attention to his unhappy childhood, Calles chose to ignore those rumours rather than to fight them.[8][9]
 One of the major points of contention with the U.S. was oil. Calles quickly rejected the Bucareli Agreements of 1923 between the U.S. and Mexico, when Álvaro Obregón was president, and began drafting a new oil law that would strictly enforce article 27 of the Mexican constitution. The oil problem stemmed from article 27 of the Mexican Constitution of 1917, which re-stated a law from Spanish origin that made everything under the soil property of the state. The language of article 27 threatened the oil possession of U.S. and European oil companies, especially if the article was applied retroactively. A Mexican Supreme Court decision had ruled that foreign-owned fields could not be seized as long as they were already in operation before the constitution went into effect. The Bucareli Agreements stated that Mexico would agree to respect the Mexican Supreme Court decision in exchange for official recognition from Washington of the presidency of Álvaro Obregón.[10]
 The reaction of the U.S. government to Calles's intention to enforce article 27 was swift. The American ambassador to Mexico branded Calles a communist, and Secretary of State Frank B. Kellogg issued a threat against Mexico on 12 June 1925.[11] Calles himself never considered himself a communist but considered revolution a way of governing rather than an ideological position. [citation needed] Public opinion in the United States turned particularly anti-Mexican when the first embassy of the Soviet Union in any country was opened in Mexico, on which occasion the Soviet ambassador remarked that "no other two countries show more similarities than the Soviet Union and Mexico".[12] After this, some in the United States government, considering Calles's regime Bolshevik, started to refer to Mexico as "Soviet Mexico".[13]
 The debate on the new oil law occurred in 1925, with U.S. interests opposing all initiatives. By 1926, the new law was enacted. In January 1927 the Mexican government canceled the permits of oil companies that would not comply with the law. Talks of war circulated by the U.S. president and in the editorial pages of the New York Times. Mexico managed to avoid war through a series of diplomatic maneuvers. Soon after, a direct telephone link was established between Calles and President Calvin Coolidge, and the U.S. ambassador to Mexico, James R. Sheffield, was replaced with Dwight Morrow. Morrow successfully won the Calles government over to the United States position and helped negotiate an agreement between the government and the oil companies.[14]
 Another source of conflict with the United States was Mexico's support for the liberals in the civil war in Nicaragua, as the United States supported the conservatives. This conflict ended when both countries signed a treaty in which they allowed each other to support the side they considered to be the most democratic.
 On 14 June 1926, President Calles enacted anticlerical legislation known formally as The Law Reforming the Penal Code and unofficially as the Calles Law.[15] His anti-Catholic actions included outlawing religious orders, depriving the Church of property rights and depriving the clergy of civil liberties, including their right to trial by jury (in cases involving anti-clerical laws) and the right to vote.[15][16] Catholic antipathy towards Calles was enhanced because of his vocal atheism.[1] He was also a Freemason.[17] Regarding this period, recent President Vicente Fox stated, "After 1917, Mexico was led by anti-Catholic Freemasons who tried to evoke the anticlerical spirit of popular indigenous President Benito Juárez of the 1800s. But the military dictators of the 1920s were a lot more savage than Juárez."[18]
 Due to Calles's strict and sometimes violent enforcement of anti-clerical laws, people in strongly Catholic areas, especially the states of Jalisco, Zacatecas, Guanajuato, Colima and Michoacán, began to oppose him, and on 1 January 1927, a war cry went up from the faithful Catholics, "¡Viva Cristo Rey!" Government hostility to Catholicism led to the Cristero War from 1926 to 1929, which was characterized by reprisals and counter-reprisals. Some Cristero groups became little more than terrorists, while the Mexican government violently persecuted the clergy, massacring suspected Cristeros and their supporters. On 28 May 1926, Calles was awarded a medal of merit from the head of Mexico's Scottish rite of Freemasonry for his actions against the Catholics.[19]
 About 90,000 people on both sides died in the war. A truce was negotiated with the assistance of U.S. Ambassador Dwight Morrow in which the Cristeros agreed to lay down their arms.[20] Calles, however, did not abide by the terms of the truce; he had approximately five hundred Cristero leaders and 5,000 other Cristeros shot, frequently in their homes in front of their spouses and children.[20] Particularly offensive to Catholics after the supposed truce was Calles's insistence on a complete state monopoly on education, suppressing all Catholic education and introducing "socialist" education in its place: "We must enter and take possession of the mind of childhood, the mind of youth."[20] The persecution continued as Calles maintained control under his Maximato and did not relent until 1940, when President Manuel Ávila Camacho, a faithful Catholic, took office.[20]
 The effects of Calles's policy on the Church were profound. Between 1926 and 1934 at least 4000 priests were killed or expelled; one of the most famous was the Jesuit Miguel Pro.[20] Where there were 4,500 priests serving the people before the rebellion, in 1934 there were only 334 priests licensed by the government to serve fifteen million people, the rest having been eliminated by emigration, expulsion, and assassination.[20][21] By 1935, seventeen states had no priests at all.[22]
 Under Calles's rule in 1926, a constitutional change was passed that allowed for a non-consecutive reeelection,[23] and in 1928 Obregón was elected as Calles's successor; this amendment was later repealed in 1934.[24] In addition, Mexico passed an amendment to the constitution in 1927 that allowed a President to serve a six-year term.[25] However, Obregón was murdered by José de León Toral, a Catholic militant, before he could assume power. To avoid a political vacuum, Calles named himself Jefe Máximo, the political chieftain of Mexico and Emilio Portes Gil was appointed temporary president, although in reality he was little more than a puppet of Calles. The following year, Calles founded the PNR, or Partido Nacional Revolucionario, the predecessor of today's Partido Revolucionario Institucional (PRI).
 The period which Obregón had been elected to serve between 1928 and 1934, in which Calles was Jefe Máximo, is known as the Maximato in Mexican history, with many regarding Emilio Portes Gil, Pascual Ortiz Rubio, and Abelardo Rodríguez as his puppets. Officially, after 1929, he served as minister of war, as he continued to suppress the rebellion of the Cristero War, but a few months later, after intervention of the United States ambassador Dwight Morrow, the Mexican government and the Cristeros signed a peace treaty. During the Maximato, Calles became increasingly authoritarian and would also serve as Minister of Industry and Commerce.[26] In the early 1930s he appears to have flirted with the idea of implementing aspects of fascism in the government,[27] and the ideology clearly had an influence on him.[28] After a large demonstration in 1930, the Mexican Communist Party was banned, Mexico stopped its support for the rebels of César Sandino in Nicaragua, strikes were no longer tolerated, and the government ceased re-distributing lands amongst poorer peasants. Calles had once been the candidate of the workers and at one point had used Communist unions in his campaign against competing labor organizers but later, having acquired wealth and engaging in finance, suppressed Communism.[29]
 In 1934, Calles selected his old wartime subordinate Lázaro Cárdenas as presidential candidate on the false assumption he could control Cárdenas as he had controlled his predecessors. Soon after his inauguration, however, conflicts between Calles and Cárdenas started to arise. Calles opposed Cárdenas's support for labor unions, especially his tolerance and support for strikes, while Cárdenas opposed Calles's violent methods and his closeness to national organizations, most notably the Gold Shirts of general Nicolás Rodríguez Carrasco, which harassed Communists, Jews and Chinese.[30]
 Cárdenas started to isolate Calles politically, removing the callistas from political posts and exiling his most powerful allies: Tomás Garrido Canabal, Fausto Topete, Emilio Portes Gil, Saturnino Cedillo, Aarón Sáenz and finally Calles himself. Calles and Luis Napoleón Morones, one of the last remaining influential callistas and onetime Minister of Agriculture,[26] were charged with conspiring to blow up a railroad and placed under arrest under the order of President Cárdenas. They were deported to the United States on 9 April 1936
along with the three last highly-influential callistas in Mexico-Morones, Luis León (leader of the Radical Civic Union in Mexico),[31] and General Rafael Melchor Ortega(onetime Governor of Guanajuato)-plus his secretary and his son Alfredo.[26] At the time of his arrest, he was reportedly reading a Spanish translation of Mein Kampf.[32][33]
 In exile in the United States, Calles was in contact with various American nationals, although he rejected their anti-Semitic and anti-Mexican sentiments, and also befriended José Vasconcelos, a Mexican philosopher who had previously been a political enemy. Calles was allowed to return to Mexico under the reconciliation policy of Cárdenas's successor Manuel Ávila Camacho in 1941. He spent his last years quietly in Mexico City and Cuernavaca.
 Back in Mexico, Calles's political position become more moderate; in 1942 he supported Mexico's declaration of war upon the Axis powers. In his last years he reportedly became interested in Spiritualism.[34] A few months before his death in October 1945, aged 68, Calles reportedly stated that he "most certainly believed" in a higher power.[35]
 Calles' main legacy was the pacification of Mexico ending the violent era of the Mexican Revolution through the creation of the Partido National Revolucionario (PNR) which eventually became Partido Revolucionario Institutional (PRI), which governed Mexico until 2000.
 Calles's legacy remains controversial today. He is honored with statues in Sonoyta, Hermosillo, and his hometown of Guaymas. The official name of the municipality of Sonoyta is called General Plutarco Elías Calles in his honor. His starting of the PRN is criticized by many as the beginning of a long undemocratic period in Mexico.
 He was denounced by Pope Pius XI in the encyclical Iniquis Afflictisque (On the Persecution of the Church in Mexico) as being "unjust", for a "hateful" attitude and for the "ferocity" of the war which he waged against the Church.[36]
 Calles will be portrayed by the actor Ruben Blades in the upcoming film Cristiada, an epic historical drama also starring Andy Garcia, Peter O'Toole, Eva Longoria, Alma Martinez, and Eduardo Verástegui.[37]
 1 Early years 2 Presidency

2.1 U.S.-Mexico Relations During Calles's Presidency
2.2 Cristero War
2.3 Aftermath of the Cristero War and toll on the Church

 2.1 U.S.-Mexico Relations During Calles's Presidency 2.2 Cristero War 2.3 Aftermath of the Cristero War and toll on the Church 3 Maximato and Exile 4 Legacies 5 Popular culture 6 References 7 Further reading, viewing 8 External links ↑ 1.0 1.1 David A. Shirk (2005). Mexico's New Politics. Lynne Rienner Publishers. ISBN 1588262707. 
 ↑ Krauze, Enrique. Mexico: Biography of Power. A History of Modern Mexico, 1810–1996. HarperCollins Publishers Inc. New York, 1997. Page 436-437
 ↑ Gonzales, Michael J., The Mexican Revolution, 1910–1940, p. 203, UNM Press, 2002
 ↑ http://eliasofsonora.net/eliaspdf/historicalDataTrans.pdf
 ↑ Gonzales, Michael J. The Mexican Revolution, 1910–1940. University of New Mexico Press. Albuquerque, 2002. Page 203
 ↑ 6.0 6.1 6.2 Stacy, Lee. Mexico and the United States. Marshall Cavendish Corporation. Tarrytown, New York, 2002. Page 124
 ↑ Krauze, Enrique. Mexico: Biography of Power. A History of Modern Mexico, 1810–1996. HarperCollins Publishers Inc. New York, 1997. Page 412
 ↑ Krauze, Enrique. Mexico: Biography of Power. A History of Modern Mexico, 1810–1996. Zeta. Mexico City, 2006. Page 413
 ↑ Medina-Navascues, Tere. Plutarco Elías Campuzano, mal conocido como presidente Calles. HarperCollins Publishers Inc. New York, 1997. Pages 9–11
 ↑ Kirkwood, Burton. The history of Mexico. Greenwood Press, Westport, 2000. pages 157–158
 ↑ Krauze, Enrique. Mexico: Biography of Power. A History of Modern Mexico, 1810–1996. HarperCollins Publishers Inc. New York, 1997. Pages 417
 ↑ Krauze, Enrique, Mexico: biography of power : a history of modern Mexico, 1810–1996, p. 418, Harper Collins 1998
 ↑ Richards, Michael D. Revolutions in World History p. 30 (2004
Routledge) ISBN 0415224977
 ↑ Krauze, Enrique. Mexico: Biography of Power. A History of Modern Mexico, 1810–1996. HarperCollins Publishers Inc. New York, 1997. Pages 417–419
 ↑ 15.0 15.1 Joes, Anthony James Resisting Rebellion: The History And Politics of Counterinsurgency p. 70, (2006 University Press of Kentucky) ISBN 081319170X
 ↑ Tuck, Jim THE CRISTERO REBELLION – PART 1 Mexico Connect 1996
 ↑ Denslow, William R. 10,000 Famous Freemasons p. 171 (2004
Kessinger Publishing)ISBN 1417975784
 ↑ Fox, Vicente and Rob Allyn Revolution of Hope p. 17, Viking, 2007
 ↑ The Cristeros: 20th century Mexico's Catholic uprising, from The Angelus, January 2002 , Volume XXV, Number 1 by Olivier LELIBRE, The Angelus
 ↑ 20.0 20.1 20.2 20.3 20.4 20.5 Van Hove, Brian (1994). Blood-Drenched Altars. Faith & Reason. Eternal Word Television Network.
 ↑ Scheina, Robert L. (2003). Latin America's Wars: The Age of the Caudillo, 1791–1899. Brassey's, 33. ISBN 1574884522. 
 ↑ Ruiz, Ramón Eduardo (1993). Triumphs and Tragedy: A History of the Mexican People. W. W. Norton & Company, 393. ISBN 0393310663. 
 ↑ Mexico: an encyclopedia of contemporary culture and history, Don M. Coerver, Suzanne B. Pasztor, pg. 55
 ↑ http://www.globalsecurity.org/military/world/mexico/president.htm
 ↑ http://www.latin-focus.com/latinfocus/factsheets/mexico/mexfact_history.htm
 ↑ 26.0 26.1 26.2 http://www.time.com/time/magazine/article/0,9171,848503-2,00.html
 ↑ Payne, Stanley (1996). A History of Fascism. Routledge. ISBN 1857285956 p.342
 ↑ Blamires, Cyprian and Paul Jackson, World fascism: a historical encyclopedia, Volume 1, p.148, ABC CLIO 2006
 ↑ Calles, Plutarco Elias Columbia Encyclopedia, Sixth Edition. 2001–05
 ↑ Meyer, Michael C. and William L. Sherman, The Course of Mexican History (5th E. Oxford Univ. Press 1995)
 ↑ http://www.26noticias.com.ar/fallecio-luis-leon-81622.html
 ↑ Krauze, Enrique. Mexico: Biography of Power. A History of Modern Mexico, 1810–1996. HarperCollins Publishers Inc. New York, 1997. Page 436
 ↑ Larralde, Carlos "Roberto Galvan: A Latino Leader of the 1940s". The Journal of San Diego History 52.3/4 (Summer/Fall 2006) p. 160.
 ↑ Larralde, Carlos Roberto Galvan: A Latino Leader of the 1940s
 ↑ Krauze, Enrique. Mexico: Biography of Power,
A History of Modern Mexico, 1810–1996. HarperCollins Publishers Inc. New York, 1997, page 436
 ↑ Iniquis Afflictisque, 12, 15, 19–20
 ↑ Cristiada (2011) IMDB, Accessed Oct. 8, 2010
 Buchenau, Jurgen, Plutarco Elias Calles and the Mexican Revolution, (Denver: Rowman & Littlefield, 2006). Mexico Before the World by Plutarco Elías Calles at archive.org El General, film on P.O.V. on PBS (US) co-presented by Latino Public Broadcasting; July 20, 2010. Filmmaker Natalia Almada works from audio recordings made by her grandmother about Calles, Almada's great-grandfather, relating history to present in Mexico. Lucas, Jeffrey Kent. The Rightward Drift of Mexico's Former Revolutionaries: The Case of Antonio Díaz Soto y Gama. Lewiston, New York: Edwin Mellen Press, 2010. Plutarco Elías Calles at Find a Grave Vicente Guerrero José María Bocanegra Pedro Vélez Anastasio Bustamante Melchor Múzquiz Manuel Gómez Pedraza Valentín Gómez Farías Antonio López de Santa Anna Miguel Barragán José Justo Corro Nicolás Bravo Francisco Javier Echeverría Valentín Canalizo José Joaquín de Herrera Mariano Paredes José Mariano Salas Pedro María de Anaya Manuel de la Peña Mariano Arista Juan Bautista Ceballos Manuel María Lombardini Martín Carrera Rómulo Díaz Juan Álvarez Hurtado Ignacio Comonfort Félix María Zuloaga Manuel Robles Pezuela Miguel Miramón José Ignacio Pavón Benito Juárez Sebastián Lerdo de Tejada José María Iglesias Juan N. Méndez Porfirio Díaz Manuel González Flores Francisco León de la Barra Francisco I. Madero Pedro Lascuráin Victoriano Huerta Francisco S. Carvajal Venustiano Carranza Eulalio Gutiérrez Roque González Garza Francisco Lagos Cházaro Adolfo de la Huerta Álvaro Obregón Plutarco Elías Calles Emilio Portes Gil Pascual Ortiz Rubio Abelardo Luján Rodríguez Lázaro Cárdenas Manuel Ávila Camacho Miguel Alemán Valdés Adolfo Ruiz Cortines Adolfo López Mateos Gustavo Díaz Ordaz Luis Echeverría José López Portillo Miguel de la Madrid Carlos Salinas de Gortari Ernesto Zedillo Vicente Fox Felipe Calderón Haciendas History of Mexico Francisco I. Madero Victoriano Huerta Francisco "Pancho" Villa Venustiano Carranza Emiliano Zapata Álvaro Obregón Pascual Orozco Plutarco Elías Calles Lázaro Cárdenas José Yves Limantour Ramón Corral Francisco León de la Barra Félix Díaz Velasco Bernardo Reyes Eufemio Zapata Manuel Palafox Genovevo de la O Plan of Ayala Plan of Guadalupe Plan of Agua Prieta Plan of San Diego Decena trágica Convention of Aguascalientes Querétaro Constitutional Convention United States involvement
Formations Formations Cristero War Zapatista Army of National Liberation Popular culture Historical Museum Authentic Party of the Mexican Revolution Factions
Liberation Army of the South
Constitutionalists
Felicistas Liberation Army of the South Constitutionalists Felicistas Articles in need of neutralization 1877 births 1945 deaths Anti-Catholicism in Mexico Atheism activists Catholicism and Freemasonry Cristero War Former atheists and agnostics Institutional Revolutionary Party politicians Laborist Party (Mexico) politicians Mexican generals Mexican people of Spanish descent Mexican presidential candidates (1924) Mexican Secretaries of Defense Mexican Secretaries of Economy Mexican Secretaries of Education Mexican Secretaries of Finance Spiritualists Mexican Secretaries of the Interior People from Guaymas People of the Mexican Revolution Presidents of Mexico Pages using duplicate arguments in template calls Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 10 April 2012, at 20:47. Privacy policy About Metapedia Disclaimers 
  In office Plutarco Elías Calles Plutarco Elías Calles Álvaro Obregón President of Mexico Emilio Portes Gil Chauncey M. Depew Cover of Time Magazine Dwight F. Davis Vice President of Mexico Authority control Main article: Cristero War Main article: Cristero War Plutarco Elías Calles
 
 In office1 December 1924 – 30 November 1928
 Álvaro Obregón
 Emilio Portes Gil
 
 25 September 1877(1877-09-25)Guaymas, Sonora
 19 October 1945 (aged 68)Mexico City
 Mexican
 Laborist Party (PL), after 1929 National Revolutionary Party (PNR)
 Natalia Chacón
 Atheist,[1] Spiritualist (later in life)[2]
   Wikimedia Commons has media related to: Plutarco Elías Calles  Preceded byÁlvaro Obregón
 President of Mexico1 December 1924 – 30 November 1928
 Succeeded byEmilio Portes Gil
 Preceded byChauncey M. Depew
 Cover of Time Magazine8 December 1924
 Succeeded byDwight F. Davis
 v • d • e
Presidents of the United Mexican States*Guadalupe Victoria
Vicente Guerrero
José María Bocanegra
Pedro Vélez
Anastasio Bustamante
Melchor Múzquiz
Manuel Gómez Pedraza
Valentín Gómez Farías
Antonio López de Santa Anna
Miguel Barragán
José Justo Corro
Nicolás Bravo
Francisco Javier Echeverría
Valentín Canalizo
José Joaquín de Herrera
Mariano Paredes
José Mariano Salas
Pedro María de Anaya
Manuel de la Peña
Mariano Arista
Juan Bautista Ceballos
Manuel María Lombardini
Martín Carrera
Rómulo Díaz
Juan Álvarez Hurtado
Ignacio Comonfort
Félix María Zuloaga
Manuel Robles Pezuela
Miguel Miramón
José Ignacio Pavón
Benito Juárez
Sebastián Lerdo de Tejada
José María Iglesias
Juan N. Méndez
Porfirio Díaz
Manuel González Flores
Francisco León de la Barra
Francisco I. Madero
Pedro Lascuráin
Victoriano Huerta
Francisco S. Carvajal
Venustiano Carranza
Eulalio Gutiérrez
Roque González Garza
Francisco Lagos Cházaro
Adolfo de la Huerta
Álvaro Obregón
Plutarco Elías Calles
Emilio Portes Gil
Pascual Ortiz Rubio
Abelardo Luján Rodríguez
Lázaro Cárdenas
Manuel Ávila Camacho
Miguel Alemán Valdés
Adolfo Ruiz Cortines
Adolfo López Mateos
Gustavo Díaz Ordaz
Luis Echeverría
José López Portillo
Miguel de la Madrid
Carlos Salinas de Gortari
Ernesto Zedillo
Vicente Fox
Felipe CalderónVice President of Mexico  *Guadalupe Victoria
Vicente Guerrero
José María Bocanegra
Pedro Vélez
Anastasio Bustamante
Melchor Múzquiz
Manuel Gómez Pedraza
Valentín Gómez Farías
Antonio López de Santa Anna
Miguel Barragán
José Justo Corro
Nicolás Bravo
Francisco Javier Echeverría
Valentín Canalizo
José Joaquín de Herrera
Mariano Paredes
José Mariano Salas
Pedro María de Anaya
Manuel de la Peña
Mariano Arista
Juan Bautista Ceballos
Manuel María Lombardini
Martín Carrera
Rómulo Díaz
Juan Álvarez Hurtado
Ignacio Comonfort
Félix María Zuloaga
Manuel Robles Pezuela
Miguel Miramón
José Ignacio Pavón
Benito Juárez
Sebastián Lerdo de Tejada
José María Iglesias
Juan N. Méndez
Porfirio Díaz
Manuel González Flores
Francisco León de la Barra
Francisco I. Madero
Pedro Lascuráin
Victoriano Huerta
Francisco S. Carvajal
Venustiano Carranza
Eulalio Gutiérrez
Roque González Garza
Francisco Lagos Cházaro
Adolfo de la Huerta
Álvaro Obregón
Plutarco Elías Calles
Emilio Portes Gil
Pascual Ortiz Rubio
Abelardo Luján Rodríguez
Lázaro Cárdenas
Manuel Ávila Camacho
Miguel Alemán Valdés
Adolfo Ruiz Cortines
Adolfo López Mateos
Gustavo Díaz Ordaz
Luis Echeverría
José López Portillo
Miguel de la Madrid
Carlos Salinas de Gortari
Ernesto Zedillo
Vicente Fox
Felipe Calderón   Vice President of Mexico v • d • e
Mexican RevolutionBackground* Porfiriato
Haciendas
History of MexicoImportant people* Porfirio Díaz
Francisco I. Madero
Victoriano Huerta
Francisco "Pancho" Villa
Venustiano Carranza
Emiliano Zapata
Álvaro Obregón
Pascual Orozco
Plutarco Elías Calles
Lázaro Cárdenas
José Yves Limantour
Ramón Corral
Francisco León de la Barra
Félix Díaz Velasco
Bernardo Reyes
Eufemio Zapata
Manuel Palafox
Genovevo de la OPlans* Plan of San Luis Potosí
Plan of Ayala
Plan of Guadalupe
Plan of Agua Prieta
Plan of San DiegoPolitical developments* Treaty of Ciudad Juárez
Decena trágica
Convention of Aguascalientes
Querétaro Constitutional Convention
United States involvement
FormationsLegacy* PRI
Cristero War
Zapatista Army of National Liberation
Popular culture
Historical Museum
Authentic Party of the Mexican RevolutionOther* Timeline
Factions
Liberation Army of the South
Constitutionalists
Felicistas  Background * Porfiriato
Haciendas
History of Mexico  Important people * Porfirio Díaz
Francisco I. Madero
Victoriano Huerta
Francisco "Pancho" Villa
Venustiano Carranza
Emiliano Zapata
Álvaro Obregón
Pascual Orozco
Plutarco Elías Calles
Lázaro Cárdenas
José Yves Limantour
Ramón Corral
Francisco León de la Barra
Félix Díaz Velasco
Bernardo Reyes
Eufemio Zapata
Manuel Palafox
Genovevo de la O  Plans * Plan of San Luis Potosí
Plan of Ayala
Plan of Guadalupe
Plan of Agua Prieta
Plan of San Diego  Political developments * Treaty of Ciudad Juárez
Decena trágica
Convention of Aguascalientes
Querétaro Constitutional Convention
United States involvement
Formations  Legacy * PRI
Cristero War
Zapatista Army of National Liberation
Popular culture
Historical Museum
Authentic Party of the Mexican Revolution  Other * Timeline
Factions
Liberation Army of the South
Constitutionalists
Felicistas NAME
 Calles, Plutarco Elias
 ALTERNATIVE NAMES
 Calles, Plutarco Elías (Spanish)
 SHORT DESCRIPTION
 President of Mexico (1924–1928)
 DATE OF BIRTH
 1877-09-25
 PLACE OF BIRTH
 Guaymas, Sonora, Mexico
 DATE OF DEATH
 1945-10-19
 PLACE OF DEATH
 Mexico City, Mexico
 Plutarco Elías Calles Contents Early years Presidency Maximato and Exile Legacies Popular culture References Further reading, viewing External links Navigation menu U.S.-Mexico Relations During Calles's Presidency Cristero War Aftermath of the Cristero War and toll on the Church Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 