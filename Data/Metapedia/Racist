Racism is a term usually only used by critics. Official definitions of racism often state that the term should only be applied to the belief that some races are superior and to negative actions due to this. In practice, the term is often applied as a form of ad hominem on anyone who believes in the existence of races and even on persons only advocating restricting immigration, persons only criticizing another culture or multiculturalism, persons only supporting their own country/ethnicity, and so on.
 The Oxford English Dictionary states that the first English use of the word racism occurred in 1902. The word at this early time did not necessarily have the same meaning or frequent usage as today. Starting the in the 1920s, it was used as a derogatory and misleading translation of the "untranslatable" German term völkische. The German language, especially at this time, makes a distinction between Völk ("People") and Rasse ("Race"). The word was later applied to claimed National Socialism racial ideology and policies. The 1938 English translation of the book Racism, by the Jewish sexologist Magnus Hirschfeld (who was also an early proponent of the sexual revolution), used the word in a sense similar to the modern one and may have contributed to this usage of the word becoming widespread.[1][2][3][4]
 The term racism "replaced earlier words, racialism (1871) and racialist (1917), both often used early 20c. in a British or South African context. In the U.S., race hatred, race prejudice had been used, and, especially in 19c. political contexts, negrophobia."[5] Today, the term racialism instead has a meaning similar to race realism.
 Individuals/organizations supporting the existence of races may use terms such as race realism or racialism.
 Individuals/organizations supporting their own group may use terms such as patriotism, nationalism (and derivatives such as national socialism), identitarianism, pro-[name of own group], etc. This does not necessarily mean considering this group to be a race. On the contrary, officially sanctioned patriotism in multiracial countries typically explicitly rejects links to race. Neither does it necessarily mean considering this group to be superior to other groups, anymore than an individual supporting the individual's own family necessarily considers this family to be superior to other families.
 It is possible to advocate for racial segregation despite not seeing any group as superior and instead argue that racial segregation would be beneficial for all groups, due to factors such as problems caused ethnic heterogeneity and mixed race groups having problems due to genetic outbreeding depression and psychological issues caused by identity issues. Regardless, racial segregation is often viewed as a form of racism.
 Descriptions of argued racism tend to be completely one-sided where the argued racist offender is depicted as acting out of innate evilness and unexplained spite and the argued victim is depicted as completely blameless with no responsibility for the events. In reality, the circumstances, interactions, and/or motives of the two sides may be more complex. For example, some cases of anti-Semitism have been argued to have developed as responses to intense resource competition between Jews and non-Jews (see the Jewish group evolutionary strategy).
 Pro-White or pro-European organizations are often more or less automatically accused of racism and they have little influence. They are often given labels such as "White supremacist and/or "hate group".
 In contrast, applying a double standard, accusations of racism are much rarer against, for example, pro-Black or pro-Jewish organizations, which openly and extensively lobby for the self-interests of their own group.
 Laws regarding affirmative action, hate crime, and hate speech give special legal privileges to certain groups, but not to other groups, creating legal inequalities, and with which groups are given these special legal privileges varying greatly between different countries. Such laws may be considered to apply a double standard, be discriminatory, and/or be racist.
 See also the articles on these topics.
 See the Anti-racism article.
 There are several kinds of empirical evidence against "colorism" (skin color differences as the cause of group differences due to factors such as racism). For example, darker skin color not being associated with more negative outcomes after controlling for IQ differences.[6]
 The World Value Survey has asked people in more than 80 different countries to identify the kinds of people they would not want as neighbors. Some respondents, picking from a list, chose "people of a different race." The frequency of picking this has been argued to be an indicator of the degree of racism in a country. White countries were generally among the least racist according to this measurement.[7]
 White countries had the most positive views on homosexuality in a 2013 survey.[8]
 A 2013 US survey found that both Blacks and Whites thought that Blacks were the most racist group.[9]
 A 2011 study found that both White and Blacks in the US agreed that anti-Black racism had decreased over the last 60 years. However, Whites believed that anti-White racism had increased and had become a bigger problem than anti-Black racism. Also Blacks believed that anti-White racism had increased.[10]
 A 2012 survey in France found 56% thought that anti-White racism was increasing in areas in the large cities.[11]
 A 2012 study found that almost one in three (29%) of Whites in Britain said they now expected to be treated worse than other races by key public services, including the police, prisons, courts, Crown Prosecution service, probation service, local housing organizations, schools or GPs. Council housing departments or housing associations were identified as the most likely to discriminate against Whites. Increasing numbers of Whites also stated they had failed to win promotion because of their race and been turned down for a job for the same reason. Whites were also more likely than those from ethnic minorities to believe that racial prejudice and discrimination was getting worse and such views were increasing in prevalence. Whites also felt less able than other ethnic groups to influence decisions affecting their local area and the country as a whole.[12]
 A 2017 survey found that 55 percent of Whites stated that there is discrimination against Whites in America today. Lower- and moderate-income White Americans were more likely to state that Whites are discriminated against — and to state that they have felt it, either when applying for a job, raise or promotion or in the college-admissions process.[13]
 A 2018 study stated that "This study reports results from a new analysis of 17 survey experiment studies that permitted assessment of racial discrimination, drawn from the archives of the Time-sharing Experiments for the Social Sciences. For White participants (n=10 435), pooled results did not detect a net discrimination for or against White targets, but, for Black participants (n=2781), pooled results indicated the presence of a small-to-moderate net discrimination in favor of Black targets [...] given that some of the studies have never been fully reported on in a journal or academic book, the results also suggest the need for preregistration to reduce or eliminate publication bias in racial discrimination studies."[14]
 In some African countries, albinos are extensively persecuted due to their white skin color.
 See the Hate crime article regarding statistics showing Whites having a disproportionate risk of being the victims of hate and interracial crimes.
 Also, as noted there, some hate crimes are fabricated. This likely also applies to accusations of racism in general.
 See Kill the Boer, South African farm attacks, South Africa and Zimbabwe.
 Racism has been argued to be an important aspect of topics such as slavery, colonialism, eugenics, social Darwinism, subhumans, and master race. There are various less politically correct views on these topics and their argued associations with racism. See the articles on these topics.
 Prejudice has sometimes been claimed to be associated with low IQ, such as in an often cited 2012 study.[15] The study has been criticized for a variety of different reasons and has been described as "a textbook example of confused data, unrecognized bias, and ignorance of statistics" and "competition for the worst use of statistics in an original paper."[16]
 A 2016 study argued that smart people are not less racist, but simply better at concealing this.[17]
 A 2006 study found that the greater the education of White parents, the more likely they will remove their children from public schools as the percentage of Black students increases. More-educated Whites also lived in more White neighborhoods than less-educated Whites.[18]
 See also Political correctness: IQ and political correctness.
 "Scientific racism" is supposed to be "the use of scientific techniques and hypotheses to support or justify the belief in racism, racial inferiority, or racial superiority, or alternatively the practice of classifying individuals of different phenotypes into discrete races".
 The term can be seen as a form of ad hominem against politically incorrect group statistics or politically incorrect race research results which are often simply branded as "racism" rather than being answered with factual arguments.
 It has sometimes been argued that race and racism did not exist before scientists started studying race. Thus, they would be recent social constructs. Regarding the early history of the concept of race, see the race article. Regarding racism, this has been argued to have existed in numerous early cultures and long before scientists started studying race. Some early examples of views and actions that today would be considered racism can be found in the Jewish Bible and in the Indian Vedas (which according the Aryan invasion theory describe the conquest of Northern India by a race with a lighter skin color and which caused the development of the partially racially based Indian caste system).[19][20][21]
 The religiously based "curse of Ham", used as a justification for slavery of and/or racism against Blacks, existed in Jewish, Christian, and Muslim sources long before any scientific study of race.
 One argument against racism existing in Ancient Greece/Ancient Rome is the absence of Greek/Latin words corresponding to negative English words such as "racism" and "prejudice". This has however been explained as there simply being no opposition in antiquity to well-documented "racist" views, such that different races have different physical and mental characteristics or that one important explanation for the argued positive characteristics and historical successes of the Greeks, the Romans, and the Germans was that these groups had previously avoided race mixing.[21]
 As discussed in the Race article, the predominant stated view of early scientific race taxonomists was that the observed race differences were completely due to environmental differences.[22] This makes it more difficult to claim that such race classification in itself would have caused the creation of racism.
 The introduction of the scientific study of race, race differences being viewed as partially caused by hereditary differences, and the discovery of natural selection and evolution were not accompanied by an increase in slavery. On the contrary, they occurred at the same time as Whites abolished slavery throughout the whole world.
 Also regarding racial theories and justification of slavery, European authorities rejected both "pre-Adamite" Amerindian theories and polygenesis racial theories, instead supporting common creation and descent, the worst alternative if trying to find a justification for slavery.[23]
 Regarding why racism exists and existed long before any race research, theories related to racial genetic interests provide possible genetic explanations for racism or at least for why individuals typically consider their own ethnicity/race to be more important than other ethnicities/races. An implication of the related genetic similarity theory is that humans will feel closer and have more altruism towards genetically similar persons, regardless of if races exist or not.
 See also Racial genetic interests: Race and perception on various kinds of research on differences regarding perception, empathy, and preference for other individuals, depending on if they are of the same race or not, even in infants.
 Race research is in itself a describing science and not a political ideology advocating a particular kind of desirable society. This also applies to explanations for race differences using arguments from evolutionary psychology and sociobiology. If researchers argue that certain evolutionary processes have caused race differences, then this does not imply that such processes are necessarily desirable or necessarily advocated by race researchers as models for how human society should be. For example, that certain infectious diseases have been argued to have caused certain race differences does not imply that these infectious diseases are seen as desirable or advocated.
 Race research results can be politically interpreted in various ways. Thus, research stating average IQ differences does not imply that the higher IQ groups have a moral right to exploit the lower IQ groups, but may be interpreted, for example, as the higher IQ groups having a moral obligation to help the lower IQ groups or be interpreted as not implying anything morally. Taking a non-race example, being very wealthy gives power and influence, but is usually not seen as implying a moral right to use this in order to exploit poor individuals.
 It has furthermore been disputed that a lower achieving group gains from denying or concealing real biological differences. An increasingly complex society built on the assumption that everyone can do equally well means that they who do not have this ability have increasing trouble functioning in most areas of life. They need various forms of special assistance which is not possible as long as the need is denied to exist.[24][25]
 Instead of lessening groups conflicts, it has been argued that denying real biological differences may cause people to seek something to blame, causing hostility between groups. In the US, examples being the views that Whites are racist or Blacks are lazy.[24] Very large groups conflicts and group killings occurred in the Communist states which denied genetic explanations for group differences and instead blamed class exploitation.
 Other possible negative effects for individuals and societies from censoring/ignoring race research due to fear of racism include being unaware of research on, for example, ethnic heterogeneity and outbreeding depression.
 Several IQ researchers have expressed very pessimistic views regarding the future of Western civilization, in part due to political correctness regarding race differences. See Dysgenics: Pessimism regarding the future of Western civilization.
 Regarding race and intelligence specifically, there are also many other arguments against equating a partially genetic explanation with racism, see the linked article.[26]
 1 Etymology 2 Terms used by individuals/organizations in order to describe themselves 3 Two sides 4 Double standard 5 Affirmative action, hate crime, hate speech laws 6 Organized "anti-racism" 7 Colorism 8 Racist countries and groups 9 Persecutions of albinos 10 Hate crime 11 South Africa and Zimbabwe 12 Slavery, colonialism, eugenics, social Darwinism, subhumans, and master race 13 IQ and "prejudice" 14 "Scientific racism" 15 Race research and the origin of racism 16 See also

16.1 Supremacism

 16.1 Supremacism 17 External links 18 References Hate group Human biodiversity White guilt White privilege Supremacism
Black supremacism
Islamist supremacism
Jewish supremacism
Leftist supremacism
White supremacism Black supremacism Islamist supremacism Jewish supremacism Leftist supremacism White supremacism The Fallacy of Equating the Hereditarian Hypothesis with Racism Neo-Racism vs. Palaeoracism ↑ "racism". OED Online. Oxford University Press.
 ↑ On the origin of the word Racist. 29 October 2012. National-Socialist Worldview. http://national-socialist-worldview.blogspot.fr/2012/10/on-unclear-origin-of-word-racist.html
 ↑ Peter Frost. Birth of a word. May 23, 2015. Evo and Proud. http://evoandproud.blogspot.de/2015/05/birth-of-word.html
 ↑ Samuel Francis. "Racism" The Curious Beginnings of a Useless Word. http://www.heretical.com/miscella/racism.html
 ↑ Racist. Etymology Online. http://www.etymonline.com/index.php?allowed_in_frame=0&search=racialism
 ↑ Category: Colorism. Human Varieties. http://humanvarieties.org/category/black-white-iq-gap/colorism/
 ↑ A fascinating map of the world’s most and least racially tolerant countries. May 15, 2013. Washington Post. http://www.washingtonpost.com/blogs/worldviews/wp/2013/05/15/a-fascinating-map-of-the-worlds-most-and-least-racially-tolerant-countries
 ↑ The Global Divide on Homosexuality. June 4, 2013. Pew Research. http://www.pewglobal.org/2013/06/04/the-global-divide-on-homosexuality/
 ↑ More Americans View Blacks As Racist Than Whites, Hispanics. Wednesday, July 03, 2013. Rasmussen Reports. http://www.rasmussenreports.com/public_content/lifestyle/general_lifestyle/july_2013/more_americans_view_blacks_as_racist_than_whites_hispanics
 ↑ Whites Believe They Are Victims of Racism More Often Than Blacks. May 23, 2011. TuftsNow. http://now.tufts.edu/news-releases/whites-believe-they-are-victims-racism-more-o
 ↑ Pour 56 % des Français le "racisme anti-Blancs" se développe. Le Monde 28.09.2012 à 15h2. http://www.lemonde.fr/societe/article/2012/09/28/pour-56-des-francais-le-racisme-anti-blancs-se-developpe_1767478_3224.html
 ↑ One third of whites claim they are victims of racism. Daily Mail. http://www.dailymail.co.uk/news/article-1043717/One-whites-claim-victims-racism.html
 ↑ Majority Of White Americans Say They Believe Whites Face Discrimination http://www.npr.org/2017/10/24/559604836/majority-of-white-americans-think-theyre-discriminated-against?
 ↑ Black and White Discrimination in the U.S.: Evidence from an Archive of Survey Experiment Studies http://journals.sagepub.com/doi/full/10.1177/2053168017753862
 ↑ Hodson, G., & Busseri, M. A. (2012). Bright minds and dark attitudes lower cognitive ability predicts greater prejudice through right-wing ideology and low intergroup contact. Psychological Science, 23(2), 187-195. http://pss.sagepub.com/content/23/2/187.short 
 ↑ William M Briggs. Low IQ & Liberal Beliefs Linked To Poor Research? Posted on 27 January 2012. http://wmbriggs.com/blog/?p=5118
 ↑ Wodtke, G. T. (2016). Are Smart People Less Racist? Verbal Ability, Anti-Black Prejudice, and the Principle-Policy Paradox. Social Problems, 63(1), 21-45. http://socpro.oxfordjournals.org/content/63/1/21.abstract
 ↑ Surprise, Surprise! Highly educated white liberals are hypocrites https://isteve.blogspot.co.uk/2006/12/surprise-surprise-highly-educated.html
 ↑ Vincent Sarich och Frank Miele. Race: The Reality of Human Differences. 2004. Westview Press.
 ↑ J. Philippe Rushton. Race, Evolution, and Behavior: A Life History Perspective. 1997. Transaction Publishers.
 ↑ 21.0 21.1 Benjamin Isaac. Collective Degradation: Slavery and the Construction of Race. Proceedings of the Fifth Annual Gilder Lehrman Center International Conference at Yale University. November 7-8, 2003. http://www.yale.edu/glc/events/race/Isaac.pdf
 ↑ John Fuerst. (2015). "The Nature of Race: the Genealogy of the Concept and the Biological Construct’s Contemporaneous Utility". Submitted: December 25, 2014. Published: June 18, 2015. Open Behavioral Genetics. http://openpsych.net/OBG/2015/06/the-nature-of-race/
 ↑ Science Strikes Back https://www.amren.com/news/2018/07/race-reality-of-human-differences-sarich-miele/
 ↑ 24.0 24.1 What if the Hereditarian Hypothesis Is True? Linda S. Gottfredson, Psychology, Public Policy, and Law Volume 11, Issue 2, June 2005, Pages 311-319
 ↑ [http://www.cato-unbound.org/2007/11/26/linda-s-gottfredson/flynn-ceci-and-turkheimer-on-race-and-intelligence-opening-moves/ FLYNN, CECI, AND TURKHEIMER ON RACE AND INTELLIGENCE: OPENING MOVES
 ↑ The Fallacy of Equating the Hereditarian Hypothesis with Racism https://www.mdpi.com/2624-8611/1/1/18/htm
 Content from Wikipedia Race Racism Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information Español Svenska  This page was last edited on 28 November 2019, at 16:12. Privacy policy About Metapedia Disclaimers 
  Ethnically  divided societies White guilt Racism Affirmative action
 Castes
 Civic nationalism
 Contact hypothesis
 Diversity
 Dysgenics
 Effects of race mixing
 Ethnic heterogeneity
 Hate crime
 Islamization and anti-Islamization
 Magic dirt
 Melting pot
 Migration
 Multiculturalism
 No-go area
 Political spectrum
 Racial genetic interests
 Racism
 Segregation
 White demographics
 White flight
 Afrocentrism
 Amerindians
 Colonialism
 Confederate revisionism
 Cultural Marxism
 Gypsies
 Hate crime
 Human Accomplishment
 Lynching
 Noble savage
 Pathological altruism‎
 Political correctness‎
 Racism
 Refugees
 Reparation
 Slavery
 Stereotypes
 Telescopic philanthropy
 The Holocaust
 Virtue signalling
 Whiteness studies
 White guilt
 White privilege
 Racism Contents Etymology Terms used by individuals/organizations in order to describe themselves Two sides Double standard Affirmative action, hate crime, hate speech laws Organized "anti-racism" Colorism Racist countries and groups Persecutions of albinos Hate crime South Africa and Zimbabwe Slavery, colonialism, eugenics, social Darwinism, subhumans, and master race IQ and "prejudice" "Scientific racism" Race research and the origin of racism See also External links References Navigation menu Supremacism Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools In other languages 