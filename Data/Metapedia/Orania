Orania is a South African town located along the Orange River in the arid Karoo region of its Northern Cape province. Orania is unique in that the entire population is made up of Afrikaners. The aim of the town is to create a stronghold for Afrikaans and the Afrikaner identity by keeping their language and culture alive. Anyone who defines themselves as an Afrikaner and identifies with Afrikaner ethnicity is welcome in Orania.[2]
 In December 1990, about 40 Afrikaner families, headed by Carel Boshoff, the son-in-law of former South African prime minister Hendrik Verwoerd, bought the dilapidated town, which was a construction camp of builders of the Vanderkloof Dam on Gariep River for around US$ 200,000.[3] This was a few months after the repeal of apartheid laws and the release of Nelson Mandela from prison. The town is privately owned by the Vluytjeskraal Aandeleblok company. The farm on which Orania was founded, is called "Vluytjekraal". Along the Orange river grows a fine reed, called "fluitjiesriet" or in old Dutch spelling "Vluytjesriet," meaning whistle reed. A kraal is an Afrikaans word borrowed from Portuguese[4] for a cattle enclosure. As wood for poles is scarce, these reeds were traditionally used for building cattle enclosures, until stone structures could be erected., Aandeleblok refers to the company structure that allows people to buy shares and thereby obtain the right to stay on and work a piece of ground within the property of the company. The shareholders thus own the company, which in turn owns the property. The town was bought from the Department of Water Affairs, which built the town for the workers building a canal network utilising the water in the Orange River, when the project was completed.
 According to its founders, the purpose of Orania is to create a town where the preservation of Afrikanerdom's cultural heritage is strictly observed and Afrikaner selfwerksaamheid ("self reliance") is an actual practice, not just an idea. All jobs, from management to manual labour, are filled by Afrikaners only; non-Afrikaner workers are not permitted. "We do not want to be governed by people who are not Afrikaners", said Potgieter, the previous chairman. "Our culture is being oppressed and our children are being brainwashed to speak English".[5]
 The flag comprises a small boy, pulling up his sleeves, and with a blue and orange background, traditional Afrikaner colours.
 The idea of a strictly Afrikaner settlement in modern South Africa is not new. In the 1980s, a group of right-wing Afrikaners, led by HF Verwoerd's son formed a group called the Oranjewerkers. They also planned a community based on "Afrikaner self-determination", and attempted to create a neo-"boerstaat" (lit. "Farmer State," a reference to an idiomatic term for an Afrikaner-only state) in the remote Eastern Transvaal (now Mpumalanga) community of Morgenzon.[6] Its failure was possibly due to the fact that South Africa had an Afrikaner government at the time and so few perceived any benefit from this community.
 In November 2005, around 60 Cape Coloured families lodged a land claim with the government, for around 483 hectares of land within the town. These families claimed to have lived in the town from 1965, when it was first constructed, up until 1991, when it was sold by the government. The claimants hold that they were forced to leave in 1991, when the town was converted into Orania, and that this constitutes a forced removal in terms of race. The community of Orania opposed the claim.[7] The land claim was settled in December 2006 when the South African government agreed to pay the claimants R2,9 million in compensation.[8]
 As of 2010[update], Orania is home to an unknown number of inhabitants, with around 10,000 registered supporters (2010).[9] Orania has three residential areas Kleingeluk ("little luck"), Grootdorp ("big town") and Orania Wes. In Orania people from all levels of society perform their own manual labour.[5]
 Since purchasing the 430ha town, the community has added 2 500ha of agricultural land to the town.[10]
 The people of Orania believe in protecting the environment.
 There are two schools, the CVO Skool Orania (where CVO stands for Christelike Volks-Onderwys or Christian People's-Education) and Die Volkskool Orania (Orania People's School). Although the official curriculum is followed, special emphasis is placed on Afrikaner history and Christian religion, though with some differences in their teaching methods. [12]
 Farming is an important part of Orania's economy, the most recent project being a massive pecan nut plantation.[15]
 During April 2004, Orania launched its own monetary system, called the Ora, based on the idea of discount shopping vouchers.[16] The Orania local banking institution, the Orania Spaar- en Kredietkoöperatief ("Orania Savings and Credit Co-operative") is in charge of this initiative. Orania recently launched its own chequebook.[17]
 Orania's tourism industry is showing rapid development with the completion of a luxury river spa and boutique hotel complex [18] in 2009. Orania Toere (Orania Tours), Orania's first registered tour operator, was also launched in 2009.
 In 2010 thirteen (13) [19] independent hospitality businesses operate in Orania. This includes, caravan park, self-catering flats, rooms, hotel and guest-houses.
 In 2005, after complaints by citizens, the Independent Communications Authority of South Africa shut down Orania's unlicensed Radio Club 100 radio station, on grounds of its alleged racism.[20] The station's equipment was seized. No criminal charges were laid against the operators of the radio station, who claim that the radio station broadcasted harmless news about birthdays and social events. Management of the radio station claimed that they repeatedly applied for a radio license in order to be a community broadcaster like other radio stations in the country.
 After being granted a license toward the end of 2007 Orania radio re-started broadcasting on the 13th of April 2008 on 95.5 MHz.[21]
 In January 2010, Afrikaans daily newspaper Beeld published an article by Frans de Klerk, chief executive of Orania, in which he sets out what he views as the successes of Orania.[22] De Klerk also distanced the town from racist organizations using Orania to further their own causes. Shortly after, Die Burger, another Afrikaans daily newspaper and sister publication of Beeld, published an article by author and journalist Marida Fitzpatrick, describing her experiences when visiting Orania.[23] Fitzpatrick praised the town for its safety and environmentally friendly approaches to living, but also wrote that overt racist ideas and ideology still underpinned the views of many residents.[23]
 On Thursday 5 June 1998, Mr. Valli Moosa (then Minister of Constitutional Development in the ANC government) stated in a parliamentary budget debate that, "The ideal of some Afrikaners to develop the North Western Cape as a home for the Afrikaner culture and language within the framework of the Constitution and the Charter of Human Rights is viewed by the government as a legitimate ideal."[24]
 On July 4, 2007 the town of Orania and the Northern Cape government agreed that the question of Orania's self-government should be discussed at all government levels.[25]
 In the recent South African general election, 2009, the community decisively voted for the Freedom Front Plus party.[26]
 Debate surrounding a volkstaat returned to the mainstream media following the murder of AWB leader Eugene Terre'Blanche in April 2010. Boshoff claimed a symbolism of the murder for farm murders that he described as "nothing other than a state of war". Yet he rejected an invitation to Terre'Blanche's funeral, "I'm not enamoured of him.. He chose a path of confrontation, of conflict. We wanted another way."[27]
 In a conciliatory gesture, then President of South Africa Nelson Mandela, visited the town in 1995 to have tea with Betsie Verwoerd, widow of Hendrik Verwoerd.[28]
 In June 2007, the Afrikaner enclave received a visit from the Coloured community of Eersterust, outside Pretoria.[29] Both groups met to discuss community development and discussed methods of self-governance. According to visitors "The reception was good" and "We definitely learned from the experience and we experienced no racial tension."
The community of Orania gave a donation to the community of Eersterust in support of their nursery school.[29]
 In 2009, an ANCYL delegation visited the town. The leader Julius Malema praised the co-operation between residents "They co-operate instead of working against each other".[30]
 On 14 September 2010 President Jacob Zuma visited Orania. He met with Orania founder Professor Carel Boshoff and his son, Orania mayor Carel Boshoff IV and other community leaders. After the meeting Zuma visited housing projects and several agricultural sites in Orania.[31]
 Election results for Orania in the South African general election, 2009.[32]
     
    
 1 History

1.1 Purpose
1.2 Flag of Orania
1.3 Past movements
1.4 Land claim

 1.1 Purpose 1.2 Flag of Orania 1.3 Past movements 1.4 Land claim 2 Orania today

2.1 Environmental practices
2.2 Education
2.3 Economy and Agriculture
2.4 Media
2.5 Self-government progress

 2.1 Environmental practices 2.2 Education 2.3 Economy and Agriculture 2.4 Media 2.5 Self-government progress 3 Co-operation with South African communities 4 Politics

4.1 Election results

 4.1 Election results 5 Video 6 External links 7 References People recycle their own garbage by sorting and placing the garbage into five different trash cans. All new houses must have solar-boilers for hot water. Various people in the town are looking at ways to switch over to green electricity. In the town there is a number of different ecological buildings, for example a straw bale house with a living roof. In the near future earthships (aardskepe)[11] will be built in Orania. The CVO-school is run along conventional lines. [13] The Volkskool use a self driven teaching (selfgedrewe) system which is unorthodox by South African standards. [14] Total votes: 279 Orania web site in Afrikaans and English. (Official website of the Orania community) Orania Toerisme (Tourism) Orania Hotel M-Net story about Orania's unique school. Pictures of Orania and Oranians made by Alan Aubry, a french photographer. Article on iol about Orania ↑ 1.0 1.1 1.2 1.3 Main Place Orania. Census 2001. 
 ↑ http://orania.co.za/english/?page_id=407
 ↑ http://www.eskom.co.za/content/Heritage%20Report.PDF
 ↑ http://www.thefreedictionary.com/kraal
 ↑ 5.0 5.1 "10 years on, Orania fades away". news24.com. April 22, 2004. http://www.news24.com/News24/South_Africa/Decade_of_Freedom/0,,2-7-1598_1515558,00.html. 
 ↑ Brian M. du Toit, The Far Right in South Africa. - p.647.
 ↑ Groenewald, Y (November 18, 2005). "Coloureds Claim the Volkstaat". Mail & Guardian. http://www.mg.co.za/articlePage.aspx?articleid=256933&area=/insight/insight__national/. Retrieved 2006-06-25. 
 ↑ SAPA (December 5, 2006). News24. http://www.news24.com/Content/SouthAfrica/News/1059/7fe126f2e0f043fabb8907b9f6b6f036/05-12-2006-09-30/Orania_pleased_at_land_claim. Retrieved 2010-04-04. 
 ↑ Orania Inligtingspamflet, 2010
 ↑ Zuma likely to visit Orania IOL. 29 March 2009
 ↑ Project Aardskip
 ↑ www.orania.co.za/?page_id=89
 ↑ www.oraniacvo.co.za
 ↑ www.afstandsleer.co.za
 ↑ Willliam Dicey, Borderline p14
 ↑ "'Whites-only' money for SA town". BBC News. April 29, 2004. http://news.bbc.co.uk/2/hi/africa/3670441.stm. 
 ↑ "Orania launches own cheque book". i Africa. February 22, 2007. http://business.iafrica.com/news/635774.htm. 
 ↑ Orania Hotel - http://www.oraniahotel.co.za
 ↑ Flitslig, Mei 2010 and Voorgrond, 2010
 ↑ "Orania radio station kicked off the air". iol.co.za. November 9, 2005. http://www.iol.co.za/index.php?set_id=1&click_id=13&art_id=vn20051109064429203C704024. Retrieved 2006-06-25. 
 ↑ Radio Orania on-air again
 ↑ http://www.beeld.com/In-Diepte/Nuus/Hi%C3%A9r-is-anderkant-die-kla-20100119
 ↑ 23.0 23.1 http://www.dieburger.com/printArticle.aspx?iframe&aid=ce6a9a6a-be03-47ca-a783-40243d4a0423&cid=1688
 ↑ . beeld.com. June 5, 1998. http://152.111.1.88/argief/berigte/beeld/1998/06/5/4/12.html, Minister Valli Moosa views volkstaat as legitimate ideal. 
 ↑ "Orania, N Cape agree on way forward". iol.co.za. July 2, 2007. http://www.int.iol.co.za/index.php?set_id=1&click_id=13&art_id=nw20070704182018850C619640. 
 ↑ Orania votes for FF+  IOL. 23 April 2009
 ↑ Orania building a different future Sunday Times. 10 April 2010
 ↑ Daley, Suzanne (March 23, 1999). "Beloved Country Repays Mandela in Kind". New York Times. http://query.nytimes.com/gst/fullpage.html?res=9F03E6DA1131F930A15750C0A96F958260&n=Top/Reference/Times%20Topics/People/M/Mbeki,%20Thabo. Retrieved May 20, 2010. 
 ↑ 29.0 29.1 "Orania community lauded". news24.com. June 11, 2007. http://www.news24.com/News24/South_Africa/Politics/0,,2-7-12_2128163,00.html. 
 ↑ Malema surprised by Orania News 24. 28 March 2009
 ↑ Jacob Zuma visited Orania News 24. 14 September 2010
 ↑ http://www.iol.co.za/index.php?set_id=1&click_id=3086&art_id=nw20090423062148232C569880
 Pages with broken file links Articles containing potentially dated statements from 2010 All articles containing potentially dated statements Content from Wikipedia Populated places established in 1990 Intentional communities Populated places in the Northern Cape Utopian communities Populated places founded by Afrikaners Karoo Gated communities White separatism Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information Deutsch Español Svenska  This page was last edited on 7 August 2013, at 22:08. Privacy policy About Metapedia Disclaimers 
  Flag Government Area Population - Estimate - Density Racial makeup (2001) First languages (2001) Orania South Africa portal 

 Flag


  Flag 
OraniaMap indicating Orania's location in the Northern Cape
 
Orania is located in South AfricaOrania Orania shown within South Africa
 Coordinates: 29°49′S 24°24′E﻿ / ﻿29.817°S 24.4°E﻿ / -29.817; 24.4Coordinates: 29°49′S 24°24′E﻿ / ﻿29.817°S 24.4°E﻿ / -29.817; 24.4 South Africa Northern Cape Pixley ka Seme Thembelihle 1990 Government Company Carel IV Boshoff  Area[1] 9.45 km2 (3.6 sq mi) 80 km2 (30.9 sq mi) Population (2001)[1] 519  - Estimate (2011) 1,523  - Density 54.9/km2 (142.2/sq mi) Racial makeup (2001)[1] 1.7% 0.6% 97.7% First languages (2001)[1] 98.8% 1.2% SAST (UTC+2) http://www.orania.co.za/ Freedom Front Plus 242 86.73%
 Democratic Alliance 26 9.31%
 African National Congress 3 1.07%
 African Christian Democratic Party 3 1.07%
 Congress of the People 3 1.07%
 Spoilt votes 2 0.71%
 
 South Africa portal
 Orania Contents History Orania today Co-operation with South African communities Politics Video External links References Navigation menu Purpose Flag of Orania Past movements Land claim Environmental practices Education Economy and Agriculture Media Self-government progress Election results Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools In other languages 