Otto Wilhelm (Wille) Kuusinen (Russian: О́тто Вильге́льмович Ку́усинен) (4 October 1881, Laukaa, Finland – 17 May 1964, Moscow, USSR) was a Finnish and, later, Soviet politician, literary historian, and poet, who, after the defeat of the Reds in the Finnish Civil War, fled to the Soviet Union, where he worked until his death.
 Kuusinen was born to the family of village tailor Wilhelm Juhonpoika Kuusinen in Laukaa in 1881. His mother died when Kuusinen was two years old, and the family then moved to Jyväskylä. Kuusinen graduated from the Jyväskylä lyceum in May 1900 and entered Helsinki University the same year. His main subjects were philosophy, aesthetics and art history. Kuusinen was an active member of the students' union, and during this period he was interested in Fennoman conservatism and Alkioism. Kuusinen graduated as a Candidate of Philosophy in 1902.
 After toppling the more moderate party chairman J. K. Kari in 1906, Kuusinen came to dominate Finland's Social Democratic Party. He was a member of Finland's Parliament 1908–1913 and the party's chairman 1911–1917. He was a leader of the January 1918 revolution in Finland that created the short-lived Finnish Socialist Workers' Republic, of which he was appointed People's Commissar of Education.[1] After the republic was defeated in the Finnish Civil War in 1918, Kuusinen fled to Moscow and helped form the Finnish Communist Party.
 He continued his work as a prominent leader of the Comintern in Bolshevist Russia, that soon became the Soviet Union. Kuusinen also became a leader in Soviet military intelligence, establishing an intelligence network against the Scandinavian countries.[2] In Finland, a more moderate faction rehabilitated the Social Democrats under Väinö Tanner's leadership. Meanwhile, Kuusinen and other radicals were increasingly seen as responsible for the Civil War and its aftermath.
 Animosity towards Socialists in Finland in the decades after the Civil War prompted many Finns to emigrate to Russia to "build Socialism." However, the Soviet Great Purge was a hard blow against Finns in the Soviet Union — most of those who didn't escape back to Finland were executed as unreliable in the 1930s — and Kuusinen's reputation in Finland was further damaged when he turned out to be one of the very few not targeted by Stalinist show trials, deportations, and executions.
 When the Red Army began its advance during the Winter War on November 30, 1939, Kuusinen was pronounced head of the Finnish Democratic Republic (also known as the Terijoki Government)—Joseph Stalin's puppet régime intended to rule Finland. A "Declaration of the People's Government of Finland" was issued in Terijoki on December 1, 1939, and a "Treaty of Mutual Assistance and Friendship Between the Soviet Union and the Democratic Republic of Finland" signed by Molotov and Kuusinen in Moscow on December 2, 1939 [1] However, the war did not go as planned, and the Soviet leadership decided to negotiate a peace with the Finnish government; Kuusinen's government was quietly disbanded and he was made chairman of the presidium of the Supreme Soviet of the Karelo-Finnish SSR (1940–1956).
 From the very outset of the war, working-class Finns stood behind the legal government in Helsinki.[3] Finnish national unity against the Soviet invasion was later called the spirit of the Winter War.[4]
 Kuusinen became an influential official in the Soviet state administration. He was a member of the Politburo, the highest state organ. Kuusinen also continued his work during the administration of Nikita Khrushchev (1953–1964). He was Secretary of the Central Committee of the Communist Party of the Soviet Union 1957–1964. In 1952 and again in 1957 he was also elected to the Presidium of the Central Committee.
 Kuusinen was one of the editors of The Fundamentals of Marxism-Leninism, considered to be one of the fundamental works on dialectical materialism and Leninist communism. In Kremlin politics he was considered a liberal — and from its temporal distance his thinking pointed forward to perestroika. While editing a new party programme for "rapid agricultural, industrial, and technological development" he championed giving up the concept of the dictatorship of the proletariat, to the horror of more conservative ideologists. In this he was supported by Khrushchev.
 Kuusinen was elected a member of the Soviet Academy of Sciences in 1958.
 After learning that he was terminally ill, Kuusinen requested (via the Helsinki Embassy of the Soviet Union) permission to visit Laukaa and Jyväskylä as a private person. The government of Finland denied this request.
 Kuusinen died in Moscow on May 17, 1964. His ashes were buried in the Kremlin Wall Necropolis.
 Otto Wille Kuusinen was survived by his daughter Hertta Kuusinen, a leading communist politician in Finland during the Cold War.
 Kuusinen was married several times, and had numerous children, such as Aino Elina (born 1901), Hertta Elina (born 1904), Esa Otto Wille (born 1906), Riikka-Sisko (born 1908), Heikki (born 1911) and Taneli (born 1913). Most of his offspring remained with his first wife Saima Dahlström. In early 1920s Kuusinen married Aino Sarola. In 1936, he fell in love with the Russian Marina Amiragova, who was 30 years younger than he, and they stayed together until Kuusinen's death. The couple never married. They had a daughter in 1937 who died at the age of eleven months.
 During the Great Purge in 1937, Kuusinen's son was arrested. Stalin asked Kuusinen why he had not protested; Kuusinen replied, "No doubt there were serious reasons for his arrest." Later Kuusinen's son was released.[5]
 1 Early life 2 Civil War and flight to the Soviet Union 3 Head of the Terijoki Government 4 Member of the Politburo 5 Family 6 References 7 Further reading 8 External links ↑ 1.0 1.1 Brody, A. (1939). The USSR and Finland – Historical, Economic, Political Facts and Documents. New York: Soviet Russia Today. 
 ↑ Chapter 3. A History of Spetsnaz Spetsnaz. The Story Behind the Soviet SAS, ISBN 978-0-241-11961-7
 ↑ Trotter (2002). p. 61.
 ↑ Soikkanen, Timo (1999). "Talvisodan henki", Talvisodan pikkujättiläinen, 235. 
 ↑ Rayfield 2005, p. 316
  (1999) Talvisodan pikkujättiläinen, 1st (in Finnish), Werner Söderström Osakeyhtiö. ISBN 951-0-23536-9.   [2004] (2005) Stalin and His Hangmen. Penguin books. ISBN 978-0-14-100375-7.   [1991] (2002, 2006) The Winter war: The Russo–Finnish War of 1939–40, 5th, New York (Great Britain: London): Workman Publishing Company (Great Britain: Aurum Press). ISBN 1-85410-881-6. “First published in the United States under the title A Frozen Hell: The Russo–Finnish Winter War of 1939–40”  An address to Finland's proletarians (in Finnish) Articles in need of neutralization Articles needing additional references from January 2013 All articles needing additional references 1881 births 1964 deaths Central Committee of the Communist Party of the Soviet Union members Heroes of Socialist Labour Finnish Comintern people Finnish communists Finnish politicians Soviet politicians Finnish emigrants to the Soviet Union Marxist writers Full Members of the USSR Academy of Sciences People buried in the Kremlin Wall Necropolis People from Laukaa People of the Finnish Civil War (Red side) Politburo of the Central Committee of the Communist Party of the Soviet Union members Russian people of Finnish descent Articles containing non-English language text Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information  This page was last edited on 30 May 2013, at 10:21. Privacy policy About Metapedia Disclaimers 
  needs additional citations for verification In office In office Otto Wilhelm (Wille) Kuusinen Authority control 
  This article needs additional citations for verification. Please help improve this article by adding reliable references. Unsourced material may be challenged and removed. (January 2013) Otto Wille Kuusinen
 
 In officeDecember 2, 1939 – March 12, 1940
 None
 NoneKarelo-Finnish Soviet Socialist Republic
 In officeJuly 11, 1940 – July 16, 1956
 Pavel ProkkonenVoldemar VirolainenPavel Prokkonen
 Mark Vasilyevich Gorbachev
 Pavel ProkkonenKarelian Autonomous Soviet Socialist Republic
 
 4 October 1881Laukaa, Finland
 17 May 1964 (aged 82)Moscow, Soviet Union
 Social Democratic Party of FinlandCommunist Party of FinlandCommunist Party of the Soviet Union
 Hertta Kuusinen (and 7 others)
 NAME
 Kuusinen, Otto
 ALTERNATIVE NAMES
 
 SHORT DESCRIPTION
 Finnish politiciain
 DATE OF BIRTH
 4 October 1881
 PLACE OF BIRTH
 Laukaa, Finland
 DATE OF DEATH
 17 May 1964
 PLACE OF DEATH
 Moscow, Soviet Union
 Otto Wille Kuusinen Contents Early life Civil War and flight to the Soviet Union Head of the Terijoki Government Member of the Politburo Family References Further reading External links Navigation menu Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools 