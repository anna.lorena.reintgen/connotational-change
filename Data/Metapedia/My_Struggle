Mein Kampf (English translation: My Struggle) is an autobiography and National Socialist manifesto by Adolf Hitler. The first volume was written in 1924 in the Landsberg prison after the failed Munich Putsch and was published in 1925. The second volume, written in 1925, was published in 1926.
 Hitler originally wanted to call his forthcoming book "Viereinhalb Jahre (des Kampfes) gegen Lüge, Dummheit und Feigheit", or "Four and a Half Years (of Struggle) Against Lies, Stupidity and Cowardice". Max Amann, head of the Franz Eher Verlag and Hitler's publisher, is said to have suggested the much shorter "Mein Kampf", or "My Struggle".
 While Hitler was in power (1933–1945), Mein Kampf came to be available in three common editions. The first, the Volksausgabe or People's Edition, featured the original cover on the dust jacket and was navy blue underneath with a gold swastika eagle embossed on the cover. The Hochzeitsausgabe, or Wedding Edition, in a slipcase with the seal of the province embossed in gold onto a parchment-like cover was given free to marrying couples. In 1940, the Tornister-Ausgabe was released. This edition was a compact, but unabridged, version in a red cover and was released by the post office, available to be sent to loved ones fighting at the front. These three editions combined both volumes into the same book.
 A special edition was published in 1939 in honour of Hitler's 50th birthday. This edition was known as the Jubiläumsausgabe, or Anniversary Issue. It came in both dark blue and bright red boards with a gold sword on the cover. This work contained both volumes one and two. It was considered a deluxe version, relative to the smaller and more common Volksausgabe.
 The book could also be purchased as a two-volume set during Hitler's reign, and was available in soft cover and hardcover. The soft cover edition contained the original cover (as pictured at the top of this article). The hardcover edition had a leather spine with cloth-covered boards. The cover and spine contained an image of three brown oak leaves.
 The copyright of Mein Kampf in Europe was claimed by the Free State of Bavaria and expired in 2015. Reproductions in Germany were authorized only for scholarly purposes and in heavily commented form. The situation was however unclear; Werner Maser comments that intellectual property cannot be confiscated and so, it still would lie in the hands of Hitler's nephew, who, however, does not want to have anything to do with Hitler's legacy. This situation led to contested trials in Poland and Sweden. Mein Kampf, however, is published in the U.S., as well as in other countries such as Turkey and Israel, by publishers with various political positions.
 After the copyright expired in 2015, a German edition with the translated title "Hitler, Mein Kampf: A Critical Edition" was published. This version is "aimed at promoting political education and is easily comprehensible.” [...] The critical edition, the result of three years of labor by scholars, will include explanatory sections and some 3,500 annotations [...] the German Teachers’ Association, which said earlier this week that the book would “inoculate adolescents against political extremism.”" However, others argue that "This book is too dangerous for the general public".[1][2]
 The book became an instant bestseller.[3]
 Mein Kampf has been translated numerous times (often several times to the same language) and has been a bestseller in many countries (including but not limited to Islamic countries hostile to Israel).[4][5][6]
 There are many English translations of Mein Kampf. Criticisms against different versions have included outdated language, awkward language, deletions of original contents, unwarranted insertion of not original contents, openly hostile introductions and explanatory footnotes, and lack of explanatory footnotes. Like most Germans of the time, Hitler wrote long sentences, fashioned into long, complex paragraphs. Some translations have attempted to make the text easier to read, by breaking up such long sentences and paragraphs into shorter ones (and have in some instances been criticized for this damaging flow and connection of ideas).[7]
 The first English translation was an abridgment by Edgar Dugdale, who started work on it in 1931, at the prompting of his wife, Blanche. When he learned that the London publishing firm Hurst & Blackett had secured the rights to publish an abridgment in the United Kingdom, he offered it for free in April 1933. However, a local National Socialist representative insisted that the translation be further abridged before publication, so it was held back from the public until October 13, 1933, although excerpts were allowed to run in The Times in late July. It was published by Hurst & Blackett as part of "The Paternoster Library".
 In America, Houghton Mifflin secured the rights to the Dugdale abridgment on 29 July 1933. The only differences between the American and British versions are that the title was translated My Struggle in the United Kingdom and My Battle in the United States; and that Dugdale is credited as translator in the American edition, while the British version withheld his name. Both Dugdale and his wife were active in the Zionist movement; Blanche was the niece of Lord Balfour, and they wished to avoid publicity.
 One of the first complete English translations of Mein Kampf was by James Murphy. It had initially started in 1936, as an officially approved National Socialist translation. However, there was a falling out with National Socialist officials and Murphy was ‘fired’ sometime in 1938, with the translation still incomplete. After his dismissal, Murphy returned to England and revised and completed his translation, which was published by Hurst & Blackett in 1939. It is one of the more popular translations.[7]
 This translation has been criticized: "because he was ill, Murphy hired one Greta Lorke as assistant translator to help him out. Unbeknownst to him, Lorke was an operative of the “Red Orchestra” (Rote Kapelle, in German), the notorious Communist espionage and sabotage ring run by the Soviet Union. Lorke saw working on the Mein Kampf translation as a perfect cover identity that allowed her to participate in Red Orchestra activities without drawing suspicion to herself. Additionally, she saw the assignment as an opportunity to discredit Hitlerism by fiddling with the passages that she translated."[8]
 See the Stalag translation below on an official National Socialist completion of Murphy's early draft translation.
 Also in 1939, the British firm Reynal & Hitchcock published a version translated by a team headed by Alvin Johnson.[7]
 The third translation appearing in 1939 was by the small Pennsylvania firm Stackpole and Sons, which published a translation by the Jewish William Soskin. It was successfully sued by Houghton Mifflin.[7]
 The fourth translation appearing in 1939 was an abridged translation by Alan Cranston, an American reporter and later a U.S. Senator. It was also successfully sued by Houghton Mifflin.[7]
 Houghton Mifflin brought out a translation by the Jewish-German writer Ralph Manheim in 1943. The Manheim version has been argued to in practice function as the "official" translation of Mein Kampf and being the translation quoted by nearly all academics and journalists.[7]
 "Along with some useful technical notes on translation and others on historical context, it also includes a number of gratuitous, negative footnotes, as well as an anti-Hitler introduction. More troubling are Manheim’s downright errors, however. For example, on page 490, when discussing the SA, Manheim translates Kampfgemeinschaft as “combat group,” whereas the proper translation is “fighting community,” a small but significant difference; Hitler goes out of his way to make the point that the SA should NOT be a military-style “combat group” (pp. 538-543). On page 623, Manheim inexplicably substitutes “Jewish” for “German” in one phrase, reducing the sentence to nonsense. (Manheim’s version reads “national folkish Jewish intelligensia,” whereas Hitler’s original is “national folkish German intelligensia.”) It is not surprising that some mistakes should occur here and there in a translation nearly 700 pages long. However, the Manheim version first appeared in 1941, and these errors should have been corrected in the numerous reprints that have been issued since then!"[8]
 This translation was first published in 2009 by Elite Minds. Its translator, Michael Ford, claims it is an English translation superior to previous English translations. Ford wrote a short pamphlet arguing for the strength of this translation entitled Mein Kampf: A Translation Controversy.[9]
 However, this translation has also been criticized.[7][10]
 National Socialist Germany completed Murphy’s draft translation on their own and published it in the late 1930s. Today, this is known as the Stalag edition. The Stalag translation "was printed by the Franz Eher Verlag in Berlin for the Central Press of the NSDAP in limited numbers during the years 1937 to 1944. Most copies were distributed to the camp libraries of English-speaking Prisoner of War (POW) camps, and became known as the “Stalag” editions (Stalag being a contraction of the German word Stammlager, or POW camp) because they all carried a camp library rubber stamp on the title page."[11][7]
 This official translation was for a long time unknown or ignored. However, today there are two different versions of this translation being sold. The version published by Elite Minds claims to be "authentic". This has been argued to mean that it retains all the original errors of grammar, punctuation, and spelling.[7]
 The Stalag translation version published by Ostara Publications has been argued to fix many of the above mentioned errors.[11][7]
 The revisionist Thomas Dalton is working on a new translation. "My forthcoming translation addresses and resolves many of these unfortunate drawbacks. First, by including the full and original German text, in a parallel translation, the English wording can be easily verified. This technique has often been used with classic Greek and Latin authors, but never before with Mein Kampf. Section headings have been added, in text, in bold. The German original employed such headings, but only at the top of each page; the reader thus never knew where a new section actually began. These headings have been translated and inserted at the appropriate points, in my estimation, and directly in the text. My translation also has helpful and relevant footnotes, a useful index, and a bibliography of relevant secondary source material. Most important of all, though, is the fact that the English reads smoothly and naturally."[7]
 The translated volume one of Mein Kampf was published in 2017. The second volume was completed in 2019.[12]
 1 Title 2 Contents 3 German publication history

3.1 Early German editions
3.2 Postwar copyright status and prohibitions on publishing
3.3 "Hitler, Mein Kampf: A Critical Edition"

 3.1 Early German editions 3.2 Postwar copyright status and prohibitions on publishing 3.3 "Hitler, Mein Kampf: A Critical Edition" 4 Translations and international readership 5 English translations

5.1 Dugdale translation
5.2 1939 "Murphy" translation
5.3 Reynal & Hitchcock translation
5.4 Stackpole translation
5.5 Cranston translation
5.6 Manheim translation
5.7 Ford translation
5.8 Stalag translation - version published by Elite Minds
5.9 Stalag translation - version published by Ostara Publications
5.10 Dalton translation

 5.1 Dugdale translation 5.2 1939 "Murphy" translation 5.3 Reynal & Hitchcock translation 5.4 Stackpole translation 5.5 Cranston translation 5.6 Manheim translation 5.7 Ford translation 5.8 Stalag translation - version published by Elite Minds 5.9 Stalag translation - version published by Ostara Publications 5.10 Dalton translation 6 See also

6.1 Controversial statements and words in Mein Kampf

 6.1 Controversial statements and words in Mein Kampf 7 External links

7.1 Articles and forum threads on Mein Kampf and the different translations

 7.1 Articles and forum threads on Mein Kampf and the different translations 8 References Volume One: A Reckoning
Chapter 1: In the House of my Parents
Chapter 2: Years of Study and Suffering in Vienna
Chapter 3: General Political Considerations Based on my Vienna Period
Chapter 4: Munich
Chapter 5: The World War
Chapter 6: War Propaganda
Chapter 7: The Revolution
Chapter 8: The Beginning of my Political Activity
Chapter 9: The "German Workers' Party"
Chapter 10: Causes of the Collapse
Chapter 11: Nation and Race
Chapter 12: The First Period of Development of the National Socialist German Workers' Party Chapter 1: In the House of my Parents Chapter 2: Years of Study and Suffering in Vienna Chapter 3: General Political Considerations Based on my Vienna Period Chapter 4: Munich Chapter 5: The World War Chapter 6: War Propaganda Chapter 7: The Revolution Chapter 8: The Beginning of my Political Activity Chapter 9: The "German Workers' Party" Chapter 10: Causes of the Collapse Chapter 11: Nation and Race Chapter 12: The First Period of Development of the National Socialist German Workers' Party Volume Two: The National Socialist Movement
Chapter 1: Philosophy and Party
Chapter 2: The State
Chapter 3: Subjects and Citizens
Chapter 4: Personality and the Conception of the Völkisch State
Chapter 5: Philosophy and Organisation
Chapter 6: The Struggle of the Early Period – the Significance of the Spoken Word
Chapter 7: The Struggle with the Red Front
Chapter 8: The Strong Man Is Mightiest Alone
Chapter 9: Basic Ideas Regarding the Meaning and Organization of the Sturmabteilung
Chapter 10: Federalism as a Mask
Chapter 11: Propaganda and Organization
Chapter 12: The Trade-Union Question
Chapter 13: German Alliance Policy After the War
Chapter 14: Eastern Orientation or Eastern Policy
Chapter 15: The Right of Emergency Defense Chapter 1: Philosophy and Party Chapter 2: The State Chapter 3: Subjects and Citizens Chapter 4: Personality and the Conception of the Völkisch State Chapter 5: Philosophy and Organisation Chapter 6: The Struggle of the Early Period – the Significance of the Spoken Word Chapter 7: The Struggle with the Red Front Chapter 8: The Strong Man Is Mightiest Alone Chapter 9: Basic Ideas Regarding the Meaning and Organization of the Sturmabteilung Chapter 10: Federalism as a Mask Chapter 11: Propaganda and Organization Chapter 12: The Trade-Union Question Chapter 13: German Alliance Policy After the War Chapter 14: Eastern Orientation or Eastern Policy Chapter 15: The Right of Emergency Defense Conclusion Index Zweites Buch - A claimed unedited text by Hitler on foreign policy written in 1928. Hitler’s Table Talk - Another claimed publication of statements by Hitler. Alleged statements by Hitler on the Holocaust: Mein Kampf Big lie Lebensraum Master race Mein Kampf - Manheim translation Mein Kampf - Manheim translation, various formats Rethinking Mein Kampf What Hitler Believed Best Mein Kampf English translation? ↑ Hitler’s 'Mein Kampf' Okayed for use in classrooms by Germany's education minister https://www.rt.com/news/327023-mein-kampf-german-schools/
 ↑ Hitler's Mein Kampf published in Germany, 1st time since WWII https://www.rt.com/news/324243-mein-kampf-republished-germany/
 ↑ Hitler's Mein Kampf sells out instantly after being published in Germany for first time in 70 years http://www.telegraph.co.uk/news/worldnews/europe/germany/12092029/Hitlers-Mein-Kampf-sells-out-instantly-after-being-published-in-Germany-for-first-time-in-70-years.html
 ↑ Why is Hitler's 'Mein Kampf' an e-book bestseller? http://www.latimes.com/books/jacketcopy/la-et-jc-why-is-hitlers-mein-kampf-an-e-book-bestseller-20140109-story.html#ixzz2qClxeHCT
 ↑ Jewish leaders fear online surge of Hitler's 'Mein Kampf' fuels new wave of hate http://www.foxnews.com/us/2014/01/16/jewish-leaders-fear-online-surge-hitler-mein-kampf-fuels-new-wave-hate.html
 ↑ The Strange History of How Hitler's 'Mein Kampf' Became a Bestseller in India http://mic.com/articles/120411/how-hitler-s-mein-kampf-became-a-bestseller-in-india
 ↑ 7.00 7.01 7.02 7.03 7.04 7.05 7.06 7.07 7.08 7.09 7.10 Rethinking Mein Kampf https://codoh.com/library/document/4033/?lang=en
 ↑ 8.0 8.1 Which is the Best Translation of Mein Kampf? http://nationalvanguard.org/2015/09/which-is-the-best-translation-of-mein-kampf/
 ↑ Mein Kampf: A Translation Controversy http://hitlerlibrary.org/Mein-Kampf-Translation-Controversy.pdf
 ↑ Errors in the Ford Translation of Mein Kampf http://bytwerk.com/Ford-Translation/ford-errors.htm
 ↑ 11.0 11.1 Mein Kampf: The Stalag Edition—The Only Complete and Officially Authorised English Translation Ever Issued http://ostarapublications.com/mein-kampf-stalag-edition-complete-officially-authorised-english-translation-ever-issued/
 ↑  Hitler's Mein Kampf: The ONLY Translation You Will Want! https://anoccasionalcomment.blogspot.com/2019/01/04201889.html
 Content from Wikipedia National Socialism NSDAP writings Works by Adolf Hitler Log in Page Discussion Read View source View history Main page Community portal Recent changes Random page Help Donate What links here Related changes Special pages Printable version Permanent link Page information Deutsch Eesti Español Français Magyar Nederlands Português Slovenčina Svenska  This page was last edited on 29 September 2019, at 10:12. Privacy policy About Metapedia Disclaimers 
  Mein Kampf My Struggle The arrangement of chapters is as follows: The arrangement of chapters is as follows: Mein Kampf Contents Title Contents German publication history Translations and international readership English translations See also External links References Navigation menu Early German editions Postwar copyright status and prohibitions on publishing "Hitler, Mein Kampf: A Critical Edition" Controversial statements and words in Mein Kampf Articles and forum threads on Mein Kampf and the different translations Personal tools Namespaces 
Variants
 Views More 
Search
 Navigation Tools In other languages Dugdale translation 1939 "Murphy" translation Reynal & Hitchcock translation Stackpole translation Cranston translation Manheim translation Ford translation Stalag translation - version published by Elite Minds Stalag translation - version published by Ostara Publications Dalton translation 