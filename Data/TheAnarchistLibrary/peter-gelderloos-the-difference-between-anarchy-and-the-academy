
I recently had the opportunity to participate in the international academic conference, “Hierarchy and Power in the History of Civilizations,” put on by the Russian Academy of Sciences, in Moscow. I was on two panels focused on building alternatives to hierarchy and current state repression of social movements. I find this amusing because I am a college dropout: I didn’t even finish three semesters of university, I generally dislike academics, and I believe the academy is one of the institutions of power that need to be abolished. Of over a hundred participants, I think I was one of only two who were not a PhD or a PhD candidate (and the other lowbrow was on the same panels as I) and the only one without any university degree. It would have been funny and worthwhile if I had scammed my way there — in fact university credentials are easy to forge, so radicals who want to be teachers need not waste five years of their lives getting the right papers. But in this case I was invited by the panel organizers, who also have their criticisms of the academy and wanted to put together panels without so great a theoretical remove from the actuality of social movements and repression.

If I were an anthropologist I could write quite an ethnology about that queer tribe of academics. But from my vantage as an anarchist I can find even more to say. It would be as easy as dogma to point out that the academy is one of the ruling institutions, therefore it’s our enemy, and that’s the end of it. This would also obscure the more complicated and useful realities. Universities have also been a hotspot (or shall I say “locus”?) for rebellion and social movements. My Russian friends tell me that the anarchist movement there largely re-emerged from the History Department in the ‘80s, and its last aboveground stronghold was the museum at the Kropotkin House, finally shut down in 1931. In between rebellions universities provide lots of free food, free copies, funding, media of communication (e.g. presentations to student bodies), spaces, and employment. University connections can mitigate state repression and confer legitimacy on those social rebels who masquerade, at least temporarily, as dissidents. It is no coincidence that scams to win free resources are so easy at universities. The university is intended to be a relatively liberated space within the framework of domination. I personally know several academics who are sincere anti-authoritarians and who have taught me a lot. And I know some people who are straight up anarchists who happen to have jobs within the academy. I can’t think of any other elite institution with so many good people in it who don’t forget social questions when they go to work but rather address them directly.

But where anarchy and the academy intersect, I always ask myself: are these people anarchist academics or academic anarchists? Howard Ehrlich, Noam Chomsky, Michael Albert, David Graeber, bell hooks (not an anarchist but theoretically relevant to many anarchists), and Pyotr Kropotkin have all said or done things I find extremely naive and damaging, in ways that directly reflect their privileged relationship with authority as members of an elite institution. But who could deny their contributions to the movement? Well, anarchists can deny anything, but most of us find something worthwhile in the work of at least some of those scholars. And without them, the movement would only have people who do research like John Zerzan (or me, for that matter). And research is a major area where the academy can be useful to anarchists. They have us cornered when it comes to investigation and critical debate.

Anarchists are lazy researchers. Many prefer religion to research. Objective, and objectively false, statements that bear great importance for anarchist theory circulate freely in our circles. Some of the basic premises of primitivist, vegan, and historical materialist strains of anarchism would have been abandoned long ago if we’d had a culture of serious inquiry and debate. Instead we have name-calling on internet forums. I think we also could have made some headway on the eternal debate about the nature of formal and informal power and the extent to which each allows hierarchies to be established or challenged. But alas, in our circles it’s still anybody’s guess.

In Moscow I found out about the Early State project, a network of social scientists studying the appearance and evolution of the State. Why aren’t anarchists aware of this research? Why do we remain without our own circles, and our own literature, when we look for new information? And why don’t we try more to intervene in and influence academic debates? An acquaintance of mine related the interesting story of a conversation he had with a depressed climate scientist. The scientist saw no way out of the disaster presented by climate change. he bemoaned the lack of some global network of active people with a vision for a decentralized and non-industrial society, and he went on to describe something very similar to the anarchist movement, unaware that it already existed. The fact that practically no climate scientists are engaged in direct action and fighting alongside the movement (and they’re a very desperate group of people) evidences our failure to communicate with a key group of potential allies, as much as it evidences a failure of academics to understand their role in the system, which I’ll get to later.

Let me interject that I don’t mean to portray academic research as something unconditionally valid. Just like anyone else, academics have their own mythology. Perhaps the most odious part can be found in their Creation Story, and it’s the part about not having a mythology. Most of the individual myths differ from one discipline to the next, but I have heard, from the mouths of professors well respected in their fields, such mythically loaded statements as: “the purpose of organisms is to perpetuate DNA” (wait, a string of acids can have agency? Something you claim to be just a collection of proteins has a purpose? And what’s your purpose in inverting the traditional value chain so that life becomes just a redundant instrument? And why do you have that knife in your hand and where did my pet rat go?)

or: “it’s useless to trace it [indigenous resistance] back any further than 30 years” (oh, so the construction of identity means that, on the one hand, since the individual subject constructs her own identity in the course of her life there is no higher validity to these identities, thus the phrase “five hundred years of resistance” is just a political slogan with no more weight than, say, “evict the indios,” and on the other hand your theory grants you the authority to interpret someone else’s identity, and it’s just coincidence that the people in your place five hundred years ago had that exact same authority).   Of course not all academics are believers, but the clear majority are.

Academics can be a really arrogant lot who object to outsiders treading on their turf. I recall an argument from a few weeks earlier that arose when an anarchist academic accused me of “romanticizing” non-Western societies. She couldn’t back up this accusation, and in fact all I had done was to name a couple societies in which the ideal for conflict resolution was based on generalized intervention rather than specialized arbiters, which is not a qualitative statement, thus there was simply no room for romanticism unless I had said something like “and they all believe that...” or “...and it works perfectly!” which I didn’t. In reality she objected to my meddling, because non-Western societies are supposed to be the intellectual property of anthropologists, at the same time as their traditional plants are patented and their religions butchered and marketed to hippies.

But in other circumstances academics will willingly step down from their elevated equines and listen to anarchists, as we are so obviously better than they are at many things. In Moscow, several regular professor-types came to the anarchist panels and later told the organizers that they were nearly moved to tears to hear people talking with passion and intelligence about lived experiences rather than *prevaricating like stuffed shirt experts guarding their terrain. And we got this sympathetic reaction despite the fact that most of us weren’t dressed up and often spoke frankly about the need to burn cop cars or bust people out of prison (you know, those things anarchists aren’t supposed to mention to normal people for fear of alienating them).

Occasionally getting our toes wet in the academy can avail us of theoretically useful and challenging information produced by people with absolutely no interest in confirming our worldview. It might also win us allies who can bring greater social legitimacy to our movement and new connections, new possibilities for communication, and we don’t even have to pretend that we don’t want to abolish them. In the paper I submitted at this conference, I openly stated that academic discourse can only contribute to the wrongs of the justice system, and that the academy needs to be abolished as much as the prison.

If anarchists do eventually make greater use of the academy, we have to be careful of several dangers, and consciously maintain the difference between anarchy and the academy. We do not want to be like these people. We must always identify and fight with the most exploited and excluded members of society, and whatever form of respectability and legitimacy we develop must be of a completely different hue. There is honor among thieves, and we prefer that kind to the honor of titled professionals. Imagine the hypocrisy, the blindness, of the social scientists studying “hierarchy and power” evident in one particular scene, the reception dinner at the end of the conference. A hundred ladies and gentlemen in expensive dresses and suits, gobbling up hors d’ouevres in a building guarded by private security in the capital of a poor country, only aesthetically aware of the dozen t-shirt- and jeans-clad anarchists among them, some packing weapons because their very real struggle against hierarcy puts them in constant risk of attack by fascists, casually stealing silverware and filling plastic bags with banquet delicacies to feed themselves for the next few days. I recall one conversation: a flirty prof mentioned the lovely seaside hotel he stayed in during a conference in Barcelona. I couldn’t help but interject: “ah yes, there used to be a fishermen’s village there before they demolished it and built the artificial beach. It was really nice.” He didn’t get the irony. Let me repeat: we do not want to be like these people.

So what does this partial separation mean for anarchists in the academy? I don’t see any hypocrisy in that position, just a conflict of interests. “You are not your job,” to quote Brad Pitt. I was a taxi driver, and I believe that cars should be abolished. This just reflects a contradiction of capitalist reality: we kill ourselves to make a living.

There’s lots of good work anarchists can do in the academy. Theoretical work, direct communication with lots of people outside our circles, and intervention in public discourse. As is the case with all anarchist work, if they do it well this work will get them into trouble. I think Ward Churchill and David Graeber, to name two examples, should be commended to sticking to their guns even as their political decisions threatened them with job loss. The academy can easily co-opt well meaning but passive anti-authoritarians, turning them into mere dissidents and functionaries. Like everyone else, academics have to choose sides.[1] Claiming objective neutrality, while saying nothing about their elite position in society, makes it all too clear what their choice is.

A serious danger posed to and by social scientists is the question of studying the movement. Our narcissistic side may be thrilled by academic studies of anarchists, but these studies are a threat. We do want constructive criticism but I argue that we should absolutely not want to be legible to the authorities, and the authorities are the ultimate audience of all academic production. Just as anthropologists help the CIA to manage Iraq and Afghanistan, they could also provide information that facilitates the infiltration and repression of our movement. We do not need professionals to enable us to communicate with other people. They will only translate us for the authorities. We must build our own networks that expand beyond the ghetto. In the meantime we need to obstruct any serious ethnologies or studies of our networks. It seems strange, since networks are second-nature to us, but the authorities really don’t get it. Many of our tactical victories so far are attributable to their ignorance of how networks function. They’re still trying to identify our leaders and funding structures for chrissake. Once some clever academic finds a way to translate networks into terms that are actionable for technocrats, police control of horizontal movements will become much more effective.

For that reason, with both irony and seriousness, I call for the excommunication of all academic anarchists who produce not for the movement but for the academy. If you study networks, find ways to explain to us how to effectively extend networks to people currently plugged into the system (or some other useful question), not how to analyze our networks so they can be understood by outsiders, as intellectually stimulating as that task may be.

Simply producing information aids the system, even if that information seems to be revolutionary in its implications. This is because in democratic societies, people are pacified, and even if they are well informed they will not have gotten what they need to fight back. Information is not what’s lacking. It is the institutions of power, and not the people, that are positioned to act on this information, and even critical information coming from dissident academics can help these institutions correct themselves. The Early State project provides a great example. Among their writings, one finds many articles that squarely disprove the statist mythology regarding the creation of the state — that it arose out of need or out of some social contract. They make it clear that the state is a coercive institution, thus they have a clearer view of the true nature of democracy than nearly everyone on the left. Yet this information will not find its way into the popular mind, because the government and the capitalists control the infrastructure that shapes the popular mind and those academics are not engaged in any political actions to directly spread that awareness to the people. And then there’s something else: among the Early State writings one inevitably finds the humanitarian pieces that, taking advantage of new knowledge on how states formed in the first place, provide analyses for how to establish state control in situations of “failed” or “weak” states, for example in Somalia, where the US and Ethiopian governments are fighting against pirates, tribes, and terrorists, many of whom are organized horizontally to a large degree.

Among these varying approaches, which studies do you suppose will find government funding? Which will be repeated and expanded, and make their way into evolving government policies and strategies? This is why the apparent independence of the academy is so indispensible. The dissidents will tweak the machine.

This ironic outcome points to perhaps the most important distinction between academics and anarchists. Academics put everything in terms of discourse. Their fundamental claim to neutrality is that they’re just trying to talk about these things, to study them, and not to be actors. At their most active, they will make policy recommendations (aimed at those who create policy, that is, the elite), and thus their preference for discourse signifies their loyal passivity as technicians in a ruling institution. At the most absurd end, things that are very clearly actions are referred to as part of the “literature.”

Anarchists, on the other hand, talk about things in terms of action. Even speech, in its ideal form, is an action, because its purpose is to create change. In our most absurd moments, we refer to purely symbolic protests as “direct action.”  With this language we signify that we are at war with the system and we actually want to do something about it, to empower ourselves rather than to become invisible observers.

This is our strength, and whatever forays into the academy some anarchists may choose to make, it is the one thing we must not lose. And it is also this approach, this emphasis on action, that we must push those academics who consider themselves anti-authoritarian to adopt.

 
[1] From an anarchist standpoint, choosing sides has to include the possibility of creating your own side. If I talk about choosing sides I’m not saying that anyone has to toe a party line, just that it’s impossible to be neutral on a moving train.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

