      AGAINST NATIONALISM      BAKUNIN AND THE FIRST INTERNATIONAL      EAST EUROPE      EGYPT AND ALGERIA      EUROPE AND MOROCCO      CUBA      MEXICO, NICARAGUA AND AUGUSTINO SANDINO      LIBYA AND ERITREA      IRELAND AND JAMES CONNOLLY      ANARCHIST REVOLUTION IN KOREA      IN CONCLUSION: TOWARDS THE DESTRUCTION OF IMPERIALISM
The anarchist movement has a long tradition of fighting imperialism. This reaches back into the 1860s, and continues to the present day. From Cuba, to Egypt, to Ireland, to Macedonia, to Korea, to Algeria and Morocco, the anarchist movement has paid in blood for its opposition to imperial domination and control.

However, whilst anarchists have actively participated in national liberation struggles, they have argued that the destruction of national oppression and imperialism can only be truly achieved through the destruction of both capitalism and the state system, and the creation of an international anarcho-communist society.

This is not to argue that anarchists absent themselves from national liberation struggles that do not have such goals. Instead, anarchists stand in solidarity with struggles against imperialism on principle, but seek to reshape national liberation movements into social liberation movements.

Such movements would be both anti-capitalist and anti-imperialist, would be based on internationalism rather than narrow chauvinism, would link struggles in the imperial centres directly to struggles in the oppressed regions, and would be controlled by, and reflect the interests of, the working class and peasantry.

In other words, we stand in solidarity with anti-imperialist movements, but condemn those who use such movements to advance reactionary cultural agendas (for example, those who oppose women's rights in the name of culture) and fight against attempts by local capitalists and the middle class to hijack these movements. We oppose state repression of anti-imperialist movements, as we reject the right of the state to decide what is, and what is not, legitimate protest. However, it is no liberation if all that changes is the colour or the language of the capitalist class.

This is where we differ from the political current that has dominated national liberation movements since the 1940s: the ideology of nationalism.

Nationalism is a political strategy that argues that the key task of the anti-imperialist struggle is to establish an independent nation-state. It is through these independent states, nationalists argue, that the nation as a whole will exercise its general will. In the words of Kwame Nkrumah, who spearheaded the formation of the independent nation-state of Ghana, the task was to "Seek ye first the political kingdom, and all else shall be given unto you."

In order to achieve this goal, nationalists argue that it is necessary to unite all classes within the oppressed nation against the imperialist oppressor. Nationalists tend to deny the importance of class differences within the oppressed nation, arguing that the common experience of national oppression makes class divisions unimportant, or that class is a "foreign" concept that is irrelevant.

Thus nationalists seek to hide class differences in a quest to found an independent nation-state.

The class interests that hide behind nationalism are obvious. Nationalism has, historically, been an ideology developed and championed by the bourgeoisie and middle class in the oppressed nation. It is a form of anti-imperialism that wishes to remove imperialism but retain capitalism, a bourgeois anti-imperialism that wishes, in short, to create for the local bourgeoisie more space, more opportunities, more avenues to exploit the local working class and develop local capitalism.

Our role as anarchists in relation to nationalists is thus clear: we may fight alongside nationalists for limited reforms and victories against imperialism but we fight against the statism and capitalism of the nationalists.

Our role is to win mass support for the anarchist approach to imperial domination, to win workers and peasants away from nationalism and to an internationalist working class programme: anarchism. This requires active participation in national liberation struggles but political independence from the nationalists. National liberation must be differentiated from nationalism, which is the class programme of the bourgeoisie: we are against imperialism, but also, against nationalism.

Support for national liberation follows directly from anarchism's opposition to hierarchical political structures and economic inequality, and advocacy of a freely constituted international confederation of self-administrating communes and workers' associations. At the same time, however, anarchism's commitment to a general social and economic emancipation means that anarchism rejects statist solutions to national oppression that leave capitalism and government in place.

If anyone can be named the founder of revolutionary anarchism, it is Mikhail Bakunin (1918-1876). Bakunin's political roots lay within the national liberation movements of Eastern Europe, and he retained a commitment to what would nowadays be called 'decolonisation' throughout his life. When Bakunin moved from pan-Slavic nationalism towards anarchism in the 1860s, following the disastrous 1863 Polish insurrection, he still argued in support of struggles for national self-determination.

He doubted whether "imperialist Europe" could keep the colonial countries in bondage: "Two-thirds of humanity, 800 million Asiatics asleep in their servitude will necessarily awaken and begin to move."[1] Bakunin went on to declare his "strong sympathy for any national uprising against any form of oppression", stating that every people "has the right to be itself .... no one is entitled to impose its costume, its customs, its languages and its laws."[2]

The crucial issue, however,"in what direction and to what end" will the national liberation movement move? For Bakunin, national liberation must be achieved "as much in the economic as in the political interests of the masses": if the anti- colonial struggle is carried out with "ambitious intent to set up a powerful State" or if "it is carried out without the people" and "must therefore depend for success on a privileged class," it will become a "retrogressive, disastrous, counter-revolutionary movement."[3]

"Every exclusively political revolution - be it in defence of national independence or for internal change.... - that does not aim at the immediate and real political and economic emancipation of people will be a false revolution. Its objectives will be unattainable and its consequences reactionary." [4]

So, if national liberation is to achieve more than simply the replacement of foreign oppressors by local oppressors, the national liberation movement must thus be merged with the revolutionary struggle of the working class and peasantry against both capitalism and the State. Without social revolutionary goals, national liberation will simply be a bourgeois revolution.

The national liberation struggle of the working class and peasantry must be resolutely anti-statist, for the State was necessarily the preserve of a privileged class, and the state system would continually recreate the problem of national oppression: "to exist, a state must become an invader of other states .... it must be ready to occupy a foreign country and hold millions of people in subjection."

The national liberation struggle of oppressed nationalities must be internationalist in character as it must supplant obsessions with cultural difference with universal ideals of human freedom, it must align itself with the international class struggle for "political and economic emancipation from the yoke of the State" and the classes it represents, and it must take place, ultimately, as part of an international revolution: "a social revolution .... is by its very nature international in scope" and the oppressed nationalities "must therefore link their aspirations and forces with the aspirations and forces of all other countries."[5] The "statist path involving the establishment of separate .... States" is "entirely ruinous for the great masses of the people" because it did not abolish class power but simply changed the nationality of the ruling class.[6] Instead, the state system must be abolished and replaced with a coalition of workplace and community structures "directed from the bottom up .... according to the principles of free federation."[7]

These ideas were applied in East Europe from the 1870s onwards, as anarchists played an active role in the in 1873 uprisings in Bosnia and Herzegovina against Austro-Hungarian imperialism. Anarchists also took an active part in the "National Revolutionary Movement" in Macedonia against the Ottoman Empire. At least 60 gave their lives in this struggle, particularly in the great 1903 revolt.

This tradition of anarchist anti-imperialism was continued 15 years later in the Ukraine as the Makhnovist movement organised a titanic peasant revolt that not only smashed the German occupation of the Ukraine, and held off the invading Red and White armies until 1921, but redistributed land, established worker- peasant self-management in many areas, and created a Revolutionary Insurgent Army under worker-peasant control.

In the 1870s, too, the anarchists began to organise Egypt, notably in Alexandria, where a local anarchist journal appeared in 1877,[8] and anarchist group from Egypt was represented at the September 1877 Congress of the "Saint-Imier International" (the anarchist faction of the post-1872 First International).[9] An "Egyptian Federation" was represented at the 1881 International Social Revolutionary Congress by well-known Errico Malatesta, this time including "bodies from Constantinople and Alexandria."[10] Malatesta, who lived in Egypt as a political refugee Egypt in 1878 and 1882,[11] became involved in the 1882 "Pasha Revolt" that followed the 1876 take-over of Egyptian finances by an Anglo-French commission representing international creditors. He arrived specifically to pursue "a revolutionary purpose connected to the natives' revolt in the days of Arabi Pasha," [12] and "fought with the Egyptians against the British colonialists."[13]

In Algeria, the anarchist movement emerged in the nineteenth century. The Revolutionary Syndicalist General Confederation of Labour (CGT-SR) had a section in Algeria. Like other anarchist organisations, the CGT-SR opposed French colonialism, and in a joint statement by the Anarchist Union, the CGT-SR, and the Association of Anarchist Federations on the centenary of the French occupation of Algeria in 1930, argued: "Civilisation? Progress? We say: murder!".[14]

A prominent militant in the CGT-SR's Algerian section, as well as in the Anarchist Union and the Anarchist Group of the Indigenous Algerians, was Sail Mohamed (1894-1953), an Algerian anarchist active in the anarchist movement from the 1910s until his death in 1953. Sail Mohamed was a founder of organisations such as the Association for the Rights of the Indigenous Algerians and the Anarchist Group of the Indigenous Algerians. In 1929 he was secretary of the "Committee for the Defence of the Algerians against the Provocations of the Centenary." Sail Mohamed was also editor of the North African edition of the anarchist periodical Terre Libre, and a regular contributor to anarchist journals on the Algerian question.[15]

Opposition to imperialism was a crucial part of anarchist anti-militarist campaigns in the imperialist centres, which stressed that colonial wars did not serve the interests of workers but rather the purposes of capitalism.

The General Confederation of Labour (CGT) in France, for example, devoted a considerable part of its press to exposing the role of French capitalists in North Africa. The first issue of La Bataille Syndicaliste, which appeared on the 27 April 1911, exposed the "Moroccan syndicate": the "veiled men" who dictated to the ministers and diplomats and sought a war that would boost demand for arms, lands, and rail and lead to the imposition of tax on the indigenous people.[16]

In Spain, the "Tragic Week" began on Monday 26 July 1909 when the union, Solidarad Obrero, which was led by a committee of anarchists and socialists, called a general strike against the call-up of the mainly working class army reservists for the colonial war in Morocco.[17] By Tuesday, workers were in control of Barcelona, the "fiery rose of anarchism," troop trains had been halted, trams overturned, communications cut and barricades erected. By Thursday, fighting broke out with government forces, and over 150 workers were killed in the street fighting.

The reservists were embittered by disastrous previous colonial campaigns in Cuba, the Philippines, and Puerto Rico,[18] but the Tragic Week must be understood as an anti-imperialist uprising situated within a long tradition of anarchist anti-imperialism in Spain. The "refusal of the Catalonian reservists to serve in the war against the Riff mountaineers of Morocco," "one of the most significant" events of modern times,[19] reflected the common perception that the war was fought purely in the interests of the Riff mine-owners,[20] and that conscription was "a deliberate act of class warfare and exploitation from the centre."[21]

In 1911, the newly founded, anarcho-syndicalist, National Confederation of Labour (CNT), successor to Solidarad Obrero, marked its birth with a general strike on the 16 September in support of two demands: defence of the strikers at Bilbao and opposition to the war in Morocco.[22] Again, in 1922, following a disastrous battle against the forces of Abd el-Krim in Morocco in August, a battle in which at least 10,000 Spanish troops died, "the Spanish people were full of indignation and demanded not only an end to the war but also that those responsible for the massacre and the politicians who favoured the operation in Africa be brought to trial", expressing their anger in riots, and in strikes in the industrial regions.[23]

In the Cuban colonial war (1895-1904), the Cuban anarchists and their unions joined the separatist armed forces, and made propaganda amongst the Spanish troops. The Spanish anarchists, likewise, campaigned against the Cuban war amongst peasants, workers, and soldiers in their own country.-[24] "All Spanish anarchists disapproved of the war and called on workers to disobey military authority and refuse to fight in Cuba," leading to several mutinies amongst draftees.[25] Opposing bourgeois nationalism and statism, the anarchists sought to give the colonial revolt a social revolutionary character. At its 1892 congress in Cuba, the anarchist Workers' Alliance recommended that the Cuban working class join the ranks of "revolutionary socialism" and take the path of independence, noting that

"....it would be absurd for one who aspires to individual freedom to oppose the collective freedom of the people...."[26]

When the anarchist Michele Angiolillo assassinated the Spanish President Canovas in 1897 he declared that his act both in revenge for the repression of anarchists in Spain and retribution for Spain's atrocities in its colonial wars.[27]

In addition to its role in the anti-colonial struggle, the anarchist-led Cuban labour movement played a central role in overcoming divisions between black, white Cuban, and Spanish-born workers. The Cuban anarchists "successfully incorporated many nonwhites into the labour movement, and mixed Cubans and Spaniards in it", "fostering class consciousness and helping to eradicate the cleavages of race and ethnicity among workers."[28]

The Workers Alliance "eroded racial barriers as no union had done before in Cuba" in its efforts to mobilise the "whole popular sector to sustain strikes and demonstrations."[29] Not only did blacks join the union in "significant numbers," but the union also undertook a fight against racial discrimination in the workplace. The first strike of 1889, for example, included the demand that "individuals of the coloured race able to work there."[30] This demand reappeared in subsequent years, as did the demand that blacks and whites have the right to "sit in the same cafes," raised at the 1890 May Day rally in Havana.[31]

The anarchist periodical El Producter, founded in 1887, denounced "discrimination against Afro-Cubans by employers, shop owners and the administration specifically." And through campaigns and strikes involving the "mass mobilisation of people of diverse race and ethnicity," anarchist labour in Cuba was able to eliminate "most of the residual methods of disciplining labour from the slavery era" such as "racial discrimination against non-whites and the physical punishment of apprentices and dependientes." [32]

In Mexico, anarchists led Indian peasant risings such as the revolts of Chavez Lopez in 1869 and Francisco Zalacosta in the 1870s. Later manifestations of Mexican anarchism and anarcho-syndicalism, such as the Mexican Liberal Party, the revolutionary syndicalist "House of the Workers of the World" (COM) and the Mexican section of the Industrial Workers of the World (IWW), Mexican anarchism and revolutionary syndicalism continually challenged the political and economic dominance of the United States, and opposed racial discrimination against Mexican workers in foreign-owned enterprises, as well as within the United States.[33]

In the 1910s, the local IWW's focus on "'bread and butter' issues combined with the promise of future workers' control struck a responsive chord among workers caught up in a nationalist revolution that sought to regain control from foreigners the nation's natural resources, productive systems and economic infrastructure".[34]

In Nicaragua, Augustino Cesar Sandino (1895-1934), the leader of the Nicaraguan guerrilla war against the United States' occupation between 1927-33, remains a national icon. Sandino's army's "red and black flag had an anarcho-syndicalist origin, having been introduced into Mexico by Spanish immigrants." [35]

Sandino's eclectic politics were framed by a "peculiar brand of anarcho-communism,"[36] a "radical anarchist communism"[37] "assimilated .... in Mexico during the Mexican revolution" where he received "a political education in syndicalist ideology, also known as anarchosyndicalism, libertarian socialism, or rational communism."[38]

Despite political weaknesses, Sandino's movement, the EDSNN, moved steadily leftwards as Sandino realised that "only the workers and the peasants will go all the way to the end" in the struggle. There was thus increasing emphasis on organising peasant co-operatives in the liberated territories. The US forces were withdrawn in 1933 and the EDSNN largely demobilised. In 1934 Sandino was murdered and the collectives smashed on the orders of General Somoza, the new, pro-US ruler.

In Italy in the 1880s and 1890s "anarchists and former anarchists" "were some of the most outspoken opponents of Italian military adventures in Eritrea and Abyssinia."[39] The Italian anarchist movement followed these struggles with a significant anti-militarist campaign in the early twentieth century, which soon focussed on the Italian invasion of Libya on 19 September 1911.

Augusto Masetti, an anarchist soldier who shot a colonel addressing troops departing for Libya whilst shouting "Down with the War! Long Live Anarchy!" became a popular symbol of the campaign; a special issue of the anarchist journal L'Agitatore supporting his action, and proclaiming, "Anarchist revolt shines through the violence of war," led to a roundup of anarchists. Whilst the majority of Socialist Party deputies voted for annexation,[40] the anarchists helped organise demonstrations against the war and a partial general strike and "tried to prevent troop trains leaving the Marches and Liguria for their embarkation points."[41]

The campaign was immensely popular amongst the peasantry and working class[42] and by 1914, the anarchist-dominated front of anti-militarist groups - open to all revolutionaries - had 20,000 members, and worked closely with the Socialist Youth.[43]

When Prime Minister Antonio Salandra sent troops against anarchist-led demonstrations against militarism, against special punishment battalions in the army, and for the release of Masetti on the 7 June 1914,44 he sparked off the "Red Week" of June 1914,45 a mass uprising ushered in by a general strike led by anarchists and the Italian Syndicalist Union (USI). Ancona was held by rebels for ten days, barricades went up in all the big cities, small towns in the Marches declared themselves self-governing communes, and everywhere the revolt took place "red flags were raised, churches attacked, railways torn up, villas sacked, taxes abolished and prices reduced."[46] The movement collapsed after the Italian Socialist Party's union wing called off the strike, but it took ten thousand troops to regain control of Ancona.[47] After Italy entered the First World War in May 1915, the USI and the anarchists maintained a consistently anti-war, anti-imperialist position, continuing into 1920, when they launched a mass campaign against the Italian invasion of Albania and against imperialist intervention against the Russian Revolution.[48]

In Ireland, to cite another case, the revolutionary syndicalists James Connolly and Jim Larkin sought to unite workers across sectarian religious divides in the 1910s, aiming at transforming the Irish Transport and General Workers' Union, which they led, into a revolutionary "One Big Union."[49] Socialism was to be brought about through a revolutionary general strike: "they who are building up industrial organisations for the practical purposes of to-day are at the same time preparing the framework of the society of the future .... the principle of democratic control will operate through the workers correctly organised in .... Industrial Unions, and the .... the political, territorial state of capitalist society will have no place or function...."[50]

A firm anti-imperialist, Connolly opposed the nationalist dictum that "labour must wait," and that independent Ireland must be capitalist: what would be the difference in practice, he wrote, if the unemployed were rounded up for the "to the tune of 'St. Patrick's Day'" whilst the bailiffs wore wear "green uniforms and the Harp without the Crown, and the warrant turning you out on the road will be stamped with the arms of the Irish Republic"?[51] In the end, he insisted, "the Irish question is a social question, the whole age-long fight of the Irish people against their oppressors resolves itself, in the final analysis into a fight for the mastery of the means of life, the sources of production, in Ireland."[52]

Connolly was sceptical of the very ability of the national bourgeoisie to consistently fight against imperialism, writing it off as a sentimental, cowardly, and anti-labour bloc, and he opposed any alliance with this layer: the once-radical middle class have "bowed the knee to Baal, and have a thousand economic strings .... binding them to English capitalism as against every sentimental or historic attachment drawing them toward Irish patriotism," and so, "only the Irish working class remain as the incorruptible inheritors of the fight for freedom in Ireland."[53] Connolly was executed in 1916 following his involvement in the Easter Rising, which helped spark the Irish War of Independence of 1919-1922, one of the first successful secessions from the British Empire.

A final example bears mentioning. The anarchist movement emerged in East Asia in the early twentieth century, where it exerted a significant influence in China, Japan and Korea. With the Japanese annexation of Korea in 1910, opposition to the occupation developed in both Japan and in Korea, and spilled over into China. In Japan, the prominent anarchist Kotoku Shusui was framed and executed in July 1910, in part because his Commoner's Newspaper campaigned against Japanese expansionism.[54]

For the Korean anarchists, the struggle for decolonisation assumed centre-stage in their political activity: they played a prominent part in the 1919 rising against Japanese occupation, and in 1924 formed the Korean Anarchist Federation on the basis of the "Korean Revolution Manifesto" which stated that

"we declare that the burglar politics of Japan is the enemy for our nation's existence and that it is our proper right to overthrow the imperialist Japan by a revolutionary means".[55]

The Manifesto made it clear that the solution to this national question was not the creation of a "sovereign national State" but in a social revolution by the peasants and the poor against both the colonial government and the local bourgeoisie.

Further, the struggle was seen in internationalist terms by the Korean Anarchist Federation, which went on to found an Eastern Anarchist Federation in 1928, spanning China, Japan, Taiwan, Vietnam and other countries, and which called upon "the proletariat of the world, especially the eastern colonies" to unite against "international capitalistic imperialism". Within Korea itself, the anarchists organised an underground network, the Korean Anarcho-Communist Federation, to engage in guerrilla activity, propaganda work and trade union organising.[56]

In 1929, the Korean anarchists founded an armed liberated zone, the Korean People's Association in Manchuria, which brought together two million guerrillas and Korean peasants on the basis of voluntary farming co-operatives. The Korean People's Association in Manchuria was able to withstand several years of attacks by Japanese forces and Korean Stalinists backed by the Soviet Union before being forced underground.[57] Resistance continued throughout the 1930s despite intense repression, and a number of joint Sino-Korean operations were organised after the Japanese invasion of China in 1937.[58]

Anarchists cannot be 'neutral' in any fight against imperialism. Whether it is the struggle against the third world debt, the struggle against the Israeli occupation of Palestine, or opposition to US military attacks on the Middle East, we are not neutral, we can never be neutral. We are against imperialism.

But we are not nationalists. We recognise that imperialism is itself rooted in capitalism, and we recognise that simply replacing foreign elites with local elites will not solve the problem in a way that is fundamentally beneficial for the working class and peasantry.

Establishing new nation-states means, in effect, establishing new capitalist states that, in turn, serve the interests of the local elite at the expense of the working class and peasantry. Thus, most nationalist movements that have achieved their goals have turned on the working class once in power, crushing leftists and trade unionists with vigour. In other words, internal oppression continues in new forms.

At the same time, imperialism cannot be destroyed by the formation of new nation-states. Even independent nation-states are part of the international state system, and the international capitalist system, a system in which the power of imperialist states continues to set the rules of the game. In other words, external repression continues in new forms.

This means that the new states - and the local capitalists that control them- soon find themselves unable to fundamentally challenge imperialist control and instead set about trying to advance their interests within the overall framework of imperialism. This means that they maintain close economic ties with the western centres, whilst using their own state power to build up their own strength, hoping, eventually, to graduate to imperialist status themselves. In practice, the most effective way for the local ruling classes to develop local capitalism is to crush labour and small farmers in order to be able to sell cheap raw materials and manufactured goods on the world market.

This is no solution. We need to abolish imperialism, so creating conditions for the self-government of all people around the world. But this requires the destruction of capitalism and the state system. At the same time, our struggle is a struggle against the ruling classes within the third world: local oppression is no solution. The local elites are an enemy both within national liberation movements and even more so after the formation of new nation-states. It is only the working class and peasantry who can destroy imperialism and capitalism, replacing domination by both local and foreign elites with self-management and social and economic equality.

Hence, we are for working class autonomy and unity and solidarity across countries, across continents, and for the establishment of an international anarcho-communist system through the self-activity of the global working class and peasantry. As Sandino said, "In this struggle, only the workers and peasants will go all the way to the end."

 

Lucien van der Walt is an anarchist activist based in Johannesburg, and involved in struggles and movements against privatisation, neo-liberalism and racism. Contact him through the bikisha@mail.com (Bikisha Media Collective, South Africa) address if you are interested in reprinting this text.

This article was translated into French as Pour une histoire de l'anti-impérialisme anarchiste

Dans cette lutte, seuls les ouvriers et les paysans iront jusqu'au bout.

 

Footnotes
[1] Cited in D. Geurin, 1970, Anarchism, Monthly Review, p. 68
[2] ibid.
[3] Geurin, 1970, op cit., p. 68
[4] M. Bakunin, [1866] "National Catechism," in S. Dolgoff (editor), 1971, Bakunin on Anarchy, George Allen and Unwin, London, p. 99.
[5] Bakunin, [1873], "Statism and Anarchy," in S. Dolgoff (editor), 1971, op cit., pp. 341-3
[6] ibid.
[7] Cited in S. Cipko, 1990, "Mikhail Bakunin and the National Question," in The Raven, 9, (1990), p. 3 p. 11.
[8] http://members.tripod.com/~stiobhard/east.html
[9] G. Woodcock, 1975, Anarchism: a History of Libertarian Ideas and Movements. Penguin, pp. 236-8
[10] H. Oliver, 1983, The International Anarchist Movement in Late Victorian London, Croom Helm, London/ Rowman and Littlefield, New Jersey, p. 15
[11] V. Richards, 1993, Malatesta: Life and Ideas, Freedom Press, London, p. 229
[12] Ibid.; P. Marshall, 1994, Demanding the Impossible: a history of anarchism, Fontana, p. 347
[13] D. Poole, 1981, "Appendix: About Malatesta", in E. Malatesta, Fra Contadini: a Dialogue on Anarchy, Bratach Dubh Editions, Anarchist Pamphlets no. 6, London, p. 42
[14] From Sail Mahomed, 1994, Appels Aux Travailleurs Algeriens, Volonte Anarchiste/ Edition Du Groupe Fresnes Antony, Paris (Edited by Sylvain Boulouque).
[15] From Sylvain Boulouque, 1994, "Sail Mohamed: ou la vie et la revolte d'un anarchiste Algerien" in Mahomed, 1994, op cit.
[16] F.D., 27 April 1911, "Le Syndicait Marocain," in Le Bataille Syndicaliste, number 1
[17] R. Kedward, 1972, The Anarchists: the men who shocked an era, Library of the Twentieth Century, p. 67
[18] Kedward 1971, op cit., p. 67
[19] Nevinson was an English critic of imperialism; the quote is from 1909. Cited in P. Trewhela, 1988, "George Padmore: a critique, "in Searchlight South Africa, volume 1, number 1, p. 50
[20] B, Tuchman, cited in Trewhela, 1988, op cit., p. 50.
[21] Kedward 1971, op cit., p. 67
[22] M. Bookchin, 1977, The Spanish Anarchists: the heroic years 1868-1936 (Harper Colophon Books: New York, Hagerstown, San Francisco, London, 1977, p. 163
[23] A. Paz, 1987, Durruti: the People Armed, Black Rose, Montreal, p.39
[24] J. Casanovas, 1994, Labour and Colonialism in Cuba in the Second Half of the Nineteenth Century, Ph.D. thesis, State University of New York at Stony Brook
[25] ibid., p. 436.
[26] F. Fernandez, 1989, Cuba: the anarchists and liberty, ASP, London, p. 2.
[27] Casanovas, 1994, op cit., p. 436
[28] Casanovas, 1994, op cit., p. 8
[29] ibid., p. 366.
[30] ibid., p. 367.
[31] ibid., pp. 381, 393-4.
[32] J. Casanovas, 1995, "Slavery, the Labour Movement and Spanish Colonialism in Cuba, 1850-1890", International Review of Social History, number 40, pp. 381-2. These struggles are detailed in Casanovas, 1994, op cit., chapters 8 and 9.
[33] See, inter alia, N. Caulfield, 1995, "Wobblies and Mexican Workers in Petroleum, 1905-1924", International Review of Social History, number 40, p. 52, and N. Caulfield, "Syndicalism and the Trade Union Culture of Mexico" (paper presented at Syndicalism: Swedish and International Historical Experiences, Stockholm University: Sweden, March 13-4, 1998); J. Hart, 1978, Anarchism and the Mexican Working Class, 1860-1931, Texas University Press
[34] Caulfield, 1995, op cit.; Caulfield, 1998, op cit.
[35] D.C. Hodges, The Intellectual Foundations of the Nicaraguan Revolution, cited in Appendix, "The Symbols of Anarchy", The Anarchist FAQ, http://flag.blackened.net/intanark/faq/.
[36] ibid.
[37] See Navarro-Genie, Sin Sandino No Hay Sandinismo: lo que Bendana pretende (unpublished mimeo: n.d.).
[38] A. Bendana, 1995, A Sandinista Commemoration of the Sandino Centennial (speech given on the 61 anniversary of the death of General Sandino, held in Managua's Olaf Palme Convention Centre, distributed by Centre for International Studies, Managua)
[39] C. Levy, 1989, "Italian Anarchism, 1870-1926", in D. Goodway (editor), For Anarchism: history, theory and practice, Routledge, London/ New York, p. 56.
[40] G. Williams, 1975, A Proletarian Order: Antonio Gramsci, factory councils and the origins of Italian communism 1911-21, Pluto Press, pp. 36-7
[41] Levy, 1989, op cit., p. 56; Williams, 1975, op cit., p. 37
[42] ibid. p. 35
[43] Levy, 1989, op cit., p. 56
[46] ibid., pp. 56-7; Williams, 1975, op cit., pp. 51-2. The quote is from Williams.
[47] ibid., p. 36
[48] See, inter alia, Levy, 1989, op cit., pp. 64, 71; Williams, 1975, op cit.
[49] On Connolly and Larkin, see E. O'Connor, 1988, Syndicalism in Ireland, 1917-23, Cork University Press, Ireland. I do not intend to enter into a detailed debate over Connolly in this paper, except to state that the recurrent attempts to appropriate Connolly for Stalinism, Trotskyism and/ or the Marxist tradition, more generally - not to mention Irish nationalism and/or Catholicism - are confounded by Connolly's own unambiguous views on revolutionary unionism after 1904: see the materials in collections such as O. B. Edwards and B. Ransom (editors), 1973, James Connolly: selected political writings, Jonathan Cape: London
[50] J. Connolly, 1909, "Socialism Made Easy," Edwards and Ransom (editors), op cit., pp. 271, 274
[51] Connolly, [1909], op cit., p. 262
[52] J. Connolly, Labour in Irish History (Corpus of Electronic Texts: University College, Cork, Ireland [1903-1910]), p. 183
[53] Connolly, [1903-1910], op cit., p. 25
[54] Ha Ki-Rak, 1986, A History of Korean Anarchist Movement, Anarchist Publishing Committee: Korea, pp. 27-9
[55] Ha, 1986, op cit., pp. 19-28
[56] Ha, 1986, op cit., pp. 35-69
[57] Ha, 1986, op cit., pp. 71-93.
[58] Ha, 1986, op cit., pp. 96-11358 Cited in D. Geurin, 1970, Anarchism, Monthly Review, p. 68




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



"Every exclusively political revolution - be it in defence of national independence or for internal change.... - that does not aim at the immediate and real political and economic emancipation of people will be a false revolution. Its objectives will be unattainable and its consequences reactionary." [4]



"....it would be absurd for one who aspires to individual freedom to oppose the collective freedom of the people...."[26]



"we declare that the burglar politics of Japan is the enemy for our nation's existence and that it is our proper right to overthrow the imperialist Japan by a revolutionary means".[55]

