      Human Superiority
Underpinning the frantic, unsyncopated clatter of civilisation 
lie the deep, constant rhythms of nature. 
Without these powerful earth rhythms, 
our directionless babble would be all there was; 
an earth populated by humans 
surrounded by their genetic slaves and mutants, 
the cattle, grains and rows of identical trees. 
Knowledge without wisdom, 
information without understanding. 
And managers 
Everywhere.

The legions of would-be planet managers who misread James Lovelock’s Gaia hypothesis (if they read it at all) believe that we are on the verge of a new phase in evolution in which the earth will become a superconcious single entity. This is not a new idea, except to those who have spent too long in further education, submerged in economics and so on. In various forms it is as old as language.

On its own, this idea would not really appeal to the egotistic nature of these people, so new agers have decided that if it is true, then they must be the brain of the new being. They will be the ones who make all the decisions, using the new tools. New agers believe that “Our ‘satellite vision’ means that all the planet’s resources — soils, forests, rivers, oceans, minerals — can be not only mapped in fine detail, but vetted for pollution, erosion or drought; for changes in albedo or humidity; for movements of shoaling fish or migratory creatures.” [1] The fact that one of the things these satellites will be monitoring will be the devastation caused to the rainforests by their own launch and support facilities is ignored.

The idea is that we will, through biotechnology and all the other things, be able to run the planet as a very efficient economic system. It’s not really surprising that the people who have learned to make leisure an indispensable part of their economy, and advertising into a respectable full time profession worth more than health care, should want to make the world as simple as possible. What I do find surprising is that a species which has survived the last few millions of years by virtue of its intelligence alone should allow these individuals to be its pack leaders.

It was the predecessors of these global management merchants who promised us unlimited free electricity from nuclear power “too cheap to meter” and a solution to all our food problems via the “green revolution” of chemical farming. Now that they have so obviously failed to deliver the goods, and made a hideous mess of things into the bargain, why do we still tolerate them? They come from the same tradition as those who transformed the wolf into the pathetically sick parody that is the poodle — are they to be entrusted now with the entire planet?

According to this economic evangelism, the global communication system we are setting up is to function as the central nervous system of the earth, with billions of messages buzzing around continuously. The whales and dolphins have had a global communication system, probably for millions of years. Their ‘songs’ are enormously long and complex, and use a range of frequencies far wider than our ears can detect. They can ‘talk’ to each other across whole oceans (or they could until our noisy ships started trading vast quantities of consumer baubles all over the place). They don’t have machines because they don’t need them. I’ve no idea what they talk about, but you can bet it’s nothing like the inconsequential drivel that dominates the internet.

I’m not suggesting that the idea that the earth may be more than just a lump of lifeless rock is wrong. On the contrary, I believe that there is a strong possibility that we are already part of a global life form. Why do geese fly at 20,000 feet? How and why do some animals migrate such vast distances? I don’t know the answers to these questions, but the answers I am given by experts are far from satisfying and they are only theories, though some of their proponents seem to forget this important fact. The way in which salmon travel vast distances and return to their rivers to spawn and die reminds me of the way in which nutrients are transported around the body. There is so much that we just don’t know about the planet we are supposedly about to manage.

Yep. It seems to me that the only purposes we are capable of comprehending are those that pertain to ourselves. We see a purpose in trees as a carbon sink to absorb our foul emmisions, or as fuel or for pleasure. We see other plants as sources of food or medicine, or food for the animals we eat. Every purpose we percieve is geared to us. But what purpose does the tree have? or the deer? Does the deer view the tree as a source of food? Of course it does. It is quite understandable that we, just like any other life form, should view the world in terms of our purposes.

But for one who aspires to the role of planetary manager, with the means to enforce this position with or without the ability to fulfill it, somethiing more is needed. A wider vision of a wider purpose, and the humility to accept that every living thing is a part of the whole, and the loss of every one is a loss to the whole, no matter whether we know what the part was.

This is not to say that we should not kill anything, but that if we are to raise ourselves to a level of physical power unprecedented in the time of life on earth, then we must also raise ourselves to mentally and spiritually be able to cope with it. If we leave the eternal now of animal living behind, we must also leave behind the attitudes that go with it, for they will not serve us in the new world we are entering. They will destroy us.

As we assume ourselves omnipotent we become obsessed with our own mortality. Indeed we even go so far as to deny it and attempt to obliterate everything that threatens to shatter that illusion.We alone of all animals have voluntarily lowered the quality of our lives in order to buy off death for a few years. We left eden of our own volition. Even the myth tells us that. Our mortality is a part of our life, and in the process of defending our illusion what we are actually destroying is life itself. Our subcoscious knows this and the knowledge peeps through in words like ‘antibiotic’ and ‘biocide’ — hostile to life and life-killing — truths veiled by the mists of our history from a time of myth-knowledge before science, to creep through and assert itself in the midst of our language of self importance like a Freudian tit. (sorry, I always call it that, it amuses me — sad eh?).

Instead of using science — possibly our greatest tool — to further knowledge and widom, we subvert it to the furtherance of our petty obsessions. Our animal instincts are still very much in charge, and there is not much time left to decide which way to go — if indeed we still have a choice.

The new agers believe we are on the threshold of a new form of consciousness, but they are sadly deluded, they allow this prospect to feed their self-importance, and think they understand as they meditate and dream of a world run on hemp. The technophiles are lost in the intricacies of the tools they have made, and the mechanisms of their ever more complex models. In their own way they too are contemplating their navels. Know thy tools — know thyself.

Meanwhile mother earth grows restless. The child must leave the womb eventually, dead or alive, weak or strong, ready or not.And the contractions are starting.

Underlying all this is the assumption that humans are the pinnacle of evolution, the summation of all the billions of years since life first wriggled its DNA in the primeval swamp (oh, sorry... RNA was it? I wasn’t there at the time).

At first, the justification for this view was that our brains are so much bigger than most other animals. When it was pointed out that elephants, for example, have bigger brains we decided it must be brain size in relation to body weight, thus cleverly still excluding other primates. (Why should it take more brain power to operate a body that is not more complex, only bigger? In fact I know a few people who show quite clearly that this is not the case!) Unfortunately this didn’t exclude the dolphins, so the argument shifted to language. Research then showed that many other animals do have language, but this has now been overcome by asserting that they cannot master syntax (I jest not — this is the subject of much serious animal research).

Before we go any further, I will just say quite categorically that I am opposed to anything other than non-invasive observation. I have the healthiest loathing for people who consider themselves important enough to regard other forms of life as theirs to play with. The experiments and their results illustrate the futility of their quests and the unscientific nature of their basic assumptions.

The first explorers in the ‘new world’ couldn’t even recognise other humans as intelligent or sentient beings, in fact they weren’t even considered human for a long time. For hundreds of years the vast majority of people in the civilised world concurred with this view. Even now, many people hold bigoted views about other races of humanity. What hope, then, does any other species have? The attitudes of people doing research on animals now is the same as that of the slave owners then, and remember, there are more slaves in the world now than there were when slavery was abolished in the nineteenth century. [2]

The main thrust of research into animal intelligence now seems to be the obsession with language. Not just any old language, mind, but human language. If the poor chimps in those cages are going to get any respect from their masters, they’re going to have to fill in forms just like anybody else, then they will have to queue up with all the other non-white, non anglo-saxon heathens and wait patiently for their ‘rights’ to drop from the table.

Those clever chimps who managed to master some human language were immediately confronted by the next obstacle: syntax. When researchers excitedly told their peers that they had primates who could communicate using a version of American Sign Language, the response was not an immediate demand that they be released and their homelands protected from being turned into aluminium for coca cola cans and pulpwood for research papers; they were merely told that this was not in itself a sign of any real level of language, and that they would need to be able to put words into coherent sentences to show this (another way of saying ‘more research is necessary’ ie. grants).

The fact that no human researcher has ever managed to communicate with another animal in its own language is ignored. It is simply assumed that they don’t have anything but a rudimentary system of signals. This is getting away from the argument about superiority though, as it is intended to do. Even if we stay with this silly argument about language, we can still show that there is no proof that we are more important than other animals, because it can’t be proven that they don’t have language abilities similar in extent to our own.

A fax machine will transmit a large amount of information over the telephone lines, a computer hooked up to a fibre optic network vastly more. Within a couple of seconds, many thousands of words can be communicated, all in a short burst of noise. If you slow down a recording of dolphins’ voices, the similarity is undeniable (explainable by an expert, no doubt, but undeniable nevertheless). I’m not suggesting that dolphins send each other faxes, only that the sounds they make have a similar but far more refined format. Our languages are made up of words which in turn are made from a limited number of sounds. There is no reason for this to be the only way of making a language. Indeed, if other animals do use language, it must be done in a different way, otherwise we would have noticed. Also aborigine languages are vastly different from our own, and virtually beyond our comprehension unless we live with them for many years and change our fundamental world view. These are fellow humans; how would we even recognise language in another species?

Researchers at the Siberian Academy of Sciences did a study of communication between scouts and foraging parties of worker ants. [3] The scouts are able to communicate the route to food so that the others can find it. The researchers removed the possibility of the workers following the scent of the scout. They didn’t find out the method of communication though. (to say that ants use their antennae or body movements is rather like saying humans use sound or mouth movements — it doesn’t explain much). The description of a route with a degree of accuracy sufficient to enable others to find something requires something more than grunts of anger or contentment. Whether it qualifies for the award of linguistic merit from the humans is relevant only to the humans. The fact that something the size of an ant can do this is significant.

The basic unit of our language, the noises we put together, are called phonemes. We have around 30 of them. African milkweed butterflies apparently have over 200 different chemicals to choose from. The researchers who discovered this said that they do it “in order to make pheromones for recognising each other.” [4] This statement reminded me of a poster from BBC Wildlife magazine purporting to show “emperor penguins searching for squid”. How do they know? I have a picture of some people walking across a sunny meadow, perhaps I’ll call it “humans searching for a video shop”.

I’m only pointing out that it is possible that other animals use other ways to communicate things to each other that are equal to or better than ours in their efficiency, not that they necessarily do. I only want to get rid of the assumption that they don’t.

We assure ourselves that we are justified in our domination of the planet by saying that we are more important than anything else. Traffic could easily drive more slowly (or preferably not at all) instead of killing countless millions of animals, but humans have decided that their merest impulse, even, is more important than pregnant badgers or foxes with families to support.

The arguments about language or intelligence, or even sentience (now there’s a good one), merely serve to avoid the admission that we are arrogant and we rule the planet by pure brute force. The cries we often hear of ‘but it’s natural for the strongest to rule’, usually accompanied by a smug smile, only point out the obvious. It doesn’t make us superior, only equal. The only superiority we show at the moment is our vastly superior ability to kill, maim and destroy, and that is the last thing the new age mantra mumbling students of the umbilicus would like to admit.

Other than this, we have no right to dominate the planet and wipe out or adapt to our own petty whims the whole panoply of forms that life on this planet has manifested itself in. Whether or not these other species have bigger brains, smaller fingernails or automated shopping trolleys built into their thoraxes has nothing to do with it.

All the arguments that people use to defend their plundering of the wild are based on unfounded presumptions. Unfortunately, humans have the power to take what they want, so it is up to some of us to speak and act on the behalf of other life. My basic assumption (unfounded as it may be) is that they would like some share in the future of this planet, that they want to live, and to do so without interference. If the new age is to be any different from this one, the human part of it must respect all other forms of life, and must really learn to tread lightly upon the earth, rather than just pay lip service to the idea when it suits them.

 
[1] Norman Myers, “An Atlas of Planetary Management”
[2] See “Children Enslaved” by Roger Sawyer, Routledge 1988
[3] BBC Wildlife, Nov.1991.
[4] New Scientist, 19 Feb. 1994.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



Underpinning the frantic, unsyncopated clatter of civilisation 
lie the deep, constant rhythms of nature. 
Without these powerful earth rhythms, 
our directionless babble would be all there was; 
an earth populated by humans 
surrounded by their genetic slaves and mutants, 
the cattle, grains and rows of identical trees. 
Knowledge without wisdom, 
information without understanding. 
And managers 
Everywhere.

