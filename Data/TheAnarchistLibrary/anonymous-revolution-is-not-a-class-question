
The examination and consideration of certain demagogic attitudes, such as that involving the bolshevik slogan on the unity of the proletariat, have brought us, anarchists, again face to face with a question quite easy to resolve: the idea of classes and class struggle. We have not given any basic theoretical conclusions to this problem; we’ve done nothing more than put the marxist conception in doubt, criticizing its foundations and, perhaps, preparing the terrain for a few of our own that will someday seriously deal with the subject from a libertarian point of view.

Whatever the cost to our natural differences with marxist doctrine, we must recognize that many of our ideas come from Marx, from whom — while denying him certain basic ethical qualities and attributing to him inordinate authoritarian ambitions — we cannot take away the credit for having created a social system in the German style, i.e., precisely elaborated, with an answer to every question and a theory for every stance.

Early anarchists accepted Marx’s economic doctrines well before marxists appeared; but, as Malatesta said, if we aren’t deceiving ourselves, this is because no time was left for dealing with these questions ourselves. The years have passed, and marxism, in its political aspect, was totally eradicated from the anarchist environment; meanwhile its imprint remains in economic affirmations, and if one can compromise with it so long as reality doesn’t demand clear and definite positions or concrete answers, the moment arrives when we notice the contradictions and feel the need to stick with our own ideas and subordinate marxism to the libertarian conception of revolution and social life.

In our opinion, the idea of class contradicts the principles maintained by anarchism. We believe that we see in it the last refuge of authoritarianism; we brag about having removed the influence of political parties from the workers’ movement, but by allowing the idea of class to ferment, we prepare the terrain for a new form of domination. Syndicalism has served marvelously for doing this. Syndicalists, even those who claim to be libertarian, see the world through the one-sided prism that places one class ahead of the other; they have created a fixed idea of exploiters and exploited, of capitalists and wage laborers, and instead of confirming through the examination of real life the content of this idea, the existence of the homogeneity of classes in struggle, they make the opposite operation.

If everyone of those who is active and takes his place in social and revolutionary struggle asks himself why he acts in a given way, she won’t respond as a member of a social class, but as the partisan of an idea. When we embark on an action against capitalists or against the state, we do it more for our conceptions of justice, equality and freedom than as members of an economic class. Individual or collective poverty can be a stimulus to rebellion, to the consideration of current ills, to the search for remedies, a thing that we don’t do as workers, but as human beings. Corporatist and marxist reformists have made the possible everything because the thought of workers is in agreement with the craft the they practice and not with their human condition.

Furthermore, daily life offers us a spectacle that is the complete opposite of the struggle of the exploited against the exploiters; the struggle that we observe is that of the exploited against… themselves; very rarely the privileged resort to direct action, and generally they make use of the ignorance, of the poverty, etc. of the subordinate to defend their positions and put one exploited person up against the other.

Syndicalists say: «All workers, all wage laborers must unite in common struggle against the common enemy, the capitalist; the interests of all workers are the same, all workers are brothers!».

We doubt that the interest of the striker is identical to that of the scab, that the interest of the waged factory worker is equal to that of the waged police, or that the interest of the revolutionary worker is on par with that of the christian worker; far from seeing the existence of general lines of common struggle among wage laborers, we notice the greatest division, and we anarchists should not fight against this division (which will be as one will see equally artificial and inconsistent) in the name of supposed common class interests, but in the name of human interests. We should not repeat, like the syndicalists, «all workers are brothers!», but «all men are brothers!», because the idea of class implicitly contains the idea of class domination. It is certain that the fighters of social revolution belong, have belonged and will belong almost exclusively to the working masses. It is thoroughly understandable that the rebellious part of society is that which suffers, and it is equally understandable that it is the part of society that suffers exploitation and domination that aspires and is capable of aspiring to the suppression of these basic ills for all. However, this does not authorize us to proclaim that the revolution is a class question, that the solution to the problems of social life is in accordance with the point of view of a portion of society that thinks as such and not as a fraction of humanity. Up to now history has given us plenty of examples of this racial, caste, dynastic and party exclusivism. Anarchism would experience the greatest defeat if it were to stimulate human beings to think like mechanics or peasants, like wage laborers or black people and not like human beings; beyond craft, race, color one finds humanity.

The appraisal of the value of ideas in social life is much too neglected whereas human beings are separated or united more by ideas or the lack of ideas, than by nationality, craft, color. The claim of syndicalists (supported by some anarchists) to measure human beings by work and by what they think, has always seemed a great absurdity to us. If the union has a higher mission than that of maintaining a salaried secretary, if it has an intention of struggle for a more just society, when it acts it will see endless conflicts and will have to recognize that among workers in the same craft, ideas are what determine the conduct of individuals: the christian will consider rebellion as crime, because his aim is to conquer a place in heaven, not on earth; the marxist will avoid the terrible moments of a clash with the waged police or with the soldiers of the army and will prefer to entrust the mission of defending his interests to a parliamentary representative. The anarchists will not be able to compromise with either christian resignation or the marxist panacea. We therefore see that the harmony of the unionized workers of a corporation cease as soon as one wants to do more that pay the union dues and maintain the salaried secretary.

The syndicalists also say that workers must unite on the basis of class interests; we don’t know which class interests it’s about, because it isn’t so easy to imagine defining what a class is. Of course, it is because we don’t know interests that are not linked to respective ideas and one cannot speak of interests without considering the ideas that they generate or those that have given birth to them. It is quite possible that in someone the idea of justice is born from the interest in the just, but it is also true that the interest for the just can be born from the idea of justice. In other words, freedom, for example, may be born from the interest in the free life, but it can also come before and independently of this interest. We don’t love the good only when this is united to an interest, we love it even when it is detrimental to our interests.

We have never believed in the logic of revolutionary associations based on interests and we have not been able to conceive that one could ignore ideas, without which any association is artificial.

The idea of class naturally excludes the action of ideas in the life of collectivities; the idea of class fuels historical determinism, marxist fatalism; they are inseparable. And if we are convinced that the working class is not called by fate to replace the bourgeois class or to move in any direction, we must make a new factor enter into the social movement: human will; and if we accept human will in the social movement, we will not be able to affirm that revolution is the exclusive affair of this or that class, because we will not realize the existence of this will through any one-sided prism of a party or an economic faction. In the last century, there was belief in People-Messiahs; the syndicalists have dreamed up the Class-Messiahs. We anarchists see things from a broader point of view, and we affirm that the revolution that must bring freedom and equality can not be made in the name of a class but in the name of humanity, even though firmly convinced that it will be realized almost exclusively by revolutionary workers.

We protest against the syndicalists who say that the revolution is a class question for the same reason that we protest when the bolsheviks or the social-democrats affirm that it is a question of the party, of their party.

We have seen the dictatorship of the proletariat ultimately become the dictatorship of Lenin. If the syndicalist experiment is made some day, we will see that the class idea will be limited to workers unions and, more, to executive committees, and, more still, to the most able, the most clever of these executive committees. And as Lenin could have said: «the dictatorship of the proletariat is me», we could see some synidicalist say: «the class is me».

In 1908, in the columns of Protesta (Buenos Aires), there was an interesting debate on the question of classes; the main protagonists were E. G. Gilimon, one of the most solid minds to have spent time in the editing of the old anarchist daily, and Antonio Loredo, then the editor of L’Azione Operaia from Montevideo. It would be interesting to reread the arguments of this debate. Gilimon set forth on that occasion the ideas that we have seen again in Protesta ten years later and that would deserve still wider discussion.

The idea of class cannot satisfy anarchists and we would only like to draw the attention of comrades to this, and if we lack a Marx to examine this from a libertarian point of view, we could replace the absence of a theorist with our joint efforts.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

