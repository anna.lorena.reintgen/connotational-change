
Yesterday I attended a memorial gathering for Bob McKinney. He died November 20, 2006, at the age of 75, from melanoma. His wife Carol had preceded him in death by several years. The meeting was attended by his two daughters, Beth and Christine, and their spouses and children, by his long time work partner Lenore Pereira and her two daughters, Shanakawa and Teefee, and their three children, and by various friends and comrades, many of whom knew Bob either through his work as a plumber or through his work at the Red Book Store, which was housed in the Red Sun Press building on Green Street in Jamaica Plain, Boston from 1983 to 1994. In attendance, in addition to the above, were Kathy Hoffman, Annie Hoffman, Tom Keefer, Kevin Murray, Maggie Cohen, Eric Johnson, Nancy Nichols, James Herod, Betty, Eleanor, and several people I did not know. There were about twenty of us. We sat around in a circle in a room in the Democracy Center at 45 Mt. Auburn Street in Cambridge and reminisced about Bob. It was the first such memorial meeting I had ever attended, but I thought it was an excellent way to pay tribute to someone's life.

Soon after leaving the meeting however I suddenly realized to my amazement that no one (including me) had mentioned Bob's politics, the political philosophy that had guided his life. This was a strange omission, but sort of understandable. You see, Bob was an anarchist. Those in attendance didn't really reflect Bob's political orientation, being either apolitical, Marxist, progressive populist, left-liberal, or communist. I'm reasonably sure that I was the only anarchist there. This is why I decided to write up this tribute to him. A man who led such an exemplary life, who actually practiced day by day the anarchist principles he believed in, deserves to be remembered and honored as such by anarchists as well as others. But he probably won't be. He left no writings. He belonged to no anarchist organizations. He did not participate in any great anarchist campaigns. He is not known beyond his small circle of friends in Boston. Hence this remembrance.

Bob McKinney was an absolutely unique personality. Upon that we were all agreed. I suppose that's why we all came to a gathering in his honor. As each of us began to talk about our experiences with him some aspects of that uniqueness began to become clear. He combined in ways you rarely see traits that usually are not found together in the same person. For example, he was exceptionally soft-spoken, kind, and gentle, yet this was combined with a certain moral fierceness. He was a very strong personality, but yet he was not domineering. He was uncompromising in his beliefs, yet he was strangely tolerant of those who lived differently, by different codes. He was a plumber, yet was as well-read and knowledgeable as any Harvard intellectual. He was a staunch atheist, yet somehow there was a spiritual tone to him. One friend described him as almost saint like. Another friend however brought in a picture of Bob kneeling with his head under a kitchen sink, and commented that Bob often joked that for an atheist he sure did spend a lot of time on his knees. Many people who have a keen sense of moral outrage at the injustices of the world are often confrontational, even pugnacious, fighting as they are to change the world into a better place. This was not Bob's way. He was gentle and patient. Lenore said that in all the years she worked with him she had seen him really angry only once. Among the revolutionaries, he probably most resembled Kropotkin. In a memorial meeting like this the natural tendency is to focus on the good points and pass over the bad points of someone's life, but in Bob's case there really weren't all that many bad points to have to pass over.

I worked with Bob for five years, from 1987 to 1992, in the Red Book Store in Jamaica Plain, which was Boston's only independent, radical bookstore. After finishing his day's work as a plumber he would come into the store and volunteer a few more hours to help keep it operating. I saw him several times a week, sometimes almost daily. We never became friends in the social sense, but we were pretty much on the same page politically, and worked well together in the store. Indeed, if it hadn't been for Bob and Lenore I probably wouldn't even have been admitted to the Red Book Store collective to begin with. There was some resistance. I was an older man. No one really understood my politics. But Bob and Lenore fought for me. We ended up on the same side in many of the political struggles that took place within the project during in the next five years.

One thing I liked about Bob were his hatreds. What someone hates defines them as much as what they like. He hated capitalism, the state, and God, the three traditional anarchist hatreds. But Bob also hated liberals, reformists, academics, and pretentious middle class "radicals." Naturally, he wasn't too happy with sectarian Marxists either. He had an instinctive understanding of the class structure of the country, and unerringly identified with the poor and oppressed, and opposed the oppressor. This was just as true for race and gender issues as for straight class issues. He invariably sided with women and people of color.

Another thing I liked about him is that no one could intimidate him. Some Harvard so-called radical intellectual would come into the store and start pontificating. Bob would just stand there quietly listening, and then eventually ask a couple of incisive questions and make a comment or two, and that whole bubble of hot air and jargon would just burst into thin air. It was a delight to behold.

He hated the gentrification that was then coming into full swing in Jamaica Plain and did what he could to oppose it. He hated the Community Development Corporation there, which he regarded as reformist, and an arm of the state. He hated mainstream American culture, and lived as much apart from it, off the grid, as anyone could and still function. Much of the time he didn't pay taxes, he didn't keep his plumbing license up to date, violated bureaucratic rules by the dozens, was not a good materialist consumer, and didn't pay any attention to fashions or contemporary pop culture. His hatred of the state and capitalist culture was expressed daily by not cooperating with it or participating in it.

Several friends noted what a voracious reader Bob was. He kept abreast of what was happening in the world, and was reading radical magazines right up until he died. He read the tough books too. Kevin reported that he first met Bob when Bob was in Harvard Bookstore buying a copy of Nicolas Poulantzas, Political Power and Social Class. Bob was a genuine intellectual, in the sense of someone who studies hard to understand the world he lives in and then applies that knowledge to his life. But he had little patience with what passes for intellectualism in the United States.

Bob's main work though was as a plumber. He was a very skilled craftsman, who knew his trade backwards and forwards, and knew the historical backgrounds to the materials and equipment he was using. Many of the stories about Bob's plumbing work recounted what a talented, meticulous, careful, and patient worker he was. But here is what was unique about him. He used his trade to really help people. He worked mostly on jobs for poor people. He always had time for emergency jobs, and would refuse to sign on to a long-term job, even if financially attractive, if it was so rigidly scheduled that he couldn't take time out for emergencies. He refused to work on large, commercial jobs. If he and Lenore were absolutely broke they would sometimes take a job with a more middle class client who could pay, but they did this only under duress. He spent a lot of time on jobs for various progressive organizations, who never could pay much. The result was that Bob lived in poverty most of his life. He was always giving away work, to people who needed it.

The poverty was exacerbated however by his slowness, even reluctance, to collect the money that was owed him. Many people commented on this trait of his, and what a puzzle it was, and what misery it caused him. But that's just the way he was. Lenore told a typical story. They had spent two days of very hard work in a freezing basement putting in a new furnace, so their client would have heat, only to find that when they finished and went upstairs to collect their check, the client told them that because of Christmas expenses she wouldn't have the money to pay them until later. Bob didn't raise a stink about it or get mad, but just let it slide.

Another story told of how Bob spent an afternoon fixing a broken kitchen sink. On his way out, the owner mentioned that her oven door wasn't working. So Bob went back in, took a look at it, and soon was busy taking the whole thing apart, fixing it, and putting it back together again, with the result being that he got home several hours later, late in the evening. He never got paid for either job.

Then there was Tommy. Tommy was different, learning impaired as they now say. Tommy worked with Bob and Lenore for years. It was only because of Bob that he was let out of an institution. He went with them everywhere and helped out as best he could.

Well, there were many other stories told, about his radical environmentalism, about what a healthy diet he ate, about what a lean and fit man he was all his life, about what a calming influence he had been with Lenore's children when they were growing up, about his hatred of doctors, and about his unusual marriage.

All in all, a most unusual person, who led a most unusual life. Everyone said they will miss him greatly.

James Herod

December 17, 2006




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

