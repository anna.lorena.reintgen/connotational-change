
In May 1898, king Umberto I, worried about the news reaching him from Milan where a general strike had broken out, entrusted general Bava Beccaris with the task of repressing the revolt. The order is given to the soldiers to shoot at sight, and Bava Beccaris opens fire on the town with canon shot. The balance is 80 dead and 450 wounded. Proud of having done his duty, the general telegraphs the king that Milan is now ‘pacified’. The head of the government, the marquis Di Rudini, prohibits over one hundred opposition newspapers, the Bourses de Travail, socialist circles, Mutual Societies, and also at least 70 diocesain committees and 2,500 parish committees. Moreover, the universities of Rome, Naples, Padova and Bologne are closed, while thousands of arrests are made. Umberto I immediately sends a telegramme of congratulations to Bava Beccaris and decorates him with the cross of the Military Order of Savoy ‘for precious services rendered to the institutions and civilisation’. Two years later, on July 29 1800, the anarchist Gaetano Bresci relieves king Umberto I of the weight of his responsibilities by killing him in Monza. The King and the anarchist. Two assassins, their hands stained with blood, that’s undeniable. Yet, can one put them on the same level?I don’t think so, any more than one can consider the motivations and consequences of their acts in the same way. And so, because they can’t be united in a common execration, which of the two committed an act of terrorism? The king who had the crowd massacred, or the anarchist that slayed the king?

To ask oneself what is terrorism is one of those questions that it would seem pointless to ask, because it is destined to get a univoque answer. In reality — when it is formulated rigorously — it doesn’t fail to give rise to surprising reactions. The answers are actually different and contradictory. ‘Terrorism is the violence of those that fight the State’, some say, ‘Terrorism is the violence of the State’, others answer, ‘but no, terrorism is any act of political violence, no matter where it comes from’, the last point out. And all the debates that open up in the face of the distinctions that can then be made on the subject: for example, terrorism is only violence against people or can also be against things? Must it necessarily have a political motivation or is it only characterised by the panic is seminates? The multiplicity of meanings assigned to this term is suspect. The sensation here is not of finding oneself in the presence of the usual malcomprehensions linked to the incapacity of words to express a reality whose complexity goes beyond the symbols that would like to represent it. On the contrary, one gets the impression that one is face to face with deliberate confusion, a relativism of interpretations created artificially with the intention of emptying ideas of their meaning, or neutralising practical strength, banalising the whole question by reducing all reflection that one might carry out on the subject to chatter.

All the same, this nine-letter word must have an origin, a history, from which it would be possible to deduct a meaning capable of dissipating at least a good part of the ambiguities that its use generates today. And that is in fact so.

The first definition that is given of this term by most dictionaries is of an historical character: ‘the government of terror in France’. One thereby discovers the precise origin of the word. Terrorism corresponds to the period of the French Revolution that goes from April 1793 to July 1794, when the Committe of public health led by Robespierre and Saint-Just ordered a huge number of capital executions. The terror was therefore represented by the guillotine whose blade cut the head off thousands of people who, one presumes, constituted a threat for the security of the new State in formation. Starting off from this base, the same dictionaries add by extension a more general definition of terrorism: ‘all methods of government based on terror’.

At the present time this interpretation of the concept of terrorism is extremely clear. First of all, it highlights the narrow line that exists between terrorism and the State. Terrorism is born with the State, is exercised by the State, is precisely a ‘method of government’ that the State uses against its enemies to guarantee its own conservation. ‘The guillotine — said Victor Hugo — is the concretisation of law’. Only the State can promulgate laws. And law, far from being the expression of this social contract garantor of harmonious cohabitation among humans, represents the barbed wire with which power protects its privileges.Whoever dares to go beyond it will have to pass through the hands of the hangman. In fact, before the month of April 1793, some so-called common law criminals and some insurgents had already climbed the scaffold.

Whatever one might think, the guillotine is not actually an invention of monsieur Guillotin. In France this instrument of capital execution already had a history, but nobody had talked about Terror yet.It is only when the authority of the State, then in the hands of the jacobins, is threatened by a revolutionary wave, when it is no longer a question of simple outlaws or isolated insurgents, but a huge social movement capable of overthrowing it, only then does repressive violence come to be called terror’.

But, apart from its institutional character, another characteristic distinguishes terrorism: anyone can become a victim of it. During the period of the Terror there were no fewer than 4,000 executions in Paris alone. Louis Blanc found the identity of 2,750 guillotined people, discovering that only 650 of them belonged to the wealthy classes. That means that the State machine of the guillotine did not make many distinctions, decapitating anyone it considered a nuisance or suspect. It was not only noblemen, military men and priests that lost their heads these days — as the most conservative and traditional propaganda would have it — but above all simple artisans, peasants, poor people. Terrorism is such because it strikes blindly, hence the feeling of collective panic it inspires. The indiscriminate use of the guillotine, systemised thanks to the simplification of judicial procedures consented by the law of Prairial, created the ineluctable effect of chain operations, annuling the individual differences between all the decapitated. This practise of amalgam has a precise political sense: regrouping into one single seance the people suspected of ‘crimes’ of a nature or identity that were completely different. Terror aims at eliminating individual differences to create popular consensus, and to destroy ‘the abjection of the personal me’ (Robespierre), given that there must only exist one single entity into which to melt individuals: the State. Terrorism is therefore born as an institutional and indiscriminate instrument. These two aspects also retentissent in current expressions, as for example ‘terrorising bombardments’. Not only does bombardment take place during wars carried out by States, it seminates death and desolation among the whole population. One could say the same thing concerning the psychological terrorism considered ‘a form of intimidation or blackmail’ in order to manipulate public opinion, effectuated above all through the means of communication, by the exaggeration of the dangers of certain situations or even inventing them, in order to induce the masses to behave in a certain way in political, social and economic projects. One can see clearly how only those who hold power are able to manipulate the great means of communication and, through them, the ‘masses’, in order to reach their aim.

Terrorism is therefore the blind violence of the State, as the origin of the term shows clearly. But language is never a neutral expression. Far from being merely descriptive, language is above all a code. The meaning of words always points to the side on which the balance of power is leaning. He who holds power also possesses the meaning of words. That explains how it is that, over time, the concept of terrorism has taken on a new meaning that completely contradicts its historical origins but corresponds to the needs of power. Today, this concept is defined ‘a method of political struggle based on intimidatory violence (murder, sabotage, explosive attacks, etc.) generally used by revolutionary groups or subversives (left or right)’. As we can see, this interpretation, which began to spread at the end of the 19th century, is in complete opposition to what has been said until now.In the initial acceptation of the word, it is the State that has recourse to terrorism against its enemies; in the second, it is its enemies that use terrorism against the State.The upturning of meaning could not be more explicit. The usefulness of such an operation for the Reason of State is only too clthe Terror in France was the work of a state born from the Revolution.To justifythe present meaning of the concept of terrorism, the dominant ideology has had to intervertire its subjects and attribute to the Revolution the responsibility that in reality belongs to the State. Ainsi, we are taught today that Terror is the work of the Revolution which, in this far off historical context, took the form of the State. Terror is therefore synonymous with revolutionary violence. An acrobatic jump in logic that continues to enchant the parterres of spectators the world over, who don’t seem to realise de l’arnaque more than obvious.

In reality, one cannot attribute Terror to the Revolution, the insurgent people, because it is only when the Revolution becomes a state that the Terror has appeared. It is an enormous ideological lie and a gross historical error to make Terror the very expression of ‘massacrante’ revolutionary violence, that in the streets, ythe days on the barricades, of popular vengeance. Before April 17 1793 (day of the foundatio of the revolutionarytribunal), the violence exercised against power, even that which was particularly cruel, had never recouvert the name of terrorism. Neither the bloody Jacqueries of the XIV century, nor the excesses that deroule during the Great Revolution (such as for example the demonstratio of the women of Marseille who carried a la ronde, on top of a pike, the visceres of Major De Beausset to the sound of ‘who’s for tripe?’) were ever considered as acts of terrorism.This term indicates only the repressive violence of the State apparutus at the moment in which it has to defend itself — for the first time in history — from a revolutionary assault. En somme, the historic aspect of the term shows how terrorism is violence of power that defends itself from the Revolution, not Revolution attacking power.

What a social monstruosity, what chef d’oeuvre of Machiavelism is this revolutionary government! For any being that reasons, government and revolution are incompatible. 
Jean Varlet, Gare l’explosion, 15 vendemaire an III.

It should be said a ce propos that the persistence of this ambiguity has been encouraged for a long time by the revolutionaries themselves, who have accepted this qualificativ de bon gres, without realising that in so doing they were helping the propaganda of the very State that they wanted to strike. And if the concept of terrorism can legitimately find its place in an authoritarian concept of revolution (as Lenin and Stalin demonstrated in Russia), it is absolutely devoid of sense, not to say abhorrant, in an anti-authoritarian perspective of liberation. It is not by chance thast it is precisely the anarchists to have in first revu the improper use of this term, perhaps pushed by events. In 1921 the tragic attentat took place against the cinema-theatre Diana in Milan, causing the death and wounding of numerous spectators, although it had the objective the town prefect who was responsible for the imprisonment of some well-known anarchists. In spite of the authors’ intentions, it was an act of terrorism. As one can imagine, this act has led to many arguments within the anarchist movement. Ainsi, in the face of the condemnation of the gesture by many anarchists, both the revue Anarchisme of Pisa, undoubtedly the most widely distributed publication of autonomous anarchism in Italy, continued to defend ‘this cardinal anarchist truth, of knowing the impossibility of separating terrorism from insurrectionalism’, it began on the other hand to esquisser the first critical reflections on the concept of terrorism: ‘why name and tax with ‘catastrophic terror’ — which is the propre of the State — the act of individual revolt? The State is terrorist, the revolutionary who insurges, never!’ Half a century later, within a context of strong social tension, this critique was to be taken up again and developed by those who did not intend to accept the accusation of terrorism launched by the state against its enemies.

Words have always been subject to an evolution in meaning. It is not surprising that the meaning of the term terrorism has also been modified. It is all the same unacceptable that it contradict each one of its original characteristics, which are those of the institutional and indiscriminate aspectof violence. This violence can be exercised against people or against things, it can be physical or psychological, but in order to be able to speak of terrorism, there must be at least one of these two characteristics remains. For example, one has rightly spoken of terrorism to indicate actions carried out by death squads of the Spanish State against the militants of ETA. These actions were directed against a precise objective, but it was all the same a question of a form of institutional violence against a threat considered as revolutionary. In the same way terrorism can not always be carried out by institutions. But in order for us to consider it such, its manifestations must then strike in an indiscriminate way. A bomb in a station or an open supermarket or on a crowded beach can rightly be defined terrorist. Even when it is fruit of the delirium of a ‘madman’ or when it is claimed by a revolutionary orga nisation, the result of such an action is to seminate panic in the population.

When on the other hand violence is neither institutional nor indiscriminate, it is a non-sense to speak of terrorism. An individual that exterminates his family in prey of a crisis of madness is not a terrorist. Any more than a revolutionary or a subversive organisation that choses its objectives with care. Of course there is violence, revolutionary violence, but not terrorism. It is aimed neither a defending the State nor at seminating terror in the population. If, during such attacks, the media talk of ‘collective psychosis’ or ‘whole nations trembling in fear’, it is merely in reference to the old lie that wants to identify a whole country with its representatives, in order to better justify the pursuit of the private interests of some in the name and at the cost of the social interests of all the others. If someone were to start to kill politicians, industrialists and magistrates, that would merely seminate terror among politicians, industrialists and magistrates. Nobody else would be materially touched. But if someone were to put a bomb in a train, anyone could be a victim, without exclusion: the politician just like the enemy of politics, the industrialist just like the worker, the magistrate just like the repris de justice. In the first case we are faced with an example of revolutionary violence, in the second it is a question of terrorism on the other hand. And in spite of all objections, critiques and perplexities that the first form of violence can raise, one certainly cannot compare it to the second.

That said, we come back to the initial question. Between the king who has the crowd massacred and the anarchist that shoots the king, who is the terrorist?

Mare Almani




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



What a social monstruosity, what chef d’oeuvre of Machiavelism is this revolutionary government! For any being that reasons, government and revolution are incompatible. 
Jean Varlet, Gare l’explosion, 15 vendemaire an III.



Mare Almani

