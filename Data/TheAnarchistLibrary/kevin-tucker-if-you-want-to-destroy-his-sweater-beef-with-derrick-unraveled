      Why Bother?      Self-Help and the Other      Horticultural Warfare      Delusions
I think to a certain extent, we’re all finally tired of Derrick Jensen and Deep Green Resistance. The anti-trans stance that DGR has held steadfast to has done most of the work in demolishing any residual legitimacy that Derrick and his crew might have had.

Frankly, I’m relieved to see DGR fade and that’s hardly a secret. I think DGR is dangerous. I think the ideas behind it are lacking any and all historical/real world context. As anarchists, we’ve been gnawing at the roots of organized movements for nearly a century, so this isn’t new turf. However, it’s the first time we’ve seen a group try to tap into the anti-civilization milieu to build an organized resistance.

And not just an organized resistance, but an organization. Replete with affidavits, codes of conduct, member purges, authoritarian structures, and all the usual trappings of organization, DGR is dangerous in attempting to fill the perceived void of action in a post-9/11 world by presenting itself as the means to a common end. In doing so, they’re doing the paperwork, filing, and organizing that any well balanced security culture sought to eradicate.

That is why is can’t be ignored.

For the most part, DGR is a public joke.

You have rigid authoritative figures posing as revolutionaries while they personally call on the cops and FBI for protection, namely against anti-civilization anarchists. The DGR book/approach takes the level of revolutionary romanticism that tends to fade after the first year in college and extrapolates it into a world where we can act as though the Left never picked up arms and used them against the people they claimed to speak for. Occupy imagination, right?

It is posturing in the worst sense. It plans to create an organization that has above and below ground factions and, ideally, will destroy or help destroy civilization. Or industrial civilization. Or Capitalism. Whatever it is that sells books while getting support and funding from key Left groups. There’s a slippery slope there and Derrick didn’t just take DGR for the ride, he formed it somewhere in the mudslide.

For the record, I find Lierre Keith so utterly useless that it’s not worth going into detail about her contributions to DGR. Derrick is using her, just like any other “co-author” that he’s worked with; he usurps identity. That is after running it through his Left-approved byline filter of “writer, activist, farmer”. Here’s a self-published novelist that was thrown into the spotlight by maximizing on the migration from herbivore to omnivore. If you’ve had the misfortune of reading The Vegetarian Myth, you’ve seen that it reads as an angry break up letter after a 20 year relationship with a diet. The relationship sounded rough, but the book falls flat on dogma on all ends of the spectrum (articulating why animal liberation oriented folks would be drawn to veganism, health and nutrition points, and overstating pro-farming claims). It fails to make and carry a central point other than the fact that Lierre doesn’t like vegans. The feeling appears to be mutual.

I, myself, am far from vegan, yet was let down that what appeared to be a book that had the potential to draw veganism (ideology) into question, turned out to be an attack on vegans instead. A complete an utter waste of time, but seemingly completely in line with all of the insane horse shit that Lierre has let out since.

Digressions aside, the problems with Derrick are complex.

There are plenty of books that I recommend to anyone seeking to expand their understanding of civilization that I don’t agree with the authors 100%. That’s expected. Most of the people who have helped me form my views would most likely never call themselves an anarcho-primitivist. More so, some would consider anarchists as abhorrent, but not in the way Derrick does.

I trust that people have brains. I trust that my arguments stand on their own feet. I have no interest in being the sales pitch for an ideology, movement, revolution, or anything else. In accordance with that, I don’t posit myself as the hitch of my arguments. The problem with Derrick is that he does. That means that when he went off the deep end, his cult building library goes with him.

So let me clarify a point here: Derrick Jensen is a good writer. Not a thinker, but a writer. He is able to write effectively and I know plenty of people personally who were turned onto anarcho-primitivist thought because of his work. I think there are plenty of strengths in Culture of Make Believe, Language Older Than Words, and Welcome to the Machine (Derrick’s best book in my opinion, co-authored, of course). His strength lies in articulating points that others have made. As far as I can see, his only really original thought (aside from whatever horrible and ill-advised contributions he made to DGR) comes down to the arguments against pacifism in Endgame which I consider brilliant, even if they become ironic in hindsight.

But I can not, in good conscience, recommend any of Derrick’s books.

A good writer is expected to write well. A strong thinker is expected to articulate their ideas and be able to defend them. Defense is a ban-worthy concept in Derrick’s world. He’s stated openly that he only takes positive criticism from a minor handful of people. He is heavily scripted, as can be witnessed if you’ve seen his Talk at any point in the last decade. He rarely goes off the cuff during question and answer. When he does, expect unflinching insanity, like comparing himself to Tecumseh or talking about having sex with trees.

When questions are raised about the validity or realities of anything Derrick has said or done, he cuts  the party in question off, usually in a rant filled flare. If a lot of points are raised, he’ll rant privately and make reference to it, but will absolutely refuse any and all public airing. He’s even had “offensive” YouTube videos mocking him taken down, which is no easy feat. It’s not because he’s trying to take the high road, he’s routinely made ridiculous attempts at character assassination in books and interviews as his chosen form of pre-pubescent revenge. It’s because the open arena isn’t his forte. So he takes a combination of back roads and dead ends to get out of the situation.

But here’s the thing: none of this should matter.

In fact, it pains me to even draw it out further. I don’t care about Derrick. I don’t care about his story, his hopes, his dreams, any of it. I can be fully content ignoring him and his ever-flowing stream of work under one single condition: it didn’t attempt to channel the critique of civilization into a pile of names and address in the hands of a narcissistic egomaniac.

Both myself and John Zerzan have continually offered to publicly discuss our differences in approach and the holes in Jensen’s narrative with Derrick for years. There are clear points where you see Derrick’s cult building up and around his written persona and that resulted in a direct change in language, targets, and approach from what we (being most green anarchists at the time) were helping him push with his earlier books and where Derrick started going in Endgame, but went off the deep end after.

I have to admit that it was Ted Kaczynski that was the most adamant from the start that Derrick’s victimization approach would be the source of his decline. When Ted was pointing that out in 2002, I wasn’t seeing it that way, but that is clearly what happened. And it just got worse and worse.

So let’s rewind a bit.

What drew John and I towards pushing Derrick’s books was that his approach was and remains intrinsically different than ours. There are key points in Derrick’s early books that were always problematic; implying that chickens would literally put their heads on the chopping block for his axe, feeling the need to write in detail about jerking off to internet point to drive home points about objectification that folks like Susan Sontag and Susan Griffin made decades prior, things of that nature. But it was close enough that we were willing to share the stage.

Up through the mid-late 2000s, Derrick was willing to call himself an anarcho-primitivist. He was coming at it differently than any of us, but there wasn’t question about where he aligned. He clearly didn’t come from the anarchist world or understand it, but he didn’t claim to. We all knew that the price tag on his Talk was extraordinarily high. When John and I would do gigs with him, he would ensure that we were treated as opening acts (twice John and I were given paltry 15 minute slots when Derrick went on with his whole 90 minute + routine). He always had agents and publishers to do his leg work and we all just accepted it. It was about spreading the word.

But there was more to it.

It became glaringly obvious that Derrick was able to talk in more detail than any other anarchist. Even in the pre-9/11 world, green anarchists had targets on them from the state. So while we were all being followed, surveilled, taped, brought into Grand Juries, courtrooms, trials, chased down by the Joint Terrorism Task Force, FBI, ATF, and Secret Service, Derrick is writing about hacking the grid. Seemingly without consequence. And to largely anti-anarchist Leftists groups.

It was baffling, but who was going to complain? I clearly remember defending Derrick to other anarchists who were already calling him a liberal because of his audience. Either they were right or they saw the writing on the wall, but as far as could be told, his audience could only be blamed for not putting the pieces together since he laid it all out.

Or at least that’s how it seemed.

In hindsight, maybe the direction Derrick was going in was the direction he always aimed for. Even though Derrick was saying extremely radical things, he was cushioning it. His writing is formulaic and seems to mirror his Neo-Con past and we weren’t prepared to see that arc in the mix.

Derrick’s writing is about a personal journey. There are cold hard facts, there are soul wrenching moments, there’s a call to arms, and then there’s a link to normalcy through universal purging. As destroying as Derrick’s words can be, they are therapeutic. It happens to be the case that the things he’s writing about are driving at the tension in loving the earth and contributing to it’s destruction. I was able to take away a call to action, because that’s my cross over point. But the cycle is straightforward, pedestrian Derrick stumbles into a horrifying realization, damning facts are laid out, pedestrian Derrick is pulled into it, someone more knowledgable about the matter is brought in to confirm and consult, then in the moment of realization, another “average Joe” pedestrian confirms their agreement on the issue.

Somewhere around 2004–6, I probably asked Derrick a handful of times why he thought his ultra-liberal audience members were throwing support at him while attacking anarchists. He had no answer. He might have not even realized it, but in seeing Derrick’s firm move beyond the radical Left and into the professional liberal fold, it’s clear that Derrick is the sherpa on the mountain of liberal guilt. He is their motivational speaker; well rehearsed, sitting cross legged on a table, reciting the same effective jokes time after time, walking liberals through their darkest thoughts, and taking them back home.

The reason that Derrick is able to say horribly detailed things about destroying civilization, attempt to organize a revolutionary movement, be pushed by liberals and anarchists alike, call the cops and FBI on anarchists, and get away with all of it is that he’s not a threat. He’s a professional. An entertainer. A therapist.

And yet DGR carries the allure of a revolutionary movement against civilization. And woe ye who don’t know enough to not sign (literally) onto his movement. To date, the only action taken by DGR is selling Derrick’s books. Their newswire service simply takes any other action and puts their name on it.

For these reasons alone, it should be fairly obvious why you won’t see Derrick’s books on Black and Green recommended reading lists.

But it gets worse.

A longstanding dispute I’ve had with Derrick is over his portrayal of horticultural warfare. In the decade since I initially brought this up, he’s only made flippant mention of it as a minor point in public. But as someone who bases their ideas on facts rather than whims and appeals to personality, I find this sticking point rather irritating.

In Culture of Make Believe, Derrick has a discussion about battlefield warfare amongst horticulturalists in Papua New Guinea. What he says is largely true; battlefield warfare is particularly less lethal than one would imagine. Before the battle, there are large pork feasts, which, if you haven’t overloaded yourself with pork before, will tire you out quickly. The weapon of choice is typically large and not horribly accurate or effective arrows, but the tongue is equal as insults are more often shot across than darts. Derrick mentions this to distinguish it from modern warfare for obvious reasons: civilization is unequivocally more violent, faceless, and ruthless.

This sounds nice, but it’s not the full truth.

Derrick has no interest in attacking the roots of civilization. This has gotten worse over the years as his focus has shifted in accordance with liberal targets. In my eyes, looking at the consequences of domestication truthfully and honestly is the most telling way to understand how civilization could exist. So horticultural societies, as societies with domestication, but without civilization, are telling. I don’t wish to damn them, but it’s important to understand them.

Derrick’s fairytale version of horticultural warfare would be far more pleasant than the resource wars that our civilization currently undertakes, but it’s not true. Most people who have taken a Cultural Anthropology 101 course could tell you this.

Battlefield warfare is a part of horticultural warfare patterns. It is, typically, the least fatal method of warfare. The problem is that warfare is a consequence of resource competition. This applies equally to horticulturalists as it does to us, but it’s a matter of scale. In having semi-sedentary lifestyles with gardens, granaries, and surplus, you have property, power, and boundaries that simply don’t exist with nomadic gather-hunter societies. The response isn’t just battlefield warfare, it’s warfare culture.

This is where you get the origins of patriarchy. In warfare culture, you see, for the first time, a preference for males (warriors) over females, hence a higher rate of female infanticide (curbing population). In the warfare cycle, battlefield warfare has little on the more important aspect of raiding. In raids, a lot of people can die. Wives and children are taken, villages are burned. It is, after all, warfare, and it’s messy.

I don’t say this to judge, but this is what domestication does to us: it’s a socio-religious justification for an ecological reality. Sedentary life challenges natural means of birth control associated with nomadism. People settle, numbers go up, surplus is finite, numbers need to go down. It’s a cycle.

Printing what Derrick did once isn’t lying. It could be a mistake. I talked to Derrick about this at length in person and in email (we were friends at this time) and he was aware that these are the kinds of issues that I was pursuing.

But then it shows up again in Endgame. Same story, same omission.

At this point, it’s clearly no accident.

It becomes harder to trust a person as a thinker when their writing is set on a simplistic narrative. It becomes obvious why the material doesn’t and can’t go deeper. It’s a sales pitch. It makes up in simplicity what it lacks in honesty. But who is this helping? No one. It’s a weak move for someone with no spine who has no interest in ever having to directly answer questions about it (Jensen has, at times, required that all audience questions be pre-scripted and screened). The arc of the narrative becomes more apparent and the words become secondary to the speaker.

But what does that mean when these books are selling Derrick and he’s clearly lying? What else is he lying about? What else is a sales pitch?

As an anarchist, I’ve come from a tradition where the individual is never as important as the ideas they bring to the table. Sacred cows are sacrificed regularly.

As an anarcho-primitivist, my goal has never been to hand someone a neat and tidy package that they can chose to use or not. I’m not selling myself. I’m not selling anyone. My job, as I see it, is to be honest, to put it out there as openly as possible. I trust that individuals are capable of thinking on their own. I know that the things that I see happening to this world are happening everywhere and we all feel it. My goal isn’t to create a struggle, but to put as much out there as possible to help others contextualize their struggles. It’s not enough to walk them through scenarios and make them feel renewed and comfortable in their surroundings again. If there is going to be change, it will take individuals making real connections with the real world, not by identifying with another persona.

I refuse to sell myself. I refuse to see myself as an ideal. I refuse to see myself as a leader. These ideas exist outside of ourselves and I feel responsibility to put it out there freely and honestly.

This point can appear minor, but it goes deep. If someone wants to market themselves as a big name in a niche market, I’m not going to let them do it off of my back.

As I said at the outset, I think it’s clear now where Derrick was headed and that he’s firmly lodged in that corner now saying absurd things. I can’t support his current or previous work because it’s all about him, not the ideas. Even if he hadn’t gone this direction it would still be an issue, but it’s become intensely clear as time has gone on.

The problem is that Derrick won’t address any of these points head on. I’m not sure it matters at this point because I think he is far beyond salvaging. John and I tried early on, but it was a one way door.

So here’s the shitty part. Since Derrick won’t respond directly to John or myself, but puts out this narrative that we are merely jealous (ha!) of his fame and success, hence the split, let me just briefly give a little background to what actually happened. Derrick’s fans will attempt to assert otherwise, but nearly all of them weren’t around at the time and all of this is pretty fully documented (mostly in laughable emails from Derrick).

I don’t have pride or joy in this. It’s ridiculous and I’m leaving out a lot of Derrick’s worst side just because it’s not necessary to go beyond the public side of this. But Derrick’s maniacal egomania is entirely driven by whims, emotions, and knee jerk reactions to unwanted criticisms. Question him and you’re out. Good riddance in my opinion.

So a brief look at Derrick’s evolving hatred of anarchists;

2007: First Deep Green Resistance weekend retreats. Participants pay north of $1000. At most, Derrick appears via Skype only.

2008: Last issue of Green Anarchy prints an overall positive review of Derrick’s Thought to Exist in the Wild. It contains a line questioning the quantity of Derrick’s work (including massive reprints from previous books) and wondering if he might lose the effectiveness of his writing by over-saturating.

Derrick loses his shit. Calls it a “hit piece” and refuses to be swayed by reasonable discussion. Before the ink dries, he’s doing interviews with liberal magazines and websites about how anarchists are dirty, ineffective, unorganized, and mean.

At the same time, he starts insisting that anarchists have all ripped him off. Despite years of helping scrape together his hefty speaker/travel fees and not being engaged personally plus selling a ton of his books, he goes on undeterred to spread the narrative that we’re all out to get him.

Around this time he starts getting quickly accepted and spread by increasingly liberal and progressive outfits. A column in Orion magazine and then doing fundraisers for anti-radical groups. Anarchists are purged from the Derrick Jensen forum.

Drops discussion of civilization in favor of “industrial civilization”.

By 2010, Derrick is working with staunch anti-anarchist Chris Hedges about how anarchists are ineffective, mean, and starts talking about the rights of “citizens”.

In 2011, he tries to co-opt Occupy Wall Street and bring it in the fold. Drops discussion of “industrial civilization” to target “Capitalism”. Does hilarious pay-per-view Skype sessions. Supports Lierre working with the cops after she is pied. Supports Peter “Urban Scout” Bauer in talking to the cops and FBI after he is mailed a rock. Security lock down at talks, scripted questions only.

Deep Green Resistance is published.

In 2012, article with Chris Hedges comes out, attacking anarchists and talking about the complete ineffectiveness of the Black Bloc. Derrick starts calling anarcho-primitivists racist and misogynist while claiming he never took the title on despite plenty of evidence to the contrary online, in books, and in talks. He’s only interested in throwing out anarchists so he doesn’t have to engage them.

In 2013, Derrick and Lierre anti-trans politics come to the forefront and are indisputable. Huge round of DGR purges, Aric McBay (co-founder) leaves amongst others.

This is an overview, but it should be pretty clear about how Derrick’s personal attitudes and paying clientele have directly impacted his approach and impact.

I say good riddance to old trash.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

