
Michael Schmidt and Lucien van der Walt’s book Black Flame: The Revolutionary Class Politics of Anarchism and Syndicalism published by AK Press in 2009 has caused considerable debate within the anarchist movement. While the debate itself is refreshing, it might not always be conducted in the most productive of ways.

With respect to the book, two issues have to be separated. Black Flame is an outstanding contribution to the historiography of anarchism. It seems hard for anyone to deny that, no matter how strongly some might object to the definition of anarchism presented in the book. So the question of anarchism’s definition, which is at the center of the Black Flame debate, must not be confused with disregarding the work itself.

In this spirit, the following remarks must be read as an attempt to advance a discussion about the meaning of anarchism rather than as an outright rejection of the definition proposed in Black Flame. In a respectful exchange of ideas, we can hopefully all improve our definitions, concepts, and the way we present them, and therefore anarchism as a whole.

Fights over definitions often take on curious forms. It is impossible to decide on a “correct” definition outside of a commonly accepted framework that provides commonly accepted criteria allowing to distinguish between a “correct” and an “incorrect” definition. For the most part, this works well in everyday language. If we give someone directions and say, “You will see a house as you come down the hill”, no one will answer, “Hold on a second: what’s your definition of a ‘house’?” Interestingly enough, if we were forced to define a “house”, our definitions might be quite diverse, but the common language we use — what Wittgenstein calls a “language game”, for those who are interested in such things — provides clear enough criteria for us to not even need a spelled-out definition in order to understand one another.

Things become more complicated with terms that are more complex and abstract. If you tell someone, “My new neighbor is an anarchist,” the response quite likely might be, “What do you mean by ‘anarchist’?” In general, there seem to be three possible ways for the conversation to proceed:

1. You present your definition, the person you are talking to says, “Okay, I see” (whether your definition matches his or hers or not), and that’s it.

2. You present your definition, the person you are talking to says, “Interesting, because my definition would be a different one”, and you have an inspiring discussion about the different possibilities of defining anarchism.

3. You present your definition, the person you are talking to says, “That’s the dumbest thing I’ve ever heard”, and you end up having a fight.

Personally, I see number three as the worst-case scenario. Unfortunately, it is the form the Black Flame debate often takes — which, again unfortunately, is nothing new in anarchist circles.

One of the problems is that without an accepted framework providing commonly accepted rules for “correct” and “incorrect” definitions, there is no end to the fighting. People appear to argue when, in fact, they aren’t really talking to one another because they move in different frameworks (“language games”).

When trying to engage with the definition of anarchism provided by Black Flame, a difficulty stems from the fact that subjective assessments and general assumptions are often intertwined in the book, which can be confusing. On the one hand, Schmidt and van der Walt “argue [that] the most important strand in anarchism has always been syndicalism”, they “develop an understanding of the doctrine of anarchism and its origins”, they “reject the notion that anarchist currents can be found throughout history”, they “challenge the view that any philosophy or movement that is hostile to the state, or in favour of individual freedom, can be characterised as anarchist”, and they conclude that “it is our view that the term anarchism should be reserved for a particular rationalist and revolutionary form of libertarian socialism that emerged in the second half of the nineteenth century” (all emphases mine).

On the other hand, they proclaim that “anarchism is a revolutionary, internationalist, class struggle form of libertarian socialism [that] first emerged in the First International”, that “syndicalism is a variant of anarchism”, that “there is only one anarchist tradition”, and that various thinkers and movements commonly considered anarchist “cannot truly be called anarchist” and are therefore “not part of the anarchist tradition”.

Sometimes, the general assumptions and subjective assessments converge, for example in the following statement: “‘Class struggle’ anarchism, sometimes called revolutionary or communist anarchism, is not a type of anarchism; in our view, it is the only anarchism” (emphasis mine).

Sometimes, the convergence leads to apparent arguments that are actually mere tautologies: “Not only is it the case that anarchism did not exist in the premodern world; it is also the case that it could not have, for it is rooted in the social and intellectual revolutions of the modern world.”

It has to be assumed that Schmidt and van der Walt would object to subjective assessments playing a role in their definition of anarchism, at least if we consider the following statement: “...we maintain that the meaning of anarchism is neither arbitrary nor just a matter of opinion.” However, the explanation that immediately follows is hardly convincing and rather confirms the subjective element in their definition: “...the historical record demonstrates that there is a core set of beliefs.” Which “historical record” are we talking about? Asked differently: how can we talk about a “historical record” if we do not already have an understanding of what anarchism is? How would we know what to look for? Obviously, much of what has been considered anarchist historically falls outside of the Black Flame definition of anarchism.

In a similar manner, the following conclusion is only valid if you have already decided what anarchism is: “Given that antistatism is at best a necessary component of anarchist thought, but not a sufficient basis on which to classify a set of ideas or a particular thinker as part of the anarchist tradition, it follows that Godwin, Stirner, and Tolstoy cannot truly be considered anarchists.” If antistatism alone is not a sufficient basis for anarchism, why would it follow that Godwin etc. cannot truly be considered anarchist? Eliminating antistatism as the exclusive criterion for anarchism only says what anarchism is not, but gives no answer at all as to what it is. So, once again, how would we know who to exclude if we do not already have an idea of what anarchism is? We could just as well exclude Bakunin and Kropotkin and call Benjamin Tucker the only true anarchist. Of course one could argue that more self-identified anarchists were inspired by Bakunin and Kropotkin than by Tucker, but this is a line of argumentation that Schmidt and van der Walt clearly reject.

To paraphrase Schmidt and van der Walt, what they are basically saying is: “Historically, there is a core set of beliefs that defines what we see as the broad anarchist tradition. It is this history that we will therefore present as the history of anarchism.” This is a perfectly legitimate approach — but it is based on a preconceived notion of anarchism.

By no means do I mean to critique the authors for entertaining such a notion, for it could not be any other way. Definitions are not about discovering the (ideal) “truth” behind a (worldly) phenomenon and then framing it into words — if one claimed the ability to do this, it would be a crude form of Platonic thought unsuitable for antiauthoritarianism by any standards. Hence, there is nothing wrong with having a subjective approach. But the approach should be unambiguous to avoid confusion and enable a clearer debate.

Some people worry that a definition becomes “random” if a subjective element enters the equation. But that is not the case at all. Of course Schmidt and van der Walt have reasons for choosing the definition of anarchism they chose. It might be most attractive to them, it might make the most sense, it might take us the furthest in the struggle against the state and capital. Whatever the reasons are, there are arguments for them and these have to be taken seriously.

Before we look at the main arguments for the Black Flame definition of anarchism, let us talk a little more about what we expect from a “good” definition. Also in this case, there is no randomness only because there is lack of truth. If what English-speakers call a house is not truly a “house” (as in: not more so than a “maison” or a “camera”), it doesn’t mean that you can just define the word “house” in whatever way you want. Well, you can, but you will soon realize that defining it as “a red fireball with three arms” is a bad definition — because no one will know what you are talking about. The main criterion for a good definition (beyond the Platonic world) is to ease communication — which also includes analysis and research. Schmidt and van der Walt obviously hold this view as well: “A good definition is one that highlights the distinguishing features of a given category, does so in a coherent fashion, and is able to differentiate that category from others, thereby organizing knowledge as well as enabling effective analysis and research.”

In everyday terms, a “good” definition is “useful” and “makes sense”. These seemingly trivial criteria go a long way. For example, defining a house as “a red fireball with three arms” doesn’t make any sense and we know it, even if we have a hard time coming up with a good definition of “house” ourselves. We could go through countless similar examples.

For the definition of a word to make sense, the following criteria usually apply: the definition must not contradict the everyday usage of the term entirely; it must emphasize the distinct characteristics of the defined thing/phenomenon; it should not be too wide and not too narrow; it must not be ideologically biased; it must not contain too many ambiguous and contested terms, etc. All of these criteria should apply to a good definition of anarchism too.

In the case of a complex and ideologically charged terms like “anarchism”, there are two other important aspects that don’t necessarily apply to a term like “house”:

1. Identity. To be an “anarchist” is important to many self-identified anarchists. If someone says, “sorry, but you’re not really an anarchist”, it’s hard to just shrug your shoulders and walk away if feeling affiliated with anarchism is a strong part of your identity. In simple words, emotional factors come into play.

2. Politics. If we want to overthrow, undermine, or at least challenge the ruling system, we want to use everything we can as a weapon — that includes language. In this respect, it is important to consider the fact that our definition of anarchism can impact the effectiveness of revolutionary movements.

With this in mind, let us turn to the main Black Flame arguments for the definition used in the book:

1. Coherence. Schmidt and van der Walt argue against “anarchism being defined so loosely that it is not clear what should be included and what should not, and why some things are included and others are not.”

I believe that Schmidt and van der Walt sometimes exaggerate the differences between thinkers that are commonly considered as anarchists. For example, I cannot see an “abyss” between “the notion of freedom articulated by Stirner and that of the anarchists [read: the people Schmidt and van der Walt consider anarchists]”.

I also think a claim like the following is hard to uphold: “...no serious examination of Lao-tzu, the Anabaptists, and Bakunin can maintain that they shared the same views and goals, so it is not clear why they should be grouped together”. Schmidt and van der Walt could hardly argue that Bakunin, Malatesta, and Connolly shared the same “views” (perhaps no two anarchists do; in any case we need to at least know what kind of views we are talking about). The category of “goals” is also difficult. Many anarchists share the same goals as Marxists, yet they would never want to be in the same category. In short, whether we can group people together or not doesn’t depend on how many views and goals they share, but which of their views and goals we want to consider in this specific case of grouping people together. For example, if one wants to group all people born in Austria together, I’m afraid it’s legitimate to put me in the same category as Arnold Schwarzenegger, while I certainly hope that there aren’t too many others we would share.

Nonetheless, I believe the “coherence argument” to be strong. It is probably true that “the seven sages approach [referring to Paul Eltzbacher’s classic book on anarchism, published in 1900] inevitably creates the impression that anarchism is contradictory as well as unfocused, and renders the theoretical analysis of anarchism a frustrating task at best”. It is also true that a definition can be “so loose as to be practically meaningless”, which is why I agree with Schmidt and van der Walt that neither self-identification nor antistatism serve as useful defining grounds for anarchism.

2. Clarity. Schmidt and van der Walt repeatedly stress that “we must have a clear understanding of what ideas we mean by anarchism”.

This, in fact, is a curious argument once we take a closer look at Black Flame. The reason is that the book never actually formulates a concise “this is it” definition of anarchism. Instead, there are numerous allusions and references that frame a definition. These include the information that anarchism is “a libertarian type of socialism”, stands “against capitalism and landlordism”, considers “individual freedom and individuality [...] extremely important”, favors a society that is “classless, egalitarian, participatory, and creative” as well as “rational, democratic, and modern”, envisions a “planned economy”, contains “a deep respect for human rights” as well as “a profound celebration of humankind and its potential”, struggles to “replace capitalism and the state with collective ownership of the means of production”, pursues a society where “individual freedom would be harmonised with communal obligations through cooperation”, emerged “from within the socialist and working-class movement 150 years ago” and “against the backdrop of the Industrial Revolution and the rise of capitalism”, is “a child of the eighteenth-century Enlightenment” and “not a universal aspect of society or the psyche”, and so on. I think it is difficult to argue that this list makes a “clear” definition. Albeit very selective, it already contains at least fifteen terms that are highly contested: what exactly does “libertarian” mean, or “rational”, or “planned economy”, etc.? In short, while the Black Flame definition of anarchism might be “narrow”, it is not necessarily “clear”. Those are two different things.

I should add that the authors cannot be criticized for not presenting a clear definition of anarchism. After all, it is near impossible to present a “clear” definition of any more complex phenomenon. The only thing the authors might be criticized for is to promise something that practically no one can deliver.

3. Effectiveness. Schmidt and van der Walt claim that “by presenting anarchism as vague and rather formless, it also makes it difficult to consider how the broad anarchist tradition can inform contemporary struggles against neoliberalism”.

I am not sure if I find this argument convincing. If the “broad anarchist tradition” can inform contemporary struggles, why would it not do so only because some folks you don’t consider anarchists are also called anarchists by others? I suppose that this doesn’t change the ideas of the “broad anarchist tradition”, so if these ideas have the potential to inform contemporary struggles, they shall do so regardless.

One could possibly argue that because of a “vague and rather formless” perception of anarchism, people will never get to the “real” anarchism buried underneath a lot of pretense, but that’s a different kind of argument — and a very bold one indeed, since it would not only imply that many apparent anarchists are no real anarchists but also that they have nothing to contribute to contemporary struggles. If that is the opinion of the Black Flame authors, it would have to be supported more strongly.

4. Organization. Schmidt and van der Walt write: “The view that anarchists and syndicalists will be strengthened by the formation of an organisation that is open to all anarchist currents is also open to question.”

This is an interesting argument because the aspect of strategy comes into play. When Schmidt and van der Walt suggest that “synthesist” anarchist organizations “often have difficulties in operating”, we might be at the core of the “political” implications of anarchism’s definition: which definition will allow for the most effective form of anarchist organizing?

Intuitively, it seems right to suggest that an organization uniting people who, fundamentally, share the same views, will operate better than an organization uniting people with all sorts of different perspectives. However, there are some problematic aspects in such an assumption. These do not necessarily concern the fact that some self-identified anarchists see formal organization per se as an affront to the anarchist idea. Let’s put such views aside here for a moment and focus on the question of whether organizations really become better if we cut down on diversity? In my opinion, this is not the case. Diversity also means critical intervention and creativity, essential features of antiauthorianism.

Of course I don’t want to romanticize. Too much “critical intervention” and “diversity” can make organizing outright impossible, there is no denying that. At the same time, the answer cannot be to only organize with anarchists we have no disagreements with. The challenge for anarchist organizing rather seems to develop forms of organizing that turn diversity from a destructive threat to a productive tool. This is tremendously difficult, but I think it is the only chance we have.

Furthermore, people’s views are only one factor that determine the success or failure of organizations. Others include membership numbers, organizational structure, community support, links to social movements, individual dedication, personal maturity, etc. For example, many platformist groups — a model for anarchist organizing that Schmidt and van der Walt have clear sympathies for — have difficulties in operating, too. On the other hand, anarchist-inspired organizations like Common Ground Relief in New Orleans operate rather effectively, although they aren’t “unifying anarchists around clear objectives,” “elaborating a shared set of strategic and tactical choices”, or “uniting energies around a common programme”. A strong sense of solidarity can help overcome many differences. It is therefore unfortunate that the value of solidarity often disappears behind struggles of identity or mere personal turf wars in anarchist debates.

There are a few other problems I see with the Black Flame definition of anarchism.

1. The identity problem. As stated before, certain definitions have a strong identity value; they are not just a question of logical categorization but of emotional attachment. Heated arguments about whether a band is “really” punk or not is one example. When someone says, “Green Day is the best punk band ever”, punks with a strong DIY-ethos might just shrug their shoulders and say, “Well, in my definition, Green Day is not punk, but whatever”, but it is understandable if this does not always happen. They are not defending a mere word, they are defending an identity.

2.

We are experiencing similar reactions with respect to Black Flame. If anarchist identity wasn’t important to people, non-anarchists in the eyes of Schmidt and van der Walt could just say, “Interesting definition, but I don’t see it that way.” However, for many, this doesn’t work. If you have strongly identified as an anarchist for a long time, you easily take it as an affront if someone comes along and says you’re not. The sometimes categorical tone in Black Flame probably doesn’t help either. Needless to say, nor does the sometimes categorical tone of the book’s critics. As a result, the debates often turn sour fast and make alliances between people impossible who, actually, have very similar views and goals. This, to say the least, is unfortunate.

Sometimes, it is argued that the debates only get out of hand if there is no common ground for alliance anyway. The differences in people’s views are supposedly unbridgeable and this is the reason for the lack of collaboration, not failed communication. In some cases, this might be true. In others, I don’t think it is.

Let us consider the debates about the significance of “class”, for example. A lot has been said about the conflict between “class struggle” and “lifestyle” anarchists, a debate that Black Flame also refers to. Some, including the Black Flame authors, suggest that the latter are so fundamentally different from the former that they cannot be useful comrades in the struggle against the state and capitalism. “Lifestyle” anarchists are usually accused of neglecting class, while “class struggle” anarchists are usually accused of neglecting all other forms of oppression. Curiously enough, I have hardly ever met a “lifestyle” anarchist who flat-out denied the significance of class, and I have hardly ever met a “class struggle” anarchist who flat-out denied the significance of other forms of oppression. This is not what people fight about. People fight about priorities, strategies, in the worst case personal vanity. So how come these differences are perceived as so fundamental that they make collaboration impossible? Is this really inevitable?

In Black Flame, the authors express a number of views that I don’t agree with. For example, I have no problem with the term “anarcha-feminism” (with respect to a term like “social anarchism” the authors concede that, for clarity, its usage “is probably sometimes necessary”, although they consider the term tautological — why does the same not apply to “anarcha-feminism”?); I don’t think that stressing the specific characteristics of, for example, racial and gender privilege fosters “an identity politics that makes a virtue of fragmentation and pitting different groups of workers against one another” (in my opinion, the stressing of these forms of privilege only adds important perspectives that allow for an overall more differentiated analysis); I don’t believe that the very diverse phenomenon of “postmodernism” can be reduced to a school of “relativists for whom truth is a matter of opinion” and who “adopted an idealist form of determinism” (despite all the nonsense that happens under the postmodern label today, postmodernists have, for example, successfully challenged authoritarian schools of thought). However, despite such reservations, why should I draw the conclusion that Schmidt and van der Walt are no comrades in the struggle against the state and capital, or that it would be impossible to work with them? Judging from the effort that went into their book, they are probably great people to work with — at least if we consider commitment and diligence a virtue. Of course we might encounter problems along the way, but what does it say about the anarchist movement if we don’t even try because of this? Do we not leave for an exciting trip because we might catch a cold, our car might break down, or we might not sleep that comfortably every night? Without accepting challenges and without stepping out of our comfort zones, it is unlikely that we will ever get very far.

3. The exclusion problem. The Black Flame definition of anarchism excludes many individuals, groups, and movements that are usually considered anarchist and who consider themselves anarchist. The authors acknowledge this, stating, “We are aware that our approach contradicts some long-standing definitions”. This is not necessarily a problem, but you need to make a strong case for the exclusions, otherwise it is hardly worth causing confusion to the point where basically all existing histories of anarchism would have to be rewritten — not to mention the many personal upsets you cause.

4.

Now, is the Black Flame case really strong? As I tried to sketch above, not in my eyes. I think that Schmidt and van der Walt have a strong case in arguing that some of the most common definitions of anarchism — especially those based on mere self-definition and a crude understanding of antistatism — are indeed too vague. Therefore, they also have a strong case for proposing a different definition of anarchism. However, I think that they end up with a definition that is too restrictive and unconventional to be accepted. Why not call Proudhon or Tolstoy anarchists and adjust your definition accordingly? I simply can’t see the harm it would cause.

In general, though, the question of which historical figures to include in the anarchist movement is not the decisive one, and of rather academic nature. For example, is it really important to argue about whether Max Stirner was an anarchist or not? I have a much higher opinion of Stirner the philosopher than Schmidt and van der Walt do, but Stirner never called himself an anarchist and he was never part of a social movement that used the name, so I guess I wouldn’t care either way.

The by far bigger concern are contemporary anarchists. I have been active in the anarchist movement for more than twenty years, have met with self-identified anarchist activists in numerous countries, and have recently co-edited a German book on contemporary anarchism that includes fifty interviews with anarchists from five continents. According to the Black Flame definition of anarchism only a very small portion of the people I have met, spoken to, and worked with would qualify as anarchists. Of course, one could say that I move in the wrong circles. This might be the case, but I would still like to point out that I am neither an opponent nor a stranger to syndicalism. I am a member of the Swedish SAC and consider syndicalism to be an extremely important part of anarchist agitation and activism. However, I am willing to acknowledge that this kind of struggle is, for a variety of reasons, not a priority for many of today’s self-identified anarchists. Does this mean that they do not fight against the state and capitalism? No, they just do it in different ways. And even if I might sometimes think that their priorities are screwed, they are genuinely struggling for a world of justice and equality in which individuals — all individuals — can develop freely.

Now, given the abovementioned problems of self-identification and the political significance of the definition of anarchism, I believe that this struggle will suffer if we tell all these people that they are not really anarchists. We will be considered arrogant, self-righteous, in the worst case power-hungry. The movement will fight about labels instead of discussing common strategies. We will strengthen sectarianism rather than cooperation. Why not try to convince these folks of the importance of the class struggle — if we believe in it — rather than rejecting them as “lifestylists”? I simply cannot see the benefit, although I concede that my belief in possible alliances might be stronger than that of those who rather stress the “unbridgeable chasms”.

It is of course possible to say that we could still work with these “false anarchists” and that it is just important to point out that they are not anarchists. Apart from the fact that pointing this out might make these folks not want to work with us, I think it is also important to have a broad self-identified anarchist movement for three main reasons:

a) You derive strength and inspiration from being part of a broad movement of people who gather under the same banner.

b) You will be recognized as a strong social force, a fact that attracts attention from the wider public, helps spread your ideas, and poses a threat to those in power. “Divide and conquer” is still one of the most useful tools of domination. We might not like it that these kinds of “identity politics” are important for our struggle, but they are. In postmodern times it is hip to eschew all labels. However, common labels serve a political purpose. It is one reason why the definition of anarchism matters for how effective we are as a movement.

3. The inclusion problem. Ironically, the Black Flame definition of anarchism not only entails a problematic act of exclusion but also of inclusion. Schmidt and van der Walt include many individuals in their definition of anarchism who did not use the label themselves, although they were fully aware of its existence. Some, like Daniel de Leon, openly rejected it. Again, Schmidt and van der Walt know this, stating that they “do not use self-identification but rather ideas as the basis for inclusion in the broad anarchist tradition”. There is nothing to be said against this, in principle. However, if we agree that a definition has to be “useful”, how useful is it to include people who do not want to be included? Can this really help to make anarchists a closer and more tight-knit group, which is one apparent intention of the Black Flame authors? Are we really going to have a more effective movement with people who refuse to be a part of it?

There is a difference between making self-identification the only criterion for being an anarchist and for making it one criterion for being an anarchist. This also concerns basic issues of respect. Many SAC members, for example — just like many members of other syndicalist organizations — do not consider themselves to be anarchists, and they do not consider the organization to be anarchist either. Is it really appropriate to tell them, “Well, you might say whatever you want, but you are an anarchist and you belong to an anarchist organization”? I don’t think that any attempt of “forced integration” can do the anarchist movement any good.

I should probably finish with proposing a definition of anarchism myself. I would go for “a credible commitment to a just and egalitarian society without institutionalized authority”.

To be clear: this definition does not claim to capture the “true” meaning of anarchism. As stated above, I don’t think any “true” definition exists. I favor this definition according to the main criteria for good definitions outlined in this text, namely communicative and political usefulness. Needless to say, better definitions might exist according to these criteria. Other suggestions will only take us further.

I would like to explain some of the implications of the proposed definition as follows:

Of course, it is not an entirely “clear” definition, as we’ll always fail to reach that standard. However, I will try to be more precise. The term “credible” includes an (inevitable) subjective moment, but its meaning is fairly uncontested as most of us understand the same by it: people say something and we believe that they really mean it. With a “just” society, I mean a society in which all individuals are offered the same possibilities (and truly, not just on paper) for free development, i.e., economically, socially, culturally. With an “egalitarian” society, I mean a society in which no individuals or groups hold special privileges of power. With “institutionalized authority”, I mean authority that is not based on an immediate mandate of the community that can be revoked at any time but rests on means of oppression (the state, the military, etc.).

Demands for “clarity” can cause endless chains of definitions, but I hope that these qualifications allow the proposed definition of anarchism to take on a fairly solid shape.

“Antistatism” is obviously not a decisive criterion for this definition, and “self-identification” is none at all — although, along the lines of what has been argued above, I would add the following amendment: only such individuals, organizations, and social movements should be called “anarchist” who use the name themselves or, at least, do not object to it as a description for their politics. Of course, this doesn’t mean that we can’t call individuals, organizations, and social movements “inspired by anarchism” or “embracing anarchist ideas”, if we believe that this is the case.

While the proposed definition is more precise than definitions merely resting on “antistatism” or “self-identification”, it still leaves room to include most of the historical figures commonly considered anarchists. Some, like Max Stirner, will always be controversial. The dispute over whether he was committed to a just and egalitarian society cannot be resolved here, and perhaps never will. However, as has been stated, complete clarity is too much to ask for any definition, and there will always be elements that straddle the boundaries. This is inevitable. But is it a big problem? I don’t think so.

Especially not because important distinctions can still be made. In the case of the proposed definition, it is possible to draw strong lines between anarchism and phenomena such as “anarcho-capitalism” or “national anarchism”. These are paradoxical neologisms that signify schools of thought that fit nowhere into the proposed definition, mainly because they have divisions between people built into their make-up that undermine any meaningful sense of justice and egalitarianism. Hence, there is not even “family resemblance,” to use another Wittgenstein term.

In fact, I would like to close with quoting the aphorisms 66 and 67 of Wittgenstein’s Philosophical Investigations, as I cannot think of a better guideline for a useful definition of anarchism. May many new propositions follow!

66. Consider for example the proceedings that we call ‘games’. I mean board-games, card-games, ball-games, Olympic games, and so on. What is common to them all? — Don’t say: ‘There must be something common, or they would not be called ‘games’’ — but look and see whether there is anything common to all. — For if you look at them you will not see something that is common to all, but similarities, relationships, and a whole series of them at that. To repeat: don’t think, but look! — Look for example at board-games, with their multifarious relationships. Now pass to card-games; here you will find many correspondences with the first group, but many common features drop out, and others appear. When we pass next to ball-games, much that is common is retained, but much is lost. — Are they all ‘amusing’? Compare chess with noughts and crosses [tic-tac-toe]. Or is there always winning and losing, or competition between players? Think of patience. In ball games there is winning and losing; but when a child throws a ball at the wall and catches it again, this feature has disappeared. Look at the parts played by skill and luck; and at the difference between skill in chess and skill in tennis. Think now of games like ring-a-ring-a-roses; here is the element of amusement, but how many other characteristic features have disappeared! And we can go through the many, many other groups of games in the same way; can see how similarities crop up and disappear.

And the result of this examination is: we see a complicated network of similarities overlapping and cries-crossing: sometimes overall similarities, sometimes similarities of detail.

67. I can think of no better expression to characterize these similarities than ‘family resemblances’; for the various resemblances between members of a family: build, features, colour of eyes, gait, temperament, etc. etc. overlap and cries-cross in the same way. — And I shall say: ‘games’ form a family.

And for instance the kinds of number form a family in the same way. Why do we call something a ‘number’? Well, perhaps because it has a-direct-relationship with several things that have hitherto been called number; and this can be said to give it an indirect relationship to other things we call the same name. And we extend our concept of number as in spinning a thread we twist fibre on fibre. And the strength of the thread does not reside in the fact that some one fibre runs through its whole length, but in the overlapping of many fibres.

But if someone wished to say: ‘There is something common to all these constructions-namely the disjunction of all their common properties’ — I should reply: Now you are only playing with words. One might as well say: ‘Something runs through the whole thread — namely the continuous overlapping of those fibres.’”




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

