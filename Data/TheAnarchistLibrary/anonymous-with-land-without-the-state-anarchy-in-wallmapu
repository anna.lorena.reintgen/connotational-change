      Foundations      Fire      We don’t want autonomy      Timber!      Clandestinity      Little by little
“Walmapu Liberado 
Con Tierra, Sin Estado”

The guards at the Temuco prison search us over, and lead us into a room off the main hallway. The four men come in a little later and begin telling us their stories. They choose their words solemnly, and take long pauses. Seventy days without eating has taken its toll. “Our bones hurt, we get dizzy, tired, we have to rest a lot, lay down a lot. It’s uncomfortable going so long without eating. But we’re going to go until the final consequences. We’re putting our bodies and health on the line for the Mapuche people.”

They start with what we already know: the reasons for the hungerstrike, the Chilean state’s use of the antiterrorist law against Mapuche warriors, and the long history of their struggle. When they find out we’re not human rights activists, but anarchists, they smile and warm up to us a little more. After all, the human rights organizations have shown concern for the Mapuche once they end up in prison, but have never taken a position on Mapuche independence. One of them tells us: “First Nations have given a deeper sense to the word ‘anarchy.’ We were the first anarchists. Our politics is an anti-politics.”

José Llanquileo is four years into a five year sentence for arson. For three years he was living in clandestinity with his partner, Angelica, and for a year was one of the Chilean state’s most wanted fugitives. In 2006, the two were finally captured. She was acquitted on charges of illegal association, under the antiterrorist law. He was convicted for burning pine trees on a forestry plantation belonging to a major logging company, as part of a land reclamation action. Now he gets work release during the day, and furloughs on the weekends, so he has time to take us around Temuco, introduce us to the hungerstrikers, and tell us his story.

We’ve come here as anarchists, to learn about the Mapuche struggle, to tell about our own struggles, to see where we have affinity, and begin creating a basis for long-term solidarity.

Fortunately, we can start on a good foundation. The leftists have had a patronizing attitude towards the Mapuche, says José, but “the anarchists have been very respectful, and shown lots of solidarity. I think we should be grateful for that.” He’s clear, however, that the Mapuche’s struggle is their own. Marxism was influential at a certain moment, but they are not Marxists. One could characterize the Mapuche way of thinking as environmentalist, but they are not environmentalists. They have affinity with anarchists, but they are not anarchists. “We are Mapuche. We are our own people, with our own history, and our struggle comes directly out of that.” Contrary to the assertions of the leftists, the Mapuche are not the marginalized lower class of Chilean society. They are not the proletariat, and the idea of class war does not correspond to their reality. Consequently, they may find some affinity with the revolutionary movements that developed in the context of class war in European society, but these movements do not adequately address their situation.

“The Left consider the Mapuche as just another sector of the oppressed, an opinion we don’t share. Our struggle is taking place in the context of the liberation of a people. Our people are distinct from Western society.” Moreover, the Mapuche people have a proud history of fighting invasion, resisting domination, and organizing themselves to meet their needs and live in freedom, so their own worldview and culture are more than sufficient as an ideological basis for their struggle.

This point is stressed by nearly everyone we meet, and I think our ability to become friends and compañeros rests directly on the fact that we respect their way of struggle rather than trying to incorporate them into our way of struggle.

I want to be upfront with the people I meet, with whom I want to build relationships of solidarity, so on the first day I tell him my motivations and assumptions. The comrades who put us in touch already told José I’m an anarchist, and informed him of the kind of work I do, so the fact that he invited us into his community and took time off to guide us around is a good sign. I let him know that many US anarchists already have a little familiar with the Mapuche struggle, and our understanding is that their culture is anti-authoritarian, and they organize horizontally. Is this correct?

José says it is, but I notice a little eurocentrism on my part, a difference in worldviews, when he automatically replaces my word, “horizontal,” with the word “circular,” to describe Mapuche society. There is no centralization of power among the Mapuche, who in fact are a nation of several different peoples, living in different geographic regions, and speaking different dialects of the same language. The land belongs to the community, and it is maintained collectively, as opposed to individually or communally. Each community has a lonko, a position generally translated as “chief,” but each family has a large degree of autonomy, and many decisions are made by the whole community in assemblies. Lonkos are usually men, but have been women as well. There are other traditional roles of influence: the machi is a religious figure and a healer. Men and women can become machis, but they are neither chosen nor self-appointed. Those who have certain dreams or get inexplicably sick as children, and who demonstrate a certain sensitivity, will become machis. Then there is the werken, the spokesperson, a role that has taken on explicitly political characteristics as Mapuche communities organize their resistance. Historically there were tokis, war leaders that different communities followed voluntarily, though currently no one plays this role, as the Mapuche have not gone to war since being occupied by the Chilean and Argentinean states in the 1880s.

I ask about gender relations and how the Mapuche view things like family structure and homosexuality, making clear my own feelings but also trying not to be judgmental. José says the Mapuche family structure is the same as in European society, and there is a great deal of conservatism, pressure to marry and have children, and disapproval of anything that falls outside of this format. He thinks that maybe it didn’t used to be like that, and perhaps the Catholic missionaries and conservative Chilean society have changed traditional values. In any case, the women we meet during our limited time in the communities are all strong, active, vocal, and involved, and in the homes we stay in there seem to be a sharing and a flexibility of roles. The people in our group, meanwhile, don’t try too hard to present as heterosexual or cis-gendered and don’t have any problems.

* * *

It’s an exciting time to be in Wallmapu. All the communities in resistance are united behind the hungerstriking prisoners, but behind the scenes, important debates are taking place. The hungerstrike, based directly on the ongoing struggle (all the Mapuche prisoners are accused or convicted of crimes related to land recovery actions, such as arsons targeting the forestry companies, or related to conflict with the Chilean state, such as the seizing of a municipal bus or a shooting that gave a good scare to a state’s attorney), has focused the Mapuche nation and captured the attention of the entire Chilean population. It has won a popular legitimacy for the Mapuche struggle, undermining the demonization of the direct tactics they use and weakening the government’s position in casting these tactics as terrorism. In this situation, the Mapuche can go beyond calls for greater autonomy or land reform within the Chilean state.

“The so-called Mapuche conflict doesn’t have a solution. The demands we have necessitate a break with the framework of the state. What we demand is sovereignty and Mapuche independence. We consciously propose the historical foundations of these demands [...] Our struggle is fundamentally opposed to capitalism and the state [...] I believe we have to open a space internationally to spread our demands. The Mapuche struggle has to be internationalist, as the struggle of a people. Many of the things that affect us, like capitalism and the states that represent it, the US, the EU, are an enemy to peoples, First Nations as much as oppressed classes around the world, and that’s a point of concordance.”

“The biggest problem is the advance of capitalism, in the form of investment on our lands. This is one of the principal threats that the Mapuche face because it means the exploitation of natural resources. These resources are on Mapuche lands, so investment means the expulsion of the inhabitants,” José explains. “Even while we’re recovering our lands, this investment is going on, which endangers everything we have achieved.”

* * *

After a few days, we leave Temuco and head for the hills, to the town of Cañete, and then to the first of a couple autonomous Mapuche communities in resistance we’ve been invited into, in the area of the lake Lleu Lleu, south of the city of Concepcion. Mapuche communities have two names, or rather, the place has a name, and the group of people has another name. José’s community, Juana Millahual, at Rucañanko, sits on a steep hill above one arm of the lake. It is a small community, with just a few dozen families. José’s brother is lonko. The houses are mostly small, rectangular, wooden buildings sitting atop low stilts. José explains that the traditional houses, the ruca, had thatched instead of tin roofs, but these have been mostly burned down over the decades of struggle.

The oldest knowledge they have of the community is in 1879, when José’s great grandmother had 10,000 hectares. Now the community only has 300 hectares, but they are in the process of recovering 1000 more hectares, 220 of which they have occupied. “In these territories there is a profound transformation where big capital has exploited natural resources and where the Mapuche are trying to recompose their spaces.” They’re recovering their traditions and parts of their culture that were nearly lost, and when they retake a plot of land, they take it out of the hands of Capital “which says it exists to serve man and must be exploited. When the Mapuche occupy it, there is a revolutionary change, a profound transformation to the social, cultural, religious, and economic fabric.” When they recover land, their machis come and the whole community performs a Ngillatun, a major ceremony, to purge it from its time as private property and to communalize it.

At his house, during his weekend furlough, José tells us more about the Mapuche history. The Mapuche territories used to extend from near the present locations of Santiago and Buenos Aires, Pacific coast to Atlantic coast, south to the island of Chiloe. Farther south, on the southern cone of the continent, other peoples lived. They were hardy nations that survived the extreme temperatures without problems, but were mostly exterminated when the Europeans came.

José explains that winka, the term the Mapuche have given to the European invaders, simply means “new Inca.” Before the arrival of the conquistadors, the Inca nation were already engaging in a sort of regional imperialism, which the Mapuche wanted no part in. The Inca armies got as far south as present-day Santiago, where they were defeated and consistently prevented from advancing any farther. When the Spanish arrived, the Mapuche treated them as just the most recent invaders, and defeated them as well. It’s a point of pride that the Inca, who had an advanced, centralized civilization, fell easily to the conquistadors, while the Mapuche, who were decentralized, never did. What the Spanish couldn’t understand was that there was no single Mapuche army. Each group of communities had their own toki, and if the Spanish won a battle against one group of warriors, as soon as they advanced a little farther they’d have to face another one.

During my time in Wallmapu, I think a lot about what it means to be a people. From the traditional anarchist standpoint, a people or a nation is an essentializing category, and thus a vehicle for domination. However, it becomes immediately clear that it would be impossible to support the Mapuche struggle while being dismissive of the idea of a people.

Hopefully by this point all Western anarchists realize that national liberation struggles aren’t inherently nationalist; that nationalism is a European mode of politics inseparable from the fact that all remaining European nations are artificial constructions of a central state, whereas in the rest of the world (excepting, say, China or Japan), this is usually only true of post-colonial states (like Chile or Algeria) that exist in direct opposition to non-state nations. Many other nations are not at all homogenizing or centrally organized.

Going beyond this, though, is it essentializing to talk about a Mapuche worldview or way of life? The more I listen, however, the more I doubt my accustomed standpoint. To a great extent, Mapuche is a chosen identity. Most “Chileans” have black hair, broad faces, and brown skin, while less than 10% of the population of the Chilean state identify as Mapuche. In a context of forced assimilation and a history of genocide, choosing to identify as Mapuche is, on some levels, a political statement, a willful inheritance of a cultural tradition and hundreds of years of struggle, and an engagement with an ongoing strategic debate that perhaps makes it legitimate to talk about what the Mapuche want, what they believe, in a more singular way. At one point, when we’re talking about mestizos, José makes it clear that someone is Mapuche if they identify as such, even if they have mixed parentage. In other words the Western notion of ethnicity, which leaves no room for choice because it is based on blood quanta, does not apply. Also, the fact that the Mapuche call the Europeans the “new Inca” show that they do not have an essentializing, generalizing view of sameness between all indigenous peoples. On the contrary, many people we met specified an interest in connecting specifically with other First Nations that were fighting back against their colonization, showing that what they cared about was not a racial category, but a struggle.

So if Mapuche is a chosen identity based on a very real shared history, shared culture, and ongoing collective debate of strategy, is it actually all that different from the identity of anarchist? Well, yes: it has a longer history, tied to a specific geographic territory and cultural-linguistic inheritance. Anarchism also contains a greater diversity of worldviews, but on the flipside no one I met tried to present the Mapuche as homogenous, even as they talked about a Mapuche worldview.

In sum, the concept of belonging to a people brings a great deal of strength to the Mapuche struggle. Because the state falls outside of and against that people and their history, I find some elements of the Mapuche reality, of their world, to be a more profound realization of anarchy than I have found among self-identified anarchists. And considering that those anarchist movements that have been able to maintain just 40 years of historical memory (Greece, Spain) are consistently stronger than anarchist movements that have a hard time even understanding the concept of historical memory (US, UK), it is no surprise that the Mapuche, who maintain over 500 years of historical memory, are so strongly rooted that they seem impervious to repression.

For the third time, I carry a smoldering branch from the cooking fire to the nest of dry kindling I’ve placed in the brush, and finally it catches, and the orange feathers flap and flutter like a bird stuck in a thorn bush. Despite all the anarchist romancing of fire, I’ve never before thought of arson like this.

Angelica and José have taken us to reclaimed land, a plot well suited for farming, where the hillside isn’t so steep. José is driving the team of oxen over the acre that was cleared last year, pulling a heavy iron plow through the earth to make furrows for sowing potato and onion. The adjacent acre has already been seeded with barley. Angelica, meanwhile, is tending the fire. One fire is patiently cooking our lunch, while its children are spreading through the brush to clear the earth for next year’s fields. And my friends and I are helping. Environmentalists starting forest fires, I snicker.

Of course, there’s been no forest here for decades. This was a pine plantation on stolen Mapuche lands, identical rows of genetically modified, non-native pine trees planted by Forestal Mininco, a company owned by one of the wealthiest families in Chile. Ten years ago, a number of hectares were taken over by community members. At first, only the most politically active members of the community dared to participate in the re-occupation, and some others would come out to cook or otherwise give support. When the courts found out that both the community and the timber company held titles to the same land, they declared they could take no action, and on the ground the community members have overwhelmed the forestry employees. Now, it’s basically a done deal, and the whole community comes out to farm the recovered land. Each family has its own plot of land that inheres to it individually. The recovered land, meanwhile, is communally owned and collectively maintained. One family will work a specific plot one year, but another family might work the same plot the next year. When needed, the whole community will get together to talk about how to use the land, but they seem to prefer to work things out on their own and informally, within the framework of common understandings of what’s proper.

Soon enough, we figure out how to work the wind and fuel, and here and there, flames leap twenty feet to the sky before calming down and slowly gnashing through the thorn bushes and old pine stumps. It’s a small section we clear, not even a quarter acre, but it’s not bad for a day’s work, and the watchword of the Mapuche I meet seems to be “poco a poco.” Little by little.

Angelica finds me an herb, sietevenas, for me to press against the thorn-cuts on my ash-black hands, and then I walk down to the lake, the Lleu Lleu, to cool off in its waters.

* * *

Mapuche land takeovers began in the early 1990s, after the end of the Pinochet dictatorship, with groups like Consejo de Todas las Tierras. They would take over plots of usurped Mapuche lands for one day, symbolically, to remind themselves and the world that it was their land. It was an important step forward, but like any step forward, it wasn’t enough. “It didn’t frighten the big companies.” Angelica tells me how subsequently, in 1998, the C.A.M. formed, Coordinadora de Arauca-Malleco. By developing the tactic of “productive recoveries,” the C.A.M. “enraged” the landlords. They recovered land for good, coming in with a group of thirty people to cut down the trees, turning timber plantations into gardens so Mapuche communities could feed themselves. Back in Temuco, when I asked about all the “C.A.M.”s I saw graffitied on the walls, José had joked that “C.A.M. was to the Chilean state what Al Qaida is to the U.S. government.”

Angelica tells us how both she and José had been members of C.A.M., and it too was an important step forward, but they left the organization when they realized it had a fundamentally leftist way of thinking, “not truly Mapuche. We’ve always survived because we have our own way of thinking. We can build solidarity with the Left but we can’t become part of it; that would be against who we are.”

I ask if the land recovery actions sometimes involve replanting native forests. Angelica says that some Mapuche are replanting native tree species, and perhaps it needs to happen more often, but for now they are focused on planting gardens so they can win the ability to feed themselves, and create their independence at an economic level.

Later, she tells us about living in clandestinity. “For one thing, you don’t have any peace of mind. On top of that, you can’t plan for the future or have any projectuality. While you’re eating breakfast, you’ll be keeping your eyes on the road outside, ready to run at any time.” One time, a caravan of 400 cops with buses, tanks, water cannons, and jeeps came to arrest them, a huge display of force to show the futility of resistance. But Angelica saw the caravan when it was still on the other side of the lake, and they ran for the hills. “The whole path was green” with uniformed police.

Angelica gave birth to their son while the two were underground. Eventually they were caught when a neighbor became an informant for 500,000 pesos (about a thousand dollars). Angelica spent 4 months in pretrial detention and went through three trials, but was ultimately acquitted of “illegal association” under the antiterrorist law. Before being accused she had almost completed university, everything except the final exams, but it was a Catholic school and they wouldn’t let her take the exams in jail so she never got her diploma. Now, in her community on the banks of the Lleu Lleu, she smiles at the thought of university.

On the way back from the fields, José has me help him return the oxen and the plow to the neighbors from whom he has borrowed them. He talks to the oxen in a special language or touches them on a shoulder with a long stick to guide them through the turns, and they need no more prompting than that. As we walk he tells me more about the Mapuche worldview. “Unlike Western society, the Mapuche don’t see humans as the center of the world. We don’t think humans are the perfect species that can dominate all the other species. We understand that we are just a part of the world.” In turn I tell him about debates anarchists have had, regarding animal liberation, ecocentrism, and veganism. When we reach the neighbors’ house, the oxen bow their heads so we can untie the yoke, and then they wander off in search of hay. We take a shortcut back to the house, following the path he and Angelica used to escape the police, a few years earlier.

Daniel and Miriam live in the community Juan Lincopan, at Ranquilhue, with their three daughters. The community consists of about 300 families living on 400 hectares, and is trying to retake another 1000 hectares. They live amongst gentle, rolling hills, partially forested, above the northwest part of the Lleu Lleu. Alongside their house, which they’ve just finished building themselves, Daniel and Miriam have a large garden, and higher up on the hill a field for potato and barley. Someone in the community owns a tractor he rents out for plowing, otherwise they would plow with oxen. In their garden they practice organic agriculture, though they haven’t yet begun to implement this practice in the fields.

They have chickens and a steady supply of eggs, dogs that live in the space under the house and warn of anyone approaching, they make their own bread and cook and heat the house with a wood stove. The house has a water connection but no sewage; all the graywater drains into the garden, and at the edge of the yard is an outhouse.

Proudly, Miriam shows me a line of trees they have planted near their house, all native species like the notro, haulli, arayan, and hazelnut. “We found the seedlings up in the mountains and brought them down here,” she explains. The top of the hill is still covered in exotic eucalyptus trees, which drain the water table, but they’re harvesting the eucalyptus for firewood and slowly replacing them with native species.

They want their daughters to go to school at least until they learn how to read, but there doesn’t seem to be any great pressure to attend. During the days that we stay with them, one daughter seems to be playing hooky permanently. Miriam says she likes to bring her daughters along on land recovery actions so they can get a sense of the struggle, and an understanding that all this is their territory.

In the past, most young Mapuche went to the cities but now an increasing number are staying in the country. What they really need now is an independent school in their community, that will not train Chilean citizens but will be based in the Mapuche worldview.

Both Daniel and Miriam used to belong to the C.A.M. but they have since left it. “The C.A.M. came from the outside and did their work very well, but after the actions they’d leave, and who would receive the consequences? The community. We don’t think that’s a good strategy. We work inside the community to make the struggle from the inside. Even if it takes 15, 20 years.”

C.A.M., though it was the most radical Mapuche organization until recently, proposes autonomy instead of independence, meaning that the Mapuche would receive cultural and political rights, and perhaps their own regional government, within the Chilean state. Some of their lands would be returned to them, though ownership would still be formulated according to the existing capitalist laws. An increasing number of Mapuche are beginning to think that the time has come to openly propose independence, restoring the pre-1880 borders, as guaranteed by multiple treaties with the Spanish crown and the Chilean state, and restoring a sovereign Wallmapu, self-organized according to its own cultural traditions, circular, ecocentric, decentralized, and nonhierarchical.

We talk with Daniel and Miriam about all the similarities between the struggles in Wallmapu and in Euskal Herria, the Basque country. The Basques have won an autonomous government within the Spanish state, and some cultural rights for the preservation of their language, coupled with an even stronger repression that applies the antiterrorist law, torture, and long prison sentences against anyone who fights by any means for the full independence of the Basque people. If that’s autonomy, “then we won’t fight for autonomy,” laughs Miriam.

As the Mapuche struggle strengthens, the repression also becomes more effective. In the past, the police would come into Mapuche communities and get lost, but now they know where everything is. Now there are also police experts who know Mapudungun, the language, and there are more infiltrators, like one university student from Concepcion whose testimony led to several arrests, and who is currently working in Mexico, they say. “Bachelet,” the Socialist president who preceded Piñera, “had two faces. She showed a nice face, and then sent in the repression. There was more repression with her than there is now.”

In fact, a number of young Mapuche were killed by police during the previous government. Three cases are best known, and their names grace the walls of many towns and cities around the Mapuche territories. Alex Lemun, shot in the head near Angol. Matias Cachileo, shot in the back in January 2008 on the estate of a big landlord, his body fell into a canal and had to be fished out. Mendoza Colliu, shot in the back in August 2009. “All of them were shot from behind, none in the front,” Daniel explains gravely. “They were all running!” Miriam adds, and they start to laugh.

They talk about how the struggle is growing beyond the exhausting cycle of action, arrest, and support, and how they need to develop a legal aid organization, as a shield, to function alongside the more militant parts of the struggle.

The Mapuche are by no means victims. These confrontations have taken place during a forceful struggle for the recovery of their lands. At Ranquilhue, there used to be some trailers where timber employees lived, watching over the usurped lands. Around the 2004, the houses were set on fire, and the workers and their families were burned out. Then the state set up a makeshift command center where a number of police lived, to guard the timber plantation. Chile’s strategy of control is highly legalistic, so instead of hiring mercenaries or paramilitaries as they might in other countries, the timber corporations rely directly on police protection.

Around 2006, it was time for the police to go. This time, the community members didn’t come in the night, but in the daytime, in their hundreds, and forced out the police. Since then, that particular pine plantation has been unprotected, and the community has begun clearing it so they can plant fields. Up until then, the forestry company wanted to rent out land right on the banks of the lake, the Lleu Lleu, to build a tourist hotel. The local Mapuche would receive employment, Daniel relates with disgust, working at the hotel for the tourists, selling them vegetables, cleaning their toilets. After the police were forced out, the hotel project was put on hold. Some tourist cabins owned by outsiders were also torched around this time. Around Ranquilhue there are a few vacation cabins owned by Mapuche and rented out in the summers to generate some income, but they are low key and exist on the terms of the community members themselves.

There is, however, a problem with indigenous capitalism. Daniel and Miriam relate one story of a community member who used his lands for small-scale agribusiness, and others who kicked out the logging companies only to continue to harvest and sell exotic trees on that land. But the only companies able to buy the lumber were the very same logging companies, so in the end they didn’t care who controlled or managed the land as long as it continued producing under a capitalist logic. Recreating capitalism within their struggle is a recognized danger.

And then there are Mapuche politicians. There are those who work with the government, and those who try to form political parties to co-opt the struggle, “but there is no Mapuche political party, it doesn’t exist, because we closed the door and they’re left on the outside.”

“The Mapuche can have their independence, but if they lack the spiritual side of things it’s nothing. A Mapuche without newen is not Mapuche.” Newen, they explain, means strength, but it is also the strength of nature, or the energy one receives from the natural world. “The time when the sky goes from dark to light is when you receive all your strength.” Accordingly, there is a specific Mapuche ritual that one undertakes in times of difficulty, getting up before dawn to ask for strength and draw on the power of the world.

A fifty year old pine tree, standing one hundred feet tall, does an incredible thing to sound as it falls towards the earth. First, when it first starts to lean, you are incredibly attuned to every creak and crack of wood. Then, as soon as nothing is left holding it up, the gravity of its fall pulls all other sounds with it into an acoustic black hole, and the whole world goes silent. Then, just before impact, you become aware of a terrible wind, as all the thousand branches pull faster and faster through the air. Finally, when the great tree hits the ground, you feel the thunderclap in your very bones, as if for a moment, you are the tree.

It’s a sad thing to kill such a great tree, especially with a tool so crude as a chainsaw. But death can also be an occasion for joy. This tree should not be here, and in its body is held fifty years of richness, that a colonizing company tried to steal, but that will now be returned to the soil from which it came. Next year, there will be a fire, and the ashes will come back as potatoes or arayan trees.

We spend the day walking through the plantation, cutting down trees here and there, staying on the move. At one point we pass the burned out remains of old trailers, spraypainted with Mapuche slogans, and the flipped over, rusted wreck of a police car. Here there was a battle, a victory. The land reclamation on this terrain is a new one, but already a field has been plowed in its midst. Next year there will be more. Little by little.

On the road fifty meters away, a big red pickup truck stops abreast of us. It could be company workers, or undercover police. We run into the woods. The struggle continues.

It’s late at night when we’re taken over winding roads to the place where Juan Carlos Millanao is hiding. He’s on the run, living a few months now in clandestinity, accused of a crime under the antiterrorist law. The prosecutor is seeking 73 years imprisonment in his case. He tells us his story.

He left his community at 16 years old, and lived in the city for nine years, homeless, learning a few different trades in order to survive. In 1990, he returned home, but found that no one was talking about struggle. So he went to Santiago, where he found a job in a mine in the north. For ten years he worked in the mine, occasionally coming back home to bring money and participate in the struggle, leaving again before he appeared on the authorities’ radar. For years he supported the struggle and evaded arrest. After ten years of work, his community finally took over the estate that had usurped some of their lands, and he returned home to live and think about independence. “I have to fight for my people,” he says.

“The Mapuche struggle has all it needs,” he tells us. “And we’re always three steps ahead of the state.” They’re very strategic, he explains. Once, they took over an estate surrounded by a moat and electric wire and guarded by police. Hundreds of people came to protest and confront the police at the front of the estate, and then others appeared inside the property, behind the police, and burned it down. The police never figured out how they got in, he chuckled.

Clandestinity, explains Juan Carlos, rarely lasts more than two years before the fugitive is caught, but going on the run can show contempt for state justice and a refusal to submit to their institutionality. The time on the run can also make things more difficult for the prosecution, as witnesses are lost or change their story.

At another point in the conversation, he explains how important the lake is to people in that region, and how it’s no coincidence that it’s kept so clean. Earlier, we had been brought across the lake on a little motor boat, because travelling from one side to the other was almost impossible, but usually the lake is undisturbed, and a motorboat will never be left in the water when it is not in use. He jokes that if any tourists came and tried to jet ski on the Lleu Lleu, they would be taken care of. “We Mapuche are very good with a rock and sling,” he smiles.

When I ask about their relations with the anarchists, he agrees that they give good solidarity, but the Chilean anarchists “lack newen.” The spiritual side is completely missing, and that’s a great weakness, he says.

The hungerstrikers in the Temuco prison also underscored the importance of their spirituality. Their machis had been able to visit them and tend to them while they were locked up, and this support allowed them to go much longer without eating.

Mauricio Huaquillao, one of the hungerstrikers in the Temuco prison, told us: “We want to rebuild our people, and this project directly leads us to confront the state by all means possible. Until now we haven’t had any armed organizations. We’re dangerous for our ideas.”

The best solidarity is based in personal relationships and reciprocity. As anarchists continue to support the Mapuche struggle and attack those who repress them, we also gain a great deal, as the Mapuche burn out the shallow foundations of State and Capital in one corner of the world, and continue to develop a profound struggle we can learn a great deal from.

We also have been struggling for hundreds of years, but we need to revive these roots in order to gain strength from them. We can see that there are a hundred paths of struggle, and a hundred kinds of anarchists who are not anarchists, comrades who are different, and separate, but reflect a little of ourselves, in our shared love of freedom and hatred of domination. As anarchists, we do not need to belong to the same organization as them, use the same strategy, or adopt the same worldview, to fight together and apart as comrades, because we are against the world of forced centralization.

Hopefully, they will succeed in moving from autonomy to independence, and hopefully we will succeed in finding roots in rootless countries, and developing a strength that goes far deeper than repression.

For Land and Freedom, we continue.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“Walmapu Liberado 
Con Tierra, Sin Estado”

