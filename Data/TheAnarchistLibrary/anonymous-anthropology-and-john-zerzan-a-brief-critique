
Anthropology, the study of human beings throughout time and space, is often criticized as anthropocentric. This is because anthropologists tend to extract human beings from their natural environments, looking at them as isolated animals above all others, and the dominatrix of nature. Anarcho-primitivism, the radical critique of civilization which uses a great deal of anthropological and ethnographic data tends to reconnect humanity with nature.

John Zerzan is anarcho-primitivism’s most famous and vocal thinker. He has published many texts in which he gives a thorough argument against civilization.  He traces the demise of civilization back through domestication, time, art, numbers, language, and symbolic thought. His arguments linking these to the rise of civilization, and thus our downfall as a species, are powerful. He paints a marvelous and accurate picture of how human beings lived as gatherer-hunters: peaceful, egalitarian, sustainable, affluent, little to no division of labor, face to face, unmediated with the Earth, and adventurous. He looks at the lifestyle and overall health of both pre-civilization foragers, and today’s remaining gatherer-hunters — who are not living fossils but people who have managed to resist such a force as civilization. He uses a library full of ethnographic data to show how gatherer-hunters worked far less, participated in their own society more, were able to do everything they needed to do to survive and live, were healthier psychologically and physically, and possibly even happier than we are today. The evidence is apparent from ethnography, historical writings, Native writings, and bioanthropology. However, regarding perhaps his most profound and deep attack on the origins of the totality of civilization — symbolic thought and language — there are some problems with his argument.

The problems can be looked at in two ways: superficially and deep.  Superficially, there is a problem with his argument against symbolic thought in pre-civilized human beings. While many philosophers, historians, and even some anthropologists have stated that there is something in symbols and symbolic thought that leads to the alienation of the human species from nature and each other. While this is an interesting proposition that has some merit, there is no way we can tell if pre-civilized human beings (gatherer-hunters, scavengers, etc.) has symbolic thought. This is something that does not appear in the fossil record.

However, cave paintings and rock art, dating back to around 50,000 years ago — well before civilization — can be seen as evidence of symbolism. The image of animals are transferred to rocks and cave walls for purposes unknown (didactic?  magic?). The symbols, however realistic, stand for something outside themselves — the animals in the lives of human beings. Making most of the animals pregnant also depicts symbolism, as most of the animals around these early human beings were most likely not pregnant all the time. The fact that these painting were found in hard to reach and hidden places shows that there was a special meaning to these acts.

Similarly with the argument against language, there are problems. Obviously all contemporary human beings — civilized or “primitive” — have language. There is no such thing as a “primitive” language, meaning a simple or not complex language. All languages are filled with complexities regarding syntax, phonological combinations, pronunciation and inflection, etc. Furthermore, all anthropological data regarding the origins of language look at two things: brain casts and the hyoid bone. Certain regions of the brain are considered to be areas in which we learn language, and thus brain casts — literally molds made from the inside of skulls — are supposed to shed light on who could have acquired language. Likewise, The hyoid bone is the bone responsible for the ability to create human speech, and is supposed to be an indicator of language.  Zerzan is correct in quoting those who claim that we will never know the origins of language, but perhaps the Neanderthals may make a case against Zerzan’s critique of language. The Neanderthals (100,000–30,000 years ago) possessed extremely large brains (larger than modern humans) and hyoid bones. They created art, tools, buried their dead, and had some form of religion, spirituality, or belief in the after life (inferred from grave goods, red ochre, and flowers found with the dead as well as bear skulls positioned in caves). Obviously “primitives” most of the evidence says they have symbolic thought and language.

Saying that language is a human only creation is highly anthropocentric, and this is what we can infer from Zerzan’s argument. While Language (capital L) may be a human creation, language, or communication, is not. All animals have communication systems, from the waggle dance of the bee (where there are different waggle dances for different reasons and they cannot be understood outside their regions), to the bark of a dog, to the calls and expressions of primates. To claim that human communication is complex, and thus separate is ridiculous and anthropocentric. The fact that we cannot understand animal communication proves their complexity. Even Zerzan’s claim that animals don’t have language because they hunt is wrong. Animals are constantly communication, through sound or body movement, the signs of food, danger, etc. Some remain silent during the actual climax of the hunt, but silence too is a form of communication.

Zerzan, in his writings on language, brings up a debate regarding which came first: tool use / technology or language. In fact, they are both forms of technology, and symbolic thought is required for both. The ability to have arbitrary sounds (words or grunts) to stand for something in reality is essentially language, which requires the projection of the symbolic — symbolism.  To look at a few rocks and visualize a scraper or atlatl is symbolic thought — most often called productivity, or the ability to think about the past, present, and future. These are not human hallmarks. Animals, especially chimpanzees, have displayed symbolic thought, in both language and productivity. No doubt it was understood through civilized means, but it shows that these animals do think about things beyond their immediate survival. Stripping an animal of symbolic thought is turning it into a mere machine — acting and reacting only in response to external stimuli without thinking about it. This is the rationalization of the Enlightenment — a definite precursor to planetary human destruction.

At times, it seems as though John Zerzan is creating a unilineal scale of civilized evolution, beginning with the invention of symbolic thought, and then language, art, numbers, time, agriculture, etc. Since these things increasingly stretch back in time, it may seem as though civilization was inevitable. But I know this is not his point, and this is far from the truth. So where does it leave us? Obviously language puts constraints of our ways of thinking and acting, but so too does the natural environment. There will always be a “limiting factor”, if you want to call it that, to human freedom. Inuits can’t run around naked all the time, even if they really want to, because they’ll freeze to death. The !Kung can’t build boats and other watercraft because their environment doesn’t allow it. This does not mean we are slaves to nature, but our ways of life can often be a reflection of our greater context.

John Zerzan’s critique, especially of symbolic thought and language, is extremely interesting, and it generates much thought. My comments, if they are correct, should not throw out any portion of his arguments, for they are all very important. Perhaps Zerzan’s argument against language should have focused more on the written aspect of it, which definitely coincides with domestication and takes and entire system of domination and alienation to uphold.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

