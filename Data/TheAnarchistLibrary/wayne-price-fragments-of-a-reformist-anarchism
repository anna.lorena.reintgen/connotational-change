      A review of David Graeber (2004), Fragments of an Anarchist Anthropology      “Why are there so few Anarchists in the Academy?”      Reform or Revolution?      Conclusion      References
A collection of scattered thoughts about anarchism, anthropology, and academic studies is reviewed. David Graeber argues against the need for a revolutionary confrontation with the state or its eventual overthrow. Instead, he favors a gradualist approach which leaves the state alone.

In attempts to build an anarchist theory, there has been a natural interest in anthropology. Anthropology demonstrates that, for most of human existence, people have lived in stateless, non-market, societies. Rather than there being one kind of “human nature,” people have been very flexible about how they interact and organize themselves. Anarchists have examined this anthropological data, such as Harold Barclay’s People without Government, An Anthropology of Anarchy (1990, see also Barclay, 1997). When it comes to understanding the state, the market, and the family, even Marxists look to anthropological data, as in the classic The Origin of the Family, Private Propery, and the State by Engels (1972). That work is now out of date, of course, but still interesting (see the Introduction by Eleanor Burke Leacock, pp. 7 — 85).

This little book by the anthropologist David Graeber is not the same thing. It does not examine political and economic structures of hunting, gathering, and horticultural societies to show what anarchy has been like in practice, how societies without rulers have made decisions and maintained order. If you want that, you should read Barclay (whom Graeber never mentions, oddly enough). Instead, it is, he explains, “a series of thoughts, sketches of potential theories, and tiny manifestoes,” which he hopes will contribute someday to a “body of radical theory.” That is, it is “fragments.”

These scattered thoughts are in a short book (105 small pages). They include brief comments and (relatively) longer commentaries. For example, early on he raises the differences between anarchism and Marxism as movements. This is an important topic which shows the different ways anarchists and Marxists relate to their “founders.” Other tid-bits include a fragment on democracy and consensus. As I do, Graeber sees anarchism as a form of direct, radical, democracy, unlike those anarchists who reject democracy. He raises other fractions of subjects.

However, there are two main topic fragments in this book. The first is the one he begins the book with, “Why are there so few anarchists in the academy?” In the U.S. there are thousands of Marxists working in colleges and universities, but only handfuls of anarchists. Graeber is particularly concerned about the lack of anarchist professional anthropologists, his field, but also asks the question about other academic fields such as political science, sociology, economics, or literary theory.

Graeber suggests that this may be due to an anarchist orientation for action rather than theory. Perhaps even more, he suggests that anarchist thinking does not fit in with colleges and universities as well as Marxism does. Anarchism is too rebellious, too bottom-up, he suggests, to fit into academic departments. This too may be true, although it may give anarchist professors too much credit.

In any case there is a simpler reason, not considered by Graeber, for the relative lack of anarchist professors compared to Marxists. Most of the current U.S. Marxist professors became radicalized in the 60s and 70s, when Marxism-Leninism was the mainstream of radicalization. This was caused by the attraction of the governments of Cuba, North Vietnam, and China, which appeared to be fighting Western imperialism in the name of Marxism. Many young people were radicalized and then, inexorably, became older (!). As the radicalization died down, a layer of Leninists went on to graduate school and professorships (the “academy”). More recently there has been the beginning of a new radicalization. This time, Marxism-Leninism is not so attractive, due to the collapse of the Soviet Union and events in China. Many young adults are being radicalized as anarchists. They are still too young to have completed graduate school, those who will do so. It is simply too soon to expect the Marxist professors to be followed by a wave of anarchist professors.

Personally I do not find the lack of anarchist professors to be an important or interesting topic, which does not mean that the author should not write about it if he choses. Since writing this book, David Graeber has been unfairly “let go” (that is, de facto fired) from being assistent professor of anthropology at Yale University. His anarchist political views probably played a part in his being dropped from the Yale anthropology department. He deserves our support in any conflict with Yale.

Graeber displays a weakness of an anarchist orientation to anthropology. That is, he is concerned to show the continuities between modern society and pre-capitalist, pre-industrial, societies. This has value. However, to make a radical change in our society requires a concrete analysis of the current world, in particular of the nature of the state and of the class structure of capitalist society. The argument for revolution is based on the nature of the bourgeois state and on the capitalist ruling class which it serves, as well as on the strengths and weaknesses of the working class under capitalism. These topics are not covered in anthropology.

Graeber rejects the concept of a workers’ revolution without making any analysis of the ruling class or the state. He rejects the idea of a revolution on the model of the U.S. Revolution, the French Revolution, the Russian Revolution, or the 1956 Hungarian Revolution. He rejects the revolutionary perspectives of Marx, Bakunin, Kropotkin, Luxemburg, Malatesta, and Goldman. He sneers at popular uprisings as “apocalyptic revolution[s]” (p. 19) or “cataclysmic ruptures.” (p. 44)

The only theorist of revolution whom he discusses is not Marx or Kropotkin but Georges Sorel. Sorel was an elitist who became a proto-fascist. Contrary to Graeber’s implications, Sorel was not an influential anarchist-syndicalist. Marginal at the most, he was influenced by anarchist-syndicalism, but anarchist-syndicalism was not influenced by him. Graeber focuses on Sorel, rather than Luxemburg or Malatesta, in order to discredit revolution.

Graeber states, “Revolutionaries...have increasingly abandoned even talking about seizing power.” (p. 2) “Revolutionary action does not necessarity have to aim to topple governments.” (p. 45)

He does not distinguish between “taking power” and “taking state power.” (Price 2006b) Taking power means that the workers and oppressed smash the state (“topple governments”) and take over society, running it in a new, radically democratic, way, using federated councils and assemblies, in workplaces and neighborhoods. But “taking state power” is a different matter for anarchists. it would mean setting up a new state, a socially alienated and bureaucratic machine with specialized layers of police, military, officials, and politicians. Revolutionary anarchists should be for the oppressed taking power but against the establishment of a new state.

Graeber is explicitly anti-revolutionary. But he makes only two arguments against “challenging power head on (this usually leads to being slaughtered, or if not, turning into some — often even uglier — variant of the very thing one first challenged).” (p. 61) These are the usual liberal arguments against revolution. Liberals agree with conservatives that the government should not be toppled and the workers should not take power. The last thing they want is for the working class to take power! To justify this, liberals express fears, just like Graeber, of massacres or new tyrannies.

The danger of being “slaughtered” should be an argument in favor of making revolutions which are successful, not for rejecting revolutions altogether. Of course, revolutions can fail, as can reformist struggles. But some of the worst massacres occured when the people failed to make revolutions, because they followed only reformist programs. Nazism came to power after the failure of the reformist, gradualist, policies of the German social democrats. Pinochet took power due to the reformist policies of Allende in Chile in the 70s — which irritated the rich and military but did not threaten to overturn them.

As for the danger of “turning into [an] even uglier variant of the very thing one first challenged,” this implies that revolutions have never made any improvements. Yet the U.S. Revolution created a bourgeois democracy with expanded freedoms for many people. The French Revolution gave land to the peasants, which lay the basis for an eventual bourgeois democracy. Of course, no revolutions have produced self-managing socialist-anarchist societies. Neither have nonviolent reformist strategies. Nothing has yet. This is not a reason for giving up or for believing that reformist strategies will work any better. Essentially this is the cynical belief that the oppressed are unable to get rid of the old rulers and manage society for ourselves. True, there is likely to be a tendency to recreate the old authoritarianism during a revolt. This is why anarchists need to organize ourselves to fight for libertarian self-management among the people. (Bookchin 1986)

Instead of revolution, Graeber advocates a nonconfrontational policy of gradual, peaceful, change by incremental steps, building up alternate institutions around the state and the market. “...The question [is] how to neutralize the state apparatus itself, in the absence of a politics of direct confrontation....Perhaps existing state apparati will gradually be reduced to window-dressing....There are times when the stupidest thing one could possibly do is raise a red or black flag and issue defiant declarations. Sometimes the sensible thing is just to pretend nothing has changed, allow official state representatives to keep their dignity, even show up at their offices and fill out a form now and then, but otherwise ignore them.” (pp. 62 — 64) Actually this is not what he advocates for just “sometimes” but for most of the time and maybe all the time.

Graeber expresses his agreement with the Italian autonomous Marxists who advocate “revolutionary ‘exodus’....The most effective way of opposing capitalism and the liberal state is not through direct confrontation but by means of...mass defection by those wishing to create new forms of community....slipping away from [the] grasp [of power], from flight, desertion, the founding of new communities.” (pp. 60, 61)

As evidence that this would work, Graeber cites examples from anthropology, particularly his own experience. According to Graeber, in the 1980s, the overall state of Madagagar pretty much collapsed due to a financial crisis. The state stopped providing most services or intruding in people’s lives through the police. Malagasy people stopped paying most taxes. People went to government offices and filled in legal forms but otherwise ignored the state. “In some sense this was indeed a revolution.” (p. 33) Although, he adds, “How long it would last is another question; it was a very fragile, tenuous, kind of freedom. Many such enclaves have collapsed.... Others endure.” (p. 34)

This is not much of a recommendation for this strategy. It may have worked in a poor, oppressed, nation — during an economic crisis — in marginal parts of the country — for a brief period. But more industrialized, centralized, nations, closer to the center of world imperialism, with mighty militarized states, do not so easily give up power. The ruling class will not permit its wealth and power to be taken away by even the most nonviolent and democratic methods. Its members are not so stupid that they will not figure out what the methods of “exodus” are — and oppose them with the full force of its state.

The anthropologist Barclay cites “the obvious anarchist truth that the state is an institution which will not voluntarily abdicate its power. Those in power...will act to suppress any perceived threat to their positions. The state in a modern capitalist society...may readily tolerate...credit unions and cooperatives....This support would soon change to suppression if such movements became a threat to the banking and corporate interests of the country.” (1990, p. 143)

And Graeber admits this! Elsewhere in his little book, Graeber states that efforts to bring about a new and better world “would meet with stubborn, and ultimately violent, opposition from those who benefit the most from existing arrangements.” (p. 1–2) Indeed it would. That is why we advocate revolution. This recognition by Graeber completely contradicts his strategy of gradual, nonconfrontational, change.

Let me give an example from history, not from anthropology. In an Introduction to a book on pre-Nazi Germany by Daniel Guerin (1994), the translator writes, “In Germany...adherence to either the Social Democratic party...or to the Communist party...made one part of a cultural community, not just a narrowly political one. Each party boasted a range of services and institutions that embraced its millions of members from the cradle to the grave: community health projects; relief and charitable works; educational programs; musical, theatrical, cinematic, and literary activities; sports clubs; vacation colonies; travel exchange programs; libraries; clubs for children of different age groups; women’s organizations; a network of local, regional, and national newspapers and magazines; housing cooperatives; and self-defense militias.” (p. 23)

This demonstrated that a working class political movement could create a network of alternate institutions inside of, and against, capitalist society. However, once the Nazis took power, in 1933, in merely two or three months all these institutions were gone. They were either suppressed or absorbed into Nazi organizations. Having failed to make a revolution, the German working class was confronted with a strengthened capitalist state which smashed all its institutions and slaughtered its leaders and members. Alternate institutions are not enough. Confrontation and revolution are needed.

Graeber’s strategy can be compared with that of Murray Bookchin (1986) when discussing the French almost-revolution in May-June 1968. This was a rebellion which began in the universities and spread to the factories and to virtually every sector of society. A general strike broke out in this industrialized, modern, capitalist country. Bookchin believed, “Whether this sweeping movement would become a complete social revolution depended on one thing — would the workers not only occupy the plants, but work them?” (p. 285) This could have swept away the capitalist class and its system.

As for the army and police, “Had the armament workers not merely occupied the arms factories but worked them to arm the revolutionary people, had the railroad workers transported these arms to the revolutionary people in the cities, towns, and villages, had the action committees organized armed militias — then...an armed peple...would have confronted the state.” (p. 289) He does not believe that the mass of conscripts would have fired on the people, and he thinks that the revolutionary people, properly organized, could have beaten back the hard core of counterrevolutionary forces. Bookchin believes that there was a need for anarchist groupings to fight for this program.

Unlike Bookchin of that time, Graeber rejects revolution, toppling of governments or even confrontation with governments. His program is gradual, nonviolent, nonconfrontational, leaving the state alone. Yet he repeatedly refers to his perspective as “revolutionary.” This is a peculiar use of “revolutionary,” to mean nonrevolutionary reforms. Of course, he can use language in any way he wishes — it is a semi-free country. But it does not advance the discussion to use words to mean the opposite of what they usually mean.

By “revolution,” Graeber probably refers to his desire to change society completely...in the long run. I do not doubt that he is sincere about that. But historically those who wanted to totally change society by gradual, incremental, nonconfrontational, measures were called “reformists.” This included the British Fabians, the French Possiblists, the German Revisionists, and the Russian Mensheviks. Those who did not wish to change society in any deep sense were not reformists but liberals. Whatever he thinks of himself, Graeber is, at most, a reformist.

For example, Graeber offers a three point program “to alleviate global poverty,” in fact to abolish it:

“*an immediate amnesty on international debt....

“*an immediate cancellation of all patents other intellectual property rights....

“*the elimination of all restrictions and global freedom of travel or residence

“The rest would pretty much take care of itself.” (p. 78)

Graeber is aware that such a program is “unrealistic” in the sense that the rich and powerful people of the world would never enact it. But he seems to believe that such demands (all of which I am for), if actually implemented, would really end global poverty. He does not understand that world capitalism is based on exploitation, pumping surplus labor out of the workers of the world. Such a program, even if (magically) put in place, would not change international capitalism. It would not stop world poverty and exploitation. It is a liberal fantasy.

As Graeber remarks, his belief in a gradual and peaceful “revolution,” one which does not challenge the state, is widespread among anarchists and autonomist Marxists. After all, it would be nice if it could work. I have previously reviewed other advocates of this perspective, from the Parecon theory to Holloway’s “open Marxism.” (Price 2006; 2005)

The problem with Graeber is not his study of anthropology; this is valuable. It is his commitment to reformism. Such an approach fails to warn the workers that even a consistent fight for reforms will provoke a confrontation with the state. For Graeber, long-term revolutionary goals are best won through the struggle for reforms. For revolutionary class-struggle anarchists, even short-term reforms are best won through the guiding struggle for revolution.

Barclay, Harold (1990). People without Government; An Anthroplogy of Anarchy. London: Kahn & Averill.

Barclay, Harold (1997). Culture and Anarchism. London: Freedom Press.

Bookchin, Murray (1986). Post-Scarcity Anarchism (2nd ed.). Montreal-Buffalo: Black Rose Press.

Engels, Frederick (1972). The Origin of the Family, Private Propery, and the State; In the Light of the Researches of Lewis H. Morgan. NY: International Publishers.

Graeber, David (2004), Fragments of an Anarchist Anthropology. Chicago: Prickly Paradigm Press.

Guerin, Daniel (1994). The Brown Plague; Travels in Late Weimar and Early Nazi Germany. (Robert Schwartzwald, trans.). Duham and London: Duke University Press.

Price, Wayne (2005). Parecon and the nature of reformism. A review of Robin Hahnel (2005). Economic Justice and Democracy. www.anarkismo.net

Price, Wayne (2006a). An Anarchist Review of Change the World without Taking Power by John Holloway. www.anarkismo.net

Price, Wayne (2006b). Confronting the Question of Power www.anarkismo.net




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“*an immediate amnesty on international debt....



“*an immediate cancellation of all patents other intellectual property rights....



“*the elimination of all restrictions and global freedom of travel or residence



“The rest would pretty much take care of itself.” (p. 78)



Barclay, Harold (1990). People without Government; An Anthroplogy of Anarchy. London: Kahn & Averill.



Barclay, Harold (1997). Culture and Anarchism. London: Freedom Press.



Bookchin, Murray (1986). Post-Scarcity Anarchism (2nd ed.). Montreal-Buffalo: Black Rose Press.



Engels, Frederick (1972). The Origin of the Family, Private Propery, and the State; In the Light of the Researches of Lewis H. Morgan. NY: International Publishers.



Graeber, David (2004), Fragments of an Anarchist Anthropology. Chicago: Prickly Paradigm Press.



Guerin, Daniel (1994). The Brown Plague; Travels in Late Weimar and Early Nazi Germany. (Robert Schwartzwald, trans.). Duham and London: Duke University Press.



Price, Wayne (2005). Parecon and the nature of reformism. A review of Robin Hahnel (2005). Economic Justice and Democracy. www.anarkismo.net



Price, Wayne (2006a). An Anarchist Review of Change the World without Taking Power by John Holloway. www.anarkismo.net



Price, Wayne (2006b). Confronting the Question of Power www.anarkismo.net

