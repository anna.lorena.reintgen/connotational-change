
In the course of those polemics which arise among anarchists as to the best tactics for achieving, or approaching the creation of an anarchist society - and they are useful, and indeed necessary arguments when they reflect mutual tolerance and trust and avoid personal recriminations - it often happens that some reproach others with being gradualists, and the latter reject the term as if it were an insult.

Yet the fact is that, in the real sense of the word and given the logic of our principles, we are all gradualists. And all of us, in whatever different ways, have to be.

It is true that certain words, especially in politics, are continually changing their meaning and often assume one that is quite contrary to the original, logical and natural sense of the term.

Thus the word possibilist. Is there anyone of sound mind who would seriously claim to want the impossible? Yet in France the term became the special label of a section of the Socialist Party who were followers of the former anarchist, Paul Brousse - and more willing than others to renounce socialism in pursuit of an impossible cooperation with bourgeois democracy.

Such too is the case with the word opportunist. Who actually wants to be an in-opportunist, and as such renounce what opportunities arise? Yet in France the term opportunist ended up by being applied specifically to followers of Gambetta[1] and is still used in the pejorative sense to mean a person or party without ideas or principles and guided by base and short-term interests.

The same is true of the word transformist. Who would deny that everything in the world and in life evolves and changes? Who today is not a "transformer?" Yet the word was used to describe the corrupt and short-term policies pioneered by the Italian Depretis.[2]

It would be a good thing to put a brake on the habit of attributing to words a meaning that is different from their original sense and which gives rise to such confusion and misunderstanding. But how to do it is another matter, particularly when the change in meaning is a deliberate tactic on the part of politicians to disguise their iniquitous purposes behind fine words.

Maybe it is true, therefore, that the word gradualist, as applied to anarchists, could end up in fact describing those who use the excuse of doing things gradually, as and when they become possible, and in the last analysis do nothing at all - either that or move, if they move at all, in a contrary direction to anarchy. If this is the case the term has to be rejected. Yet the real sense of gradualism remains the same: everything in nature and in life changes by degrees, and this is no less true of anarchy. It can only come about little by little.

***

As I was saying earlier, anarchism is of necessity gradualist.

Anarchy can be seen as absolute perfection, and it is right that this concept should remain in our minds, like a beacon to guide our steps. But quite obviously, such an ideal cannot be attained in one sudden leap from the hell of the present to the longed-for heaven of the future.

The authoritarian parties, by which I mean those who believe it both moral and expedient to impose a given social order by force, may hope - vain hope! - that when they come to power they can, by using the laws, decrees... and gendarmes subject everybody indefinitely to their will.

But such hopes and wishes are inconceivable for the anarchists, since anarchists seek to impose nothing but respect for liberty and count on the force of persuasion and perceived advantages of free cooperation for the realisation of their ideals.

This does not mean I believe (as, by way of polemic, one unscrupulous and ill-informed reformist paper had me believe) that to achieve anarchy we must wait till everyone becomes an anarchist. On the contrary, I believe - and this is why I'm a revolutionary - that under present conditions only a small minority, favoured by special circumstances, can manage to conceive what anarchy is. It would be wishful thinking to hope for a general conversion before a change actually took place in the kind of environment in which authoritarianism and privilege now flourish. It is precisely for this reason that I believe in the need to organise for the bringing about of anarchy, or any rate that degree of anarchy which would become gradually feasible, as soon as a sufficient amount of freedom has been won and a nucleus of anarchists somewhere exists that is both numerically strong enough and able to be self-sufficient and to spread its influence locally. I repeat, we need to organise ourselves to apply anarchy, or that degree of anarchy which becomes gradually possible.

Since we cannot convert everybody all at once and the necessities of life and the interests of propaganda do not allow us to remain in isolation from the rest of society, ways need to be found to put as much of anarchy as possible into practice among people who are not anarchist or who are only sympathetic.

The problem, therefore, is not whether there is a need to proceed gradually but to seek the quickest and sincerest way that leads to the realisation of our ideals.

***

Throughout the world today the way is blocked by privileges conquered, as a result of a long history of violence and mistakes, by certain classes which in addition to an intellectual and technical superiority which they enjoy as a result of these privileges, also dispose of armed forces recruited among the subject classes and use them when they think necessary without scruples or restraint.

That is why revolution is necessary. Revolution destroys the state of violence in which we live now, and creates the means for peaceful development towards ever greater freedom, greater justice and greater solidarity.

***

What should the anarchists' tactics be before, during and after the revolution?

No doubt censorship would forbid us to say what needs to be done before the revolution, in order to prepare for it and to carry it out. In any case, it is a subject badly handled in the presence of the enemy. It is, however, valid to point out that we need to remain true to ourselves, to spread the word and to educate as much as possible, and avoid all compromise with the enemy and to hold ourselves ready, at least in spirit, to seize all opportunities that might arise.

***

And during the revolution?

Let me begin by saying, we can't make the revolution on our own; nor would it be desirable to do so. Unless the whole of the country is behind it, together with all the interests, both actual and latent, of the people, the revolution will fail. And in the far from probable case that we achieved victory on our own, we should find ourselves in an absurdly untenable position: either because, by the very fact of imposing our will, commanding and constraining, we would cease to be anarchists and destroy the revolution by our authoritarianism; or because, on the contrary, we would retreat from the field, leaving others, with aims opposed to our own, to profit from our effort.

So we should act together with all progressive forces and vanguard parties to attract the mass of the people into the movement and arouse their interest, allowing the revolution - of which we would form a part, among others - to yield what it can.

This does not mean that we should renounce our specific aims. On the contrary, we should have to keep closely united and distinctly separate from the rest in fighting in favour of our programme: the abolition of political power and expropriation of the capitalists. And if, despite our efforts, new forms of power were to arise that seek to obstruct the people's initiative and impose their own will, we must have no part in them, never give them any recognition. We must endeavour to ensure that the people refuse them the means of governing - refuse them, that is, the soldiers and the revenue; see to it that those powers remain weak... until the day comes when we can crush them once and for all.

Anyway, we must lay claim to and demand, with force if needs be, our full autonomy, and the right and the means to organise ourselves as we see fit and to put our own methods into practice.

***

And after the revolution - that is after the fall of those in power and the final triumph of the forces of insurrection?

This is where gradualism becomes particularly relevant.

We must pay attention to the practical problems of life: production, trade, communications, relations between anarchist groups and those who retain a belief in authority, between communist collectives and individualists, between the city and the countryside. We must make sure to use to our advantage the forces of nature and raw materials, and that we attend to industrial and agricultural distribution - according to the conditions prevailing at the time in the various different countries - public education, childcare and care for the handicapped, health and medical services, protection both against common criminals and those, more insidious, who continue to attempt to suppress the freedom of others in the interests of individuals and parties, etc. The solutions to each problem must not only be the most economically viable ones but must respond to the imperatives of justice and liberty and be those most likely to keep open the way to future improvements. If necessary, justice, liberty and solidarity must take priority over economic benefit.

There is no need to think in terms of destroying everything in the belief that things will look after themselves. Our present civilisation is the result of thousands of years of development and has found some means of solving the problem of how millions and millions of people co-habit, often crowded together in restricted ares, and how their ever-increasing and ever more complex needs can be satisfied. Such benefits are reduced - and for the great majority of people virtually denied - due to the fact that the development has been carried out by authoritarian means and in the interests of the ruling class. But, if the rules and privileges are removed, the real gains remain: the triumphs of humankind over the adverse forces of nature, the accumulated weight of experience of past generations, the sociable habits acquired throughout the long history of human cohabitation, the proven advantages of mutual aid. It would be foolish, and besides impossible, to give up all this.

In other words, we must fight authority and privilege, while taking advantage from the benefits that civilisation has conferred. We must not destroy anything that satisfies human need however badly - until we have something better to put in its place.

Intransigent as we remain to any form of capitalist imposition or exploitation, we must be tolerant of all those social concepts that prevail in the various human groupings, so long as they do not harm the freedom and equal rights of others. We should content ourselves with gradual progress while the moral level of the people grows, and with it, the material and intellectual means available to mankind; and while, clearly, doing all we can, through study, work and propaganda, to hasten development towards ever higher ideals.

***

I have here come up with more problems than solutions. But I believe I have succinctly presented the criteria which must guide us in the search and application of the solutions, which will certainly be many and vary according to circumstances. But, so far as we are concerned, they must always be consistent with the fundamental principles of anarchism: no-one orders anyone else around, no-one exploits anyone else.

It is the task of all comrades to think, study and prepare - and to do so with all speed and thoroughly because the times are "dynamic" and we must be ready for what might happen.

 
[1] Léon Gambetta was a prominent republican politician of the French Third Republic, until his death in 1882.
[2] Agostino Depretis was Italian prime minister nine times between 1876 and 1887. During his uninterrupted premiership from 1881 to 1887 he changed his cabinet five times, supported by majorities that shifted from the Left to the Right, based on short-term convenience rather than long-term programmes.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

