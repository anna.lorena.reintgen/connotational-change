  Part I. Anarchist Strategy: A Reintroduction  Part II      Will we need an army?      Means and Ends  Part III  Part IV. Unconventional War
Why are we concerned with anarchist strategy?

If strategy is the process of having priorities and subsequently acting on those priorities then an anarchist strategy names a discreet objective (in this case the establishment of an anarchist society upon the destruction of a capitalist and statist one) and sacrifices other priorities in the pursuit of that goal. An anarchist strategy is not a strategy about how to make a capitalist or statist society less authoritarian or spectacular. It assumes that we cannot have an anarchist society while the state or capitalism continues to reign.

We are not for more freedom. More freedom is given to the slave when his chains are lengthened. We are for the abolition of the chain, so we are for freedom, not more freedom. Freedom means the absence of all chains, the absence of limits and all that ensues from such a statement. (Bonanno The Anarchist Tension)

It is important to inform our discussions about strategy in a clear vision about what exactly our strategic goals are. If we are incoherent on this point our efforts will suffer.

An anarchist strategy differs from a military strategy. Military strategy is the conduct of warfare. Warfare is a particular technological application of violence from states, or statist bodies, upon people; sometimes citizens of rival states, sometimes people in the way, and sometimes in the name of defending the very people it violates. It differs from other forms of confl ict that could and have occured in other contexts (economic, political, and technological). An anarchist strategy should understand more about military strategy than just the tactics of the Sierra Maestra guerillas, the movements of the armored columns against Rommel, or the Miami Model — while refusing to confuse the medium of warfare with strategic goals.

Social change does not always occur due to warfare, or violence, or the threat of either. Social change (throughout the past 10,000 years of human history) can be generally understood as happening through a) conquest, b) decline, c) the power of ideas, d) economics, e) the changing of the guard, or f) revolution. Revolution is the most recent addition to this list and doesn’t have nearly the currency that those of us who were raised to believe in social change through direct action, protest, and petition believe that it does.

Because of the semiotic coding of the term “social change” within radical or revolutionary movements (embedding a meaning of such social change as being composed of entirely, or primarily, positive characteristics), examination of how societies (the various formations of humans throughout history) have changed becomes complicated. Change is not necessarily positive. Especially from a historical perspective it is probably more accurate to understand how most societies have changed, as being entirely negative. If we refer to the Eurasian exportation of values, systems, and technics upon the rest of the world we are not referring to choices made by the people of Oceania, the Americas, or Sub-Sarahan Africa in an egalitarian, or even well-informed, fashion. We are talking about imposition, warfare, genocide, and human bigotry in the most pure forms ever run rampant. We call this social change because it is, not because it is good.

Why do we use words like good, or bad, in relation to phenomena like social change? Do we understand the transformation of society from the drudgery of our industrial proletarian forebears to the happy communards we would like to become as happening through an evolutionary, or staggered, process of good things happening? If we understand history, whether strictly materialistic, sociological, or mythological, as being a series of bad events for good people and victories for bad people, are we limiting our own ability to accrue information that might actually allow us to make more successful strategic choices than scaring off the cops for 12 hours in parts of Seattle? Does our inability to stop morally coding things limit our ability to make more interesting choices?

Why am I interested in anarchist strategy? Because I would like to be truly free. I would like to be free of rent and work and shitty microscopic sectarian politics. I would like to slow down and learn more about trees and walking and many of the people I blow by out of impatience. I have these desires in such abundance that I choose to devote my limited abilities and potential to understanding how things that I abhor work and things that I love thrive. An anarchist strategy is the body of ideas about how things (societies, people, structures) have changed, how people have changed them, and the practice of being that change.

Enough of the preliminaries — let’s get down to brass tacks. Whenever the question of a total social transformation is raised, the accompanying concerns multiply during every breath taken in response. Anarchists have, by and large, rejected traditional models of Social Revolution a la France even as they have not rejected the imagery of the storming of the (metaphorical) Bastille. The question first and foremost would be how exactly would we deal with the military might that currently exists? Do anarchists need to raise an army to stand against the military might of today?

Many, if not most, anarchists feel comfortable responding to the more general question of whether the ends justify the means by stating unequivocally that they do not. Since, given this statement, most scenarios of contestation against forces of repression are going to be under conditions of tension it is safe to assume that many of them will be under terms un-anarchistic. If the ends do not justify the means and the means are evaluated as the mechanisms by which conflict would be waged, this argument does not allow for vigorous conflict. War, by any definition but the most tortured, is not anarchist. Put another way, you cannot make revolution and keep your hands clean. Radical social transformation is, just about, the most authoritarian action ever undertaken. It is pretty hard to make an anarchist case to the contrary.

The two popular approaches today to this question hedge somewhat against this question. The Mass Movement model implies that the radical transformation of society through minority action of scale (whether it be labor groups or the dispossessed) would mitigate the authoritarian reality of imposing social transformation on a docile population. In practice this model uses the language of democracy, and internally democratic (also often representative) structures, to cloak its oppositional and political (as in partisan) nature. If there were a Mass Movement on the scale of even the American resistance to the Vietnam War our generation would see these things in practice. Instead we watch or participate as activists attempt to build a movement, with greater concern to its efficacy in-this-world and its size than in its potential structural and political constraints.

The people using the insurrectionary model argue that the decision to make a radical break will happen in a time of crisis and that our task is to encourage the conditions of this crisis. Additionally, they encourage, this corresponds well with anarchist principles like direct action, resisting the state, and “action without measure.” If the action that anarchists take already corresponds to our desire, then the fact that it may not result in a radical break is of little consequence. The authoritarian aspects of this break will be shared with everyone who participates on the day-of-days and therefore doesn’t have to be examined today.

The means and ends question (and particularly the way we answer it) prevents us from asking the hard questions about whether we are being honest with ourselves about either the implications of our personal and political practice or the consequences of those actions into the future.

Possibly the question of an anarchist army should be approached in reference to a few libertarian revolutionary moments. The Russian Revolution was not won with an army; the Bolsheviks filled a power vacuum created by the handling of the German war and missteps of the Provisional Government. The army was only needed to defend the revolution. The Spanish Civil War was not fought by a specifically anarchist army but a coalition of Anarchists, Communists, and Democrats. The Paris Commune was inspired by the militias and rebelling army troops but not by force of arms against the population as much as reaction to the failure of the Franco-Prussian War.

If there is a lesson to draw here it is not so much that there is the need for an anarchist army but a need to be able to communicate with members of the military when morale is low. If the new film Sir! No Sir! reminds us of anything it is that members of the military are not unthinking automatons. This should be particularly clear after hearing the stories of the latest Gulf War when tens of thousands of Reserve soldiers gave up their daily lives. As we have forgotten the story of the rebellion within the Army in Vietnam we will not hear the story of the people who have rejected their orders to be stationed in Iraq.

Even if we were to reject the basic canon of modern anarchists that the ends do not justify the means we still would not advocate for an anarchist army. Social struggles of the past have not required such artificial contrivances, the exposure to the forces of repression that such an effort would cause would be incredible, and the paradigm of social conflict that such a question embeds is archaic. When struggle ensues next it will not look anything like redcoats lining up against bluecoats. It will likely not look like militias holding the line against the forces of counter-revolution. It will likely be a total surprise.

Next time we are going to develop more of the consequences of the means-and-ends conundrum. What use is talking about strategy if we are not willing to act in the world? Can we stand still on a moving train?

I want to apologize for the terminology I am about to use. I believe that this information, on the militaristic approach to problem solving, is important information for those who are seen as the problems. In understanding this approach I have used militaristic language that converts humans and groups into units, squads, and platoons. This language is par for the course given their intentions but it is important to draw a clear distinction between their mentality, our education, and how we would want to apply this knowledge. I strongly discourage using their terms and methodology in contexts that we choose to involve ourselves with. Naming is power.

At the Our Lives Ahead conference this summer I attended an interesting workshop on Crowd Control & Street Tactics led by an ex-National Guardsman who had been through several sessions of (anti) protest training. This person led the room full of people through exercises that included marching, baton handling, formations, and an introduction to crowd control. It was interesting to see the reactions of the attendees to this education, to guess at what trainees experience during similar situations in the Guard, and see how the attendees transformed through the process of the techniques taught in the workshop. Additionally, the strategic implications of this workshop demand reflection.

What was not surprising, given the context of 1) video game culture, 2) the attendance of several boys around the age of 16–20 and 3) the presence of sections of PVC pipe serving as metaphorical batons, was the level of horseplay, phallic and weaponized. More surprising was exactly how quickly the group of baton-wielding humans turned into a scary, seemingly trained, group of crowd-controlling automatons. As an observer I could feel the terror of how easy it would be for these people to hurt me, how it would be possible for them to do it without compassion (as I was not part of the group), and how attractive being on the other side of this line was for the participants.

These were not bad people. They were goofy, young, activist types who wanted to understand the specifics of how they are terrorized by armored thugs when they go to protest events. However, the logic of formations and batons was far more powerful than the intentions of the people who participated in that training. The feeling of pushing other people around, and having group approval to do it, to have the stick instead of merely being right, was the lesson.

I have no doubt that the majority of the people who went through this training and experienced this will not become cops or automatons but will remember the power of simple techniques in controlling people. A lot of time and energy is spent by social scientists and military functionalists to come up with these techniques and they use several metrics to determine success. Are the techniques actually usable to achieve their primary goal? Do they achieve their secondary goals? Are the techniques trainable?

Regarding crowd control there can be several primary goals: containment, dispersal, and immobilization. The training that the NG uses (as demonstrated in the workshop cited above) focuses on formations, baton control, and technology to accomplish their primary goal. They are less interested in immobilization (and capture) than a police force would be. Secondary goals include maintaining unit morale, demonstration of force, and mobility. The implication of the question of morale is of much more concern with the NG than with the police because of their voluntary rather than professional status and the limitations of their training. Finally, and related to the first two issues, is the idea that while certain techniques may be more effective at tactical containment and dispersal, their training and implementation require professionalism that doesn’t exist in the National Guard.

The bulk of the training in the workshop reflected what would be necessary in an NG unit. The formation training was simple but distilled the basic formation types in a brief period of time. The line formation is the classic crowd control formation with the unit, a squad of eight, facing the crowd in a single file line with squad leaders in a receded line. Three squads (in the context of NG) comprise a platoon.

The column formation is used to move a unit from one point to another. Other formations (the wedge or square) were also taught but their use, in a modern context, is related more to team building and hierarchal self-identification than to practical pursuits. The baton training was similarly simple and boiled down to two actions, using the baton to push a crowd and using the baton to hold and shape the line formation. Since the goal of the presentation, and the training generally, is to hold the line rather than to beat a crowd (we will get into the reasons for this in a minute) baton use is a strategic rather than a tactical consideration. It is more important that everyone is doing the same thing rather than anyone being particularly proficient — never mind skilled — in baton use.

This training is markedly different from police training in a number of regards. Police have a primary goal of immobilization leading to prosecution, which means that even in the context of the traditional line formation they have additional operational forces than the corresponding NG unit (although it is possible to imagine NG units using these special units in many situations). The linebackers (what we usually refer to as the snatch squad) have mobility behind and in front of the line and use cues to target and immobilize members (usually perceived leaders) of a crowd. They usually do not dress in uniform but are known to wear either some sort of marker (including visible badges, armbands, hats, etc) or use hand signals to pass through control lines. Many crowd control situations include several kinds of grenadiers using a variety of types of projectile weapons against crowds. These include, but are not limited to, tear gas canisters, projectile rubber batons, rubber bullets, beanbags, wooden dowels, tear gas projectiles (fired from paint ball guns with the same form factor), and water cannons.

An article from the FBI informs about police innovation in crowd control over the past decade. “Riot Response: An Innovative Approach” (1997) distills the lessons of the LA riots into a few simple lessons. This is a lesson you will not see applied in an NG context but only in one where the units are highly trained. Whereas the traditional line formation (the Skirmish Line) has made sense in a variety of contexts, it de-emphasizes mobility and flexibility in the interest of containment and dispersal. The FBI proposes the Augmented Skirmish Line, where squad-level units can act semi-autonomously (they are still directed by a platoon leader), thereby allowing for a greater degree of granularity in achieving primary goals. The second proposal is the creation of TANGO (Tactically Aggressive and Necessary Gambit of Options) squads. The TANGO squad is essentially a high tech snatch squad that waits behind police lines until deployed against so-called aggressive targets. “The Tango Team can bring to bear the entire spectrum of use-of-force options from command presence through deadly force — in a controlled, self-contained package.”

Developing an understanding of the mentality and tactics of state-sponsored groups stands on its own as a worthwhile activity for anti-statists. The state’s reliance on simple objectives and techniques to accomplish complicated tasks is a testament to the amount of human, intellectual effort that is put into these problems. The abandonment by the planners and participants in these activities of their own individuality and critical thought is but one horrible consequence. Another is the complex and scientific examination of what works to disturb, terrify, and isolate individuals, done by the planning class and implemented by the participant class. These processes of social abandonment and social quantification are two mechanisms that anarchists can avoid in their own practice and in their understanding of how to engage with each other. Recognizing these traits in the state’s behavior can allow some forewarning of the specifics of their intentions. Developing ideas on how to foil these processes should continue to inspire our activity.

Board games are immensely popular in Germany where some recent games are a genre unto themselves. German-style board games combine thoughtful play, some strategic elements, and enough randomness to make games competitive for different levels of players. They usually do not include elements like player elimination or complicated calculations. They tend toward themes rather than abstraction (think Risk rather than Chess). While some of these games have become popular in North America, the difference between the German family sitting at the dinner table playing a board game and an American family whose only time together is spent watching television speaks volumes about the difference between the two societies.

An interesting characteristic of German Games (GG) is the exploitation of different kinds of game processes, which makes a game more playable for more types of players — if not more satisfying. Competitive players, new players, and casual gamers can all enjoy German-style board games.

Recently I was at the home of a co-worker, where we played a highly modified version of Settlers of Cataan, the most popular GG in North America. These house rules softened most of the hard elements of the game (namely the elements that are competitive and aggressive) to make the game more pleasant for some of the players. The result is that the number of ways to win the game were drastically reduced, more time was spent setting up the pieces (the technics of the game) than actually playing it, and the lifespan of the game (the amount of time it would take to grow bored of it) was greatly reduced. Our hosts did not realize that it is the complexity-through-simplicity of Settlers of Cataan that makes it appealing, rather than the hard or soft elements of the game. These house rules apparently worked for my hosts but made the game, on the whole, less engaging to me and the other non-house players.

On the flipside of this kind of play, a small circle of us around town have taken to playing Settlers of Cataan with some regularity. Our games are, to put it gently, rough. Rough enough that people who are turned off by competitive environments steer clear of our games altogether and only a few types of personalities stick with the playing. The play itself is an odd combination of psychological conflict, harsh laughter, and the different personalities. Along with the boorish alpha males (among whom I count myself) are the pre-postal uptight white people, and the people who play to lose. The gaming becomes a microcosm of the political universe most of us have a desire to escape.

Those who refuse to play write off the whole practice as more-of-the-same and while they are right, their analysis raises a question. How do we break patterns, socialize, or engage in any project, if we don’t do it with the full knowledge of who we are working with? How do they deal with pressure? How do they win and how do they lose?

What if, instead of judging the merits of an activity (like a board game) on its political palatability or how anarchist it is, we evaluate games on criteria like rule implementation, effective symbolism, and relation to life outside the game? Rather than focus on the correctness of a perspective or how it will play to the Lowest Common Denominator, we could focus on systemic flexibility. For example, if our goal is to have a pleasant evening, we first have to provide for chatty and competitive people; second, provide enough structure to give our evening a beginning, middle and an end; third, allow enough fluidity for everyone to feel included in each part of the evening and the game.

Warfare has heretofore entailed the strategic placement of material and actors. Resolution invariably reflected the amount of material placed, positional superiority, and/or the kind of violence inflicted by the actors. The major conflicts of the twentieth century were of this type; one brutal violent machine pressing against another — grinding people in between.

The twenty-first century (especially if you start the 21st century immediately at the fall of the Berlin Wall) holds the possibility of transforming (a)social violence beyond all recognition. There will still be terror raining from the sky (at least until the fuel runs out) but the likelihood of another conflict where rival factions place nearly identical military units on a battlefield to slug it out for a hill or a city seems as quaint as lining up redcoats in the city square. It could still happen, but the past 20 years doesn’t lead one to believe it will.

Birthrates, new holy wars (the Fedayeen vs. the Neo-Cons), food riots, fuel riots, and suicide bombing comprise modern elements to conflicts in this epoch. They aren’t icons that can be placed on a battlefield by disinterested generals. These are not vectors with one dimension. They are markers to a conflict in a multi-dimensional universe. They represent forces that combine ideology and power in such measure as to defy (post)modernist categorization.

* * *

Anarchism has become both more like a game and more like non-traditional warfare since the twentieth century came to a close. On the one hand the goals of anarchism have become as varied as life in society. No longer are anarchists chained to the role of leftist partisans, givers-of-charity, martyrs, or villains. Anarchists can be book sellers, academics, carpenters, and a thousand other things. Anarchism, as a goal and a practice, is something that brings joy to the practitioners or is a habit to be shunned. Anarchists are either people we enjoy playing with or they should return to the gray.

On the other hand, the methods by which Capitalism and the State will be defeated (in North America at least) will not look like organizing the workplace, selling newspapers, or chanting the name of our fearless leader. It will probably not look like black masks and broken windows either, but it is likely there will be both. It is likely that if a near-total transformation is to happen, it will be by NTW (non-traditional warfare). It will be because of rioting, IEDs (improvised explosive devices), and un-mappable violence in the belly of the beast. It is likely to look like attack-by-all-means. It will look like raising children without aspirations toward the colonists, and without hope. It will, if it is to occur, look like the last gasps of a cultural regime that has run its course.

Our play today speaks, without comprehension, to this future. Knowing the futility of running into planted pikes, we resign ourselves to this play. But pikes are nothing but metal-capped staves.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



We are not for more freedom. More freedom is given to the slave when his chains are lengthened. We are for the abolition of the chain, so we are for freedom, not more freedom. Freedom means the absence of all chains, the absence of limits and all that ensues from such a statement. (Bonanno The Anarchist Tension)



I want to apologize for the terminology I am about to use. I believe that this information, on the militaristic approach to problem solving, is important information for those who are seen as the problems. In understanding this approach I have used militaristic language that converts humans and groups into units, squads, and platoons. This language is par for the course given their intentions but it is important to draw a clear distinction between their mentality, our education, and how we would want to apply this knowledge. I strongly discourage using their terms and methodology in contexts that we choose to involve ourselves with. Naming is power.

