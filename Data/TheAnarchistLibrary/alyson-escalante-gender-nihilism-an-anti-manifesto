      Introduction      Antihumanism      Gender Abolition      Radical Negativity
We are at an impasse. The current politics of trans liberation have staked their claims on a redemptive understanding of identity. Whether through a doctor or psychologist’s diagnosis, or through a personal self affirmation in the form of a social utterance, we have come to believe that there is some internal truth to gender that we must divine.

An endless set of positive political projects have marked the road we currently travel; an infinite set of pronouns, pride flags, and labels. The current movement within trans politics has sought to try to broaden gender categories, in the hope that we can alleviate their harm. This is naive.

Judith Butler refers to gender as, “the apparatus by which the production and normalization of masculine and feminine take place along with the interstitial forms of hormonal, chromosomal, psychic, and performative that gender assumes.” If the current liberal politics of our trans comrades and siblings are rooted in trying to expand the social dimensions created by this apparatus, our work is a demand to see it burned to the ground.

We are radicals who have had enough with attempts to salvage gender. We do not believe we can make it work for us. We look at the transmisogyny we have faced in our own lives, the gendered violence that our comrades, both trans and cis have faced, and we realize that the apparatus itself makes such violence inevitable. We have had enough.

We are not looking to create a better system, for we are not interested in positive politics at all. All we demand in the present is a relentless attack on gender and the modes of social meaning and intelligibility it creates.

At the core of this Gender Nihilism lies several principles that will be explored in detail here: Antihumanism as foundation and cornerstone, gender abolition as a demand, and radical negativity as method.

Antihumanism is a cornerstone which holds gender nihilist analysis together. It is the point from which we begin to understand our present situation; it is crucial. By antihumanism, we mean a rejection of essentialism. There is no essential human. There is no human nature. There is no transcendent self. To be a subject is not to share in common a metaphysical state of being (ontology) with other subjects.

The self, the subject is a product of power. The “I” in “I am a man” or “I am a woman” is not an “I” which transcends those statements. Those statements do not reveal a truth about the “I,” rather they constitute the “I.” Man and Woman do not exist as labels for certain metaphysical or essential categories of being, they are rather discursive, social, and linguistic symbols which are historically contingent. They evolve and change over time; their implications have always been determined by power.

Who we are, the very core of our being, might perhaps not be found in the categorical realm of being at all. The self is a convergence of power and discourses. Every word you use to define yourself, every category of identity within which you find yourself place, is the result of a historical development of power. Gender, race, sexuality, and every other normative category is not referencing a truth about the body of the subject or about the soul of the subject. These categories construct the subject and the self. There is no static self, no consistent “I”, no history transcending subject. We can only refer to a self with the language given to us, and that language has radically fluctuated throughout history, and continues to fluctuate in our day to day life.

We are nothing but the convergence of many different discourses and languages which are utterly beyond our control, yet we experience the sensation of agency. We navigate these discourses, occasionally subverting, always surviving. The ability to navigate does not indicate a metaphysical self which acts upon a sense of agency, it only indicates that there is symbolic and discursive looseness surrounding our constitution.

We thus understand gender through these terms. We see gender as a specific set of discourses embodied in medicine, psychiatry, the social sciences, religion, and our daily interactions with others. We do not see gender as a feature of our “true selves,” but as a whole order of meaning and intelligibility which we find ourselves operating in. We do not look at gender as a thing which a stable self can be said to possess. On the contrary we say that gender is done and participated in, and that this doing is a creative act by which the self is constructed and given social significance and meaning.

Our radicalism cannot stop here, we further state that historical evidence can be provided to show that gender operates in such a manner. The work of many decolonial feminists has been influential in demonstrating the ways that western gender categories were violently forced onto indigenous societies, and how this required a complete linguistic and discursive shift. Colonialism produced new gender categories, and with

them new violent means of reinforcing a certain set of gendered norms. The visual and cultural aspects of masculinity and femininity have changed over the centuries. There is no static gender.

There is a practical component to all of this. The question of humanism vs antihumanism is the question upon which the debate between liberal feminism and nihilist gender abolitionism will be based.

The liberal feminist says “I am a woman” and by that means that they are spiritually, ontologically, metaphysically, genetically, or any other modes of “essentially” a woman.

The gender nihilist says “I am a woman” and means that they are located within a certain position in a matrix of power which constitutes them as such.

The liberal feminist is not aware of the ways power creates gender, and thus clings to gender as a means of legitimizing themselves in the eyes of power. They rely on trying to use various systems of knowledge (genetic sciences, metaphysical claims about the soul, kantian ontology) in order to prove to power they can operate within it.

The gender nihilist, the gender abolitionist, looks at the system of gender itself and see’s the violence at its core. We say no to a positive embrace of gender. We want to see it gone. We know appealing to the current formulations of power is always a liberal trap. We refuse to legitimize ourselves.

It is imperative that this be understood. Antihumanism does not deny the lived experience of many of our trans siblings who have had an experience of gender since a young age. Rather we acknowledge that such an experience of gender was always already determined through the terms of power. We look to our own childhood experiences. We see that even in the transgressive statement of “We are women” wherein we deny the category power has imposed onto our bodies, we speak the language of gender. We reference an idea of “woman” which does not exist within us as a stable truth, but references the discourses by which we are constituted.

Thus we affirm that there is no true self that can be divined prior to discourse, prior to encounters with others, prior to the mediation of the symbolic. We are products of power, so what are we to do? So we end our exploration of antihumanism with a return to the words of Butler:

<quote>“My agency does not consist in denying this condition of my constitution. If I have any agency, it is opened up by the fact that I am constituted by a social world I never chose. That my agency is riven with paradox does not mean it is impossible. It means only that paradox is the condition of its possibility.”</quote>

If we accept that gender is not to be found within ourselves as a transcendent truth, but rather exists outside us in the realm of discourse, what are we to strive for? To say gender is discursive is to say that gender occurs not as a metaphysical truth within the subject, but occurs as a means of mediating social interaction. Gender is a frame, a subset of language, and set of symbols and signs, communicated between us, constructing us and being reconstructed by us constantly.

Thus the apparatus of gender operates cyclically; as we are constituted through it, so too do our daily actions, rituals, norms, and performances reconstitute it. It is this realization which allows for a movement against the cycle itself to manifest. Such a movement must understand the deeply penetrative and pervasive nature of the apparatus. Normalization has an insidious way of naturalizing, accounting for, and subsuming resistance.

At this point it becomes tempting to embrace a certain liberal politics of expansion. Countless theorists and activists have laid stake to the claim that our experience of transgender embodiment might be able to pose a threat to the process of normalization that is gender. We have heard the suggestion that non-binary identity, trans identity, and queer identity might be able to create a subversion of gender. This cannot be the case.

In staking our claim on identity labels of non-binary, we find ourselves always again caught back in the realm of gender. To take on identity in a rejection of the gender binary is still to accept the binary as a point of reference. In the resistance to it, one only reconstructs the normative status of the binary. Norms have already accounted for dissent; they lay the frameworks and languages through which dissent can be expressed. It is not merely that our verbal dissent occurs in the language of gender, but that the actions we take to subvert gender in dress and affect are themselves only subversive through their reference to the norm.

If an identity politics of non-binary identity cannot liberate us, is is also true that a queer or trans identity politics offers us no hope. Both fall into the same trap of referencing the norm by trying to “do” gender differently. The very basis of such politics is grounded in the logic of identity, which is itself a product of modern and contemporary discourses of power. As we have already shown quite thoroughly, there can be no stable identity which we can reference. Thus any appeal to a revolutionary or emancipatory identity is only an appeal to certain discourses. In this case, that discourse is gender.

This is not to say that those who identify as trans, queer, or non-binary are at fault for gender. This is the mistake of the traditional radical feminist approach. We repudiate such claims, as they merely attack those most hurt by gender. Even if deviation from the norm is always accounted for and neutralized, it sure as hell is still punished. The queer, the trans, the non-binary body is still the site of massive violence. Our siblings and comrades still are murdered all around us, still live in poverty, still live in the shadows. We do not denounce them, for that would be to denounce ourselves. Instead we call for an honest discussion about the limits of our politics and a demand for a new way forward.

With this attitude at the forefront, it is not merely certain formulations of identity politics which we seek to combat, but the need for identity altogether. Our claim is that the ever expanding list of personal preferred pronouns, the growing and ever more nuanced labels for various expressions of sexuality and gender, and the attempt to construct new identity categories more broadly is not worth the effort.

If we have shown that identity is not a truth but a social and discursive construction, we can then realize that the creation of these new identities is not the sudden discovery of previously unknown lived experience, but rather the creation of new terms upon which we can be constituted. All we do when we expand gender categories is to create new more nuanced channels through which power can operate. We do not liberate ourselves, we ensnare ourselves in countless and even more nuanced and powerful norms. Each one a new chain.

To use this terminology is not hyperbolic; the violence of gender cannot be overestimated. Each trans woman murdered, each intersex infant coercively operated on, each queer kid thrown onto the streets is a victim of gender. The deviance from the norm is always punished. Even though gender has accounted for deviation, it still punishes it. Expansions of norms is an expansion of deviance; it is an expansion of ways we can fall outside a discursive ideal. Infinite gender identities create infinite new spaces of deviation which will be violently punished. Gender must punish deviance, thus gender must go.

And thus we arrive at the need for the abolition of gender. If all of our attempts at positive projects of expansion have fallen short and only snared us in a new set of traps, then there must be another approach. That the expansion of gender has failed, does not imply that contraction would serve our purposes. Such an impulse is purely reactionary and must be done away with.

The reactionary radical feminist sees gender abolition as such a contraction. For them, we must abolish gender so that sex (the physical characteristics of the body) can be a stable material basis upon which we can be grouped. We reject this whole heartedly. Sex itself is grounded in discursive groupings, given an authority through medicine, and violently imposed onto the bodies of intersex individuals. We decry this violence.

No, a return to a simpler and smaller understanding of gender (even if supposedly material conception) will not do. It is the very normative grouping of bodies in the first place which we push back against. Neither contraction nor expansion will save us. Our only path is that of destruction.

At the heart of our gender abolition is a negativity. We seek not to abolish gender so that a true self can be returned to; there is no such self. It is not as though the abolition of gender will free us to exist as true or genuine selves, freed from certain norms. Such a conclusion would be at odds with the entirety of our antihumanist claims. And thus we must take a leap into the void.

A moment of lucid clarity is required here. If what we are is a product of discourses of power, and we seek to abolish and destroy those discourses, we are taking the greatest risk possible. We are diving into an unknown. The very terms, symbols, ideas, and realities by which we have been shaped and created will burn in flames, and we cannot know or predict what we will be when we come out the other side.

This is why we must embrace an attitude of radical negativity. All the previous attempts at positive and expansionist gender politics have failed us. We must cease to presume a knowledge of what liberation or emancipation might look like, for those ideas are themselves grounded upon an idea of the self which cannot stand up to scrutiny; it is an idea which for the longest time has been used to limit our horizons. Only pure rejection, the move away from any sort of knowable or intelligible future can allow us the possibility for a future at all.

While this risk is a powerful one, it is necessary. Yet in plunging into the unknown, we enter the waters of unintelligibility. These waters are not without their dangers; and there is a real possibility for a radical loss self. The very terms by which we recognize each other may be dissolved. But there is no other way out of this dilemma. We are daily being attacked by a process of normalization that codes us as deviant. If we do not lose ourselves in the movement of negativity, we will be destroyed by the status quo. We have only one option, risks be damned.

This powerfully captures the predicament that we are in at this moment. While the risk of embracing negativity is high, we know the alternative will destroy us. If we lose ourselves in the process, we have merely suffered the same fate we would have otherwise. Thus it is with reckless abandon that we refuse to postulate about what a future might hold, and what we might be within that future. A rejection of meaning, a rejection of known possibility, a rejection of being itself. Nihilism. That is our stance and method.

Relentless critique of positive gender politics is thus a starting point, but one which must occur cautiously. For if we are to criticize their own normative underpinnings in favor of an alternative, we only fall prey once again to the neutralizing power of normalization. Thus we answer the demand for a clearly stated alternative and for a program of actions to be taken with a resolute “no.” The days of manifestos and platforms are over. The negation of all things, ourselves included, is the only means through which we will ever be able to gain anything




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

