
Will the present-day peace movement repeat the errors of the 1960s anti-war movement by placing its primary focus on carefully orchestrated and highly centralized national actions in cities like Washington or New York? Or will it try to percolate into the localities and neighborhoods of the country -- into the body politic itself -- and become a genuinely popular movement that reaches deeply into America as a force for education as well as action, for a broad view of the causes of war as well as the dangers of war, for a vision of a harmonized society as well as a demilitarized society?

These questions, I think, are crucial and our response to them may well determine the quality of the movement as well as the "quantity" of people it can influence and "mobilize."

The Vermont town meeting process of initiating the freeze was as important as the result it achieved. People meeting in a face to face democratic arena were using a richly libertarian way of empowering themselves...

Great demonstrations and rallies in the urban centers of the United States make for splendid theater. Expressions of our fears and the problems that concern us have the attraction of simplicity, of clear-cut visceral responses, of sudden if episodic "successes" and "quick results." This, presumably, is the "American Way," like fast food and searing stimuli. We can then go home and view ourselves in huge numbers on television while movement celebrities receive wide media exposure to our utter delight as spectators. But there is a grave danger that such well-orchestrated spectacles like iridescent bubbles will burst in our eyes as soon as a limited issue is exhausted. Initiated by movement celebrities, it is quickly taken over by establishment celebrities -- and we are likely to see the nuclear freeze issue, for example, defuzed by the current Congress's cooptation of the entire demand, just as mere opposition to the Viet-Nam war was easily taken over by the very establishment figures who so readily approved of the war in the opening years of the Johnson Administration. Although the war has ended, after a fashion, southeast Asia is still an area of terrifying afflictions -- and missiles or the neutron bomb is the next "single issue" that hangs over us, not to speak of space-war technologies and potential horrors we could never have foreseen two decades ago.

I think it is crucial that the peace movement of the eighties view itself as more than a "campaign" and its supporters as more than a "constituency" devoted merely to opposing a problem such as the arms buildup. Nor should it focus merely on mere "mobilizations" and episodic theatrical actions. For one thing, the peace movement of the eighties must root itself deeply in the communities of America rather than a few offices in Washington and New York. The Vermont town-meeting process of initiating the freeze was as important as the result it achieved. People, meeting in a face-to-face democratic arena were using a richly libertarian way of empowering themselves, not merely trying to disempower the hawks and warmakers in the United States. Process, in effect, became part of program. Today, when authoritarians in the Pentagon, White House, Capitol, and the state houses of America are trying to strengthen executive authority with proposals for six-year presidencies and, in Vermont, with four-year governorships, the opposition to war, colonialism, and armaments programs is organically tied to the attempt to preserve our democratic institutions and practices.

Secondly, I think we must recognize that the peace movement is intimately linked with the environmental movement, feminist movement, ethnic minority movements, and the stirrings by the aged, poor, and unemployed who are the most direct victims of the "defense" budget and the vast reductions in expenditures for social budgets. Working patiently and unrelentingly on a grass roots, decentralized, local basis, we must reveal all the connections between these movements and the insane commitment of wealth to military ends, the authoritarian controls that threaten to destroy our very means for preventing this commitment, and the gross undermining of our environment that may destroy us as surely as war itself.

If we retain this broad vision of our goals and give it coherence, we will find many allies out there -- allies who are more meaningful, more lasting, and ultimately more effective than the celebrities from all quarters who are quite ready to turn the fundamental problem of a harmonized and free society into a mere spotlight for their own interests and careers.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

