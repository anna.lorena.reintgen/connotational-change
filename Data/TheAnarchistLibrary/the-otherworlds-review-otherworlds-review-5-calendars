
January 2018 · Sun in Capricorn · Full moon in Cancer · Saturn in Capricorn

What characterizes revolutionary classes at their moment of action is the awareness that they are about to make the continuum of history explode. The Great Revolution introduced a new calendar. The initial day of a calendar presents history in time-lapse mode. And basically it is this same day that keeps recurring in the guise of holidays, which are days of remembrance. Thus, calendars do not measure time the way clocks do; they are monuments of a historical consciousness of which not the slightest trace has been apparent in Europe, it would seem, for the past hundred years. In the July Revolution an incident occurred in which this consciousness came into its own.On the first evening of fighting, it so happened that the dials on clocktowers were being fired at simultaneously and independently from several locations in Paris. An eyewitness, who may have owed his insight to the rhyme, wrote as follows:

Who would’ve thought! As though

Angered by time’s way

The new Joshuas

Beneath each tower, they say

Fired at the dials

To stop the day.

– Walter Benjamin

The year 2018 begins with a full moon on New Year’s Day, with an overlap of two cosmic cycles: the lunar cycle at its point of greatest fulfillment, the solar cycle at its very beginning. In orthodox Maitreyan Buddhism, the Maitreya comes at the apex of a golden age far off in the future. But certain heretics in China turned this idea on its head and declared that the Maitreya would come when things were at their worst (ie right now) and unleashed a storm of insurrectionary violence against the State and the clergy. If the revolution is the messiah, we are faced with the same question of when it will come: at a high point, a low point … or both, when two different cycles coincide.

Saturn, the astrological ruler of time and inescapable limitations and reality checks, has just entered its home sign of Capricorn, a sign associated with ruthlessness and material success. Capitalism is always in crisis, but thus far it has turned every crisis into an opportunity for further expansion and accumulation of wealth. Even as apocalyptic civil wars and climate disasters ravage the face of the earth, the capitalist class seeks to escape earthly and human limitations by turning dystopian science fiction into reality. In Canada, 42% of the workforce is at risk of losing their jobs to automation in the next twenty years. Techie scum have founded a church for the worship and proselytization of AI. Private corporations plot to subject the moon and Mars to the same sacrilegious mining that has desecrated and poisoned so much of the earth.

The future is terrifying, and the prospects for widespread liberatory social change are bleak. But every empire must eventually collapse, every hubristic fool who tries to forcibly climb up to the stars while still living must fall, men who seek immortality through machines are doomed to die. As the prophet Fredy Perlman wrote:

In ancient Anatolia people danced on the earth-covered ruins of the Hittite Leviathan and built their lodges with stones which contained the records of the vanished empire’s great deeds. The cycle has come round again. America is where Anatolia was. It is a place where human beings, just to stay alive, have to jump, to dance, and by dancing revive the rhythms, ​recover cyclical time.

The technological transhumanists and space-​colonizers are enemies of the earth and the heavens alike, and despite their pretensions, they are human, all-too-human. But we are heirs to the true transhumanism, to the ancient traditions of animism and initiation, to spiritual technologies tens of thousands of years older than the aberration of so-called civilization. Animism connects us to all which is truly more-than-human, to the true worldwide web, to the spider-woven tapestry of relationships between the life force embodied in each and every animal, plant, rock, river, wind, forest, landscape, sea, star, and being in the world. Initiation promises us true immortality among the stars of the night sky below the daytime earth, among the stars whence we originally came, among the constellations of our beloved ancestors and blessed heroes.

Two irreconcilable worlds – one full of spirit and life, one lifeless but undead – are locked in battle for the heavens, for the earth, for the hearts and souls of the living and the dead alike. One world experiences and knows the ouroboros of eternal recurrence, the other believes in the impossibility of endless progress. As 2018 begins, it is not only the cycles of the moon and sun which collide, but the two worlds, the worlds of the calendar and the clock. Well into the Christian era, the Kalends of January was a day when both pagans and baptized Christians “deliberately transformed themselves into the state of wild beasts” by donning animal hides and men dressed as women, omens were observed, and feasts were laid out to bring prosperity for the coming year.

For this new year, then, a transformation, an omen, a feast. May Saturn’s sickle cut the throat of Leviathan for good, imposing a final limitation on the cancer before it metastasizes to other planets. May a new Luddism arise, a new Boxer Rebellion, to break the machines of the capitalist class and herald a new sacral sovereignty: “down with all kings but King Ludd,” wrote Byron. May the Angel of History at long last be granted a reprieve from the storm of progress and be allowed to “awaken the dead and to piece together what has been smashed.”

Merry crisis and a happy new fear; to 2018, an other planetary rotation within rotation. To 2018, the smoldering seed in the deep freeze, the rising tides, the accelerating apparatuses. To 2018, when visions immaterial congeal materially again. To seeing pieces of dreams in everyday life, to bridging the gap between ours and other worlds.

• • •

The new year renews dedication to the lines of attack and resistance that don’t abide time, but crawl through the centuries as long as it takes to break the spell, burn the cage, free the prisoners.

Between the fourteenth and the nineteenth centuries, the spatial horizon of Europe expanded considerably. The Atlantic gradually became the epicenter of a new concatenation of worlds, the locus of a new planetary consciousness. The ships into the Atlantic followed European attempts at expansion in the Canaries, Madeira, the Azores, and the islands of Cape Verde and culminated in the establishment of a plantation economy dependent on African slave labor.

To 2018, the end of the american plantation.

To the flowing nature of time as water, renewed blessings and respect in 2018 to the water, boiling, freezing, and flooding. The concatenation continues by the tens of millions trapped in servitude the world over: in the human-drug industrial complex of the Bakken oil fields of sacred North Dakota, in the hell of gender, the terror of white supremacy, the violence of colonialism, trafficked by the worshipers of greed, ego, power. The planetary consciousness continues, the strong hearts of prisoners across time weave and breathe life and death and we welcome them.

Other worlds overlay ours and we hail the presence of those who arrive by our side to bolster our bravery, to guide our sight. To 2018, to strengthening our bonds to ancestors of insurrectionary action. “While some citadels have collapsed, other walls have been strengthened. As has long been the case, the contemporary world is deeply shaped and conditioned by the ancestral forms of religious, legal, and political life built around fences, enclosures, walls, camps, circles, and, above all, borders.” To 2018, the collapse of the nation, cataclysms of empire, the destruction of what binds us to unfreedom at all levels, an end to enclosures.

Think of this: When they present you with a watch they are gifting you with a tiny flowering hell, a wreath of roses, a dungeon of air.

To time on our own terms, to healed memories, mended lapses, trauma that comes to a close. To openings, slow and old growth alike. Webs of our own spinning, encrypted, intimate, to the security of true commitment, depth for the roots, purchase without capital, multiplicity without commodification. Seeing that “thinking is not necessarily circumscribed by language, the symbolic, or the human,” to representation left to ruin in 2018. To the growing affinity of ranters, dreamers, and augurs, undoing the material and spiritual existent.

that the Army not come

Xuaka’ Utz’utz’ Ni’



Hear us, sacred lightning,

hear us, holy hill,

hear us, sacred thunder,

hear us, sacred cave:

We come to awaken your conscience.

We come to awaken your heart,

that you may shoot your rifle,

that you may fire your cannon,

that you may close the road to those men.

Though they come at night.

Though they come at dawn.

Though they come bearing arms.

May they not come to beat us.

May they not come to torture us.

May they not come to rape us

in our houses, in our homes.

Father of Huitepec hill, mother of Huitepec hill,

Father of the white cave, mother of the white cave,

Father of San Cristóbal hill, mother of San Cristóbal hill:

May they not enter your lands, great lord.

May their rifles cool, may their pistols cool.

Kajval, accept this bouquet of flowers.

Accept this offering of leaves, accept this offering of smoke,

Sacred father of Chaklajún, sacred mother of Chaklajún.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



What characterizes revolutionary classes at their moment of action is the awareness that they are about to make the continuum of history explode. The Great Revolution introduced a new calendar. The initial day of a calendar presents history in time-lapse mode. And basically it is this same day that keeps recurring in the guise of holidays, which are days of remembrance. Thus, calendars do not measure time the way clocks do; they are monuments of a historical consciousness of which not the slightest trace has been apparent in Europe, it would seem, for the past hundred years. In the July Revolution an incident occurred in which this consciousness came into its own.On the first evening of fighting, it so happened that the dials on clocktowers were being fired at simultaneously and independently from several locations in Paris. An eyewitness, who may have owed his insight to the rhyme, wrote as follows:


Who would’ve thought! As though


Angered by time’s way


The new Joshuas


Beneath each tower, they say


Fired at the dials


To stop the day.


– Walter Benjamin



In ancient Anatolia people danced on the earth-covered ruins of the Hittite Leviathan and built their lodges with stones which contained the records of the vanished empire’s great deeds. The cycle has come round again. America is where Anatolia was. It is a place where human beings, just to stay alive, have to jump, to dance, and by dancing revive the rhythms, ​recover cyclical time.



Between the fourteenth and the nineteenth centuries, the spatial horizon of Europe expanded considerably. The Atlantic gradually became the epicenter of a new concatenation of worlds, the locus of a new planetary consciousness. The ships into the Atlantic followed European attempts at expansion in the Canaries, Madeira, the Azores, and the islands of Cape Verde and culminated in the establishment of a plantation economy dependent on African slave labor.



Think of this: When they present you with a watch they are gifting you with a tiny flowering hell, a wreath of roses, a dungeon of air.



that the Army not come


Xuaka’ Utz’utz’ Ni’





Hear us, sacred lightning,


hear us, holy hill,


hear us, sacred thunder,


hear us, sacred cave:


We come to awaken your conscience.


We come to awaken your heart,


that you may shoot your rifle,


that you may fire your cannon,


that you may close the road to those men.


Though they come at night.


Though they come at dawn.


Though they come bearing arms.


May they not come to beat us.


May they not come to torture us.


May they not come to rape us


in our houses, in our homes.


Father of Huitepec hill, mother of Huitepec hill,


Father of the white cave, mother of the white cave,


Father of San Cristóbal hill, mother of San Cristóbal hill:


May they not enter your lands, great lord.


May their rifles cool, may their pistols cool.


Kajval, accept this bouquet of flowers.


Accept this offering of leaves, accept this offering of smoke,


Sacred father of Chaklajún, sacred mother of Chaklajún.

