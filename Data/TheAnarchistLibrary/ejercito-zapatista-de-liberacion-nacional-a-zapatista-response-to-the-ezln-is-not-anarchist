      A Zapatista Response to “The EZLN Is NOT Anarchist”      Appendix: The EZLN is not Anarchist: Or Struggles at the Margins and Revolutionary Solidarity
First and foremost, it must be said that only small elements of the Frente Zapatista are willing to engage in a debate with insignificant elements along an ideological fringe. One would find even fewer warriors within the Ejercito Zapatista who would be willing to engage in intangible rhetorical battles with people whose greatest virtue is spreading their lack of understanding and knowledge around in newspapers and magazines. But the article entitled “The EZLN Is NOT Anarchist” reflected such a colonialist attitude of arrogant ignorance, several of us decided to write a response to you.

You are right. The EZLN and its larger populist body the FZLN are NOT Anarchist. Nor do we intend to be, nor should we be. In order for us to make concrete change in our social and political struggles, we cannot limit ourselves by adhering to a singular ideology. Our political and military body encompasses a wide range of belief systems from a wide range of cultures that cannot be defined under a narrow ideological microscope. There are anarchists in our midst, just as there are Catholics and Communists and followers of Santeria. We are Indians in the countryside and workers in the city. We are politicians in office and homeless children on the street. We are gay and straight, male and female, wealthy and poor. What we all have in common is a love for our families and our homelands. What we all have in common is a desire to make things better for ourselves and our country. None of this can be accomplished if we are to build walls of words and abstract ideas around ourselves.

Over the past 500 years, we have been subjected to a brutal system of exploitation and degradation few in North America have ever experienced. We have been denied land and freedom since before your country was even made and accordingly have a much different view on the world than you. We were subjected by colonial rule first by the Spanish, then by the French and Germans and lastly by the North Americans. For centuries Mexicans have been slaves and fodder and treated as less than human; a fact that scars us to this day and a fact we cannot and should not forget. Our past has made us what we are today and in attempting to break this historical trend of exploitation, we have risen up multiple times in attempts to reclaim our humanity and better our lives. First we fought with Juarez and Hidalgo against the Spanish crown, then Zapata and Villa against the Porfiriato. Now we fight against the different faces of the same head seeking to keep us enslaved as subhuman servants to Capital. This is not a struggle that was picked up from a book or gleaned from a movie, but a struggle we all inherited the moment we were given the light of life. This is a struggle that is in front of all our lives, even running through our blood. It is a struggle many of our fathers and grandfathers died for and one we ourselves are willing to die for. A struggle necessary for our people and our country. It is apparent from your condescending language and arrogant shortsightedness that you understand very little about Mexican History or Mexicans in general. We may be “fundamentally reformist” and may be working for “nothing concrete that could not be provided for by capitalism” but rest assured that food, land, democracy, justice and peace are terribly precious when you don’t have them. Precious enough to struggle for at any cost, even at the risk of offending some comfortable people in a far off land who think their belief system is more important than basic human needs. Precious enough to work for with whatever tools we have before us, be it negotiations with the State or networking within popular culture. Our struggle was raging before anarchism was even a word, much less an ideology with newspapers and disciples. Our struggle is older than Bakunin or Kropotkin. Even though anarchists and syndicates have fought bravely with us, we are not willing to lower our history to meet some narrow ideology exported from the same countries we fought against in our Wars for independence. The struggle in Mexico, Zapatista and otherwise, is a product of our histories and our cultures and cannot be bent and manipulated to fit someone else’s formula, much less a formula not at all informed about our people, our country or our histories. You are right, we as a movement are not anarchist. We are people trying to take control of our lives and reclaim a dignity that was stolen from us the moment Cortes came to power.

In fighting for these ends, we must do what is most effective for us, for all of us, without succumbing to the temptation of being divided into small little groups that are more easily purchased by those keeping us enslaved. We learned this lesson from La Malinche as she helped Cortes divide 30 million Mexicans up into an easily conquered group of feuding bodies. We learned this lesson from the post-independence reign of the Porfiriato and from the post-revolutionary betrayal at the hands of the rich powers. We see narrow-minded ideologies like anarchism and communism as tools to pull apart Mexicans into more easily exploitable groups. Rather than facing our enemies as groups that can be turned against each other, we prefer to work together as a common people with a common goal. Your article used the word “compromise” as though it were profanity. For us it is the glue that holds us all together in a common struggle. Without these compromises that allow us to work together, we would be nowhere; lonely slaves waiting to be exploited just as we have been in the past. We will not be bought off this time. We will not allow ourselves to be treated as particulars and accept favors from the powers that harvest wealth from our misfortune. And as we are doing things right now, it is working. 60 million people signed petitions to stop the War in Chiapas. Zapatismo is alive again. We have cells in every town in every state all across the country made up of people from all over the demographic spectrum. We are organized. We are powerful. We will succeed in our fight simply because we are too large and too well organized to be ignored or quashed by the Powers. What we have may not be perfect. It may not be ideal. But it is working for us now in a very much visible fashion. And we wouldn’t hesitate to say that if you were in our position, you would be doing the same things. But what really enraged us in your article was the familiar old face of colonialism shining through your good intentions. Lots of North Americans come to Mexico and turn up their nose at our food and our lifestyles, claiming that we are not as good as things they have “back home.” The author of your article does the same thing in his “critiques” of Zapatismo. If these “critiques” had included a detailed discussion on our tactics with reference to our history and current positions in the world, it wouldn’t have been a big deal, nothing that we don’t do constantly within our own organizations. But the fact that he just slagged Zapatismo off as being a vanguard of reformist nationalists without even a touch of analysis on WHY this is, illustrates that once again we Mexicans are not as good as the all knowing North American Imperialist who thinks himself more aware, more intelligent and more sophisticated politically than the dumb Mexican. This attitude, though hidden behind thin veils of objectivity, is the same attitude that we have been dealing with for 500 years, where someone else in some other country from some other culture thinks they know what is best for us more than we do ourselves. Even more disgusting to us was the line “The question of revolutionary solidarity in these struggles is, therefore, the question of how to intervene in a way that is fitting with one’s aims, in a way that moves one’s revolutionary anarchist project forward.” It would be difficult for us to design a more concise list of colonial words and attitudes than those used in this sentence. “Intervene?” “Moves one’s ‘project’ forward?” Mexicans have a very well developed understanding of what “intervention” entails. Try looking up Conquista and Villahermosa and Tejas and Maximilian in a history book for even a small glimpse of what we see when North Americans start talking about “intervention.” But once again, the anarchists in North America know better than us about how to wage a struggle we have been engaged in since 300 years before their country was founded and can therefore, even think about using us as a means to “advance their project.” That is the same exact attitude Capitalists and Empires have been using to exploit and degrade Mexico and the rest of the third world for the past five hundred years. Even though this article talks a lot about revolution, the attitudes and ideas held by the author are no different than those held by Cortes, Monroe or any other corporate imperialist bastard you can think of. Your intervention is not wanted nor are we a “project” for some high-minded North Americans to profit off.The author talks much about revolutionary solidarity without ever defining the term. What does revolutionary solidarity mean to him? From the attitude of his article it is apparent that revolutionary solidarity is more or less the same thing to him as “profit margins” and “cost/benefit analyses” are to corporate imperialists, ways to use someone else for one’s own gain. So long as North American anarchists hold and espouse colonialist belief systems they will forever find themselves without allies in the third world. The peasants in Bolivia and Ecuador, no matter how closely in conformity with your rigid ideology, will not appreciate your condescending colonial attitudes anymore than would the freedom fighters in Papua New Guinea or anywhere else in the world.

Colonialism is one of the many enemies we are fighting in this world and so long as North Americans reinforce colonial thought patterns in their “revolutionary” struggles, they will never be on the side of any anti-colonial struggle anywhere.We in the Zapatista struggle have never asked anyone for unflinching, uncritical support. What we have asked the world to do is respect the historical context we are in and think about the actions we do to pull ourselves from under the boots of oppression. At the same time, you should be looking at your own struggles in your own country and seeing the commonalties we have between us. This is the only way we have to make a global Revolution.

Willful Disobedience Volume 2, number 7

In a future revolutionary period the most subtle and most dangerous defenders of capitalism will not be the people shouting pro-capitalist and pro-statist slogans, but those who have understood the possible point of total rupture. Far from eulogizing TV commercials and social submission, they will propose to change life...but to that end, call for building a true democratic power first. If they succeed in dominating the situation, the creation of this new political form will use up people’s energy, fritter away radical aspirations and, with the means becoming the end, will once again turn revolution into an ideology.— Gilles Dauve

The current restructuring of capital and its global expansion intrudes to an ever greater extent in to the lives of those on its margins. Peasants and indigenous people in non-Western, so-called “third world” nations, who have maintained some level of control over their subsistence up to now, are finding themselves forced to leave their lands or conform their activities to the needs of the world capitalist market simply to survive. It is, therefore, not surprising that movements of resistance against the various aspects of capitalist intrusion have arisen among these people in many parts of the world.

In previous issues of Willful Disobedience, I have written about the West Papua Freedom Movement (OPM). This movement of the indigenous people West Papua, many of whom continue to live as they did for centuries before any colonial powers arrived, against their Indonesian rulers is quite clear about refusing “modern life” — that is, the state, capital and everything that industrial civilization imposes. Or as they have said in communiqués: “We want to be left alone!” But this is the one thing that capital and the state will never grant. Although the OPM has sent delegates to demand talks with the Indonesian government, the West Papuans are increasingly aware of the futility of such negotiations. Recent communiqués talk increasingly of fighting to the death if necessary. After all, succumbing to the intrusion of capital would mean their spiritual death in any case. Their clarity about what they do not want has probably played an important part in guaranteeing that this movement, though armed, has never developed a separated military body, but rather has fought using methods traditional to their cultures. On the other hand, they have not completely escaped the ideology of nationalism, or at least its use in an attempt to have some credibility before world opinion. Still, this movement stands for having very few illusions about what the civilized social order and its institutions have to offer.

Another struggle at the farthest fringes of capitalist expansion is that of the people of Bougainville, an island about five miles west of the Solomon Islands, which has been under the rule of Papua New Guinea (not to be mistaken for West Papua) since 1975. The people of this island were pushed to revolt when CRA, an Australian subsidiary of Rio Tinto Zinc, installed a copper mine, causing hundreds of locals to lose their homes, lands and fishing rights, as well as destroying much of the jungle. The mine expanded until it was a half kilometer deep and seven kilometers in diameter. Protests, petitions and demands for compensation proved ineffective. So in 1988, a handful of islanders stole explosives from the mining company and began to destroy its structures and machinery. When the Papua New Guinea (PNG) government sent in its armed forces, the Bougainville Revolutionary Army (BRA) was formed to battle the PNG military and their Australian advisers. Armed only with homemade guns, dealing with a total blockade of the island by Australian boats and helicopters and largely ignored by the outside world, the people of Bougainville have nearly achieved autonomy. A peace process began in 1997 and those PNG soldiers still on the island have been confined to their barracks. An independent governing authority has begun to develop — certainly to give credibility in the eyes of the states of the world to an autonomous Bougainville — and this will likely have a negative effect on the reconstructing of the community and the environment, making it easier for Bougainville to be drawn into the world economic order. As was said in Terra Selvaggio: “The history of rebellion is much too full of liberators who transform themselves into jailers and radicals who ‘forget’ their programs of social change once they’ve seized power.” Nonetheless, the small dimensions of the island combined with the absence of any urban centers makes the process of construction of state power difficult. And the determination of the people not to allow the mine to reopen is their best protection against the expansion of capital on the island.

While the indigenous people of West Papua and Bougainville have not really yet been integrated in to the capitalist market at all — giving them certain advantages both in terms of clarity about what they have to lose and in terms of knowledge of the still mostly wild terrain on which they fight — other indigenous people and small-holding peasants who were already involved in the market economy to some extent, but have maintained some real control over their subsistence, are now seeing this last bit of self-determination eaten away and are responding.

In India, groups of peasants have organized to attack genetically engineered crops. Recognizing the genetic engineering of seeds and the and the patenting of genetic structures as methods for finalizing the control of multi-national corporations over food production, even on the subsistence scale, these groups have attacked GMO fields and the property of corporations like Monsanto. But by no means do these groups have a clear critique of capitalism or the state. So alongside these direct attacks, the groups also petition the Indian state to make laws protecting them and preserving their place within the present social order. Their movement in its present form remains a movement for anti-global reform.

In India, groups of peasants have organized to attack genetically engineered crops. Recognizing the genetic engineering of seeds and the and the patenting of genetic structures as methods for finalizing the control of multi-national corporations over food production, even on the subsistence scale, these groups have attacked GMO fields and the property of corporations like Monsanto. But by no means do these groups have a clear critique of capitalism or the state. So alongside these direct attacks, the groups also petition the Indian state to make laws protecting them and preserving their place within the present social order. Their movement in its present form remains a movement for anti-global reform.

In India, groups of peasants have organized to attack genetically engineered crops. Recognizing the genetic engineering of seeds and the and the patenting of genetic structures as methods for finalizing the control of multi-national corporations over food production, even on the subsistence scale, these groups have attacked GMO fields and the property of corporations like Monsanto. But by no means do these groups have a clear critique of capitalism or the state. So alongside these direct attacks, the groups also petition the Indian state to make laws protecting them and preserving their place within the present social order. Their movement in its present form remains a movement for anti-global reform.

In India, groups of peasants have organized to attack genetically engineered crops. Recognizing the genetic engineering of seeds and the and the patenting of genetic structures as methods for finalizing the control of multi-national corporations over food production, even on the subsistence scale, these groups have attacked GMO fields and the property of corporations like Monsanto. But by no means do these groups have a clear critique of capitalism or the state. So alongside these direct attacks, the groups also petition the Indian state to make laws protecting them and preserving their place within the present social order. Their movement in its present form remains a movement for anti-global reform.

Probably the best known of the indigenous struggles is the one happening in Chiapas, Mexico. This struggle came into the light of day with the uprising of January 1, 1994. The strength of the insurrection, the preciseness of its targets and the general situation from which it arose aroused immediate sympathy among leftists, progressives, revolutionaries and anarchists throughout the world. The uprising was led by the Zapatista Army for National Liberation (EZLN). The sympathy for this struggle is understandable as is the desire to act in solidarity with the indigenous people of Chiapas. What is not, from an anarchist perspective, is the mostly uncritical support for the EZLN. The EZLN has not hidden their agenda. Their aims are clear already in the declaration of war that they issued at the time of the 1994 uprising, and not only are those aims not anarchist; they are not even revolutionary. In this declaration, nationalist language reinforced the implications of the army’s name. Stating: “We are the inheritors of the true builders of our nation”, they go on to call upon the constitutional right of the people to “alter or modify their form of government”. They speak repeatedly of the “right to freely and democratically elect political representatives” and “administrative authorities”. And the goals for which they struggle are “work, land, housing , food, health care, education, independence, freedom, democracy, justice and peace”. In other words nothing concrete that could not be provided by capitalism. Nothing in any later statement from this prolific organization has changed this fundamentally reformist program. Instead the EZLN calls for dialogue and negotiation, declaring their willingness to accept signs of good faith from the Mexican government. Thus, they send out calls to the legislature of Mexico, even inviting members of this body to participate in the EZLN march to the capital, the purpose of which is to call on the government to enforce the San Andres peace accords worked out by Cocopa, a legislative committee in 1995. So we see, regardless of the fact that they are armed and masked, the EZLN is a reformist organization. They claim to be in the service of the indigenous people of Chiapas (much as Mao’s army claimed to be in the service of the peasants and workers of China before Mao came to power), but they remain a specialized military organization separate from the people, not the people armed. They have made themselves the public spokespeople for the struggle in Chiapas and have channeled it into reformist demands and appeals to nationalism and democracy. There are reasons why the EZLN has become the darling of the anti-globalization movement: its rhetoric and its aims present no threat to those elements in this movement who merely seek more national and local control of capitalism.

Of course, the social struggles of exploited and oppressed people cannot be expected to conform to some abstract anarchist ideal. These struggles arise in particular situations, sparked by specific events. The question of revolutionary solidarity in these struggles is, therefore, the question of how to intervene in a way that is fitting with one’s aims, in a way that moves one’s revolutionary anarchist project forward. But in order to do this, one must have clear aims and a clear concept of one’s project. In other words, one must be pursuing one’s own daily struggle against the present reality with lucidity and determination. Uncritical support of any of the struggles described above is indicative of a lack of clarity about what an anarchist revolutionary project might be, and such support is most certainly not revolutionary solidarity. Each of our struggles springs from our own lives and our own experiences of domination and exploitation. When we go into these battles with full awareness of the nature of the state and capital, of the institutions by which this civilization controls our existence, it becomes obvious that only certain methods and practices can lead toward the end we desire. With this knowledge, we can clarify our own projects and make our awareness of the struggles around the world into a tool for honing our own struggle against the present social order. Revolutionary solidarity is precisely fighting against the totality of an existence based on exploitation, domination and alienation wherever one finds oneself. In this light, revolutionary solidarity needs to take up the weapon of unflinching, merciless critique of all reformist, nationalist, hierarchical, authoritarian, democratic or class collaborationist tendencies that could undermine the autonomy and self-activity of those in struggle and channel the struggle into negotiation and compromise with the present order. This critique must be based in a lucid conception of the world we must destroy and the means necessary to accomplish this destruction.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



In a future revolutionary period the most subtle and most dangerous defenders of capitalism will not be the people shouting pro-capitalist and pro-statist slogans, but those who have understood the possible point of total rupture. Far from eulogizing TV commercials and social submission, they will propose to change life...but to that end, call for building a true democratic power first. If they succeed in dominating the situation, the creation of this new political form will use up people’s energy, fritter away radical aspirations and, with the means becoming the end, will once again turn revolution into an ideology.— Gilles Dauve

