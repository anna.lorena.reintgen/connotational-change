
The two most common criticisms of the recent mass protests against the institutions of neo-liberalism have been the lack of local focus with summit-hopping and the lack of quantifiable direct action. The two problems are interconnected — involvement with the struggle of a local community is often the very terrain in which direct action becomes possible on a popular scale. However, that does not mean that protests against international trade agreements and neo-colonial debt are useless — quite the contrary — a global struggle often can have a local connection. The struggle by laundry workers in Baltimore is one example of how an anarchists and anti-globalization protesters can make a local difference.

The Union of Needletrades & Industrial Textile Employees (UNITE!) has been hit particularly hard by the North American Free Trade Agreement (NAFTA). It has lost tens of thousands of union jobs as textile factories have relocated as sweatshops. UNITE! has had to at least partially changed it’s organizing focus from clothing manufacture to shops that can’t “run away”. Industrial laundries are largely tied to their local community as they provide linen cleaning to hospitals and hotels. Ironically, the have become more of an industry as hospitals have outsourced their laundry to these firms to avoid paying for union benefits.

The first target in Baltimore for UNITE!’s strategy of organizing laundries was Up-To-Date Laundry, the largest in the city. Up-To-Date is an expanding business specializing in the “challenging-to-clean” linens of Baltimore and DC’s large health care industry. Employing some 250 workers, the projected sales are over $5.5 million. It’s new facility gave it additional capacity to carry the work of other industrial laundries in the area. To organize all of the laundry workers in the area, winning this shop was key.

Working conditions in industrial laundries are harsh with their noise, heat and chemical treatments. Respiratory distress like “lint lung” can develop from the large amounts of dust and lint. The danger is further complicated when dealing with medical waste; workers in the “soil room” where dirty laundry is unloaded and sorted are exposed to blood, fecal matter, vomit, afterbirth, body parts and needles. Even in the best situations, it’s a tough job — but the bosses at Up-To-Date made it hell.

Workers at Up-To-Date were not properly trained about the dangers of working with hospital linens and toxic chemicals. Ironically since they deal primarily with hospitals, the workers lacked health insurance, Hepatitis B immunization, and failed to receive immediate medical attention after being stuck by needles. They even lacked basic safety equipment like gloves. For all the health risks that workers endured while working as long as ten to twelve hours, starting pay was $5.15 an hour; don’t even think about a pension.

The class struggle at Up-To-Date was not only about wages and working conditions; but also about dignity for the mostly Black and Latino workers. Racial discrimination by the bosses was rampant at Up-To-Date starting with the first day on the job, with Black workers earning $5.15 an hour while other workers started at $6 for the same work, as well as denied the “opportunity” to work overtime. Black workers were assigned the worst shifts and tasks, as well as being denied state-legislated breaks.

The bosses weren’t just racist, but sexist as well to largely female work force. Both male and female workers faced unwanted sexual advances, sexually explicit and suggestive remarks, groping and stalking. The organizing drive with Laundry and Dry Cleaning Local 66 from Boston also had to deal with the issue of sexual harassment, just like the Up-To-Date workers. “We had one woman who was being harassed by a supervisor who had a history of sexually harassing all the women, dating many of the women that worked in the shop and threatening them with the loss of their jobs if they didn’t cooperate with him. When we went out and tried to solicit support from the other workers and the other women who had actually witnessed a lot of this, or had been victims of it, it was very difficult to get people to come forward. A lot of women felt that if a woman gave in to the supervisor, then she was just as much to blame. A lot of the men didn’t want to deal with the issue at all.” At Up-To-Date, however, women and men came forward to expose the sexual harassment in the workplace.

While Black workers faced racial discrimination and women were the most sexually harassed, Latino workers had the most precarious employment. When workers were hired, the bosses often informed them that they knew about their undocumented status and would “protect” the workers from immigration, since the bosses found the workers attractive. The bosses would fire groups of Latino workers for demanding raises, resisting assault and supporting the union.

In the beginning, the UNITE! campaign at Up-To-Date wasn’t well known outside of the workers and their families. The organizing drive began as a rather typical National Labor Relations Board (NLRB) election. 80% of the workers had signed cards authorizing a union election, but through the lead up to the election, over 50 workers were fired. The Union filed over 100 charges of Federal labor law violations including: illegal termination, coercion, intimidation, discrimination, bribery, slashing working hours, rearranging schedules, moving union supporters into the “soil room”, and in some cases even threatening workers and union organizers with weapons. The bosses also stepped up surveillance, installing cameras, searching workers’ vehicles and personal property. In this environment of fear, UNITE! lost the election.

After having lost the initial election, UNITE! appealed to the NLRB and the Maryland Commission on Human Relations. While the cases dragged on through the courts, local activists began to get involved. It started with a solidarity action by local students with the Student Labor Action Committee (SLAC) at John Hopkins University, Z-point student group at the Maryland Institute College of Art, and the local Direct Action Network’s Baltimore cluster, the Coalition Against Global Exploitation (CAGE). It was through their participation as individual members of student groups and CAGE that anarchists became involved in the class struggle at Up-To-Date.

On September 26, as part of a local solidarity action with the protests against the IMF and World Bank in Prague, they strung up a “dirty laundry” line outside the laundry with slogans of support for the workers and condemnation of the bosses. By December, the campaign expanded to include students from four universities, Greens and SEIU who protested outside the University Hospital, fore-shadowing the secondary boycott campaign.

The legal battle with the NLRB led to a settlement between UNITE! and Up-To-Date, granting the organizers shop floor access, reinstatement of fired workers with back pay, and a new election. Committing to the difficult campaign, UNITE! continued to do out reach in the local community. CAGE and UNITE! planned a local protest in solidarity with the demonstrations in Quebec City against the Free Trade Agreement of the Americas (FTAA). Maryland had lost 8,000 jobs, while UNITE! had lost tens of thousands of union jobs to NAFTA, and the FTAA was expected to exacerbate the problem. Starting at the World Trade Center and thenmoving to a nearby customer of Up-To-Date, the Sheraton Hotel. At this action, anarchists distributed literature linking the actions against the FTAA with the struggle of workers at Up-To-Date sweatshop along with the “No Borders, No Bosses” and “Globalize Liberation” banners. By the time of the teach-in that evening, the Sheraton Hotel had pulled it’s contract with Up-To-Date, and at least one of the bosses was vomiting.

One misused phrase in the recent protests against global trade agreements and banks has been the reference to street protest as “Direct Action”. A protest alone is not direct action, particularly if it is only an appeal to power — that is indirect. Direct Action acts without intermediaries, our class taking action in struggle that builds dual power. The phrase originates from the labor movement with it’s strikes, boycotts, sabotage and expropriation... but it has found more manifestations. The workers at Up-To-Date saw the success of the protest at the Shertaon Hotel, and decided that direct action could meet their needs in the way that the indirect action of the NLRB could not.

Since the settlement, 11 workers had been fired at Up-To-Date. Two days after this strong show of community support at the FTAA protest, the workers at Up-To-Date abandoned the strategy of an NLRB election for a recognition strike! 150 workers participated in the strike vote, and 170 workers of 240 walked out, and the bosses brought in scabs from a temp service. The strike was to last 60 days before reaching victory for the workers. As anarchists were involved as neither Up-To-Date Workers or UNITE! staff, discussion will focus on the community support.

Mutual aid was supported by the community with fund-raising for the strike, food donations some of which came from Food Not Bombs. At one rally, the UNITE! offices were robbed. The thieves took only the union’s computers. Suspicion fell upon the bosses at Up-To-Date. Within days, anarchists had donated computers to UNITE! to keep the union running.

The rallies supporting Up-To-Date workers continued to increase in size and composition. They included students from half a dozen campuses, an equal number of labor unions, NAACP, and the majority of the left in Baltimore. Among anarchists, there was involvement from wobblies, black blocers, Food Not Bombs, punks, Claustrophobia, Agitate! and Roundhouse. The rallies, alone might not have been able to win the strike, what was needed was more direct action to support the direction action of striking workers.

A secondary boycott was the natural next step. A secondary boycott is where economic pressure is brought on businesses that are the customers of another business. In the case of Up-To-Date, where the customers were institutions like hospitals and businesses like hotels, then the only place to mount a successful boycott is by targeting the hospitals and hotels. Secondary boycotts, like sympathy strikes, are a prohibited activity for unions under Federal labor law. They are prohibited because the can be quite effective and risk spreading a labor struggle across whole industries and regions, which is against the collective interests of capitalists and the State — and is in the interests of anarchists and the working class.

UNITE! organizers, laundry workers and community supporters (including anarchists) began leafleting hospitals and affiliated universities. The leaflets called attention to the strike, talked about the risk to people’s safety from badly cleaned linen by scabs; but stopped just short of calling for a secondary boycott. Still, when leaf-letters were caught on hospital property they were arrested. In response, students and alumni used one of the signature tactics of the World Trade Organization and World Bank protests — they locked down in the hospital with bicycle locks around their necks, while others leafleted staff, patients and visitors — they too were arrested, and the television media was refused entry to the building.

Two days after the lockdown, the Mid-Atlantic Anarchist Bookfair came to town. The anarchists were under surveillance, and the police had staked out the Up-To-Date Laundry early that morning and were asking those on the picket line about anarchists — they were at the wrong place. Anarchists organized a march of about a hundred protesters to a Union Memorial Hospital, a portion of the corporate Medstar Health System that was a large customer of Up-To-Date. This was the first protest at any of the Medstar hospitals. The literature and banners were provocatively titled, “Blood on Their Sheets”, and explicitly called for a secondary boycott of Union Memorial and Medstar for endangering their patients and dealing with a struck company employing scabs. This action got UNITE! hauled into the NLRB court with charges of secondary boycott; however since the action was organized by anarchists independent of the union, none of the charges stuck.

This was to set the tone for the rest of the strike, with rallies increasing in size and diversity, with more protests at hospitals and hotels. The protests spread from Baltimore, to Washington, DC. Eventually, Up-To-Date customers buckled from community pressure, many of them announcing 60 day notices of contract termination. Two months after the strike started, the bosses at Up-To-Date finally caved into workers. The workers won recognition of their union, a pay raise to $7 an hour, free health insurance, an employer-paid pension plan, and a union health and safety committee. The charges of racism and sexually discrimination were being settled on an individual basis.

Unfortunately, that is the end of the story. The unfortunate part has to do with lost opportunities. From the beginning, UNITE!’s community supporters were informed that the struggle at Up-To-Date was part of a larger campaign to organize all the industrial laundries in Baltimore. The organizers were planning on borrowing a tactic from Justice for Janitors, when victory appeared close they would roll their striking picketers over to other laundries and activate walk-outs with the workers committees inside those laundries, effectively spreading the recognition strike through sympathy. Now, almost a year after the Up-To-Date strike, the rest of that campaign has failed to materialize and it would help future struggles to ask why.

Often, during a struggle like this, criticism of the strike doesn’t appear until after public discussion of it would not cripple the strike and show division among it’s supporters to the bosses. Even if stated, there are almost always divisions. Divisions between the workers, the organizers, the union local and international, with and between the community.

One of the divisions was the nature of the organizing many international unions are engaged in. Often, they “parachute” in organizers to a community who have just enough time to organize a drive, and then when victory or defeat appears eminent, they are then relocated to another city for another drive. This creates problems for building long term connection between the union organizers and the community, as well as having the organizers being viewed as “outsiders”. Often, organizers spend much of their time just getting to know the workers, that outreach to the community is considered a secondary concern when it is considered at all. Some unions have responded by sending in special community organizers to do outreach. Many organizers complain of their inability to connect with the community and their feelings of isolation that can lead to burn out. While the organizers are often dedicated to helping workers organize, ultimately the International calls the shots and relocate them in the middle of the campaign. During the Up-To-Date strike, one of the organizers who was working close with anarchists and students engaging in extralegal action was relocated to Toronto. After the workers at Up-To-Date won, almost the entire organizing staff was broken up and sent to different locations as a way for the International to discipline a disobedient staff.

Through much of the campaign, even being outside the union, there was a tension between locals on the ground, and the direction from the International in New York. The International, while obligated to support the recognition strike was hostile to direct action coming from the uncontrollable community supporters. The strike was expensive, costing the union a lot in funds, and risking injunctions and fines by skirting close to an illegal secondary boycott. The strike, for involving such a small number of workers was extremely high profile. At times, it seemed that the International would have been happy with the financial ruin Up-To-Date, so that a nearby regional laundry with a union contract could acquire Up-To-Date. New York didn’t even believe that the NAACP would support the strike.

While the community, including anarchists, was quite supportive of the laundry workers, they failed to develop a relationship directly with the workers at Up-To-Date and other laundries in the area. After the UNITE! organizers were pulled out by the International, the connections didn’t exist for the community to help laundry workers continue the campaign in the absence of support from the International. It was often much “easier” to talk to union organizers about what they needed done next instead of crossing a threshold of comfortability to talk with the workers. The barriers here are difficult to recognize while they are happening. Partially it was linguistic with so many Spanish speaking workers and English speaking supporters, sometimes the only exchange was in chants.

It may have been easier for the predominately white activists to identify with some of the organizers as fellow activists, than the middle aged black worker from the soil room. The striking workers have an even more intimidating threshold of comfortability to reach out to others for support. While the UNITE! strike was one of the largest multi-racial actions in Baltimore in the last five years, it also shows that there is a long way to go.

The anarchist response to the Up-To-Date struggle was overwhelmingly positive, but somewhat uncoordinated. Early on, it was not “as anarchists” that anarchists were participating, but rather through student and activist organizations. While some anarchist literature was distributed on the picket lines and with the boycott campaign, much more could have been done to agitate for anarchist ideas during the strike. There were often opportunities, like the daily picket lines, and the participation of UNITE organizers and workers speaking at the anarchist book fair.

Anarchists have largely abandoned the mainstream labor movement in the U.S. Part of that is the historical divide between the Industrial Workers of the World (IWW) and the AFL-CIO, but when the IWW was at a low point after the 1950s, the hesitation had to be more than old arguments about “dual unions”. Part of this has been the rejection of organization, the myth that somehow organization alone leads to hierarchy. Another problem has been a question for ideological purity in the organizations that anarchists do participate in, which has been an failing of numerous splits in the International Workers Association (IWA). There is a mistaken notion among some that participation with mass organizations is a form of Trotskyist or Fosterian “entryism” where the goal is to capture the union by seizing positions of authority in it’s hierarchical structure. Finally, there is often hesitation of involvement with a union because, by their very nature, unions have become integrated with the capitalist state by labor law and compromise with the bosses.

The problem is in expecting unions to be revolutionary. The majority of unions are not revolutionary, in so far in that as they currently exist they will bring about the revolution. Rather, unions are designed to reduce the rate of exploitation of workers in an employee-employer relationship. A union must act as a mediate between labor and capital, to the workers it must deliver increasing benefits, and to the employer it must deliver a work force. No matter how militant the strike, and how much popular support it has... until there is a social revolution against capitalism and expropriation, workers will have to compromise and return to an exploitive relationship.

As anarchists, we must participate in the popular broad-based movements of our class and agitate for our principles of direct action, direct democracy and solidarity. Through our participation in these movements, we can push for them to not only resemble the structure of a future society, but also through class conflict we can bring about ruptures in the system which will one day lead to a social revolution. If we fail to engage with mass movements, we will always be on the outside of those struggles, offering what support we can, but ultimately voiceless and powerless when it comes to the direction a movement moves. Participation in unions, for all their contradictions, provides us with a forum to agitate for our ideas with our class that has the potential for social change. A unionized shop might already have several workers who understand the importance of direct action, solidarity and democracy, and he struggle to form a union can be just as enlightening.

Some anarchists have learned through the protests against neo-liberalism that it is possible for anarchists to participate in mass demonstrations with a variety of ideas, fighting the ideas of both liberal reformism and authoritarianism, to create a more democratically structured movement from the bottom up that favors illegal direct action and puts forth a revolutionary perspective. We can participate in a principled way that doesn’t require us to water-down our politics, that we can do it in a collective way that protects us from isolation, and that we will not be resigned to only doing the grunt work of a struggle. It is now time to reach out from that protest movement into areas of popular struggle of our class. One day, we will have a strike that fortunately ends — in a social revolution.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

