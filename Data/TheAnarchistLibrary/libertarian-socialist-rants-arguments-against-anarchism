
Anarchists believe that social hierarchies, including but not limited to capitalism and the state, should be dismantled if they cannot be proven to be just or necessary. So, to effectively argue against anarchism, you have to prove the premise that the state and/or the capitalist system are just and necessary forms of social hierarchy that should exist. In this video, I'm going to be looking at various arguments against anarchism and address whether or not they successfully prove that premise.

A common objection to anarchism is the idea that anarchism is wrong because brutes will rise to power. Let's look at this argument in further detail.

1. If hierarchies are bad, then we ought to advocate the society with the least number of hierarchies.

2. The state is a hierarchy whose existence results in fewer hierarchies than would occur in the absence of the state.

C: Therefore, given that we ought to advocate the society with the least number of hierarchies, a statist society is preferable to a stateless one.

An initial problem with this argument is that a stateless society is not necessarily an anarchist society. For example, anarchists may agree that a society in which capitalism is curtailed by a state will result in fewer hierarchies than a society in which capitalism is completely unregulated, while at the same time believing that a society with neither capitalism nor the state, built on non-hierarchical free associations, is the most preferable. However, to fully address the anarchist position, one would have to prove that the kind of stateless society anarchists advocate would result in more hierarchies than a society with a state. The problem is, it just doesn't stack up to the facts.

The state has a tendency to produce more hierarchies, not less. This is primarily because the state is a centralised institution with a monopoly on the use of violence - it is a large concentration of power, and large concentrations of power tend to attract a specific kind of people, most likely those who desire the exercise of power.

Politicians have an incentive to go to war. War gives politicians and their cronies an opportunity to gain material wealth through arms manufacturing and the plundering of natural resources. It gives politicians the power to pass laws that wouldn't be acceptable in a time of peace. It allows them to mobilise popular support in a way that otherwise wouldn't be possible, extend their time in office in the name of national safety, and secure them a position of glory in history as a 'great wartime leader'. Politicians are almost always ambitious and power-hungry, and wars often give politicians a chance to increase their own power. And because the state is a centralised institution with a monopoly on the use of violence, the politicians always have the military power to go right ahead and start a war. While not every state engages in war, most states do at some point, because of the incentives that exist for politicians.

Contrast this with an anarchist system, in which capitalism would be abolished, the state would be dissolved, and instead, decentralised, freely associated workers' collectives would directly carry out decision-making based on consensus democracy. In such a system, there are no concentrated centres of power in which individuals and groups can command and control others - it is engineered so that brutes cannot rise to power. Without capitalism, there would no longer be the mindless pursuit of growth for growth's sake, no matter the cost to others, and so the pursuit of material wealth would not be of relevance. In an anarchist system, people who want to engage in wars would have absolutely no excuse, and indeed no opportunity to do so.

But what about capitalism? Well, in terms of hierarchy, the state doesn't hold a candle to the hierarchies that capitalism creates! To begin with, those who own the most property have authority over those who have nothing to sell but themselves. This manifests itself in wage labour and the boss-worker relationship, which is hierarchical. Under anarchism, private property would be abolished, and the boss-worker relationship with it.

Under capitalism, competing for more profit is favoured as a mechanism for providing better goods and services. Anarchism on the other hand achieves this through co-operation. People who are highly skilled would help those who are not as skilled, rather than simply stomping their feet, claiming their cash prize, and doing a little victory dance.

Under capitalism, it is not uncommon for economic crises to occur - they are considered natural, almost like bad weather. In a crisis, people have an opportunity to bring the whole capitalist system into question - but because capitalism gives the ruling class a cosy, privileged lifestyle at the expense of everyone else, the ruling class have an incentive to divert the public from the causes of their own suffering. This is why the economic crises of capitalism often provide a platform for racism, sexism, homophobia, transphobia, ableism, nationalism, fascism and other forms of bigotry to emerge towards anyone who isn't a straight white male. By contrast, such an elite would no longer exist under anarchism, not to mention the fact that it is a contradiction in terms for an anarchist to be a bigot of any kind, because bigotry is inherently hierarchical.

Under capitalism, advertising socialises people into believing that those with certain products have a higher social standing than those without them, as well as generally serving the purpose of systematically dulling people's brains and getting them to conform, obey and consume. Schools in capitalist society serve a similar purpose, indoctrinating children into blind acceptance of authority so that they will make an obedient workforce for the capitalist class. In an anarchist society, based on common ownership of the means of production and non-hierarchical free associations, these problems simply wouldn't exist.

Does capitalism result in fewer hierarchies than would emerge in an anarchist society? I really doubt it! Just like statism, the nature of the capitalist system is to perpetuate more hierarchy, not less, and so if we accept the premise that hierarchies that are unjust and unnecessary ought to be avoided, and that an anarchist society will be less hierarchical than a statist society, it follows that we should accept anarchism rather than statism.

Let's look at some other arguments.

'But don't people need a leader of some sort?'

What this seems to imply is that a statist society will satisfy this need, whereas an anarchist society wouldn't. The most obvious problem with this is that it doesn't necessarily follow that statism is therefore preferable, unless you make the assumption that systems should be judged solely on whether they meet this particular need for a leader, and everything else should be disregarded. This is, of course, bollocks.

Secondly, the state is not simply a benign institution which satisfies a need for 'leadership'. People are subordinate to the state regardless of whether they vote or not. The consent of the public doesn't matter, the public are not autonomous within their relationship with the state, and they cannot choose to terminate it at any time. This is domination, not leadership. You cannot argue that because people need leaders, they must have masters forced upon them whether they like it or not.

Studies have shown that hierarchies of power increase our stress hormones, adrenaline and glucocorticoids. These stress hormones are very useful for dealing with threats, initiating the fight or flight response and allowing us to act fast when we're in danger. But when these hormones are released without any immediate threat to our survival, we can be stressed for no reason at all, which damages our health and shortens our life expectancy. We certainly don't have a need for domination of this kind.

http://regardingwork.com/2011/07/17/hierarchies-kill/

Thirdly, an anarchist society (surprisingly enough) could indeed satisfy the need for leadership. Anarchists do not reject all authority, but merely that which is unjust or unnecessary. If you want to learn how to paint, then it is perfectly fine for you to go and see a painter for guidance, provided you consent to it, you remain autonomous within the relationship and you can terminate it at any time. What is not okay is if the painter imposes his or her authority upon you.

'Wouldn't there be any law and order in an anarchist society?'

It's important to note that anarchists have a problem with rulers, not rules. A rule is a statement about how things should be done according to a certain criteria - the rules for football determine what should happen in a football game. Rulers, on the other hand, exercise dominion over others. A ruler makes rules in an imposing way, because the people who are affected by such rules don't get to decide what they are, and usually rulers are far less subject to the rules as everyone else, because they are further up on the hierarchy of power. Rulers are in contradiction to the anarchist position because they violate the principle that people should be autonomous, and they should have control over their own lives. However, anarchists can still support rules because statements about how we ought to conduct ourselves in society do not necessarily need to be made in an imposing way. So could there be rules in an anarchist society? Of course.

But there's an important point to make here about the anarchist approach towards crime (that is to say, anti-social behaviour) - crime does not simply emerge for its own sake or because humans are inherently evil and they have 'original sin', but it has causes. Capitalism causes poverty, homelessness, unemployment, and alienation; it requires a massive degree of social atomisation, putting people at each other's throats, destroying the positive social connections between human beings that are required for an ethical society to function, creating massive wealth disparities, destroying the environment and replacing any critical or independent thought with the desire to mindlessly accumulate more commodities - all of which is facilitated by a cruel and demeaning education system which beats out any potential in children before it is even realised.

Rather than punishing those who act in response to these contributing factors, anarchists seek to prevent anti-social behaviour by eliminating the oppressive social conditions which give rise to it in the first place. In a horizontal network of voluntary associations through which everyone will be free to satisfy his or her needs, the likelihood of anti-social behaviour occurring would be dramatically reduced, if not eliminated altogether. For more information about how remaining anti-social behaviour may be dealt with in an anarchist society, I recommend you check out Section I of an Anarchist FAQ, which gives more detail about what an anarchist society might look like than I can in this video.

There are many people who make arguments against anarchism that are based on a claim about human nature, a claim that irrespective of the influence of culture, human beings have a natural tendency to behave in a way which is selfish, power-seeking, competitive, lazy, greedy, and stupid, which is completely fixed, and therefore social anarchism, with its core values of liberty, equality and solidarity, will fail.

Even I used to subscribe to such a view, but there are countless problems with this argument. First and foremost, it's quite clear that human behaviour is not strictly limited to greed and selfishness, because we clearly aren't biologically incapable of altruism, and we can recognise this fact every time we see someone carrying out an altruistic act.

Over time, the human brain has developed a mirror neuron system, which gives us the ability to put ourselves in other peoples' shoes. When we are put under threat, activity increases in the parts of our brain that deal with threat response - the anterior insula, putamen and the supramarginal gyrus. A study carried out by a group of psychologists at the University of Virginia showed that when our loved ones are under threat, activity increases in these areas to the same extent as it does when we ourselves are under threat. While the activity didn't significantly increase when people saw strangers being put under threat, the study nevertheless shows that we have a capacity to empathise with others.

http://www.sciencedaily.com/releases/2013/08/130822085804.htm

In Mutual Aid: A Factor of Evolution, Peter Kropotkin looks at co-operative behaviour, giving numerous examples of mutual aid occurring throughout various species in the animal kingdom, and human societies, both ancient and modern. In 'Nice Guys Finish First', Richard Dawkins explains how altruistic behaviour can emerge despite survival of the fittest, not only giving examples of it within single species, but also giving examples of it occuring between individuals who aren't even a part of the same species.

http://www.complementarycurrency.org/ccLibrary/Mutual_Aid-A_Factor_of_Evolution-Peter_Kropotkin.pdf

http://www.youtube.com/watch?v=I71mjZefg8g

Research in positive psychology seems to suggest a strong link between altruistic behaviour and happiness. For example, a study carried out by psychologists from the universities of Pennsyllvania, Rhode Island and Michigan, as well as a study by psychologists at Harvard, both came to the conclusion that expressing gratitude towards others can make you a happier person.

http://www.ppc.sas.upenn.edu/articleseligman.pdf

http://www.health.harvard.edu/healthbeat/giving-thanks-can-make-you-happier

A study from the university of Wisconsin-Madison found out that those who help others at work are more likely to be committed to their work, less likely to quit their jobs, and are also happier than those who don't.

http://www.news.wisc.edu/21983

Psychological scientists from the universities of Cambridge, Plymouth and California also found out that not only do we often feel elevated and get that warm, fuzzy feeling inside when we witness other people behave in a very compassionate and altruistic way, but also having that feeling of elevation can compel us to behave more compassionately and altruistically ourselves.

http://www.psychologicalscience.org/media/releases/2010/schnall.cfm

So - the science seems to show a few things as far as altruism goes:

· We have a capacity for altruism

· Witnessing altruism often makes us feel happy

· Witnessing altruism often inspires more altruism

· Performing altruism often makes us feel happy

· Thanking others for performing altruism often makes us feel happy

With all of this evidence in mind, the claim that we somehow have a fixed predisposition towards selfishness is not only inaccurate, it's ridiculous.

Other claims, such as 'people love power' have their own problems. This is because power requires a superior and a subordinate - someone to exercise the power, and also someone to have the power exercised over them. This is where the problem lies - to suggest that 'people love power' is to ignore the feelings of those having power exercised over them, and to only account for the feelings of the people exercising the power. It's a bit like saying 'people like slavery' - only if you're a slave owner, but certainly not if you're a slave. Do unto others as you would have done unto you - if you don't enjoy being a slave, don't enslave others. The same principle applies to the exercise of power.

In regards to altruism, I'm not saying that humans have an innate, fixed tendency towards altruism, but rather that humans are capable of both egotism and altruism, and which way the table turns depends on the environment. The second problem with this argument from human nature is that it assumes human nature is a fixed concept - however, this is not the case.

A study by psychologist Solomon Asch explored the effects of group pressure upon the modification and distortion of judgement. He found that if people are presented with unanimous agreement, it's not uncommon for them to give in to conformity, and that after some time of repeated conformism, their perception can be distorted to the extent that they are no longer aware of a discrepancy between their own thinking and that of the group. Asch's elevator conformity experiment also provides a good example of people conforming to group norms.

A study by Weaver and colleagues found out that our brains often have trouble distinguishing between an opinion expressed three times by the same person in a group, and the same opinion expressed by three different people in a group. If this is the case, then I think it's safe to say that it's possible for a small number of people to manufacture the consent of the public and create an illusion of conformity.

http://www.youtube.com/watch?v=TrNIuFrso8I

Consider that many of us gather information from schools and the mass media - these institutions are run from the top down by concentrated centres of power. In capitalist countries, the media are private companies that usually make their profits by selling audiences to advertisers (other businesses). If that transaction is to be mutually beneficial for the parties involved, then we should have a media system that reflects the interests of... Wait for it... Businesses! And when ordinary members of the public watch television and they are given a view on the world from the same angle repeatedly, it is very likely that conformity will kick in and dissent towards the norm will be completely internalised. We live in an authoritarian society, but many of us simply don't realise it because we've been socialised into acceptance of it. If we are subjected to propaganda from a very early age, our behaviour can change radically. If you had been brought up in Nazi Germany, the likelihood of you becoming a Nazi would be much greater than if you had been brought up elsewhere.

http://torontoforumoncuba.weebly.com/uploads/5/1/8/5/5185218/manufacturing_consent_-_the_political_economy_of_the_mass_media.pdf

People in positions of power also have their behaviour affected by their environment as well. A study by a group of neuroscientists at Wilfrid Laurier University in Ontario, Canada, found out that even small increases of power, such as getting a job promotion or more money, have a tendency to decrease activity of the mirror neuron system and reduce a person's ability to empathise with others.

http://nymag.com/news/features/money-brain-2012-7/

Psychologists at the University of Berkeley, California, carried out a study in which two students played a game of monopoly that was unfairly rigged so that one player would never lose. As the game went on, this player behaved in a more and more egotistical and dominating manner. The same could not be said for his opponent.

http://www.michaelinzlicht.com/wp/wp-content/uploads/downloads/2013/06/Hogeveen-Inzlicht-Obhi-in-press.pdf

The Stanford Prison Experiment also provides a good example of people behaving in a brutal and dehumanising manner when they occupy authoritarian institutional roles.

Empirical evidence seems to suggest that human behaviour is not fixed, but that it changes according to the environment. If we have an environment which nurtures bigotry, hostility, and egotism, then people will behave in a bigoted, hostile and egotistical way. If we have an environment which nurtures altruism and co-operation, then people will behave in an altruistic and co-operative way.

This is not some wild impossibility. This is attainable. We do have the capacity to behave well and look after one another, and that capacity will be able to realise itself if we deconstruct the social and economic institutions that have systematically repressed the best parts of our character, and in doing so have caused the suffering of millions of innocent people around the world. The idea that human beings are somehow worthless and evil by default is a murderous delusion used to prop up outdated institutions which serve little purpose other than to accumulate their own power at the expense of the masses which they depend on to survive. The toxic religious doctrine of original sin has one purpose, and one purpose only: to make you obey.

So what's it to be? Do we accept the institutions that perpetuate violence and starvation while depriving ordinary human beings of any sense of autonomy, community, and democracy? Do we accept the institutions that have resulted in the proliferation of weapons of mass destruction around the world? Do we accept the institutions that dull the brains of the public into conspicuous consumption and the mindless satisfaction of created wants, and yet rely on just enough creativity and scientific innovation to make sure it doesn't all just collapse?

If we continue walking on this precarious path, there's a significant chance that one hundred years from now, our species will cease to exist. Are you willing to take that risk? Or are you going to recognise this situation for what it is? An emergency. A crisis. A threat to human survival, which cannot simply be remedied by having kinder, gentler forms of oppression. If you believe that a better world is both possible and desirable, it's not a question of if and when we will change things. It is a question of how do we change things right now.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

