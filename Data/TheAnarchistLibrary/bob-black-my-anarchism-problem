
Anarchism has always been problematic for me. It helped me to arrive at an unconditionally anti-statist, anti-capitalist perspective by the mid-1970’s, and yet my first public statement from that perspective explained why I did not identify with anarchism. By dictionary definition, I am an anarchist, but the dictionary is only the beginning of wisdom. It cannot bestow coherence where contradictions abound or reduce differences to a unity by calling them by the same name.

Once an idea is launched into history it takes more and more of its meaning from its experience. Revivalist calls to return to first principles prove the point, for they are history too. And just as no Protestant sect has ever really recreated the primitive Church, no subsequent anarchist fundamentalism ever did or could reenact pure anarchism on Bakuninist, Kropotkinist or any other models. Anything which has entered importantly into the practice of the anarchists has a place in the anarchist phenomenon-in-process, whether or not it is logically deducible from the idea or even contradicts it. Sabotage, vegetarianism, assassination, pacifism, free love, co-operatives and strikes are all aspects of anarchism which their anarchist detractors try to dismiss as un-anarchist.

To call yourself an anarchist is to invite identification with an unpredictable array of associations, an ensemble which is unlikely to mean the same thing to any two people, including any two anarchists. (The most predictable is the least accurate: the bomb-thrower. But anarchists have thrown bombs and some still do.)

The trouble with anarchists is that they think they have agreed on what they all oppose — the state — whereas all they have agreed on is what to call it. You could make a good case that the greatest anarchists were nothing of the sort. Godwin wanted the state to wither away, but gradually, and not before the progress of enlightenment prepared people to do without it. Which seems to legitimate really existing statism and culminate in the banality that if things were different they would not be the same. Proudhon, who served in the French national legislature, in the end arrived at a theory of “federalism” which is nothing but the devolution of most state power on local governments. Kropotkin’s free communes may not be nation-states but they sure sound like city-states. Certainly no historian would regard as anything but ludicrous Kropotkin’s claim that medieval cities were anarchist.

If some of the greatest anarchists, upon inspection, appear to fall somewhat short of consistency on even the defining principle of anarchism itself — the abolition of the state — it is not too surprising if some of the lesser lights are likewise dim bulbs. The One Big Union of the syndicalists, who also uphold the duty to work, is one big state to everybody else, and totalitarian to boot. Some “anarcha”-feminists are book-burners. Dean Murray Bookchin espouses third-party politics and municipal statism, eerily parallel to the borderline fascist militia/Posse Comitatus movement which would abolish all government above the county level. And Bakunin’s “invisible government” of anarchist militants is, at best, a poor choice of words, especially on the lips of a Freemason.

Anarchists are at odds over work, industrialism, unionism, urbanism, science, sexual freedom, religion and much more which is more important, especially when taken together, than anything that unites them. Each of the North American annual “gatherings” of 1986–1989, the first time most of these anarchists dealt with one another face to face, resulted in a hemorrhage of the disillusioned. Nobody cares to host the next one, although some regional gatherings have gone off fairly well.

But despite the demogogues, doctrinaires and dimwits, a portion of the anarchist press has let in some air, not all of it hot air; and oxygen is antiseptic. Anarchist or, better, anarchistic marginals have often known what to take and what to leave. A family of unorthodoxies I’ve called “Type 3” or “Watsonian” anarchism has made major inroads into the traditionalists in recent years. Type 3’s, the category of the unclassifiables, enrich their anarchism (or whatever it is) with borrowings from neo-primitivism (or else neo-futurism!), surrealism, situationism, the joke religions (Discordian, Moorish Science, SubGenius), punk culture, dope culture, beer culture and Beat culture. Several years ago the outnumbered workerists launched a hate campaign against Type 3’s among others — lumped or I should say lumpened together by the moronic epithet “neo-individualist”. We are social parasites, mystics, kiddy-diddlers and just generally amoral savages. Yeah, but they are college boys in designer hardhats.

The anarchists ... can’t live with them, can’t live without them. As I once informed Demolition Derby, anarchists may make lousy comrades but they’re excellent customers. In 1985 I was so disgusted with the lot of them that I broke off all ties. Over the years that became meaningless, since exactly what was “anarchist” enough to boycott got blurry. Now I proceed on a case-by-case basis.

This chapter, like the next, is a rogues’ gallery. For some of those anarchists I respect, like Ed Lawrence and Hakim Bey, I have shown my esteem in other chapters. Meanwhile I resume the wrestle with terminology. Am I an anarchist or not? Like Feral Faun and others, I have shuffled by counterposing “anarchy” and “anarchism”. Even if the distinction catches on, what to call the respective parties? This is what I suggest. Let the anarchy-ists call themselves anarchs, a word whose first known appearance — in Milton’s Paradise Lost! — antedates anarchist by nine years. It’s better because, like the corresponding distinction of monarch from monarchist, it designates not what we believe but what we are, insofar as our power permits: powers unto ourselves.

Too often have the anarchists lectured me to shun “feuds” and “in-fighting” the better to fight “the real enemy,” by which they mean some conveniently remote abstraction such as capitalism or the state. Mow it’s arrogant for people who say I’m arrogant to tell me they’re better at spotting my real enemies than I am. In its most seductive form — the flattering suggestion that my enemies are unworthy of me — I have refuted the argument by the way I praise John Crawford (chapter six). I might dismiss the standard, cruder version as a cynical self-serving ploy to escape my criticisms by misdirecting them. Though offered, occasionally, in good faith, it’s rubbish.

The Lone Ranger and Tonto are surrounded by Indians. The Ranger says, “It looks like we’ve had it, old friend.” Tonto says, “What you mean we, paleface?”

“The real enemy” is the totality of physical and mental constraints by which capital, or class society, or statism, or the society of the spectacle expropriates everyday life, the time of our lives. The real enemy is not an object apart from life. It is the organization of life by powers detached from it and turned against it. The apparatus, not its personnel, is the real enemy. But it is by and through the apparatchiks and everyone else participating in the system that domination and deception are made manifest. The totality is the organization of all against each and each against all. It includes all the policemen, all the social workers, all the office workers, all the nuns, all the op-ed columnists, all the drug kingpins from Medellin to Upjohn, all the syndicalists and all the situationists.

This isn’t rhetoric to me; it informs my choices. It implies that I can expect to find authoritarian actions, opinions and personalities among anarchists as elsewhere. “Comrades” are not my comrades — nor am I, at my worst, my own comrade — insofar as they or I behave like “the real enemy”. There is no real enemy apart from human agency.

And what better place for authoritarians to nest than among anarchists who are so easily taken in by labels, so easily dazzled by slick production values, and so easily confused by the facts? Although it is only an ideal type, the authoritarian personality is all but completely realized in anarchists like Jon Bekken, Michael Kolhoff, Chaz Bufe, Fred Woodworth and Chris Gunderson as in anti-authoritarians like Caitlin Manning, Chris Carlsson, Adam Cornford and Bill Brown. (Anti-authoritarian, what a story that word could tell; as Bill Knott put it, “If only mouthwash could talk”.)

If anarchists are capable of authoritarian attitudes and ideological incoherence, I should no more hail one as a comrade, sight unseen, than I would a state trooper or used-car dealer. The label is not a warranty. An important reason for my 1985 disclaimer of anarchism was to forstall any claims on my loyalty or for exemption from criticism on the basis that “we” are on the same side. A real comrade would welcome critique.

Talk of my “feuds” is usually foolishness. While there is no ultimate separation of personal from political, especially if you are as political a person as I am, predominantly personal quarrels find no place in this book. An argument does not become a feud just because I take it past the mutual monolog stage or the other guy starts calling me names. Ideologues who lack the ability or maturity to defend their opinions in depth should keep them to themselves, especially if they publish magazines.

I’ve been accused of overkill for the following attentats against anarchist publishers Fred Woodworth and “Spider Rainbow”. It’s a close call. Spider Rainbow did dry up and blow away, but there’s one born every minute. Woodworth wheezes along, for that which never really lived can never really die: I excavate the mummy and his mummery. The proper measure of the value of my words is not the stature of my subjects. They don’t have to be important to be useful for a change.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

