
There are periods in the life of human society when revolution becomes an imperative necessity, when it proclaims itself as inevitable. New ideas germinate everywhere, seeking to force their way into the light, to find an application in life; everywhere they are opposed by the inertia of those whose interest it is to maintain the old order; they suffocate in the stifling atmosphere of prejudice and traditions. The accepted ideas of the constitution of the State, of the laws of social equilibrium, of the political and economic interrelations of citizens, can hold out no longer against the implacable criticism which is daily undermining them whenever occasion arises, — in drawing room as in cabaret, in the writings of philosophers as in daily conversation. Political, economic, and social institutions are crumbling; the social structure, having become uninhabitable, is hindering, even preventing the development of the seeds which are being propagated within its damaged walls and being brought forth around them.

The need for a new life becomes apparent. The code of established morality, that which governs the greater number of people in their daily life, no longer seems sufficient. What formerly seemed just is now felt to be a crying injustice. The morality of yesterday is today recognized as revolting immorality. The conflict between new ideas and old traditions flames up in every class of society, in every possible environment, in the very bosom of the family. The son struggles against his father, he finds revolting what his father has all his life found natural; the daughter rebels against the principles which her mother has handed down to her as the result of long experience. Daily, the popular conscience rises up against the scandals which breed amidst the privileged and the leisured, against the crimes committed in the name of the law of the stronger, or in order to maintain these privileges. Those who long for the triumph of justice, those who would put new ideas into practice, are soon forced to recognize that the realization of their generous, humanitarian and regenerating ideas cannot take place in a society thus constituted; they perceive the necessity of a revolutionary whirlwind which will sweep away all this rottenness, revive sluggish hearts with its breath, and bring to mankind that spirit of devotion, self-denial, and heroism, without which society sinks through degradation and vileness into complete disintegration.

In periods of frenzied haste toward wealth, of feverish speculation and of crisis, of the sudden downfall of great industries and the ephemeral expansion of other branches of production, of scandalous fortunes amassed in a few years and dissipated as quickly, it becomes evident that the economic institutions which control production and exchange are far from giving to society the prosperity which they are supposed to guarantee; they produce precisely the opposite result. Instead of order they bring forth chaos; instead of prosperity, poverty and insecurity; instead of reconciled interests, war; a perpetual war of the exploiter against the worker, of exploiters and of workers among themselves. Human society is seen to be splitting more and more into two hostile camps, and at the same time to be subdividing into thousands of small groups waging merciless war against each other. Weary of these wars, weary of the miseries which they cause, society rushes to seek a new organization; it clamors loudly for a complete remodeling of the system of property ownership, of production, of exchange and all economic relations which spring from it.

The machinery of government, entrusted with the maintenance of the existing order, continues to function, but at every turn of its deteriorated gears it slips and stops. Its working becomes more and more difficult, and the dissatisfaction caused by its defects grows continuously. Every day gives rise to a new demand. “Reform this,” “reform that,” is heard from all sides. “War, finance, taxes, courts. police, everything must be remodeled, reorganized, established on a new basis,” say the reformers. And vet all know that it is impossible to make things over, to remodel anything at all because everything is interrelated; everything would have to be remade at once; and how can society be remodeled when it is divided into two openly hostile camps? To satisfy the discontented would be only to create new malcontents.

Incapable of undertaking reforms, since this would mean paving the way for revolution, and at the same time too impotent to be frankly reactionary, the governing bodies apply themselves to halfmeasures which can satisfy nobody, and only cause new dissatisfaction. The mediocrities who, in such transition periods, undertake to steer the ship of State, think of but one thing: to enrich then.selves against the coming débâcle. Attacked from all sides they defend themselves awkwardly, they evade, they commit blunder upon blunder, and they soon succeed in cutting the last rope of salvation; they drown the prestige of the government in ridicule, caused by their own incapacity.

Such periods demand revolution. It becomes a social necessity; the situation itself is revolutionary.

* * *

When we study in the works of our greatest historians the genesis and development of vast revolutionary convulsions, we generally find under the heading, “The Cause of the Revolution,” a gripping picture of the situation on the eve of events. The misery of the people, the general insecurity, the vexatious measures of the government, the odious scandals laying bare the immense vices of society, the new ideas struggling to come to the surface and repulsed by the incapacity of the supporters of the former régime, — nothing is omitted. Examining this picture, one arrives at the conviction that the revolution was indeed inevitable, and that there was no other way out than by the road of insurrection.

Take, for example, the situation before 1789 as the historians picture it. You can almost hear the peasant complaining of the salt tax, of the tithe, of the feudal payments, and vowing in his heart an implacable hatred towards the feudal baron, the monk, the monopolist, the bailiff. You can almost see the citizen bewailing the loss of his municipal liberties, and showering maledictions upon the king. The people censure the queen; they are revolted by the reports of ministerial action, and they cry out continually that the taxes are intolerable and revenue payments exorbitant, that crops are bad and winters hard, that provisions are too dear and the monopolists too grasping, that the village lawyer devours the peasant’s crops and the village constable tries to play the role of a petty king, that even the mail service is badly organized and the employees too lazy. In short, nothing works well, everybody complains. “It can last no longer, it will come to a bad end,” they cry everywhere.

But, between this pacific arguing and insurrection or revolt, there is a wide abyss, — that abyss which, for the greatest part of humanity, lies between reasoning and action, thought and will, — the urge to act. How has this abyss been bridged? How is it that men who only yesterday were complaining quietly of their lot as they smoked their pipes, and the next moment were humbly saluting the local guard and gendarme whom they had just been abusing, — how is it that these same men a few days later were capable of seizing their scythes and their iron-shod pikes and attacking in his castle the lord who only yesterday was so formidable? By what miracle were these men, whose wives justly called them cowards, transformed in a day into heroes, marching through bullets and cannon balls to the conquest of their rights? How was it that words, so often spoken and lost in the air like the empty chiming of bells, were changed into actions?

The answer is easy.

Action, the continuous action, ceaselessly renewed, of minorities brings about this transformation. Courage, devotion, the spirit of sacrifice, are as contagious as cowardice, submission, and panic.

What forms will this action take? All forms, — indeed, the most varied forms, dictated by circumstances, temperament, and the means at disposal. Sometimes tragic, sometimes humorous, but always daring; sometimes collective, sometimes purely individual, this policy of action will neglect none of the means at hand, no event of public life, in order to keep the spirit alive, to propagate and find expression for dissatisfaction, to excite hatred against exploiters, to ridicule the government and expose its weakness, and above all and always, by actual example, to awaken courage and fan the spirit of revolt.

When a revolutionary situation arises in a country, before the spirit of revolt is sufficiently awakened in the masses to express itself in violent demonstrations in the streets or by rebellions and uprisings, it is through action that minorities succeed in awakening that feeling of independence and that spirit of audacity without which no revolution can come to a head.

Men of courage, not satisfied with words, but ever searching for the means to transform them into action, — men of integrity for whom the act is one with the idea, for whom prison, exile, and death are preferable to a life contrary to their principles, — intrepid souls who know that it is necessary to dare in order to succeed, — these are the lonely sentinels who enter the battle long before the masses are sufficiently roused to raise openly the banner of insurrection and to march, arms in hand, to the conquest of their rights.

In the midst of discontent, talk, theoretical discussions, an individual or collective act of revolt supervenes, symbolizing the dominant aspirations. It is possible that at the beginning the masses will remain indifferent. It is possible that while admiring the courage of the individual or the group which takes the initiative, the masses will at first follow those who are prudent and cautious, who will immediately describe this act as “insanity” and say that “those madmen, those fanatics will endanger everything.”

They have calculated so well, those prudent and cautious men, that their party, slowly pursuing its work would, in a hundred years, two hundred years, three hundred years perhaps, succeed in conquering the whole world, — and now the unexpected intrudes! The unexpected, of course, is whatever has not been expected by them, — those prudent and cautious ones! Whoever has a slight knowledge of history and a fairly clear head knows perfectly well from the beginning that theoretical propaganda for revolution will necessarily express itself in action long before the theoreticians have decided that the moment to act has come. Nevertheless, the cautious theoreticians are angry at these madmen, they excommunicate them, they anathematize them. But the madmen win sympathy, the mass of the people secretly applaud their courage, and they find imitators. In proportion as the pioneers go to fill the jails and the penal colonies, others continue their work; acts of illegal protest, of revolt, of vengeance, multiply.

Indifference from this point on is impossible. Those who at the beginning never so much as asked what the “madmen” wanted, are compelled to think about them, to discuss their ideas, to take sides for or against. By actions which compel general attention, the new idea seeps into people’s minds and wins converts. One such act may, in a few days, make more propaganda than thousands of pamphlets.

Above all, it awakens the spirit of revolt: it breeds daring. The old order, supported by the police, the magistrates, the gendarmes and the soldiers, appeared unshakable, like the old fortress of the Bastille, which also appeared impregnable to the eyes of the unarmed people gathered beneath its high walls equipped with loaded cannon. But soon it became apparent that the established order has not the force one had supposed. One courageous act has sufficed to upset in a few days the entire governmental machinery, to make the colossus tremble; another revolt has stirred a whole province into turmoil, and the army, till now always so imposing, has retreated before a handful of peasants armed with sticks and stones. The people observe that the monster is not so terrible as they thought they begin dimly to perceive that a few energetic efforts will be sufficient to throw it down. Hope is born in their hearts, and let us remember that if exasperation often drives men to revolt, it is always hope, the hope of victory, which makes revolutions.

The government resists; it is savage in its repressions. But, though formerly persecution killed the energy of the oppressed, now, in periods of excitement, it produces the opposite result. It provokes new acts of revolt, individual and collective, it drives the rebels to heroism; and in rapid succession these acts spread, become general, develop. The revolutionary party is strengthened by elements which up to this time were hostile or indifferent to it. The general disintegration penetrates into the government, the ruling classes, the privileged; some of them advocate resistance to the limit; others are in favor of concessions; others, again, go so far as to declare themselves ready to renounce their privileges for the moment, in order to appease the spirit of revolt, hoping to dominate again later on. The unity of the government and the privileged class is broken.

The ruling classes may also try to find safety in savage reaction. But it is now too late; the battle only becomes more bitter, more terrible, and the revolution which is looming will only be more bloody. On the other hand, the smallest concession of the governing classes, since it comes too late, since it has been snatched in struggle, only awakes the revolutionary spirit still more. The common people, who formerly would have been satisfied with the smallest concession, observe now that the enemy is wavering; they foresee victory, they feel their courage growing, and the same men who were formerly crushed by misery and were content to sigh in secret, now lift their heads and march proudly to the conquest of a better future.

Finally the revolution breaks out, the more terrible as the preceding struggles were bitter.

The direction which the revolution will take depends, no doubt, upon the sum total of the various circumstances that determine the coming of the cataclysm. But it can be predicted in advance, according to the vigor of revolutionary action displayed in the preparatory period by the different progressive parties.

One party may have developed more clearly the theories which it defines and the program which it desires to realize; it may have made propaganda actively, by speech and in print. But it may not have sufficiently expressed its aspirations in the open, on the street, by actions which embody the thought it represents; it has done little, or it has done nothing against those who are its principal enemies; it has not attacked the institutions which it wants to demolish; its strength has been in theory, not in action; it has contributed little to awaken the spirit of revolt, or it has neglected to direct that spirit against conditions which it particularly desires to attack at the time of the revolution. As a result, this party is less known; its aspirations have not been daily and continuously affirmed by actions, the glamor of which could reach even the remotest hut; they have not sufficiently penetrated into the consciousness of the people; they have not identified themselves with the crowd and the street; they have never found simple expression in a popular slogan.

The most active writers of such a party are known by their readers as thinkers of great merit, but they have neither the reputation nor the capacities of men of action; and on the day when the mobs pour through the streets they will prefer to follow the advice of those who have less precise theoretical ideas and not such great aspirations, but whom they know better because they have seen them act.

The party which has made most revolutionary propaganda and which has shown most spirit and daring will be listened to on the day when it is necessary to act, to march in front in order to realize the revolution. But that party which has not had the daring to affirm itself by revolutionary acts in the preparatory periods nor had a driving force strong enough to inspire men and groups to the sentiment of abnegation, to the irresistible desire to put their ideas into practice, — (if this desire had existed it would have expressed itself in action long before the mass of the people had joined the revolt) — and which did not know how to make its flag popular and its aspirations tangible and comprehensive, — that party will have only a small chance of realizing even the least part of its program. It will be pushed aside by the parties of action.

These things we learn from the history of the periods which precede great revolutions. The revolutionary bourgeoisie understood this perfectly, — it neglected no means of agitation to awaken the spirit of revolt when it tried to demolish the monarchical order. The French peasant of the eighteenth century understood it instinctively when it was a question of abolishing feudal rights; and the International acted in accordance with the same principles when it tried to awaken the spirit of revolt among the workers of the cities and to direct it against the natural enemy of the wage earner — the monopolizer of the means of production and of raw materials.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

