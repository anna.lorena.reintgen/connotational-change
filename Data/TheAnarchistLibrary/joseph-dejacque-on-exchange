
“Be then frankly an entire anarchist and not a quarter anarchist, an eighth anarchist, or one-sixteenth anarchist, as one is a one-fourth, one-eighth or one-sixteenth partner in trade. Go beyond the abolition of contract to the abolition not only of the sword and of capital, but also of property and of authority in all its forms. Then you will have arrived at the anarchist community; that is to say, the social state where each one is free to produce or consume according to his will or his fancy without controlling, or being controlled by any other person whatever; where the balance of production and consumption is established naturally, no longer by the restrictive laws and arbitrary force of others, but in the free exercise of industry prompted by the needs and desires of each individual. The sea of humanity needs no dikes. Give its tides full sweep and each day they will find their level.”

(The Human Being, Letter to P.-J. Proudhon.)

Exchange, like all things, can be considered from three perspectives: the past, the present, and the future.

In the past, those who would gather the scattered products of industry and agriculture in a bazaar, the merchants who would spread under a portico what they called their merchandise, would thus engage, to a certain degree, in exchange. Today, we call this commerce, which is to say parasitism, and we are right to do so. For if, relative to the state of places and minds, they had been of some use in their time, in our own time those who keep shops have not the same excuses for continuing to live at the expense of the producers and consumers. The trader is purely and simply a legal thief. In a district of the city, for example, where just one bazaar would be sufficient, and where a few hundred employees could easily provide the service, there exist perhaps a thousand shops and six thousand, or even ten thousand, owners or clerks. To the extent that there are more intermediaries than those hundreds strictly necessary to meet the needs of exchange, there are parasites, thieves. And now, if we consider how much labor these shops have cost, how much manpower and materials have thus been diverted from their true destination, let us judge the quantity of production squandered daily to satisfy the appetites of that rapacious and pedantic bourgeoisie, a caste of monopolists and mercenaries destined by collegiate education and paternal tradition for the noble mission of salesman, civil service brats, practiced from infancy in the handling of coins, raised with a love of plunder. The character of commerce is not debatable: it is organized pillage. It legally robs both those who produce and those who consume.

The shopkeeper—at wholesale, wholesale to the public, or retail—is not the only intermediary between the producer and consumer. That triple usury only fastens itself to their flanks in the last instance.

The producer who does not have in their possession the instruments of labor (and that is the majority, if not the totality), is also exploited by another sort of parasite—the industrialist—the head of the factory and his clerical staff, to say nothing of the banker and his assistants, fed by the manufacturer, and consequently fed by the worker, since nothing productive is done except by the worker’s hands, and since everything done by those hands passes under control of the owner. In exchange for the instruments of labor the workers delivers their labor to the master and receive a wage from him. They give the master an apple to eat, so that the master will leave them the seeds. What a curious compensation! What a laughable exchange! It is the same for the peasant with regard to the landlord, for the proletarian with regard to the proprietor. The proletarians have built the house; the masons, carpenters, roofers, joiners, locksmiths, painters, to say nothing of the quarry-workers, lumberjacks, miners, foundry workers and smiths, potters and glass-blowers, all those who work the earth, the sand and stone, the wood and iron have labored there. It is they who have made the house, from the foundations to the roof’s peak. Well! To live there, even in the attic, they still must pay an odious, quarterly tribute, house-rent, to the fortunate lazy-bones who holds the property. All these proprietors, these landlords, these factory bosses and their clerical personnel, their superiors, the bankers, and the budgetary bureaucracies, all these are so many swarms of locusts who swoop down on the harvest of the towns and the countryside, and devour the wheat while it is green, the bread before it is cooked. Thieves! Thieves! Thieves!

And yet all these vampires are within the law, these rogues are honest people! Will you rely then on official qualifications?

Such is exchange, as the reactionaries understand it, otherwise known as commerce, or exploitation, or theft. It is exchange in civilization, in its barbarity, in its primitive savagery, exchange in its original arbitrariness, exchange by divine right, commerce in its absolute despotism.

At the present time,—not in fact, since commerce, exploitation, and theft always have legal force, but as an idea,—exchange is understood differently.

The uselessness of the owner and shopkeeper once recognized, we say to ourselves: everything that is useless is dangerous, and what is dangerous should be suppressed; the intermediary must disappear. Parasitism, like the barren fig tree, is condemned by the masses to be cast in the revolutionary inferno to be destroyed. “That which does not produce is unworthy of life.” The idea of justice, growing more prominent in public opinion, has expressed exchange thus: the right to the possession of the instruments of labor, that is, to free credit; and the right to the possession of the fruits of their labor, that is the democratization of property, universal and direct commerce,—a formula for social transition which in the political order corresponds to this: the right to the instruments of government, that is, democratization of government, universal and direct legislation.

Commerce and government thus understood,—commerce, as direct exchange, and government, as direct legislation—is a transitory organization which preserves the tradition of the past, while letting the future begin to speak. As soon as we could apply this organization, that is, as soon as we want it, our society, which declines today in misery and slavery, amidst bundles of sticks and piles of coins, will immediately enter into an ascending phase of wealth and liberty. The mark of authoritarian prejudice, the stain of propertarianism and legalism will be little by little wiped from the human brain; intellectual and moral exercise will develop the anarchist sentiment in the individual; industrial and legislative exercise will develop the sentiments of social community and individual liberty in society.

In beginning this article, I only wanted to speak of exchange, and I have been led to also speak of government. It was the least that I could do. Indeed, if contract is the law between the laborers, law is the contract between the people. A national or departmental or communal administration should no more make laws than an agricultural or industrial administration should make contracts. It is the business of all the laborers in the group to contract among themselves and with others, as legislation is a matter for all the inhabitants of a commune or nation. The administration, whether agrico-industrial, or communal, or national, does not command, but obeys. The administration is the delegate; the group of laborers or inhabitants is the master—and doesn’t the master always have the right to stop the wages and immediately dismiss the agent who fulfills their functions poorly?

Without doubt, conventional right, contract and law, even universally and directly exercised, is not natural right, or justice. It is a compromise between anarchy and authority, and everything that is not completely just is injustice. Direct exchange, that reform introduced into popular thought by Proudhon, is still a halfway measure. It is an addition of capacities, the diversification of the commercial census. However, we require not only the absolute overthrow of commerce that we require, but also the overthrow of constitutional or contractual commerce. We require, with regard to productive and consumptive circulation, the declaration of the individual rights of the human being, and the proclamation of the commonwealth, the res publica, that is, the freedom of production and consumption accorded to every individual with regard to the unity and universality of capital.

Nonetheless, a change similar to that which direct-exchange would produce would be a great social improvement, towards which all laborers should strive today. All their efforts should be directed towards this point, and we will arrive there before long, I hope. But in the end, that point is not the goal, that progress is not justice. It is only a stage on the best route, a step made in the direction of justice. We can relax and refresh ourselves there for a moment; but it would be dangerous to sleep there. In revolution it is necessary to double or triple the stages; we must gain ground on the enemy, if we want to escape their pursuit and instead track them down. The point farthest from the past, passing through the present, that is the point that we must try to reach. Abandoning commerce to enter into direct-exchange, we must push all the way to natural-exchange, the negation of property; moving from governmental authority to direct legislation, we must push all the way to anarchy, the negation of legalism.

By natural exchange I mean the unlimited liberty of all production and all consumption; the abolition of every sign of property, whether agricultural, industrial, artistic or scientific; the destruction of all individual monopolization of the products of labor; the demonarchization and demonetization of manual and intellectual capital, as well as instrumental, commercial and monumental capital. Every individual capital is usurious. It is a hindrance to circulation; and everything that hinders circulation hinders production and consumption. All of that is to be destroyed, and the representative sign as well: it accounts for the arbitrariness in exchange, as well as in government.

In mechanics, we almost always proceed from the simple to the composite, and then from the composite to the simple. One man discovers the lever, a simple instrument, endowed with a certain power. Others come who take hold of it, and in their turn make of it a more complicated device. They add wheels and gears, and they increase its power tenfold. However, continual frictions occur which are detrimental to the operation of this mechanism. One overloads it with other wheels and gears; one obtains results that appear more satisfactory, but always very imperfect, and above all small in relation to the care and labors spent on the improvement. Then there comes another engineer, free from the spirit of routine and having in his head the idea for a new motor; experiment has shown to him that an old mechanism overloaded with complications will not be repaired; that it must be replaced by simplifying it; and having cast down this malformed thing,—which drags along its blade on the edge of a ditch whose flow, exhausted at its source, no longer feeds it sufficiently,—he reconstructs on entirely new plans a considerably simplified machine, driven by steam or electricity, which functions this time without loss of force and produces a hundred times what was produced by the old apparatus.

It is the same for the social organism. Primitive commerce has been the lever, the simple and artless instrument of circulation; production and consumption have received an initial impetus. Today, it is an old mechanism which disgraces progress, which has, between its gears of metal, ground up enough (more than enough) of the laborers, of whose sweat and blood and tears it is the expression. Innumerable modifications, each more complicated and more monstrous than the others, have been supplied; and still it isn’t worth a thousandth part of what it has cost the proletarian. This is ruinous for the producer as well as for the consumer.

Direct-exchange, the possession by the laborer of the products of his labor, will certainly change the face of things and accelerate in considerable proportion the movement of production and consumption, and thus it will increase the amount of individual and social well-being. But numberless upsets will still take place, and circulation will not always be free, and without the liberty of circulation there is no liberty of production, no liberty of consumption.

Once more there will be progress, but not justice. An evolution is not a revolution.

In principle, should the laborers have the produce of their labor?

I do not hesitate to say: No! although I know that a multitude of workers will cry out.

Look, proletarians, cry out, shout as much as you like, but then listen to me:

No, it is not the product of their labors to which the workers have a right. It is the satisfaction of their needs, whatever the nature of those needs.

To have the possession of the product of our labor is not to have possession of that which is proper to us, it is to have property in a product made by our hands, and which could be proper to others and not to us. And isn’t all property theft?

For example, suppose there is a tailor, or a cobbler. He has produced several garments or several pairs of shoes. He cannot consume them all at once. Perhaps, moreover, they are not in his size or to his taste. Obviously he has only made them because it is his occupation to do so, and with an eye to exchanging them for other products for which he feels the need; and so it is with all the workers. Those garments or shoes are thus not his possessions, as he has no personal use for them; but they are property, a value that he hoards and which he can dispose of at his own good pleasure, that he can destroy if it pleases him, and which he can at least use or misuse as he wishes; it is, in any case, a weapon for attacking the property of others, in that struggle of divided and antagonistic interests where each is delivered up to all the chances and all the hazards of war.

In addition, is this laborer well justified, in terms of right and justice, in declaring himself the sole producer of the labor accomplished by his hands? Has he created something from nothing? Is he omnipotent? Does he possess the manual and intellectual learning of all eternity? Is his art and craft innate to him? Did the worker come fully equipped from his mother’s womb? Is he a self-made man, the son of his own works? Isn’t he in part the work of his forebears, and the work of his contemporaries? All those who have shown him how to handle the needle and the scissors, the knife and awl, who have initiated him from apprenticeship to apprenticeship, to the degree of skill that he has attained, don’t all these have some right to a part of his product? Haven’t the successive innovations of previous generations also played some part in his production? Does he owe nothing to the present generation? Does he owe nothing to future generations? Is it justice to combine thus in his hands the titles of all these accumulated labors, and to appropriate their profits exclusively to him?

If one admits the principle of property in the product for the laborer (and, make no mistake, it really is a property, and not a possession, as I have just demonstrated), property becomes, it is true, more accessible to each, without being for that better assured to all. Property is inequality, and inequality is privilege; it is servitude. As any product will be more or less in demand, its producer will be more or less harmed, more or less profited. The property of one can only increase to the detriment of the property of the other, property necessitates exploiters and exploited. With the property of the product of labor, property democratized, there will no longer be the exploitation of the great number by the smallest minority, as with property of labor by capital, property monarchized; but there will still be exploitation of the smaller number by the larger. There will always be iniquity, divided interests, hostile competition, with disasters for some and success for the others. Without doubt these reversals and triumphs will not be at all comparable to the miseries and scandalous fortunes which insult social progress in our time. However, the heart of humanity will still be torn by fratricidal struggles which, for being less terrible, will not be less detrimental to individual well-being, to well-being in general.

Property is not only inequality, it is also immorality. Some producer favored with a lucrative specialty could, in their prosperity, use their daily earnings as an excuse to distract from their work a woman (if he is a man), or a man (if she is a woman), and infect them with the virus of idleness, the contagious germ of physical and moral degradation, the result of prostitution. All the vices, all the depravations, all the pestilential exhalations are contained in that substantive hieroglyphic, a case that is only a coffin, a mummy from ancient civilizations, which has arrived in our time carried by the tides of commerce, by centuries of usury,—property!

Thus let us accept direct-exchange, like direct legislation, only conditionally, as an instrument of transition, as a link between the past and the future. It is a question to present, an operation to accomplish; but let that operation be like the welding of a transpresent cable with one end touching the continent of the old abuses, but whose other end unwinds towards a new world, the world of free harmony.

Liberty is Liberty: let us be its prophets, all of us who are visionaries. On the day when we will understand that the social organism must not be modified by overloading it with complications, but by simplifying it; the day when it will no longer be a question of demolishing on thing in order to replace it by its fellow, by denominating and multiplying it, on that day we will have destroyed, from top to bottom, the old authoritarian and propertarian mechanism, and recognized the insufficiency and harmfulness of individual contract as well as the social contract. Natural government and natural exchange,—natural government, which is the government of individuals by individuals, of themselves by themselves, universal individualism, the human self [moi-humain] moving freely in the humanitary whole [tout-humanité]; and natural exchange, which is individuals exchanging of themselves with themselves, being at once producers and consumers, co-workers and co-inheritors of social capital, human liberty, infinitely divisible liberty, in the community of goods, in indivisible property. On that day, I say, of natural government and natural exchange, an organism driven by attraction and solidarity will rise up, majestic and beneficent, in the heart of regenerated humanity. And authoritarian and propertarian government, authoritarian and propertarian exchange, machineries overburdened with intermediaries and representative signs, will collapse, solitary and abandoned, in the dried-up course of the flood of ancient arbitrariness.

So let all these Babylonian institutions perish quickly, with their unnatural wheels and gears, and on their ruins let the universal and fraternal solidarization of individual interests, society according to nature, be enthroned forever!

People of the present, it is necessary to choose. Not only is it immoral and cowardly to remain neutral, it is degrading, but still there is peril. It is absolutely necessary to takes sides for or against the two great, exclusive principles that the world debates. Your salvation is at stake. Either progress or devolution! Autocracy or anarchy!—For a radically flawed society, radical solutions are required: for large evils, grand remedies!

Choose then:

—Property is the negation of liberty.

—Liberty is the negation of property.

—Social slavery and individual property, this is what authority affirms.

—Individual liberty and social property, that is the affirmation of anarchy.

People of progress, martyred by authority, choose anarchy!

[Working translation by Shawn P. Wilbur; revised 2/28/2012]




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“Be then frankly an entire anarchist and not a quarter anarchist, an eighth anarchist, or one-sixteenth anarchist, as one is a one-fourth, one-eighth or one-sixteenth partner in trade. Go beyond the abolition of contract to the abolition not only of the sword and of capital, but also of property and of authority in all its forms. Then you will have arrived at the anarchist community; that is to say, the social state where each one is free to produce or consume according to his will or his fancy without controlling, or being controlled by any other person whatever; where the balance of production and consumption is established naturally, no longer by the restrictive laws and arbitrary force of others, but in the free exercise of industry prompted by the needs and desires of each individual. The sea of humanity needs no dikes. Give its tides full sweep and each day they will find their level.”

