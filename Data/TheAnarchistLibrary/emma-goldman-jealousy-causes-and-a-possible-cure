
No one at all capable of an intense conscious inner life need ever hope to escape mental anguish and suffering. Sorrow and often despair over the so-called eternal fitness of things are the most persistent companions of our life. But they do not come upon us from the outside, through the evil deeds of particularly evil people. They are conditioned in our very being; indeed, they are interwoven through a thousand tender and coarse threads with our existence.

It is absolutely necessary that we realize this fact, because people who never get away from the notion that their misfortune is due to the wickedness of their fellows never can outgrow the petty hatred and malice which constantly blames, condemns, and hounds others for something that is inevitable as part of themselves. Such people will not rise to the lofty heights of the true humanitarian to whom good and evil, moral and immoral, are but limited terms for the inner play of human emotions upon the human sea of life.

The “beyond good and evil” philosopher, Nietzsche, is at present denounced as the perpetrator of national hatred and machine gun destruction; but only bad readers and bad pupils interpret him so. “Beyond good and evil” means beyond prosecution, beyond judging, beyond killing, etc. Beyond Good and Evil opens before our eyes a vista the background of which is individual assertion combined with the understanding of all others who are unlike ourselves, who are different.

By that I do not mean the clumsy attempt of democracy to regular the complexities of human character by means of external equality. The vision of “beyond good and evil” points to the right to oneself, to one’s personality. Such possibilities do not exclude pain over the chaos of life, but they do exclude the puritanic righteousness that sits in judgment on all others except oneself.

It is self-evident that the thoroughgoing radical — there are many half-baked ones, you know — must apply this deep, humane recognition to the sex and love relation. Sex emotions and love are among the most intimate, the most intense and sensitive, expressions of our being. They are so deeply related to individual physical and psychic traits as to stamp each love affair an independent affair, unlike any other love affair. In other words, each love is the result of the impressions and characteristics the two people involved give to it. Every love relation should by its very nature remain an absolutely private affair. Neither the State, the Church, morality, or people should meddle with it.

Unfortunately this is not the case. The most intimate relation is subject to proscriptions, regulations, and coercions, yet these external factors are absolutely alien to love, and as such lead to everlasting contradictions and conflict between love and law.

The result of it is that our love life is merged into corruption and degradation. “Pure love,” so much hailed by the poets, is in the present matrimonial, divorce, and alienation wrangles, a rare specimen indeed. With money, social standing, and position as the criteria for love, prostitution is quite inevitable, even if it be covered with the mantle of legitimacy and morality.

The most prevalent evil of our mutilated love-life is jealousy, often described as the “green-eyed monster” who lies, cheats, betrays, and kills. The popular notion is that jealousy is inborn and therefore can never be eradicated from the human heart. This idea is a convenient excuse for those who lack ability and willingness to delve into cause and effect.

Anguish over a lost love, over the broken thread of love’s continuity, is indeed inherent in our very beings. Emotional sorrow has inspired many sublime lyrics, much profound insight and poetic exultation of a Byron, Shelley, Heine, and their kind. But will anyone compare this grief with what commonly passes as jealousy? They are as unlike as wisdom and stupidity. As refinement and coarseness. As dignity and brutal coercion. Jealousy is the very reverse of understanding, of sympathy, and of generous feeling. Never has jealousy added to character, never does it make the individual big and fine. What it really does is to make him blind with fury, petty with suspicion, and harsh with envy.

Jealousy, the contortions of which we see in the matrimonial tragedies and comedies, is invariably a one-sided, bigoted accuser, convinced of his own righteousness and the meanness, cruelty, and guilt of his victim. Jealousy does not even attempt to understand. Its one desire is to punish, and to punish as severely as possible. This notion is embodied in the code of honor, as represented in dueling or the unwritten law. A code which will have it that the seduction of a woman must be atoned with the death of the seducer. Even where seduction has not taken place, where both have voluntarily yielded to the innermost urge, honor is restored only when blood has been shed, either that of the man or the woman.

Jealousy is obsessed by the sense of possession and vengeance. It is quite in accord with all other punitive laws upon the statutes which still adhere to the barbarous notion that an offence, often merely the result of social wrongs, must be adequately punished or revenged.

A very strong argument against jealousy is to be found in the data of historians like Morgan, Reclus, and others, as to the sex relations among primitive people. Anyone at all conversant with their works knows that monogamy is a much later sex from which came into being as a result of the domestication and ownership of women, and which created sex monopoly and the inevitable feeling of jealousy.

In the past, when men and women intermingled freely without interference of law and morality, there could be no jealousy, because the latter rests upon the assumption that a certain man has an exclusive sex monopoly over a certain woman and vice-versa. The moment anyone dates to trespass this sacred precept, jealousy is up in arms. Under such circumstances it is ridiculous to say that jealousy is perfectly natural. As a matter of fact, it is the artificial result of an artificial cause, nothing else.

Unfortunately, it is not only conservative marriages which are saturated with the notion of sex monopoly; the so-called free unions are also victims of it. The argument may be raised that this is one more proof that jealousy is an inborn trait. But it must be borne in mind that sex monopoly has been handed down from generation to generation as a sacred right and the basis of purity of the family and the home. And just as the Church and the State accepted sex monopoly as the only security to the marriage tie, so have both justified jealousy as the legitimate weapon of defense for the protection of the property right.

Now, while it is true that a great many people have outgrown the legality of sex monopoly, they have not outgrown its traditions and habits. Therefore they become as blinded by the “green-eyed monster” as their conservative neighbors the moment their possessions are at stake.

A man or woman free and big enough not to interfere or fuss over the outside attractions of the loved one is sure to be despised by his conservative, and ridiculed by his radical, friends. He will either be decried as a degenerate or a coward; often enough some petty material motives will be imputed to him. In any even, such men and women will be the target of coarse gossip or filthy jokes for no other reason than that they concede to wife, husband or lovers the right to their own bodies and their emotional expression, without making jealous scenes or wild threats to kill the intruder.

There are other factors in jealousy: the conceit of the male and the envy of the female. The male in matters sexual is an imposter, a braggart, who forever boasts of his exploits and success with women. He insists on playing the part of a conqueror, since he has been told that women want to be conquered, that they love to be seduced. Feeling himself the only cock in the barnyard, or the bull who must clash horns in order to win the cow, he feels mortally wounded in his conceit and arrogance the moment a rival appears on the scene — the scene, even among so-called refined men, continues to be woman’s sex love, which must belong to only one master.

In other words, the endangered sex monopoly together with man’s outraged vanity in ninety-nine cases out of a hundred are the antecedents of jealousy.

In the case of a woman, economic fear for herself and children and her petty envy of every other woman who gains grace in the eyes of her supporter invariably create jealousy. In justice to women be it said that for centuries past, physical attraction was her only stock in trade, therefore she must needs become envious of the charm and value of other women as threatening her hold upon her precious property.

The grotesque aspect of the whole matter is that men and women often grow violently jealous of those they really do not care much about. It is therefore not their outraged love, but their outraged conceit and envy which cry out against this “terrible wrong.” Likely as not the woman never loved the man whom she now suspects and spies upon. Likely as not she never made an effort to keep his love. But the moment a competitor arrives, she begins to value her sex property for the defense of which no means are too despicable or cruel.

Obviously, then, jealousy is not the result of love. In fact, if it were possible to investigate most cases of jealousy, it would likely be found that the less people are imbued with a great love the more violent and contemptible is their jealousy. Two people bound by inner harmony and oneness are not afraid to impair their mutual confidence and security if one or the other has outside attractions, nor will their relations end in vile enmity, as is too often the case with many people. They many not be able, nor ought they to be expected, to receive the choice of the loved one into the intimacy of their lives, but that does not give either one the right to deny the necessity of the attraction.

As I shall discuss variety and monogamy two weeks from tonight, I will not dwell upon either here, except to say that to look upon people who can love more than one person as perverse or abnormal is to be very ignorant indeed. I have already discussed a number of causes for jealousy to which I must add the institution of marriage which the State and Church proclaim as “the bond until death doth part.” This is accepted as the ethical mode of right living and right doing.

With love, in all its variability and changeability, fettered and cramped, it is small wonder if jealousy arises out of it. What else but pettiness, meanness, suspicion, and rancor can come when a man and wife are officially held together with the formula “from now on you are one in body and spirit.” Just take any couple tied together in such a manner, dependent upon each other for every thought and feeling, without an outside interest or desire, and ask yourself whether such a relation must not become hateful and unbearable in time.

In some form or other the fetters are broken, and as the circumstances which bring this about are usually low and degrading, it is hardly surprising that they bring into play the shabbiest and meanest human traits and motives.

In other words, legal, religious, and moral interference are the parents of our present unnatural love and sex life, and out of it jealousy has grown. It is the lash which whips and tortures poor mortals because of their stupidity, ignorance, and prejudice.

But no one need attempt to justify himself on the ground of being a victim of these conditions. It is only too true that we all smart under the burdens of iniquitous social arrangements, under coercion and moral blindness. But are we not conscious individuals, whose aim it is to bring truth and justice into human affairs? The theory that man is a product of conditions has led only to indifference and to a sluggish acquiescence in these conditions. Yet everyone knows that adaptation to an unhealthy and unjust mode of life only strengthens both, while man, the so-called crown of all creation, equipped with a capacity to think and see and above all to employ his powers of initiative, grows ever weaker, more passive, more fatalistic.

There is nothing more terrible and fatal than to dig into the vitals of one’s loved ones and oneself. It can only help to tear whatever slender threads of affection still inhere in the relation and finally bring us to the last ditch, which jealousy attempts to prevent, namely, the annihilation of love, friendship and respect.

Jealousy is indeed a poor medium to secure love, but it is a secure medium to destroy one’s self-respect. For jealous people, like dope-fiends, stoop to the lowest level and in the end inspire only disgust and loathing.

Anguish over the loss of love or a nonreciprocated love among people who are capable of high and fine thoughts will never make a person coarse. Those who are sensitive and fine have only to ask themselves whether they can tolerate any obligatory relation, and an emphatic no would be the reply. But most people continue to live near each other although they have long ceased to live with each other — a life fertile enough for the operation of jealousy, whose methods go all the way from opening private correspondence to murder. Compared with such horrors, open adultery seems an act of courage and liberation.

A strong shield against the vulgarity of jealousy is that man and wife are not of one body and one spirit. They are two human beings, of different temperament, feelings, and emotions. Each is a small cosmos in himself, engrossed in his own thoughts and ideas. It is glorious and poetic if these two worlds meet in freedom and equality. Even if this lasts but a short time it is already worthwhile. But, the moment the two worlds are forced together all the beauty and fragrance ceases and nothing but dead leaves remain. Whoever grasps this truism will consider jealousy beneath him and will not permit it to hang as a sword of Damocles over him.

All lovers do well to leave the doors of their love wide open. When love can go and come without fear of meeting a watch-dog, jealousy will rarely take root because it will soon learn that where there are no locks and keys there is no place for suspicion and distrust, two elements upon which jealousy thrives and prospers.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

