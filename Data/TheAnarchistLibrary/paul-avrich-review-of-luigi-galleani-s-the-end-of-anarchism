
The career of Luigi Galleani involves a paradox. During the first two decades of the twentieth century, he was the leading Italian anarchist in the United States, one of the greatest anarchist orators of his time, in a class with Emma Goldman and Johann Most, editor of the foremost Italian-American anarchist periodical, La Cronaca Sovversiva (The Subversive Chronicle), which ran for fifteen years before its suppression by the American government, and inspirer of a movement that included Sacco and Vanzetti among its adherents.

Yet Galleani has fallen into oblivion. He is virtually unknown in the United States, outside of a small circle of scholars and of personal associates and disciples, whose ranks are rapidly dwindling. No biography in English has been devoted to him, nor is he mentioned in the general histories of anarchism by George Woodcock and James Joll or in the comprehensive history of American anarchism by William Reichert. His writings, moreover, had remained untranslated until the appearance of the work under review, which, distilling the essence of his radical beliefs, his credo of revolutionary anarchism, fills a conspicuous gap in the literature of anarchism available to English readers and restores a major figure in the movement to his proper historical place.

Galleani was born on August 12, 1861, in the Piedmont town of Vercelli, not far from the city of Turin. The son of middle-class parents, he was drawn to anarchism in his late teens and, studying law at the University of Turin, became an outspoken militant whose hatred of capitalism and government would burn with undiminished intensity for the rest of his life. Galleani, however, refused to practice law, which he had come to regard with contempt, transferring his talents and energies to radical propaganda. Under threat of prosecution, he took refuge in France, from which he was expelled for taking part in a May Day demonstration. Moving to Switzerland, he visited the exiled French anarchist and geographer Elisée Reclus, whom he assisted in the preparation of his Nouvelle Géographie universelle, compiling statistics on Central America. He also assisted students at the University of Geneva in arranging a celebration in honor of the Haymarket Martyrs, who had been hanged in Chicago in 1887, for which he was expelled as a dangerous agitator. Returning to Italy, Galleani continued his agitation, which got him into trouble with the police. Arrested on charges of conspiracy, he spent more than five years in prison and exile before escaping from the island of Pantelleria, off the coast of Sicily, in 1900.

Galleani, now in his fortieth year, began an odyssey that landed him in North America. Aided by Elisée Reclus and other comrades, he first made his way to Egypt, where he lived for the better part of a year among a colony of Italian expatriates. Threatened with extradition, he moved on to London, from which he soon embarked for the United States, arriving in October 1901, barely a month after the assassination of President McKinley. Settling in Paterson, New Jersey, a stronghold of the immigrant anarchist movement, Galleani assumed the editorship of La Questione Sociale — The Social Question, then the leading Italian anarchist periodical in America. Scarcely had he installed himself in this position when a strike erupted among the Paterson silk workers, and Galleani, braving the anti-radical hysteria which followed the shooting of McKinley, threw all his energies into their cause. In eloquent and fiery speeches he called on the workers to launch a general strike and thereby free themselves from capitalist oppression. Paul Ghio, a visitor from France, was present at one such oration. “I have never heard an orator more powerful than Luigi Galleani,” he afterwards wrote. “He has a marvelous facility with words, accompanied by the faculty — rare among popular tribunes — of precision and clarity of ideas. His voice is full of warmth, his glance alive and penetrating, his gestures of exceptional vigor and flawless distinction.”

The strike occurred in June 1902. Clashes took place between the workers and the police, shots were fired, and Galleani was wounded in the face. Indicted for inciting to riot, he managed to escape to Canada. A short time after, having recovered from his wounds, he secretly recrossed the border and took refuge in Barre, Vermont, living under an assumed name among his anarchist comrades who regarded him with intense devotion. It was in Barre, on June 6,1903, that Galleani launched La Cronaca Sovversiva, the mouthpiece for his incendiary doctrines and one of the most important and ably edited periodicals in the history of the anarchist movement, its influence, reaching far beyond the confines of the United States, could be felt wherever Italian radicals congregated, from Europe and North Africa to South America and Australia. In 1906, however, during a polemical exchange with G.M. Seratti, the socialist editor of Il Proletario in New York, the latter revealed Galleani’s whereabouts (a charge also leveled at the English writer H.G. Wells), and Galleani was taken into custody. Extradited to New Jersey, he was tried in Paterson in April 1907 for his role in the 1902 strike. The trial, however, ended in a hung jury (seven for conviction, five for acquittal), and Galleani was set free.

Galleani returned to Barre and resumed his propaganda activities. Now in his late forties, he had reached the summit of his intellectual powers. Over the next forty years his fiery oratory and brilliant pen carried him to a position of undisputed leadership within the Italian-American anarchist movement. An eloquent speaker, Galleani had a resonant, lilting voice with a tremolo that kept his audience spellbound. He spoke easily, powerfully, spontaneously, and his bearing was of a kind that made his followers, Sacco and Vanzetti among them, revere him as a kind of patriarch of the movement, to which he won more converts than any other single individual. Galleani was also a prolific writer, pouring forth hundreds of articles, essays, and pamphlets that reached tens, perhaps hundreds, of thousands of readers on several continents. Yet he never produced a full-length book: the volumes appearing over his signature, such as Faccia a Faccia coi Nemico, Aneliti e Singulti, and Figure e Figuri, are collections of shorter pieces previously published in La Cronaca Sovversiva. In this respect he resembles Johann Most, Errico Malatesta, and Benjamin Tucker (author of Instead of a Book: By a Man Too Busy to Write One), rather than, say, William Godwin, Pierre-Joseph Proudhon, or Peter Kropotkin.

The End of Anarchism?, Galleani’s most fully realized work, itself began as a series of articles. In June 1907, shortly after Galleani’s acquittal at Paterson, the Turin daily La Stampa published an interview with Francesco Saverio Merlino, himself a former anarchist of distinction, under the title “The End of Anarchism.” Merlino, like Galleani, had been trained in the law, had lived in the United States, and had founded an important Italian-American journal, Grido degti Oppressi (The Cry of the Oppressed), which appeared in New York from 1892 to 1894. Unlike Galleani, however, Merlino had abandoned anarchism in 1897, joining the socialist movement. Merlino, in his interview with La Stampa, pronounced anarchism an obsolete doctrine, torn by internal disputes, bereft of first-rate theorists, and doomed to early extinction. Galleani was incensed. “The end of anarchism?” he asked in La Cronaca Sovversiva, adding a question mark to the title of Merlino’s interview. Just the opposite was the case. In an age of growing political and economic centralization, anarchism was more relevant than ever. Far from being moribund, “it lives, it develops, it goes forward”.

Such was Galleani’s reply to Merlino, elaborated in a series of articles into Cronaca Sovversiva from August 17,1907, to January 25,1908. Combining the spirit of Stirnerite insurgency with Kropotkin’s principle of mutual aid, Galleani put forward a vigorous defense of communist anarchism against socialism and reform, preaching the virtues of spontaneity and variety, of autonomy and independence, of self-determination and direct action, in a world of increasing standardization and conformity. A revolutionary zealot, he would brook no compromise with the elimination of both capitalism and government. Nothing less than a clean sweep of the bourgeois order, with its inequality and injustice, its subjugation and degradation of the workers, would satisfy his thirst for the millennium.

Galleani produced ten articles in response to Merlino. He intended to write still more, but day-to-day work for the movement — editing La Cronaca Sovversiva, organizing meetings, issuing pamphlets, embarking on coast-to-coast lecture tours — prevented him from doing so. In 1912 he moved La Cronaca Sovversiva from Barre to Lynn, Massachusetts, where he had won a dedicated following.

When the First World War broke out in 1914, he opposed it, in contrast to Kropotkin, with all the strength and eloquence at his command, denouncing it in La Cronaca Sovversiva with an oft-repeated slogan, “Contro la guerra, contro la pace, per la rivoluzione sociale!” (Against the war, against the peace, for the social revolution!) With America’s entry into the conflict in April 1917, Galleani became the object of persecution. His paper was shut down and he himself was arrested on charges of obstructing the war effort. On June 24, 1919 he was deported to his native Italy, leaving behind his wife and four children.

In Turin, Galleani resumed publication of La Cronaca Sovversiva. As in America, however, it was suppressed by the authorities. On Mussolini’s accession to power in 1922, Galleani was arrested, tried, and convicted on charges of sedition, and sentenced to fourteen months in prison, where his health began to deteriorate. After his release, he returned to his old polemic against Merlino, completing it in a series of articles in L’Adunata dei Refrattari (The Call of the Rebels), the journal of his disciples in America, who issued it in 1925 as a booklet. Malatesta, whose conception of anarchism diverged sharply from that of Galleani, hailed the work as a “clear, serene, eloquent” recital of the communist-anarchist creed. In its present English edition, it takes its place beside Malatesta’s own Talk About Anarchist Communism, Alexander Berkman’s What Is Communist Anarchism?, and Nicolas Waiter’s About Anarchism as a classic exposition of the subjects.

It is a pleasant task, in this age of shoddy production, to review a work of such notable aesthetic quality. Apart from its handsome cover by Flavio Costantini, the celebrated Italian anarchist writer, it is attractively designed and printed, and the frontispiece contains a drawing of Galleani, based on a well-known photograph, by Bartolo Provo. The translation by Max Sartin, longtime editor of L’Adunata dei Refrattari and associate of Galleani, and Robert D’Attilio, an authority on Italian-American anarchism, is both readable and accurate. There are a number of typographical and factual errors, especially in the notes, but these, while regrettable, do not detract from the overall value of the book.

The publication of the L’Adunata edition of this work in 1925 did not endear Galleani to the Mussolini government. Arrested in November 1926, Galleani was locked up in the same cell in which he had spent three months in 1892 and found it “as dirty and ugly” as before. Soon afterwards, he was banished to the island of Lipari, off the Sicilian coast, from which he was later removed to Messina, and condemned to serve six months in prison, for the crime of insulting Mussolini. In February 1930. Galleani, in failing health, was allowed to return to the mainland. Retiring to the mountain village of Caprigliola, he remained under the surveillance of the police, who seldom left his door and followed him even on his solitary walks in the surrounding countryside. Returning from his daily walk, on November 4,1931, Galleani collapsed and died. His anarchism, to the end, had burned with an undiminished flame. Ever hopeful for the future, despite a life of bitter experience, he had remained faithful to the ideal which had inspired his life, convinced that liberty would ultimately triumph over tyranny and oppression.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

