      Developing the network of Social Centres
Social centres have increasingly become an integral part of anarchist and anti-capitalist activity in the UK. At present there are around fifteen such places which operate as public, political and social centres. Some were formed from situations way back in the early eighties, like Bradford’s 1 in 12, while others came into being recently, through the anti-G8 mobilisations in Scotland (2005). In London there has been a very active “push” for social centres largely developed on the initiative of the anarchist collective WOMBLES starting in 2002. Squatting has always been associated with radical politics and there has been a long history of occupied political spaces mainly functioning as “squat cafes” and other resource centres. There has, however, been an attempt to move away from the “squatter” image of these places and move towards a more engaging aesthetic based on experiences from around Europe and especially Italy. The ideas which have developed around occupying private space and turning them into political and cultural hubs has come through the experimenting and experiences of those involved. A certain genealogy of social centres in London has been formed over the last few years, to include the Radical Dairy (Stoke Newington), Occupied Social Centre (Kentish Town), Ex-GrandBanks (Tufnell Park), Institute For Autonomy (Bloomsbury), The Square (Bloomsbury) and most recently, the Vortex (Stoke Newington). Despite the unavoidably short life span of each of these projects, knowledge and experience have been built upon and mistakes, on the whole, being learnt from.

What social centre projects have managed to do in a relatively short time span is to intensify the political activation and the scope of interaction of those that dwell through them: Thousands of people have passed through social centres attending hundreds of film showings, discussions, events, concerts and cultural events. Presence, in most cases, is guaranteed. If we build it they will come and if we present ourselves as open, inviting and our spaces as clean and accessible, the diversity of people quickly expands. Almost gone are the days of the pissed up punk drinking special brew whilst his/her stereotyped dreadlocked brethren roles another joint. In come mother and baby groups, packed out cinemas, good quality food, well organised concerts and political mobilisations. This consistency becomes easier as more people become involved, not looking for a subculture to indulge in, but a place of social interaction that presents and communicates ideas. With every occupation there is a willingness to go beyond the limitations of the last, to attempt to answer the critiques or lack of radicalisation that certain activities contain. This dynamic of constant self-critique and analysis becomes the driving force of developing new politics to face up to the changing nature of a society which is less concerned with anti-systemic change and more interested in surviving within the schemas of capitalism. To many, social centres become a first “port of call” — their first interaction with ordinary people who want to fully participate in reshaping and re-imaging their environment. Interaction with anarchists becomes normalised and barriers fall.

London is an odd place. Highly urbanised and commercialised, with thousands of interweaving communities, gentrified by the spoils of war — the immense financial capital that passes through it on a daily basis. Property prices have risen to historically high levels forcing many out to the urban periphery. Due to this upsurge in highly priced property and its commodification we see a terrain of struggle which has come to dictate and cascade through other areas of life. The control over property has become a major battleground even more so as the neo-liberal doctrine permeates all areas of “public” spaces and services. The mass sell-offs and takeovers have spurned conflicts not only in London but all over the UK. Gentrification has been the most widely used term and accurately describes a process of transformation based on the new material conditions generated both by the integration of telecommunications technologies within the economy and the break-down of the social democratic contract.

The idea of public and common space is fast being undermined as the limitless demand of profit takes over. Everyday experiences become increasingly mediated by our relationship to capital while our ability to impose our own desires and autonomy is increasingly undermined. With each generation, the struggles and defeats of the previous one are embodied and reflected within our social reality. Public spaces once existing and able to create elements of autonomy outside the market logic are now where the state surveys and controls — by use of surveillance cameras, privatisation, commercialisation and intrusions by the police.

There is nowhere that we can socialise and exist without being exploited or expected to participate in a certain level of capitalist consumption. Social need is constructed through the systemic denials of capitalist society. Our needs and the needs of capital diverge and therefore what we are offered leaves a lot to be desired, literally! Our needs are social in that they are part of a social fabric that makes us human. Alas, rather than being met by the economy, our needs are subservient to it, manipulated and directed into consumer demands and fashion trends. Our real needs become marginalised and shaped into commodified needs, readily equated with commodified products. Our alienation leads to increased uncertainties and insecurities reducing our potential for public participation.

Within this context of the social reality that we experience, occupation/expropriation becomes a choice in participating on our own terms. Self-organisation becomes a mode of inclusion, anti-hierarchy both a political rejection of the present order and a way to maximise the human potential that already exists. Anti-capitalist as a process of basing our real existence on individual and collective needs without the distortions for the abstract push for profit. These forms our “platform” to open up space in London.

In January 2007 the second nationwide gathering of social centres was held at Bradford’s 1in12. Around forty people from fifteen different collectives attended the meeting to discuss how the various spaces could connect and organise between each other. The discussions veered from the predictable “technical” discussions around “how we organise our small corner of the world”, to much wider, deeper discussions on why we need to do so. The “how” question has become a particularly annoying fetishisation and specialisation much seen in the UK activists’ “scene”: If we don’t know how, then we don’t know anything... but it’s the “why” which gives doing the “how” meaning, and it’s this meaning that we are trying to produce.

A project was unveiled, put together by the author of this piece in the form of an enquiry. This initial “taster” was in the form of a survey with questions attempting to gather some basic information about each social centre. The survey focussed on quantifying the scope of this embryonic movement. Social centres were asked how many people were involved in there collective, how many events are organised per month on average, how many visitors they get. Though a “guestimate”, I am sure there is constant monitoring of who turns up when and what is organised so I take these responses to be more legitimate than other similar reflections. The results show that between the fifteen spaces, there are around 350–400 people involved in social centres around the country — organising around 250 events per month and gaining the presence of 4,000 to 6,000 people. Not bad for a political minority! By making this data visible and presenting it back to those of us involved in such projects the aim is to expand the knowledge of what we do, and with whom. We have these resources, we have this presence, we need to transform it and develop it. It is up to us from that start point to attempt to strategise the future developments of social centres as a political project. Are we content on where we are? Is it enough? Ideology is dead, and with it the dogma of both the left and traditional anarchists. If we are to re-imagine and give meaning to revolutionary praxis in the 21st Century we would need reconnect with not just ourselves and others like us who oppose capitalism but also the multitude of people who are not satisfied with a private existence. Only through this process are we truly going to get to a level where we are asking the right questions, let alone providing the right answers.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

