
In memoriam Paul Goodman, 1911–1972

My novel The Dispossessed is about a small worldful of people who call themselves Odonians. The name is taken from the founder of their society, Odo, who lived several generations before the time of the novel, and who therefore doesn’t get into the action — except implicitly, in that all the action started with her.

Odonianism is anarchism. Not the bomb-in-the-pocket stuff, which is terrorism, whatever name it tries to dignify itself with; not the social-Darwinist economic “libertarianism” of the far right; but anarchism. as prefigured in early Taoist thought, and expounded by Shelley and Kropotkin, Goldman and Goodman. Anarchism’s principal target is the authoritarian State (capitalist or socialist); its principal moral-practical theme is cooperation (solidarity, mutual aid). It is the most idealistic, and to me the most interesting, of all political theories.

To embody it in a novel, which had not been done before, was a long and hard job for me, and absorbed me totally for many months. When it was done I felt lost exiled — a displaced person. I was very grateful, therefore, when Odo came out of the shadows and across the gulf of Probability, and wanted a story written, not about the world she made, but about herself.

This story is about one of the ones who walked away from Omelas.

 

The speaker’s voice was as loud as empty beer-trucks in a stone street, and the people at the meeting were jammed up close, cobblestones, that great voice booming over them. Taviri was somewhere on the other side of the hall. She had to get to him. She wormed and pushed her way among the dark-clothed, close-packed people. She did not hear the words, nor see the faces: only the booming, and the bodies pressed one behind the other. She could not see Taviri, she was too short. A broad black-vested belly and chest loomed up, blocking her way. She must get through to Taviri. Sweating, she jabbed fiercely with her fist. It was like hitting stone, he did not move at all, but the huge lungs let out right over her head a prodigious noise, a bellow.. She cowered. Then she understood that the bellow had not been at her. Others were shouting. The speaker had said something, something fine about taxes or shadows. Thrilled, she joined the shouting — “Yes! Yes!” — and shoving on, came out easily into the open expanse of the Regimental Drill Field in Parheo. Overhead the evening sky lay deep and colorless, and all around her nodded the tall weeds with dry, white, close-floreted heads. She had never known what they were called. The flowers nodded above her head, swaying in the wind that always blew across the fields in the dusk. She ran among them, and they whipped lithe aside and stood up again swaying, silent. Taviri stood among the tall weeds in his good suit, the dark grey one that made him look like a professor or a play-actor, harshly elegant. He did not look happy, but he was laughing, and saying something to her. The sound of his voice made her cry, and she reached out to catch hold of his hand, but she did not stop, quite. She could not stop. “Oh, Taviri,” she said, It’s just on there!” The queer sweet smell of the white weeds was heavy as she went on. There were thorns. tangles underfoot, there were slopes, pits. She feared to fall, to fall, she stopped.

Sun, bright morning-glare, straight in the eyes, relentless. She had forgotten to pull the blind last night. She turned her back on the sun, but the right side wasn’t comfortable. No use. Day. She sighed twice, sat up, got her legs over the edge of the bed, and sat hunched in her nightdress looking down at her feet.

The toes, compressed by a lifetime of cheap shoes, were almost square where they touched each other, and bulged out above in corns; the nails were discolored and shapeless. Between the knob-like anklebones ran fine, dry wrinkles. The brief little plain at the base of the toes had kept its delicacy, but the skin was the color of mud, and knotted veins crossed the instep. Disgusting. Sad, depressing. Mean. Pitiful. She tried on all the words, and they all fit, like hideous little hats. Hideous: yes, that one too. To look at oneself and find it hideous, what a job! But then, when she hadn’t been hideous, had she sat around and stared at herself like this? Not much! A proper body’s not an object, not an implement, not a belonging to be admired, it’s just you, yourself. Only when it’s no longer you. but yours, a thing owned, do you worry about it — Is it in good shape? Will it do? Will it last?

“Who cares” said Laia fiercely, and stood up.

It made her giddy to stand up suddenly. She had to put out her hand to the bed-table, for she dreaded falling. At that she thought of reaching out to Taviri in the dream.

What had he said? She could not remember. She was not sure if she had even touched his hand. She frowned, trying to force memory. It had been so long since she had dreamed about Taviri; and now not even to remember what he had said!

It was gone, it was gone. She stood there hunched in her nightdress, frowning, one hand on the bed-table. How long was it since she had thought of him — let alone dreamed of him — even thought of him, as “Taviri?” How long since she had said his name?

Asieo said. When Asieo and I were in prison in the North. Before I met Asieo. Asieo’s theory of reciprocity. Oh yes, she talked about him, talked about him too much no doubt, maundered, dragged him in. But as “Asieo,” the last name, the public man. The private man was gone, utterly gone. There were so few left who had even known him. They had all used to be in jail. One laughed about it in those days, all the friends in all the jails. But they weren’t even there, these days. They were in the prison cemeteries. Or in the common graves.

“Oh, oh my dear,” Laia said out loud, and she sank down onto the bed again because she could not stand up under the remembrance of those first weeks in the Fort, in the cell, those first weeks of the nine years in the Fort in Drio, in the cell, those first weeks after they told her that Asieo had been killed in the fighting in Capitol Square and had been buried with the Fourteen Hundred in the lime-ditches behind Oring Gate. In the cell. Her hands fell into the old position on her lap, the left clenched and locked inside the grip of the right, the right thumb working back and forth a little pressing and rubbing on the knuckle of the left first finger. Hours, days, nights. She had thought of them all, each one, each one of the Fourteen Hundred, how they lay, how the quicklime worked on the flesh, how the bones touched in the burning dark. Who touched him? How did the slender bones of the hand lie now? Hours, years.

“Taviri, I have never forgotten you!” she whispered, and the stupidity of it brought her back to morning light and the rumpled bed. Of course she hadn’t forgotten him. These things go without saying between husband and wife. There were her ugly old feet fiat on the floor again, just as before. She had got nowhere at all, she had gone in a circle. She stood up with a grunt of effort and disapproval, and went to the closet for her dressing gown.

The young people went about the halls of the House in becoming immodesty, but she was too old for that. She didn’t want to spoil some young man’s breakfast with the sight of her. Besides, they had grown up in the principle of freedom of dress and sex and all the rest, and she hadn’t. All she had done was invent it. It’s not the same.

Like speaking of Asieo as “my husband.” They winced. The word she should use as a good Odonian, of course, was “partner.” But why the hell did she have to be good Odonian?

She shuffled down the hall to the bathrooms. Mairo was there, washing her hair in a lavatory. Laia looked at the long, sleek, wet hank with admiration. She got out of the House so seldom now that she didn’t know when she had last seen a respectably shaven scalp, but still the sight of a full head of hair gave her pleasure, vigorous pleasure. How many times had she been jeered at, Longhair, Longhair, had her hair pulled by policemen or young toughs, had her hair shaved off down to the scalp by a grinning soldier at each new prison? And then had grown it all over again, through the fuzz,to the frizz, to the curls, to the mane... In the old days. For God’s love, couldn’t she think of anything today but the old days?

Dressed, her bed made, she went down to commons. It was a good breakfast, but she had never got her appetite back since the damned stroke. She drank two cups of herb tea, but couldn’t finish the piece of fruit she had taken. How she had craved fruit as a child badly enough to steal it; and in the Fort — oh, for God’s love stop it! She smiled and replied to the greetings and friendly inquiries of the other breakfasters and big Aevi who was serving the counter this morning. It was he who had tempted her with the peach, “Look at this, I’ve been saving it for you,” and how could she refuse? Anyway she had always loved fruit, and never got enough; once when she was six or seven she had stolen a piece off a vendor’s cart in River Street. But it was hard to eat when everyone was talking so excitedly. There was news from Thu, real news. She was inclined to discount it at first, being wary of enthusiasms, but after she had read the between the lines of it, she thought, with a strange kind of certainty, deep but cold. Why, this is it: it has come. And in Thu, not here. Thu will break before this country does; the Revolution will first prevail there. As if that mattered! There will be no more nations. And yet it did matter somehow, it made her a little cold and sad-envious, in fact. Of all the infinite stupidities. She did not join in the talk much, and soon got up to go back to her room, feeling sorry for herself. She could not share their excitement. She was out of it, really out of it. It’s not easy, she said to herself in justification, laboriously climbing the stairs, to accept being out of it when you’ve been in it, in the center of it, for fifty years. Oh, for God’s love. Whining!

She got the stairs and the self-pity behind her, entering her room. It was a good room, and it was good to be by herself. It was a great relief. Even if it wasn’t strictly fair. Some of the kids in the attics were living five to a room no bigger than this. There were always more people wanting to live in an Odonian House than could be properly accommodated. She had this big room all to herself only because she was an old woman who had had a stroke. And maybe because she was Odo. If she hadn’t been Odo, but merely the old woman with a stroke, would she have had it? Very likely. After all, who the hell wanted to room with a drooling old woman? But it was hard to be sure. Favoritism, elitism, leader-worship, they crept back and cropped out everywhere. But she had never hoped to see them eradicated in her lifetime, in one generation. Only Time works the great changes. Meanwhile this was a nice, large, sunny room, proper for a drooling old woman who had started a world revolution.

Her secretary would be coming in an hour to help her despatch the day’s work. She shuffled over to the desk, a beautiful, big piece, a present from the Noi Cabinetmakers’ Syndicate because somebody had heard her remark once that the only piece of furniture she had ever really longed for was a desk with drawers and enough room on top ... damn, the top was practically covered with papers with notes clipped to them, mostly in Noi’s small clear handwriting: Urgent. — Northern Provinces. — Consult w/R. T.?

Her own handwriting had never been the same since Asieo’s death. It was odd, when you thought about it. After all, within five years after his death she had written the whole Analogy. And there were those letters, which the tall guard with the watery grew’ eyes, what was his name, never mind, had smuggled out of the fort for her for two years. The Prison Letters they called them now, there were a dozen different editions of them. All that stuff, the letters which people kept telling her were so full of “spiritual strength” — which probably meant she had been lying herself blue in the face when she wrote them, trying to keep her spirits up — and the Analogy which was certainly the solidest intellectual work she had ever done, all of that had been written in the Fort in Drio, in the cell, after Asieo’s death. One had to do something, and in the Fort they let one have paper and pens... But it had all been written in the hasty, scribbling hand which she had never felt was hers, not her own like the round, black scrolling of the manuscript of Society Without Government, forty-five years old. Taviri had taken not only her body’s and her heart’s desire to the quicklime with him, but even her good clear hand-writing.

But he had left her the Revolution.

How brave of you to go on, to work, to write, in prison, after such a defeat for the Movement, after your partner’s death, people had used to say. Damn fools. What else had there been to do? Bravery, courage — what was courage? She had never figured it out. Not fearing, some said. Fearing yet going on, others said. But what could one do but go on? Had one any real choice, ever?

To die was merely to go on in another direction.

If you wanted to come home you had to keep going on, that was what she meant when she wrote “True journey is return,” but it had never been more than an intuition, and she was farther than ever now from being able to rationalize it. She bent down, too suddenly so that she grunted a little at the creak in her bones, and began to root in a bottom drawer of the desk. Her hand came on an age-softened folder and drew it out, recognizing it by touch before sight confirmed: the manuscript of Syndical Organization in Revolutionary Transition. He had printed the title on the folder and written his name under it, Taviri Odo Asieo, IX 741. There was an elegant handwriting, every letter well-formed, bold, and fluent. But he had preferred to use a voiceprinter. The manuscript was all in voiceprint, and high quality too, hesitancies adjusted and idiosyncrasies of speech normalized. You couldn’t see there how he had said “o” deep in his throat as they did on the North Coast. There was nothing of him there but his mind. She had nothing of him at all except his name written on the folder. She hadn’t kept his letters, it was sentimental to keep letters. Besides, she never kept anything. She couldn’t think of anything that she had ever owned for more than a few years, except this ramshackle old body, of course, and she was stuck with that...

Dualizing again. “She” and “it.” Age and illness made one dualist, made one escapist; the mind insisted, It’s not me, it’s not me. But it was. Maybe the mystics could detach mind from body, she had always rather wistfully envied them the chance, without hope of emulating them. Escape had never been her game. She had sought for freedom here, now, body and soul.

First self-pity, then self-praise, and here she still sat, for God’s love, holding Asieo’s name in her hand, why? Didn’t she know his name without looking it up? What was wrong with her? She raised the folder to her lips and kissed the handwritten name firmly and squarely, replaced the folder in the back of the bottom drawer, shut the drawer, and straightened up in the chair. Her right hand tingled. She scratched it, and then shook it in the air, spitefully. It had never quite got over the stroke. Neither had her right leg or right eye, or the right corner of her mouth. They were sluggish, inept, they tingled. They made her feel like a robot with a short circuit.

And time was getting on, Noi would be coming, what had she been doing ever since breakfast?

She got up so hastily that she lurched, and grabbed at the chairback to make sure she did not fall. She went down the hall to the bathroom and looked in the big mirror there. Her grey knot was loose and droopy, she hadn’t done it up well before breakfast. She struggled with it a while. It was hard to keep her arms up in the air. Amai, running in to piss, stopped and said, “Let me do it!” and knotted it up tight and neat in no time, with her round, strong, pretty fingers, smiling and silent. Amai was twenty, less than a third of Laia’s age. Her parents had both been members of the Movement, one killed in the insurrection of ‘60, the other still recruiting in the South Provinces. Amai had grown up in Odonian Houses, born to the Revolution, a true daughter of anarchy. And so quiet and free and beautiful a child, enough to make you cry when you thought: this is what we worked for, this is what we meant, this is it, here she is, alive, the kindly, lovely future.

Laia Asieo Odo’s right eye wept several little tears, as she stood between the lavatories and the latrines having her hair done up by the daughter she had not borne; but her left eye, the strong one, did not weep, nor did it know what the right eye did.

She thanked Amai and hurried back to her room. She had noticed, in the mirror, a stain on her collar. Peach juice, probably. Damned old dribbler. She didn’t want Noi to come in and find her with drool on her collar.

As the clean shirt went on over her head, she thought, What’s so special about Noi?

She fastened the collar-frogs with her left hand, slowly. Noi was thirty or so, a slight, muscular fellow, with a soft voice and alert dark eyes. That’s what was special about Noi. It was that simple. Good old sex. She had never been drawn to a fair man or a fat one, or the tall fellows with big biceps, never, not even when she was fourteen and fell in love with every passing fart. Dark, spare, and fiery, that was the recipe. Taviri, of course. This boy wasn’t a patch on Taviri for brains, nor even for looks, but there it was: she didn’t want him to see her with dribble on her collar and her hair coming undone.

Her thin, grey hair.

Noi came in, just pausing in the open doorway — my God, she hadn’t even shut the door while changing her shirt! She looked at him and saw herself. The old woman.

You could brush your hair and change your shirt, or you could wear last week’s shirt and last night’s braids, or you could put on cloth of gold and dust your shaven scalp with diamond powder. None of it would make the slightest difference. The old woman would look a little less, or a little more, grotesque.

One keeps oneself neat out of mere decency mere sanity, awareness of other people.

And finally even that goes, and one dribbles unashamed.

“Good morning,” the young man said in his gentle voice.

“Hello, Noi.”

No, by God, it was not out of mere decency. Decency be damned. Because the man she had loved, and to whom her age would not have mattered — because he was dead, must she pretend she had no sex? Must she suppress the truth, like a damned puritan authoritarian? Even six months ago, before the stroke, she had made men look at her and like to look at her; and now, though she could give no pleasure, by God she could please herself.

When she was six years old, and Papa’s friend Gadeo used to come by to talk politics with Papa after dinner, she would put on the gold-colored necklace that Mama had found on a trash heap and brought home for her. It was so short that it always got hidden under her collar where nobody could see it. She liked it that way. She knew she had it on. She sat on the doorstep and listened to them talk, and knew that she looked nice for Gadeo. He was dark, with white teeth that flashed. Sometimes he called her “pretty Laia.” “There’s my pretty Laia!” Sixty-six years ago.

“What? My head’s dull. I had a terrible night.” It was true. She had slept even less than usual.

“I was asking if you’d seen the papers this morning.”

She nodded.

“Pleased about Soinehe?”

Soinehe was the province in Thu which had declared its secession from the Thuvian State last night.

He was pleased about it. His white teeth flashed in his dark, alert face. Pretty Laia.

“Yes. And apprehensive.”

“I know. But it’s the real thing, this time. It’s the beginning of the end of the Government in Thu. They haven’t even tried to order troops into Soinehe, you know. It would merely provoke the soldiers into rebellion sooner, and they know it.”

She agreed with him. She herself had felt that certainty. But she could not share his delight. After a lifetime of living on hope because there is nothing but hope, one loses the taste for victory. A real sense of triumph must be preceded by real despair. She had unlearned despair a long time ago. There were no more triumphs. One went on.

“Shall we do those letters today?”

“All right. Which letters?”

“To the people in the North,” he said without impatience.

“In the North?”

“Parheo, Oaidun.”

She had been born in Parheo, the dirty city on the dirty river. She had not come here to the capital till she was twenty-two and ready to bring the Revolution, though in those days, before she and the others had thought it through, it had been a very green and puerile revolution. Strikes for better wages, representation for women. Votes and wages — Power and Money, for the love of God! Well, one does learn a little, after all, in fifty years.

But then one must forget it all.

“Start with Oaidun,” she said, sitting down in the armchair. Noi was at the desk ready to work. He read out excerpts from the letters she was to answer. She tried to pay attention, and succeeded well enough that she dictated one whole letter and started on another. “Remember that at this stage your brotherhood is vulnerable to the threat of... no, to the danger... to...” She groped till Noi suggested, “The danger of leader-worship?”

“All right. And that nothing is so soon corrupted by power-seeking as altruism. No. And that nothing corrupts altruism-no. O for God’s love you know what I’m trying to say, Noi, you write it. They know it too, it’s just the same old stuff, why can’t they read my books!”

“Touch,” Noi said gently, smiling, citing one of the central Odonian themes.

“All right, but I’m tired of being touched. If you’ll write the letter I’ll sign it but I can’t be bothered with it this morning.” He was looking at her with a little question or concern. She said, irritable, “There is something else I have to do!” 


When Noi had gone she sat down at the desk and moved the papers about, pretending to be doing something, because she had been startled, frightened, by the words she had said. She had nothing else to do. She never had had anything else to do. This was her work: her lifework. The speaking tours and the meetings and the streets were out of reach for her now, but she could still write, and that was her work. And anyhow if she had had anything else to do, Noi would have known it; he kept her schedule, and tactfully reminded her of things, like the visit from the foreign students this afternoon. Oh, damn. She liked the young, and there was always something to learn from a foreigner, but she was tired of new faces, and tired of being on view. She learned from them, but they didn’t learn from her; they had learnt all she had to teach long ago, from her books, from the Movement. They just came to look, as if she were the Great Tower in Rodarred, or the Canyon of the Tulaevea. A phenomenon, a monument. They were awed, adoring. She snarled at them: Think your own thoughts! — That’s not anarchism, that’s mere obscurantism. — You don’t think liberty and discipline are incompatible, do you? — They accepted their tongue-lashing meekly as children, gratefully, as if she were some kind of All-Mother, the idol of the Big Sheltering Womb. She! She who had mined the shipyards at Seissero, and had cursed Premier Inoilte to his face in front of a crowd of seven thousand, telling him he would have cut off his own balls and had them bronzed and sold as souvenirs, if he thought there was any profit in it — she who had screeched, and sworn and kicked policemen, and spat at priests, and pissed in public on the big brass plaque in Capitol Square that said HERE WAS FOUNDED THE SOVEREIGN NATION OF A-IO ETC ETC, pssssssssss to all that! And now she was everybody’s grandmama, the dear old lady, the sweet old monument, come worship at the womb. The fire’s out, boys, it’s safe to come up close.

“No, I won ‘t,” Laia said out loud. “I will not.” She was not selfconscious about talking to herself, because she always had talked to herself. “Laia’s invisible audience,” Tavari had used to say, as she went through the room muttering. “You needn’t come, I won’t be here,” she told the invisible audience now. She had just decided that it was she had to do. She had to go out. To go into the streets.

It was inconsiderate to disappoint the foreign students. It was erratic, typically senile. It was unOdonian. Psssssss to all that. What was the good working for freedom all your life and ending up without any freedom at all? She would go out for a walk.

“What is an anarchist? One who, choosing, accepts the responsibility of choice.”

On the way downstairs she decided, scowling, to stay and see the foreign students. But then she would go out.

They were very young students, very earnest: doe-eyed, shaggy, charming creatures from the Western Hemisphere. Benbili and the kingdom of Mand, the girls in white trousers, the boys in long kilts, warlike and archaic. They spoke of their hopes. “We in Mand are so very far from the Revolution that maybe we are near it,” said one of the girls, wistful and smiling: “The Circle of Life!” and she showed the extremes meeting, in the circle of her slender, dark-skinned fingers. Amai and Aevi served them white wine and brown bread, the hospitality of the House. But the visitors, unpresumptuous, all rose to take their leave after barely half an hour. “No, no, no,” Laia said. “stay’ here, talk with Aevi and Amai. It’s just that I get stiff sitting down, you see, I have to change about. It has been so good to meet you, will you come back to see me, my little brothers and sisters, soon?” For her heart went out to them, and theirs to her, and she exchanged kisses all round, laughing, delighted by the dark young cheeks, the affectionate eyes, the scented hair, before she shuffled off. She was really a little tired, but to go up and take a nap would be a defeat. She had wanted to go out. She would go out. She had not been alone outdoors since-when? Since winter! before the stroke. No wonder she was getting morbid. It had been a regular jail sentence. Outside, the streets, that’s where she lived.

She went quietly out the side door of the House, past the vegetable patch, to the street. The narrow strip of sour city dirt had been beautifully gardened and was producing a fine crop of beans and ceea, but Laia’s eye for farming was unenlightened. Of course it had been clear that anarchist communities, even in the time of transition, must work towards optimal self-support, but how that was to be managed in the way of actual dirt and plants wasn’t her business. There were farmers and agronomists for that. Her job was the streets, the noisy, stinking streets of stone, where she had grown up and lived all her life, except for the fifteen years in prison.

She looked up fondly at the facade of the House. That it had been built as a bank gave peculiar satisfaction to its present occupants. They kept their sacks of meal in the bomb-proof money-vault, and aged their cider in kegs in safe deposit boxes. Over the fussy columns that faced the street carved letters still read, “National Investors and Grain Factors Banking Association.” The Movement was not strong on names. They had no flag. Slogans came and went as the need did. There was always the Circle of Life to scratch on walls and pavements where Authority would have to see it. But when it came to names they were indifferent, accepting and ignoring whatever they got called, unafraid of being pinned down and penned in, unafraid of being absurd. So this best known and second oldest of all the cooperative Houses had no name except The Bank.

It faced on a wide and quiet street, but only a block away began the Temeba, an open market, once famous as a center for blackmarket psychogenics and teratogenics, now reduced to vegetables, secondhand clothes, and miserable sideshows. Its crapulous vitality was gone, leaving only half-paralyzed alcoholics. addicts, cripples, hucksters, and fifth rate whores, pawnshops, gambling dens fortune-tellers, body-sculptors, and cheap hotels. Laia turned to the Temeba as water seeks its level.

She had never feared or despised the city. It was her country. There would not be slums like this, if the Revolution prevailed. But there would be misery. There would always be misery, waste, cruelty. She had never pretended to be changing the human condition, to be Mama taking tragedy away from the children so they won’t hurt themselves. Anything but. So long as people were free to choose, if they chose to drink flybane and live in sewers, it was their business. Just so long as it wasn’t the business of Business, the source of profit and the means of power for other people.

She had felt all that before she knew anything; before she wrote the first pamphlet, before she left Parheo, before she knew what “capital” meant, before she’d been farther than River Street where she played rolltaggie kneeling on scabby knees on the pavement with the other six-year-olds, she had known it: that she, and the other kids, and her parents, and their parents, and the drunks and whores and all of River Street, were al the bottom of something — were the foundation, the reality, the source. But will you drag civilization down into the mud? cried the shocked decent people, later on, and she had tried for years to explain to them that if all you had was mud, then if you were God you made it into human beings, and if you were human you tried to make it into houses where human beings could live. But nobody who thought he was better than mud could understand. Now, water seeking its level, mud to mud, Laia shuffled through the foul, noisy street, and all the ugly weakness of her old age was at home. The sleepy whores, their lacquered hair-arrangements dilapidated and askew, the one-eyed woman wearily yelling her vegetables to sell, the half-wit beggar slapping flies, these were her countrywomen. They looked like her, they were all sad, disgusting, mean, pitiful, hideous. They were her sisters her own people.

She did not feel too well. It had been along time since she had walked so far, four or five blocks by herself, in the noise and push and striking summer heat of the streets. She had wanted to get to Koly Park, the triangle of scruffy grass at the end of the Temeba and sit there for a while with the other old men and women who always sat there, to see what it was like to sit there and be old; but it was too far. If she didn’t turn back now, she might get a dizzy spell, and she had a dread of falling down, falling down and having to lie there and look up at the people come to stare at the old woman in a fit. She turned and started home, frowning with effort and self-disgust. She could feel her face very red, and a swimming feeling came and went in her ears. It got a bit much, she was really afraid she might keel over. She saw a doorstep in the shade and made for it, let herself down cautiously, sat, sighed.

Nearby was a fruit-seller, sitting silent behind his dusty, withered stock. People went by. Nobody bought from him. Nobody looked at her. Odo, who was Odo? Famous revolutionary, author of Community, The Analogy, etc. etc. She, who was she? An old woman with grey hair and a red face sitting on a dirty doorstep in a slum, muttering to herself.

True? Was that she? Certainly it was what anybody passing her saw. But was it she, herself, any more than the famous revolutionary, etc., was? No. It was not. But who was she, then?

The one who loved Taviri.

Yes. True enough. But not enough. That was gone; he had been dead so long.

“Who am I?” Laia muttered to her invisible audience, and they knew the answer and told it to her with one voice. She was the little girl with scabby knees, sitting on the doorstep staring down through the dirty golden haze of River Street in the heat of late summer, the six-year-old, the sixteen-year-old, the fierce, cross, dream-ridden girl, untouched, untouchable. She was herself. Indeed she had been the tireless worker and thinker, but a blood clot in a vein had taken that woman away from her. Indeed she had been the lover, the swimmer in the midst of life, but Taviri, dying, had taken that woman away with him. There was nothing left, really, but the foundation. She had come home; she had never left home. “True voyage is return.” Dust and mud and a doorstep in the slums. And beyond, at the far end of the street, the field full of tall dry weeds blowing in the wind as the night came.

“Laia! What are you doing here? Are you all right?”

One of the people from the House, of course, a nice woman, a bit fanatical and always talking. Laia could not remember her name though she had known her for years. She let herself be taken home, the woman talking all the way. ln the big cool common room (once occupied by tellers counting money behind polished counters supervised by armed guards) Laia sat down in a chair. She was unable just as yet to face climbing the stairs, though she would have liked to be alone. The woman kept on talking, and other excited people came in. It appeared that a demonstration was being planned. Events in Thu were moving so fast that the mood here had caught fire, and something must be done. Day after tomorrow, no, tomorrow, there was to be a march, a big one, from Old Town to Capitol Square the old route. “Another Ninth Month Uprising,” said a young man, fiery and laughing, glancing at Laia. He had not even been born at the time of the Ninth Month Uprising, it was all history to him. Now he wanted to make some history of his own. The room had filled up. A general meeting would be held here, tomorrow, at eight in the morning. “You must talk, Laia.”

“Tomorrow? Oh, I won’t be here tomorrow,” she said brusquely. Whoever had asked her smiled, another one laughed, though Amai glanced round at her with a puzzled look. They went on talking and shouting. The Revolution. What on earth had made her say that? What a thing to say on the eve of the Revolution, even if it was true.

She waited her time, managed to get up and, for all her clumsiness, to slip away unnoticed among the people busy with their planning and excitement. She got to the hall, to the stairs, and began to climb them one by one. “The general strike,” a voice, two voices, ten voices were saying in the room below, behind her. “The general strike “ Laia muttered, resting for a moment on the landing. Above, ahead, in her room, what awaited her? The private stroke. That was mildly funny. She started up the second flight of stairs, one by one, one leg at a time, like a small child. She was dizzy but she was no longer afraid to fall. On ahead, on there, the dry white flowers nodded and whispered in the open fields of evening. Seventy-two years and she had never had time to learn what they were called.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

