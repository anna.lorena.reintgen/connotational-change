
Can you tell us about the history of anarchism in Indonesia?

MT: As far as I know from my friends' stories and from what I’ve learned, the origin of anarchism in Indonesia came together with the arrival of punk music around 1998. At that time anarchy was synonymous with punk and some people in that community began to delve deeper into anarchic ideology and values. Since that time anarchist discourse began to develop amongst individuals or collectives in the punk / hardcore community, and later to a broader range of groups such as activists, students, workers; essentially reaching a wider public with different backgrounds.

Along with the spread of anarchic discourse, many discussions on this topic began to occur, and anarchy began to be debated, analysed and criticized more deeply (and this process continues until today, now with a wider arena of different analyses). The next step was to bring it into praxis, for instance forming collectives with anarchic principals and values (decentralized, non-hierarchical & consensus). Despite the many problems these collectives faced, collective models like this could be seen as something different, a counter to the model of groups which always seek to dominate (both in the political sphere and the non-political) through their hierarchical, centralist, and authoritarian forms or structures.

Actions such as Food Not Bombs can be regarded as one of the early forms of direct action emerging from an anarchic praxis here, along with producing zines and other publication such as newsletter, pamphlets, etc. At first the themes and issues of zines were mostly about the punk / hardcore surroundings, but as time went on and the process developed, more varied themes and issues were presented such as feminism, anarchy values, anti-capitalism, global & social resistance, varients of anarchism, environmental and animal movements, political news , and others. The progress of anarchy is also helped by the increasing levels of Internet access; internet media are used by our friends to disseminate information about anarchist discourse.

PM: I think I have to tell you first, that Indonesian is not familiar with english language so even in that time internet was also started to spread, just a little bit of the whole first generation of anarcho-punk whom understood what is anarchism theoretically. But the urge to do something, shaped also by the turbulence of economical, political and social situation around that time, and our thirst to understand about who we are, led almost half of the first generation towards PRD (Partai Rakyat Demokratik, or People’s Democratic Party), a Leninist political party. We didn’t have much choice since we didn’t have much references in our own language. There were several anarchist pamphlets by Bakunin, Emma Goldman, Rudolf Rocker, translated by several anarchist friends and distributed widely. But it didn’t help much since what that classic thinker wrote is hardly connected to our understanding about our own place and time. Like, how could we really understood about the evil of the goverment since at that time Indonesian history had only experiences life under two presidents? We’re a post-colonial state, and it has a lot of things to do with the believe that the state is an evil naturally.

Around 2001, one by one burn out and left the party, but a lot of us still hold the understanding that the only thing that seems possible to do, is just a way which Lenin said: a revolution under one banner, transitional government, and such stink like that. We’re also treated as anomali by a lot of people, since we as anarchists start to believe that the state is also our systematical enemy. When other people demands to have more power to Indonesia as a state to have a voice in the international circle, we declare that the state and all of the functionaries are also our enemy. And in this time, came the second generation of self-identify anarchists as a result of the first generation activities and confusion. This second generation quitely has more deeper understanding about anarchism and can point the differences with Leninism, or I can say, this generation learn from the first generation’s mistakes.

What is the situation today? What are the main groups and currents in Indonesian anarchism?

JC: As far as I know there is no predominant grouping in Indonesia. There are just several collectives and individuals from various backgrounds and pursuing a range of different strains of anarchism. Not infrequently, this difference in methods gives rise to debate, although this debate does not cause hostilities between the collectives or individuals involved. There are even various occasions when these collectives and individuals from different backgrounds and variants of anarchism become involved in the same project. Mayday 2007 and 2008 are examples of this. Several collectives and individuals from various cities became involved in planning the occasion of Mayday 2007. The collectives involved were: Affinitas (collective from Yogyakarta), Jaringan Otonomis (collective from Jakarta), Apokalips (collective from Bandung) and Jaringan Autonomus Kota (collective from Salatiga). Aside from these collectives, individuals from various cities such as Bali and Semarang also participated, as well as people from punk collectives in Jakarta. The collectives and individuals involved chose to name themselves Jaringan Anti-Otoritarian (anti-authoritan network). On Mayday 2007, the number of participants on the action came to around 100 people, all dressed in black. At that time, the message the Jaringan Anti-Otoritarian was aiming to bring was the redefinition of Mayday, because at that time Mayday was an occasion dominated by leftist groups and individuals. Jaringan Anti-Otoritarian's action that day can be called a success. The anarchist movement, which at that time was considered non-existant, started to receive attention.

After Mayday 2007, the anarchist movement started to flare up. Several new groups appeared in different cities. Anarchists also became more involved in protests, the protests against the building of a nuclear power station, for example.

As Mayday 2008 approached, the co-ordination between collectives and individuals in different cites was reactivated. This co-ordination followed two channels: via the internet and through face-to-face meetings. This time round, an increasing number of participants were involved. Unfortunately around that time Apokalips (collective from Bandung) and Sindikat Melawan (collective from Salatiga) backed down from being involved in the action for various reasons. The coordination seemed in disarray, but Affinitas (collective from Yogyakarta) and the Jakarta collectives as well as some individuals from different towns still managed to pull through with an action. As with the year before, the Mayday 2008 action took place in Jakarta. Up to 200 people participated, including punkers. The issue focussed on was corporate abuses, and the target of the action was the Bakrie Building. This building is the office of the various companies owned by business mogul and politician Aburizal Bakrie. Some participants vandalised the Bakrie building, and not long after that, police arrived and began their repression. Several participants were arrested by force (although some of those arrested were successfully freed by the other participants). After resisting as they could, the participants continued the march, but unfortunately the police returned in greater numbers. All participants on the action were then arrested.

Post-mayday 2008, the escalation of the anarchist movement started to decline. Some collectives also collapsed. But that didn't mean anarchism was dead. New collectives and individuals, as well as those who were already a long time involved in the Indonesian anarchist movement, started networking and taking action with people in various places who have suffered at the hands of corporations. Until the present day, more than a few anarchists are involved in people's struggles against corporations and the state. Some anarchists even start with more militant actions such as attacking police posts or damaging shopping malls.

MT: I will respond more about the current situation, since JC’s answers provide more description about group dynamics.

We can say that the state of anarchism today is still an ongoing process. And I see it, it can be divided into 2 areas, first; that of discourse & theory and second; the praxis.

In the area of discourse & theories; nowadays the topics for discussion and analysis are becoming more widespread starting from capitalism, work, security culture, civilization, philosophy, media, consumerism, religion, gender, corporations, forms of free association, the relevance of anarchy in the context of daily life and Indonesia, and so on. This discussion is carried out both directly (classes, film screenings, regular discussions, group discussions, open/public discussions), or indirectly (mailing lists, email, books, journals, pamphlets, zines, newsletters, websites, blogs).However I feel that at the moment, discussions about corporations, capitalism, social resistance, and direct action are becoming the hot topics in anarchist circles. As a woman I perceive that issues around women, body, sexuality, sexual orientation, and personal relationships are not being discussed enough. That's why I am trying to raise this issue in variety ways and through different media (direct action, criticism, writing, artwork), and now I'm preparing a novel about rebellious genitals. Hopefully through fiction my message can be more easily accepted and understood.

I also saw recently that similar kinds of women's writing are on the rise and are becoming known as 'sasatra lendir'. They are concerned with sexual freedom (women's' sexuality, lesbians and gay), yet without highlighting the relationships and values that lie behind it, such as power, dominance, control and so on.

When it comes to the practical level, nowadays more and more friends are taking direct action based on anarchic values, such as food not bombs, skill shares, street campaigns and demonstrations (about corporate abuses, police violence, consumerism, capitalism, etc.), building collectives with different focusses (such as free schools, reading spaces, infoshops), DIY festivals, following an anarchist approach to organising together with people affected by corporate abuse, and also sabotage. It can be said that the existence of anarchists and their discourse and activism are increasingly being 'noticed' by wider groups, and of course that generates many dynamics and debates, especially in the authoritarian left circles (Marxist-Leninist-Maoist-Chavist activist groups) that dominate resistance movements. There are many errors and misunderstandings about anarchist theory, philosophy and action amongst this 'left' scene. I can understand this, because left groups always see ideology only as a 'tool of their political struggle', while anarchists understand that ideology is something very philosophical and personal (something internalised in ourselves), which can be applied in a range of contexts, whether personal, in relationships or daily life, or in social and political struggles. At the same time I feel there is a more widespread desire to know about anarchist discourse coming from other individuals and groups. This can be seen from the increase of questions, and friends becoming attracted by the idea of anarchy.

The involvement of women in anarchist activism is still quite minimal, this has been troublesome for me during the more-or-less 10 years since I began 'the struggle'. I feel that I have made an effort very often to support and encourage female friends, but it always stops halfway, and the majority opt to go back to their 'prison'. They choose to marry and build a family, or choose to work, or choose to work in a woman's NGO. I feel that in the society I inhabit, patriarchal and sexist values and practices still have a very strong impact, and arise in nearly every context (family, relationships of lovers, friendship, community, organisations, schools, work, society, religion, etc.), maybe that is one reason the situation is as it is. In Indonesia there is very little history of radical women's movements. There was a movement during the time of the PKI (Partai Komunis Indonesia = Indonesian Communist Party), which included an organisation of women known as GERWANI (Gerakan Wanita Indonesia = Indonesian Women's movement). This movement was destroyed during the muzzling of the PKI by the state in 1965-66. Everything that was connected with the PKI or communism has already left a profound trauma on Indonesian society, and this has also become a propaganda tool and threat used by authority to quell any form of popular struggle. “You don't want what happened in 1965 to come back again, right?” After this time there was almost no women's movement or radical individual women, nowadays what is called the 'women's movement' is to a large extent concentrated in women's NGOs or left organisations that repeatedly compromise and are pro- the concept of the state.

Misunderstandings: I also see amongst friends that believe in anarchy and live it in their lives that there are often debates and differences of opinions, they still can't organise themselves and their groups well, there are still misunderstandings about anarchist values and principles in everyday relationships (for example hierarchy, domination and sexism still exist). But I can also understand this, it is part of our process of learning.

The lack of access to information and literature; apart from that, I see that anarchist references and literature are still minimal and hard to access. The majority of information available is in English (or other languages) or from websites, however neither internet access, access to books nor the English language are yet widespread in our society. Institut-A Infohouse and Community Center was started more or less for that reason, there is a need for a space to access anarchist literature.

Are there any historical movements in Indonesia that anarchists today draw inspiration from - even if these movements did not self-identify as "anarchist"?

JC: There are some movements which, although they never identified themselves as anarchists, are nonetheless quite inspirational. Such as in Blora and Pati, anarchists are inspired by the Sedulur Sikep movement (known as Samin people), a social dissident movement that has existed since the Dutch colonial period. However this was only ever active on a local scale. There are also a lot of anarchists who feel social and political movements in Indonesia are not inspirational. For various reasons, some anarchists make a serious attempt to delve into the history of struggle in Indonesia, struggles that despite not clearly identifying themselves as anarchists, still have anarchistic features.

MT: I'm going to try to give a more complete answer about the Sedular Sikep (Samin) movement and its philosophy.

The teachings of Sedulur Sikep were spread by Samin Surosentiko (1859-1914), and were a concept for the rejection of the Dutch colonial culture and capitalism as it emerged during the colonial era in Indonesia, in the 19th century. This movement grew first of all around Klopoduwur, Blora, Central Java. In 1890 the Sedular Sikep movement developed in two forest villages in the Randublatung district, Bojonegoro Regency, East Java, and quickly spread to other villages. It ranged from the northern coast of Java to the forests of the North and South Kendeng mountain range. All around the borders regions of East Java and Central Java according to today's maps, a struggle arose to resist the authority of the Dutch as as they seized land to use as teak plantations.

The teachings of Sedulur Sikep emerged as a result of, or reaction to, the Dutch colonial government and its arbitrary acts. The struggle did not take the form of physical confrontation but instead laid down a challenge to all regulations and obligations of the people to the government, for example by not paying taxes. Influenced by their attitude of challenging everything, they built up their own social order, customary law and traditions independently.

One principle of Sedulur Sikep was “kulo ndiko sami, kowe aku podho” (me and you are equal, so all people may be equal). This is a principle of equality without making a distinction based on skin colour, ethnicity or religion/beliefs. No human being ranks higher than any other, or has more rights or obligations. Samin felt that in all matters, people should sort things out on their own or by voluntary co-operation.

Sedulur Sikep was a practical philosophy, it didn't aim to create something that's impossible. The objective of Sedular Sikep was to bring about an effective freedom for all humanity. The minority does not have any more rights than the majority, and vice versa. It is assumed that each and every person must have the same right to obtain the richness of the earth without money or payment; what a person produces becomes their possession, and no one person, or people acting together, has the right to take any such possession without that person's consent; that everyone can exchange things they produce if they wish to; there are no limits to what people can write, eat or drink, as long as it doesn't disturb the rights of others.

Sedulur Sikep refused the rental of land, allowing whoever lives on the land to make use of it, refused patents and copyright that are a form of monopoly, rejected the imposition of tax on people because tax must be voluntary, didn't send their children to formal education, treated nature and the environment wisely because it was considered as a mother that must be respected.

The key points of the Sedulur Sikep teachings are as follows: -religion can be a weapon or a principal for life. It must be understood that Sedulur Sikep didn't differentiate between religions, and because of this followers of Sedulur Sikep never disavowed or hated religion. The most important thing is how people behave in their lives. Weddings were carried out directly, without involving government or religious institutions because their religion wasn't recognised by the state. -don't annoy other people, don't quarrel, don't be envious and don't try to take others' possessions. -Choose to act wisely, and don't be arrogant. -A living human must develop an understanding of their own life, because our life is the same as our soul and we only have one for all eternity. According to Sedulur Sikep, when a person dies their soul doesn't die, it only takes off it''s clothes. -when speaking we should take care of what comes out of our mouthed, it must be honest and show respect for others. For Sedulur Sikep trade was forbidden because trade contains an element of 'dishonesty'. Also contributions could not be received in the form of money.

PM: Sadly, the regime under Suharto succeed in clearing of any historical material that they found doesn’t suit their positions. An Australian historian, Anton Lucas, wrote books about interesting social upheaval in Indonesia in time the declaration of Indonesian independence, that I found not specifically anarchist, but it autonomously risen against the upper class—against the colonial forces also against Indonesian Republic. I think Papuan indigenous movement also interesting. I met one of their leaders around 2000, and several years later I realized that his vision, his critics about Indonesian, made me remember about the conflict between egalitarian (but primitive) society against hierarchical modern society. I think the first thing to do to find our own history, is a really hard work that have to be done.

Indonesia is an enormously big country with many cultural and religious traditions. In which parts of Indonesia and in which of the country's communities do you find anarchists?

JC: I live on the island of Java, and all this time my activities have mostly been with anarchists that are also on Java (and I don't even know all of those), and a little with anarchists from other islands (and this is only in the context of communication, we have not yet reached the stage of working on a project together).

MT: You have to understand that in Indonesia, the island of Java is the 'heart' of government, information, education, etc. which causes an imbalance in access to this in other parts of the country. The state intentionally creates this condition so that the people outside Java are kept 'stupid' as their territories become the object for large-scale exploitation by the state and corporations.

I live in Jakarta (in the west of Java) and I know of anarchists (individuals or collectives) on the islands of: -Java (Jakarta, Bandung, Jogya, Semarang, Pati, Blora, Surabaya, Rembang, Randublatung, Salatiga, Porong) -Sumatra (Palembang, Pekanbaru, Medan, Aceh) -Kalimantan (Balikpapan) -Sulawesi (Makassar, Manado, Gorontalo) -Bali However some of the people or collectives on this list I have yet to meet with in person.

PM: I believe that anarchy is a basic human character. So in that believe, I think there’re some communities which we can find some anarchist values, even if these communities doesn’t call themselves as anarchist. Some even embrace religious traditions—that usually different with the mainstream, legal religions.

Does religion play any role in the anarchist movement at all? Have people drawn parallels between anarchism and Muslim traditions, for example? Or are all anarchists anti-religion?

JC: I am an anarchist that is agnostic. Many of my anarchist friends hold almost the same view as I do. But there are also some anarchists which are inspired by the Sufi movement, for example. For me, religion is a coercive and hierarchical institution, for which reason it's necessary not only to reject it, but also destroy it. Religion is not the same as spirituality. Although anti-religion, I'm not one of those modern people that glorifies reason and negates spirituality.

MT: I made the choice to not follow a religion before I knew about anarchism, because of my personal experiences in the family and in wider society. I have felt critical of the strong domination or control which religious institutions have in Indonesia ever since first hearing the views and attitudes about religion of some friends about religion. However in anarchist circles, many friends still 'have religion', or are not yet brave enough to have an open mind about the rejection of religion. Usually they are looking for a balance where they can still show respect for their families or religious environments.

In the context of Indonesia being the country with the largest number of Muslim inhabitants in the world, these values are rigorously implanted into us, enforced on us, handed down to us and control us, starting at the levels of family, society and state. However many of us have discussed these points and see religion as a source of domination and control that must be destroyed. Some anarchists look deeper into this issue and make criticisms of Islam from an anarchist perspective.

PM: If we speak about anarchist movement, I can tell you that religion doesn’t play any role in it. But one of my friends is a muslim who also an anarchist. And yes, he can traces any parallels between anarchism and muslim traditions. He even work as an editor in a book publisher that focused more on publishing muslim books.

Is there a strong connection between all anarchists in Indonesia?

PM: I don’t know if I can call this “strong connection”, but yes, almost all of anarchists (the one who self-identify as anarchist) in Indonesia know each other.

MT: I can say yes, maybe because we feel that we are a minority, the links between us are quite strong, there's a feeling that each of us supports the others and there is a desire to know about the activities of other friends. However because of the geographical and financial situation of people in Indonesia, it is quite difficult to meet up for face-to-face communication. Usually we communicate through the internet or telephone.

What are anarchist positions on the independence struggles that have been led in Indonesia over the last decades: in East Timor, West Papua, Aceh, and other regions?

PM: I can’t speak for any anarchist in Indonesia, but personally I think in some level I can support these struggle. I was also involved in the struggle for free Timor Leste (they prefer Timor Leste, not East Timor) before they became an independent state and have a corrupt government.

MT: It needs to be understood that every part of Indonesia has its own problems, that is why anarchists will always take action and resist in a way that is appropriate to the problems they encounter in their own place first of all. We avoid methods that involve representatives (in contrast to the majority of left groups), and this is why we always build our movements and actions in the places we happen to be. Also because there are no anarchist friends that live in those places (actually there is one friend in Aceh, but he has not been able to do so much because he has also just recently arrived there).

But we are nevertheless aware of what is going on in those places, and we support all forms of struggle to fight state authority and all autonomous struggles. However there is a contradiction when we look at the people's struggles in those places to free themselves from Indonesian and form a new nation (the system is the same, government and power have new faces), as can be seen in the case of East Timor for example.

Meanwhile we also discuss these issues from different viewpoints, especially in the context of state authority, militarism and the exploitation of the environment.

The Indonesian state has a terrible legacy as far as the persecution of political dissidents goes. How have anarchists been affected by this historically? What is the situation today?

JC: The Indonesia of today is an Indonesia that is not so totally different from the Indonesia of the New Order period. What I mean by that is, those who refuse to submit to the powers that be today are still persecuted. Nowadays, Indonesia's leaders have learned from the mistakes of their predecessors, and are better organised in their repression. If in the New Order time dissidence was destroyed with in a crude way, it's not like that any more. Those in power in Indonesia today promote an erroneous image that they do not rule through violence, and push the illusion of peace and security to a fantastic degree. This makes many people more pro-government - and even if they are not, they only disagree with its leaders. Hierarchy and power itself are considered legitimate by the majority of people.

MT: Several of us have already had to confront repressive actions from the state such as terror, intimidation, raids and arrests. To continue from JC's answer, we remain alert to this situation and try not to be influenced by the image which is projected by the state. We also try to pay more attention to problems of security and safety by starting to build a 'security culture' as something which should be seen as an important consideration in all our activities, and we are developing methods and strategies to face these situations. But it is important to point out that it is still only a few people that are concerned about this issue.

PM: Nowadays, the government actually still the same as before, but they play it smoothly. They don’t openly declare their statement violently anymore. But it doesn’t mean that they don’t use violence to solve what they see as problems. Several months ago, a village in Riau (Sumatra island) bombed and burned heavily by army helicopters because the peasants in that village didn’t want to give their land to a national palm oil corporation. In Sulawesi, several peasants being shot in the similar case. Also in several village in Java. But no state controlled media covered it all. Me and several of my friend usually publish the terrible news independently.

How about international connections? Do you have strong relations with other anarchists in South-east Asia, and further afield?

PM: Formally no. But for some friends, yes they have.

MT: For me personally, I have several connections with anarchists outside Indonesia, and from the beginning Institut - A was able to exist thanks to the help and solidarity of the international network. We try to make connections with various collectives in order to spread information about our existence and activities here. We are aware of the importance of sharing information and networking, and actually we know what tools we can use to share information, only sometimes we are hampered by language constraints, or not having the time or being in the right mood to translate a lot of material. Not many of us have the ability and desire to be intensively translating.

How do you see the future possibilities of anarchism in Indonesia and South-east Asia?

PM: I can’t speak about it as I don’t know much about anarchism in Southeast Asia. I know, a network is important, but for now, I just want to focus more on Indonesia since there’re so many things to do but so few Indonesian anarchists who really do it down here.

JC: With the ongoing process that has been taking place, I think there is lots of space that can be filled by anarchists in Indonesia. More than a few people who used to be Marxist-Leninist are beginning to think that all forms of power corrupt and repress. People that have become victims of corporations also start to feel that government will only ever take the side of corporations, and so it it is necessary to do something stronger than just asking the government for help.

MT: I feel very positive, especially in the context of Indonesia where there are so many problems. More and more different people and groups start to feel that their form of struggle is arriving at a dead end, and anarchy is seen as something very logical. Nevertheless, all this will need a long process and hard work, we need to build up collaborations and firm solidarity between us. I believe we will see the seeds of anarchy which we sow and scatter today start to grow everywhere...




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

