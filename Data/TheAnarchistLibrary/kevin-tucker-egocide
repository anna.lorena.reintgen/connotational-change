
The primal war is a spiritual war.

It began as the spirit of wildness was buried beneath the interests of domesticators: within history and within ourselves.

At its core lies the spiritual connection, the wordless sense of being that flows through the world. It is not about fighting for ‘Nature’ or about individual desire. It is about egocide: killing the self/Other split that underpins all civilized relationships.

There is no ‘Nature’, alone and isolated outside of our grasp. There is only the life that is in and of us. This is something that cannot be taught, written about or described. It’s not filling in space for god/s, nothingness, economics or science. It is not a cognitive force that hears every prayer.

I can’t say what it is that I feel. I can’t objectively prove its existence. But without my soul, I am as good as dead.

The domesticators have known this for a long time.

I can say that I feel something. It’s something that I know is real. It’s something worth fighting for.

That’s something that wild peoples and places have been telling us for ten thousand years.

 

Humans, like all beings, are intrinsically spiritual. Not in the sense of elaborate ritual or religious beliefs or anything of that sort, but spiritual in a much different way: a lived spirituality.

There is a flowing, organic nature to the world. It’s something you can feel as you follow tracks through the new snowfall. Something felt in a handful of wild berries or the smell of roots. It’s something you see in the eyes of an animal as their pupils dilate for the last time. The sting of a thorn, the protests of squirrels, and the ambience of rain on leaves, the sound of rushing water: there is life in all of these things. An essence that simply living brings you into.

The world of the nomadic gatherer/hunter knows no ‘Other’. There is no concept of nature. But there is a greater connectivity. There is no survival, no smallness or grandiose feeling. There is only life and death, interwoven and honestly laid out before you.

An individual exists as a part of this. Not in the manufactured sense of communistic groupthink, but in the spiritualistic sense. Life is inseparable. There is no dependency. There is no fear of a Future. No path of Progress. You can say there is an implicit sense of trust and honesty, but neither word does it justice. No word does it justice. Life simply is.

That needs to be restated: for most of us, life simply is an ideal. It’s a utopian desire or an irretrievable past. We simply can get closer to it or we can’t. But life can simply be life. It always has and always will be there. But we don’t think of it like that. We can’t think of it like that. We’ve been trained to see it differently. Life is simply something ‘Other’: either as a religious/anti-religious ideal or as a deadened scientific definition.

It must always be distant.

 

How do you turn someone against themselves? Against those around them? How do you tame the spirit? These are the issues that domesticators have always had to answer. The necessary response is what makes up our everyday lives: to domesticate, you must break someone mentally, physically, and spiritually.

Simply put, you must disconnect.

That disconnection, that mediation, has always been the primary goal of domesticators. The reason why is simple: domestication is about dependency. But that dependency is not about necessity, it’s about perceived dependency. It comes down to belief.

Most people believe that the state and civilization are necessary now because we know nothing else. We are raised in a manufactured reality. A sterile, planned world complete with heated seats, air conditioning, and power locks. Food is the processed side note to our consumption. Work is something you must do and the boss is someone you must obey.

The idea of living without civilization, and even more so, living well, is about as alien to us as the idea of living in this reality would be to anyone who lives without it. These are intrinsically different ways of viewing and being in reality. One is about the vital freedom to choose between the lesser of two evils and the better of two brands. The other is about the difficult choice about which direction you feel like roaming in today and which leaf looks most tempting.

How did we get from the latter to the former? How did we come to accept so little from life? How did we become so dependent?

How did food in storehouses become more important than the world outside? Filling those storehouses with large amounts of wild grains or dried meat or fish is an easy enough thing to do. For the most part, it may take a few days for a huge amount of food for the societies willing to do so. Becoming the person to ration the surplus isn’t that complicated either. Making people listen to that person, however, is.

The issue is about control. Power flows from control. But control requires physical and mental force. You can force someone in a cage, but it’s another thing to get them to accept it.

To successfully gain control over another being, that cage must be internalized.

For us, unfortunately, that cage has been internalized. This is the domestication process at work.

 

No one gives up their autonomy freely. The spirit of wildness which flows through all life must be broken.

To break the spirit, you must first isolate it. This is both the hardest and most important thing that must be done. We are born physically and mentally for a life of nomadic gathering and hunting. Like wildness/life everywhere, our spirit is inseparable from the world around us.

This needs some clarification.

I’m not talking about some new age ‘oneness’ anymore than I’m pushing for some kind of universal ‘indigenous perspective’. I’m talking about an unmediated relationship with the world. I’m talking about something that is felt and known without words. Nearly all human societies to have existed have lived with this spirit in their being. I’m talking about the same spirit that must be killed so that we can become who we are now. The spirit must be killed so that we can turn against ourselves and the earth.

Killing that spirit is impossible. It exists in all life. But at some point people began burying it: began accepting cheap substitutes. It was a long, hard and isolated problem, but the original trauma of domestication is a deep wound. One that spreads quickly and destroys anything in its path: always moving and searching for some kind of meaning. What that meaning is will always change shape and form, but the seekers are trained to look everywhere and destroy anything that stands in the way.

We are trained to look everywhere but our own damaged souls. We are trained to look for something, but never to feel. That, of course, is intentional.

No matter how we view the world, be it egocentric, anthropocentric, biocentric, etc.: we must always see the world (read: ‘Nature’) as someplace wholly separate. Certain people are/were a part of that (read: ‘Indigenous’), but that is gone, at least as far as we’re supposed to be concerned. The earth is a place, life is an ideal: you have only yourself.

Disconnected, lonely and desperate, we sink or swim in their reality. This is domestication. This is us occupying land that we have little sense of and alone in an environment flooded with billions of others.

This is your soul on Prozac.

 

The self/Other split begins with domestication. You can’t take control over a world that you are a part of. According to the monotheists, Adam and Eve took the first step by naming the animals in Eden. They may very well be right to a certain degree: life dissected and categorized is far more of an experiment than a community.

But the greatest damage was the one that turned life into property. It turned the spirit into wild grains, fish and large mammals into surplus: into wealth. The world of the gardener turns the world of the gatherer/hunter into a world of weeds, crops, gardens-in-use, fallowing gardens, and the village. The farmer dissects that even more into rows of crops in fields, animals-as-food or animals-as-workers, thinkers and doers. The capitalist sees consumers, distributors, managers, producers, and guards.

The world of wildness becomes processed and refined. The spirit of all things becomes the spirit of all things ‘useful’. The divide continues: we are no longer mere apes or wild beasts. We are the stewards of the earth, the bringers of the Future. Subject, object.

The soul must be isolated to be re-contextualized.

This is done subtly at first. As people in some places did settle and did start taking stored food, the initial roles for power began to emerge. But that power needed to be implicit even for the power of suggestion that Big Men would wield. This meant tinkering with the spirit. That became the job of shamans: the first specialists.

The role of the shaman spreads from the healer. A shaman is usually still a healer, but there is rarely a shortage of healers. For nearly all nomadic gatherer/hunters, healing is a communal activity. Healers deal with their reality through that communal spirit. Everyone is involved. The shaman, on the other hand, interprets that reality. That is extremely important.

Many shamans only slightly inserted their message into their interpretations of the spirit. The most important idea was implicit in their existence: the soul of the world is more open to certain individuals. Their position was as mediator between the individual and the rest of the world. And through this, the seeds for a self/Other split are born.

The message of the shaman, like the message of the preacher and the pundit, validates the social and political reality. As society becomes increasingly dependent on certain foods, the gods become specialized to ensure their growth (sun, water, earth/soil, and seed). As the political realm becomes more hierarchical, so does the cosmic one. As settlements become more permanent and spread into villages, the once unified world turns into the village, the gardens, and the forest. The dead become ancestors to fear as witches, werewolves, and sorcerers become the all seeing eye of morality.

The interpretation of the world around us becomes subject to the ancestors, to gods, then to god and science. But at the base of this is the self/Other split. The world of the nomadic gatherer/hunter based on cooperation and openness is replaced by competition and fear. People follow the hand that feeds as it substitutes their unmediated connection with the world through its vision.

First we split from the world and then we fear it. That’s where domestication begins. Fear and dependency grow to the point where anything else is unthinkable and even more so, frightening.

This is the world we are born into. This is our dependency.  This is our inheritance.

We are raised to accept it and continue substituting the spirit of wildness for the soulless world of domestication and mediation. The only spirit left is the self.

In a dog-eat-dog world, you sink or swim.

Subject or object. At least that much is supposed to be up to you.

 

The domesticators have been at their job a long time. For the most part they are successful at replacing the total world that we know in our hearts with the totality they have placed around our minds. But their job can never be complete. They sedate, distract and occupy us, but the wildness will always slip through the cracks.

For too many the uncontrollable urge to live free is too far beyond reach. It ends in self destruction or in the splitting of the mind.

The shell cracks only partially.

The totality of civilization in our minds is mirrored by the world it has created. Concrete, steel, glass, and iron do for the body what the church and state have done for the mind. Hierarchy and domination become structural. Our smallness and insignificance is constantly reinforced.

The revolt against civilization means that we must attack both internally and externally. In reality, there is no separation between the two. This attack is a response: a response to the totality we’ve been lulled into that seeks to destroy everything. For some that is meant literally. Their goal is to eliminate everything from concrete to Nature so that you are free to do anything or go anywhere. It’s a nihilistic rage that seeks honesty only where the individual remains isolated: to remove any and all conceivable chains.

To a degree I can understand this active nihilism. When everything you know feels tainted, it seems instinctive to deconstruct not only everything you know but how you think and feel. It makes sense as part of a process of shedding the totality of civilization, but that is it. Far too often it is seen as a goal in itself: a methodology towards the radical purity and free from all constraint. It stands as a deadening response to the sterile corpse of the city and country.

But nihilism, like its more honest relative, egoism, fails to break free of that initial grasp of domestication: the self/Other split. Both rely on that isolation, that Neverland of Self. To the nihilist and egoist there can be no greater connectivity without morality. The two oppositions remain: self and Other.

The initial lie of the domesticators comes full circle.

 

Civilization kills the spirit. It must in order to exist.

We think, build and maintain civilization. It is the reality created for us and the reality that we recreate daily. It is our addiction. It is everything we are given so that the soul cannot breathe: all the cheap replacements for wildness, for spirit. It is what we are given so that the spirit cannot remember wildness. So that we will no longer desire wildness.

It has always been this way. It must always be this way for civilization to exist.

It comes back to domestication.

But domestication is not irreversible any more than it is evolutionary. It has always been resisted by the spirits that refused to be tamed. Wild beings, human or not, have always fought against it: if not in mind and soul than in body.

This is the primal war: the refusal of life to be domesticated. It is the refusal of wildness to become ordered and civilized. It is the spirit that refuses to die.

It is not about a certain people, place or time: it is about life. Those who know that spirit without mediation have always put up the hardest fight. There was no fight or revolution for abstract ideals, for some unknown or unknowable place of undefined and questionable freedom as individuals. The fight was about something felt, something innate. The fight, then, now and always, is the rage of the spirit of life and wildness. It knows no isolation or mediation. It grows through the cracks in the sidewalk and the refusal of toxins in our bodies. It will stop for nothing and it is extremely deadly.

It is within us, anxiously waiting. It cries for the healing of the spirit (rewilding) and the healing of the body (resistance). Both are one in the same. Our deepest wound cries for healing. That is a cry for action.

 

For the nihilists and egoists, resistance comes from the immediate need to destroy what destroys you. Its only construction is in its destruction. I’m not going to say that is always a bad thing. But I will say this: I have no question in my being that there is something that I am fighting for, not just something I’m fighting against.  It is not about morality or about some lofty new age crap: it’s about something unmediated and present. Something real.

As my ideas of self and Other dissolve, I’ve come to realize that there is life in this world. I know it is interconnected. It comes through the spirit that is never dead, but it is channeled and caged by the domesticators. The end result of ten thousand years of mediation.

I know this like I know civilization must be destroyed. My spirit knows this. My spirit feels this. The spirit of all life knows this. It has always known this.

I’ve only begun to listen.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

