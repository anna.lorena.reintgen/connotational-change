      Summation
The capacity for warfare is both the ultimate justification and great curse of the state. And the problem of both war and the state is, sadly, one of the deepest tragedies of the human condition.

The following is one possible solution to the problem.

War as it has been played out throughout so much of history and into the present day accepts a basic authoritarian assumption that quite possibly accounts for almost the entire horror of warfare. That assumption is that a nation’s leadership, and by implication the individuals who are instrumental in enacting a state of war, should be immune from the fray. The idea that warfare should consist of a clashing of arms between soldiers and often the populations of the afflicted countries is accepted without question, when in truth the first front of warfare should be against the perpetrators of war themselves...its leaders, architects, and profiteers.

If Nation A declares war on Nation B, then the most astute response on the part of the population of Nation B is to target the leadership of Nation A, given their initiative in creating a state of war. Not only is this the most direct and least tragic solution, it is hard to imagine a better deterrent to war than the knowledge that those who perpetrate it will be targeted in proportion to their culpability.

The rub in the above example is that while the above actions on the part of Nation B may be the wisest for the general population, they are not necessarily the best solution for the heads of state of Nation B, since Nation A will likely retaliate by trying to target the Nation B leadership as well. Thus a certain “king’s agreement” of mutual protection, one where the leadership is not directly targeted, has reigned through so much of history. Exceptions in the modern world are mostly confined to the conclusions of full-scale wars, where one country is largely at the mercy of the other and hence not deemed a threat in its ability to respond in kind; [1] or in cases where a nation’s government is already inculcated in activities that could be perceived as evoking an assassination threat against a nation’s head of state, hence abrogating the king’s agreement.

It is important to note, that I am using the term “king’s agreement” to describe not so much a conscious awareness of the mechanism described, but what is apparently a feature of conditioning by authority that applies to all of us in accepting the status quo of war.

What is fascinating is that, while we more or less accept the true nature of war, it is not the image that is usually sold to us in the West as justification. What both the government and the media commonly do is focus upon particular personalities, usually the active or titular head of the opposing nation, as malefactor and cause for the current aggression. There is often a backdrop of a hostile governing elite, e.g., Taliban, Politburo, etc., that is painted in to complete the picture, but animosity towards entire populations is a relatively minor theme, at least in the West. [2]

The media and government don’t focus on leadership personalities primarily as an exercise in deception, but in response to what is ultimately a humanitarian impulse. Few people really want to focus on mass slaughter of the enemy population as the goal of warfare...rather, they want a limited number of specific culprits who can be blamed for hostilities. Unfortunately, even the best-intentioned modes of contemporary warfare fail to show the same extreme prejudice in choice of targets. I think we can do better. A system of war by assassination effectively rips the facade off of war in that you are actually fighting the perceived enemy, and not those under the enemy’s rule.

One of the most important features of war by assassination is its portability to the needs of stateless and even cashless societies. The issue of war by hostile states has been perhaps the hardest conundrum for primitive societies, as well as proposed anarchist societies. [3] While not necessarily a panacea for all the problems these societies might face on this front, it is far more credible an approach in facing off against the state than using conventional warfare, and much more appealing than guerilla warfare. A more decentralized society may even have a slight advantage in implementing this against a state, as the latter is relatively more dependent on a particular leadership.

What might a system of war by assassination look like?

For most nations, it would involve some kind of bounty system, usually involving the payout of very large sums of money (although still small relative to the costs of traditional modes of warfare). The sums of money would have to be substantive to warrant the effort...they should in fact be representative of the value of that individual’s death to the paying nation. Moreover, there should be ransoms of varying amounts placed on multiple key individuals within the target government or terrorist organization. Not only can this potentially cut a broad swathe through the target’s leadership, which will often be necessary anyway, but it considerably increases the likelihood that any private mercenary efforts can recoup capital investments and, naturally, be well rewarded for the considerable risk of their undertaking.

In cashless societies, the reward may involve goods like land, or such intangibles as being a national hero and the gratitude of the peoples rescued from the horrors of invasion. While this may sound ineffectual in comparison with the cash bounty system described above, cashless societies are generally far smaller in population and land area than are societies utilizing some form of cash. Hence, they tend to be much less valuable to an aggressor nation. [4]  This reduced value can balance out their reduced capacity to generate a thorough and effective response. And of course, idealism and the desire to effectively protect one’s homeland for invasion may be other relevant factors.

The existence of special forces to implement assassination policies as needed. These forces can be either governmental or private or both. Private mercenary groups that would implement specific assassination policies can be funded as necessary by speculators.

A guarantee of safe haven for individuals who implement assassination policies. This can involve disappearance via identity changes similar to those performed by the United State’s Witness Protection Program, and relocation to parts of the country or the world that would afford them minimum risk of exposure.

In the event of death while successfully implementing assassination policies, allowance should be made for the bounty to be paid to the assassin’s family or designated beneficiaries.

It is not the author’s intent to emphasize a particular approach to warfare as an adjunct to conventional warfare or warfare by any other means, but to elucidate a method that effectively targets the individuals concerned with creating war, thus giving them a powerful incentive to avoid war, including war by assassination, altogether. In upping the ante by making the act of war more personal for those involved in its initiation and maintenance, it makes the use of diplomacy and a more considered foreign policy far more appealing to a country’s leadership.  At its best, the acceptance of war by assassination is a powerful check on the growth and depredations of governments. At its worst, it is a sometimes useful tool to avoiding the needless deaths of innocents, i.e., “collateral damage,” when nations clash.

 
[1] And even these instances usually involve some form of trial, e.g., those at Nuremburg following WWII.
[2] It is, however, much more common in wars with a religious or ethnic basis, i.e., where the differences between cultures can’t be credibly ascribed to a particular leadership. Conflicts of this nature may be more resistant to resolution by an assassination approach, but the open adoption of this method as a valid means of warfare does gives the leadership a powerful incentive to forestall the breakout of hostilities.
[3] To give but one example, in The Machinery of Freedom, anarcho-capitalist David Friedman titles the relevant chapter “National Defense: The Hard Problem” to denote the very real difficulties the issue of war poses for an anarchist society, and writes “I would not try to abolish that last vestige of government” should his proposals for defending an anarchist society against a hostile state be inadequate.
[4] However, this safeguard can be overridden if any particularly valuable resource is found on the land of a primitive society that does not wish to capitalize on it, or in any small society occupying an area that attains a crucial strategic importance to much greater powers.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



For most nations, it would involve some kind of bounty system, usually involving the payout of very large sums of money (although still small relative to the costs of traditional modes of warfare). The sums of money would have to be substantive to warrant the effort...they should in fact be representative of the value of that individual’s death to the paying nation. Moreover, there should be ransoms of varying amounts placed on multiple key individuals within the target government or terrorist organization. Not only can this potentially cut a broad swathe through the target’s leadership, which will often be necessary anyway, but it considerably increases the likelihood that any private mercenary efforts can recoup capital investments and, naturally, be well rewarded for the considerable risk of their undertaking.



In cashless societies, the reward may involve goods like land, or such intangibles as being a national hero and the gratitude of the peoples rescued from the horrors of invasion. While this may sound ineffectual in comparison with the cash bounty system described above, cashless societies are generally far smaller in population and land area than are societies utilizing some form of cash. Hence, they tend to be much less valuable to an aggressor nation. [4]  This reduced value can balance out their reduced capacity to generate a thorough and effective response. And of course, idealism and the desire to effectively protect one’s homeland for invasion may be other relevant factors.



The existence of special forces to implement assassination policies as needed. These forces can be either governmental or private or both. Private mercenary groups that would implement specific assassination policies can be funded as necessary by speculators.



A guarantee of safe haven for individuals who implement assassination policies. This can involve disappearance via identity changes similar to those performed by the United State’s Witness Protection Program, and relocation to parts of the country or the world that would afford them minimum risk of exposure.



In the event of death while successfully implementing assassination policies, allowance should be made for the bounty to be paid to the assassin’s family or designated beneficiaries.

