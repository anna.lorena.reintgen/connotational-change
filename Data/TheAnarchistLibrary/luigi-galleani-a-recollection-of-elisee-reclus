
Recollect a dusty August afternoon. The oppressive and suffocating atmosphere lay heavy upon the immobile lake, scintillating as an immense sheet of polished steel. It lay heavy over the exhausted vines of the hill, invading even the penumbra of the vast study where opposite each other, we worked on some given statistics relative to the Republic of Guatemala. Moreover, as every day, he had reproached me that afternoon for having begun to work: “You need air, light, sunshine, a great deal of sunshine, much activity,” he said to me, “and the close air of the room is not at all good for you. Go away to Clarens; you will start again tomorrow morning; the work you have done this forenoon will suffice me.” But I did not like this. True, I had just returned from an incarceration in the most sombre prisons of France and I could only have benefited by the healing powers of air and sunshine, but what should I have done at Clarens, idling away eight or nine hours and could I have been able to derive greater pleasure and benefit in doing that than in these fifteen minute respites when Elisée, laying down his pen, searched for my benefit among the treasures of his recollections,or better still, driving away some doubt, rendered firmer still my intimate aspirations toward revolt? I remained, then, by his side, working or reading, sometimes interrupting unawares his feverish industry by a burning question and caressed by his simple and kindly words I drank long draughts of felicity and of joy. Why, then, should I have gone out? But that day, the servant, interrupting one of these delicious truces, brought two cards to Elisée: one from Floquet, president of the chamber of deputies, the other from Freycinet, who was then, if my memory does not deceive me, minister of war. These personages politely asked to pay their respects to the illustrious geographer Elisée Redus. “Say that Reclus cannot receive them,” he said firmly to the servant and to me, who had risen to leave the room:--“remain here. I will not receive these low creatures.” For a moment he seemed to wish to give me the intimate reason for this harsh refusal, to give vent to the bitterness which the sight of these two names, become famous, caused him, to the memories of sordidness and intrigues which they recalled to him. A slight flush overspread his face, he glanced out at the glycins bending over the ardent mirror of the lake, then, bending his pure brow over the white pages he almost inaudibly murmured: “it would be better to work.” But we were not destined to work that day. Silence was hardly reëstablished when Thérèse, the maid, reëntering the study, whispered in my ear that someone outside was waiting for me. I got up very softly, and, happy surprise, I found in the vestibule, Auguste, an excellent comrade with whom I had shared the black bread of the Republic at Mazas, at Chaumont, at Lyons. Sent back forcibly to Italy, he had set off again from Milan, pidibus calcantibus and on foot, he returned to Paris. He was then an adolescent, almost a child, but full of ardour and intelligence; years, struggles, sufferings, have happily not detracted either from his vigour or his goodness and then, as today, he was for me, a dear, a very dear, comrade. But in what a state! He had left the better part of his shoes upon the summit of Simplon. The sojourn of refugees had not been without damaging his wardrobe and on his head of hair, a la Danton, one could count almost as many straws as hairs; his sleeves were riddled with holes about the elbows and to cap the climax, his unmanageable feet protruded through the interstices of his stockings. I gave him the keys to my little room, begging him to make use of my clothespress where the clothes were at least mended and laundered. I then begged him to return as soon as possible and meet me again. We would joyously keep the rendezvous. I reentered the study.

“Anything new?” anxiously asked Elisée.

“An excellent Italian comrade who came from Milan and goes to Paris--on foot.”

“Why have you not asked him to come in?”

“Because the poor devil is in such a state!...”

“What does that matter? make him come in; it is pleasant for me to see him and to know him, since he is so young and so good.”

I had to go and look for him. Auguste had slowly climbed the hillock which led to my little hut, dragging himself along with pain and weariness. He faced about, just as he was, and there, in this vast study, the door of which was dosed, scarcely half an hour earlier, to two excellencies, two mighty ones of this world, the tramp in rags, all dusty, persecuted, smiled with joy in the arms of Elisée Reclus who besieged him with questions on the movement in Italy, the comrades in Milan, their recent struggles, their future plans, their working and living conditions, gentle as a child, affectionate as a brother, modest and delicate as are all those who are strong, all those who are great, all those who are good.

Luigi Galleani




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

