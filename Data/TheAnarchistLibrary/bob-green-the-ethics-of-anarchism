
UNLIKE CHRISTIANITY WHICH HAS ITS Ten Commandments and The Sermon on the Mount, or Communism with its Manifesto, anarchism has no single authoritative statement of its aims or values. In this lies both the strength and weakness of anarchism. Without a cast iron creed there is less risk of being wedded to dogma. There is also considerable scope for skating rapidly over thin ice and avoiding uncomfortable issues.

A perusal of anarchist writers and personal contact with those currently active within the movement gives rise to the suspicion that anarchism is all things to all men. There are pacifist anarchists and violent anarchists, atheist anarchists and Catholic anarchists, evolutionary and revolutionary anarchists, altruistic and egotistic anarchists, back-to-nature anarchists and brave-new-technological-world anarchists; there are anarchists who vote and others who marry; some who see money as the symbol of all that is rotten in our social order and others who regard it as a useful medium of exchange, not in itself evil. All use it. There are even capitalist anarchists — and there are many who contrive to make a comfortable living within the plexus of a capitalist system. There may even be some anarchists who beat their wives or children — reluctantly, we trust.

What, then, is the common ground that enables all those holding these diverse viewpoints to call themselves anarchists? At a guess there is only one principle to which all would at least pay lip service. All express mistrust of, or show active opposition to the authoritarian element to be found in any social system from the family to the State.

From this rather broad general principle stem several subsidiary principles to which most, though not necessarily all, anarchists would subscribe. There is usually a rejection of entrenched privilege, since this almost inevitably requires an authoritarian underpinning. There is also a feeling that the domination or exploitation of man by man is to be condemned, as this again presupposes an authoritarian structure to maintain the inequity. One other fundamental issue may also be found to unite by far the greater majority of anarchists. This is the rejection of Original Sin. Anarchists, on the whole, have a lot more faith in the basic worth of mankind than the guilt-laden Christians. Beyond this it would probably be impossible to obtain any widespread agreement among anarchists as to what their ideals committed them.

Stated thus baldly anarchism sounds little more than the bleat of those who are opposed to what exists without any clear idea of what to do about it. There are positive aspects to anarchism, but the more positive the measure proposed the less agreement is to be found among anarchists as to its merits. The principle of mutual aid as propounded by Kropotkin ought to command universal acceptance, but even this has its difficulties. In the first place it is little more than a vague assertion that man is a co-operative animal who finds his meaning in a social context. While this idea is both laudable and almost certainly true, it will hardly serve to distinguish anarchists from Christians or Communists, let alone from humanists, rationalists or others of a humanitarian persuasion.

In the second place, there would appear to be a section of self-styled anarchists who might find the concept of mutual aid little to their taste. These are the egotistic anarchists whose declared over-riding concern is with Number One. For this brand of anarchist mutual aid is only to be espoused insofar as it furthers the interests of the self-centred creature pursuing his narrow ends. He is concerned with opposing authority or achieving social aims only when he is directly affected. If he seeks the freedom of others it is because he sees this as a necessary condition of his own freedom. Logically, if such an anarchist were world dictator he would have arrived at his Nirvana.

He may try to escape this dilemma by avowing that he could not be happy as world dictator where other men are not free, and it is his personal happiness that he is seeking. However, this is anarchism by default, not from any commitment to anarchist principles. Given a straightforward choice between personal happiness and the happiness of others the egotistic anarchist has no scruples. It is only to the extent that the happiness of others coincides with his own well-being that he is a social animal at all. For him, then, mutual aid is a means to an end — his personal welfare. And it is only while mutual aid serves this limited end that it finds his favour. For such anarchists the answer to the question first posed is easily answered. They are not essentially humanitarian. The egotistic anarchist quite frankly doesn't give a damn for anyone but himself. His feelings for mankind and the common weal are strictly subsidiary to his self interests.

Perhaps this is not the kindest way of presenting a Stirnerite view. In some ways there is little to choose between the conscious egotist and the enlightened self-interest of the 19th century utilitarians. There is a shift in emphasis, however, in that the Stirnerite is incensed by the hypocrisy of those Puritans and do-gooders who wish to stuff their sanctimonious pretensions down defenceless throats — the "This hurts me more than it hurts you" — Sado-masochistic syndrome of the Sunday Observance misery mongers. If these and their kind would only pursue their own happiness with just half the zeal they muster to pursue the unhappiness of others the world would be a much pleasanter place for all concerned.

In sharp contrast to the egotistic type is the individual whose anarchism is also derivative, but from the opposite direction. This kind of anarchist is first and foremost a humanitarian; he subscribes to anarchism simply because he believes that personal freedom is a vital condition for human happiness. For him anarchism is again a means and not an end in itself. He differs from the egotistic anarchist in that his cardinal concern is with the welfare of mankind rather than the pursuit of personal goals. Given the choice between his own happiness and that of others he is, in principle, prepared to sacrifice his own interests to what he conceives to be the greater good.

Kropotkin and Godwin seem to have been men of this ilk. Their writings give the overwhelming impression that they are involved in mankind to a rare degree. Whereas Marx directs his moral indignation against the hated capitalist class, the humanitarians are moved by compassion for those exploited by the system. Marx sees the horrors of the Industrial Revolution in abstract terms of supply and demand, monopolies and flow of money, where the humanitarians feel for the victims and seek alleviation of their distress. Marx is hungry to believe in the cataclysmic revolution that will sweep away the tyrants; Kropotkin would prefer to believe, and Godwin did believe, that men can change their hearts and live in harmony without the benefit of an initial blood bath.

While Kropotkin and Godwin had more real love for their fellow men it must be admitted that Marx was the better scholar. This, however, is incidental. The point is that Kropotkin and Godwin represent a type of anarchist who is essentially humanitarian. Such men believe in anarchism only because they conceive that man needs freedom to be happy as he needs breath to live. Convince such an anarchist that man would be happier, more content, more at peace with himself and society, more fulfilled as an individual, under some other system — say a benevolent meritocracy — and he would be prepared to yield on his anarchist principles.

These, then, are the two main types of derivative anarchists — the egotists and the humanitarians. As a rule the egotists are more given to the apocalyptic vision, while the humanitarians are more likely to be of pacifist persuasion with an evolutionary approach. There is no logical necessity in this, though there is an emotional link; it is just that the egotist is more willing and eager to give free rein to his aggressive impulses.

For similar reasons the Sermon on the Mount anarchist is more likely to be found in the humanitarian ranks, with the militant atheist among the egotists. It is only fair to point out that most anarchists are inclined to agnosticism or plain indifference to religion, though nearly all are implacably opposed to organised religious movements with their hierarchical structure, authoritarian mood, traditional dogma, and mutilation of the young.

The third distinct group comprises what might be termed the hard-core or fundamentalist anarchists. This breed has a philosophy that is in no sense derivative. Anarchism for these folk is a faith that they will go with right down the line. If in opposing authority they risk destroying themselves, then this is a price they are prepared to pay. If the happiness of mankind is opposed to their anarchist ideals, so much the worse for mankind.

In its way this viewpoint is as ruthless as that of the egotist. If anarchism is incompatible with the modern technological society, then back to hair shirts and the primitive rural community. The argument runs that if the anarchist ideal is worth anything at all then sacrifices must be made to further the ideal. Bakunin falls fairly into this category, as do a substantial proportion of the blood and tears brigade.

Before dismissing these dedicated souls as just another brand of fanatic it is worth considering what is implied by this school of thought. Here, if anywhere, we should be able to uncover the basic tenets of anarchism. If these people are not moved by simple egotism or broad compassion, where do they find their zeal?

As far as can be made out the philosophy goes something like this: Man, the social animal, can never realise his full potential as an individual so long as he is involved in any authoritarian structure, whether as victim or oppressor. To be involved in an authoritarian system, be it religious, military, political, educational, within the family, at work or play is to accept a limitation to the growth of the individual; to be less than one might be. It is this refusal to accept the authoritarian condition whatever its benefits, material or emotional, because of the stunting of an individual's potential, that characterises the fundamental anarchist position. The central value is not the happiness of mankind nor that of the individual — it is an almost mystical belief in the individual himself. Whatever stands between the individual and the realisation of his full potential must be swept aside, no matter the cost.

But just what is this potential that an individual must be free to develop? It is here that the philosophy gets a bit woolly. Perhaps the most enlightening statement of the position has been made not by an avowed anarchist, but by Erich Fromm and Carl Rogers, both psychotherapists. In Escape from Freedom and Man for Himself Fromm discusses at length the implications of this article of faith. Rogers in Counselling and Psychotherapy and Client Centred Therapy puts forward a similar view of the nature of man. As a result of their clinical observations Fromm and Rogers believe that children grow best and patients recover best in a free social environment. By "best" is meant the development of a more adequate, diversified personality and a happier, more creative individual.

This psychological growth process they believe to be as natural and spontaneous as physical growth. All you have to do is provide the right conditions and the individual will do the rest. And the right conditions they are quite adamant, are non-authoritarian conditions. Given a non-authoritarian family background and a Froebel type, or similar free environment education, the individual will grow into a happy, creative, co-operative, good-hearted, positive social being. All the inner resources will be mobilised to make the most of life. Without such conditions the individual will, to some degree, be stunted and warped, far from happy, not very creative, co-operative to only a limited extent, evincing illwill rather than goodwill for his fellow men, destructive and negativistic. In other words, socially sick.

In their turn such individuals will pass on their disease to those with whom they are in contact, particularly their children, who will react negativistically to reinforce the symptoms. Unhappiness begets unhappiness, illwill begets illwill, and so on. The victim is caught in a vicious circle and compulsively forges his chains day by day. Yet, all the time within him is a yearning for happiness, creativity, a striving for acceptance and love. The victim wants to be wanted, but cannot set in motion the wheels that will release him from his bondage to the past.

To reverse this malignant process a special set of therapeutic conditions is necessary. The patient, as he has become, is provided with a benign environment in which he is accepted without question, without condemnation, for what he is. If he confesses to having put the dog through the mangle that morning, then the therapist controls his own feelings of horror, and expresses only interest in the why and wherefore of such aberrant behaviour, encourages the patient to talk, to put his point of view, to reveal the emotional content behind the action.

Within this extremely permissive atmosphere the patient has a chance to find himself, to examine and understand the springs of his own conduct, and eventually to shed the straightjacket of his past. Like Brutus he learns to look inside himself for the key to his fortunes. He assumes responsibility for his own conduct; takes command of his own life; learns to believe in himself again.

So, if the psychotherapists are right, anarchists have spotted something about the nature of man previously overlooked by other schools of thought. Man is not by nature steeped in Original Sin, nor is he simply an economic animal. Basically, he is driven neither by guilt nor greed but by an overwhelming urge to grow, to diversify, to make the most of himself as an individual in a social context. He is driven towards the stars by something inside himself that will not accept limitations.

Society as currently structured does not make it easy for him to pursue this course. Family, school, church and job often conspire to frustrate his vital urge to grow — precisely because of their authoritarian structure.

All the time he is consciously or unconsciously seeking ways round and through these artificial barriers to growth. Where the barriers prove too strong the pent up energy may eventually break with explosive force into criminality — the individual's protest, or war — the bursting of a whole society's abscess.

Anarchism, by recognising this basic urge to growth within the individual, draws attention to those aspects of the social system that thwart or warp such growth. Anarchism is not concerned with specifics such as monogamy versus polygamy or polyandry. All it insists on is that the family, whatever else it may or may not be, must be non-authoritarian in its structure. It is not concerned with whether children should be taught arts or science subjects at School, only that the school should be non-authoritarian in outlook. It is not, in any essential sense, opposed to religion — only to religious bigotry. And in capitalism, socialism and communism it sees the same fault — all are authoritarian and all restrict the growth of the individuals trapped within them.

Here, then, is the basic article of faith of the hard core anarchist; man can discover what is best in himself only under non-authoritarian conditions.

It is easier to see now why anarchism appears at first glance to offer so little by way of positive content. Its basic premiss provides a touchstone for deciding personal conduct, but does not lead with logical necessity to any particular social system. It tells us what is wrong with established systems without providing a blue print of the ideal society. It can tell us only that the ideal society must be non-authoritarian — and this condition could hold for a diverse number of societies that differ in their family traditions, educational systems, religious beliefs and economic structure.

In passing it should be pointed out that in attempting to analyse the value systems of anarchists it is not suggested that they can be sorted into three neat piles, egotistic, humanitarian and hard core. Many anarchists, possibly most, have never bothered to consider to what extent their anarchism is based on self interest, a love of mankind, or an article of faith concerning the nature of man. Elements of all three may probably be found in various proportions in all anarchists.

Quite complex arrangements of these values are possible. An anarchist may be essentially humanitarian in his dealings with his fellow men while being more ruthless with himself. He might, for instance, refrain from encouraging some young person from breaking with an authoritarian family because of the ensuing unhappiness, while having been quite prepared to make such a break himself, and damn the consequences. That is, he is prepared to stand on his own feet, come hell or high water, while recognising that others may not be able to find sufficient strength within themselves under the same circumstance.

There is a wider issue involved here. Anarchists on the whole are more willing to face up to the shortcomings of society, less gullible regarding patriotism, church-going, marriage, prisons and the thousand and one social institutions accepted without question by the vast majority of their fellow citizens. There is a ruthless pursuit of truth with regard to society to be found elsewhere only among professional social anthropologists as a rule.

It does not follow that anarchists are any more willing to face up to the truth about themselves. On the contrary, most have learned to externalise their aggression, finding fault with society rather than burdening themselves with a sense of inadequacy or guilt. This is not to suggest that in choosing to debunk the holy cows worshipped by others, anarchists have found a comfortable resolution of personal problems. They have in fact chosen to reject the bogus values of present day society the hard way. Little comfort or support can be expected from their family, workmates or other associates. This in turn exposes the anarchist to the dangers of a holier-than-thou attitude. Having suffered and been shriven in the pursuit of social truths the anarchist is all too prone to the temptation of parading his unpalatable discoveries before unwilling victims. Moral indignation is all right as steam in the boiler, but it makes a dangerous star to steer by.

Which brings us to the crux of a moral dilemma faced by any humanitarian, anarchist or otherwise. Many, if not most, people prefer happiness to truth. A few will pursue truth wherever the trail may lead and whatever the cost. A worthy, even heroic, stand to take — provided the pursuer is the one who suffers in the cause. But what if, as a result of pursuing truth, others are made to suffer in a cause not of their choosing? Noble self-sacrifice is in danger of degenerating into the cruel imposition of suffering onto others less fitted, perhaps, to survive the onslaught. The medical practitioner has long since learned that the last thing most of his patients want to hear is the clear, unvarnished truth. Some, of course, are motivated less by sympathy than by a desire to play God — the omniscient Almighty who dispenses only as much information as he thinks you are fitted to receive. Nevertheless, many people would much prefer not to be told that they are about to shuffle off this mortal coil, and to impose the painful truth would be a heartless addition to their misery.

There is a multitude of other truths, too, that come too near the knuckle for comfort. Self knowledge and happiness are all too often incompatible; and who is to say which is the "right" choice for others? An anarchist may prefer the cold light of reason, but he is in no better case than the Sunday Observance fanatic when it comes to justifying scourging of the innocents in the name of the cause they do not espouse.

Similarly, most people would appear to place a sense of security higher than a need for personal freedom. Anarchists may deplore this, and even marvel at the perfidy of their weaker brethren, but the fact remains that most people do not share the anarchist's appetite for freedom to the extent of wishing to make the sacrifices involved. It follows that if anarchists are humanitarians then they will insist on paying the price for freedom themselves, but will leave those who prefer their chains to their own devices.

The only snag with this argument is that many anarchists suspect that freedom, like peace, in indivisible. In which case others must be made free, like it or lump it. The system that enslaves those who prefer enslavement also enslaves both anarchists, who would choose otherwise, and children, who will form the next generation of emotional cripples.

Hence the moral dilemma. Whatever happens someone is going to get hurt. All the humanitarian can do is to weigh up the issues involved on each specific occasion and decide whether and where to throw his weight into the balance. The average bonehead, for example, seems quite content with the laws on abortion and homosexuality in this country, despite the fact that these laws seem designed to ensure the maximum amount of misery for all, and happiness for none. On these particular issues there is no doubt where you will find the anarchists — which, as it so happens, is where you will also find the humanitarians.

Not all issues, however, are anything like so clear cut. Such vicious laws are readily opposed because the suffering is universal and not confined to the masochistic pea-brains who support them. But what of the law relating to drunken driving? As things stand the abolition of this law would undoubtedly lead to an increase of slaughter on the roads. It is here that the humanitarian and hard core anarchist part company. And also where the hard core anarchist gets dismissed as a crank by many who are otherwise sympathetic to anarchist ideals. This does not prove that the hard core anarchist is wrong — only that he is willing to pay a far higher price for his personal freedom than the vast majority. At least, he says he would pay this price, but one wonders if a lively encounter or two with drunken motorists would modify his ardour. A broken limb, loss of sight, or death of his child might make the price seem excessive.

Anarchists face another dilemma with regard to the role of violence in their scheme of things. A resolution of differences by the use of violence is, by definition, an imposed settlement. Yet, anarchism by its very nature is committed to non-authoritarian solutions. Hence, it may be argued, the anarchist is precluded from the use of violence in promoting his ideals, as this would involve repudiating his basic premiss. On these grounds the humanitarian, the pacifist, and the evolutionary anarchist find common cause in rejecting the proposition that a free society can be brought about by violent revolution. The end precludes such means. Governments may be overthrown in a matter of hours, but the hearts of men do not change overnight. A free society presupposes men nurtured in freedom. The present generation has acquired a taste for its chains and wouldn't give a thank you for the sort of society envisaged by anarchists. It follows that the revolutionary dream would prove to be a nightmare. There are no short cuts to the free society. The problem is basically educational, and the process is inevitably a long one. The most that can be hoped and worked for is that the next generation will be less authoritarian in outlook than the present one.

This is a gradualist point of view, held in contempt and vilified as "reformism" by the revolutionary anarchist, usually a hard core specimen, sometimes an egotist. There is a powerful counter-argument to thoroughgoing pacifism. Violence can, in the long run, be met effectively only by violence. Gandhian passive resistance, the usual alternative offered by pacifists, is a technique with only limited application. It worked in India only because the British were not willing to go the whole way against the courageous men and women who lay on the railway tracks. It could not, and did not, work in Nazi Germany. The ghosts of an army of Jehovah's Witnesses bear silent testimony to this unpleasant fact. Their passive resistance led them straight to the gas-chambers. Hitler recognised only one argument — might is right.

The revolutionary anarchist then points out that Hitler was simply an extreme example of the authoritarian in naked action. All governments are fundamentally authoritarian. They believe in and rely on the threat of violence to maintain their position. Their police and soldiers are trained in violence and will attack anyone designated as an enemy by those in power, be they CND passive resisters or colonial peoples struggling for national independence. And, again, the only argument with meaning in these circumstances is the one conducted in the language of violence. Those in power will not yield their power and privileges without a fight. So, eventually, like a good Marxist, the lover of freedom must be prepared for the violent uprising which holds out the only hope of sweeping away the armed citadels of entrenched privilege.

The main drawback to this argument is historical fact. When oppressive governments have been swept away by armed revolt the outcome has often turned out to be quite as unsavoury as the original evil. One authoritarian regime is ousted and another rises from the ashes. The net result — a pile of corpses, lots of work for the artificial limb industry, and a new set of backsides in the seats of power. Ride the tiger, and you'll end up inside it.

Nevertheless, there have been revolutions that on balance seem to have been justified, and without doubt there have been cases where the radical and violent course would have saved mankind a lot of unnecessary suffering. The greater happiness of a large section of mankind, for example, would almost certainly have been served had someone had the nerve and foresight to pop a bomb in Hitler's pyjamas in the early 1930's. And a similar kind of service would have done Torquemada a power of no-good.

Where the evolutionary and revolutionary anarchists fail to agree is on the question of where to draw the line. When in doubt the evolutionary anarchist prefers a cautious "wait and see" policy, on the grounds that to incur a very certain evil in the name of a very speculative good is a transaction of dubious worth. In the same circumstances the revolutionary anarchist displays less patience and more panache. Who is in the right on any given occasion would appear to be largely a matter of opinion, and what you care to believe largely a question of temperament. Even the most pacific humanitarian with a utilitarian ethic will agree, however, that there comes a time to dig your heels in and fight it out. This is when the very certain immediate evil follows from pacifism — as with the gas chambers.

So much for the inner conflicts of the humanitarian cum hard core anarchists. Other forms of heart searching are just as complicated. An anarchist may recognise in himself a large egotistical streak without being proud of it. That is, part of his motive in pursuing anarchism is pure self-interest, but this for him is not what justifies his belief in anarchism. He may see such egotism as ancillary to his basic belief, possibly irrelevant, possibly as a personal weakness opposed to what he really wants to stand for.

On the other side of the coin, the egotistic anarchist who makes a song and dance about his dedication to self interest may be covering up humanitarian feelings which he fears may be taken as a sign of weakness, exposing him to exploitation by leeches of one kind or another. Or he may quite simply abhor the idea of being taken for a humbug.

And so on. The permutations are as many and diverse as there are anarchists. They are united only in their opposition to authoritarian systems. As a philosophy anarchism is hardly more systematic or less emotional than existentialism and nihilism, with which it has historical links. As a movement it can never sweep the country like Protestantism or Socialism as it has no blueprint, no rallying point, no central organisation, no leader to direct and channel the social forces it wishes to arouse. The most effective anarchists have either been propagandists, like Kropotkin, or pioneers in the educational field like Homer Lane and A. S. Neill. In the industrial field neither syndicalism nor mutual aid has fired the imagination of any significant proportion of the population. So far from being interested in workers' control, the average worker cannot be bothered to take an active part in Union activities.

Individuals can solve this problem by becoming self-employed, but as our industrial units become still larger and more complex the prospects for syndicalism become yet more remote. Which may help to explain why the average sort of bloke finds anarchism as pie-in-the-sky as any other religious vision.

However, even in the industrial field things are not as gloomy as they might appear. The social sciences lend support to the anarchist point of view, and it is only a question of time before we begin to apply what we have learned and are learning about the social needs of man to education, family life, and industrial organisation.

In the meantime anarchists can continue to protest against the authoritarian aspects of all our social institutions. By propaganda they can present their ideas as clearly and cogently as possible. By modifying the institutions whenever they have the chance they can demonstrate a better way of doing things. By their day-to-day behaviour and personal contact with other people they can display the more intimate social consequences of the non-authoritarian viewpoint. They cannot change the educational system of this country overnight, but they can easily make sure that their own children are not beaten at school, just as they can refrain from using this primitive argument at home.

By exposing the shortcomings of authoritarian pseudo-solutions to social problems they can hope by precept and action to strike the same spark of protest off in those who long since gave up hope. When enough people have seen through the swindle of authoritarian systems clearly enough to feel cheated themselves, then it won't matter whether they vote with their hands or their feet. One way or another society will just have to move in an anarchist direction.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

