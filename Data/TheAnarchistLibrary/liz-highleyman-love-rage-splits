      Accusations of Trotskyism      "Fucking Shit Up"      Mutually Incompatible      Love & Rage Splits      "A Classic Leftist Coup"
Love & Rage (L&R), a continental anarchist organizing and newspaper network, underwent a major split at its annual conference in San Diego last July as a result of long-standing internal differences concerning structure and goals. The debates which brought the four-year-old network to a crisis point reflected conflicting ideas about contemporary anarchist activism.

Several people who left L&R plan to initiate a communication network promoting mutual aid among local anarchists, while those who remain have created a more formal organizational structure and changed its name to the Love & Rage Revolutionary Anarchist Federation (L&R-RAF).

Love & Rage began as a newspaper after preliminary meetings at the 1988 Toronto anarchist gathering, and finalized in 1989 at the San Francisco gathering. As time went on, L&R grew beyond the paper and began to take on other projects such as the Anti-Racist Summer Project, support for political prisoners, and anarchist contingents at national marches.

The network's decision-making structure included a facilitator responsible for overall coordination, a ten-member Coordinating Group (CG) elected by an annual conference to make editorial and project-related decisions, and a Network Council (NC) made up of delegates from each participating local group. A Production Group (PG) in New York City selected articles and produced the paper; a PG in Mexico City produced a Spanish section for the last four months.

From its inception, a core group of people involved with L&R provided the bulk of time, effort and money needed to publish the newspaper and carry out its other projects. They initially included members of the Revolutionary Anarchist Bowling League in Minneapolis (including the primary initiator and first facilitator, Chris Day), some former members of the defunct Revolutionary Socialist League (RSL) who turned over their office and printing facilities to L&R, and several other independent individuals from around the country.

Because of its RSL connection, accusations of Trotskyism have followed L&R from its inception, although the vast majority of participants over the years had no connection with any sectarian group, and in fact, many were probably unaware of L&R's history. Serious concerns about L&R have been aired in letters and articles in The Fifth Estate and Anarchy over the years, with critics charging that L&R was an attempt by a small group to build a formal organization promoting a specific political program and to increase their power and influence within the anarchist movement.

Many people became involved with L&R believing they would have equal influence in shaping the project, and could help move it in the direction they felt fit their vision of anarchism. As far as I can tell, there never was a clear consensus about what the L&R network was trying to accomplish. It's apparent, however, that many participants did believe such a consensus existed, although there were different understandings of what that consensus was.

Several core participants envisioned a more formal organization with a well-defined mission and set of political principles which could develop and disseminate an anarchist analysis of current issues and provide the nucleus of a revolutionary movement.

For example, Todd Prane, current staff person of the new L&R-RAF stated: "L&R was formed for a particular segment of the anarchist movement...anarchists who are in favor of organization and the critical analysis and construction of it, who want to work for revolution in our lifetime...."

I joined L&R with the idea it was an open network whose primary purposes were to improve communications among anarchists, disseminate information about anarchism, and help facilitate locally-initiated projects and actions. I joined the Network in 1991 and was subsequently elected to two terms on the CG, but will not be a member of the reorganized L&R-RAF.

The U.S. hardly seems on the brink of revolution and any revolution that is not supported by a large segment of the population is by definition vanguardist and authoritarian. Many participants in the network favored a decentralized, bottom-up approach and envisioned L&R as a communication and mutual aid network, and criticized what they saw as attempts at top-down organizing.

There was extensive political debate around these and other issues within the network since its inception. At a 1991 Minneapolis conference, differences were quite apparent regarding a proposed statement of political principles (both whether such a statement was necessary or even possible, and about the actual content of the statement). A fairly basic, compromise political statement was adopted which included positions such as anti-statism, anti-racism, anti-sexism, pro-ecology and pro-queer liberation. Some people felt this "laundry list" approach to politics was misguided since some worthy cause would inevitably be omitted while others felt a political statement was necessary to let people know what the network stood for.

There also were other issues of concern such as the focus on militant activism, which sometimes seemed to lack much purpose other than "fucking shit up." Some believed such a focus was necessary, while others wanted at least as much emphasis on the creation of counter-institutions (making the state obsolete through self-help and community autonomy) as was given to the destruction of the status quo. There were disagreements about whether to focus on Black liberation and whether an anti-sexist position implied opposition to pornography. Another area of contention within L&R has been persistent tensions regarding the level of support for national liberation movements and the apparent endorsement of Marxist/Leninist organizations and actions.

Over the course of the project there have been concerns about issues of power and privilege. There were ongoing tensions between more and less economically advantaged participants, and between younger and older participants. There also were arguments over issues of theory vs. practice (which I believe to be a spurious dichotomy) and debates about the level of intellectualism and the use of opaque theoretical language, both in the paper and in political discussions at conferences and among the PG.

Unsurprisingly, the PG bore the brunt of these tensions (which had personal as well as political manifestations). It seemed to me (a non-PG member) that PG members were expected to give their lives to the L&R project, sometimes spending 24-hour days at the office and going into serious personal debt. Interpersonal relationships deteriorated noticeably under such constant strain.

Activists need to take care of themselves and interpersonal relationships should reflect political values. Expecting people to sacrifice friendships, sleep and outside activities to the L&R project is a sure way to rapidly burn out people. Some were willing to give this level of single-minded devotion, and as a result gained more power and influence within the project.

At the Atlanta conference in 1992, some persistent differences concerning structural issues were brought up. Several members of the PG, CG and others wanted to institute a more formal membership status as a basis for participation in the network. Their argument was that many people showed up to annual conferences, took part in the decision-making, then did nothing to help carry out decisions. It was argued that people who contributed the most time, effort and money to make the network happen should have the most to say in shaping its direction not just those with the time and money to attend conferences. Others were strongly opposed to formally defined membership and felt anyone who sincerely wanted to be a part of the project should be able to, and everyone should have an equal say in major decisions at conferences which were open to all. Some strongly opposed tying membership to a financial commitment or adherence to a political statement.

It was becoming clear L&R was working from two mutually incompatible models. Some people began trying to move L&R openly toward being a more grassroots-based, decentralized network, while others wanted a more formal organization with a clearly defined set of political goals and strategies. Sentiment seemed roughly evenly divided.

Given the lack of consensus about which direction to go, the network more or less came to a standstill. Decisions were not made, communications broke down, and efforts to distribute tasks away from the New York office did not work out well. The impasse prevented both the "pro-organization" and the "decentralization" advocates from moving ahead with what they wanted to do.

In Spring 1993, a draft letter, entitled "Five Concerns," was circulated among select participants, signed by 22 people, and published in the L&R Discussion Bulletin only immediately prior to the San Diego conference. The letter called for formally defined membership and a political statement. Only those who agreed with these positions were shown the draft, and the fact that it was being circulated at all was not made known to members of the PG and CG and others who held opposing views, This, combined with ongoing personal and political difficulties, led four members of the PG (including one of the two co-facilitators elected in Atlanta), to leave the project.

At the San Diego conference in July, tensions between the "pro-organization" and the "decentralization" camps came to a head over the issue of membership, but this issue was reflective of differing views on overall organizational strategy and revolutionary goals. It was suggested these underlying political differences should be discussed at greater length rather than buried under an organizational detail, but almost everyone was eager to get things resolved one way or the other, once and for all.

Several people on both sides made it clear they would leave the network if the membership decision did not go their way. The conference attendees seemed evenly divided over the issue and it was clear to most that a genuine consensus was not achievable. Supporters of the membership resolution included most of the New York and Mexico City PGs and the producers of the Discussion Bulletin in Minneapolis, and a group that proposed to open a new office in Oakland. Faced with this, those who disagreed "stood aside" and allowed the proposal to pass. "Pro-organization" advocates retained the L&R infrastructure and production facilities. The name was changed from the Love & Rage Network to the Love & Rage Revolutionary Anarchist Federation to acknowledge the shift in emphasis and take into account the views of those who felt an organization was not the same thing as a network. Membership will be defined by general agreement with the stated politics of the Federation, identification as a member, and payment of a waivable $25 yearly fee.

Various participants summed up the outcome of the charges in different ways. From Todd Prane, of the new federation: "There already exists an informal network of anarchists in North America...That was not the gaping hole that people who supported L&R were interested in working on...There was no national coordinating presence of anarchists that was able to address politics at a national, continental and international level. That is what we are working towards." In the opinion of one decentralist from Berkeley: "I oppose this move because I feel it continued and strengthened the top down approach to movement building. A revolutionary anarchist movement will be best served by working on regional information networks which facilitate local groups working together."

In effect, Love & Rage has been pulled back into line with what was envisioned by those who initiated the project, people who have over the years been among the most vocal and influential participants. As is often the case in such shake-ups, those who were less influential, less vocal or less in-the-know about the history of the project ended up leaving. Some felt forced out because they did not have an equal voice in shaping the project to reflect their goals.

Some participants did not disagree with the "pro-organization" direction, but rather opposed what they saw as the manipulative way in which the reorganization came about.

Former co-facilitator Tommy Lawless expressed these sentiments in her letter explaining why she will not be part of the new L&R-RAF: "Forcing a vote on membership without a collective discussion [on the function and goals of the Network] was a sinister way of skirting the real issues, of forcing the goals of one faction onto the whole group...The participatory decision-making process for the Love & Rage Network has always been at the very heart of its anarchist politics...It is vile that this [membership] proposal was passed by a minority--not even a majority of 51%. Shoving this proposal through in this way destroyed the Network and everything the Network stood for...What happened was nothing less than a classic leftist coup. A small minority of people came in with their agenda, got their way, and went home with all the goods, leaving everyone else out in the cold."

As I see it, the reorganization of L&R is a positive development. Those who want a more formalized organization with well-defined membership criteria and political principles can carry on with that type of project. Those who seek a more loosely-defined network focused on communication and skill-sharing can move forward in that direction. Neither side will impede the other in the pursuit of their goals. While there is a certain amount of bitterness between the sides, there is not as much animosity as one might expect.

Current L&R-RAF projects included a day of anti-fascist actions on the anniversary of Kristallnacht, an International Day of Action Against Immigration Controls/Anti-Immigrant Violence planned for May 9, 1994, a poster campaign and actions opposing police brutality. The L&R newspaper will continue to be published in New York, a new Federation office has been set up in Oakland, and an active group in Mexico is producing the (now separate) Spanish paper Amor y Rabia. For information contact L&R-RAF, PO Box 853, New York, NY 10009.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

