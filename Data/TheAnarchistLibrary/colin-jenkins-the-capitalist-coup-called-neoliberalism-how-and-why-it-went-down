
Rich people have always had class consciousness because... they want to stay rich. This collective consciousness led the "founding fathers" of the United States to set up systems of governance that would, first and foremost, protect them (the wealthy, landowning minority) from the landless, working majority (slaves, indentured servants, laborers). Since then, the rich have had undue influence on every aspect of US life: housing, food production and distribution, education, media, and politics. As capitalism has developed well into its late stages, this has led to large concentrations in wealth and power, and thus influence.

In order to maintain control, the rich have learned over time that minimal concessions must be given to the working class to avoid societal unrest. Marxist theorists like Antonio Gramsci and Nicos Poulantzas described this process as using the state to steady the "unstable equilibrium." This instability is produced by capitalism's tendency to pool wealth at the top while dispossessing the majority. For much of the 20th century, capitalists in the US were successful in maintaining an internal equilibrium, mainly due to their ravaging of the so-called "third world" through colonialism and imperialism. With this massive theft of resources throughout the global South (Africa and Latin America), a robust "middle class" was carved out from a mostly white sector of the US working class. This "middle class" consisted of workers who were provided a greater share of the stolen loot than their class peers, and thus awarded the "American Dream" that was widely advertised.

The US "middle class" was a crucial development for the rich because it provided a buffer between them and the masses. Due to the relative comfort they were allowed, "middle-class" workers were more likely to support and collaborate with capitalists, even at the expense of their fellow workers who were left struggling for scraps from below. After all, for there to be a middle class, there must be a lower class. Under capitalism, the lower class is the invisible majority by design. The capitalist class shapes dominant culture from above, the middle class serves as the standard bearer of this culture, and the lower class clings to survival mode in the shadows of society. The key for the rich is to keep the invisible majority in check. The "middle class" has always played a crucial role in this.

Despite this balancing act that was maintained for decades, capitalism's internal contradictions became predictably volatile heading into the latter part of the century, culminating into what economist Michael Roberts refers to as the profitability crisis of the 1970s . As the capitalist system was approaching this crisis, US society had already begun confronting social ills stemming from systemic white supremacy, patriarchy, and the Vietnam war. Naturally, this moved into the economic sphere, as workers and students began to successfully tie together the array of social injustices to the widespread economic injustice created by the capitalist system. The existence of an invisible majority, the victims of capitalism and its corollary systems of oppression, was uncovered. This scared the rich, enough to where they felt the need to fortify their previously unshakable privileges. After the groundswell of liberation movements that formed during the 60s, which was fueled by a wave of (working) class consciousness from below, the rich decided to organize and weaponize their own (capitalist) class consciousness to protect their assets, collectively, from the threat of democracy.

In examining what had gone wrong in the 60s and why so many people had the audacity to demand more self-determination, the notorious Trilateral Commission convened in 1973, bringing together economic and political elites from North America, Europe, and Japan. The Commission, as described by Encyclopedia Britannica , "reflects powerful commercial and political interests committed to private enterprise and stronger collective management of global problems. Its members (more than 400 in the early 21st century) are influential politicians; banking and business executives; media, civic, and intellectual leaders."

In 1975, Michel Crozier, Samuel P. Huntington, and Joji Watanuki published a report for the Commission, titled: "The Crisis of Democracy: On the Governability of Democracies." In assessing the various movements that gained momentum in the 60s (racial justice, economic justice, anti-war, etc.), the report determined that these "problems" stemmed from an "excess of democracy." Huntington specifically noted that, "the vitality of democracy in the United States in the 1960s produced a substantial increase in governmental activity and a substantial decrease in governmental authority." The solution to this, according to the report, was to reverse direction - decrease "governmental activity" and increase "governmental authority" to restrict democratic impulses from the masses and maintain the capitalist power structure internally, while retaining "hegemonic power" internationally. In other words, rather than government serving people and regulating capitalists, government should serve capitalists and regulate people.

Since maintaining a "middle class" had become such a fragile proposition, the capitalist class forged a new direction. Rather than rely on this historical buffer and continue the concessionary and fickle balancing act , they decided it would be more effective to simply take ownership of the legislative and judicial process. This process began when executive officers from several major corporations joined together to form private groups like the Business Roundtable, for the purpose of "promoting pro-business public policy." In other words, to make sure that the "excess of democracy" which occurred during the 60s would never return. Why? Because any such mass movement toward relinquishing power to the people is a direct threat to capitalist profit and corporate America's existence as a collection of unaccountable, authoritarian, exceptionally powerful, private entities. The Business Roundtable, which included executives from corporations like Exxon, DuPont, General Electric, Alcoa, and General Motors, gained instant access to the highest offices of the government, becoming extremely influential in pushing for corporate tax cuts and deregulation during the Reagan era.

Since the 1980s, the Business Roundtable has run roughshod over American workers by using the federal government to:

reduce consumer protections,

obstruct employment stimuli,

weaken unions,

implement "free trade" agreements that spur offshoring and tax havens,

ease environmental protections,

increase corporate subsidies,

loosen rules on corporate mergers and acquisitions,

open avenues of profit in the private healthcare system,

privatize education and social programs,

and block efforts to make corporate boards more accountable.[1][2][3][4] [5]

As political momentum developed within corporate America, additional players jumped aboard this strategic and highly coordinated capitalist coup. While groups like the Business Roundtable targeted legislation, the US Chamber of Commerce (CoC), a "private, business-oriented lobbying group" which had already served as a popular vehicle for turning (capitalist) class consciousness into action since 1912, shifted its focus onto the court system. Since then, the CoC has used its immense resources to influence US Supreme Court decisions that benefit big business, a tactic that has become increasingly successful for them over time. The CoC's business lobby had " a 43 percent success rate from 1981 to 1986 during the final years of Chief Justice Warren Burger's tenure," a 56 percent success rate from 1994 to 2005 (the Rehnquist Court), and boasted a 68 percent success rate (winning 60 of 88 cases) during John Roberts first seven years as Chief Justice. The CoC improved even more on its pro-corporate, anti-worker attack in 2018, winning 90 percent of its cases during the court's first term. As Kent Greenfield reported for The Atlantic ,

"One measure of the [2018 term's] business-friendly tilt is the eye-popping success rate of the U.S. Chamber of Commerce, the self-proclaimed "Voice of Business." The Chamber filed briefs in 10 cases this term and won nine of them. The Chamber's victories limited protections for whistleblowers, forced changes in the Securities and Exchange Commission, made water pollution suits more difficult to bring, and erected additional obstacles to class action suits against businesses. Only the geekiest of Supreme Court watchers monitor such cases. But the Chamber pays attention, and it pays off."

Groups like the Trilateral Commission, Business Roundtable, and Chamber of Commerce have taken prominent roles on the front lines of the 40-year, capitalist slaughter of American workers, but if there was a single, powerful element that solidified this coup it was a memo written in 1971 by Lewis Powell. The Powell Memo, or Powell Manifesto, as it has come to be known, made its rounds among corporate, economic, and political elites during this crucial time. Powell, a corporate lawyer, board member of nearly a dozen corporations, and soon-to-be Supreme Court Justice, sent the memo to the Director of the U.S. Chamber of Commerce, Eugene Sydnor, Jr., as a call to action for corporate America.

Powell's memo was a diatribe against any and all elements that would dare to question capitalism. While giving mention to "Communists, New Leftists and other revolutionaries who would destroy the entire system, both political and economic," the memo focused on what was viewed as the most immediate threat - the same "excess of democracy" referred to in the Trilateral Commission's report. "What now concerns us is quite new in the history of America," wrote Powell. "We are not dealing with sporadic or isolated attacks from a relatively few extremists or even from the minority socialist cadre. Rather, the assault on the enterprise system is broadly based and consistently pursued. It is gaining momentum and converts" throughout the working class. Powell took special interest in those "from the college campus, the pulpit, the media, the intellectual and literary journals, the arts and sciences, and from politicians" whom he regarded as small in size but "the most articulate, the most vocal, the most prolific in their writing and speaking."

Powell's memo laid out a blueprint for the capitalist coup that is now referred to as neoliberalism , including everything from identifying and listing the enemies pushing for self-determination, criticizing the business community for its apathy and lack of urgency in recognizing this growing threat, suggestions for how business executives and the Chamber of Commerce may proceed in obstructing these democratic impulses from below, and even laying out detailed plans on how to infiltrate campuses, the public, media, the political arena, and the courts with pro-capitalist action and propaganda.

Reclaim Democracy, an activist organization based in Montana explains,

"Though Powell's memo was not the sole influence, the Chamber and corporate activists took his advice to heart and began building a powerful array of institutions designed to shift public attitudes and beliefs over the course of years and decades. The memo influenced or inspired the creation of the Heritage Foundation, the Manhattan Institute, the Cato Institute, Citizens for a Sound Economy, Accuracy in Academe, and other powerful organizations. Their long-term focus began paying off handsomely in the 1980s, in coordination with the Reagan Administration's "hands-off business" philosophy."

At a time of monumental capitalist regrouping and coalescing against the "dangerous rise" of self-determination, the influence of Powell's manifesto is difficult to underestimate. It provided ideological fuel to the birth of a substantial corporate lobbying industry, which produced immeasurable pro-business and anti-worker legislation for decades to come. The memo also served as a wake-up call to capitalists throughout corporate America, supplementing the formation of groups like the Business Roundtable and urging forceful actions from the US Chamber of Commerce. The results, according to Jacob S. Hacker and Paul Pierson, were undeniable:

"The organizational counterattack of business in the 1970s was swift and sweeping - a domestic version of Shock and Awe. The number of corporations with public affairs offices in Washington grew from 100 in 1968 to over 500 in 1978. In 1971, only 175 firms had registered lobbyists in Washington, but by 1982, nearly 2,500 did. The number of corporate PACs increased from under 300 in 1976 to over 1,200 by the middle of 1980. On every dimension of corporate political activity, the numbers reveal a dramatic, rapid mobilization of business resources in the mid-1970s." [6]

The real-life effects of this capitalist coup have been disastrous for most. US workers have experienced declining or stagnant wages since the 1970s. As a result, many must rely on credit (if lucky enough to qualify) even to obtain basic necessities, which has resulted in skyrocketing household debt across the board. The debt-to-disposable income ratio of American households more than doubled from 60% in 1980 to 133% in 2007. Meanwhile, any hope of saving money has disappeared. While the household "savings rate roughly doubled from 5% in 1949 to over 11% in 1982, it looks like a downhill ski slope since then," and registered in negative territory by 2006. Conversely, as designed, the rich have benefited immensely, to the point where income inequality has increased to pre-Great Depression levels. Those who orchestrated the coup (the top 1%) claimed about a quarter of all wealth during the 1980s, and now own over 40% of all wealth in the country. To put this in perspective , the bottom 90% of all Americans combined account for barely half of that, claiming 21% of all wealth.

And, perhaps most importantly, the coup helped fund the growth of a massive capitalist propaganda machine to convince the working class to support our own demise. This includes everything from a co-opted and recalibrated liberal media, a rise of right-wing talk radio, and the birth of the Fox News network - all designed to do one thing: "inform and enlighten" workers on the wonders of capitalism and American exceptionalism, the friendly nature of big business, and the "excessive" dangers of self-determination.

As Powell noted in 1971, "If American business devoted only 10% of its total annual advertising budget to this overall purpose (of marketing and selling the idea of capitalism), it would be a statesman-like expenditure." And statesman-like it has become, running interference and garnering " manufactured consent" for a capitalist coup that has been cemented over the course of four decades, six presidential administrations, a Wall Street run amok, and a massive transfer of generations (including future) of public revenue into private hands.



Notes
[1] "The Business Roundtable and American Labor," a report by J. C. Turner, General President International Union of Operating Engineers, AFL-CIO (May 1979). Accessed online athttp://laborrising.com/2013/07/union-organizing-and-the-business-roundtable-and-american-labor/
[2] "The Anti-Union Game Plan," Labor Notes (July 2, 2018). Accessed online athttps://labornotes.org/2018/07/anti-union-game-plan
[3] Lafer, G. (October 31, 2013) "The Legislative Attack on American Wages and Labor Standards, 2011-2012," Economic Policy Institute. Accessed online at https://www.epi.org/publication/attack-on-american-labor-standards/
[4] Gilbert, D. (2017) The American Class Structure in an Age of Growing Inequality (SAGE publications)
[5] Goldfield, M. (1989) The Decline of Organized Labor in the United States (University of Chicago Press), p. 192
[6] Hacker, J.S. & Pierson, P. (2011) Winner-Take-All Politics: How Washington Made the Rich Richer - And Turned Its Back on the Middle Class (Simon & Schuster)




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



reduce consumer protections,



obstruct employment stimuli,



weaken unions,



implement "free trade" agreements that spur offshoring and tax havens,



ease environmental protections,



increase corporate subsidies,



loosen rules on corporate mergers and acquisitions,



open avenues of profit in the private healthcare system,



privatize education and social programs,



and block efforts to make corporate boards more accountable.[1][2][3][4] [5]



"One measure of the [2018 term's] business-friendly tilt is the eye-popping success rate of the U.S. Chamber of Commerce, the self-proclaimed "Voice of Business." The Chamber filed briefs in 10 cases this term and won nine of them. The Chamber's victories limited protections for whistleblowers, forced changes in the Securities and Exchange Commission, made water pollution suits more difficult to bring, and erected additional obstacles to class action suits against businesses. Only the geekiest of Supreme Court watchers monitor such cases. But the Chamber pays attention, and it pays off."



"Though Powell's memo was not the sole influence, the Chamber and corporate activists took his advice to heart and began building a powerful array of institutions designed to shift public attitudes and beliefs over the course of years and decades. The memo influenced or inspired the creation of the Heritage Foundation, the Manhattan Institute, the Cato Institute, Citizens for a Sound Economy, Accuracy in Academe, and other powerful organizations. Their long-term focus began paying off handsomely in the 1980s, in coordination with the Reagan Administration's "hands-off business" philosophy."



"The organizational counterattack of business in the 1970s was swift and sweeping - a domestic version of Shock and Awe. The number of corporations with public affairs offices in Washington grew from 100 in 1968 to over 500 in 1978. In 1971, only 175 firms had registered lobbyists in Washington, but by 1982, nearly 2,500 did. The number of corporate PACs increased from under 300 in 1976 to over 1,200 by the middle of 1980. On every dimension of corporate political activity, the numbers reveal a dramatic, rapid mobilization of business resources in the mid-1970s." [6]

