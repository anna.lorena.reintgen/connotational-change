      creationism      procreationism      re-creationism      nihilism      notes
You had me at necrophilia. - Q

…we believe in one Lord, Jesus Christ,

the only Son of God,

eternally begotten of the Father,

God from God, Light from Light,

true God from true God,

begotten, not made,

of one substance with the Father.

Through Him all things were made.

- The Nicene Creed

No logic is more complete than that of monism, though none is more often protested. As long as all Creation is derived from God and His written Word, no assault can breach the walls of its castle. In Truth the logic of monism contains everything, and it can have no enemy. The cries of atheists and non-believers cannot reach the ears of its inhabitant, because heresy is logically not possible.

God’s reproduction adds nothing to His perfect self. God can only reproduce Himself—man in His image, Son in His image—copies, not offspring[1]. God’s Creation is made by Him only, it cannot surpass Him nor exist beyond Him. When God masturbates, He reproduces. Wherever His jism is spilled, life bursts forth.

In a flash, it’s as if you were born, flung into dark. Restless space, utterly foreign to the Last Times. No idea where you are naturally, you are shipwrecked, you have only the word shipwreck as lantern and explanation, for the rest you are in the dark. All is lost. This lostness—a state you knew nothing about. You are adult and biped, but the species is unknown. You know nothing about being. We don’t remember this world at all.

In monism, there is only one gender, that of man. Man who was made in God’s image and, like God, reproduces by spilling his seed onto the fallow earth. The earth—what we would call woman—does not constitute a distinct gender to herself, rather, she is without singularity or soul, an empty material form like the earth itself.

To speak of woman in the ideology of monism is an impossibility—that is, unless one speaks of a nothingness, an absence, a ghost. Not being a man, woman cannot exist in God’s Creation because that which is not One, that is not God, is not. The void is woman’s ontological origin. She emerges from nothingness because her existence is not only impossible but quite impermissible in monist logic. If a woman did exist (which, of course, she could not), she would have to be a nothingness. And so it was only by continuously asserting her very nonexistence that she was able to exist. She could not, in monist Truth, be, and so she was a ghost when she lived in the garden, and it was a void that suffered the pain of childbirth, and a specter that passed through the halls of the king to leave behind traces of desire on his body.

When girls and boys reach puberty, their bodies start to

change and become more mature. From this time, if a male

and a female have sexual intercourse (often called ‘making

love’, or ‘sleeping with someone’), it is possible that the girl

could get pregnant, i.e. a baby could start to grow.

- How Babies Are Made

From the moment we begin to speak of woman as such, we are not speaking of God and His world, but rather the world of opposition. Once woman existed as even a thought, in fact in any form exceeding nonexistence, monism’s ontological center could not hold. It must be said that in this crisis lay an intimate potential for the utter annihilation of the existent— would woman, as yet nonexistent herself, a being of the void, who came from nothing and returned all to nothingness, could she negate the existent that was her own negation? But so fierce was woman’s affirmation of her own existence that her force would not cause monism to stagger and collapse under the weight of the impossibility of something truly outside itself, but rather would balance and harmonize Man.

All that separates modernity from what existed before it is the radical shift from a monolithic existent to a dichotomous one.

We don’t stop killing ourselves. We die one another here and there my beloved and it’s an obsession, it’s an exorcism, it’s a feignt what we are feigning I have no idea is it a sin a maneuver a vaccination the taming of a python the fixing-up a cage, it’s an inclination, we don’t stop rubbing up against our towers touching our lips to them... eroticism to the nth degree lips on the sacred scroll, the innocent handle of the book, the saint, the simpleton, we know all about it, we always thought it, we’d also always feared for our towers, such striking clarity, and naked, but what terror when the real planes really crashed into them, a black terror that bit into our hearts, so this in reality can happen, in reality there was a tomb on one of our bodies, this was a fact and no waking, we’d awakened assassinated...

The Promethean feat is accomplished. The act of creation stolen from God and spread before mere mortals for their defilement. They engage in carnal desire and, when small, wet, stunted humans emerge from their loins, they marvel at their godlike power to create life.

The image of the one God, the Father, the Almighty, maker of Heaven and Earth no longer prevails. His Creation has forsaken Him, and now they bow down before false idols and genuflect before the image of the Child.

God still lingers here. With His masturbatory acts of Creation derided, He puts auto-eroticism aside and panders to the heresy of His flock. He constructs His procreative Trinity: Father, Son, and Holy Spirit—father, mother, and Child. This divine mimicry of the people’s profane procreative acts is too queer. He has not admitted the existence of woman, of dualism, of the Other; and in the binary regime, heterosexuality is sacrosanct. From now on, they worship the Child.

While before the Creator had been the object of adoration, procreationism displaces the focus to that which is created. The act of sex is banal enough that only the conservative thinkers, attached still to the old ways of praising God, could envision it as a divine act. The parent is imbued with no shroud of mystery or the unknown. But the Child—who can remember his childhood?—the Child is an image that could stand for the sacred mystery of reproduction.

The image is singular but its source is binary. Now the myth that subjectivities come from God is pushed aside by the idea that one exists simply because one’s parents fucked at some point and didn’t use birth control. Religious faith in an event one didn’t experience now shattered, only a scientific and historically rigorous explanation will suffice. One still does not remember one’s conception or birth, but the scientific method confirms that other babies are made this way, and so ‘I was too.’

The procreative myth—or fact—structures and gives meaning to the binary opposition of the sexes. The categories of male and female have meaning and power because their stability and duality is generative. Like God’s potency to make life spring from Himself, the male and the female, in their opposition and union, have the power to create life. No longer “God made me, therefore I am,” but now “my mother and father made me, therefore I am.”

The family constitutes the procreative apparatus, distinguished from the creative apparatus by a binary opposition inherent in the inclusion of woman into the realm of substance—while from God the Father, the Son and the Holy Ghost were begotten, not made, and remained of one substance with the Father, it is only through the union of opposite sexes that babies are made. A monist world could self-reproduce in perfect singularity and sameness, but once woman had posited herself in her difference, the force of her Otherness was established as a procreative force. And so it would be with the reproduction of the future, of the political order, and of capitalism.

The apple that Eve ate from the Tree of Knowledge may have initiated the fall from monist grace to dualist profanity in the mythology we know; in history it was the apple that fell on Newton’s head which ushered in dualistic thought. Newton’s theory identifies the forces which hold and harmonize the dualistic world in the void against threat of collapse (much as the high walls of God’s castle held the monist world together). Namely, for each force there is a reaction of equal force and opposite in direction. A simple example in the system of Newtonian physics explains how two heavenly bodies of sufficient velocity can orbit each other in harmony through the dynamic of their relative forces of gravity without collapsing into each other and spreading their dust into the void. So does dualist thought balance and harmonize the world of procreationism.

Modernity is Newtonian physics is dialectics is liberalism is binary reproduction is capitalism. It is unnecessary to draw the connections between each of these, when each is an aspect of the self-same logic. The triumph of the logic of two over the logic of one defines the world we inhabit, though this world is being supplanted by the world of plurality.

From the image of the Child comes the image of the Future, our hopes and dreams, our investment in a better world for future generations. Towards the image of the Future the political project is always aimed. The Child is the Future, and just as the Family produces the Child, the political apparatus produces the Future.

The political order of modernity is liberalism. Monarchy ruled the monist world, and the binary world requires something more balanced. With one gesture the sovereignty of the state is balanced against the rights of the people while the state deploys opposing political parties and systems of checks and balances to oppose it itself against itself. The liberal system of government, quite simply, is the state-form wrapped in the logic of dichotomy. Each and every political ideology that exerts itself in the effort to combat another political tendency or to assert the power of the people in resistance or opposition to that of the government participates in the discourse of liberalism.

The world of opposition is the world of dialectics. Just as the opposition of the sexes produces the Child and together these constitute the Family, so does capitalism reproduce itself through the opposition of the classes.

In dialectics, the existent contains its own contradiction— the proletariat. The proletariat is the negative force that could destroy capitalism; rather than excluding its enemy, as woman was excluded by monism, capital locks its negative force in struggle with itself and thereby exploits its labor for production, manages its reproduction as the source of more labor, and reproduces capitalism through class struggle.

Dialectics dictate that the negative project of the proletariat—the abolition of capital—entails the destruction of the proletariat-as-such. The stakes have been raised. The negative force is no longer a logical impossibility in the realm of the existent, but a machine integral to the reproduction of the latter. At the same time, however, the former is validated, reproduced, and fed by the same order that exploits it. In the end, the desire of the proletariat for liberation and autonomy from the control of the bourgeoisie, like woman’s desire to assert her existential being in the realm of man, would overpower its desire to abolish the present state of things.

Each and every stage of class struggle gave birth to another stage of capitalism, and every new manifestation was more perfect than the last. The latest high point in class struggle— May 1968—with its radical demands to sever the workers’ movement from the management of union bureaucrats (that is, its project of autonomous, liberated labor) was the most significant of these, and it birthed the postmodern era along with a mode of reproduction that surpassed dialectical opposition.

The Tower of Babel had fallen long ago; now it was the Twin Towers’ turn.

Discover the new you...

It’s your world...

A phone that gets you...

It’s so you...

One can hear it on the street and in the workplace, in the college classroom and the executive boardroom, at the latest radical convergence and at the beach, at dance parties and in underground venues: the logic of duality is so last millennium.

We are living in a postmodern world, and you are a postmodern girl. Which is to say, you are not really a girl as such.

Postmodernism posits a social order in which binary structures are destabilized. Foremost of these is the structuring of sexual difference, the very structure which constituted the means by which life was created.

The destabilization of binary oppositional sexes constitutes a crisis in the family and in the reproduction of life, but this crisis is not one that must spell the end of reproduction. A whole assemblage of techniques of biotechnology, cyberproduction, and social work are being deployed to enable, among other things, ‘queer’ reproductive possibilities as well as overcome the limits of the human womb which too easily ceases to function, especially under the stress of postmodern life. This analysis falls short, however, of recognizing the way that the central questions of reproduction have been displaced from the act of baby-making to the construction of the self, just as the centering of baby-making in procreationist thought usurped the former importance of the question of cosmic creation.

The primary mode of reproduction in a post-dialectic world is the reproduction of the individual—that is, re-creationism. The postmodern singularity is not created by God or its parents, but constructed through a pluralistic process that is increasingly ‘artificial,’ ‘social,’ and, paradoxically, selfrealized. This process is the process of identification[2].

The pluralistic reproductive process could not suffer limits. Each time it reproduces itself exists on an ontological scale. There is no need to speak of the modes of reproduction constituted by three, four, or fifty-four towers because, once there are three, the towers reproduce not themselves and not the relation between themselves, but more and more towers, not twin but unique and individuated, marching across the landscape at an ever-increasing rate. The World Trade Center has fallen but today there stand more skyscrapers than ever before.

Since their death, all their suffering..., our store of poisons entrusted to the Towers, counting of course on the jumbo apparatus of the American passions, in some the whole anxious and malificent game—of our lives, our primitive beliefs, our frissions, the deadly Greek and Bible-inspired phantasms, all that ghastly archivery we’d intuitively conserved in our Towers—hence unconsiously used as the colossal envelopes of all ideas of catastrophe, coffins customed as the temples of our death wishes.

Diversity is the scientific imperative imposed by the evolutionary science and post-modern theories of becoming.

The stresses of the rapid evolution of commodities and commodity-forms in late-capitalism impose rapid shifts in the labor market which manifest in the postmodern worker’s life as the condition of precarity. Precarity, in turn, is the impetus for the workers’ continual re-creation of themselves. This process is experienced in the worker’s life as the scarcity of jobs he already knows how to do; the creation of ever more numerous—and ever more abstract—job titles; the drive for continual education and training (‘upgrading’ himself); the loss of long-term salaried career possibilities as well as benefits and pensions; and the increase in part-time, shortterm, piecemeal or waged work.

The ability of capitalism to reach new markets, now that geographical and material expansion are complete, is based on its ability to reach ever-new identities. Thus identities must be produced, and produced as commodities. Identification, that is, the process of re-creationism, is the apparatus that produces these identities.

Each new identity is a new tower to which consumers can flock to escape the passe nature of the old ones. Eventually— that is, soon and very soon—there will have to be a tower for each person (“You know, there could be as many genders as there are people...”), probably more, and the scale of such production far surpasses the limits of the old workplaces, which are based on the assembly-line’s ability to make multiple, identical products. In the late-capitalist economy, however, each commodity must have the air of the unique, and this goes doubly for identity-commodities. The labor of this ‘creative’ work is displaced from the old workplaces; by social imperative and desire, the individual is put to work, unpaid (reproductive work— baby-making, class struggle, Facebook— is always unpaid), to create new identities ‘for himself’.

The postmodern Spectacle is a collection of images that must increasingly be constructed uniquely for each individual, the ghost of reproduction must not linger on the screen; but it must also afford him to interact with others. An apparatus of Spectacle-production that is socially-networked affords its consumer a profile and newsfeed unique to him but also the ability to ‘connect’ with his ‘real’ friends. Reality, in the end, is the product.

Political struggle is no longer epitomized by a war of one party or class against another, nor the people against the state, but becomes the battlefield of social war fought between many identities or forms-of-life against one another. Just as the war between parties within the government served to mask class struggle, today the war over identities masks the war of forms-of-life.

In the war over the towers, identity is the basis for political struggle as well as its aim. Struggles fought over control of the creation and maintenance of identities is no more a threat to the existent than struggles over the making of babies.

The war between forms-of-life is not a war between identities, though it may often manifest as if it were. In this war the negative party is that of the queer, the abnormal. Queer constitutes the negative force that is centrally involved in the proliferation of identity through its struggle to assert itself positively outside of the realm of the normal (each positive queer act yields yet another position within normalcy), yet only for the queer does the overcoming of its limits in struggle threaten to demolish all the towers. This is because queer is positioned to destroy the mechanism of reproduction that it inhabits and asserts—the mechanism of difference, of abormality, of queerness.

Let us be clear: the queer revolt is yet the avant-garde of capitalism, and that is because it is the positive queer revolt and not yet the purely negative one. The latter does not distinguish itself from the former by its violence and destruction alone-- an assault on existing identities is inherent in the production of new ones—but by its gestures of abortion and its rendering of impotence.

The fact that we are writing these notes is evidence enough that the purely negative tendency has yet to reveal itself sufficiently to destroy the world as we know it.

Aneantir le neant.

Annihilate the void.

Up to now, every critique of the social order has been more or less successful while every proposal to negate it has only accomplished a strengthening or reconfiguration of it. The existent is readily described by the discourse it contains, but the purely negative force is truly unspeakable. There is no reason to believe that the discursive elaboration of the purely negative project is at all possible. Nevertheless...

The common essence of monist, binary, and pluralist ontology is the elevation of the subject to a (singular or multiple) substance—the failure to grasp the nothingness that defines subjectivity. The question of ‘why am I?’ contains its own answer. Without a subject to pose the question, the question could not be posed. No reproductive apparatus is necessary to create or explain subjectivity. The origin and definition of subjectivity is the abyss; all else consists of substance that is constructed around the void and mistaken as the self. When we say that the self consists of a nothingness, this is the same as the assertion that there is no self.

The avant-garde of capitalism has been misconstrued as its enemy. Granted that the destruction of reproduction is the project of queer negation, what has come to be known as ‘radical queerness’ is a largely positive, rather than purely negative, project. In opposition to the world of binary gender, procreationism, the family, politics, modernism, structuralism, dialectics, &c., the ‘queer revolt’ posits pluralist gender, re-creationism, the identity group, identification, postmodernism, post-structuralism, multiplicitous struggle, &c. The latter constitute the reproductive apparatuses of the pluralist existent.

In a crucial point of emergence long ago, woman established herself as existent rather than plunge the monist world of Man into the void from which she came. In another, the proletariat struggled to secure its autonomous liberation from the bourgeoisie rather than destroy the bourgeoisie and itself entirely. On the stage set by the present order, the queer force is making itself busy with the proliferation of identities rather than the utter negation of them.

In the re-creationist order, life is experienced as void and death as the only escape. Such is not far from the truth. For those singularities which are born or incorporated into the reproductive order of identification—which now includes even woman, the proletariat, the queer, the hipster, the anarchist and all the rest—the void is no longer experienced as something outside the castle, but as dwelling within.

Like the negative project of the proletariat, the negative queer project entails the negation of the existent, of the existent’s reproductive apparatuses, and of itself. What’s more, the latter’s self-abolition must take place not only as death, but also the murder of a certain kind of death. This is because even suicide, or self-abolition, has been subsumed under the process of re-creationism. Death is necessary in the process of self-creation because in the act of becoming, one kills the old version of himself. In order to destroy the reproductive process of re-creation, the queer must destroy the latter’s false version of suicide. The queer death-drive is an urge for pure suicide, which is also pure murder.

It is no coincidence that those who theorise on themes of pluralist gender identity, postmodernism, intersectionality, and encourage the reader/subject to not kill himself and instead to kill a part of himself in order to reinvent himself anew.

Man’s fall from grace and the collapse of the Twin Towers pale in comparison to the purely negative project of today, so awful are its manifestations. These are yet unspeakable, but if we could imagine the entire world rendered as an aborted fetus, the plunging of the universe into an abyss that opened in the space-time continuum, or the people of the world digging corpses out of their graves and fucking them endlessly, we would catch a glimpse of the death it seeks to unleash. To those who love this world, human strike will appear as no thing of beauty, but to those who hate it there is nothing as beautiful.

Pure suicide is not the suicide of the individual motivated by hopelessness, though it is antithetical to hope. It is not the suicide that comes from a moment of despair, but from an entire world of despair. It is not decided in the turn of an instant, but carefully considered over time. For before it destroys itself, the purely negative singularity strives to destroy this world, render impotent its apparatuses of reproduction, and bring to an end its sense of the Future.

If the Tower our mother our body our sex burned down tonight—the hypothesis cannot be rejected, the whole castle has already burned down except for the Tower, the Tower’s turn will come, since what else remains to burn? How what’s more to explain than that such a Tower, such a perfect gem of human grandeur not be condemned and executed in these days of perverse criminality? For sure it’s a target, plans are afoot... She is there, round, delicious, appetizing, eternal, pregnant with genius and with books, and she is not there. One gulp of the plane. We are already killed. Read all about it in tomorrow’s paper— if the Tower has burned, we are already dead and tomorrow we shall die of it.

If the Tower has not yet burned, it will burn in a day or two.
[1] That these copies deteriorated over time and became an imperfect image of God is one theory given to explain the fall of monism into dualism. The myth of Lucifer suggests another theory. God reproduced Himself perfectly, but His most identical creation was that which would challenge Him. It was the likeness to God of the archangel Lucifer—his almighty power and equally strong ego—that compelled him to challenge and attempt to usurp His power. Lucifer’s banishment locates him not in the dualistic system of procreationism (after all, in that scientific and material world, some still cling to belief in God, but they are overshadowed by a dominant mode of thought that is atheistic or at least agnostic), but as banished to the Void that surrounds God’s castle and against which its stalwarts are constructed. Here Lucifer will corrupt the void-dwelling figure of woman, and seduce her into introducing the first cracks in the monist castle.
[2] Identification is the process of realizing oneself. Not recognizing but realizing because identification positions itself against the logic of procreationism that the subjectivity is made (being) by positing as subjectivity as a process of realization (becoming). Becoming is one of the postmodern theorist’s central concerns. The popular term ‘self-identification’ is thus a redundancy. The popular form of identity in the era of (positive) queer theory is the selfrealized identity, the one that imposes itself in opposition to, amendment of, or spectacular ‘reclamation’ of one’s ‘sociallyimposed’ identity (‘socially-imposed,’ ‘assigned,’ or ‘sociallyconstructed’ because self-realization is always understood as a process of becoming, not construction, even though the selfimposed identity can itself arise only from the structures and impositions of society). One is not born, but becomes, a woman.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



…we believe in one Lord, Jesus Christ,


the only Son of God,


eternally begotten of the Father,


God from God, Light from Light,


true God from true God,


begotten, not made,


of one substance with the Father.


Through Him all things were made.


- The Nicene Creed



When girls and boys reach puberty, their bodies start to


change and become more mature. From this time, if a male


and a female have sexual intercourse (often called ‘making


love’, or ‘sleeping with someone’), it is possible that the girl


could get pregnant, i.e. a baby could start to grow.


- How Babies Are Made



Discover the new you...


It’s your world...


A phone that gets you...


It’s so you...



Aneantir le neant.


Annihilate the void.

