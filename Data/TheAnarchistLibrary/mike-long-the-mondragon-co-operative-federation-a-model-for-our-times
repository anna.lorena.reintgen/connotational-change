      A model for our times?
The Mondragon Co-operative Federation (MCF) is a community of economically highly successful worker-owned, worker-controlled production and consumption co-operatives centred around Mondragon, a town in the Basque region of northern Spain, and now spreading throughout the Basque provinces and beyond. The MCF is an experiment in participatory economic democracy rooted in a powerful grassroots movement for Basque cultural revival and autonomy, but inclusive of non-Basques. 

The MCF began quietly on a tiny scale with one co-op and 12 workers nearly 40 years ago under the fascist Franco dictatorship. The original members were educated but poor and had to borrow money from sympathetic community members to get started. By 1994, the MCF had become the fifteenth biggest business group in Spain, comprising some 170 co-ops and over 25,000 worker members and their families, with vast assets, large financial reserves, and annual sales of around three billion US dollars.

Studies have shown that the co-ops have consistently outperformed surrounding capitalist industry on all the usual measures, and while unemployment in Spain has hovered around 20% for many years, full employment has been maintained within the Federation. All this has been achieved with a level of internal democracy and concern for social justice undreamt of by most workers struggling under exploitative state systems, whether capitalist or authoritarian socialist.

Not surprisingly, international interest in the MCF has grown over the past 20 years, especially now that so many governments are unable to provide even for basic human needs food, shelter, education, healthcare, art and recreation — and are increasingly recognised as uninterested in doing so. (As anarchists have long pointed out, that is not what governments are for, after all.) There is a sizeable literature in several languages on Mondragon. Harvard business students study management within the Mondragon co-ops. Stanford law students learn about the legal obstacles to setting up such entities in the USA Enlightened Australian trade unionists consider whether using union funds to start “mini-Mondragons” for their unemployed members might be more effective than filling politicians’ pockets in the vain hope of slowing corporate job export to non-union, low-wage, third world countries. And some anarchists wonder if the MCF is a test, or even a vindication, of their ideas.

This article has three aims. The first is to sketch the historical context for the MCF, including the wide-scale experimentation with worker-controlled industry and agriculture that took place during the early months of the Spanish Civil War.

There are similarities, ignored by many professional MCF observers, although not by all, between the internal structure and day-to-day functioning of the CNT/UGT collectives in 1936 and 1937 and the MCF co-operatives since 1956. This is so despite the undeniable compromises which today’s worker-owners have made (or as most of them see it, have been forced to make) in order to stay afloat in the hostile capitalist sea in which they operate, and despite the fact that the debt appears to go unrecognised by many of the co-operators themselves, few of whom consider themselves anarchists. The second aim is to provide a brief overview of the Federation’s development, structure and functioning. The third is to evaluate its significance for anarcho-syndicalists.

Industrial unions are not only the means to an end, for anarcho-syndicalists, however. They also offer a mechanism for the rational co-ordination of the production and distribution of goods and services in the new society on a scale demanded by its modern size and complexity — a scale that is difficult, perhaps impossible, for either pure anarcho-communism or collectivism to manage. To illustrate, union and industry-wide councils can preempt the potential for selfish competition inherent (although not inevitable of course) in collectivism, with its retention of assets and property ownership by collective members. They can do this, for example, by sheltering one collectively owned farm, factory or service from a more successful one, or by researching planning and funding the initial implementation of new unionfunded ventures, such as co-operatives, ensuring that they will be useful, economically viable, and will not duplicate services offered elsewhere. Their size and strength also allow industrial unions to guarantee protection for sick, weak or temporarily unproductive community members, rather than leaving them to depend on what is essentially the charity of others, as pure collectivism tends to do. Finally, as evidenced by the historical record, anarcho-syndicalism has long been recognised as relevant to their needs by far more than “just” blue-collar smokestack operators, appealing instead to workers of all kinds: to sailors, dockers, miners, lumberjacks, bakers, cobblers, barbers, needleworkers, educators, postal workers, flight attendants and computer operators, to white-collar providers of numerous other goods and services, and to collectivism, with its retention of millions of landless peasants.

In addition to all these options and variants in anarchist economics, there are disagreements within the various camps about how to get from here to there. Anarchists have long argued over whether, as one collectivist, Proudhon, believed, it is possible to evolve gradually and peacefully towards one or the other system, or whether, as another collectivist, Bakunin, asserted, what they aspire to can only be achieved by revolution and expropriation of the existing means of production, forcibly if necessary. Not surprisingly, therefore, anarchists’ attitudes towards Mondragon vary, too, ranging from enthusiastic (e.g. Benello, 1986/1992) to dismissive (e.g. Chomsky, 1994). What follows is based on my reading of English, and some Spanish, literature on the MCF, coupled with a week-long visit to Arrasate (the Basque name for Mondragon) in June, 1994, with fellow Wobbly, Charlene “Charlie” Sato (we visited as individuals, not as representatives of any organisation). Our stay in Arrasate included an intensive series of pre-arranged interviews, informal group discussions, and site visits, as well as enjoyable and equally informative evenings spent socialising with co-op members over bottles of the MCF’s excellent Rioja wines.

The generalizability of the Mondragon model may be considered in at least two ways: in terms of its practical viability and its ideological acceptability. Much has been written about the former, with some debate about the relative contributions to the MCF’s economic success of the following factors, and various combinations thereof: Basque nationalism; co-operative values; a strong sense of (Basque or any other) ethnic, linguistic and cultural identity among the participants; the foresight and leadership of Father Arizmendiarrieta; the compatibility of MCF values with Basque traditions, such as co-operative farming practices and the relatively equitable land distribution among Basque families compared, for instance, with the hacienda system of southern Spain; the rapid expansion of the Spanish economy after the Civil War, with a heavy demand for household goods and other early MCF products; the political and economic history of Spain, with its strong anarchist and anarcho-syndicalist traditions and lengthy prior experience with agricultural, fishing, and industrial production co-ops; Mondragon’s strategic location, with easy access to large ports like Bilbao, and short distances to major export markets; the scope and diversity of the MCF’s high technology products; the use of crucial second degree co-ops; early establishment of the CLP; the centrality of the industrial co-ops; the relatively low cost of land for the agricultural sector; the availability of a highly educated work force with relevant skills; and the felt need to look to a self-help model, given the Basque people’s long history of state oppression.

Also widely considered crucial is the MCF co-ops’ internal worker-member economic structure. My own view is that perhaps all, of the above factors were differentially important at various times in the MCF’s history, it is in their internal structure and functioning that the co-ops’ main ingredient for success lies — and in this domain, too, that they come closest to anarchist principles and values. I believe that (a) the motivation and commitment needed to buy or work one’s way into a co-op; (b) the initial extra capitalisation provided by retention of a portion of members’ income in their internal capital accounts; (c) the equality and mutual respect produced by the one person, one share, one vote, system; and (d) the stability and freedom from external control guaranteed by the impossibility of members selling shares to each other or to outsiders, have made for a system of worker ownership and (with some dilution in the interests of operational size and efficiency) worker control. The pride and security this brings the MCF members, the feeling of control over their own lives, the visible economic success of their efforts, the decent standard of living they have achieved for themselves and their families, and the positive impact all this has on the communities to which they return after work each day, have had a liberating effect on the workers of Mondragon, just as anarchist theory would predict.

If this analysis is accurate, or even close to it, variants of the model adapted for local conditions must be of interest to like-minded individuals or whole communities elsewhere. In fact, co-ops on something like the Mondragon model are already operating in several countries, including Germany and the USA. Many writers have discussed the MCF or similar projects positively, and several have provided practical information on how to go about setting up new co-ops.

Whether worker or union-owned and/or controlled, and no doubt accompanied by militant union organising in existing workplaces, it is clear that something like Mondragon-style co-op federations, and federations of federations, are urgently needed in many countries today. Quite apart from the human misery and environmental devastation it causes, capitalism simply does not work even judged by its own execrable standards. The desperate plight of growing millions of unemployed and never-to-be-employed workers in the inner city ruins of so many “advanced” industrialised countries attests to this. So does the poverty, disease and starvation that is the lot of millions of capitalism’s third world victims. These people are viewed by “their” governments merely as the inevitable statistical fall-out from multinational corporate “restructuring” and increased “efficiency”. Politicians, states and the capitalist system have nothing to offer them. Radical industrial unions, like the CNT, the SAC and the IWW have something. Ultimately, however, their future lies in their own hands, just as it did the oppressed citizens of the small town of Arrasate some fifty years ago.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

