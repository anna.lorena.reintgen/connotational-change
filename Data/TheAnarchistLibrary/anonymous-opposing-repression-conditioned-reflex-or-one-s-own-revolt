
A nasty wind is blowing, it’s useless to hide it. So nasty that a certain worry even spreads among the fine souls of the left, and the establishment of a “dictatorship” by the current government is denounced in an increasingly vehement manner. It is true, on the right they have never forgotten their traditional inclination for castor oil and the cudgel, but the fact remains that repression, censure and prohibition are the bread that every form of government administers to us, no matter what it is. In reality, beyond the political faction momentarily charged with administering it, it is this one-way world that requires a one-way life, made from a one-way thought and one-way behavior... in an authentic coherence of abjection. Up to the point of placing a ban on any critique, any dissent, any opposition, all of which are punctually isolated, circumscribed, slandered, suffocated, locked up, wherever they show themselves.

It would be sufficient to take a quick look at what is happening in the latest period pretty much all over Italy. Within the “movement”, investigations, arrests, detentions, searches, beatings and intimidation are spreading farther and farther and reaching practically everyone, from the hot heads to the coldest passing through the tepid. The prison doors open to all: it is enough to be accused of an attack, or of having formed yet another subversive association, or of having hindered an identification or a police roundup, or of having removed an infiltrator from a demonstration, or of having participated in a vigil, or of having occupied a building; and soon even the mere accusation of coloring shop windows that overflow with commodities will be reason enough for ending up behind bars. At the same time, the authorities run through the thousands of possibilities furnished by the penal code in order to block every form of activity, freely dealing out expulsion orders and prohibiting entrance into cities to comrades residing in small neighboring villages (charming modern version sweetened from the old forced residence). Easy to foresee the increase of such repressive practices.

But more importantly, it is not only the movement — in its multiple shades — that the repression is aimed at, but rather it is society in its totality that is suffering a tight turn of the screw. The prohibition of criticism to the presence of Italian troops in Iraq has reached staggering levels: a soccer field was disqualified because the fans of the team that played there did not show the proper sorrow for the soldiers who died at Nassiriya; some underage students, guilty of having hung banners against the war, were dragged into a barracks and interrogated; other students who distributed a flyer had their houses searched; the shutting down of an information site like Indymedia was requested in parliament because it has hosted voices outside of the nationalist chorus. More generally, entire schools are passed through the sieve in search of drugs; foreigners possibly suspected of anything are expelled from the country in a few hours; hundreds of them are thrown out of their home in the middle of winter; satirical television shows are censored for being too satirical... and one could go on. Unfortunately, the examples are not lacking. On the contrary, they are increasing with a certain progression, like the furious reaction to the transit strike in Milan that left the city on foot for a day: if on the right a harsh punishment was invoked for the strikers, on the left there are those who call for the intervention of the army [to operate the streetcars] in the case of a new interruption of service. And it is easy to imagine what will happen as the new laws on drugs begin to be applied.

In the face of this, public discussion seems truly urgent to us, before every space for talk or action is forbidden to us.

Let’s start with a premise. The fact that today everyone who is not quick to spring to attention ends up in the sights of repression means that the division between the “good” to comfort and the “bad” to beat up has had its day. This will certainly not serve to unite the different spirits of the movement — with the permission of so many ecumenicists — divided by something quite different than the grade in conduct on the report card of the state, but it could contribute to the sweeping away of an old and insipid commonplace that is unfortunately widespread, the one according to which repression would be equivalent to a certificate of radicality: “I am repressed, therefore I am”. The conviction that leads some people to believe that the more one is repressed the more one is, in a delirium of self-congratulations that in any case overflows in sacrifice. It is obvious that, at the moment in which repression is extending itself to every sector of society, it becomes ridiculous to think that it only strikes those who attack the security of the state. This means that, contrary to what the mob leaders of the various militant rackets think, the increase in repression does not, in fact, correspond to the growth of the revolutionary threat of the movement or any of its components. To be honest, it seems to us that the movement, meant in its broadest sense, is reaching one of its lowest points, everyone busy on the one hand with getting their share of the media and institutional pie and on the other hand with thrashing about in a chronic lack of perspective. The explosion of Genoa itself, which happened over two years ago, seems to us to be more due to an ensemble of circumstances, realized above all at the international level, than to a supposed maturity reached by the movement here in Italy (the flow that immediately followed from it give evidence of this).

But then, if the movement in itself is not really so strong, so dangerous for the sleep of their lordships, why are we witnessing this continual repetition of arrests and intimidations? In our opinion, it is because the social situation in its totality is now so weak and fragile as to not allow the ruling class to run too many risks. The edifice is still standing in all its monumental grandeur, but its foundations become more and more putrescent and the creaking gets louder and louder. As if to say that that we are not repressed because we are strong, decidedly not, but rather because they are weak. To be clear, we are not saying that this social order is not able to impose its will or that it is militarily vulnerable or any other such thing. Only that it advances more due to a motion of inertia than a propulsive action, resting more on a passive resignation than on an active consensus, in a context so lacerated that it is unable to guarantee any enduring stability whatsoever. In short, precariousness is even afflicting the ruling order. Aware of its own weakness, it finds itself constrained to raise its voice and intimidate its enemies, real or presumed as they may be; it does it now, because it can still take the liberty to do so. This leads it to exaggerate every event with the aim of creating alarmism capable of publicly justifying measures that would otherwise not be able to be proposed, and also of provoking the panic necessary for a shred of security capable of encouraging it.

As we have already said, this great barking of the guard dogs of power indeed strikes fear, but it also denotes a certain fragility. This should make us reflect on the possibilities that open before us, on how to outwit these mastiffs with the aim of laying hands on what they protect. Instead, it seems to us that the barking is becoming an obsession for too many comrades, leading some people to occupy themselves with exclusively with healing the wounds inflicted by their bites, and some people to challenge them for the pleasure of the conflict or because they aren’t able to see beyond this. We want to point out how in both cases, a decline in our objectives takes place, and thus also in our practice, as our aim is modified, changing from the struggle against the existent to the struggle against the forces that defend it. Is it the same thing? No, it is not without confusing cause and effect. Fighting and defending oneself against the forces of order does not in itself and for itself mean subverting the dominant social relationships. And at a time when these social relationships are particularly unstable, this is where we need to focus our attention, our theoretical and practical critique, avoiding as much as possible being driven by the conditioned reflex provoked by repression. Because otherwise we end up abandoning the fertile but unknown terrain of social conflict in order to take up a defensive position in the sterile but well-known terrain of opposition between us and them, between comrades and cops, in a conflict rich in spectators but poor in accomplices.

Now, with the mere act of investigating and arresting, the state manages to give someone who is repressed the illusion of being dangerous, of already doing something concrete, for this very reason. It gives all of us the fatal illusion of being strong , the illusion that our struggling is meaningful, whereas, in reality it is extremely weak (even if potentially harmful for the ruling order). In this way, we can claim to be satisfied with our activity, however lacking it may be, without asking ourselves how to improve it, dismissing every critical debate, because often they are considered a waste of time. Besides, as is well-known, repression puts the movement on the defensive; it leads us all to have to occupy ourselves with arrested comrades, finding lawyers, collecting money, organizing demonstrations in front of prisons, attending hearings. Even those who have recourse to the most extreme practices of protest, like the sending of mail bombs, don’t escape this logic: the state against the movement, the movement against the state in a frantic succession of arrests, protests against the arrests that lead to new arrests, that lead to new protests that lead to new arrests,... Yes, they are repressing us all. But can we claim that we are dangerous because of this? Or is all the repression that is coming down on the movement nothing but a way of preventing us from truly becoming so?

Perhaps there is a need to clarify some questions. The material support for those who end up in prison, a sad eventuality that is becoming more and more concrete for everyone and that would merit greater consideration, is and must remain a technical problem. The question of what we desire to do against this intolerable world is of quite another nature. As cruel as it may seem, it is necessary to reject the moral blackmail that is launched every time a comrade is arrested. There is no obligation of solidarity with which one must comply. No one ends up in prison in place of those who are outside, and no one is outside of prison thanks to those who are inside. Even though their liberation is one of our principle preoccupations, we should not let it become the end to which everything is subordinated, we should not give up running because someone who stands with us has been stopped. Rather we should dedicate ourselves to acting to create the conditions for their liberation and for that of others, not fixing our gaze and attention on what we see immediately before us, but making ourselves unpredictable, not being obsessed with the dates established in advance, but establishing our own for ourselves. Our agenda should not be traced either on that of the government, or that of the judicial system, or even less that of the various political grouplets that chase after the spotlight of notoriety. In short, rather than stopping to find ourselves before the walls of a prison to demand the release of those who are locked up there, it would be better to go on ever stronger and in all directions. Not only because this is the best way for expressing our solidarity with them, since the awareness that there are those who continue along the path that has been undertaken is more comforting than a noisy greeting; but above all because it is the way to show the uselessness of such arrests to those who order and execute them.

This is why we think that the best way to discuss what to do against repression, aside from any possible consideration and agreement of a technical type, really consists in constantly questioning ourselves about what to do in order to damage this society in its totality and in finding answers in the course of action. Because it is true that a nasty wind is blowing, there is no use in hiding it. But it is also true that if we truly desire the unleashing of the tempest, the problem of the wind that blows can only be a false problem.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

