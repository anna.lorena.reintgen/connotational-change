      Post-Script: You have to go through hell to get there...
Sensory Deprivation is the reduction of sensory stimulation to a minimum. It is depriving human beings of all normal contact with their environment through sight, hearing and movement.

Folsom prison, in California, confines inmates to special cells with only necessary facilities and enough food to keep them alive. They are forced to lay quietly on bunks for as long as months at a time. They call it “Administrative Segregation.” We’ve heard of it as “the hole” or solitary.

Prison bureaucrats learned from Prisoners of War who have been “softened up” and prepared for brainwashing by being subjected to solitary confinement. The experiments have revealed that sensory deprivation can have marked effects on practically every mental process, although these effects have varied from subject to subject. The most common emotional effects are restlessness, boredom, and irritability. Some subjects enjoyed the experience during the first few hours, but tended to become apprehensive after two days of isolation. In some cases anxiety rose to panic proportions: “The quiet was so loud it was like a knife stabbing through my eardrums.” The effects on thinking processes also increased as time wore on.

In the first few hours they have found it increasingly hard to concentrate and control their thoughts. They finally drifted into daydreaming and incoherent fantasies. A distinct loss in the ability not only to solve problems but to adjust to novel situations has been found. There was also a general loss of efficiency in motor ability. Co-ordination was poorer and reaction time was longer than usual. (If an inmate in Folsom reacts slowly to a guard’s command they can be punished further and usually are.)

Two other effects are particularly significant. It has been revealed that subjects were more easily swayed by propaganda than the ones not under Administrative Segregation — a fact that helps to explain the use of isolation in brainwashing. Second, gross perceptual changes frequently occurred during sensory deprivation. Illusions and even hallucinations, similar to those produced by mescaline and other drugs have been experienced.

Although they were in a silent environment, they heard strange music and chirping birds or saw door knobs on imaginary walls. In trying to hold on to reality they struggled to recall recent real life events and were unable to do so.

So far the experiments have indicated beyond much doubt that the human being cannot continue to function in a normal way if he is deprived of sensory impressions from the external world. In the absence of continuous and varied stimulation, all psychological processes tend to become disrupted and disorganized. The cognitive, perceptual, and emotional changes associated with and consequent to deprivation leave the subject less competent to meet the demands of this environment and the society which he must reenter at some point.

Therefore, while this age old brainwashing technique sometimes works to control the prison environment it only serves to increase the danger to the general population.

The hole, adjustment unit, the box, Siberia, Klondike, solitary, isolation, “Administrative Segregation.”

In one prison, an American Indian had been so uncontrollable that he had been kept in the hole for years. His cell door had been welded closed. Eventually, a new warden released him. But by this time he had become blind. That was in an old prison, but there are uncivilized practices in some of the newest.

Difficult inmates may be segregated without specific offense or hearing for months or even years, simply because they are active in prison politics or just plain ornery. In short, due process has not had a significant place in that most autocratic of American institutions, the prison.

Folsom was built in 1890 in Sacramento, California. There, the inmates that are put in the hole are being punished for such offenses as fighting, possessing homemade weapons such as nails, or drinking ‘julep’. Most often the complainant is an officer who may or may not be objective and fair. Nevertheless the officer’s accusation is tantamount to a finding of guilt, for it is a correctional precept that the officer’s word must be upheld. For example, if it looks like a fight has taken place the standard response is to treat the two as equally guilty, they say weakly, “It takes two to tango,” and lock up both suspects.

Once the inmate has been accused he is put in the hole to wait for a hearing by the prison bureaucrats. In the hole he is virtually cut off from everything, including his possessions, and it may be as long as 15 days before he is given the charges against him. This is a further means of prolonging his tension so that, long before questioning starts, his thinking may have begun to get distorted. He will have been foraging in his mind for all possible reasons why he has been imprisoned, and perhaps finding every answer but the right one. He may even begin to believe in his speculations as though they were facts.

Confessions can be made which, though largely false, may come to be believed by both the examiner and the prisoner. This is because the examiner first suggests that he is guilty of a crime and tries to convince him. Even if the inmate is innocent, the long tension to which he has been subjected may well have already frightened him into suggestibility. If the examination is pressed, he may even begin to play back an old record — confessing to crimes suggested by the officer in earlier cross-examinations. The officers, forgetting that the incidents were originally their own guesswork, are deceived: the prisoner has now ‘spontaneously’ confessed what they have been suspecting all along. It is not realized that fatigue and anxiety induce suggestibility in the examiner as well as the prisoner.

This increased suggestibility, or the paradoxical or ultraparadoxical phase of reaction to stress is most likely to occur when such persons may be most easily persuaded to make statements which not only increase their chances of conviction, but even sometimes incriminate themselves unjustly. Later they may calm down, return to a more normal state of brain activity, and ask to withdraw these statements. It is then too late.

The prisoner often spends the entire period before his bureaucratic hearing, and during it, trying to understand how he came to sign so damaging a ‘voluntary’ statement as he has given and wonder how to explain or extricate himself from its implications.

Police have no compunctions about writing practical textbooks on the subject of how to elicit confessions. The lie detector and urine test has been found to be wonderfully effective for scaring the inexperienced into making confessions. Even if a test is negative, the examiner can still pretend to believe it positive, to help win a confession. ‘Career’ criminals have learned by experience the danger of co-operating in any form with police questioning or examination and so refuse to answer any questions at all.

The experienced criminal is handled by “keeping at him” day after day until he ‘breaks’. They get him into a mental corner, a wedge in as a start, which is his weak spot. Once the weak spot is found the prisoner becomes confused, all his defenses have been beaten down. He’s cornered, trapped. That’s when he breaks. The torture comes from his own mind, not from the outside.

With such a technique, truth and falsehood can get hopelessly confused in the minds of both the suspect and the examiner; and if what he calls a “weak spot” is not present, the police examiner determined to get a confession can create it by suggestion. The prisoner has merely given back what has been originally implied or suggested. They have both been brainwashed.

Major A. Farrar-Hockley gave a description of the technique by which ideas can be implanted without the use of strong, direct and obvious suggestion. He had been a British prisoner of war in Korea. “The Chinese are past masters of this technique. They wouldn’t tell me what they really wanted. Whenever .we got near to something substantial, they would immediately come back to it from another angle and we’d go all round it, but I’d never find out what it was. And then they would go away and leave me thinking. I believe if the interrogator went on long enough with someone who is in a very weak state, and then sprang the idea suddenly on him, the chap would seize on it and become obsessed by it. He would begin to say, ‘Well, I wonder whether in fact it’s all really true, and this is what I was thinking in the first place.’ Every time they went away I spent hours saying, ‘Now was it that? No, it couldn’t have been that. I wonder if it was so and so?’ and that’s what they were trying to do. They were trying to get me to a state when the idea would suddenly come, and I would begin to wonder whether I’d thought of it or they had. Now another method is to gradually suggest something by talking round it and getting a little nearer each time and just giving a fragment so that you build up the idea in your mind. And they say, ‘But you said this, we didn’t’ and you would think you had.” Of course, for this technique to work the prisoner must be put in segregation where the sensory deprivation is greatest.

In the years 1644–1646 women suspected of being witches were also put in the hole with nothing so that the official witch-finders could gain their confession. The U.S. Justice Department shares a long and distinguished career with witch hunters, the Inquisition, the Russian purges, and Chinese torturers.

If you’re planning on going to hell, get up early. A lot of people are in line already at the airport ahead of you. I took the cheapest flight, which turned out to be a “milk run.” No, they don’t serve milk or any food for that matter. Plan on fasting. It took off from San Antonio, landed in El Paso, took off from there and landed in San Diego and then landed in San Francisco. The pilot flying that plane had to be the very best at landing aircraft as he had a lot of practice that day. Catching a flight to Sacramento isn’t easy, either. United wanted as much for it as I had paid to come all the way from Texas. I considered taking a bus but being a true Texan, I decided I’d had enough of public transportation and rented a car.

Don’t ever take directions from someone at an airport. They want you to see the countryside, at 200 miles per hour. These people are in a hurry out here. After traveling some distance north, when I should have been going northeast I spotted a beloved McDonald’s in the middle of nowhere, where I received a sugar fix and valid directions.

To enter the gates of hell, get up early. There are a lot of rules and it takes time to get it right. First of all you can’t take anything with you except your ID. Even pens and pencils are not allowed. This means you have to keep every instruction in your head. For instance, you have to wear underwear. Now, who would have thought of that? (Buy a bra, girls!)

Folsom State Prison is clean. In fact, there are inmates all over the place mopping, sweeping, raking, etc. The “corrections officers” in the visiting room were polite.

The inmate that I had come half way across the country to visit had been placed in the hole a few days before my arrival. That meant our visit would have to be behind glass, on a telephone and could only last one and one-half hours instead of the usual six hours in the regular visiting room. These are large rooms with snack machines and tables and chairs where many others are visiting at the same time. Usually an inmate is allowed a ‘greeting’ which can only be one kiss and one hug at the beginning of the visit, nothing at the end, or in-between, or at any other time. No holding hands please!

No one on the outside can possibly imagine what it means to someone to have contact with normality. Things like a radio, letter, newspaper, books, can mean the difference between the life and death of a person’s spirit, soul and mind. It’s survival of the personal self.

The “goon squad” is a group of officers dressed like a SWAT team or the Delta Force. They bring the inmate up from the hole by handcuffing his wrists behind his back, and putting on leg irons which are all attached to a chain around the waist. When the inmate leaves the visiting cell he backs up to a slot in the steel room with his wrists behind him. He is hooked up to other inmates and marched back down to the depths of hell, to total sensory deprivation: no radio, no TV, no food, no heat, no shower, no personal belongings, no hygiene, nothing, nothing, nothing.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

