
I believe that we are taught to give way too much deference to law. Law is treated like it is the end all and be all of human morality and interaction. Law is held up to be a god and it is not to be questioned by the layman, only obeyed with all reverence. Law has many similarities to the church. In the church, you must have a minister or a priest to interpret and explain the Bible or the nature of God to you. You can read the Bible, but the true interpretation is reserved for the priestly caste. Granted, Luther and the Reformation did create the conditions whereby everyone can read and interpret scripture, but as we all know the acceptable parameters of interpretation are quite stringent and if a personal interpretation is too extreme one is ostracized until their theology comes back in line with the accepted ideology. This is all so that power and control stays within the hierarchy of the church while the layperson has the illusion of being empowered. But ultimately the “official” theology is upheld; a theology that has not come down from God, if God exists or even cares about theology. The official theology is what was adopted by the church about seventeen-hundred years ago, not by consensus, not by divine decree, but by political expediency and coercion.

Law has been established and interpreted in much the same way that “official” church dogma has been developed. The layman who the law applies to is forbidden to interpret it for him or herself and apply it to life accordingly. No, we must turn to the priests of law interpretation, the lawyers. But this interpretation comes at quite a price and that is by design. And just like at Nicaea, where the most formidable and convincing theologians won the day, and more importantly, the emperor’s approval; so in law the most adept at semantics and convoluted rhetoric gets the nod from the bench and from then on the law is interpreted a certain way. But the biggest offense is that these cases where laws are created and interpreted take place without the active involvement of the very people who the laws will affect; often to their individual diminishment if not downright detrimental to their very well being. Simply put, many of the laws have been created to protect the interests of the powerful and wealthy few and in the process robbing the many of their own personal sovereignty and self-creation, while keeping them enslaved by a system of economic and opportunistic inequality.

Of course there are good laws, but most of them are self evident and universally accepted with little need for enforcement. Who among us really argues that murder is wrong, or theft from individuals, or rape, or child molestation, etc.? Sure there are laws on the books against these activities, and sure there are times when they are violated and the violator needs to be removed from society for its own protection. But relatively speaking, the great, great majority of us are not murderers, are not thieves, are not rapists, etc. Furthermore, we need no law commanding us not to be those things because it is innate in most of us. It is part of our moral make-up. But as far as these are laws, they are good because they are universal and they are beneficial to all of us and to the perpetuation of our lives as a species. We cannot even imagine life in a world where murder for example would be considered good and indeed life in a world like that would be quite short.

And what of the lesser laws such as stopping at red lights? These are not innate, like the above, but conditioned through experience. While we all know in our gut that murder is wrong, we must learn that stopping at a red light is a good thing if we don’t want to have our car smashed and perhaps be killed in the process. But is it really a law or just common sense and self-preservation to stop at a red light? Is not the whole purpose of having a law that you must stop at a red light really in place to have someone to blame and be held liable for when it happens? So the law isn’t designed to protect the individual as much as to protect the insurance company. And indeed it is to give the local constabulary a justification to collect fines and fees. But all in all, law or no law, it is certainly advisable to stop at a red light for your own safety and that of your passengers and the persons in the other vehicles.

For all of the above examples and I’m sure a few more could be added, having laws against the activity is for the most part redundant because they are activities that any reasonable and even unreasonable person would agree are good ideas not to engage in. Here law only serves as a method of determining who to punish and hold liable when the offense is committed. But even to that extent, who benefits from the punitive measures taken? Is it the victim or their family? When a person pays a fine for committing an unlawful act does that money go to the person who was wronged? It ends up in government coffers and of course it also feeds the law industry because that is exactly what law creation is. It is a lucrative industry designed to make billions for lawyers and the state and often is used to help perpetuate the wealth of the corporate world as laws are tailor made for them.

Now there are laws that I hesitate to even assign that designation because they are created to protect us from ourselves or to protect special interests using the “justification” that they are there to protect us. A perfect example is the seat belt law. This law was created for the insurance companies who saw it as an opportunity to pay less in medical claims because injuries are generally less serious and thus less costly to pay for. Now I am not arguing that it is a not a good idea to wear a seat belt, in most cases it probably is. What I am saying is that being forced to do something, even ostensibly for your own good, diminishes you as a human being and is a violation of your sovereignty as a free moral agent. Secondly, the law favors an entity (the insurance company) over a flesh and blood person. Additionally, with the same logic, what would keep health insurance companies from lobbying for legislation against eating fast food because it is unhealthy and increases health care claims. I would imagine this will not happen however because it would result in one powerful and wealthy lobby, the insurance lobby, going head to head with an equally powerful and wealthy lobby, the fast food industry. Money and power talks on Capitol Hill, but who speaks for trampled on individuals? Nobody unless you can afford an attorney, and who can? Indeed justice is not completely blind, it sees dollar signs and unlike the pledge of allegiance there is not” justice for all”; only for those who can afford it. And the reason this intolerable situation has lasted so long is proof of how entrenched the corruption is. Another example is the laws against marijuana use. The idea behind the laws against marijuana is that it is harmful and the state has a duty to protect its citizenry from harmful behavior. Okay, what about tobacco use? Hasn’t it been proven that tobacco is a major health hazard? Of course it has. But tobacco will probably never be outlawed because it is an extremely lucrative business and has a very well powerful lobby in its arsenal. The glaring hypocrisy of  the state and legal system is never so blatantly evident as when money and power trumps protecting the people from harm.

And what about tax law? Who benefits from it? Would anyone invest money in something where there was no return on their investment? Would you donate money to a cause you didn’t believe in? What benefit do we get from our tax dollars, particularly the federal? Now I enjoy having nice paved roads to travel and I know that part of my taxes go toward their maintenance. I know that part of my local taxes go toward maintaining a police department which I am grateful for and educating my son and the children of my community which is a positive thing. But just how much of my tax dollars are allocated to programs and services which me and the people in my community benefit from? The problem is we don’t know because we do not receive an itemized list that breaks down every expense that our tax dollars go to meet. We must report to the taxing authorities every dollar we earn and then account for every dollar spent on tax deductible expenses and contributions but the taxing authority does not reciprocate. They just collect and we have no idea where the money goes from there. Indeed the most “bang for our buck” is probably garnered with our local taxes. There tends to be more accountability at home. I don’t disfavor taxes in principle as long as they are voluntary and as long as they benefit everyone equally. Otherwise taxes are nothing more than theft and the tax collector should be treated as nothing less than a thief.

So, returning to the idea of law; I contend that the laws that are justified are redundant to ethics and all the rest are unjustified and deserve no honor or obedience. The illegitimacy of “laws” which violate personal choice in order to protect one from oneself is compounded by the fact that the intent of its creation was primarily to protect the profits a nameless, faceless entity such as an insurance lobby. What about prostitution? Is it really the sex act between consenting adults that is being outlawed? Sex between consenting adults who are not married to each other happens every day! The laws against prostitution are there because there is a strong conservative religious lobby in Washington. It is a law created to benefit them and by extension keep certain politicians who are supported by them in power. Simply stated any “law” that is designed to benefit or was created through the influence of a particular group or lobby is unjustified. If a law does not benefit everyone equally and violates any form of self expression and personal autonomy, which does no harm to others, it is unjustified.

The overabundance of laws and the convoluted way they are interpreted serves a dual function: First, it paralyzes people by making them fearful of becoming lawbreakers with all the consequences that go with it while instilling in people a sense of precariousness and uncertainty about their lives. Of course all this helps to make a person more easily controlled by the power structures. Secondly, it keeps lawyers in business. Laws that are vague and not easily interpreted require high priced attorneys and the judicial machine. But this system excludes most of us because we simply do not have the time or resources.

It is time to reaffirm what is already ours and reclaim our individual sovereignty. It is time for our self ownership to be reaffirmed and lived out in life. It is a metaphysical fact that we own our bodies and minds. All other ownerships can be challenged and are transitory at best, but self ownership is undeniable and permanent as long as we are living beings. Therefore it is ultimately, indeed must be our decision as to how we will conduct our lives the only law that we must accept is to do no harm to others and to recognize and respect the personal sovereignty of the other as they must ours. Recognition and respect of every person’s individual sovereignty is the only way in which systems of mutual cooperation can be successfully developed and maintained. And indeed is the only law required for peaceful coexistence with the greater society. But it is not a law of compulsion like most laws, but is rather the natural state of things such as the laws of physics. No person ever felt that the laws of physics ever diminished their freedom but rather defined the boundaries within which real freedom can be exercised. So the law of mutual respect for the sovereignty of others is not only a boundary but indeed the foundation upon which true freedom for all can be solidly constructed to withstand all the torrents and storms of societal living.

Returning now to law, only a law that respects and protects individual autonomy and self expression has any true justification. Any laws designed to protect corporations at the expense or diminishment of the individual is unjustified and must be disregarded as it is always the individual and by extension, the greater society that must be the primary, indeed the sole concern of government if government is to be legitimate. A government that protects the interest of the capitalists while ignoring the needs of the individuals has violated the social contract upon which its very justification rests. Therefore it is the duty of the collective individuals to withdraw their consent and thus nullify the social contract which at best was only tacitly agreed to by the masses from the start.

Therefore rejection of state control over our lives, until such time that the state recognizes its true accountability is to the people, not the corporate interests, will also be the catalyst for the destruction of the restrictive legal apparatus which has enslaved and criminalized the people while creating a whole new subclass of oppressors: the lawyers and the corporate elite.

How can the individual and by extension the collective society go about throwing off these chains of oppression? First adopt the empowerment philosophy that you are right ethically even if the state says you are wrong legally. Again, any law that is not designed to protect and perpetuate individual sovereignty and by extension societal sovereignty is unjustified and deserves not just to be passively ignored, but aggressively resisted. Live your life by the ethic of mutual respect and tolerance for all regardless of race, gender, sexual orientation and religious affiliation. Concern yourself less with being a law abiding citizen and more with being a decent human being. Redefine patriotism to mean a love and pride in your country as you love and take pride in your garden or your backyard; not as a blind acceptance of the propaganda that your nation or your ethnicity is better than others. Remember, every state needs an enemy from without so that the citizens take their attention away from the enemy at home. Determine to have no enemy except state and capitalist oppression. Practice civil disobedience, or better yet selective obedience. Obey the laws that perpetuate social harmony while preserving individual autonomy. These are the only legitimate laws anyway and they are self evident and don’t require an expensive lawyer for their interpretation.

Whether done passively or in a more pro-active way, marginalize the state. Many philosophers have suggested that much of our reality is self-created and may be more subjective than once believed. If this is the case, it certainly behooves those in power to keep the people in a state of disillusionment and to have them feel they have no power over their lives. This may indeed be the greatest weapon the state has. As long as we are afraid to do anything for fear of breaking some unknown law or for fear of being sued for breaking a law we didn’t even know existed we will not rebel or believe that successful rebellion is even possible; we will just resign ourselves to oppression and meager subsistence believing there can be no other way. The thought that we can help to create a new reality by simply collectively changing our thinking will never occur to us in a condition of perpetual oppression and fear.

The state as it exists now is in violation of the social contract and does not have the best interests of the people at heart. Therefore we must withdraw our consent, marginalize the state and the coercive capitalist institutions and perhaps through a change in our subjective experience we can bring about a radical change in the objective reality.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

