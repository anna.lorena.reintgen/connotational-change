        Works Cited
With renewed interest in North America of building dual-power, and forming strategies to achieve these objectives, it is important to look around the world at existing modes of resistance and power-building. This strategy is well known to anyone following efforts of the Zapatistas, the Self-Administration of Northern Syria (Rojava), Bakur, and recently the Mapuche people in the Wallmapu region of South America. These struggles forsake the creation or control of the State as a goal, and create a dual-power scenario within the states they occupy. This approach broadly aligns with Murray Bookchin’s political vision of Communalism, adapted and to specific programs for specific contexts. Indeed, many of these struggles predate Bookchin or have developed parallel to his work.

It is in this context that I bring up the iTaukei (E-tow-kay) people of Fiji, for whom many aspects of the dual-power struggle for autonomy against the state are built into the foundations of local self-government. Dual-power in Fiji shows us the importance of communally-controlled land and taking power from local and municipal government organs, as well as how these can contribute, even in a latent context, the weakening of state power that Communalism calls for. In addition, it shows how these institutions can indeed become the forms of freedom, and that it is up to people and organizations to give it content.

First, a brief sketch of the elements of grassroots, confederated direct democracy found among the iTaukei peoples. Villages throughout Fiji only have limited penetration by the cash economy and state control—which leaves the community largely free to direct and decide its own development. They are confederated by region into larger council bodies. Issues are raised in a monthly village meeting called a bose va koro, wherein people take part in the indigenous practice of talanoa (story telling). Talanoa functions as a sort of unity of work and play—people discuss the goings-on in the village, air grievances, laugh, joke, and report from the various councils—all while drinking kava and smoking cigarettes. In this way, the tedium of the meeting is suffused with care, humor, and interest. There exist various councils that the villagers can be a part of, depending on the village itself. These councils include: crime and mediation, water sanitation, health and safety, finance and investment, community development, women and youth groups, church groups, and elder care. This is combined with the iTaukei concept of living vaka vanua, or with the land. Vanua has multiple connotations: it can simply mean the land itself, or can be reference to both the land and the people that inhabit it. In addition, it implies a style of living—in harmony with the land, with one’s community, and putting the well-being of both above profit for oneself (Parke, 2014).

The bose va koro have been codified into the government through the Ministry of iTaukei Affairs, which has a number of other interesting features, including the National Land Trust Board (NLTB), which keeps track of lineages and doles out money made from leasing to the clans. The grassroots meetings connect to a system of district and provincial meetings comprised of hereditary chiefs to decide the affairs of villages on the regional, provincial, and national level. While these councils are currently controlled by chiefly elites, this was not always the case. There is historical and anthropological evidence to show that making these positions hereditary and giving them so much control over these councils is a result of British colonialism (Macnaught, 2016; Parke, 2014). Before that, the leaders of these councils came about through support of their people in various ways (valor in war, service to people, or as a compromise between various factions) and their power over the rest of the village was in many places negligible. Another major aspect of this ministry is the communalization of land rights. Over 80% of the land of Fiji is owned communally by various clans—it is inalienable and the financial proceeds of leases and other development projects are distributed to clan members annually through the NLTB. This has in large part stunted capitalist penetration into the villages of Fiji and prevented their proletarianization (Norton, 2012). The iTaukei people can participate in commerce at will, and have land to farm and live available to them without rent or tax.

In this brief outline, we can see elements of the “forms of freedom” that Boookchin discussed. The power given to village and municipality, even in latent form, is worth investigating deeper for Communalists interested in building dual-power. In the bose va koro, villages have the potential to shape their development and culture as they see fit, which leads both to instances of domination and control but also mutualism and horizontal decision-making. Despite this, looking to indigenous struggles for autonomy is an important cornerstone of Social Ecology, and the iTaukei people of Fiji have been grappling with autonomy for well over a century.

Chodorkoff, Dan. (2014). The Anthropology of Utopia: Essays on Social Ecology and Community Development. Norway: New Compass Press.

Macnaught, Timothy. (2016). The Fijian Colonial Experience: A Study of the Neotraditional Order Under British Colonial Rule Prior to World War II. Acton, A.C.T.: ANU Press.

Norton, Robert. (2012). Race and Politics in Fiji (Second edition. ed., Pacific studies series). St Lucia, Australia: University of Queensland Press.

Parke, Aubrey. (2014). Degei’s Descendants: Spirits, Place and People in Pre-Cession Fiji. Australia: ANU Press.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

