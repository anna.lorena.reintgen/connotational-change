
How happy I was as a child to lie on green fields and look up into blue skies. The sweet smells of Spring would waft through the air as dreamed of my bright future. I dreamed of becoming a great man. I would throw fistfuls of gold out of my carriage and masses of poor and stunned people would worship me. I would build fairy palaces and alhambras. Rosy girls would attend to my every need in flowering gardens. Had I been able to throw myself directly into the work force I should have indeed become a rich and famous man. But alas, I would have only have had the possibility of becoming it, and was therefore not a great man. — Afterall who does not feel great Hope fill his breast? Whose heart does not ache with sweet impatience when he reads page after page about the great things the German people could do and about how everything could potentially become? Yes, we are referred to the following quote: “Hegemony: the German nature carries the stamp of intellectual supremacy and is uniquely blessed with an abundance of talent. Yet how is it that in such a union there is no nation? We are politically and militarily, philosophically and scientifically, poetically and artistically, musically and linguistically, industrially and nautically, and technically gifted. We are so rich in our talents that we outdo other peoples in uniting the individuals pieces of society. Germany identifies as belonging to the constitutional monarchy of the great polity of Europe” (pg. 169). Alas, one must be incapable of desiring blossoming hopes and one must cash in all sweet enthusiasm for homeland and humanity, if one did not want to longingly demand, contemptuously despise, joyously expect, and youthfully dream with the author. And I demanded, despised, expected and dreamed with him. I nourished my love on his enthusiasm and strengthened my beliefs on his hopes. I have lived in rich abundance — Then why am I still discontented, discontented with this book?

Therefore, because I do not find passion here, the passion that wakes the soul of men, therefore, because even here I meet the same well-meaning half-measure, that wants to reconcile before dividing that has come not to send the sword like Christ did, but rather to send peace. therefore, because wrath and hate do not burn here; therefore, because even this enthusiasm is nothing more than fleeting excitement and even this prediction of revival is nothing more than the political and diplomatic sayings of sly politics. Finally, I am discontented because instead of demanding us to look inside ourselves and to look at the disgrace of our broken souls the author speaks only of our future prospects. On a single page (pg. 171; for the flimsy second chapter is hardly worthy of our attention), the author dismisses our afflictions, and what horrid afflictions they are! The “Stader Zoll”, Hannover’s and Mecklenburg’s refusal to step up to the German Customs Union ect.; is only incidentally thought of in court constitution and press terms. Then he swiftly concludes : “Let us turn away from this sad view and look to the vision the future has to offer us.” He excuses this tenderness with the following: “It is of less use to illuminate single deficiencies where evil has taken root, not because it isn’t necessary or worth the effort, but rather because the Illumination of said deficiencies here is less effective than anywhere else; Because the nation has the clearest insight on countless things, whose repeal imposes the most simple understanding, and which in spite of the most outspoken will of the public voice spreads nevertheless.

“The nation has the clearest insight!” Tell me, where is the nation supposed to have attained this insight? For example, how many Prussians read more than just the national newspaper or other popular newspapers of the inland? Nothing is discussed in these newspapers but frivolous festivities. The atrocities committed by the rotten people are completely ignored. When will the decisive ideas of our modern age, like freedom of press, transparency, and self-determination be discussed in an inclusive manner. So far these ideas have only been negotiated in alienated chambers, as if these notions had no direct impact on us. Go into the provinces and be shocked to learn about the inexpressible efficacy of censorship. Such a widespread ignorance in regard to the holy as well as unholy questions of political life is hard to find elsewhere. And this ignorance is so ingrained that no quiet ray of reason may ever penetrate this wild darkness, and only a blazing bolt of lightning that starts a fire that burns everything to the ground will brink clarity and light. Already, I see a little cloud rising on the horizon. To be sure it is still inconspicuous and desperate. But many others see it too, although it is only visible to the Sunday children. There could be a pretty thunderstorm after the muggy days.

A book has appeared in the press-free country of Switzerland that we should not ignore just because we are ashamed. Rather, we should allow it to dismantle the hypocritical priesthood of the wolf soul. Neither tirades, admonitions, nor boring discussions gnaw at the heart and kidneys of the naked child. So exposed is this child that the cutting winter frost begins to freeze his limbs until his spirit begins to hunt helplessly for protection and shelter. There is a forty-seven page book, which I cannot name, but that has become as popular as the “Spener’she” Newspaper and has extinguished more than twenty years of this revolution.

Paper: which is to this day unsurpassed in the incomparably simple manner by which to revolutionize the spirits and awaken conviction. There is nothing that proves that we belong to the best race and that this race is blessed by God. But this was asked four times and answered four times. The author could have taken a hint from this and been more popular. But he is a German with skin and hair. On page 201 he says, “First the human soul must be dissected, its structure known, its functions described, its development explained; first we should work out the lessons we can learn from the spirit, the individual, races, nations, tribes, and families. Only then will humanity be able to open its eyes for the first time: It will get to know itself, the time of its emancipation has arrived. The more their self awareness grows, the more generally the psychological consciousness of the masses can understand themselves, the greater the possibility of attain the best of history — the perfect country. It must appear almost deceitful that I selected this laughable passage, in which the author recommended that the masses study psychology, in order to finally arrive at the “perfect State”. Unfortunately this is no incidental idea, no excusable side note, but rather it forms the base of the book. Can it amaze us that the Author is so enthusiastic about German-nature, when we recognize that he himself is such a thoroughly German man? Understand (this is about how Mr. Rohmer speaks) the character and the meaning of the Russians and Polish, the Frenchmen, Englishman and Spaniards, the Chinese and Indian, indeed all peoples of the earth, understand and compare yourself to them: then you will see that you are called to hegemony as the “most blessed” people, and you will fly on this knowledge and obtain it through unity. My book should help you come to every understanding in which peoples are reviewed and measured and it should seep into your heart. In this way unity will be found, and then you my German People cannot fail to be comfortable, for “the greatness in you must and will awake”.

No matter what, the “Holy Ghost” will come but no sooner than the time is right. And does the time come without any reason? We must make the time come. Pass through the country, go into each cottage, preach discord and even violence, not dull unity and comfortable contentment. We must wake the drowsy souls, not with fleeting hope of comfort, no, and with the knowledge that you are informing them of the gruesome happenings, the secret, forbidden stories, keep in mind that these trusting citizens didn’t even know to suspect such things.

What for does Mr. Rohmer plea we need connection? Are we Germans not connected? Do we not sing the same song: “what is our German Vaterland”? Do not all Germans happily great one another as friends and trust each other, and do we not feel connected in the idea or meaning of the word “German”? This Mr. Rohmer knows as well as we do; and still he says we do not possess this connection. With what right? Sadly, with one that is all too true! We are as connected as a herd of sheep. We graze in incomparable calm next to one another and defecate in masses in the barnyard and eat our meals while we allow our coats to be cut. From time to time, a wolf takes one of us as his meal, and in these times the cowardly insidious rascal might dress up like one of the sheep, as though this were necessary, and be unable to choose his meal because none seem to mind being chosen.

Sheep are connected, but they have to will. Rip one of our submissive people into shreds and we will all be filled with a feeling of connectedness with that individual.Show the horrors committed by the authorities elected by the people, those living in their comfortable homes with their children and do not wish to be imposed upon with these events, and these people will undermine every belief short of the one in the spirit to forget what they have heard and so they are dependent. Only when a human feels lost and alone will he rise up and realize such things; an urge will then flow through his muscles and his courage will swell and he will realize who he is and the power he possesses. Bravely he will no longer follow with blind trust and beliefs, and only those stripped of their blind will to follow will experience freedom. Have the courage to be destructive and you will soon see which wonderful flowers grow out of the ashes of what you have torn down.

Nothing can heal a person better than the power of a great thought, the kind that fills our souls with an incredible will that pushes us to act. Where in us lies this obligated desire that comes from a great idea, that no matter what, is unstoppable in creating a new world and being within ourselves? We Germans have great pride, which can become vanity and bring us to shame. But pride — pride we can only possess for one thing: we can only be proud of a freedom we have accomplished through thought, pride for the meaning of “I”. And still we are only free in a matter of thought and we are not yet proud of this freedom. No civilization has a greater right to describe their “I” with larger letters than the Germans, but we do not capitalize it: we leave it hidden and allow the English language to have the capital advantage. Let us accept the German “Ich” as the most important because the English “I” remains under the despotism and authority of the Church and the French is fading under the influence of the gloire — let us internalize this right and become proud. The pride is what we lack and the pride alone. Away with the humility, that harms itself! A man is independent! Stop asking for duties that become offerings; make your owns laws and then follow with your own will, knowing that you are then free.

But the time is coming that I address the reasons for this book: the groups that have refreshed my faith and if it were radical enough, and it must be because all else is too mainstream, I would attempt to explore these deep thoughts. The idea that Germany may be obligated to reach hegemony becomes like a red thread through the whole thought process. This same idea brings Germany a worldview that it must compare itself to other nations, to ask them what to do in order to establish a national character. In the introduction, the German consciousness is designated as that “more politically minor-ness”; out of the story on the one hand, the German’s intellectual salaries are indicated henceforth in rather wide comprehensiveness and with witty reflection. The situation with Europe, on the other hand, whose view forms the second part of the story, the occupation of Germany is supposed to be demonstrated. From this story, which was written with fairly clear detail and reflection, the opinions of Europe, which can be found in the second part of the book, should finally become the opposite of those of Germany. The story shows that Germany always made decisions and stood as the heart of Europe; now, finally, the Germans should, now that other nations are facing destruction, grow and build. Protestantism, like someone who has searched high and low for truths, must be at the center of principles as the only answer to the deepest questions and the solution to the most extreme problems. Protestantism must be freely accepted as part of the German identity. This principle will become the greatest yearning for all mankind, the kind of longing for a righteous word view, a knowing that there is a connection between man and God (Verf.). But it is not so simple; the longing does not occur for a correct or righteous world view, not for a theoretical contentment; instead, the longing is for a right to one self, and the ability to act as oneself. Each of Goethe’s hardships was absolved through the following and Shiller’s bragging was thereby extinguished. The question is not how should man behave towards God but how should God as a free entity be viewed by man and this is a question of yearning for time. “The millennium brings a ring of the natural reasoning for the true state” says Verf. (44). This goal, in regard to Mr. Rohmer’s idea, misleads us many times over and only so long as we want to produce something other then ourselves, we must make ourselves, manifest ourselves. To inform us and become honest souls will create a true state. We cannot create the state because the state is made up of the souls of its people that offer themselves freely as souls of the state. It is necessary for the people to come together, to see what belongs to themselves as belonging to the state and the other people and this is what is known as a state; the free connection of the people. The type of state that one can make like a college course pales in comparison to the former in terms of freedom just as the Church would. With such lack of freedom the church and the state become invisible. Mr. Rohmer asserts, however, that men should move ever more towards a stage in which their worth and health is established and the freedom of trade is theirs. It is to be accomplished through a condition focused on the persons (status — country), in which they should find its welfare and pains.

If on their part, the story now showed to which important roll Germany is appointed, the same results from a review of the loyalty of other peoples. “The political situation of Europe, the role of the people and countries, the state of its ambitions, should show us their occupation, as is inferred by Germany’s central position in Europe. In truth, since the Reformation, at least for those given a sharp impression and see only the surface, since the revolution, Europe is seen as an incessant fermentation pot: Everything only transitional, only crisis or intermezzo; the new Age in which strives, the innumerable spasms of humanity, must yet be born. The pains of birth, here violently convulsive, though gradually borrowed, has depicted the story of Europe (not to say: the earth) since 1789.”

In the fourteen chapters of the second part, European unity is discussed multilaterally. The most important of these uncovered, would bring the reader an enjoyment, to whom we urgently summon to be minded insofar; it would also be in vain effort, to want to hint at such rich material so briefly and unpleasantly. The sum is as follows: “Unite — and two great powers will be inspired out of your will, and it will become a single power with two arms. Unite-- and Holland will sacrifice the old rigid spirit for you, and, what in Belgium and Switzerland is German nature, will itself become the new light with or without the desire to do so. Unite — and Scandinavia will grasp your hand. Unite — and England will seek your alliance at the first sign of danger. Unite — and Russia will tremble, while Poland finds hope. Unite — and Austria, the substructure that supports Germany and Hungary, will rise at your will to the law in the question of the orient. Unite — and Italy will desire the support of your forces in the future; and yes through your unity, Portugal, Spain and France will be compelled to unite as well. If only you were to unite — you would be the first people of the earth”. (Pg. 161)

But how do we achieve this unity? By what means is it supposed to be created? “One principle requires time, in order to destroy the demands of falsehood, and to create and proclaim the new truth. “And the creator of this principle — that even Mr. Rohmer himself recognizes, that no philosopher can in any manner name — this is how the philosophy should be. “We all know that only the German philosophy can lay for us the cornerstone of a higher future.” (pg.188) We arrived at this conclusion in the final chapter of the book. How one must regret here, that the author knows so little of the subject that he discusses! He wants only to describe to be sure, the “general effect of the philosophy on the time and people;” however, how would he recognize the effects without understanding the causes? He will not look at the innumerable effects of the philosophy or see all its effluences in the same way, because his eyes have never perceived the [its] source. As a spectator from a distance he makes ingenious observations and opens us up to many a bitter truth; he does not however, touch on the essence anywhere, and, what pertains uniquely to the meaning of Hegelian philosophy. In so doing his negligence allows for naivety and ignorance-- traits of which we already so long are accustomed to by our most valiant southern brothers. “Spruce” is to him “an entirely other and only man”, and certainly “he should reverently hold the German nation eternally as one of the rescuers arising out of deep need; if however Hegel is admittedly of the same motto, certainly the people are no longer under the foreign currency: For King and Homeland! Igniting a war of revenge, he knows — yet in the whereto I become lost. Should I, like the author, only factor in the effects of the same right of Hegel and philosophy? I esteem him highly but repeat that his book, with all its joy, does not come from inertness of the heart — for of those matters he understands nothing. He, who knows for example only a “Burdachian” or “Schubertian” psychology, not to mention Hegelian and subsequent psychologies, the psychology that we now possess, can only as “a miserable conglomerate of notices and observations” and that “none can name a science that would possess that courage”? The fantasticalities Mr. Rohmer generally understands under psychology, is already indicated above. This he calls “the apprenticeship of the spirit”, and expects its implementation in the golden days. On pg. 200 he prescribes not only the problem of the philosophy, but rather also the results to which it must lead; that wholly arises out of the needs of the heart, although shortly before (pg.196) he did not want to hold scholasticism for philosophy as such. — Certainly the philosophy and newer development does not have all the effects, that the author wishes would become fulfilled; but what lies just in these wishes, is that, as already mentioned, reconciliation comes without divisiveness, and the old concepts from “the unification of country and church, sovereign force etc.” separate of new, or rather not — are radical.

Unfortunately we cannot fend off the sad outlook that the author preaches his prophetic world view throughout the entire book to mostly dumb ears. Those who, enthused by it (i.e., the world view) might move toward its implementation, have only a sense for their own violence and its justification, without thinking and intending anything cosmopolitan. The others, however, who are only to be won over to it (i.e., the author’s world view), are still far away from freeing themselves from their subservience (Bedientenhaftigkeit) of opinion to think of ideas as something more than an entertaining fancy/enthusiasm. Only the youth and the youthful spirits remain, and in their hearts, this sowing — that we hope for — so luxuriously, will rise that the weeds of the selfish power-holders — and they amount to millions — cannot proliferate any further.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

