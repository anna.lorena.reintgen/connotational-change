      Anarcha-feminist theory      Organising      Analysis of Power      Our political practice
Because anarchism is purported to oppose all usage of power and forms of oppression the term anarcha-feminsim should actually be unnecessary. All anarchists should, if they really meant what they said about being against all forms of oppression, work against, or at least not support, the oppression of women. That’s theoretically. However, our reality is that we are all products of our societal surroundings. It is also a fact that those who find themselves in a hierarchical position of power have a hard time accepting that a hierarchy even exists! Men do not recognise the oppression of women to the same extent or to the same degree that women do. Those who have power and privilege are in addition, often unwilling to relinquish these. Because of these reasons, many male anarchists have not activated themselves in the struggle against the oppression of women and, for these same reasons, it has become necessary for female anarchists to denote themselves as anarcha-feminists.

Feminists can be divided into two types, essentialists (difference) or constructionalists (equality). On the one side, essentialist feminists propose that the differences between men and women are based upon the occurrences of nature. On the other side, constructionalist feminists propose that the differences between men and women are a result of societal socialisation. Anarcha-feminists are constructionalists. The norms controlling in which ways men and women should present themselves or how they should interact with each other are regarded as social constructions. In order to change these power-related relationships between the sexes it is therefore, to a large degree, necessary for people to change themselves. This is the logical conclusion of examining how we as individuals are affected by the societal demands placed upon us as men or women. In this perspective, this socialisation pertains to matters ranging from ones choice of profession and clothing to sexual partners. In regards to sexuality we are socialised into heterosexuality, and this is one of feminism’s most complex aspects.

Women, as a social group, interact with men, as a social group, in a completely different manner than, for example, in which the working class interacts towards the upper class. This is because many women have intimate relationships with men. During the 1970’s, many radical feminists spent a great deal of time and energy on analysing heterosexuality’s meaning for the oppression of women and many came to the conclusion that women should choose to live as lesbians as a method of revolt against patriarchy. This is a strategy that perceives people as tools used to achieve different goals and does not pay a great deal of attention to individual feelings or situations. However, these analyses have meant a great deal for the development of feminist theory. They concern how we regard heterosexuality’s penetration of society and how this affects relationships between people. Heterosexuality is posed as the “natural” order of things and takes support from such sources as the Bible and Darwinism. If heterosexuality is so “natural”, why the need for all this propaganda?

The radical feminists who supported lesbianism as a political strategy used strongly authoritarian arguments. They took upon themselves the role of telling other women how they should regard heterosexual relationships and what they should do with their lives. An anarcha-feminist should contemplate upon these insights in regards to heterosexuality as a societal phenomenon and hopefully reach their own conclusions, such as a libertarian method of struggling against the heterosexual norm without condemning the free choice of individuals. We should always question the idea that heterosexuality is something self-evident and natural and attempt to make truly conscious choices concerning our lives. We should also attempt to focus attention upon homosexuality and bisexuality and struggle against the violence and oppression that these individuals are exposed to.

Another aspect of the reality that women live under today is that of ideal beauty. Everyday we are exposed to pictures of thin women, women without hair on their legs, women who have perfect skin, women who are made-up and women who wear the latest fashions. This ideal, which is represented by tall, thin photo-models and participants in beauty pageants, is far removed from the actual appearance of the majority of the female population. This results with the larger portion of the female population feeling unsatisfied with their own appearance. More and more women suffer from eating disturbances today, and this is not a reference made only to those who starve themselves or force themselves to regurgitate their food. These extreme examples are of course eating disturbances and should be treated as such but, many women are held in the constant stranglehold of dieting and this is regarded as normal. It is nearly considered to be more atrocious to not be concerned about ones weight and eat a bit less just before the swimsuit season than not. Eating disturbances have become a more or less normalised female behaviour.

If we are to change this society we must examine our own lives. Anarcha-feminists attempt to free ourselves from these norms and ideals which we have been exposed to. Through the power of our own example, which is a conscious strategy, we want to prove that it is possible to change ones life. We discuss in affinity groups, we organise lectures and cafés and we hold courses in self-defence. And we do this in an attempt to become those women we want to be and not those women who are expected of us. But, this must be done as a collective force because as individuals we can never free ourselves, for an injury to one is an injury to all.

The majority of the radical feminists of the 1960’s and 1970’s were organised in small groups without leaders. They put anarchistic and anti-authoritarian ideas into practice without realising that this is what they had done. Professed anarcha-feminists have since then continued to organise themselves in this fashion, sometimes with radical feminists, sometimes separately. To organise separatistically, in this case in groups of women only, is a strategy that originally comes from the black civil rights movement in the united states. This strategy has been, and is currently, practised by many different groups, such as homosexuals, the crippled, immigrants as well as many others. Separatism can reach different extremes. With our reference point in an all-encompassing anarchistic perspective we choose to work parallel with, and sometimes together with, other movements.

Radical feminists and anarcha-feminists often organise themselves in affinity groups that work with conscious raising activities. This means that through sharing and discussion the group participants help each other to expose structural oppression and recognise that different oppressive occurrences haven’t just happened to them. In an affinity group participants can discuss a wide variety of topics such as how they relate to the beauty ideal, how their relationships work as well as much else. Other women’s groups may function as study groups or commandos.

An organisational problem that may be specific for Stockholm, as well as other large cities, is the fact that the radical political scene is concentrated in the inner city. This is where we have our spaces, our cafés and where we often organise demonstrations. This is besides the fact that many of us live in the outlying suburbs. Some of these suburbs have as many immigrants as an entire small town. The conclusion being that if we were to organise ourselves locally we would more than likely be able to reach a larger number of interested individuals; especially women with children who do not have the opportunity to travel to the inner city to attend meetings. A decentralising of our political scene could be of great importance.

A consciousness of how all of societies many hierarchies work together and reinforce each other has been around quite awhile. Just as the conclusion that these hierarchies must be fought in parallel. At a women’s conference organised in 1975 by the New American Movement, one parole was; “We are united in the understanding that all oppression, whether directed towards class, sex or sexual preference, has a mutual interaction and likewise, all struggle for freedom from oppression must occur in unison and in co-operation.” Much later, autonome activists in Germany coined the term “triple oppression” which is an attempt to explain how racism, sexism and class oppression work in unison. This phrase was first presented in an article written by the imprisoned German anti-imperialist Klaus Viehmann. Using an example, Viehmann explains his view upon how all forms of oppression, not just the three named above, interact:

“It’s not a bad idea to conceive of supremacy as a sort of net. The meshes can be bigger (metropoles) or smaller (Third World). The threads can be older (patriarchy) or newer (capitalism), more stable (Germany) or weaker (Central America). The threads are knotted in different ways (racisms are connected to capitalism differently than patriarchy is, for example), and the net is constantly being repaired and renewed by many different forces (capital, state, whites, men) so as to catch others in it (women, blacks, workers), and these tear it as best they can.”

Anarchists in Sweden have further developed these ideas and coined the expression “co-operation of oppressions” (förtryckssamverkan). Partly to have a Swedish term for the phenomenon and partly to make clear the fact that it is not only racism, sexism and class oppression which are important oppressions to struggle against. With this in the ideological baggage it is has become quite important to employ forms of struggle in which no form of oppression is supported or upheld. An example when this can go awry is when feminists, in an attempt to expose family abuse, state that the most dangerous place for a woman is in her own home. This statement supports the idea that most women either live with men or have men as guest in their homes. This is of course true for many women, but highlighting the fact that many women do not live with men does not lessen the seriousness in that which is being said concerning abuse.

It may seem a contradiction to use separatism on the one hand and struggle against all forms of oppression the other, but it is this complex perspective that is the core of our strength. We can choose to work with a specific question or a specific group but the complexity of today’s situation is always in our consciousness. Although many anarcha-feminists work mainly within the feminist struggle, we are continually conscious of other oppressions such as racism and homophobia and that consciousness is carried over into the feminist struggle. It may involve attempting to recruit women with different ethnic backgrounds or to expose the situation of lesbians. I certain questions we may decide to co-operate with other groups, while in others we may decide to work completely separatistic. It is also important to acknowledge that many of us are members or different oppressed groups and it may not be so clear that we should work in just one specific group, but decide to work parallel in many groups. Personally as a woman and lesbian I work in two different groups. I many different groups all of the societal hierarchies are more or less represented. In our anarchistic groups there are people with different class backgrounds, there are crippled individuals, immigrants and Swedes, homosexual and heterosexual, both youths and older people and so on and so on. This means that we are forced to struggle against these hierarchies within our own groups.

Anarcha-feminists in Sweden today work with the following methods; affinity groups, lecture cafés, courses in self-defence, demonstrations and direct action against for example Hennes & Mauritz and pornographic boutiques. Direct action is one of our main strategies. It is an extension of the anarchistic tradition of acting without representatives in questions that directly affect oneself. Instead of voting for politicians who we want to do certain things we do them ourselves. We directly confront the problem.

Our struggle has come to focus upon questions such as pornography and objectification and violence against women. Other, more classic anarchistic, struggles have fallen by the wayside. This has much to do with the fact that we have been heavily influenced by radical feminists who work with these above-mentioned problems. It also has partly to do with the fact that sexualised oppression is so massive that it feels like a much more acute and actual problem than capitalism or the parliamentary system. However, it is safe to say that anarcha-feminists work with these questions in a very different way than radical feminists do. We use direct action and do not believe in using the judicial system as a way of guaranteeing freedom by, for example, forbidding pornography.

A question of increasing interest is how we relate to questions of work and unemployment. What is the situation of women in the workforce today? How would we like it to be? Many political activists are relatively young and many are unemployed. It is relieving not to have to wonder about finding a job which is both worthwhile and which doesn’t stride against ones political beliefs. Living on social welfare and working with politics or studying full-time is quite alright, but for how long, and what happens with us when we become older and still can’t find a job — or only a demeaning “shit job”? In the long run anarchists want to reorganise the entire societal economy and remove wage slavery, but how do we achieve this?

Anarcha-feminism is the result of many different influences and we co-exist with many different tendencies in the political arena. We do not strive for the creation of a mass movement but to be everywhere within society. We spread our ideas and strategies in order to increase democracy and equality on every level wherever we are.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“It’s not a bad idea to conceive of supremacy as a sort of net. The meshes can be bigger (metropoles) or smaller (Third World). The threads can be older (patriarchy) or newer (capitalism), more stable (Germany) or weaker (Central America). The threads are knotted in different ways (racisms are connected to capitalism differently than patriarchy is, for example), and the net is constantly being repaired and renewed by many different forces (capital, state, whites, men) so as to catch others in it (women, blacks, workers), and these tear it as best they can.”

