    The Nature of Colonization — Empires, Land, and Cultures      The Ideology of Colonization      The Question of Land and Resources    Colonial Identity      The Colonizer        The Colonialist that accepts        The Colonialist that Resists      The Colonized        Images and Myths of the Colonized        Dependancy Complex of the Colonized    Colonial Conflict/Relationship      The Settler Village      The Native Village    Freedom from Despotism      Decolonization      Cultural Revitalization    References cited    References used but not cited
Those who discovered and conquered other lands were entitled to them, their riches, and their spoils. The conquered people could be treated as slaves, banished to other lands, or assimilated into the society and institutions of the conquering people — Vine Deloria, Jr. (1983)

The present global stratification and make-up has been dictated in totality by the colonization and conquest of European nations. Although direct colonialism has largely ended, we can see that the ideology of colonialism has lingered in the identity of people within the general cultural sphere as well as the institutions of political, economic, and social practices. Colonization or the “colonial complex” is: (1) colonization begins with a forced, involuntary entry; (2) the colonizing power alters basically or destroys the indigenous culture; (3) members of the colonized group tends to be governed by representatives of the dominate group; and (4) the system of dominant-subordinate relationship is buttressed by a racist ideology. (Marger, 2000:132) This process has created the identities of both the colonized and the colonizer with pathological effects. It has destroyed both the lives and the cultures of the colonized and implanted a culture of destruction upon all inhabitants, both the colonized and the colonizer. There are two reasons for exploring the pathology of colonization. First we must understand the creation of the present social, political and economic dichotomy we face, but more importantly we must understand the psychological problems created by colonization, so we as humans can deconstruct the present Leviathan we live in and create a world based on cultural diversity, liberty, and mutual aid.

Colonization is based on the doctrine of cultural hierarchy and supremacy. The theory of colonialism is the domination by a metropolitan center which rules a distant territory through the implanting of settlements. It is the establishment and control of a territory, for an extended period of time, by a sovereign power over a subordinate and “other” people which are segregated and separated from the ruling power. Features of the colonial situation include political and legal domination over the “other” society, relations of economic and political dependance, and institutionalized racial and cultural inequalities. To impose their dominance physical force through raids, expropriation of labor and resources, imprisonment, and objective murders; enslavement of both the indigenous people and their land is the primary objective of colonization.

Another technique used to subdue the native population is the sacking of cultural patterns; these cultural values are stripped, crushed and emptied. The colonialists see their culture as a superior culture; usually tied to either Cultural Evolutionary or Social Darwinist theories. In an attempt to control, reap economic benefits, and “civilize” the indigenous peoples the colonialist dismantle the native cultures by imposing their own. There is a destruction of the cultural values and ways of life. Languages, dress, techniques are defined and constructed through the ideology and values of the colonialist. Setting up the colonial system does not destroy the native culture in itself; the culture once fluid, alive and open to the future becomes classified, defined and confined through the interpretation, imposed oppression, and values of the colonialist system. At this point the native culture turns against its members and is used to devalue and define the identity of the native population.

Their constant and very justified ambition is to escape from their colonized condition, an additional burden in an already oppressive status. To that end, they endeavor to resemble the colonizer in the frank hope that he may cease to consider them different from him. Hence their efforts to forget the past, to change collective habits, and their enthusiastic adoption of Western language, culture and customs. (Memmi, 1965:15)

Human History is rooted in the earth, everything is centered around or is connected to our use of land and territory. This has meant that much of human activities has revolved around the territory they live in and extract resources from. This has lead some cultures to desire more land and obtain new territory; therefore they must deal with the indigenous peoples of that land. At a very basic level colonialism is the desire for, settling on, and controlling of land that a culture does not posses; land that is lived on an owned by other people. Edward Said points out the rate at which Europe acquired lands at the end of their colonial reign.

Consider that in 1800 Western powers claimed 55 percent but actually held approximately 35 percent of the earth’s surface, and that by 1878 the proportion was 67 percent, a rate of 83,000 square miles per year. By 1914, the annual rate had risen to an astonishing 240,000 square miles, and Europe held a grand total of roughly 85 percent of the earth as colonies, protectorates, dependencies, dominions, and commonwealths. (Said, 1993:8)

It was not only the acquisition of land that drove colonialism, but there was also a desire for natural resources and labor. The colonialist countries needed raw materials to support their growing economies. Places such as the Americas and Africa offered natural resources they could utilize for manufacturing — as well as opened up new markets to sell their goods. Political structures of the colonial countries both economically and militaristically backed the establishment and maintenance of the colonies, but it can not be ignored that a high percentage of the funding for the colonies were provided by emerging capitalists; the Europeans extended their power by promoting merchant houses and chartered companies. In settler colonies like Kenya and Mozambique, there was a plantation-based export-commodity production of products like cotton, tea, coffee and sugar. Places like South Africa and Zaire were exploited for their gold and diamond mines. For the economies of the colonialist states the resources were harvested by the native populations (either through direct slavey or extreme wage-slavery).

“The colonial situation manufactures colonialists, just as it manufactures the colonized (Memmi, 1965:56).” If we are to look at how colonization created the identities of both the colonized and the colonizer, we must recognize that historical situations are created by people, but people are in turn created by these situations. The way a person sees the world, both geographically and culturally, is dictated by their abstract understanding of the world. Although culture does exist as a tangible entity, it is the abstract ideologies of comparison between cultures that create cultural identities situated in social, economic, and political hierarchies. It is in this abstract world of ideas that the colonizer, by creating the “other” which was to be colonized, created his own identity in opposition to that of the colonized. (Said, 2000:71–74)

The colonist who was either born in the colony or traveled there to better himself economically (often those who traveled and established themselves in a colony were from poor or working class backgrounds; only in the colony did they have a chance to make something of themselves) and embraces the colonial structure in which he was, in his eyes, entitled to was obviously the majority of the colonists. By accepting the role of the colonizer, he accepted the responsibility and identity of both himself and the colonized. Although the colonized are an interracial and necessary economic part of the colony, the colonizer must disown the colonized and defend his identity both intellectually and physically. He must accept the violence and poverty he sees daily; it is his job to rationalize the actions of himself and fellow colonialists because he needs to absolve himself of the atrocities committed in the name of economic and cultural superiority.

This man, perhaps a warm friend and affectionate father, who in his native country (by his social condition, his family environment, his natural friendships) could have been a democrat, will surely be transformed into a conservative, reactionary, or even colonial fascist. He cannot help but approve discrimination and codification of injustice, he will be delighted at police tortures and, if the necessity arises, will become convinced of the necessity of massacres. (Memmi, 1965:55–56)

The contradiction of his lifestyle, even with the economic benefits and cultural justifications, takes a tool on his psyche. Deep down inside himself he lives with the knowledge of his actions, and no matter how much he justifies or rationalizes his behavior the colonist pleads guilty. But a person cannot live with such contradictions, and thus the colonist creates an identity to defend his actions. It is at this point that he creates the image of the colonist as a humanitarian, who just happens to gain economic benefit. In his eyes he is bringing “civilization” to the “savages.” As Social Evolutionary Theory teaches the colonialist; all cultures evolve into centralized industrial nations. He is helping these “backward’ countries reach their evolutionary goal. He is bringing high civilization to them, and yes there is some hardship, but evolution is rough; if the natives would just stop resisting this natural process and abandon their traditional ways, they could learn to live the right way.

At the core of the colonizer is his privilege, some individuals born or traveled to the colonies felt overt guilt for this privilege. At first they deny such privilege, but when it is in their face daily it can no longer be ignored; at this point they try to resist, but to do so would be to give up their privilege. He finds himself on the other side of the scale from the colonized. If his living standards are high, it is only because the colonized live in poverty. He has positions of authority because these positions are reserved for him. To refuse means to either withdraw physically from the colonial condition or remain there and fight to change them.

The choice to stay and fight puts the colonizer into a life of contradiction; he is now at odds with his country men, and cannot easily escape mentally from the concrete situations and ideology that make up the actual relationships of the colony. This contradiction deprives him of all coherence and tranquillity of his identity. He finds that it is one thing to refuse colonization, but it is quite another to accept the colonized and be accepted by them because who can completely rid themselves of bigotry in a country and system founded on such a principle. No matter how genuine he is, there remains a fundamental difference between himself and the colonized. “In other words, either he no longer recognizes the colonized, or he no longer recognizes himself (Memmi, 1965:32).” In resisting he is aiding the birth of a social order which may not have room for him.

He dreams of a new social order were the colonized stop being colonized, but he does not see a transformation of his own situation and identity. In the new harmonious social order he will continue being who he is, with his language intact and his cultural traditions dominating because though he hates the oppression of colonization he too buys the theories of Social Evolution. In other words, he hopes to continue his identity within the abstract concepts of the dominate culture with a situation where the dominate culture would not exist. He calls for a revolution, but refuses to conceive that this revolution would result in the overthrow of his situation and identity. It is hard to imagine or visualize one’s own end, even if it’s to be reborn as another; especially if like the colonized, one can hardly evaluate such a rebirth.

To justify the colonization of a people, images need to be created so that the subjugation makes sense. These images become the identity of the colonized. There are many images used, but one universal image that has been put on native people is laziness. This image is a good example of how the colonizer justifies his actions. This image becomes the excuse for the colonial situation because without such images the actions of the colonialist would appear shocking. The image of the lazy native is a useful myth on many levels; it raises the colonizer and humbles the colonized. It becomes a beautiful justification for the colonizer’s privilege. The image is that the colonized are unbearably lazy; in contrast the colonizer is always in action. It implies that the employment of the colonized is not very profitable, therefore justifying the unbearable wages paid to them. The logical assertion would be that colonization would profit more by employing experienced workers, but this is not the case. The qualified worker, then comes from the colonizer’s class; they then earn three to four times that of the colonized. It is more profitable to use the labor of three of the colonized and pay them less than what would be paid to one colonialist. Therefore the colonialist becomes the specialist, and the colonized become the laborers.

Dependancy Theory is when the colonizing states exploit their colonizing regions that enhance their own development and accumulation of capital. When wealth and resources are extracted from the colony, colonist stunts the development or undo past development. This lack of development or modernization is placed on the colonized as their failure to be able to compete with the colonial state. What development that does occur is then distorted by a dependancy relationship and creates both internal and external problems to the local communities, thus creating an image of inadequacy upon the colonized. The colonial states manipulate the industrialization process in order to increase their profits, by undermining the local autonomy of the native population. Often they control supplies and resources available to the colonized community, forcing them to produce cash-crops instead of food, then sell food at an inflated price to the native population. This not only makes the colonized dependent psychologically, but also dependent on the colonial system for basic resources. If one adds that many Europeans go to the colonies because it is possible for them to grow rich quickly there, that with rare exceptions the colonial is a merchant, or rather a trafficker, one will have grasped the psychology of the man who arouses in the autochthonous population “the feeling of inferiority.” (Fanon, 1967:108)

The image of the settler and native village, although a physical reality of habitation; there is a psychological distinction between the two, and when we see this physical and mental connection, there is an understanding of identity. The colonial world is really a Manichean world, there is that of the native village and that of the settler’s village. Between these two worlds are the policeman and the soldier, they are the true officials and liaisons of the colonial system. The dividing lines between these two separate worlds are the barricades, barbed wire and police stations.

The settler town is strong; it is made of stone and steel, and the streets are covered in asphalt. The town is brightly lit. The streets are clean and the people are clean. They are all well clothed and well feed. Education is a given in this world. “The settler’s town is well-fed town, an easygoing town; its belly is always full of good things. The settlers’ town is a town of white people, of foreigners. (Fanon, 1963:39)

The native village, otherwise known as the shanty-town, getto, or reservation, is an infamous place throughout the colony. The colonized are born there, and die there with no notice or thought given to them. It is rarely open, the space is cramped and stifling (both mentally and physically.) The people live on top of each other, hungry, malnourished, barely clothed. There are next to no streetlights and darkness is not only a physical but psychological reality. The walls that are built to keep the natives out of the settler town, in fact keep them in the squaller of the native town. There is no way out of this village. The barbed wire and lack of education, hand in hand with skin color, makes sure the doors are closed and the colonized stay in their village. “The native town is a crouching village, a town on its knees, a town wallowing in the mire.” (Fanon, 1963:39)

“The crux of the matter rests, not merely in the resistance to the predatory nature of the present Eurocentric status quo, but in conceiving viable sociocultural alternatives (Churchill, 1996:31).”

Most decolonization theory is solely focused on the decolonization of the colonized. There is a necessary reason for this. The issue of colonization and the atrocities committed by the colonists towards the colonized is no less than cultural and physical genocide, but as Frantz Fanon discusses in The Wrenched of the Earth, there is no way to return to a pristine/pre-colonial time, so the only way to change the stratification of the post-colonial world is through decolonization. But this decolonization cannot just be of the colonized, this process must be also of the colonizer. White people need to deconstruct their culture and ideologies because the stratification is founded and maintained in our hegemony in regards of this culture of colonization.

The new relationships are not the result of one barbarism replacing another barbarism, of one crushing of man replacing another crushing of man. What we Algerians want is to discover the man behind the colonizer; this man who is both the organizer and the victim of a system that has chocked him and and reduced him to silence. As for us, we have long sense rehabilitated the Algerian colonized man. We have wrenched the Algerian man from a centuries-old and implacable oppression. We have risen to our feet and we are now moving forward. Who can settle us back in servitude? (Fanon, 1965:32)

The key to decolonization is a conscious act of cultural revitalization. There needs to be a rebirth of cultures dismantled during colonialism. The cultures of colonized and traditional people need to teach this culture lessons of the past. In this I don’t mean teach the Eurocentric power structure the mistakes of their past, but the teaching of traditional knowledge, values and lifestyles. This also means returning stolen lands and creating relations that are not based on white privilege. In a very real sense, we need to overthrow our own existence to be reborn, in the sense that Memmi speaks of. “Only in that way can we transcend the half millennium of culture shock brought about by the confrontation with Western civilization. When we leave the culture shock behind we will be masters of our own fate again and be able to determine for ourselves what kind of lives we will lead.” (Deloria, 1999:153)

With understanding of political, economic and cultural knowledge of traditional culture, white people can break away from the pathology created with colonization and live in a culturally diverse society; one in which we live under a new cultural understanding in solidarity with those living within their traditional cultural ways. “I may say that I believe such an agenda, which I call ‘indigents,’ can and will attract real friends, real allies, and offer real alternatives to both marxism and capitalism. What will result, in my view, is the emergence of a movement predicated on the principles of what are termed ‘deep ecology,’ ‘soft-path technology,’ ‘anarchism,’ and global ‘balkanization.’” (Churchill, 1996:480) This traditional knowledge offers us a way out of the stratification and poverty, both economically and psychological, that we face to day. It is a starting point to destroying the structures established by colonial ideologies, and creating a society based on equality, liberty, and mutual aid.

Churchill, Ward From A Native Son. Boston: South End Press, 1996

Deloria, Vine Jr. and Clifford M. Lytle American Indians, American Justice. Austin: University of Texas Press, 1983

Deloria, Vine Jr. Spirit & Reason. Colorado: Fulcrum Publishing, 1999

Fanon, Frantz The Wretched of the Earth. New York: Grove Press, 1963

Fanon, Frantz A Dying Colonialism. New York: Grove Press, 1965

Fanon, Frantz Black Skin, White Masks. New York: Grove Press, 1967

Marger, Martin N. Race an Ethnic Relations 5th Edition. Belmont: Woadsworth, 2000

Memmi, Albert The Colonizer and the Colonized. Boston: Beacon Press, 1965

Said, Edward Culture and Imperialism. New York: Vintage Books, 1993

Said, Edward The Edward Said Reader. New York: Vintage Books, 2000

Anzaldu’a, Gloria Borderlands La Frontera. San Francisco: aunt lute books, 1987

Churchill, Ward ed. Marxism and Native Americans. Boston: South End Press, 1983

Churchill, Ward Since Predator Came. Littleton: Aigis Publications, 1995

Churchill, Ward Fantasies of the Master Race. San Francisco: City Lights Books, 1998

Fanon, Frantz Towards the African Revolution. New York: Grove Press, 1967 Glendinning, Chellis Off the Map. Boston: Shambala, 1999

Mbah, Sam and I.E. Igariwey African Anarchism. Tuson: See Sharp Press, 1997

Mumford, Lewis The Pentagon of Power. New York: A Harvest Book, 1964

Mumford, Lewis Technics and Human Development. New York: A Harvest Book, 1966

Ngu’gi’ Detained. London: Heinemann, 1981

Watson, David Against the Megamachine. New York: Autonomedia, anti-copyright




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



Churchill, Ward From A Native Son. Boston: South End Press, 1996



Deloria, Vine Jr. and Clifford M. Lytle American Indians, American Justice. Austin: University of Texas Press, 1983



Deloria, Vine Jr. Spirit & Reason. Colorado: Fulcrum Publishing, 1999



Fanon, Frantz The Wretched of the Earth. New York: Grove Press, 1963



Fanon, Frantz A Dying Colonialism. New York: Grove Press, 1965



Fanon, Frantz Black Skin, White Masks. New York: Grove Press, 1967



Marger, Martin N. Race an Ethnic Relations 5th Edition. Belmont: Woadsworth, 2000



Memmi, Albert The Colonizer and the Colonized. Boston: Beacon Press, 1965



Said, Edward Culture and Imperialism. New York: Vintage Books, 1993



Said, Edward The Edward Said Reader. New York: Vintage Books, 2000



Anzaldu’a, Gloria Borderlands La Frontera. San Francisco: aunt lute books, 1987



Churchill, Ward ed. Marxism and Native Americans. Boston: South End Press, 1983



Churchill, Ward Since Predator Came. Littleton: Aigis Publications, 1995



Churchill, Ward Fantasies of the Master Race. San Francisco: City Lights Books, 1998



Fanon, Frantz Towards the African Revolution. New York: Grove Press, 1967 Glendinning, Chellis Off the Map. Boston: Shambala, 1999



Mbah, Sam and I.E. Igariwey African Anarchism. Tuson: See Sharp Press, 1997



Mumford, Lewis The Pentagon of Power. New York: A Harvest Book, 1964



Mumford, Lewis Technics and Human Development. New York: A Harvest Book, 1966



Ngu’gi’ Detained. London: Heinemann, 1981



Watson, David Against the Megamachine. New York: Autonomedia, anti-copyright



Those who discovered and conquered other lands were entitled to them, their riches, and their spoils. The conquered people could be treated as slaves, banished to other lands, or assimilated into the society and institutions of the conquering people — Vine Deloria, Jr. (1983)



“The crux of the matter rests, not merely in the resistance to the predatory nature of the present Eurocentric status quo, but in conceiving viable sociocultural alternatives (Churchill, 1996:31).”



The new relationships are not the result of one barbarism replacing another barbarism, of one crushing of man replacing another crushing of man. What we Algerians want is to discover the man behind the colonizer; this man who is both the organizer and the victim of a system that has chocked him and and reduced him to silence. As for us, we have long sense rehabilitated the Algerian colonized man. We have wrenched the Algerian man from a centuries-old and implacable oppression. We have risen to our feet and we are now moving forward. Who can settle us back in servitude? (Fanon, 1965:32)

