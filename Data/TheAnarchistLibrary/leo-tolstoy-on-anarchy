
The Anarchists are right in everything; in the negation of the existing order and in the assertion that, without Authority there could not be worse violence than that of Authority under existing conditions. They are mistaken only in thinking that anarchy can be instituted by a violent revolution. But it will be instituted only by there being more and more people who do not require the protection of governmental power and by there being more and more people who will be ashamed of applying this power.

“The capitalistic organization will pass into the hands of workers, and then there will be no more oppression of these workers, and no unequal distribution of earnings.” [Marxist]

“But who will establish the works; who will administer them?” [Anarchist]

“It will go on of its own accord; the workmen themselves will arrange everything.” [Marxist]

“But the capitalistic organization was established just because, for every practical affair, there is need for administrators furnished with power. If there be work, there will be leadership, administrators with power. And when there is power, there will be abuse of it — the very thing against which you are now striving.” [Anarchist]

* * *

To the question, how to be without a State, without courts, armies, and so on, an answer cannot be given, because the question is badly formulated. The problem is not how to arrange a State after the pattern of today, or after a new pattern. Neither I, nor any of us, is appointed to settle that question.

But, though voluntarily, yet inevitably must we answer the question, how shall I act faced with the problem which ever arises before me? Am I to submit my conscience to the acts taking place around me, am I to proclaim myself in agreement with the Government, which hangs erring men, sends soldiers to murder, demoralizes nations with opium and spirits, and so on, or am I to submit my actions to conscience, i.e., not participate in Government, the actions of which are contrary to reason?

What will be the outcome of this, what kind of a Government there will be — of all this I know nothing; not that I don’t wish to know; but that I cannot. I only know that nothing evil can result from my following the higher guidance of wisdom and love, or wise love, which is implanted in me, just as nothing evil comes of the bee following the instinct implanted in her, and flying out of the hive with the swarm, we should say, to ruin.[1] But, I repeat, I do not wish to and cannot judge about this.

In this precisely consists the power of Christ’s teaching and that not because Christ is God or a great man, but because His teaching is irrefutable. The merit of His teaching consists in the fact that it transferred the matter from the domain of eternal doubt and conjecture on to the ground of certainty. You are a man, a being rational and kind, and you know that today or tomorrow you will die, disappear. If there be a God then you will go to Him and He will ask of you an account of your actions, whether you have acted in accordance with His law, or, at least, with the higher qualities implanted in you. If there be no God, you regard reason and love as the highest qualities, and must submit to them your other inclinations, and not let them submit to your animal nature — to the cares about the commodities of life, to the fear of annoyance and material calamities.

The question is not, I repeat, which community will be the more secure, the better — the one which is defended by arms, cannons, gallows or the one that is not so safeguarded. But there is only one question for a man, and on it is impossible to evade: “Will you, a rational and good being, having for a moment appeared in this world, and at any moment liable to disappear — will you take part in the murder of erring men or men of a different race, will you participate in the extermination of whole nations of so-called savages, will you participate in the artificial deterioration of generations of men by means of opium and spirits for the sake of profit, will you participate in all these actions, or even be in agreement with those who permit them, or will you not?”

And there can be but one answer to this question for those to whom it has presented itself. As to what the outcome will be of it, I don’t know, because it is not given to me to know. But what should be done, I do unmistakably know. And if you ask: “What will happen?”, then I reply that good will certainly happen; because, acting in the way indicated by reason and love, I am acting in accordance with the highest law known to me. The situation of the majority of men, enlightened by true brotherly enlightenment, at present crushed by the deceit and cunning of usurpers, who are forcing them to ruin their own lives — this situation is terrible and appears hopeless.

Only two issues present themselves, and both are closed. One is to destroy violence by violence, by terrorism, dynamite bombs and daggers as our Nihilists and Anarchists have attempted to do, to destroy this conspiracy of Governments against nations, from without; the other is to come to an agreement with the Government, making concessions to it, participating in it, in order gradually to disentangle the net which is binding the people, and to set them free. Both these issues are closed. Dynamite and the dagger, as experience has already shown, only cause reaction, and destroy the most valuable power, the only one at our command, that of public opinion.

The other issue is closed, because Governments have already learnt how far they may allow the participation of men wishing to reform them. They admit only that which does not infringe, which is non-essential; and they are very sensitive concerning things harmful to them — sensitive because the matter concerns their own existence. They admit men who do not share their views, and who desire reform, not only in order to satisfy the demands of these men, but also in their own interest, in that of the Government. These men are dangerous to the Governments if they remain outside them and revolt against them — opposing to the Governments the only effective instrument the Governments possess — public opinion; they must therefore render these men harmless, attracting them by means of concessions, in order to render them innocuous (like cultivated microbes), and then make them serve the aims of the Governments, i.e., oppress and exploit the masses.

Both these issues being firmly closed and impregnable, what remains to be done?

To use violence is impossible; it would only cause reaction. To join the ranks of the Government is also impossible — one would only become its instrument. One course therefore remains — to fight the Government by means of thought, speech, actions, life, neither yielding to Government nor joining its ranks and thereby increasing its power.

This alone is needed, will certainly be successful.

And this is the will of God, the teaching of Christ. There can be only one permanent revolution — a moral one: the regeneration of the inner man.

How is this revolution to take place? Nobody knows how it will take place in humanity, but every man feels it clearly in himself. And yet in our world everybody thinks of changing humanity, and nobody thinks of changing himself.

Leo Tolstoy
1900

 
[1] Tolstoy’s point seems to be that death is inevitable and that we should not fear it




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“The capitalistic organization will pass into the hands of workers, and then there will be no more oppression of these workers, and no unequal distribution of earnings.” [Marxist]


“But who will establish the works; who will administer them?” [Anarchist]


“It will go on of its own accord; the workmen themselves will arrange everything.” [Marxist]


“But the capitalistic organization was established just because, for every practical affair, there is need for administrators furnished with power. If there be work, there will be leadership, administrators with power. And when there is power, there will be abuse of it — the very thing against which you are now striving.” [Anarchist]



Leo Tolstoy
1900

