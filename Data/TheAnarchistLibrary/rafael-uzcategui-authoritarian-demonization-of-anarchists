      Cuba 1961      The Isolated anarchists      Half a century later, the farce
The possibilities anarchism points toward have many enemies. Its fiercest opponents, however, are those authoritarian regimes which, distorting and subverting the ideas of socialism, have promoted themselves historically as embodying the values of liberty, equality and fraternity. In every situation, state capitalist governments, applying religious categories to secular contexts, have over-simplified conflicts by defining them as being between “the faithful”--those who support them, and “heretics”--those who oppose them. In this alleged confrontation between the two positions (in religious terms the struggle of “good” against “evil”), the authoritarians need to claim that all the world’s revolutionaries are with them confronting the forces that oppose them--even though this isn’t certain.

As we know, ever since the Russian Revolution of 1917, nationalists and authoritarian leftists who have come to power have developed various strategies to dismantle, co-opt, and fragment whatever revolutionary organizations remain. They do this precisely because such groups constitute opposition to their plans from the left, because they denounce the contradictions and abuses of the ongoing process of the centralization of power. In the case of anarchism--an attitude and way of thinking which is highly resistant to bureaucratic reorganization of power and authority--tragic examples abound. In Russia (and later the Soviet Union), China, Spain, and dozens of other examples, statists have systematically persecuted and exterminated anarchists. The killing, however, has been accompanied by lies, misrepresentation, and the creation of nonexistent support to confuse and paralyze the international anarchist movement.

Those who know the history of anarchism in Latin America know that Cuba developed, along with countries like Argentina, Chile, Peru, and Uruguay, an anarcho-syndicalist movement that played an important role in the labor conflicts of their times. As is well documented in the book El anarquismo en Cuba by Frank Fernández, [1] anarchists on the island were prominent in several unions, published newspapers and magazines for discussion, propaganda, and agitation, as well as providing social centers for people to meet and interact.

The Cuban anarchists, as interested people can confirm, joined the popular struggle against the dictator Fulgencio Batista and, his ouster in 1959 aroused in them the same positive expectations about the future of the island as in the rest of society. As Fernández relates, the anarchist publications of the day, Solidaridad Gastronomica and El Libertario, expressed a favorable and hopeful attitude regarding the new government, while not trusting it unthinkingly. [2] But, in late 1959, any criticism of the government, no matter its source, began to be labeled “counterrevolutionary” in the new language of power. At the same time, the Castro clique began inviting representatives from all the revolutionary tendencies of the world to the island in order to convince them of the regime’s goodness.

Among those invited was the German anarchist Augustin Souchy, who visited Havana in the summer of 1960 to learn about the experiments with land reform. His inquiry resulted in a lengthy article, printed in an official publication, reporting on what he had seen during his visit. Souchy also wrote a pamphlet entitled Testimonies on the Cuban Revolution [3], which was published without going through official censorship, and had a tone different from what the regime had hoped for. In this pamphlet Souchy warned of the authoritarian turn the new administration was taking. Soon after he left Cuba, the entire print run of the pamphlet was seized and destroyed by the government, following a recommendation by the Cuban Communist Party (PCC). But the pamphlet was made available outside the country thanks to an edition published by Reconstruir of Buenos Aires. The anarchists could not be easily convinced by the Castro government’s propaganda. A new strategy was needed.

Many Cuban anarchists of the time belonged to the Asociación Libertaria de Cuba (ALC). In 1961 its Secretary of Relations, Manuel Gaona Sousa, was in charge of maintaining contacts with the international anarchist movement. However, from early on, Gaona was enthusiastic about both the July 26 Movement (M26J) and Fidel Castro. Gaona’s prestige and record of participation within the anarchist movement, his key role in communications with the outside world, and his desire to cooperate with a government that he supported were used to maximum effect by the Cuban authorities. Gaona wrote a manifesto, “A clarification and a statement of the Cuban anarchists” [4] which asserted that “nearly all anarchist activities are now integrated in the various agencies of the Cuban Revolution.” It also denied that anarchists were being imprisoned for their activism. Both assertions were contradicted repeatedly by anarchist publications on the island.

Gaona’s manifesto, which was sent to all anarchist publications of the time, contained five key ideas: The first that there were no anarchists arrested for their convictions; second, that there was no political or religious persecution in Cuba; third, that anarchists supported Castro’s government; fourth, that Castro’s government represented the ideals for which the anarchists fought; and the fifth part was a crude and literal copy of the government’s propaganda about the purported political and economic benefits delivered by the Castro regime. Finally, the document stated: “We want to alert fellow anarchist Movements in Mexico, Latin America, and the world, and fellow Spanish-speaking exiles in America, so that they won’t be taken unawares by the malicious and deceitful information sent out by people who serve, consciously or unconsciously, the Cuban counterrevolution.” Although the manifesto claimed to express the position of Cuban anarchism, it was signed by just 25 individuals, and it later became known that some signatures were collected by Gaona through deception. Many anarchists who he asked refused to sign a text that they regarded as renouncing the basic principles of anarchism. Among them was the well-known comrade Marcelo Salinas y Lopez; they were persecuted and sooner or later forced into exile.

Gaona’s manifesto brought about several dire consequences for the anarchist movement of the island. From the point of view of the Cuban authorities, it divided the anarchists into “good,” the small group that supported Gaona’s position, and “bad,” the rest. It also sowed confusion in anarchist organizations outside Cuba, especially in Latin America.

At this time there was also a U.S. government offensive against Cuba. And in this context, on the left there was a lot of admiration for the bearded July 26 Movement (M26J) as a model for guerrilla insurgencies in Latin America. On the other hand, there was poor communication with the anarchist activists on the island. Under the circumstances, the manifesto literally paralyzed anarchist criticisms and questioning of the new regime. In fact, the isolating of the Cuban anarchists promoted their persecution and extermination. To give a few examples: Augusto Sánchez was imprisoned and murdered; Rolando Tamargo and Ventura Suárez were shot; Sebastian Aguilar Jr. was shot; Eusebio Otero was found dead in his room; Raúl Negrín was burned alive. Casto Moscú, Modesto Piñeiro, Floreal Barrera, Suria Linsuaín, Manuel González, José Aceña, Isidro Moscú, Norberto Torres, Sicinio Torres, José Mandado Marcos, Plácido Méndez and Luis Linsuaín were arrested and sentenced to prison. Some comrades could not stand the torture in prison, such as: Francisco Aguirre, who died in his cell; Victoriano Hernández, sickened and blinded by the abuse, committed suicide; and José Álvarez Micheltorena, died a few weeks after his release.

Manuel Gaona was actively involved in promoting the persecution of his former comrades. Although the accusations against the genuine anarchists employed the typical Stalinist epithets--such as labeling them “CIA agents” among other things--they proved to be effective. According to Fernández, “The confusion in the international anarchist camp regarding the Cuban situation was promoted by the Cuban government’s propaganda machine, which had enormous resources, talent, imagination, and great political skill.” Even exiled Cuban anarchist groups, like the Cuban Libertarian Movement in Exile (MLCE), [5] were accused by other anarchists and anti-authoritarians of being “counterrevolutionaries”. For example, Daniel Cohn-Bendit, at the International Anarchist Congress of Carrara in 1968 accused the MLCE of “being funded by the CIA.” The abandonment of Cuban anarchists by their peers is one of the worst mistakes in the history of the anarchist movement. It was not until 1978, with the publication of The Cuban Revolution: A critical perspective by Sam Dolgoff, [6] that the world’s anarchists began to understand what really had happened on the island. But it was too late.

Fifty years after the Gaona manifesto, there are attempts to use the same strategy again. At a time when various self-described leftist and progressive governments have come to power in Latin America, the new bureaucracies are trying to spread the idea that all revolutionaries, including anarchists, are on their side. Some converts, inventing phantom organizations and initiatives, spread the idea through the Internet that the “true anarchists” support the governments of Rafael Correa, Evo Morales, Cristina Kirchner, and Hugo Chávez, among others, and that those who criticize them are “false anarchists”, and are “far from the popular struggles.” One of the most extravagant attempts has been made by a freewheeling “Revolutionary Anarchist Federation of Venezuela”, which in its first statement expresses support for the Bolivarian government of Hugo Chavez and affirms the need to join in its electoral coalition, the Gran Polo Patriótico, contending in the upcoming presidential elections. However, there is a big difference between now and Manuel Gaona’s times. Information technologies nowadays make it almost impossible for people to lack information in the way that allowed ignorance of the real nature of Fidel Castro’s government in the past. Anyone interested and concerned can now research and find out the different opinions and initiatives in the popular and revolutionary milieus which expose the contradictions of these governments and their increasing involvement with today’s globalized capitalism. They can learn about these regimes’ criminalization of those who are involved in social struggles, and the protection of the new bourgeoisie through state capitalism. History repeats itself--the first time it is tragedy, the second time it is farce.

Originally published in Spanish in Tierra y Libertad. Translated into English by Charlatan Stew and friends, June, 2014.

There is an earlier English translation by Christie Books, titled Authoritarian Chimeras, Cuba, and the Gaona Manifesto, posted on the Christie Books website (February 5, 2012): http://www.christiebooks.com/ChristieBooksWP/2012/02/authoritarian-chimeras-cuba-and-the-gaona-manifesto-by-rafael-uzcategui/

Tierra y Libertad original: http://www.nodo50.org/tierraylibertad

Frank Fernández notes that Gaona’s manifesto was dated and signed in Marianao on November 24, 1961, and clearly denounced the Cuban anarchists who didn’t share his enthusiasm for the Castro regime.
[1] Published in English as Cuban Anarchism The history of a movement. San Francisco: See Sharp, 2001. Available online at http://theanarchistlibrary.org/library/frank-fernández-cuban-anarchism-the-history-of-a-movement

See especially Chapter 4: Castroism and Confrontation (1959–1961), and Chapter 5: Exile and Shadows (1961–2001)
[2] In Cuban Anarchism: the History of a Movement, Chapter 4, Fernández relates how the anarchists in Cuba decided to issue a Declaración de Principios (Declaration of Principles), in the summer of 1960, accusing the Castro regime of strengthening government centralization, and moving toward a Marxist dictatorship. The eight points of the Declaración also outlined the ways in which their anarchist perspective differed from the policies of the regime: “1) it defined, in accord with libertarian ideas, the functions of unions and federations in regard to their true economic roles; 2) it declared that the land should belong “to those who work it”; 3) it backed “cooperative and collective work” in contrast to the agricultural centralism of the government’s Agrarian Reform law; 4) it called for the free and collective education of children; 5) it inveighed against “noxious” nationalism, militarism, and imperialism, opposing fully the militarization of the people; 6) it attacked “bureaucratic centralism” and weighed forth in favor of federalism; 7) it proposed individual liberty as a means of obtaining collective liberty; and 8) it declared that the Cuban Revolution was, like the sea, “for everyone,” and energetically denounced “the authoritarian tendencies that surge in the breast of the revolution.” http://www.illegalvoices.org/apoc/books/cuban/front.html
[3] Augustin Souchy, Testimonios sobre la revolución cubana. Buenos Aires: Editorial Reconstruir, 1960. Available online at http://issuu.com/ellibertario/docs/testimonios_souchy2
[4] For a discussion of the use of the terms “libertarian” and “anarchist” interchangeably, especially in places outside North America, see: An Anarchist FAQ (02/17), The Anarchist FAQ Editorial Collective, Published June 18, 2009. Version 13.1 http://www.theanarchistlibrary.org/print/The_Anarchist_FAQ_Editorial_Collective__An_Anarchist_FAQ__02_17_.html

See Section A.1.3, Why is anarchism also called libertarian socialism?: “Anarchists have been using the term ‘libertarian’ to describe themselves and their ideas since the 1850’s. According to anarchist historian Max Nettlau, the revolutionary anarchist Joseph Dejacque published Le Libertaire, Journal du Mouvement Social in New York between 1858 and 1861 while the use of the term ‘libertarian communism’ dates from November, 1880 when a French anarchist congress adopted it. [Max Nettlau, A Short History of Anarchism, p. 75 and p. 145] The use of the term ‘Libertarian’ by anarchists became more popular from the 1890s onward after it was used in France in an attempt to get round anti-anarchist laws and to avoid the negative associations of the word ‘anarchy’ in the popular mind (Sebastien Faure and Louise Michel published the paper Le Libertaire--The Libertarian--in France in 1895, for example). Since then, particularly outside America, it has always been associated with anarchist ideas and movements. Taking a more recent example, in the USA, anarchists organised ‘The Libertarian League’ in July 1954, which had staunch anarcho-syndicalist principles and lasted until 1965. The US-based ‘Libertarian’ Party, on the other hand has only existed since the early 1970’s, well over 100 years after anarchists first used the term to describe their political ideas (and 90 years after the expression ‘libertarian communism’ was first adopted).”
[5] “Una aclaración y una declaración de los libertarios cubanos”, available online at http://issuu.com/ellibertario/docs/manifiestogaona
[6] Montreal: Black Rose Books (1976). Available online at http://libcom.org/history/cuban-revolution-critical-perspective-sam-dolgoff




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

