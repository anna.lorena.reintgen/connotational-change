
So you want me to tell you what Anarchism is, do you? I can do no less than make the attempt, and in my own simple way try to make you understand at least that it is not what the uninformed and the capitalistic newspapers, liars, fools and villains generally say it is.

In the first place, let me urge upon all who desire to learn the truth about Anarchism not to go to its enemies for information, but to talk with Anarchists and read anarchistic literature. And it is not always safe to take what one, two or even a dozen persons may say about it, either, though they call themselves Anarchists. Take what a goodly number of them say and then cancel those statements in which they are not in accord. What remains in all probability is true. For example, what is Christianity? Ask a dozen or more people and it is likely their answers will not agree in every particular. They may, however, agree upon some fundamental propositions. This more likely to be the correct position of Christianity than the statements made by any one of them. This process of cancellation is the best way of finding out what any philosophy is. This I have done in determining what Anarchism is, and it is a fair presumption that I have arrived tolerably near the truth.

Anarchism, in the language of Benjamin R. Tucker, may be described as the doctrine that “all the affairs of men should be managed by individuals or voluntary associations, and that the state should be abolished.”

The state is “the embodiment of the principle of invasion in an individual, or a band of individuals, assuming to act as representatives or masters of the entire people within a given area.”

Government is “the subjection of the noninvasive individual to an external will.”

Now, keep these definitions in mind, and don’t use the word “state” or “government” or “Anarchy” in any other sense than that in which the Anarchist himself uses it. Mr. Tucker’s definitions are generally accepted by Anarchists everywhere.

The state, according to Herbert Spencer and others, originated in war, aggressive war, violence, and has always been maintained by violence. The function of the state has always been to govern — to make the non-ruling classes do what the ruling classes want done. The state is the king in a monarchy, the king and parliament in a limited monarchy, elected representatives in such a republic as exists in the United States, and the majority of the voters in a democracy as in Switzerland. History shows that the masses are always improved in mental, moral, and material conditions as the powers of the state over the individuals are reduced. As man becomes more enlightened regarding his interests, individual and collective, he insists that forcible authority over him and his conduct shall be abolished. He points to the fact that the church has improved in its material affairs, to say nothing of the spiritual, since the individual is not compelled to support it and accept its doctrines or be declared a heretic and burned at the stake or otherwise maltreated; to the fact that people are better dressed since the state has annulled the laws regulating dress; to the fact that people are happier married since each person can choose his own mate; to the fact that people are better in every way since the laws were abolished regulating the individual’s hair-cut, his traveling, his trade, the number of window panes in his house, chewing tobacco or kissing on Sundays, and so on without number. In Russia and some other countries even now you would not be allowed to go into the country or come out of it without legal permission, to print or read books or papers except those permitted by law, to keep anyone in your house over night without notifying the police, and in a thousand ways the individual is hampered in his movements. Even in the freest countries the individual is robbed by the tax-collector, is beaten by the police, is fined and jailed by courts — is browbeated by the authority in many ways when his conduct is not aggressive or in violation of equal freedom.

It is a mistake often made, even by some Anarchists, to say that Anarchism aims to establish absolute freedom. Anarchism is a practical philosophy, and is not striving to do the impossible. What Anarchism aims to do, however, is to make equal freedom applicable to every human creature. The majority under this rule has no more rights than the minority, the millions no greater rights than one. It assumes that every human being should have equal rights to all the products of nature without money and without price; that what one produces would belong to himself, and that not individual or collection of persons, be they outlaw or state, should take any portion of it without his knowledge or consent; that every person should be allowed to exchange his own products wherever he wills; that he should be allowed to co-operate with his fellows if he chooses, or to compete against them in whatever field he elects; that no restrictions whatsoever should be put upon him in what he prints or reads or drinks or eats or does, so long as he does not invade the equal rights of his fellows.

It is often remarked that Anarchism is an impractical theory imported into the United States by a lot of ignorant foreigners. Of course, those who make this statement are as much mistaken as though they made it while conscious of its falsity. The doctrine of personal freedom is an American doctrine, in so far as the attempt to put it into practice is concerned, as Paine, Franklin, Jefferson and others understood it quite well. Even the Puritans had a faint idea of it, as they came here to exercise the right of private judgement in religious matters. The right to exercise private judgement in religion is Anarchy in religion. The first to formulate the doctrine of individual sovereignty was a blue-bellied Yankee, as Josiah Warren was a descendant of the Revolutionary General Warren. We have Anarchy in trade between the states in this country, as free trade is simply commercial Anarchy.

No one who commits crime can be an Anarchist, because crime is the doing of injury to another by aggression — the opposite of Anarchism.

No one can kill another, except in self-defense, and be an Anarchist, because that would be invading another’s equal right to live — the antithesis of Anarchism.

Hence assassins and criminals generally are called Anarchists only by the ignorant and malicious.

You can’t be an Anarchist and do the things which Anarchism condemns.

Anarchism would make occupancy and use the sole title to land, thereby abolishing rent for land.

It would guarantee to each individual or association the right to issue money as a medium of exchange, thereby abolishing interest on money in so far as co-operation and competition can do it.

It denies the justice of patent and copyrights, and would abolish monopoly by abolishing patent rights.

It denies the right of any body of people to tax the individual for anything he does not want, but that taxation should be voluntary, such as is now done by churches, trade unions, insurance societies and all other voluntary associations.

It believes that freedom in every walk of life is the greatest possible means of elevating the human race to happier conditions.

It is said that Anarchism is not socialism. This is a mistake. Anarchism is voluntary Socialism. There are two kinds of Socialism, archistic and anarchistic, authoritarian and libertarian, state and free. Indeed, every proposition for social betterment is either to increase or decrease the powers of external wills and forces over the individual. As they increase they are archistic; as they decrease they are anarchistic.

Anarchy is a synonym for liberty, freedom, independence, free play, self-government, non-interference, mind your own business and let your neighbor’s alone, laissez faire, ungoverned, autonomy, and so on.

Now that I am done, I find that you have been given only a faint outline of what Anarchism is and is not. Those who desire to pursue the subject further will find food for intellectual adults in Tucker’s Instead of a Book; Proudhon’s What is Property? and Economical Contradictions; Tandy’s Voluntary Socialism; Mackay’s The Anarchists; Auberon Herbert’s Free Life; The Demonstrator; Lucifer, and a lot of other books, papers and pamphlets which may be had by addressing Henry Bool, Ithaca, NY, E.C. Walker, 244 West 143rd Street, NYC, “Liberty,” Box 1312, New York, or “Mother Earth,” P.O. Box 217, Madison Square Station, New York city.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

