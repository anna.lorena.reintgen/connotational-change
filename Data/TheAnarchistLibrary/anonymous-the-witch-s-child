
Doubts blew in with the clouds over the grassy hill and down by the willow tree where the child played in the afternoons. The witching hour had come, and the child was still awake, following those doubts past the rows of empty houses to the corner, where the bus comes in the mornings to take them all away.

A shadow came in the window and settled over the bed. The winds outside calmed, but a little breeze in the room itself seemed to tousle the child’s hair, and caress its cheeks. When the child fell asleep, the shadow bent over its ear and whispered a bedtime story:

Child, beneath this golden roof, you, of all people, know what it means to be homeless. It is the tragedy of some in this world to be uprooted, of others, to be rootless. This is the story of the rootless ones. Your bones already know the story, though your mind does not yet understand it. One day, in the waking world, this story will come back to you.

Some say it began with the Romans, with their new geometries of warfare, their civilization and slavery. But the truth is, we only have ourselves to blame. Already before the Southerners came, we had lost the first battle. We chose the War, and have been living in it ever since.

It was a small mistake, but it was we who opened the gate to our enemy. We can still undo this mistake, but the hour grows late.

Our Mistake was this: we stopped celebrating the Spring. We let the silence of Winter extend over the whole of the year. The time of the year for turning inwards became our entire lives. We turned away from one another, and became bored with ourselves. We cared less about what happened in the world outside. So we turned the sensitives into priests, and asked them to bring the mysteries to us, rather than searching for them. We turned our war leaders into leaders of every day: of course all of our days should become a quiet war! We began to fear our own adventures, and asked the leaders to entertain us with their trite wars.

We fell into routines. Life was no longer becoming, but simply being. The women turned to their fertility cults, replacing an unending web of mysteries with fascination over the one that still fit into their lives: when the one becomes two. The men, jealous of their exclusion from the possibility of creation and foolish to think they ever could be excluded from creation, turned to their destructive sports and wars. The war against women began as just another game, another competitive raid.

Those little wars became our pasttime, and the years went by as though we were trapped in a little house, counting the days until the equinox. Waiting for a Spring we never welcomed. But Spring has to be welcomed, or it never comes. For the spectator, Winter never ends. Spring is an insurrection. A hammer that cracks the walls of seed pods, a bud that breaks the sheets of ice. An erotic meeting that shatters the contemplation of Winter and upsets all the old orders by the untamed collision and growth of bodies.

This is why on May Day, we celebrate. A people who welcome Spring will never know of kings.

When the Romans came they trumped all our little games. Our raids and fertility tricks were child’s play next to their polity and economy. Their contemplations were more sophisticated than ours. Their silence much louder than ours. Their Winters did not even come with cold.

We saw that they had perfected the game of conquest, and we envied them. In our own eyes, we became the barbarian at the gate. Hermann, who smashed the legions on the forest road, had studied under them. We were not the slave who rebelled, but the young Prince. In the end we were the ones who sacked Rome, but only because we thought to outdo it.

When the Christians came, some among us adopted their God, because He was a God of conquest, a jealous God, created in our image. A God better suited to this war. When they began to destroy the sacred places, to burn the books, to torch the holy groves, there were finally those who understood our Mistake. We rose up. We joined those fighting against the legions and the slavers, those princes fighting to become kings, and we marched on Rome.

But we misunderstood who we were. We thought that without the Romans, their geometries would disappear as well. But those we followed into war carried them in their breasts. We thought that they were our people because they spoke the same language, but in secret they were speaking the language of conquest. We thought that they were our people because they celebrated the same gods, but in secret they bowed down to the God of jealousy. We thought that we had destroyed Rome, but in fact we had spread it everywhere.

This is why on May Day, we mock. Most of all, we mock ourselves, for this mistake that still haunts us.

Since that day, our task has been to learn who we are.

Because it was not our fight that we joined. The war leaders and their closest followers were still playing their games, but we did not see this because they had long since stopped laughing. They were like the farmer scrutinizing his root cellar, counting the days until the last frost, weighing the diminishing supplies, eating the maths in his head. In their permanent contemplation, they saw that the Romans were becoming weak, and the time was coming to attack them, and take over their game.

Rome could not be made to work. A succession of war leaders, pretending to sack it, tried studiously to put its geometries back to work, but there was always another war leader eager to try his hand, and the whole thing fell apart.

For a thousand years, an uneasy truce prevailed. The slavery of the Romans was ended, but a venomous compromise snuck into its place. The war leaders who led us to victory returned half of the greatness wrested from the Romans, parcelling it out among us. They kept the other half for themselves, and passed from being leaders to lords. On the estates of the Romans, the servants were told they were free. The land was no longer their prison, but a trust between themselves and the new lords. They could live as they chose, as long as they tithed a part of the harvest to feed their liberators and the armies that protected them. A compromise between lord and peasant. A new equation, for a thing that was being called freedom.

Caesar was not dead. There were a thousand Caesars. And the Church kept peace among them.

The priests came among us. They taught us to hate our bodies. They taught us to fear the forest, the mountains, the black night. They monopolized magic and wed it to ceremony.

Many free communities remained, and stayed true to the old ways, but one by one they were conquered, chased out, razed to the ground. The princes offered us protection, but they were the ones bringing war. No one could stand up to them, but by allying with another of them. The princes were brought into the Church first. In order to make us believe we were the same, and at the same time to make themselves seem godly, they brought the religion down on their serfs.

There were heretics, but thousands of us were thrown on the fire. Most of all our memories were burned. The voice was replaced with paper, and a greater silence came to reign. Any stories that were not in their one Book were banished. Memories of magic, of healing, of speaking with the forest, of our origins, memories of the time when we shared everything and nothing was owned, were suppressed.

This is how they destroyed our roots. And this is why, on May Day, we tell stories. Stories of our lives, of our struggles, of the future we want, of a past we invent because we no longer remember it.

One year, their whole game was almost wrecked by a simple force of nature. A disease swept through the crowded cities and hungry countrysides, and few had the strength to resist it. Every third person perished. The princes and the priests were most troubled by their loss of subjects. Bodies in their service became the most prized wealth, and they suddenly discovered that they were not in charge of the production of bodies. The mothers and midwives were. What if these should decide to sabotage the growth of the nation for their own, personal reasons? There were even some who declared they would not bring children into this world to live as servants.

Contraception and abortion became the worst of crimes and sins. Love was only allowed if it bore children. The Christians had always hated women who loved women, and men who loved men, but now they turned ever more from preaching their hatred to enforcing it at the gallows.

Midwives were suppressed, wherever the princes could afford it. The mistrusted choices of the mother were overruled with the loyalty of the professionals. The worst heresy was that people could learn from their own bodies. The only learning to be trusted in the future was the education in schools and the new universities. And all the professions they produced were self-regulating conspiracies. One could only practice by joining the profession, and one could only join the profession by passing through the training administered in the universities, and one could only pass the training by adopting the goals of the princes who funded those universities.

In the interests of keeping subjects alive, midwives were kept on as nurses, because the new doctors were inadequate on their own. But they excelled at governing the bodies under their charge with an iron discipline. The world before these doctors was mute and witless. In their minds, sickness was not an attempt to communicate, and bodies could not be trusted to heal themselves. Disease was something to be located, named, and excised. In time, even the experience of pleasure would be classified as a sickness.

This is why we celebrate May Day with orgies. To learn from our own bodies. To show that pleasure can be shared with whomever we choose. To respect another’s desires and take joy in their satisfaction.

There were many revolts, many reversals, but over the centuries the princes became kings, and their domain expanded. Our yoke became heavier, and we were expected to pull more and more wealth from the land to give to our lords. And these lords denied there was ever a time when they were our brothers-in-arms. They were separated by blood, related to God, unlike anything else on the forsaken earth. It was forgotten that being a serf was once thought of as infinitely better than being a slave. There was less and less difference.

Still the lords needed to squeeze more blood out of the earth. They turned towards faraway lands, and they called the people they met “slaves.” But this was a crueler slavery than anything the Romans had ever inflicted. If their God despised the human body, He hated the slaves’ very souls.

They needed our help in these new wars of conquest, and above all, they needed to prevent our defection. So they told us we were white, which was immutably different from being black, or being a savage. The lords and their priests, cops, and explorers could not build new cages fast enough, so they built categories, and taught us that we were born into them, and could never choose who we were. And who we were was an army, mobilized to assault all those who still had roots in the world.

This is why we celebrate May Day with visions. To see that magic is everywhere, and all life is mutable, all categories inadequate.

For in those years we fought many wars against them. We burned lords and priests, we ran off with savages, we threw captains overboard. And they responded by intensifying their war against us. They burnt millions for using sacred plants, for healing, for speaking with the forest, for communing with the old gods, for refusing to be white, for disrespecting their new laws that said land was not a trust but property, inhering to individuals, and only to men. And they slaughtered many millions more of the rooted ones, to take their lands, or punish them if they refused to be uprooted.

And then they moved us all about, wherever they wanted us, rootless and uprooted, mixed together, tracked by our categories, until the very earth became strange to us, and we to ourselves. They put us to work. They no longer asked for a portion, but for everything, for our very time. It was not enough to partition the land. They also had to partition our lives into hours, and assign each one a price. They learned to kill us in how they kept us alive. They taught us to view life as a series of numbers, to convert joy to value. The forest became lumber. Our hands became labor. They ruled us with calculations determining the cost of our lives, the price they needed to pay to keep us working. Eventually they tricked us to view life in the same terms.

This is why we celebrate May Day with feasts. Because scarcity is a phantom that must be banished. Because the only things that matter cannot be counted. Because despite all that we have endured, we love ourselves and we will not be instruments for the ambitions of others.

On several occasions we still rose up. We captured kings and threw them from their towers, bombed their carriages, or cut their heads off. But the war still carried on. Over time the kings fractured and multiplied into a whole array of technicians. They made us accessories to production. They turned our bodies into machines. The factories were the new model for humankind, the new treasure of our rulers, and they despised us for the fact that their precious machines needed us. The old equation changed. Freedom no longer meant a compromise between master and subject. It meant all power to the machines, and the greatest mobility for their product. Our lives were sacrificed for the machines to keep running.

This is why we celebrate May Day with sabotage. For we will not surrender the rhythm of life to the timing of gears.

All their new techniques of warfare could not quench our rage. Even in the factories, or in the private places where they tried to confine the women, we formed new communes. Major upheavals shook the halls of the well masked princes, and they began to call these upheavals revolutions. They said the old forms of authority were finished. They said we were all free, and could participate in their project as equals. And most of us were fooled. Just like the barbarians before us, we reacted more to our exclusion than to our domination, and tried to become the new Romans.

But our new freedom was worse than the slavery that had come before it. We spent our days chained to assembly lines, not by links of iron or steel, but by our own vulnerability. Our lives were dedicated to the production of objects, and we became objects as well. Our masters no longer worried about keeping us alive because if we fell from hunger or disease or were consumed by the machines themselves, ten more would step forward to take our places, desperate to sell themselves so they could buy back the little they needed to survive. The world was stripped bare and filled again with objects, and we produced all of them, but they no more belonged to us than we ourselves did. We were fed to the roaring machines so they could continue without stop, filling an empty, hungry world with more emptiness and more hunger.

This is why on May Day we go on strike. To listen to the silence as we bring all their machinery to a halt.

So they negotiated with us and gave us some privileges, gave us some fancy clothes so we could pretend to be like them, and they let us decorate our lives with their abundance of objects. But more and more are beginning to realize that this project we’re invited to participate in is the war against all of us. It allows us anything but mutiny. It keeps us alive as long as we do not nourish ourselves. It demands only our complicity in this constant uprooting, and the suppression of those who still remember their roots.

They put our freedom down on paper, the better to silence it.

This is why we celebrate May Day with riots. To make a noise that will not go away. To burn all that is not true. To rip up the paving stones and discover, beneath them, the earth. To begin to grow roots again.

This is your story, child. This is why it seems you have everything, but you feel you have nothing. Trust your feelings. Do not numb them with the pills they offer you. Because those feelings of anguish and rage are the same itch the seed feels in the last days of Winter, before it bursts open and sends out its buds into the world. It is this growth—uncontrolled, spontaneous—that would deprive them of their soldiers, which is why they fear it above all else.

Not everyone arrived in this desert along the same path. But there are many who share your story. There are others who still remember their roots, and know where to find them. But those like you do not even know what is missing. Remember this story, and there will be hope for Spring.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

