
Christendom is an effort of the human race to go back to walking on all fours, to get rid of Christianity, to do it knavishly under the pretext that this is Christianity, claiming that this is Christianity perfected.  In the Christianity of Christendom the cross has become something like a child’s hobby horse and trumpet.(Kierkegaard, 1968, p260)

According to orthodox opinion, Christianity is synonymous with order, authority and state power. Even the most casual glance at the history of the Church reveals a reliable and systematic pattern of political subservience; Imperialist in Rome, Monarchist in Renaissance Europe, Stalinist in Russia, and “Democratic” in America. Clearly, Christianity not only supports authorities, but presupposes that authorities exist. For Calvin, even the most brutal tyrant is better than the absence of civil authority, and Luther’s own endorsement of the bloody suppression of the peasant rebellion is well known. It would appear any reconciliation between Christianity and an-arche: the absence of authority and command, is out of the question. Expounding his own brand of Christian anarchy Leo Tolstoy complained of “a tacit but steadfast conspiracy of silence about all such efforts.” (Tolstoy,1984,p8) Nevertheless, from the very foundation of Christianity there has been an undercurrent of opposition to both secular and Church authority, much more than just incidental protests against given power abuse, but essentially anarchistic in character. So it is my purpose here to demonstrate the radical incompatibility between the ethics of state power and the ethics of the gospel. This is not an attempt to construct a sociology of Christianity, but simply an effort to isolate and analyse its socio-political dimensions and show that anarchism is the only “anti-political political position” in harmony with Christian thought. This will require scrutiny of both biblical doctrine, as well as some of the various anarchistic Christian movements throughout history. First of all, a brief definition of anarchism is necessary.

Most basically, anarchism is the extreme scepticism of all forms of social hierarchy and entrenched and coercive institutional authority. Emma Goldman, in her essay “Anarchism” defines it as “the theory that all forms of government rest on violence, and are therefore not only wrong and harmful, but also unnecessary.” (1973,p12) Anarchy is broader than this however.  It views the nature of power as essentially malignant, and is in opposition to all coercive forms of cultural, economic, social and political authority, i.e. those forms of authority that which maintain obedience through violence or the threat of negative sanctions. Thus, as Rudolph Rocker elegantly puts it, “Power operates only destructively, bent always on forcing every manifestation of life into the straightjacket of its laws. Its intellectual expression is dead dogma, its physical form brute force. And this unintelligence of its objectives sets its stamp on its supporters also and renders them stupid and brutal, even when they were originally endowed with the best of talents. One who is constantly striving to force everything into a mechanical order at last becomes a machine himself and loses all human feeling.” (Pennock&Chapman,1978,p5)

So anarchy maintains that the abolition of social hierarchy is essential in establishing a society based on equality and individual liberty. While theories on how this society might be structured are complex and diverse, the most common element uniting them is the replacement of state authoritarianism with some form of non-governmental cooperation between free individuals. Usually, this takes the form of self-governing, decentralised, directly democratic community based assemblies and their confederations. These flax-roots community bodies would function on the principles of self-help, mutual-aid and voluntary cooperation, and would be linked cooperatively through federation to other autonomous communities from the local, to the bioregional, to the global level. Naturally, socialisation of the major means of production and economic self-management are primary. The aim is to replace the pyramidal hierarchy of the modern state with an organic sphere, the true diffusion of power. These are the fundamental principles of anarchist thought, which will be central to this discussion.

So, now we will consider the other side of the coin, beginning with a (very) limited examination of biblical data in both the New and Old Testaments. In Samuel 1 we see Israel’s social structure as traditionally anarchistic.  After the liberation from Egypt there were no clan princes, and families that might have been considered aristocratic were either destroyed or vanquished.  The God of Israel declared he alone would be the head of Israel, yet this was not a theocracy, for God had no representatives on earth and clan assemblies deliberated on community decisions. In periods of crisis God would appoint a “judge” to a position of leadership, but these individuals had no permanent authority, and after they had played out their role they were said to efface themselves and rejoin the people. Against the will of God, the Israelites decide on a monarchy, a king for the sake of efficiency, and to be conventional with dominant civilisations. God accepts their demand, but gives them a warning in the form of a particularly accurate assessment of the nature of political power:

This is what the king who reigns over you will do: He will take your sons and make them serve with his chariots and horses, and they will run in front of his chariots. Some he will assign to be commanders of thousands and commanders of fifties, and others will plough his grounds and reap his harvests, and still others will make weapons of war and equipment for his chariots. He will take your daughters to be perfumers and cooks and bakers. He will take the best of your fields and vineyards. He will take a tenth of your grain and of your vintage and give it to his officials and attendants. He will take a tenth of your flocks and you yourselves will become his slaves. (1 Samuel 8:8)

As Jacques Ellul writes, this passage boils down to 3 messages: “(1) political power rests on distrust and rejection of God; (2) political power is always dictatorial, excessive and unjust; (3) political power in Israel is established through conformity, in imitation to what is done in neighbouring kingdoms.”(1991,p46) To this I would add that it also states the existence of social hierarchy is inseparable from exploitation and stratification.

This is certainly a repudiation of the legitimacy of political power, one which is regular throughout the bible. Vernard Eller points to a systematic representation of monarchy in the Old Testament. Efficient kings, i.e. those that exercised political power normally by enriching their people, making conquests, consolidating rule etc are consistently represented as idolatrous, unjust tyrannical murderers. In contrast, the inefficient and weak kings, those who allowed their administrations to crumble, who lost wars and the wealth of the people are historically defined as the great kings. As Eller says, “this observation either means that the only acceptable power in the long run is the weakest one, or that if a political leader is faithful to god, he is necessarily a bad political leader.”(1987,p34) In addition to this pattern, next to every king we have the appearance of the most charismatic figure of Christian mythology — the prophet, who is always the harshest critic of the prevailing authorities, and is always brutally oppressed by them (Isaiah, Ezekiel, Daniel, Elijah). All these factors manifest in a profound way an anti-royalist and anti-statist sentiment.

Turning to the New Testament we seem to find two contradictory tendencies.  The first ostensibly favourable to political power, seen mainly in Paul’s infamous “there is no authority but from God” (Rom. 13:1). The other, a much more pronounced and extensive hostility to power, apparent mostly in the gospels and Revelation. Since Constantine exegetes have based their entire theology of state on the few isolated statements that seem to offer a divine legitimation of hierarchical domination, most significantly Romans 13, and Jesus’ trail before Pilate. Before considering these factors it will be useful to look at Jesus’ own radically negative attitude to political power.

When Jesus began his public ministry the gospels tell of his temptation by Satan. The devil tempts him 3 times, the last of which is relevant to this discussion.  The devil takes Jesus to a high mountain and shows him all the kingdoms of the world: “I will give you all these things if you will prostrate yourself and worship me.” (Matt. 4:8–9)

Or: “I will give you all this power and the glory of these kingdoms, for it has been given to me, and I will give it to whom I will. If you then, will prostrate yourself before me, it shall be yours.” (Luke. 5:8–7) It is important to emphasise, as Ellul does in his analysis, that the gospels were probably targeted at Christian communities with a Greek origin in view, so the reference is to political power in general, not just Rome and the Herod dynasty.  (1991,p58) The text clearly states that the political realm is a satanic domain and we may thus say that among Jesus’ immediate followers and the first Christian generation political institutions — what we now recognise as the state — belonged to the devil, and that those who held power received it from him.

Another saying by Jesus on political authorities is found in a discussion in Matthew 20:20–25. The disciples are accompanying him to Jerusalem where some believed he would seize power and establish a sovereign Jewish kingdom. The wife of a man named Zebedee presents her sons to Jesus; James and John, and requests that they should be seated on the right and left hands of him in heaven — in other words, that they be promoted to positions of leadership and authority. Jesus first tells his disciples that they have no understanding, and then says:

You know the rulers of the nations lord it over them, and those in high positions enslave them. It should not be so among you; but whoever should be great among you must be the servant, and whoever wants to be first must be your slave — just as the son of man did not come to be served, but to serve. (Matt. 20.20)

The passage speaks for itself, and should be compared with the quote from Rocker above. There is no distinction made here, all political regimes lord it over their subjects — there can be no political power without tyranny.

This is of course only a rudimentary synthesis of Jesus’ various sayings on political power. There are other, equally forceful negations, most significantly his trail before Roman law. Here we see an almost mocking distain for both the Roman state and the temple priests (nascent Christendom). There are differences between the four gospels, but as Ellul comments, “the attitude is always the same whether it takes the form of silence, of accusation of the authorities, or of deliberate provocation — a refusal to accept any authority other that that of God”. (1991,p61) Some theologians such as Karl Barth contend that since Jesus did not rebel against the verdict of the authorities he regarded the jurisdiction as legitimate, and thus we find the basis for state power.  (Eller,1987,p124) This understanding is derived mainly from the statement of Jesus: “You would not have the least power over me unless it had been given to you from above, therefore he who delivered me to you is more guilty that you.” (John 19:10–11) Unless we totally isolate this statement from every other biblically recorded statement Jesus made on political power it is obvious he is saying Pilate has received his power from Satan, not from God as is the popular interpretation. Furthermore, this is congruent with an earlier statement Jesus makes, commenting that the powers of darkness are at work in his trial. (Luke 22:52–53) Indeed, every text relating to Jesus’ encounters with political and religious authority find subtle mockery, irony, non-cooperation, indifference and challenge. Jesus was certainly no guerrilla, he was a non-violent resistor, an anarchist of purely Tolstoian character.

The political refusal is constant throughout the bible, and finds its most violent expression in Revelations. While this is a contentious book subject to a diverse variety of interpretations, among Christian theologians there is little dispute that it is a prophetic representation of the apocalypse. Without engaging in a lengthy analysis it is enough to say that Revelation is concerned with the inevitable self-destruction of the human race brought on by the nature of political power — represented first by the red horse with the sword (whose sole function is making war, exercising power, and causing human beings to perish), and in the end by Babylon, the focus of political power, the power of money, and the structure of civilisation. (Morris,1987,pp45-62)

We thus find a systematic pattern of biblical negations of political power, of witness to its lack of validity and legitimacy. It is in this context we must put the very few isolated passages, such as Romans 13, which Christendom has consistently reified as a basis for hierarchy and political domination.

“There is no authority but from God” (Rom 13) should be reduced to its real meaning rather that giving us the last word on political authority — it seeks to apply love in circumstances where Christians were brutally suppressed by the ruling powers.

Essentially then, both the new and old testaments consistently reject political power. No power can claim legitimacy in itself, and by character they will always contradict the morality of God. Therefore Christians must always deny, challenge and object to this power. Without doubt, Christendom has incessantly sought to subvert this teaching, to obscure the distinction between service and power and deny the radical antagonism between gospel and state. Nevertheless, throughout Church history movements have sporadically arisen that can be defined as anarchistic in the sense they have radically reaffirmed the illegitimacy of coercive authority.  Murray Bookchin admits the origins of anarchist thinking can be found in Christianity, (Bookchin,1971,p67) and George Woodcock traces the roots of anarchism back to the heretical millenarian Christian sects of the fourteenth century. (Woodcock,1972,pp30-33)

These millenarian movements arose during the reformation period, and spread throughout Europe as the feudal system disintegrated and the lowest classes became increasingly rebellious against the imposition of serfdom. Millenarian Christianity can be broadly described as apocalyptic communalist movements which directly challenge the power of both state and Church and strove to create a society based on the community of the apostles. As Kenneth Rexroth states, “We should think of this great wave of spirituality not as something new, but as the rediscovery of something old; not as a body of doctrinal, mystical theology, and least of all in terms of the sensational episodes of the history of its struggle against the Pope and the Church, but as a way of life.” (1974,p44) The roots of these sects are found in the thinking of individuals such as Saint Francis and John Ball, The Free Spirit Brethren and the Hussite Wars, but here I will focus on 3 specifically anarchistic movements: the Anabaptists, the Diggers and the Doukhobors.

In the early 1500’s small conventicles of Anabaptist communal groups were springing all around North and West Germany. Anabaptism was an attack on the authority of the established Church to dictate such things as the rites of baptism and transubstantiation. In 1534 the town of Munster became an Anabaptist commune, Catholics and Lutherans ejected from the city and quickly replaced by incoming Anabaptists seeking refuge from persecution in the feudal provinces.  The economic structure of Munster was communist, a community of goods was implemented and all wealth in money, jewellery and precious metals was brought into a common fund. Communism of production was also introduced, a kind of anarchistic “gift economy” where guild members whose work was essential to the life of the community were ordered to work without wages and contribute their products to a common pool of goods, from which all could take freely according to their needs. While the self-appointed leader of Munster, Jan Bockelson preached equality amongst the brethren, the commune quickly became a chiliast theocracy, Bockelson implementing a strict set of laws and displaying a fetish for executions by decapitation. Munster was eventually crushed by an army of unified feudal princes and most of the population slaughtered. While the internal power structures of Munster were authoritarian, its relation to the Church and state authorities was undoubtedly anarchistic. The goal of Munster was total political, economic and religious autonomy, an ethic intensified in following Anabaptist movements.

After Munster the movement was largely divided into three parts: pacifists who refused oaths, public office and military service, but who rejected communism; those who were both pacifists and communists; and militant chiliasts who literally became extinct under relentless persecution. For years after Muster the Anabaptists were hunted pariahs, and it was difficult to practice any form of communalism.  Many Hutterite and Mennonite groups were able to find sanctuary in Moravia under the patronage of sympathetic nobility and were able to maintain outlying colonies in Slovakia and Bohemia. By the standards of the day, communities were organised in an astoundingly equalitarian fashion and perceived the state and Church as “morbid growths on the normal body of oeconomia” (Rexroth, 1974, p ix) In Austerlitz, historically the longest lived communalist society, both communism of production and communism of consumption were successful. They established their own schools (although higher education was rejected), socialised childcare and public health, and generated substantial surpluses from their systems of production and distribution. The Anabaptists’ refusal of imperial and Church hegemony ultimately lead to their expulsion from Moravia in 1622, and they were scattered throughout Eastern Europe and Russia. Eventually, many emigrated to the United States and Canada and formed sundry contemporary anarcho-communalist sects such as the Quakers, Mennonites and to a lesser extent, the Amish.

These movements had important long-term consequences in uniting religious and political dissent, an ethic closely paralleled in the anarchistic Christian movements of the English Civil War Period. It was in these conditions of class struggle that, among a whole cluster of radical groups such as the Fifth Monarchy Men, the Levellers and the Ranters, there emerged perhaps the first real proto-anarchists, the Diggers, who like the classical 19th century anarchists identified political and economic power and who believed that a social, rather than political revolution was necessary for the establishment of justice. Gerrard Winstanley, the Diggers’ leader, made an identification with the word of God and the principle of reason, an equivalent philosophy to that found in Tolstoy’s The Kingdom of God is Within You. In fact, it seems likely Tolstoy took much of his own inspiration from Winstanley:

Where does that reason dwell? He dwells in every creature according to the nature and being of that creature, but supremely in man. Therefore man is called a rational creature. This is the kingdom of God within man. Let reason rule the man and he dares not trespass against his fellow creatures, but does as he would be done unto. For reason tells him — is thy neighbour hungry and naked today? Do thou feed him and cloth him; it maybe thy case tomorrow and then he will be ready to help thee. (Woodcock, 1972,p31)

For Winstanley private property (especially land, in an agricultural economy “the major means of production”) was the source of all wealth and therefore, “the cause of all wars, bloodshed, theft and enslaving laws that hold people under misery.” (Rexroth,1974,p145) Private property divides humans, nations, and incubates the conditions of perpetual war on which the state thrives. Winstanley declared that not only masters and magistrates, but also husbands and fathers “do carry themselves like oppressing lords over such that are under them — not knowing that these have an equal privilege with them to share the blessings of liberty.” (Rexroth,1974,p141) He sketched out a vision of free society based on the teachings of Christ whom he gives the name Universal Liberty. It seems Winstanley envisioned something akin to the polity of the ancient Israelites in which the state would have power only as a court of final appeal. Some of his passages come remarkably close to the works of the great 19th century anarchists and their projections of social liberty:

When this universal equity rises upon every man and woman, then none shall lay claim to any creature and say, This is mine and that is yours. This is my work, that is yours. But everyone shall put their hands to till the earth and bring up cattle, and the blessing of earth shall be common to all; when a man hath need of corn or cattle, he shall take from the next store house he meets with.  There shall be no buying or selling, no fairs and markets. And all shall cheerfully put their hands to make those things that are needful, one helping another. There shall be none lords over others, but everyone shall be lord of himself, subject to the law of righteousness, reason and equity, which shall dwell and rule in him, which is the Lord. (Wookcock,1972,p33)

Winstanley was an extreme pacifist and seems to have believed he could achieve social transformation through peaceful example. If only the Diggers were able to implement an equalitarian community and cultivate commons and wastelands, the community of love would naturally interpenetrate all aspects of English society, eventually encompassing both rich and poor. The Diggers used a kind of direct action, squatting on unused land throughout southern England and farming it for their own sustenance. The local land owners and the state authorities went into alliance against this subversive little company, and the Diggers practised passive resistance for as long as they could endure, and were eventually violently dispersed. Indeed, the movement was a trivial and insignificant event at the time, but the erudition and sophistication of Winstanley’s writings has meant that the Diggers are now claimed by both contemporary socialists and anarchists.

Parallel to Winstanley, perhaps the most influential Christian anarcho-communalist, more influential as an individual than any populist group, was Leo Tolstoy.  Tolstoy’s works on Christian anarchy or “non-resistance” (non-violent resistance) are only a small part of Russia’s rich history of religious dissent, anchored in cultural and philosophical traditions revolving around ideas of justice, beauty (especially spiritual beauty), goodness and service to universal values. Perceiving all power as an evil, Tolstoy arrived at an unconditional rejection of all violence. Believing that the state and civil law rested on violence, Tolstoy refused its authority and held that the abolition of all coercive institutions must be brought about through peaceful means, by members of society freely abstaining from and avoiding participation in state exigencies. Tolstoy was an “essential disputer”, and his denial of state authority was in line with the statements of Jesus, “And so a Christian cannot promise to do another person’s will without knowing what will be required of him, nor can he submit to transitory human laws or promise to do or abstain from doing any specified thing at any given time, for he cannot know what may be required of him at any time by that Christian law of love, obedience to which constitutes the purpose of his life. A Christian, by promising unconditional obedience to the laws of men in advance, would indicate by that promise that the inner law of God does not constitute the sole law of his life.” (Tolstoy,1984,p143)

So the Tolstoian communes were aimed at an abrogation of power and the establishment of an organic community of non-coercive human relations. Some communes were successful and lasted for many years, but most, as Rethrox states, “were tragi-comic stories where landowners turned their estates into communes, invited their bohemian friends from the city, and urged ‘their’ peasants to share in the building of a new society in the womb of the old,” and were suppressed in short order by the official Church and Tsarist authorities.  (1974,p169)

Another such group that was active during this period in Russia were the primitivist and anarchistic Doukhobors, or “Spirit Wrestlers”. As a sect they arose in opposition to reforms in the Orthodox Church under Catherine the Great, but the movement was galvanised in 1895 when they refused conscription into the Tsar’s military. Non-violence was the core of the Doukhobor philosophy and, in their estimation, the Tsar and by association the Orthodox Church were illegitimate in the eyes of God. While the Doukhobors preceded Tolstoy, his works formed a central part in the movement’s intellectual development, he even personally paid a part of their costs to emigrate to Canada to escape state persecution. Once in Canada, the Doukhobors split into three groups: the independents, who chose to accept the requirement of citizenship and the ownership of private property, the communalists, and the radical “Sons of Freedom.” The communalists enjoyed a season of remarkable prosperity under the de-facto leadership of Peter Veregin, and their communalist economic system generated considerable wealth. Their communal structure dissipated gradually during the depression era however, exacerbated by continual relocation brought on by a refusal to swear allegiance to the king.

The most radical splinter of the Doukhobor people, the “Sons of Freedom” rallied around extreme expressions of Veregin’s anti-state and vegetarian doctrine. He wrote that an earthly paradise would only be possible with a return to “primitive conditions, and a spiritual state lost by Adam and Eve.” (Momonova,1995,p6) Vergin was largely deified inside a cultural context of traditional muzhik mysticism, and the Sons of Freedom’s subsequent fame for nudity and arson is not something happily discussed by contemporary Doukhobors. Although Doukhobors no longer live in communal structures, their Church still remains non-hierarchical and anti-authoritarian.

So, from this very limited analysis of biblical text and various sociological manifestations of Christian anarchism, it seems that not only is Christianity and anarchy mutually reinforcing, but that the theoretical base of rationalistic anarchism is deeply rooted within the history of Christian dissent. The often repeated truism, “power corrupts; absolute power corrupts absolutely” is a central tenant of both. The purpose of this essay was to formulate a reconciliation, but I have completely ignored anarchy’s sometimes rabid enmity to all religion, best summed up in Bakunin’s well known inversion of Voltaire, “If God did exist, it would be necessary to abolish him.” (Pennock&Chapman,1978,p113) Indeed, many anarchists, almost all the classics, view God as the supreme arche on which all other forms of authority find their justification, and unless the individual can learn to raise the ego to the position of the religious God, they will remain a slave. This would require a whole other discussion on the nature of divine authority as represented in the bible, and I think Jacques Ellul has persuasively argued the compatibility between “No Gods — No Masters” and “I believe in God the Father Almighty” in his essay “Jesus and Marx”. Suffice to say, Christianity’s historical perversion was to recognise the state, and I think that fundamentally, it was the character of this perversion and its many destructive consequences that the early anarchists were attacking.

An articulation between intellectual strands of Christian anarchy and rationalistic anarchy could prove seminal. Christianity’s conception of human nature could act as a counter-balance to anarchism’s more utopian tendencies, the prospect of a total eradication of societal power relations for example.  Likewise, rational anarchism could provide a springboard for transcending the orthodox doctrine of the fall as a negation of the transformation of society.  However, the possibility of any such dialectic will rest on anarchism’s realisation that Christianity does not necessarily presuppose established and rigorously maintained political power structures, and Christianity’s recognition that anarchy is the only political position in accord with scripture. Only then can Christians take their place beside anarchists.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



Christendom is an effort of the human race to go back to walking on all fours, to get rid of Christianity, to do it knavishly under the pretext that this is Christianity, claiming that this is Christianity perfected.  In the Christianity of Christendom the cross has become something like a child’s hobby horse and trumpet.(Kierkegaard, 1968, p260)



This is what the king who reigns over you will do: He will take your sons and make them serve with his chariots and horses, and they will run in front of his chariots. Some he will assign to be commanders of thousands and commanders of fifties, and others will plough his grounds and reap his harvests, and still others will make weapons of war and equipment for his chariots. He will take your daughters to be perfumers and cooks and bakers. He will take the best of your fields and vineyards. He will take a tenth of your grain and of your vintage and give it to his officials and attendants. He will take a tenth of your flocks and you yourselves will become his slaves. (1 Samuel 8:8)



You know the rulers of the nations lord it over them, and those in high positions enslave them. It should not be so among you; but whoever should be great among you must be the servant, and whoever wants to be first must be your slave — just as the son of man did not come to be served, but to serve. (Matt. 20.20)



Where does that reason dwell? He dwells in every creature according to the nature and being of that creature, but supremely in man. Therefore man is called a rational creature. This is the kingdom of God within man. Let reason rule the man and he dares not trespass against his fellow creatures, but does as he would be done unto. For reason tells him — is thy neighbour hungry and naked today? Do thou feed him and cloth him; it maybe thy case tomorrow and then he will be ready to help thee. (Woodcock, 1972,p31)



When this universal equity rises upon every man and woman, then none shall lay claim to any creature and say, This is mine and that is yours. This is my work, that is yours. But everyone shall put their hands to till the earth and bring up cattle, and the blessing of earth shall be common to all; when a man hath need of corn or cattle, he shall take from the next store house he meets with.  There shall be no buying or selling, no fairs and markets. And all shall cheerfully put their hands to make those things that are needful, one helping another. There shall be none lords over others, but everyone shall be lord of himself, subject to the law of righteousness, reason and equity, which shall dwell and rule in him, which is the Lord. (Wookcock,1972,p33)

