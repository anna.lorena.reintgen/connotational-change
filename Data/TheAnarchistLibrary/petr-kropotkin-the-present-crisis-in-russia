
The last students' disturbances in Russia were quite different from all the disturbances which have taken place in the Russian universities for the last forty years. They began, as all students' movements begin, with an insignificant incident, which concerned the students alone; but, owing to a series of circumstances quite peculiar to Russia, they took, all of a sudden, a political complexion; and in this respect they acquired such a significance that they will now count in the history of the constitutional movement in Russia as an important milestone. Consequently it is impossible to speak of the last events without going deeper than their surface — that is, without touching upon the general problem of education in Russia, and without mentioning the steps through which the development of the constitutional idea has passed in our country since 1861.

During my stay last month, at Cambridge, the Harvard students held a noisy meeting to protest against the "mutton monotony" of their food at Memorial Hall. In a similar way, but with a more serious purpose, the Kiev students, excited by the fact that one of them had been brought before the justice of the peace for misconduct in the streets of that city, also held a meeting. At Harvard, the meeting ended in fun; but at Kiev the dean of the university excluded a number of students from the university, for one year, for having held that meeting, and put others under arrest. "What would you do in such a case?" I asked several Harvard men; and the reply always was: "Why, of course, hold another meeting!" This is what the Kiev students did. They held another general meeting and asked the dean to have a talk with them. Whereupon the dean sent for the town police, the state police and the troops. Incredible though this may seem, it actually happened. It is confirmed, not only by scores of private letters, but also by an official statement, published in the Official Messenger. "The meeting was illegal, and the dean sent for troops to disperse it." The most intelligent of all those who were summoned by the dean proved to be the head of the Kiev State Police, the Gendarme General Novitsky. I knew him: he examined me while I was kept in the St. Petersburg fortress. He is intelligent, and my opinion of him was that, like so many others, he is a better man than the institution he belongs to. General Novitsky, seeing that the meeting was quite peaceful, brought in the dean, who was immediately offered an armchair by the side of the chairman, and was treated very politely by the students. But the meeting ended in nothing — the dean refusing to revise his orders. "Was not the meeting uproarious? Was it not political?" I was asked by my American friends. "No, the facts were as I state them; the Official Messenger itself has confirmed them." "Was, then, the dean a madman, or a fool?" "No, he was not either of these." And there lies the cue to all the students' disturbances of the last forty years.

Everything has been reformed in Russia since 1861. Serfdom was abolished; corporal punishment was nearly got rid of; new, open courts, with juries, were introduced; some sort of self-government was given; military service was entirely reformed and rendered obligatory upon all — education alone was treated as a step-daughter, with suspicion. All Russia, from the log hut to the mansion, wanted and loudly called for education; women and men of the wealthier classes were ready to give any amount of time and money to spread education among the peasants. They are ready still. And everywhere the efforts of the university professors and of the directors of the colleges, of the provincial self-governments, of the wealthy municipalities and the private donors, were rebuked, annihilated, by the successive Ministers of Public Education, who, since 1862, have always been nominated, not to spread education broadcast throughout the country, but to prevent its spreading.

Such a statement must sound, of course, very strange to an American ear; but if persons belonging to different parties in Russia may explain and excuse the fact in different ways, all will agree, nevertheless, that the Ministers really endeavored to keep education within certain narrow limits, rather than to allow it to be spread. Moreover, here are some conclusive facts. Thus, for instance, while even now we have in European Russia only one school for each 2,230 inhabitants, and while only one child out of every twenty or thirty children of school age goes to school (as against seven in England), the Ministry of Public Instruction, for years in succession under Alexander II., returned every year to the State Exchequer one-half of the poor allowance of $4,000,000 a year for the primary schools, which was inscribed in the budget. It found no use for the money! And if the Ministry of Public Instruction spends now its budget allowance in full, it is because it has hit upon the following plan: It does not open schools of its own, but spends the money in subsidies to the village clergy, who, leaving aside their general ignorance, keep schools mostly on paper only. Their time being fully taken up by their regular duties (marriages, burials, etc.), they generally pay quite ignorant cantors, or retired soldiers, to attend to the schools. And all this is perfectly well known in Russia. It is continually mentioned and repeated in the press, in the provisional assemblies, and in the local school boards. And yet no heed is taken by the central government of this permanent, standing cause of growing discontent.

The more free a region of Russia is from the direct influence of the Ministry of Public Instruction, the better it stands for education: this may be taken as a general rule. Thus, in Caucasia and Turkestan, which both find protection from the Ministry of Public Instruction in their independent Governors General — who usually are enlightened general staff officers — there are more schools and the schools are better than in Russia proper. The Caucasian educational district is especially foremost on this account. As to Russia itself, the province of the Don Cossacks, of which the Cossack territory is under the Minister of War, has beyond comparison the greatest number of the best schools, primary and secondary, for boys and girls alike. Again, the provinces which have local self-government (zemstvo) have nearly twice as many schools, in proportion to their population, as the provinces which get their schools from the Ministry.

Nay, nowhere else in the world could one find the following anomaly. For thirty years, all parents in Russia were crying, shouting and agitating for a reform of the lyceums. Not Greek and Latin, they said, but natural sciences and technical knowledge must be laid as a foundation of the teaching in lyceums. "More technical schools of all degrees," was for forty years the general demand. Everywhere Russia wants more engineers, more chemists, more skilled workers and more educated technical experts. All the reviews and papers, with the exception of the Moscow Gazette, are full of bitter complaints of the parents about the want of Realschulen [secondary schooling] and technical schools. Protectionism, maintained with a view of developing national industries, and an absence of technical schools are evidently self contradictory; and yet, for forty consecutive years the Ministry of Public Instruction has bitterly fought against all Russia, refusing technical schools; it has maintained that a scientific and technical training would breed revolutionists; while — to use an official avowal — "the boy who must translate for to-morrow morning two pages from Cicero will have no time to read Pisareff or Dobroluboff." It is only now, since the Minister of Finances, De Witte, has publicly declared that the State cannot own the mines and the State railways, regulate the output of all the spirit and sugar factories, and favor the growth of Russian industries in the interest of the State budget, unless it does its utmost for spreading technical education, that a step has been taken to reform education in the lyceums, to turn it more in the direction of natural sciences, and to open a number of polytechnicums and lower technical schools.

The fear of the revolutionary spirit, which would grow, it was said, in Russia and render absolute rule impossible, so soon as education is spread in the country, was so great that two generations of young men were sacrificed to it. Scores of quite ignorant Czechs, who did not even know Russian, were shipped from Bohemia to teach Latin and Greek; and they taught it in such an abominable way that only two per cent, of all the boys who entered the lyceums could finish the eight years' course and be admitted to a university. The Government preferred to let loose upon Russia crowds of boys who left the lyceums as absolute ignoramuses after a three or four years' mechanical study of Latin and Greek, rather than to make any concession to public opinion in the way of a more reasonable scheme of education.

As to the universities, nearly all the best professors, the glories of Russian historical and humanitarian sciences, were compelled to abandon their chairs: Kostomaroff, Kavelin, Stasulevitch, M. Kovalevsky, the physiologist Syechenoff, and others like them, were compelled by ignorant heads of Educational Districts, and equally ignorant Ministers of Education, to retire.

The study of comparative State law was prohibited, and the Russian students had to remain in ignorance of the constitutional laws of the civilized nations. The study of Russian history, law and economics became a study of "conventional lies;" the general tone of university teaching was lowered. With natural sciences it was still worse; such chairs as that of geology and physiology remained unoccupied for years. A geologist myself, I have passed through the St. Petersburg University without ever having heard one single lecture on geology.

As to the students, every young man, as soon as he entered the university, was placed on the list of suspects. Police spies and provoking agents swarmed in the universities; laws upon laws were issued to prohibit all sorts of meetings in the university buildings or in private lodgings. Gradually, the higher authorities of the Ministry of Public Education came to the view that every student must be looked upon as an enemy of society, and be treated as such; so that both the deans and the curators of the Educational Districts were chosen by the Ministry from among men who were better known for their police capacities than for their learning. Consequently, when, three or four years ago, the St. Petersburg students, at their anniversary meeting, whistled at the appearance of one of their professors, while they cheered the others, a thing which happens and will happen in every university, the dean sent immediately for the police, who brutally assailed and dispersed the students as they were leaving the university building in a crowd, and the famous beating of the students on a bridge across the Neva followed. Many of the students were arrested on this occasion, and hundreds were excluded from the university. On learning this, all other universities made a strike, refusing to follow the courses so long as their St. Petersburg comrades were not released; the result being that many hundreds of young men were excluded from the other universities as well. It so happened, however, that when the police were beating the students on the Neva bridge, the Dowager Empress passed by, and was cheered by the students. So she spoke to her son: "They were quite loyal," she said; "they cheered me. Why do you allow the police to treat them so brutally?" The result was that the ex-Minister of War, General Vannovsky, was appointed to make a general inquiry. He proved that there was not the slightest reason for calling in the police, lectured the police authorities, canceled nearly all the orders of exclusion of students, and released all of them. A military officer had thus to interfere for the defense of the students against the Ministry of Public Instruction.

It is thus evident that the dean of the Kiev University was neither a lunatic nor a fool. He was simply an obedient functionary, who acted in accordance with the instructions of his principal — the Minister of Public Instruction, Bogolépoff.

M. Bogolépoff, in his younger days, was more or less of a Liberal; but, since he has obtained his nomination as Minister of Public Instruction, he has been a mere tool in the hands of the Procurator of the Holy Synod, Pobiedonostseff, a narrow-minded fanatic of the State religion, who — if it were only in his power — would have burned at the stake all protestants against Orthodoxy and Catholicism. And it was these two men, Bogolépoff and Pobiedonostseff, who reported the Kiev affair to the Czar.

The further development of events is well known through the daily press.[1] When the Kiev disorders were reported to Nicholas II., he said, first, that he had had enough of these students' riots and would close all the universities.[2] He spoke next of sending all "riotous" students to Port Arthur, and finally issued, through his Minister of Public Instruction, but against the advice of the Minister of War, an order, in virtue of which the students who took part in university disturbances would henceforward be punished by being sent as privates to the army for terms of from two to three years — the punishment to be inflicted by special courts nominated ad hoc and composed of university professors, town-police and State-police officials, and military officers; their sittings to be kept secret. One hundred and eighty-three Kiev students and twenty-two St. Petersburg students were condemned to this punishment and were carried away as criminals, in absolute secrecy, to some unknown destination, presumed to be Port Arthur. Twelve of them refused to take the military oath of allegiance to the Czar, and were consequently court-martialed and condemned to death, and finally sent to military hard labor for life in military punishment battalions.

These measures produced, as might be expected, a general commotion all over the country. I have seen letters written by parents of high standing to their friends, showing a state of complete exasperation. Hundreds of parents rushed to St. Petersburg in order to try to save their sons. Representatives of the law — namely, the public prosecutors at Kiev and St. Petersburg — one General of the State Police and one military General who took part in the above-mentioned courts, protested in writing against the application of the Imperial Order; and sixty-five university professors wrote to the Czar a letter, at the risk of being treated as rebels and sent to Siberia — collective letters to the Czar falling under the law of conspiracy — urging him to withdraw his Order, and sending their letter to London for publication. And when 12,000 students united in a general uprising, and the student manifestations at St. Petersburg, Moscow and Kharkoff, supported by demonstrations of the organized workingmen, were dispersed by the lead-weighted horsewhips (nagaikis) of the Cossacks, who cut open the faces of men and women in the streets, the general indignation was so intense that it burst out openly. The "respectable" Society of Authors, a venerated member of the Council of the State, Prince Vyazemsky — nay, the very Cossacks of the Bodyguard — protested against the treatment of the crowds; and finally the Committee of the Ministers, assuming for the first time since the reign of Alexander I. the rôle of a "Ministry," discussed the Imperial Order and insisted upon its withdrawal. It refused to acquiesce in the will of the Czar, which was to proclaim a state of siege at St. Petersburg, and it obtained from the Emperor the dismissal of the St. Petersburg préfet de police, General Kleigels.

The cause of this unanimous discontent is self-evident. What would the Americans say if President McKinley had ordered the Harvard students involved in the above-mentioned meeting to be sent to the Philippines? The country would certainly rise in indignation; that is what happened in Russia. All Russia said that the Czar's Order was a return to the abhorred "times of Nicholas I." And yet, in fairness to Nicholas I., I must say that the idea of making military service a general, legal measure of punishment never crossed the brain of "the iron despot," although he might have had an excuse for it, because in those times the serf-owners used to punish their serf-servants by sending them to the recruiting boards. It is true that Nicholas I. sent the poets, Polezhaeff and Shevchenko to be soldiers, but he did it in the following way: The student Polezhaeff had written a poem, "Sashka," in which he insulted the Czar and his favorites; and the poem circulated in manuscript copies, one of which was reported to the Czar. Nicholas I. sent for Polezhaeff at night, made him read the poem aloud, and finally said: "You know what punishment you must undergo for having written these verses? Now, I waive it, and give you the means of rehabilitating yourself by military service. Are you ready to serve as a private in the army?" And when Polezhaeff, having no choice, accepted the offer, Nicholas added: "Try to distinguish yourself, and to win the officer's grade; and if you are in any difficulty, write to me directly" — which Polezhaeff really did once, with a good result.

All Russia knows this episode from the unfortunate poet's life, which is recorded in his biography. All Russia has commiserated, and commiserates still, himself and Shevehenko. Could it, then, accept the Order of Nicholas II. otherwise than with general indignation?

As to the idea of making military service a general measure of punishment, it shows on the face of it that it is unconstitutional; and it remains an open question whether the Cassation or Judiciary Departments of the Senate (which have lately shown on several important occasions their intention to prevent the abuses of power of the high functionaries), if the question had been brought before them, would not have declared the Czar's Order contrary to the existing military law and reprimanded the Minister of Public Instruction for having submitted a measure which was unlawful for the signature of his sovereign. Of course, we have no constitution in Russia; but "Autocracy" is not "Despotism." The Czar may repeal any existing law by bringing the proposal of its repeal before the Council of the State; even if his proposal obtains only the minority of voices, he can carry it through by voting with the minority. But the proposal must be laid before the Council, and so long as a law has not been repealed, it is equally binding both on the Czar and his subjects. Thus, a Czar cannot marry a lady who is his own subject without forfeiting his rights to the throne — such is the law of Russia; and he cannot reintroduce serfdom, or abolish obligatory military service without submitting such schemes of law to the Council of the State. Still less can he issue an Order which violates the existing law — it being the duty of the Senate to remonstrate with him in such a case before it promulgates the Order. Such is, at least, the State Law in Russia.

Of course, if the Czar's Order had not run so strongly against the general feeling of the country, its illegal character, would have passed unnoticed. But now that a rash and ill-tempered measure of the young autocrat has set the whole country on fire, resulted in bloodshed in three large cities, and nearly became the cause of further disasters, not only its illegal character has become the subject of general discussion, but the thinking and mature portion of the country, including the Czar's own ministers, have vividly realized the dangers of autocracy — that is, of the rule of an irresponsible clique of courtiers. The Minister of Finance, De Witte, evidently has grown especially hostile to that rule since he saw the other day that, were it not for his violent opposition, all his many years' patient work of reconstructing Russian finances would have been nearly upset by a panic-born declaration of a state of siege at St. Petersburg, which measure would have meant the rule of martial law, executions, gallows and terrorist reprisals — and also the failure of all loan negotiations, so important now, after the China and Manchuria scare. The education question was thus driven to the background, and the great question, Autocracy or Representative Government, which has never ceased to pre-occupy Russia since the year 1861, suddenly rose full-fledged out of the disturbances.

Foreigners do not usually realize the depth and breadth of the constitutional movement in Russia; but the fact is that twice within the last forty years — namely, in 1860-1863 and in 1880-1881 — Russia has been on the eve of becoming a Constitutional Monarchy. When serfdom was abolished in 1861, and the series of reforms which ended in the abolition of the knout, the installment of provincial self-government, the new judicial law and the military reform, was under discussion, it was generally considered that these reforms were only preliminary steps toward what was described then, in Napoleon III.'s words, as "le couronnement de l'édifice" — the crowning of the building — that is, the convocation of a Parliament. Every one at that time was persuaded that the granting of a Constitution was only an affair of a few years — deferred only for such time as might be necessary for working out the preliminary reforms, such as the reform of the courts, or the establishment of local self-government. Nay, the way in which the financial questions concerning the general economics of the Empire were neglected then by the reformers was very characteristic of their ways of thinking; the general impression being that the reconstruction of the miserably poor financial affairs would be the proper duty of the Zemskiy Sobor, or Representative Assembly.

I have described in my memoirs how, in a remote province of Siberia, the Governor of the province, with his aide-de-camp and the heads of the Cossack administration, and of the Judicial and Excise departments, worked hard in those years upon reforms which the St. Petersburg Government intended to accomplish — municipal reform, prison reform, and so on. But what we did then in one small town was done in every other provincial town of Russia, thousands and thousands of men working most conscientiously to complete the great changes which were considered as preliminary to the great reform — the Constitution. All honor is certainly due to Alexander II. for having dared to announce his intention of liberating the serfs and of reforming all the inner life of Russia, and especially for the support he gave to the granting of land to the liberated serfs. But the colossal work of elaborating the scheme of emancipation, the new Judicial Law, and so on, in their infinitely complicated, minute details, belongs to Russia itself — to the many thousands of men who joined in this work. All intellectual Russia — historians, political writers, landlords, functionaries of all classes, military men and "men of no rank" — have had their share in these reforms. And all of them, beginning with the Winter Palace itself and ending in the smallest provincial town, knew, and said, and wrote that a representative government would be the only way to consolidate these reforms and to make them bear fruit. Nay, the necessity of this "consolidation" was so keenly felt in 1861 that I often heard it said in my youth that, if Alexander II. should fail to grant a Constitution, his brother, the Grand Duke Constantine, might become, in case of need, a Constitutional King of Russia.

The Polish insurrection of 1863, and especially the menaces of intervention in favor of the Poles which were made by Napoleon III., and the vague promises made to them in England, put an end to all these hopes; the "nationalist" serf-owners' party headed by Katkoff took the upper hand, and there was no question more of a Constitution till the years 1880-1881.

In 1880, when the Terrorist Executive Committee fought its terrible war against the Czar, Alexander II. himself renewed the constitutional hopes, after the Winter Palace explosion, by investing General Loris Melikoff with nearly dictatorial powers. This nomination was generally understood as indicating a desire on the part of Alexander II. to grant a Constitution, and the subject began to be discussed in veiled terms in the Russian press itself. In fact, when one reads the memoirs of Loris Melikoff, in connection with what is known from various other sources about the same period, one necessarily comes to the conclusion that the promulgation of a Constitution was extremely near at hand during the last few months of the life of Alexander II. If it was not done, the fault was, on the one side, in the indefinite and changeable mood of the Emperor's mind, and, on the other, in the lack of decision on the part of Loris Melikoff himself. Alexander II. evidently wanted to have by his side a man who would, so to say, force upon him the decision of which he saw at times the necessity; but Loris Melikoff was not the man of firm will who was required to achieve that end.

At any rate, it is a well-known fact — which has been rendered public even in Russia, with the authorization of censorship — that on March 13th, 1881, Alexander II. had signed an Order enjoining Loris Melikoff to lay, on the following Thursday, before the Council of the State, a scheme for the convocation of what the Emperor himself described as an Assemblée des Notables. Representatives elected in each province through the intermediary of the provincial district Assemblies (Zemstvos) had to be summoned to St. Petersburg, in order to discuss the general affairs of the State.[3] It is also known that Alexander II. was killed this same day; whereupon Loris Melikoff, instead of sending immediately to the Senate's printing office the order signed by the Czar, hesitated to do so, and waited for orders of the new Czar, Alexander III., who, after a few weeks' hesitation, issued a Manifesto, in which he announced his intention of remaining an autocratic sovereign. This Manifesto induced all the ministers of his father, including Loris Melikoff, to resign.

The history of these few weeks is extremely interesting and is told in detail in the Memoirs of Loris Melikoff; but, strangely enough, it is hardly known except among the Russians. And yet it is a fact that it was only owing to a chain of circumstances, almost accidental in character, that Russia did not get a Constitution during these five or six weeks — historical "accidents" being evidently due themselves to deep-lying causes. The old German Emperor, Wilhelm I., very seriously advised his nephew, by letter, to grant a Constitution, only adding that he must have the civil list in his own hands. Several schemes providing for a Constitution were also submitted to Alexander III. — the most intelligent of them being, in my opinion, the scheme of the Grand Duke Constantine. He advocated something similar to what Canada has now — namely, seven different Parliaments; and, taking into consideration the vast population of Russia (150,000,000 by this time), its sparseness over vast parts of the Empire, the unmanageable character of a Parliament which would have at least 3,000 deputies, as also the diversity of the manners, customs and interests in different parts of Russia — it is evident that a federalist scheme, similar to the scheme advocated by Constantine, would have been infinitely preferable to any centralist scheme. As to Loris Melikoff's scheme, I have already mentioned it. It came, after all, to very little, and was evidently centralist. Notwithstanding all these difficulties, Alexander III. seems to have made up his mind to grant a Constitution, and Melikoff mentions a note which the Czar wrote to his brother: "At last," he said in this note, "I have the mountain off my shoulders. I have asked my ministers to draft the scheme of an Assembly of Representatives." But the ministers seemed to lose time in further hesitations, while the head of the conservative party, Katkoff, lost no time in coming to St. Petersburg and in supplicating Alexander III. to take no such step. It is also very probable that Pobiedonostseff, and even the quite honest democrat Slavophile, Ivan Aksakoff, acted in the same way — the latter advising the Czar to reduce, first, by his own authority, the taxation burdens which were crushing down the peasants. At any rate, the "Program of Alexander III.," which was printed in a French review, and the authorship of which is attributed to Turgueneff (the translation, I should rather say), contained such a series of measures in the interests of the peasants as was suggested by Aksakoff — namely, the abolition of the poll-tax and the tax on salt, a notable reduction in the redemption tax for the allotments of land, the consolidation of the village community. Seeing on the other side that his ministers were extremely slow in preparing a draft of a Constitution, and thinking that it was necessary to put an end to the unsettled state of affairs, Alexander III. wrote, a few weeks later, to his brother that he had at last decided to retain autocratic power, and that he had asked Pobiedonostseff to write a manifesto to that effect.

It is thus seen that foreign rather than domestic causes prevented Alexander II. from taking in the sixties further steps in the constitutional direction; and that twice during the year 1881, the two Czars, Alexander II. and Alexander III., were on the very point of granting to Russia a Constitution, or, at least, of taking the first decisive steps in that direction. The idea of a Constitution is ripe in Russia, even in the highest administrative spheres, and consequently one need not be astonished to see that disturbances which began in a university suddenly acquired the importance of a constitutional question. In fact, this idea has never been abandoned since 1881, and it has ripened, especially since the death of Alexander III. The nomination of General Vannovsky to the post of Minister of Public Instruction will not diminish the difficulties of the general situation, and new conflicts are sure to arise upon minor points between the young Czar and the country, as well as the highest functionaries in the State Administration. Speaking plainly, the fact is that Russia has outgrown the autocratic form of government; and it may be said confidently that if external complications do not disturb the peaceful development of Russia, Nicholas II. will soon be brought to realize that he is bound to take steps for meeting the wishes of the country. Let us hope that he will understand the proper sense of the lesson which he has received during the past two months.
[1] See also an article of mine in The Outlook, April 6, 1901. The doubts which I expressed there as to the accuracy of the sensational telegrams concerning plots against the Czar's life have been fully confirmed since. It is now stated by the New York daily press itself that they were mere inventions, coming no one knows whence.
[2] Telegram to the London Times from its own correspondent, confirmed since by private letters.
[3] The details concerning this "constitution" are published in Loris Melikoff's Memoirs, and, independently, in a work on the Russian State Law, published in Russia in 1900.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

