
“And there are other losses involved. A loss of basic trust. A loss of feeling of mutuality of relatedness. In its stead is emplaced a contempt for self and others. If you’ve been punished for showing autonomy, iniative, or independence, after a while you’re not going to show them.

In the aftermath of this kind of brutalisation, victims have a great deal of difficulty taking responsibility for their lives...They seem to behave as though they’re still under the perpetrator’s control, even though we think they’re now free. But in some ways the perpetrator has been internalised.”

— interview with Judith Herman in “A Language Older Than Words” by Derrick Jensen.

Anarchists and activists are just as traumatised and brutalised as Shell executives or George Bush. We can be just as short-sighted, just as destructive in our own way.

To think we’d be able to create a better world while we’re still sick is to deny the nature of oppression cycles.  And just as many abused children go on to abuse kids themselves unless they are healed, so we replicate the patterns we’ve learnt as traumatised, abused citizens. We repeat the manners and ways of civilisation within whatever fight we choose. So doesn’t it make sense to look at our own thoughts and actions and start healing our domesticated, fractured selves?

How do we go about this? How do we re-wild ourselves? I am loath to share my ways with you because they don’t translate into the language I’ve been trained in. Our civilised language doesn’t have the right words for processes that matter. And besides, my ways mightn’t suit you or your experiences and would only cripple you further. Trusting in your own direct experiences is the key to all this healing so there’s not much point in me telling you — that would only direct you in the way that I went, not in the way you need to go.

So what was the point of this article then? The start of the journey. The begining of a process of re-wilding, of healing. Of admitting there is a problem with ourselves — yes! us enlightened ones! Of realising how little we know as civilised people and then trying to learn from Life all that it’s willing to teach us. And Life’s not stingy — we got a lot to learn.

We can listen to the lessons the wild teaches. We can listen to the words of still-wild people alive today. Then we can change our lives — transform ourselves from crappy slaves to budding freemen.

“You don’t need to save the world, you need to save yourselves.”

— Mbatian Nkomo

Maybe it’s because we feel we don’t have any self-control that we feel the need to control others. More and more I’m thinking the activism is just another way of indulging in that other-control rather than self-control.

The Aborigines (and apologies for lumping in hundreds of different tribes, but I think there was many similiarities between the groups) are allowed to behave any way they want when they are babies. They are not chastised for having tantrums, for being greedy, for throwing stones.  They are allowed to express any emotion, any feeling they have. By the time they are toddlers they have grown out of this self-centred, infantile behaviour. (Gently encouraged to be responsible animals, not forced to be ‘good’) They then learn to be selfcontrolled, autonomous beings within the safety and love of their community of people, animals, plants, minerals, etc.How do we get beyond our infantile behaviour without the security and love of the community of life? We can’t. So we have to feel this safety and love and completedness of Life that’s ever present in the Wild before any healing can occur.  Maybe if people can tap into this energy things would start changing.  We would start changing.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“And there are other losses involved. A loss of basic trust. A loss of feeling of mutuality of relatedness. In its stead is emplaced a contempt for self and others. If you’ve been punished for showing autonomy, iniative, or independence, after a while you’re not going to show them.


In the aftermath of this kind of brutalisation, victims have a great deal of difficulty taking responsibility for their lives...They seem to behave as though they’re still under the perpetrator’s control, even though we think they’re now free. But in some ways the perpetrator has been internalised.”


— interview with Judith Herman in “A Language Older Than Words” by Derrick Jensen.



“You don’t need to save the world, you need to save yourselves.”


— Mbatian Nkomo

