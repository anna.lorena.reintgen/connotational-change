
The situation in the Middle East demands the attention and deep concern of all of us.  Once again the government is gearing up to solve a problem which past and present US administrations have helped to create and exacerbate.  The media are filled with propaganda calculated to stampede Americans into support for a war to defend oil interests.  But we must reject these tactics and their perspectives.  We must not lose sight of the stark realities which have shaped the current crisis.

For many years, and until very recently, the brutal, inhumane Iraqi regime was supplied with US military and other aid, US corporations had no reluctance about doing business in Iraq.  Right up to the August, 1990 invasion of Kuwait, the US was Iraq's largest trading partner.  The US government willingly Guaranteed billions of dollars in loans to help this process along.  It was common knowledge that a large portion was going to buy high-tech military instruments of mass destruction.  This was justified as countering the Iranian fundamentalist Moslem regime and Soviet influence in the region—and no US official ever showed any real concern for the ways in which the Iraqi rulers brutalized their own population, or the way they repressed political dissenters, not even when poison gas was used against entire Kurdish villages.  The US government was most certainly aware, while furnishing this aid, that thousands of political prisoners, including pro-democracy dissenters, were detained without charges or trials in Iraqi prisons, or jailed after trials which did not meet even generally accepted international standards of fairness; that torture of political prisoners and others was widespread; and that relatives, including children, of suspected dissenters were imprisoned while the authorities were seeking the suspects.  All of this was widely publicized by Amnesty International and other human rights organizations, and was aired in the US press as well.

American officials didn't care about the fate of those people caught in territory occupied by the Iraqi army during the Iran-Iraq War.  They readily provided the Iraqi military with helicopters, satellite reconnaissance and other logistical and economic support for the war with Iran.  And now, in the same way, they are building up the Saudi Arabian military for a war with Iraq.

The Bush administration was quite willing to countenance some Iraqi intimidation of the Emir of Kuwait, and even some seizure of disputed territory, with the aim of stopping the Kuwaitis from overproducing and thereby depressing oil prices.  Only when the Iraqi military overstepped the bounds, and invaded the entire country, was Iraq condemned as a despotic and expansionist state on the model of Nazi Germany.  Now, we are told, young Americans are to be sacrificed to rescue Kuwait, to protect our standard of living by keeping oil prices down, to unseat Saddam Hussein, the new Hitler, to get rid of Iraqi nuclear weapons...or what will they tell us next?

The Iraqi government is clearly abominable and has been all along, even when the US, the Soviets and others were aiding it.  But this is no reason why we should support the Kuwaiti or the Saudi Arabian regimes, even if they represent lesser evils.  Neither of them have been anything but autocratic and exploitative.  While we feel deep sympathy for the present suffering of the ordinary Kuwaiti people, we must not forget the problems they had under the government of the royal family before the August, 1990 invasion.  Although the Kuwaiti regime had a semblance of constitutional monarchy, a broad section of people born, educated and employed in the country were denied citizenship.  Only those whose families could prove residence in Kuwait before 1920 had full civil rights.  Of the 1.9 million Kuwaiti subjects, fewer than 800,000 were considered citizens, and of those only 62,000 men (3 percent of the overall population) were eligible to vote.  No women could vote, and their civil rights were limited in other ways as well.  Most political parties were outlawed.  Labor unions and other associations were severely restricted, when permitted at all.  Public employees could be fired without being given a reason or the right to appeal.  The press was strictly censored on a regular basis, and newspapers were closed arbitrarily, also without any way of appealing decisions.

Although political repression was not as brutal or thorough in Kuwait as in Iraq or Syria, or for that matter in Saudi Arabia, it certainly did exist.  Still, there were thousands of Kuwaiti Progressives and democrats of all sorts protesting against the Sabah regime.  But the government did not want even minimal participation on the part of its subjects, and therefore in 1986 even the semblance of democracy was suspended in a move to silence all those calling for reforms.  The elected national assembly was dissolved by royal decree.  But democratic-minded Kuwaitis have not ceased to protest against the regime's injustices.  In late 1989 the Emir refused to accept petitions signed by thirty thousand citizens calling for more representative government and freedom of expression, association and the press.  In response, thousands rallied in protests.  Some were forcibly and brutally dispersed by police.

Over the years, opponents of the Kuwaiti government have been arrested and detained without charges or trials, and some were given blatantly unfair trials.  Some were tortured.  In February of 1990 a US State Department report excused human rights abuses because of what it called Kuwaiti “security concerns stemming from the large expatriate population and Iranian-inspired subversion.”  But pro-democrats have insisted that government abuses cannot be justified on security grounds.  They have pointed out that the broad political repression struck many people beyond the small circles of Iranian government-inspired groups.

Saudi Arabia, which US forces are supposedly “protecting” against Iraqi invasion, does not even have a semblance of democracy or a minimum of social equality.  Political dissent is not allowed.  For years, large numbers of people have been arrested and held without charges or trials, denied contact with family, friends or lawyers for long periods of time.  Torture has been common.  The most brutal forms of punishment—including amputation of fingers and hands, floggings and stoning to death—continue to be utilized.  Strict censorship is enforced.  Women are excluded by law from participation in most aspects of public life, even from driving vehicles.  Those who recently protested against their subjugation were fired from their jobs, and some were jailed.  The US government has been giving billions of dollars and huge quantities of arms to help strengthen this government.

And what about the Bush administration's claims that it is going to deliver the people of Kuwait, to rescue them from the clutches of the new Hitler?  The only answer has to be that most Kuwaitis have no reasonable hope for anything from US military intervention.  During the many decades that this government has been playing world policemen and savior the record has not been good at all for the fate of ordinary people—although it may give comfort to friendly brutal regimes.  From the devastation of the Philippines at the end of the nineteenth century, to the subjugation of Haiti in the second decade of the twentieth, to Nicaragua in the 'twenties and 'thirties, to support for the blood-thirsty regime restored to power in Greece at the end of World War II, the record is replete with examples of rescues which crushed popular insurgencies and left the majority of the people worse off.  Where was this government's vaunted concern for democracy when it was supporting Cuba's Batista, the Shah of Iran, the Somoza regime, Marcos, the Duvaliers ...?  And so on.  Just ask the Chilean people who were rescued from the Allende government in 1973 to be delivered into the monstrous hands of the military.  Ask the people of Nicaragua, who have had to suffer the murderous assaults of the US-backed contras and the strangling of their economy by US sanctions.  Ask the people of El Salvador, who are still being saved from Communism by death squads trained and backed by the United States.  Ask those people of Panama who survived the bombing of their impoverished neighborhoods, and are still living in temporary shelters, and are once again being subjected to a corrupt regime and CIA-sponsored surveillance.

In Granada, since US troops rescued it impoverishment has been dramatic:  the majority of people are less able to obtain food, clothing, housing and even health care than before.  Small farmers have been pushed off the land; unemployment has risen to an all-time high of 25 to 30 percent.  And as for democracy, the post-invasion government under Blaize adopted an Emergency Powers Act in 1987, giving the police extensive powers to detain and deport political suspects, to declare curfews and enforce censorship of “politically sensitive” calypso music.  The only real beneficiaries of the US rescue have been a few wealthy Granadians and big US corporations operating there.

Nor should we lose sight of this government's support of so many other repressive regimes, including the South Korean police state, the Indonesian military mass murderers, and now even including the Syrian state.  In exchange for support of its Persian Gulf adventure, the Bush administration is giving Syria $1 billion in arms aid, and tacit permission to wipe out all opposition to its occupation and domination of parts of Lebanon.  Nobody disputes that the Syrian government is at least as brutal and expansionist as the one headed by Saddam Hussein.

We have no reason to believe that the US government is motivated by concern for the Kuwaiti people, or the ordinary people anywhere in the Middle East.  As a matter of fact, American military action there can only make a horrible situation worse for the vast majority.  A military assault to reestablish the royal family must inevitably bring to Kuwait a social and economic order even more exploitative and inhumane than before, when the vast majority of people there lived miserably.  The devastation already caused by the Iraqi military can only be compounded by a US attack.  Conditions can only be made worse for those without wealth and privilege.  The administration's callous disregard for the human suffering that must inevitably result from its intervention is reflected in a Newsweek article of August 27, 1990, in which it assesses the projected cost to the Kuwaitis:  “The damage would be immense.  The United States might have to destroy Kuwait—oil refineries, its port, and much of its capital city—to save it.”  The tragedy overlooked in this gross calculation is that many thousands of Kuwait's people must die horribly, or be permanently maimed in the process.

The war is also certain to be an ecological catastrophe, leaving behind massive contamination of the soil, water and air.  From the example of Vietnam, we know how long lasting such devastation can be.  Fifteen years after the end of the US invasion, all living things are still plagued by illness and disease from the residue of modern US weaponry.

The ordinary people are always the losers in such wars, no matter which state wins.  Only the most brutal and brutalized have a good chance of surviving.  These wars leave behind hatred, discontent and impoverishment—the material for more state rivalries and future wars.

All those who were lured into the US military with promises of job training, education and regular employment, which they hoped would get them out of poverty, are now being told that the bill is coming due, and that the price quite literally, may be their lives.  It is expected that tens of thousands may die and just as many may be injured for life.  Such a bargain is clearly outrageous and unjust.

And here in the US, the cost of the war will be borne by the rest of us as well.  Already the military has been draining vital resources away from real human needs.  A war must erode rather than maintain our standard of living.  It can only promote the interests of the well-to-do, strengthen the power and the profits of the oil industry and its capacity to destroy the environment.  It can only serve to perpetuate the state's prerogative to fling young Americans into battle to bolster the global image of American state power.

And if the Iraqi regime is weakened or eliminated by US military force, the way will only be opened for the horrific Iranian, Syrian, and quite possibly Turkish states to become more powerful in the region.  None of these are good choices, any more than the Iraqi regime was a good choice as a balance against the Iranian state.  But this in no way justifies Iraqi aggression:  it only shows that no really good choice can be made between the various states involved.  Even if the Iraqi regime stands down and war is averted, the repressive and degrading conditions for the majority of people in the region remain, as does the danger for future conflict.  For those of us who care about human dignity and freedom there are no good state solutions.

From now on we must refuse to choose the supposedly lesser evils of any states whatever.  This is the only honest and realistic position we can take.  All states are the enemies of individual and social possibilities.  No matter their claims, all have set the framework for some form of exploitation and domination of the majority by a minority.  We have nothing to hope for from any state solution of the problems in the Middle East or anywhere else.  We must take a stand against all the past, present and future atrocities committed by all power wielders, a stand against all power as a poison to real social possibilities.

The only good choice is a human social solution, based on insurgent self-activity, not on any kind of statist political or military action.  We can only support anti-hierarchical self-liberation from below.  In the Middle East, as everywhere else, we must ally ourselves with all those who aspire to end all exploitation and domination, and want to create a truly social life for all.  The only way that tyranny can be abolished is if those who are sincerely interested in a better life for all are able to defeat oppressors by going beyond the deadly ideologies of nationalism, religious, racial and ethnic bigotry, by going beyond the market and wage labor; beyond domination by any elite; and by going beyond dependency on a way of life based on the use of petroleum and other life-threatening energy sources.  This is the only fight worth fighting, and the only one that, in the long run, can bring freedom for all.

CHARLATAN STEW P.O. BOX 17138 SEATTLE WA 98107 U.S.A.

 




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

