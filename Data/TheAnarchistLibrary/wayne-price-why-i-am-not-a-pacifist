      Revolution, Violence, and Nonviolence      Nonviolence Does Not Always Work      Some Struggles Have to be Fought Through
While absolute pacifists are a small minority in the general population, they are a large proportion of anarchists. Pacifists are completely against war or any type of mass violence under any circumstances, even in defense from military invasion or to make a democratic revolution. Naturally many pacifists are also anarchists — being against armies, they also oppose the police. It has been said jokingly (with what truth I do not know) that during retreats of the pacifist War Resisters League, softball games are played between the anarchists and the Socialist Party members.

When I first became an anarchist, it was of the anarchist-pacifist tendency. I admired the pacifist Paul Goodman, who was perhaps the most influential anarchist of the sixties. I also admired leading radical pacifists, such as the great A.J. Muste, David Dellinger, David McReynolds, and Bayard Rustin. These people combined pacifism with a radical, even revolutionary, critique of capitalism and the war-waging state. I studied Gandhi, who was no anarchist (he led a movement for a national state for India) but was a decentralist.

It should not be surprising that many good radicals are attracted to pacifism and its nonviolent program. The history of war-making has come to its climax in the potential for nuclear war. Humanity has to find a way to end war, if it is to survive. The history of violent revolutions has produced gains, but still leaves humanity with societies ruled by minorities which exploit the workers and wage wars of extermination. “Terrorist” tactics of violence by small groups of would-be revolutionary heros have had little result except to let the state increase repression.

But eventually I was persuaded that pacifism (and the version of anarchism which went with it) was not sufficient to make the revolution which was needed — but I respect those who believe in it. I do not share the views of Ward Churchill (1998, Pacifism as Pathology, Winnipeg: Arbeiter Ring) that a political belief in pacifism is a mental illness.

Rejecting pacifism does not mean that I am “for” violence. Personally I hate violence, like most sane people. But like 99.999...% of humanity, I believe that sometimes violence is justified, particularly in defense against the violence of others. I believe that there are two basic programmatic weaknesses in pacifism: nonviolence does not always work and some conflicts are irreconcilable.

Pacifists argue that if negotiations fail, it is possible to use techniques of mass nonviolence. This includes strikes, boycotts, sit-ins, pickets, demonstrations, and other forms of civil disobedience. In mass nonviolence, the activists permit themselves to be arrested or beaten by the police or army, but do not fight back in any way. “If blood be shed, let it be our blood.” Presumably this leads to winning over the opponent, to reaching out to the good that is within them. Less emphasized is that this includes a certain use of power: boycotts and strikes cause financial loss to businesspeople and pressure them to do what they do not want to do, to make a deal with the demonstrators. Similarly, brutality against peaceful demonstrators, if widely reported, can appeal to decent people elsewhere, embarrassing the government, and causing outside forces to put pressure on local powers to let up (when the local cops or vigilantes would just as soon massacre the people).

These techniques work part of the time. The problem is that they do not work all the time. Pacifists do not say, Let us consider how to use nonviolent tactics when we can, or as much as possible. Pacifists say, Only nonviolent tactics should be used. Violent self-defense should never be used. To refute pacifism it is not necessary to show that nonviolence never works, just to show that it does not work all the time, that sometimes armed struggle is necessary.

Nonviolent tactics will fail when faced with an absolutely ruthless enemy. Gandhi suggested that the Jews should have used nonviolence against the Nazis. This would have been pointless. The Holocaust could have only been prevented by a workers’ revolution in Germany. Instead, it was finally ended through the Allied military victory. Similarly, a Nazi occupation of India — or a Japanese invasion, which could have happened — would have killed Gandhi and the membership of the Congress Party. Also, successful nonviolent methods require publicity, so the rest of the world knows about it and can put pressure on the oppressors. The Nazis or Imperial Japanese would not have let nonviolent campaigns be reported. Gandhi and Nehru would have vanished without the world’s knowledge. The same can be said of nonviolence methods when used against other ruthless and secretive regimes.

The two most famous nonviolent campaigns are the independence struggle in India and the civil rights movement of African-Americans. In India, the movement succeeded due to the weakness of the British imperialists. In the past, they had been willing to simply massacre the Indians, as they did with the Amritsar massacre (shown in the movie “Gandhi”). But they were being replaced by the U.S. (and the Soviet Union) as the world’s greatest imperialists. They no longer had the power or wealth to hold down India. The Japanese army softened them up in World War II. Had they repressed Gandhi’s movement, they knew they would have faced an armed struggle instead (after all, the Chinese revolution was happening next door). Finally, they knew that the issue was not all-or-nothing for British capitalism; after independence they had more investments in India than before.

Nonviolence worked in the African-American civil rights struggle because the South was part of the larger U.S. The national capitalists, while not supporters of Black people, had no essential need for Southern racial segregation. National politicians were embarrassed internationally as they competed with the Communists. Internationally and domestically their pretense of “democracy” and “freedom” were being given the lie. So they put pressure on the Southern racists to clean up their act and end overt Jim Crow. African-Americans remained on the bottom of U.S. society but were freed from legal segregation.

But if the Southern racists had been left to themselves, uncontrolled by national forces, they would have drowned the nonviolent movement in blood.

Nonviolence was always limited. Nonviolent demonstrators were often protected at night by local Black people patrolling their neighborhoods with rifles. As mentioned, boycotts and strikes were also means of coercion against the local power structure, not just means of appealing to their consciences. Efforts to use courts and to get laws passed are only seen as nonviolent because we are taught to ignore the violence of the state. Actually, court rulings for integration and laws against discrimination only work if they are backed by the armed power of the state. This became clear when the federal government had to call up the National Guard to integrate colleges and schools.

A test case came in South Africa after World War II. As parts of Africa won independence, the Afrikaners imposed a system of apartheid on South African Blacks. The Blacks organized a mass nonviolent movement. The apartheid regime brutally repressed the movement, shooting down demonstrators in cold blood at Sharpesville and elsewhere. The movement was disorganized and driven underground. Nelson Mandela and others had to give up nonviolence in favor of armed struggle. The system lasted for decades more, until economic weakness, combined with a violent rebellion forced the rulers to give up apartheid (although they kept the capitalist system under which Black workers remain oppressed and exploited). South Africa demonstrated that a ruthless enough power structure can defeat nonviolent methods.

Some social conflicts are simply irreconcilable. The two sides cannot come to an agreement. The enemy cannot be won over, except as isolated individuals here and there.

In India and the U.S. South, there were political changes but capitalism was not challenged. This was even true of South Africa. It was also true of the changes in Eastern Europe and the former Soviet Union. The rich mostly kept their wealth and power (Communist bureaucrats became private capitalists). They were willing, when it was necessary, to make changes which did not take away their control and ownership of the economy.

A socialist revolution would be quite different. The workers would take away the total wealth, power, and position of the ruling class. The capitalist class has educated itself that it stands for God and civilization. It believes it stands for law and order, against chaos and barbarism. It will not permit itself to be easily overturned. It will fight with the fiercest of barbaric brutality. Right now the U.S. ruling class supports dictatorships all over the world and wages cruel warfare against the people of several countries. It would not do less inside North America if it felt it was necessary. Like the rise of German Nazism or of Pinochet’s coup in Chile, the capitalist class is capable of overturning even its limited democracy and replacing it with the most horrific repression. We must not underestimate the vileness of the capitalist class of the big imperial states.

Such repression cannot be avoided by any attempt at humanistic or Christian reconciliation. I do not advocate any sort of premature or minority violence. But eventually there will be a confrontation between the working people and oppressed and the capitalists and their hangers-on and agents.

In my country, the United States of America (and in similar countries), I foresee one of two outcomes for a revolution. One is that a revolution may be a particularly bloody conflict, a vicious civil war. After all, the U.S. has a large middle class and a well-off layer of workers, with traditions of patriotism, religious superstition, racism and sexism, as well as the already-mentioned reactionary ruling class. Such forces may oppose a working class rebellion to the bitter end. It may be necessary for U.S. rebels to bring in a revolutionary army from Mexico.

On the other hand, it is possible that a U.S. revolution could be fairly peaceful and almost nonviolent. Unlike many other countries, the big majority of U.S. people are working class (perhaps 80%). Most of the military ranks are from the working class. Unity among the workers, as well as other oppressed groups, could prevent much violence. Especially if revolutions have been successful in other countries, the ruling class and its agents could be demoralized and easier to overthrow.

But even in the preferred case, violence will be kept to a minimum precisely if we are prepared, organized, and unified. The more prepared our class is to defend itself, the more likely the enemy is to be demoralized and to give up easily. And if an armed conflict becomes inevitable, as per the first possibility, then obviously it will be better to have been prepared. So either way, it is better for workers and the oppressed not to have illusions in the peaceful nature of the capitalist enemy.

Revolutions always use elements of what is otherwise regarded as “nonviolence.” Revolutionary struggles often include strikes and other mass actions which are often unarmed, at least at first. Also, revolutions always try to win over the troops on the other side (and no future revolution will succeed without winning over the troops of the empire’s army), as well as to raise the morale of the troops in any revolutionary army. Revolutions seek to win over the population behind the troops on the counterrevolutionary side as well as to encourage the population on the revolutionary side. Revolutions try to demoralize the core of hardened counterrevolutionary forces. These effects are done by propaganda but more than that, by politics. Revolutionaries raised demands for land, freedom, an end to poverty and oppression, and peace, and implement these ideas in whatever territory they control.

Strikes, propaganda, and political moves are all part of any revolutionary struggle — but they are not enough. For example, troops will not lightly come over to the workers’ side. After all, it is a very serious matter for soldiers to disobey their officers — they can be shot. Rebellious troops must believe that the people are prepared to go all the way, to protect them through a successful revolution. Nonviolent methods may be used, but are not sufficient.

We anarchists want a world without war or any sort of violence. But to get it, there will have to be a social revolution to completely change society, overturning the ruling class and its state. We will try to keep revolutionary violence to a minimum, but the vicious, brutal, nature of the capitalist class will require at least the threat of mass violence.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

