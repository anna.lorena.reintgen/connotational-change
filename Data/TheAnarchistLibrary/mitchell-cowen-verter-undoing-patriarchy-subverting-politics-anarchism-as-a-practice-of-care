    Subverting the Military Logic of Politics    Communality as the Nurturance of Needs    Maternal Nurturance as a Model for Care    Anarchism as a Programme of Nurturance    Conclusion    References
At its roots, anarchism is already deeply feminist. When we consult Greek literature, we learn that the term ‘anarchy’ was first used in the active, anti-political sense to describe the behaviour of Antigone, a re/sister who rose up against her uncle Creon [1] to rebel against the military logic of fraternity and fratricide, a logic which divides humanity into friends who are loyal to the state and enemies who betray it. Denounced as an anarchist in both Aeschylus’s (1991) and Sophocles’ (1994) accounts of her tragedy, [2] Antigone opposes this antagonistic logic in the name of a more ethical mode of human interconnection, one that affirms that we must unconditionally nurture each other, even beyond the moment of death.

The link between feminism and anarchism has similarly been noted in the writings of more recent anarcha-feminists, as in Lynne Farrow’s 1974 proclamation that ‘Feminism practices what Anarchism preaches. One might go as far as to claim feminists are the only existing protest groups that can honestly be called practicing Anarchists’ (2002). Taking these examples as our inspiration, we will endeavour to recollect this deep connection between the project of anarchism and the feminist critique of patriarchy.

Before we consider the importance of the feminist critique, let us begin by asking what are the aims and aspirations of anarchism. Anarchism is sometimes defined simply as the refusal of the state. However, anarchism must strive towards a much more profound goal than this. The long history of authoritarian domination has penetrated our ways of thinking and acting so deeply that an anarchist critique must re-evaluate the very roots of political philosophy, of the thinking that considers communality in terms of political association. The feminist scholar Nancy Hartsock argues that Western political thinking has been shaped by the way that the Greek polis (city-state) emerged out of what she terms the ‘barracks community’ (Hartsock, 1982, p. 283). Within this military encampment, the paradigmatic virtues were defined as courage, heroism, glory and the striving for immortality; human relationships were conceived as being fundamentally antagonistic and competitive, as struggles for power and domination. Hartsock claims that war and the masculine role of the warrior-hero have been central to our conception of politics ever since: for example, the warrior’s dominance on the physical battlefield has been transformed into the citizen’s dominance on the battlefield of rhetoric and into the businessman’s dominance on the field of commerce (Hartsock, 1982, pp. 285–6).

The logic of militarism prevails not only within mainstream conceptions of politics, but also within various strands of radical thinking, from Marx’s belief that class struggle is the engine of history to Badiou’s celebration of the militant as a model for political subjectivity. Most alarming is the way that such militarism runs throughout the writings of the French Tiqqun group. Within these texts, we find the standard masculinist warnings against the way one is ‘castrated’ (Tiqqun, 2010b, p. 18) by mass society, as well as a hostile denunciation of the figure of the ‘Young Girl’, who represents for them the shallow bitch who succumbs to the idiocy of consumer culture. Worse yet are the recurrent calls to violence. Not only does it cite Clastres’s proclamation that ‘war is the truth of relations between communities’ (Tiqqun, 2010a, p. 22), but the text Introduction to Civil War also tells us, ‘Only the timid atom of imperial society thinks of “violence” as a radical and unique evil. ... For us, ultimately, violence is what has been taken from us, and today we need to take it back’ (Tiqqun, 2010a, p. 10). We are similarly informed that hostility is a primordial relationship and that the ‘hostis is a nothing that demands to be annihilate’ (2010a, p. 12).

Clearly, some sort of revolution will be necessary to disarm the elite who oppress and immiserate the mass of humanity by their maintenance of power, property and violence. However, it is even more important to recover models of subjectivity and sociality that do not follow this military model. Is the political conception of human sociality sufficient to describe our relationships with each other, and more importantly, should it be the basis for imagining our anarchist future? Part of the problem is that we tend to think of human sociality as being already saturated by politics; that we subscribe either to the Hegelian vision that the state is the transcendental sphere which sustains all other particular affiliations, or to the Foucaultian vision of an immanent micropolitics that determines and disciplines all intimate relationships. But is human existence really so dominated by the political? By submitting to this domination, do we not already foreclose the exploration of other forms of human sociality?

It is important to note that, even in one of the foundational texts of political philosophy, there are already clues that point towards other possibilities. In the first lines of his treatise on Politics, Aristotle declares that the polis (city-state) is only one among several kinds of human koinon (community), another of which is the oikos (home) (1996, 1251b1ff ). Unfortunately, however, the analysis he offers of the home is the prototypically patriarchal one, a definition that has been influential throughout Western history. Not only does Aristotle claim that the home is encompassed within the state, but he also intersects these two communal spheres in the figure of the patriarch, who establishes his dominion over the domestic sphere through a process of domination and domestication, and who establishes the science of oikonomos (economy) to order the household and to acquire property (1996, 1253b1ff). However, Aristotle supplements this patriarchal analysis of the household with a secondary definition. The household is not simply the locus of domination, but more fundamentally, it is the association where people come together to attend to their everyday needs and wants, the communal space where people become companions through the activity of eating together (Aristotle, 1996, 1252b12–20).

How are these two definitions of the home, as the sphere of domination and as the sphere of need, related to each other? Hannah Arendt collapses these two definitions, arguing that the vulnerability of bodily need is our primordial experience of being dominated, and that our need to dominate the needs that dominate us is the reason why we need to elaborate structures of hierarchical political domination (1998, p. 31). Arendt’s correlation of dependency with domination is one of the most problematic and most characteristic aspects of patriarchal thinking. This repudiation of dependency becomes so absurd that many patriarchal thinkers even conceal the fact that we are born unto women. Thomas Hobbes, for example, inaugurates the modern conception of citizenship by comparing political subjects with mushrooms that spring out of the ground fully formed, emerging as isolated individuals who can freely establish contracts and submit to rulers (1998, p. 205).

Are there not different ways of understanding the vulnerability experienced in individual and social life? Can we not embrace a non-patriarchal vision of the home as a site for the enactment of responsibility for the needs of ourselves and other people, as a place for caring, refuge and hospitality; as a model for empathetic sociality? Could the affirmation of such social nurturance not subvert the hierarchical and antagonistic logic of the political?

In order to consider these alternatives, it is useful to return to the critical interventions of feminist ethics. These analyses are interesting not because they posit an essence of ‘the feminine’, but rather because they have engendered a fecund critique of the patriarchal system. According to Nancy Hartsock, the dominant powers in any society promulgate an understanding of social relations that is both partial and perverse, an ideology that not only perpetuates social inequity but also denies the true nature of social life (1983, pp. 287–8). Hartsock argues that a feminist standpoint arises not out of any essential female difference, but rather because women have tended to occupy social roles involving material sustenance and support, and this proximity to materiality allows them to understand how much our everyday lives depend upon the satisfaction of material needs (1983, pp. 291ff). Influenced by Hartsock, Sara Ruddick explains that:

care workers depend on a practical knowledge of the qualities of the material world, including the human bodily world, in which they deal. This means that the material world, seen under the aspect of caring labor, is organized in terms of people’s needs and pleasures and, by extension, of the needs and pleasures of any animal or plant that is instrumental in human caring or is tended for its own sake. (Ruddick, 2006, p. 130)

Ruddick’s research into caring labour is carried out though an investigation of maternal thinking. She defines maternal thinking as the pattern of cognition that emerges out of the practice of mothering, a set of attunements women acquire as they become responsible for meeting the demands of a child. The child confronts the mother as a completely helpless being, one who is totally dependent upon her for the satisfaction of all its most basic needs. The activity of mothering requires that the mother – who according to Ruddick can be of any gender (2006, pp. 40ff) – respond to the material reality of this human vulnerability in a way that does not enforce power relations. Even the weakest mother can overwhelm the fragility of a dependent child, but domination is not what defines the activity of mothering. Instead of establishing rigid control over the child, Ruddick recommends what she terms ‘holding’ as the best way to preserve its fragility, a practice that maintains safety of the child, promotes its strength, and allows it to flourish without establishing any ownership over it (2006, pp. 78–9). In addition to reconsidering the importance of maternal care, other feminist thinkers have similarly re-evaluated the category of domestic nurturance. For example, bell hooks discusses how black women maintained what she calls ‘home place’ as a site of resistance against rampant racism, a refuge where people could gather and heal themselves from the wounds inflicted by a hostile society (2001, pp. 41–9).

Many contemporary anarcha-feminists avoid themes such as mothering and the household out of a desire to avoid gender essentialism. However, we do read anarcha-feminist authors arguing that traditionally feminine concerns such as domestic labour, sexuality and child rearing should not be treated as mere addendums to the anarchist project, but must be central to our vision of an anarchist society. This attitude was expressed most strongly in an early essay by Roxanne Dunbar in which she argued:

If the maternal traits conditioned into women are desirable traits, they are desirable for everyone, not just women. By destroying the present society and building a society on feminist principles, men will be forced to live in the human community on terms very different from the present. For that to happen, feminism must be asserted by women as the basis of revolutionary social change. (Dunbar, 1970, p. 499)

I would argue that the importance of maternal nurturance is at the very core of the anarchocommunist project articulated by Peter Kropotkin and others. Errico Malatesta once attested to this aspect of Kropotkin’s personality, remarking:

I remember what [Kropotkin] did in Geneva in the winter of 1879 to help a group of Italian refugees in dire straits, among them myself; I remember the small attentions, I would call maternal, which he bestowed on me when one night in London having been the victim of an accident I went and knocked on his door; I recall the innumerable kind actions towards all sorts of people. (Malatesta, 1965, p. 258)

Beyond this anecdotal evidence, when we read Kropotkin’s work, we should be reminded of how much his thinking resonates with Sara Ruddick’s characterization of care work as the organization of the material world in terms of people’s needs and desires.

The best way to understand Kropotkin’s thinking is to see how much it embraces human vulnerability and how much it insists upon the paramount virtue of nurturance. In his writings on mutual aid, Kropotkin repeatedly articulates the value of human dependency. For example, he characterizes solidarity as ‘the unconscious recognition of the force that is borrowed by each man from the practice of mutual aid; of the close dependency of everyone’s happiness upon the happiness of all’ (1902, pp. xliii–xliv). Kropotkin’s notion of dependency seems remarkable in several ways. First, he does not equate the notion of dependency with that of domination. Second, his notion of mutual aid emphasizes human dependency more than it does human capacity. That is, he concentrates not on the fact that people possess powers that they can contribute to the common good, but rather that each of us depends radically on the sustenance granted by infinite others. Third, his idea of dependency should not be reduced to the reciprocity of interdependence. While it may be true from an outside perspective that all of our social contributions balance each other out, what is important is that, from my own perspective, I realize how indebted I am to the rest of humanity.

Kropotkin’s interpretation of human dependence provides the basis for his critique of property ownership. People depend so utterly upon what other people have already contributed that they never have a foundation to claim anything as their own. Private property is unjust not simply because it fails to recognize the worker’s agency as a producer, but rather because it neglects to accept our infinite dependency as consumers. For this reason, Kropotkin declares:

All things are for all men, since all men have need of them, since all men have worked in the measure of their strength to produce them, and since it is not possible to evaluate every one’s part in the production of the world’s wealth. That each and every person has a right to well being; there is a right to well being for all. (Kropotkin, 1995, p. 19)

According to Kropotkin, the problem of satisfying needs is the most essential problem of all revolutionary problems, and the question of how we nurture each other is the most important of all revolutionary questions.

The analysis of human needs also provides the basis for Kropotkin’s critique of capitalism and the state. First, Kropotkin argues that capitalism, supported by the state, reorients material life such that it caters to the needs of the rich: rather than providing well-being for all of humanity, production becomes focused on producing luxury items for the wealthy – and by extension, for wealthy countries like our own. Second, he explains that the wealth of the wealthy ultimately derives from the poverty of the poor. Only because people are allowed to suffer such profound material destitution can the capitalist compel them to become labourers, paying them a meagre wage that allows them barely to subsist. Third, one of the alibis that the state employs to justify its existence is its monopoly over the activity of care. The state eliminates autonomous institutions of mutual aid, replacing them with various forms of charity, welfare and health care. While any form of care is significant and should be defended, the care function of the state allows it to mask the fact that the state exists as the institution that facilitates the domination of the rich and powerful and abets the immiseration of the poor and subjugated.

Despite the efforts of the state to monopolize caring, anarchists have persevered in the effort to create a society based on mutual aid. The revolutionary significance of an anarchism based on nurturance can be observed in both institutional and spontaneous settings. Anarchist groups such as Food Not Bombs organize to feed the hungry; the Really Really Free Market organizes to provide a space for free exchange of goods; the Icarus Project organizes to help people with psychological difficulties to give each other support and therapy; various squatting initiatives help people to find shelter.

In addition, we have seen examples of spontaneous anarchist nurturance throughout the uprisings in North Africa. What seems remarkable about these revolutions is not just that people rose up en masse to overthrow their leaders, but also the way that they supported each other throughout. Protesters in Tahrir Square, for example, managed to keep each other fed, tended to each other’s bodily needs, and endeavoured to keep each other safe. Reporting from Egypt, Mohammed Bamyeh marvelled at how the occupants of the square:

established autonomous field hospitals to treat the injured; formed street committees to maintain security and hygiene. I saw peasant women giving protestors onions to help them recover from teargas attacks .... and countless other incidents of generous civility amidst the prevailing destruction and chaos. ... During the ensuing week and a half, millions converged on the streets almost everywhere in Egypt, and one could empirically see how noble ethics – community and solidarity, care for others, respect for the dignity of all, feeling of personal responsibility for everyone – emerge precisely out of the disappearance of government. (Bamyeh, 2011)

A similar gesture of solidarity was practised by Tunisians who, having consummated their uprising, welcomed refugees from the Libyan conflict with food and shelter. The New York Times quotes Abdallah Awaye explaining, ‘This is how it is, these are our customs. If there is something to eat, we will eat it together. If there is nothing to eat, we will have nothing together’ (Sayare, 2011). Such behaviour constitutes the very greatest example of ethical anarchism.

In conclusion, let me reiterate some of my basic points and then comment briefly about what this might say regarding our current state of anarchist thinking. First of all, rereading feminist critiques can help us to remember what has been suppressed by centuries of patriarchal thought: namely, that the nurturance of material needs is more fundamental than the establishment of control. Anarchist thought should focus more on how to nurture and sustain each other. Furthermore, this is precisely what many anarchist initiatives have practised and continue to practise.

Not only should anarchists focus on promoting human well- being, we should be more careful about using the same military logic promoted by the patriarchal state. As stated in my introduction, there is something really terrifying about not only the violence but also the sectarianism promoted by the Tiqqun group. Their various texts urge us to ‘find each other’ (Invisible Committee, 2009, p. 65). While human solidarity is always a worthwhile goal, the Invisible Committee’s notion of communal organization sounds combative to the point of paranoia. The positive task of human affiliation is shadowed by an intense antipathy towards others who are not ‘worthy’ of being part of the commune (Invisible Committee, 2009, p. 66). The group proclaim:

To the citizens of Empire, we have nothing to say. That would mean we shared something in common. As far as they are concerned, the choice is clear: either desert, join us and throw yourself into becoming; or stay where you are and be dealt with in accordance with the well-known principles of hostility: reduction and abasement. (Tiqqun, 2010a, p. 39)

The destructiveness of such a statement is idiotic and reprehensible. In New York and elsewhere, we have seen how this type of sectarian rhetoric has produced very real violence within the anarchist milieu. Once we understand that the goal of anarchism is human nurturance, we should reconsider the priority of our anarchist mission to be not to ‘find each other’ but rather to ‘feed each other’. Once we understand caring as a core principle of anarchism, we will endeavour not to establish coming communities of like-minded friends who are bound together by a common political spirit, but instead to create the ‘coming community’ as ‘the community of those who have nothing common’ (Lingis, 1994), one that will spread well-being to all.

Aeschylus (1991) Aeschylus II, ed. D. Grene and R. Lattimore. Chicago, Ill.: University of Chicago Press.

Arendt, H. (1998) The Human Condition. Chicago, Ill.: University of Chicago Press.

Aristotle (1996) Politics, ed. H. Rackham. Cambridge, Mass.: Harvard University Press.

Bamyeh, M. (2011) ‘The Egyptian Revolution: first impressions from the field’, Jadaliyya. Accessed July 15, 2011, www.jadaliyya.com/pages/ index/561/the-egyptian-revolution_first-impressions-from-the-field-

Dunbar, R. (1970) ‘Female liberation as the basis for social revolution’, in R. Morgan (ed.), Sisterhood is Powerful. New York: Random House.

Farrow, L. (2002) ‘Feminism as anarchism’, in Dark Star Collective (ed.), Quiet Rumors. Oakland, Calif.: AK Press.

Hartsock, N. (1982) ‘The barracks community in western political thought: prolegomena to a feminist critique of war and politics’, Women’s Studies International Forum, 5:3/4.

Hartsock, N. (1983). ‘The feminist standpoint’, in S. Harding and M. B. Hintikka (eds), Discovering Reality. Boston, Mass.: D. Riedel.

Hobbes, Thomas (1998) Man and Citizen, ed. B. Gert. Indianapolis, Ind.: Hackett.

hooks, b. (2001) ‘Homeplace: a site of resistance’, in Yearning. Boston, Mass.: South End Press.

Invisible Committee(2009) The Coming Insurrection. New York: Semiotext.

Kropotkin, P. (1902) Mutual Aid as a Factor in Evolution. London: Heinemann.

Kropotkin, P. (1995) The Conquest of Bread and Other Writings, ed. Marshall S. Shatz. Cambridge: Cambridge University Press.

Lingis, A. (1994) The Community of Those Who Have Nothing in Common. Bloomington, Ind.: Indiana University Press.

Malatesta, E. (1965) ‘Peter Kropotkin: recollections and criticisms of an old friend’, in V. Richards (ed.), Errico Malatesta: His life and ideas. London: Freedom Press.

Ruddick, S. (2006) Maternal Thinking: Towards a politics of peace. Boston, Mass.: Beacon Press.

Sayare, S. (2011) ‘Thousands fleeing Qaddafi bask in Tunisia’s hospitality’, New York Times, April 28, 2011.

Sophocles (1994) Antigone, ed. H. Lloyd-Jones. Cambridge, Mass.: Harvard University Press.

Tiqqun (2010a) Introduction to Civil War, trans. J. E. Smith and A. Galloway. Cambridge, Mass.: MIT Press.

Tiqqun (2010b) Theory of the Young Girl, Zine Library. Accessed May 1 2011, http://zinelibrary.info/files/jeune-fille.pdf
[1] The Greek κρε′ων translates literally as ‘ruler’.
[2] In Seven Against Thebes, Antigone declares that she is ‘not ashamed to act in anarchist opposition to the rulers of the city’ (oud’ aischunomai echous’ apiston tênd’ anarchian polei) (1035–6). In Antigone, Creon condemns her, asserting that ‘there is no evil worse than anarchy’ (anarchias de meizon ouk estin kakon) (673).




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

