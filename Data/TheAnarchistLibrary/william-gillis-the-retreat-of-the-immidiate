
Anarchists who intend to act as though we didn’t live in a dystopic world must find themselves perplexed at every moment. With the ecosystems of civil society so atrophied and virtually every surviving institution of value captured and beaten into participating in the bloody circus of statism, who do you call? What do you do when you see a thug with a gun (and a badge) looming over someone, much less kidnapping or shooting people? Hell, how do you deal with the existence of sitcoms?

Ethically navigating the horrors of our world is a challenging task for anyone with a shred of humanity, but it’s unfathomable once you abandon the notion of strategy – the pursuit of wider context.

And yet the appeal of immediatism has grown widely in recent years, under various banners and in many circles. Perhaps this is a reaction to the patently ludicrous Plans of social democrats, state communists, vulgar libertarians and organizationalist ideologues–in such light it’s clearly a sympathetic instinct. But it is also a surrender of the mind and heart.

Immediatism, in almost every formulation, has two sides.

On one side is Rothbard’s famous “big button” that we might break our fingers pushing to suddenly poof away the militaries, courts, politicians and police of the world – come what may in the aftermath. I waft back and forth on this hypothetical. It’s certainly rhetorically convenient for emphasizing the scale of state atrocities being committed right now, but unconvincing to anyone versed in the wilds of sociopaths, thugs and would-be-DMV-administrators that currently infest our world. The state is but the apex predator in a rich ecosystem of would-be states. As anarchists our goal to abolish power relations doesn’t stop at merely the most prominent ones. And fractured civil war between would-be warlords and social democrats can be many times more destructive and oppressive than the off-hand tyranny of old, fat and senile sociopaths.

We anarchists are objectively right, centralization is inefficient. But this cuts two ways. Rwanda’s machetes were more efficient than Hitler’s gas chambers. Robust markets will efficiently deliver death just as much as any other “good” a certain culture might value. Meanwhile the wicked truth is libertarians often flourish in overextended empires where the mountains are high and the emperor far away. At one time I used to retort that if I could push a button and create a single incredibly centralized global government I would. Better to have a single enemy to focus on than two hundred, interlocking, redundant and locally attentive ones.

States create game theoretic environments around their peripheries that suppress cooperation and reward antisocial strategies. Primordial empires wouldn’t have persisted if they didn’t constantly sow the seeds of future cops, rulers, and bureaucrats through the cultural and economic norms they instilled. But not every bully can grow up to be picked as Head Genocidaire and the landscape is littered with the failures. Some are too stupid to make it, some confined to small-time crime, some in miniature statelettes like the mob running in parallel to their more official brothers, some seeing greater advantage in milking hidden privileges from the current state, and some simply unlucky. Many more, despite being distorted and corrupted by their environment, are too humane to function well in the gears of state power. They nevertheless instinctively support the stability of any known social form and lash out at deviation, thoroughly persuaded that cooperation is impossible on any significant level and our only hope is to eek out a living as moss on a wall without attracting the wrath of whatever sociopaths are in power.

If we were to press that magical button these residual forces, endemic across our society, would immediately begin the reconstitution of states. There would be serious opportunity for sustained development of more ideal communities (as we have seen in virtually every crisis), but so too, in the absence of vigorous preexisting social antibodies to power accumulation, would there be terror and micro-totalitarianisms. Not universally, but all too often even a small presence leads to widespread PTSD, a willingness to grasp any known “solution” however imperfect rather than spend the time and iterations of trial-and-error necessary to win categorical improvements. The most staunch conservatives and proponents of totalitarianism I’ve met have been survivors of civil wars. Only when there are anarchist community centers in every neighborhood, self-defense cooperatives, arbitration bodies, autonomous basic-needs infrastructure, widespread awareness of alternative justice systems, et alia, would pushing that button actually be a surefire reduction in state violence.

Of course I don’t fault anyone for lusting after that button, I even tend to lean towards pushing it in my read of the weighted probabilities, but A) the button is very much just an unrealistic thought experiment, and B) focusing on the dichotomy it frames things in is incredibly strategically unhealthy. We don’t win the moment a state ceases to exist, much less all two hundred or so officially registered “states.” To even speak of anything approaching a win condition for us we must damn well consider the default strategies and frameworks ossified in a number of people’s heads. While the decline and fall of existing states will be an amazing battle to win, it is not the war. We win by turning the tide against power psychosis, not certain symptoms. And that, sadly, is an inherently gradual thing without clear markers.

But then we’re anarchists: Our decentralized and autonomous asses flourish in situations involving vastly complicated contexts unknowable to a single actor or reducible to simple terms! Which brings me to my second point.

The other side of immediatism is the adoption of limited ethics, whether deontological or nihilistic. Pretending we live today in the world we’d like to see (or dismissing any ideal or goal as hopeless) explicitly involves ditching context.

The world is not a simple place and simplistic abstractions (even in the form of “shit’s too complicated” or “we’re sure to lose”) do violence through irresponsibility. Further they signal a cognitive surrender to the ossified and sweeping logic of the state.

Rather than delve after the true comprehensive roots of a dynamic and risk being reshaped in the process, the rigid algorithms that make up the psychosis of power try to impose simplified and relatively unchanging macroscopic abstractions. To think, to reformulate with greater context, is to risk deviation from the game theoretic dynamics that preserve simplicity. The drive for control is the drive to reduce the amount of thinking one has to do–often by force. The state requires this strategic rigidity and simplicity in its components so that they might be A) calculable and B) stable in the weird niche of game theoretic phase space it survives in. While the state embraces limited attempts at foresight, explorations in meta-strategy and awareness are always, by necessity, confined.

Conscious intentional actors are the state’s worst nightmare. The mere pressure of oppositional tactics alone is easily integrated into state calculations, even reformulated as a vital organ. If every sharp grievance is turned into a mindless rupture, then the number of burnt cars this week becomes just another focus group report. They have storefronts and cops aplenty to sacrifice. Sure, despite our best efforts they might miscalculate still, and our endless siege rush through the cracks to some meaningful accomplishment/destruction, but there’s no good reason to settle for this minimal effectiveness. Like the old post-left slogan, “an insurrection of generals not an army of soldiers” actively thinking through strategies of attack and exploitation individually is the only way to leverage the state’s calculational constraints. What does our embrace of agency as anarchists even mean, if in our resistance we gravitate towards any form of attack in front of us or stirs our first impulse? If all your resistance can be easily replaced with a lego mindstorms robot, identifying cops via python script and chucking firebombs at them, it stands to reason you might be at least a little bit more effective at building such robots. And if you’re willing to take one step of foresight in the causal pursuit of our desires, why not more?

Barely better than chucking our bodies at their nightsticks or shooting the first thug with a badge we see kidnapping people is the sort of internalized legalism that tries to slice up the world in terms of immediately visible violence. We see this most egregiously among certain vulgar anarcho-capitalists who famously can’t tell if something is unethical unless things have gotten to the point where someone is openly pointing a gun. Never mind amorphous culturally implied threats or conversations about the unbelievable subsidy left by historical genocide and slavery. Coercive power and profit from it is a tangled thing and if we throw up our hands at a few steps of removal or the blurring of direct responsibility through convoluted shell games we invite sociopaths to walk all over us. Pick two random people, even two random anarchists, and they’ll give you two very different definitions of “what counts”.

The answer is, of course, that it all counts.

Lines of power, control and implicit coercion crisscross our world; we are all chained up in them to varying degrees. Perhaps it really would be a good thing, if we all started blazing away at our oppressors and the only people left standing to start over were a couple saintly homeless queer disabled black kids. Sometimes, in despair, I think exactly that.

I understand the common impulse to ignore the big picture entirely and attempt to lose oneself in the accounting of proportionality, personal blame and other relatively crisp immediates. But this is suicidally insufficient and to constrain ourselves to such immediate reactions is to become complicit. It is the nature of tangles that they cannot be resolved by merely pushing back. We do not live in a world where violence is a deviation rather than the norm and, thus, easily squashed the moment it rears its head. When a shell game has been going on for centuries, passing balls of coercion between billions, retribution can only get us so far. How responsible is the individual cop that shoots us for resisting an IRS warrant versus the officer who gave the order, or the politician who signed the law, or the friend who snitched, or the teacher whose salary it’ll partially go to? How “responsible” is a white american who’s benefited from centuries of subsidy for the relative immiseration of a decedent of slaves? What of the beneficiaries of economies of scale generated by a transportation infrastructure built on genocide? The framework of blame is the fantasy of quick answers from immediate context. We cannot know the constraints placed on other people, the distorted choices and incomplete information available to them. “Responsibility” much less “proportionality” are profoundly arbitrary in most situations. Focusing on them frequently poses daunting collective action problems as well as issues of representation and, thus, effectively prioritizing some situations of sharp oppression over others.

But all is not lost, we can at least try to minimize oppressive constraining bullshit or, equivalently, maximize agency. This instinct is shared by both those who talk of responses being justified “up to what’s necessary to immediately stop the aggression” and those who instead talk about rehabilitory approaches that “even if they may never end up working all that well all of the time,” will at least avoid escalating to the point of murdering every last person who adamantly refuses to stop some micro-aggression. Both approaches, however, by attempting to write out a simple universal operating method, are too cute and fall into the same statist trap of ossified frameworks rather than active and fully-context-sensitive strategizing.

As anarchists, native to the knowledge problems of subjectivity, we need to embrace knowing when we don’t have the answers. Not knowing the full particulars and context of a comrade struggling on the other side of the world we can at best only helpfully point out glaring contradictions, externalities or potential inefficiencies of one strategy versus another (imprisoning people in gulags, for example, won’t make them freer or lead to the state withering away). Sometimes this means not acting. Shooting a politician might lead to better conditions, it might lead to sharply worse ones. Same with blowing away the first cop you see. Sincere passionate, highly intelligent and considered anarchists will disagree on whether or not to push Rothbard’s hypothetical button this very moment. Merely by virtue of having different life experiences and seeing different spatterings of data on social conditions. On the other hand dramatically increasing the power of the state to fight the corporations historically inseparable from the state, without a viable means of then fighting the resulting super-empowered state (never mind whether it gets the corporations or just increases the potency of regulatory capture), is clearly a strategy developed with limited exploration of ramifications. Continuing to investigate is important.

Merely blindly escalating to the level of retaliation necessary to fend off each and every aggression flowing through the facet of this world would mean a total war of annihilation. Conversely, in many cases, failing to escalate beyond some arbitrary line or apportioning our efforts according to degrees of “responsibility” rather than “what will stop the violence” can leave us in an intractable mess. The solution is to reject the paradigm of escalation entirely, a notion that was only possible by examining interactions in isolation. Reprisal is but one tiny sliver of tactics. When facing an ungodly mesh of knots you don’t push or pull, you examine the whole structure and look for weak points. The question before us, as anarchists, isn’t how hard to bluntly react when our world fails to be perfect but where and how to proactively strike against dystopia.

Sometimes that means letting things pass in silence, sometimes it means sucker punching, and sometimes it means something completely orthogonal.

Some problems can’t be solved directly. Sometimes you have to go around them. This requires seeing the full breadth of our society as it is, not as we’d like it to be. In a world filled with people who feel entitled to control others in a million tiny and not-so-tiny ways, selfcenteredly focusing on wiping the blood off our own hands or trying to pin precise apportionments of blame can only leave us complicit in the blood awash around us. There is no universal formula, no simple heuristic or paint-by-numbers methodology that will get us to a better world. Indeed such shortsighted procedurism flies in the face of virtually every anarchist vision. “Freedom” is a meaningless slogan without vigilance and agency. If “freedom” from proactive consideration is what we were looking for this world already offers many avenues. Indeed that is practically all it embraces.

That anarchists occasionally throw up their hands and retreat to a tiny sphere of immediate considerations – whether embracing blind optimism or blind despair–is entirely understandable given the challenges we face. But such a retreat is not a victory, nor could it ever somehow be representative of liberation.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

