
What is an affinity group?

An affinity group is a small group of 5 to 20 people who work together autonomously on direct actions or other projects. You can form an affinity group with your friends, people from your community, workplace, or organization.

Affinity groups challenge top-down decision-making and organizing, and empower those involved to take creative direct action. Affinity groups allow people to "be" the action they want to see by giving complete freedom and decision-making power to the affinity group. Affinity groups by nature are decentralized and non-hierarchical, two important principles of anarchist organizing and action. The affinity group model was first used by anarchists in Spain in the late 19th and early 20th century, and was re-introduced to radical direct action by anti-nuclear activists during the 1970s, who used decentralized non-violent direct action to blockade roads, occupy spaces and disrupt "business as usual" for the nuclear and war makers of the US. Affinity groups have a long and interesting past, owing much to the anarchists and workers of Spain and the anarchists and radicals today who use affinity groups, non-hierarchical structures, and consensus decision making in direct action and organizing.

Affinity Group Roles [in a protest]

There are many roles that one could possibly fill. These roles include:

Medical - An affinity group may want to have someone who is a trained street medic who can deal with any medical or health issues during the action.

Legal observer - If there are not already legal observers for an action, it may be important to have people not involved in the action taking notes on police conduct and possible violations of activists rights.

Media - If you are doing an action which plans to draw media, a person in the affinity group could be empowered to talk to the media and act as a spokesperson.

Action Elf/Vibes-watcher - This is someone who would help out with the general wellness of the group: water, massages, and encouragement through starting a song or cheer. This is not a role is necessary, but may be particularly helpful in day long actions where people might get tired or irritable as the day wears on.

Traffic - If it is a moving affinity group, it may be necessary to have people who are empowered to stop cars at intersections and in general watch out for the safety of people on the streets from cars and other vehicles.

Arrest-able members - This depends on what kind of direct action you are doing. Some actions may require a certain number of people willing to get arrested, or some parts of an action may need a minimum number of arrest-ables. Either way, it is important to know who is doing the action and plans on getting arrested.

Jail Support - Again, this is only if you have an affinity group who has people getting arrested. This person has all the arrestees contact information and will go to the jail, talk to and work with lawyers, keep track of who got arrested etc.



[Affinity groups are not just useful within a protest or direct action setting, this form of organization can be used for a wide variety of purposes as the history of affinity groups below illustrates.]

History of Affinity Groups

The idea of affinity groups comes out of the anarchist and workers movement that was created in the late 19th century and fought fascism in Spain during the Spanish Civil War. The Spanish Anarchist movement provides an exhilarating example of a movement, and the actual possibility of a society based on decentralized organization, direct democracy and the principles behind them.

Small circles of good friends, called "tertulias" would meet at cafes to discuss ideas and plan actions. In 1888, a period of intense class conflict in Europe and of local insurrection and struggle in Spain, the Anarchist Organization of the Spanish Region made this traditional form (tertulias) the basis of its organization.

Decades later, the Iberian Anarchist Federation, which contained 50,000 activists, organized into affinity groups and confederated into local, regional, and national councils. Wherever several FAI affinity groups existed, they formed a local federation. Local federations were coordinated by committees were made up of one mandated delegate from each affinity group. Mandated delegates were sent from local federations to regional committees and finally to the Peninsular Committee. Affinity groups remained autonomous as they carried out education, organized and supported local struggles. The intimacy of the groups made police infiltration difficult.

The idea of large-scale affinity group based organization was planted in the United States on April 30, 1977 when 2,500 people, organized into affinity groups, occupied the Seabrook, New Hampshire nuclear power plant. The growing anti-nuclear power and disarmament movements adopted this mode, and used it in many successful actions throughout the late 1970s and 1980s. Since then, it has been used by the Central America solidarity movement, lesbian/gay liberation movement, Earth First and earth liberation movement, and many others.

Most recently, affinity groups have been used in the mass actions in Seattle for the WTO and Washington DC for the IMF and World Bank, as well as Philadelphia and Los Angles around the Republican and Democratic National Conventions.

What is a Cluster and a Spokescouncil?

A cluster is a grouping of affinity groups that come together to work on a certain task or part of a larger action. Thus, a cluster might be responsible for blockading an area, organizing one day of a multi-day action, or putting together and performing a mass street theater performance. Clusters could be organized around where affinity groups are from (example: Texas cluster), an issue or identity (examples: student cluster or anti-sweatshop cluster), or action interest (examples: street theater or [black bloc]).

A spokescouncil is the larger organizing structure used in the affinity group model to coordinate a mass action. Each affinity group (or cluster) empowers a spoke (representative) to go to a spokescouncil meeting to decide on important issues for the action. For instance, affinity groups need to decide on a legal/jail strategy, possible tactical issues, meeting places, and many other logistics. A spokescouncil does not take away an individual affinity group's autonomy within an action; affinity groups make there own decisions about what they want to do on the streets.

How to start an affinity group

An affinity group could be a relationship among people that lasts for years among a group of friends and activists, or it could be a week long relationship based around a single action. Either way, it is important to join an affinity group that is best suited to you and your interests.

If you are forming an affinity group in your city or town, find friends or fellow activists who have similar issue interests, and thus would want to go to similar actions. Also, look for people who would be willing to use similar tactics - if you want to do relatively high risk lockdowns, someone who does not want to be in that situation may not want to be in the affinity group. That person could do media or medic work, but it may not be best if they are completely uncomfortable around certain tactics of direct action.

If you are looking to join an affinity group at a mass action, first find out what affinity groups open to new members and which ones are closed. For many people, affinity groups are based on trusting relationships based around years of friendship and work, thus they might not want people they don't know in their affinity group. Once you find which affinity groups are open, look for ones that have an issue interest or action tactic that you are drawn to.

What can an affinity group do?

Anything!!! They can be used for mass or smaller scale actions. Affinity groups can be used to drop a banner, blockade a road, provide back-up for other affinity groups, do street theater, block traffic riding bikes, organize a tree sit, [confront the police, strategic property destruction], change the message on a massive billboard, play music in a radical marching band or sing in a revolutionary choir, etc. There can even be affinity groups who take on certain tasks in an action. For instance, there could be a roving affinity group made up of street medics, or an affinity group who brings food and water to people on the streets.

What makes affinity groups so effective for actions is that they can remain creative and independent and plan out their own action without an organization or person dictating to them what can and can't be done. Thus, there are an endless amount of possibilities for what affinity groups can do. Be creative and remember: direct action gets the goods!




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

