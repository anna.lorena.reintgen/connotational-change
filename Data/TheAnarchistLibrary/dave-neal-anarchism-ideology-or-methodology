      Anarchism: Ideology or Methodology?      Method versus Madness      Anarchism, not Anarchists      Ideology and Human Nature      The Truth Is: There Is No Truth!      Anything Goes?      Deductive and Inductive Anarchists      So, What’s the Point?
“It’s an odd feature of the anarchist tradition over the years that it seems to have often bred highly authoritarian personality types, who legislate what the Doctrine is, and with various degrees of fury (often great) denounce those who depart from what they have declared to be the True Principles. Odd form of anarchism.” — Noam Chomsky

One issue that remains unresolved within the anarchist movement revolves around the nature of anarchists themselves. If you’ve perused these pages, you by now know about social anarchism versus lifestyle anarchism as the most public schism among anarchists, with the latter deriding class struggle as fruitless, pointless, and irrelevant, and the former declaring that the latter aren’t anarchists at all, but are rather bourgeois poseurs.

To the casual browser, it seems a silly, pointless debate. And in many respects, you’re right! The social versus lifestylism debate revolves around the idea of what it means to be an anarchist.

However, underlying this debate is a less obvious thread, namely whether anarchism is an ideology — a set of rules and conventions to which you must abide, or whether anarchism is a methodology — a way of acting, or a historical tendency against illegitimate authority. I believe this debate underlies the social versus lifestylism dilemma, and will attempt to elaborate on it.

I’ll call ideological anarchists Anarchists — big “A” anarchists, and methodological anarchists anarchists — small “a” anarchists, so you know who I’m referring to.

Anarchism clearly means a particular thing. For example, it is defined by the American Heritage Dictionary as:

1. The theory that all forms of government are oppressive and undesirable, and should be abolished; 2. Active resistance and terrorism against the state, as used by some anarchists; 3. Rejection of all forms of coercive control and authority.

So, in this sense, an anarchist is one who finds all forms of government oppressive and undesirable, and rejects all forms of coercive control and authority. A person who doesn’t fit this criterion is no anarchist.

This supports the idea that anarchism is an ideology — a consistent set of ideas based on a core principle. Does that mean, however, that every person who says they’re an anarchist is an anarchist?

Clearly not, which forms the basis for the lifestylism argument, as well as anarchist opposition to the intellectual affront that is “anarcho-”capitalism.

But there’s a difference between ideological objection and methodological opposition. For the Anarchist, they say “X is not anarchism” with the implicit understanding that they know what anarchism is about. For them, there is no need to prove or demonstrate it — their statement alone is fact enough.

To the anarchist, lifestylism and “anarcho-”capitalism are rejected because, methodologically, they aren’t the way to arrive at anarchism. They use the wrong means to achieve similar ends — namely, human happiness.

See the difference in approaches?

The Anarchist stresses ideological conformity as the prerequisite for social revolution — in other words, you swallow A,B, and C doctrines and then you are an Anarchist. Their plan of action revolves around: 1) creating a central Anarchist organization; 2) educating (e.g., indoctrinating) the working class as to the tenets of Anarchism; 3) thereby building a mass movement; 4) creating a social revolution.

The Anarchist is comfortable with the idea of a manifesto, platform, or other guiding doctrine as the means of “spreading the gospel” — their emphasis is unity in thought and action, and ideological conformity as the basis for effective organization.

The anarchist, however, rejects all of this. We hold, instead, that: 1) anarchist organizations cannot be created before the demand for them exists; 2) indoctrinated people are not free people; 3) a movement based on a central authority (e.g., the central Anarchist organization) and on masses of indoctrinated followers will be an elite, political one, not a popular, social one; 4) the social revolution will invariably be betrayed by such an effort, becoming a political revolution whereby the Anarchists seize power.

This is not a semantic difference; rather, it strikes at the heart of the movement itself, and the roots of this debate go back to the founding of the first International, which was why I posted those essays by Bakunin.

Who is right? I hold that the methodology of anarchism is more important and vital than the ideology of it. That’s because I recognize that language, particularly in the services of the ambitious, is routinely turned on its head in the service of power elites.

A group could call themselves Anarchists, but that surely doesn’t make them anarchists, does it? You’d do well not to take them at their word blindly, but rather approach them on your own terms.

The two models of social struggle from history are the Marxist model — the idea of a political vanguard guiding the masses to a socialist society; and the Bakuninist model — the idea of rejecting all political authority and using popular direct action as the means of realizing socialism in the here and now versus some unforseen future.

To date, the Marxist model has dominated the radical left for over a century, although recently, with the demise of the USSR, we see the ideological air clearing for the first time in decades. This is why the debate is so timely and critical, if anarchism is to proceed and grow.

My main objection to ideological Anarchism is that it depends not on freethinking and direct action, but on obedience, passivity, and conformity to an externality — either a manifesto, platform, or other mechanism of control. Further, it focuses on a top-down, centralized organization as a means of bringing Anarchism from the center outward.

It is ludicrous to assume, however, that you can use unfree means to attain a free society. It is similarly ridiculous to try to create a popular, libertarian organization before you have a mass following! What you’d get, instead, is an elite cadre of activists, which, unsurprisingly, parallels the current situation of the radical left!

Further, since doctrinal purity is most important to the ideologue, they end up: 1) eternally quarrelling about minor points; 2) forever looking for and purging heretics; 3) alienating potential fellow travellers through this elitism.

Anarchism isn’t “anything goes” — it means something. However, a working person shouldn’t have to be indoctrinated to make them “suitable” to the movement. Noam Chomsky put the methodological view of anarchism best when he said that he saw anarchism as the historical tendency of people to rise up against illegitimate authority.

For example, when the sailors of Kronstadt rose against the Bolsheviks in 1921, they were engaging in methodological anarchism — direct popular action against illegitimate authority — whereas the Bolsheviks had betrayed the Revolution by securing themselves in power, despite their claims to the contrary.

Anarchists should focus on passing along anarchist ideas and, most importantly, anarchistic ways of organizing, rather than trying to turn people into Anarchists. It’s a fine, but an important distinction.

Anarchists hold that the social struggle itself — propaganda by the deed — politicizes and radicalizes the masses. When they get a sense of their own empowerment, attained through collective direct action, what you get are “anarchized” people — folks who will understand the ideas of anarchism in practice rather than doctrinally, which is where it matters. You get empowered, active freethinkers, who are not afraid to engage in direct action — in other words, anarchists.

Not to say that all activists are anarchists, because they aren’t. The right wing has a fair share of reactionary activists, but they are, in truth, functionaries of a larger authority structure — drones, who jump when their bosses order them to. Or (more commonly), they are well-meaning people who have been duped and manipulated into supporting a position contrary to their real interests.

But when you get a group of people working together, organizing and engaging in direct action against illegitimate authority, you’re more likely to have folks sympathetic to anarchism than to any other doctrine, which calls for obedience and passivity. The social struggle itself promulgates the anarchist idea, when waged anarchistically.

Sadly, what we have today are a plethora of Anarchists — ideologues — who focus endlessly on their dogma instead of organizing solidarity among workers. That accounts for the dismal state of the movement today, dominated by elites and factions, cliques and cadres.

And, since the Cardinal Rule of Ideology applies — that the ideologue is not, and cannot ever be wrong — what it means is the disputes never, ever end, and everyone divides into countless little, irrelevant enclaves.

Methodology is far more open — there is that which works, and that which doesn’t, and degrees between those points. If one strategy doesn’t work, you adjust until you get something that does work.

The anarchist holds that the working person is ready in the here and now for social revolution, in terms of inclination and instinct — people want to be free; they want an improvement in their circumstances and quality of life. People don’t want to be slaves — those in power spend much time convincing people that they’re free when, in fact, they aren’t. We believe that everyone values their freedom, whereas the Anarchist holds that the working people are too racist, sexist, apathetic, homophobic to “get the message” — they view the masses with almost Marxist contempt.

In fact, when things don’t go the Anarchists’ way, they blame everyone but themselves, which accounts for the isolation and elitism of the left wing — you working people are just “too stupid/racist/sexist” to get their Lofty Ideas. With that attitude, you can see why working people ignore the radical left.

One thing ideologues of all stripes share is a negative view of human nature — they see us all as basically bad, and in need of improvement (achieved by a period of indoctrination, naturally, which they offer). Further, ideologues hold themselves exempt from this principal of negative human nature — that is, they are okay, but the rest of the world is screwed.

However, this view is incompatible with anarchism, and entirely appropriate to authoritarian ideologies — authoritarians all view people as basically bad, and in need of education, supervision, and above all, control, which they are all too willing to provide.

The anarchist, conversely, holds that human beings are basically good and not in need of guidance, coercion, and control — indeed, we hold steadfastly to the idea that the only evils in society come about when some seek to control and coerce others, and that the mechanisms of power, privilege, and control turn even the saintliest stalwart into a connniving manipulator.

In other words, anarchists view people as good, and systems of control as bad, whereas ideologues hold the other view — that people are bad, and systems of control are good (so long as they control those systems — if someone else controls them, then they’re bad — that’s how they seem anti-authoritarian when out of power — but just wait until they do get a measure of power, and you’ll see). It’s an important difference, and determines the nature of the organization that arises from these foundations.

The organization based on a negative view of human nature will focus on power and control, centralizing these things in as few hands as possible — the people who can be trusted with such power (meaning, the most obedient and doctrinally sound), whereas the organization based on a positive view of human nature will seek to disseminate power and eliminate control, decentralizing and dispersing these in as many hands as possible.

The most pernicious threat of the ideologue is that they exempt themselves from their own rules — again, stemming from the notion that they have “seen the light” and the rest are either: 1) idiots; or 2) evil (for turning their backs on the Truth). Thus, they can never be reasoned with, because they are irrational themselves — if you object to their program, regardless of the reason, then you are at fault, not them.

That’s why a natural corollary of the ideologue is the use of force — because they are dogmatic and irrational, all they can ultimately rely on for legitimacy is force, which necessitates centralization and control of force — e.g., the State, in a newer, more pernicious form.

In a sense, the ideologue is a closeted authoritarian, which is why they are so treacherous. They seem anarchistic because they reject authority that exists when they have no part in it; however, they are really objecting to being disempowered themselves, rather than rejecting authority itself. When they attain a position of authority, they turn as despotic as anyone who preceded them.

Their Authority is in their ideology itself — their Big Idea — which you resist at your own peril. It was this that caused the Galleanists (Italian anarchist followers of Luigi Galleani) to engage in several bombing campaigns, even against innocent passerby — to the Galleanists, anyone who didn’t get The Idea wasn’t innocent.

This may seem paradoxical coming from a political Web page, but that’s okay — the anarchist holds that Truth tends to end up in the back pocket of the most powerful — that is, the most powerful hold that their views are the Truth, and woe to you if you say (or even think) otherwise.

There’s nothing more ideological than pretensions toward ultimate Truth, and anarchists should have no part of it. Our view, conversely, is that the only truth worth holding is that there is no truth, because there is no external truth out there for us to perceive — there is merely that which makes sense to us and that which doesn’t.

Reality exists (although some philosophers debate that, too) — reality is objective, whereas truth is entirely subjective. If you hold out a rock and let it go, it will drop. That’s because gravity is an objective force — it’s an aspect of what is — reality. Truth derives from reality (e.g., let go of a rock and it will drop), not the other way around.

The subjectivity of truth is something authorities are very uncomfortable with, because it’s a revolutionary concept — if truth is subjective, then the framework of our society collapses — law, religion, the State — all implode if you recognize that what some claim to be Truth is, in reality, opinion backed by force. Where power is concerned, what is considered Truth ends up, in reality, mythmaking, lies, and superstition.

Anarchists hold that truth is subjective, or they should, which forms the basis for our rejection of dogma and manifestos. No Anarchist can come up with an ultimate manifesto which can account for every possible human encounter and interaction, although some do try.

Freethinking is the only methodology you can safely rely on, in the absence of external Truth — that is, thinking and evaluating for yourself what is and isn’t, rather than letting someone else define your world for you. And the currency of this type of exchange is reason, rather than force.

Authoritarians hold to an objective ideal — the Truth — which only they can see, of course. And your role in the process is to obey their Truth or suffer accordingly. Thus, the liberty-cherishing capitalist puts a “Trespassers will be shot” sign on “his” property and sleeps easy at night (even though the original title holder trespassed and shot others to get that property!), and the god-fearing Christian puts a witch to the torch, while preaching “love one another” from the Good Book.

Ideologues are forever trampling their lofty words by their atrocious deeds — and anarchists want no part of it. We reject them and their Truths!

Does anarchist rejection of Truth mean that anarchism, in turn, means anything goes? Yes, and no — that which destroys illegitimate authority is anarchistic; that which doesn’t, isn’t. That is the basis for our methodology, and for our resistance to the privileged and powerful.

It means that the only legitimate authority is that which is freely accepted, in the complete absence of coercion — e.g., free association. This allows for an extraordinarily wide range of human activity, and creates the appearance of “anything goes” — anarchy — but this can only be attained through consistent, dedicated organizing on the part of the members of society.

In this manner, we reject lifestylists, because what they seek — narcissistic autonomy — is impossible in our interconnected society, and is not anarchistic, because it disdains class struggle and organization in favor of turning inward and abandoning human solidarity.

The methodological basis for our rejection of lifestylism is that it liberates no one, including the lifestylist, and is thus no threat to illegitimate authority whatsoever. The “temporary autonomous zone” is a pipe dream, as it leaves the prime source of oppression — the State — untouched, unchallenged, and intact.

It’s the wrong method, even if the lifestylist disdain for ideology is well-founded. Social anarchists should leave lifestylists to their antics, rather than forever arguing with them. For the social anarchist, the goal, instead, is to organize effectively, rather than deriding lifestylists for their way of life.

Anarchism is a rational theory and philosophy, requiring observation and thought, and above all else, organizing and action.

While on the topic of reason and rationality, there is something which distinguishes the ideological Anarchist from the methodological anarchist — namely, deduction versus induction. I’ll elaborate.

Deduction is where you proceed from a premise. For instance, if I say:

“I am an Anarchist, therefore all which I do is anarchistic.”

I am being deductive in my assessment of my anarchism. If you say that something I’m doing isn’t anarchistic, I’d disagree for that reason — I’d say, “no, you’re wrong, because I’m an Anarchist — I know what Anarchism is — Anarchism is what I do. And, since you are disagreeing with me, and I am an Anarchist, then you must be an authoritarian — you, therefore, are my enemy.”

See the problem? Now, this kind of deductive ideology isn’t confined to Anarchism — in fact, it’s even more common among all the authoritarian ideologies out there, in which people say one thing and do quite another.

However, with anarchism, this kind of thinking is positively deadly — it gets in the way of freethinking and closes your mind.

Inductive anarchism, rather, looks at what you do and why, and comes to the conclusion that you are an anarchist based on what you do, not on what you say.

Not everyone who is fighting illegitimate authority is an anarchist — that’s not the case at all. Rather, what inductive anarchism means is that one’s actions become the criterion of judgment, not one’s claims.

This is a very important distinction, because it allows you to be on guard for creeping authoritarianism and vanguardism within the movement itself. That’s what Bakunin noticed when he was confronting Marx — Marx and his gang all said they were for socialism, and wanted everyone to embrace their program as the “best” way to get to it, even as their program proved to undermine and destroy the socialism it claimed to be for.

The same risk exists with anarchism. Where deductive Anarchism can be easily turned on its head by authoritarian opportunists within the movement (and are unlikely to be challenged because such movements discourage dissent and disagreement in favor of ideological conformity) — meaning that such opportunists won’t be challenged within their own groups!

Inductive, or methodological anarchism, however, can’t be so readily betrayed, because it involves adding everything up and determining for yourself if it balances out, rather than letting someone else tell you it does. It means thinking for yourself instead of letting others think for you.

Deductive Anarchists are fond of manifestos and platforms — tracts and doctrines which they produce and expect you to learn, memorize, and obey. They think that if they could just convert enough of you to their way of thinking, then Anarchy will be possible. They hold that you’re not ready for it yet.

Inductive anarchists think that’s ridiculous — we hold that no tract or manifesto can possibly cover all human dreams, hopes and aspirations. Further, we hold that everyday people are already able to understand anarchist ideas, and put them into practice — they earn this faith on our part by virtue of being human.

Humans don’t like being told what to do, or being kept in bondage. If they did, those in power wouldn’t spend so much time, energy, and money hoodwinking you into thinking you’re free when in truth you’re a slave. The anarchist’s role in all of this is merely to create that initial awareness, and to communicate organizational methods that weaken and destroy authority, and let the process take care of itself.

The Anarchist, conversely, wants a more active, vanguardist role — since they hold that only their tribe can be trusted with the Truth only they can see, they see themselves as the shadow guides who’ll keep everything in line from behind the scenes, because everyone knows you poor slobs can’t be trusted to do it yourselves.

That attitude is why the radical left so often derides the working class as apathetic, reactionary, racist, sexist, homophobic — a thousand maladies. They see you as lesser beings who are in need of their guidance and instruction.

As an anarchist, I think that attitude is insane — indoctrinated people are unfree, and it is impossible to create a free (that is, anarchist) society using unfree methods.

The point is that only two things really matter: 1) organizing solidarity among working people; 2) encouraging popular direct action. That’s the goal of anarchists, or should be. It’s not our purpose to teach others how to behave, or what to think — that’s their own business, certainly not ours.

The Anarchist holds that “if only the rest of the world were Anarchists (like me) everything would be fine” — they hold themselves as the sum total of anarchistic purity — but that’s a vanguardist sentiment in the extreme, and is Marxist at root, and ultimately, in effect.

The methodology of anarchism is most important, because it’s so easy to determine if you’re off course or not, whereas words and doctrines are hollow and meaningless — they can be wrapped around the basest tyranny and made to seem sweet and true. All the enemies of freedom practice this — the US carpet bombs people and assassinates democratically elected leaders in the name of “democracy” and “freedom” — a claim that holds up only if you embrace the Ideology of America, rather than the methodology of democracy!

In fact, if you examine the US system of government methodologically, you find that it doesn’t even remotely approximate “democracy,” “freedom,” “popular will,” or “representation” — but all of these words are used with nauseating frequency by the elites in power.

Lenin, while attempting to rally support for the Bolsheviks, made “all power to the soviets” the slogan of his party, knowing that popular self-rule was what the workers wanted. The workers put their faith in Lenin and Trotsky to do this, and lo and behold, when the Bolsheviks came to power, they quickly shifted gears, and destroyed every worker soviet they came across — “all power to the soviets” in practice became “all power to the Bolsheviks” (which really meant The State). The “Communist” Party destroyed communism, because the latter threatened their power base!

The anarchist’s job is solely to shows the means by which libertarian social revolution can be carried out — the anarchist’s toolkit, if you will, rather than a roadmap. And this strategy is more anarchistic than the other route, because it leaves the initiative where it should be: on the street, at the shop floor, in the classroom — a thousand arenas where individuals band together to fight illegitimate authority.

Dave Neal

9/17/97




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“It’s an odd feature of the anarchist tradition over the years that it seems to have often bred highly authoritarian personality types, who legislate what the Doctrine is, and with various degrees of fury (often great) denounce those who depart from what they have declared to be the True Principles. Odd form of anarchism.” — Noam Chomsky



1. The theory that all forms of government are oppressive and undesirable, and should be abolished; 2. Active resistance and terrorism against the state, as used by some anarchists; 3. Rejection of all forms of coercive control and authority.

