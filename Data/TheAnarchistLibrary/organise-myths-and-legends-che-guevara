      Army      Missile Deal      Shambles
In the second of our series we look at the life and ideas of Ernesto Che Guevara. Che has been in the news a lot lately, with his remains being dug up in Bolivia and reburied in Cuba, the publication of hitherto unknown photos of his Bolivian campaign and two new biographies. The heroic cult that has developed around him has taken on new life. Whilst his image — on T-shirts, posters, and beer labels — continues to make money for capitalists, there seems to be a revival among the young in the idea of Che as idealistic hero and fighter for freedom. This hero cult seems to have infected many young radicals, some of whom regard themselves as anarchists.

The truth may be unpalatable to many. After all, the Che cult is still used to obscure the real nature of Castro’s Cuba, one of the final bastions of Stalinism. As jaded Stalinists and fellow-travelling Trotskyists celebrate Che’s anniversary we take a look at the real man behind the legend.

Born in Argentina to a Cuban aristocratic family who had fallen on hard times but who still had much wealth, Guevara had a comfortable upbringing. When Juan and Eva Peron started on their rise to power, using populism and appeals to workers and peasants to install a regime that had many fascist characteristics (1944–1952) Guevara was still a youth. At this period he seemed remarkably disinterested in politics and failed to offer any opinions for or against the Peron regime.

Events in Guatemala were to change this. Arbenz, a leftist army officer, was elected as President. In 1952 he nationalised the property of the United Fruit Company, a major US company which owned much land and had great economic and political influence. He also began to nationalise the land of the local big ranchers and farmers. Guevara was caught up in enthusiasm for this experiment in ‘socialism’ which infected middle class Latin American youth. Just before a trip to Guatemala he wrote: “I have sworn before a picture of the old and mourned comrade Stalin that I won’t rest until I see these capitalist octopuses annihilated.”

Guevara was in Guatemala when a US backed invasion force smashed the Arbenz regime. He was able to flee to Mexico. Here he joined up with the Cubans around Fidel Castro and his brother Raul. In November 1956, Che and 80 other members of the July 26 Movement (J26M) founded by Fidel had landed in Cuba to carry on a guerrilla campaign against the US backed dictator Batista. Here Che proved to be the most authoritarian and brutal of the guerrilla leaders. In fact Che went about turning volunteer bands of guerrillas into a classic Army, with strict discipline and hierarchy. As he himself wrote: “Due to the lack of discipline among the new men...it was necessary to establish a rigid discipline, organise a high command and set up a Staff.” He demanded the death penalty for “informers, insubordinates, malingerers and deserters.” He himself personally carried out executions. Indeed the first execution carried out against an informer by the Castroists was undertaken by Che. He wrote: “I ended the problem giving him a shot with a.32 pistol in the right side of the brain.” On another occasion he planned on shooting a group of guerrillas who had gone on hunger strike because of bad food. Fidel intervened to stop him. Another guerrilla who dared to question Che was ordered into battle without a weapon!

Apart from the drive towards militarisation in the guerrilla groups, Che also had another important duty. He acted as the main spreader of Stalinism within J26M. He secretly worked towards an alliance with the Popular Socialist Party (the Cuban Communist Party). Up to then there were very few Stalinists within J26M and other anti-Batista groups like the Directorate and the anarchists were staunchly anti-Stalinist. The communists were highly unpopular among the anti-Batista forces. They had been junior partners of the regime and had openly condemned Castro’s previous attacks on Batista in 1953. They belatedly joined the guerrilla war.

With the Castroite victory in 1959, Che, along with his Stalinist buddy Raul Castro, was put in charge of building up state control. He purged the army, carried out re-education classes within it, and was supreme prosecutor in the executions of Batista supporters, 550 being shot in the first few months. He was seen as extremely ruthless by those who saw him at work. These killings against supporters of the old regime, some of whom had been implicated in torture and murder, was extended in 1960 to those in the working class movement who criticised the Castro regime. The anarchists and anarcho-syndicalists had their press closed down and many militants were thrown in prison. Che was directly implicated in this. This was followed in 1962 with the banning of the Trotskyists and the imprisonment of their militants. Che said: “You cannot be for the revolution and be against the Cuban Communist Party.” He repeated the old lies against the Trots that they were agents of imperialism and provocateurs. He helped set up a secret police, the C-2 and had a key role in creating the Committees for the Defence of the Revolution, which were locally and regionally based bodies for spying on and controlling the mass of the population.

Che was the main link, indeed the architect, of the increasingly closer relation between Cuba and the Soviet Union. The nuclear missile deal which almost resulted in a nuclear war in 1962 was engineered at the Cuban end by Che. When the Russians backed down in the face of US threats, Che was furious and said that if he had been in charge of the missiles, he would have fired them off!

By 1963, Che had realised that Russian Stalinism was a shambles after a visit to Russia where he saw the conditions of the majority of the people, this after “Soviet-style planning” in the Cuban economy had been pushed through by him. Instead of coming to some libertarian critique of Stalinism, he embraced Chinese Stalinism. He denounced the Soviet Union’s policy of peaceful co-existence, which acknowledged that Latin America was the USA’s backyard, and gave little or no support to any movement against American control. Fidel was now obsessed with saving the Cuban economy, himself arguing for appeasement. Against this Che talked about spreading armed struggle through Latin America, if necessary using nuclear war to help this come about!

It was on this basis that Che left Cuba never to return. He went to the Congo, where he worked with the Congolese Liberation Army, supported by the Chinese Stalinists. This was a shambles of a campaign, and Che ended up isolated with many of his band dead. Despite this, Che still believed in guerrilla struggle waged by a tiny armed minority. His final, fatal, campaign was in Bolivia.

This also was a fiasco. Basing himself once more on old Castroist strategies, he failed to relate to the industrial working class. The Bolivian working class, and especially the tin miners, had a recent record of militancy and class consciousness. The peasants, on the other hand, among whom Che hoped to create an armed insurrection, had been demobilised by the land reforms of 1952. So, Che was unable to relate to either workers or peasants. The local Communist Party failed to support him. Robbed of support, Che was surrounded in the Andean foothills, captured and executed.

Yes, Che was very brave physically. Yes, he was single-mindedly devoted to what he saw as the revolution and socialism. Yes, he refused the privilege and luxury granted to other leaders of Castroist Cuba, taking an average wage and working hard in his various government jobs. But many militarists, fascists and religious fanatics share these characteristics of bravery and self-sacrifice. Che’s good looks and ‘martyr’s’ death turned him into an icon, an icon duly exploited by all those wanting to turn a fast buck selling ‘revolutionary’ chic.

But good looks and bravery camouflage what Che really was. A ruthless authoritarian and Stalinist, who expressed admiration for the Peronista authoritarian nationalists, Che acted as a willing tool of the Soviet bloc in spreading their influence. Even when he fell out with the USSR about the possibility of guerrilla war in Latin America, he still remained a convinced Stalinist with admiration for China and North Korea. He had no disagreements with the Soviets about what sort of society he wanted — a bureaucratic authoritarian state-capitalist set up with contempt for the masses.

Che may look like the archetypal romantic revolutionary. In reality he was a tool of the Stalinist power blocs and a partisan of nuclear war. His attitudes and actions reveal him to be no friend of the working masses, whether they be workers or peasants.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

