
Transcript of a trial of Joseph Déjacque and a printer for the attempted publishing of a book of fables against the government of the Second French Republic and the capitalist system. Contains some fragments of the text submitted for evidence.

Courts and Tribunals

COURT OF ASSIZE OF THE SEINE.

M. d’Esparbès de Lussan, presiding.

Offense involving the press. The Lazarenes.

Mr. Joseph Déjacque, a paper hanger, thirty years of age, author of a work entitled The Lazarenes, Social Fables and Poems is arraigned before the jury and accused of the crimes of: l) exciting hate and contempt for the government of the republic; 2) having sought to disturb the public peace by exciting the contempt or hatred of the citizens against one another; 3) justifying acts described as criminal by the penal law.

Mr. Beaulé, printer, is accused of aiding and abetting the same crimes by knowingly printing the work in question.

Here are the facts in the indictment:

“Mr. Déjacques, who calls himself a man of letters, paper hanger, and who had been arrested during the events of 1848 and 1849, has published in the month of August, 1851 a little brochure entitled The Lazarenes, Social Fables and Poems. That brochure appears to have only one thought, which is to forbid the advantages of fortune in the name of equality.

“Lazarus,” he says in the epigraph, “is the poor, the suffering, the starving, the ghost, the great disinherited,” but he does not limit himself to these whimpers. In the second fable, he makes the approaching advent of socialism a threat. In the fourth, he explicitly endorses the reds, and rues the bad days of June. In the eighth, following the thread of the principal idea, he says he would like to see the people, like a lion, roar the cry of deliverance; but that people, he says, are in the chains of capital.

“Nothing is more clear than the song entitled “The Family of the Transported;” it is a poem in honor of the insurgents of June. In the piece entitled The Past, the Present, and the Future, he declares straightaway that the present government does not care about the sufferings of the people and drives them with whip blows.”

The Chairman, to Mr. Déjacque: You are a paper hanger, and you assume the title of a man of letters.

Mr. Déjacque: I reject that last qualification as insulting. (Signs of astonishment in the court.)

The Chairman: When you appeared before the examining magistrate, you took that position. You see in your deposition that you have declared yourself to be a man of letters and paper hanger. That deposition was signed by you without the least protestation.

Mr. Déjacque: I signed without reading what had been written.

The Chairman: You admit to being the author of a collection of poems entitled Les Lazaréennes. Is this the first publication you have made?

Mr. Déjacque: In 1848, I published in a journal[1] one of the pieces from my collection, entitled “An Hour in the Tuileries.”

The Chairman: Various manuscript pieces have been found in your home; have some of these poems already been printed and published?

Mr. Déjacque: No, Monsieur.

The Chairman: An unsigned letters has also been found among your papers, in which someone has written: “It is impossible for me to take on the printing of your work (Les Lazarenes), and I regretfully return your manuscript.” Can you say who that letter came from?

Mr. Déjacque: I do not see the use of it.

The Chairman: I am asking you if you want to make known the name of that printer.

Mr. Déjacque: Since he did not sign his letter, I suppose that he desires his name to remain unknown.

The Chairman: So, it is after the refusal by that printer that you contacted Mr. Beaulé?

Mr. Déjacque: Yes, Monsieur; but I do not think that he had time to read my manuscript: he received it by way of one of my friends, a compositor in his shop.

The Chairman: How many copies of your work have been made?

Mr. Déjacque : A thousand copies.

The Chairman: You were asked for no corrections?

Mr. Déjacque : Just one. Mr. Maignan, an associate of Mr. Beaulé asked me to suppress one passage where he thought he saw an allusion to the President of the Republic.

The Chairman: It was on the first of last August that the thousand copies were presented to you without being bound?

Mr. Déjacque : Yes, Monsieur, it was. I would have bound them and charged one of my friends with distributing them.

The Chairman: You haves said in the examination that you were about to sell all the copies, at a twenty-five percent discount, to someone that you do not wish to name. One of these copies has been found at the home of the Lucas [named in the indictment], arrested and charged in the German plot.

You admit that you have been transported, following the events of June, 1848?

Mr. Déjacque : Yes, Monsieur.

The Chairman: For how long were you transported?

Mr. Déjacque : For eleven months.

The Chairman: You have since been pardoned?

Mr. Déjacque: Yes, I returned May 28, 1849.

The Chairman: The following June 12, you were arrested for rebellion and conspiracy.

Mr. Déjacque: It was an error. I was stopped in the Rue Saint-Honoré, close to the Rue Bourdonnais, on the eve of the events of June 1849, because someone believed that I was coming to attend the sessions of the democratic committee.

The Chairman: Did you actually go there?

Mr. Déjacque: No.

The Chairman: You, Mr. Beaulé, you are the printer; the license is in your name?

Mr. Beaulé: Yes, Monsieur.

The Chairman: Mr. Maignan is only your associate?

Mr. Beaulé: Yes, Monsieur.

The Chairman: As licensee of record, you are responsible for everything that comes from your press! Did you know that the manuscript of Déjacque had already been rejected by one printer?

Mr. Beaulé: No, Monsieur, we were completely unaware.

The Chairman: If you had bothered to examine this manuscript, you would have seen that there was a real danger in printing it.

Mr. Beaulé: It was Mr. Maignan who looked over the work and asked the author to make some changes.

The Chairman: All that was suppressed in the manuscript was one allusion to the President of the Republic. You have delivered the copies unbound; is that consistent with the normal practices of your house?

Mr. Beaulé: Yes, Monsieur. The booksellers or authors often do the binding themselves, and sometimes they also furnish the paper.

The Chairman: How much time passed between your receipt of the manuscript and your delivery of the thousand copies?

Mr. Beaulé: The copies were delivered twenty-eight hours later.

The Chairman: Have you not already appeared in court for printing other works?

Mr. Beaulé: Yes, on the occasion of the songs published by Durand.

The Chairman: The author of those songs was convicted?

Mr. Beaulé: Yes, Monsieur.

The Chairman: And you, have you been convicted?

Mr. Beaulé: No, Monsieur.

Mr. Croissant, attorney general, spoke for the prosecution. Mr. Déjacque, he said, is one of those hateful socialists who hold society in horror, and who have no other aim, no thought but to constantly excite the wicked passions of those who possess nothing against those who do possess, so that their detestable doctrines may triumph. This is how one foments the hatred of the tenants towards the proprietors and especially of the workers towards the bosses.

The execrable doctrines of the author are found on every page of his book, we might say in every line of his fables and poems. So, by acquainting you with some passages from that work, it will be easy for you to establish that they contain irrefutable evidence of the crimes that we allege.

The first charge, that of exciting the hatred and contempt of the people against one another, results from the very preamble of the work. Here is its epigraph:

Lazarus, the poor, anonymous existence,

The sufferer who knocks at the door of opulence,

The starveling who demands a place at the feast

When the rich sit, haughty and selfish.

Lazarus, the specter waving its shroud,

The great disinherited,

Who rises up from the depths of his bitter, cold misery

And cries: Equality!…..

Then, in the fable entitled The Lion, we find the following stanza:

Sometimes, too, the people, out of patience,

Roar and cry for deliverance.

But, political victor, social slave,

It falls back, unnerved, far from the immense ideal

In its den of abuse, vice and ignorance

Under the chains of capital…..

You see, Lazarus is the poor man; the rich man is a miser, a haughty, selfish man who pushes away the hand of the poor man who implores his charity, and leaves him to die of hunger.

The equality that is preached is nothing other than the destruction of property; socialism wants who-knows-what absurd division, which would cause the poverty of all overnight.

Never has there been more concern for the condition of the poorer classes; never have more efforts been made to remedy their needs. The institutions of charity created before 1848 have been increased since. All that can presently be done has been done to come to aide of those who suffer: day-nurseries, infant schools, and primary schools have been established everywhere for the poor. Banks have been created to assist industry, and pension funds as well; you know what has been done for the pawnbrokers, and all the care that has been taken for the placement of the elderly, insane and infirm. Finally, in order that the poor can make their legitimate complaints heard, legal aid services have been created.

The government has done all that could be done to provide occupations for the workers; significant labors are being carried out, and charitable lotteries are authorized everywhere. I have intentionally passed over the relief that private charity gives to those who suffer, but those helps are endless. Everywhere you see with what eagerness subscriptions are opened to assist those that fire has reduced to poverty, or who have been victims of a flood or some other disaster. Finally, in short, 116 million per year are dedicated to public assistance, without counting private charity, the importance of which is impossible to calculate, even approximately.

The attorney general gave one more reading from another piece entitled The Past, the Present, and the Future. We will content ourselves with reproducing the first stanza:

THE PRESENT.

Proletarian, under the whip,

Under the spur and the bit,

Bent all day without release,

Produce and die for the boss.

I want to use your misery,

I want, with my strong knee

To reduce you to grazing the earth,

Look! I am the Present!

In the following pieces, entitled The Minotaur and The Pirate Slaver, the attorney general pointed to the crime of incitement to hate and contempt for the government of the republic.

The Minotaur is the image

Of the oppressors of the nations

Who, always consumed with the thirst for carnage,.

Always insatiable in their exactions,

Stifle every effort of the democracy

In the dark labyrinth of their diplomacy.

But one day, one day soon, a stout-hearted people,

Guided by socialism

And rejecting this odious yoke of tyrants,

Will trample underfoot, with a victorious tread,

The carcass of despotism.

More than one government in Europe and the world,

Like the slavery on the sea,

Sporting on its mast the horrible flag

Of brutal force,

And putting, nightly, as an infernal level

The fratricide sword in the hands of every felon,

Packs in its forts, its hulks, its bastilles

The reds torn from the heart of their families,

And, remorseless pirate,

Gives chase to new ideas,

Which, sailing in its waters under full sail,

Run under its broadsides

To carry their social treasures to the human race.

Finally, according to the minister public, the crime of justifying acts defined as crimes by the law ensues from the piece entitled The Family of the Transported. It is nothing but a long dithyramb in honor of the insurgents of June.

In closing, the attorney general asked the jury a verdict of guilty against both defendants.

Mr. Déjacque presented his own defense by reading from a written statement.

The jury, after a short deliberation, returned an affirmative verdict on all counts; however, extenuating circumstances were admitted on behalf of Mr. Beaulé.

Consequently, the Court has condemned Mr. Déjacque to two years’ imprisonment and 2,000 francs in fines and M. Beaulé to six months in prison and a 2,000 franc fine. The Court has also declared them jointly liable for fines and fees, and set at two years for Mr. Déjacque and one year for Mr. Beaulé as the duration of the imprisonment for debt.

The destruction of all the copies seized has also been ordered.

Journal des Débats, October 23, 1851, p. 3.

[Working translation by Shawn P. Wilbur. Revised 2/18/2012.]
[1] “Une heure aux Tuileries” in La Voix des Femmes, n°45, June 15,1848.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

