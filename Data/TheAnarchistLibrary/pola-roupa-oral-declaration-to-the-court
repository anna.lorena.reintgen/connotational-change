
Let me begin by commenting on the main accusation against us, that we have founded a “terrorist organization”, by which “we were targeting the country’s established constitutional order seeking its overthrow or even its alteration”. I would like to underline this point. This charge by itself is the biggest proof that this is in fact a political trial. There is no better confession that this is an armed group which has turned against the establishment, against the established order. This charge by itself is a confession that we are political subjects, who have shared political objectives, and that the action of our group was clearly political.

However, you do not admit that we are a political organization, because this would in fact be an indirect admission that there is some political rival to this system; namely that Revolutionary Struggle (Epanastatikos Agonas) is a political organization which advocates another type of society; another type of social, political and economic organization. An acknowledgment of the fact that you are facing political adversaries would actually mean you acknowledge that within this society, this system you loyally serve, there are people who are struggling, looking, standing, striving for another type of social organization.

This alone would be a blow to the very same establishment you serve. On the other hand, however, you wish to present this establishment as a one-way; namely that there is no life outside this system; that there is no life outside this society. You would like to convince everyone that any attempt, any struggle to liberate ourselves from this social situation would be disastrous. You would like to convince everyone that the existence of a stateless society, without supervisors and subordinates, is impossible. You would like to convince everyone that the existence of a society without masters and slaves is impossible. You would like to convince everyone that the existence of a society without rich and poor is impossible. Obviously this line of thinking has a political and theoretical background. We know that much. There are theorists such as Thomas Malthus and Adam Smith, and there have been many throughout the history of the capitalist system you serve, who theorized the existence of poverty, who theorized the notion that the existence of a society beyond social divisions is impossible.

This is something you yourselves serve also through this court proceeding; this entire theory, which in reality seeks to convince people that the fate of the poor is to be poor, the fate of the oppressed is to be oppressed, and as Smith said — and those who are familiar with his papers know that — the pursuit of profit, the very concept on which a society of inequalities and injustices as ours is in fact based, is the inalienable right of everyone to improve their financial status by walking on other people’s heads. This is what you consider to be everyone’s inalienable right. To you, this could be characterized as “economic freedom”. It’s what you defend — the mere effort to legitimize the establishment’s criminal nature. It’s exactly this establishment we have tried to overthrow as an organization. This society of inequality is what we wish to overthrow.

I would now like to refer to an example in relation to a ruling of the Supreme Court (Areios Pagos) concerning its response to an appeal against the court decision in the case of the R.O. 17 November (17N). In response to an issue also examined by your court, namely the question of “terrorist” organization and political “crime” — words should be in quotation marks as terms used by the judiciary and not myself — the Supreme Court cited the objective theory arguing that, when someone turns to action against the country’s established constitutional order and seeks its overthrow or even its alteration, in order for this action to be classified as political, the intent alone does not suffice — even if you may all acknowledge political motives as for the intent — but rather the outcome of this effort determines whether or not the action is political. In other words, the Supreme Court ruled that a judgment on the issue should depend on the result, so if we have failed to really overthrow the establishment, we are not deemed political prisoners, and our case does not have a political nature. This is yet another confession. What kind of confession? The judiciary admits, “We go with the winner”. In other words, the Supreme Court said, “If you had managed to win, then you would have been recognized as political beings. Since you didn’t manage to do that, your action is not political. You hold a minority activity”.

It is only logical that an organization such as Revolutionary Struggle alone cannot bring on an overturn. It takes entire political and social processes for this to succeed. But if we assume that Revolutionary Struggle did manage to overthrow the establishment, what would that mean? It would mean that we would automatically be recognized as political subjects. We would seize to be terrorists because we would be winners. What does this point to? That this court just as the entire culture prevailing in this system says, “We go with the one who wins”. This actually means, “We are not interested in the quality of this system. We are not interested in the quality of this organization. What interests us is who is on top”. It is the winner who defines who is a political prisoner, which is a political trial, who is the political subject, and who is the criminal, who is a terrorist, and who is the one that can be vilified as a criminal offender.

I take it for granted that in a society like ours, as it exists today, in a society dominated by the State as a mechanism of Power, it is inevitable that the values of this society will be defined by whoever wins; by whoever has power in his/her hands. That is who will give meaning to things. Whoever struggles from below will be determined by those who dominate in terms that they choose.

This is the case here, too. The reality is that you yourselves would have to immediately change your attitude and recognize ‘political subjects’ in any one group that managed to overturn the existing order.

Similarly, let’s suppose that there had been a social revolution, and we had managed to overthrow this establishment, this very system. If this was the case, we can easily imagine the prime minister (Papandreou, for example) attempting to combat this revolution; to organize, bring troops in and put an end to our social attempt to subvert the established order. In this case, according to your line of argumentation, since it would be the prime minister who would be defeated, he would also be a terrorist, he would be a criminal. After being defeated, there’s no way he could be a political subject any longer. Of course, this is only for the sake of argument, because if we were the winners there would be no question of accusations.

Now I would like to take a position on issues raised also by comrade Maziotis that concern who can speak of terrorists, from which position they speak, and whom they accuse of being terrorists.

In reality — as we mentioned in our on the first day of this trial — Revolutionary Struggle is clearly a political organization that has deep social and class characteristics, fighting for economic equality and political freedom for all people.

It is truly outrageous, especially under the conditions we live in today, that we are accused of being terrorists, when all that is happening around us constitutes a huge crime.

What this court serves is an economic and political system in deep crisis; a crisis that is the result of a long process of profound oppression and exploitation across the globe, the impacts of which are experienced today by all people. The “honourable gentlemen” of the system, investors, industrialists, capitalists, are in reality indifferent to people’s lives, and in fact profit is the only value to them. They go and invest in stock markets, which are nothing but a temple of money and a mechanism that legitimizes the euthanasia of entire parts of population worldwide. As an organization, we went and blew up the stock exchange building, and we admitted we did so, and we did well — I bring the example of an action for which you accuse us based on your criminal law. Every “reputable” capitalist goes to the stock market with his Samsonite bag and tie round his neck in order to increase his own profit and his own property, investing in human lives as if they were peanuts. It makes no difference to that capitalist if he’s investing in peanuts, in indebted countries, or in people’s death. Anyone who has elementarily dealt with these issues (because one does not need to have a degree in Business from some economic faculty to learn these things), anyone who has addressed at an elementary level what it means in our time to invest in stock markets, in food stock markets, in derivatives exchanges that the big-time investors themselves classify as weapons of mass destruction, must realize that indeed such investments result in the deaths of literally hundreds of thousands of people.

Recall the recent global food crisis, in 2008, which pushed millions of people to the streets. Many people died. Many more were marginalized. Entire classes of people were rendered extinct, thrown out of the picture.

The elites preach that the remedy for this crisis we are experiencing today is this social euthanasia, as I call it, which is presented as the only way to actually save the system from decay and to give it a breath of life. They are prepared to put people to death, not only to marginalize others but to kill people in cold blood, in order to survive. I will further analyze this in my final statement to the court. It’s a long story and analysis that I believe Revolutionary Struggle has already offered and will be revived in this courtroom as well.

I believe it is utterly ridiculous that this system should accuse us of being “terrorists”, when we are fighters, and have been since our teens, when most of us have been arrested and in many cases been beaten by cops, when we’ve been dragged into court many times because of protests, squats or occupations and demonstrations, when we’ve acted in so many ways for a common struggle in the streets, when we’ve fought in the context of Revolutionary Struggle that, I repeat, is a political organization with profound social and class characteristics, when we’ve factually stood in a meaningful way at the side of proletarians, when we’ve stood in a meaningful way at the side of the poor, when we’ve opposed the Capital and Power, when we’ve opposed the real criminals. It is ridiculous that it is us who were locked up in prison even before trial, it is utterly ridiculous that it is us whose life is jeopardized as we are threatened with being sent back to prison for many years.

I do not consider this court impartial. You are conducting a special trial, in a special courtroom, under special conditions, and you have imposed a ban on media coverage of the proceedings without even allowing a proper recording of minutes, thus rendering the registration of this historical process impossible. This alone shows that there is premeditation in relation to this trial. I believe you have no basis for convicting us for any specific actions — because everyone knows what lack of evidence there is against the three of us (let alone against the other co-accused). Nevertheless, Maziotis, Gournas and I stand trial as “leaders” of the organization, and we will be convicted in the end. The three of us have claimed political responsibility for our participation in Revolutionary Struggle, we have declared that we are proud of our participation, and we will repeat it as many times as necessary. Still, there is no evidence whatsoever tying us to any specific actions.

Regarding the issue of leadership in the organization, I would like to say this: We are anarchists, and as anarchists not only do we not accept hierarchy, but we despise it. Of course, it is also clear to me that no other revolutionary organization active in the revolutionary urban guerrilla warfare has a hierarchical structure either.

We fight to banish hierarchy in society. It would be impossible to build an organization that would nurture within its core the same social structures that we are fighting to overthrow. I think that Revolutionary Struggle is in fact a miniature version of what we propose as a model of social organization, i.e. a horizontal organization, without any leaders, without the rulers and the ruled.

You have no basis for convicting us on the charge of “leadership” either, but you will sentence us on these grounds anyway. The decision to include this accusation against the three of us is political. Regarding Gournas, he was labelled a leader in the process; he was not one from the beginning. What does this mean? That by taking political responsibility Gournas was automatically included in those who are allegedly leaders, because he took on the political weight of an organization. I bet that if there were another five claiming political responsibility, they would also be charged with leadership. What would we be talking about then? A horizontal organization of leaders. Is that so?

Another thing I want to emphasize is this: in the whole of Greece it is well known that Maziotis and I by extension (since we are life companions) have been tailed and surveyed since 2002, when my comrade had been released from prison, until recently. There have been many TV shows about this matter. In other words, there was a long-term surveillance of us two — mainly Maziotis, but we lived together all these years, so as a result of me as well — and this did not reveal any evidence about our organization or indeed about anything. I would like to say this: Despite the obvious lack of evidence, I believe that ultimately we will be convicted for both leadership and all specific actions. I think that you will issue a political judgment because you will not accept that ‘these persons come forward, claim political responsibility, publicly speak out about their positions and defend actions of such nature, and we’ll let them walk away by simply sentencing them for their participation.’

I believe you have orders to convict us. And it is important to point this out: that the court judgment will not only be sentencing us to a hundred of years in prison, but it will further set a political precedent. What will this precedent be? That all these “gentlemen”, former ministers of Public Order, current and former directors and governors of intelligence agencies and the antiterrorist force, several CIA agents who have tailed us from time to time — for how long I cannot know really — and in any case all secret services of Greece and some from abroad, politicians in Greece, former ministers and by extension the governments themselves are actually quite incapable and powerless in the matter of security. Or, in other words, it will be said, “You had the heads ever since 2003, when the organization first appeared, you were constantly on their tail watching them, and what did you do? Nothing! They acted undisturbed all these years”. So, as “leaders” we went out, organized, carried out actions, wrote proclamations, and lifted the entire burden of an organization, and behind us there were hordes of secret agents and police officers.

These are not just stories from my personal experience. It has all been recorded by TV channels, in their various news shows; it has all been broadcasted on radios. And Markogiannakis, former minister of Public Order, had undertaken to bear the entire burden in relation to the surveillance, to defend the secret services in Greece and the antiterrorist force of the Greek police throughout the period preceding our arrests, and he even stated that they did not seize for a minute to track our every move.

I will propose the following challenge, and say: Bring them here. Call them to the stand. Bring in Markogiannakis; call Korantis; call Chorianopoulos, who is mentioned by Markogiannakis in his interviews; call all these “gentlemen” to testify. Call the former ambassador of the United States in place when the American embassy was attacked. Call the CIA agents who had us under their surveillance. Bring all these “gentlemen” into this court to publicly admit — this would make me very happy, I assure you — that “we had them under constant surveillance all these years, but we lost track of them for long periods” (because in order to prepare an action of the organization it takes time, it cannot be done in a moment), “they vanished, we did not know their whereabouts, and then attacks were made, and they would suddenly reappear, and we just did not do anything”. Call them here to admit that their security measures were shredded by us, that we have put them to complete shame. Let them come here and say this publicly, and I assure you that I myself will accept my involvement in any specific acts they’ll refer to. I will also accept the part related to the acts themselves. Bring these people here. It would be my pleasure as revolutionary that the establishment be humiliated publicly and forced to admit that “we had the particular persons” (this be unprecedented in the history of the revolutionary movement worldwide), “but they managed to make total fools of us and went ahead and did all they did anyway”. If this happens, then I can assure you that—apart from my participation in Revolutionary Struggle — I myself will take responsibility for the specific actions, too.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

