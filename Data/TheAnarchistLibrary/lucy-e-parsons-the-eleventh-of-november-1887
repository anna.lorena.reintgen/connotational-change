
The Eleventh of November has become a day of international importance, cherished in the hearts of all true lovers of liberty as a day of martyrdom. On this day were offered upon the cruel gallows-tree, martyrs as true to their high ideals as were ever sacrificed in any age.

The writer will assume that the present generation is but superficially informed regarding the details that led up to the eleventh of November, for in this busy age, twenty-five years is a long time to remember the details of any event, however important.

In 1886 the working class of America, for the first time, struck for the reduction of the hours of daily toil to eight per day. It was a great strike. Chicago was the storm-center of that strike, because of the activities of the martyrs of the eleventh of November, 1887.

The working class practically tied up the city of Chicago, Illinois, for three days. On the afternoon of May 3rd, of that year the police shot several strikers and clubbed many more brutally. The next evening, May 4th, the now historic Haymarket meeting was held. The Haymarket meeting is referred to historically as the “Haymarket Riot.” This Haymarket meeting was absolutely peaceable and quiet. The Mayor of Chicago attended the meeting, and subsequently took the stand as the first witness for the defense at the Anarchist Trial, so-called. Following is the mayor’s testimony in part:

I went to the meeting for the purpose of dispersing it, in case I should feel it necessary for the safety of the city. There was no suggestion made by either of the speakers looking toward calling for immediate use of force or violence towards any persons that night; if there had been, I should have dispersed them at once. I went to the police station during Parsons’ speech and I stated to Captain Bonfield that I thought the speeches were about over; that nothing had occurred or looked likely to occur to require interference, and that he had better issue orders to his reserves at the stations to go home. Bonfield replied that he had reached the same conclusion from reports brought to him. During my attendance I saw no weapons at all upon any person. In listening to the speeches, I concluded that it was not an organization to destroy property. After listening a little longer, I went home.

This extract is here given from the Mayor’s testimony, because this meeting is referred to very often, even by radicals, as the “Haymarket Riot.”

Had the inspector of police obeyed the Mayor’s orders and not rushed a company of police upon that peaceful meeting, there would have been no trouble. Instead, as soon as the Mayor left, the inspector rushed a company of bluecoats to the meeting; they began clubbing the men and women and scattered them in every direction. Upon this onrush of the police, someone threw a bomb. Who threw that bomb, no one to this day knows, except he who threw it.

The bomber has never been identified, never been arrested, consequently could never have been tried, but my husband and his comrades were put to death on November 11th as co-conspirators with the bomb-thrower, but he is unknown!

Our comrades were not murdered by the state because they had any connection with the bomb-throwing, but because they had been active in organizing the wage-slaves of America.

The capitalistic class didn’t want to find the bomb-thrower; they foolishly believed that by putting to death the active spirits of the labor movement of that time they could frighten the working class back into slavery.

The so-called trial was the greatest travesty upon justice of modern times. The bailiff who was selecting the jury, a creature named Ryce, boasted thus:

I am managing this case and I know what I am about. Those fellows are going to hang as certain as death. I am calling such men as the defendants will have to challenge peremptorily and waste their time and challenges. Then they will have to take such jurymen as the prosecution wants.”

The jury that did try the case was out less than three hours. They left the court room after four o’clock on August 23 and before seven o’clock the self-same afternoon had reached the astounding verdict, sending seven men to the gallows and the eighth man to the penitentiary for the term of fifteen years. The trial had lasted some sixty-three days. Think of the massive testimony that the jury would have had to go over in order to give them even the semblance of a fair trial! Then think of the audacity of a jury being out less than three hours, and of the brutality of a community putting men to death under such a verdict and never allowing them a new trial!

Albert R. Parsons, my husband, never was arrested. On May 5, the day after the Haymarket meeting, when he saw the men with whom he had been organizing labor for the past ten years of his life, being arrested and thrown into prison and treated generally as criminals, he left Chicago. On June 21, the day the trial began, he walked into the courtroom, unrecognized by the police and detectives, and surrendered himself, having been indicted during his absence and a reward of $5,000 having been offered for his arrest. He asked the court to grant him a fair trial that he might prove his absolute innocence. He was never granted the shadow of a fair and impartial trial and was put to death with the rest of his comrades on November 11, 1887!

The men were asked if they had anything to say as to why sentence of death should not be passed upon them. They arose in the court room on the days of October 7, 8 and 9, 1886, and delivered their now so “Famous Speeches,” giving their reasons why the sentence of death should be suspended and they be given a new trial. They called the judge’s attention to the fact that the leading capitalistic paper in Chicago had opened up its columns to receive subscriptions to a fund of $100,000.00, to be paid to the jury as a present for the verdict it had rendered against them. But they were never granted a new trial. They were, instead, railroaded to the gallows at the command of the money power!

For the past two years I have devoted myself to selling their speeches. The Seventh Edition, of 14,000 will be out in a few weeks’ time, being now in press. These copies of the Speeches have been practically all sold amongst the members of the conservative organized labor unions. The Jewish comrades now have a movement started to have these Speeches translated and published in the Yiddish language.

Verily, their “silence is more powerful than the voices that were strangled” that dark November day!

There could be no other results in a matter that had already been fixed in advance, except such as came on October 10, when the infamous Judge Jeffreys gave out his reasons for denying a new trial, and afterwards sentenced them to death. No more remarkable scene than that could well be imagined. The hot, stifling courtroom, crammed to its utmost capacity by an eager crowd, quite in sympathy with the capitalistic ideas and breaking at times into clapping, which was silly and hypocritically repressed by the only too gratified court—the little, ugly, hard-visaged judge, with a nutcracker bald head and cunning eyes. One could fancy his tender mercies, if ever they existed, as dried up and long since fallen to dust. Then the coarse, brutal state’s attorney, with the ferocious howl of an infuriated, blood-hungry wild beast, who continually bellowed for the lives of these men before him. And the little cunning, red-headed lawyer who made the most telling speech that the State gave out, a cruel, crafty effort that misrepresented everything, absolutely, and did it so foxily that each point drew blood like the slash of a claw.

Forever will live in the minds’ eye of those who had the sad privilege of seeing this strange and terrible scene, the calm and noble countenances of the accused, who showed no feeling except when an occasional flicker of fine scorn passed over their refined countenances, as they sat and heard their every act, deed, thought, meaning, however innocent, misrepresented, twisted and their lives going to certain destruction at the hands of their enemies’ tools and minions. All the way through and especially noticeable the last day, detectives and police, plain-clothes men, and others of that ilk, filled the court room. When the sentence of death was being pronounced, these fellows stood up and pointed their revolvers right into the faces of our comrades, evidently fearing—scoundrels are ever cowards—an attempt at rescue on the part of friends, on this, the last appearance of the prisoners outside of the jail. But no such attempt was made and sentence was passed, the date of the execution being set for December 3. One instant to give a passing hand-shake to sorrowing relatives and indignant friends and they were marched back again to their dungeons.

Then began the long, tedious period that lasted for over a year, our comrades languishing in their living tombs. The attorneys for the defense began occupying themselves with their preparations for taking the case to the Illinois Supreme Court and accordingly an appeal was made to a judge of that body, on November 25, who granted a supersedeas and admitted that error had been made. Many friends believed that this meant that our comrades would evidently walk out free men, but those who had seen the working of the trial knew better. They knew that the supersedeas as well as every other step of the proceedings, was carefully taken with a view to giving the world an idea of the “impartiality” of the absolutely hellish conspiracy, the animus of which was to do away with certain labor leaders whose intelligence, honesty and fearlessness had made them objects of the fear and hatred of the capitalistic “Robber-Baron” element.

This supersedeas, therefore, merely gave a breathing spell for the lawyers to get ready their briefs for a hearing of the plea for a new trial before the Illinois Supreme Court. The friends and many persons, indignant at the monstrousness of proceedings they had only supposed possible under the reign of the “Little Father” of all the Russians, were firmly determined to try every court available, in order to prove and make the point that the courts were not impartial but, instead, the ready tools of the moneyed power. Our comrades themselves always believed this, that no justice would ever be done them by any court, and, indeed, that the whole affair was to be regarded in the light of a howling farce, were not murder, of the most revolting and cold-blooded sort, the evident intention.

This appeal went to the Illinois Supreme Court on March 18, had the same hypocritical examination, the honorable judges deciding that no errors had been made of any gravity—when, as a matter of fact, they were there thick—and the decision of the lower court was sustained, the day of execution being again set, this time for November 11, 1887.

So month after month dragged along for our comrades, suffering acutely for want of exercise and fresh air—when this old jail was subsequently torn down to make way for the new one, a black lake of putrid filth was found, fully explaining why our comrades had their teeth decay and fall out—and in the frantic efforts of friends, sympathizers and enlightened persons generally, to show the mass of the people what was being done, all in the name of law and order, the relatives, friends, the Defense Committee and many persons of recognized position—writers, lecturers, and poets—held meetings, distributed circulars, brochures and wrote articles for the radical press—the capitalistic press was solidly closed against one word of the truth—and the public would have finally seen at least something of what was being done, had not the police, ever vigilant in their hate, counteracted it all by “finding” bombs at regular intervals, under sidewalks, in alleys, etc. Made by the police themselves, placed there in the night, these bombs were solemnly “found” in the morning and served as the subject of blazing editorials and solemn, life-sized pictures in the leading capitalistic papers. The public, which does not go below the surface, believed what it was told.

The old, wicked Judge “Jeffreys,” the State’s Attorney, and other tools of the money-power, all whined about threats on their lives and so on, and so the public was kept at that excited and ugly temper that was wanted of them—as when, two thousand years ago they shouted “Let loose to us Barabbas”—for the hirelings of the high and mighty scoundrels who were putting through this judicial murder fully meant to so befuddle the public on the facts as to get its backing and consent.

So the time wore away through the hot summer to autumn, when the attorneys for the defense took the case to the U.S. Supreme Court. These scoundrelly bigwigs, in solemn conclave, decided that no Constitutional right had been violated, although two of the main points in the Constitution had been grossly trodden under foot, namely, the right of free speech and free assembly at the Haymarket meeting, and the right to free and impartial trial at the hands of the law, which was absolutely wanting. It is a matter of conjecture as to how many millions of capitalistic gold went to animate that decision!

So we are brought down to the last days when the friends and sympathizers circulated petitions for executive clemency by the thousands and the police, no less active, “found” bombs and even one of the Supreme Court judges got an infernal machine (which turned out to be a box of papers about some other case!). The only serene people were our condemned comrades. Finally, at the last hour, an appeal was to be made to the governor for executive clemency. This meant a sort of pilgrimage to the City of Springfield by hundreds of persons, including scores of friends and some of the relatives. Thousands of others wrote letters; our comrades themselves—except in the cases of Fielden and Schwab—positively refusing to admit that they had committed any misdemeanor or to ask for any mercy. They protested that they wished merely justice. The city was, at this point, in a perfect state of martial law. Several regiments were camped, with cannon, close to the city hall, and sleuths, armed police and such wretches were everywhere. One wondered why the plutocrats were so afraid of their bad consciences which, doubtless, raised avenging hands from every shadow!

Our comrades, in the meantime, were subjected to every outrage and humiliation. Their clothing and even their persons were continually searched, the daily papers were denied them, they were no longer allowed the freedom of the corridors for a moment’s exercise, relatives and friends no longer admitted to see them. They were even forced to the horrid task of willing their bodies, each separately to their families, to keep them from being desecrated by the police, after death.

The weather had turned very cold and those of the members of the families who had not gone to Springfield to see the governor gathered in a pitiful group in the corridor of the jail vestibule, really then the court house, and beginning in the early morning, begged for a last word of farewell with their loved ones. This was flatly denied. All the livelong, terrible day these people, mostly women, had to stand on their feet in the bitter cold and witness the preparations for the execution—they had seen the coffins carried in the evening before!—without either food or water, hour after hour, with a brutal crowd of police and their friends crowding about to the extent of hundreds, staring and commenting. At midnight, a very few of the relatives were taken in, one at a time, by a turnkey, with a lantern in his left hand and a revolver in the other. The inside of the jail buzzed like a hive, so full was it with reporters, police, sleuths, and other tools of the moneyed class.

After a few seconds of agonized parting, each poor woman was marched back and left in the dark corridor. After midnight, the “decision” of the governor was not announced in order to keep down any attempt at rescue by friends and sympathizers. The governor simply refused interference, except in the cases of Schwab and Fielden, who received life-sentences in the penitentiary (afterwards, these were pardoned by Governor Altgeld).

The morning of the eleventh found our dear comrades composed, smiling, firm without bravado. I, who had been denied admission on Thursday evening, went again in the morning, accompanied by a woman friend and comrade and our two children, to say a last farewell to my beloved husband and that the children might have their father’s blessing and last remembrance.

A cordon of police armed with Winchesters surrounded the jail. Pressing against this was a crowd of thousands of persons. To one policeman after another I appealed without effect, until one told us to come around the corner and he would “let us in,” which he proceeded to do by hustling us into a patrol wagon and taking us to the station house, where we were stripped naked, searched and locked up all day, until three in the afternoon, that is three hours after the execution. The city was in the hands of the people and drunken police. The rich, to a man, had gone away for a few days’ vacation, terrorized by their own black consciences.

The execution itself was put through as swiftly as possible. Our comrades were not to be permitted the usual speeches, always accorded to doomed men. They had, however, foreseen this and each had prepared a sentence to express his last feelings. This they said just as the caps were being adjusted that forever shut the light from their eyes. Their clear voices rang out in those sentences now become classics. Let us pass over the agonizing scenes at the homes of the men, when wives, children, mothers, sisters, brothers, friends, received back the bodies of their dear ones, from whom life had been crushed out and all only because they had dared to tell the workers the simple truth!

On Sunday morning, November 14, the funeral took place, and no more remarkable sight will ever be witnessed than that procession of countless thousands that filed past the dead as they lay in their homes, and then the procession of five black hearses that passed through the city, accompanied by bands playing dirges and carriages bearing the friends and sympathizers, the mourners directly behind the hearses. Past the offices of the newspapers that Parsons and Spies had edited, to the Northwestern train in waiting, passed the cortege which bore them to Waldheim Cemetery. The streets along which this remarkable procession wended its way were solidly packed with human faces and as the hearses passed, hats were taken off by thousands, instinctively, as it were. They did not know it, but they somehow felt that they were in the presence of great dead who had died nobly!

At the cemetery, a way had to be cleared through the dense throng for the procession. Four addresses were made in English and German, the most notable being the oration pronounced by Captain Black, leading attorney for the defense. And so, beneath mountains of floral offerings, before sorrowing relatives and friends, all that was left of our beloved comrades was consigned to their last resting place, on the banks of the Des Plaines River.

But only their ashes, for their noble, true souls, animated by an undying faith in and love of humanity, will never die and their last words will continue to echo in the hearts of people, down through the ages of men, who still believe in rights and the brotherhood of man. That the present generation thinks this may be gathered by the fact that on every Decoration Day, or day set apart for the decoration of the graves of soldiers that died in our wars, thousands pass around the Anarchist Monument in silent homage or grave thoughtfulness, as if weighing the question of these men “who were not as other men.”




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



I went to the meeting for the purpose of dispersing it, in case I should feel it necessary for the safety of the city. There was no suggestion made by either of the speakers looking toward calling for immediate use of force or violence towards any persons that night; if there had been, I should have dispersed them at once. I went to the police station during Parsons’ speech and I stated to Captain Bonfield that I thought the speeches were about over; that nothing had occurred or looked likely to occur to require interference, and that he had better issue orders to his reserves at the stations to go home. Bonfield replied that he had reached the same conclusion from reports brought to him. During my attendance I saw no weapons at all upon any person. In listening to the speeches, I concluded that it was not an organization to destroy property. After listening a little longer, I went home.



I am managing this case and I know what I am about. Those fellows are going to hang as certain as death. I am calling such men as the defendants will have to challenge peremptorily and waste their time and challenges. Then they will have to take such jurymen as the prosecution wants.”

