      Charity Is to Solidarity What Ally Is to Affinity      Allyship as Identity      Legitimacy, Justification, Authority      Example from the Streets      Community Policing and Power      Not Trying to Get Comfortable—Trying to Get Free      Just Because You Feel Like You Broke It, Doesn’t Mean You Need to Fix It
After several recent disappointing and hurtful experiences—and to be clear, a lifetime of related minor and major run-ins with friends, comrades, and activists—my need is unrelenting for us to rethink how we engage with the question of otherness and our organizing. How do we integrate a genuine approach to oppression and anti-oppression? This writing takes apart the concept of “ally” in political work with a focus on race, though clearly there are parallels across other experiences of identity.

Thanks to experience working with indigenous and other international solidarity movements, anarchists and antiauthoritarians draw a clear line between charity and solidarity based on the principles of affinity and mutual aid. Affinity is just what it sounds like: the idea that we can work most easily with people who share our goals, and that our work will be strongest when our relationships are based in trust, friendship, and love. Mutual aid is the idea that we all have a stake in one another’s liberation, and that when we act from that interdependence, we can share with one another as equals.

Charity, by contrast, is one-sided: one shares “excess” wealth on one’s own terms. Ideologically, charity implies that others inherently need the help—that they are unable to take care of themselves. This is patronizing and selfish. It establishes some people as those who assist, and others as those who need assistance, stabilizing oppressive paradigms by solidifying people’s positions in them.

Autonomy and self-determination are essential to the distinction between solidarity and charity. Recognizing the autonomy and self-determination of individuals and groups acknowledges their competence and capability. The framework of solidarity affirms that other groups have something of worth to be gained through interactions with them, whether materially or by gaining something less tangible like perspective, joy, or inspiration. The solidarity model also dispels the idea of one inside and one outside, foregrounding how individuals belong to multiple groups and groups overlap with one another, while demanding respect for the identity and self-sufficiency of each of those groups.

The charity and ally models, on the other hand, are so strongly rooted in the ideas of I and the other that they force people into distinct groups with preordained relationships to one another. According to ally politics, the only way to undermine one’s own privilege is to give up one’s role as an individual political agent, and follow the lead of those more or differently oppressed. White allies, for instance, are explicitly taught not to seek praise for their ally work, especially from people of color—yet there is often a distinctly self-congratulatory air to the work of allyship and a false humility. Many white allies do their support work in a way that recentralizes themselves as the only individuals willing to come in and do the hard work of fighting racism on behalf of people of color.

Whereas ally politics suggest that in shifting one’s role from actor to ally one can diminish one’s culpability, a liberating or anarchist approach presumes each person retains their own agency while also accounting for and responding to others’ desires, revealing how our survival/liberation is fundamentally linked with the survival/liberation of others. This fosters interdependence while compelling each person to take responsibility for their own choices, with no boss or guidance counselor to blame for their decisions.

For a liberating understanding of privilege, each of us must learn our stake in toppling those systems of power, recognizing how much we all have to gain in overturning every hierarchy of oppression. For many people, this requires a shift in values. A rights-based discourse around equality would lead us to believe that we could all become atomized middle-class families of any race who are either straight or gay married. But anyone who’s been on the bottom knows there’s never enough room for everyone on the top—or even in the middle. A collective struggle for liberation can offer all of us what we need, but it means seeking things that can be shared in abundance—not privileges that are by definition limited resources, such as wealth and social legitimacy that are only available on account of others’ poverty and marginalization.

The concept of allyship is embedded in the rights-based discourse of identity politics. It assumes that there are fixed groups of people (black people, women, gay people, and so on) who are structurally oppressed in our society, and that we must work across these differences in identity to achieve equality for all. In the discourse of ally politics, this responsibility falls especially on those who benefit from those structural oppressions. Thus allyship is born as an adjunct of identity—and as an identity unto itself.

Allyship centers on the idea that everyone’s life experiences are shaped by their perceived identities, and thus someone with an identity that is privileged in our society cannot understand the experiences of someone with an identity that is oppressed. There is no “essential” experience of belonging to any of these categories, however. Oppression runs along countless axes, and the subtleties of our experiences are irreducible—which makes a strong case for listening to and trusting each other wherever we possibly can.

A good ally learns that if one can never understand the implications of what it is to walk through this world as an oppressed [fill in the blank with a person on the receiving end of a specific oppression], the only way to act with integrity is to follow the leadership of those who are oppressed in that way, support their projects and goals, and always seek out their suggestions and listen to their direction.

This gets complicated, quickly, as soon as the aspiring ally starts navigating through the world and discovers that there is no singular mass of black people, latino folks, or “people of color” to take guidance from, and that people within a single identity not only disagree with each other but also often have directly conflicting desires and politics. This means that one cannot be a white ally (for example) as an identity; one can be an ally to specific people of color in specific situations, but not to people of color as a whole category.

In seeking oppressed groups to take direction from, white folks of ten end up tokenizing a specific group whose politics most match their own. “What does the NAACP [or Critical Resistance, or the Dreamers] think about this?” Likewise, they may latch on to the most visible “leaders” of a community because it is quicker and easier to meet the director of an organization, minister of a church, or politician representing a district than to build real relationships with the people those leaders purport to represent. This approach to dismantling racism structurally reinforces the hierarchical power that we’re fighting against by asking a small group to represent the views of an entire category of people with radically different lived experiences.

Perhaps you’ve watched or participated in organizing that seeks to develop the leadership of individuals who live in a specific neighborhood or work in a particular kind of labor force. This language seems to offer the benevolence of the skills of the organizing group to those who haven’t been exposed to such ideas. In fact, it is coded language describing a reductive and authoritarian approach imposing an organizing model on a group of people from the outside. It also conveniently creates spokespeople who can then be used to represent the whole of that (often heterogeneous) body of people. Over the last several decades, an entire elite class of politicians and spokespeople has been used to politically demobilize the communities they claim to represent.

Antiauthoritarian white allies often express that they are working with authoritarian or nonpartisan community groups, sometimes on projects they don’t believe in, because the most important thing is for them to follow the leadership of people of color. The unspoken implication is that there are no antiauthoritarian people of color—or none who are worth working with. Choosing to follow authoritarian people of color in this way invisibilizes anarchist and antiauthoritarian people of color; it also functions to marginalize and suppress efforts from less powerful or influential members of these communities. In this way, white allies diminish the agency and leverage of people of color who disagree with the established, institutionalized groups, reinforcing hierarchies of legitimacy and policing the boundaries of political approach by throwing the weight of their privileges behind those who already have more power. There is at least as broad a range of political ideologies in communities of color as in white communities, but no one would ever assume that there is a single white community or that there are “representatives” capable of speaking for all white people as a whole.

When learning how to appropriately take leadership from those more affected by oppression, activists may seek out the leader of a community not simply because it’s the easiest approach but also because—whether they admit it to themselves or not—they are not just looking for guidance; they also are seeking to legitimize their own political projects and analysis. Sometimes they are looking for legitimacy in the traditional sense by siding with others who carry more mainstream social or political capital. At other times they are seeking the legitimacy of siding with those who offer the most anti-oppression credibility—and the goal of opposing oppression morphs into a strange political competition in which we valorize oppressed identities to such an extent that people strive to be identified as oppressed, or at least to be allied with the “most oppressed.”

As an ally gaining an anti-oppression education, each person learns how they benefit from the oppression of others because of the way our society values certain identities. Allies must come to terms with the fact that they are granted privilege in our society simply because of how they look or where their family comes from—and there is nothing they can do to fully refuse or redistribute those privileges, because they are re-created across society. The knowledge that one has advantages that others can never have, which one has done nothing to deserve, often produces a deep sense of white guilt.

This sense of guilt, coupled with the idea that the only ethical way to act is to take direction from others, can make one feel powerless. The model of ally politics puts the burden of racism on white folks, intentionally flipping the social hierarchies, emphasizing that white allies can never escape this deep inequality, but offering at least a partial absolution for allies who can stick to this script: Listen to people of color. Once you’ve learned enough from people of color to be a less racist white person, call out other white people on their racism. You will still be a racist white person, but you’ll be a less racist white person, a more accountable white person. If nothing else, you’ll gain the ethical high ground over other white people so you can tell them what to do. This model has repeatedly failed to equip would-be allies to do more than seek their own endlessly deferred salvation.

Being an ally has come to mean legitimizing a political position by borrowing someone else’s voice—always acting in someone else’s name without questioning the principle of appropriating others’ struggles. It’s a way of simultaneously taking power and evading personal accountability. The idea of allyship obscures the fact that hidden choices are being made about who is being listened to, inculcating the idea that there is a single “community of people of color” sharing common interests that could be properly represented by leaders, rather than a heterogeneous mass with both overlapping and sometimes deeply contradictory ideas. This repositions the white ally to wield the power of determining who are the most representative and appropriate black and brown voices. And who are white allies to determine who is the most appropriate anything?

On Sunday, July 14, 2013, in response to the acquittal of Trayvon Martin’s killer, and the widespread and consequenceless murder of black and brown youths in our society, our small city witnessed the collision of a rowdy, angry demonstration and a somber, sedentary speak-out. The speak-out was intended to be a space where individuals could give voice to their sorrow and pain, be held by friends and strangers, and find solace in one another. The marching crowd was lively, vocalizing rage with a palpable energy to release. In the short stretch from the plaza to the courthouse, folks of a variety of ages, racial and ethnic groups, and genders found pace in the streets together, resolute in our desire for rebellion on this day of ferocious mourning. The incongruent energies of the two different events met each other abruptly. As the march arrived, small groups tumbled into the awaiting speak-out, meeting and chatting with one another. This suddenly overflowing crowd began situating itself, joining the group on the sidewalk and settling into the street in front of it.

The march was clearly an uninvited disruption, and the friend who was holding the space of the speak-out, a prison abolitionist and organizer from a radical African American cultural organization, was encouraging people to quiet down and move to the sidewalk so the speak-out could continue. Among hesitant attempts to bring the clatter down, the noise of the new crowd slowly began to lower; but rather than giving space for a true silence to settle, a few white allies came to the edge of the sidewalk, physically and verbally corralling people out of the streets and shouting such choice phrases as, “Shut up! Have some respect! You’re all idiots!”

Their comments were pointedly directed to the white folks in the street, though the crowd in the street included people of many races. Did this make them uncertain to how to proceed without clear guidance from a single, united community of color? What does the white ally handbook say you should do when groups of people of color are actively engaged in conflict? In this case, white allies gave preference to the elder, the one with the most important reputation in radical community.

Personally, I think the most respectful thing would have been to get out of the way.

Perhaps these white allies thought that’s what they were doing by addressing their directives solely to the white people in the street. An irritated brigade of bike cops had been tailing the march, however—also nudging folks on to the sidewalk. White allies guilted many demonstrators out of the street, physically attempting to move some people in close proximity to the police officers who were trying to do the same thing—without yet putting their hands on anyone. The effect of this was to leave me and another woman of color isolated in the streets with only the police around us because all our comrades had been pushed away.

After listening to many, many speeches, the crowd began to get restless, though folks didn’t want to disrespectfully leave before the speak-out ended. Some of the folks who had marched from the plaza to the speak-out, including several mothers of youths being held in the jail, rallied the crowd to march to the jail, and a few people continued the speak-out as many folks from the speak-out joined the marching crowd in taking the demonstration out into the night.

Did the black folks at the speak-out need a few young white folks to speak for them? Certainly none of us needed white radicals to do the police’s job for them.

Perhaps the least legible aspect of ally politics is the tendency for people who otherwise seem to aspire to relationships free of domination to try to exert control over others. Is it because when we feel like we occupy the most legitimate or most objectively justified position, it is easy to inflate our sense of righteousness? Or is it that we feel that when we have the most information—or most connec tions—we can make decisions for others better than they can make them themselves? (ick!)

Respecting individual and group autonomy means that we don’t need managers. It means that no matter how well positioned or knowledgeable we believe ourselves to be, we understand that people can communicate and resolve conflicts best when speaking with humility from their own direct experiences. Some of the first skills taught in conflict resolution, facilitation, and de-escalation trainings are how not to speak for others. Good mediators learn that you break trust when you try to represent others without their consent.

During the anti-globalization movement at the turn of the twenty-first century, I often found myself in baffling arguments about “violence” with pacifists or others who self-described as adhering to a strict code of nonviolence. Many of the same folks who argued that we shouldn’t do anything that could hurt someone else’s property consistently yelled at other people so aggressively as to make them feel threatened; some also engaged in emotional manipulation and passive-aggressive maneuvers in meetings and during demonstrations. Several times, I saw “nonviolent” demonstrators physically hurt other protesters, attempting to drag them out of the streets for spray painting a wall or breaking a window.

Why do people feel justified trying to pacify others—even when they know little about them? Such vehement attempts to contain others’ rage and rebellion often needlessly escalate conflict between those who should be able to struggle together, not against each another.

For instance, a few years ago, at a May Day march in our town, an unnecessary conflict erupted out of attempts to negotiate within a large crowd about whether or not some should continue marching in the street without a permit. At least one group of organized undocumented folks asked others to stay out of the streets because they didn’t want to risk arrest. In this minimally policed and low-tension situation, rather than beginning conversations about whether it was possible to create space where some people could be in the street and some could be on the sidewalk, several people shifted immediately into control and management mode, increasing the antagonism and artificially creating two opposing sides.

In retrospect, there were numerous ways that we could have worked through this respectfully with better communication both before and during the march. The conflict brought up important questions about how to navigate multiple risk levels within a single event, build trust that can translate into plans for safety in the streets, and organize exit strategies that accommodate different groups of people. But the communication by some people on behalf of others dramatically escalated the situation.

While the march was still in progress, somehow I was tasked with talking to members of a different organization who work in a nearby neighborhood with undocumented folks. I approached a group of people who were visibly upset that others remained in the streets, and I had a brief but intense interaction with a man I’d never met before. I don’t remember the exact words that we exchanged, but I remember calmly approaching him and asking if we could speak about what was going on. He responded by screaming in my face.

After walking away from that interaction, I turned to a woman from the same organization to try again to see if we could strategize a workable solution. She launched into a tirade about how I must not understand the disproportionate police harassment that people of color—especially undocumented people—would face if the police chose to attack the march that day. With hard-to-veil irritation, I asked her if she had ever personally experienced police violence or had ever spent time in jail. When she answered “no,” I told her how ridiculous it felt for her to make such baseless assumptions about me when I had more stories than I cared to share about police violence in both social and political contexts relating to race and gender. Then I asked her what kind of conversation she expected we could have when she was speaking so stridently about experiences that weren’t even hers. She apologized and said that she would just rather talk after the march was over.

After the march, my housemate told me a story from the day that I can only explain as a temporary loss of perspective. While she was walking in the street with her five-year-old nephew, a mutual friend of ours who was frustratedly trying to redirect everyone off the street and on to the sidewalk approached her. With a bullhorn to her mouth, this friend shouted at my housemate to get out of the street. At this point, my housemate said to me with some confusion and sadness, “I thought she was coming to talk to me, but she didn’t even say hello to me. She didn’t speak my name. She pretended that she didn’t know me. I know she knows who I am, but she acted like I was just a body, separated from our hearts.”

We are told that resistance lies in “speaking truth to power” rather than attacking power materially. We are told by an array of highly trained “white allies” that the very things we need to do in order to free ourselves from domination cannot be done by us because we’re simply too vulnerable to state repression. At mass rallies, we’re replayed endless empty calls for revolution and militancy from a by gone era while in practice being forced to fetishize our spiritual powerlessness." —from the zine Escalating Identity

Revolutionary struggle is indeed radically unsafe. It is a project that can and does mean prison or death for some of us, and it is important to be aware that these risks can intensify based on where people are situated in the matrices of oppression. The concept and role of ally politics, however, has distorted this awareness into a practice of collective policing by would-be managers who are shielded from criticism by the authority of a depersonalized, stereotyped other.

The ally framework individualizes structures of oppression, shifting discussion away from how to attack those structures and emphasizing individual behavior instead. The focus on individual privilege has become such a popular political discourse precisely because it does not necessarily question the structures that create that privilege. It is essential to understand how systematic forms of oppression shape us, but the point is to collectively dismantle the structures of domination that produce and perpetuate those privileges. Individual transformation can only happen concurrently, not prior to this. We all experience fear and doubt, and wanting to relinquish our responsibility for the choices we make is a natural response to those feelings, but we must hold those fears as our own, as we must hold our desires for freedom as our own. When we act on behalf of an imagined “other,” it makes honest communication around tactics, strategy, and solidarity impossible, shattering our relationships and fueling mistrust where there could be affinity. Our relationships are not what we need to be breaking.

Growing up in this culture, we’re taught so much hatred for the parts of ourselves as well as others who are different from the mainstream or dominant culture. We learn what it means to have good hair or a good nose; we’re told our lightest-skinned sibling is the most beautiful; we’re taught shame about the size and shape of our bodies, and about who and what we desire. White supremacy, misogyny, and all the ideologies that create “the other” are at once superficial and incredibly rooted within us.

It is inevitable that as we develop a critical analysis of the various axes of identity—race, gender, class, ability, and more—that we will experience deeply personal and political moments of self-realization about ourselves and our relationships with others as well as the way this culture functions. It is important and positive that we make those kinds of developments in identifying how oppression works, internally and externally. Yet we must not get so caught up in our own self-discoveries that we unthinkingly put those breakthrough moments on others who live daily with the realities we are just beginning to understand.

Trayvon Martin became a symbol for this generation of the normalcy of violence perpetrated against criminalized, black bodies. The events around his death and his murderer’s acquittal were dramatically emotional for many of my younger white friends; it was clearly a moment of realization about something big. In conversations with other friends of color, however, the pain of the unexceptionality of this case was always at the forefront. We all know this is standard treatment for youths of color. A young friend of mine put it best when he said, “Of course I’m mad; I’m always mad at the police. But I don’t know why anyone is surprised. This is how we’re always treated. I just wish those white girls would stop crying and get up.”

Here are a few tips.

Slow down: Don’t try to fix it. Don’t rush to find an answer or act out of your guilt. Remember that many of your comrades have been doing this work for a long time and experience the kind of oppression you’re learning about more acutely than you. It didn’t start with you and isn’t going to end with you.

Keep it internal: Don’t take up too much space with your thoughts and emotions. Be sensitive to the fact that folks are in a variety of places in relation to what you’re working through; don’t force conversations on others, especially through the guise of public organizing.

Write about it: Give yourself the unedited space to feel all the things you need to, but know that it may hurt others if you share your feelings unthinkingly.

Read about it: Look for resources from people of a variety of political ideologies and experiences of identity to challenge yourself and get the widest range of input.

Listen to older people: Listening to stories from your eighty-year-old African American neighbor when you’re working through questions around racism will likely be thought provoking, regardless of their political ideology or your life experience. Don’t underestimate what a little perspective can do for you.

Don’t make your process the problem of your comrades: Be careful not to centralize yourself, your stake in fixing the problem, or your ego. Work it out on your own, and with close friends and mentors.

"All you see are demographics

All you hear is “systems”

Without undressing me down to the sum of my parts you cannot achieve that checking-your-privilege erection.

You defend dogma cuz it’s all you’ve got left

But

Humanity won’t fit into data bars or scripted syllabi

And won’t stick around when you can no longer see it.

Undressing us all with your politics you become the most correct

And also an entity you’d probably hate—could you escape for a moment.

You steal our dignity and undermine our friendship

When the dots connect

And I see you seeing me through the activist gaze.

I’m not the beating heart I feel

Your eyes just reflect a female queer blob of color."

—Rakhee Devasthali




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



"All you see are demographics


All you hear is “systems”


Without undressing me down to the sum of my parts you cannot achieve that checking-your-privilege erection.


You defend dogma cuz it’s all you’ve got left


But


Humanity won’t fit into data bars or scripted syllabi


And won’t stick around when you can no longer see it.


Undressing us all with your politics you become the most correct


And also an entity you’d probably hate—could you escape for a moment.


You steal our dignity and undermine our friendship


When the dots connect


And I see you seeing me through the activist gaze.


I’m not the beating heart I feel


Your eyes just reflect a female queer blob of color."


—Rakhee Devasthali

