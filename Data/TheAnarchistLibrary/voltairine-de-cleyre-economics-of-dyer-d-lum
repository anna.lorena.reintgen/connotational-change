
If Dyer D. Lum were living I doubt whether the articles of Mr. Black, recently copied by the Twentieth Century from the “Australian Workman,” would elicit anything further from him than a hearty laugh. Mr. Lum had a very keen appreciation of the ludicrous and the richness of being classed in company with Victor Yarros as a Communist would have touched what he called his “Sense of ticklety” sufficiently to have compensated him for being subjected to the treatment of such a reviewer. He can, indeed, well afford to be accounted as “lacking in understanding” by this “turgid and tangled” gentleman from New South Wales. It is better to be praised by such a critic’s damnation than damned by his praise.

Mr. Yarros is able to speak for himself, and, if he deem it worth while, will no doubt do so in terms which may clarify Mr. Black’s mind. I do not pretend to more than average discernment, nor do I think more is necessary, to understand Yarros as holding to the completest individualism consonant with equal freedom, both as a political and an economic principle.

There is a difference between Yarros and Lum however, as will be found by contrasting the latter’s “Economics of Anarchy” with the former’s writings in “Liberty.” And as there are, no doubt, many readers of the Twentieth Century who have never seen the book from which Mr. Black quotes, I shall venture to point out wherein the difference lies; not so much for the purpose of clearing Mr. Lum from the charge of inconsistency as showing Mr. Black’s inability to distinguish between a vital issue and a divergence reconcileable with a common starting point.

But first, a brief explanation concerning the style of “Economics of Anarchy.” It is, admittedly, not an A, B, C book. The language is heavy, and to a reader like Mr. Black may very probably appear as if “plainer to the writer than to the reader.” (I have a suspicion, indeed, that all writings are so.) Be that as it may, in the case of Lum’s work the causes were two. In the first place, financial limits made it impossible for him to treat of the subject at length, as much had to be packed into as small a compass as possible. The chapters on land and capital were necessarily crowded into a few pages, and every sentence was “boiled down” till it was thick. The author was compelled to depend on the reader for dilutions.

In the second place, Mr. Lum was, on serious subjects, always a concise writer. A lifelong student, he was as familiar with jawbreaking terminologies as most of us are with the multiplication table. And as one technical term often expresses precisely what half a dozen other words fail to convey with exactness, he naturally chose the former. I distinctly remember his reply to me when I complained that working people could not understand him: “I can’t talk philosophy in unphilosophic language—” (a brilliant but shallow orator) “tries to do it and succeeds—by making an ass of himself.” So much for the alleged turgidity.

In the sentence “Any attempt to institute artificial regulations over production, to limit the free scope of individual action by the organization of groups in which self-elected needs rather than deeds become the governing principle of distribution, is a violation of logical deductions from liberty,” to which sentence Mr. Black objects, that it “begins by attacking ‘artificial regulation over production,’ and concludes by showing that he was really tilting at regulated distribution,” our critic seems not to understand that the second violation is not given as a sequence of the first, but that either of the things, both of which are in the programme of government Communism, and the latter in some phases of so-called free Communism, are violations of liberty. Equal freedom! This is the foundation rock of the Individualists! And the sentence quoted might have been written by any one of them. But Lum preferred the word mutualism to individualism, because he recognized the progress of society not only towards freedom, but towards solidarity. And in Mr. Blacks second quotation, while registering his protest against the authoritative “central bureau” of Socialism, which would create solidarity from the top down, he explains how such solidarity might grow from the bottom up, after the natural process of growth.

And herein lay the principal difference between Lum and the other Individualists, that in discussing economy he laid more stress upon the positive side, gave more weight to the facts presented by Communists than they. Unless I am very much misinformed the so-called “Boston Anarchists” consider the present immense massing of workmen together in shops and factories (a characteristic feature of our present conditions constantly emphasized by Socialists) as an outgrowth of the introduction of steam power and its complicated machinery; that the whole system is therefore liable to be again revolutionized the moment steam is superseded by some superior agent, say electricity, which can be utilized by the workman at home or in small shops, where the slavery of the large factory can give place to the independence of the individual. That all forms of production are passing phases dependent upon circumstances which it is impossible to foresee; and hence wisdom in the matter will content itself by saying laissez faire.

Lum, however, believed that the factory represented not only power and machinery but division of labor and as division of labor appears as a continuous process in all organic life, from protista up, he could hardly conceive a reversal of the law in the case of the social organism. For this reason he laid emphasis upon the coming solidification of industry; and because he did was accused, on the one side, of truckling to Communists, and on the other was claimed as a Communist after his death by the very man who did his best to manoeuvre him out of the editorship of the “Alarm” while living, because of his Individualism—John Most. Possibly Mr. Black may consider this corroborative of his classification of Lum as a Communist; I do not, however, credit Most with stupidity.

With the Mostian exposition of Communism, which sixteen days before his death he declared “logically leads to and rests upon authority,” Lum made no compromise. But between his mutualism and the Communism of Krapotkin the difference is not one of irreconcilable basis, but chiefly one of faith.  That there is a distinct difference between government and social administration, that the former tends always to crystalize existing forms, thus fastening on the living the slavery of the dead, while the latter gives free play to all the plastic elements of society, constantly adapting and readapting itself to changing demands, is something Mr. Black evidently does not see, but which Mr. Lum did. Hence his “boards of administration,” chosen by “natural selection,” not majority vote, having jurisdiction only over affiliated industries, in no wise meddling with affairs they do not understand, and in no wise enforcing their decisions, even within their limits, by legal penalties. Krapotkin’s illustrations in his “Anarchist Communism are very much in point.

In conclusion, my intention has been to show Mr Black’s incompetency to criticise Mr. Lum. That he is equally incompetent to criticise the other Anarchists quoted could be easily demonstrated.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

