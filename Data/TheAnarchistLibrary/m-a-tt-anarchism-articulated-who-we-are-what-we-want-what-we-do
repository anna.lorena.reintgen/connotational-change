      Introduction      Who we are      What we want      What we do      Conclusion
Every election year, and this one in particular, I feel the urgency of articulating an alternative narrative and different ideas about how to make sense of the increasingly-chaotic world around us. With so many of my friends and comrades jumping on the Obama bandwagon, with admirable hope and ambition, I feel at the same time both great sympathy as well as great concern.

Looking at history, I walk away with the unshakeable belief that electoral work does more to stunt the growth of popular upsurges for change than it does to serve them. As I will argue at length, the political system we live under is not set up to be a vehicle for change. It is an investment, or more accurately, a gamble, and because the house makes the rules, ultimately the house always wins. So when a movement of regular people, with scarce resources and energy gets behind an electoral campaign, it is reminiscent of a working class parent with a gambling problem. What I believe is that change is a process that is forced upon the system from the outside, harnessing the greatest strengths we have: our numbers and our creativity.

Anarchism is a global political movement with its roots in the labor struggles of over a hundred years past, and has profoundly influenced many progressive movements since. We have led strikes, revolutions, street actions, and festivals. In that period of time, it would be impossible to capture every idea expressed by all the anarchist thinkers and groups in this small pamphlet; this is merely a snapshot of the thoughts of this one contemporary anarchist activist. The purpose of this is to familiarize a new generation of advocates for change with our politics, not solely to proselytize, but to create a common understanding for mutual cooperation, and dispel any myths or disinformation that may exist.

“If you have come here to help me then you are wasting your time, but if you have come because your liberation is bound up with mine then let us work together.”

— Lila Watson, aboriginal activist

I identify not only with anarchism, but also a very selective interpretation of all the various schools of anarchist thought and radical social theory. I tend to take bits and pieces from each tendency, recognizing that each one brings something unique to the table, but generally I fall within the category of “social anarchism.” I also draw from radical feminism, critical race theory, social ecology, Marxist economics, youth liberation, radical queer and trans praxis, among other things, for the purpose of understanding all the intersections and overlapping of the oppressions that we find in reality. The priority is not to decide who is the most oppressed, but to look at oppressive dynamics and systems objectively, to tailor our methods and processes appropriately, and make strategic interventions where it makes most sense.

This can be done not only at key points of societal tension, but within our own movement as well. Simply adopting the label of “anarchist,” or “revolutionary,” or what have you, does not mean you are suddenly free from all the oppressive and submissive behavioral tendencies we’ve all been socialized with. When we are conscious of these tendencies, we can alter our group process and level the playing field a bit, to ensure that all of the people we are struggling alongside are enfranchised and respected members of the movement, regardless of their age, ethnicity, gender, sexuality, disability or whatever their identity might be. This is done with the recognition that we all carry a part of the answer to the problems we are grappling with. When we can all contribute as free individuals, our collective knowledge thus gives us a more complete picture of the world as it is, as it should be, and how to get there.

So, for example, during meetings we pay attention to who is speaking, for how long, who is not speaking, if they are being prevented from speaking, why, and what can be done to make sure everyone is heard. If men are speaking over women, if whites are being dismissive of the concerns of people of color, if sexist or homophobic language is being used, then clearly we have a space where some are inclined to speak their mind, and others will most likely remain silent or leave. What about the people who can’t make it to meetings? What about working people and/or parents with major time constraints? If meetings drag on without productivity, someone whose free hours are an extremely precious commodity can be very disinclined to participate.

Do we write off these concerns as petty and unavoidable? Or do we work out a process that limits the amount of time a person can speak, prioritize who should be heard first, mandate that timely decisions be made, and even go so far as to provide childcare for parents? If we really want a social revolution, isn’t it reasonable to say that maybe we need to revolutionize our basic interactions with one another? In a society where we are all cut off from one another — arguably by design in order to divide and conquer — we need to start demolishing walls with everything in our arsenal.

But even these accommodations are only a part of the equation. Yes, our process must be all-inclusive, within the context of our agreed — upon politics. But then it only follows that this process must be reflected in our political work. Let’s say for example, we are engaged in a campaign to win union recognition from an employer. Usually the demands center on wages, hours and safety. But if a significant number of the workers are upset about racist or sexist behavior from management, do we brush that aside as divisive or as not a union-related issue? If we do, what are the consequences? Will these workers take the union seriously? Will other workers resent them for not showing enough support for the campaign? It would be impossible to give a blanket solution to all such problems, but I think the best approach is to spend energy on building bridges rather than to repress the concerns of people with whom you share a common cause. While this may seem like simplistic reasoning, in the end, regardless of which way you go there is always a chance of losing the battle; the important thing to remember is that we are at war, and the only thing that will allow us to win is building solidarity over the long haul. As much as anything, anarchism is about building a common bond between all regular people. If the best we can do is to build up institutions, such as unions, based upon the domination of one group or another, then all we are doing is reproducing the very oppressive systems we are seeking to dismantle; such cynicism will surely smother our liberatory impulses.

Like most anarchists, I see nothing particularly enlightened or progressive about representative democracy, and like most anti-capitalists, recognize the social and ecological destruction that is inherent under free market capitalism. Realistically, the government we live under is far better to its subjects than many, if not most other states around the world. However, with a sober appraisal of the history of the United States, not to mention the way it has conducted itself internationally, it is clear that its seemingly-benevolent face has more to do with defusing social upheaval and protecting the status quo (”the state of things”) rather than some kind of altruistic desire to improve the lot of the masses.[1]

I believe strongly in an “us-and-them” dichotomy between those who govern and those who are governed (as has always been the case). There is an entire class of people who exclusively wield the political machinery of this society, at the behest of the wealthy elite. They are generally called politicians, but could also be described as managers, supervisors, school administrators, bureaucrats, academics, small business owners, et cetera. They are sometimes generalized by the term, “middle class,” while some of us call them the “coordinator class.” These are essentially working class people who made enough sacrifices and are granted the privilege of managing others (instead of being saddled with actual productive labor) with the promise that they will uphold the status quo. The faces comprising this class change over time, but the ruling class, directly above them, always controls who takes their place, and whose interests they will serve. Many working people are convinced they are “middle class,” because they may have picked up some skills or accumulated some privileges. But the important distinction to make is that real coordinators don’t actually produce anything, they merely manage labor and wealth. Under bad economic conditions, a working class person can be one job termination away from poverty or homelessness. Coordinators seem to have the uncanny ability to find employment no matter how many times they are fired.

The ruling class owns vast properties, finance political campaigns, and exclusively sit on the boards of the world’s most influential institutions. They own capital, which is wealth and property that they do not personally need or use. They accumulate capital for the sole purpose of wielding it against anyone who would try to take it from them; this is demonstrated by the state’s slavish devotion to upholding property rights, long before it ever considers the human rights of regular people without wealth. Their primary interest is to indefinitely accumulate more wealth and power; wars, trade, and elections all essentially have the same goal in mind, by different means. Working class people, on the other hand, have a tendency to be concerned mainly with raising themselves out of a precarious, miserable existence, second only to subsistence.

In short, you will find that anarchists tend to oppose most of society’s institutions, or at least, believe a radical restructuring is necessary. To me, anarchism means that all authority must justify itself in order to exist, and if it cannot, it should be abolished to make way for new institutions that serve the people, which means they must be accountable to the people they are serving.

It is we the workers who built these palaces and cities here in Spain and in America and everywhere. We, the workers, can build others to take their place. And better ones! We are not in the least afraid of ruins. We are going to inherit the earth; there is not the slightest doubt about that. The bourgeoisie might blast and ruin its own world before it leaves the stage of history. We carry a new world here, in our hearts. That world is growing this minute.

— Buenaventura Durruti, Iberian Anarchist Federation and Anti-Fascist.

Without an alternative, a critique would be nothing more than an exercise in cynicism and depression. Lots of people would probably agree with what I have to say about the state of the world, but tend not to think too much about it because of the overwhelming enormity of it all. The reason why this critique is so all-encompassing is because anarchists are also unique in what we advocate. Fundamentally, we believe in the human potential for self-governance. Drawing on the various schools of thought described above, we must understand that our potential is something that is squandered and battered down from the time we are children, right up until the day we die. Society’s notions of education, history, gender roles, work habits, the value we attach to one another, discipline and structure are all completely backwards, geared solely toward making good workers, “professionals,” or inmates out of us; with a liberatory approach, our full potential can be reached, and all of these things can be radically altered to foster new generations of free-thinking, self-directed revolutionaries.[2]

A common criticism of anarchism is that it’s unworkable, on the basis that a strict division and hierarchy of labor are necessary in order to make society function. But in reality, there is almost no task or job in the economy that is so specialized that anyone else could not be taught to do it. With an equal opportunity to direct one’s own learning and development, all the academic disciplines, trades and the coordinator class are rendered completely impotent and useless, and the entire argument for a hierarchical division of labor collapses like a house of cards.[3] Why would we have specialists when we can do things ourselves? In essence, this is what is considered to be socialism through an anarchist lens. Now clearly there needs to be some level of specialization, what with things such as surgery, but my point is that with equal access to real education, there is almost no basis for exclusion from any job, and therefore, there is no real basis whatsoever for labor shortages or scarcity of any kind. And as I will elaborate on further, management is a task that can easily be carried out by those who are doing the actual productive labor, via election and/or rotation.[4]

Unlike most Marxists, anarchists see nothing potentially liberatory about any state apparatus, whether it’s a socialist government, or a hierarchical political party that would like to control the government. Even in the short-term, we generally consider any orientation toward capturing control of the state to be backwards; it is an institution created by and for the ruling class, for very specific ends; that is, the protection of privilege and controlling property. There is nothing neutral about it, and we believe that the repressive nature of the socialist state reflects that reality.[5] Subjugating popular movements — be it feminist or labor — to the “be-all/end-all” pursuit of state power is not only shortsighted, it is also detrimental to their immediate strength and versatility. To win elections, you must make concessions to the coordinator class. Ultimately, it is the ruling class who determines who enters or leaves power, and this particular government has survived for 219 years by selectively bringing potential rebels to the table, and isolating those in rebellion. In fact, it has built a global empire on this basis.

As an alternative process by which society could function, social anarchists believe in and practice a radical form of democratic process; the core concept is that people should have a direct say in the decisions that affect them, proportionate to how they are affected. Therefore, we consider most hierarchical decision-making bodies to be inherently anti-democratic, regardless of whether or not it was elected, appointed, or imposed itself upon the people. While I am not categorically opposed to electing people to an office in some future society, or even in one of our own organizations, a fundamental principle is that of accountability. If office-holders can unilaterally make decisions, without necessarily having to consult her or his constituents (as in representative democracy) then that would be considered unaccountable and likely to become entrenched power. Case in point, the US political system has an incumbency rate well over 90%.[6] Our preference, where possible, is to appoint recallable, rotating delegates to decision-making bodies; these delegates have varyingly specific mandates, and there may be a number of checks and balances to ensure her or his actions reflect the will of the constituency. It is in this process that we break down the dichotomy between coordinators and the rest, and put the tools of governance into as many hands as possible.

Overall, our alternative is an economically self-managed society that integrates the liberation of women, people of color, youth, queer and gender non-conforming people, sustainable ecology, and strives to understand the entire spectrum of liberatory and oppressive social dynamics, for a self-directed culture. This is not entirely utopian; not only do we practice these principles in our organizations and projects, there are also people all over the world carving out space for a new world in the shell of the old, today, and fighting with all their might to preserve it. While anarchists are constantly experimenting with process and building new institutions that reflect our ideals, indigenous communities frequently share our principles in their centuries-old traditions, and anarchists are often entering into alliances with them.[7]

While ultimately I believe in revolution, my day-to-day work is not some strange fixation upon armed struggle in the immediate future, nor am I part of a millenarian cult that wants your devotion. My co-thinkers and I are activists involved in a variety of struggles, from different backgrounds and experiences, looking to foster and intervene in movements that can increase the autonomous power of oppressed people everywhere. When we talk about autonomy, it’s not meant that we should be isolated from each other. Rather, that our power and freedom should be derived from the unity of people who recognize the things they have in common with one another, and directed by our own collective values, needs and desires. To depend upon a state, or a party, or any other hierarchical institution primarily concerned with preserving its own dominance, is to cede the agency and autonomy from our struggles for self-determination. Anarchists emphasize the consistency of ends and means. Sometimes called “pre-figurative politics,” we recognize that the methods and models we use today have a direct bearing upon the new world we seek to create, as well as movements we are building to make that world a reality. This is less a moralistic view than a practical one; how, after all, are we to create a world so radically different from this one if we don’t get in some practice before the revolution?

In practice, most of our work doesn’t look so different from conventional political groups, particularly compared to youth organizations. In other ways, we have stark differences. While organizations like the Young Democrats of America groom their members for a career in political maneuvering and power consolidation, our projects tend to equip participants with the tools to effectively resist the status quo wherever they go in life. The practice of “pre-figurative politics,” then, is not limited to anarchists (and feminists), though we are fairly unique in our grasp of its importance. Marxists and other leftists tend to ignore the concept entirely, believing that the instrument of mass violence known as the state can be used to make progress; and unlike most leftists, we take the conventional wisdom of the political mainstream (resembling social Darwinism, or maybe more aptly termed, political Darwinism) and flip it on its head with a process that engenders camaraderie and cooperation.

We organize around issues that are immediately important: US military aggression in Iraq and elsewhere; repression on immigrant communities; harassment against women, people of color, trans and queer folk, or anyone else that is seen as, “undesirable,” by dominant opinion. And while oppositional political activity is a time-consuming but necessary use of our energy, we do manage to do productive work as well. Anarchists have historically made great cultural contributions and today are involved in countless musical, theatrical and artistic projects that embody our ideas and create space for popular participation. We tend to organize wherever we find ourselves; if we get a job, we might organize a union; if we’re students, we organize on campus; if we live in an area where there’s a sense of community, we’ll organize with our neighbors. The landscape may change, but the basic ideas are the same. Ultimately, a social anarchist strategy relies upon the long-term building of popular power in industries and communities of oppressed peoples, and this requires more focus than jumping from issue to issue in the hopes of siphoning off new recruits to our cause.

The decision-making process tends toward building consensus and unanimity, coupled with an understanding that in order to be relevant, we need to be active. When we pursue our objectives in the public sphere, we do so as directly and confrontationally as possible. Recognizing that we have nothing to gain from collaboration with entrenched hierarchies, we focus on direct action and disruptiveness as a way to avoid being ignored.

There is substantial evidence that the fear of domestic disruption has inhibited murderous plans. One documented case concerns Vietnam. The Joint Chiefs of Staff recognized the need that ‘sufficient forces would still be available for civil disorder control.’ If they sent troops to Vietnam after the Tet Offensive, and Pentagon officials feared that escalation might lead to massive civil disobedience, in view of the large-scale popular opposition to the war, running the risk of ‘provoking a domestic crisis of unprecedented proportions.’ A review of the internal documents released in the Pentagon Papers shows that considerations of cost were the sole factor inhibiting planners, a fact that should be noted by citizens concerned to restrain the violence of the state. In such cases as these, and many others, popular demonstrations and civil disobedience may, under appropriate circumstances, encourage others to undertake a broader range of conventional action by extending the range of the thinkable, and where there is real popular understanding of the legitimacy of direct action to confront institutional violence, may serve as a catalyst to constructive organization and action that will pave the way to more fundamental change.

— Noam Chomsky, anarchist scholar of politics and linguistics.

In a society where power is very centralized into the hands of a few, our everyday, relative social peace depends upon the masses’ general tolerance of the status quo. Our job, then, is to make it possible for all the various groups of oppressed peoples to express their needs and desires in a way that brings them into a direct (un-armed) conflict with the state. One way of looking at our politics is to contrast the terms, “social peace,” with “social anarchism;” we’re not against a peaceful social order, but you might say that our emphasis is on a just one, and therefore we incite and engage in social conflict. We do this with the recognition that capitalism can never be peaceful; the fact that it is a system which necessitates social hierarchy and unfulfilled basic needs means that it will always ultimately rely upon force or the threat thereof to maintain the current order. So regardless of whether or not we choose to fight back, we are being assaulted on a daily basis. So long as capitalism and hierarchy exist, this will be our reality, which is why they must ultimately be abolished through social revolution. What form it takes is another question altogether, but an old pamphlet from the Industrial Workers of the World (IWW) or the “Wobblies” gives us a hint at an ideal situation:

The answer is that, as the I.W.W. conceives of the General Strike, it would be so perfectly organized by workers and technicians and effectually used that the feeding, supplying and transportation of [counter-revolutionary] armed mercenaries would be practically impossible. The strikes at Seattle and Winnipeg gave some indication of the ability of strikers to organize, picket and police their strike and, at the same time arrange for the adequate distribution of food stuffs to the population. As for machine guns, tanks, airplanes and bombs of asphyxiating or incendiary character, it is well to remember that such things are only available when they are manufactured and transported by labor...

— Ralph Chaplin, “The General Strike,” 1933.

In other words, the better organized you are, the less violence is necessary or possible.

The best-known example of anarchist resistance to the status quo was the 1999 mobilization against the World Trade Organization (WTO) in Seattle, WA. While a number of anarchists engaged in targeted property destruction, the main emphasis was on shutting down the entire downtown area where the WTO was holding its meetings. The latter of these tactics included street blockades, with approaches including both non-violent civil disobedience, as well as fighting back against police repression, and were carried out very much along anarchist or “anti-authoritarian” organizational models, as described above. As tens of thousands of people from all walks of life poured into the city — workers, students, poor farmers, queers, people of color, environmentalists and more — the state quickly realized that it was going to lose the “Battle of Seattle,” as it became known, which it finally did when the WTO meetings collapsed due to corporate delegates’ inability to do so much as move from one building to another, because the people were in control of the streets. Eventually, the National Guard was called in at the infuriated demands of then — Secretary of State Madeleine Albright and Attorney General Janet Reno, martial law was declared, and the world looked on in wonder as the most powerful government on the planet was reduced to declaring war on its own people. While many were injured and/or jailed, no one was killed and no one was convicted on serious charges, due to the creativity and solidarity of the people on the street and behind the scenes.[8] Whether it’s in the streets or at work or in your neighborhood, direct action is a way to get things done without going through official channels that are otherwise corrupt, or at best, inefficient. Not only is it practical, but it is also through direct action that we begin to recognize the unstable, weak position of the current power structures, and on the flip side, our own strength and potential.

Understanding our role in fomenting militant social movements as a path to revolution, the specific form my anarchism takes has to do with how best to interact with the rest of the world, specifically people who are struggling to improve their lives. It is through involvement in these struggles that we can become relevant to people outside our circles, and more importantly, to the people who will ultimately be the only ones who can make our vision for a better world a reality. Unlike many liberal and Marxist groups, the goal is not to become the center of the movement, or to force it in any one direction or the other. I do, however, very much make my views known, argue for them in discussions about strategy, and open up space for radical thought and militant action where there is none already. And again, radical democratic process is at the center of our orientation, and so this is something I push for in all activist work.

In the course of creating a new social order, there are inevitably people of other persuasions who want to see things move in a different direction. Some have very different ideas about what is and is not principled behavior, and sometimes, said parties are not as well-meaning as others. Therefore, I believe the final role of social anarchists is to minimize the influence of a minority over the majority; specifically fascists, liberal politicians, and some Marxist-Leninists of authoritarian persuasions on a case-by-case basis. It is my conviction that the inability of anarchists and similar revolutionaries to stem the influence of these factions has been the downfall of almost every modern revolution in history. On the other hand, we should be very careful not to exert undue influence over social movements beyond what has been described above.

With these tasks in mind, the question for social anarchists then is how to best organize ourselves, specifically as a grouping of anarchists involved in larger struggles that generally do not define themselves as “anarchist.” The inclination of many anarchists, even those who are very close to my thinking, is to form a grouping as loose as possible, while neglecting efficiency, self-discipline and unity. So on the one hand, I prioritize having a well-defined process and division of tasks (on a rotating basis) but on the other, I know that our ideas are constantly evolving, and to ensure that the group continues to exist as a force for good, we must cooperatively shape our understanding of the world around us. While independent thought and creativity need to be encouraged, we also must constantly discuss all the different political and theoretical questions concerning us in order to maintain our unity in action. Otherwise, we may wake up one day and find we have little in common, and no basis for collective activity.

I wanted a roof for every family, bread for every mouth, education for every heart, light for every intellect. I am convinced that the human history has not yet begun — that we find ourselves in the last period of the prehistoric. I see with the eyes of my soul how the sky is diffused with rays of the new millennium.

— Bartolomeo Vanzetti, Italian anarchist executed by the State of Massachusetts.

To be a revolutionary in the United States today, means that your history is constantly bearing down upon you, and the future is always coming at you too fast. As a result, we tend to be awfully... anxious much of the time. At the very least I know I am. But the most important part of this to grasp is recognizing your own power and agency. The dominant narrative says that we are all subjects of history, tossed about by forces beyond our control, or at best, some of us may rise to lead the nation to greatness by standing on the shoulders of the unwashed masses. In my mind, we are all potential agents for change, and the whole “Great Men of History” narrative is complete fallacy, be it Left, Right or Center. We can be agents of change in society, yes, but perhaps most importantly, change in the people around us. Societal change takes decades, but the up-close changes you see in the people you struggle alongside, that is something that is a testament to human potential, which is what this is really all about.

 
[1] Fresia, Jerry. Toward an American Revolution. Cambridge, Ma.: South End Press, 1988.
[2] Mercogliano, Chris. Making It Up as We Go Along: The Story of the Albany Free School. Portsmouth, NH.: Heinemann, 1998.
[3] Albert, Michael. Parecon: Life After Capitalism. New York.: Verso, 2003.
[4] Anonymous. “Common Sense Reasons for Worker Self-Management.” zinelibrary.info
[5] Goldman, Emma. My Disillusionment in Russia. New York. : Crowell, 1970.
[6] Malbin, Michael J. Life After Reform: When the Bipartisan Campaign Reform Act Meets Politics. Lanham, Md.: Rowland & Littlefield, 2003.
[7] Andalusia. “Bolivian Anarchism and Indigenous Resistance.” www.leftturn.org October 14, 2007.
[8] Starhawk. “How We Really Shut Down the WTO.” www.starhawk.org December, 1999.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“If you have come here to help me then you are wasting your time, but if you have come because your liberation is bound up with mine then let us work together.”


— Lila Watson, aboriginal activist



It is we the workers who built these palaces and cities here in Spain and in America and everywhere. We, the workers, can build others to take their place. And better ones! We are not in the least afraid of ruins. We are going to inherit the earth; there is not the slightest doubt about that. The bourgeoisie might blast and ruin its own world before it leaves the stage of history. We carry a new world here, in our hearts. That world is growing this minute.


— Buenaventura Durruti, Iberian Anarchist Federation and Anti-Fascist.



There is substantial evidence that the fear of domestic disruption has inhibited murderous plans. One documented case concerns Vietnam. The Joint Chiefs of Staff recognized the need that ‘sufficient forces would still be available for civil disorder control.’ If they sent troops to Vietnam after the Tet Offensive, and Pentagon officials feared that escalation might lead to massive civil disobedience, in view of the large-scale popular opposition to the war, running the risk of ‘provoking a domestic crisis of unprecedented proportions.’ A review of the internal documents released in the Pentagon Papers shows that considerations of cost were the sole factor inhibiting planners, a fact that should be noted by citizens concerned to restrain the violence of the state. In such cases as these, and many others, popular demonstrations and civil disobedience may, under appropriate circumstances, encourage others to undertake a broader range of conventional action by extending the range of the thinkable, and where there is real popular understanding of the legitimacy of direct action to confront institutional violence, may serve as a catalyst to constructive organization and action that will pave the way to more fundamental change.


— Noam Chomsky, anarchist scholar of politics and linguistics.



The answer is that, as the I.W.W. conceives of the General Strike, it would be so perfectly organized by workers and technicians and effectually used that the feeding, supplying and transportation of [counter-revolutionary] armed mercenaries would be practically impossible. The strikes at Seattle and Winnipeg gave some indication of the ability of strikers to organize, picket and police their strike and, at the same time arrange for the adequate distribution of food stuffs to the population. As for machine guns, tanks, airplanes and bombs of asphyxiating or incendiary character, it is well to remember that such things are only available when they are manufactured and transported by labor...


— Ralph Chaplin, “The General Strike,” 1933.



I wanted a roof for every family, bread for every mouth, education for every heart, light for every intellect. I am convinced that the human history has not yet begun — that we find ourselves in the last period of the prehistoric. I see with the eyes of my soul how the sky is diffused with rays of the new millennium.


— Bartolomeo Vanzetti, Italian anarchist executed by the State of Massachusetts.

