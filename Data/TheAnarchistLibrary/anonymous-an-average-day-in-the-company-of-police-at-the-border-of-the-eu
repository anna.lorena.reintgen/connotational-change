
“Information is the oxygen of the modern age. It seeps through the walls topped by barbed wire, it wafts across the electrified borders.”  Ronald Reagan

After landing in Paris the police woman sitting in a glass box scanned my passport and ushered me past her without a word. She then turned her attention to the next traveler in line. Having a criminal record in multiple country’s in the EU and having been deported twice had almost made me accustomed to the nervousness I feel whenever having to deal with les flics (French for cops) in any country. So I immediately felt a rush of relief and thought, good I am in, as I walked to the baggage pick up area. Standing there waiting for my bag I then felt a firm tap on my shoulder, already knowing who this was and what this was about, I turned in dread to face the same cop who had scanned my passport. Realizing at this point that my planned trip was over didn’t help to calm my nerves or keep my hands from sweating. She told me to hand her my passport and follow her. I followed her back to her station and she double checked my passport with whatever was on her screen. With a bit of a surprised look she scanned me up and down and shrugged her shoulders as if to say “well the computer can’t be wrong” then quickly motioned me to follow her. She sat me down in a police station and returned to her duties as an extension of some kind of giant mechanical mollusk filtering the flow of humans into the EU.

“You will never understand bureaucracies until you understand that for bureaucrats procedure is everything and outcomes are nothing.”  Thomas Sowell

Across from me were two tables with computers and stacks of papers and to the left of me was a filling cabinet the size of a entire wall each drawer labeled with a code of numbers and letters. There were four young male cops in the room, three seemed to be doing nothing but joking with each other while one was frantically typing on the computer, stamping and stapling documents, making photo copies and appropriately filing them in the giant cabinet. More than once the flic who was working would lose some document and go frantically searching through the piles of papers on the desk while simultaneously asking other officers if they had seen it laying around. Occasionally he was chided by his peers and in response pointed to his uniform which had three stripes to their one or two. I thought it was sweet in a sick cynical sort of way that I could actually see the wheels of bureaucracy turning.

“The democratic mechanism of citizenship and rights, how wide that might be, will always presuppose the existence of excluded people.”  Some Italian Anarchists from the text “To The Wanderers”

Almost immediately a hulk of a man who I recognized as being on my flight was brought in and told to sit next to me. He was only slightly taller than me, had small shoulder length black braids, a small mustache, shoulders the width of two or three people, and a distinctly American accent. He introduced himself as Theo and I introduced myself, he proceeded to ask about my situation. I explained without any detail of my history that I believed it was my criminal record in another EU country that was the cause of my problems. He began to tell me his situation. He was born in South Africa but had relatives in a suburb of Chicago and immigrated there at a young age. Being the only black family in the area he often went into the city and learned from friends how to play American football. He had two passions in life computer engineering and football and was eventually drafted to the Seattle Seahawks, After a short career as a professional football player, he began work for a company that invented some military grade encryption. Already near the beginning of the Iraq war the US Army had ran out of technicians to set up all of the military communications centers around the world that they wanted. They went to the private sector, he fit this need and so was flown around the world to set up these centers all the while only having a green card (a card issued to foreign nationals permitting them to work in the U.S). He was paid well and promised American citizenship for his patriotic work. Eventually his services were no longer needed and he went to South Africa to see family, then tried to return to the US. But things got complicated at the US border the Homeland Security cops wanted to know why he had been flying to all these dodgey places, he explained, and they for obvious reasons didn’t believe a word. He was put in deportation prison for five months before the bureaucracy had sorted itself out. He was released but never received citizenship. He still always has troubles at borders. Unsurprisingly he no longer wants to be a American citizen and hates the war, cops and bureaucrats. Eventually he was allowed in France and I was moved to different police station inside the airport.

“...millions of exploited people roam through the hell of the commercial heaven, jolted from border to border, forced in refugees camps surrounded by the police and the army, handled by the so called charity organizations.....piled up in “waiting zones” in the airports or in stadiums ...., locked up in Lagers called “detention centers, and last wrapped up and expelled in the most total indifference. For many reasons we can say that the face of these unwelcome is the face of our present — and that’s also why we’re afraid of them. We’re scared by immigrants because in their misery we can see the reflection of our own misery, because in their wanderings we recognize our daily condition: the condition of persons that feel more and more like strangers both to this world and to themselves”  Some Italian Anarchists from the text “To The Wanderers”

In this station I was set to wait behind another desk, very soon after, two frightened 11 or 12 year old looking kids were brought in, a boy and a girl. It became immediately clear that they spoke no French or English and had no papers. A young pig with a bored look on his face nonchalantly locked them in a cell, went to sit behind the desk and began to read a paper and drink coffee. Two young female pigs entered the station laughing and smiling showing off a autographed soccer jersey that I guess they had found. The atmosphere made me think of the title of a essay by Hannah Arendt “Eichmann in Israel: the banality of Evil” Even though I have had anarchist ideas for a long time I was still shocked by the fact that humans (it’s sad to admit but after all cops are human) seemed to have become so used to dolling out repression that locking children in a cell simply for having no papers was a unremarkable everyday occurrence that evoked no emotion other than apathy, no physical reaction other than a yawn. Luckily (I guess?) I was only held for a few more hours until two cops came to escort me through the airport and give my passport to the pilot with the reminder not to give it to me until the plane had landed.

Having serious problems at borders is a common experience for thousands of the excluded every day around the world. Later I realized all in all the border seemed typical, characteristic, and almost clichéd in that I dealt with archetypal brainless and bored young cops, met a once patriotic victim of racism and the giant bureaucracy of the state, and observed how with not more thought than scratching his own ass a pig can lock up two children simply for not having papers.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“Information is the oxygen of the modern age. It seeps through the walls topped by barbed wire, it wafts across the electrified borders.”  Ronald Reagan



“You will never understand bureaucracies until you understand that for bureaucrats procedure is everything and outcomes are nothing.”  Thomas Sowell



“The democratic mechanism of citizenship and rights, how wide that might be, will always presuppose the existence of excluded people.”  Some Italian Anarchists from the text “To The Wanderers”



“...millions of exploited people roam through the hell of the commercial heaven, jolted from border to border, forced in refugees camps surrounded by the police and the army, handled by the so called charity organizations.....piled up in “waiting zones” in the airports or in stadiums ...., locked up in Lagers called “detention centers, and last wrapped up and expelled in the most total indifference. For many reasons we can say that the face of these unwelcome is the face of our present — and that’s also why we’re afraid of them. We’re scared by immigrants because in their misery we can see the reflection of our own misery, because in their wanderings we recognize our daily condition: the condition of persons that feel more and more like strangers both to this world and to themselves”  Some Italian Anarchists from the text “To The Wanderers”

