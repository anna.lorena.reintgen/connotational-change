      Awakening of Labor on the Eve of the International.      The Central Sections.      Central Sections Are Mere Ideological Groupings.      The Central Sections in Themselves Would be Powerless to Draw in Great Masses of Workers.      The Empirical Approach of Workers to Their Problems.      Concrete Statement Offers the Only Effective Approach to the Great Mass of Workers.      Solidarity of Trade Union Members Rooted in Actuality.      Internationalism Growing Out of Actual Experiences of Proletarian struggles.      Internationalism Issues from the Living Experiences of the Proletariat.      The Necessary Historic Premises of the International.
In 1863 and 1864, the years of the founding of the International, in nearly all of the countries of Europe, and especially those where modern industry had reached its highest development — in England, France, Belgium, Germany, and Switzerland — two facts made themselves manifest, facts which facilitated and practically made mandatory the creation of the International. The first was the simultaneous awakening in all the countries of the consciousness, courage, and spirit of the workers, following twelve or even fifteen years of a state of depression which came as a result of the terrible debacle of 1848 and 1851. The second fact was that of the marvelous development of the wealth of the bourgeoisie and, as its necessary accompaniment, the poverty of the workers in all the countries. This was the fact which spurred these workers to action, while their awakening consciousness and spirit endowed them with the essential faith.

But, as it often happens, this renascent faith did not manifest itself at once among the great masses of the European workers. Out of all the countries of Europe there were only two — soon followed by others — in which it made its first appearance. Even in those privileged countries it was not the whole mass but a small number of little, widely scattered workers’ associations which felt within themselves the stirrings of a reborn confidence, felt it strongly enough to resume the struggle; and in those associations it was at first a few rare individuals, the more intelligent, the more energetic, the more devoted among them, and in most cases those who already had been tried and developed by previous struggles, and who, full of hope and faith, mustered the courage to take the initiative of starting the new movement.

Those individuals, meeting casually in London in 1864, in connection with the Polish question — a problem of the highest political importance, but one that was completely alien to the question of international solidarity of labor-formed, under the direct influence of the founders of the International, the first nucleus of this great association. Then, having returned to their respective countries — France, Belgium, Germany, and Switzerland — the delegates formed nuclei in those lands. That is how the initial Central Sections (of the International) were set up.

The Central Sections do not represent any special industry, since they comprise the most advanced workers in all kinds of industries. Then what do those sections represent? They represent the idea of the International itself. What is their mission? The development and propagandizing of this idea. And what is this idea It is the emancipation not only of workers in such and such an industry or in such and such a country, but of all workers in all industries — the emancipation of the workers of all the countries in the world. It is the general emancipation of all those who, earning, with difficulty their miserable livelihood by any productive labor what ever, are economically exploited and politically oppressed by capital, or I rather by the owners and the privileged brokers of capital.

Such is the negative, militant, or revolutionary power of this idea. And the positive force? It is the founding of a new social world, resting only upon emancipated labor and spontaneously created upon the ruins of the old world, by the organization and the free federation of workers’ associations liberated from the economic and political yoke of the privileged classes.

Those two aspects of the same question, one negative and the other positive, are inseparable from each other.

The Central Sections are the active and living centers where the new faith is preserved, where it develops, and where it is being clarified. No one joins them in the capacity of a special worker of such and such a trade with the view of forming any particular trade union organizations. Those who join those sections are workers in general, having in view the general emancipation and organization of labor, and of the new social world based on labor. The workers comprising the membership of those sections leave behind them their character of special or “real” workers, presenting themselves to the organization as workers “in general.” Workers for what? Workers for the idea, the propaganda and organization of the economic and militant might of the International, workers for the Social Revolution.

The Central Sections represent an altogether different character from that of the trade sections, even being diametrically opposed to them. Whereas the latter, following a natural course of development, begin with the fact in order to arrive at the idea, the Central Sections, following, on the contrary, the course of ideal or abstract development, begin with the idea in order to arrive at the fact. It is evident that in contradistinction to the fully realistic or positivist method of the trade sections, the method of the Central Sections appears to be artificial and abstract. This manner of proceeding from the idea to the fact is precisely the one used by the idealists of all schools, theologians, and metaphysicians, whose final impotence has by now become a matter of historical record. The secret of this impotence lies in the absolute impossibility of arriving at the real and concrete fact by taking the absolute idea as the starting point.

If the International Workingmen’s Association were made up only of Central Sections, undoubtedly it would never attain even one hundredth part of the impressive power upon which it is priding itself now. Those sections would be merely so many workers academies where all questions would perpetually be discussed, including of course the question of organization of labor, but without the slightest attempt being made to carry it into practice, nor even having the possibility of doing it...

...If the International were made up only of Central Sections, the latter probably would have succeeded by now in forming conspiracies for the overthrow of the present order of things; but such conspiracies would be confined only to mere intentions, being too impotent to attain their goal since they would never be able to draw in more than a very small number of workers — the most intelligent, most energetic, most convinced and devoted among them. The vast majority, the millions of proletarians, would remain outside of those conspiracies, but in order to overthrow and destroy the political and social order which now crushes us, it would be necessary to have the co-operation of those millions.

Only individuals, and a small number of them at that, can be carried away by an abstract and “pure” idea. The millions, the masses, not only of the proletariat but also of the enlightened and privileged classes, are carried away only by the power and logic of “facts,” apprehending and envisaging most of the time only their immediate interests or moved only by their monetary, more or less blind, passions. Therefore, in order to interest and draw the whole proletariat into the work of the International, it is necessary to approach it not with general and abstract ideas, but with a living tangible comprehension of its own pressing problems, of which evils the workers are aware in a concrete manner.

Their daily tribulations, although presenting to a social thinker a problem of a general character and being actually only the particular effects of general and permanent causes, are in reality infinitely diverse, taking on a multitude of different aspects, produced by a multitude of transitory and contributory causes. Such is the daily reality of those evils. But the mass of workers who are forced to live from hand to mouth and who find hardly a moment of leisure in which to think of the next day, apprehend the evils from which they suffer precisely and exclusively in the context of this particular reality but never or scarcely ever in their general aspect.

It follows then that in order to touch the heart and gain the confidence, the assent, the adhesion, and the co-operation of the illiterate legions of the proletariat — and the vast majority of proletarians unfortunately still belong in this category — it is necessary to begin to speak to those workers not of the general sufferings of the international proletariat as a whole but of their particular, daily, altogether private misfortunes. It is necessary to speak to them of their own trade and the conditions of their work in the specific locality where they live; of the harsh conditions and long hours of their daily work, of the small pay, the mean ness of their employer, the high cost of living, and how impossible it is for them properly to support and bring up a family.

And in laying before them the means to combat those evils and to better their position, it is not necessary at all to speak to them at first of the general and revolutionary means which now constitute the program of action of the International Workingmen’s Association, such as the abolition of individual hereditary property and the collectivization of property the abolition of the juridical right and that of the State, and their replacement by the organization and free federation of producers’ associations The workers, in all probability, would hardly understand all that. It also is possible that, finding themselves under the influence of the religious political, and social ideas which governments and priests have tried to implant in their minds, they will turn away in anger and distrust from any imprudent propagandist who tries to convert them by using such arguments.

No, they should be approached only by way of holding up before them such means of struggle the usefulness of which they cannot fail to comprehend hend, and which they are prone to accept upon the promptings of their good sense and daily experience. Those first elementary means are, as we already have said, the establishing of complete solidarity with their fellow-workers in the shop, in their own defense and in the struggle against their common master; and then the extension of this solidarity to all workers in the same trade and in the same locality in their joint struggle against the employers — that is, their formal entrance as active members into the section of their trade, a section affiliated with the International Workingmen’s Association.

The economic fact, the conditions in a special industry and the particular conditions of exploitation of that industry by capital, the intimate and particular solidarity of interests, of needs, sufferings, and aspirations which amongst all workers who are members of the same trade section — all that forms the real basis of their association. The idea comes afterward as the explanation or the adequate expression of the development and the mental reflection of this fact in the collective consciousness.

A worker not need any great intellectual preparation to become a member of trade union section [of the International] representing his trade. He is a member of it, in quite a natural way, before even being aware of it. All he has to know is that he is being worked to death and that this killing work, so poorly paid that he has hardly enough to provide for his family, enriches his employer, which means that the latter is his ruthless exploiter, his tireless oppressor, his enemy, his master, toward whom he owes no other feeling but that of hate and the rebelliousness of a slave, to give place much later, after he has vanquished the employer in the final struggle, to a sense of justice and a feeling of brotherhood toward the former employer as one who is now a free man.

The worker also must realize — and this is not difficult for him to understand that by himself he is powerless against his master and that to prevent his being utterly crushed by the latter, he must first unite with his fellow-workers in the shop, and be loyal to them in all the struggles arising there against the master.

He also must know that merely a union of workers in the same shop is not sufficient, that it is necessary that all the workers in the same trade employed in the same locality should unite. Once he realizes this — and if he is not exceedingly stupid, his daily experience will teach him as much as that — he consciously becomes a devoted member of his corporative section. The latter already exists as a matter of fact, but it is still devoid of international consciousness, it is still only a local fact. The same experience, at this time collective, will soon overcome in the consciousness of the least intelligent worker the narrow limits of exclusively local solidarity.

There comes a crisis, a strike. The workers in a certain locality belonging to the same trade make common cause, demanding from their employers a wage increase or a reduction of hours of work. The employers do not want to grant those demands; and since they cannot do without workers, they bring them from other localities or other provinces of the same country or even from foreign countries. But in those countries the workers work longer hours for less pay; and the employers there can sell their products cheaper, successfully competing against countries where workers working less earn more, and thus force the employers in the latter countries to cut wages and increase the hours of their workers.

Hence it follows that in the long run the relatively tolerable position of the workers in one country can be maintained only on condition that it be more or less the same in other countries. All this repeats itself too often to escape the attention of even the most simple-minded workers Then they come to realize that in order to protect themselves against the ever-growing exploitation by the employers, it is not enough to organize solidarity on a local scale, but that it is necessary to unite the workers of the same trade not in one province only — and not even in just one country — but in all countries, and above all in those countries which are inter-linked by commercial and industrial ties. When the workers come to realize all this, then an organization will be formed not only on a local nor even on a national scale, but a truly international organization embracing all the workers in a given trade.

But this is not yet an organization of workers in general, it is only an international organization of a single trade. And in order that non-educated workers realize and recognize the actual solidarity existing among all the trade unions of all the countries of the world, it is necessary that the other workers, intellectually more developed than the rest and having some knowledge of economic science, should come to their aid. Not that the ordinary worker lacks daily experience in that respect, but the economic phenomena through which this solidarity manifests itself are exceedingly complex, so that their true meaning may be above the comprehension of the unenlightened worker.

If we assume that international solidarity has been established in a single trade while lacking in the others, it follows that in this organized industry wages will be higher and hours of work shorter than in all other industries And it having been proven that because of the competition of employers and capitalists, the source of real profits of both is the comparatively low wages and the long hours imposed upon workers, it is clear that in the industry in which the workers are organized along international lines, the capitalists and the employers will earn less than in all the others, as a result of which the capitalists will gradually transfer their capital and credit, and the employers their exploiting activity, into the less organized or altogether unorganized branches of industry.

This will necessarily lead to a falling off in the demand for labor in the internationally organized industry, which will naturally result in a worsening of the situation of the workers in that industry, who will have to accept lower wages in order not to starve. Hence it follows that conditions of labor cannot get worse or better in any particular industry without immediately affecting the workers in other industries, and that workers of all trades are interlinked with real and indissoluble ties of solidarity.

This solidarity has been proven by science as well as by experience — science for that matter being simply universal experience, clearly expressed, systematically and properly explained. But solidarity manifests itself in the workers’ world by a mutual, profound, and passionate sympathy, which, — in a measure that economic factors and their political and social consequences keep on developing, factors telling more and more distressingly upon the workers of all trades — grows and becomes ever more of an intense passion with the proletariat.

The workers in every trade and in every country; owing on one hand to the material and moral support which in the course of their struggle they find among workers in other trades and other countries, and on the other hand, because of the condemnation and the systematic, hate-breathing opposition with which they meet not only from their own employers but also from employers in other, even very remote industries, and from the bourgeoisie as a whole — become fully aware of their situation and the principal conditions necessary to their emancipation. They see that the social world is in reality divided into three main categories:

The countless millions of exploited workers;

A few hundred thousand second — or third-rank exploiters;

A few thousand, or, at the most, a few tens of thousands of the larger beasts of prey, big capitalists who have grown fat on directly exploiting the second category and indirectly the first category, pocketing at least half the profits obtained from the collective labor of humanity.

As soon as the worker takes note of this special and abiding fact, he must soon realize, backward though he may be in his development, that if there is any means of salvation for him, it must lie along the lines of establishing and organizing the closest practical solidarity among the proletarians of the whole world, regardless of industries, or countries, in their struggle against the exploiting bourgeoisie.

Here then is the ready framework of the International Workingmen’s Association. It was given to us not by a theory born in the head of one or several profound thinkers, but by the actual development of economic facts, by the hard trials to which those facts subject the working masses, and the reflections, the thoughts, which they naturally engender in the minds of the workers.

That the International Association could come into existence it was necessary that the elements involved in its making — the economic factors, the experience, strivings, and thoughts of the proletariat — should already have been developed strongly enough to form a solid base for it. It was necessary that there already should have been, in the midst of the proletariat, groups or associations of sufficiently advanced workers who, scattered throughout the world, could take upon themselves the initiative of the great emancipatory movement of the workers. Following that comes, of course, the personal initiative of a few intelligent individuals fully devoted to the cause of the people.

It is not enough that the working masses come to realize that international solidarity is the only means of their emancipation; it also is necessary that they have faith in the real efficacy and certainty of this means of salvation, that they have faith in the possibility of their impending deliverance. This faith is a matter of temperament, collective disposition, and mental state. Temperament is given to various peoples by nature, but it is subject to historic development. The collective disposition of the proletarian is always a two-fold product: first, of all preceding events, and then, especially, of his present economic and social situation.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



The countless millions of exploited workers;



A few hundred thousand second — or third-rank exploiters;



A few thousand, or, at the most, a few tens of thousands of the larger beasts of prey, big capitalists who have grown fat on directly exploiting the second category and indirectly the first category, pocketing at least half the profits obtained from the collective labor of humanity.

