
I have often wondered why, with millions of people taking part in progressive and labor movements of all kinds, comparatively few accept Anarchism fully as we do. What is better known than the exploitation of labor by capital, the oppression of the individual by the State, to the student the least interested in social matters and to the practical observer of everyday life? Again, if Anarchist propaganda has not yet touched every remote place in all countries, there are numerous localities where it has been carried on for a generation and more, and even there it does not affect more than a certain proportion of the people. As long as I believed in unlimited possibilities of education and agitation, the fact stated was incomprehensible and disappointing to me. Some reasoning and observation led to an explanation satisfactory to me, which I now venture to place before others, cager to hear their opinion with regard to it.

What constitutes, after all, the essence of Anarchism? In all living organisms, to begin with the lowest, we notice three tendencies: that of appropriating and assimilating such surrounding matter which is most conducive to the well-being of the organism; that of extending its own sphere of action by expansion, overcoming obstacles whenever possible; and finally the strictest operation of heredity, surroundings, etc., tending to more and more differentiate organisms as generations pass on. In mankind these three tendencies take the form of the desire for material well-being in the largest sense the desire for freedom, and the development of individuality, private, personal life replacing more and more the social, gregarious life of earlier times. Anarchism is the goal of this evolution, namely: the greatest amount of freedom and well-being made accessible to each individual in the particular form which will best harmonize with his individuality and enable it to reach the highest possible degree of perfection.

Anarchy, then, would be the state of things where each one reached the greatest happiness he would be capable to feel. It is useless to dream over the economic and other bases of such a society, as there would necessarily be as many systems or ways of arranging matters as there will be individuals. Not only would this, and this alone, correspond to the practical wants of free men and women, but during the long period of winning over the more recalcitrant part of the population to Anarchism, the earlier Anarchists will not remain stationary and stagnant, but will march forward on their own part. Thus a state of equal development of all and corresponding equal economic, moral, etc., arrangements can never exist in the future — no more than they ever existed in the past or exist now.

These considerations, relating to future times which may yet he distant, lead me to have a closer look at men as they really are at present. They are all different, and, with the exception of those who are victims of the crimes of past and present ages against their natural development, on the way to further differentiation. The craving for well-being and freedom is active in all but in each person in a different degree and proportion; moreover, surroundings, heredity, age and an infinite number of other causes, intensify these different degrees while some causes tend to attenuate the difference: similar material position, suggestion and persuasion, etc.; it is here where educational propaganda steps in, trying to create common feelings of solidarity, of enthusiasm of sacrifice, — and fortunately it often succeeds in tearing a man from his isolation and enabling him to extend either his well-being, his freedom or his individuality by combining with others.

Even then however, the particular proportion and degree in which a man cares for increasing his well-being and his freedom will determine in the end to what extent he accepts the demands of solidarity and sacrifice which his new ideas impress on him. No one can give more than is in him and while his natural disposition will carry A to the highest degree of self-sacrifice, B will live along quietly, loving on, helping a little to the limited extent of his abilities; it may indeed be possible in exceptional moments by exceptional means, to rouse B to actions of the A, type, but he will soon relapse into his relative apathy, which is not his fault but the result of his disposition. Because these exceptional effects of suggestion etc., occur, we are too easily persuaded that education and agitation can to some considerable extent equalize natural differences of disposition. In reality however, even among those who accept Anarchism, have any amount of varieties — all are different, in fact the moment they think and act for themselves.

Moreover whilst an Anarchist of the golden age real Anarchy will be the most harmonious and developed person conceivable, an Anarchist in our fighting days is from the beginning forced to cease to care much for his well-being and the cultivation of his individuality and feels but inclined to manifest his desire for freedom by pulling down the prison walls of authority which crush all of us. This fighting attitude requires a certain mentality which all do not possess who otherwise care for freedom; as they lack very often other opportunities to manifest themselves, considerable influence that might advance the cause of Anarchism is lying waste. To be an Anarchist in our times requires, thus, a more than ordinary love of liberty, and our numbers will increase when these requirements of personal sacrifice once may become smaller.

For at present the desire for well-being weighs much stronger in the scale with the millions of organized workers who try before all to better their material position and who will tell you that they “cannot afford” to look out for freedom at the same time. That a commercial age should have created this spirit of caring first for material advantages is as inevitable, unfortunately as that ages of State oppression should have created that modern feeling of indifference against oppression the moment it is disguised by the veil of parliamentary government. I fear even that the wish of the great mass of the workers to have their revenge on society, which so long deprived them of everything, will make them hard masters in their turn perpetuating class rule and authority just as the bourgeois after paralyzing feudalism did not inaugurate liberty but a class rule of their own; these tendencies are likely to overrule the efforts of earnest but not very numerous Socialists to establish their new society, What could Anarchists do against this action of immense masses over whom they have no control, who relegate the desire for freedom to the background? Evidently, they could only continue their present work, which will then be as useful and necessary to rouse the slumbering forces of freedom and to expose and combat authority as it is now.

Socialists of the older type are in a similar position; after a century of propaganda they fluid that the overwhelming majority of their own followers cares for little more than some economic improvements which they expect to get without an), serious effort of their own, by means of the ballot or by the worn-out routine methods of trade unionism. In some countries, it is true, large masses are ready, at a moment’s notice, to begin General Strikes — France, Italy, Spain, to some extent Holland, Austria and French-speaking Switzerland are foremost in this respect; but that decisive step which theoretically seems so logical, so natural, the step from tile General Strike to Revolution has never yet been taken, — not even in Russia in October, 1905 when the failure to make this step brought about all the disasters which befell revolutionary Russia since that date. Why was this step never taken? Simply because the great majority do not want to go farther and the few who would are powerless.

It is commonly said that all progress is due to minorities. Of course it is; new ideas, new experiences, are tile result of a complex of favorable circumstances that at first exist only in one or a few places. But the right of minorities is to be rejected on the same ground as the right of majorities; a minority has no more right to coerce a majority than vice versa. We all reject the tyranny of reactionary minorities; progressive minorities are in the same position: they must not become tyrannical. Anarchists before all must recognize this; for authoritarian measures may be imposed by the energy of despotic minorities; but how can freedom be imposed upon people who do not care for it sufficiently to get it themselves, to take it?

Look at science and ignorance — a parallel to Anarchism and the masses. Science does not argue with ignorance; it marches forward and sets all examples by its results and the less deep shades of ignorance by and by try to follow ill). Free thought and religion is another parallel — some are able to free themselves from the shackles of religious idiocy, large masses remain unable to do so. In both cases a modus vivendi is found by a sort of mutual toleration compare the infamous brutality of ignorant bigotry against science and free thought in past centuries to the relative indifference that exists today in these matters. I know very well that it is only an armed peace and that reaction is lurking there, every instant waiting for her opportunity; but still the position is different from that of past ages — science and free thought have conquered general recognition, whilst they were outlawed but a short time ago.

What brought about these persecutions and this relative change? Ignorance and bigotry wanted to perpetuate their rule, and they believed that science and free thought were going to fight them directly; therefore they carried oil a-war of absolute extermination against them. Of course, free thought should like to destroy religion radically, absolutely, just as Anarchism should like to uproot the idea of authority definitely’ and once for all. But this could only be achieved materially by the destruction of ninety-nine percent. of mankind, and such a struggle — if it were possible would destroy the sense of freedom in the remaining minority. It was seen by and by that science and free thought were as unable to destroy ignorance and religion as the latter, with all power at their disposal, could arrest the progress of science and crush free thought. Hence this state of a relative cessation of hostilities of today with continuous propaganda and small warfare going on (11) both sides, by which those who are really capable and desirous of clearing the cobwebs of ignorance and bigotry out of their minds, have a chance to find their way to science and free thought. This is all that progress could obtain oil this field — will it really be different with regard to Anarchism?

The destruction of the capitalist system is but one step oil the road to Anarchism. Energetic minorities carry great weight in the moment of immediate action; suppose, then, that Anarchists did their best, that the system is overthrown and that the prestige of Anarchism has grown enormously by its prominent part in that victory; suppose, further, that in many places people lay aside all their prejudices and try to live in an Anarchist way;will not all these arrangements for which, evidently, no rules would be laid down by any leading people, result very soon in new differentiation as people are differently developed in various ways? ‘Fake the question of organization; some Anarchists are ready to accept various degrees of organization, provided only they freely consent to it; others are not. A series of groups and communities would thus exist in which freedom was realized in a different degree, according to the various interpretations given to it by various people, etc. This is quite the right thing; experience will bring further results, .and by and by freedom will be more full understood and more perfectly realized; meanwhile all these organisms would co-exist side by side of each other in peaceful emulation, though many institutions may not at all satisfy the more advanced, who, in their turn, will not yet be fully appreciated by the less advanced.

I selected this as the most fortunate eventuality. It may happen, however, that capitalism is defeated under conditions which bring the organized State Socialist masses, that is, their leaders, into power, and whilst direct economic exploitation would be abolished, freedom would not exist and a new governing class would gradually grow up, a new set of parasites whom labor would have to feed. Anarchists would hardly be more welcome to these people than they are to-day to official and labor politicians. They would have to strike to overthrow that new society, too: whether this will be more easy than the first struggle — as, people may be more educated, having no economic cares, or more difficult, as people, satisfied economically, would not care to move any farther — I cannot decide; I suppose both will be the case, and progress toward reducing and abolishing the power of the new States, communes, etc., will be made first locally. Thus here also differentiation will take place and Anarchism can but hope to be realized to some degree, first, in the most advanced parts, under most favorable conditions.

If these are the likely results of an effective and definite overthrow of capitalism, the problem arises for me: In what way can Anarchists already now conform their methods of action to these probable developments — namely, to the fact, in my opinion inevitable, that their ideas will not be fully realized at the beginning of a new society that they will have to live, as to-day, side by side with persons who are, in various degrees, adversaries of their ideas, or as yet very imperfect interpreters thereof?

The right step to take would be, in my opinion, to get used to the idea of co-existing with not Anarchist institutions — in other words, to mutual toleration. We do this already practically every day, with the exception of those whom indignation drives to direct acts of revolt. It is infinitely far from my thought to mean by this submission to, law and authority. On the contrary, I mean that Anarchists should boldly ignore all laws interfering with their personal freedom and conquer the full recognition of their right to do so by those who, themselves, are in favor of these laws and might have them in operation among those who believe in them.

This will sound quite, utopian and impractical to many, but sooner or later, either with regard to the present or some Socialistically modified system, Anarchists will have to take up such an attitude which, side by side with economic independence secured in various ways of co-operation, will bring about the first direct realization of Anarchism, however imperfect it may be. Possibly, if this happens after a social revolution they may, by means of expropriation, get hold of sufficient land, instruments of production, etc., to have a safe economic basis; possibly also, they will have to construct basis by the slow process of co-operation, and a beginning might be made even now when most Anarchists are scattered in various branches of capitalist production. For just as Anarchist members of trade unions become, as such, solidaric with large masses of workers and share in the united power which for themselves alone they do not possess; in the same way this systematic objection to existing laws would create links of solidarity between them and numerous people of all classes who object to, at any rate, a portion of the existing laws; just as trade unionists on the average are Socialists, but object only to certain features of the capitalist system and yet, leavened by Socialists and Anarchists they are expected some (lay to overthrow this system. In the same way, all those who do not like laws — and who has ever met with people not personallyinterested who did like laws for themselves? — all these discontented masses about whom nobody cares to-day, might form enormous anti-law or anti-State associations, inspired by Anarchists, and striving, finally, to overthrow all laws as the economic associations of workers will finally reject — and overthrow all capitalism. Direct action, strike arid boycott would be means in this anti-law, anti-political struggle, as they are in the economic struggle.

Nor is this something new, unheard of. Whenever a law had really become intolerable to large masses of people, they have always adopted some method of systematically violating or ignoring it. The history of the old Abolitionists, of the Irish movement, etc., is full of examples, and it is the same in private life. In fact, if statistics were available as to the large extent in which laws and regulations are habitually ignored and remain a dead letter, I think the absurdity of law-making would be palpable to almost all, which shows that society cannot live and evolve under law, but only by brushing aside — at every moment, as useless obstacles — regulations and laws. When, in England, people may be exempted from the vaccination laws if they declare to have a “conscientious objection” against vaccination, is this really not a step in the right direction, and if the adherents of other causes had made similar efforts to have laws which they are not strong enough to abolish and which suit the convenience of others who think different — to have such laws at any rate made not applicable to them, they might have achieved similar results, and that is what I urge upon them all to do with the greatest possible energy and on the broadest lines. Such methods were discussed with regard to taxation by the late Auberon Herbert, who propagated the idea of voluntaryism: that those who cared for the objects to be paid from taxation should pay taxes, and others not. In other fields the rights of minorities not to be wiped out by majorities, but to have a voice in proportion to their numbers, begin to be recognized by the various schemes of proportional representation, etc. Economic movements of little advanced character once preceded the trade union movements, which to-day culminate in the large European organizations, accepting the revolutionary General Strike, etc., to destroy capitalism altogether. In a similar way these scattered efforts for exemption from law on the ground of a “conscientious objection,” the opposition to compulsory taxation, the representation of minorities, etc., are yet extremely weak and inefficient movements, the first signs only of anti-State revolt, but they may be followed — if taken in hand by Anarchists and all who sympathize with them in this particular object — by larger movements more directly pointed against State-power arid when, at the time of the economic revolution brought about by large masses of anti-capitalist workingmen, equally large masses will act as determined anti-Statists. This will be an efficient and the best possible means of building up and developing the new social organism on anti-governmental lines, approaching as near as possible to Anarchism and some day realizing it entirely. The undeveloped character of such movements at the present day must not deter us; On the contrary, it must rouse us to greater efforts to make up for time lost.

Our adversaries, the Social Democrats or State Socialists, have not neglected the political side to their movement. By their electoral organizations they are in contact with millions of people who do not fully share their ideas about Socialism, but look tip to them as their future leaders, and are ready to accept a new system of government headed by Socialist leaders — something as deadly opposed to Anarchism as present governments are. If these millions were counter-balanced by other millions of anti-Statists of various degrees — or, better still, if the good sense of anti-Statist ideas would attract the greater part of these millions and other millions not yet aroused, then and then alone a coming change might bring us nearer to Anarchism than anybody now can imagine.

It might be said: Is not this anti-Statist propaganda carried on already by the revolutionary trade unionists such as those of the French confederation? It is, and so much the better, but only with regard to labor matters and anti-militarism; moreover, as the unions to be strong must comprehend workers of all shades of opinion, Anarchist propaganda, however desirable, cannot become general in them. But anti-State movements, such as I am thinking of, would clearly and directly lead to this propaganda whilst they would be neutral as to economic theories, just as the anti-capitalist trade union movements are neutral as to political theories. Whilst these anti-government movements, awakening the latent desire for real freedom, would counteract the creation of a Socialist State, they would also counteract the possibility of a new labor rule which might arise from a victory of exclusive revolutionary trade unionism. In one word, it would be the effective weapon for Anarchism to obtain full elbow-room in the next coming society and, to a degree otherwise impossible, already it) present society, if only efficient efforts are made.

It seems such a pity to me that the splendid idea of Anarchism should to such an extent lay barren to-day. The real reason is, I believe, that this idea was too early — at its very beginning — coupled with economic hypotheses, which for many soon became economic theories or doctrines. The obvious desire to prove the practicability of Anarchism-practical experience being impossible under existing conditions — led to the construction of economic utopias, and this tremendously narrowed the scope of Anarchist propaganda. The latter holds out the hope of greatest possible freedom with one hand, and with the other seems to take that freedom away by tying you down to an economic system-individualist, collectivist or communist. I say nothing against any of these systems; I ignore their working under real freedom, as everybody does, and whether I am sentimentally inclined toward this or that system is of no importance whatsoever; I may also be able or may try to heap arguments in favor of my particular theory — what does it matter? Anarchism would be of small value if, to be realized, it were required that somebody should beforehand discover, out of the infinite field of economic possibilities, just the one which would be the only right one!

But, unfortunately, sectarian division predominates and if a newcomer wishes to learn Anarchism pure and simple, he has practically no place where to go — no group, no paper, no book — everywhere lie is at once considered only as a possible convert to some particular economic doctrine. Might one not reject capitalism and work hard at its destruction, without professing to know anything about future economics save that capitalism be excluded, and without preferring this or that new system, whilst they are all as yet untried? I know very well that the adherents of each system believe that all other systems will lead back to capitalism, and are, therefore, from the beginning, harmful and misleading; I believe this, too, personally, with regard to some; but I believe also in the recuperative power of freedom which will make up all this small loss, and if it is to stay at all, will not be overthrown by the failure of some economic experiments.

Everyone will act according to his inclinations, and in most Anarchists altruistic tendencies are so strong that they feel most inclined to help the workers in their economic struggle; hence revolutionary syndicalism. Others do not feel inclined to bring the sacrifice of direct, Anarchist propaganda which syndicalist work requires. Some of these will also feel unable to listen to my suggestions, as they prefer direct Anarchist propaganda and action in any case. Those, however, who wish to emerge from the relative isolation of exclusively Anarchist propaganda and yet dislike to merge into syndicalism, — it is to these I suggest to look out for fields of anti-law, anti-State action and propaganda as I have described them. Think how we looked on trade unionism fifteen years ago, and to what extent it has been to spread the idea of the General Strike and the economic revolution, — at any rate, among large of trade unionists in several countries, France, the feeble and scattered Federalists, autonomists other anti-State and anti-law movements of to-do similarly strengthened by the co-operation of An and new movements created, in a few years’ time the political strike and the coming abolition of State power would equally come to the front — why not make efforts in this direction?

It is useless to expect that all should ever reach an equal development. We need, therefore, not be angry at those who remain behind and might leave them alone, provided they leave us alone. Progressive movements cannot at the same time proceed and be burdened with the masses who lag behind. Therefore, some day — perhaps soon, perhaps only after centuries of undecided struggles, just as ages ago centuries were wasted on undecided religious wars — it will be more generally recognized that various political and economic systems can by side, just as to-day free thinkers live side by side with believers in hundreds of shades of religion. To make this possible each section must Possess economic and political independence; whilst economic independence will be won for Anarchists either by the overthrow of capitalism and expropriation or by slow co-operation, political independence will be won either by the abolition of the State after a revolution, or by living outside of the State, side by side with it, as cooperation lives side by side with capitalism. The former means (expropriation, etc.) do not depend upon Anarchists alone; therefore they have no prospects to see their ideas fully realized in this way. The latter method (cooperation and political existence outside of the State) can be realized by Anarchists and their sympathizers alone, and as it could only strengthen the power of Anarchism, I do not see that it could do any harm to what some will call the more revolutionary method. Very seldom two bona fide methods exclude each other; usually they support each other, though this is not always seen — so strange is the fetish of unity, unification, etc., one of the many forms of authority.

We see every day more clearly how odious, insidious and infamous State power is; and, to be sure, large numbers of people see it as well whom our propaganda does not reach because it is so closely connected with economic doctrines which are nothing but hypotheses: this makes the friendliest inquirer sceptical. If, by the methods described — anti-Statist work on the broadest lines — we come into contact with all the latent enemies of the State and strive to win independence from the State (as long as people in general are not advanced enough to abolish it) — I feel we should do something which today is too much left undone: the work of political emancipation, the essential corollary of economic emancipation.

London, Nov. 5, 1907. 
M. N.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



London, Nov. 5, 1907. 
M. N.

