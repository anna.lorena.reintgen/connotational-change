
I’m not a nihilist, but I wish they would burn every fucking thing down except for the houses. So that people would begin to understand that we don’t need this system.

- Bella Eiko, speaking to the Oakland City Council, during a vote called to ban “tools of violence” at political demonstrations, 2012

During the Summer [of 1966], there were rumors in the Oakland ghetto that Molotov cocktails were being manufactured in empty garages; that arms caches had been discovered; and that new tactics based on a study of Watts were being taught to young Negro militants, stressing the folly of burning their own homes and shops in the ghetto, and urging that their protest would be more effective if they burned City Hall, the business district, and the homes of the Whites on the hillside.

-Amory Bradford, Oakland’s not for Burning, 1968

*    *    *

Make no mistake: Oakland has changed drastically over the last few years. More and more trendy boutiques and cafes sink their roots into the Telegraph corridor and downtown streets. The deserted center of Oakland of the early 2000′s has been replaced by a young and artsy street life. Recently, Forbes magazine rated Uptown Oakland the 9th hippest neighborhood in the United States. Almost simultaneously, in 2010, Oakland had the highest per-capita violent crime rate in all of California. This dissonance between the violence of Oakland’s failed economy and the recent influx of “culture” and financial capital has come to define Oakland in the present moment.

This piece is written as a friendly challenge to the narrative of gentrification as it exists within radical milieus. True or not, radicals in Oakland often view themselves, with no small amount of guilt, as “foot soldiers” or, more jokingly, the “vanguard” of gentrifying processes. The narrative goes: mostly white radicals move to Oakland because of their interest in the social conflicts here, and with the radicals comes their race and class privileges that feed the mechanisms of gentrification. As a result of this gentrification, historically black neighborhoods are whitening and rental prices are pushing out the working class elements. In its broad strokes, this narrative might be indisputable; there is some truth to everything it describes. Here, the effort will be spent to clarify specific parts of this process, identifying possible points of activity and intervention. An argument will be made here for wholly clarifying the confused and recuperated narrative of gentrification with a nuanced and localized understanding of the processes at work.

Unfortunately, broad swathes of the radical milieu can, with all of their rhetorical power, only muster a few lackluster approaches to this very real situation. Common within activist circles is to assert that by moving to Oakland you will inevitably further gentrification and push people out of their homes. Therefore, don’t move to Oakland. Or, if you do, engage yourself in community projects (social services, legitimized by the state or not) to offset your impact. These approaches, stuck in a simple sentimental reaction to the reality of the situation, frustrate rather than enable a combative response to the present circumstances.

In this piece, some history will be briefly explored, both to make better sense of the present situation and to analyze previous demographic shifts in this area. Then we will proceed to discard race as the only useful marker for gentrification. Moving beyond this narrative, an attempt will be made to discover a more useful antagonism or set of questions, centering around development and infrastructure, with which to engage in the situation here. Finally, a few specific development projects will be outlined, inviting everyone to attack and organize against the misery that defines and binds this society.

*    *    *

During the industrial frenzy of World War II, de-segregation in industry caused a massive influx of blacks from The South into West Oakland to work in the ports, the canning industry, and machine shops of West Oakland. After the war, most industries slipped back into racial segregation (formal and informal), leaving large numbers of black residents jobless. Many of those that remained employed worked for the railroads as porters or waiters. Though jobs were scarce after the war, black migration from The South continued. After the war, the white middle class moved from Oakland into the surrounding areas. Their “white flight” from the urban centers was coupled with an attraction to the property-owning middle class lifestyle available in the new suburbs.

The waves of deindustrialization in East Oakland in the 50′s and again in the 80′s, with the corresponding exodus of its white residents, allowed blacks into the depths of East Oakland, previously a space reserved for the comfortably white and middle class. More and more, large swaths of Oakland were transforming from the idyllic garden city of balanced industrial and residential development into a post-industrial ghetto. The border between East Oakland and San Leandro, though, was as strictly enforced then as it is now. “Racial Covenant” laws in many of the suburbs, including San Leandro, specifically excluded blacks from neighborhoods and cities.

Even as formal segregation was undone on national and state levels during the 60′s, blacks and latinos in Oakland reaped none of those benefits. This reality, combined with a history of black nationalism and the radical strength of black railroad unions, provided the perfect backdrop for the formation of a revolutionary black consciousness. The Black Panthers were particularly successful here: Their own special brand of maoism was successful because it not only enabled them to stand alongside the other anti-imperial struggles of the time, but because it allowed them to apply the same narrative to Oakland. The dissonance between national civil rights campaigns and local segregation exacerbated the feeling that Oakland was under colonial rule, surrounded by the “white noose” of the suburbs.

The Panthers were not the only revolutionary group in Oakland during the 60′s and the 70′s: The Brown Berets and other Latin@ groups were organized in the Fruitvale district (and also had armed copwatch patrols). During this same period, the Symbionese Liberation army was in and out of Oakland, and killed the superintendent of schools in 1972. Parallel to the constellation of revolutionary activity, crime syndicates also flourished in the east bay, the Hell’s Angels being a flagship of East Oakland lawlessness at that time.

Despite the history of (anti-)social uprisings in Oakland, the town has always been controlled politically by business interests. Rebels from other episodes of struggle understood this well. From the 1930′s until the 1970′s, people called to “Take the Power From the Tower” in reference to the Tribune Tower and the newspaper’s well-connected owners, the Knowlands. Today, the situation is the same, but with different faces. Business is still organized into associations and districts that broker power in the same way it has always happened in Oakland. The appearance of power has become more subtle over time, quick to hide behind a language of progressive politics or conjure up community initiatives to disguise their consolidation of capital.

*    *    *

In and beyond radical milieus, “gentrification” is often roughly translated to “white people moving to the neighborhood.” There is more than a grain of truth to this statement. In this white supremacist world, the privileges of whiteness include the social mobility that can create gentrification. This rough idea, though, that whiteness equates gentrification is mistaken in its assumptions and misunderstood in its consequences. The discourse of gentrification, recuperated by leftists and nonprofits, is now void of meaning. Instead of helping us understand the functioning of the city, it has further obfuscated this understanding. By honing our understanding of the different factors at work, and by separating their significance, perhaps we can exit the quagmire of the discourse on gentrification.

By equating whiteness and gentrification, one assumes a certain homogeneity amongst white people. While certainly privileged as a group in this society, it would be ridiculous to say that a white man moving to Oakland from Blackhawk and a white transwoman from Antioch posses the same privileges in this society. More pertinent to the questions of development and gentrification, it cannot be said that every white person has the same class values and interests. Many of the recent migrants to Oakland (both the déclassé from San Francisco and the youth fleeing the suburbs), can be said to carry a particular set of middle class values that is more of a contribution to the development (in the pejorative sense) of the neighborhood than their race. These values–a concern for property values, an antipathy to street life, homeownership, civic-mindedness, complicity with the policing apparatus, interest in urban beautification, etc.–provide the social conditions for the development of a neighborhood. These values are held across race lines. While often true, the  equivocation of these values with white gentrifiers can obscure or occlude other dynamics. The tension between longtime Black residents and Arabs being the most glaring example (a subject that deserves its own writing); second, the KONO business district, which uses its status as “minority-owned” to disguise its project of destructive redevelopment.

It would be absurd, though, to reduce development to economic forces. Additionally, especially in Oakland, one loci of the dispute is the culture of this place. As Oakland whitens, the history of black power (and much more) is at risk of erasure. This is not foremost a result of white people moving to Oakland, I would argue, but also a product of (re-)development. Development and the reorganization of space by power actively erases memories, replacing it with timeless, placeless places. The narrative of development erasing memory has been central to many Indigenous struggles for land, recognition, and autonomy. Especially in the Bay Area, where a tragic history of development has destroyed or desecrated burial sites. (Clearly, viewing the current wave of development and gentrification as parallel with anti-colonial struggles is a powerful tool, both in understanding and acting within our situation.)

This is not at all to say that economic shifts and the struggle over the memory of this place should replace a discussion of race, thus soothing the guilt of the white activists. Whiteness plays a crucial role in the current reorganization in Oakland. Many understandings of the real (and not) effects of whiteness upon this place unfortunately seem to result in a simplistic ultimatum: Don’t Move to Oakland. While not entirely determinist, processes like demographic shifts in an area follow patterns beyond the choices of individuals. The history of Oakland parallels other cities around the United States. Post-industrial cities everywhere are swelling with people and investment. Oakland’s affordability is attractive to both the new investors and the displaced residents of the surrounding areas.

People, a lot of them white, will move to Oakland regardless of what we tell our friends. Rather than lamenting this, we need to forge a practice and critique of development that separates the inevitable (white people moving to Oakland) from the approachable points of antagonism (particular development, infrastructural, cultural or beautification projects). Our project within the metropolis is to disrupt the flows of financial and cultural capital, not to frustrate our own projects with a guilty conscience.

Contemporaneous with recent development and gentrification in Oakland, other historically working class neighborhoods have been faced with similar pressures. East Palo Alto became home to an Ikea in 2002 and has been the site of numerous housing developments for Silicon Valley commuters. Rents in the Fillmore District of San Francisco have doubled in the last few years alone, the apartments have become condos and the dive bars have become fancy jazz clubs. Oakland is by no means alone in its circumstances but we can seek to distinguish it in its response.

*    *    *

The narrative of gentrification only goes so far and in caught in the mire of leftist politics. A critique of development specifically, on its own and as a part of gentrification, is much more useful to the insurgent. First, it allows one to identify the enemy in much clearer terms. It identifies developers, real estate brokers and property managers as the enemy, instead of the wide hostility towards “gentrifiers.” Secondly, it suggests a particular space of activity, beyond the vagaries of “don’t move here” or “if you move here, do it right.” A critique of development suggests the organization of attack, both social and not, on development projects, real estate companies, and all of the administrators of control over the place where we live.

Further development will not open space for meaningful social activity and will only constrict it. In the slew of development projects coming down the pipe, residents will be free to consume, travel to and from work, or stay inside to not bother anyone. The possibility for creating space for rebellion will happen in the subversion of the city’s architecture (such as the occupations of vacant houses or the lot at 19th and Telegraph). Even the most hopeful possibilities are rapidly being foreclosed in the proliferation of counter-insurgency-style policing.

The physical layout of space, its geography and organization, determine how power can flow within. A critique of space and the way it governs relationships is not solely the domain of radicals and antagonists. Older developments such as the ACORN projects in West Oakland have been able to act as hubs for illegal capitalism because their architecture was opaque to social control. These mistakes in development culminated in a counter-insurgency-style raid on the projects in 2008, with 400 police and at least one armored personnel carrier. J Stalin describes how he and his partners would construct escapes through the fences in the ACORN Projects:

“Put the car jack in [the fence] to make hella cuts through this motherfucker. Jack it all the way down, then turn it like you were jacking up a car. Say the police come. They got them big-ass belts. That can’t fit through this. Then I’m gone, feel me?” (Quoted in the Thizz Nation Block Report)

These mistakes in architecture will never be repeated in future developments. The UC system learned the danger in building large plazas where dissident students could gather during the free speech movement at Berkeley. University of California campuses built since the sixties are subdivided into a number to smaller campuses, to better contain and neutralize student revolt. Housing projects are built to make the space transparent and easily surveillable, often by the administrators of social services. Likewise, we can be entirely sure that the city of Oakland will never allow the construction of another space like Oscar Grant Plaza, where thousands of people were able to gather, meet their needs and organize an assault against capitalism.

The struggle against development, just like the struggle against gentrification, is still limited struggle. As antagonists, our goal is to expand its proportions towards an anti-infrastructural struggle. Development, on its own, is only the evolution and transformation of the metropolis: The adaptation of the city to its present circumstances. This evolution is often predicated on facilitating new techniques of control and policing. That is to say, behind the smokescreen of politics, power is every day being consolidated, the production and reproduction of life is being controlled in the physical space of the city. New neighborhoods are being built to be better policed. Narrow pedestrian walkways and alleys are being replaced by wide paths for wheelchair access, yes, but also for police vehicles. Broad boulevards and gridded neighborhoods exist to simplify and reduce traffic congestion, yes, but also to mitigate potential urban uprisings.

As the entire physical space of the metropolis is constructed to reproduce a certain set of relations (capitalist, patriarchal, alienated), the entirety must be destroyed or subverted. The revolutionary project (to use a term of convenience) must be anti-infrastructural: Anything less can be turned on its head to buttress the functioning of this repressive society. This project, of course, is suicidal. The networks of domination and control no longer administer merely our death or our imprisonment, they also administer our live and the reproduction of our conditions. To refuse the constraints and control of society is to attack the very thing that gives us life. When we enter into refusal together, we occasionally find sustenance outside the flows of capital. This sustenance has, at different times, been called “communism”, “friendship” or maybe doesn’t exist at all.

*    *    *

In other places in the world, social antagonists have understood the need to attack infrastructural projects. NO TAV activists in Italy have been opposing the development of a high-speed rail line since the mid-90′s. In Canada, indigenous communities have responded to attacks on their autonomy with rail line and highway blockades. More recently, in Greece, the opposition to a garbage dump in Keratea that was mandated by the IMF did much to destabilize the economy in that country. Current anti-colonial struggles reveal the same set of practices. The Afghan War Logs released by Pfc. B. Manning show a strong pattern of infrastructural sabotage by insurgents in Afghanistan, primarily of transmission towers and oil pipelines. The question to pose locally: How can we shift a narrative of struggle against gentrification to one of attack against development, and how can we expand that struggle to take on anti-infrastructural dimensions?

By focusing on the material situation in the city, we can direct our attacks against the apparatuses that reproduce society. The power to reproduce the misery of society is exchanged in the material realm, not in politics. The success of anti-infrastructural projects is that they actually disrupt the spread and strengthening of empire, rather than engaging on a spectacular level.

By discovering together a practice that seeks to 1) identify and attack development, 2) subvert and destroy the infrastructure of the city, and 3) destroy whiteness as an identity and a set of practices, we can forge a response to gentrification that hinges not on inactivity out of guilt, but instead from action motivated by our insurgent spirit.

SPECIFIC PROJECTS:

This list of enemies is truly endless. The associations and projects listed below should be seen nothing but the tippiest tip of the iceberg. Behind these larger projects, there are myriad smaller realtors, property managers, security organization, and countless other administrators and managers of social control.

• CBDs and BIDs

Community Benefit Districts (CBDs) and Business Interest Districts (BIDs) are more or less the same beast, even by their own admission. They are both associations of commercial property owners that advocate the economic development of specific neighborhoods. While the spectacle of politics takes the stage, always mired in this or that debate, these groups consolidate control over specific neighborhoods, hire private (sometimes armed) security, and shape the make-up of places that we live. Here, we will look briefly at the KONO, Uptown/Lake Merritt, and Downtown districts.

KONO

Offices:

2633 Telegraph Ave #107

Oakland, CA 94612

The Koreatown Northgate (KONO) business district was formed in the Summer of 2007, the brainchild of developer Alex Hahn. Before it was KONO, the neighborhood was known as Northgate-Waverly. Many of the business owners in the neighborhood are resentful of the sudden re-branding of their neighborhood as Koreatown, especially because the neighborhood’s resident’s are predominantly black. Furthmore, only 7% of property in the neighborhood is owned by Koreans.

The conflict between black residents and Korean business owners is uncomfortably present in KONO’s mind; much of the KONO project is designed to avoid the radicalized conflicts that exploded in the 1992 rebellion in Los Angeles. To avoid these conflicts, KONO has hired black security guards and “neighborhood ambassadors” to patrol the neighborhood and talk to businesses, acting as the face of KONO. In the words of KONO board member Ben Schweng, the activity of the neighborhood ambassadors “goes a long way to prevent what happened in Southern California [in 1992], which is not what I want. (Oakland’s Koreatown isn’t your typical ethnic enclave in the May 2009 East Bay Express)

Reducing the 1992 rebellion to racial violence completely obscures the centrality of class antagonism to those events. In the words of the Aufheben Collective, in their illuminating essay The Rebellion in Los Angeles:

One form that the rebellion took was a systematic assault of Korean businesses. The Koreans are on the front-line of the confrontation between capital and the residents of central L.A. – they are the face of capital for these communities.

KONO attempts to disguise class tensions with a lot of handwaving about race. They are capitalizing on the loaded nature of racial politics in Oakland by describing their business district as minority-owned. Thusly, they attempt to sidestep responsibility for pushing other people of color out of the neighborhood and to respond to accusations of gentrification. Furthermore, they attempt to diffuse the class tension in the neighborhood by using black street ambassadors.

Shari Godinez, the executive director of KONO, has claimed that crime in the KONO corridor dropped by 70% in the first month of their security programs. Their security staff aggressively encourage panhandlers and the homeless to leave the neighborhood, directing them towards (inadequate, moralizing) social services. They monitor drug spots and report them to the police. They report graffiti and coordinate their efforts with a buff squad that is also directed by the business district. The KONO website reports that 60% of the business district’s money is spent on cleaning and security. This makes their purpose clear: They seek to sterilize and scrub the neighborhood of its undesirable street life, to create the conditions favorable to development. It is this development, with its corresponding social cleansing, that is redefining the telegraph corridor.

• Uptown/Lake Merritt and Downtown Business Associations

Offices:

388 19th Street

Oakland, CA

From the 1970′s up until the early 2000′s, downtown and uptown Oakland were desolate. They were ghost towns on Friday nights, quiet except for an occasional person or group crossing the neighborhood to go from east to west, or maybe on their way to Jack London Square. In 1965, an ambitious urban renewal plan sought to demolish 70 blocks of downtown buildings, many of them SROs, pawnshops, and small theaters. Eventually, the plan was rolled back and only 12 city blocks were razed, but the same effect was achieved. The vibrant seediness of downtown Oakland was forcibly removed. In the absence of anything else, though, a more desperate lawlessness took root. In one resident’s description of downtown Oakland at that time:

In those days there were no lights from Grove St. to Broadway. I used to get off the bus here at 14th and walk up to DeLauer’s to look at comic books, and you had to be careful where you stepped or you might step on someone and get a bottle thrown at you. We called it ‘Wino Alley’.

Slowly, the empty lots in Downtown Oakland were developed. The Clorox Building, Oakland City Center and the Federal Building all were built (none of them quickly) in the period between the 1970′s and the 1990′s, attempting to shape the downtown space, bringing order and law to the disorganized and lawless space that downtown Oakland was.

In 1999, a group of developers, investors, and city bureaucrats came together to restore the Fox Theatre, which had been closed since 1970. In 2009, ten years later, the Fox Theatre reopened, a foothold in the uptown neighborhood. The renovation was spearheaded by Phil Tagami, head of the major property investment firm CCIG (California Capital Investment Group). From there, the developments poured in: Hip bars and restaurants, ugly condos and cafes.

Located near the 19th street BART station, the Uptown area provided the perfect inlet for financial runoff from San Francisco. People could live and go to concerts in Oakland without ever interacting with the rest of the city. The Fox Theatre, condominium developments like 555 City Center, combined with other nightlife elements like Cafe Van Kleef to form the social foundation supporting the current influx of investment and capital.

Fox-Uptown Entertainment Complex

Between the current Fox Theatre and San Pablo, Sunfield development is planning the “Fox-Uptown Entertainment Complex.” The plan is for a 500- to 800-car parking lot, ground-level retail, and office space. Currently an empty lot, the development will provide the missing link between the Rotunda Building and the Uptown Condos on the San Pablo corridor.

Sunfield Development, LLC

562 14th St.

Oakland, CA

510-452-5555

• MacArthur Transit Village

As of this writing (Fall 2012), the MacArthur Transit Village is being built piecemeal. The first phase, the BART parking garage on West MacArthur, is having its foundation dug. The plan is for much more than parking, though. The transit village, roughly analogous to the one at Fruitvale BART, is a “live/work” development filling the gap between the wealthy Temescal District and the rapidly developing KONO corridor. The entire project is valued at $200 million dollars (over three times the worth of the Fruitvale Transit Village). The MacArthur Transit Village LLC is a partnership between two mega-developers: McGrath Properties and BRIDGE housing.

Once constructed, the Transit Village will be more a part of San Francisco than of Oakland. The transit village is designed as a commuter enclave. Without ever stepping into the surrounding north Oakland neighborhoods, yuppies can live near BART, travel to and from work and, on their Friday nights, visit the upscale restaurants on Telegraph. This is not to say that the Transit Village will have a neutral effect on surrounding areas. Similar to many other development projects, there is talk of revitalizing an ailing neighborhood or, worse, reducing the existing neighborhood to a blank canvas on which developers can capitalize on a “booming real-estate market.” Regardless of the rhetoric, the construction on the Transit Village can be anticipated to bring an increased security presence, a fresh assault on graffiti and street art, and scores of new residents sympathetic to the police and unaware of the neighborhood in which they live.

McGrath Properties

1625 Clay Street, Suite 100

Oakland, CA

BRIDGE Housing

345 Spear Street, Suite 700

San Francisco, CA

Oakland Global

Most often, opponents of gentrification attack residential developments or development in residential neighborhoods. This approach, however, attacks only the small appendages of the leviathan. A deeper attack, locally based, must go further, attacking the creation and consolidation of infrastructure. Infrastructure which, under empire, can only exist as the infrastructure of control.

Anti-infrastructural struggles, struggles against the myth and physicality of progress, provide a ground on which anarchists and social antagonists can articulate a total refusal of society. Campaigns or actions against one specific developer or another, as fertile as they may be, are also well within the domain of leftists and reformers. Attacking the very functioning of this society, the way it moves commodities and mediates exchange, is opaque to the logic of the left. A praxis of critique and attack positioned against progress (which is only the refinement and spread of empire) will not create jobs, but rather destroy them, in will not preserve a neighborhood, it very well might impoverish one and, most of all, it cannot as easily be turned on its head to buttress the functioning of this repressive society.

Since 2008, a project as been underway to redevelop the old Oakland Army Base. The goal is to develop the 330 acres of land into a world-class port and freight train terminal. Like so many other Oakland redevelopment projects, Phil Tagami is once again the front man. Unlike other projects, this is not a business or residential development seeking to capitalize on the latest surge of money into the Oakland economy. This project is an attempt to secure the Port of Oakland’s future as a major port, especially while mega-ports are being developed along the Pacific Coast (like the $4bn effort in Punta Colonet, Mexico).

The port expansion, obviously, must be opposed, sabotaged, and, in our wildest dreams, stopped. People have said many times that the two port shutdown actions were not merely union actions, but were a mass of people participating in an economic attack on global capitalism. While this feels like a generous analysis, there is also more than a little truth to it. This little bit of truth must become the seed for a new flourishing of anti-economic activity. We can start in our neighborhoods, that seems prudent, but we mustn’t stop until the ruins stretch all the way to the water.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

