
A chaos of beings, of acts and ideas; a disordered, bitter, merciless struggle; a perpetual lie, a blindly spinning wheel, one day placing someone at the pinnacle, and the next day crushing him: these are just a few of the images that depict current society, if it were possible for it to be depicted. The brush of the greatest of painters and the pen of the greatest of writers would splinter like glass if we were to employ them to express even a distant echo of the tumult and melee that the is depicted by the clash of appetites, aspirations, hatreds and devotions that collide and mix together the different categories among which men are parceled out.

Who will ever precisely express the unfinished battle between private interests and collective needs? The sentiments of individuals and the logic of generalities? All of this makes up current society, and none of this suffices to describe it. A minority which possesses the faculty to produce and consume and the possibility to parasitically exist in a thousand different forms: fixed and movable property, capital as tools or as funds, capital as teaching and capital as education.

Facing it an immense majority, which possesses nothing but its arms or brains or other productive organs which it is forced to rent, lease, or prostitute, not only in order to procure what it needs so as not to die of hunger, but also to permit a small number of holders of the power or property or exchange values to live more or less in luxury at its expense. A mass, rich and poor, slaves of immemorial, hereditary prejudices, some because this is in their interest, the others because they are sunk in ignorance or don’t want to escape it. A multitude whose cult is that of money and the prototype of the rich man, the rule of the mediocre incapable of both great vices and great virtues. And the mass of degenerates on high and down low, without profound aspirations, without any other goal than that of arriving at a position of enjoyment and ease, even if it means crushing, if necessary, the friends of yesterday, become the downtrodden of today.

A provisional state that ceaselessly threatens to transform itself into a definitive one, and a definitive state that threatens to never be anything but provisional. Lives that give the lie to espoused convictions, and convictions that serve as a springboard for crooked ambitions. Free thinkers who show themselves to be more clericalist than the clerical, and believers who show themselves to be coarse materialists. The superficial individual who wants pass for profound and the profound individual who doesn’t succeed in being taken seriously. No one would deny that this is a portrait of society, and no thinking person would fail to see that this painting does not even begin to depict reality. Why? Because there is a mask placed before every face; because no one a care to be, because all aspire only to seem. To seem: this is the supreme ideal, and if we so avidly desire ease and wealth, it is in order to seem, since only money now allows one to make an impression.

This mania, this passion, this race for appearances, for what can procure them, devours both the rich man and the vagabond, the most erudite and the illiterate. The worker who curses his foreman wishes to become one in turn; the merchant who evaluates his commercial honor to be of an unequalled price doesn’t hesitate to carry out dishonorable deals; the small shop owner, member of patriotic and nationalist electoral committees, hastens to transmit his orders to foreign manufacturers as soon as he finds this profitable. The socialist lawyer, advocate of the poverty-stricken proletariat herded into the malodorous parts of the city, passes his vacations in a chateau or resides in the wealthy neighborhoods of the city, where fresh air is abundant. The free thinker still willingly marries in church, and often has his children baptized there. The religious man doesn’t dare express his ideas, since ridiculing religion is the done thing. Where is sincerity to be found? The gangrene has spread everywhere. We find it in the family, where often father, mother, and children hate and deceive each other while saying that they love each other, while leading each other to believe that they feel affection for each other. We see it at work in the couple, where the husband and wife not meant for each other betray each other, not daring to break the ties that bind them. It is there for all to see in groups, where each seeks to supplant his neighbor in the esteem of the president, the secretary, or the treasurer, while waiting to assume their place when they no longer need them. It abounds in the acts of devotion, in public doings, in private conversations, in official harangues. To seem! To seem! To seem pure, disinterested, and generous, while at the same time we consider purity, disinterest, and generosity as vain foolishness; to seem moral, honest, and virtuous when probity, virtue, and morality are the least concerns of those who profess them.

Where can one find a person who escapes corruption, who consents not to seem?

We don’t claim to ever have met such a one. We note that sincere, eminently sincere individuals are rare. We affirm that the number of human beings who work disinterestedly is quite limited. Right or wrong, I have more respect for the individual who cynically admits to wanting to enjoy life by profiting from others than for the liberal and philanthropic bourgeois whose lips resound with grandiose words, but whose fortune is built on the concealed exploitation of the unfortunate.

It will be objected that we are allowing ourselves to be led by our indignation. That in the first place nothing proves that our anger and invectives are not also a way of seeming. Be aware: what you will find here are observations, opinions, theses: it will be left to the reader to determine what they are worth. The pages that follow are not marked with the seal of infallibility. We don’t seek to convert anyone to our point of view. Our goal is to make those who browse these pages reflect, with the right to accept or reject that which is not in accord with their own convictions.

It will be objected that this is dealing with the question at too high a level, or from a metaphysical point of view; that we must descend to the level of concrete reality. The reality is this: that current society is the result of a long historical process, perhaps still just beginning; that humanity — or the different humanities — are simply at the point of seeking or preparing their way, that they are groping and stumbling; that they lose their way, find it again, advance, retreat, lose their way; that they are at times shaken to their foundation by certain crises, dragged along, cast on destiny’s road and then slow down or march in place; that by scratching the polish, the varnish the surface of contemporary civilizations we would lay bare the stammering, the childishness, and the superstitions of the prehistoric. Who denies this? We accept that all these things render the “human problem” singularly complex.

Finally, it will be objected that it is folly to seek to discover, to establish the responsibility of the individual; that he is submerged, absorbed in his environment; that his ideas reflect the ideas and his acts the acts of those around him; that it can’t be otherwise, and if from top to bottom of the social ladder it is “seeming” and not “being” that is the aspiration, the fault is that of the current stage of general evolution and not of the individual, the member of society, minuscule atom lost in a formidable aggregate.

We answer honestly that we don’t intend to write for all the beings who make up society. Let us be understood: we address ourselves to those who think or are in the process of thinking, to those who have grown impatient from waiting for the mass, who can’t or won’t think; to those who can’t adapt to appearances and who the current stage of society doesn’t satisfy. We write for the curious, for thinkers, for the critical — for those who aren’t content with formulas or empty solutions.

It’s either the one or the other: either there’s nothing else to be done than to allow the inevitable evolution to run its course, to cowardly bow before circumstances, to passively witness the parade of events and admit that, while waiting for something better, all is for the best in the best of societies. Our theses and opinions will not interest those who share this way of seeing things. Alternatively, without arming yourself with an exaggerated optimism, you can step off the main roads, withdraw to a great height, question yourself, look into yourself for the roots of our own malaise. We address ourselves to those not satisfied with the current society, to those who are thirsty for real life, for real activity and find only the artificial and the unreal around them. There are those who are thirsty for harmony and ask themselves why disorder and fratricidal struggles abound around them...

Let us conclude: the sprit that reflects and attentively considers men and things encounters in the complex of things we call society a nearly insurmountable barrier to truly free, independent, individual life. This is enough for him to qualify it as evil, and for him to wish for its disappearance.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

