      The Problems We Face      Green Titans    Articulating a Radical Response      Radical Green Populism      Questions Moving Forward      References:
Over the past decade, climate change has finally established itself as a recognized global problem, drawing attention, discussion and even action from governments, their allies, the media and industry. Further, there is no lack of input from the ‘social progressive’ perspective, dominated by the Left, Old Left and Corporate-NGO/US Democratic Party. However, there has been comparatively little comment from anti-authoritarian, radical perspectives. This means that even the critical discourse surrounding mainstream climate change solutions has been controlled by a small number of opinions. This is the source of the consumption-heavy, technocratic marketing campaigns that masquerade as solutions; this dominant discourse is little more than a celebration of ‘green’ consumer goods and, more importantly, large-scale energy systems.

Indeed, virtually all popular discussion about the future of carbon-neutral energy is predicated upon the assumption of centralized generation by large-scale systems. For example, wind power is symbolized by images of massive farms, huge turbines dotting the coastline or prairie by both sides of the debate. Those who resist wind power cite noise, bird mortality, insufficient transmission capacity and, most commonly, visual pollution in their arguments against farm construction; wind supporters muster ornithologists, acoustics experts and artists in their defense. The scale of the farms is not part of the debate. Geothermal, nuclear, hydropower, hydrogen and bioenergy, the main carbon-neutral systems supported by US Department of Energy grants, are all large-scale projects based upon a centralized generation paradigm, either because grants push research in that direction or because of the nature of the source, as in the case of geothermal.

In this sense, the energy systems touted as saviors of modern society, glorified and worshipped through ad campaigns full of calming, green vistas, have more in common with coal power plants of the past than any energy independence utopia of the future. They continue the tradition of monolithic energy systems, bringing with them the attendant social and political characteristics, the political economy of the energy of the past. Langdon Winner refers to the social and political dimensions associated with a given physical energy technology as an energy regime.

To provide the variety of goods and services that sustain them, modern societies have created elaborate socio-technical systems that link production, distribution, and consumption in coherent patterns. Within such systems, the activities of work, management, finance, planning, marketing, and the like are coordinated in highly developed institutional arrangements. These institutions, together with the physical technologies they employ, can well be characterized, borrowing a term from political theory, as “regimes” under which people who use energy are obliged to live. Such regimes of instrumentality have meaning for the way we live not unlike regimes in politics as such. It is possible to examine the full range of structural features contained in a particular socio-technical arrangement and to identify the qualities of its rules, roles, and relationships. (Winner 1982: 271)

The energy regime of the past, and the one large-scale renewables stand to replicate, is characterized by “extremely large, complex, centralized, and hierarchically managed” systems reliant on, and constantly reinforcing, a social contract that is predicated upon a highly developed technocrat class, the political will to support them and the positioning of ‘energy users’ as ‘energy consumers,’ purchasing from an amorphous energy system, “black boxes — input/output devices whose internal structure is of no particular public concern.” (Winner 1982: 273;272).

Here emerges a crucial point of action for the radical, anti-authoritarian left. The development of new energy systems that mimic the energy regime of the past stand to repeat and strengthen the authoritarian, capitalist social forms of the past, quite literally cementing them in steel and iron, plastic and glass.

To be sure, there exist radical analyses of the complex dynamics and social implications of proposed solutions; however, it appears that radical discussions are not any more immune to the humbling enormity of the issue than is popular discourse. What is lacking is a truly radical response to the problem, that is, a proactive attempt to craft a scheme for addressing the issue of climate change in an immediate way without sacrificing or subjugating a wider concern for social change and freedom.

Here we aim to develop a model for framing radical discourse surrounding the problem of and responses to climate change. We begin by highlighting the complexity of the problem, focusing on how dominant climate change solution discourse stands to repeat and further entrench the social inequalities and authoritarian practices of the past. We then discuss what a radical approach might look like given the complexity of the climate change issue. We argue that many radical communities are already quite adept at practices that, when applied to climate change, may offer an educative, productive and emancipatory response. We label this intersection of ideas and practices Radical Green Populism and then layout how this might be used as a framework for approaching radical climate change discourse. Finally, we offer some questions for further discussion as well as examples of the specific elements that might help flesh out the Radical Green Populism model, that is, the specific, day-to-day practices that combine theory with praxis in a way that remains grounded in the needs and desires of individuals’ daily lives.

There is no shortage of radical commentary on the problem of climate change and a great deal of this discussion delves into the serious and far-reaching implications of dominant solution discourse. Recently, a brief article in the most recent issue of the Irish anarchist newspaper Workers Solidarity nicely encapsulated the issues facing radicals looking to tackle climate change. It covers the obvious problem of the vested economic interests of nations and corporations as well as the dual role of advanced science and technology in both creating and identifying the problem. The most interesting part for me came at the end:

Finally, the third major problem is that many proposed solutions do not question at all the current political and economic order. This leads to solutions such as “the power of one” — solutions based on consumer choice and education. In reality, consumers generally don’t get enough information to truly make informed choices, while very few have enough money to actually have any significant choices in the marketplace. The major over-riding problem is that our world is organized according to competitive principles and maximizing the profits of the wealthy. Given this reality, common problems that require broad, cooperative input from the entire species are difficult or impossible to address. If we can get rid of that problem, stopping and reversing climate change will be child’s play in comparison. (Feeney 2009)

There are two points raised here that are important to consider in developing a truly radical approach to the problem of climate change: 1) The education of individuals and 2) access to positive choices, in terms of both economic barriers and authoritarian decision-making models. In fact, these issues are deeply intertwined. Individuals can be educated about environmental problems and solution paths that serve only to support and maintain prevailing economic and political power structures, as appears to be the case in the contemporary situation.

Indeed, this prevailing ‘flavor’ of green education is crucial to the continued viability of capitalist social relations in the face of severe environmental problems. This point cannot be emphasized enough. Since the industrial revolution, social progress has been measured by material affluence. Living well in modern times is linked to a free and constantly rising flow of goods and services delivered conveniently and, ideally, at low cost. In turn, the perceived ‘need’ to assure wealth and its increase creates and supports a set of institutions, both physical and normative, capable of creating this boundless frontier of expanding production and consumption. It is a hegemonic social order capable of creating modes of behavior and expectations consistent with a ‘growth = wealth = living well’ paradigm.

Within this hegemonic social order, energy is the one commodity always needed to make and use anything. In this respect, energy supply is what enables the pursuit of boundless growth; because of modern energy, we can aspire to produce and possess everything. After all, the potential for incessant growth can only be exploited if an ever-present capacity to fuel such growth exists. Thus, energy systems must constantly expand. Having just enough energy presumes the nonsensical idea of just enough growth; there is never enough growth in the modern era. Thus, again, popular climate change discourse focuses on how societies can maintain their energy practices — through switching inputs — and avoids discussion of the social relationship with energy.

The wealth-energy association and its concomitant environmental needs has produced a feedback loop: the physical processes that produce material wealth are reliant on energy regimes which foster continued growth of output; increased growth in resource use and consumptive demand (through planned obsolescence and advertising) create and reinforce social norms and obligations to increase consumption; increased demand encourages expansion of the physical processes that produce material wealth; and so on. Perpetuation of this self-sealing logic is a defining characteristic of the modern energy regime, with little distinction between public and private operations. For example, critiques of the centralized energy monopolies and oligopolies from “big oil” to “giant” electric utilities (Pinchot & Ettinger, 1925; Yergin, 1991) were answered by public replicas of the large, complex, and hierarchically managed energy systems: the Tennessee Valley Authority, the Bonneville Power Administration, and the Rural Electrification Administration. These public programs reinforce, rather than oppose, the reigning energy regime.

The predominant energy systems of the past 100 years are part of an energy regime, a particular configuration of material, social, economic, political and psychological patterns and institutions. This particular regime is typified by its complex, centralized, and gigantic physical technologies and the technocracy, commodification, and hierarchy that support and reinforce their primacy. There is constant reciprocity among these factors, each one deepening the strength and logic of the others.

Enter renewable energy systems.

Renewable energy systems are ushering in the same, large-scale, centralized and complex forms as their predecessors. The technophilic awe inspired by massive coal plants and nuclear reactors in previous decades is replicated in visions of vast wind farms, huge tidal capture systems, lonely desert solar arrays, a complex hydrogen infrastructure, and so on. The old energy regime is maintained in that we are simply exchanging our sources, in the same extra-large form, while leaving the basic social configuration intact. The unique opportunity to question our relationship with energy offered by the decline of fossil fuels is lost in a seamless swap of inputs.

There is, however, a critical problem raised by the incorporation of renewable technologies into this regime. The commodification process not only alienates the user/consumers from the energy production process but also the resources consumed in that process. While the physical technologies of the past did rely on organic sources, these were discrete inputs, that is, non-renewable sources. A commodified renewable energy not only maintains the alienation of the production process, but also its resources, in this case the Earth’s renewable, organic and omnipresent resources. The problem is not that the seemingly ceaseless march of commodification continues into the realm of basic ecosystems, but that the economic logic of commodification stands to erect barriers around these most pervasive of resources, these renewable energy commons.

Some might argue that by their very nature these resources cannot be appropriated or privatized and, thus, are not susceptible to the same capitalist economic logic as fossil fuels. To be sure, it is true that, for example, wind resources are not technically excludable, in that you cannot prevent others from using them, and that they are not technically rival, in that one person’s use does not affect the ability of others to do the same. However, when government grants, investment portfolios and sheer technophilia support the development of wind farms over distributed, small, home-based turbines, the cost incentives for research effectively privatizes the commons. It is privatization through economies of scale, appropriation through (unbalanced) competition.

Given the obvious seriousness of climate change and depth of the problems with the dominant discourse discussed here, how can radicals best approach the issue of climate change without losing focus, becoming (or remaining) ineffective or betraying a commitment to a wider project of social change? Many radical communities are already acting in ways that have the potential to threaten and sever these deep roots of both the climate change problem and the dominant solution discourse.

Again, this hegemonic order influences what is perceived as a ‘conceivable’ response to environmental problems. For example, reigning ‘green’ energy discourse is focused on change of inputs rather than changes in our relationship with energy; environmental degradation is pigeonholed as a problem of pollution and resource scarcity as opposed to tackling how we see ourselves in relation to the non-human world. Thus, the problem of climate change, despite being so big as to be a perfect metaphor for the complexity of environmental problems on the whole, is reduced to one of trading pollution rights and carbon-free energy inputs. This is the reigning ‘flavor’ of green education, one that maintains the normative framework of the energy regimes of the past.

Consequently, what needs to be emphasized is the importance of carefully articulating the ‘flavor’ of radical environmental education. The way in which this education frames environmental problems and prevailing solution options must be understood as a foundational element of the larger radical solution scheme. Those seeking change must develop and disseminate discourse that offers the tools necessary to conceive of different modes of life, that is, a counter-hegemonic radical green articulation. Armed with the language of an alternative discourse, anyone becomes capable of describing (to themselves most importantly) how their daily practices and internalized values are bound up in the ‘growth = wealth = good life’ hegemony.

In this way, careful articulation or framing of environmental problems and solution schemes can not only encourage a more positive direction in environmental discourse, but offers the ability to incorporate other social problems as well. Issues of access to/affordability of more ‘green’ ways of life are necessarily linked with the social and economic systems that are hegemonic in their framing of those problems. Radicals must articulate approaches to dealing with climate change that account for these disparities, proposing solution schemes that are simultaneously grand enough to envision deep changes in hegemonic social relations and radical visions of the future while remaining grounded in day-to-day realties. For example, mainstream schemes include massive hydrogen or electric grids to support a revamped vehicle fleet. This disproportionately affects those capable of making the investment necessary to upgrade and those who rely primarily on personal forms of transportation. Alternatively, an expansion and modernization of public transportation systems and the promotion and support of human-powered options (i.e. walking, bicycling) have the potential to benefit a much wider swath of society.

Fortunately, the careful articulation of social problems within solutions that remain grounded in day-to-day reality is something radical communities are pretty good at. Indeed, the articulation of contemporary radical politics has evolved its early focus on style, moved past a focus on confrontation with economic and political institutions, and has blossomed into a complex network of communities, organizations and institutions. Radical communities are doing their best to operate outside of those systems they wish to change, building potentially powerful foundations for their vision of another world. Heavily influenced by the DIY (Do-it-Yourself) ethic of the modern Punk community, these efforts reflect a growing understanding among activists of the important differences between reflex, reaction, and action:

REFLEX is to get pissed off...To talk shit...To get drunk...To bicker and complain.

REACTION is throwing bricks...It’s stealing food and eating out of dumpsters...It’s a defense...It’s saying “NO!”

ACTION is growing vegetables...Action is saying “yes” to community needs...It is building our own future.

(Augman 2005: 236)

Over the past fifteen years, radical communities have focused their energies on the development of a diverse set of social, political and cultural institutions including bookstores, infoshops, zines, bands, food distribution schemes, broadcasting stations, internet databases, libraries, cafes, squats, video networks, public kitchens, clubs, online message boards, record labels, bars, and more. While this may seem like nothing more than an inflated opinion of your local anarchist coffee-shop, these activities and practices have the potential to create entirely new social arrays, altering the expectations, values and belief systems of individuals by linking counter-hegemonic social conventions with foundational, everyday material practices. Gardens, childcare co-ops, bicycle lanes and farmers’ markets can combine theory with practice in ways that form strong social-material-psychological bonds, bonds that are the bedrock for developing alternative ways of living.

Thus, contemporary radical communities should strengthen what they’ve made and continue to do what they’re good at: building with culture. Rather than developing broad plans and strategies, the best place for radicals to begin creating change is in their own lives and build from there, constantly expanding the scope of projects and educating with the power of material practices. As Feeny hinted at in Workers Solidarity, ‘green’ education has the potential to be either exclusionary and/or unproductive if it does not connect with the reality of individuals’ daily lives. By creating the resources for environmentally sustainable ways of life, radical communities have something to offer. This approach allows for individuals to understand and tackle the large-scale social and economic aspects of environmental problems by framing these issues within the context of daily life.

This decentralized, emancipatory and human-scaled approach to environmental change is a marriage of theory and praxis I term Radical Green Populism.

Radical Green Populism represents a complex intersection of ideas, people and practice. It is also a material practice, a theory best expressed in terms of concrete reality, one that flowers when connected to lived experience. Thus, I present Radical Green Populism (RGP) as a temporary label. It is a simplified way of expressing a complex reality. For that reason, I have chosen these three words carefully to demonstrate this intersection of theory, practice and values.

Radical Green Populism is radical. It is built upon inherited tenets of autonomy, means-over-ends praxis, self-reliance, community, and personal responsibility. Its historical foundations are broad, spanning numerous radical traditions, including Marxism, anarchist thought, utopianism, deep ecology, social ecology, and myriad others. RGP is one way these traditions have converged to address environmental issues, an organic expression of activists using the tools around them.

Radical Green Populism is green. However, its ‘green’ is neither a cleaner form of conspicuous consumption nor a centralized system of impersonal and monolithic ‘green’ energy systems. Because of its radical foundations, RGP links green issues with other social problems, avoiding the tendency toward simple fixes and shallow analyses of the source of environmental problems. RGP’s normative framework is centered around a basic desire to see the human species live harmoniously as a part of the Earth and its ecological systems rather than separate from them. Thus, green change must be deep change, a fundamental shift in current normative and ideological systems that infuses ecological sensitivity into thought, relationships and practice.

Radical Green Populism is also populist. It’s populist in that it is ‘of the people,’ arguing that real change can and must come from the people. It is an approach that places the power to create change in the hands of individuals rather than representatives, in the hands of communities instead of corporations. RGP’s paths to change are human-scaled approaches, changes that can be made in one’s own life, a counter to authoritarian visions of centralized energy systems or stepping back and letting NGOs and government ‘take care of it.’ RGP is a path to change that re-appropriates power by subverting and reclaiming the social and cultural institutions of daily life, remaking them and creating alternatives that are infused with logic of a radical ecology.

Radical Green Populism is a model or framework to potentially guide further discussions of radical responses to climate change. It comes from a strong desire to see radical discourse that can encompass both the large and small-scale elements of social change. Environmental problems must be framed and connected to the social systems that created them. However, this can prove to be an overwhelming picture. By linking that critique to alternative visions, embodied in day-today material practices that can fulfill basic needs, the overwhelming picture suddenly becomes a bit more manageable.

That said, there remains a great deal of work to be done. Again, this is a framework for discussion; the internal elements of the strategy must be filled in. Radicals must carefully deliberate the development of alternative social institutions and intellectual resources for subversion and, ultimately, change. What will they look like? Self-managed energy systems, car and bicycle shares, farming collectives, green technology design firms, recycling and composting operations, construction and refitting operations...the needs are broad and the possibilities are endless, but each must be carefully considered. What institutions and resources might prove most valuable over the long term? What institutions and resources can help strengthen radical communities? What institutions and resources would other communities be best served by, a particularly important question in the process of broadening the cultural-social unity of a wide social base for change.

Finally, radicals must also consider the limits of this form of change. Is this an exclusively incremental, decentralized vision or one capable of maneuvering rapid, dramatic shifts in power? What communities and cultures might be alienated by such an approach?

All of these questions are intended to spark discussion and debate, something that is already underway in many radical communities.

Feeney, Chekov. 2009. “Thinking About Anarchism: The Politics of Climate Change” Worker Solidarity. Issue No. 110.

Pinchot, G. & Ettinger, J. (1925). Report of the Giant Power Survey Board to the General Assembly of the Commonwealth of Pennsylvania. Harrisburg, PA: The Telegraph.

Winner, L. 1982. “Energy Regimes and the Ideology of Efficiency” in Daniels, G.H., Rose, M.H. eds. Energy and Transport: Historical Perspectives on Policy Issues. New York: Sage Publications.

Yergin, D. (1991). The Prize: The Epic Quest for Oil, Money & Power. New York: Simon & Schuster.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



Feeney, Chekov. 2009. “Thinking About Anarchism: The Politics of Climate Change” Worker Solidarity. Issue No. 110.



Pinchot, G. & Ettinger, J. (1925). Report of the Giant Power Survey Board to the General Assembly of the Commonwealth of Pennsylvania. Harrisburg, PA: The Telegraph.



Winner, L. 1982. “Energy Regimes and the Ideology of Efficiency” in Daniels, G.H., Rose, M.H. eds. Energy and Transport: Historical Perspectives on Policy Issues. New York: Sage Publications.



Yergin, D. (1991). The Prize: The Epic Quest for Oil, Money & Power. New York: Simon & Schuster.



To provide the variety of goods and services that sustain them, modern societies have created elaborate socio-technical systems that link production, distribution, and consumption in coherent patterns. Within such systems, the activities of work, management, finance, planning, marketing, and the like are coordinated in highly developed institutional arrangements. These institutions, together with the physical technologies they employ, can well be characterized, borrowing a term from political theory, as “regimes” under which people who use energy are obliged to live. Such regimes of instrumentality have meaning for the way we live not unlike regimes in politics as such. It is possible to examine the full range of structural features contained in a particular socio-technical arrangement and to identify the qualities of its rules, roles, and relationships. (Winner 1982: 271)



Finally, the third major problem is that many proposed solutions do not question at all the current political and economic order. This leads to solutions such as “the power of one” — solutions based on consumer choice and education. In reality, consumers generally don’t get enough information to truly make informed choices, while very few have enough money to actually have any significant choices in the marketplace. The major over-riding problem is that our world is organized according to competitive principles and maximizing the profits of the wealthy. Given this reality, common problems that require broad, cooperative input from the entire species are difficult or impossible to address. If we can get rid of that problem, stopping and reversing climate change will be child’s play in comparison. (Feeney 2009)



REFLEX is to get pissed off...To talk shit...To get drunk...To bicker and complain.


REACTION is throwing bricks...It’s stealing food and eating out of dumpsters...It’s a defense...It’s saying “NO!”


ACTION is growing vegetables...Action is saying “yes” to community needs...It is building our own future.


(Augman 2005: 236)

