
We live in an era of decadence. The world is peopled only with walking corpses. Everything that moves, moves slowly. A sovereign indolence weighs on nations and individuals alike. However, looking deeply into this human charnel house, we glimpse the subterranean life that stirs, swarms and sometimes ventures to the surface. Our century is a century of transition; under its visible inertia an immense transformation is taking place. This is not yet the complete death of the old social order, but it is already the beginning of the new. The operation, although it is latent, is nonetheless real. Government, property, family, religion, everything that makes up the organism of the civilized societies breaks down and begins to rot. There are no more morals; the morals of the past no longer have any sap; those of the future are still only a sprout. What is good for the one, is evil for the other. Justice has no criterion other than force; success legitimates all crimes. Mind and body are prostituted in the commerce of mercantile interests. Pleasures are no longer possible, if they are not the pleasures of the brute. Dignity, friendship, and love are banished from our mores, lie separated from one another, or perish, strangled, as soon as they want to dawn across this officially bourgeois society. There is no more grace or beauty in this world, no naïve smile or delicate kiss. The feeling for art is replaced by the taste for the disgusting and grotesque. Society, in its decrepitude,  resorts to bloody flagellations to over-stimulate its old carcass and sometimes still give itself some dreadful semblance of virility. Atony and gangrene have blunted all its capacities for labor, as well as for pleasure. It can no longer enjoy anything. For it, work is a punishment and pleasure a labor. It does not know what it wants or what it does not want. Everything weighs on it; it stumbles and sinks in all sorts of depravity and cowardice. It wants to escape from that horrible nightmare, to shake off the burden of degradation that suffocates it; it looks forward to waking up; it knows that it only has to stand up on its feet to destroy that oppression, and it is so drained that it does not have the strength to rise, or the courage to conquer its numbness. And yet the idea ferments in it, and enlightens it internally in its sleep, until it is powerful enough to make it open its eyes and shine from its pupils. One side of its life, its robe of flesh, is left in the sepulcher of the past; the other side, its mind or spirit, floats on the winds of the future.

It is up to us, revolutionaries, tatters of humanity whom the breath of progress lifts, social rags that the light of understanding colors with its purple fires, and that it displays above the Civilized like a scarecrow or a flag,—a scarecrow for those who want to remain stationary, and a flag for those who want to press forward,–it is up to us to stimulate the work of decomposition, up to us to try to indicate the stone that holds Humanity in immobility, up to us to open the paths of universal regeneration.

Two manners of acting present themselves to those who want to become propagators of new ideas. One is calm, scientific discussion, without renouncing anything of principles, to report them, and comment on them with a fine courtesy and firm restraint. This process consists of injecting truth drop by drop into minds that are already prepared, elite intelligences, still beset by error, but animated by good will. Missionaries of Liberty, preachers with smiling faces and caressing voices, (but not hypocrites,) with the honey of their words they pour conviction into the hearts of those who listen to them; they initiate into the knowledge of truth those who have a feeling for it. The other is bitter argument, although scientific as well, but which, standing firm in the principles as in a coat of mail, arms itself with Scandal as with an axe, to strike redoubled blows on the skulls of the prejudiced, and force them to move under their thick covering. For those, there are no words blistering enough, no expressions cutting enough to shatter all these ignorances of hardened steel, that that dark and weighty armor that blinds and deafens the dull masses of the people. All is good to them–the sharp sting and the boiling oil—in order to make these apathetic minds tremble to their heart of hearts, under their tortoise shells, and to make resonate, by tearing at them, these fibers which do not ring out. Aggressive circulators, wandering damned and damnators, they march, bloodthirsty and bleeding, sarcasm on the lips, the idea before them, torch in the hand, across hatreds and hisses, to the accomplishment of their fateful task; they convert as the spirit of hell converts: by bite and fire.

The two approaches are good and useful, depending on the sorts of listeners we encounter along our way. Some require one, and some require the other. For both, it is a matter of temperament, a question of their condition in the current society. They can even be alternately applied, according to the disposition of the mind or the environment in which we find ourselves. Both, if they do not back down from the principles, if they cling firmly to liberty, are agents provocateurs [in the sense of inciting agents] of the Revolution. However, in our civilized societies, it is the smallest number who are disposed to listen. The greatest number turn a deaf ear, and it is by Scandal that one pierces the eardrum.

How, anyway, not to employ words forged with the tongue of scorn to penetrate into this manure of the world where strut, like some like some poisonous mushrooms, the round, flat faces of the ignominious bourgeoisie. Can one employ anything but the teeth of a pitchfork to speak to these vegetations of legal matters? Does all of that feel? Does all of that think? Can a man with a heart live in such a society? Is he called to live only to drag along his days among that filthy rabble? Is it my fault, it is our fault, who have in our heart the poetry of the future, if nature has given us some disposition to love, an intelligence of the good, enthusiasm for the beautiful, and if we encounter at every step only intellectual and moral deformities? Is it our fault if in such a society we only find hate to dispense, if there we can only revel in disgust?

O Scandal! Vengeful fury, be my companion as long as the world remains the old world, as long as bourgeois obesity and obscenity ripen on the velour of exploitation, as long as servility and idiocy of the workers will grovel in the rut and under the halter of capital!

Yes, there must be some like me, like us–the cursed, the rebels–to march unbending–in the direction of progress, to move the inert blocks, to face the avalanches of stones and smooth the way for those who have the same goal, but who make the propaganda in less irritating forms, who engage in polemics with more peaceful epithets.

Scandal, avenging fury, to you my pen and my lips!

It is through you that shame enters the hearts of men. It is through you that their minds awaken to enlightenment. It is through you that the wicked tremble, and through you that the good hope.

If there is still, or rather if there is already some modesty in the world, Scandal, avenging fury, great redresser of morals, it is to you that it is owed.

It is you that forces enemies of the new idea to serve this idea by criticizing it. All who speak of socialism, for good or evil, spread socialism by spreading its name. Sooner or later truth emerges from untruth, it gets the better of its detractors in the long run. Only silence is harmful, and it is you, Scandal, who imposes speech on the mute and, whether they like it or not, forces them to make themselves heralds of that which they persecute.

Scandal, anarchic authority, you are more powerful than all the authorities of the official world. The kings and the bourgeois, the emperors and their subjects can only put the gag of death on the mouths of men; you, voice strident, fiber electric, you make even the stones speak!

O Scandal! Great educator of the deaf and mute, revolutionary breath, satanic deity, spread your wings and vibrate over the world; bring forth the idea from all these skulls of granite, like the sibylline sounds from the depths of the grottos.

Scandal, you are the organ that makes the Civilized bow down their heads in their shame, and that their thought raises up the spheres of future harmony.

Bellow and rumble still, provocative storm. Your thunder-bursts are a salutary anthem.

My pen and my lips are yours, Scandal!




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

