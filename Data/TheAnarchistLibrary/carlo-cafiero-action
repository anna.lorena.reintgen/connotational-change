
There’s no reason for scholars to shrug their shoulders so much, as if they had to bear the weight of the whole world: it wasn’t they who invented the revolutionary idea. It was the oppressed people, who by their often unconscious attempts to shake off the yoke of their oppressors drew the attention of scholars to social morality; and it was only later that a few rare thinkers managed to find this insufficient, and later still that others agreed to find it completely false.

Yes, it is the blood split by the people which ends by forming ideas in scholars’ heads. ‘Ideals spring from deeds, and not the other way round,’ said Carlo Pisacane in his political testament, and he was right. It is the people who make progress as well as revolution: the constructive and destructive aspects of the same process. It is the people who are sacrificed every day to maintain universal production, and it is the people again who feed with their blood the torch which lights up human destiny.

When a thinker who has carefully studied the book of the sufferings of mankind defines the formula of popular aspiration, – conservatives and reactionaries of all kinds all over the world begin shouting at the top of their voices: ‘It’s a scandal!’

Yes, it is a scandal: and we need scandals; for it is by the force of scandal that the revolutionary idea makes its way. What a scandal was stirred up by Proudhon when he cried: ‘Property is theft!’ But today there is no man of sense or feeling who does not think that the capitalist is the worst scoundrel among thieves; more than that, – the only true thief. Armed with the most terrible instrument of torture, hunger, he torments his victim, not for a moment but for a lifetime: he torments not only his victim, but also the wife and children of the man he holds in his power. The thief risks liberty and often life, but the capitalist, the real thief, risks nothing, and when he steals he takes not just a part but the whole of the wealth of the worker.

But it is not enough to find a theoretical formula. Just as the deed gave rise to the revolutionary idea, so it is the deed again which must put it into practice.

At the first Congress of the International, there were only a few workers in the French proletariat who accepted the idea of collective property. It needed the light which was thrown on the whole world by the incendiaries of the Commune to bring to life and to spread the revolutionary idea, and to bring us to the Hague Congress, which by the votes of 48 representatives of the French workers recognised free communism as the goal. And nevertheless we still remember that certain authoritarian dogmatists, full of seriousness and wisdom, repeated only a few years ago that the Commune had checked the socialist movement by giving rise to the most disastrous of reactions. Facts have shown the soundness of the opinions of these ‘scientific socialists’ (most of them knowing no science) who tried to spread among socialists the well-known ‘politics of results’.

So it is action which is needed, action and action again. In taking action, we are working at the same time for theory and for practice, for it is action which gives rise to ideas, and which is also responsible for spreading them across the world.

But what kind of action shall we take?

Should we go or send others on our behalf to Parliament, or even to municipal councils?

No, a thousand times No! We have nothing to do with the intrigues of the bourgeoisie. We have no need to get involved with the games of our oppressors, unless we wish to take part in their oppression. ‘To go to Parliament is to parley; and to parley is to make peace,’ said a German ex-revolutionary, who did plenty of parleying after that.

Our action must be permanent rebellion, by word, by writing, by dagger, by gun, by dynamite, sometimes even by ballot when it is a case of voting for an eligible candidate like Blanqui or Trinquet. We are consistent, and we shall use every weapon which can be used for rebellion. Everything is right for us which is not legal.

‘But when should we begin to take our action, and open our attack?’ friends sometimes ask us. ‘Shouldn’t we wait until our strength is organised? To attack before you are ready is to expose yourself and risk failure.’

Friends, if we go on waiting until we are strong enough before attacking, – we shall never attack, and we shall be like the good man who vowed he wouldn’t go into the sea until he had learnt to swim. It is precisely revolutionary action which develops our strength, just as exercise develops the strength of our muscles. True, at first our blows will not be deadly ones; perhaps we shall even make the serious and wise socialists laugh, but we can always reply: ‘You are laughing at us because you are as stupid as those who laugh at a child falling down when it learns to walk. Does it amuse you to call us children? All right then, we are children, for the development of our strength is still in its infancy. But by trying to walk, we show that we are trying to become men, that is to say, complete organisms, healthy and strong, able to make a revolution, and not scribbling editors, old before their time, constantly chewing over a science which they can never digest, and always preparing in infinite space and time a revolution which has disappeared into the clouds.’

How shall we begin our action?

Just look at an opportunity, and it will soon appear. Everywhere that rebellion can be sensed and the sound of battle can be heard, that is where we must be. Don’t wait to take part in a movement which appears with the label of official socialism on it. Every popular movement already carries with it the seeds of the revolutionary socialism: we must take part in it to ensure its growth. A clear and precise ideal of revolution is formulated only by an infinitesimal minority, and if we wait to take part in a struggle which appears exactly as we have imagined it in our minds, – we shall wait for ever. Don’t imitate the dogmatists who ask for the formula before anything else: the people carry the living revolution in their hearts, and we must fight and die with them.

And when the supporters of legal or parliamentary action come and criticise us for not having anything to do with the people when they vote, we shall reply to them: ‘certainly, we refuse to have anything to do with the people when they are down on their knees in front of their god, their king, or their master; but we shall always be with them when they are standing upright against their powerful enemies. For us, abstention from politics is not abstention from revolution; our refusal to take part in any parliamentary, legal or reactionary action is the measure of our devotion to a violent and anarchist revolution, to the revolution of the rabble and the poor.’




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

