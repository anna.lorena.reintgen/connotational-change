
Wearied by the struggle of life, how many close their eyes, fold their arms, stop short, powerless and discouraged. How many, and they among the best, abandon life as unworthy of continuance. With the assistance of some fashionable theories, and of a prevalent neurasthenia, some men have come to regard death as the supreme liberation.

To those who hold this view, society replies with the usual clichés.

It speaks of the “moral” purpose of life; argues that one has no right to kill himself, that “moral” sorrows must be borne courageously, that a man has duties, that the suicide is a coward or an “egoist”, etc. etc. All of these phrases are religious in tone; and none of them are of genuine significance in rational discussion.

What after all is suicide?

Suicide is the final act in a series of actions that we all tend to carry out, which arise from our reaction against our environment, or from that environment’s reaction against us.

Every day we commit suicide partially. I commit suicide when I consent to inhabit a dwelling where the sun never shines, a room where the ventilation is so inadequate that I feel like I am suffocated when I wake up.

I commit suicide when I spend hours on work that absorbs an amount of energy which I am not able to recapture, or when I engage in activity which I know to be useless.

I commit suicide whenever I enter into the barracks to obey men and laws that oppress me.

I commit suicide whenever I grant the right to govern me for four years to another individual through the act of voting.

I commit suicide when I ask a magistrate or a priest for permission to love.

I commit suicide when I do not reclaim my liberty as a lover, as soon as the time of love is past.

Complete suicide is nothing but the final act of total inability to react against the environment.

These acts, which I have called partial suicides, are no less truly suicidal.

It is because I lack the strength to react against society, that I inhabit a place without sun and air, that I do not eat in accordance with my hunger or my taste, that I am a soldier or a voter, that I subject my love to laws or compulsion.

Workers daily commit mental suicide by leaving the mind inactive, by not letting it live, as they kill within themselves their enjoyment of the arts of painting, sculpture, music, which offer some relief from the cacophony which surrounds them.

There can be no question of right or duty, of cowardice or of courage in relation to suicide; it is purely a material problem, of power or lack of power. One hears it said, “Suicide is a human right when it constitutes a necessity ...” Or again, “one cannot take the right of life and death away from the proletariat.”

Right? Necessity?

Shall one debate his right to breathe poorly, i.e., to kill most of the health-giving molecules to the advantage of the unhealthy ones? His right not to eat in accordance with his hunger, i.e., to kill his stomach? His right to obey, i.e., to murder his will? His right to love the woman designated by the law or chosen by the desire of one period forever, i.e., to slay all. the desires of days to come?

Or if we substitute the word “necessity” for the word “right” in these phrases, do we thereby make them the more logical?

I do not intend to “condemn” these partial suicides more than definitive suicides; but it seems to me pathetically comical to describe as right or necessity this surrender of the weak before the strong — and a surrender made without having tried everything. Such expressions are merely excuses one clings to.

All suicides are imbecilities, total suicide more than the others, since it is possible to bring oneself out of the partial forms.

It would seem that at the moment of the departure of the individual, all energy might be focused on a single point of reaction against the environment, even with a thousand to one chance of failure in the effort. This seems still more necessary and natural in view of the fact that one leaves those one loves behind. For this part of one’s self, this portion of the energy of which one consists, cannot one engage in a gigantic struggle, however unequal the combat, capable of shaking up the colossal Authority?

Many die, declaring themselves to be victims of society; do they not realize that, since the same cause produces the same effects, their comrades, those they love, could die as victims of the same state of things? Won’t a desire then come to them to transform their vital force into energy, into power, so as to burn the pile rather than to separate its elements?

Once one has overcome the fear of death, of the complete dissolution of the human form, one can engage in the struggle with that much more strength.

Some will respond to us, “We have a horror of bloodshed. We do not wish to attack this society, made up of men who seem to us to be both unaware and irresponsible.”

The first objection does not hold. Does the struggle only take a violent form? Is it not multiple, diverse? And all the individuals who understand its usefulness, can they not take part each according to his own temperament?

The second is too inexact. Such words as “society”, “knowledge”, “responsibility” are too often repeated and too little explained.

The barrier that obstructs the road, the biting serpent, the tuberculosis microbe are unaware and without responsibility, yet we defend ourselves against them. Still more irresponsible (in the relative sense) are the cornfields which we reap, the ox that we kill, the beehive that we rob. Nevertheless we attack them all.

I know nothing of “responsible” nor of “irresponsible”. I see the causes of my suffering, of the cramping of my personality; and my efforts are bent to suppress or to conquer them by every possible means.

According to my power of resistance I assimilate or I reject, I am assimilated or rejected. That is all.

Even stranger objections are advanced, in a form neurotically scientific: “Study astronomy, and you will realize the negligible duration of human life as compared to the infinite ... Death, is a transformation and not termination.”

For myself, being finite, I have no conception of the infinite; but I know that duration consists of centuries, centuries of years, years of days, days of hours, hours of minutes, etc. I know that time is made up of nothing but the accumulation of seconds, that great immensity formed from the in-finitely small. Short as our life may be, it has its dimensional importance from the point of view of the whole. Life, seen from my own point of view, with my own eyes, cannot be of little importance to me; and all seems to me to have had no purpose but to prepare for us — for myself and for that which surrounds me.

The stone which caresses the head when dropped from a meter above, will break it open if it falls twenty meters. Arrested on the way, seen from the point of view of the whole, it differs in no particular; but it lacks the energy which makes it a power.

I disregard all that I cannot conceive, and look primarily to myself; and a dissolution or rather a non-absorption of strength that acts to my detriment occurs in either a partial or a definitive suicide.

Death is the end of a human energy, as the dissociation of elements of a battery is the end of the electricity which it releases, as the dissolution of threads of a tissue is the end of that tissue’s strength. Death, as the end of my “I”, is more than a transformation.

There are those who say to one, “The goal of life is happiness,” and who profess to be unable to attain it. It seems to me simpler to say that life is life. Life is happiness. Happiness is life.

All the acts of life are a joy to me. Breathing pure air, I know happiness; my lungs are expanded, an impression of power makes, me glow. The hour of work and that of rest afford me equal pleasure. The hour which brings the meal-time; the meal itself with its labor of mastication; the hour which follows with its interior activity — all give me joy of varying sorts.

Shall I evoke the delicious attention of love, the sense of power in the sexual encounter, the succeeding hours of voluptuous relaxation?

Shall I speak of the joy of the eyes, of hearing, of odor, of touching, of all the senses, of the delights of conversation and of thought? Life is a happiness .

Life has not a goal. It is. Why wish for a goal, a beginning, an end?

Let us recapitulate. Whenever, hurled on the stones by an earthquake, avid for air, we bow our head against the rock, whenever seized by the regimentation of society as it is, avid for the ideal (to make this vague term exact: avid for the integral development of one’s self and one’s loved ones) we arrest our life we obey, not a necessity nor a right, but as obsession of force, of the obstacle. We do no voluntary act, as the partisans of death profess; we obey the power of the environment which crushes, and we depart precisely at the hour the weight is too heavy for our shoulders.

“Then,” they say, “we do not go except at our hour — and our hour is now.” Yes. But since, resigned, they envisage their defeat in advance; since they have not developed their tissues with a view to resistance; they have not made due effort to react against the regimentation of the environment. Unaware of their own beauty, of their own force, they add to the objectives of the obstacle all the subjective weight of their own acceptance.

Like those resigned to partial suicides, they surrender themselves to the great suicide. They are devoured by an environment avid for their flesh, eager to crush all energy that appears.

Their error lies in the belief that the dissolution is by their own will, that they choose their hour, while actually they die crushed inevitably by the wickedness of some and by the of others.

In a locality by the maleficient of typhus, of tuberculosis, I do not think of absenting myself to avoid the malady, rather, I proceed immediately to disseminate disinfectant’s, without any fear of killing millions of microbes.

In present society, made foul by the conventional defecations of property, of patriotism, of religion, of family, of ignorance, crushed by the power of government and the inertia of the governed; I wish not to disappear, but to throw upon the scene the light of truth, to provide a disinfectant, to it by any means at my command.

Even with death approaching, I shall have still the desire to chair my body by means of phenol or acid, for the sake of humanity’s health.

And if I am destroyed in this effort, I shall not be totally effaced. I shall have reacted against the environment, I shall have lived briefly but intensely; I shall perhaps have opened a breach for the passage of energies similar to my own.

No, it is not life that is bad, but the conditions in which we live. Therefore we shall address ourselves not to life, but to these conditions: let us change them..

One must live, one must desire to live still more abundantly. Let us accept not even the partial suicides.

Let us be eager to know all experiences, all happiness, all sensations. Let us not be resigned to any diminution of our “me”. Let us be champion of life. so that desires may arise out of our turpitude and weakness; let us assimilate the earth to our own concept of beauty.

Thus may our wishes be united, magnificently; and at the last we shall know the Joy of Life in the absolute.

Let us love life




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

