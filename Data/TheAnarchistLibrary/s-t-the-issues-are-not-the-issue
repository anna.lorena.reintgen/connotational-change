      An Image from the Past      A Glimpse of the Future      Nothing Doing and Doing Nothing      Political Identity vs. Affinity      The Issues are not the Issue
Once upon a time, I found myself before dawn hiding in the kudzu and ivy that grew just below the treeline of a mountain gravel road. Time had slowed down, as it often does in those situations, but eventually the moment came when a dozen others, armed with locks, a soon-to-be-disabled car, and a tripod materialized out of the darkness to block the mine’s entrance. Looking back up the steep incline to see the barricade lit red by flares, rendering the further destruction of that beautiful place impossible for at least a few hours, remains one of my fondest memories.

Eight years have passed since that small experience. A lot of water has flowed under the bridge. I continue to be involved in struggle, though more out of a desire for survival, conflict, vengeance, and affinity than a hope for social change. Nevertheless the return of the Earth First! Rondy to my home state seemed an appropriate time to renew certain critical questions, questions that have been raised before by better writers than I but were seemingly set aside under the constant pressure to address the newest threat that would destroy The World. Though certainly a critique, I hope that this can be seen as a gesture of affinity and communication to people who also want to live wild and free.

The larger world of radical politics during my EF! years was suffocated by the anti-war movement, which was dominated by the Left and various socialist sects. These folks were lost in the anti-capitalist riots of the anti-globe era but at home in the lukewarm waters of “anti-imperialism.” Anarchists, for the most part, felt awkward and at odds with this period, especially those of us like myself who sharpened our political teeth in the street conflicts at the turn of the century. The anti-war days molded our thinking and our practices nonetheless. We became sequestered in “community building” and single-issue politics which could never fully reflect our ideas or desires. Earth First! made sense in some ways, as the best possible version of that model, so many of us got involved with eco-defense in this period.

The prevailing winds changed, however: riots broke out in the slums of Europe, Greece was set ablaze when Alexis was murdered, the black bloc re-awoke at the 08’ conventions, university occupations in 09’ refused to make any demands of Power, widespread and generalized antagonism to police broke out in the Northwest a year later, Oakland got revenge for Oscar Grant and a couple years later went on general strike. Many of us felt like we had come home again. Others remained in the activist house they had built for themselves, limited but comfortable. Seeking different experiences, we began to speak different languages that reflected not only conflicting analyses but, maybe even more divisive, different desires. This was not fundamentally a conflict over specific activities or post-rev visions (i.e. infrastructure vs. attack or green vs. red), but over how the matrix of capitalism, politics, activism, and “issues,” functioned, and thus over what it meant to try to intervene. Increasingly it has become difficult even to talk to each other, our words and deeds passing unheeded like ships in the night.

If it was not already, it became clear to many of us that single-issue politics and its activist campaigns were a dead-end. This understanding was rooted in the desires of admittedly impatient and unruly participants, as it should be, but also in a hardnosed analysis of late 21st century industrial capitalism, a system that is always able to evolve one step ahead of even the most radical demands, more than willing to replace fracking with tar sands, tar sands with coal, coal with wind, wind with solar, solar with hydro, hydro with nuclear, forever leaping from one issue to the next in perpetual self-preservation.

In reflection, I realized that what was meaningful about these EF! campaigns to me was not the ever-elusive possibility of reform or change but those rare accidental moments of rupture, the time when the lockdown unintentionally became a trampling mob destroying the office lobby, or when the Appalachian campaign spilled over into locals taking potshots at bulldozers with their .308s. This was not mere adventurism, but a real desire to break out of the stranglehold of politics.

I gave up on the idea of gradually increasing our power with small victories, for this approach had little to no basis in reality. Insurrections do not erupt on the surface of history via gradualist-oriented issueactivism. Put another way, Turkey is not currently exploding to save a tree-lined park; those trees are a coincidence that provides shade to the multitudes who rebel for a thousand different reasons against every aspect of capitalist life. Thousands of people do not riot to save a few trees or, for that matter, the life of one murdered youth. In this sense the struggle in Turkey is politically legible neither to Power nor to the social movements that would manage it, including the country’s radical environmentalists. This is an advantage.

The camps of Occupy, the Arab Spring, the austerity riots across Europe, the demand-less explosions which occur every time the police murder youth, the flash mobs that steal en masse, even just the general breakdown of civil society, all make it more clear where industrial society and our resistance are heading. Months after a black bloc awakens at the heart of a second Egyptian revolution, Turkey explodes, and weeks later Brazil’s cities are set ablaze by its poorest inhabitants, explained away by the media as a response to “corruption.” The time between these moments is decreasing, the ruptures themselves increasingly violent and generalized. We are entering a period where the state of exception is increasingly permanent and deterritorialized. This is our future. In this context, to speak of drawn out, gradually escalated strategic campaigns against specific ecological practices makes no sense.

After witnessing and participating in these events, many of us have tried to find a different path, keeping our love and fondness for the land while seeking new ways to develop into a social force that can contribute to a more total break with the society we live in. Like any experiment, this has been wrought with failures and mistakes. But we have also undoubtedly interrupted and intervened successfully in many of the aforementioned rebellions. Much of what was once specific to the trajectory discussed here has become general features of rebellion around the world: a refusal to make demands, the creation of autonomous communal spaces, a hatred of the police, a critique of the media, a critique of the Left, a critique of direct democracy, a sharpened understanding of recuperation, an emphasis on attack. To be sure, this generalization is not something any single ‘we’ can take credit for. These positions are as much descriptive as prescriptive, less the product of a certain milieu advocating certain strategies and more a reflection of modern life and social conditions. But this is our world, the one that creates us. Our revolt flows inside it, and must evolve alongside it.

Many of these positions incubated awkwardly during the mid-2000s, but are now reflected (albeit very unevenly) by everyone from Raging Grannies to homeless youth to New York Times editorialists. That such premises have found expression around the globe in so many circles, and yet stay more or less aloof from the Earth First! activist subculture, remains a mystery to me. When so much has changed, not just within the boardrooms of our enemies but in the kinds of revolt present among our friends, how can a network of creative and brilliant people still be doing activism and issue politics in the same old ways? When a formerly middle-class Obama voter can be heard articulating a critique of the demand-form at an illegal public encampment, how and why does such a critique elude the militants of Earth First? Do Earth Firsters still believe they can save the World one forest, one species, one dirty energy method at a time? Is the change they wish to see merely the summary of every individual campaign issue?

Driven by an almost theological morality, many will respond with the age-old strawman that to not do activism means to do nothing, that to not try to stop fracking or save the wolves we are letting the world burn. Such a statement may have held sway in earlier, quieter times, but the events of the past few years have exposed this to be a false dichotomy. I am not contesting involvement or even engagement with issues per se, but rather the manner in which it occurs and the intention behind the activity itself. Put another way, I would argue that what is exciting about the ZAD struggle in France is not stopping the airport, which will likely just be built elsewhere in France if the occupiers ‘succeed,’ but the actual rupture, the mass revolt itself, represented both by the conflicts with police as well as the network of communal relationships established via the illegal occupation. The activist would see the ZAD as a tactic to protect a piece of land; I am arguing that it should be seen instead as an end in itself, and perhaps a path to greater insurrectionary possibilities in the future.

One might suggest that this is all mere semantics, that it doesn’t matter why someone is excited about doing direct action as long as they’re doing it. This is wrong; that which we find meaningful and useful about an experience affects the kind of experiences we will choose to create in the future. It drives the trajectory of our struggle. If petition drives and scary home demos seem more ‘realistic’ ways of accomplishing a specific political goal, and that single issue is your priority, then you’re less likely to make strategic choices which later put you shoulder to shoulder with a thousand comrades fighting cops among the trees. If a moment of revolt happens in this activist context, as does sometimes occur, it is more as a coincidence than anything else, one which the participants will be ill prepared to spread and deepen.

Both literally and figuratively, the activist is often at the back of the surging crowd in such situations, dragging their feet and desperately trying to hold back a struggle that threatens to break the barriers of their carefully chosen issue-narrative. Many Earth Firsters will personally object to such a characterization, but it is a framework of doing politics I’m discussing, not the authenticity of its individual participants. How that framework contributes (intentionally or not) to techniques of government by sequestering revolt to “issues” is what concerns me. A more militant or DIY version of the same framework is not adequate.

The intention behind our activity also affects with whom we form relationships. Earth First! is traditionally an ally of mainstream enviro groups in many campaigns; as the ‘extremists’ they offer a convenient whipping boy for the Big Greens, but benefit from the institutional connections and power-broking that helps accomplish their issue-goals, all while maintaining a radical image. The historical analogy of MLK and Malcolm X is often made here, but misses the point that both these men were statists that were highly legible to Power, and were more or less politicians in their own way. When they ceased to be so, their relationship both to Power and each other changed dramatically.

Historically Earth First! itself has contributed to a critique of the Green Left, but it nonetheless continues to operate in the same framework. EF!ers are radical environmentalists, no doubt, but they are still environmentalists, still doing the same politics as Sierra Club and Greenpeace but in a more militant way. Is it any surprise that so many older EF!ers get day-jobs with Rainforest Action Network, Sierra Club, Greenpeace, etc.? A friendly relationship with the institutional Left makes sense given the group’s issue-focus. This is not an accusation of selling out—a meaningless epithet in any case—but it is worth thinking about how the political method we choose affects the relationships we prioritize.

If, on the other hand, one’s priority is to perpetuate a general culture (and develop new practices) of revolt, it makes more sense to be antagonistic to the Left but tight with one’s neighbors or co-workers or “non-political” friends, whomever one judges might go crazy with you when the shit hits the fan. Affinity rather than political identity becomes the center of gravity of the relationship. What someone “thinks about the environment” is meaningless to me. Do they hate the police? Do they hate work? Do they hate having mercury stored up in their gut? Do they hate some aspect of capitalist life? Do they want to knee-cap nuclear execs? Do we do similar kinds of crime to get by?Could I be friends with them, and do we have meaningful skills or ideas to share with each other or teach other? These questions are more interesting.

I realize none of this is particularly new. Around 15 years ago now participants in UK anti-road struggles raised many of the same points, and in 2007 an editor for the EF! Journal proclaimed “Earth First! Means Social War” loud and clear, attempting to shift the direction of a waning movement, writing that, “Political identity and its limited effects have reached their expiration date. What little autonomy we carved out by producing EF! as an activist approach is being taken from us. Whether we call it ‘climate justice’ or whether we relate our notion of we to a philosophy of biocentricism, we are still failing to draw lines that are based in reality.”

That expiration date is now long past. The priorities and restructuring of Capital in the 21st century, along with our own experiences of revolt of the last few years, have confirmed this fact irrevocably. The enemy we face is adaptable, flexible, horizontal, a better democrat and better environmentalist than any Earth Firster could ever hope to be. Likewise, the experience of comrades from Athens to Cairo has proven that it is easier to topple governments than to reform them. This can only be more true when an ‘issue’ strikes at the core of industrial society. The methodology of campaign activism that Earth First! has inherited from forest defense and the animal rights movement is hopelessly out of touch with this reality. Left to itself, would Earth First! as it currently stands have conducted Occupy as a campaign against corporate tax policies? Would it see the insurrection in Istanbul as a campaign to save a few urban trees? Would it reduce the 2008 riots in Greece to a way to achieve ‘criminal justice’ for Alexis’ murderers? I am left wondering.

Ultimately, Earth First!, a non-organization full of non-members, is besides the point. People will continue to intervene in ecological crises and struggles, as there are certain to be more of them, and the name with which they do so is irrelevant. But it is time to engage in a new way, with the conscious intention of breaking out of the barriers set by activism and issues. Political success is a quantitative thing that can be known through policy changes, polls, and statistics. It offers a degree of comfort in its legibility and pragmatism, and makes its participants feel reasonable. This continues to be the seductive logic of activism, militant or not. But this cannot be our logic.

The point is not to stop the Keystone Pipeline, for example, but to expand that struggle so that it becomes unrecognizable to its former self, so that it is no longer an ‘anti-pipeline movement’ but multitudes of different kinds of people revolting against intersecting aspects of capitalist life. Because a pipeline will eventually be built anyway, even if the route changes a hundred times, because there will be fracking, even it’s moved to another bioregion due to stronger resistance here, the center of gravity of our intervention must be fomenting general revolt, not “winning issues.” A critique of green capitalism does not alone accomplish this task, if our method remains enmeshed in issue politics. Building a dam to hold back individual flows of Capital is not a viable option anymore, if it ever was.

As a proposal this probably sounds ridiculous to at least a few readers, but it’s not so impossible as it sounds. Every neighborhood reaction to a police murder, every illegal encampment, every food riot, every prison fire, every land takeover of the last few years has taught us that any moment of disobedience has the potential to transform into a general ungovernability. We can contribute meaningfully to this potential in myriad ways, from helping a kid tie his shirt into a mask or calling out wouldbe politicians to building clever barricades or facilitating neighborhood assemblies. The skills we’ve learned as Earth Firsters are still useful, but the orientation has changed.

So I’m suggesting it’s time to take a deep breath and reorient ourselves. The monster of civilization will not be brought down by gradualist activist campaigns, small nighttime bands of eco-issue warriors, or some combination of the two. Nor will industrial capitalism simply collapse of its own weight, at least not into anything other than a nightmarish fascism. Accepting these realities does not mean abandoning struggle, but changing how and why we intervene. I still look back fondly on the days when I considered myself an Earth Firster, but as I read the reports from around the world, and think about my own experiences in the US, I must admit it feels like a very, very long time ago.

In love and struggle,

for good BBQ

and insurrection,

s.t.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

