      Anarchism vs. Trotskyism      D’Amato’s Experiences      Prefiguring a Stateless Society      Implications      References
An ISOer describes why he went from being an anarchist to being a Trotskyist. He expands on his experiences to make general criticisms of anarchism. I explain why his views are mistaken.

Recently a prominent member of the International Socialist Organization (ISO) of the US published a criticism of anarchism in the ISO’s newpaper, the Socialist Worker (D’Amato, 2009). It was particularly interesting because it was based on his own life experience.

(The ISO is in the wing of Trotskyism which rejects Trotsky’s theory that the Soviet Union under Stalin remained a “degenerated workers’ state.” Instead the ISO regarded the USSR, more-or-less correctly, as “state capitalist.” It was affiliated with the International Socialist Tendency, which was controlled by Britain’s Socialist Workers Party [no relation to the US group of the same name] until they split.)

Paul D’Amato reminisces, “...As a teenager...I found my way to anarchism first. Anarchism — hatred of all authority — flowed naturally from my reaction to the way I saw the world around me...” He and his friends went to an anarchist bookstore and bought pamphlets. He was impressed by the writings of Errico Malatesta, the great Italian anarchist, and quotes a passage from him because of “the way that it explained how capitalism reinforces its rule.”

However, “A few things made me drift away from anarchism: my own experience, and meeting socialists who did not identify socialism with state tyranny, as the Stalinists had done.” Today he is managing editor of the ISO’s journal, the International Socialist Review, and has written a book, The Meaning of Marxism.

The arc of my own political history has some similarities to, and differences from, that of D’Amato. (See Price 2008b.) As a teenager I became an anarchist, of the anarchist-pacifist trend. However, I became persuaded that a revolution was needed and that anarchist-pacifism could not make a revolution. I became a Trotskyist of the same trend that D’Amato is in now; I participated in founding the International Socialists, the predecessor organization of the ISO. Eventually my friends and I decided that the IS was too mushy and reformist, and we split to form the Revolutionary Socialist League. We sought a revolutionary, and eventually, libertarian, interpretation of Trotskyism. Finally we rejected Trotskyism altogether and became revolutionary, pro-organization, anarchists. We dissolved into the Love and Rage Anarchist Federation. Today I am a member of the US Northeastern Federation of Anarchist-Communists.

With this background, I think I can understand the reasons why someone would come to reject anarchism for Trotskyism, but I have come to disagree with this conclusion.

D’Amato tells of the experiences which turned him off to anarchism. While he was living in Britain, he writes, a socialist group was organizing demonstrations by unemployed workers to demand jobs. His anarchist friends refused to support this because they believed that “fighting for ‘the right to work’...was to fight for the right to be exploited.” Instead they advocated dropping out of the system by squatting, taking over abandoned buildings, and by living alternate, bohemian, lifestyles. He concluded, “No one...should begrudge anyone the lifestyle they chose, but all too often...personal revolt becomes a substitute for social and collective struggle for a better world.”

Later he was part of the Clamshell Alliance’s efforts to shut down the Seabrook Nuclear Power Plant in Massachusetts. He came to dislike the anarchists’ use of consensus, instead of majority rule, to run planning meetings. He felt that consensus led to endless long meetings, acrimony, and de facto minority rule.

To be fair, he admits “I later learned that by no means did all anarchists abstain from struggle for purist reasons, but ‘lifestyle’ anarchism is a strong strain in the movement, even among activists.” So if many anarchists are not “lifestylists,” and if even many “lifestylists” are also, he says, “activists,” then it is hard to see the value of this objection to anarchism.

My revolutionary class struggle anarchism is not opposed to people living nonconformist lives, being vegetarians, riding bicycles, or organizing alternate institutions, including squats or coops. But I do not regard this approach as a strategy for getting rid of capitalism and the state. My comrades and I advocate mass struggles by workers and oppressed people, leading ultimately to a workers’ revolution. Nor do we insist on consensus in our meetings; we use majority rule and democratic procedures. So D’Amato’s bad experiences with anarchists in his youth, while real, do not apply to the trend of anarchist-communism which many of us believe in.

Generalizing from his experiences, D’Amato concludes, “Anarchism makes the error of believing that the means to achieving a classless, stateless society must prefigure the end result....One does not expect the plow to prefigure the wheat; nor should we expect our methods of organizing to fight for a better world to prefigure or look exactly like the world we plan to achieve.”

This implies that means and ends have little relation to each other, and that revolutionary socialists should build institutions which do not embody the classless, stateless, goal. They should build revolutionary institutions — e.g., the “democratic-centralist” “vanguard party,” the “workers’ state”/”dictatorship of the proletariat” — which presumably are hard and sharp, like a steel plow.

While revolutionary anarchists do not focus on building coops and squats, we do indeed seek to “prefigure” the future. Our goal is a self-managed, radically democratic, society, and our method is to build self-managing, radically democratic, mass movements today. Our ends and our means are the same. We do not build centralized parties which seek to take power, but build revolutionary federations of anarchists which encourage workrs to take power for themselves.

Of course, there will have to be compromises. It is a straw man for D’Amato to say that anarchists “expect our methods of organizing to look exactly like the world we plan to achieve.” Not exactly, no. But as close to it as possible, with as much direct democracy as possible and as little centralization as is minimally necessary at any one time.

As is common among Trotskyists, D’Amato cites against us the Friends of Durruti Group from the Spanish Revolution of 1936–7. The Friends of Durruti disagreed with the main anarchist organizations, which had allied with pro-capitalist parties in the Popular Front, joining the bourgeois-democratic government of the Republic when it was fighting a civil war against the fascists. Instead they called for a strategy aimed at replacing both the Republican and fascist states with a federation of workers’ and peasants’ organizations, coordinating armed workers’ militias. He claims that they advocated “tak[ing] state power,” creating a non-prefigurative workers’ state/proletarian dictatorship, as do the Trotskyists.

As the heirs of the Friends of Durruti Group, we know that is not true. They advocated the workers taking power, but not taking STATE power, that is, not creating a new state (Price 2006). What is the state? In a statement which should be authoritative for Marxists, Engels (1972) described it as “an armed power...[a] special public force....It consists not merely of armed men but also of material appendages, prisons and coercive institutions of all kinds...organs of society standing above society...representatives of a power which estranges them from society...” (p.230). Does this sound like an institution through which the workers and oppressed could rule? But it certainly does not prefigure a classless, stateless, society!

After all previous revolutions, a minority continued to rule and exploit the majority. Therefore the ruling class needed a state, to hold down the majority. But after a socialist (anarchist) revolution, the ruling class will be the workers, in alliance with all the oppressed, that is the big majority holding down the minority of capitalists and their hangers on, for a period. This does not require a socially-alienated bureaucratic-police machine such as the state. It does require the self-organized, self-managed, working class, with its (prefigurative) democratic mass institutions and armed population.

To some it may seem as if I am quibbling. After all, the ISO says that it is for democratic movements, advocates a “workers’ state” which is controlled by the workers, seeks to build a revolutionary party which is “democratic centralist” (centralist but democratic), and opposes the belief of the orthodox Trotskyists that the Soviet Union was really some kind of “workers’ state”. It is these apparently libertarian-democratic aspects of this variety of Trotskyism which makes it possible for some activists, antagonized by various weaknesses in contemporary anarchism, to become Trotskyists of the ISO type (I am speaking from experience).

In fact, it does not take much experiece with the ISO to find out how undemocratic its internal life is or how manipulative it is in its organizing. The ISO broke from its international organization, when its British co-thinkers started giving the ISO arbitrary and bossy orders, without democratic discussion.

For several presidential elections the ISO opportunistically campaigned for Ralph Nader, despite his support for capitalism and the state. Apparently they thought that supporting a bourgeois politician who wants to run the bourgeois state and bourgeois economy was consistent with revolutionary socialism (after all, they did not have to prefigure the future society here and now). When asked how they justify this, one ISOer told me, “It builds the organization.”

Similarly they have supported voting for Hugo Chavez in Venezuela, although Chavez is running a bourgeois state and a bourgeois economy, despite his talk of “socialism.” With Nader and Chavez, the ISO made exactly the same mistake, the same crime against the working class, as did the leading Spanish anarchists of the 30s. The ISO denounces them (correctly) for joining the Spanish Popular Front with pro-capitalist parties and being willing to support a capitalist government. But how are the ISO’s politics any different?

While ISOers claim to be for a radically-democratic “workers’ state,” they believe that Lenin and Trotsky ran a “workers’ state” when they established a one-party police state after the Russian civil war. In fact, they believe that the Soviet Union continued to be a “workers’ state” under Stalin up until 1929 when he began a major industrialization drive (Cliff 1970; Price 2008a). Therefore they believe that there can be a so-called workers’ state, a rule of the working class, even after the workers have lost all political power for years. Someone else, such as the party, can stand-in for the working class. This is not qualitatively better than the views of the orthodox (“Soviet defensist”) Trotskyists. (Most members of the ISO may not know about this aspect of its theory of the state, or have not thought through the implications, but the leaders, such as D’Amato, have.)

Since the ISO believes that the “workers’ state” does not have to be prefigurative (it can be like a sharp metal plow), and since a “workers’ state” can exist without any control by the workers (as under Lenin and the early reign of Stalin), then it would seem dangerous to ever let the ISO get near state power. Although their practice in such areas as the Nader campaign makes them more likely to be like wishy-washy, defeated, social democrats than state-power-seizing state-capitalists!

It is a pity that D’Amato never found his way back to anarchism, in a revolutionary version.

Cliff, Tony (1970). Russia; A Marxist Analysis (3rd. ed.). London: International Socialism.

D’Amato, Paul (2009). Refusing to be ruled over. socialistworker.org

Engels, Frederick (1972). The Origin of the Family, Private Property, and the State. (Intro. and Notes by Eleanor Burke Leacock.) NY: International Publishers.

Price, Wayne (2006). Confronting the question of power. www.anarkismo.net

Price, Wayne (2008a). The degeneration of the Russian revolution; The date question. www.anarkismo.net

Price, Wayne (2008b). What I believe and how I came to believe it. www.anarkismo.net




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



Cliff, Tony (1970). Russia; A Marxist Analysis (3rd. ed.). London: International Socialism.



D’Amato, Paul (2009). Refusing to be ruled over. socialistworker.org



Engels, Frederick (1972). The Origin of the Family, Private Property, and the State. (Intro. and Notes by Eleanor Burke Leacock.) NY: International Publishers.



Price, Wayne (2006). Confronting the question of power. www.anarkismo.net



Price, Wayne (2008a). The degeneration of the Russian revolution; The date question. www.anarkismo.net



Price, Wayne (2008b). What I believe and how I came to believe it. www.anarkismo.net



An ISOer describes why he went from being an anarchist to being a Trotskyist. He expands on his experiences to make general criticisms of anarchism. I explain why his views are mistaken.

