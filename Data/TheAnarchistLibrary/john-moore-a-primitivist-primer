      Author’s note      What is anarcho-primitivism?      How does anarcho-primitivism differ from anarchism, or other radical ideologies?      Where, according to anarcho-primitivism, does power originate?      How does anarcho-primitivism view technology?      What about population?      How might an anarcho-primitivist future be brought about?      How can I find out more about anarcho-primitivism?
 This is not a definitive statement, merely a personal account, and seeks in general terms to explain what is meant by anarcho-primitivism. It does not wish to limit or exclude, but provide a general introduction to the topic. Apologies for inaccuracies, misinterpretations, or (inevitable) overgeneralizations.

Anarcho-primitivism (a.k.a. radical primitivism, anti-authoritarian primitivism, the anti-civilization movement, or just, primitivism) is a shorthand term for a radical current that critiques the totality of civilization from an anarchist perspective, and seeks to initiate a comprehensive transformation of human life. Strictly speaking, there is no such thing as anarcho-primitivism or anarcho-primitivists. Fredy Perlman, a major voice in this current, once said, “The only -ist name I respond to is ‘cellist’.” Individuals associated with this current do not wish to be adherents of an ideology, merely people who seek to become free individuals in free communities in harmony with one another and with the biosphere, and may therefore refuse to be limited by the term ‘anarcho-primitivist’ or any other ideological tagging. At best, then, anarcho-primitivism is a convenient label used to characterise diverse individuals with a common project: the abolition of all power relations — e.g., structures of control, coercion, domination, and exploitation — and the creation of a form of community that excludes all such relations. So why is the term anarcho-primitivist used to characterise this current? In 1986, the circle around the Detroit paper Fifth Estate indicated that they were engaged in developing a ‘critical analysis of the technological structure of western civilization[,] combined with a reappraisal of the indigenous world and the character of primitive and original communities. In this sense we are primitivists...’ The Fifth Estate group sought to complement a critique of civilization as a project of control with a reappraisal of the primitive, which they regarded as a source of renewal and anti-authoritarian inspiration. This reappraisal of the primitive takes place from an anarchist perspective, a perspective concerned with eliminating power relations. Pointing to ‘an emerging synthesis of post-modern anarchy and the primitive (in the sense of original), Earth-based ecstatic vision,’ the Fifth Estate circle indicated: We are not anarchists per se, but pro-anarchy, which is for us a living, integral experience, incommensurate with Power and refusing all ideology... Our work on the FE as a project explores possibilities for our own participation in this movement, but also works to rediscover the primitive roots of anarchy as well as to document its present expression. Simultaneously, we examine the evolution of Power in our midst in order to suggest new terrains for contestations and critique in order to undermine the present tyranny of the modern totalitarian discourse — that hyper-reality that destroys human meaning, and hence solidarity, by simulating it with technology. Underlying all struggles for freedom is this central necessity: to regain a truly human discourse grounded in autonomous, intersubjective mutuality and closely associated with the natural world. The aim is to develop a synthesis of primal and contemporary anarchy, a synthesis of the ecologically-focussed, non-statist, anti-authoritarian aspects of primitive lifeways with the most advanced forms of anarchist analysis of power relations. The aim is not to replicate or return to the primitive, merely to see the primitive as a source of inspiration, as exemplifying forms of anarchy. For anarcho-primitivists, civilization is the overarching context within which the multiplicity of power relations develop. Some basic power relations are present in primitive societies — and this is one reason why anarcho-primitivists do not seek to replicate these societies — but it is in civilization that power relations become pervasive and entrenched in practically all aspects of human life and human relations with the biosphere. Civilization — also referred to as the megamachine or Leviathan — becomes a huge machine which gains its own momentum and becomes beyond the control of even its supposed rulers. Powered by the routines of daily life which are defined and managed by internalized patterns of obedience, people become slaves to the machine, the system of civilization itself. Only widespread refusal of this system and its various forms of control, revolt against power itself, can abolish civilization, and pose a radical alternative. Ideologies such as Marxism, classical anarchism and feminism oppose aspects of civilization; only anarcho-primitivism opposes civilization, the context within which the various forms of oppression proliferate and become pervasive — and, indeed, possible. Anarcho-primitivism incorporates elements from various oppositional currents — ecological consciousness, anarchist anti-authoritarianism, feminist critiques, Situationist ideas, zero-work theories, technological criticism — but goes beyond opposition to single forms of power to refuse them all and pose a radical alternative.

From the perspective of anarcho-primitivism, all other forms of radicalism appear as reformist, whether or not they regard themselves as revolutionary. Marxism and classical anarchism, for example, want to take over civilization, rework its structures to some degree, and remove its worst abuses and oppressions. However, 99% of life in civilization remains unchanged in their future scenarios, precisely because the aspects of civilization they question are minimal. Although both want to abolish capitalism, and classical anarchism would abolish the State too, overall life patterns wouldn’t change too much. Although there might be some changes in socioeconomic relations, such as worker control of industry and neighbourhood councils in place of the State, and even an ecological focus, basic patterns would remain unchanged. The Western model of progress would merely be amended and would still act as an ideal. Mass society would essentially continue, with most people working, living in artificial, technologised environments, and subject to forms of coercion and control. Radical ideologies on the Left seek to capture power, not abolish it. Hence, they develop various kinds of exclusive groups — cadres, political parties, consciousness-raising groups — in order to win converts and plan strategies for gaining control. Organizations, for anarcho-primitivists, are just rackets, gangs for putting a particular ideology in power. Politics, ‘the art and science of government,’ is not part of the primitivist project; only a politics of desire, pleasure, mutuality and radical freedom.

Again, a source of some debate among anarcho-primitivists. Perlman sees the creation of impersonal institutions or abstract power relations as the defining moment at which primitive anarchy begins to be dismantled by civilized social relations. In contrast, John Zerzan locates the development of symbolic mediation — in its various forms of number, language, time, art and later, agriculture — as the means of transition from human freedom to a state of domestication. The focus on origin is important in anarcho-primitivism because primitivism seeks, in exponential fashion, to expose, challenge and abolish all the multiple forms of power that structure the individual, social relations, and interrelations with the natural world. Locating origins is a way of identifying what can be safely salvaged from the wreck of civilization, and what it is essential to eradicate if power relations are not to recommence after civilization’s collapse. What kind of future is envisaged by anarcho-primitivists? Anarcho-primitivist journal “Anarchy; A Journal of Desire Armed” envisions a future that is ‘radically cooperative & communitarian, ecological and feminist, spontaneous and wild,’ and this might be the closest you’ll get to a description! There’s no blueprint, no proscriptive pattern, although it’s important to stress that the envisioned future is not ‘primitive’ in any stereotypical sense. As the Fifth Estate said in 1979: ‘Let us anticipate the critics who would accuse us of wanting to go “back to the caves” or of mere posturing on our part — i.e., enjoying the comforts of civilization all the while being its hardiest critics. We are not posing the Stone Age as a model for our Utopia[,] nor are we suggesting a return to gathering and hunting as a means for our livelihood.’ As a corrective to this common misconception, it’s important to stress that that the future envisioned by anarcho-primitivism is sui generis — it is without precedent. Although primitive cultures provide intimations of the future, and that future may well incorporate elements derived from those cultures, an anarcho-primitivist world would likely be quite different from previous forms of anarchy.

John Zerzan defines technology as ‘the ensemble of division of labor/ production/ industrialism and its impact on us and on nature. Technology is the sum of mediations between us and the natural world and the sum of those separations mediating us from each other. It is all the drudgery and toxicity required to produce and reproduce the stage of hyper-alienation we languish in. It is the texture and the form of domination at any given stage of hierarchy and domination.’ Opposition to technology thus plays an important role in anarcho-primitivist practice. However, Fredy Perlman says that ‘technology is nothing but the Leviathan’s armory,’ its ‘claws and fangs.’ Anarcho-primitivists are thus opposed to technology, but there is some debate over how central technology is to domination in civilization. A distinction should be drawn between tools (or implements) and technology. Perlman shows that primitive peoples develop all kinds of tools and implements, but not technologies: ‘The material objects, the canes and canoes, the digging sticks and walls, were things a single individual could make, or they were things, like a wall, that required the cooperation of many on a single occasion .... Most of the implements are ancient, and the [material] surpluses [these implements supposedly made possible] have been ripe since the first dawn, but they did not give rise to impersonal institutions. People, living beings, give rise to both.’ Tools are creations on a localised, small-scale, the products of either individuals or small groups on specific occasions. As such, they do not give rise to systems of control and coercion. Technology, on the other hand, is the product of large-scale interlocking systems of extraction, production, distribution and consumption, and such systems gain their own momentum and dynamic. As such, they demand structures of control and obedience on a mass scale — what Perlman calls impersonal institutions. As the Fifth Estate pointed out in 1981: ‘Technology is not a simple tool which can be used in any way we like. It is a form of social organization, a set of social relations. It has its own laws. If we are to engage in its use, we must accept its authority. The enormous size, complex interconnections and stratification of tasks which make up modern technological systems make authoritarian command necessary and independent, individual decision-making impossible.’ Anarcho-primitivism is an anti-systemic current: it opposes all systems, institutions, abstractions, the artificial, the synthetic, and the machine, because they embody power relations. Anarcho-primitivists thus oppose technology or the technological system, but not the use of tools and implements in the senses indicated here. As to whether any technological forms will be appropriate in an anarcho-primitivist world, there is debate over this issue. The Fifth Estate remarked in 1979 that: ‘Reduced to its most basic elements, discussions about the future sensibly should be predicated on what we desire socially and from that determine what technology is possible. All of us desire central heating, flush toilets, and electric lighting, but not at the expense of our humanity. Maybe they are all possible together, but maybe not.’ What about medicine? Ultimately, anarcho-primitivism is all about healing — healing the rifts that have opened up within individuals, between people, and between people and nature, the rifts that have opened up through civilization, through power, including the State, Capital, and technology. The German philosopher Nietzsche said that pain, and the way it is dealt with, should be at the heart of any free society, and in this respect, he is right. Individuals, communities and the Earth itself have been maimed to one degree or another by the power relations characteristic of civilization. People have been psychologically maimed but also physically assaulted by illness and disease. This isn’t to suggest that anarcho-primitivism can abolish pain, illness and disease! However, research has revealed that many diseases are the results of civilized living conditions, and if these conditions were abolished, then certain types of pain, illness and disease could disappear. As for the remainder, a world which places pain at its centre would be vigorous in its pursuit of assuaging it by finding ways of curing illness and disease. In this sense, anarcho-primitivism is very concerned with medicine. However, the alienating high-tech, pharmaceutical-centred form of medicine practised in the West is not the only form of medicine possible. The question of what medicine might consist of in an anarcho-primitivist future depends, as in the Fifth Estate comment on technology above, on what is possible and what people desire, without compromising the lifeways of free individuals in ecologically-centred free communities. As on all other questions, there is no dogmatic answer to this issue.

A controversial issue, largely because there isn’t a consensus among anarcho-primitivists on this topic. Some people argue that population reduction wouldn’t be necessary; others argue that it would on ecological grounds and/or to sustain the kind of lifeways envisaged by anarcho-primitivists. George Bradford, in How Deep is Deep Ecology?, argues that women’s control over reproduction would lead to a fall in population rate. The personal view of the present writer is that population would need to be reduced, but this would occur through natural wastage — i.e., when people died, not all of them would be replaced, and thus the overall population rate would fall and eventually stabilise. Anarchists have long argued that in a free world, social, economic and psychological pressures toward excessive reproduction would be removed. There would just be too many other interesting things going on to engage people’s time! Feminists have argued that women, freed of gender constraints and the family structure, would not be defined by their reproductive capacities as in patriarchal societies, and this would result in lower population levels too. So population would be likely to fall, willy-nilly. After all, as Perlman makes plain, population growth is purely a product of civilization: ‘a steady increase in human numbers [is] as persistent as the Leviathan itself. This phenomenon seems to exist only among Leviathanized human beings. Animals as well as human communities in the state of nature do not proliferate their own kind to the point of pushing all others off the field.’ So there’s really no reason to suppose that human population shouldn’t stabilise once Leviathanic social relations are abolished and communitarian harmony is restored. Ignore the weird fantasies spread by some commentators hostile to anarcho-primitivism who suggest that the population levels envisaged by anarcho-primitivists would have to be achieved by mass die-offs or nazi-style death camps. These are just smear tactics. The commitment of anarcho-primitivists to the abolition of all power relations, including the State with all its administrative and military apparatus, and any kind of party or organization, means that such orchestrated slaughter remains an impossibility as well as just plain horrendous.

The sixty-four thousand dollar question! (to use a thoroughly suspect metaphor!) There are no hard-and-fast rules here, no blueprint. The glib answer — seen by some as a cop-out — is that forms of struggle emerge in the course of insurgency. This is true, but not necessarily very helpful! The fact is that anarcho-primitivism is not a power-seeking ideology. It doesn’t seek to capture the State, take over factories, win converts, create political organizations, or order people about. Instead, it wants people to become free individuals living in free communities which are interdependent with one another and with the biosphere they inhabit. It wants, then, a total transformation, a transformation of identity, ways of life, ways of being, and ways of communicating. This means that the tried and tested means of power-seeking ideologies just aren’t relevant to the anarcho-primitivist project, which seeks to abolish all forms of power. So new forms of action and being, forms appropriate to and commensurate with the anarcho-primitivist project, need to be developed. This is an ongoing process and so there’s no easy answer to the question: What is to be done? At present, many agree that communities of resistance are an important element in the anarcho-primitivist project. The word ‘community’ is bandied about these days in all kinds of absurd ways (e.g., the business community), precisely because most genuine communities have been destroyed by Capital and the State. Some think that if traditional communities, frequently sources of resistance to power, have been destroyed, then the creation of communities of resistance — communities formed by individuals with resistance as their common focus — are a way to recreate bases for action. An old anarchist idea is that the new world must be created within the shell of the old. This means that when civilization collapses — through its own volition, through our efforts, or a combination of the two — there will be an alternative waiting to take its place. This is really necessary as, in the absence of positive alternatives, the social disruption caused by collapse could easily create the psychological insecurity and social vacuum in which fascism and other totalitarian dictatorships could flourish. For the present writer, this means that anarcho-primitivists need to develop communities of resistance — microcosms (as much as they can be) of the future to come — both in cities and outside. These need to act as bases for action (particularly direct action), but also as sites for the creation of new ways of thinking, behaving, communicating, being, and so on, as well as new sets of ethics — in short, a whole new liberatory culture. They need to become places where people can discover their true desires and pleasures, and through the good old anarchist idea of the exemplary deed, show others by example that alternative ways of life are possible. However, there are many other possibilities that need exploring. The kind of world envisaged by anarcho-primitivism is one unprecedented in human experience in terms of the degree and types of freedom anticipated ... so there can’t be any limits on the forms of resistance and insurgency that might develop. The kind of vast transformations envisaged will need all kinds of innovative thought and activity.

The Primitivist Network (PO Box 252, Ampthill, Beds MK45 2QZ) can provide you with a reading list. Check out copies of the British paper Green Anarchist and the US zines Anarchy: A Journal of Desire Armed and Fifth Estate. Read Fredy Perlman’s Against His-story, Against Leviathan! (Detroit: Black & Red, 1983), the most important anarcho-primitivist text, and John Zerzan’s Elements of Refusal (Seattle: Left Bank, 1988) and Future Primitive (New York: Autonomedia, 1994). How do I get involved in anarcho-primitivism? One way is to contact the Primitivist Network. If you send two 1st class postage stamps, you will receive a copy of the PN contact list and be entered on it yourself. This will put you in contact with other anarcho-primitivists. Some people involved in Earth First! also see themselves as anarcho-primitivists, and they are worth seeking out too.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

