      Introduction      Anarcha-Feminism: Why the Hyphen?
In the lead up to the November 20, 2012 Vancouver launch of Volume Three of Anarchism: A Documentary History of Libertarian Ideas, I will be presenting some of the material I couldn’t fit in. Kytha Kurin was part of the collective which published the Open Road anarchist news journal from 1976 to 1990. The name was inspired by Emma Goldman, who originally wanted to name her monthly review The Open Road, from a Walt Whitman poem, but for copyright reasons had to use another name, ultimately choosing Mother Earth. At its peak, Open Road was the largest circulation English language anarchist publication in North America, with over 14,000 readers. Selections from Open Road, including this one, are included in Allan Antliff’s anthology, Only a Beginning (Vancouver: Arsenal Pulp Press, 2004). In this article, originally published in Open Road No. 11 (Summer 1980), Kytha Kurin describes how state laws regarding abortion and the failure of state authorities to deal with violence against women not only radicalized many women but also inspired some to become anarchists.

Robert Graham, 2012.

For many women, our first specifically feminist politicization came through demanding the right to abortion, that is, the right to control our own bodies. When anti-woman laws were exposed not as neglected holdovers of the Dark Ages, but as conscious means of reinforcing a woman’s body as property of the State, many feminists were prepared to work in political movements because we had already found ourselves in a political confrontation. There was no question of “learning” to make politics personal; the intimacy of the personal was made political by the intervention of the State.

Men hadn’t been so clearly confronted by this reality. In spite of the fact that most men sell their body/mind power and potential through wage slavery, and that their creative abilities are drained, suffocated and side-tracked into commodity consumption, many so-called radical men still acted as if they accepted an electoral definition of “politics’ —something you go out and “do” for at most, a few hours a day. While many men recognized the urgency of political activity (something’s got to change soon), most did not recognize the immediacy (we’ve got to make changes everyday)...

Anarchism, with its recognition that the process of making a revolution can’t be separated from the goals of that revolution, appeared to understand the political in much the same way that feminism did. Anarchists recognized that an authoritarian, exploitative movement could not possibly create a non-authoritarian, non-exploitative society. But what anarchist theory recognized, feminists demanded.

Anarchist meetings were not substantially different from other Left party meetings. There were some subjects that were relevant to political meetings and there were proper ways of speaking at political meetings. But feminists who now understood politics all too well, demanded that all types of domination and exploitation be recognized as political issues because when oppression confronts people in every aspect of their lives, how can some areas of living be acceptable for political work and others not? These feminists insisted on confronting domination, power tripping, and sexism right when it happened in a meeting instead of simply in the abstract or outside the group.

Feminists also refused to decapitate the “reasoning” self from the “emotional” self before participating in political meetings and demanded that the whole person, complete with warmth and confusion of life, be present. We exposed the irrationality of believing that a life direction that didn’t spring from a sensitivity to the totality of life could in any sane way be considered rational.

Most anarchists had never been asked to so directly live their anarchism and found the feminist insistence on “process” and the repeated “interruptions” about male domination upsetting. And many feminists who had been attracted by anarchist theory but were really more concerned with anarchist practice, felt frustrated and refused to be placated with the rhetoric that would have one believe that anarchists couldn’t possibly be authoritarian sexists.

So a lot of feminists left mixed groups. Some worked in anarcha-feminist groups and many gave up on anarchism altogether...

Confrontations over abortion rights being the catalyst to many women becoming political, a logical extension was the growth of self-help health collectives. Aware that authoritarian structures, whether of the State or radical political groups, retain the power of authority by hoarding and mystifying knowledge, feminists tried to avoid becoming the “new experts.”

They worked to reclaim the body as a natural organism that could be understood and cared for by women themselves rather than left to the authority of doctors, multi-billion dollar drug companies or even radical feminists. They tried to share skills among themselves and tried to share knowledge’ and skills with the “patients.” Thus, “self-help” health collectives rather than simply “women’s” health collectives.

But the big job of combatting the insidious drug pushing in our culture and the need for major medical research has meant that if feminists are to be really effective we have to also work outside our small collectives. If contraceptive research has only managed to deteriorate since the Dark Ages because it is economically profitable to drug companies and patriarchy to have it that way, and if contraceptive research is absolutely essential for women, then the power of drug companies and patriarchy has to be confronted.

People working in rape relief centres faced the same kind of problems. While the centres are essential to rape victims, if they’re primarily “reaction” centres, they’ve got an unending future as helpers of the State.

While many women have pushed for stricter enforcement of rape laws, radical feminists know that rape is not a crime against society as we know it, but rather the ultimate expression of our society’s belief in and acceptance of force as righteous. Aside from the fact that it’s almost always poor and minority race men who are actually convicted, it’s to the advantage of the patriarchal State to encourage its citizens to see rape as a perverted form of sexual pleasure because that helps to contaminate the whole concept of sexuality as nasty, thus reinforcing the idea of the body as something that has to be controlled and legislated against by that State. When the State calls rape a crime it distracts people from realizing that implicitly through advertising, frustration inducement, and the concept of the righteousness of power of the stronger over the weaker, this society in fact promotes rape.

The reality of the staggering number of rape victims who are battered wives and the State’s horror of upsetting the nuclear family has further forced feminists into directly confronting and educating society about rape rather than relying on legal channels. In transition houses battered wives help each other in rejecting the “security” of their violent relationships. Unlike traditional social workers, radical feminists aren’t interested in patching things up in the home or “getting even” through the courts. They’re interested in eliminating rape. By distributing literature, which tries to explain the role of society in rape, by printing descriptions of rapists so that the rapists lose their anonymous power, and by going with rape victims in groups to confront rapists in public, feminists work to expose rapists, expose society’s implicit approval of rape, and by clearly attacking the real problems of frustration, weakness, capital and power, develop the highest form of education. That is, an education that learns from what really is and then moves forward to change the reality.

The kind of shared, living, explorative education that has grown within the self-help clinics and rape relief centres is representative of education as practiced by most radical feminists. The sharing of knowledge and skills is something women have been doing in their homes for centuries but because these skills were centered around such things as cooking and child care, they’ve generally been denigrated as “women’s stuff.” Likewise, the openness of women in talking about their relationships has been swept aside as “gossip.” Now, in our printing, theatre, health—in all our groups—women have continued sharing our skills, knowledge and feelings.

As feminists rejected the lopsided histories of patriarchal society and demanded “herstory,” we set to liberating education as lived experience in place of taught submission...

Peggy Kornegger suggested that women were “in the unique position of being the bearers of a subsurface anarchist consciousness”... Elaine Leeder said, “It has been said that women often practice Anarchism and do not know it, while some men call themselves Anarchists and do not practice it.” While neither Kornegger nor Leeder are saying that females biologically make for better anarchists, a too facile acceptance of their statements has encouraged many to believe just that. But if anarchistic tendencies within the feminist movement are accepted as a natural by-product of being female, it puts unfair pressure on women to “live up to their natural anarchism” and it limits our potential for political development because it discourages us from examining why women behave more anarchistically than men. Many women’s groups do disintegrate, many women do exploit other women and men, and feminists haven’t been able to liberate humanity. These “shortcomings” don’t make women less female, they confirm woman’s humanness.

So why have feminist groups incorporated so many anarchistic principles in our work situations? Largely because as women we’ve been raised to be sensitive, nurturing, and to think of our activities as being carried out in small intimate circles. While in the past these traits have facilitated the brute force of male domination, keeping women ineffectual in “worldly issues,” now, with a conscious appreciation of the life nurturing power of our “female” qualities, we are in a position to expand their influence while retaining their strength.

Also, by realizing that it is our education that has brought us to this point, we can more consciously extend that kind of education to men, and in particular, to rearing our sons and reinforcing our daughters. We can also recognize the inherent limitations of that very education. Those hesitations include a tendency towards passivity and towards exploding inside our heads instead of fighting our oppressors. While we may excel at working in small groups we’ve traditionally been cautious of larger groups and need to guard against isolation...

[A]narchism isn’t what it was before the radical feminist experience. If anarchism is its history, it is also a continuously created explorative and active response to the immediate and to the future. In theory, anarchism always included feminism but it’s only in the last few years that we’ve really discovered what that means and therefore been able to learn about that part of ourselves.

Theoretically anarchists shouldn’t have had to learn to be feminists, but they did have to learn and the lessons have been invaluable. These lessons have taught us what it really means to live our politics and they’ve given concrete, contemporary examples of direct, local, collective action.

It’s easy so see how anarchism has benefited from feminism and there are many who argue in favour of a feminist rather than an anarchist movement. But while I think it is premature to drop the hyphen in anarcha-feminism, I do see the eventual return to—or rather arrival at—anarchism as a liberating prospect.

Putting the anarcha into feminism has helped to place the immediate concrete work done into a historical perspective. That’s important so that successful, collective human ways of dealing with our struggles aren’t seen as isolated flukey episodes but rather as part of a total life approach and vision to ALL our living.

While we can only move forward if we first perceive the present real problems (and these have become clearer through the work of feminists), we need a vision if we are to move freely forward. A vision can only be the expression of our past, present and future. Part of that vision includes our anarchist history and part of that history includes the sharing of skills traditionally considered male. If our positive “female” skills are products of our education, so are our “female” deficiencies. Our male comrades can help us liberate “male” skills from our denied pasts and from the destructive uses they generally suffer in capitalist society.

Although the feminist experience has advanced the practice, we will find attempts at living non-authoritarian collective lives in our anarchist history—and present.

Anarcha-feminism isn’t the only compound in the movement. The other two one hears of most frequently are anarcho-syndicalism and anarcho-communism. In all cases the addition to the anarchism is the element of anarchism that seems to need the most emphasis. Anarcho-syndicalists recognize that most people’s lives center around work and they believe that that is where the major organizing must be done. Anarcho-communists stress the importance of the communes and the community. Because anarcho-communism is concerned with life in all its personal interactions I would suggest that the word anarchism includes the communism.

Anarcha-feminism exhibits aspects of both anarcho-syndicalism and anarcho-communism. To the extent that women are being exploited and degraded more than men, anarcha-feminism is like anarcho-syndicalism. The emphasis has to be on that part of anarchism that deals with personal and sexual exploitation. To the degree that feminism moves beyond “reaction” to exploitation and poses a total life approach, it is like anarcho-communism in that it becomes synonymous with anarchism.

Having said that it’s premature to drop the feminist stress in anarchism, why have I done it? Mainly because I do see anarchism—an anarchism broadened by the feminist experience—as the most viable revolutionary direction for the 80s. Those of us who choose at times to work in mixed groups will probably still have to direct a lot of our energy to emphasizing the feminism in anarchism and of course, many of us will continue to call ourselves anarcha-feminists. For myself, I drop the feminism in the label, but not in the struggle.

Work that I hope will be inspired by the feminist experience includes uncovering our own anarchist roots and experiences, and recognizing the political as an everyday issue.

Anarchist roots doesn’t just mean specifically anarchist inspired actions or theories. It means paying attention to all expressions of revolt and anti-authoritarianism. From such diverse revolts as the Diggers in England in the 1600s, to the Spanish collectives of the 1930s, to May 1968 in France, to squatters in present day Amsterdam, we are reminded that anarchist theory has grown from a human revolt against oppression and a responsibility to life that has preceded any theory. The experience of radical feminism is the most obviously recent example of this truth.

More attention to this heritage should encourage us to examine our immediate living situations more closely and to recognize in them the frequent indications of, and overwhelming potential for, radical rejection of authoritarian society. This is crucial if we are to be more than a discontented few and if we genuinely believe in the possibility of human liberation.

Particularly through “outreach” work such as the health collectives, street theatre, and rape relief, feminists have been most successful in combining a conscious political perspective with the unarticulated need of those whose lives are the expression of the need and potential for liberation.

The relation between a sense of immediacy and the effectiveness of the work being done has become clearer through feminist struggles and I expect that most radical feminists will continue doing the kind of work we’ve been doing for the last decade—fighting sexism wherever we encounter it. Women definitely are still more oppressed than men, the State is trying to crack down on abortions now that it sees the serious consequences of “granting” a woman some say in her own body, and for the most part, political groups are still sexist...

If we really do intend to live our politics more immediately, we’re going to have to work more on liberating our workplaces. Feminists have become progressively more involved in workplace organizing because the number of working women has risen so dramatically in the last two decades. As with our other political work we’ve had to fight the hierarchies of male dominated unions. Where unions already existed, women have fought to introduce even a slight degree of feminism, but for the most part, unions hadn’t previously been interested in organizing women so that now to a large extent we’re doing our own distinctly feminist organizing. It’s important that our organizing be as creative and liberating as our lives should be...

Just as feminists have fought to clarify the personal of politics, now feminists and anarchists have to insist on our humanness at our workplaces and reject our objectification as workers. It is as harmful to organize workers on authoritarian lines as to simply wish that people weren’t primarily workers. Because the workplace is generally so alienating and boring it seems difficult to liberate human energy. But, because the workplace is where most of us are, once we liberate the human being from the worker, the power of anarchy will be unlimited. Just as feminism has broadened the reality of anarchism, so will the unleashed energy of working people astound us with our own potential. If we are successful in claiming work as something we do for ourselves rather than something we are for others, our imaginative creative future will know no bounds. If we fail, we know our future only too well...

Obviously we can’t all be actively involved in fighting all the oppression weighing down on us but unless we see our struggles in their global context, we’re doomed to the repetition of individual or small collective struggles and finally, to no struggle at all because at some point we will be destroyed by nuclear insanity. That’s where the importance of an anarchist vision, history, and network come in.

It’s important to see our constructive local struggles in their global context so that we don’t get assimilated into the system, so that we can learn from others who are struggling in their own areas, so that we never forget that we’re involved in a world revolution and so that when we do join in large demonstrations such as a militarist and anti-nuke, we do so from an informed position and are able to participate constructively... we’re going to need all the spirit, imagination, and endurance we can get. The big powers are gearing up for war and playing with nuclear power. We’d be foolish to be optimistic about our future.

But with the vision of anarchism, and the example of feminism’s durability, we’ll put up one hell of a fight to be human.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

