      References
According to the late Wobbly organizer and Earth Firster, Judi Bari, a truly biocentric perspective must really challenge the system of industrial capitalism which is founded upon the ‘ownership’ of the earth. Industrial capitalism cannot be reformed since it is founded upon the destruction of nature. The profit drive of capitalism insists that more be taken out than is put back (be it labour or land). Bari extended the Marxist discussion of surplus value to include the elements of nature. She argued that a portion of the profit derived from any capitalist product results from the unilateral (under)valuing, by capital, of resources extracted from nature.

Because of her analysis of the rootedness of ecological destruction in capitalist relations Bari turned her attentions to the everyday activities of working people. Workers would be a potentially crucial ally of environmentalists, she realized, but such an alliance could only come about if environmentalists were willing to educate themselves about workplace concerns. Bari held no naïve notions of workers as privileged historical agents. She simply stressed her belief that for ecology to confront capitalist relations effectively and in a non-authoritarian manner requires the active participation of workers. Likewise, if workers were to assist environmentalists it was reasonable to accept some mutual aid in return from ecology activists.

In her view the power which manifests itself as resource extraction in the countryside manifests itself as racism and exploitation in the city. An effective radical ecology movement (one which could begin to be considered revolutionary) must organize among poor and working people. Only through workers’ control of production and distribution can the machinery of ecological destruction be shut down.

Ecological crises become possible only within the context of social relations which engender a weakening of people’s capacities to fight an organized defence of the planet’s ecological communities. Bari understood that the restriction of participation in decision-making processes within ordered hierarchies, prerequisite to accumulation, has been a crucial impediment to ecological organizing. [1] This convinced her that radical ecology must now include demands for workers’ control and a decentralization of industries in ways which are harmonious with nature. It also meant rejecting ecological moralizing and developing some sensitivity to workers’ anxieties and concerns.

To critics this emphasis on the concerns of workers and the need to overcome capitalist social relations signified a turn towards workerist analysis which, in their view, undermined her ecology. Criticisms of workers and ‘leftist ecology’ have come not only from deep ecologists, as discussed above, but from social ecologists, such as Murray Bookchin and Janet Biehl, who otherwise oppose deep ecology. Social ecology guru Bookchin has been especially hostile to any idea of the workplace as an important site of social and political activity or of workers as significant radical actors. Bookchin repeats recent talk about the disappearance of the working class [2], although he is confused about whether the working class is ‘numerically diminishing’ or just ‘being integrated’. Bookchin sees the ‘counterculture’ (roughly the new social movements like ecology) as a new privileged social actor, and in place of workers turns to a populist ‘the people’ and the ascendancy of community. Underlying Bookchin’s critique of labour organizing, however, is a low opinion of workers which he views contemptuously as ‘mere objects’ without any active presence within communities. [3]

Lack of class analysis likewise leads Janet Biehl to turn to a vague ‘community life’ when seeking the way out of ecological destruction. [4] Unfortunately communities are themselves intersected with myriad cross-cutting and conflicting class interests which, as Bari showed, cannot be dismissed or wished away. Notions of community are often the very weapon wielded by timber companies against environmentalist ‘outsiders.’

Biehl recognizes the ecological necessity of eliminating capitalism but her work writes workers out of this process. This is directly expressed in her strategy for confronting capital: ‘Fighting large economic entities that operate even on the international level requires large numbers of municipalities to work together’. [5] Not specific social actors — workers — with specific contributions to make, but statist political apparatuses — municipalities. To confront ‘macrosocial forces like capitalism ... [Biehl proposes] ... political communities’. [6] All of this is rather strange coming from someone who professes to be an anarchist.

Biehl even states that the ‘one arena that can seriously challenge’ current hierarchies is ‘participatory democratic politics’ but makes no reference to the specificity of the workplace in this regard. [7] Yet, within capitalist relations, the workplace is one of the crucial realms requiring the extension of just such a politics. And that extension is not likely to occur without the active participation of people in their specific roles as workers. Bari, concerned with encouraging this participation, did not have the luxury of overlooking the everyday concerns of workers.

As a longtime feminist and unionist Judi Bari was well aware of tendencies within the labour movement, and the left generally, to treat concerns of gender or environment as subordinate to the larger movement or worse as distractions. Bari was no vulgar materialist given to economistic analyses, however, and she rejected Dave Foreman’s characterization of Local 1 as simply ‘leftists’ or a ‘class struggle group’. She too remained sharply critical of Marxist socialism and what she saw as its acceptance of the domination of nature.

We are not trying to overthrow capitalism for the benefit of the proletariat. In fact, the society we envision is not spoken to in any leftist theory that I’ve ever heard of. Those theories deal only with how to redistribute the spoils of exploiting the Earth to benefit a different class of humans. We need to build a society that is not based on the exploitation of Earth at all — a society whose goal is to achieve a stable state with nature for the benefit of all species. [8]

For inspiration Bari turned to non-authoritarian traditions of socialism. Specifically, her materialism took the form of syndicalism — revolutionary libertarian unionism. [9] Bari developed her green syndicalist approach as an attempt to think through the forms of organization by which workers could address ecological concerns in practice and in ways which broke down the multiple hierarchies of mainstream trade unionism. She recognized in syndicalist structures and practices certain instructive similarities with the contemporary movements for ecology and radical feminism.

Historically anarcho-syndicalists and revolutionary unionists fought for the abolition of divisions between workers based upon, for example, gender, race, nationality, skill, employment status and workplace. Revolutionary unions, such as the IWW, in fighting for ‘One Big Union’ of all working people (whether or not they were actually working) argued for the equality of workers and the recognition of their unity as workers while realizing that workers’ different experiences of exploitation made such organization difficult.

Like radical feminists, anarcho-syndicalists have argued for the consistency of means and ends. Thus syndicalists organize in non-hierarchical, decentralized and federated structures which are vastly different from the bureaucratic structures of mainstream trades unions which have been largely resistant to participation by women. The alternative organizations of anarcho-syndicalism are built upon participation, mutual aid and cooperation. Anarcho-syndicalism combines the syndicalist fight against capitalist structures and practices of exploitation with the anarchist attack on power and awareness that all forms of oppression must be overcome in any struggle for liberty. The IWW has long fought for the recognition of women as ‘fellow workers’ deserving economic and physical independence (i.e. self-determination) and access to social roles based upon interests and preferences. [10]

Regarding the affinity between anarcho-syndicalist organization and ‘second wave’ feminist practice Peggy Kornegger [11] has commented: ‘The structure of women’s groups bore a striking resemblance to that of anarchist affinity groups within anarchosyndicalist unions in Spain, France, and many other countries.’ Kornegger laments that feminists did not more fully explore the syndicalist tradtions for activist insights.

Besides, as Purchase argues, industrial unions ‘are composed of people — feminists, peace activists and ecologists included — and are simply a means by which people can come to organise their trade or industry in a spirit of equality, peace and co-operation.’ [12] The exclusion of workers from new social movements discussions is both arbitrary and inaccurate.

Exactly what sense we are to make of such sweeping dismissals of centuries of sustained resistance to the encroachments of capital and state by ordinary working people is quite unclear. Besides, in the absence of state-supported industrial [or green] capitalism, trades unions and workers’ co-operatives — be they bakers, grocers, coach builders, postal workers or tram drivers — would seem to be a quite natural, indeed logical and rational way of enabling ordinary working people to co-ordinate the economic and industrial life of their city, for the benefit of themselves rather than for the state or a handful of capitalist barons, and it is simply dishonest of Bookchin to claim that anarchism has emphasised the historical destiny of the industrial proletariat at the expense of community and free city life. [13]

The concerns raised by Foreman, Bookchin and Biehl are well taken. Indeed, much Old Left thinking, of various stripes, did fail to appreciate the causes or consequences of ecological damage. However, as Graham Purchase has pointed out, the reasons for this are largely historically specific rather than inherent. [14] The ecological insights of social ecologists like Bookchin (e.g. ecological regionalism, and green technologies) are not incompatible with syndicalist concerns with organizing workers.

Bari asked how it could be that there were neighbourhood movements targeting the disposal of toxic wastes but no workers’ movement to stop the production of toxics. She argued that only when workers are in a position to refuse to engage in destructive practices or produce destructive goods could any realistic hope for lasting ecological change emerge. The only way to bring the system to a standstill is through mass-scale non-cooperation, what an earlier generation of syndicalists knew as the ‘General Strike.’ Bari’s vision for Earth First! combined a radicalization of the group’s initial ideas of biocentrism and an extension of the decentralized, non-hierarchical, federative organization, the nascent syndicalist structure of EF!, into communities and workplaces.

While agreeing with the old guard of Earth First! that efforts should be given to preserving or re-establishing wilderness areas, Bari saw that piecemeal set-asides were not sufficient. The only way to preserve wilderness was to transform social relations. This meant that Earth First! had to be transformed from a conservation movement to a social movement. Earth First! needed to encourage and support alternative lifestyles. To speak of wilderness decontextualized the destruction of nature.

Jeff Shantz is currently living in Toronto where he has been active for several years with the Ontario Coalition Against Poverty (OCAP). He is the host of the Anti-Poverty Report on community radio station CHRY in Toronto and is a co-founder of his union’s Anti-Poverty Working Group.

Bari, Judi, Timber Wars (Monroe: Common Courage Press, 1994)

Biehl, Janet, Finding Our Way: Rethinking Ecofeminist Politics (Montreal: Black Rose Books,1991)

Bookchin, Murray, Remaking Society (Boston: South End Press, 1990)

Bookchin, Murray, ‘Deep Ecology, Anarchosyndicalism and the Future of Anarchist Thought’ in Deep Ecology and Anarchism (London: Freedom Press, 1997)

Kornegger, Peggy, ‘Anarchism: The Feminist Connection’ in Reinventing Anarchy, Again, ed. by Howard J. Ehrlich (Edinburgh: AK Press, 1996)

Purchase, Graham, ‘Social Ecology, Anarchism and Trades Unionism’ in Deep Ecology and Anarchism (London: Freedom Press, 1997)

 
[1] Judi Bari, Timber Wars (Monroe: Common Courage Press,1994)
[2] Murray Bookchin, ‘Deep Ecology, Anarchosyndicalism and the Future of Anarchist Thought’ in Deep Ecology and Anarchism (London: Freedom Press,1997), p.57
[3] Bookchin goes so far as to claim that the ‘authentic locus’ of anarchism is ‘the municipality.’ This is a rather self-serving claim given that Bookchin has staked much of his reputation on building a ‘libertarian municipalist’ tendency within anarchism. It also runs counter to almost all of anarchist history. (Bookchin, 1997, p.51) (See Bookchin, 1990)
[4] Janet Biehl, Finding Our Way: Rethinking Ecofeminist Politics (Montreal: Black Rose Books, 1991), p.134
[5] Biehl, p.152
[6] Biehl, p.152
[7] Biehl, p.151
[8] Bari, 1994, p.57
[9] For a detailed discussion of green syndicalist theory see Shantz (1999).
[10] As Purchase (1997, p.32) awkwardly overstates: “Moreover the IWW ... was the first union to call for equal pay and conditions for women and actively sought to set up unions for prostitutes — and in doing so achieved far more for the feminist cause than any amount of theorising about the evolution of patriarchy could ever hope to have done.”
[11] Peggy Kornegger, ‘Anarchism: The Feminist Connection.’ in Reinventing Anarchy, Again, ed. by Howard J. Ehrlich (Edinburgh: AK Press, 1996), p.161
[12] Graham Purchase, ‘Social Ecology, Anarchism and Trades Unionism.’ In Deep Ecology and Anarchism (London: Freedom Press, 1997), p.28
[13] Purchase, p.28
[14] Purchase, p.25




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

