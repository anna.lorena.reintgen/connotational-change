
1

One of the most harmful prevailing prejudices of our times is the belief in Nature as a unified being separate from, and even opposed to Humanity (also perceived as a unified being). In the context of this doctrine, what is specifically Human – what is created by conscious human activity – is called Artificial as opposed to Natural.

2

The concept of Nature (that is the concept that all beings, things, relationships and activities not created by human beings constitute a unified whole that stands in contrast to all the things, beings, relationships and activities consciously created by human beings) is itself a product of conscious human activity and, thus, artificial.

3

Etymologically, “nature” simply refers to what is born into something, what is inherent to it; “artifice” refers to something that is made through consciously applied skill. Considered in this way, there is no necessary (“natural” if you will) opposition between “nature” and “artifice”, since what is consciously and skillfully created can only be made by natural beings (at least as of now) with an inborn capacity to learn to act consciously and with skill.

This does not mean that all or even most “artificial” creations are desirable. Just as there are certain “natural” realities that may cause us harm, so there are many “artificial” realities that are detrimental to us. Furthermore, while “natural” harms are usually temporary events that we can endure and get beyond, artificial creations that cause us harm are often meant to be permanent and even expansive. Thus, the only way to put an end to their harmfulness is to dismantle or destroy them. For example, institutions, large-scale structures and technological systems are all created through conscious human activity. They form a network that defines and limits the possibilities of our lives. They harm us socially and psychologically through these limitations that cripple imagination and creative capacity. They harm us physically by causing or enhancing disasters, illness, poverty, pollution, etc. Getting beyond them requires not endurance, but rather conscious human activity aimed at destruction…

In addition, there are aspects of the reality in which we live that are neither “natural” or “artificial”, neither inborn nor consciously created, I am speaking here of the vast array of historical, social and cultural contingencies that develop out of the continuous, fluid interweaving of human relations amongst themselves and with non-human beings and things. Though they develop from human activity, they are not conscious creations, but rather reflect the meeting of chance and necessity in living in the world. For this reason, they often reflect the absurdity of the attempt to institutionally rationalize the world. But they also often provide the opportunities for challenging this institutional rationalization. Thus, in order to attack the civilized ruling order, we need to see beyond the “natural”-“artificial” dichotomy and explore this realm of historical, social and cultural contingency in order to grasp what we can as weapons for our revolt.

4

The conception of Nature as a unified entity is the basis for two apparently contradictory, but in fact complementary, ideologies that serve the ruling order by enforcing control over our lives: the moral ideology that ascribes goodness to the Natural and evil to the Unnatural and the metaphysical ideology of inherent alienation that sees Nature as a force hostile to Humanity and its development, a force that must be conquered and brought under control.

The moral ideology is applied most widely to in the sexual realm, but has also been used against magical and alchemical experimentation as well as any activity that is looked upon as a challenge to god’s rule (hubris). In our times, it is used against a variety of sexual acts as well as against abortion. Sexual minorities interested in assimilating often try to prove the naturalness of their sexuality (for example, by claiming it is genetic) as opposed to the unnaturalness of certain other forms of sexuality (pedophilia, whose definition has been expanded in recently years to mean the sexual attraction of an adult for anyone under the legal age of consent[1], and to a lesser extent bestiality are the prime contemporary examples of “unnatural” desire). But whether used against the hubris of alleged sorcerers, alchemists or courageous infidels, or against specific sexual or reproductive acts, this moral Nature serves as a tool for keeping passion and desire in check and thus for keeping us under control.

The ideology that views Nature as a hostile force which Humanity must conquer in order to meet its needs occurs to some extent within all civilizations, but only seems to have become the dominant conception within western civilization in the past five or six hundred years. Its rise to dominance, in fact corresponds with the rise of capitalism and the beginnings of industrialism. It was necessary to begin to channel human creative endeavors into activity that would maximally exploit all potential economic resources – natural and human – and this ideology provided a justification for just such an exploitative development. It makes use of disease, storms, floods, droughts, earthquakes and other so-called natural difficulties and catastrophes to back up this perspective and justify the most intrusive and controlling technological interventions. More than the moral ideology, this perspective is the modern justification for domination and control.

5

Civilization is a network of institutions that materially and practically alienate us from our own lives and creativity and, at the same time, from the myriad of relationships with the infinite variety of beings and things that make up the world in which we live. This alienation is what transforms the variety of beings and things into the unity of Nature. This unity mirrors the imposed unity of civilization.

6

Overcoming alienation could thus be seen as a process of decivilizing. But what does this mean? It does not mean rewilding, going back to the primitive, going back to Nature. All these ideas imply a return to a way of being that is in reality a conceptual model (the Wild, the Primitive, the Natural) and thus a civilized ideal. Decivilizing is not a return to anything. The flow of relationships between ever-changing individuals that is existence outside of the Civilization-Nature dichotomy is never repeatable. So decivilizing has to be understood and explored without models, without any concept of a return.

7

A process of decivilizing would instead be a process of destruction and dismantling. Of material and social institutions and structures, of course. But also of the ideological structures, the false conceptual unities (Stirner’s “spooks”) which channel thinking to such an extent that most of us don’t even notice these chains on our thoughts. The oneness of Nature, the oneness of Life, the oneness of the Earth are all civilized ideological constructions that guarantee that we continue to view our relationship with the rest of the world through the lens of alienation.

8

In this light, the desire to attack and destroy the institutions, structures and people that enforce the rule of the civilized regime becomes meaningful only when we are experimenting with ways of grasping our lives as our own and encountering other beings as individuals striving to create their lives – i.e., when we are practically attacking the ideological structure that channel our thoughts and desires. This does not mean rejecting all categorization, but rather recognizing its limits as a specific tool. Categorization can, for example, help us to distinguish poisonous from edible plants. But it cannot tell us the reality or even the most significant aspects of another being: their desires, their aspirations, their dreams…

9

By recognizing and encountering the uniqueness of each being in each moment, we find the basis for determining how to carry out our desires, for recognizing where complicity and mutuality are appropriate, where conflict is inevitable or desirable, where passionate encounter might flare up and where indifference makes sense. Thus, we are able to focus on what we need to realize desire, what place other beings and things and the relationships we build with them have in this creative process.

10

In terms of attacking civilization, this means rejecting any monolithic conception of it, without losing sight of its nature as an intertwining network of interdependent institutions and structures. These institutions and fundamental structures can only exist through the alienation of individuals from their lives. That alienation is their basis. This is why we can never make these institutions and basic structures our own, and there is no use in trying to grasp them as such. Rather they need to be destroyed, removed from our path.

But the development of civilization has created a great many byproducts of all sorts: materials, tools, buildings, gathering spaces, ideas, skills, etc. If we view civilization simplistically, as a solid monolith, then we can only bemoan our need to continue to use some of these byproducts as we dream of a distant future when we will live in a paradise where every trace of this monolith is gone.

If, on the other hand, we can distinguish what is essential to civilization from its byproducts and encounter the latter immediately in terms of our needs and desires (i.e., in a decivilized manner), new possibilities open for exploring how to live on our own terms.

11

This is how outlaws, the so-called “dangerous classes”, tend to encounter the world. Everything that isn’t nailed down is there for the taking to create life with. As anarchists who recognize civilization as the institutionalization of relationships of domination and exploitation, we would also encounter these byproducts in terms of how they can be used to attack, destroy and dismantle civilization.

12

But how does the idea of relating to each individual being in its uniqueness affect the human need to consciously and skillfully create? If we conceive of the ever-changing myriads of relationships around us as a monolithic Nature that is basically hostile toward us, the techniques methods and structures we develop will aim to conquer, control and dominate this hostile force (perhaps even to destroy it). If, instead, we see ourselves and all the beings around us as unique individuals in an ever-changing interaction with each other, we would still use skill and artifice, but not to conquer a monolith. Instead, we would use them to weave our way through a wonderful dance of relationships – destroying the calcifying institutions that block this dance – in a way that brings the greatest enjoyment to our lives.

13

A practice of this sort requires a vital and active imagination and a resolute playfulness.

By imagination, I mean the capacity to “see beyond” what is, to see possibilities that challenge and attack the current reality rather than extending it. I am not talking here of an adherence to a single utopian vision – which would tend to create authoritarian monstrosities in search of adherents to devour – but of a capacity for ongoing utopian exploration without a destination, without a goal.

Perhaps this is what distinguishes anarchists from other outlaws. Imagination has moved their conception of the enjoyment of life beyond mere consumption to playful creation. Certainly, the ways in which outlaws have often historically consumed – the squandering of all they gained through their wits and daring in excesses of debauched feasting and immediate enjoyment of luxuries – runs counter to the capitalist value of accumulation, but it still equates wealth with things, reflecting the alienation of current relationships. Active, practical imagination can show us the real wealth that can spring from free relationships as creative activity.

By resolute playfulness, I mean the refusal to compromise oneself by taking on an identity that pins one down, the refusal to take seriously precisely those things to which this society gives importance, the insistence upon experimenting with one’s life in each moment without worrying about a future that does not exist. The world is full of toys, games and challenges that can heighten the intensity of living. They are often hidden, buried beneath the institutional seriousness or the necessities of survival imposed by the ruling order. The insurgent and outlaw grasping of life involves breaking through these barriers.

14

So, a process of decivilization, of freeing ourselves from the constraints and obligations imposed by the network of institutions that we call civilization, is not a return to anything. It does not center around learning certain skills and techniques or applying certain utilitarian measures. It is rather a matter of refusing the domination of the utilitarian, the domination of survival over life, of insisting upon going out into the world to play on our own terms, taking hold of what gives us pleasure, and destroying what stands in our way.
[1] It originally meant the sexual attraction of an adult for prepubescent children.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

