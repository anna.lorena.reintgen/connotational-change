
The first time you may have seen me was in the gallery of the New York Stock Exchange, hurling money on the brokers below. Of course, you didn’t actually see me because no photographs of the incident exist: newsmen are not allowed to enter the sacred temple of commerce.

It all began with a simple telephone call to the Stock Exchange. I arranged for a tour, giving one of my favorite pseudonyms, George Metesky, the notorious mad bomber of Manhattan. Then I scraped together three hundred dollars which I changed into crispy one-dollar bills, rounded up fifteen free spirits, which in those days just took a few phone calls, and off we went to Wall Street.

We didn’t call the press; at that time we really had no notion of anything called a media event. (And to make one very important point, I never performed for the media. I tried to reach people. It was not acting. It was not some media muppet show. That is a cynical interpretation of history.) We just took our places in line with the tourists, although our manner of dress did make us a little conspicuous. The line moved its way past glassed-in exhibits depicting the rise of the industrial revolution and the glorification of the world of commerce. Then the line turned the comer. Suddenly, we saw hordes of reporters and cameras. Somebody must have realized a story was in the making and rung up one of the wire services. In New York the press can mobilize in a matter of minutes. Faster than police, often.

We started clowning, kissing and hugging, and eating money. ext, some stock exchange bureaucrats appeared and we argued until they allowed us in the gallery, but the guards kept the press ut. I passed out money to freaks and tourists alike, then all at once we ran to the railing and began tossing out the bills. Pandemonium. The sacred electronic ticker tape, the heartbeat of the estern world, stopped cold. Stock brokers scrambled over the floor like worried mice, scurrying after the money. Greed had burst through the business-as-usual

It lasted five minutes at the most. The guards quickly ushered us out with a stem warning and the ticker tape started up

The reporters and cameramen were waiting for us outside:

“Who are you?”

“I’m Cardinal Spellman.”

“Where did you get the money?”

“What are you saying? You don’t ask Cardinal Spellman where he gets his money!”

“How much did you throw?”

“Thousands.”

“How many are you?”

“Hundreds—three—two—we don’t exist! We don’t even exist!” As the cameras whirred away we danced, burned greenbacks and declared the end of

Bystander: “This is a disgusting display.”

Me: “You’re right. These people are nothing but a bunch of filthy commies.”

The story was on the air waves that night and our message went around the world, but because the press didn’t actually witness the event they had to create their own fantasies of what had happened inside the money temple. One version was we threw Monopoly money, another network called it hundred-dollar bills, a third shredded money. A tourist from Missouri was interviewed who said he had joined in the money-throwing because he’d been throwing away his money all over New York for several days anyway and our way was quicker and more fun. From start to finish the event was a perfect myth. Even the newspeople had to elaborate on what had

A spark had been ignited. The system cracked a little. Not a drop of blood had been spilled, not a bone broken, but on that day, with that gesture, an image war had begun. In the minds of millions of teenagers the stock market had just

Guerrilla theater is probably the oldest form of political commentary. The ideas just keep getting recycled. Showering money on the Wall Street brokers was the TV -age version of driving the money changers from the temple. The symbols, the spirit, and the lesson were identical. Was it a real threat to the Empire? Two weeks after our band of mind-terrorists raided the stock exchange, twenty thousand dollars was spent to enclose the gallery with bullet-proof glass. Someone out there had read the ticker.

In The Theatre and Its Double, Antonin Artaud called for a new “poetry of festivals and crowds, with people pouring into the streets.” No need to build a stage, it was all around us. Props would be simple and obvious. We would hurl ourselves across the canvas of society like streaks of splattered paint. Highly visual images would become news, and rumor-mongers would rush to spread the excited word. Newscasters unconsciously began all reports of our actions with the compelling phrase “Did ya hear about—.”

For us, protest as theater came natural. We were already in costume. If we went above Fourteenth Street we were suddenly semi-Indians in a semi-alien culture. Our whole experience was theater-playing the flute on the street comer, panhandling, walking, living protest signs. Our theatricality was not adopted from the outside world. We didn’t buy or read about it. It was not a style  like disco dressing that you could see in ads and imitate. Once we acknowledged the universe as theater and accepted the war of symbols, the rest was easy. All it took was a little elbow grease, a  little hustle.

At meetings people would divide up in groups to work on one  theatrical action or another. Some took only a few participants and  others were more elaborate. Some had to be planned like bank robberies and others like free-for-all be-ins.

One night we decided to do something that would express the  neighborhood’s dismay over increased traffic and thought for the first time about using mobile tactics-people running around and creating a little chaos rather than just standing still. To get  everyone assembled and disbursed we put out an anonymous leaflet telling people to gather at St. Mark’s Place at 9p.m., wait for  a signal from God, then scatter through the streets. Two thousand  people responded.

One of us (guess which one) had gone to a chemist’s shop and  bought two pounds of magnesium which we packed in coffee tins  and put on the roofs around St. Mark’s Place. Then we rigged the  cans with delay fuses by shoving lighted cigarettes in match packs.  Once done, we raced down to the streets where people were  milling around, waiting for God. All of a sudden the whole sky lit up with a huge blast of exploding magnesium. People started  running all over. Fire trucks poured into the area. Sometimes  chaos makes a good point.

In incense-filled rooms we gathered cross-legged on the rugs,  conspiring dastardly deeds. The Jokers would show Gotham City  no mercy:

“We’ve just got to end this tourist gawking,” complained provo agitator Dana Beal.

“Hey, how’s this for the tourist problem?” said Radio Bob Fass. “Wavy Gravy gets dressed up real straight and buys a ticket to go on one of the tours. We all get dressed up as cowboys and  hold up the bus when it turns the corner into Second Avenue. We  board it, pull Wavy off and hang him from the lamp post.”

“Hang him?”

“Well, not really. We rig up one of those harnesses under his jacket just like they do in the movies.”

The major event that spring was the be-in in Central Park. That’s when I really got hooked in to the whole idea. I was at Liberty House when Lynn House and Jim Fouratt came by and said, “We’re going to put on this be-in.”

I went on the air to promote the event and Bob Fass at radio station WBAI interviewed me. I started to fantasize about what the be-in was going to be about — no speakers, platform, leaders, no clearly defined format so people could define it for themselves. folks would just come to the park on Easter Sunday dressed for the occasion and exchange things, balloons, acid, jelly beans, Easter eggs; do Druid dances, or whatever their hearts desired.

Thirty-five thousand people showed up. The traditional Fifth Avenue Easter Parade, our competition, drew less than half that. After the be-in, Anita and I walked out of the park and joined the Fifth Avenue Parade, singing “In Your Easter Bonnet” Our faces were painted silver and I was carrying a huge Easter bunny. In front of St. Patrick’s Cathedral the loudspeakers blared, “Come in, come in and worship.” Why not? But as soon as we mounted the steps we were stopped by a line of cops.

“You can’t go in there looking like that.”

“What do you mean, we can’t come in? Don’t you see who we’re with? We’re with the Easter bunny.”

“The Cardinal says no hippies on Easter Sunday.”

A crowd began to gather. We continued to “play the dozens” with the cops. The confrontation heated up so we staged a strategical withdrawal, already plotting a sequel: “We’ll come back next Christmas. We’ll rent a mule and get some dude with long hair, dress him up in a white robe and sandals, and have him ride right up to the door of St. Patrick’s with people waving palm branches, and Cardinal Spellman will come out and say, ‘You can’t come in here ...”

It’s so easy. All you need is a little nerve and a willingness to be considered an embarrassment. Then you just keep pushing it, repeating what they say: “You mean the Cardinal says ...”

If observers of the drama are allowed to interpret the act, they will become participants themselves. Too much analysis kills direct theatrical experience. The put-on allows you to circumvent the trap. Smashing conventional mores becomes essential. The concept of mass spectacle, every-day language, and easily recognized symbols was important to get public

Artists, the vanguard of communication, had grown weary of decades of abstract shapes. Modern art was already institutionalized; ersatz Kandinskys hung in dentists’ offices. Andy Warhol broke through the abstraction and let us see the raw stuff of art in supermarkets, on TV, in magazines, and at the garbage dump. Allan Kaprow and other artists were experimenting with a new form called “happenings”—half-scripted, half-chance public exhibitions—3-D art, with people as paint.

“Happenings” were an extension of abstract art and as such were designed for the ruling class. I thought we could improve on that. Perhaps the audience that appreciated All in the Family did not approve of our “message” but they did understand it. It was public and popular. If we were not accepted by the Archie Bunkers of America, then perhaps by the children of Archie themselves. That the Museum of Modern (sic) Art honored “happenings” and “pop art” while ignoring our brand of political theater just proves the connection between successful artists and the rich.

Lenin once wrote that art was counter-revolutionary because it showed beauty in the present, while revolution promised beauty in the future. It’s true that art-for-art’s sake leads to performing modern dance for Shahs and Sheiks or discussing sculpture at afternoon tea with the Rockefellers. Yet creativity is needed to reach people snowed under by ruling-class images, and only artists can manage the breakthrough. Artists are the collective eyes of the future. One of the worst mistakes any revolution can make is to become boring. It leads to rituals as opposed to games, cults as opposed to community and denial of human rights as opposed to

In organizing a movement around art we not only allowed people to participate without a sense of guilt but also with a sense of enjoyment. The use offun in struggle was a new notion. Even in Mississippi where we were truly frightened most of the time with people shooting at us, living with the constant thought that we might lose our lives, it seemed like people enjoyed their “work.” All I did was admit it felt good. There’s no incongruity in conducting serious business and having fun. This pissed offthe straight left no

One of the principles of good theater is not to overburden the audience with footnoted explanations of what they are seeing. In 1967 a picket sign saying end the — was far more involving than one that said END THE WAR. People love filing in the blanks and you could always count on straight people to stick to the core message. A populist movement must allow people to define their own space, their own motives, to be their own critics. A good explanation is no explanation, keeping your mouth shut a correct response. There was, however, an even higher form of communication, since “no response” sounds the same as the bureaucracy’s “no comment.” Street players have nothing to hide. The solution lies in the zen axiom: say everything by saying nothing, remain silent by telling all. Any good Jewish comedian from Hillel to Don Rickles knows what I’m talking about. Partly truth, partly fiction, the “put-on” gets the job

Guard: Sorry, hippies are not allowed in the Stock Exchange.

Actor: But we’re not hippies, we’re Jewish. Should we tell the press you kept Jews out of Wall Street?

Theater of protest, for me, was a marriage of circumstances and personality. After a while I couldn’t keep it a secret who I was. At first, my identity was a bit of a mystery. I often wrote under weird aliases: George Metesky the bomber, Jim Metesky, which was a cross between him and Jim Piersall, a Red Sox ball player I liked, Frankie Abbott, a figure in the Amboy Dukes (dutifully reported in the New York Times as Mr. Frank Abbott), Free, The Digger, or just A. Hippie. (The period after A made it me.) After I became well known I couldn’t continue the pretense, even if the attitude was right. It was all part of a reluctance (maybe an inability) to define. Definition always seemed to contain an element of

On April 15, 1967, the largest demonstration in the country’s history, 700,000 people, marched to the United Nations to prot.est the escalation of the war in Vietnam. Our Lower East Side contingent assembled at Tompkins Square Park and marched north, gathering more people along the way. The artists all turned out, so naturally our form of presentation was pretty colorful: Ginsberg’s bells and chants, The Bread and Puppet Theater group, gaily dressed and stoned, a Yellow Submarine, and a lot of people who looked like they had posed for the Sergeant Pepper album  cover. (One of the first examples of a masterpiece entering the Supermarket.)

A month later the right-wingers responded with a Support Our  Boys rally, and we organized a “flower brigade” to march in their  parade. There were about twenty of us with flowers and banners that read “Support Our Boys—Bring Them Home,” and we all carried little American flags. I wore a multi-colored cape with the  word “Freedom” on it. Anita was all decked out in red, white, and  blue. Joe Flaherty of the Village Voice came by and told us we  were asking for trouble. Even the cops tried to talk us out of marching and wouldn’t give us an escort for protection. But we  saw the “Support Our Boys” stickers on their windshields and we  knew we were better off without them.

For a while everything went fine. We marched behind some Boy  Scouts from Queens (“Oh, look, they’re kissing!” they’d squeal  and break formation) and then walked straight into trouble. They  came at us with fists, feet, beer, spit, red paint. They even ripped  up our American flags. Then a flying wedge of cops appeared out of nowhere and escorted us, bleeding and limping, all the way  back to St. Mark’s Place.

Undaunted, we marched again, this time to Lincoln Center for a cultural exchange program. “March to Lincoln Center. Bring Your Own Garbage. Let’s Trade It for Their Garbage—Even Steven,” read the leaflets. About thirty of us walked from our neighborhood through the streets of Manhattan to newly-opened Lincoln Center with our bags of garbage and dumped them in the courtyard fountain, scattering in every direction when the cops chased us. The media got hold of it, turning the event into the potent image we had intended. “Oh, those hippies-they went up and threw garbage at Lincoln Center.” That’s enough, that was the message. The press didn’t yet realize that these images were disruptive to society and they were quickly caught up in the excitement and fashion. Later, editors became  sophisticated.

Once you get the right image the details aren’t that important. Over-analyzing reduced the myth. A big insight we learned during this period was that you didn’t have to explain why. That’s what advertising was all about. “Why” was for the critics.

Radical theater burst onto the streets with a passion. Our guerrilla band attacked Con Edison, New York’s utility company. On cue, soot bombs exploded in offices, smudge pots billowed thick smoke into lobbies, black crepe paper encircled the building, and a huge banner hung across the front door: BREATHING IS BAD FOR YOUR HEALTH. Cops and firemen appeared on the scene. We ran in all directions, losing ourselves in the crowds. The six o’clock news opened with clouds of smoke, a pan shot of the banner, and strange-looking guttersnipes running amok. An official from the power company wearing a suit and tie explained Con Ed’s position. As he spoke he nervously touched his face. Self-inflicted black spot marks appeared on his cheeks: the vaudeville show was completed by unwitting self-ridicule. The fatter they are, the harder they fall.

The Army recruiting center in Times Square was plastered with stickers: SEE CANADA NOW. Stop signs on street corners now read STOP WAR. Witches in black robes, bearing roses, exorcised the FBI building of its evil spirits. Hundreds crowded the lobby of the Daily News smoking grass and passing out leaflets to employees that began, “Dear Fellow Members of the Communist Conspiracy.” A tree was planted in the center of St. Marx Place (we took the liberty of changing the spelling) while 5,000 celebrators danced to rock music. Midnight artists snuck into subway stations and painted huge murals on the walls. Naked people ran through churches. Panhandlers worked the streets for hours, took the change they collected to the nearest bank and scattered it on the floor. A giant Yellow Submarine mysteriously kept appearing in tow-away zones. Tourist buses, now detouring to watch the hippies cavort, were greeted by freaks holding up huge mirrors, screaming, “Dig thyself!” All this and more Anita and I got high doing.

Some events grew out of unexpected donations. A person called up, “I’ve got 10,000 flowers you can have.” I had an idea: wouldn’t it be great to have these flowers come showering down over the be-in in Central Park? We had to get hold of a head who knew how to fly a plane and was ready to risk arrest. I found one in New Jersey and told him to act fast. He raced to the airport on his motorcycle, smashed-up, left his bike on the street, called a cab and arrived just in time. All the connections were made perfectly except the last one — he dropped all 10,000 flowers blocks away on an empty side street.

If street theater is to avoid growing tedious, it benefits from an edge of menace — a touch of potential violence. When Secretary of State Dean Rusk came to town to speak to some war hawk assemblage at the Waldorf-Astoria, we rallied at 57th Street and Seventh Avenue, ready to “bring the war home.” Plastic bags filled with cow’s blood flew through the night air. Tape recordings of battle sounds screamed above the crowds. Urban monkeys (not yet guerrillas) with painted faces and water pistols attacked tuxedoed enemy collaborators. Fire alarms were pulled and swarms of angry demonstrators shouting, “Hey, hey, LBJ, how many kids did you kill today?” surged through midtown.

A couple observing the melee said, “What’s going on?” “There’s a war on, can’t you see?” I answered as the police on horseback began to attack. We scattered the sidewalk with marbles and the horses slipped and stumbled. Innocent bystanders (no bystander is innocent) were caught between the clashing armies. The cops waded right into the crowds of people, clubbing away. Crunch! I got carted off to jail.

The head of a pig was delivered to Hubert Humphrey on a silver platter before a shocked throng of liberals. Shelley Winters, that pompous phony, denounced us. Mice were released at a Dow Chemical stockholders’ meeting. Cardinal (pass-the-Lord-and-Praise-the-Ammunition) Spellman, who went to Vietnam and posed behind a machine gun, was confronted by angry Catholics during a church service.

When all else failed, we simply declared the war over. Five thousand of us romped through the streets, hugging people in stores and buses. “The war is over! Hip-hip-hurray!! It’s over!!” Balloons, confetti, singing, dancing. If you don’t like the news, we reasoned, make up your own.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

