
October 17, 1848

Citizens,

When our friends of the democratic republic, apprehensive of our ideas and our inclinations, cry out against the qualification of socialist which we add to that of democrat, of what do they reproach us? — They reproach us for not being revolutionaries.

Let us see then if they or we are in the tradition; whether they or we have the true revolutionary practice.

And when our adversaries of the middle class, concerned for their privileges, pour upon us calumny and insult, what is the pretext of their charges? It is that we want to totally destroy property, the family, and civilization.

Let us see then again whether we or our adversaries better deserve the title of conservatives.

Revolutions are the successive manifestation of justice in human history. — It is for this reason that all revolutions have their origins in a previous revolution.

Whoever talks about revolution necessarily talks about progress, but just as necessarily about conservation. From this it follows that revolution is always in history and that, strictly speaking, there are not several revolutions, but only one permanent revolution.

The revolution, eighteen centuries ago, called itself the gospel, the Good News. Its fundamental dogma was the Unity of God; its motto, the equality of all men before God. Ancient slavery rested on the antagonism and inequality of gods, which represented the relative inferiority of races, in the state of war. Christianity created the rights of peoples, the brotherhood of nations; it abolished simultaneously idolatry and slavery.

Certainly no one denies today that the Christians, revolutionaries who fought by testimony and by martyrdom, were men of progress. They were also conservatives.

The polytheist initiation, after civilizing the first humans, after converting these men of the woods, sylvestres homine, as the poet says, into men of the towns, became itself, through sensualism and privilege, a principle of corruption and enslavement. Humanity was lost, when it was saved by the Christ, who received for that glorious mission the double title of Savior and Redeemer, or as we put it in our political language, conservative and revolutionary.

That was the character of the first and greatest of revolutions. It renewed the world, and in renewing it conserved it.

But, supernatural and spiritual as it was, that revolution nevertheless only expressed the more material side of justice, the enfranchisement of bodies and the abolition of slavery. Established on faith, it left thought enslaved; it was not sufficient for the emancipation of man, who is body and spirit, matter and intelligence. It called for another revolution. A thousand years after the coming of Christ, a new upheaval began, within the religion the first revolution founded, a prelude to new progress. Scholasticism carried within it, along with the authority of the Church and the scripture, the authority of reason! In about the 16th century, the revolution burst out.

The revolution, in that epoch, without abandoning its first given, took another name, which was already celebrated. It called itself philosophy. Its dogma was the liberty of reason, and its motto, which follows from that, was the equality of all before reason.

Here then is man declared inviolable and free in his double essence, as soul and as body. Was this progress? Who but a tyrant could deny it? Was it an act of conservation? The question does not even merit a response.

The destiny of man, a wise man once said, is to contemplate the works of God. Having known God in his heart, by faith, the time had come for man to know him with his reason. The Gospel had been for man like a primary education; now grown to adulthood, he needed a higher teaching, lest he stagnate in idiocy and the servitude that follows it.

In this way, the likes of Galileo, Arnaud de Bresce, Giordano Bruno, Descartes, Luther — all that elite of thinkers, wise men and artists, who shone in the 15th, 16th and 17th centuries as great revolutionaries — were at the same time the conservatives of society, the heralds of civilization. They continued, in opposition to the representatives of Christ, the movement started by Christ, and for it suffered no lack of persecution and martyrdom!

Here was the second great revolution, the second great manifestation of justice. It too renewed the world — and saved it.

But philosophy, adding its conquests to those of the Gospel, did not fulfill the program of that eternal justice. Liberty, called forth from the heart of God by Christ, was still only individual: it had to be established in the tribunal. Conscience was needed to make it pass into law.

About the middle of the last century then a new development commenced and, as the first revolution had been religious and the second philosophical, the third revolution was political. It called itself the social contract.

It took for its dogma the sovereignty of the people: it was the counterpart of the Christian dogma of the unity of god.

Its motto was equality before the law, the corollary of those which it had previously inscribed on its flag: equality before God and equality before reason.

Thus, with each revolution, liberty appeared to us always as the instrument of justice, with equality as its criterion. The third term — the aim of justice, the goal it always pursues, the end it approaches — is brotherhood.

Never let us lose sight of this order of revolutionary development. History testifies that brotherhood, supreme end of revolutions, does not impose itself. It has as conditions first liberty, then equality. It is as if just said to us all: Men, be free; citizens, become equal; brothers, embrace one another.

Who dares deny that the revolution undertaken sixty years ago by our fathers, and which the heroic memory makes our hearts beat with such force that we almost forget our own sense of duty — who denies, I ask, that that revolution was a progress? Nobody. Very well, then. But was it not both progressive and conservative? Could society have survived with its time-worn despotism, its degraded nobility, its corrupt clergy, with its egotistical and undisciplined parliament, so given to intrigue, with a people in rags, a race which can be exploited at will?

Is it necessary to blot out the sun, in order to make the case? The revolution of ’89 was the salvation of humanity; it is for that reason that it deserves the title of revolution.

But, citizens, if our fathers have done much for liberty and fraternity, and have even more profoundly opened up the road of brotherhood, they have left it to us to do even more.

Justice did not speak its last word in ’89, and who knows when it will speak it?

Are we not witnesses, our generation of 1848, to a corruption worse than that of the worst days of history, to a misery comparable to that of feudal times, an oppression of spirit and of conscience, and a degradation of all human faculties, which exceeds all that was seen in the epochs of most dreadful cruelty? Of what use are the conquests of the past, of religion and philosophy, and the constitutions and codes, when in virtue of the same rights that are guaranteed to us by those constitutions and codes, we find ourselves dispossessed of nature, excommunicated from the human species? What is politics, when we lack bread, when even the work which might give bread is taken from us? What to us is the freedom to go or to become, the liberty to think or not to think, the guarantees of the law, and the spectacles of the marvels of civilization? What is the meager education which is give to us, when by the withdrawal of all those objects on which we might practice human activity, we are ourselves plunged into an absolute void; when to the appeal of our senses, our hearts, and our reason, the universe and civilization reply: Néant! Nothing!

Citizens, I swear it by Christ and by our fathers! Justice has sounded its fourth hour, and misfortune to those who have not heard the call!

— Revolution of 1848, what do you call yourself?

— I am the right to work!

— What is your flag?

— Association!

— And your motto?

— Equality before fortune!

— Where are you taking us?

— To Brotherhood!

— Salut to you, Revolution! I will serve you as I have served God, as I have served Philosophy and Liberty, with all my heart, with all my soul, with all my intelligence and my courage, and will have no other sovereign and ruler than you!

Thus the revolution, having been by turns religious, philosophical and political, has become economic. And like all its predecessors it brings us nothing less than a contradiction of the past, a sort of reversal of the established order! Without this complete reversal of principles and beliefs, there is no revolution; there is only mystification. Let us continue to interrogate history, citizens.

Within the empire of polytheism, slavery had established and perpetuated itself in the name of what principle? In the name of religion. — Christ appeared, and slavery was abolished, precisely in the name of religion.

Christianity, in its turn, made reason subject to faith; philosophy reversed that order, and subordinated faith to reason.

Feudalism, in the name of politics, controlled everything, subjecting the laborer to the bourgeois, the bourgeois to the noble, the noble to the king, the king to the priest, and the priest to a dead letter. — In the name of politics again, ’89 subjected everyone to the law, and recognized among men only citizens.

Today labor is at the discretion of capital. Well, then! The revolution tells you to change that order. It is time for capital to recognize the predominance of labor, for the tool to put itself at the disposition of the worker.

Such is this revolution, which has suffered sarcasm, calumny and persecution, just like any other. But, like the others, the Revolution of 1848 becomes more fertile by the blood of its martyrs. Sanguis martyrun, semen christianorum! exclaimed one of the greatest revolutionaries of times past, the indomitable Tertullien. Blood of republicans, seed of republicans.

Who does not dare to acknowledge this faith, sealed with the blood of our brothers, is not a revolutionary. The failure is an infidelity. He who dissembles regarding it is a renegade. To separate the Republic from socialism is to willfully confuse the freedom of mind and spirit with the slavery of the senses, the exercise of political rights with the deprivation of civil rights. It is contradictory, absurd.

Here, citizens, is the genealogy of social ideas: are we, or are we not, in the revolutionary tradition? It is a question of knowing if at present we are also engaged in revolutionary practice, if, like our fathers, we will be at once men of conservation and of progress, because it is only by this double title that we will be men of revolution.

We have the revolutionary principle, the revolutionary dogma, the revolutionary motto. What is it that we lack in order to accomplish the work entrusted to our hands by Providence? One thing only: revolutionary practice!

But what is that practice which distinguishes the epochs of revolution from ordinary times?

What constitutes revolutionary practice is that it no longer proceeds by technicality and diversity, or by imprescriptible transitions, but by simplifications and enjambments. It passes over, in broad equations, those middle terms which suggest the spirit of routine, whose application should normally have been made during the former time, but that the selfishness of the privilege or the inertia of the governments pushed back.

These great equitations of principles, these enormous shifts in mores, they also have their laws, not at all arbitrary, no more left to chance than the practice of revolutions.

But what, in the end, is that practice?

Suppose that the statesmen we have seen in power since February 24, that these short-sighted politicians of small means, of narrow and meticulous routines, had been in the place of the apostles. I ask you citizens, what would they have done?

They would have fallen into agreement with the innovators of the individual conferences, in secret consultations, that the plurality of gods was an absurdity. They would have said, like Cicero, that it is inconceivable that two augurs could look at one another without laughter; they would have condemned slavery very philosophically, and in a deep voice.

But they would have cried out against the bold propaganda which, denying the gods and all that society has sanctified, raised against it superstition and all the interests; they would have trusted in good policy, rather than tackling the old beliefs, and interpreting them; they would have knelt before Mercury the thief, before impudent Venus and incestuous Jupiter. They would have talked with respect and esteem of the Floralia and the Bacchanalia. They would have made a philosophy of polytheism, retold the history of the gods, renewed the personnel of the temples, published the payments for sacrifices and public ceremonies, according, as far as it was in them, reason and morality to the impure traditions of their fathers, by dint of attention, kindness and human respect; instead of saving the world, they would have caused it to perish.

There was, in the first centuries of the Christian era, a sect, a party powerful in genius and eloquence, which, in the face of the Christian revolution, undertook to continue the idolatry in the form of a moderate and progressive republic; they were the Neo-Platonists, to whom Apollonius of Tyana and the Emperor Julian attached themselves. It is in this fashion that we have seen with our own eyes certain preachers attempt the renovation of Catholicism, by interpreting its symbols from the point of view of modern ideas.

A vain attempt! Christian preaching, which is to say revolutionary practice, swept away all the gods and their hypocritical admirers; and Julian, the greatest politician and most beautiful spirit of his time, bears in the histories the name of apostate, for having been madly opposed to evangelical justice.

Let us cite one more example.

Let us suppose that in ’89, the prudent counselors of despotism, the well-advised spirits of the nobility, the tolerant clergy, the wise men of the middle class, the most patient of the people — let us suppose, I say, that this elite of citizens, with the most upright vision and the most philanthropic views, but convinced of the dangers of abrupt innovations, had agreed to manage, following the rules of high policy, the transition from despotism to liberty. What would they have done?

They would have passed, after long discussion and mature deliberation, letting at least ten years elapse between each article, the promised charter; they would have negotiated with the pope, and with all manner of submissiveness, the civil constitution of the clergy; they would have negotiated with the convents, by amicable agreement, the repurchase of their goods; they would have opened an investigation into the value of feudal rights, and on the compensation to be accorded to the lords; they would have sought compensation to the privileged for the rights accorded to the people. They would have made the work of a thousand years what revolutionary practice might accomplish overnight.

All of this is not just empty talk: there was no lack of men in ’89 willing to connect themselves to this false wisdom of revolution. The first of all was Louis XVI, who was as revolutionary at heart and in theory as anyone, but who did not understand that the revolution must also be practiced. Louis XVI set himself to haggle and quibble over everything, so much and so well, that they revolution, growing impatient, swept him away!

Here then is what I mean, today, by revolutionary practice.

The revolution of February proclaimed the right to work, the predominance of labor over capital.

On the basis of that principle, I say that before overriding all reforms, we have to occupy ourselves with a generalizing institution, which expresses, on all the points of social economy, the subordination of capital to labor; which, in lieu of making, as it has been, the capitalist the sponsor of the laborer, makes the laborer the arbiter and commander of the capitalist, an institution which changes the relation between the two great economic powers, labor and property, and from which follows, consequently, all other reforms.

Will it then be revolutionary to propose an agricultural bank serving, as always, the monopolizers of money; there to create a certified loan office, monument to stagnation and unemployment; elsewhere, to found an asylum, a pawn-shop, a hospital, a nursery, a penitentiary, or a prison, to increase pauperism by multiplying its sources?

Will it be a work of Revolution to finance a few millions, sometimes a company of tailors, sometimes of masons; to reduce the tax on drink and increase it on properties; to convert obligations into losses; to vote seeds and pick-axes for twelve thousand colonists leaving for Algeria, or to subsidize a trial phalanstery?

Will it be the speech or act of a revolutionary to argue for four months whether the people will work or will not, if capital hides or if it flees the country, if it awaits confidence or if it is confidence that awaits it, if the powers will be divided or only the functions, if the president will be the superior, the subordinate or the equal of the national assembly, if the first who will fill this role will be the nephew of the emperor or the son of the king, or if it would not be better, for that good use, to have a soldier or a poet; if the new sovereign will be named by the people or by the representatives, if the ministry of reaction which goes out merits more confidence than the ministry of conciliation which comes, if the Republic will be blue, white, red, or tricolor?

Will it be revolutionary, when it is a question of returning to labor the fictive production of capital, to declare the net revenue inviolable, rather than to seize it by a progressive tax; when it is necessary to organize equality in the acquisition of goods, to lay the blame on the mode of transmission; when 25,000 tradesmen implore a legal settlement, to answer them by bankruptcy; when property no longer receives rent or farm rent, to refuse it further credit; when the country demands the centralization of the banks, to deliver that credit to a financial oligarchy which only knows how to make a void in circulation and to maintain the crisis, while waiting for the discouragement of the people to bring back confidence?

Citizens, I accuse no one.

I know that to all except for us social democrats, who have envisioned and prepared for it, the Revolution of February has been a surprise; and if it is difficult for the old constitutionals to pass in so short a time from the monarchical faith to republican conviction, it is still more so for the politicians of the other century to comprehend anything of the practice of the new Revolution. Other times have other ideas. The great maneuvers of ‘93, good for the time, do not suit us now any more than the parliamentary tactics of the last thirty years; and if we want to abort the revolution, you have no surer means than to take up again these errors.

Citizens, you are still only a minority in this country. But already the revolutionary flood grows with the speed of the idea, with the majesty of the ocean. Again, some of that patience that made your success, and the triumph of the Revolution is assured. You have proven, since June, by you discipline, that you are politicians. From now on you will prove, by your acts, that you are organizers. The government will be enough, I hope, with the National Assembly, to maintain the republican form: such at least is my conviction. But the revolutionary power, the power of conservation and of progress, is no longer today in the hands of the government; it is not in the National Assembly: it is in you. The people alone, acting upon themselves without intermediary, can achieve the economic Revolution begun in February. The people alone can save civilization and advance humanity!




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

