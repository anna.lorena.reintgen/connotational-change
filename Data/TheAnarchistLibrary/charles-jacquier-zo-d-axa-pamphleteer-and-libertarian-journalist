
Alphonse Gallaud — the future Zo d’Axa — was born on May 24, 1864 in Paris to a well-to-do family, his father being a municipal engineer in the city. After completing his studies in Chaptal College, he entered Saint-Cyr’s military academy. He enlisted in a grenadier cavalry regiment and departed with infantrymen to Africa. But the adventure wasn’t as colorful as he dreamed. The youth was bored stiff, and then, in a sudden move, he deserted, with an officer’s wife. Throughout the rest of his life he would be strongly anti-militarist and, at every opportunity, would demonstrate his solidarity with the victims of military institutions. As a refugee in Belgium, he became a journalist for Nouvelles du Jour, but the sedentary life did not suit him well, and he traveled to Switzerland and then Italy. When he was amnestied in 1889, he returned to Paris.

According to Jean Grave, “He made his appearance in the literary environment of Montmartre, where he began to make himself known in some minor circles by announcing his intention of publishing a journal.” Initially, he vacillated between faithfulness to monarchy or to anarchy, as Grave said, classifying him among “those original types who come to probe anarchy”. There were no obvious points in common between Zo d’Axa and the “Pope of Rue Mouffetard”[1]! Despite opposed personalities — Lucien Descaves speaks of “errant chivalry with regard to the former and calls the latter a “sedentary plebeian” — Grave recognized in Zo d“Axa an aristocratic temperament and the merit of publishing excellent articles.

In May 1891, Zo d’Axa published the first issue of L’Endehors, “essentially a literary organ of anarchy” (Flor O’Squarr) of which six thousand copies were printed. Lucien Descaves, a young writer and future member of the Goncourt Academy, knew Zo d’Axa at the time and gives a portrait full of admiration in his memoires. “With a red beard cut to a point, Zo d’Axa resembled a musketeer dressed in civilian clothes. He was beautiful, daring, sarcastic and of unequaled independence. He didn’t mince words either with friends or opponents, when it came to what he held to be truth, his truth. He was en dehors (outside, beyond) with his entire person. He didn’t have to wait for provocation to put up his guard. As independent as he was incapable of calculation, he followed his impulses without answering to anyone. Under the eloquent banner L’Endehors, he had hired at his own risk the low-tonnage boat chartered to torpedo a corrupt society.” An engraving from that time shows the editorial staff of the journal in a basement on Boulevard Rochechouart. Alongside Zo d’Axa are Jean Grave, Augustin Hamon, Bernard Lazare, Charles Malato, Octave Mirbeau. People as varied as Tristan Bernard, Georges Darien, Lucien Descaves, Sébastien Faure, Félix Fénéon, Emile Henry, Camille Mauclair, Pierre Quillard, Emile Verhaeren would collaborate with him. Zo d’Axa offered each one a platform from which they could “express themselves without discreet euphemisms or timorous reticence”. In Le Figaro, Jules Huret wrote: “L’Endehors is a weekly that publishes anarchic writings and ultra-modern literary criticism with unbridled vehemence. It is the refuge of refractories like Georges Darien and pure poets like Henri de Regnier and Saint-Pol Roux. The editor, Zo d’Axa, is a courageous man.” According to Paul Adam “Zo d’Axa is a journalist of valor” and his articles “offer excellent and correct diatribes against the wickedness of the times” (Entretiens politiques et littéraires, volume VI, #37, February 25, 1893).

Very soon, of course, the authorities began to focus on L’Endehors. The author of an article, the editorial director, Louis Matha, and Zo d’Axa, were sentenced to pay a fine of a thousand francs each. In the next issue, Zo d’Axa commented, “Three thousand francs is not expensive”; and he gave the magistrature another thrashing! After the arrest of Ravachol and his comrades in March 1892, L’Endehor opened a subscription “in order to keep the kids whose parents are implacably struck by Society as rebels from starving to death.” Zo d’Axa was arrested, charged with association to commit crimes and imprisoned in Mazas. L’Endehors continued to come out during his imprisonment, thanks above all to Félix Fénéon. A bit later, another article led to further persecution. Without waiting to be arrested again, Zo d’Axa went across the channel.On June 1 and July 5 of 1892, he was sentenced to 18 months and then to two years and two thousand francs, for incitement to murder and pillage.

In exile along with Matha, he briefly found asylum with Charles Malato, one of many political exiles in the French Quarter of London. His host depicted him as a “writer and knight errante..., wrapped in a coat of dark color, with a sombrero on his head. Under its wide brim, only the tufts of his luxuriant beard could be distinguished... Zo d’Axa could have claimed the pen, the sword and the guitar as weapons, since he was a formidable polemicist, a valiant swordsman and an irresistible Don Juan.” After three months, weary of the gray life on the banks of the Thames, he decided to leave on a long journey that would carry him through Europe to the Middle East. In the course of that strange journey, he asked himself if “by taking advantage of the suspicions of the authorities and benefiting from the consequent expulsions — locomotive forces that haul you from one side of a country to the other — one might not be able to go around the world with a minimum of goodwill?” In December of same year he was arrested in Jaffa by the French military that, manu militari, [2] force him to return home on a French Delivery Service boat. He was arrested on his arrival and spent a few days in the Marseilles prison before being transferred to Saint-Pélagie prison in Paris, where he refused to sign a petition for reprieve. He was freed on the day that the corpse of the President of the Republic, Sadi Carnot, who had been killed in Lyon on June 24 by Italian anarchist Sante Caserio, was buried and passed by the depot at the time of the funeral.

While in prison, he wrote the tale of his journey, De Mazas à Jérusalem. The work received an excellent reception from the critics, from Jules Renard to Octave Mirbeau, From Laurent Tailhade to Georges Clémenceau. For example, Jules Renard says, “his book makes you love his character”.

Heedless of any literary career, Zo d’Axa managed the ephemeral anarchis daily La Renaissance (December 1895-January 1896), for which he wrote along with Félix Fénéon, Mécislas Goldberg, Bernard Lazare, Laurent Tailhade, Michel Zévaco. At the end of this experiment, he returned to his traveling life.

In October 1897, in the middle of the Dreyfus affair, Zo d’Axa tried a new experiment. Whenever he could, he published La Feuille. The following fragment could be its manifesto:

“We will also speak to the people, and not in order to flatter them, nor to promise them oceans and mountains, rivers and natural borders, nor even an upright republic or an honest candidate; nor a revolution that foreshadows the advent of an earthly paradise...

“All these anthems are currently crooned; here we will speak clearly. No promises. No deceptions. We will speak of more varied events, we will show the latent causes, we will point out the reasons. And we will reveal the tricks and tell the names of swindlers, thieving politicians, literati — all the sir whatevers.

“We will speak of simple things in a simple manner.”

A single sheet on which we find a design on the front by Maximilien Luce, Steinlein, Wilette, etc., and on the back an article by Zo d’Axa. To give an idea of what the newspaper was like, let’s take the case of the tone used with regard to the Dreyfus affair that enraged France. “Though this gentleman was not a traitor — he was still a captain. Best to drop it.” Of course, there were also words for the copyists of the elder statesman (“Ready! Aim! Distort!”), and for the antisemitic pipsqueaks of La libre parole (“Drumont and Vacher”. But his master stroke was the proclamation of La Feuille‘s candidate, as ass named Nul, that was hoisted on to a cart and walked around Paris on election day to the cheers of passers-by. When the forces of order intervened, Zo d’Axa declared: “Let’s not dwell on it, now that he’s an official candidate.” Hadn’t he presented the little donkey as “a not overly educated ass, a peaceful being who drinks only water and would back away from a glass of wine. More or less the perfect example of a majority (?) representative (m.p.).”

At the dawn of the new century, Zo d’Axa, weary of so much struggle, closed La Feuille and took up the vagabond life on three continents. He sent his impressions of his wanderings to various journals. He wrote for L’Ennemi du peuple, edited by Emile Janvion, who published it from August 1, 1903 through October 1, 1904. In the United States, he went to Paterson (New Jersey) where, in his words, “refugees from the Old World go to sharpen their knives and brood over bullets against the quiet of kings.” In an outskirt of Jersey City, he met the widow of anarchist Gaetano Bresci who had killed King Umberto I of Italy on July 30, 1900. The Revue Blanche of the Natanson brothers published his story in September 1902.

When he returned to France, he settled in Marsailles, where one could have met him “in passing on the Canebière or riding his bicycle around the sunny Corniche” (L. Champion). In 1917, when the chief editor of L’Ordre, Emile Buré — “well-known renegade, but a journalist of talent” according to Mualdes — asked him to write his memoires, Zo d’Axa responded: “It’s no accident that i no longer write, and if, by chance, I should want to dedicate myself to the vain pleasure of thinking out loud, it would not be retrospective. It is the present that I would talk about and, believe, well outside the purr that the Sacred Union makes, since I am still the same despite white hair and silence...” He was still a refractory, “neither the war of 1914–1918 nor the Bolshevik dictatorship had his approval” (L. Champion). In 1921, while passing through Paris, he published his last article in Le Journal du Peuple to respond to journalistic nonsense. The man had aged, but his pen was still sharp and brilliant: “to remain silent would not serve to preserve me from the honor of appearing to be a penitent... The last friends of L’Endehors and La Feuille know the significance of a past that the present has no intention of disowning. For a good stretch, we reacted together against the disgusting reality of the times. We were treated as anarchists. The label wasn’t very important... So what is living? I enjoy the morning on near and distant paths, and without a pen, with the sole aim of comprehending the clear day outside of any wavering mirage, far from the pages on which one writes.”

Zo d’Axa first attempted suicide in 1927. Three years later on August 30, 1930, while living at 71 Promenade de la Corniche in Marseilles, he put an end to his life. The previous night, he had burnt almost all of his papers.

Contrary to Victor Méric’s prediction, Zo d’Axa’s name is not still printed “in fiery letters” in anthologies of great French writers and pamphleteers. All the more reason to simply recall his memory in expectation of a possible but belated recognition of his quality as a writer and his moral rectitude as one of the most original and engaging figures of “fin de siecle” anarchism.

 
[1] A nickname for Jean Grave
[2] By force of arms




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“We will also speak to the people, and not in order to flatter them, nor to promise them oceans and mountains, rivers and natural borders, nor even an upright republic or an honest candidate; nor a revolution that foreshadows the advent of an earthly paradise...


“All these anthems are currently crooned; here we will speak clearly. No promises. No deceptions. We will speak of more varied events, we will show the latent causes, we will point out the reasons. And we will reveal the tricks and tell the names of swindlers, thieving politicians, literati — all the sir whatevers.


“We will speak of simple things in a simple manner.”

