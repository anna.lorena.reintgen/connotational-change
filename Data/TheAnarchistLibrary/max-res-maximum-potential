      strength      solidarity      autonomy      maximum potential
I started this essay with a dilemma—though my intent was to write about anarchists doing fitness, it didn’t seem like there were any. Searching yielded very little, and despite going to the gym myself my motivations are less political praxis and more trying to minimize some of the negative health impacts of late capitalism on my body. Yes, I lacked anything particularly profound to say on the subject matter, but surely in this age of people clamoring for physical conflict in the form of antifascism there was someone writing or doing something relevant.

Maybe the problem is our aversion to the markers of fitness culture? Jocks, hypermasculity, competition, vanity, perhaps the lingering trauma of being pushed into a locker in high school all combine to make fitness potentially unattractive to anarchists. And yet there were other, not-anarchist nerds who were engaging with working out in ways that I found interesting and two essays from whom I’ve included in this pamphlet. The first is from the website Ultra, which describes its contributors as “...those who have been transformed by the recent crises and the sequence of riots, blockades, occupations and strikes that followed” and includes “lift weights” on its list of central tenets[1]. In it, Kyle Kubler’s “Auto Body” gives us a fascinating genealogy of fitness culture in the US rooted in the impromptu bodybuilding culture of Santa Monica’s Muscle Beach in the 40s and 50s to the development of its commodified, yuppy heir CrossFit today. The second is also a history, this one written by Adam Curtis, and if you’re familiar with his work at all you’ll be hearing his voice as you’re reading as it comes though quite clearly. In his signature style, he uses “Bodybuilding and Nation-Building” to connect the seemingly disparate elements of yoga and the roots of the 2003 invasion of Iraq and in doing so looks at cultures (one of the body, the other of imperial fantasy) obsessed with the fantasy of a purer, stronger way of being. Both of these pieces are great and I’m sure you’ll enjoy them as I did, but neither of these is written by anarchists and I wanted to say a little about what we’re thinking and doing for us, today.

This proved more easily said than done. Given the spirit of our era, much discussion about anarchists doing fitness that I could find was so steeped in the language and goals of anti-fascism that the a-word was hardly present. Which, if you consider anarchy and antifa the same thing (or if you’re into lifting weights next to statists) isn’t much of a problem, but this is all to say that there isn’t a whole lot of stuff out there that puts “anarchist” and “going to the gym” in the same sentence without completing it with “to get better at punching Nazis”. But for a look at how some anarchists are doing fitness, this essay will consider Haymaker, which describes itself as a “popular fitness & self-defense gym”[2] located in Chicago. In examining Haymaker’s attempt to create a radical culture of fitness and self-defense we’ll see how they challenge the practices of mainstream fitness presented to us by Kubler and Curtis while also resembling them in its desire to shape and mobilize bodies. To do this, we’ll consider the three points listed on their homepage around which they organize themselves: strength, solidarity, and autonomy.

People associated with Haymaker frequently cite the desire to remove themselves from a culture of fitness defined by machoness and normative body types as incentive for starting a different kind of space. This culture of getting big and the celebration of the ideal body is explored inboth of the other pieces contained in this pamphlet, and Adam Curtis’s exploration of the weird history of yoga in particular is filled with all sorts of priceless overblown advertisements from the turn of the last century filled with shockingly muscled men in loincloths promising to return the reader’s body to some sort of perfection lost in corrupt modernity. Set decades later, Kubler profiles CrossFit gym-goers as solidly upper-middle class and ranks the popularization of the exercise routine alongside “beards, tattoos, ‘work’ boots, and lumbersexuality” in the culture of late capitalism.

In opposition to this, Haymaker gives us a different interpretation of strength:

“Strength is not primarily being able to resist or overcome forces outside of us. To us, strength means overcoming our own weaknesses – it means changing ourselves, together. Such strength is necessary if we are to become a force capable not only of self-defense, but of social transformation.”[3]

Expanding on this, in their promotional video they also express the intent to create their “own ideas of fitness that don’t mean fitting in with status quo body norms”[4]. Here and elsewhere we can see an array of people—different body types, differing gender presentations, and differing cultural practices (including a prominent selection of clips showing people in hijab). A look at their calendar of events also shows a number of “liberatory mixed martial arts” sessions specifically for “trans, queer, and women-aligned folx”[5]. And while they place themselves firmly in the world of anti-fascist physical training, this concept of a radical gym space is also conceived of as an alternative to a macho European antifa culture as mentioned in their Final Straw interview.

This critique of strength as muscle mass and the culture of machoness which can surround it isn’t all that novel—“Auto Body” shows us a history of fitness in the US that moved away from the rougher, bigger bodies of Muscle Beach and the first Gold’s Gym towards something more accessible to the masses in which “you can get strong, but not too big”. This is manifest in Planet Fitness’s “Judgment Free Zone,” which even comes with a “lunk alarm” to shame people if they’re throwing weights and is accompanied by a description of a “lunk” which could easily describe any of the average denizens of these earlier spaces who, in additional to slamming weights, is wearing a bodybuilding tanktop and drinking out of a gallon jug of water. It is also manifest in the mantra “strong is the new skinny” which calls for a more holistic and personalized concept of strength[6]. With a nod to the fact that the “inclusivity” of these mentalities and spaces often falls short of even their own modest advertised goals, nevermind what one might consider desirable in an anarchist space, what makes Haymaker’s critique different is the emphasis on getting strong together, which brings us to our second point.

No less important to Haymaker’s critique is the alienation embodied in much of commercial fitness culture, something which is reflected in my own experience of going to the gym. Planet Fitness is about as far from macho gym culture as you can get – surrounded by mottoes like “You Belong” and “Judgment Free Zone,” much of the crowd when I go is older people and especially older women. However, it and similar chains are an embodiment of the shift identified by Kubler away from the DIY community at Muscle Beach (where bodybuilders were improvising training routines and some even living together) and the first iteration of Gold’s Gym (where the front door was locked to keep out those who weren’t in the know) and towards a mass product dominated by machines with operating instructions and populated by consumers and staff members. A typical workout consists of me talking to one person – the employee at the desk who checks my card and says goodbye when I leave – with the rest of my routine spent listening to music and working out alone in a room of people, most of whom are doing the same (though you do see a regular gathering of old men around the stationary bikes – clearly some of us are more alienated than others).

Community is an attractive commodity in a world where alienation is the norm, and this is no less true in the world of fitness. Beyond Planet Fitness’s “You Belong” and halfhearted monthly pizza nights and bagel breakfasts, Kubler shows us how CrossFit sells the experience of an intense camaraderie through working the body which acts as a commodified version of the long since extinguished days of Muscle Beach (extinguished, by the way, by the long, flabby arm of the law). Compare this to Haymaker’s concept of solidarity:

We believe in solidarity because we know our personal transformation is also a collective transformation and, as the saying goes, an injury to one is an injury to all. We vow to care for each other in times of vulnerability and to keep each other safe as we become dangerous together. [7]

The sell here is an attempt at constructing a community very different from the examples given above – access is free, classes include a section where participants improvise exercises together rather than learning from an instructor, and their promotional video even includes the promise of a juice bar and donation-based food pantry. Haymaker is conceived of as the convergence of a “multitude of different bodies” in a gym that will “cut across social divisions”[8] that they claim are being worsened under the Trump presidency, and a place where, as they put it in their promotional video, “leaving wouldn’t mean leaving alone”[9].

But the primary way that solidarity and mutual aid are expressed at Haymaker is in the form of self-defense training, something that’s emphasized again and again in their interviews and promotional materials to the point where they refer to themselves as a “popular fitness and self-defense gym”. Self-defense here is a physical response to “a political climate that’s increasingly violent, especially towards marginalized peoples”[10], language which mirrors the general antifascist stance since the election of Donald Trump and a practice to which the gym traces a lineage going back to physical defense training among Jews during the second world war and Indian nationalist physical culture gyms under British colonialism[11]. Training at Haymaker is advertised on the premise of reactive violence which is intended to protect people endangered by racists and abusers and those entering into street fights at antifa demos. It is through this violence and the community which it’s suggested emerges from training for it that community is formed. It’s this vision of community that leads to the third point.

It’s easy to see how practical this all is, at least to a certain social set. If you’re concerned about physical conflict and feel unprepared, training to respond physically gives you another tool to deal with an attacker. If you’re in a protest situation where you may end up in a fight, knowing how to fight better than the person you’re in conflict with is to your advantage. Having a free space to learn those skills or just work out and maybe make some friends sounds great, although this seems like it may be the kind of space where friendship is mandatory, and the juice at the juice bar in the promotional video looks… well, you’d have to watch it and decide for yourself.
But all that said, the point of Haymaker isn’t the juice or even the self-defense and strength training. At its heart is the concept of “social transformation” that’s come up in each of the previous points, and that they put forward clearly in their third organizing point:
We believe in autonomy because strength and care cannot grow amidst institutions that disempower us. In this precarious world, we don’t expect anyone to come and save us. We have to fight for ourselves and each other, because we’re all we’ve got.
This definition is a little complicated, in part because, like the concept of “social transformation” mentioned in their point about strength it implies a lot without stating anything clearly. Or to put it another way, its use of simple-sounding language and concepts muddies the radical implications of the ideas driving such a space. While this makes a good talking point if you’re trying to hook a socially-minded outsider, for this essay we’ll use the gym’s interview with Final Straw Radio to draw out a more substantial definition of this term.
The interview with Final Straw is important for a number of reasons – it’s one of two places I could find where, as opposed to the vague, popular language used in interviews with people like Buzzfeed on their website, Haymaker is identified as an anarchist project (the other being the promotion on It’s Going Down)[12]. It’s also during this interview that we’re told the deeper intentions of their project, where, despite all the emphasis placed upon antifa tactics and self-defense in response to violence under Trump in other interviews and promotional material—antifascism is described as “a practical way to make ourselves visible to others” and “an important and significant framework but also to a certain extent quite limited for what we want to try to achieve”[13].
A fair bit of this interview is spent discussing the concept of autonomy, and the guests provide us with a couple of definitions. In the face of a state by which we have become dispossessed and helpless and that perpetuates violence against people though police killings, “autonomy through collective organizing shifts our focus to what we can control and prepare for and builds a politics of our own values”—these values being communization, sharing, and care for each other. Autonomy is also considered as a more precise term for anarchy, in which they’re “creating the conditions of living together that capitalism doesn’t provide”. The gym here acts as one of those conditions, a material grounds for establishing this autonomy as part of a greater project to “reclaim and reappropriate territory” that we’ve been dispossessed of and a “nodal point” at which friendship is supposed to turn into a culture of resistance.
All of this is a far cry from the limited scope of confrontational violence through antifa tactics that characterizes much of the public face of the gym. It’s also refreshing to hear some critique of antifa coming from people who are still located very close to that milieu! But I’m left somewhat confused, because even in this interview the concept of violence is still framed as defensive, something that appears at odds with the stated goals of the project. If we imagine a group of people starting a gym and attempting to reclaim something that they’ve been dispossessed of by the state and capitalism, what kind of resistance might they run into? Things like gym spaces and equipment or food to make juice for the juice bar—attempting to reappropriate these (that is, without someone paying for them or picking them out of the garbage) will almost inevitably lead to some resistance in which violence will end up being used, most likely the violence of the state as the police are called by one’s landlord or some unhappy store manager. What does it look like to face physical conflict with the state rather than, as Los hijos del Mencho put it, “live-action role playing in the streets and hitting each other with sticks”[14]?
This kind of interaction is described to us by one of its members in an interview with a media outlet for Dick’s Sporting Goods of all places, where he talks about how physical self-defense training didn’t help him when he was brutalized by the police except that he felt more prepared to “mentally react”[15] to the assault. This is something I again can see the usefulness of, though it probably doesn’t make getting beaten up by a cop at a protest any less unpleasant. More, mental preparedness for getting beaten or tortured isn’t going to bring down the institutions that disempower us any more than Jews training to fight in the Roman Ghetto ended the functioning of that ghetto, never mind ending the machinery of the Holocaust as a whole. While it may feel good to claim that “strong people are harder to kill”—a slogan, by the way, that’s also on the Ultra website—our present reality is one in which that’s just not the case when it comes to the state exercising power over our bodies, and to think otherwise is to risk falling into the positivist self-improvement mentality that characterizes so many dietary regimens and workout routines.
But let’s assume that the people around Haymaker understand this, aren’t looking for direct conflict with the state (at least not yet), and that, despite the fact that they characterize this project so thoroughly in its public image, antifascism and training around defensive violence is just an opportunity to pull a wide swath of potential allies to get involved in the deeper project of building an autonomy that doesn’t (yet!) mean taking over buildings or driving the cops out of neighborhoods. Let’s also assume that the statement of autonomy isn’t a description of a lived reality but rather a goal towards which establishing a gym is one part of a many-linked chain. After all, the Breakaway Social Center with which the gym is affiliated stresses patience in the process of realizing a “strategy of giving ourselves the means to be more powerful and to face up to the need for another way of life”[16]. Still, it’s hard to look forward to the day when Haymaker and its cousins stop paying rent or needing to collect donations when that day seems so far away, and I’m also not sure who will defend those spaces when the state objects. I’m also somewhat curious about how many people who get pulled in because of the antifa sales pitch and increasing violence under the current president (rhetoric or reality) will stick around when the wind goes out of those sails.

To repeat, I like Haymaker as a response to the dominant culture of fitness. Even in this critique I hope that the reader is able to pull some of their strong points about redefining strength and offer a space that is free, lacks some of the hierarchies of typical training spaces, and is open to all sorts of people while also not open to cops or the extreme right. Beyond the criticisms already mentioned in this essay, though, there is an underlying presumption about bodies and their potential to save us that overlaps with Adam Curtis’s look at the history of yoga.
In “Bodybuilding and Nation-Building,” Curtis observes that the physical cultures of both Britain and colonized India arose as a reaction to an undesired present – for Britain, an escape from a waning empire filled with factories and slums, for Indian nationalists an escape from what they perceived to be a weak, decadent body that characterized its colonial past. For both Britains and Indians, the body formed a site at which revolution could be affected. And while Haymaker’s approach to fitness isn’t in search of some mythic past, it too looks to the body—which they’ve referred to as “the most intimate of material forces”[17]—as a tool for revolution, and strength as a means by which to change the world.
I also wonder at this celebration of the material—the terms “material force” and “material resistance” that can be found in many of their interviews and promotions. Can we draw a line between this and the Indian physical culture described by Curtis as trying to escape what they perceived as the weakness of their past? Out of physical culture came a revolution in yoga that transformed it from a practice that centered around a limited set of poses and emphasized spiritual development to a yoga that showcased muscular bodies and feats of strength a change considered necessary to end British colonial rule and escape the burden of the past. In Haymaker’s description of the material, they make some explicit attempts at differentiating themselves from both “‘critical’ posturing that puts one on the sidelines of every situation”[18] and what they observe as an anarchist fetish for form and process rather than the material conditions which shape interactions in a space. While the former is likely a shot at their anarchist critics who aren’t interested in getting organized, there’s also a sense of self-criticism here—looking back with a critical eye upon the wave of insurrectionary anarchist activity in the US towards the end of the last decade which emphasized movement; discreet, temporary projects; anonymity; and the riot as a point in which people are changed and community is formed. This wave crashed around Occupy, and it’s telling that in Haymaker’s interview with Final Straw Occupy is singled out as an example of anarchists favoring form over substance. It’s not much of a stretch to consider Haymaker and related projects as offering a vision of a winning, muscular anarchy that provides the substance that its own weak past (or querulous cousins in the present) do not, one which is necessary to change the world.
As opposed to this, I would offer that strong bodies can’t necessarily change the world in the way Haymaker wishes. As mentioned earlier, the goal of becoming a “material force” which establishes itself as autonomous from the institutions that rule us doesn’t really follow from the practices of defensive violence or strength training at Haymaker. Even if they were doing combat training, there’s no amount of physical strength or confidence that’s going to create the kind of “collective transformation” they’re interested in. The kind of potential they see in the body runs into the trap of futurity—what they see as progress, we could consider akin to running on a treadmill, the body getting stronger but tiring over time, the great goal of autonomy from the institutions that oppress us always out of reach.
I’ll end this essay with some open thoughts —I don’t know if there’s a better way to run an anarchist gym, but it’s worth further considering what anarchist fitness could look like when not motivated by revolutionary goals or a defense mentality:
— What if training focused on training the body for avoidance and stealth rather than face-to-face confrontation? What does training to avoid security cameras or to act casual when questioned by airport security look like? After all, blending into a crowd while one’s adrenaline is rushing after doing something dangerous and highly illegal is also a study in bodily movement and mindset all of its own.
That said, I feel like a lot of skill training ends up being less about immediate ends and more about making the person who’s training feel like they’re accomplishing something and giving them the comfort that they’re in control of their lives. Fitness consciously motivated by totally mundane incentives (confidence in one’s body, avoiding some of the unpleasant health impacts of living in Society, etc.) in some ways feels more honest.
— I feel a reflexive unease at the concept of my body as a tool or weapon for struggle. If we want to call it my most intimate material, I don’t find the idea of making it serve “the struggle” very attractive. I also think that the body can be undependable, when it can appear as a stranger to me. While strength can be a nice idea, I think understanding the world through weakness (that is, my limitations, where my strength and the strength of others fails) is more informative.
— While I appreciate the potential usefulness of violence in response to the violence of a friend, partner or stranger, I also think it has an attraction that can be misleading. This attraction comes in part from the sense of having a simple answer to a complicated problem, one which anarchists (along with the rest of society) often handle badly. I think that violence used against an abusive partner or friend who has hurt us can achieve some desired outcomes, but can also complicate things and produce undesired outcomes which are neither simplifying nor worth celebrating.
[1] Ultra (n.d.). Retrieved from www.ultra-com.org.
[2] Haymaker Gym (n.d.). Retrieved from haymakergym.org/.
[3] Ibid.
[4] [Haymaker Gym]. (July 12th 2017). Haymaker Official Video [Video File]. Retrieved from vimeo.com.
[5] Haymaker Gym April 2018 calendar (n.d.). Retrieved from haymakergym.org.
[6] See for example Amy K. Mitchell’s “Why Strong is the New Skinny, and Why That’s a Good Thing” in The Huffington Post:: “The bottom line is, weight aside and skinny aside, you won’t be happy unless you are holistically strong: Strong in body, mind, and spirit”. Retrieved from www.huffingtonpost.com.
[7] Haymaker Gym (n.d.).
[8] viiiHaymaker Official Video.
[9] ixIbid.
[10] [Mong Phu]. (July 3rd 2017). Original Haymaker Collective Video from Unicorn Riot [Video File]. Retrieved from www.youtube.com – note that this is not a friendly source, the original has been deleted from the Unicorn Riot website and this video now lives through circulation by alt-right-ish people. Why this was deleted is unclear (it’s nowhere in Haymaker’s promotional material either), but the quote and sentiments expressed in it are reflected other interviews with members as well. The (now dead) source URL is here: www.unicornriot.ninja.
[11] Anonymous contributor. “Announcing Haymaker: Popular Fitness and Self-Defense in Chicago.” It’s Going Down, April 11th 2017. Retrieved from itsgoingdown.org.
[12] During this interview we’re told that not everyone associated with Haymaker is an anarchist so perhaps this is part of the term’s absence, but the fact that neither it nor autonomist, appelista, etc. appear on their site or in most other interviews or promotional material where they’re describing themselves makes this decision appear to be more about salesmanship rather than inclusivity.
[13] The Final Straw Radio. (June 4th 2017). Podcast special: Haymaker Gym in Chicago [Audio Podcast]. Retrieved from thefinalstrawradio.noblogs.org. Of interest is the fact that this critique of antifa also occurs in one of the few interviews they have with an anarchist source (though not on IGD, for obvious reasons).
[14] Los hijos del Mencho. “Against the World-Builders: Eco-extremists respond to critics.” Anarchist News, January 14th 2018. Retrieved from anarchistnews.org.
[15] Sarit Luban. “The Chicago Gym Using Fitness As Political Resistance.” Good Sports, September 19th 2017. Retrieved from sports.good.is.
[16] Breakaway Autonomous Social Center (n.d.). “Who we are.” Retrieved from breakaway.center.
[17] Antifascistfront. “Introducing Haymaker, Chicago’s New Anti-Fascist Gym.” Anti-Fascist News, April 19th 2017. Retrieved from antifascistnews.net. See also their interview with Final Straw where similar language is used.
[18] Breakaway Autonomous Social Center. “Who we are.”




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

