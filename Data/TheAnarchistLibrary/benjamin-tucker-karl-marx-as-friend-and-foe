
By the death of Karl Marx the cause of labor has lost one of the most faithful friends it ever had. Liberty says thus much in hearty tribute to the sincerity and hearty steadfastness of the man who, perhaps to a greater extent than any other, represented, by nature and by doctrine, the principle of authority which we live to combat. Anarchism knew in him its bitterest enemy, and yet every Anarchist must hold his memory in respect. Strangely mingled feelings of admiration and abhorrence are simultaneously inspired in us by contemplation of this great man’s career. Toward the two fundamental principles of the revolution of to-day he occupied an exactly contradictory attitude. Intense as was his love of equality, no less so was his hatred of liberty. The former found expression in one of the most masterly expositions of the infamous nature and office of capital ever put into print; the latter in a sweeping scheme of State supremacy and absorption, involving a practical annihilation of the individual. The enormous service done by the one was well-nigh neutralized by the injurious effects resulting from his advocacy of the other. For Karl Marx, the égalitaire, we feel the profoundest respect; as for Karl Marx, the autoritaire, we must consider him an enemy. Liberty said as much in its first issue, and sees no reason to change its mind. He was an honest man, a strong man, a humanitarian, and the promulgator of much vitally important truth, but on the most vital question of politics and economy he was persistently and irretrievably mistaken.



We cannot, then, join in the thoughtless, indiscreet, and indiscriminate laudation of his memory indulged in so generally by the labor press and on the labor platform. Perhaps, however, we might pass it by without protest, did it not involve injustice and ingratitude to other and greater men. The extravagant claim of precedence as a radical political economist put forward for Karl Marx by his friends must not be allowed to overshadow the work of his superiors. We give an instance of this claim, taken from the resolutions passed unanimously by the great Cooper Union meeting held in honor of Marx: "In the field of economic social science he was the first to prove by statistical facts and by reasoning based upon universally recognized principles of political economy that capitalistic production must necessarily lead to the monopolizing and concentrating of all industry into the hands of a few, and thus, by robbing the working class of the fruits of their toil, reduce them to absolute slavery and degradation." These words were read to the audience in English by Philip Van Patten and in German by our worthy comrade, Justus Schwab. Is it possible that these men are so utterly unacquainted with the literature of Socialism that they do not know this statement to be false, and that the tendency and consequence of capitalistic production referred to were demonstrated to the world time and again during the twenty years preceding the publication of Das Kapital, with a wealth of learning, a cogency and subtlety of reasoning, and an ardor of style to which Karl Marx could not so much as pretend? In the numerous works of P. J. Proudhon, published between 1840 and 1860, this notable truth was turned over and over and inside out until well-nigh every phase of it had been presented to the light.



What was the economic theory developed by Karl Marx? That we may not be accused of stating it unfairly, we give below an admirable outline of it drown by Benoit Malon, a prominent French Socialist, in sympathy with Marx’s thought. Aside from the special purpose which we have in quoting it, it is in itself well worth the space which it requires, being in the main a succinct and concise statement of the true principles of political economy:



All societies that have existed thus far in history have one common characteristic,—the struggle of classes. Revolutions have changed the conditions of this struggle, but have not suppressed it. Though the bourgeoisie has taken the place of feudalism, which was itself the successor of the old patrician order, and though slavery and serfdom have been succeeded by the prolétariat, the situation has retained these two distinctive characteristics,—"the merciless oppression and exploitation of the inferior class by the dominant class, and the struggle, either open or concealed, but deadly and constant, of the classes thus confronting each other."

The bourgeoisie, to obtain power, had to invoke political and economic liberty. In the name of the latter, which it has falsified, and aided by scientific and industrial progress, it has revolutionized production and inaugurated the system of capitalistic production under which all wealth appears as an immense accumulation of merchandise formed elementarily upon an isolated quantity of that wealth.

Everything destined for the satisfaction of a human need has a value of utility; as merchandise it has a value of exchange. Value of exchange is the quantitative relation governing the equivalence and exchangeability of useful objects.

As the most eminent economists have shown, notably Ricardo, this quantitative relation, this measure of value, is time spent in labor. This, of course, can refer only to the amount of labor necessary upon an average and performed with average skill, mechanical facilities, and industry under the normal industrial conditions of the day.

It seems, therefore, that every one should be able to buy, in return for his labor, an amount of utilities and exchangeable values equivalent to those produced by him.

Nevertheless, such is not the case. "The accumulation of wealth at one of the poles of society keeps pace with the accumulation, at the other pole, of the misery, subjection, and moral degradation of the class from whose product capital is born."

How happens this? Because, by a series of robberies which, though sometimes legal, are none the less real, the productive forces, as fast as they have come into play, have been appropriated by privileged persons who, thanks to this instrumentum regni, control labor and exploit laborers.

To-day he who is destined to become a capitalist goes into the market furnished with money. He first buys tools and raw materials, and then, in order to operate them, buys the workingman’s power of labor, the sole source of value. He sets them to work. The total product goes into the capitalist’s hands, who sells it for more than it cost him. Of the plus-value capital is born; it increases in proportion to the quantity of plus-value or labor not paid for. All capital, then, is an accumulation of the surplus labor of another, or labor not paid for in wages.

For this singular state of things individuals are not to be held responsible; it is the result of our capitalistic society, for all events, all individual acts are but the processus of inevitable forces slowly modifiable, since, "when a society has succeeded in discovering the path of the natural law which governs its moment, it can neither clear it at a leap nor abolish by decree the phases of its natural development. But it can shorten the period of gestation and lessen the pains of delivery."

We cannot, then, go against the tendencies of a society, but only direct them toward the general good. So capitalistic society goes on irresistibly concentrating capital.

To attempt to stop this movement would be puerile; the necessary step is to pass from the inevitable monopolization of the forces of production and circulation to their nationalization, and that by a series of legal measures resulting from the capture of political power by the working classes.

In the meantime the evil will grow. By virtue of the law of wages the increase in the productivity of labor by the perfecting of machinery increases the frequency of dull seasons and makes poverty more general by diminishing the demand for and augmenting the supply of laborers.

That is easily understood.

For the natural production of values of utility determined and regulated by real or fancied needs, which was in vogue until the eighteenth century, is substituted the mercantile production of values of exchange,—a production without rule or measure, which runs after the buyer and stops in its headlong course only when the markets of the world are gorged to overflowing. Then millions out of the hundreds of millions of prolétaires who have been engaged in this production are thrown out of work and their ranks are thinned by hunger, all in consequence of the superabundance created by an unregulated production.

The new economic forces which the bourgeoisie has appropriated have not completed their development, and even now the bourgeois envelope of capitalistic production can no longer contain them. Just as industry on a small scale was violently broken down because it obstructed production, so capitalistic privileges, beginning to obstruct the production which they developed, will be broken down in their turn; for the concentration of the means of production and the socialization of labor are reaching a point which renders them incompatible with their capitalistic envelope.

At this point the prolétariat, like the bourgeoisie, will seize political power for the purpose of abolishing classes and socializing the forces of production and circulation in the same order that they have been monopolized by capitalistic feudalism.

The foregoing is an admirable argument, and Liberty endorses the whole of it, excepting a few phrases concerning the nationalization of industry and the assumption of political power by the working people; but it contains literally nothing in substantiation of the claim made for Marx in the Cooper Institute resolutions. Proudhon was years before Marx with nearly every link in this logical chain. We stand ready to give volume, chapter, and page of his writings for the historical persistence of class struggles in successive manifestations, for the bourgeoisie’s appeal to liberty and its infidelity thereto, for the theory that labor is the source and measure of value, for the laborer’s inability to repurchase his product in consequence of the privileged capitalist’s practice of keeping back a part of it from his wages, and for the process of the monopolistic concentration of capital and its disastrous results. The vital difference between Proudhon and Marx is to be found in the respective remedies which they proposed. Marx would nationalize the productive and distributive forces; Proudhon would individualize and associate them. Marx would make the laborers political masters; Proudhon would abolish political mastership entirely. Marx would abolish usury by having the State lay violent hands on all industry and business and conduct it on the cost principle; Proudhon would abolish usury by disconnecting the State entirely from industry and business and forming a system of free banks which would furnish credit at cost to every industrious and deserving person, and thus place the means of production within the reach of all. Marx believed in compulsory majority rule; Proudhon believed in the voluntary principle. In short, Marx was an autoritaire; Proudhon was a champion of Liberty.

Call Marx, then, the father of State Socialism, if you will; but we dispute his paternity of the general principles of economy on which all schools of Socialism agree. To be sure, it is not of the greatest consequence who was the first with these doctrines. As Proudhon himself asks: "Do we eulogize the man who first perceives the dawn?" But if any discrimination is to be made, let it be a just one. There is much, very much that can be truly said in honor of Karl Marx. Let us be satisfied with that, then, and not attempt to magnify his grandeur by denying, belittling, or ignoring the services of men greater than he.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

