      Humans for Hire: Selling Yourself for Fun and Profit      More = Better??      Putting Profits Above People      “Anarcho” capitalism: Bourgeois Bombast      “Anarchy” with Bosses?      “Freedom” to Starve      Goons with Guns      Owners Uber Alles
“From each according to their gullibility, to each, according to his greed.”

Capitalists are always eager to put glossy packaging on tired old products in order to put one over on the purchasing public. In this way, they hope to rekindle demand for what is actually the same product they have been providing people in the past.

This is the rationale behind what can only be called “anarcho” chic; that is, the usurpation and appropriation of anarchist forms without anarchist substance, in an effort to create the illusion that somehow, magically, capitalism is about freedom, liberty, and anarchy!

The following terms are generally used by these laissez-faire capitalists to describe themselves:

“anarcho” capitalist

libertarian

libertarian capitalist

anarch

“anarchist”

While we (actual anarchists, e.g., those who oppose rulers) can’t claim possession of any term, we have an obligation to point out the glaring inconsistencies in the laissez-faire capitalist use of anarchistic terminology. They use the term “anarchist”, but at the expense of their credibility — why? Because their self-definition doesn’t hold up to even the most rudimentary questioning.

“Anarcho” capitalists are, in fact, simply capitalists who object to the State cutting into their own profits by way of regulations and taxation. That is their sole gripe with the State. They see the bureaucrat as the nefarious boogeyman in their lives, motivated solely to enmesh the world in red tape — simply out of maliciousness alone.

“Anarcho” capitalists do not object to private property, to class distinctions, social stratification, concentrated wealth, and other bourgeois trappings in society. Their idea of a utopia is a world of unaccountable, unfettered corporate power where literally everything is up for sale and is negotiable.

Far from being the vindication of humanist values, the “anarcho” capitalist ethic is the denial of them before arbitrary, inhumane market forces. The “ideal” social interaction, in “anarcho” capitalist terms, is that of prostitution.

Prostitution, e.g., selling your services for an anticipated monetary gain, is the highest definition of “anarcho” capitalist “empowerment”, amazingly. The ability to sell yourself to whomever you want is the “anarcho” capitalist idea of “freedom”.

Nothing would be free from market forces. Not families, not children, not the environment, and, of course, not you! Literally everything would have a price tag! Clean air, clean water, housing, human organs — each not an end unto themselves, but a marketable commodity: a product! In such a dystopia, anything which could not be readily translated into product would be cast out as pointless and without value (measured only in economic terms, of course).

Thus, visual art would become, instead, graphic design; writing would be merely ad copy; poetry reduced to syrupy greeting card maxims; and so on — The humanities as we know them would wither away. This is occurring already in higher education, as humanities departments get less and less academic funding.

To the “anarcho” capitalist, there is no problem here. If humanities were “worth” anything, economically, universities would invest in them more heavily. Why this attitude?

It is because, to the “anarcho” capitalist, what is “good” is purely what is profitable. Conversely, that which is not profitable is termed “bad” (or at best, “worthless”).

You can see how this attitude has poisoned our existing culture to the extent that it has. How do you defend an open park along such harsh, utilitarian lines when to the “anarcho” capitalist an open park is a parking lot waiting to happen?

This quantitative ethic messes up their reasoning. If what is profitable is good, then a book that sells a million copies MUST be good, right? Or a coat that costs $2,000 has to be high quality, by their own definition. But this isn’t so. A good book may be bought by a lot of people — but then again, it may be ignored for generations! The fact that lots of people buy it doesn’t make it intrinsically good!

Moreover, what sells the most tends to be that which appeals to the largest number of people — this means that things which challenge or threaten people the least will typically do the best, economically. It is in this manner that within a capitalistic society, culture fizzles out, as art and literature are co-opted into feel-good propagandistic fluff.

Because “anarcho” capitalists use the market as their sole gauge of good and bad, they are, in effect, unable to make effective moral judgments! This percolates into all of their thinking — they revere wealthy entrepreneurs as examples of virtue, basing this solely on their quantitative ethic. If this ethic holds true, then, in the US, Bill Gates must be the most virtuous person in human history! General Motors must be the most virtuous of corporations! This is clearly untrue, therefore: Merit (e.g., good and bad) cannot be ascertained in quantitative economic terms.

“Anarcho” capitalist “freedom” is the freedom to have anything which you can afford! Thus, those with the most money in an “anarcho” capitalist society have the MOST freedom — which means that those with the LEAST money have the LEAST freedom. This bothers true anarchists very much. It doesn’t trouble “anarcho” capitalists in the least.

To anarchists, freedom has to be available for ALL, not just those with the cash to afford it! Otherwise, it is meaningless. True anarchists would never put a price tag on freedom!

It is this difference that reveals the manifestly bourgeois, reactionary quality of “anarcho” capitalism, contrasted with the revolutionary, radical outlook necessary for anarchistic consistency.

“Anarcho” capitalists talk of freedom as a negative, in a (Ayn) Randian definition of: “the absence of physical violence”. They see capitalism as the epitome of this ethic, and the State as the antithesis of it (defining the State as “the institution with a monopoly of force”).

This is the cornerstone of their professed anarchism. They say, “we oppose the State; anarchists oppose government; ergo, we are anarchists.”

But anarchists look at that statement and ask:

What of the boss in the workplace?

What of the wealthy owner of property?

What of the capitalist industrialist?

What of the church elder?

What of the judge?

What of the patriarch of a family?

Don’t these people have very real authority over others’ lives? Haven’t each of these, in their way, brought shame, misery, and degradation to those under their control?

The “anarcho” capitalist has no problem with rulers below State level, so long as they don’t impinge on profit and property! So, if your boss eavesdropped on your calls, the “anarcho” capitalist would say, “hey, you can always get a new job” rather than taking the anarchist stance of “how dare X boss eavesdrop on their employees?! We must work to end workplace tyranny!”

In fact, to the “anarcho” capitalist, being able to work for whomever you want (including working for clients [e.g., “self”-employment) is what they consider “freedom”. This amounts to choosing who gets to be your boss! Some choice, huh?

Anarchists, in contrast, don’t think there should BE any bosses. Everyone pulls their fair share of the collective social burden of day-to-day living. And, while everyone works, the distinction between this and typical capitalist drudgery is that, in anarchy, you’d be working for your own needs, rather than for the profit of another! As such, you wouldn’t have to put in 40+ hour weeks lining the pockets of whoever owns the company you work for (or servicing your clients’ needs).

But “anarcho” capitalists don’t want any part of that; they cling to vague notions of “freedom” and “liberty” that simply fail to stand up under scrutiny. The only “freedom” that exists in the capitalist laissez-fairyland “anarcho” capitalists defend is the freedom to work for another’s gain or starve!

Any rational being knows that you have to work to survive. This is a law of nature. But in capitalist society, some people (owners), don’t HAVE to work! They live off of the surplus (that is, profit) earned by others — their employees! So, magically, some people are able, within capitalist society, to defy the laws of nature — they profit without working for it!

But profits come from property; that is, assets that allow for the generation of surplus. And for this to occur, these owners must own capital (land, factories, etc.) Which means that any old Joe can’t come onto “their” property and live off it — otherwise, no surplus...no profit...no capitalism!

In other words, the “choice” of working for another or starving isn’t a choice, in capitalist society, because the worker can’t go off and live on their own; somebody owns the very ground they walk on.

And this leads us to the next glaring inconsistency of “anarcho” capitalism: the absolute necessity of the State in their affairs. All rhetoric aside, laissez-faire capitalists NEED the State to uphold contracts and defend property “rights”. Otherwise, there is nothing to prevent squatters from coming along and usurping someone’s holdings.

So, these selfsame “anarchs” will rely on law enforcement personnel and paramilitary goons to protect their property. Now, they note that these latter-day Pinkertons would not be instruments of Statist oppression, but rather, are employees of private “defense firms”. But I guarantee that the truncheons they use on you will feel the same, regardless of who their boss is. In fact, there are fewer safeguards with paramilitaries, because, unlike municipal police forces, these are paid employees of the capitalists in question! Thus, if their boss wants them to shoot strikers, they’ll do it, or risk losing their employment! And you know what? This is exactly what happened during the golden age of laissez-faire capitalism, when the Pinkerton Detective Agency serviced industrialists across the United States.

Further, the “anarcho” capitalists will still require a court system, and thus laws, to uphold property rights and contracts! These private judicial firms would offer the “best” justice to the clients who paid them the best! Some justice!

Laissez-faire capitalists don’t particularly care what happens to people; despite their lofty declarations about liberty and freedom, their actions put the lie to them. They say, “nobody FORCES you to work for somebody else”, but if you don’t have your own capital reserve (like most of us), what choice do you have? You must work or starve!

Nothing humanistic about this ideology! In fact, laissez-faire capitalism has much more in common with fascism, the old enemy of anarchism, than with democracy! The simplest exploration of the workplace reveals this reality: who has the final say in the workplace...the average worker, or the owner? The owner, of course. That’s why they’re called “the Boss”. It’s their property, the laissez-faire capitalists say, so they have the authority. Pure, top-down, fascistic decision-making in action.

Now, certainly, workplaces make a grand show of including workers in the decision-making process, but you’ll find that this involvement focuses on ratifying and executing decisions the owners have already made,instead of the owners seeking the advice and experience of the people who actually DO THE WORK within the company! Ultimately, where a given property is concerned, the owners have the final authority. Anarchists rightly see this as deeply authoritarian; “anarcho” capitalists pretend otherwise, and advise you to start your own company, or become self-employed (as if these were effective remedies)!

Anarchism is about challenging unjust authority (and any authority wrought by coercion is unjust); capitalism is about making a profit from the labor of others. The two have nothing in common! The “anarchs”, “anarcho” capitalists, laissez-faire capitalists, and Libertarians of this world don’t object to rulers, except when rulers cut into their profits! This makes them not anarchistic at all, but manifestly bourgeois in character, ethic, and temperament. “Anarcho” capitalism is a reactionary credo with more in common with postindustrial feudalism and outright fascism than anything remotely anarchistic, for the aforementioned reasons.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“anarcho” capitalist



libertarian



libertarian capitalist



anarch



“anarchist”



What of the boss in the workplace?



What of the wealthy owner of property?



What of the capitalist industrialist?



What of the church elder?



What of the judge?



What of the patriarch of a family?

