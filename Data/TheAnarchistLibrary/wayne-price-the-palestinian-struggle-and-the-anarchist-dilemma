      Comments on Gordon’s Anarchy Alive!      Possible Anarchist Responses      Gordon’s Weaknesses      Gordon’s Attack on My Views      Which Side Are You On?      References
There has been very little written on the relation between anarchism and the Palestinian struggle against Isreali oppression. Therefore it is interesting to read the discussion of this topic by an Israeli anarchist, Uri Gordon, in his recent book, Anarchy Alive! Chapter 6 is titled, “HomeLand: Anarchy and Joint Struggle in Palestine/Israel.” (pp. 139 — 162) Unfortunately, the chapter is marred by an intemperate and gratuitous attack on my views. Before getting to this, I will review his discussion.

Gordon confronts “the apparent contradiction between anarchists’ commitment to support oppressed groups on the latter’s own terms, and those terms being — in the Palestinian case — a new nation-state.” (p. 139) Again, he says that the conflict “...between anarchist’ anti-imperialist commitments ... and their traditionally wholesale rebuttal of the state and nationalism..., would seem to leave them at an impasse regarding the national liberation struggles of oppressed peoples.” (p. 152) This expresses the dilemma nicely.

He briefly notes that Bakunin, Gustav Landauer, and Rudolf Rocker — all historically important anarchists — supported a people’s attachment to its own culture and land (including their right to secede from larger units) but opposed national states. Kropotkin supported national liberation struggles of stateless peoples to remove foreign domination. Gordon could have mentioned anarchists’ participation in many national liberation and anti-imperialist struggles around the world, perhaps the most famous being Nestor Mahkno in the Ukraine. However, these examples do not resolve the dilemma of Palestine/Israel.

As he notes, most Palestinians want their own state next to Israel. He worries that anarchist opposition to this demand could be seen as “paternalism,” saying that we know what is good for the Arabs better than they do. More significantly, he is concerned that opposition to a Palestinian state leaves anarchists with nothing positive to say, except that Palestinians need anarchism. But they do not — yet? — want anarchism and it is not going to happen anytime soon. (Nor, I would add, are people likely to be persuaded of anarchism if it is seen as opposed to what they do want, namely national self-determination.) Shall anarchists say that we refuse to support the Palestinians’ struggle against a brutal national oppression until the Palestinians see the light and oppose states and capitalism?

Gordon offers a series of possible “responses” (by Israeli or European anarchists essentially) to this dilemma.

A first possible response, he writes, is to accept that there is inconsistency in “endorsement of Palestinian statehood by anarchists,” (p. 154) but to endorse it anyway due to the primary value of solidarity. It may be the only “pragmatic,” “viable,” way to counter the Palestinians’ oppression “in the short term.” (pp. 154–5) (I am reviewing his opinions, which I find thought-provoking, but not yet stating my own.)

A second possible response, he suggests, would deny that there is any inconsistency for anarchists. Palestinians already live under a state, that of Israel (including in the Occupied Territories). To demand that Palestinians live under a Palestinian state instead of under that of Israel would not be unprincipled for an anti-statist, he argues. At most it would be just as bad for the Palestinians; at best, it might be somewhat better, due to the removal of direct foreign oppression.

His third response is “anarchists can support a Palestinian state as a strategic choice...” (p. 155), one step in a long term struggle. Obviously, the region will not move immediately into anarchism; there will be many stages to go through. Decreasing the tensions between the Israeli Jews and Palestinian Arabs can open things up for further struggles around gender, sexual orientation, and class in each nation. Having got what they wanted, the Palestinians may learn the limitations of statist solutions and continue the struggle on a new basis.

A fourth response may seem to completely contradict the other three responses. It is to ignore the issue of national statehood while supporting day-to-day Palestinian struggles for jobs and dignity. This includes defending farmers from attacks by Jewish settlers, opposing the wall when it cuts through villages, taking apart roadblocks, etc. Anarchists can be engaged in as united fronts with nationalists, without agreeing with their politics. Israeli anarchists may loyally participate in them without endorsing a Palestinian state. He cites the work of Bill Templer, an anarchist, who recognizes that there will be an eventual two-state settlement in the short run, but focuses his work among Israelis and Palestinian villagers around such issues as resisting the wall. Templer believes that such work will someday lead to a “dual power” situation as it “hollows out” the state and capitalism. (p. 161)

Uri Gordon deserves credit for trying to face up to the anarchist dilemma in dealing with the issue of Palestinian oppression. Although he never says which response he agrees with (which is somewhat confusing), he seems to support them all to some degree. As anyone who has consistently read my material on this site knows, I am sympathetic to an anarchist who wants to both support national movements against oppression while remaining anti-statist and anti-capitalist (e.g., Price, 2006). However I do not think he has quite found the proper resolution of the dilemma.

Gordon does not distinguish between “endorsement of Palestinian statehood by anarchists,” or “anarchists can support a Palestinian state,” on the one hand, and anarchist support or endorsement of the Palestinians’ national self-determination, on the other. The first idea means that anarchists would say that we agree with the program of an independent state for Palestinians, that we think it would be a good thing for them. This would be a drastic mistake. It would be “paternalistic” in that it would not tell the Palestinians the truth as best s we see it.

Support for self-determination is quite different. It implies that out of solidarity we defend Palestinians getting the solution they want, because they want it, even though we anarchists would not make this choice. Similarly, we defend the freedom of workers to join the union of their choice, even though we are likely to oppose most business unions. We defend people’s legal right to vote, as against dictatorships, even though we are anti-electoralists. We defend the legal right to divorce, even though we neither advocate that any particular couples break up nor support bourgeois marriage. In brief, anarchists should defend oppressed people’s freedom to make choices, without having to agree with the choices they pick. Making their own choices is how people (and peoples, and classes) learn.

Further, his “endorsement” and “support” for a new state, in the short term, misses the point that nationalism can misdirect the struggle. While in solidarity with the Palestinian people (who are mostly peasants, workers, and small businesspeople), anarchists still oppose the program of nationalism. At best, the Palestinians could win their own, structurally independent, state. But they would still be dominated by the world market and international power politics. That is, they would not win real national liberation. That needs an international revolution of the workers and all the oppressed. The nationalist leaders have a disasterous program for the Palestinians. Whether or not Gordon knows this, he does not insist that anarchists say this, even while supporting Palestinian struggles (not say it at every moment of course, but over time and in various ways). He never discusses how to help persuade some Palestinians of anarchism instead of nationalism.

He tries to deal with this by his suggested fourth response, in which anarchists ignore the statehood question while showing solidarity in action. In practice, solidarity actions, united front work, is the right tactic, but eventually the statehood issue would become impossible to ignore. Surely friendly Palestinians would want to know whether we support their freedom to have their own state or not. What would Gordon answer? Templer, his model, apparently does accept the coming of a Palestinian state; he could not ignore the issue in practice.

Gordon introduces his discussion of my opinions by saying that Wayne Price “descends into very crude terms.” (p. 150) He then quotes me:

“...Israel is the oppressor and the Palestinian Arabs are the oppressed. Therefore anarchists, and all decent people, should be on the side of the Palestinians. Criticisms of their leaderships or their methods of fighting are all secondary; so is recognition that the Israeli Jews are also people and also have certain collective rights. The first step, always, is to stand with the oppressed as they fight for their freedom.” (quoted on p. 150)

This passage does not deny that nationalist misleaders should be criticized or that some methods of fighting (e.g. attacks on civilians) should be criticized nor does it deny that Israeli Jews are people and that they should have certain collective rights. But it says that anarchists (and all decent people) should start by being on the side of the oppressed, the Palestinians, against the state of Israel. Frankly I thought this was noncontroversial among anarchists.

Not so for Gordon. He writes, “Asking all decent people to see someone else’s humanity and collective rights as secondary to anything — whatever this is, this is not anarchism....This kind of attitude has become...a typically leftist form of Judeophobia or anti-Semitism.” (p. 150) So, I am not an anarchist and am perhaps an anti-Semite! (It is a blessing that Gordon does not like using “crude terms.”)

He claims that I ignore the (small minority of) Israelis who have worked with Palestinians. Based on nothing whatever, he refers to “Price’s complete indifference to those who consciously intervene against the occupation....” (same) He says that they take action not “because they are ‘siding with the Palestinians,’ but rather out of a sense of responsibility and solidarity.” (same) Responsibility for what, if not for the oppression of Palestinians by the Israeli state? Solidarity with whom, if not with the Palestinians? Earlier, he even quoted, with approval, a statement by the International Solidarity Movement, which declared a need “to actively engage in resistance to the Occupation, to take sides...” (quoted on p. 142) That is, “siding with the Palestinians.”

Again he quotes me: “We must support the resistance of the Palestinian people. They have the right to self-determination, that is, to choose their leaders, their programs, and their methods of struggle, whatever we think.” (quoted on p. 151)

Gordon again goes ballistic, calling this passage, “A blank check, then, to suicide bombings and any present or future Palestinian elite.” (p. 151) But as the last phrase (“whatever we think”) should make clear, supporting the Palestinians’ resistance and self-determination does not mean that we have to agree with their leaders, programs, or methods of struggle. In this I disagree with Gordon, as stated above, since he apparently does support and endorse a Palestinian state, despite its inevitable “Palestinian elite” (in Responses 1, 2, and 3).

Interestingly, throughout this chapter, he only discusses conceivable anarchist “responses” to the two-state program (Israel plus Palestine), never to the idea of a democratic-secular (or binational) single state. Perhaps (I speculate), this is due to his concern for the interests of Israeli Jews, since a two-state settlement would mean that they would keep their own, Zionist-oppressor, state?

Gordon argues that it wrong of me to ask the movement to make demands on the Israeli, the U.S., or any other state. “...This would be a ‘politics of demand’ which extends undue recognition and legitimation to state power....” This is “far removed from anarchism.” (p. 151) (Personally I do not say that people who call themselves anarchists, but with whom I otherwise disagree, are not anarchists, nor am I interested in “proving” that what I propose is anarchist. ) In any case, this is an odd attack coming from someone who is willing to consider “endorsing” or “supporting” the Palestinians’ demand for their own state (a demand on the Israeli and U.S. states).

Anarchists have often made demands on the state, such as to stop waging specific wars or to release prisoners. And we have made demands on capitalists, as in fighting for union recognition or better working conditions. Refusing to make demands on the state or on the capitalists may sound very radical (as if they care whether anarchists give them “recognition and legitimation”!) but it is a reformist cop-out, an abdication of the struggle.

Gordon is so upset that I denied the humanity of Israeli Jews (which I did not do), that I wondered if he would be as concerned about the humanity of other oppressors. And he is! He quotes the revolutionary anarchist Errico Malatesta, “The slave is always in a state of legitimate defense and consequently, his [note] violence against the boss, against the oppressor, is always morally justifiable.” (quoted on p. 100) However, Malatesta added that violence should be “controlled” by taking into account “human effort and human sufferings.” (same)

Gordon reacts by noting that the modern worker, even though exploited, is not the same as a chattel slave (true, but irrelevant to Malatesta’s point). He then writes that Malatesta is seeking “a convenient way to dehumanize ‘class enemies’ for the sole purpose of making the violation of persons more palatable.” (p.100) This is in spite of the fact that Gordon does not come out for absolute pacifism in his discussion of violence and non-violence (chapter 4).

What Gordon wants to emphasize is the humanity of the exploiter. Yet oppressors have never suffered from a lack of defenders. It is the slaves, the workers, and the oppressed nations who need defenders — or more precisely, comrades.

I think that Uri Gordon expresses well the dilemma of anarchists in dealing with national liberation struggles. He looks for ways to be for the oppressed nation of Palestine while remaining true to his anti-statist and anti-capitalist convictions. While respecting his motives, and sharing them, I do not think that he succeeds. I suggest an alternate approach based on defending national self-determination while opposing nationalism.

Unfortunately, his thought-provoking discussion is marred by intemperate attacks on my opinions. His reaction is apparently due to his over-sensitivity toward the interests of oppressors (such as the Israeli Jews or the capitalists — his examples). He objects to the idea that we should be “siding with the Palestinians.” By his own account, then, Gordon does not stand unequivocally on the side of the oppressed, the exploited, and the wretched of the earth.

Gordon, Uri (2008). Anarchy alive! London/Ann Arbor: Pluto Press.

Price, Wayne (2006). Lessons for the Anarchist Movement of the Isreali-Lebanese War; The Anarchist Debate About National Liberation www.anarkismo.net




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



Gordon, Uri (2008). Anarchy alive! London/Ann Arbor: Pluto Press.



Price, Wayne (2006). Lessons for the Anarchist Movement of the Isreali-Lebanese War; The Anarchist Debate About National Liberation www.anarkismo.net

