
“And when the foam of the arrogant waves flows back sighing,

the lustrous pearls of life lie on the sand.”

— Giorgio Colli, La Ragione Errabonda (The Wandering Reason)

I see Empedocles walking away from Agrigento. Saluting the inhabitants with his unmistakable wave, he sets out toward the summit of Etna. With the nimbleness of thought his feet move swiftly, almost to the top. Now the volcano captures his thought...I mean his senses. It seems to him that earth has never vibrated with so much force, as if all the mysteries were no longer hidden in the abysses, but were on the surface among the minerals and the crevices. The air becomes increasingly tense like the strings of a lyre, nearly taking the breath away from anyone who can climb no further, but only return to his paths and his habits. In the distance, Empedocles sees the sea, the night is accomplice. There will be no return. Now the fire of the crater is making him dizzy.

This is the one who described the four elements of the cosmos in his great poem: earth, air fire and water. He had written that the life of every being depends upon their mixing and separating. Love unites them and Hatred divides them in an endless harmony. Empedocles did not achieve his wisdom through the academy, but by dancing as prey to divine madness, moved by the arrogant force of a communism of the spirit. But on this night, he is alone and there is no longer time for celebrations. Everything becomes simple, the earth throbs, the wind blows, the sea bides its time, the fire calls. He has seen the enigma of the cosmos, which the gods left as a challenge to the intelligence. Tonight he has nothing to add to his life. He will reach out to drink fire from the eternal craters in first flight.

I see Homer seated on the rocks of an island, facing the sea of Io. He is a tired man, the wisest of the Greeks, lover of rest after many battles in the arena of the intellect and of words. Almost absent-mindedly, he asks a group of fishermen coming from the sea if they caught anything. “What we caught, we left; we carry what we did not catch”, they answer him. With this riddle, they refer to the fleas, some of which they found and crushed and some of which they carry in their clothes. The content of the statement is empty, but the formulation is that of the classic enigma. And the enigma is a challenge, an encounter at daggers drawn between the intellect and that which is hidden. Homer’s passion for knowledge is enflamed again, the attack allows no escape, the wise one must solve the enigma. The wager is understanding, the risk is life. Homer does not know how to solve the enigma, does not hold the square and dies “from discouragement.”

Alluding to the Sphinx which is its symbol, a fragment from Pindar speaks of the enigma that “resounds from the ferocious jaws of the virgin.” Therefore, the challenge that can hide itself even in a riddle that resounds from the jaws of a fisherman is ferocious. The Sphinx, which is the figure that precisely represents the common background of human and animal life, will address its enigma to all the inhabitants of Thebes. The wager in play is peace against a terrible miasma, a famine that kills old and young. The challenge of the monster, like that of the fishermen, is trifling as a riddle. (“What creature goes first on four legs, then on two and, finally, on three?”) No one is able to solve it, and the city counts its corpses. A vagabond named Oedipus will answer “Man” (first, an infant who cannot walk, then an adult and finally an old man who needs a cane). The sharp word causes the sphinx to sink into the abyss.

I see Heracleitus the obscure, alone in his forest. he spoke only to the children among the inhabitants of Ephesus. I watch him feed on roots and berries until swollen with water, just one step from death. Having gone through the pathos of the hidden, he knows that speech is a weapon of Apollo the oblique, the god who strikes from a distance. He never says it nor hides it, but nods. Thus, he asks the doctor to whom he turns to stop the dropsy (the wise lover of fire — symbol of the infinite possible — is consumed by water) if with his knowledge he is able to nurture a fire in this place where there is a flood. The doctor who does not enjoy solving enigmas is astonished and powerless, and Heracleitus the obscure returns to the forest where one alone is like ten thousand. He buries himself under dung in an attempt to dry his own body out, counting on the force of the sun which is new every day. He had written that it is most necessary extinguish the arrogance (the hubris) from the fire. But he does not renounce the fire, the arrogance of that which is hidden inside of us. Vanquished by water, he is torn apart by dogs.

I see Diogenes the Cynic, wandering and renegade, walking on that roadway that leads anyone who is sent into exile to become a bandit. A question of words of course, seeing that their interpretation can change life. With Diogenes, the sovereign detachment of the wise one is transformed into the histrionic gesture and the publicized style of provocation. Empedocles and Heracleitus are already distant, but their fire is not completely extinguished. Having gone to Delphi to consult the oracle, Diogenes did not succeed in solving the enigma. The oracle had told of the necessity of overturning the political institutions of his city, but he had interpreted this is minting false coins. But ethics, or rather being in agreement with oneself, which is to say with the enigmas of one’s demons, is not an inconsequential hobby. Now Diogenes is in exile, and there is no place for an honest counterfeiter among those who respect money at least as much as they respect authority.

I see Socrates on his deathbed. He no longer has the pedantry that distinguished him in the plaza at Athens, in the guise of the moralist of the state who uses his own courage to as a defense of the Law. A greater Sophist than the others, if he did not love the haughtiness of the powerful, he loved the individual who would place her own ethic above collective morality and authority even less. Rightly, some have seen the first specter of the baleful shadow of christianity in him. Socrates carries the signs of the community of contestation and of the challenge in decline, as well as those of the fulltime intellectual, the dialectical peacemaker, the professor of philosophy within himself. With him, the reason that is born from play and ecstasy wrongly begins to renounce its own ferocity with the sole aim of institutionalizing its power. And yet, Socrates never wrote, convinced that truth is inseparable from the one who states it and from the moment in which it is stated, that it is not a logical or juridical principle.

I see something allusive turn about him, something theatrical and deep. To anyone who awaits some clear precept, his jaw leaves only one phrase, an enigma on which understanding continues to stumble: “We owe a rooster to Asclepius.”

The god hurls deceptive words; he is cruel; he wants to subdue intelligence. His intent is to cause death with banal and solemn formulas in the name of his own power. Human intelligence doesn’t immediately grasp it, convinced, for fear of seeming overly simple, that the solution of an enigma could never be within reach. The god laughs.

What is an ideology at bottom if not a fixed game of enigmas? To say that the boss exists to maintain the workers (and not the reverse), is this not a mystery that would make a child smile? Calling war a humanitarian operation, is this not a riddle unworthy of the fishermen who tricked Homer? State propaganda is a refrain that perpetually repeats: “What is it that owns nothing, but can grant everything?” Even the teeth with which it bites us, this biter by the name of “state”, are ours. Yet it seems omnipotent; its deadly riddle deceives. Its concessions are our “rights”, it tells us, the rest is prohibited, criminal, terrorist.

Television and newspapers — those ferocious jaws of power — tell us over and over that the rich and poor form a “nation” with common interests, that money produces social wealth, that freedom is the child of law, that the police defend the collective well being, that anyone who steals is at fault, etc. Mediocre enigmas, the aim of which is not to challenge the intelligence, but to put it to sleep; the banality of these enigmas serves to hide the wager in play: death. In fact, just as in ancient Greece, anyone who doesn’t know how to uncover the illusion — anyone who doesn’t know how to escape from the ideology — dies. Every day the world is planted with corpses because of Exchange, the contamination of food, war, work, nuclear experiments, everyday banking operations and so on. And yet is all this justified with these enigmas? Pure and simple reversals of reality, boorish lies, tattered sophisms and flashy mystifications. The majority of our contemporaries repeat them without conviction. Men, women and children die, but intelligence is not gripped by distress nor allowed to be overcome by discouragement. The Sphinx of money and law laughs.

The reason is that the daily constrictions, the obligations of survival, the forced displacements, the misery of relationships are the terrain where the stupid and ferocious riddles of power take hold. The banality of gestures and habits, this treacherous enemy of amazement and thought, renders one inept for the contestation in which intelligence forges its arms, the unending dialogue in which ideas become dangerous. This is the terrible coherence of the society in which we live: programmed and subjugated activity produces the opinions adapted to accepting it, lived misery elaborates its own conception. Besides, doesn’t the blackmail of ideology — which is to say the justification of this reality — perhaps present itself as opinions? And it is well known that everyone has his opinion; so the concrete activity of individuals thinks to abolish differences of fashion in it.

The sudden entry of the unknown into daily life shakes up the reign of enigmas. This id shown by all the cases of men and women who “go out of their heads”, as they say, in other words, who are no longer able to bring their social identity and the perception they have of themselves into agreement. But it is demonstrated still more clearly by what occurs in the course of revolts, of riots, of insurrections. Why do men and women who had supported the police up until the night before find themselves attacking them? Why does the economic need of demanding more money turn into the anti-economic desire to burn down the banks? Why does the petty hatred toward criminals transform itself into assaults on prisons and solidarity in revolt? To sum it up, why are the enigmas of money and authority solved?

Because, in the course of these social tempests, individuals become more intelligent. Because the tricks of power become banal when men and women cease to be so. Because insurgents, in the community of unique ones, change their own lives and hence their understanding of the world. Because amazement, which is the source of all authentic thought, stands out above habit. Because social relationships — which the enigma reveals and hides at the same time — become more direct, burning down the foundation of the “dialectic of negotiation” and of ideology: from one side or the other of the barricades. Because the challenge of the enigma, the ferocity of its attack on the intelligence, the extreme competition of knowledge, finds men and women ready to welcome it rather than merely submit to it, aware now of the wager in play: freedom or death. Because at last the Nietzschian metamorphosis of the lion into the child, of sheltered self-defense into the opening to the game and to the marvelous completes itself.

That these moments of suspension of habit — and thus, of norms — are often only excesses of fever of an organism that then returns to the normal temperature (the zero degrees of survival), only serves to confirm the enigmatic relationship that exists between intelligence and revolt. The arrogant wave of the latter leaves its pearls on the shore for the former, then it retreats. But where?

For the Greeks, the dialectic was the art of solving enigmas. Mortal battle of reason, contest between humans and gods, mad and impassioned game, arrogance of thought: the dialectic — which precedes the birth of philosophy — is the intellectual expression of the enigma. It is mental and physical confrontation at the same time, knowledge for living. It has nothing, then to do with the hegelian or marxist dialectic. It is not a question of an objective mechanism of History, nor of a device of conciliatory reason. In this sense, when Engels said that the proletariat had to realize modern German philosophy, that is to become dialecticians, he jumbled the papers. The dialectic of the exploited is only the practical intelligence that dissolves the illusions of ideology, first of these illusions being that of waiting on the shore of the tranquil and inexhaustible river of Progress. That tomorrow will be better than today is a perfidious illusion of the god, the monstrous sneer of the Sphinx.

Someone correctly interpreted the bombing of the plaza Fontana as an enigma thrown at proletarians. The overturning of reality was flagrant and ordinary but the immediate result was death. The state accused anarchists of having done that which only the state and its servants could and can do. The aim was clear: to bring social subversion to defeat. An enigma. The clash was unequal like that between a god and a human: official propaganda had all the newspapers and television as its own, the revolution had leaflets and the streets. And yet dialectical intelligence (in the ancient Greek sense, and not the hegelian as some would claim) didn’t fail. The challenge was accepted with all arms. A decade of social war demonstrates it.

The enigmas of ideology are degraded along with the hearts and minds of those that don’t know how to solve them. The current lies of the economy and of politics talk on and on about the state of the critic. Where are the dangerous ideas? That in similar cities, with similar lines, with similar relations, with a similar look, with similar food, with similar work and similar controls, everyone doesn’t rise up — isn’t this perhaps an authentic enigma?

I see Empedocles, Homer, Oedipus, Heracleitus, Diogenes and Socrates, and I think that liquidating the enigmas of power is simply the path for arriving at last at other enigmas, those of our own demons. Knowledge for living, however it comes, will always bring us face to face with ourselves. On the summit of Etna, in the middle of a forest, in wandering or under dung.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“And when the foam of the arrogant waves flows back sighing,


the lustrous pearls of life lie on the sand.”


— Giorgio Colli, La Ragione Errabonda (The Wandering Reason)

