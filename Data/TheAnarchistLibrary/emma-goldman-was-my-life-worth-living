      I      II      III
It is strange what time does to political causes. A generation ago it seemed to many American conservatives as if the opinions which Emma Goldman was expressing might sweep the world. Now she fights almost alone for what seems to be a lost cause; contemporary radicals are overwhelmingly opposed to her; more than that, her devotion to liberty and her detestation of government interference might be regarded as placing her anomalously in the same part of the political spectrum as the gentlemen of the Liberty League, only in a more extreme position at its edge. Yet in this article, which might be regarded as her last will and testament, she sticks to her guns. Needless to say, her opinions are not ours. We offer them as an exhibit of valiant consistency, of really rugged individualism unaltered by opposition or by advancing age.

The Editors.

* * *

How much a personal philosophy is a matter of temperament and how much it results from experience is a moot question. Naturally we arrive at conclusions in the light of our experience, through the application of a process we call reasoning to the facts observed in the events of our lives. The child is susceptible to fantasy. At the same time he sees life more truly in some respects than his elders do as he becomes conscious of his surroundings. He has not yet become absorbed by the customs and prejudices which make up the largest part of what passes for thinking. Each child responds differently to his environment. Some become rebels, refusing to be dazzled by social superstitions. They are outraged by every injustice perpetrated upon them or upon others. They grow ever more sensitive to the suffering round them and the restriction registering every convention and taboo imposed upon them.

I evidently belong to the first category. Since my earliest recollection of my youth in Russia I have rebelled against orthodoxy in every form. I could never bear to witness harshness whether I was outraged over the official brutality practiced on the peasants in our neighborhood. I wept bitter tears when the young men were conscripted into the army and torn from homes and hearths. I resented the treatment of our servants, who did the hardest work and yet had to put up with wretched sleeping quarters and the leavings of our table. I was indignant when I discovered that love between young people of Jewish and Gentile origin was considered the crime of crimes, and the birth of an illegitimate child the most depraved immorality.

On coming to America I had the same hopes as have most European immigrants and the same disillusionment, though the latter affected me more keenly and more deeply. The immigrant without money and without connections is not permitted to cherish the comforting illusion that America is a benevolent uncle who assumes a tender and impartial guardianship of nephews and nieces. I soon learned that in a republic there are myriad ways by which the strong, the cunning, the rich can seize power and hold it. I saw the many work for small wages which kept them always on the borderline of want for the few who made huge profits. I saw the courts, the halls of legislation, the press, and the schools — in fact every avenue of education and protection — effectively used as an instrument for the safeguarding of a minority, while the masses were denied every right. I found that the politicians knew how to befog every issue, how to control public opinion and manipulate votes to their own advantage and to that of their financial and industrial allies. This was the picture of democracy I soon discovered on my arrival in the United States. Fundamentally there have been few changes since that time.

This situation, which was a matter of daily experience, was brought home to me with a force that tore away shams and made reality stand out vividly and clearly by an event which occurred shortly after my coming to America. It was the so-called Haymarket riot, which resulted in the trial and conviction of eight men, among them five Anarchists. Their crime was an all-embracing love for the fellow-men and their determination to emancipate the oppressed and disinherited masses. In no way had the State of Illinois succeeded in proving their connection with the bomb that had been thrown at an open-air meeting in Haymarket Square in Chicago. It was their Anarchism which resulted in their conviction and execution on the 11th of November, 1887. This judicial crime left an indelible mark on my mind and heart and sent me forth to acquaint myself with the ideal for which these men had died so heroically. I dedicated myself to their cause.

It requires something more than personal experience to gain a philosophy or point of view from any specific event. It is the quality of our response to the event and our capacity to enter into the lives of others that help us to make their lives and experiences our own. In my own case my convictions have derived and developed from events in the lives of others as well as from my own experience. What I have seen meted out to others by authority and repression, economic and political, transcends anything I myself may have endured.

I have often been asked why I maintained such a non-compromising antagonism to government and in what way I have found myself oppressed by it. In my opinion every individual is hampered by it. It exacts taxes from production. It creates tariffs, which prevent free exchange. It stands ever for the status quo and traditional conduct and belief. It comes into private lives and into most intimate personal relations, enabling the superstitious, puritanical, and distorted ones to impose their ignorant prejudice and moral servitudes upon the sensitive, the imaginative, and the free spirits. Government does this by its divorce laws, its moral censorships, and by a thousand petty persecutions of those who are too honest to wear the moral mask of respectability. In addition, government protects the strong at the expense of the weak, provides courts and laws which the rich may scorn and the poor must obey. It enables the predatory rich to make wars to provide foreign markets for the favored ones, with prosperity for the rulers and wholesale death for the ruled. However, it is not only government in the sense of the state which is destructive of every individual value and quality. It is the whole complex of authority and institutional domination which strangles life. It is the superstition, myth, pretense, evasions, and subservience which support authority and institutional domination. It is the reverence for these institutions instilled in the school, the church and the home in order that man may believe and obey without protest. Such a process of devitalizing and distorting personalities of the individual and of whole communities may have been a part of historical evolution; but it should be strenuously combated by every honest and independent mind in an age which has any pretense to enlightenment.

It has often been suggested to me that the Constitution of the United States is a sufficient safeguard for the freedom of its citizens. It is obvious that even the freedom it pretends to guarantee is very limited. I have not been impressed with the adequacy of the safeguard. The nations of the world, with centuries of international law behind them, have never hesitated to engage in mass destruction when solemnly pledged to keep the peace; and the legal documents in America have not prevented the United States from doing the same. Those in authority have and always will abuse their power. And the instances when they do not do so are as rare as roses growing on icebergs. Far from the Constitution playing any liberating part in the lives of the American people, it has robbed them of the capacity to rely on their own resources or do their own thinking. Americans are so easily hoodwinked by the sanctity of law and authority. In fact, the pattern of life has become standardized, routinized, and mechanized like canned food and Sunday sermons. The hundred-percenter easily swallows syndicated information and factory-made ideas and beliefs. He thrives on the wisdom given him over the radio and cheap magazines by corporations whose philanthropic aim is selling America out. He accepts the standards of conduct and art in the same breath with the advertising of chewing gum, toothpaste, and shoe polish. Even songs are turned out like buttons or automobile tires — all cast from the same mold.

Yet I do not despair of American life. On the contrary, I feel that the freshness of the American approach and the untapped stores of intellectual and emotional energy resident in the country offer much promise for the future. The War has left in its wake a confused generation. The madness and brutality they had seen, the needless cruelty and waste which had almost wrecked the world made them doubt the values their elders had given them. Some, knowing nothing of the world’s past, attempted to create new forms of life and art from the air. Others experimented with decadence and despair. Many of them, even in revolt, were pathetic. They were thrust back into submission and futility because they were lacking in an ideal and were further hampered by a sense of sin and the burden of dead ideas in which they could no longer believe.

Of late there has been a new spirit manifested in the youth which is growing up with the depression. This spirit is more purposeful though still confused. It wants to create a new world, but is not clear as to how it wants to go about it. For that reason the young generation asks for saviors. It tends to believe in dictators and to hail each new aspirant for that honor as a messiah. It wants cut and dried systems of salvation with a wise minority to direct society on some one-way road to utopia. It has not yet realized that it must save itself. The young generation has not yet learned that the problems confronting them can be solved only by themselves and will have to be settled on the basis of social and economic freedom in co-operation with the struggling masses for the right to the table and joy of life.

As I have already stated, my objection to authority in whatever form has been derived from a much larger social view, rather than from anything I myself may have suffered from it. Government has, of course, interfered with my full expression, as it has with others. Certainly the powers have not spared me. Raids on my lectures during my thirty-five years’ activity in the United States were a common occurrence, followed by innumerable arrests and three convictions to terms of imprisonment. This was followed by the annulment of my citizenship and my deportation. The hand of authority was forever interfering with my life. If I have none the less expressed myself, it was in spite of every curtailment and difficulty put in my path and not because of them. In that I was by no means alone. The whole world has given heroic figures to humanity, who in the face of persecution and obloquy have lived and fought for their right and the right of mankind to free and unstinted expression. America has the distinction of having contributed a large quota of native-born children who have most assuredly not lagged behind. Walt Whitman, Henry David Thoreau, Voltairine de Cleyre, one of America’s great Anarchists, Moses Harman, the pioneer of woman’s emancipation from sexual bondage, Horace Traubel, sweet singer of liberty, and quite an array of other brave souls have expressed themselves in keeping with their vision of a new social order based on freedom from every form of coercion. True, the price they had to pay was high. They were deprived of most of the comforts society offers to ability and talent, but denies when they will not be subservient. But whatever the price, their lives were enriched beyond the common lot. I, too, feel enriched beyond measure. But that is due to the discovery of Anarchism, which more than anything else has strengthened my conviction that authority stultifies human development, while full freedom assures it.

I consider Anarchism the most beautiful and practical philosophy that has yet been thought of in its application to individual expression and the relation it establishes between the individual and society. Moreover, I am certain that Anarchism is too vital and too close to human nature ever to die. It is my conviction that dictatorship, whether to the right or to the left, can never work — that it never has worked, and that time will prove this again, as it has been proved before. When the failure of modern dictatorship and authoritarian philosophies becomes more apparent and the realization of failure more general, Anarchism will be vindicated. Considered from this point, a recrudescence of Anarchist ideas in the near future is very probable. When this occurs and takes effect, I believe that humanity will at last leave the maze in which it is now lost and will start on the path to sane living and regeneration through freedom.

There are many who deny the possibility of such regeneration on the ground that human nature cannot change. Those who insist that human nature remains the same at all times have learned nothing and forgotten nothing. They certainly have not the faintest idea of the tremendous strides that have been made in sociology and psychology, proving beyond a shadow of a doubt that human nature is plastic and can be changed. Human nature is by no means a fixed quantity. Rather, it is fluid and responsive to new conditions. If, for instance, the so-called instinct of self-preservation were as fundamental as it is supposed to be, wars would have been eliminated long ago, as would all dangerous and hazardous occupations.

Right here I want to point out that there would not be such great changes required as is commonly supposed to insure the success of a new social order, as conceived by Anarchists. I feel that our present equipment would be adequate if the artificial oppressions and inequalities and the organized force and violence supporting them were removed.

Again it is argued that if human nature can be changed, would not the love of liberty be trained out of the human heart? Love of freedom is a universal trait, and no tyranny has thus far succeeded in eradicating it. Some of the modern dictators might try it, and in fact are trying it with every means of cruelty at their command. Even if they should last long enough to carry on such a project — which is hardly conceivable — there are other difficulties. For one thing, the people whom the dictators are attempting to train would have to be cut off from every tradition in their history that might suggest to them the benefits of freedom. They would also have to isolate them from contact with any other people from whom they could get libertarian ideas. The very fact, however, that a person has a consciousness of self, of being different from others, creates a desire to act freely. The craving for liberty and self-expression is a very fundamental and dominant trait.

As is usual when people are trying to get rid of uncomfortable facts, I have often encountered the statement that the average man does not want liberty; that the love for it exists in very few; that the American people, for instance, simply do not care for it. That the American people are not wholly lacking in the desire for freedom was proved by their resistance to the late Prohibition Law, which was so effective that even the politicians finally responded to popular demand and repealed the amendment. If the American masses had been as determined in dealing with more important issues, much more might have been accomplished. It is true, however, that the American people are just beginning to be ready for advanced ideas. This is due to the historical evolution of the country. The rise of capitalism and a very powerful state are, after all, recent in the United States. Many still foolishly believe themselves back in the pioneer tradition when success was easy, opportunities more plentiful than now, and the economic position of the individual was not likely to become static and hopeless.

It is true, none the less, that the average American is still steeped in these traditions, convinced that prosperity will yet return. But because a number of people lack individuality and the capacity for independent thinking I cannot admit that for this reason society must have a special nursery to regenerate them. I would insist that liberty, real liberty, a freer and more flexible society, is the only medium for the development of the best potentialities of the individual.

I will grant that some individuals grow to great stature in revolt against existing conditions. I am only too aware of the fact that my own development was largely in revolt. But I consider it absurd to argue from this fact that social evils should be perpetrated to make revolt against them necessary. Such an argument would be a repetition of the old religious idea of purification. For one thing it is lacking in imagination to suppose that one who shows qualities above the ordinary could have developed only in one way. The person who under this system has developed along the lines of revolt might readily in a different social situation have developed as an artist, scientist, or in any other creative and intellectual capacity.

Now I do not claim that the triumph of my ideas would eliminate all possible problems from the life of man for all time. What I do believe is that the removal of the present artificial obstacles to progress would clear the ground for new conquests and joy of life. Nature and our own complexes are apt to continue to provide us with enough pain and struggle. Why then maintain the needless suffering imposed by our present social structure, on the mythical grounds that our characters are thus strengthened, when broken hearts and crushed lives about us every day give the lie to such a notion?

Most of the worry about the softening of human character under freedom comes from prosperous people. It would be difficult to convince the starving man that plenty to eat would ruin his character. As for individual development in the society to which I look forward, I feel that with freedom and abundance unguessed springs of individual initiative would be released. Human curiosity and interest in the world could be trusted to develop individuals in every conceivable line of effort.

Of course those steeped in the present find it impossible to realize that gain as an incentive could be replaced by another force that would motivate people to give the best that is in them. To be sure, profit and gain are strong factors in our present system. They have to be. Even the rich feel a sense of insecurity. That is, they want to protect what they have and to strengthen themselves. The gain and profit motives, however, are tied up with more fundamental motives. When a man provides himself with clothes and shelter, if he is the money-maker type, he continues to work to establish his status — to give himself prestige of the sort admired in the eyes of his fellow-men. Under different and more just conditions of life these more fundamental motives could be put to special uses, and the profit motive, which is only their manifestation, will pass away. Even to-day the scientist, inventor, poet, and artist are not primarily moved by the consideration of gain or profit. The urge to create is the first and most impelling force in their lives. If this urge is lacking in the mass of workers it is not at all surprising, for their occupation is deadly routine. Without any relation to their lives or needs, their work is done in the most appalling surroundings, at the behest of those who have the power of life and death over the masses. Why then should they be impelled to give of themselves more than is absolutely necessary to eke out their miserable existence?

In art, science, literature, and in departments of life which we believe to be somewhat removed from our daily living we are hospitable to research, experiment, and innovation. Yet, so great is our traditional reverence for authority that an irrational fear arises in most people when experiment is suggested to them. Surely there is even greater reason for experiment in the social field than in the scientific. It is to be hoped, therefore, that humanity or some portion of it will be given the opportunity in the not too distant future to try its fortune living and developing under an application of freedom corresponding to the early stages of an anarchistic society. The belief in freedom assumes that human beings can co-operate. They do it even now to a surprising extent, or organized society would be impossible. If the devices by which men can harm one another, such as private property, are removed and if the worship of authority can be discarded, co-operation will be spontaneous and inevitable, and the individual will find it his highest calling to contribute to the enrichment of social well-being.

Anarchism alone stresses the importance of the individual, his possibilities and needs in a free society. Instead of telling him that he must fall down and worship before institutions, live and die for abstractions, break his heart and stunt his life for taboos, Anarchism insists that the center of gravity in society is the individual — that he must think for himself, act freely, and live fully. The aim of Anarchism is that every individual in the world shall be able to do so. If he is to develop freely and fully, he must be relieved from the interference and oppression of others. Freedom is, therefore, the cornerstone of the Anarchist philosophy. Of course, this has nothing in common with a much boasted “rugged individualism.” Such predatory individualism is really flabby, not rugged. At the least danger to its safety it runs to cover of the state and wails for protection of armies, navies, or whatever devices for strangulation it has at its command. Their “rugged individualism” is simply one of the many pretenses the ruling class makes to unbridled business and political extortion.

Regardless of the present trend toward the strong-armed man, the totalitarian states, or the dictatorship from the left, my ideas have remained unshaken. In fact, they have been strengthened by my personal experience and the world events through the years. I see no reason to change, as I do not believe that the tendency of dictatorship can ever successfully solve our social problems. As in the past, so I do now insist that freedom is the soul of progress and essential to every phase of life. I consider this as near a law of social evolution as anything we can postulate. My faith is in the individual and in the capacity of free individuals for united endeavor.

The fact that the Anarchist movement for which I have striven so long is to a certain extent in abeyance and overshadowed by philosophies of authority and coercion affects me with concern, but not with despair. It seems to me a point of special significance that many countries decline to admit Anarchists. All governments hold the view that while parties of the right and left may advocate social changes, still they cling to the idea of government and authority. Anarchism alone breaks with both and propagates uncompromising rebellion. In the long run, therefore, it is Anarchism which is considered deadlier to the present regime than all other social theories that are now clamoring for power.

Considered from this angle, I think my life and my work have been successful. What is generally regarded as success — acquisition of wealth, the capture of power or social prestige — I consider the most dismal failures. I hold when it is said of a man that he has arrived, it means that he is finished — his development has stopped at that point. I have always striven to remain in a state of flux and continued growth, and not to petrify in a niche of self-satisfaction. If I had my life to live over again, like anyone else, I should wish to alter minor details. But in any of my more important actions and attitudes I would repeat my life as I have lived it. Certainly I should work for Anarchism with the same devotion and confidence in its ultimate triumph.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

