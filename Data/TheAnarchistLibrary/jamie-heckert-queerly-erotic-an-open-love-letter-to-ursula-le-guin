
Dear Ursula,

This is the first love letter I’ve written to an 80-year-old woman. Most of the people I have fallen in love with, or in lust with, have been men. And so far, all of them have been at least 30 years younger than you. Nor is this love like that I feel for my grandmothers, nearer to you in age and gender. In many ways, I feel closer to you. They have not been so forthcoming with their own stories. I suppose they’ve learned not to be in a culture which often ignores old women. Nor have they had many chances to speak to me, nor I to listen. Unlike most in human history, my culture is carefully segregated by age. You have reminded me of this oddity in your own fragments of anarchist anthropology.

No, my relationship with your writing is different. This love is queerly erotic. Not straightforwardly erotic in that narrow sense of genital or sexual pleasure. Erotic as used by feminists such as Audrey Lorde, Shulamith Firestone, Chaia Heller or yourself: a profoundly joyful awareness of being alive. Even when it hurts. Pleasure and power: diffuse, decentralised. Reading your writing helps me to connect with that awareness. For this, I am deeply grateful.

I’ve been inspired, too, by how queerly you manage to do this. I’m thinking here of the Germanic origin of the word — quer: to cross — and how it’s been taken up by activists and scholars (and activist-scholars) to mean that which crosses, blurs, undermines or overflows supposed borders, not least between hetero and homo sexualities. With a gender fluid love story (The Left Hand of Darkness), tales of love and resistance in a bisexual polyamorous culture (The Birthday of the World and Other Stories), or the gender-free erotic imagery of amoeba sex, sunlight on skin, dancer’s feet (The Wave in the Mind) and of other animal bodies (e.g., “She Unnames Them”), you’ve queered gender, sexuality, species and eroticism. What gifts!

Politics, too, you have queered. Your anarchism is neither straightforward nor straight. It curves and flows, spirals and eddies. Overflowing separations of love and revolution, politics and spirituality, listening and telling, the possibilities you invite cannot be contained by any singular understanding, by any border. You’ve refused, too, that moral border between the good anarchist and the evil archist.

In recent interviews, you suggested that perhaps you don’t qualify for the label anarchist because you are middle-class or because what you do isn’t activism. I invite you to reconsider. Who needs activism when you have wu wei? And why not be a middle-class anarchist? This is no contradiction in my book — and your own books have never shied away from life’s apparent contradictions. They are embraced, queered.

The stories, essays and poems you’ve written have served as much needed guides, helping me learn to imagine my own life. With this help and the help of others, I become better able to act as a guide myself: to write and speak, listen and act in ways that help others to imagine theirs. Whether or not you call yourself an anarchist, you’ve helped me to deepen my own understanding of what an anarchist can be, can do. Of what I can do. Of who I can become.

For this, I feel the most abiding love.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

