
Pornography continues to be a controversial issue, including among anarchists, whom one might expect to be among the strongest supporters of free sexual expression. However, many anarchists have criticized pornography and some have supported and or participated in the antipornography movement, the members of which not infrequently strive to prevent those wishing to view pornography from doing so. Some anarchists in Canada even went so far as to firebomb a sex video store, an activity which many other anarchists either ignored or chose not to criticize. Meanwhile, those of us who defend pornography and freedom of expression, sexual or otherwise, are dismissed as sexists and reactionaries. Why is it that supposed lovers of freedom and sexual liberation seem to forget their principles when it comes to sexually explicit literature and pictures?

The antipornography movement, including its anarchist members and supporters, is not monolithic. Some dislike dirty books and movies, but support people’s freedom to produce and consume such material. They rely on argument and protest in an attempt to change the attitudes of those who like porn, encouraging them to refrain from indulging in it, and do not support censorship. Others, again including some anarchists, feel that physical attacks on porn stores or government-mandated censorship are acceptable tactics in the fight against porn. While only the latter position is censorious, and therefore unanarchic, the former position, which is contemptuous of depictions of sex is also problematic in a movement which purportedly favors sexual freedom.

Pornography is simply a depiction, in words or pictures, of sexual activity. Most people find sex a good, pleasurable activity and looking at pornography is sexually arousing for many people, Anti-porn people frequently say that the images of women in porn are degrading and offensive to women. However, while some women certainly are offended by pornographic images they find degrading, other women enjoy pornography. (See, for instance the book Caught Looking by Kate Ellis, et al, or Writing Sado-masochistic Pornography: A Woman’s Defense, by Deborah Ryder.) While the anti-porn movement views women as a class, who all share the same goals and desires, women are not a mass of automatons who all think and feel alike; some are pro-porn and some are anti-porn, just like men. Additionally, the images of women in porn are no more sexist and demeaning towards women than the images of women in most literature and visual media, from novels to movies to TV to magazine ads. In a sexist society, most images of women are going to contain at least some of the sexist attitudes common to both women and men. Besides, some pornography contains women characters who are very independent, self-motivated and concerned with their own pleasure, especially in S/M porn where women are frequently on top. What bothers these people is not the image of women in porn, which is like that elsewhere in society, but its sexual explicitness; they are uncomfortable with sex.

Anti-porn activists also claim that porn, with its allegedly degrading view of women is responsible for the attitudes and actions of men towards women, and therefore is different from other forms of expression. But, as with other types of writing and pictures, porn generally shows what people want to see and are comfortable with; it doesn’t plant foreign ideas in people’s minds. And, even in the few cases where novel ideas are introduced to people in porn, they remain just that, ideas. Men do not rape or beat women because they see it in a movie. Sexism, rape, and beatings of women by their partners existed long before the widespread dissemination of modern pornography, and societies with little or no porn are no less sexist and violent than those where it is common.

The claim that men are made violent by porn, besides being inaccurate, is also based on a myth; that most pornography is violent. Most porn is composed of depictions of non-violent, consensual, mutually pleasurable sex. Some of it also contains S/M sex, which, while including the trappings of violence, and involving (apparent) pain, is also consensual and mutually pleasurable. There is certainly some porn which depicts rape or other coercive and violent sex, but it is a small portion of the porn produced and consumed. Moreover, like violent non-sexual movies and books, it is simply a depiction of a fantasy, made up by the author, or performed by consenting actors. Violent porn is no more real violence than are the Halloween movies.

And if anti-porn people are truly concerned about the violence and not the sex in porn, why is it that they protest only porn shops or destroy porn mags and video stores, while ignoring Friday the Thirteenth and horror magazines and books?

One aspect of the whole phenomenon of porn that is often left out of the discussion is that of homosexual porn. Much of the pornography produced today shows men having sex with men, with a growing proportion depicting woman-woman sex. The anti-porners tend to ignore homoporn because it gives the lie to many of their arguments. If depictions of inequitable sexual encounters between men and women are degrading to women, why aren’t similarly inequitable encounters between men and other men (which are very common in all-male porn, with its tops and bottoms) degrading to men? And if they are degrading to men, why isn’t such porn offensive to men, especially bottom men? And, if there is S/M imagery and (pretend) violence in this porn, why doesn’t this result in widespread violence against men, and even rapes of men?

A discussion of such issues never takes place, since most of the people who oppose heteroporn are unwilling to talk about, let alone criticize, queer porn because they do not want to risk being seen as ‘homophobic’ or otherwise politically incorrect. This is due to the fact that porn has often been seen, rightly, as liberatory by homosexualist men (and recently also by some homosexualist women), and is a much more open part of mainstream life for queer men that heteroporn is in straight society. Because of this ‘politicization’ of queer porn, any discussion of homoporn by the anti-porners, few of whom are homosexualist men, is likely to be criticized by gay liberationists as ‘anti-gay,’ and thus effectively suppressed. This is unfortunate, since such a discussion would show the fallacies in the anti-porn arguments.

Even though it seems odd that sexual liberationists and anarchists find porn offensive, it is certainly true that people have different tastes. Just because I like porn doesn’t mean that you should. But, if one finds something offensive, they should simply avoid it, and thereby avoid the offense. However anti-porners are not content with this strategy when it comes to porn. They feel that if it offends them, it must offend others, primarily women, and they take it upon themselves to protect others from it. Additionally, since they feel it leads otherwise non-violent, women-loving men onto the path of violence and sexism, they feel they need to prevent men from seeing porn as well.

As stated above, anti-porners differ on the strategy they employ to achieve these ends. While those who rely on argument and protest to influence others to avoid porn are preferable to the censors, their ideas about people should be problematic for those with an anarchist perspective. People are free agents who make choices and decisions based on what they observe, hear, and otherwise experience, and are responsible for the outcome of these choices. The libertarian way to deal with other free agents who choose to view or read materials of which one disapproves is to let them see these books or movies and then discuss the material with them and try to convince them of one’s point of view.

More objectionable to anarchists, however, are the anti-porn activists who are frankly censorious; While sharing the views of anti-porners who seek to protect others from porn, these people go a step further and use coercive force to achieve their ends. This is totally incompatible with the kind of voluntary society sought by most anarchists, and should be denounced by all freedom-lovers.

Pornography, like any other form of entertainment can be good or bad, based on the individual merits of any particular work. However, as a genre of literature or film, it is no better or worse or good or evil than any other. If porn is bad or sexist, the best strategy is to criticize it and discuss it with others, and/or make good, non-sexist porn, not suppress it. Sex and its depiction are a source of pleasure for many and our freedom to indulge in both should be defended, or at least tolerated, by anarchists. Censors, including those who claim to be anarchists, are the enemies of freedom, and anarchists who support them call into question their commitment to a free society.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

