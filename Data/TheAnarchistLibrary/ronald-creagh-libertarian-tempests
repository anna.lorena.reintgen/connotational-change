      Egypt between Cosmopolitanism and Subversion        Land of the Pharaohs      Origins of the Movements        Georges Hénein        Ramsès Younane (1913-1960)      The Egyptian Surrealist Group      Libertarian Trotskyism?        Art and Liberty        Towards the Arab World      The Post-War Period
"To define 'liberty' is to restrict its meaning, to explain it is to limit its scope; for the word 'liberty' only reveals its meaning when interpreted loosely.

"The farthest that the human mind could go to imagine how to liberate oneself from boundaries and borders is perhaps what anarchism has said in the phrase: 'Neither god nor master.'"

—Feisal 'abd al-Rahman Shahbander. [2]

In 1973, a group of Arab students launched the Surrealist Movement in Exile in Paris, London and Vienna. They reappropriated a rebel art:

"Our surrealism destroys the so-called 'Arab homeland' [...] We explode the mosques and the streets with the scandal of sex returning to the body, bursting with flame at each encounter."

And they encouraged blasphemy! They saw in it an eloquent, necessary act, "if only because it produces a delightful pleasure and opens the doors of the imagination." [3] Their journal, Libertarian Desire, [4] was immediately forbidden in most Arabic-speaking countries.

Among the articles published in the journal were some texts of a previous generation of Egyptian surrealists. In contrast to socialists and communists of other countries, the authors expressed many libertarian ideas. They were so independent that they did not recognize any "superior" authority. They were very active in Cairo beginning in 1937, and for more than twenty years. Their history and reflections are rich in lessons for a current Egyptian resurgence.

Their works have begun to receive international recognition; the Arabic texts are trenchant and their poems and stories in French constitute an important contribution to Francophone literature and art of the twentieth century.

The abundance, diversity and complexity of the works deserve a large study which would consider all their interconnections, well beyond what can be presented in this article.

We can only briefly review here what two major figures, Georges Hénein and Ramsès Younane did in Egypt. They were very different from one another and yet always very close. They offered a singular insight through their activities in French and Arabic circles.

On January 1, 1937, the young painter Kamel El Tamisany published a Manifesto of the Neo-Orientalists:

"If you see in my works a grave and mysterious expression, I will say even unusual, apart from any classical beauty, an expression which comes from that cursed side which exists in me and which is only the reflection of repressed Oriental feelings, that is where my discoveries reside." [5]

This insistence on a repressed "Oriental" inner self seems to correspond to different ideas, expressed in the first official presentation of surrealism on February 4, 1937. The talk was titled "Assessment of the Surrealist Movement" [6] and was presented by Georges Hénein, a young man of 23 who had just received his degree from the Sorbonne.

It was given to the group Les Essayistes, in Cairo, broadcast and then repeated with some variations in Alexandria on March 1, 1937.

Hénein would become a leader of a current which we understand today as aiming to revolutionize the collective imagination of Egypt.

Everything seemed unreal. The lecturer was the son of a high dignitary. His presentation was not in Arabic but in French. His remarks were surprising. He quoted André Breton, the founding figure of an extravagant artistic movement, surrealism, who he met a few months earlier. All this was so far from an Egyptian people struggling in extreme poverty.

The Egypt of the Pharaohs also seemed unreal. The discovery of the tomb of Tutankhamun in 1920 triggered egyptomania, especially in France. The arts and letters community would not sully the pleasure of visiting the country and making contact with its intellectual elites by references to the poverty.

An area under the influence of Europe since the dissolution of the Ottoman Empire in the nineteenth century was discovered. In the aftermath of the 1914-1918 war, Egypt, under a British protectorate, had demanded independence. London, which had promised to spare the country any war effort, had nevertheless requisitioned more than a million men, buildings, equipment and even crops. But then the British government rejected all negotiations and deported the leaders of the movement, causing civil disobedience of such magnitude that Great Britain was forced to unilaterally proclaim Egypt's independence on February 22, 1922. A month later, Sultan Fouad I proclaimed himself king of the country. But the British troops still occupied the territory and monitored it to discourage any moves for real independence.

Nevertheless, the situation changed: the partisans of independence formed a large popular party, the WAFD, which was triumphantly elected in 1924. It brought together for the first time Coptic Christians and Muslims and its green flag bore both the crescent And the cross!

The Nahda, "Renaissance of Modern Arabic" in Egypt, contained contradictory currents wishing to reform a society dominated by an elite of large landowners who controlled the parliament and directed the development of a national art.

Students who traveled to France in 1920 visited the studio of their compatriot, the sculptor Mahmoud Muktar. They discovered a statue he created, entitled The Revival of Egypt. This sculpture was transported to Cairo, where it became a symbolic monument. Egyptian circles, in search of an identity, rediscovered the Pharaohs while their vanguard worked on developing a nationalist aesthetic. The Egyptian state forcefully promoted an official and ritual national art. [7]

The policy encouraged the establishment of communities of people from the countries bordering the Mediterranean: Ottomans, Syro-Lebanese, Armenians, Jews, Cypriots, Maltese, Greeks, Italians, French, and English. They found employment in offices, were involved in commerce, banks and especially the lucrative management of the Suez Canal. Their dominant position became clear: by the 1920s, many had been there for several generations.

The French language played a major role. It afforded a distance from the British occupiers, and the leaders of the country. They, however, were busy developing urban planning, education and public health. Whole districts of Cairo, Alexandria and Port Said were Europeanized. But the majority of Egyptians lived in urban slums or in the Nile valley.

Hénein's father was a comrade of the founder of the WAFD party. And George was active in French-speaking circles. He introduced the surrealist movement into the country and became the most influential figure.

"Yes we are deniers and we are heretics

To us it is violence that will destroy our masters!

We've been saying yes to them.

Now it's time to tell them SHIT!"

— Georges Hénein [8]

Georges Hénein was born in Cairo in 1914, "amidst a motley assortment of roast meat street vendors, wool carders, and beggars, in the Qobeissi district, between the predominantly Jewish and Christian neighborhoods of Ghamrah and Daher." [9] Since he was of Egyptian stock, it should not be surprising that his name "Georges" is pronounced in Arabic "Djurdj

Hunayn." He undoubtedly received the thirty-six unctions of the Coptic baptism.

His father was appointed ambassador to Italy, then to Spain, and he brought his wife and child to Europe. He then occupied important posts in various ministries and was awarded the title of "pacha." His mother, born Marie Zanelli, was an Italian raised in a Catholic family. She was an antifascist and atheist. She left her first husband, who was also Egyptian. She spoke impeccable French, "with a lot of passion," according to a friend of the family.

Georges was taken from one capital to another as his father was appointed to various posts. At 16 he began study at the Lycée Pasteur in Neuilly where he received his two diplomas. One of his teachers was the writer Daniel-Rops, who was highly regarded in Catholic circles. But Georges had become an atheist. He identified with surrealism, and sent a letter to André Breton. Breton, influenced by his reading of Freud, was aiming to subvert received values, including rationalism, by utilizing all of the mind's powers, even the unconscious. He issued his first manifesto in 1924 to create a literary, artistic and revolutionary current.

Why was George interested in Surrealism? Yves Bonnefoy, who knew him, wrote superbly that it was a matter of seeking "to understand why Rimbaud's 'change life' and Marx's 'transform the world' had to be joined together, in order to avoid two irreparable disasters." [10] In short, it was necessary to provoke this improbable marriage between poetic work and political thought. For Georges was a poet.

In Paris, the young man became friends with Breton. He maintained a correspondence with the great man, after his return to Egypt. He always treated Breton with deference, though later distancing himself.

Georges Hénein was not typical. Born in a Coptic environment, he defined himself as anti-Christian. His companion was Ikbal El-Alaily, granddaughter of the great national poet Ahmed Chawky and daughter of pious and moderate Muslims, cultured people. Both families opposed their marriage; it took place much later in France, in 1955.

Hénein's early writings were harsh in tone. They lay bare his own milieu, that of the ruling class, including public art. His "Dictionary for the use of the bourgeois world" [11] defined the museum as the

"glorification of a large pile of official garbage, an allusion to the painter Muhammad Nagi, who was recognized in official circles, which conceived the museum as a forum in the service of a nationalist construction." [12]

Hénein treated everyone with equal consideration. Despite his outbursts of temper, he attracted people. Others joined, notably Ramsès Younane, who was a fluent Arabic speaker. Hénein revised the writings of this fellow traveler, and Younane corrected his friend's Arabic. [13]

Ramsès Younane was born in Minyah, Upper Egypt, to a very poor family in the majority Coptic milieu of the city. When he was 15 years old, his father died and he had to go to work to feed his four younger brothers. He nevertheless succeeded in entering the Cairo School of Fine Arts and graduated in 1933. As a painter, translator and thinker, he was convinced, as Wilhelm Reich was later, that a people's transformation occurs through the imagination. He earned his living as an itinerant high school drawing teacher in several towns. He nevertheless participated every year in Cairo art exhibitions. Since he was friends with Hénein, they worked in close collaboration.

"Art is a powder magazine"

—Nicolas Calas [14]

A surrealist collective was formed in 1937. They met every day at a cafe. Its founders were Hénein and Younane, Kamel El Telmisany, a painter and essayist, and the brothers Fouad and Anwar Kamel. They were joined by Marie Cavadia (1901-1970), who held a progressive salon, the brothers Henri and Raoul Curiel, and the novelist Albert Cossery. [15]

They weren't all surrealists, far from it. Some became famous in other artistic movements. As for the founders, they worked together, and their surrealist ideas were always listened to even if they were not shared. They also had the support of Ikbal El-Alaili, who had her own literary activities.

A few years later, they met again almost every evening, and when the women went home, the men put revolutionary slogans on the walls of the police stations.

There were always newcomers. In 1944, for example, Hénein became friends with Arturo Schwarz from Alexandria, one of the leaders of the illegal Egyptian Trotskyist party. Schwarz was arrested and imprisoned until autumn 1945. He was imprisoned a second time later and expelled to Italy in 1949. [16]

1938 was a black year for Egypt. The agrarian crisis, government corruption, the failure of the WAFD to free itself from the British influence, prompted the emergence of groups of young fascists and royalists, the "blue shirts." Moreover, the conflict between nationalists and monarchists created another paramilitary Fascist organization, Misr Al-Fattah (Young Egypt), to counter the Muslim Brotherhood, which was already present in the country. Some of the new generation of foreign residents were interested in the Soviet revolution and communism.

Hénein was sickened by the Moscow trials, a virulent anti-Stalinist, and a passionate defender of the Spanish revolution. But he was also appalled by the degradation of culture in the Axis countries, and so he signed the manifesto that Trotsky wrote with Breton.

The text "For an Independent Revolutionary Art," signed in mid-July 1938 by Diego Rivera and Breton, proposed the creation of an International Federation of Independent Revolutionary Art (FIARI). It defined itself thus:

"If, for the development of the material productive forces, the revolution must erect a centralized socialist system, for intellectual creation it must from the very beginning establish and ensure an anarchist form of individual freedom. There must be no authority, no constraints, not the least trace of command! The various associations of scholars and the collective groups of artists who will work to take on the great tasks, can arise and carry out fruitful work only on the basis of creative free friendship, without any constraint from the outside."

And further on:

"The Marxists can walk here hand in hand with the anarchists ..." [17]

The document contained no reference to Revolutionary Guards who would watch the artists. [18]

Events pushed Hénein towards a more concrete commitment in politics. In March 1938, he created a scandal at a presentation in Cairo by the Italian futurist Marinetti. Hénein denounced works which were supposedly artistic but which were only created to serve fascism. This denunciation was a scandal. Political differences emerged among listeners, a fairly mundane group which until then shared a common attraction for culture.

Then a leaflet in Arabic and French was circulated. [19] It denounced the Nazi offensive against modern art. It was also an attack on the cultivated Egyptians who condemned "any literary and artistic creation more or less directly threatening intellectual disciplines and moral values." It was the first episode of a struggle with contradictory objectives, literary autonomy, but articulated in politics, a universalist will, but rooted in the local mother tongue. Thirty-seven artists, writers, journalists and lawyers from Egypt signed the manifesto.

In January 1939 the Art and Liberty [Jamaat al-Fann wa al-Hurriyya] association was created. Inspired by the Mexico manifesto, it adhered to the International Federation of Independent Revolutionary Art (FIARI). This political collective did not identify itself as surrealist, although Hénein and Younane were influential in the group.

According to a historian of the movement, Don LaCoss, this rallying to the emerging Trotskyist movement reflected support for anti-Stalinism, recognition of Trotsky's role in Russia's withdrawal from the First World War, but not necessarily support for its crimes against the anarchists. The members, Marxists, Trotskyists and others were completely independent. [20] Hénein was not an anarchist, but he was surely a libertarian. [21]

Like the group of surrealists, the association transgressed the religious divisions so important in the Middle East. It brought together Sunni Muslims and Shiites, Jews, Copts and Protestants. This cocktail of Jews and Christians, generally atheists, free-thinking Muslims and European anarchists, present in Cairo, Alexandria and also in Beirut, constituted a hybrid anarcho-socialism, crossing national boundaries of stagnant Established opinion. It reappeared several times in Egypt before Nasser's takeover.

The members of the association were diverse, including the Arab teacher Ramsès Younane, the Italian portraitist Angelo de Riz (an anarchist refugee in Cairo), and the Jewish poet Edmond Jabès. They also had different professions. Kamel El-Telmisany was a painter as well as a filmmaker; Émile Simon was a journalist ...

The group departed from Marxist orthodoxy in the recruitment of members and in its writings: it discussed such figures as Tolstoy. In short, it was a Trotskyist-libertarian network independent of any party. It was neither a Paris branch nor a group serving a cause. Each member retained his individuality.

The newsletter, Art and Liberty, dealt with anti-fascism, anti-imperialism, the reform of radical education and Freudian theory. It aimed at broader objectives than those of FIARI, notably the emancipation of women, a subject that frequently came up. The criticism of the state was fierce. Hénein went so far as to declare:

"There is no abstract democracy, but there is the democracy granted or withdrawn or limited, according as the use which is made of it directly threatens the interests or not of some material powers. There are only differences of degree, not nature, of intensity, not kind between the democratic and the fascist regimes." [22]

This was not far from Karl Marx's idea that the suppression of capitalist relations of production will lead to a society without a state.

Only the non-French section of the FIARI, Art and Liberty, lasted longer than this group, which disappeared in 1939 because of the Second World War. The French publications discussed international events, but in connection with specifically Egyptian problems. The influence of surrealism was obvious. Social unrest was brought to artistic and political circles.

The Art and Liberty newsletter put out only two issues. It was followed on December 6, 1939 by the weekly Don Quixote. Hénein created this magazine with his friend Henri Curiel, an unconditional admirer of the Soviet Union. [23] The two comrades used their fortunes to finance literary and socialist magazines. Nineteen issues were published until March 29, 1940. [24] Another journal followed, La Part du Sable, with four issues, published in 1947, 1950, 1954 and 1955. The group was spied on by the Egyptian and British authorities. It had crossed a red line, having very early become oriented towards the Arabic-speaking population.

As usual, the guardians of Arab Orthodoxy endeavored to enclose the association in an intellectual ghetto. The Arab newspaper al-Risala denigrated one of the group's texts, "Long Live Degenerate Art" ("Vive l'art degénéré").

El-Telmisany, among others, Responded:

"Art does not belong to any country, my friend ... Do you doubt that national art movements can circulate easily from one country to another? ... We want a culture that is in harmony with the Rest of the world." [25]

This friendly gesture towards a stubborn opponent was an invitation to cross the national barriers and also defy the alternative East-West dichotomy. El-Telmisany saw surrealism in Asia or even in popular Egyptian works such as sugar dolls that have four hands and some stories of folklore. He saw Freudian elements in some works of the great Arab painters and writers. According to El-Telmisany, surrealism is a scientific word that can be used to designate every work of the imagination that is free in its expression and style.

Younane wrote that Surrealism is neither artistic nor political, nor a mixture of the two. It is all this but also philosophical, psychological and spiritual, for it draws its inspiration from Rimbaud and Baudelaire. It is a liberating movement, but it can only be achieved with a social revolution that will introduce economic equality.

The controversy continued for a month in al-Risala, ensuring publicity for the association in the Arab world. [26]

The association published attacks on the French-speaking bourgeoisie—necessarily indirect to evade the censorship—which were aimed at appealing to Arabic cultural circles. The group waged a double battle, one against the official institutions which affected art in the service of the State; the other to agitate among the Arabic-speaking middle strata.

The Egyptian world of art, turned towards the cult of the state, received the slap of surrealism. The Art and Liberty Association organized five annual exhibitions, with the participation of painters and artists from other Arab countries as well as an impressive number of women whose creations were highlighted, including paintings and photographs. It was indeed a committed art: even exhibitions ought to change the world. [27]

In the context of Egypt, the exhibitions were seen as especially audacious because Sunni Islam condemns the reproduction of images. The works of art clearly showed images of the bodies of creatures. The exhibitions were very controversial but they were not prohibited. During the war the British tolerated Art and Liberty because its antifascist propaganda in Arabic opposed that of the national liberation movements, the Islamist Nationalist Party and the sections of the Muslim Brotherhood, all suspected of spying for the Germans. [28]

Ramsès Younane was the first to popularize the ideas of surrealism in the Arabic-speaking world through an introduction to a history of European painting, Le Dessein de l'artiste contemporain, in 1938. [29]

We must also mention the Egyptian fellah. Art and Liberty launched an Arab socialist monthly, al-Tatawwur [Evolution] which appeared from January to July 1940. [30] Anwar Kamil, the editor, was also a surrealist. The journal discussed poverty and fascism, offered a critical reading of culture and blamed the government, which resulted in it being banned and Kamil imprisoned. In 1942, the Surrealists launched another newspaper, al-Majalla al-Kifah al-Ijtima'i, which was also banned by the government in 1944. The editor, Younane, was imprisoned and then deported to France.

The group did not limit its activities to journalism. It organized conferences, debates, screenings of films, and engaged in activities in the Arab quarters of the city. In the supposedly dangerous areas of Cairo, it ran a popular university, including courses on art, from which quality artists emerged. [31]

After Greece was liberated from the Nazis, civil war broke out. The British government worked to crush anti-fascist resistance and supported the most reactionary Greek groups. British troops stationed in Egypt refused to go to fight in Greece and went on strike. Leaflets were dropped from an airplane urging them to surrender. The Egyptian surrealists and other non-Stalinist socialists dropped butterflies congratulating the soldiers for their courage and inviting them to maintain their position.

In 1944 the British and Egyptian authorities began serious repression. But the movement continued to be defiant.

The prestige of France had been much diminished because of the rapid surrender to the Nazis at the beginning of the war. But in 1945, Egyptians praised the Resistance. Georges Hénein wrote that "the poetry of resistance" is "a very recent form of resistance to poetry." [32]

That same year, Ikbal El-Alaily published an anthology of the German romantics which showed that Germany also had aspects beyond its military. She was arrested for obstructing the war effort, and questioned by British military intelligence and the Egyptian security services. El-Alaily was ordered to break with the Art and Liberty group and the surrealists, or be sent to forced labor in the notorious quarries of Tura. [33]

In February 1946, the Surrealists joined the movement against the British military occupation of Egypt. Three thousand students from Alexandria went into the neighborhoods where some thirty thousand textile workers lived. The police shot at them, killing five people. Sixteen members of the Surrealist group were imprisoned. Over time, some went into exile.

Hénein, who still resided in Egypt, broke with the Fourth International. His friend Benjamin Péret also did so, and moved to Paris, where many surrealists joined the anarchist current. [34]

Ramsès Younane moved back to Egypt in 1956. He was editorial secretary at the RTF, the official French radio and TV broadcasting system. During the French and British intervention in the Suez crisis, he refused to transmit a statement attacking Egypt. He met Georges Hénein who, in the 1950s, still frequented Marie Cavadia's salon. There a new generation of poets gathered to meet Hénein, who they called the "oracle" of this literary circle.

Before being forced to leave Egypt in 1960, Hénein published a translation of a poem by Stefano Terra in honor of Malatesta, illustrated with an automatic drawing by Younane. [35] It is worth recalling that the famous Italian anarchist fought in Egypt against the British troops in the 1880s.

It is obvious that Egyptian Surrealism was neither a direct product of European colonization in Egypt nor a spontaneous generation. It was an integral part of the history of a country that is an international crossroads.

But the debate opened by Hénein, engaging art in political protest, was repeatedly reduced to isolation, threatened and banned, not only by Egyptian authorities but also by the democratic government of Great Britain. Whether dictatorships or democracies, states do not tolerate men and women who challenge their raison d'être. Was Georges Henein right to write that between democracies and dictatorships there was only a difference of degree, and not of kind?

With his comrades, Ramsès Younane questioned the practice of exhibitions, in particular by criticizing the omission of female artists. A pioneer in art criticism [36], his sociopolitical perspective excited Arabic-speaking circles in Egypt, and his art contributed to the development of surrealism in Lebanon, Syria and Iraq. [37]
[1] The author wishes to thank Sylvie Younane and Catherine Farhi, Alain Farhi, Marcel Berthier, Guy Malouvier and Patrick Ramseyer, as well as D. Haas, who was an indefatigable editor
[2] "Limites et Frontières", (Limits and Frontiers), in al-Tatawwur [Development], 1940. [Translation into French by Ronald Creagh.]
[3] Don LaCoss, "On Blasphemy and Imagination: Arab Surrealism Against Islam" 2010. http://theanarchistlibrary.org/library/don-lacoss-on-blasphemy-and-imagination-arab-surrealism-against-islam (accessed 7/3/2015).
[4] ar-Raghba al-ibahiyya (Libertarian Desire), 1975-? The cover of one issue was illustrated by our friend André Bernard.
[5] Daniel Lançon, Le Caire (1934-1941): le défi des avant gardes européennes pour les écrivains égyptiens et pour Georges Hénein en particulier" (Cairo, 1934-1941: the challenge of the European avant-garde for Egyptian writers and for Georges Hénein in particular) in Edith Kunz, Thomas Hunkeler, Les Métropoles des avant-gardes, Genève, Peter Lang, 2009, pp. 163-174. Citation p. 2.
[6] Broadcast by Egyptian State Broadcasting and published under the same title in Revue des conférences françaises en Orient, Cairo, [s.n.], No. 8, October 1937.
[7] Alexandra Dika Seggerman, "Al-Tatawwur (Evolution): An Enhanced Timeline of Egyptian Surrealism," Dada/Surrealism, 19 (2013), no. 1.
[8] "Le chant des violents, G. Hénein, Ouvres, Poèmes, récits, essais, articles et pamphlets, (The song of the violent , G. Hénein, Works, Poems, stories, essays, articles and pamphlets), Denoël, 2006, p. 44
[9] Berto Farhi, "Avant partir" (Before leaving), in Georges Hénein, op cit, p.20
[10] Georges Hénein, op. Cit., p. 14.
[11] "Fragments du Petit Larousse illustré" (Fragments from the Petit Larousse illustré) (Dictionary for the use of the bourgeois world)", Un Effort, no. 51, February 1935.
[12] Patrick Kane, "Art Education and the Emergence of Radical Art Movements in Egypt: The Surrealists and the Contemporary Arts Group, 1938–1951, Journal of Aesthetic Education, Vol. 44, No. 4 (Winter 2010), pp. 95-119, passim.
[13] Information provided by Ms. Sylvie Younane (private conversation, 2014).
[14] Nicolas Calas, quoted by Georges Hénein, op. cit., p. 399.
[15] Don LaCoss, "Egyptian Surrealism and Degenerate Art in 1939", The Arab Studies Journal, vol. XVIII, No. 1, 1 April 2010, p. 78-117.
[16] Marie-Francine Desvaux-Mansour, "Le Surréalisme à travers Joyce Mansour. Peinture et Poésie, le miroir du désir" (Surrealism through Joyce Mansour: Painting and Poetry, the mirror of desire", Th. University of Paris 9 Panthéon-Sorbonne, 5 April 2014, vol. I, p. 42.
[17] "Pour un art révolutionnaire indépendant" (For an independent revolutionary art), online: <http://sl.e-monsite.com/2010/06/11/76925279artrevo-pdf.pdf> (accessed March 13, 2015) Cf. : Arturo Schwarz, André Breton, Trotsky et l’anarchie (André Breton, Trotsky and Anarchy), edited by Amaryllis Vassilikioti, Paris, Union Générale d'Editions, 1977.
[18] Pietro Ferrua, "Surréalisme et anarchisme. La collaboration des surréalistes au Libertaire, organe de la Fédération anarchiste" (Surrealism and Anarchism: The Collaboration of the Surrealists in Libertaire, Organ of the Anarchist Federation," in Art and Anarchy: Proceedings of the Colloquium Ten Years of Radio Libertaire, Paris, May 1991, Via Valeriano & La Vache folle, 1993, 49-67.
[19] Vive l’art dégénéré (Long live degenerate art) [Le Caire], [s.n.], 1938. Long live degenerate art. [Cairo], [s.n.], 1938. This manifesto is printed on the back of a reproduction of Picasso's "Guernica." The text, probably written by Hénein, is dated December 22, 1938. The French version is published in the Revue des conférences françaises en Orient, Cairo, [s.n.], 2nd year, January 1939.
[20] Don LaCoss, "Art and Liberty: Surrealism in Egypt" in Communicating Vessels no. 21, Autumn-Winter, 2009-2010, p. 28-33 [Portland: Mutual Aid].
[21] In a letter of 1947 he said he was "even inclined to growing sympathy for anarchists, whose attitude, despite all sorts of naivete, is acceptable, consistent and honest." Letter No. 71, February 22, 1947, in "Lettres Georges Henein - Henri Colet (1935-1956)," Grandes Largeurs, Paris, Le Tout sur le Tout, n. 2-3, Autumn-Winter 1981, p. 98
[22] "L'Art dans la mêlée" (Art in the Melee) Revue des conférences françaises en Orient, no. 24, 15 March 1939, p. 260-272. Citation p. 271. Thirty years later, Hénein added: "The ballot paper is conceived as the alms democracy pays to the citizen." D. Petite encyclopédie politique, Seuil, 1969, p. 9.
[23] Curiel was to play a major political role in the Egyptian Communist Party and then internationally. He was assassinated in Paris in 1978 by a member of Action française.
[24] Don LaCoss, "Degenerate Art", op. cit.
[25] Kamil el-Telmisany, “Hawla al-Fann al-Manhut,” al-Risala, 28 août 1939. Cité par Don LaCoss, op. cit. [my translation]
[26] Don LaCoss, "Degenerate Art", op. cit.
[27] Sam Bardaouil, "'Dirty, Dark, Loud and Hysteric': The London and Paris Surrealist Exhibitions of the 1930s and the Exhibition Practices of the Art and Liberty Group in Cairo," Dada/Surrealism, no. 19, 1 (2013); Don LaCoss, "Surrealism", op. cit.
[28] Don LaCoss, "Surrealism", op. cit.
[29] Ghayat al-Rassam al-Asri, Cairo, 1938. As of this writing I have found no trace of this work.
[30] Don LaCoss, "Degenerate Art", op. cit.
[31] On some artistic achievements in the suburbs, see, for example, regarding the Sayyidda Zayabb district, Patrick M. Kane, "Politics, Discontent and the Everyday in Egyptian Arts, 1938-1966," Ann Arbor, UMI Dissertation Service, 2007 , p. 120 et seq.
[32] Daniel Lançon. "La France des Égyptiens" (France of the Egyptians), in Michel Schmitt, Marie-Odile Andrée, La France des écrivains: éclats d'un mythe (1945-2005), Paris, Presses Sorbonne Nouvelle, 2010, p. 27-40. Cit. p. 9.
[33] Don LaCoss, "Degenerate Art", op. cit.
[34] Don LaCoss, "Surrealism", op. cit.; Cf.: Benjamin Péret, "Haute fréquence" (High Frequency), May 24, 1951, Le Libertaire, June 7, 1951.
[35] Don LaCoss, a Surrealism", op. cit.
[36] Cf. Andrea Flores, "The Myth of the False: Ramsès Younan's Post-Structuralism avant la lettre," The Arab Studies Journal, vol. 8/9 No. 2/1 (Fall 2000/Spring 2001) pp. 97 ff.
[37] Narjess D'Outre-ligne, "Surréalisme en Orient," (Surrealism in the East) Arabica, I, 2.<www.brill>




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

