      Amédée Dunois: Anarchism and Organization      Errico Malatesta: Anarchism, Individualism and Organization      Emma Goldman      Max Baginski
It is not long since our comrades were almost unanimous in their clear hostility towards any idea of organization. The question we are dealing with today would, then, have raised endless protests from them, and its supporters would have been vehemently accused of a hidden agenda and authoritarianism.

They were times when anarchists, isolated from each other and even more so from the working class, seemed to have lost all social feeling; in which anarchists, with their unceasing appeals for the spiritual liberation of the individual, were seen as the supreme manifestation of the old individualism of the great bourgeois theoreticians of the past.

Individual actions and individual initiative were thought to suffice for everything; and they applauded [Ibsen’s play] “An Enemy of the People” when it declared that a man alone is the most powerful of all. But they did not think of one thing: that Ibsen’s concept was never that of a revolutionary, in the sense that we give this word, but of a moralist primarily concerned with establishing a new moral elite within the very breast of the old society.

In past years, generally speaking, little attention was paid to studying the concrete matters of economic life, of the various phenomena of production and exchange, and some of our people, whose race has not yet disappeared, went so far as to deny the existence of that basic phenomenon — the class struggle — to the point of no longer distinguishing in the present society, in the manner of the pure democrats, anything except differences of opinion, which anarchist propaganda had to prepare individuals for, as a way of training them for theoretical discussion.

In its origins, anarchism was nothing more than a concrete protest against opportunist tendencies and social democracy’s authoritarian way of acting; and in this regard it can be said to have carried out a useful function in the social movement of the past twenty-five years. If socialism as a whole, as a revolutionary idea, has survived the progressive bourgeoisification of social democracy, it is undoubtedly due to the anarchists.

Why have anarchists not been content to support the principle of socialism and federalism against the bare-faced deviations of the [social democratic] cavaliers of the conquest of political power? Why has time brought them to the ambition of re-building a whole new ideology all over again, faced with parliamentary and reformist socialism?

We cannot but recognize it: this ideological attempt was not always an easy one. More often than not we have limited ourselves to consigning to the flames that which social democracy worshipped, and to worshipping that which burned. That is how unwittingly and without even realizing it, so many anarchists were able to lose sight of the essentially practical and working class nature of socialism in general and anarchism in particular, neither of which have ever been anything other than the theoretical expression of the spontaneous resistance of the workers against the oppression by the bourgeois regime. It happened to the anarchists as it happened to German philosophical socialism before 1848 — as we can read in the [Marx & Engels’] Communist Manifesto — which prided itself on being able to remain “in contempt of all class struggles,” defending “not the interests of the proletariat, but the interests of Human Nature, of Man in general, who belongs to no class, has no reality, who exists only in the misty realm of philosophical fantasy”.

Thus, many of our people came back curiously towards idealism on the one hand and individualism on the other. And there was renewed interest in the old 1848 themes of justice, liberty, brotherhood and the emancipatory omnipotence of the Idea of the world. At the same time the Individual was exalted, in the English manner, against the State and any form of organization came, more or less openly, to be viewed as a form of oppression and mental exploitation.

Certainly, this state of mind was never absolutely unanimous. But that does not take away from the fact that it is responsible, for the most part, for the absence of an organized, coherent anarchist movement. The exaggerated fear of alienating our own free wills at the hands of some new collective body stopped us above all from uniting.

It is true that there existed among us “social study groups”, but we know how ephemeral and precarious they were: born out of individual caprice, these groups were destined to disappear with it; those who made them up did not feel united enough, and the first difficulty they encountered caused them to split up. Furthermore, these groups do not seem to have ever had a clear notion of their goal. Now, the goal of an organization is at one and the same time thought and action. In my experience, however, those groups did not act at all: they disputed. And many reproached them for building all those little chapels, those talking shops.

What lies at the root of the fact that anarchist opinion now seems to be changing with regard to the question of organization?

There are two reasons for this:

The first is the example from abroad. There are small permanent organizations in England, Holland, Germany, Bohemia, Romandie and Italy which have been operating for several years now, without the anarchist idea having visibly suffered for this. It is true that in France we do not have a great deal of information on the constitution and life of these organizations; it would be desirable to investigate this.

The second cause is much more important. It consists of the decisive evolution that the minds and practical habits of anarchists have been undergoing more or less everywhere for the last seven years or so, which has led them to join the workers’ movement actively and participate in the people’s lives.

In a word, we have overcome the gap between the pure idea, which can so easily turn into dogma, and real life.

The basic result of this has been that we have become less and less interested in the sociological abstractions of yore and more and more interested in the practical movement, in action. Proof is the great importance that revolutionary syndicalism and anti-militarism, for example, have acquired for us in recent years.

Another result of our participation in the movement, also very important, has been that theoretical anarchism itself has gradually sharpened itself and become alive through contact with real life, that eternal fountain of thought. Anarchism in our eyes is no longer a general conception of the world, an ideal for existence, a rebellion of the spirit against everything that is foul, impure and beastly in life; it is also and above all a revolutionary theory, a concrete programme of destruction and social re-organization. Revolutionary anarchism — and I emphasize the word “revolutionary” — essentially seeks to participate in the spontaneous movement of the masses, working towards what Kropotkin so neatly called the “Conquest of Bread”

Now, it is only from the point of view of revolutionary anarchism that the question of anarchist organization can be dealt with.

The enemies of organization today are of two sorts.

Firstly, there are those who are obstinately and systematically hostile to any sort of organization. They are the individualists. There can be found among them the idea popularized by Rousseau that society is evil, that it is always a limitation on the independence of the individual. The smallest amount of society possible, or no society at all: that is their dream, an absurd dream, a romantic dream that brings us back to the strangest follies of Rousseau’s literature.

Do we need to say and to demonstrate that anarchism is not individualism, then? Historically speaking, anarchism was born, through the development of socialism, in the congresses of the International, in other words, from the workers’ movement itself. And in fact, logically, anarchy means society organized without political authority. I said organized. On this point all the anarchists — Proudhon, Bakunin, those of the Jura Federation, Kropotkin — are in agreement. Far from treating organization and government as equal, Proudhon never ceased to emphasize their incompatibility: “The producer is incompatible with government,” he says in the General Idea of the Revolution in the 19th Century, “organization is opposed to government”.

Even Marx himself, whose disciples now seek to hide the anarchist side to his doctrine, defined anarchy thus: “All Socialists understand by Anarchy the following: that once the goal of the proletarian movement — the abolition of classes — is reached, the power of the State — which serves to maintain the large producing majority under the yoke of a small exploiting minority — disappears and the functions of government are transformed into simple administrative functions”. In other words, anarchy is not the negation of organization but only of the governing function of the power of the State.

No, anarchism is not individualist, but basically federalist. Federalism is essential to anarchism: it is in fact the very essence of anarchism. I would happily define anarchism as complete federalism, the universal extension of the idea of the free contract.

After all, I cannot see how an anarchist organization could damage the individual development of its members. No one would be forced to join, just as no one would be forced to leave once they had joined. So what is an anarchist federation? Several comrades from a particular region, Romandie for example, having established the impotence of isolated forces, of piecemeal action, agree one fine day to remain in continuing contact with each other, to unite their forces with the aim of working to spread communist, anarchist and revolutionary ideas and of participating in public events through their collective action. Do they thus create a new entity whose designated prey is the individual? By no means. They very simply, and for a precise goal, band together their ideas, their will and their forces, and from the resulting collective potentiality, each gains some advantage.

But we also have, as I said earlier, another sort of adversary. They are those who, despite being supporters of workers’ organizations founded on an identity of interests, prove to be hostile — or at least indifferent — to any organization based on an identity of aspirations, feelings and principles; they are, in a word, the [pure] syndicalists.

Let us examine their objections. The existence in France of a workers’ movement with a revolutionary and almost anarchist outlook is, in that country, currently the greatest obstacle that any attempt at anarchist organization risks foundering on — I do not wish to say being wrecked on. And this important historical fact imposes certain precautions on us, which do not affect, in my opinion, our comrades in other countries.

The workers’ movement today, the syndicalists observe, offers anarchists an almost unlimited field of action. Whereas idea-based groups, little sanctuaries into which only the initiated may enter, cannot hope to grow indefinitely, the workers’ organization, on the other hand, is a widely accessible association; it is not a temple whose doors are closed, but a public arena, a forum open to all workers without distinction of sex, race or ideology, and therefore perfectly adapted to encompassing the whole proletariat within its flexible and mobile ranks.

Now, the syndicalists continue, it is there in the workers’ unions that anarchists must be. The workers’ union is the living bud of the future society; it is the former which will pave the way for the latter. The error is made in staying within one’s own four walls, among the other initiates, chewing the same questions of doctrine over and over again, always moving within the same circle of ideas. We must not, under any pretext, separate ourselves form the people, for no matter how backward and limited the people may be, it is they, and not the ideologue, who are the indispensable driving force of every social revolution. Do we perhaps, like the social democrats, have any interests we wish to promote other than those of the great working mass? Party, sect or factional interests? Is it up to the people to come to us or is it we who must go to them, living their lives, earning their trust and stimulating them with both our words and our example into resistance, rebellion, revolution?

This is how the syndicalists talk. But I do not see how their objections have any value against our project to organize ourselves. On the contrary. I see clearly that if they had any value, it would also be against anarchism itself, as a doctrine that seeks to be distinct from syndicalism and refuses to allow itself to become absorbed into it.

Organized or not, anarchists (by which I mean those of our tendency, who do not arbitrarily separate anarchism from the proletariat) do not by any means expect that they are entitled to act in the role of ‘supreme saviours”, as the song goes. We willingly assign pride of place in the field of action to the workers’ movement, convinced as we have been for so long that the emancipation of the workers will be at the hands of those concerned or it will not be.

In other words, in our opinion the syndicate must not just have a purely corporative, trade function as the Guesdist socialists intend it, and with them some anarchists who cling to now outdated formulae. The time for pure corporativism is ended: this is a fact that could in principle be contrary to previous concepts, but which must be accepted with all its consequences. Yes, the corporative spirit is tending more and more towards becoming an anomaly, an anachronism, and is making room for the spirit of class. And this, mark my words, is not thanks to Griffuelhes, nor to Pouget — it is a result of action. In fact it is the needs of action that have obliged syndicalism to lift up its head and widen its conceptions. Nowadays the workers’ union is on the road to becoming for proletarians what the State is for the bourgeoisie: the political institution par excellence; an essential instrument in the struggle against capital, a weapon of defence or attack according to the situation.

Our task as anarchists, the most advanced, the boldest and the most uninhibited sector of the militant proletariat, is to stay constantly by its side, to fight the same battle among its ranks, to defend it against itself, not necessarily the least dangerous enemy. In other words, we want to provide this enormous moving mass that is the modern proletariat, I will not say with a philosophy and an ideal, something that could seem presumptuous, but with a goal and the means of action.

Far be it from us therefore the inept idea of wanting to isolate ourselves from the proletariat; that would be, we know only too well, to reduce ourselves to the impotence of proud ideologies, of abstractions empty of any ideal. Organized or not organized, then, the anarchists will remain true to their role of educators, stimulators and guides of the working masses. And if we are today of a mind to associate into groups in neighbourhoods, towns, regions or countries, and to federate these groups, it is above all in order to give our union action greater strength and continuity.

What is most often missing in those of us who fight within the world of labour, is the feeling of being supported. Social democratic syndicalists have behind them the constant organized power of the party from which they sometimes receive their watchwords and at all times their inspiration. Anarchist syndicalists on the other hand are abandoned unto themselves and, outside the union, do not have any real links between them or to their other comrades; they do not feel any support behind them and they receive no help. So, we wish to create this link, to provide this constant support; and I am personally convinced that our union activities cannot but benefit both in energy and in intelligence. And the stronger we are — and we will only become strong by organizing ourselves — the stronger will be the flow of ideas that we can send through the workers’ movement, which will thus become slowly impregnated with the anarchist spirit.

But will these groups of anarchist workers, which we would hope to see created in the near future, have no other role than to influence the great proletarian masses indirectly, by means of a militant elite, to drive them systematically into heroic resolutions, in a word to prepare the popular revolt? Will our groups have to limit themselves to perfecting the education of militants, to keep the revolutionary fever alive in them, to allow them to meet each other, to exchange ideas, to help each other at any time?

In other words, will they have their own action to carry out directly?

I believe so.

The social revolution, whether one imagines it in the guise of a general strike or an armed insurrection, can only be the work of the masses who must benefit from it. But every mass movement is accompanied by acts whose very nature — dare I say, whose technical nature — implies that they be carried out by a small number of people, the most perspicacious and daring sector of the mass movement. During the revolutionary period, in each neighbourhood, in each town, in each province, our anarchist groups will form many small fighting organizations, who will take those special, delicate measures which the large mass is almost always unable to do. It is clear that the groups should even now study and establish these insurrectional measures so as not to be, as has often happened, surprised by events.

Now for the principal, regular, continuous aim of our groups. It is (you will by now have guessed) anarchist propaganda. Yes, we will organize ourselves above all to spread our theoretical ideas, our methods of direct action and universal federalism.

Until today our propaganda has been made only or almost only on an individual basis. Individual propaganda has given notable results, above all in the heroic times when anarchists were compensating for the large number they needed with a fever of proselytism that recalled the primitive Christians. But is this continuing to happen? Experience obliges me to confess that it is not.

It seems that anarchism has been going through a sort of crisis in recent years, at least in France. The causes of this are clearly many and complex. It is not my task here to establish what they are, but I do wonder if the total lack of agreement and organization is not one of the causes of this crisis.

There are many anarchists in France. They are much divided on the question of theory, but even more so on practice. Everyone acts in his own way whenever he wants; in this way the individual efforts are dispersed and often exhausted, simply wasted. Anarchists can be found in more or less every sphere of action: in the workers’ unions, in the anti-militarist movement, among anti-clericalist free thinkers, in the popular universities, and so on, and so forth. What we are missing is a specifically anarchist movement, which can gather to it, on the economic and workers’ ground that is ours, all those forces that have been fighting in isolation up till now.

This specifically anarchist movement will spontaneously arise from our groups and from the federation of these groups. The might of joint action, of concerted action, will undoubtedly create it. I do not need to add that this organization will by no means expect to encompass all the picturesquely dispersed elements who describe themselves as followers of the anarchist ideal; there are, after all, those who would be totally inadmissible. It would be sufficient for the anarchist organization to group together, around a programme of concrete, practical action, all the comrades who accept our principles and who want to work with us, according to our methods.

Let me make it clear that I do not wish to go into specifics here. I am not dealing with the theoretical side of the organization. The name, form and programme of the organization to be created will be established separately and after reflection by the supporters of this organization.

I have listened attentively to everything that has been said before me on the problem of organization and I have the distinct impression that what separates us is the different meaning we give words. Let us not squabble over words. But as far as the basic problem is concerned, I am convinced that we are in total agreement.

All anarchists, whatever tendency they belong to, are individualists in some way or other. But the opposite is not true; not by any means. The individualists are thus divided into two distinct categories: one which claims the right to full development for all human individuality, their own and that of others; the other which only thinks about its own individuality and has absolutely no hesitation in sacrificing the individuality of others. The Tsar of all the Russias belongs to the latter category of individualists. We belong to the former.

Ibsen writes that the most powerful man in the world is the one who is most alone! Absolutely absurd! Doctor Stockmann himself, whom Ibsen has pronounce this maxim, was not even isolated in the full sense of the word; he lived in a constituted society, not on Robinson Crusoe’s island. Man “alone” cannot carry out even the smallest useful, productive task; and if someone needs a master above him it is exactly the man who lives in isolation. That which frees the individual, that which allows him to develop all his faculties, is not solitude, but association.

In order to be able to carry out work that is really useful, co-operation is indispensable, today more than ever. Without doubt, the association must allow its individual members full autonomy and the federation must respect this same autonomy for its groups. We are careful not to believe that the lack of organization is a guarantee of freedom. Everything goes to show that it is not.

An example: there are certain French newspapers whose pages are closed to all those whose ideas, style or simply person have the misfortune to be unwelcome in the eyes of the editors. The result is: the editors are invested with a personal power which limits the freedom of opinion and expression of comrades. The situation would be different if these newspapers belonged to all, instead of being the personal property of this or that individual: then all opinions could be freely debated.

There is much talk of authority, of authoritarianism. But we should be clear what we are speaking of here. We protest with all our heart against the authority embodied in the State, whose only purpose is to maintain the economic slavery within society, and we will never cease to rebel against it. But there does exist a simply moral authority that arises out of experience, intelligence and talent, and despite being anarchists there is no one among us who does not respect this authority.

It is wrong to present the “organizers”, the federalists, as authoritarians; but it is equally quite wrong to imagine the “anti-organizers”, the individualists, as having deliberately condemned themselves to isolation.

For me, I repeat, the dispute between individualists and organizers is a simple dispute over words, which does not hold up to careful examination of the facts. In the practical reality, what do we see? That the individualists are at times “organizers” for the reason that the latter too often limit themselves to preaching organization without practicing it. On the other hand, one can come across much more effective authoritarianism in those groups who noisily proclaim the “absolute freedom of the individual”, than in those that are commonly considered authoritarian because they have a bureau and take decisions.

In other words, everyone organizes themselves — organizers and anti-organizers. Only those who do little or nothing can live in isolation, contemplating. This is the truth; why not recognize it.

If proof be needed of what I say: in Italy all the comrades who are currently active in the struggle refer to my name, both the “individualists” and the “organizers”, and I believe that they are all right, as whatever their reciprocal differences may be, they all practice collective action nonetheless.

Enough of these verbal disputes; let us stick to action! Words divide and actions unite. It is time for all of us to work together in order to exert an effective influence on social events. It pains me to think that in order to free one of our own people from the clutches of the hangman it was necessary for us to turn to other parties instead of our own. Ferrer would not then owe his freedom to masons and bourgeois free thinkers if the anarchists, gathered together in a powerful and feared International, had been able to conduct themselves the worldwide protest against the criminal infamy of the Spanish government.

Let us ensure that the Anarchist International finally becomes a reality. To enable us to appeal quickly to all our comrades, to struggle against the reaction and to act, when the time is right, with revolutionary initiative, there must be an International!

I, too, am in favour of organization in principle. However, I fear that sooner or later this will fall into exclusivism.

Dunois has spoken against the excesses of individualism. But these excesses have nothing to do with true individualism, as the excesses of communism have nothing to do with real communism... I, too, will accept anarchist organization on just one condition: that it be based on the absolute respect for all individual initiatives and not obstruct their development or evolution.

The essential principle of anarchy is individual autonomy. The International will not be anarchist unless it wholly respects this principle.

An error that is too often made is believing that individualism rejects organization. The two terms are, on the contrary, inseparable. Individualism more specifically means working for inner mental liberation of the individual, while organization means association between conscious individuals with a goal to reach or an economic need to satisfy. We must not however forget that a revolutionary organization requires particularly energetic and conscious individuals.

The accusation that anarchy is destructive rather than constructive and that accordingly anarchy is opposed to organization is one of the many falsehoods spread by our adversaries. They confuse today’s institutions with organization and thus cannot understand how one can fight the former and favour the latter. The truth is, though, that the two are not identical.

The State is generally considered to be the highest form of organization. But is it really a true organization? Is it not rather an arbitrary institution cunningly imposed on the masses?

Industry, too, is considered an organization; yet nothing is further from the truth. Industry is piracy of the poor at the hands of the rich.

We are asked to believe that the army is an organization, but careful analysis will show that it is nothing less than a cruel instrument of blind force.

Public education: are not the universities and other scholastic institutions perhaps models of organization, which offer people fine opportunities to educate themselves? Far from it: schools, more than any other institution, are nothing more than barracks, where the human mind is trained and manipulated in order to be subjected to the various social and mental phantoms, and thus rendered capable of continuing this system of exploitation and oppression of ours.

Instead, organization as we understand it is something different. It is based on freedom. It is a natural, spontaneous grouping of energies to guarantee beneficial results to humanity.

It is the harmony of organic development that produces the variety of colours and forms, the combination that we so admire in a flower. In the same way, the organized activity of free human beings imbued with the spirit of solidarity will result in the perfection of social harmony, which we call anarchy. Indeed, only anarchy makes the non-authoritarian organization of common interests possible, since it abolishes the antagonism that exists between individuals and classes.

In the current situation, the antagonism of economic and social interests produces an unceasing war between social units and represents an insurmountable obstacle on the road to collective well-being.

There exists an erroneous conviction that organization does not encourage individual freedom and that, on the contrary, it causes a decay of individual personality. The reality is, however, that the true function of organization lies in personal development and growth.

Just as the cells of an animal, through reciprocal co-operation, express latent powers in the formation of the complete organism, so the individual reaches the highest level of his development through co-operation with other individuals.

An organization, in the true sense of the word, cannot be the product of a union of pure nothingness. It must be made up of self-conscious and intelligent persons. In fact, the sum of the possibilities and activities of an organization is represented by the expression of the single energies.

It follows logically that the greater the number of strong, self-conscious individuals in an organization, the lesser the danger of stagnation and the more intense its vital element.

Anarchism supports the possibility of organization without discipline, fear or punishment, without the pressure of poverty: a new social organism that will end the terrible struggle for the means of subsistence, the vicious struggle that damages man’s best qualities and continually widens the social abyss. In short, anarchism struggles for a form of social organization that will ensure well-being for all.

The embryo of this organization can be found in the type of syndicalism that has freed itself from centralization, bureaucracy and discipline, that encourages autonomous, direct action by its members.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

