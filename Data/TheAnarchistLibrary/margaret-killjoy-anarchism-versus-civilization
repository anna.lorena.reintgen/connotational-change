
In his 2003 polemic Anarchism versus Primitivism, Brian Oliver Sheppard makes the case that primitivism is inherently in contradiction with anarchism.

Much can be inferred from his tone, which is openly mocking. He makes references to how “[u]nfortunately for anarchists, plunging into the primitivist miasma has become necessary,” openly condescending to engage the primitivists at all. But his arguments are mired in absurdities: he mocks primitivists as hypocrites for engaging in technological practices while ignoring the fact that nearly every anarchist of any stripe in capitalist and statist society is not living as she or he preaches.

The core of his argument is that primitivism is authoritarian and therefore irreconcilable with anarchism. But the anarchism he promotes is rather clearly a simplistic and “classical” one, a red anarchism that argues for worker control of a stateless society. He argues that primitivists are stuck in an illusory past that cannot be supported by evidence, yet never acknowledges his complicity in the same behavior; here is a man arguing that anarchism has always been about worker control and communistic ideas, completely ignoring the heterogeneous past and present of anarchism. The individualists, the anarchists-without-adjectives, the mutualists... these people simply never existed, if one is to infer from Brian’s[1] piece.

Well-reasoned critiques of primitivism exist, but they are rarely distributed. Instead, self-defeating and remarkably sectarian missives are the norm. But this basic idea, that anarcho-primitivism is no more anarchist than the largely dismissed ideas of “anarcho”-capitalists and “anarcho”-nationalists, is a curious one.

For the sake of argument, I make the opposite case: anarchism is and always has been anti-civilization, and that civilization and anarchism are completely irreconcilable. Anyone who claims to be for civilization and anarchism both is deluding themselves.[2]

An anthropologist named Elman Service[3] suggested a widely-used system of classification for human cultures that contains four rough categories. Firstly, there are gatherer-hunter bands, which are generally egalitarian; secondly there are tribal societies that are larger, slightly more formal, and have bits of social ranking; third are chiefdoms, which continue down the path of social stratification; and finally there are civilizations, which are anthropologically understood by their complex social hierarchies and organized, institutional governments.

The rejection of complex social hierarchies and government means, therefore, the rejection of civilization. If an anarchist society were to develop, it would be by definition a non-civilized society.

Sure, an argument can be made that “classical” anarchists[4] are in opposition to the concept of the State rather than the idea of government per say, but the overwhelming majority of contemporary anarchist thought and dialogue speaks to the rejection of government as something that is inherently tied to the stateform.

So an anarchist society would necessitate either a return to the gatherer-hunter bands or it would — and I consider this option much more likely and much preferable, personally — mean developing something entirely new. I would personally like to call it the post-civilization, but I don’t believe we need to call it that. We simply need to understand it as anarchism.

Elman understood his four-part typology to be illustrative of a linear loss of autonomy. In a band, an individual had liberty. In a civilization, an individual ceded or lost this liberty. Now, Elman was an integration theorist; he believed that citizens in early civilizations gave up their autonomy willingly — in essence, that they signed the social contract, ceding their liberty so as to allow for a more complex society. The opposing theory is conflict theory: that states have, from the beginning, sought to consolidate power into the hands of the few for the benefit of those few.

But no one is arguing that the development from band to civilization hasn’t resulted in hierarchy and a lack of autonomy. This has, historically, been quite simple and linear: the further a society “advances” along these lines towards civilization, the more that liberty has waned.

Anarchism argues for a classless, egalitarian society devoid of coercive authority and therefore argues — and always has — against some of the primary, distinguishing traits that define civilization. To argue in favor of civilization is as absurd as to argue in favor of the state.

Very few modern anarchists would argue against anarcha-feminism. Anarcha-feminism is not understood as a separate thing, alien to anarchism as a whole, but rather as an essential component to the struggle against domination. It is generally understood that there are those who identify more strongly with anarcha-feminism than others. There are those who use it as their personal lens with which to address the world, who lay down important theory and practical organizing to address and overcome patriarchy.

And this, I would argue, is the role of the anti-civilized, the anarcho-primitivists. Anti-civilization thought has greatly deepened our understandings of oppression, with its critique of the division of labor and of linear concepts of progress.

It is as much of a mistake to reject all anarcho-primitivists as genocidal hypocrites as it is to reject all communist anarchists as technophiles who want the enslavement of nature in service of the almighty Worker[5].

Patriarchy, government, capitalism, nationalism, racism, civilization... none of it has a place in the society we envision. And more importantly, none of it has a place in our struggles, here and now.

 
[1] It is, of course, the norm to refer to a writer by their last name rather than their first name. This applies much more often to men than women; compare Kropotkin and Bakunin with Voltairine DeCleyre and Emma Goldman.
[2] Or simply use different semantic set and tend to define things differently than I, or this article, do.
[3] Elman Service, by the way, for some red-anarcho cred, was an American volunteer in the Abraham Lincoln Brigade of the Spanish Civil War, fighting against Franco and the fascists.
[4] The word “classical” is getting the quotes treatment in this article because I personally disapprove of this oversimplification of “what anarchists have always wanted” that is presented to us by Brian Sheppard as much as I disapprove of the oversimplification of what “primitive people were like” that indeed many primitivists are guilty of.
[5] Of course, it would be easier for me to not make this mistake myself if I didn’t personally know more than few people who fit these rude stereotypes...




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

