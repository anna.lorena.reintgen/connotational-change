
This statement was issued by some anarchist from Turin, Italy about a month before the G8 summit in Genoa

By now, it is a matter of fact. The world is on the verge of being transformed into a single enormous supermarket. From San Francisco to Calcutta, from Rio de Janeiro to Moscow, we will all get in line to consume the same identical products of unnatural, gaudy appearance. That which forms an authentic wealth to safeguard for many — autonomy and difference — could be swept away forever by the imposition of an economic policy and the consequent social system. When we are presented with a single possibility while every alternative is kept from us by force, we cannot speak of freedom of choice in the face of an offer, but only of coerced obedience. The continuing production of our days on earth (with all their pleasures, tastes and hues), when a single model of life to which we are to conform is imposed on it, is the totalitarian abyss that many see opening before them. Briefly, neoliberalism is the name given to the particular economic policy that the Masters of the earth are applying. Globalization is the name given to the process of homogenizing unification that it entails. Over the past several months, hundreds of thousands of people have taken to the streets against neoliberalism and globalization. On the occasion of meetings between the political and economic leaders of the most powerful states (in Seattle, Davos, Washington D.C., Melbourne, Prague, Gothenburg,...), protest demonstrations have been organized that have claimed the attention of the entire mass media. The next occasion is to be in Genoa at the end of July, corresponding to the G8 summit. But if, two years ago, this protest movement could close its eyes to certain contradictions within it so as to avoid putting a brake on the initial momentum, it seems to us that reflection on its significance is becoming increasingly urgent and admits no delay.

Neoliberalism supports a kind of capitalism without frontiers. The most powerful multinationals (mostly US capital) thus succeed in imposing their interests even when these go against the “national good” of the little states. Intolerable, right? But what are the opponents of neoliberalism fighting against? Logically, the most extreme would have to answer “against capitalism”, while the less extreme would have to say, “against capitalism without frontiers”. The former, as enemies of a world based on profit — no matter who benefits from it or within what border the exploitation occurs — the latter as enemies of a world based on the profit (of the ruling class) of the richest countries at the expense of the profit (of the ruling class) of the power countries. But whoever merely protests against the limitless global expansion of capitalism, against its lack of respect for borders, in substance shows themselves to be in favor of a form of local capitalism, even if ideal controlled from the bottom. Therefore, within the movement against neoliberalism and globalization two spirits live together, which for linguistic convenience we have differentiated as the “more extreme” — who want the elimination of capitalism and declare themselves against all governments and their representatives from whom they have nothing to demand — and the “less extreme” — who support or at least end up accepting the necessity of capitalism with a human face, limited and regulated by a democratic government, and whose intention is to explain their reasons to the current rulers. Not a small difference. But then, how and why did they come to find a point of agreement? For convenience, above all. Alliances draw together to gain strength. But it would be foolish to believe that in an alliance the sides in play are all situated on the same level. There is always a stronger side and a weaker side. And naturally, it is the stronger side that dictates the conditions of an alliance, decrees its slogans, determines its movements, derives the greatest advantage from it and — if it is sufficiently able — causes the potential disadvantages to fall on the weaker side. The only thing left to the weaker side, if it wants to do anything, is to conform itself. So then, the alliance of the two spirits present in the movement is determined by the choice of a common enemy: neoliberalism. In the face of the great power of the opposing side, it is said, differences must be set aside for now: “First we stop globalization, then we will see what to do.” The condition posed would even be understandable if it were mutually respected. But how do things really stand? Do both the components of this Sacred Alliance stand to benefit from it equally? Are the existing differences expressed in the same manner and do they hold the same possibilities?

What then is the declared enemy of the anti-globalization movement, capitalism as such or neoliberalism? And when we are present there at the summits of the superpowers convinced that we are “putting pressure” on the Masters of the Earth to which side’s needs is it responding? At the various anti-globalization demonstrations, violent clashes with the forces of order have occurred. This is what has forced the mass media to pay more attention to the disputes. Here is the usefulness of the alliance — some of the more extreme will say. In the final analysis, if it hadn’t been for the thousands of other, less extreme, demonstrators whose mere presence served to hinder the maneuvers of the police, these clashes wouldn’t had such a favorable outcome for the demonstrators. But the less extreme are also satisfied that there have been clashes. In the final analysis, if the “extremist menace” that needed to be averted had not been there on display, the Masters of the Earth would have had no reason to listen to them. As to those demonstrators who use clashes with the police in order to gain recognition from the earth’s Masters as go-betweens [Most notably, the Tute Bianche (white overalls), closely associated with YaBasta! — Translator’s note.], it is clear that though they speak out of both sides of their mouth (“we are not violent, but we clash with the police”, “we give advice to government officials and sit on municipal councils but we are antagonists”), they belong by right an by deed to the less extreme objectors to neoliberalism since their objectives are the same and they only distinguish themselves from the latter through the means they use to pursue these objectives. Now battling the police is not the primary objective of the more extreme, while being heard by the earth’s Masters is the primary objective of the less extreme. Paradoxically, who has the most reason to exult in the disorders that have happened up to now? In other words, to whom is this strange anti-neoliberalist coalition benefiting the most, the more extreme like the Black Bloc or the less extreme like the Monde Diplomatique?

Let’s digress for a moment. It is not at all strange that the mass media has rebaptized the movement with the name “the people of Seattle”. It is as difficult to find a gram of intelligence in the head of a journalist as to find water in the desert. But we don’t understand why this idiotic description is repeated by a large part of the movement itself. It is useless, the American dream even enchants its would-be opponents, those who on the one hand announce their refusal to live “like Americans” and on the other hand accept protesting “like Americans”. So if the friends of neoliberalism look to Washington, D.C., its enemies look to Seattle. It matters little, after all its only a matter of miles, as long as all eyes are turned to the USA. In spite of the much praised Autonomy.

Autonomy would like every one to be more or less free to choose what, when, how, where and with whom to act. The “people of Seattle”, on the other hand, like all People, is afflicted with a political defect. Within it are aspiring mayors, aldermen, councilors, even up to parliamentary whip. Of course, we are referring to those who intend to be elected as legitimate representatives of the “people of Seattle” in order to be invited by the earth’s Masters to sit with them at the next negotiating table, after having sat at the police chief’s table. At bottom this is all more than understandable. Less understandable is that the others adapt themselves to this ignoble game and allow themselves to be treated as citizens who are requested not to disturb the public peace. For months we have witnessed a painful spectacle. The Masters of the earth meet in the most varied corners of the world to formalize decisions made elsewhere. Their opponents follow them like puppies in search of attention: they stand on two paws, bark, growl, at times even nip at the edge of the pants of those who rule them.

Now it is quite clear. Though there is nothing to say to the true citizens of “the people of Seattle, we would like to address some observations to the others — to those without fatherland, to the deserter from all citizenship. At Gothenburg, the police fired, wounding a demonstrator who was throwing a rock. The Italian government has already made it known that it is interested in listening to the less violent opponents, provided that the more stubborn are left out of the dialogue. This can only mean one thing: having achieved their first goal — the much sought after institutional recognition — the less extreme opponents will quickly cease to be interested in continuing to march along side the more extreme who were useful up to now, having at first contributed to keeping the tension that created such excellent publicity high, but who will only be an encumbrance to them from now on. As soon as they are admitted into the presence of the earth’s Masters, what use will it be to them to continue using certain means? And at that point, what will happen? Those who have participated in this movement stirred by a hatred for capitalism have fought against its guard dogs, smashing shop windows and destroying machines, determined to destroy this world from top to bottom. But who chose the place and time from which to launch this attack? The earth’s Masters chose it. They chose the battlefield, they chose the method of conflict. Up to now, most of the opposition has behaved as the police expected. Now this game is coming to an end. The police are quick and even given permission to shoot in the back. [A sadly prophetic statement. — Translator’s note.] As petty politicians, the leaders in overalls, whether white or red, have every interest in centralizing the movement of opposition to neoliberalism. As subversives, we have interest in expanding rather than “globalizing” the movement of struggle against capitalism. The police are waiting for us in Genoa at the end of July in order to beat us, photograph us, film us, arrest us and maybe shoot us. And instead we could be anywhere at any time. The shop-shutters of McDonald’s and the banks of Genoa will be armored during the days of the summit. The multinationals, the supermarkets and the banks of the rest of the world will be at our disposal at any time. And this would only be the beginning since as soon as we leave off following the due dates that others set for us, we will finally be able to choose when, where, how and who to strike.

If we decide for ourselves, we will be unpredictable. We will lose allies, but we will find comrades along the way.

— a few nobodies neither want to represent or be represented by anyone




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

