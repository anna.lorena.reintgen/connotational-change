
If that tragedy that occurred so long ago of a family, worn down by hunger and poverty and seeking in death the supreme refuge; if that drama, due to its having occurred so far in the past – nearly two months – can no longer inspire the pity that the souls of its contemporaries accorded it for a moment, it can at least still serve as a theme for useful reflections.

I don’t know if upon reading this story M. Drumont felt the profound stupor that struck me: Eight people of the Semitic race reduced to suicide by destitution as perfectly noted as is was insufficiently relieved. If one person of that confession had come to that extremity it could have been explained by a certain exaggerated love for paradox, or by a strange and rare anomaly. But eight? For a moment it didn’t displease me to see these Hayem as modern Macchabees desirous of saving some too well-known financiers or poorly regarded politicians by causing an up-swelling of sorrowful pain among anti-Semites. In order to do this they showed no hesitation in descending into the scheol haunted by their ancestors. But it suffices to think about the quality of those who would have been protected by that heroic sacrifice to reject this thought with horror. It is painful to suppose that some men are capable of inspiring such meritorious devotion. And so it is that that there are those who, though not Aryans, live in misery and, even worse, that there are those among the Israelites for whom nothing can or does succeed. These Hayems, who the press spent several days washing of the accusation of Judaism, were among the lamentable mass of the humble who labor and groan, and who have as their only goal liberating death. To be sure, they gave a deplorable lesson: shouldn’t they have known that the society that uses them by refusing them the right to live also contests their right to die? For a moment there it was believed that the last survivor of this fatal family was going to face criminal charges: several men, quite clear-sighted when it comes to social questions, approved this measure, which was rejected. Looked at from their point of view, these bourgeois were right: The knew perfectly well that if these unfortunates, ever taxable and fit for the corvée despite all modern conquests, were to liberate themselves from existence there would soon be no one left to pay taxes. And so I would advise those perhaps too timorous economists to revive the antique procedures and to hang the Hayem family in effigy: this will serve as an example. It is true that in this particular case this act of bourgeois justice can be attributed to special reasons, and one can argue for a serious anti-Semitism, something that is to be avoided. Since among those represented as having squeezed within their hooked hands all the gold in the world there are many who suffer bent under bitter glebes, many who perish like their Christian brothers, killed by a hunger too often triumphant, since there is the case of these anti-Semitism is evil or, at least, it is at one and the same too little and not sufficiently exclusive.

If we were to interrogate the most ardent of anti-Semites he would find no difficulty in admitting that there can be found among Israelites men who are honest, respectable, capable of generous acts, receptive to great and benevolent thoughts. How many times in La France Juive are these very ideas not affirmed? It even seems that a reading of the books of the good Aryan crusader against the infidels reveals that the word Jew is a simple qualifier, also applicable to Moors, Huguenots, and Catholics when they act in keeping with a detestable norm having as its main characteristic the too exclusive and constant pursuit of their interests to the detriment of their neighbor’s. The Jew is then a general and universal type, a primordial form in which participate thousands of beings belonging to diverse races, different countries, dissimilar religions.

On the other hand, I’m afraid it would be a serious mistake if we were to see anti-Semitism as a religious manifestation, and this point of view could only be accepted by those who have in interest in its being admitted. Faith has become too weak to permit crusades in the absolute sense of the word. For one thing, Israelites do not have firm convictions. As Father Ratisbonne said: “They are no longer Jews, but they are not yet Christians.” For quite a while the Hebraic religion has fallen into a foolish rationalism, it seems to have borrowed its dogmas from the declaration of the Rights of Man; like Protestantism it has forgotten this essential thing: a religion without mystery is like a straw of wheat from which we’ve winnowed the grain. What is more, the theological mood of Christians is not such as would permit such intolerance: The estimable intransigence of Torquemada is no longer in fashion, and the times can no longer give birth to dreamers like the illustrious inquisitor. The true cause of anti-Semitism is social; all those of its party affirm this in perfect good faith; it’s the same cause that, now that the century is coming to an end, brings into the streets those who have nothing against those who have too much. This being the case, there occurs an ambiguity that though simply troublesome now could one day become very dangerous. It was written in la Derniere Bataille that “at the hour of the supreme crisis, when there will be electricity in the air, nothing will bed able to save the Rothschilds.” I must admit that this perspective affects me little: if that minute were to arrive I will not be among the improbable Swiss Guards who will defend the doorways of these new monarchs. But there is something else. Among many the concept of the Jew as a special being, essentially capitalist, will prevail, and it is they who will differentiate between the too cunning banker, the crafty politician, the too adroit journalist and the Israelite who lives on his labor and loyally carries out his function as merchant, magistrate, savant, soldier or artist. On that day Monsieur Renan will easily be able to save a few members of the diverse academies and Monsieur Cladel a few of his confreres. As for the mass, the social symbol will become for them the easy to grasp symbol of a religious confession, and the humble will once again pay for the powerful. The latter have the right to be defended, and it wouldn’t be right for them to suffer: they are Israelites and not Jews, for there are Jews and there are Israelites.

The Jew (many are Jews who have become so without this having been destined for this by their race but having become so through native virtues) is he who is dominated by the sole preoccupation of rapidly making his fortune, which he will more easily obtain through theft, falsehood, and ruse. He holds virtues, poverty and disinterest in contempt. The beast that was erected in the desert by the unfaithful tribes has remained the sole object of his adoration. When the Jew is a journalist the newspaper is for him a means of obtaining lucre, and he exploits it every way he can. He has the particular talent of discovering the most obscure passions and knowing how to flatter, satisfy, and encourage them, and he glories in the debauching of others. He knows how to be obscene and how to be chauvinist; he exploits the popular tendency to withdraw itself from great spirits that frighten them and uses language within reach of the lowest. He replaces wit with inept puns, eloquence by phraseology, enthusiasm by epilepsy. The journalist lies, confuses, unsettles, and when the daily columns no longer suffice he installs himself in the theatre, where he degrades. When the Jew is a banker he possesses a powerful organization for evil, a dark genius. He is proud, greedy, and false. He accumulates frauds that go from the skillful, and even banal scam to daring theft. He thinks perpetually of subtle machinations, of daring maneuvers. He can be found everywhere, for everywhere he extracts gold: from governments that borrow, from naïve inventers who only create, at the head of countless companies that he maintains with the lie of his fascinating eloquence. If disasters occur he is sheltered from them: what use would be the ability to conquer a fortune if it doesn’t save you from disastrous consequences? So that apparent justice can always be attested to there are the weak and the simple, those who are fed crumbs, who are fattened, who are abandoned, and who save. When the Jew is a politician he arrives through charlatanry, noise, and flattery. He is a schemer, fecund in fraud, and in politics he will most often only see the possibility of paying his debts and becoming rich through manipulation and speculation. Protests about the grandeur of France, the glorification of revolutionary principles or even divine right monarchy will cover all this, and will assist him in trafficking in all that men consider great. In summary, Jews are those for whom integrity, beneficence, and abnegation are only words or virtues to be traded; they are those who make money the goal of life and the center of the world.

But alongside this contemptible Judaism, rotted by cupidity, hating noble gestures and generous impulses, there are very different beings; there are Israelites. These latter are not known and are too often forgotten. They have no history and their names are not known, for they have never been mixed up in spectacular trials, fiscal adventures, or spectacular spoliations. They have lived peacefully for years, attached to the land that gave them birth, where countless generations have succeeded each other (I am speaking and only wish to speak of the Israelites of France: the others are indifferent and foreign to me). They are poor or have just enough money, limited in their desires and with before them nothing but the narrow horizon of the relative well-being that is that of that mass. They know that powerful bankers exist, and they are led to believe that their glory comes from these bankers. They don’t protest, having the customary bedazzlement of the people for piled up millions. But they don’t ask to be like these plutocratic gentlemen: in a confused fashion they understand what tears these fortunes are made of. Of these Israelites, some are workers, others small shopkeepers. Some are doctors, and they didn’t choose that science in order to conquer the secrets of the families they care for, as M. Drumont one day said, borrowing from Eugene Sue the novelist’s opinion about Jesuits, and with just as much justice. They are magistrates, and their integrity cannot be suspected; they are soldiers, most of them officers living on their salary, lovers of possible wars since they are descendants of heroes of antiquity. They are artists and they live in the respect for their art, disdaining the unspeakable glory of the Jews of vaudeville and the operetta.

And all these Israelites are tired of seeing themselves confused with a foul mass of frauds and cheats. They are tired of the perpetual ambiguity that ranks them with rotten speculators, imbecilic music makers, witless journalists, emaciated chroniclers, talentless politicians, dishonest phrase-makers who dishonor them and for whom they feel that just contempt that is universal. Since they don’t see the danger they remain silent. In any event, they are too simple (why wouldn’t Semites be simple, Aryans being dissolute?) and precisely because they are simple it is only right that voices in their favor be heard, it is right that they be supported. Do they not have faults (I am still only speaking of the Israelites of France)? Yes; the fault of allowing themselves to be led by the unworthy, that of believing to be their superiors those barely worthy of serving them, and the no less great fault of allowing themselves to have imposed upon them by interested men a so-called solidarity that assimilates them to money-changers from Frankfurt, Russian usurers, Polish tavern keepers, Galician pawnbrokers, who they have nothing in common with. At another time I will return to this solidarity, which is a Jewish and not an Israelite solidarity. The Israelites of France must be shown all the errors they commit. It must be shouted out, since they are napping, that they must reject far from them the lepers who corrupt them. They must vomit up the rot that seeks to penetrate them. But the error is venial, and it would be only right if the anti-Semites, finally correct, were to become anti-Jewish. They can be sure on that day of having many Israelites with them.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

