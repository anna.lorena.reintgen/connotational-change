      Profession and Movement        The Aufheben Scandal        …the puddle of an iceberg        If unions and foundations beckon with jobs, funding and research projects        “Professionalisation” of what?        Critique of the profession
Three years ago a small scandal took place when the Greek group TGTP published in an open letter that the co-founder of Aufheben, John Drury 1 , lead workshops for the police and military and is known as a “provider of ideas” in these circles. These workshops took part within the framework of his academic career researching Crowd Control, mass panics and rescue operations. Together with his closest colleagues Stott and Reicher he has developed the Elaborated Social Identity Model (ESIM). The social psychologist Stott is renowned to be one of the globally leading experts for protests and violent uprisings. ESIM claims that a ‘mob’ acts according to certain patterns: people in a crowd have individual thoughts and emotions, so when the crowd is attacked indifferently by the police, people act in solidarity with each other and resist together. Therefore ESIM advices that the police should proceed in a multi-levelled approach and extract ‘individual perpetrators’ from the crowd. Using such kind of methods, Stott coordinated security preparations for the European football cup in Poland and Ukraine in 2012. (for more details see both open letters by TPTG)[1]

Aufheben is a group from Brighton, which publishes one of the few collectively produced magazines of the radical left in England. The magazine consists of mainly long articles tackling fundamental questions (what was the Soviet Union, decadence theory, “Green New Capitalism” etc.). They often deal with similar subjects as us (theory of the oil rent, criticism of Negri's autonomist marxism, debate about Beverly Silver's book). We have translated some of their articles (21st century Intifada, criticism of the commons thesis by Massimo de Angelis, Dole Autonomy) and have criticised them at certain points (e.g. in wildcat 89, “The oil rent, Ricardians amongst themselves”). We share similar positions when it comes to the issues of working-time reductions and guaranteed income. During the end of the 90s we initiated a closer collaboration with Aufheben and other collectives in Europe, which ultimately failed.

Therefore the revelation about John Drury came as quite a shock - which in itself would not have been a reason to write in Wildcat about it. But the way the debate about this case was lead within the left-communist scene in Europe has initially left us speechless. Most of the people shook it off (“let's move on”) or attacked those who had made these scandalous facts public.

The current movements open a public space again to debate ‘general interests’. But the fact that many participants of these movements don't criticise their own social situation, but rather ideologise it (“we are all precarious”) render these movements toothless. This is related to the fact that all these movements have ‘two souls’: one part of the movement is young and has formally high qualifications, whereas the other part is formally less qualified and in the long run ‘decoupled’ from social progress. During the crisis, the conditions within the movement regarding the individual ‘professional choice’ and regarding what I am willing to swallow at work have eroded dramatically. As a result of the casualisation of work relations, the interest in a ‘profession’ and in a professional career has increased enormously. Often people then cling on to jobs although they deeply hate them.

As long as our highest-performing youth still sees their chances in the highest-paid jobs in the finance industry, we don't have to fear a revolution, the Financial Times Germany commented at the beginning of 2012 - but at the same time pointed out that decreasing wages could “lead to investment bankers looking voluntarily for more sensible tasks, even when they are not actually made redundant. Amongst the Harvard graduates, Wall Street lost its mobilising effect as early as 2011.” (FTD) Perhaps revelations of the role of the German secret service during the NSU scandal [2] and the debate following Snowden's NSA-leaks will lead to a process of re-thinking even amongst programmers and the hacker scene. At least certain professional careers are now debated and scandalised publicly:

One example is Occupy Wall Street-activist Justine Tunney, one of the more famous people in Zuccotti Park, who, amongst other things, set up the website “OccupyWallSt.org” under the slogan “The only solution is WorldRevolution”. After the end of the movement she took a job as a software developer for Google, declared publicly that the Google CEO Eric Schmidt should become US-president and said thinks like “I think Google is actually doing things that are making the world a better place” and “even though they operate within a capitalist system, they still do the most good throughout the world” When Google's role in the current surveillance scandal was criticised she retorts “I am always surprised to see as to what extent people distort reality in order to denounce a company which offers everything for free”. Also politically she now argues against the movement and attacks left-wingers like David Graeber or rants against social welfare on Twitter - here she might not even have had to change her previous opinion. Her proposal to pay people for going on demonstrations finds support e.g. by Micah White, a former editor of AdBusters, who now works for a ‘social movement consulting’ company.

The scientific work on cryptography is as little ‘neutral’ as research on crowd control. And whoever presents their sociological thesis on panels organised by the secret service [Verfassungsschutz: Federal Office for the Protection of the Constitution] cannot at other occasions preach ‘no grassing to the cops’ at their antifa events. Paradoxically, in parts of the radical left these types of double-standards exist because one's own wage work is not openly debated. The ‘opposition to the state’ then becomes ideology or attitude - and expresses itself ‘practically’ only on the occasional demonstration.

Movements only gather force once they make the ‘private’ public. An important step in political groups is to discuss the earning and spending of money together. But even with the approval of the group certain borders cannot be crossed. The cooperation with NATO, police or secret services surely belongs to this category. A wide and public debate is therefore necessary. This debate will touch upon more or less every question - from one's own reproduction, to forms of organisation, revolutionary moral to ‘what revolution actually is’. This is why we want to start a small series of articles in order to encourage you to participate in the debate: What role do I play within the capitalist division of labour? What are the costs I have to pay for a professional career? Can I move around on the labour market in any other form but individually? The following is meant as an introduction for the debate.

Aufheben were doubly affected by the revelation. Alongside the aiding of organs of repression, JD was caught having friendly and cooperative dealings with reformist colleagues - Aufheben had always keenly castigated other left-wingers with far fewer reformist affinities. Aufheben reacted immediately - they had to obviously not think for long about it, as well as not change their political and theoretical assumptions. They saw nothing wrong with the fact that their comrade was making his career in a ‘state-security linked science’ (Hartmut Rübner). Instead, they generously explained to their critics how ‘academia’ works: “The ‘blue light services’ work closely together; and so talking about emergencies means probably talking to cops as well as the others. His University encouraged this, and it would have looked odd to refuse to communicate with the cops. So he accepted this as a small cost of the overall job of research work.”. They cheekily made their critics out to be acting like a police state because they had made the name of the collective's member public. (Nevertheless we've decided to use the name as well, if John Drury makes no secret of his political origins to ‘blue-light colleagues’, then the left scene can know about his academic achievements).

In the ensuing confrontation, JD was defended, also from people who were politically close to Aufheben. Here, there are deep commonalities amongst people on the ‘radical left’, who see themselves as radical but meanwhile explicitly advocate the depoliticisation of their own reproduction: how I earn my living, how I spend ‘my’ money - that's nobodies business! To confront each other in sharp ideological clashes is one thing, to share the same social behaviour ‘modes of behaviour’ is another. Food comes first, then morals. A left that can no longer imagine a revolution, look for material security and social recognition in their waged work - how else is is supposed to go any other way in capitalism?

It is problematic that the left-wing scene itself has become an inscrutable mix of political projects and sources of income. Self-employed people do contract work for leftist publishing houses; left-wing magazines offer paid jobs; many of these jobs you only get if you have the right political connections… this goes as far as self-employed activists, who protest against nuclear power, banks or gene-technology for pay; paid by people who lack the time for protesting themselves. Once the boundaries between political engagement and earning money become blurry it becomes impossible to distinguish between what people actually think and what they propagate for professional reasons.

In the UK this type of employment is called ‘movement jobs’ and compared to Germany this tendency is more widespread. Many people of the ‘radical left’ work as organisers for trade unions or as lecturers at the university. A quote from a comrade in London: “When I attend meetings to ‘support cleaning workers’ half of the meeting consists of people because they are just about to write a freelance article about the topic or because they do a PHD on ‘migration and affective labour’ - or because they have a job or function within the union and are therefore required to participate. Later on in the pub this schizophrenia continues (“do you know what, I just have to finish this article for the Guardian, this then will give me more time to write more radical stuff” etc.).

Since the Hartz-reform [3] , the left in Germany has caught up when it comes to ‘movement jobs’ - since the onset of the global crisis there is literally a boom of these types of job relations. Nearly half of the former radical left will now be dependent on political party funding (mainly from the Rosa Luxemburg Foundation of the ‘Partei die Linke’) or on doing professional ‘training against racism’ at schools, or ‘human-rights oriented children and youth work’, and so on.

The many former activists of the radical left who now work as ‘organisers’ in the trade unions are an example for the fact that by and large doing such jobs doesn't guarantee an ascent up the social ladder. We have written about the working conditions of ‘organisers’ in detail in wildcat no.78 and no.80. In addition we refer to the article ‘Left co-management - Critical remarks on ideology and practice of union organising’, by Berger/Meyer, published in 2011 in the anthology ‘Organisation and Critique’. Referring to the example of the former radical-left refugee activist Franziska Bruder, Berger/Meyer point out the difficult consequences of such a professional choice: the ‘lead-organizer’ was sent “by her employer ver.di [service union], of all possible branches, to organize the security sector”. (footnote p.261) And although the union campaign was obviously not about “an emancipatory questioning of the self-defensive interests of private property owners and state institutions” nor about “an organising of those who are locked up or excluded and who are controlled and detained by the security guards”, Bruder was quoted in the trade union journal Mitbestimmung 12/2007 that “trade unionism has to be fun”. Berger/Meyer rightly point out “the danger, that campaigns for the ‘organising of the unorganisable’ finally turn out to be vehicles for the career aspirations of left organisers”. (p.265) Which is no contradiction, given the fact that “the explicit self-positioning as think-tanks for the trade union leadership could turn out to be the academic version of the ‘self-organisation of the [self-proclaimed] precarious’”, of which there is a lot of talk within the left. (p.248)

Not only trade unions and companies are interested in the management skills of activists; having a left background and contacts to social movements are seen as an additional qualification for certain jobs. This is why Dr. JD had no problem to explain his political development in a scientific magazine [4] :

(“What critical psychology can(‘t) do for the ‘anti-capitalist movement’”) “As such, we [critical psychologists] appear to have the best of both worlds; we can satisfy some of our own needs as critical people (and be true to our conscience) while at the same time making our living as psychologists – even perhaps getting a decent career out of it.”

The “professionalisation of our media work” [5] , campaign work and the whole social-pedagogic civil-society blabber is just the other side of the coin of such careers - in the end the trained knowledge has to be made use of somehow! The “Castor Schottern”-campaign would have been the optimal fieldwork ground for JD! [6]

The left movement as a whole pays a high price for such kind of individual careers, the negative repercussions on the ‘socio-political fabric” are grave. The political left is not external to the process of the extreme increase of social inequality in society; compared to the rest of society during the last years the income gap within the left will have widened even more. Individual careers on one side, increasing pressure and atomisation on the other side pushes more people towards individually feathering their own nests. The turn towards ‘Realpolitik’ in the radical left in the first half of the 1990s was enforced by people with an intellectual and finally social self-interest in the (improved/reformed) continuation of the social division of labour (e.g. Joachim Hirsch propagated in his “The National Competitive State” in 1995 “revolutionary politics are impossible”). Today left congresses are organised like university lectures, left speak and academic jargon have become indiscernible. And people like Roland Roth collaborate with the state intelligence service - see in more detail the book “Gegnerbestimmung” [7] .

While more and more people turn their back on the state (see for example the falling election turnout), the formerly radical left has moved towards it and at various points it wasn't possible anymore to distinguish the left from state institutions. The left doesn't know their enemies anymore; the state security administrations become increasingly powerful in Germany, most of all the intelligence service [Verfassungsschutz] - and the formerly radical left share panels with representatives of these institutions or have their anti-racist pamphlets financed by it - even after the uncovering of the NSU!

It would be worth some separate research to see how many formerly left activists globally contribute on behalf of European and US-American foundations to the fact that movements of upheaval such as in Egypt are not getting out of control, that they orient themselves towards civil-society/democratic values and don't radicalise themselves through social conflicts. Also, a historical analysis of how the decline of movements result in institutionalisation, but how this institutionalisation was already present as ‘tendencies of professionalisation’ during the movement itself, could help us progress in this necessary debate; e.g. some research into the composition of the First and the Second International would be interesting (artisanal workers’ clubs vs. leadership of engineers and lawyers, who declared better state planning to be their main aim).

The assumption that by working within the institutions you could siphon off resources or money while having to give little back in return is as false as Aufheben's opinion, that working within the (academic) institutions is necessary for revolutionary theory production. “However, it is a matter of fact that a large part of theoretical Marxist production has in recent times come out from under the generous wings of academia. After all, for a young radical student who has been involved in struggles and genuinely believes in communism, a university career is ideal – it would provide the possibility of attacking the system and be paid by the system itself to do so.” Aufheben is well aware of the problematic relation between ‘revolutionary theory’ and the academic apparatus: “But this separation of human activity, which is a real separation, cannot come without concrete consequences. By submitting itself within the scope of university research, the activity of thinking was necessarily redefined as a specialist activity, done within the requirements and parameters of the academic world. However genuine the authors’ inner feelings are, this concrete aim will inevitably affect both the form and the content of their work.” [8]

But they pose the problem in a way (“The very fact of belonging to the exploited class gives us less time to make theory than the time given to those belonging to the bourgeoisie.”) which affirms the (at that point not yet publicly known) decision of JD. Their perspective before and after the ‘scandal’ is coherent - This is the really disturbing fact and forces us to reconsider left-communist theory production of recent years. Cynical commenting on left ‘Realpolitik’ and their professionalised campaigns has become the raison-d'etre of groups like Aufheben. Aufheben masters skilfully the sharp critique of collective efforts out of the off without ever having to put themselves into the spotlight of consideration. Their criticism isolates and divides - similar to what a successful police strategy has to look like according to ESIM. (In hindsight their criticism of the call centre inquiry of Kolinko - claiming that inquiry is a “functionalistic relation between militants and workers”- in which they portray themselves as critical Boheme-thinkers, who are neither militant (want to ‘intervene’) nor see ‘being worker’ as a potential starting-point of political activity, seems almost allegorical.

“with the categorical imperative to overthrow all relations in which man is a debased, enslaved forsaken, despicable being…..”

(Karl Marx)

Sergio Bologna once said that if you had to summarise the revolutionary content of the movements of 1968 and after in one sentence, it would be the critique of the profession, the critique of the capitalist division of labour. People who took part in the movements during the end of the 1960s, early 1970s knew that they would have to change themselves within the revolutionary process and criticise their position in society. They were not able to just start from their ‘interests’. Instead they radically criticised the totality of the capitalist division of labour (science, school, factory, family, prison…). This revolutionary impetus has gotten lost today - but it's certain that it will re-emerge and grow within future revolutionary movements.

Nowadays it is fashionable to snigger about people who decided ‘to go into the factories’ back then… - the widely maintained lie that all revolutionary students had left the factory again after a few weeks and made a career demonstrates that this past still raises questions and aspirations which have to be fought against if one wants to make peace with the existing social relations. (From today's perspective the idea to work in a bigger workplace has a totally different attraction, given that jobs tend to become more isolated. At university, in the web-design companies and similar jobs you often have only few work-mates; or you work completely on your own as a freelancer or other forms of (false) self-employment, or during writing your PhD. In such jobs it is pretty difficult to impossible to get something going collectively.)

The reverence for experts within social movements (theory experts, organisers, lawyers) is also related to ‘technical’ changes. With an increase in the polarisation of the social division of labour and intensified control of labour the gap between the intelligence of the collective worker (as an antagonistic subject) and the special knowledge (as scientists or ‘high-skilled’ professions) widens. A collective of mechanics was able to understand and anticipate the work of engineers (often engineers merely appropriated their ‘inventions’). Today we are often confronted with strikes of (migrant) workers who on their own are not able to make use of their productive power, given that machine operators and technicians are able to run production without them (because the training period of newly hired workers would be sufficiently short).

Trade union organising addresses these very ‘leaders’, e.g. branch supervisors in retail etc.. “Organizers focus on the ‘alpha-(fe)males’ within a circle of workers, independently from their political positions, and thereby foster the internal hierarchies and tendencies of exclusion within work-places” (Berger/Meyer, p.268). Emancipatory movements have to attack such hierarchies and try to invert them.

Critique of the capitalist division of labour also has to be a critique of the content of capitalist science; not only of the social science, but also natural and engineering science. The critique would have to unveil how the ‘gods with or without ties’ cannot develop their knowledge separate from the social cooperation of labour - once it concerns ideology (the benefits of gene-technology) or is of significant danger (nuclear technology) or is simply aimed against us (military, cops, intelligence services). It was correct that TPTG in their open letter did not criticise the discipline ‘crowd control’ as academics with a counter-expertise, but that they criticised the content of it as a political group.

Finally bin the ‘precarity-ideologies’! No one has ever promised that in capitalism everyone will get a position and income according to their qualifications! Fulfilment in your work and profession has always been a privilege of the middle-classes. Whoever sees a guaranteed/permanent job according to one's university graduation as their special and individual right, rather than criticises the capitalist rat-race behind such promises and divisive structures, affirms capitalist competition. Instead of complaining about a lack of professional prospects, the ‘overqualified precarious’ should rather criticise the capitalist social relations around them!

You cannot simply proceed in a professional career and be ‘revolutionary’ in your free-time. We need our own structures as a material alternative to the ‘profession’; we need commonly organised living arrangements, collectives and (social) centres which would allow as a different way to approach ‘work’: to kick a shit-job if necessary; to work for a low-wage, because the job is politically interesting; to stir up a work-place collectively. Instead of ‘professionalisation’ and Realpolitik we have to advance the movement through a continuous international exchange.

Pros piss off!

Everyone can learn everything.







Footnotes:
[1] The open letter of TPTG, The answer by Aufheben, The second letter by TPTG, Wikipedia Article on John Drury
[2] NSU: National Socialist Underground; After several murders of Turkish migrants by a fascist terror-cell NSU between 2000 and 2006 it became known that most members of the NSU were paid for by the intelligence service; the home ministry knew about the close intertwinement between fascist armed groups and intelligence and tried to hush things up by all means necessary. This did not prevent formerly radical left activists to ‘work together’ with representatives of the state intelligence ‘against the right-wing threat’;
[3] The Hartz-reform slashed the dole, which was connected to the lasst salary to a minimal wage and forced a lot of workers in low-paid jobs.
[4] John Drury: What critical psychology can(‘t) do for the ‘anti-capitalist movement’ in Annual Review of Critical Psychology 3: Anti-Capitalism
[5] In German on question ‘proletarian public sphere’ vs. ‘professional media work’: Wie machen wir's öffentlich?, Wildcat Nr. 81
[6] In 2010 during the protests against nuclear-waste rail transport near Gorleben the political disaster of protest management by ‘professional left campaigners’ became apparent. While ‘radical left spokespersons’ on on side mobilised protestors into ‘symbolic militancy’ around the railtracks, they at the same time had come to agreements with the police. For their media campaigns they needed some sort of ‘movement event’ (crowd) and in order to be accepted as ‘political players’ they had to contain it at the same time, e.g. through diplomacy with the cops (control). Various groups criticised in hindsight that this ‘double-play’ lead to unnecessary arrests and victimisation.
[7] Gegnerbestimmung = “Enemy Identifikation”: A former left-winger Roland Roth published the book “Die sozialen Bewegungen in Deutschland seit 1945” [The social movements in Germany since 1945], asking not only his ex-comrades from the radical left to participate, but without their knowledge also collaborated with members of the intelligence service[Verfassungsschutz], who contributed articles about the ‘radical right-wing movement’; this is one of several recent examples where ‘left-wing antifascists’ work together on ‘democratic platforms’ with representatives of ‘deep’ state institutions
[8] Reclaim the ‘state debate’; Aufheben #18 (2010)




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

