      With Friends Like These…      Fighting for the NEW One Truth      Against All Laws by Party or Property
If there is one thing the South never lost it was its Cult of the Dead.

They are everywhere, in almost every guise, for those with the eyes to see. Descendants of Confederate veterans place fresh flags of a nation that never was on battlefields long-lost; Baptists sing hymnals from faraway lands about ancient and powerful souls from the distant past; a mother walks under empty street lights to place fresh roses on roadside cross in memory to a boy taken by a mix of high speeds and even higher alcohol contents.

There is an unspoken acknowledgement pervading the culture that dirt carries imprints of the actions and personalities of those that walked or died on them.

Which is why Conjurers love dirt from a soldier’s grave.

A soldier has lived his life following orders, a trait he carried with him long after death. She is used to hearing commands, not questioning them, and achieving very specific goals against those who might wish the opposite. They tend to reduce most things down to a matter of friend or foe, giving the competent sorcerer a wide range of applications, of aggressive or defensive work, even of compelling or of controlling.

Of course not all soldiers are created equal, and neither are most dead people. A baby wizard’s first foray into a graveyard often assumes that people with any amount of time in the service were a veritable Duke Nuke’m while alive, and will jump aboard any occult operation with a smile.

The title of soldier hides a world of difference. Some people join for short periods of time, others until the day they die. Some save lives as medics or console the weary as chaplains, while others kill just for the sake of killing. A headstone leaves out much of the personality that stays with a soul long after death’s embrace, the same tweaks and turns that give a soul its power.

Only a fool would lump the souls of soldiers together, or even think them compatible, based solely on the fact they were people who had a similar occupation.

Yet this is exactly what radicals do when they call for “Left Unity.”

Somewhere out in space, far beyond the zodiac and near the platonic solids, exists an invisible chart where each political philosophy is tracked and pin-pointed. Ideologies that share certain characteristics under certain pre-arranged questions are given a direction and even an ideological “family” to feel close to. Where they fit on this metaphysical scale determines certain characteristics of each ideology, and how they should respond to issues in the mundane world that you and I actually inhabit.

Such is the prevailing wisdom of the internet and academia, an intellectual labeling system for ideas based on other ideas built from contemplation and consideration. I leave such navel gazing to the Kabbalists and prefer the rooty wisdom of experience: if the sorcerer is a natural philosopher, I take my gleanings from the dirt and bones that surround me. Most of the soldiering Dead can be called “similar” in that their ability to fight is true enough, but the why and how is a matter of individuation.

One grave I’ve worked with is home to “Shorty,” a riotous brawler who fought in the Boxer Rebellion, the Spanish-American War, and World War I. Another, Thomas Pruitt, is a romantic from Georgia who died believing he was honestly liberating Cuba from foreign tyranny.

Both share a war in common, and both for wildly different reasons. To believe those souls are interchangeable or even applicable for the same purposes is to do a disservice to the Dead and potentially pre-fuck any ritual.

The living find themselves in a similar predicament, though admittedly on un-necromantic grounds. Anarchists, whose goals don’t include replacing the ruling class with a fairer boss or a better party, find themselves lumped in with “comrades” far more grotesque than a Casey Anthony Babysitting Club.

Marxist-Leninists support the imperialist struggles of anti-imperialists provided their war-crimes are against the United States; Maoists attest that the Chinese Communist party, home to actual millionaires, is leading its pollution-choked lumpen proles towards Marx’s grand vision; primitivists decry every human life as a murder while calling for a “peaceful” culling of the herd; Syndicalists assure me freedom is no farther away than a worker-owned McDonald’s exploited by majority vote.

I don’t want any of those futures, nor would I fight for them, yet because of the place they hold on some imaginary scale I am to regard them as “friends.”

Historically the vast majority of leftist theory and practice has functioned as a loyal opposition to capitalism and left it at that. Liberals, State Communists, and even an Egoist or two could march under one banner in glorious weakness and call for the End of the World as it Was, provided of course they never, ever discuss What Was to Be.

This is fine as long as the Left is losing. When the Left seems on the ascendant the question of “who rules” becomes a matter of life and death. I won’t bore you with the historical anecdotes but needless to say the “solidarity” so treasured in times of peace becomes nothing but sympathy to exploit when ammunition is as plentiful as the fingers to fire it.

This is not by accident but a feature of the machine. Jason McQuinn in Post-Left Anarchy: Leaving the Left Behind wrote:

“Whenever any system of ideas and duties is structured with an abstraction at its center — assigning people roles or duties for its own sake — such a system is always an ideology. All the various forms of ideology are structured around different abstractions, yet they all always serve the interests of hierarchical and alienating social structures, since they are hierarchy and alienation in the realm of thought and communication. Even if an ideology rhetorically opposes hierarchy or alienation in its content, its form still remains consistent with what is ostensibly being opposed, and this form will always tend to undermine the apparent content of the ideology.”

Or as Novatore put it:

“If I look around myself I want to vomit.

On one side the scientist whom I must believe in order not to be ignorant. From the other the moralist and the philosopher from whom I must accept the commandments in order to not be a brute.

Then comes the Genius whom I must glorify and after the hero to whom I must bow affectedly. Then the companion comes and the friend, the idealist and the materialist, the atheist and the believer and all other infinity of monkeys definite and indefinite that want to give their good councils to me and to place me, finally, on the one good path. Because naturally that the path which I was on is a mistaken path, as mistaken as my ideas, my thought, my everything. I am a mistaken man.”

The Left, for all it’s prattle about “solidarity,” is as egoistic as anything else. Each ideology considers itself “the one true” answer to humanity’s infinite complications and desires with a “one true god” that the species must bow before, whether it be Nature, Science, Dialectical Materialism, or even the idea of Freedom itself.

Freedom becomes an abstraction, a thing to be preserved instead of utilized, a form of censor instead of neutrality, when it attains the status of Platonic solid so favored in the Left. When the freedom of the “Worker’s State” is what must be protected, every would-be revolutionary cheers for the same laws written by the bourgeoisie, salivating at the opportunity to fill the prisons and gallows with the new “acceptable” enemy. Witness the “Anarchists” who praise the restriction of speech in Europe or the governmental banning of symbols when they could just as easily praise the freedom of striking fists and grasping fingers that do not wait for a “law” to make them happy!

Raindrops soak my jacket as I try to scurry in the door. The moon-filled Florida night weeps like a mother that just heard her baby die and the streets are eerily still. Popeye’s is finally getting out of work at 12:30am and a cop car makes his second pass, a quiet threat to get a move on to the liquor store and get out.

Above my door are three cards rubbed in Run Devil Run oil and bathed in graveyard dirt from a less-than-helpful spirit. He had to be coached and cajoled into defending my home by tailoring what I desired in a language he could understand.

I remember the room suddenly flooding with not merely a presence but the ideas in his head as I called across the Veil. “In the Three Highest names I call on you to protect my home as you did the base you were stationed at! It is not I that calls you here but the souls of all your commanders! It is not my words you must obey but the solemn oath you took to defend your country! My home is your country, I am your officer. Obey your oaths and accept my offerings!”

As the cop lingers in the parking lot I think he’s under the same spell, abstractions rinsed with abstractions. He’s not a vessel for sanctioned violence but a defender of the law, just as any peasant-beating Knight was a noble defending his rights and a serf-shooting Commissar was defending the revolution.

All these ideologies demand I take on a role they’ve defined. For some I exist only in relation to the execution of official edicts, others I exist only in relation to my place in a social hierarchy, still others in my relation to my place in an economic exchange.

To all these things I am just a soldier whose dirt is waiting to be utilized.

The only unity I will have with the Left is the distaste for the wealthy that commodify my existence and hoard that which all require. I see no point in being anybody’s soldier other than my own and make my war on all that will limit me. If our needs coincide then we’ll work together but I will not elevate or fight for any cause that would erase my soul in favor of a life as an abstract curio. Worker’s dirt,civilian’s dirt, even sorcerer’s dirt will not do for the energy my bones will leave behind.

You will find my grave away from the soldiers and proudly among the criminals, my dust a spiritual molotov; in the next life as in this one I shall burn bright and fiery against all that would break me.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

