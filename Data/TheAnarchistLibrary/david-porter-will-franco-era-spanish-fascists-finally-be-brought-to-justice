
On October 31, an Argentine judge, Maria Servini de Cabria, issued international arrest warrants and extradition requests to question and try 20 Spanish Franco-era officials accused of crimes against humanity from 1939 to 1975.

Spanish General Francisco Franco led the Nationalists, a military/fascist rebel group, to eventual victory in a civil war (1936 to 1939), overthrowing the democratically elected republican government and quashing revolutionary social change led by anarchists and others.

Salvador Puig AntichAt least five of the mid- and high-level officials, including several ex-cabinet ministers, approved the 1974 strangulation-by-garrote torture execution of Salvador Puig Antich, a 25-year-old Spanish anarchist. Puig Antich belonged to the Movimiento Iberico de Liberación (MIL), a loose militant network of Spanish anarchists and anti-authoritarian far leftists, that carried out a series of armed bank expropriations to support the self-organized revolutionary workers movement of Barcelona in the early 1970s.

In 1973, he and another MIL militant, were arrested following an ambush in which Puig Antich was badly wounded during an ensuing shoot-out. He was charged with the murder of a Guardia Civil who was killed in the incident.

Puig Antich’s quick trial before a military court was a farcical, but typical display of Francoist justice. The stacked police case was exposed at the time by MIL supporters as falsified and inconclusive since the cop was shot by more bullets than Puig Antich’s gun had fired and he was already too wounded to shoot back in his own self-defense. Nevertheless, he was found guilty and sentenced to death.

Despite an international campaign against his execution, the regime defiantly ordered his sentence carried out to uphold Franco’s far-right dictatorial politics in the face of pressures for regime liberalization and angrily responding to Basque ETA separatists’ assassination of the Spanish prime minister a month after Puig Antich’s arrest. MIL members were inspired by May 1968 in France and liberation struggles elsewhere, but also specifically by the 1930s anarcho-syndicalist movement in Spain and its guerrilla activists after 1939. Opposed to self-promoting leftist avant-garde grouplets and paramilitaries, MIL militants disbanded their organization even before the arrest of Puig Antich and others in late 1973.

Several who escaped arrest and new supporters in the anti-execution and MIL prisoner support campaign launched a new network in France, the Groupes d’Action Révolutionnaires Internationalistes (GARI). Two months after Puig Antich’s execution, GARI kidnapped an important Spanish banker in Paris, Angel Baltasar Suarez, and demanded an end to executions, release of all MIL and many other political prisoners in Spain plus a large ransom for the cause.

While no prisoners were released, about three million French francs were handed over, though most were soon recovered by French police. Subsequent armed attacks on regime targets by other groups followed, inside and outside of Spain. But in 1976 and ’77, following Franco’s death, the Spanish Communist Party and other left and centrist parties made agreements with Franco’s successor, essentially legitimizing the regime by seeking only reforms within the right-wing political framework while gaining some relief from government repression.

As part of the accord, Spain in 1977 created an amnesty law against prosecutions of either side’s politically motivated actions during the Franco period.

The Argentine judge justified her intervention by the same universal jurisdiction standard of international human rights law concerning crimes against humanity (such as torture and mass murder) that is used by the International Criminal Court in The Hague. This standard was used by Judge Baltasar Garzón in Spain to justify both his order for the arrest of Chilean ex-dictator Augusto Pinochet in 1998 and his attempt to indict top Bush-era figures for acts of torture. If proven that such crimes cannot be prosecuted in the country where committed, certain countries (including Spain until 2009, Argentina, and over a hundred other states) have a constitutional or legal mandate to prosecute human rights cases against foreigners.

Catalonia’s parliament in 2004 recommended that the state “should annul all judgments and sentences declared under the Franco regime.” Three years later, Spain’s socialist-led government passed a Historical Memory Law merely condemning the Franco regime, removing all of its public symbols, and helping to fund local “associations for the recovery of historical memory” investigating local atrocities under Franco. But successor conservative politicians have blocked such public funding. Meanwhile, Judge Garzón was removed from office after ordering investigations into Nationalist murders of 114,000 victims during and after the civil war.

Eighty years have passed since the Spanish nationalist/fascist revolt against the elected republican government and almost 40 years since the amnesty law. While visiting Barcelona two years ago, I was struck by the minimal public commemoration of loyalist, let alone specifically anarchist, resistance to the right-wing insurrection, of the suffering of Barcelona’s residents during and after the civil war, and of the widespread revolutionary social transformation in the city and surrounding countryside before Franco’s victory.

To the south, in the Ebro Valley that experienced major wartime battles, I was equally surprised that a local civil war museum presented military paraphernalia and background information on both adversaries, with purposeful (and painful) equanimity. By such stances, no one is to blame; no historical judgments can be made.

Franco’s legacy of fear persists among politicians and much of the older population, while his political heirs are still strongly embedded in the bureaucracy, the judiciary, the military, and the police. The politics of history are quite alive and contentious in today’s Spain. The 1977 amnesty law remains in place.

While Judge Servini de Cabria’s recent decision from afar is, on the surface, a welcome public recognition of certain Franco regime crimes, it obviously causes only a small dent in the historical record and present day consciousness. It also tends to personalize and scandalize these practices of repression instead of indicting the whole regime, and of course does nothing for long-ago victims. At the most, international arrest warrants will be issued, preventing these aging defendants from traveling to certain destinations abroad.

However, the October 31 arrest warrants open a much richer conversation on the very nature of international human rights law generally. What is at stake here is the strong sense, ideal and practice of freedom without hierarchy vs. mere assertions of civil or legal human rights.

The latter are reformist, confined by arbitrary limits and framing by states with their own political agendas, procedural legalisms, and escape clauses (as with Bush and Obama defenses of torture and civilian bombing deaths in the Middle East).

While civil libertarians such as Roger Baldwin, founder of the ACLU in the 1920s, were inspired by and often supported anarchist Emma Goldman’s freedom campaigns, their efforts were within the restricted framework of constitutional and local laws, thus acknowledging in practice the legitimacy of judicial interpretations and the state in general.

Goldman rejected this approach because it is the very existence of the state and capitalism everywhere (along with other forms of hierarchical oppression) that limits human freedom.

In other words, while international human rights law, just like domestic civil liberties and civil rights cases, can sometimes cause restrictive barriers temporarily to fall and individual oppressors to be condemned, human rights discourse is reformist and statist by nature and serves to hide the overall inevitable reality of statist oppression. This actually adds to state stability.

We can easily identify with the concept of human rights victims and understand their efforts and those of their friends, families and supporters to leverage soft state tools, such as legal redress when available, against hard state persecution and repression.

Similarly, when trapped by the state, anarchists and other anti-authoritarians have usually accepted legal assistance in their own defense.

But we should remember, whether or not one agrees with specific strategy and tactics utilized in the Barcelona and broader Spanish context of the early 1970s, that Puig Antich and other anarchist revolutionaries risked their lives not to reform particular regimes, but to overthrow the entire system of hierarchical rule.

David Porter is author of Eyes to the South: French Anarchists and Algeria.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

