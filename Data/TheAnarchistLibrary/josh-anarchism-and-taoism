
Anarchism is usually considered a recent, Western phenomenon, but its roots reach deep in the ancient civilizations of the East. The first clear expression of an anarchist sensibility may be traced back to the Taoists in ancient China from about the sixth century BC. Indeed, the principal Taoist work, the Tao te ching, may be considered one of the greatest anarchist classics.

The Taoists at the time were living in a feudal society in which law was becoming codified and government increasingly centralized and bureaucratic. Confucius was the chief spokesman of the legalistic school supporting these developments, and called for a social hierarchy in which every citizen knew his place. The Taoists for their part rejected government and believed that all could live in natural and spontaneous harmony. The conflict between those who wish to interfere and those who believe that things flourish best when left alone has continued ever since.

The Taoists and the Confucians were both embedded in ancient Chinese culture. They shared a similar view of nature, but differed strongly in their moral and political views. They both had an attitude of respectful trust to human nature; the Christian notion of original sin is entirely absent from their thought. Both believed that human beings have an innate predisposition to goodness which is revealed in the instinctive reaction of anyone who sees a child falling into a well. Both claimed to defend the Tao or the way of the ancients and sought to establish voluntary order.

But whereas the Taoists were principally interested in nature and identified with it, the Confucians were more worldly- minded and concerned with reforming society. The Confucians celebrated traditionally ‘male’ virtues like duty, discipline and obedience, while the Taoists promoted the ‘female’ values of receptivity and passivity.

Although it has helped shape Chinese culture as much as Buddhism and Confucianism, Taoism by its very nature never became an official cult. It has remained a permanent strain in Chinese thought. Its roots lay in the popular culture at the dawn of Chinese civilization but it emerged in the sixth century BC as a remarkable combination of philosophy, religion, proto-science and magic.

The principal exponent of Taoism is taken to be Lao Tzu, meaning ‘old Philosopher’. He was born around 604 BC of a noble family in Honan province. He rejected his hereditary position as a noble and became a curator of the royal library at Loh. All his life he followed the path of silence — ‘The Tao that can be told is not the eternal Tao’, he taught. According to legend, when he was riding off into the desert to die, he was persuaded by a gatekeeper in north-western China to write down his teaching for posterity.

It seems likely however that the Tao te ching which is attributed to Lao Tzu, was not written until the third century BC. It has been called by the Chinese scholar Joseph Needham ‘without exception the most profound and beautiful work in the Chinese language’. The text consists of eighty-one short chapters in poetic form. Although often very obscure and paradoxical, it offers not only the earliest but also the most eloquent exposition of anarchist principles.

It is impossible to appreciate the ethics and politics of Taoism without an understanding of its philosophy of nature. The Tao te ching  celebrates the Tao, or way, of nature and describes how the wise person should follow it. The Taoist conception of nature is based on the ancient Chinese principles of yin and yang, two opposite but complementary forces in the cosmos which constitute ch’i (matter-energy) of which all beings and phenomena are formed. Yin is the supreme feminine power, characterized by darkness, cold, and receptivity and associated with the moon; yang is the masculine counterpart of brightness, warmth, and activity, and is identified with the sun. Both forces are at work within men and women as well as in all things.

The Tao itself however cannot be defined. it is nameless and formless. Lao Tzu, trying vainly to describe what is ineffable, likens it to an empty vessel, a river flowing home to the sea, and an uncarved block.’ The Tao, he asserts, follows what is natural. It is the way in which the universe works, the order of nature which gives all things their being and sustains them.

The great Tao flows everywhere, both to the left and the right. The ten thousand things depend on it; it holds nothing hack. It fulfils its purpose silently and makes no claim. (34)

Needham describes it not so much as a force, but as a ‘kind of natural curvature in time and space’.

Like most later anarchists, the Taoists see the universe as being in a continuous state of flux. Reality is in a state of process; everything changes, nothing is constant. They also have a dialectical concept of change as a dynamic interplay as opposing forces. Energy flows continually between the poles of yin end yang. At the same time, they stress the unity and harmony of nature. Nature is self-sufficient and uncreated; there is no need to postulate a conscious creator. It is a view which not only recalls that of the Greek philosopher Heraclitus but coincides with the description of the universe presented by modern physics. Modern social ecology, which stresses unity in diversity, organic growth and natural order, further reflects the Taoist world-view.

The approach to nature recommended by Lao Tzu and the Taoists is one of receptivity. Where the Confucian wants to conquer and exploit nature, the Taoist tries to contemplate and understand it. The Taoists’ traditionally ‘feminine’ approach to nature suggests that their way of thinking may well have first evolved in a matriarchal society. While at first sight it might seem a religious attitude, in fact it encouraged a scientific and democratic outlook amongst Taoists. By not imposing their own preconceptions, they were able to observe and understand nature and therefore learn to channel its energy beneficially.

The Taoists were primarily interested in nature but their conception of the universe had important corollaries for society. A definite system of ethics and politics emerges. There are no absolute Taoist values; for good and bad, like yin and yang, are related. Their interplay is necessary for growth, and in order to achieve something it is often best to start with its opposite. Nevertheless, an ideal of the wise person emerges in Taoist teaching who is unpretentious, sincere, spontaneous, generous and detached. For the Taoists, the art of living is to be found in simplicity, non-assertion and creative play.

Central to Taoist teaching is the concept of wu-wei. It is often translated as merely non-action. In fact there are striking philological similarities between ‘anarchism’ and ‘wu-wei’. Just as ‘an-archos’ in Greek means absence of a ruler, wu-wei means lack of wei, where wei refers to ‘artificial, contrived activity that interferes with natural and spontaneous development’. From a political point of view, wei refers to the imposition of authority. To do something in accordance with wu-wei is therefore considered natural; it leads to natural and spontaneous order. It has nothing to do with all forms of imposed authority.

The Tao te ching is quite clear about the nature of force. If we use force, whether physical or moral, to improve ourselves or the world, we simply waste energy and weaken ourselves: ‘force is followed by loss of strength’ (30). It follows that those who wage war will suffer as a result: ‘a violent man will die a violent death’ (42). By contrast, giving way is often the best way to overcome: ‘Under heaven nothing is more soft and yielding than water Yet for attacking the solid and strong, nothing is better; it has no equal. The weak can overcome the strong; the supple can overcome the stiff.’ (78) The gentle peacefulness  recommended by the Taoists is not a form of defeatist submission but a call for the creative and effective use of energy.

‘Practise non-action. Work without doing’ (63), Lao Tzu recommends. In their concept of wu-wei, the Taoists are not urging non-action in the sense of inertia, but rather condemning activity contrary to nature. It is not idleness that they praise, but work without effort, anxiety and complication, work which goes with and not against the grain of things. If people practised wu-wei in the right spirit, work would lose its coercive aspect. It would be undertaken not for its useful results but for its intrinsic value. Instead of being avoided like the plague, work would be transformed into spontaneous and meaningful play: ‘When actions are performed Without unnecessary speech, People say, “We did it!”’(17).

If people followed their advice, the Taoists suggest, they would live along life and achieve physical and mental health. one of their fundamental beliefs was that ‘whatever is contrary to Tao will not last long’ (55), while he who is filled with virtue is like a new-born child. In order to prolong their lives the Taoists resorted to yoga-like techniques and even alchemy.

The most important principle at the centre of their teaching however was a belief that ‘The world is ruled by letting things take their course. It cannot be ruled by interfering.’(48) The deepest roots of the Taoist view of wu-wei probably lies in early matriarchal society in ancient China. The Taoist ideal was a form of agrarian collectivism which sought to recapture the instinctive unity with nature which human beings had lost in developing an artificial and hierarchical culture. Peasants are naturally wise in many ways. By hard experience, they refrain from activity contrary to nature and realize that in order to grow plants they must understand and co-operate with the natural processes. And just as plants grow best when allowed to follow their natures, so human beings thrive when least interfered with.6 It was this insight which led the Taoists to reject all forms of imposed authority, government and the State. It also made them into precursors of modern anarchism and social ecology.

It has been argued that Taoism does not reject the State as an artificial structure, but rather sees it as a natural institution, analogous perhaps to the family. While the Tao te ching undoubtedly rejects authoritarian rule, it does read at times as if it is giving advice to rulers to become better at ruling:

If the sage would guide the people, he must serve with humility. If he would lead them, he must follow behind. In this way when the sage rules, the people will not feel oppressed (66)

Bookchin goes so far as to claim that Taoism was used by an elite to foster passivity amongst the peasantry by denying them choice and hope.

Certainly Lao Tzu addresses the problem of leadership and calls for the true sage to act with the people and not above them. The best ruler leaves his people alone to follow their peaceful and productive activities. He must trust their good faith for ‘He who does not trust enough will not be trusted.’ (17) If a ruler interferes with his people rather than letting them follow their own devices, then disorder will follow: ‘When the country is confused and in chaos, Loyal ministers appear.’ (18) In a well-ordered society,

Humans follow the earth.
Earth follows heaven.
Heaven follows the Tao.
Tao follows what is natural.(25)

However a closer reading shows that the Tao te ching is not concerned with offering Machiavellian advice to rulers or even with the ‘art of governing’. The person who genuinely understands the Tao and applies it to government reaches the inevitable conclusion that the best government does not govern at all. Lao Tzu sees nothing but evil coming from government. Indeed, he offers what might be described as the first anarchist manifesto:

The more laws and restrictions there are,
The poorer people become.
The sharper men’s weapons
The more trouble in the land.
The more ingenious and clever men are,
The more strange things happen.
The more rules and regulations,
The more thieves and robbers.

Therefore the sage says:
I take no action and people are reformed.
I enjoy peace and people become honest.
I do nothing and the people become rich.
I have no desires and people return to the good and simple life.(57)

Contained within the marvellous poetry of the Tao te ching, there is some very real social criticism. It is sharply critical of the bureaucratic, warlike and commercial nature of the feudal order. Lao Tzu specifically sees property as a form of robber: ‘When the court is arrayed in splendour, The fields are full of weeds, And the granaries are bare.’(53) He traces the causes of war to unequal distribution: ‘Claim wealth and titles, and disaster will follow ’(9) Having attacked feudalism with its classes and private property, he offers the social ideal of a classless society without government and patriarchy in which people live simple and sincere lives in harmony with nature. It would be a decentralized society in which goods are produced and shared in common with the help of appropriate technology. The people would be strong but with no need to show their strength; wise, but with no presence of learning; productive, but engaged in no unnecessary toil. They would even prefer to reckon by knotting rope rather than by writing ledgers:

A small country has fewer people.
Though there are machines that can work ten to a hundred
times faster than man, they are not needed.
The people take death seriously and do not travel far.
Though they have boats and carriages, no one uses them.
Though they have armour and weapons, no one displays them.
Men return to the knotting of rope in place of writing.
Their food is plain and good, their clothes fine but simple, their homes secure;
They are happy in their ways.
Though they live within sight of their neighbours,
And crowing cocks and barking dogs are heard across the way,
Yet they leave each other in peace while they grow old and die.(80)

The anarchistic tendency of the Taoists comes through even stronger in the writings of the philosopher Chuang Tzu, who lived about 369–286 BC. His work consists of arguments interspersed with anecdotes and parables which explore the nature of the Tao, the great organic process of which man is a part. It is not addressed to any particular ruler. Like the Tao te ching, it rejects all forms of government and celebrates the free existence of the self-determining individual. The overriding tone of the work is to be found in a little parable about horses:

Horses live on dry land, eat grass and drink water. When pleased, they rub their necks together. When angry, they turn round and kick up their heels at each other. Thus far only do their natural dispositions carry them. But bridled and bitted, with a plate of metal on their foreheads, they learn to cast vicious looks, to turn the head to bite, to resist, to get the bit out of the mouth or the bridle into it. And thus their natures become depraved.

As with horses, so it is with human beings. Left to themselves they live in natural harmony and spontaneous order. But when they are coerced and ruled, their natures become vicious. It follows that princes and rulers should not coerce their people into obeying artificial laws, but should leave them to follow their natural dispositions. To attempt to govern people with manmade laws and regulations is absurd and impossible: ‘as well try to wade through the sea, to hew a passage through a river, or make a mosquito fly away with a mountain!’. In reality, the natural conditions of our existence require no artificial aids. People left to themselves will follow peaceful and productive activities and live in harmony with each other and nature.

In an essay ‘On Letting Alone’, Chuang Tzu asserted three hundred years before Christ the fundamental proposition of anarchist thought which has reverberated through history ever since:

’There has been such a thing as letting mankind alone; there has never been such a thing as governing mankind. Letting alone springs from fear lest men’s natural dispositions be perverted and their virtue left aside. But if their natural dispositions be not perverted nor their virtue laid aside, what room is there left for government?

The Taoists therefore advocated a free society. without government in which individuals would be left to themselves. But while pursuing their own interests, they would not forget the interests of others. It is not a sullen selfishness which is recommended. The pursuit of personal good  involves a concern for the general well-being: the more a person does for others, the more he has; the more he gives to others, the greater his abundance. As the Taoist text Huai Nan Tzu put its, ‘Possessing the empire’ means ‘self-realization. If I realize myself then the empire also realizes me. If the empire and I realize each other, then we will always possess each other.’

Human beings are ultimately individuals but they are also social beings, part of the whole. Anticipating the findings of modern ecology, the Taoists believed that the more individuality and diversity there is, the greater the overall harmony. The spontaneous order of society does not exclude conflict hut involves a dynamic interplay of opposite  forces. Thus society is described by Chuang Tzu as

an agreement of a certain number of families and individuals to abide by certain customs. Discordant elements unite to form a harmonious whole. Take away this unity and each has a separate individuality... A mountain is high because of its individual particles. A river is large because of its individual drops. And he is a just man who regards all parts from the point of view of the whole.

Taoism thus offered the first and one of the most persuasive expressions of anarchist thinking. Its moral and political ideas were firmly grounded in a scientific view of the world. Although Taoist philosophy (Tao chia) contains Spiritual and mystical elements, the early Taoists’ receptive approach to nature encouraged a scientific attitude and democratic feelings. They recognized the unity in the diversity in nature and the universality of transformation. In their ethics, they encouraged spontaneous behavior and self-development in the larger context of nature: production without possession, action without self-assertion and development without domination. In their politics, they not only urged rulers to leave their subjects alone and opposed the bureaucratic and legalistic teaching of the Confucians, but advocated as an ideal a free and co-operative society without government in harmony with nature.

Taoism was not aimed by an elite at peasants to make them more docile and obedient. The Taoists social background tended to be from the small middle class, between the feudal lords and the mass of peasant farmers. Nor were they merely offering advice on how to survive in troubled times by yielding to the strong, keeping a low profile, and by minding their own business. on the contrary, Taoism was the philosophy of those who had understood the real nature of temporal power, wealth and status, sufficiently well to find them radically wanting. Far from being a philosophy of failure or quietude, Taoism offers profound and practical wisdom for those who wish to develop the full harmony of their being.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



The great Tao flows everywhere, both to the left and the right. The ten thousand things depend on it; it holds nothing hack. It fulfils its purpose silently and makes no claim. (34)



Humans follow the earth.
Earth follows heaven.
Heaven follows the Tao.
Tao follows what is natural.(25)



The more laws and restrictions there are,
The poorer people become.
The sharper men’s weapons
The more trouble in the land.
The more ingenious and clever men are,
The more strange things happen.
The more rules and regulations,
The more thieves and robbers.


Therefore the sage says:
I take no action and people are reformed.
I enjoy peace and people become honest.
I do nothing and the people become rich.
I have no desires and people return to the good and simple life.(57)



A small country has fewer people.
Though there are machines that can work ten to a hundred
times faster than man, they are not needed.
The people take death seriously and do not travel far.
Though they have boats and carriages, no one uses them.
Though they have armour and weapons, no one displays them.
Men return to the knotting of rope in place of writing.
Their food is plain and good, their clothes fine but simple, their homes secure;
They are happy in their ways.
Though they live within sight of their neighbours,
And crowing cocks and barking dogs are heard across the way,
Yet they leave each other in peace while they grow old and die.(80)



Horses live on dry land, eat grass and drink water. When pleased, they rub their necks together. When angry, they turn round and kick up their heels at each other. Thus far only do their natural dispositions carry them. But bridled and bitted, with a plate of metal on their foreheads, they learn to cast vicious looks, to turn the head to bite, to resist, to get the bit out of the mouth or the bridle into it. And thus their natures become depraved.



’There has been such a thing as letting mankind alone; there has never been such a thing as governing mankind. Letting alone springs from fear lest men’s natural dispositions be perverted and their virtue left aside. But if their natural dispositions be not perverted nor their virtue laid aside, what room is there left for government?



an agreement of a certain number of families and individuals to abide by certain customs. Discordant elements unite to form a harmonious whole. Take away this unity and each has a separate individuality... A mountain is high because of its individual particles. A river is large because of its individual drops. And he is a just man who regards all parts from the point of view of the whole.

