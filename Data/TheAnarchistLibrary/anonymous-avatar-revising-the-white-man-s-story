      Changing Priorities for a Changing Empire      The Chosen One
An anarchist critique of Hollywood’s sympathy for the indigenous,
dedicated to the 7 CIA agents blown to bits in Afghanistan.

I just got back from fulfilling a holiday obligation of going to the theater with my family, my head brimming with thoughts for an essay, only to find that James Cameron’s new flick, Avatar, has already provoked a good deal of writing on the anarchist news sites. The one appreciative article, Avatar: An Anarcho-Primitivist Picture of the History of the World sadly speaks for itself, when placed alongside the actual movie, in demonstrating a common and longstanding criticism aimed at a large part of anarcho-primitivist thought.

More on the mark is When will white people stop making movies like Avatar [1] , which analyzes the pattern of white guilt in Avatar and other movies such as Dances with Wolves and The Last Samurai.

Because the angle of white guilt has already been well covered, I want to explore how movies like Avatar constitute a revision of the global colonial narrative that does not seek to amend the past but to help create the ideological basis for changing forms of social control that are becoming necessary in the present and will be even more necessary in the future.

Some things have never changed. Western colonizers have always interpreted and portrayed gender relations in indigenous societies in a way that is convenient to Western political myths, or they have simply been blind to difference and have seen Western-style patriarchy wherever they have looked. This is reflected minutely in Hollywood’s production. In Dances with Wolves, which, remember, was made before the girl power fad modified women’s role in the consumer imagination, women appeared in traditionally limited roles, when they appeared at all. In Avatar, women can kick ass, so long as they are skinny, sexually available, and always one step behind their men, never assuming the role of protagonist. In other words, the creators of Avatar could imagine stunningly original flora and fauna for their fictitious alien planet, but they could not imagine gender relations any different from the ones that prevail in the West.

What has changed, strikingly, since the John Wayne golden days of Cowboy and Indian films, is that nowadays the natives are portrayed as the good guys. Progressives mistake this for an end to colonial attitudes, but it has been amply pointed out that indigenous characters, or their alien stand-ins, as in Avatar or Dune, are never allowed to tell their own stories; rather they must be rescued by white men (or humans) and thus assimilated as peripheral characters in someone else’s narrative. The system, personified in film by white men, never relinquishes the power of determining outcomes.

Nowadays, the whole world has long since been colonized. Wars of conquest, to the progressive elements of the power structure, are passe. Even the invasions of Iraq and Afghanistan were not strategically necessary, they were only strategically desirable for conservative elements in the power structure. The same ends could have been accomplished more slowly but more wholly with strictly economic means.

The principal task of colonization these days is consumption. Movie heroes learn from indigenous societies while real-life scientists patent indigenous genomes and traditional plants, and green capitalism sells representations of indigenous spirituality to cover up its misery and provide a comfortable substitute for fighting back against alienation and ecocide. Here it’s worth mentioning Blueberry, a horrible 2004 movie in which a white sheriff is chosen to learn native secrets in order to save their spiritual system from other Westerners seeking to exploit it. Half of the movie is an absurd, computer-generated peyote trip that crudely signifies some vague kind of spiritual journeying. No doubt the director thought it to be some kind of homage to a generic Native American wisdom.

One of the newer developments we see in Avatar is a critique of extractive industries. The humans don’t want to kill the blue people for land, but for a fuel source, the clumsily named “nonobtanium” that sits beneath the natives’ homeland. Thirty years ago, this might have been more radical. But nowadays, petroleum is peaking, and the future of capitalism lies in nuclear and solar energy. In the future the US will not need the Middle East or Nigeria. All the uranium and sunlight the economy will need can be got from within North America. As for transportation fuels, most of the growing of biofuels will certainly take place in the Global South, but there will be no need to invade any country to keep the supplies flowing. Unlike oil, nearly any place can produce ethanol or soybeans, and impoverished countries can be played off against one another to lower the prices. In such an economic climate, humanitarian solutions will be far more necessary than military solutions.

The progressive humanitarianism evidenced by Hollywood does not mean they are loosening their reins on ideological production or thinking about ushering in a more liberated society. They are simply changing their strategies for how to save capitalism from the next set of crises it has created.

A common Hollywood archetype is The Chosen One. This figure appears in Avatar, Dune, The Last Samurai, The Matrix, Star Wars, and other films. Upon examination we see that this figure does not correlate with films that portray indigenous people, but rather with films in which the protagonist becomes a hero by ethically breaking the rules and fighting against authority. Americans love an underdog story, a story about a rebel, precisely because ours is such a sycophantic, obedient society. It was no coincidence that the previews before the movie included an ad for a TV series on the US Marshals and an ad for the National Guard, both of which, aesthetically, would have made Goebbels proud. The National Guard ad flashed a number of core values onto the screen, the first of which was loyalty. And then, with no dissonance, the audience went on to watch the movie and sympathize with a protagonist who kills a bunch of Marines.

The Chosen One is a device that creates a mystical or even religious space around imagined acts of disloyalty. The audience can consume stories of rebellion without any danger to the system because in the movies rebellion must always be sanctified by a higher power. It would not be rebellion if the political authority itself were authorizing the acts of treason, so instead it must be a mystical and invisible force, or a plot device that is never fully explained; in any case something that will never appear in the lives of the audience members. There certainly was no higher authorization for the person in Afghanistan who recently sacrificed his life to blow up 7 torturers with the CIA, at the same time as US audiences were cheering on Michelle Rodriguez’s character for engaging in a suicidal game of chicken in order to slow down the invading Marines. But there’s no contradiction here because there is no oil or nonobtanium in Afghanistan, we’re not aware of any pristine forests there, and the native inhabitants just don’t live up to the idealized, noble savages we’re used to seeing on the silver screen.

As long as people in this society remain too cowardly to choose themselves, to grant themselves the legitimacy to rebel, movies like Avatar will only train Western movie-goers to sympathize with the oppressed, who, in the movie-goers’ imaginations, are unable to free themselves, and are waiting for a savior who looks like them. As climate change and energy crises cause starvation on an ever increasing scale among the peoples who must be hyper-exploited to maintain the standard of living in the wealthy countries, this progressive white savior mentality is just what capitalism needs. The future has been reproduced.

 
[1] io9.com




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



An anarchist critique of Hollywood’s sympathy for the indigenous,
dedicated to the 7 CIA agents blown to bits in Afghanistan.

