      “She never stopped thinking, questioning, and learning.”
As part of the Seattle anarchist community’s 2006 celebration of the 1936–1939 Spanish Revolution, we want to remember Pura Pérez Benavent Arcos.  She was for us one living link with that popular resistance to authoritarian domination, and a link with the positive self-organized collective activity which flowered during that struggle.

Seventy years ago, on July 19, 1936, anarchists, joined by very many other freedom-loving people in Spain, rose up to meet the threat of military dictatorship.  Their fight quickly became a revolution which threatened capitalist, Fascist and Communist power-holders alike.  The imagination, courage and achievements of the ordinary people of Spain inspire us today in our own struggle to become free, whole people.

Through her spoken and written words, and through her personal kindness and hospitality to those of us who came to visit in Windsor, Ontario, Pura helped to perpetuate the traditions of anti-authoritarian resistance for younger anarchists living outside the Detroit-Windsor area, as well as those who knew her there.  Through her memories of the positive experiences of the Revolution, and her critical perspectives on the events and the relationships between those who were then fighting for a new world, Pura helped us to feel the continuity between their struggle and ours.  We remember her with love and respect as one of our spiritual ancestors.  And we miss her.

— Sylvie Kashdan and Robby Barnes

The following appeared in the Spring, 1996 issue of The Fifth Estate (P.O. Box 201016, Ferndale, MI 48220).  We thank all of the staff of The Fifth Estate (FE) for sharing their memories of Pura which we reprint here.  We have made some additions and clarifications, in brackets, for those in our community who are unfamiliar with the Spanish anarchist movement. 

On October 12, 1995, our community lost another elder and member of a generation of anarchist revolutionary veterans now passing into history.

Purificación Pérez Benavent (Pura Arcos), companion of FE staff member Federico Arcos, was born June 26, 1919 in Valencia, Spain.  She later moved to Barcelona.

[Both her father and grandfather were transport workers and members of the CNT, Confederación Nacional del Trabajo, National Confederation of Labor, an anarcho-syndicalist trade union federation.  Pura grew up hearing discussions about social injustice, and the need for improving life for all people.]

[Although most girls and women in Spain did not go to school, or even learn to read and write, before the 1930’s, Pura was very interested in obtaining an education.  She prevailed on her parents to allow her to attend elementary school with an older cousin who was living with them.  Most working class children who attended school left when they were 11 or 12, but, Pura was eager to learn whatever she could, and determined to continue attending school.]  A very bright and promising student, Pura went to work at age thirteen to contribute to her family’s income while attending night school at the Escuela Moderna.  [In the Escuela Moderna or “modern school” students were not forced to learn lessons by rote, but were able to learn by pursuing their interests.  There Pura met other young people who were concerned about social issues, and became involved in the anarchist movement.]

Pura was very active with the local federation of the Barcelona Mujeres Libres [Free Women], and became a member of the organization’s national sub-committee.  [In 1936, groups of women in Madrid and Barcelona founded Mujeres Libres, an organization dedicated to the liberation of women from their “triple enslavement through ignorance, through the traditional social subordination of women, and through their exploitation as workers.”  During the Spanish Revolution Mujeres Libres mobilized over 20,000 women and developed an extensive network of activities to help individual women grow, realize their full potential and fully participate in creating a revolutionary anarcho-communitarian society.]

She participated in the sub-comités of branches of the CNT, FAI [Federación Anarquista Iberica, the Iberian Anarchist Federation] and the Federación iberica de juventudes libertarias (the Libertarian Youth) [the anarchist youth organization], often serving as recording secretary to meetings.  She trained to drive a streetcar, but never did.  In 1936 she met Federico Arcos in the Libertarian Youth.  They lived half a block away from each other and would walk home together from the Ateneo Libertario [an anarchist storefront school and cultural center].

Pura was not happy with the attitudes and methods of the so-called leaders, the militantes responsables of the comités, and when the opportunity arose to teach children at one of the collectives, in Tabernes de Valldigna, she accepted.  Emma Goldman once said, “To see the Revolution you had to go to the towns and villages...  It is in Madrid, Valencia, and Barcelona where my heart sinks.”  As Federico remarks, “Life in such collectives was like a big family.  Everyone was devoted to each other and to their ideals.”  Pura found happiness among the people of Tabernes.

When defeat of the Revolution came, instead of attempting to flee the country, Pura went into hiding.  Later, she moved back to Barcelona with her family.  She remained in hiding for two years until things calmed down.  If the fascists had found her, she would have been one more in the long list of those executed.  Pura spent the following years supporting libertarian prisoners.

In the 1950s, Pura emigrated to Canada with their young daughter to join Federico.  There she learned English, worked as a nurse, and collaborated by correspondence with the libertarian press, particularly the Boletín de Mujeres Libres (edited in London from 1963 to 1977), and with the magazine Mujeres Libertarias [Libertarian Women], until its demise in 1993.  She carried on an enormous correspondence with friends and comrades in Europe and the Americas, and supported Canadian women’s and peace groups as well.

Pura will also be fondly remembered for her beautiful dried flower arrangements.  It was a skill she exercised for many years, even overcoming a period of blindness and returning to her art.  Her work was displayed in many handicraft shows and won several awards, and was frequently given in our own community to observe birthdays, births, marriages and other festive occasions.  Pura’s framed flowers can be found in innumerable households all over the world.

At a crowded memorial to Pura, FE staff member Marilynn Rashid’s remarks spoke for us all:

“Pura was a very unique, strong and intelligent human being, and I feel honored to have known her and to have spent time in her kitchen eating her paella and listening to her stories...  As all their friends, knew, Pura always tempered Federico’s romantic memories of the Spanish Revolution with her very sharp, opinionated realism.

“Pura was a woman who, because of her Involvement in the Mujeres Libres and the Anarchist cooperatives, knew all about the potential for true liberation and for sexual equality.  And so she knew and understood, too, the frustrations we all experience...  the failure of those projects to achieve the expectations we have for them.  Federico, for example, would describe with his characteristic fervor a meeting of the anarchists with Durruti, and Pura would remind us of the women in the back rooms working and cooking to feed all the men.  One story did not negate the other:  rather, each enriched the other and made us understand the complexity of that time.

“I appreciated Pura’s wisdom, her sharp wit, her strong clear memory.  She ...  had a consistent love for poetry, for literature...  She was very proud of her family, devoted to her daughter and grandsons and to her infant great-granddaughter.  Pura was a very humble and unassuming woman who lived her life, fulfilled her ideals, and never stopped thinking and questioning and learning.  It is with love and profound respect that we will remember her.”

Poem

Quisiera ser la rosa
más perfumada y hermosa
que en tus rosales hubiera.
Ser para tí inmaterial
sin clamores de pecado,
ser el beso inmaculado
ensueño de tu Ideal.

Pura, 1942

Las Flores y El Arte, a Pura, con Cariño 
by Sara Berenguer (1991)

Cada pétalo, cada flor,
por su forma y lozanía,
nos llenan el corazón
de manifesta alegría.
Los tonos de cada flor,
por su color y armonía
son gamas para el pintor
y sus diferentes matices
completan la policromía.
Las flores son el amor,
el candor de cada día,
las hay modestas, chiquitas
y no por ser más bonitas
llegan más al corazón.
Los hay que ven en ellas,
luz de encanto y estrellas
y las formas concebidas
por madre naturaleza
les dan cierto resplandor
de finura y de belleza.
Como los minuciosos manojos
de con tanta galanura
y sutil delicadeza
nos compone, nuestra Pura.

The Song Of Mujeres Libres
Lucia Sanchez Saornil, Valencia 1937

Puño en alto mujeres de Iberia
hacia horizontes preñados de luz
por rutas ardientes,
los pies en la tierra
la frente en lo azul.
Afirmando promesas de vida
desafiamos la tradición
modelemos la arcilla caliente
de un mundo que nace del dolor.
— que el pasado se hunda en la nada!
— Qué nos importa del ayer!
Queremos escribir de nuevo
la palabra mujer.
Puño en alto mujeres del mundo
hacia horizontes preñados de luz,
por rutas ardientes
adelante, adelante
de cara a la luz.

Fists upraised, women of Iberia
toward horizons filled with light
paths afire
feet on the ground
face to the blue sky.
Affirming the promises of life
we defy tradition
Let us mold the warm clay
of a new world born of suffering.
Let the past crumble into oblivion!
What is yesterday worth to us!
We want to write anew
the word woman.
Fists upraised, women of the world
toward horizons filled with light
on paths afire
onward, onward
toward the light.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



Poem


Quisiera ser la rosa
más perfumada y hermosa
que en tus rosales hubiera.
Ser para tí inmaterial
sin clamores de pecado,
ser el beso inmaculado
ensueño de tu Ideal.


Pura, 1942


Las Flores y El Arte, a Pura, con Cariño 
by Sara Berenguer (1991)


Cada pétalo, cada flor,
por su forma y lozanía,
nos llenan el corazón
de manifesta alegría.
Los tonos de cada flor,
por su color y armonía
son gamas para el pintor
y sus diferentes matices
completan la policromía.
Las flores son el amor,
el candor de cada día,
las hay modestas, chiquitas
y no por ser más bonitas
llegan más al corazón.
Los hay que ven en ellas,
luz de encanto y estrellas
y las formas concebidas
por madre naturaleza
les dan cierto resplandor
de finura y de belleza.
Como los minuciosos manojos
de con tanta galanura
y sutil delicadeza
nos compone, nuestra Pura.


The Song Of Mujeres Libres
Lucia Sanchez Saornil, Valencia 1937


Puño en alto mujeres de Iberia
hacia horizontes preñados de luz
por rutas ardientes,
los pies en la tierra
la frente en lo azul.
Afirmando promesas de vida
desafiamos la tradición
modelemos la arcilla caliente
de un mundo que nace del dolor.
— que el pasado se hunda en la nada!
— Qué nos importa del ayer!
Queremos escribir de nuevo
la palabra mujer.
Puño en alto mujeres del mundo
hacia horizontes preñados de luz,
por rutas ardientes
adelante, adelante
de cara a la luz.


Fists upraised, women of Iberia
toward horizons filled with light
paths afire
feet on the ground
face to the blue sky.
Affirming the promises of life
we defy tradition
Let us mold the warm clay
of a new world born of suffering.
Let the past crumble into oblivion!
What is yesterday worth to us!
We want to write anew
the word woman.
Fists upraised, women of the world
toward horizons filled with light
on paths afire
onward, onward
toward the light.

