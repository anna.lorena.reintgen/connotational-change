
Kant tried through his philosophy to provide a basis for his Christian altruist upbringing, Ayn Rand did the same, but against her Communist upbringing, i.e. in favor of capitalist "egoism." In The Objectivist, printed September 1971, Rand called Kant the "most evil man" in history. This is an assertion I can only view with ironic derision, given the content of their systems, but since Rand is an excellent example of the faults rational egoism ("egoism" that springs from the treatment of myself as only a concept) is necessarily subject to, her shoddy thinking starkly juxtaposes the sagacity of Kant, and is useful here in the way that a pitch blackness seems to emphasize whatever illuminates it. In actual fact, Rand and Kant accomplished intellectual ends diametrically opposed to the principles they sought to preserve at the outset. The division of objects into phenomena and noumena furnished Stirner (through Hegel) with precisely the tools he needed to mine "spooks" from the mind, while Ayn Rand's Objectivism devolves into nothing more than moralistic hero-worship: a call to the weak to serve the strong. To the end of showing this, it will be helpful to embark on a further exploration of the nature of a "spook."

Stirner calls the spook a "fixed idea" in The Ego and Its Own. A spook at its simplest is any notion that one will defend as greater than oneself, that "passes into our stubbornness." To him, the process for their coming into power was a simple one: people just believe in something that isn't there, hence the name, "spook." But what he means by "fixing" an idea is, as we can see from his examples (God, the State, Mind, etc.), a process of logical subterfuge that transforms what was formerly merely a conception into an intelligible object in the Kantian sense, i.e., an overawing entity that exists in spite of you, your perception of it, or any other particular egoist. The ontological proof of God is a splendid example of this process. God is "a being than which no greater can be conceived," and since he would be made greater by existing, he must exist. By distinguishing the empty things in themselves (noumena) from phenomena, which contain sense (i.e. content relating to perception), Kant showed that such an argument is tautological. The conception of God in this case already assumes its own existence, since God, as assumed as the superlative totality, must already contain the notion of existence. The entirety of the content is given in the premise, the proof is mere logical formalism, and can prove nothing empirical. It develops from what is absolutely notional, i.e. from a form of thought that applies only to thoughts themselves (God would in this case be a conception of a conception, an idea, and so only have reference to notions), a conception that pretends to apply to all phenomena. If we say, "If God were to exist, he would be a being than which no greater can be conceived," this renders the proof honest, but shows it explicitly as worthless circularity.

This criticism of confusing the ideal with the real applies equally well to all spooks. In terms of pre-Christian spooks (e.g. Plato's forms), the real was idealized (another conception I have borrowed from The Ego and Its Own), which lead to the absolute denigration of human perception. The Forms were the true reality, which humans could not perceive because of our faulty faculties. But they were simply the "real" to which humans could not attain through perception, and so essentially the norm, while humans were considered as severely flawed. Post-Christian spooks are realized ideals (instead of idealized reals), so they involve the belief in a supremely great power that dominates the "normal" individual, as in the ontological case. The end result is, however, the same. I am to be subjected to a notion, whether it is because I am basically feeble or because the notion is basically great makes no material difference to the spook or to me, if I am possessed.

Now to Rand's nonsense. I will begin with her meretricious premise, "A = A." Wittgenstein has already shown this (and any similar construction) to be a trick of superfluity, so to paraphrase: the law of identities applies only to cosmetically distinct conceptions (as in the case "A = B"), since "A" must contain the entire conception of itself, and so the self-equation is pointlessly tautological. Her subterfuge lies in what she deems to follow from "A = A," that "existence exists." To her, this means "existence is selfsame" (she sought to replace "A" in the above equation with "Existence"), but the logical meaning, since "exists" functions as a predicate of its noun-form subject "existence," is quite different.

The meaning of "existence exists," when we expand the terms, is: "the state of having objective reality has objective reality." In other words, I may say her meaning is "existence is a sensuous object." But if existence itself is a particular object to which the general category of itself can be applied, then this particular object would contain all particular sensuous objects to which its category may be applied. This would mean that a particular object could be multiple, distinct particular objects at one point in time: an intuitively palpable contradiction. Since "existence exists" and "A = A" are the fundamental premises of her philosophy, it must rise or fall with these, and it has most assuredly fallen. It is also clear now why her philosophy degenerates into hero-worship. Existence is made into "the Cause" (Stirner's conception), so it is the goal for all that exists, including individual humans. The Randian hero is he who exists "the most," that is, makes the materially largest mark on the Earth. This is why she treats bourgeois capitalists with such reverence (she employs the appropriative understanding of property, also based in her system on "A = A," so she believes that the legal "owners" serve a necessary material function) while those who have no claim on production are to her moral scum. The greatest travesty she ever committed was writing twelve hundred pages of platitudinous capitalist onanism on a trivial philosophy that could be so quickly summarized.

But it is nevertheless remarkable that "A = A" can be simply rephrased as "You are You," and so be used to describe the ideology of capitalism. The cunning bourgeois understands that this alone is enough, and more, because of the amphiboly that is the word "You." The particular sense is the subject of the proposition (the corporeal you), while the linguistic ideal is the predicate. "You" are transformed into "You," i.e. your ideal of yourself is to be your holy Cause.

This is the total function of all advertising. "You are of course your particular self, but to really be You, the best You you can be, you have to buy my product. Don't you want to be the real You?" The product, interestingly, can be an idea (since the "real You" in this case is nothing but an idea to be fixed, but still subsumes within it all particular individuals) or a material object. This is the peculiarity of capitalist ideology that distinguishes it from all others, which Debord exposed in Society of the Spectacle. "You are You" is the only premise of absolute capitalism, "I" in the case of the person flimflammed, and since you can annihilate any idea, capitalism is likewise ideologically augmented by the totality of the bourgeoisie (the legal monopolizers of all forms of capital) through their "sterilization" of revolutionary ideas and incorporation of them into the advertising schema as products to be acquired to complete the "real You."

This capitalist insatiability, which devours the sustained appearance of "internal contradictions" so necessary to dialectic materialism, derives from the reverse logical mechanism of that employed in advertising. The penetration of capital into all areas of life has created manifold types of bourgeois persons. Celebrities, for instance, are the owners of the means to produce one sort of social capital, that of the ideal image, which exists in themselves on screen, while film producers own the means to produce the means to produce the ideal image (i.e. they create celebrities). Of all the spectacular images that assist in the maintenance of state capitalism, the most essential is that of pure competition. The more superficial divisions there are among the bourgeoisie as a totality, the more competition arises (apparently), the more capitalism is supposed to be functioning according to its plan. But the more superficial realms of competition there are, the more the statists must interfere in order to keep the competition "fair," i.e., from undermining itself or the social position of its bourgeois companions. This is the unacknowledged content of the various self-inflicted public disgraces concocted by the high bourgeoisie (e.g. scapegoating Harvey Weinstein) in order to appease the bleating reformists. "Well, this particular person may have abused his position as a legally codified exploiter, but that doesn't mean there's anything wrong with striving to become the best manipulator. He obviously failed to achieve that goal."

Whoever can bring the many machines of image propagation, and with them, the mob, onto their side at the appearance of the latest internal contradiction (and so control the means of sterilization) will inevitably be the victor, and will be able to rearrange the spectacular image as they see fit. The efficacy of state capitalist notions rests not on the supposed authority of one person, or group, but on mass belief in the abstract Self. The state is nobody, and everybody.

Now I hope it is absolutely clear that the basic Kantian distinction is of self-benefitting utility. But Kant gave more to Stirner than this. Kant, though he tried, could never escape his profound uniqueness: the "definitive eigenheit" of a genius. The foundation of his philosophy is the "unity of apperception." He describes it: "the manifold representations which are given in intuition would not all of them be my representations, if they did not belong to one self-consciousness, because otherwise they would not all without exception belong to me."[1] This can be generously read as an assertion of the conscious egoist's power over his own thoughts, in that the command of them is necessary to properly analyze them: something a possessed man cannot do, at least, to his spook, since analysis is the breaking apart of a conception on the basis of its logical relation to other concpetions. I say "generous," because logically speaking, his statement is a tautology in the form of, "I own what belongs to me." His philosophy would be subject to the same criticism as Rand's, if he did not admit the tautological character of its foundation.

The greatest service performed by Kant in the destruction of all iterations of those ideologies that treat I myself as an abstract a priori conception (all presentations of rational egoism) is the deflation of what he called "rational psychology." This is based upon his (Kant's) own understanding that:

"The unity of consciousness, which lies at the basis of the categories, is considered to be the intuition of the subject as an object; and the category of substance is applied to the intuition. But this unity is nothing more than the unity in thought, by which no object is given; to which the category of substance - which always presupposes a given intuition - cannot be applied. Consequently, the subject cannot be cognized. The subject of the categories cannot, therefore, for the very reason that it cogitates these, frame any conception of itself as an object of the categories; for, to cogitate these, it must lay at the foundation its own pure self-consciousness - the very thing that it wishes to explain and describe."[2]

Whether the Kantian categories are the universally applicable bases of understanding is here immaterial to the question. The philosophers that invariably use the conception of substance (a necessarily empirical conception) as a method for proving the soul commit a formal petitio principii regardless of the objective validity of the latter or former conception. By treating the abstract Self as an object, and from this proceeding on a basis of internally sensible confirmation, the character of the abstract Self is assumed at the outset as the foundation for its own supposedly objective understanding. The subject cannot be objectively determined by itself, nor can it be totally determined through its own internal sense, so we arrive at the seeming impasse of a necessarily undetermined constitutive self-conception, which, to Kant, meant that the soul was beyond the scope of human cognition, and that it functioned only as a hard limitation of this process.

But does this mean what Kant wanted it to, namely, that there is a soul, but that it exists beyond our ability to conceive it, because we are but poor worms in the face of the "being of all beings," God? Certainly not; what I necessarily cannot conceive of, is no detriment to my conceptual abilities, simply because in no circumstances could I conceive of it. That I will never know myself as only a concept, or only an object, is no problem with my thought, for I am neither the absolute one nor the other. This is exactly the line of reasoning that prefigures Stirner's "unique." While Kant saw our reason and understanding here as deficient, perhaps due to his Christian childhood, Stirner was conscious of no such deficiencies. That I am not a subject nor an object does not show that I am less than the two, but that I am both, and more: I may take possession of whatever concepts or objects I choose. But here we come to the end of conceptuality, of proofs, and of reason, which the empty unique is.

I think I'll go for a walk.
[1] Critique of Pure Reason, Transcendental Logic, Chapter Twelve
[2] Critique of Pure Reason, Transcendental Dialectic, Chapter One, subsection "Refutation of the Argument of Mendelssohn for the Substantiality or Permanence of the Soul"




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

