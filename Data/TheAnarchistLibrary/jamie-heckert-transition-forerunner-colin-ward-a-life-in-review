
Colin Ward’s writing, spanning three decades and twenty-seven books, focus on recognising the low-input alternatives which already exist and which can help inspire transitions to more resilient and sustainable cultures. His work also shares a number of key concerns and ideas about organisation with Permaculture — the ethical design system which underlies the Transition movement.

Throughout his career as an architect, educationalist, planner and writer, Colin Ward expressed an enduring commitment to sustainability, freedom and equality (in permaculture terms: Earth care, people care and fair shares). He also wrote directly about key concerns of the Transition movement — the importance of allotments (The Allotment: Its Landscape and Culture [1988]), critiques of industrial agriculture, the unequal distribution of land and the need for sustainable housing (Cotters and Squatters: The Hidden History of Housing [2004]), car culture (Freedom to Go: After the Motor Age [1991]), and the uses and abuses of natural resources (Reflected in Water: a Crisis of Social Responsibility [1997]) as well as documenting rarely told British histories of self-organised non-profit communal spaces (Goodnight Campers! The History of British Holiday Camps [1986] and Arcadia for All [2003], both co-written with Prof. Dennis Hardy). Ward was also editor of BEE (Bulletin of Environmental Education) from 1971 to 1979.

Clearly a social critic and keen observer, Colin Ward was also committed to designing alternative solutions both through engaging with the official planning system and through people working out together, freely and co-operatively, their own solutions. This passage from Cotters and Squatters demonstrates the permaculture design principles of using small and slow solutions, integrating rather than segregating and using and valuing renewable resources:

Fifty years of subsidies had made the owners of arable land millionaires through mechanised cultivation and, with a crisis of over-production; the European Community was rewarding them for growing no crops on part of their land. However, opportunities for the homeless poor were fewer than ever in history. The grown-up children of local families can’t get on the housing ladder. [...]There should be some place in every parish where it’s possible for people to build their own homes, and they should be allowed to do it a bit at a time, starting in a simple way and improving the structure as they go along. The idea that a house should be completed in one go before you can get planning permission and a mortgage is ridiculous. Look at the houses in this village. Many of them have developed their character over centuries — a bit of medieval at the back, with Tudor and Georgian add-ons.

Another reason why those of us involved in the Transition movement might look to the writings of Colin Ward for inspiration is the 11th of 12 permaculture design principles: “use edges and value the marginal”. While Colin Ward had a long and distinguished career, marked by numerous public speaking engagements and even an Honorary Doctor of Philosophy awarded by Anglia Ruskin University in 2001, he was certainly marginal. From his army service in World War II to his recent death, Ward identified himself as an anarchist.

Now, the word anarchy might raise some eyebrows. It is after all often used to mean chaos. However, as George Santayana so eloquently put it, “Chaos is a name for any order that produces confusion in our minds.” Someone keen on straight lines and tidy gardens might look at a permaculture system and feel confused seeing only chaos and disorder. Like a permaculturalist, Ward spent a lifetime carefully observing the ways in which people (who are also part of nature) create their own forms of organisation in order to meet their needs without depending on the authority of government to do so.

For him, anarchism is a theory of organisation. In this, he was profoundly influenced by the keen observer of social and other natural systems of organisation, Peter Kropotkin. Like many permaculturists and unlike those who see nature as red in tooth and claw, Kropotkin argued that cooperation and mutual aid are more fundamental to evolution than competition and inequality. This then was the basis of Kropotkin’s and Ward’s understanding of anarchism: that we require neither State nor Market to organise our lives for shared benefit. Instead, we can organise ourselves in far more sustainable ways. Controversially, he argued, we already do: “far from being a speculative vision of a future society, [anarchism] is a description of a mode of human organisation rooted in the experience of everyday life.”

In the fields of housing and environmental design, Colin Ward noted that current housing policy “assumes that people are helpless and inert consumers” whereas he recognised that people have a strong desire and capacity “to shape their own environment”. Sounds to me like the permaculture understanding that “everything gardens” — that all forms of life contribute to the shaping of their environments. And, as with permaculture, learning to do so in a way that meets the needs of all living beings takes practice and the freedom to learn from mistakes. Mutual aid and freedom, they might have said, are key to nurturing into life the resilient communities advocated by Transition as the surest way to surf through the challenges of climate change and peak oil.

As Bill Mollison and Reny Mia Slay argued, “permaculture is information and imagination intensive”. The anarchist poet, essayist and storyteller Ursula K. Le Guin might agree. As she once wrote: “All of us have to learn how to invent our lives, make them up, imagine them. We need to be taught these skills; we need guides to show us how. If we don’t, our lives get made up for us by other people.” Colin Ward’s legacy to us is an extensive body of informative and imaginative writing: a guide to help us imagine our lives, both now and after oil.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



Fifty years of subsidies had made the owners of arable land millionaires through mechanised cultivation and, with a crisis of over-production; the European Community was rewarding them for growing no crops on part of their land. However, opportunities for the homeless poor were fewer than ever in history. The grown-up children of local families can’t get on the housing ladder. [...]There should be some place in every parish where it’s possible for people to build their own homes, and they should be allowed to do it a bit at a time, starting in a simple way and improving the structure as they go along. The idea that a house should be completed in one go before you can get planning permission and a mortgage is ridiculous. Look at the houses in this village. Many of them have developed their character over centuries — a bit of medieval at the back, with Tudor and Georgian add-ons.

