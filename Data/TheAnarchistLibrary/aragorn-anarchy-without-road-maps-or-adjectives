
Most tendencies within anarchist circles have a narrow conception of what exactly makes an anarchist, what an anarchist project is, and what the transformation to an anarchist world will look like. Whether Green or Red, Communist or Individualist, Activist or Critical, Anarchists spend as much time defending their own speculative positions on these complicated issues as they do learning what others have to offer — especially other anarchists.

As a result many find that they would prefer to do their projects, political and social, outside of anarchist circles. Either they do not think their particular project is interesting to anarchists but believe it’s important none the less (as in most progressive activism) or they do not particularly enjoy the company of anarchists and the kind of tension that working with anarchists entails. Both reasons are almost entirely accountable to the deep mistrust anarchists have of other anarchists’ programs.

Once upon a time there was an anarchist call for “Anarchism without Adjectives,” referring to a doctrine that tolerated the co-existence of different schools of anarchist thought. Instead of qualifying Anarchism as collectivist, communist, or individualist, Anarchism without Adjectives refused to preconceive economic solutions to a post-revolutionary time. Instead, Anarchism without Adjectives argued that the abolition of authority, not squabbling over the future, is of primary importance.

Today there are as many (if not more) divisions about what the abolition of authority should look like, as there were divisions on the question of the economic program for After the Revolution a hundred and twenty years ago. Anarchist activists (“organizers”) believe that a power-from-below will abolish authority. Class-struggle anarchists believe that the working class will end the authority of capitalist society. Collapsists believe that economic and environmental conditions will inevitably lead to social transformation and an end to authority.

Then again, many anarchists do not believe that the abolition of authority is of primary importance for anarchists at all. Their arguments are that authority cannot be simply understood (it is both capitalism and the state and neither of these). That anarchists do not have the (political, social, people or material) power to bring about this abolition, and that authority has transformed itself into something far more diffuse than the kings and monopolists of the 19th century. If authority can best be understood as a spectacle, today, then it is both diffuse and concentrated. This flexibility on the part of spectacular society has resulted in the effort for the abolition of authority (and the practice of many anarchists), for its own sake, to be perceived as utopian and (spectacularly) ridiculous.

Anarchists of all stripes agree that the revolutionary programs of the past have fallen far short of the total liberation of the oppressed. Leftists believe that the programs were likely to have been right but that the timing and conditions were wrong. Many other anarchists believe that the time for Programs is over. These perspectives are represented in the history of anarchism and are the source of endless contention in the founding of and meetings of anarchist groups.

History should be used to provide the context of these differing perspectives but is, instead, seen as providing evidence for one or another. Instead of trying to understand one another, to communicate, we seem to use the opportunity of our lack of success to fix our positions and argue for decreasing returns.

If anarchy does not have a road map then we (as anarchists) are free to work together. Our projects might not be of the same scale as the general strike, or even the halting of business-as-usual in a major metropolitan area, but they would be anarchist projects. An anarchy without road map or adjectives could be one where the context of the decisions that we make together will be of our own creation rather than imposed upon us. It could be an anarchy of now rather than the hope of another day. It would place the burden of establishing trust on those who actually have a common political goal (the abolition of the state and capitalism) rather than on those who have no goal at all or whose goal is antithetical to an anarchist one.

An anarchy without road map or adjectives does not ignore difference but instead places it in the context that it belongs in. When we are faced with a moment of extreme tension, when everything that we know appears about to change, then we may choose different forks in the road. Until that time anarchists should approach each other with the naïvete that we approach the world with. If we believe that the world can change and could change in a radical direction from the one traveled the past several thousand years then we should have some trust in others who desire the same things.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

