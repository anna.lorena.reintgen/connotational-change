
We can define “The Media” according to whether or not a given medium professes itself to be “objective” — in three senses of the word, i.e., that it “reports objectively” on reality; and that it defines itself as part of an objective or natural condition of reality; and that it assumes reality can be reflected and represented as an object by an observer of that reality. “The Media” — used here as a singular but collective noun-brackets the subjective and isolates it from the basic structure of mediation, which is professed as the self-reflecting gaze of the social, “impartial”, “balanced”, pure empirical reportage. By deliberately blurring the line between the objective and the subjective — as in “infotainment”, or the “soaps” which so many people believe are “real”, or the “real-life” cop shows — or in advertising — or the talkshows — the Media constructs the image of a false subjectivity, packaged and sold to the consumer as a simulacrum of his/her own “feelings” and “personal opinions” or subjectivity. And at the same time, the Media constructs (or is constructed by) a false objectivity, a false totality, which imposes itself as the authoritative world-view, far greater than any mere subject — inevitable, inescapable, a veritable force of Nature. Thus as each “feeling” or “personal opinion” arises within the consumer it is felt as both deeply personal and as objectively true. I buy this because I like it because it’s better; I support the War because it’s just and honorable, and because it produces such entertaining excitement (“Desert Storm”, a made-for-TV primetime mini-series). Thus by seeming to refute the merely subjective (or to bracket it as “art”), the Media actively recuperates the subject and reproduces it as an element within the great object, the total reflection of the total gaze: — the perfect commodity: — oneself.

Of course all media behave like this to some extent, and should perhaps be consciously resisted or “criticized” precisely to that extent. Books can be just as poisonous as Top-40 Radio, and just as falsely objective as the Evening News. The big difference is that anyone can produce a book. It has become an “intimate medium”, one in which critical faculties are engaged, because we now know and understand the book as subjective. Every book, as Calvino remarked, embodies a personal politique — whether the author is conscious of it or not. Our awareness of this has increased in direct proportion to our access to the medium. And precisely because the book no longer possesses the aura of objectivity which it enjoyed in, say, the 16th century, that aura has migrated from the intimate media to “The Media”, the “public” media such as network TV. The media in this sense remains by definition closed and inaccessible to my subjectivity. The Media wants to construct my subjectivity, not be constructed by it. If it allowed this it would become — again by definition — another intimate medium, bereft of its claim to objectivity, reduced (in Spectacular terms) to relative insignificance. Obviously the Media will resist this eventuality — but it will do so precisely by inviting me to invest my subjectivity in its total energy. It will recuperate my subjectivity, bracket it, and use it to reinforce its own false objectivity. It will sell me the illusion that I have “expressed myself”, either by selling me the lifestyle of my “choice”, or by inviting me to “appear” within the gaze of representation.

In the 1960’s the Media was still emerging and had not yet consolidated its control over the realm of the image. A few strange glitches occurred. It tried to trivialize and demonize the counter-culture, but inadvertently succeeded in making it appear more attractive; it tried to glorify and justify the neo-colonialist war in Vietnam, but inadvertently revealed it as cruel and meaningless, like a bad acid trip. These glitches arose out of a dissonance between ideology and image. The voice told us that the counter-culture was clownish and wicked, but it looked like fun: the voice told us the war was just and heroic, but it looked like Hell. Luckily for the Media, however, McLuhan and Debord came along to explain what was really going on, and the situation was soon rectified. (McLuhan wanted to empower the Media, Debord to destroy it — but both writers analyzed and criticized with such insight that their findings proved useful to the Media in ways that neither of them intended.) The media was able to bring ideology and image into focus, so to speak, and eliminate virtually all cognitive dissonance.

During the 1960’s a few people began to sense or even understand the misalignment of ideology and image in the media, and perceived therein an opening, an unguarded means of access to power. The counterculture and protest movements began to seek out “media exposure” because they were confident that their image was more attractive than the ideology which sought to interpret that image. Some theorists became adept at seizing the media. The eye appeared to be drawn irresistibly to gaze upon certain images, even those images which were coded as assaults on “the system” or “the establishment”. But once again, the Media survived — and even thrived — on the very oppositional dissident imagery which sought to assault its power. Finally what was important was “good TV”, and TV thrived on hot images of protest, Yippie stunts, devilish rock stars, psychedelic aesthetics and the like. The media appeared now far stronger and more resilient than its opposition; in fact, the reality studio had been stormed (as Burroughs urged), and had resisted by opening all image — doors and ingesting its enemies. For, ultimately, one could only appear in the Media as an image, and once one had reduced oneself to this status, one simply joined the shadow-play of commodities, the world of images, the spectacle. Without a few hundred millions to buy a network for yourself, there was no way to impose one’s subjectivity on the Media. (And even this would prove impossible, since no one with that much money and egotism could ever produce anything but oppressive banality; is this a “law of nature”?) The media, in other words, lost a few battles in the sixties — but won the war. Once it understood that the medium (the image) is the message (the ideology), and that this identity itself constitutes the spectacle and its power, the future was secure. Kennedy had acted like an actor to win power, but Reagan was an actor — the first symbol of the emptying of the spectacle itself and its re-consolidation as pure simulation. Bush then perfected “pure” or simulated war and Clinton is our first fully “virtual” president, a symbol of the absolute identity of image and ideology. It’s not that the Media has all the “power” now, or that it uses power in any conspiratorial manner. The truth is that there is no “power” — only a complete and false totality in which all discourse is contained — a false and totalitarian objectivity — an absolute Empire of the Image outside of which nothing exists except the pathetic and insignificant and (in fact) unreal subjectivity of the individual. My subjectivity. My absolute meaninglessness.

This being the case — and so obviously the case — it would seem a cause for amazement that media theorists and activists still talk and behave as if it were 1964 instead of 1994 — nearly a third of a century later. We still hear about “seizing the media”, infiltrating, subverting, or even reforming the media. Of course, some of the master media manipulators of the 60’s are still alive, Allah bless and preserve them, old beatniks and hippies, and one can forgive them for urging on us tactics which once seemed to work for them. As for me, however, it was one of those old 60’s types who alerted me to what was really going on. In 1974, I was seated at a dinner table in Tehran, Iran, at the house of the very hip Canadian ambassador, James George, with Ivan Illich, when a telegram arrived from Governor Brown of California, inviting Illich to fly there at Brown’s expense to appear with him on TV and accept a post in the administration. Illich, who is a fairly saintly individual, lost his temper for the first and only time during his stay in Iran, and began cursing Brown. When the Ambassador and I expressed puzzlement at this reaction to a cordial offer of money, fame, and influence, Illich explained that Brown was trying to destroy him. He said he never appeared on television because his entire task was to offer a critique of institutions, not a magic pill to cure humanity’s ills. TV was capable of offering only simple answers, not complex questions. He refused to become a guru or media-star, when his real purpose was to inspire people to question authority and think for themselves. Brown wanted the display of Illich’s image (charismatic, articulate, unusual-looking, probably very televisual) but not the task of thinking about Illich’s critiques of consumer society and political power. Furthermore, said “Don Ivan”, he hated to fly, and had only accepted our invitation to Iran because our letter was so full of typing errors!

Illich’s answer to the question, “Why do you not appear in the media?”, was that he refused to disappear in the media. One cannot appear in “the media” in one’s true subjectivity (and the political is the personal just as much as the personal is the political); therefore one should refuse the Media any vampiric energy it might derive from the manipulation (or simply the possession) of one’s image. I cannot “seize the media” even if I buy it, and to accept publicity from, say, the New York Times, Time magazine, or network TV, would simply amount to the commodification of my subjectivity, whether aesthetic (“feelings”, art) or critical (“opinions”, agitprop). If I wish to bring about this commodification — if I want money and fame — there might be some reason to “appear in the Media” — even at the risk of being chewed up and spat out (for the Gaze is cold and bored and easily distracted). But if I value my subjectivity more than the dubious gamble for 15 minutes of fame and twice that many pieces of silver — I will have one very good reason not to “appear”, not to be gazed upon. If I wish my own “everyday life” to be the site of the marvels I desire, rather than wishing to project those desires into a bodiless progression of images for public consumption (or rejection), then I will have another good reason to evade the media rather than try to “seize” it. If I desire “revolution” I have an urgent motive not to exchange the chance of social change for the image of change, or (even worse) the image of my desire for revolution, or (worse yet) the image of the betrayal of my desire.

From this point of view I can see only two possible strategies toward “the Media”. First, to invest our energies in the intimate media, which can still play a genuine role (of “positive mediation”) in the everyday lives of ourselves and others. And second, to approach the “major public media” (or “negative mediation”) either in the mode of evasion, or the mode of destruction. Creativity in this case would indeed have to be destructive, since the “space” taken up by false representation can only be “liberated” by violence. Needless to say, I don’t mean violence to individuals — which would be utterly futile in this case, however tempting — but violence to institutions. I admit that in both these strategic positions (evasion and destruction) I have not yet developed very many specific and effective tactics — and of course tactics are vitally necessary, since we must precisely break through the spooky realm of ideology and image into a real “field of struggle” which can be compared with war. The last thing we need in this struggle are more naive theories about seizing the media or boring from within or liberating the airwaves. Give me one example of a radical take-over of major media, and I’ll shut up and apply for a job at PBS, or start looking around for a few million dollars.

[Not one??]

Then I’ll stick to my silence.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



We can define “The Media” according to whether or not a given medium professes itself to be “objective” — in three senses of the word, i.e., that it “reports objectively” on reality; and that it defines itself as part of an objective or natural condition of reality; and that it assumes reality can be reflected and represented as an object by an observer of that reality. “The Media” — used here as a singular but collective noun-brackets the subjective and isolates it from the basic structure of mediation, which is professed as the self-reflecting gaze of the social, “impartial”, “balanced”, pure empirical reportage. By deliberately blurring the line between the objective and the subjective — as in “infotainment”, or the “soaps” which so many people believe are “real”, or the “real-life” cop shows — or in advertising — or the talkshows — the Media constructs the image of a false subjectivity, packaged and sold to the consumer as a simulacrum of his/her own “feelings” and “personal opinions” or subjectivity. And at the same time, the Media constructs (or is constructed by) a false objectivity, a false totality, which imposes itself as the authoritative world-view, far greater than any mere subject — inevitable, inescapable, a veritable force of Nature. Thus as each “feeling” or “personal opinion” arises within the consumer it is felt as both deeply personal and as objectively true. I buy this because I like it because it’s better; I support the War because it’s just and honorable, and because it produces such entertaining excitement (“Desert Storm”, a made-for-TV primetime mini-series). Thus by seeming to refute the merely subjective (or to bracket it as “art”), the Media actively recuperates the subject and reproduces it as an element within the great object, the total reflection of the total gaze: — the perfect commodity: — oneself.



Of course all media behave like this to some extent, and should perhaps be consciously resisted or “criticized” precisely to that extent. Books can be just as poisonous as Top-40 Radio, and just as falsely objective as the Evening News. The big difference is that anyone can produce a book. It has become an “intimate medium”, one in which critical faculties are engaged, because we now know and understand the book as subjective. Every book, as Calvino remarked, embodies a personal politique — whether the author is conscious of it or not. Our awareness of this has increased in direct proportion to our access to the medium. And precisely because the book no longer possesses the aura of objectivity which it enjoyed in, say, the 16th century, that aura has migrated from the intimate media to “The Media”, the “public” media such as network TV. The media in this sense remains by definition closed and inaccessible to my subjectivity. The Media wants to construct my subjectivity, not be constructed by it. If it allowed this it would become — again by definition — another intimate medium, bereft of its claim to objectivity, reduced (in Spectacular terms) to relative insignificance. Obviously the Media will resist this eventuality — but it will do so precisely by inviting me to invest my subjectivity in its total energy. It will recuperate my subjectivity, bracket it, and use it to reinforce its own false objectivity. It will sell me the illusion that I have “expressed myself”, either by selling me the lifestyle of my “choice”, or by inviting me to “appear” within the gaze of representation.



In the 1960’s the Media was still emerging and had not yet consolidated its control over the realm of the image. A few strange glitches occurred. It tried to trivialize and demonize the counter-culture, but inadvertently succeeded in making it appear more attractive; it tried to glorify and justify the neo-colonialist war in Vietnam, but inadvertently revealed it as cruel and meaningless, like a bad acid trip. These glitches arose out of a dissonance between ideology and image. The voice told us that the counter-culture was clownish and wicked, but it looked like fun: the voice told us the war was just and heroic, but it looked like Hell. Luckily for the Media, however, McLuhan and Debord came along to explain what was really going on, and the situation was soon rectified. (McLuhan wanted to empower the Media, Debord to destroy it — but both writers analyzed and criticized with such insight that their findings proved useful to the Media in ways that neither of them intended.) The media was able to bring ideology and image into focus, so to speak, and eliminate virtually all cognitive dissonance.



During the 1960’s a few people began to sense or even understand the misalignment of ideology and image in the media, and perceived therein an opening, an unguarded means of access to power. The counterculture and protest movements began to seek out “media exposure” because they were confident that their image was more attractive than the ideology which sought to interpret that image. Some theorists became adept at seizing the media. The eye appeared to be drawn irresistibly to gaze upon certain images, even those images which were coded as assaults on “the system” or “the establishment”. But once again, the Media survived — and even thrived — on the very oppositional dissident imagery which sought to assault its power. Finally what was important was “good TV”, and TV thrived on hot images of protest, Yippie stunts, devilish rock stars, psychedelic aesthetics and the like. The media appeared now far stronger and more resilient than its opposition; in fact, the reality studio had been stormed (as Burroughs urged), and had resisted by opening all image — doors and ingesting its enemies. For, ultimately, one could only appear in the Media as an image, and once one had reduced oneself to this status, one simply joined the shadow-play of commodities, the world of images, the spectacle. Without a few hundred millions to buy a network for yourself, there was no way to impose one’s subjectivity on the Media. (And even this would prove impossible, since no one with that much money and egotism could ever produce anything but oppressive banality; is this a “law of nature”?) The media, in other words, lost a few battles in the sixties — but won the war. Once it understood that the medium (the image) is the message (the ideology), and that this identity itself constitutes the spectacle and its power, the future was secure. Kennedy had acted like an actor to win power, but Reagan was an actor — the first symbol of the emptying of the spectacle itself and its re-consolidation as pure simulation. Bush then perfected “pure” or simulated war and Clinton is our first fully “virtual” president, a symbol of the absolute identity of image and ideology. It’s not that the Media has all the “power” now, or that it uses power in any conspiratorial manner. The truth is that there is no “power” — only a complete and false totality in which all discourse is contained — a false and totalitarian objectivity — an absolute Empire of the Image outside of which nothing exists except the pathetic and insignificant and (in fact) unreal subjectivity of the individual. My subjectivity. My absolute meaninglessness.



This being the case — and so obviously the case — it would seem a cause for amazement that media theorists and activists still talk and behave as if it were 1964 instead of 1994 — nearly a third of a century later. We still hear about “seizing the media”, infiltrating, subverting, or even reforming the media. Of course, some of the master media manipulators of the 60’s are still alive, Allah bless and preserve them, old beatniks and hippies, and one can forgive them for urging on us tactics which once seemed to work for them. As for me, however, it was one of those old 60’s types who alerted me to what was really going on. In 1974, I was seated at a dinner table in Tehran, Iran, at the house of the very hip Canadian ambassador, James George, with Ivan Illich, when a telegram arrived from Governor Brown of California, inviting Illich to fly there at Brown’s expense to appear with him on TV and accept a post in the administration. Illich, who is a fairly saintly individual, lost his temper for the first and only time during his stay in Iran, and began cursing Brown. When the Ambassador and I expressed puzzlement at this reaction to a cordial offer of money, fame, and influence, Illich explained that Brown was trying to destroy him. He said he never appeared on television because his entire task was to offer a critique of institutions, not a magic pill to cure humanity’s ills. TV was capable of offering only simple answers, not complex questions. He refused to become a guru or media-star, when his real purpose was to inspire people to question authority and think for themselves. Brown wanted the display of Illich’s image (charismatic, articulate, unusual-looking, probably very televisual) but not the task of thinking about Illich’s critiques of consumer society and political power. Furthermore, said “Don Ivan”, he hated to fly, and had only accepted our invitation to Iran because our letter was so full of typing errors!



Illich’s answer to the question, “Why do you not appear in the media?”, was that he refused to disappear in the media. One cannot appear in “the media” in one’s true subjectivity (and the political is the personal just as much as the personal is the political); therefore one should refuse the Media any vampiric energy it might derive from the manipulation (or simply the possession) of one’s image. I cannot “seize the media” even if I buy it, and to accept publicity from, say, the New York Times, Time magazine, or network TV, would simply amount to the commodification of my subjectivity, whether aesthetic (“feelings”, art) or critical (“opinions”, agitprop). If I wish to bring about this commodification — if I want money and fame — there might be some reason to “appear in the Media” — even at the risk of being chewed up and spat out (for the Gaze is cold and bored and easily distracted). But if I value my subjectivity more than the dubious gamble for 15 minutes of fame and twice that many pieces of silver — I will have one very good reason not to “appear”, not to be gazed upon. If I wish my own “everyday life” to be the site of the marvels I desire, rather than wishing to project those desires into a bodiless progression of images for public consumption (or rejection), then I will have another good reason to evade the media rather than try to “seize” it. If I desire “revolution” I have an urgent motive not to exchange the chance of social change for the image of change, or (even worse) the image of my desire for revolution, or (worse yet) the image of the betrayal of my desire.



From this point of view I can see only two possible strategies toward “the Media”. First, to invest our energies in the intimate media, which can still play a genuine role (of “positive mediation”) in the everyday lives of ourselves and others. And second, to approach the “major public media” (or “negative mediation”) either in the mode of evasion, or the mode of destruction. Creativity in this case would indeed have to be destructive, since the “space” taken up by false representation can only be “liberated” by violence. Needless to say, I don’t mean violence to individuals — which would be utterly futile in this case, however tempting — but violence to institutions. I admit that in both these strategic positions (evasion and destruction) I have not yet developed very many specific and effective tactics — and of course tactics are vitally necessary, since we must precisely break through the spooky realm of ideology and image into a real “field of struggle” which can be compared with war. The last thing we need in this struggle are more naive theories about seizing the media or boring from within or liberating the airwaves. Give me one example of a radical take-over of major media, and I’ll shut up and apply for a job at PBS, or start looking around for a few million dollars.


[Not one??]


Then I’ll stick to my silence.

