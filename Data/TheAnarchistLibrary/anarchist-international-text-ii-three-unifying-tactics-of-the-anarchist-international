      1: Build and maintain antagonistic infrastructure      2: Ignore national or state boundaries      3: Undermine all authority      Conclusion
As an international think-tank, we offer the following basic principles and tools to all anarchists. These principles are currently being put into practice across the world. These tools are meant to be experimented with, transformed, and enhanced.

The first priority for anarchists is to build and maintain antagonistic infrastructure. We use this term to mean infrastructure that is inherently conflictual by right of existing. Infrastructure cannot be antagonistic unless its very existence challenges the laws, morals, and equilibrium of the world capitalist system.

A community garden on land that is purchased by the gardeners merely provides a few lucky people with the chance to grow their own food. But if the garden is planted on land that is stolen from a real estate company, that garden becomes antagonistic to capitalism. While the chances of the garden’s complete destruction increase dramatically, the antagonism also increases the chances of the struggle for the garden to resonate with and attract new people into the struggle against global capitalism. In this way, a garden can stop feeding the belly of green capitalism and contribute to the revolutionary anti-capitalist struggle.

Antagonistic infrastructure must also provide everything that it offers for free. All plans for creating infrastructure must factor in the cost of providing free resources to everyone before being created. If infrastructure becomes reliant on capitalist laws and methods to sustain itself, it loses its ability to be hostile to those same laws and methods. Fearing the loss of a bar license or the violation of some petty city council ordinance are completely disabling to a revolutionary project.

By providing free resources, infrastructure can break the dominant way-of-being in capitalist culture. When people no longer need to pay to be somewhere, the fundamental principle of capitalism collapses in the building or neighborhood that houses the antagonistic infrastructure. If there is a concentration of infrastructure in one area that allows free access to buildings, encourages people to loiter outside, and provides food, clothing, or housing for free, that area will no longer replicate capitalist relations.

Paying rent is a choice the organizers and creators of infrastructure will make as they see fit. As long as everything provided by the infrastructure is free, paying rent is not an issue. However, the same principle that applies to gardens applies to squats. A squat that maintains its illegality in the face of law and the police is far more antagonistic than a building that is rented could ever be. To break, push back, and eradicate all law is our ultimate goal. While maintaining a meeting space, social center, or people’s kitchen may not allow for the law to be broken, providing everything for free still defies capitalist logic. However, an illegal social center is preferable to a rented one.

Breaking the law while creating infrastructure will increase the chance of that infrastructure not existing, but the act of breaking the law will attract support that did not exist before. Struggle is an important variable that can snap people out of passivity by giving them something to fight for. It is completely possible to make rent payments, get a building up to code, legalize a squat, or buy land. These are achievable goals.

But struggling to create something that is free from law, that is fully self-realized, and that is achieved through struggle is something that is impossible in the minds of many people. Transcending the law is an experience few people go through, but once they do, they become thirsty for more. The taste of impossible freedom is infectious and this is not a slogan. Other than death and imprisonment, comfort is the primary neutralizer of the practice of freedom.

Comfort is what appears like a plague when a struggle ends, when legality takes over, or when it is perceived that there is nothing left to fight for. Antagonism is hostile to comfort and is the only tool the Anarchist International can offer to avoid the seemingly inevitable stagnation that has occurred all over the world wherever a scene or milieu has achieved its limited means and cannot imagine itself any further into the future.

Our advice, put simply, is to break any law that prevents you from building, stealing, and transforming whatever you see fit. Never allow yourself to become neutralized by the safety of legality. Antagonistic infrastructure is not meant to be comfortable. Antagonistic infrastructure is an act of war.

If the first principle is put into practice in your area, the next priority is to dissolve the mental and physical borders of the nation state in which you operate.

In smaller countries, such as Belgium, where there are two primary languages and four surrounding nation states, there should be no emphasis on building a specifically Belgian anarchist movement and more of a focus on building regional ties between either French speaking or Dutch speaking cities. Anarchists in Brussels or Antwerp should focus on first building up their cities and then strengthening their ties with projects and efforts in the outlying towns. The national borders of the Belgian state should be ignored.

It is a temptation, especially in a nation state as small as Belgium, to unite anarchists according to the official borders that surround them, given that these countries are so small. But there will never be any substantial unity if people yield to this temptation. Only by looking to the nearest anarchists will there be any escalation of our offensive that is not as illusory as the borders that contain us all. Unity of purpose is molecular, rhizomatic, and spreads like thousands of separate cancers. It does not immediately take over the whole of a nation state nor seek to imitate its borders and limits. Instead, it devours it from the inside until it the borders are gone.

There is an even greater temptation to organize along national borders in the large nation states like Russia, the USA, and Brazil. The tendency in countries like these is to feel daunted by the size of the country and erroneously believe that the most practical solution to this problem of distance is to unify groups of people who are sometimes over two thousand kilometers away from each other. At various times, there have been efforts to create a specifically North American anarchist movement. Because of the isolation of the USA relative to Europe and South America, internationalism was abandoned in favor of a subcultural, youth-focused movement composed of anarchists who wandered the thousands of kilometers of the USA, drifting from town to town, perpetuating the same tactics, engaging in the same forms of entertainment, and focusing largely on removing oneself from capitalist culture while simultaneously leaving that culture firmly in place.

In the end, this approach created a culture that shared similar practices and tactics but remained rootless and disconnected from any specific geographical area. This approach ended around the same time that the global economic collapse began in 2008, leaving anarchists in the USA scattered in a giant nation state with little or no connection to their geographical areas. The USA proved too big to unify, with auto mechanics in Utah still having no substantial connection with suburban teenagers in Washington DC. The same is true for the lack of connection between a baker in Saint Petersburg and a farmer outside of Angarsk. The only things that unify the land of Russia and the USA are fictions, police, and borders.

There is no legitimate reason for anarchists to replicate national borders in their organizing. Each region of the world is distinct, possessing different rhythms, dialects, languages, and climates. The process of molecular growth should not be assumed to aim for a nation. Growth should first concern city blocks, then neighborhoods, then surrounding towns. Connections should be made with anarchists in distant cities or towns, but those anarchists should be left to continue on their similar but completely separate projects of molecular growth. There should be many separate geographical areas that have an anarchist presence, not anarchist movements that aim to unify a territory that should never have been considered singular in the first place.

It takes thousands of years for physical geography to alter, for valleys to disappear, and for hills to rise. But borders appear and disappear all the time. There is nothing to be gained in considering them. Land is land. While this point may be basic and self-explanatory, we wish to make our position clear in the hopes of avoiding any further confusion on the subject.

At this point, authority is everywhere and we are the dark matter behind the authoritarian order of capitalist society. We cannot be detected, measured, or sufficiently explained. We are the chaos that is tamed in order to construct a road, to build a skyscraper, or plan a water system. Before an area can be thoroughly pacified, before any totalitarian nightmare can take over, people likes us have to be mislabeled, disappeared, and liquidated. Just as scientists build particle colliders in order to explain-away the existence of dark matter, so to does the capitalist order label us as agents of chaos in order to hide and shelter themselves from the mystery of our existence.

Therefor, fellow members of the Anarchist International, our opponent is everywhere, and we need not consider how seemingly insignificant our efforts are. As long as the chaos, mystery, and ferocity of our actions pierce directly into the heart of capitalist normality, we may consider all tactics legitimate and worthy of repetition. Authority is everywhere and thus so are we, constantly struggling to undermine it. We need only lift our heads to see a manifestation of the reigning order.

Every city is a vampire. Its teeth are nearly invisible to others, but not to us. First, there are the prohibitions that have been internalized by the population. These prohibitions are psychologically enforced in many different ways. Next, there are the laws that are physically enforced. Then there is the structure and the administration of the city itself. Beyond this, there are only a few high capitalists pulling the strings. Following this narrative of the city’s power structure, we offer the following outline for challenging all authority. We must be forgiven for the chaotic nature of what follows. It is as unreasonable and serious as it sounds.

Begin by routinely smoking weed outside a placid cafe or tavern. Wait for people to join you until everyone is doing every drug imaginable. Then begin to have public sex in nearby fields or parks. Also start covering this area with as much graffiti as possible, with the occasional broken window. Establish public urination areas. Re-establish the practice of burning everything on hand in communal bonfires. Always manifest fire. It is the most potent element for our purposes. Waste as many resources as possible, especially the most opulent ones, such as champagne. Steal from every store and share it all. Encourage a collective shame for not-stealing, for hording, and for being nervous while committing a crime. Brazenness in everything. Start yelling in quiet areas. Laugh as loudly as possible. Pull people into collective moments of insanity. Abandon the private nest, lose control on a busy street corner, and advocate extreme mental instability in all nearby persons. This is an incomplete description of ending the psychological slavery and inverted-desire caused by submission to capitalist law. Follow these guidelines as closely as possible or be prepared to suffer moments of extreme boredom, isolation, existential horror, and defeat.

Defend everyone from the police without exception. After successfully creating uncontrollable areas, defend them with you body, your words, and whatever is at hand. Do not make reasonable arguments. People break out of slavery through collective insanity and rage, not through logic or reason. Spit venom and throw bricks at parking enforcement, police, repo-men, and every other stooge physically enforcing capitalist law. Destroy all parking meters, traffic lights, and roads. Stop paying taxes and create an aura of utter guilt and shame around people who continue to feed the vampire of the city. Collectively commandeer trains and buses, encourage complete non-payment of fare. Disrupt all local government meetings, police press-conferences, community meetings, etc. Respond to all police violence with unreasonable emotion, rage, fierceness, and unrelenting attack. Sweep up everyone in what, in the end, amounts to magic but is most often simply dismissed as frenzy.

Take over every building, stop paying rent, steal all water and electricity, encourage the abandonment of the city. Highlight contradictions of civilization. Empty supermarkets, eat everything, then remind people that food is grown and harvested, not purchased. Collectively reclaim space, close roads, erect new structures, have orgiastic frenzies, act as if there is one final party, an epic feast, or a farewell diner preceding the destruction of the old world. Bring on the cold sweats of chaotic fever in everyone. Act as antibodies, do not let people die of their fever, instead be there when they awaken from their endless party and point them out of the ruined and desolate metropolis. Leave on all lights, air conditioning, and water. Trash the city, empty it of its treasures, enjoy those treasures, savor them, burn them, eat them, and vampirize the vampire without becoming one. This is possible and quite insane. Enjoy.

While the first two tactics we have outlined will undoubtedly make more sense than our third, we still insist on its relevance. It is quite difficult to explain what we have only caught glimpses of and has yet to eradicate the capitalist world. We are pluralists in every sense of the word. Unfortunately, we are trapped between the incommunicable nature of what we wish to say and our desire to provide a clear sign that points the way out of this endless death-camp. Our task is impossible, and that is why we will triumph. The impossible is all that is left.

A fire was lit in the center of Reykjavik in 2009 during the collapse of the island’s capitalist economy. Around this fire were dark, radiant people, bringing the chaos we have described above. As members of the Anarchist International, we were all there, and it is this fire that we wish to spark everywhere.

Towards the total triumph of chaos 
and the setting of uncontrollable fires

- ANARCHIST INTERNATIONAL




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



Begin by routinely smoking weed outside a placid cafe or tavern. Wait for people to join you until everyone is doing every drug imaginable. Then begin to have public sex in nearby fields or parks. Also start covering this area with as much graffiti as possible, with the occasional broken window. Establish public urination areas. Re-establish the practice of burning everything on hand in communal bonfires. Always manifest fire. It is the most potent element for our purposes. Waste as many resources as possible, especially the most opulent ones, such as champagne. Steal from every store and share it all. Encourage a collective shame for not-stealing, for hording, and for being nervous while committing a crime. Brazenness in everything. Start yelling in quiet areas. Laugh as loudly as possible. Pull people into collective moments of insanity. Abandon the private nest, lose control on a busy street corner, and advocate extreme mental instability in all nearby persons. This is an incomplete description of ending the psychological slavery and inverted-desire caused by submission to capitalist law. Follow these guidelines as closely as possible or be prepared to suffer moments of extreme boredom, isolation, existential horror, and defeat.



Defend everyone from the police without exception. After successfully creating uncontrollable areas, defend them with you body, your words, and whatever is at hand. Do not make reasonable arguments. People break out of slavery through collective insanity and rage, not through logic or reason. Spit venom and throw bricks at parking enforcement, police, repo-men, and every other stooge physically enforcing capitalist law. Destroy all parking meters, traffic lights, and roads. Stop paying taxes and create an aura of utter guilt and shame around people who continue to feed the vampire of the city. Collectively commandeer trains and buses, encourage complete non-payment of fare. Disrupt all local government meetings, police press-conferences, community meetings, etc. Respond to all police violence with unreasonable emotion, rage, fierceness, and unrelenting attack. Sweep up everyone in what, in the end, amounts to magic but is most often simply dismissed as frenzy.



Take over every building, stop paying rent, steal all water and electricity, encourage the abandonment of the city. Highlight contradictions of civilization. Empty supermarkets, eat everything, then remind people that food is grown and harvested, not purchased. Collectively reclaim space, close roads, erect new structures, have orgiastic frenzies, act as if there is one final party, an epic feast, or a farewell diner preceding the destruction of the old world. Bring on the cold sweats of chaotic fever in everyone. Act as antibodies, do not let people die of their fever, instead be there when they awaken from their endless party and point them out of the ruined and desolate metropolis. Leave on all lights, air conditioning, and water. Trash the city, empty it of its treasures, enjoy those treasures, savor them, burn them, eat them, and vampirize the vampire without becoming one. This is possible and quite insane. Enjoy.



As an international think-tank, we offer the following basic principles and tools to all anarchists. These principles are currently being put into practice across the world. These tools are meant to be experimented with, transformed, and enhanced.



Towards the total triumph of chaos 
and the setting of uncontrollable fires


- ANARCHIST INTERNATIONAL

