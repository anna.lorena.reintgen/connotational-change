
Even before the start of the air war, the crisis in the Persian Gulf reinvigorated many discussions and criticisms of American society, capitalism in general and even modern industrial civilization. This may be positive; it is impossible to say for certain at this time. These discussions may simply be absorbed into the pluralist political circus or may lead to real ongoing thought and social action. Discussion has opened up partly because many people who never were before involved have become active in the opposition. But the dead hand of the bureaucratic left is still very evident. Unfortunately, a great deal of the organized anti-war activity has been dominated by mainstream, conventional left-liberal and authoritarian-bureaucratic political activists. Many of them are in love with the most modern business and political techniques and have been applying them to the oppositional movement.

Many of the leftists have had a lot of practice in previous opposition to US policies regarding El Salvador, Nicaragua, etc., which they have often linked with public-relations campaigns in favor of the Salvadoran rebel FMLN and the Sandinistas. They have brought their experiences with a vengeance into the new movement by demanding compromise with the status-quo ideology and calling for protest within the context of peaceful obedience to the authorities, so as to gain their respect. Many urge "working through the system." They tell us we must put pressure on elected representatives in Washington, including both the Congress and the president; we must elect better representatives in the next national elections, which will not be held until November of 1992, and which are obviously open to all sorts of media and other political manipulations. They urge that we "support our troops," not hurt their feelings by criticizing the job they do, and that we should express patriotism while criticizing government policy. We must prove that we deserve to be listened to by obeying the rules of law and order, and by respecting the police. As strange as it may seem to you in present-day Europe, the traditions of the communist party have not been totally discredited among a large part of the US left. Many still long for a 1930s-type of popular front politics similar to the idealized version, put forward by so many communists, which involves harmony and consensus between all sorts of left and liberal groups.

The split in the US ruling class and even status-quo institutions goes very deep. A number of local politicians in many cities and towns have openly opposed the war. The City Council and the mayor of Seattle came out in favor of negotiations and sanctions, and against military intervention. The city police were instructed to treat with consideration those demonstrators who engage in certain approved forms of protest. This basically meant that those who chose to passively sit or lie in the way of federal government workers trying to go to work or held peace rallies or tried to block traffic in the downtown area were generally treated with respect, gently asked to move, or gently carried out of the stream of traffic. Some leftists and left-liberals have been very enthusiastic about this cooperation and consideration. This kind of police behavior has also occurred in some other parts of the US; but, in most places the police have retained their usual brutal ways. And here in Seattle those who dared to engage in unauthorized forms of protest, such as trying to block the highways, were treated rather more harshly.

On the other hand, the Bush administration and most of the media have made a great attempt to convince the population that there has been no real opposition to the military incursion. Bush has even given a number of speeches in which he has simply and boldly stated that there was no opposition at all--despite massive numbers of people in the streets, at church services against the war and all sorts of lectures and discussion groups, vigils, etc. Although opinion polls indicated that an overwhelming majority, ranging from 75 to 95 percent of the population, supported the president's war policy, there are many good reasons to doubt the accuracy of these polls and to believe that the proportion of the population which, for various reasons, opposed the war was substantial. These polls generally interviewed a very small number of people, usually only about a thousand at a time. They generalized from the responses of these people based on their job, income, religious, racial, and other classifications. But in this crisis very many people clearly departed from the political stance that would be projected for their socio-economic profiles. Many who defined themselves as political conservatives or centrists opposed the war; large numbers of devout churchgoers, both conventional and evangelical, were against it; many affluent professionals also opposed it, as well as others not usually expected to oppose government policy. If a small number of people with similar profiles said they supported the war, the pollsters' projections that the majority of such people supported it were not necessarily correct. It should be noted that most of the US Catholic and Protestant hierarchies came out against the war, even though they rarely oppose government policy. Many people who define themselves as Jewish came out against the war, even though it was supported by the spokespeople for the major Zionist organizations. We really don't know what proportion of the population opposed this war, although it seems that there was more vocal opposition and activity in the western part of the country than in the East among all social groups. But certainly the public opinion polls on which the president and the media relied as proof that the population supported the policy should not be given greater credibility than the many thousands of letters and phone calls received by Congressional representatives indicating strong opposition to the war on the part of constituents. While there was much more public acquiescence and support for the war than we would have wanted, and while the opposition that did appear came from diverse motivations, the depth and extent of opposition must not be discounted.

The government has engaged in a concerted effort to shame the US population into passivity by telling us that we would be betraying our friends and relatives in the military by opposing the war, this despite the fact that large numbers of people called to serve in the Persian Gulf have also expressed opposition themselves. There is even an organization of families and friends of military people founded explicitly to express opposition to US hostilities in the Gulf. Although the fighting is now over, the military involvement is not, and neither is the opposition.

For a long time the US government has been engaging in various campaigns intended to both convince and intimidate the population into passively accepting military interventions in Central America and elsewhere, and into accepting the repression of those who resist brutal regimes friendly to the US. In the 1980s and since, the authorities have used the "war on drugs" not primarily to apprehend big drug dealers and their large financial and CIA collaborators, but to frighten the American population into agreeing to a continued military-imperial role for the US government. The drug war has served to mask similarities between the government's deadly intervention and manipulation in Latin-America and Vietnam.

It is generally recognized that most of the American populace was thoroughly sickened and outraged by the Vietnam war, although many people opposed it primarily because it was hellish for "our boys," that is, the US soldiers. Others opposed it mainly because it was clearly an unwinnable war. But there were also many (including ourselves) who opposed it as a heinous imperialist incursion which was destroying the lives of the people of Vietnam because they would not docilely submit to the brutalities of the US puppet regime in the South. The US authorities were also faced with rebellion and disorder in the military, especially among the lower-ranking soldiers. In addition, there was social unrest in the US, both in poor neighborhoods due to appalling living conditions (which by now have actually--though unbelievably--gotten worse) and among college students and young people from all social classes and groups who were demanding social justice at home as well as an end to the war abroad. The authorities and the established media labeled the pervasive aversion to official policy "the Vietnam syndrome," as if it were a sickness from which the population suffers. They have been trying to cure us of it since the mid-1970s.

Part of this attempt has involved restructuring the military to make it more reliable and less open to charges of class-based inequity. In the 1960s and '70s, the US military was primarily composed of conscripted soldiers. It was relatively easy for the affluent and the politically well-connected to avoid serving in the military if they wanted to. Most of the soldiers were from working-class and impoverished backgrounds, and were generally not enthusiastic about the army and their position in it. When faced with the realities of Vietnam, they often proved rebellious. The US now has an all-volunteer military. It is smaller, but the authorities hope and believe that it is more reliable. It is made up of dedicated career militarists, but also of very many people who would have found much lower wages or lower-skilled jobs in civilian life. These include black and Latino people (for the most part men, but also women) who, due to discrimination, have many fewer opportunities of getting decently-paid jobs in the private sector. A lot of attention has recently been focused on these people because of the importance of racial conflict and discrimination in past and present social movements here in the US. During the Vietnam war the rebelliousness of many troops was related to ill-treatment and discrimination, which many minority members were protesting within the military.

Government figures indicate that 20 percent of the US military forces stationed in the Persian Gulf area before and during the war were black; blacks constitute only 12% of the population as a whole. Altogether, 28% of the army is black, about 16.5% of the navy and the air force, and about 20% of the marine corps. Close to one-third of the troops in frontline army units are black. In some of the airborne units blacks constitute about 35% of the personnel. But, only a small proportion of blacks get the most prestigious jobs. There are relatively few fighter pilots who are black. Somewhat more are helicopter pilots and specialized-traffic pilots.

Nevertheless, over the last decade, there has been a significant increase in opportunities for blacks and other minority members to gain training in skills and higher education leading to more highly paid positions. Ten years ago there were very few blacks in electronics, communications and intelligence units; today their numbers have increased substantially, as much as 200% in some fields and specialties. In the military there are training and job opportunities open to blacks which are not available to them in private industry. For that reason many who have enrolled in the armed forces, especially during the past decade or so, have been strongly motivated to succeed individually. Additionally, they have felt that their successful participation could contribute to opening up new opportunities for acceptance and success for all blacks, both inside the military and in civilian life. However, at the same time that job opportunities have opened up in the military (and to a lesser extent on the outside as well), racial bigotry and conflict in the society at large have, if anything, worsened. And even within the military blacks are often directed into occupations which fall below their skill levels or educational attainments, so as to exclude them from the more prestigious jobs (fighter pilots, etc.).

Most of those who have joined the military have always come from working-class backgrounds. But, over the last decade the socio-economic composition of the military has changed to an extent. Today's soldiers in general are better-educated and come from higher income groups than before. In the past, many had not completed high school (12 years of education). Presently the vast majority have, and some have even begun college before joining. A much smaller proportion of today's recruits come from impoverished backgrounds. Many more are from the families of skilled industrial and clerical workers and, in the case of minority youths, even professionals.

The black soldier of today is on average from a slightly higher economic stratum within the black population than is his or her white counterpart from within the white population. Minority members are not generally joining to escape a life of petty crime or personal problems, as many did in earlier times. They are generally very ambitious and achievement-oriented youths; a much higher proportion of them decide to make the military their career than do white youth. About 57.5% of black soldiers re-enlist after their first tour of duty, while 35% of white soldiers do so.

Recently there have been heated discussions among black politicians concerning what kind of attitudes black people should or do have toward government policies such as the incursion in the Gulf. Obviously there is a wide diversity of opinion among people who are black; but there are two main pressures from black politicians of various tendencies: one demands complete loyalty to government policies in hopes of expanding opportunities for blacks based on their commitment to the status quo; and another demands opposition to policies on the basis of the obvious lack of opportunities, as well as government neglect of the needs of black people in the US.

A poll published in The Wall Street Journal at the end of January, 1991, indicated that support for the Gulf war was much weaker among black voters than among the voting population as a whole. While 78% of white voters asked said that the president had waited long enough before using military force, only 52% of black voters agreed. Of the black voters asked, 39% thought that Bush should have given sanctions more time to work, while only 19% of the non-black voters did. Many blacks, including intellectuals and professionals, feel that it is unfair that minorities should be more in danger proportionately of suffering casualties in a war than non-minority members of the population. They feel that the high proportion of minority members in the frontline units, as compared with the more prestigious and favored specialties and the support units, is directly related to the inequalities in American society as a whole. And they criticize the fact that racial discrimination drives black people into the military as a job of last recourse. But other black politicians and intellectuals argue that black people who are successful in the military lay the groundwork for civil rights gains in the rest of society and for greater acceptance as patriotic citizens by the society as a whole. To this their adversaries respond that in virtually every instance of US military conflict flack soldiers have found themselves in the situation of fighting and struggling to remove discrimination which affected them before enrollment in the military, either personally or as a group. After all US wars political considerations have determined whether or not policy changes affecting the status of blacks have been put into affect. The Civil War resulted in the abolition of slavery and the granting of voting rights to adult black men directly afterwards. But neither the government nor the society at large generally acted to institute policies to remove discrimination. All subsequent wars raised the hopes of blacks for relief from discrimination with the return of peace. Even though some improvements did occur, despite hard work and dedication on the part of black soldiers, their basic hopes for better treatment were never fulfilled. But those who favor black loyal participation in the military argue that such criticisms openly expressed threaten to incite further hostility toward blacks on the part of the rest of the population.

In general, the composition of the US military has been very much affected by reductions in civilian social welfare and educational programs. For example, over the last ten years there have been drastic reductions in financial assistance for college education, while the cost of college education has risen sharply, even in state-affiliated institutions. Many students (working-class whites as well as minorities) have turned to the military as a source of funding for training and education. Often they have viewed themselves as primarily acquiring skills or higher education and engaging in other peacetime endeavors. Recruiting advertisements have purposely given this impression; one of their main slogans has been "Be All You Can Be." They have generally avoided war imagery.

Once in the military, many recruits have married, settled down, begun to have children, gotten second jobs to supplement their military incomes, and generally established themselves as reputable citizens. The call-up for duty in the Gulf very much disturbed these people, who had to leave their second jobs, their new wives or husbands and young children. Their experience has been very different from that of the 18-year-old unmarried men who were drafted to go to Vietnam, who, for the most part, had no illusions that the military offered job training or career opportunities and were fairly certain they would be involved in war.

It is unclear whether the US military is a more reliable or obedient force now that it is a volunteer army than it was when it was a drafted army during the Vietnam era. There have been a number of dramatic cases of resistance to the Gulf war by both active troops and by people in the military reserves. Groups which counsel military resisters have reported thousands of inquiries and calls for help. They also report that at least a thousand reservists and active-duty troops were restricted to their bases for refusing to go to the Persian Gulf (by the end of January). There are indications that resistance may have been widespread, although the government has not released information about it. Ten Marine recruits in training at Camp Pendleton in California are known to have refused to go to the Gulf and were put in prison. At Camp Lejeune in North Carolina, 14 Marine reservists also refused. A group of 27 US soldiers stationed in Germany requested political asylum in Sweden to avoid being sent to the Gulf; their request was rejected because of the UN resolution. Some troops who refused to go were forcibly sent in chains. Some military resisters have spoken publicly at anti-war rallies, including the largest ones, and have appeared on nationwide radio and TV programs. Many have asserted that they are willing to fight for their country but that they didn't believe that their country was at risk in this war. A number of military doctors and other medical personnel have refused to serve because they consider the war to be both inhumane and immoral. But very few resisters have so far expressed any kind of thoroughgoing criticism of the military or the political-social system.

Many of today's recruits are themselves the children of Vietnam veterans, with firsthand experience of that war's toll on surviving US soldiers and their families. The constant attempts to reinterpret the Vietnam war, so as to make new incursions more palatable, have not really achieved their goal of creating complete passivity and obedience, although they have succeeded in confusing and intimidating many people. Only a small right-wing portion of the population is seriously dedicated to US imperial aims. Most other people seem to have at least some doubts, even when they can conceive of little opportunity to influence events.

Although some politicians have argued that the Gulf war will benefit the United States in the same way as did World Wars I and II, it must be remembered that the US entered those wars after the main combatants had begun to exhaust each other. Neither US troops nor the civilian population suffered the human casualties or material destruction that were sustained in Europe and Asia. These wars also offered massive opportunities for the US capitalists and government to lend money to the allied governments to purchase arms, industrial material and consumer goods produced in the US. This, along with the arms and equipment required by the US military, generated many jobs and brought a prosperity of sorts. And after both of those conflicts the US was the only major industrial power and society in the world which had not been devastated by war. This enabled US capitalists to become dominant in world markets, beginning with the end of World War I and becoming definitive after World War II. The demand for goods produced in this country provided jobs here, which meant prosperity, if not wealth, for the majority of people. But this kind of economic stimulation has not accompanied subsequent wars waged by the US. It had to pay for the fight in South Korea and for the troops maintained there ever since, without gaining a market for massive amounts of goods produced in the US comparable to that gained after World War II. And the Vietnam war hurt the US economy deeply, even as it was thriving during the 1960s. Neither the Vietnam war nor any of the other "covert" wars the US government waged over the years has opened up new major markets for the sale of American-made goods and services, and therefore they have not created a significant number of jobs for ordinary people.

The government budget deficits caused by military spending in Vietnam and thereafter have been consistently used as justification for reductions of social welfare spending, such as for health care, schools, unemployment assistance, aid for mothers with young children or for the elderly, etc. At the same time, during the 1970s and 1980s the government has also reduced spending for maintaining roads, bridges and sidewalks, railroads and public transportation and other such services which businesses value and expect government to provide so as to make doing business easier and more efficient. These factors have contributed to the decision of many firms to move their production facilities out of the US, which eliminates jobs here. This process will certainly not be reversed by the Gulf war. The small number of jobs which may be generated for Americans in replacing military equipment, providing some civilian equipment and supervisory personnel for the reconstruction of Kuwait will not reverse the long-term trend of job loss to other parts of the world. With respect to Kuwait, most of the construction and other jobs will go to low-paid Middle Eastern and Asian laborers; only a small number of Americans will be employed as supervisors and technical staff. And only a few large corporations in the US will be making and selling equipment to the Kuwaitis and other parts of the region. They will not have to hire many workers in the US for this purpose.

In addition, the Gulf war will not remedy the deep problems faced by some of this country's key economic sectors, such as banking, finance, insurance, construction, real estate, and retail sales. It is generally expected that there will continue to be a lot of job reductions in all of these areas. Jobs are also being eliminated in the air line and military production industries because of restructuring which is aimed at increasing efficiency. As more people become unemployed, they buy less, and also put stress on state and local government finances because of their increased need for unemployment assistance payments and because they are paying less taxes. This is causing state and local governments to eliminate many of their own workers in an attempt to save money. This process has been accelerating over the last decade and is expected to continue. In general, none of the basic political and economic problems from which the country suffers will be fundamentally solved by the war.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

