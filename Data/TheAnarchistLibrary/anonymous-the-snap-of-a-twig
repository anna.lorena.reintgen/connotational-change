      Seeing the Danger through the Trees      The Technology of Control      So What’s a Poor Activist to Do?
“Force enhancers.” The ominous phrase shot across the airwaves with a crack like a 22. I stared at the radio. A Congresswoman was describing her work on transferring military technology to civilian police force.

In military jargon “force enhancers” refer to technologies that improve the killing capacity of each soldier. An army can never have enough soldiers, so true to Western thinking, there has been an incredible drive to make the individual soldier better equipped on the battlefield.

This Congresswoman was saying how today’s cop is like a Wyatt Earp, out there alone with a police cruiser replacing the horse and nothing else to help out. This so called “liberal” legislator sought to deliver “force enhancers” to local police. I guess she felt that there can never be enough police officers in this “war against crime.”

I wondered how the military style assault units (SWAT teams), police dogs, police helicopters with infrared, tear gas and riot gear fit into this lone Wyatt Earp image she was painting. I also wondered what kind of military technology she had in mind for cops of the future.

Never forget that crime is a social and political construct, not an absolute, as they attempt to sell us this business of “fighting crime.” The real product beneath all the rhetoric is social control. Police forces defend the owning class. (This becomes clear to any forest activist attempting to stop an illegal timber sale. Enforcers of the law only care about the protesters trespassing, not about any violations of environmental laws endangering the greater community.) Laws such as the Seditious Conspiracy Act (18 USCS 2384) have been constructed specifically to criminalize political dissent. The Racketeer Influenced and Corrupt Organizations (RICO) Act, originally intended to combat organize crime, has been twisted around to punish political targets. Political prisoners have received up to fifty years sentences just for RICO convictions.

Social control is big business. Every night the cop shows on TV advertise this expensive product. Funding for the criminal “just-us” system has increased seven-fold over the past twenty years, from $10 billion to $74 billion a year, with $25 billion spent for incarceration. Not only is prison construction a major growth industry, but an increasing number of private companies are reaping the benefits of cheap prison labor

Upon hearing the crack of a twig, a deer will look and listen motionlessly for a few moments. If there’s no apparent danger rushing towards her, the deer goes right back to browsing. This is a dangerous survival strategy in an age where technology allows the hunter to kill at a distance. The American Left seems to mimic this behavior. When we hear about something alarming, we look up from our work only momentarily. Upon finding that there’s still a US Constitution guaranteeing free speech and the right to peacefully protest the government, we go back to painting yet another banner and planning the next demo.

We forget that the constitution didn’t prevent federal agents from raiding radical organizations throughout the country in September of 1918. Thousands were arrested and 500 deported. A thriving socialist press was effectively destroyed, along with the Industrial Workers of the World (IWW), amidst the red scare and war propaganda of WWI.

WWII brought the internment of 100,000 Japanese Americans. The Cold War brought the chill of McCarthyism. Now we are in the middle of a war against drugs and crime, an internal war, against citizens within our own borders. It seems the politicians are getting ready to declare yet another war — this one against terrorism. Better hold on tight to your civil liberties.

A twig snaps... We look up... What? Not another Crime Control Bill! 100,000 more cops on the street. This is costing us some bucks. Good thing I don’t pay taxes.

Was that the wind... No, just another proposal for a national identification card utilizing fingerprints or retina scans. No need to worry; the American people will never accept that.

Another twig... Anti-terrorism legislation, funding 10,000 more FBI agents ans authorizing the military involvement in certain domestic investigations. Hmmm...

Rustling of leaves... $50 million dollars spent just on the Unabomber investigation. Library computer records search for particular books that may have been checked out. Satellite surveillance of Kaczynski’s cabin... But that’s OK I ain’t no terrorist like he was.

A big thud nearby... Prison population doubled between 1975 and 1985 and then doubled again in the last decade... But I’m not in jail. Well, at least I’m not behind bars.

Political climates do swing back and forth between permissive and reactionary. Maybe things will lighten up at the turn of the millennium. (Fat chance!) However, technological developments are not cyclical like politics, they are linear and exponential. The technology of control is becoming ever more sophisticated and pernicious.

The digitizing of fingerprints was quite a revolution in law enforcement. To check a set of fingerprints once required someone to visually compare them with each set of prints one after another. It was time consuming and labor intensive. Most investigations only compared prints with those on file locally. These days portable fingerprint scanner in the cop car can check a set of prints against the entire national law enforcement database. No wonder the authorities are eager to get kids fingerprinted “for their own safety.”

Not to be left out, corporations will introduce credit cards next year that utilize fingerprints to verify identity. Certain banks now require thumb prints before cashing a questionable check. They claim the prints are destroyed after the transaction is successfully completed. When it comes to the security business, I trust corporations even less then government. (The Pinkerton Detective Agency’s style of handling labor disputes was influential in shaping my view on the subject.)

DNA printing is another quiet revolution in law enforcement. The reliability of today’s DNA evidence received a lot of attention during the Simpson murder trial, but there has never been public debate on how this technology might be used in the future. Humans leave behind a trail of hair and skin flakes in cars and on furniture. It is assumed that only criminals and terrorists would desire to keep their identity or their whereabouts hidden from the government (or unfriendly corporations).

A government database has been created to store DNA prints from all new members of the armed forces. The positive identification of corpses is the stated intention behind this program.

Microchips have been implanted in several million cats and dogs. Maybe this will become a routine practice for infants and immigrants as well. Then there will be no question as to who the owner is.

Wildlife managers can fill you in on the latest advances in satellite telemetry to track radio collared animals. I’m sure the self-appointed managers of humans will be happy to try out this technology. (A primitive version of a human radio collar is already in use for prisoners under house arrest. The collar is not removable and signals the authorities if the prisoner leaves the house.)

These technological advances all rely on computers. So don’t listen to fools expressing the virtues of e-mail and desktop publishing. The banking and insurance industries, the military and police establishment have all gained much more powerful tools then the ability to send a letter electronically or choose between 25 different fonts.

By the way, e-mail is the perfect medium for interception by unknown third parties. A massive volume of messages can be checked for key words and phrases or stored for future analysis. The US government admits to routinely checking international cablegrams in this way.

In three or four years it will be possible to determine the location of a 911 call on a cellular phone to within 125 meters. Combine this technological ability to instantly track down a mobile radio signal with the mentality that allows microchips to be implanted for identification in living beings, and you have something much worse than Orwell’s nightmare. What repressive control technologies will the next generation of activist face?

Digging trenches in logging roads and building rock walls may be great exercise, but it is an inadequate response to the forces, both political and technological, gathering against political dissidents. Even the most formidable barricades at Cove/Mallard were easily cleared by big yellow machines. Thousands of person-hours were spent building fortifications at Warner Creek, yet it was all swept away in a few hours.

Blockades are a poor defense. Tree sits, though noble, don’t fare any better. Simply put, our defense sucks.

Attempts at swaying the public mind by garnering media attention seems naive. Short news blips stand little chance of registering concern in a population dulled and disempowered by the banality of the media. Even if we circumvent the typical reporting of only two moderate sides to an issue, we then encounter multi-million dollar advertising budgets, greenwashing, campaigns, and pervasive “conventional wisdom” of talk show hosts, news columnists and cultural spin doctors. These soft control tactics of the corporations are every bit as advanced as the hard control tactics of the police and military.

I suggest we shift our focus from defense to offense. All those hours spent dragging logs onto the road in a short-lived attempt at defending a place could be better spent attacking the buildings, vehicles, heavy machinery and other assets of earth destroying agencies and corporations. Very few EF!ers have been practicing an offensive strategy, yet there are more than enough targets in cities and rural areas to go around. The police are not yet able to be everywhere. Learn to (carefully) play with matches and diesel fuel. If that’s not your style, try neighborhood organizing. Leave the media out of the picture and take the offensive by going door to door. Don’t go there asking for money, organize, and patiently show people how to fit into the resistance.

We are caught up in a technological war against the Earth and against political dissidents. Adhering to a non-violent code is a fine strategy, but it is nevertheless imperative that we think like guerrilla fighters — learn new skills, prepare for a repressive crackdown, and inflict maximum political and economical damage.

Earth First! is already considered subversive by our opponents. No matter what tactics we choose, the forces of oppression will try to crush us. We are a threat to big business. We want to eliminate industrial resource extraction. We want an end to the car culture and consumer culture. We want all species to be given a chance to flourish. These are indeed revolutionary aims, so let’s start acting like revolutionaries. It’s time to strike back.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

