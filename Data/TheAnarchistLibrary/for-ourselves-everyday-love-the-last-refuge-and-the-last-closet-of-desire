
“It’s still the same old story. The fight for love’ and glory. A case of do or die.”

- theme song from Casablanca

Next time you are at a party, or a bar, or any of the other bad excuses for festivity that our time has to oiler, notice the behavior of the couples there. See how they clutch at each other constantly, how they can hardly bear to he separated for one second, how one will suspiciously follow with their eyes any attractive person passing by.

It’s no accident that we are surrounded by the imagery of love on every side — comics, movies, cards, poems, songs, novels, and commercials sell back to us the fantasy of the happy ending we’ve never had, the perfect relationship we can never find. We feel that if we could just run into the right person somehow, everything would be fine — none of the ten thousand little humiliations and frustrations that stud our lives like the spikes of a bed of nails could touch us anymore: we would live forever in the frozen perfection of the last frame of the love-story comic; the eternal moment of meeting, the kiss that never ends. Love offers the last hope of escape from the terrifying isolation in which we find ourselves -the little box of a room inside the bigger box of our parents’ house, the apartment building, the commune or the college dorm, surrounded on every side by a million other identical little boxes, each one closed round its loneliness like mouths holding back the long scream they’re afraid to let out. Walk through the streets, anywhere, any night in the ghetto or in the suburbs, and listen as you pass an open window — the choked sobbing you’ll hear always sounds the same. Inside each of us is the naked, terrified child that was never allowed its childhood, dreaming of that one human being somewhere in the world to whom it can show itself, to whom it can sing and laugh and cry without being betrayed.

And when we find someone, there is the fear of loss. Couples try to build around themselves an airtight capsule to prevent the oxygen of their passion from boiling away in the huge cold vacuum around them. Often they succeed: they get rid of any outside threat to their union. But without renewal, the air inside gets stale. They turn on each other, tearing the thin walls to shreds, and hurtle away in opposite directions through emptiness. Or else they stay together, held less and less by any real desire for each other and more and more by a complicated web of habit, guilt, fear, deception and resentment, slowly poisoning each other, until they become helpless, vicious ghosts whose relationship is long revenge.

Explosion or suffocation, the result is the same — loneliness. No wonder “older and wiser heads” advise us to restrain such desires, to put up with scraps from the bare table of “companionship”. Settle for less, they say: true love is a fairy-tale. And we circle each other warily, our hearts clenched like fists around the fear of betrayal: we prefer starving alone, after awhile, to barely tasting a feast that can be snatched away from us without warning or that turns rotten after the first mouthful.

No doubt those among my readers who think they’re “liberated”, who’ve read the latest books on meaningful relationships or have soaked up the ideology of Playboy and the swinger magazines, will think this is all very old-fashioned. But ask yourself: have you ever wanted to love with such intensity that your love becomes your single greatest wealth?

The desire to love without reserve is high on the list of those forbidden by the organizers of our poverty. It is a spectre haunting the world, and all the powers of the world have united to hunt it down, from the Pope to Hugh Hefner, from Billy Graham to Mao Tsetung. Pravda, Cosmopolitan, and Psychology Today all agree on one thing: unrestrained passion is dangerous and must be stopped. Neurotic! Unrealistic! Bourgeois!

Yet even they can’t conceal the rate at which marriage, that prison which once upon a time everyone longed for and no-one dared to leave, is crumbling away, leaving nothing but rubble, in its place. During the last two years in California, there were more divorces than marriages. The family is collapsing, eaten away by the acids of frustration and undermined by the lack of any economic reason for its existence: for years, in the eyes of the planners, it’s been nothing more than a convenient unit of consumption. Its role as unit of production, strong in the days of the frontier, of farming and home industry, was made obsolete long ago by industrialization. For a while it served as a way of passing on the conditioning that makes us into slaves, a machine for locking round our legs the shackles of guilt and fear: but even in this it is failing. Let it go. Our love must deliver in its ruins.

Certainly, the family-worship of Readers’ Digest and the old type of women’s magazines is already a joke, a target as broad as a preacher’s ass. It serves the usual function of the extreme Right, like the Birchers with their Communists under the bed — that of deflecting us toward the “modern”, “free” approach that the bosses are already offering, complete with glossy humanistic packaging by Madison Avenue. This way, ladies and gentlemen, the therapists are waiting for you, the group-leaders, the new priests of nonattachment. It’s no accident that prostitutes are now advertising “private nude encounter sessions on a waterbed”. There’s gold in them thar frustrations.

The encounter group is a ritual in which the victims, one after another, offer themselves up for sacrifice, a sacrifice they have already paid to make. It’s like buying someone a knife on the express condition that they’ll skin you alive. Sure, the therapist will help you get rid of some illusions: but along with the illusions, you are stripped of the desires for which the illusions were an inadequate cloak. And the most disgusting part is that you’re supposed to adore them after they do it to you. In all the talk about “losing one’s defenses”, the essential questions are never asked: Why do people have defenses? Couldn’t it be because the world is so alien and hostile? Isn’t it rational to defend oneself against such a world? Or even more, to attack it tooth and nail? Why should I trust all these people anyway? What have I got in common with them besides having paid to be here? SHUT UP AND CONFESS!

Making us believe we should love each other without any good reason, and in the process providing us with all kinds of substitutes for a communal life that has disappeared (in a welter of apartment complexes, laundromats, movie theatres, massage parlors, and streets that are about as safe as a headhunter’s trophy-room) has become a major industry. Jesus communes, ashrams, sensitivity sessions, swapping-markets, and a dozen other brands of phony community compete for our attention and our pocket-money — but, like all the different subjects in school, they all carry the same message; in this case: if you can’t get it together with other people, it’s your fault, and only by doing exactly as we say will you be saved. These days, they’re even organizing encounter groups between managers and workers, in which the managers promise to be more polite and the workers to be more productive. So much less wasteful than strikes, and so much more sophisticated than the sixteenth-century German prince who used to stalk through the streets of his capital, slashing his subjects across the face with a riding crop and screaming: “Love me, you swine!”

Meanwhile, everyone wonders: why is it so hard to meet people? The sociologists and the commentators have a field’ day. “Overpopulation” they cry, “The Increasing Complexity of our Industrial Society.” The priests get in their two cents as well: “It’s the decline of Faith — if only more people went to Church!” Some of the most advanced smokescreen-spreaders even talk about something called “alienation”, as if alienation was some strange kind of mental weather, falling out of the sky like a snowstorm.

But, once again, if we look back at our own lives it’s simple enough. When we’re with other people, at work, we’re not with them because we choose to be, or because we have a real common interest in doing the job: we’re there because we have to be there to sell our time for a wage, just like them — our survival plus the personnel manager’s choice is what puts us there. When we’re shopping, it’s the same thing; the other people in the store are there to buy, because they need or want goods that are for sale there. In the apartment- house and the neighborhood it’s the same thing again: we have to live somewhere, and we have little or no choice about who we’re going to share a building or a street with (or, at best, the negative choice afforded by income and ethnic group). In the laundromat, the same. In school, the same. The movies, the same. What we have in common with all these people is having to earn money and having to spend it: they’re just more bodies competing for a job or a place in line. Relationships based on any genuine mutual affinity are squeezed out by relationships based on money exchange. When we reach to touch someone, money is between us like an invisible wall: even between two people in an encounter group locked in a desperate tearful embrace. They paid to get there: each one of those tears has a price.

Yet there are moments: those flashes of recognition, when eyes suddenly see each other in the middle of a blinded crowd: those silent exchanges of anger and sympathy as the boss swaggers away after spewing out his latest tirade: those hands brushing as bodies sway too close on a packed dance floor: those quick smiles exchanged through car windows, before the traffic divides us again. The eyes say: I know you because you are alone like me. There is no truth in the language of words that can match this honesty: only the language of acts is equal to it, and alone we cannot act.

Alone? There’s the biggest irony of all. The overwhelming majority of people on the planet are now woven into a global web of production and consumption: it takes the combined effort of tens of millions of human beings doing every conceivable kind of work to produce the life of anyone of us, every single day. Yet, at the same time, this vast cooperation is almost entirely unconscious and unwilling: we sell our time and our power to work individually to this or that corporation (or, in the “Communist” countries, this or that government), each of which competes with each other for a bigger share of the world market. For the first time in history there exist the technological means, in the form of computers and telecommunications, to create a free community of the whole human race -means brought into being through the very market that divides us, that throws us in and out of work, that is dragging us closer and closer to the third worldwide slaughter in one century.

The agonizing contradiction between what is and what could be, between what we have and what we could have, builds and builds inside us. Its like a huge fault running beneath the surface of every city, the stresses growing the heart of the rock until the slightest tremor can set off a gigantic quake. In the sleepy, conservative West German town of Hannover in 1973, a few hundred high school students come together one day to protest the second rise in streetcar fares in a year. They sat down on the streetcar turntables during the evening rush hour and were promptly set upon by riot cops. But the people coming home from work and the streetcar drivers saw and remembered: within a few days there was a citywide transport strike and boycott. Some drivers kept the trolleys running without taking any fares, while auto-owners organized a free ride service, turning their “private” cars into communal transport. In the square where the turntables were, there was a constant crowd of people at all hours of the day, talking, laughing, getting to know each other. The frightened city authorities backed down and cut the fares again, and life in Hannover returned to normal: but the real victory was that the students, the strikers, the commuters had broken for a time out of the radioactive circles of their isolation. They had begun to create the new social relationships which are the basis for a new world. For a time, the language of eyes became the language of deeds, and the result was real community — the community of freedom.

 

— Louis Michelson




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“It’s still the same old story. The fight for love’ and glory. A case of do or die.”


- theme song from Casablanca

