      I      II      III      IV      V
Note: The following issue of COMMENT was presented as a lecture to the Critical Theory Seminar of the University of California at Los Angeles on May 29, 1980. My remarks are intended to emphasize the extreme importance today of viewing Anarchism in terms of the changing social contexts of our era — not as an ossified doctrine that belongs to one or another set of European thinkers, valuable as their views may have been in their various times and places. Today, more than ever, the viability of Anarchism in America will depend upon its ability to speak directly — in the language of the American people and to living problems of the American people — rather than to resurrect ideas, expressions, slogans and a weary vernacular that belong to eras past. This is not to deny the internationalist spirit of Anarchism or its historical continuity, but rather to stress the need to solidarize with libertarian traditions and concepts that are clearly relevant to dominated peoples in the areas — conceived in terms of place, time, and forms — in which libertarian movements function.

There is a grave danger that Anarchism may be dealt with simplistically, the way we deal with most radical “isms” today — as a fixed body of theory and practice that so often reduces Socialism to the textual works of Marx and Engels and their acolytes. I do not mean to deny the generic meaning of terms like “Socialism.” There are many types of Socialisms ranging from the utopian to the Leninist, from the ethical to the scientific. I simply wish to stake out the same claim for Anarchism. We must always remember that there are also many forms of Anarchism, notably anarcho-syndicalism, anarcho-individualism, anarcho-collectivism, anarcho-communism, and, amusingly enough, anarcho-Bolshevism if I read the history of the Spanish Anarchist movement correctly. These Anarchist theories and movements have been burdened by all the intramural conflicts we encounter between Socialists, albeit in a less bloody and lethal form.

What really concerns me with the wide range of Anarchisms, however, goes well beyond the generic character of the term. I cannot stress strongly enough that Anarchism not only encompasses a wide variety of theories and movements but more importantly it has a very rich historical genesis and development. This is crucial to an understanding of what I have to say. More so than any radical movement with which we are familiar, Anarchism is a profoundly social movement as distinguished from the usual political movements we associate with The Left. Its vitality, its theoretical form, indeed its very raison d’etre stem from its capacity to express the millenia-long aspirations of peoples to create their own egalitarian or, at least, self-administered social structures, their own forms of human consociation by which they can exercise control over their lives. In this sense, Anarchism really constitutes a folk or people’s social philosophy and practice in the richest sense of the term, just as the folk song constitutes the emotional expression of a people in their esthetic or spiritual depths. The Hellenic origins of the terms anarche or “no rule” should not deceive us into thinking that it can be readily placed in the academic spectrum of social ideas. Historically, Anarchism has found expression in non-authoritarian clans, tribes and tribal federations, in the democratic institutions of the Athenian polis, in the early medieval communes, in the radical Puritan congregations of the English Revolution, in the democratic town meetings that spread from Boston to Charleston after 1760, in the Paris Commune of 1871, the soviets of 1905 and 1917, the Anarchist pueblos, barrios, and worker-controlled shops of the Spanish Revolution of 1936 — in short, in the self-directed, early and contemporary, social forms of humanity that have institutionally involved people in face-to-face relations based on direct democracy, self-management, active citizenship, and personal participation.[1] It is within this electric public sphere that the Anarchist credo of direct action finds its real actualization. Indeed, direct action not only means the occupation of a nuclear power plant site but less dramatic, often prosaic, and tedious forms of self-management that involve patience, commitment to democratic procedures, lengthy discourse, and a decent respect for the opinions of others within the same community.

This institutional framework and sensibility is the authentic mileau of Anarchism, its very protoplasm. The theories that emerge from the activity of this protoplasm are the forms of self-reflexive rationality that give it coherence and consciousness. To my thinking, the “Digger” Winstanley, the Enrage Varlat, the artisan Proudhon, the worker Pelloutier, and the Russian intellectuals Bakunin and Kropotkin voice at various levels of consciousness different, often clearly delineable, phases of humanity’s organic evolution toward freedom. One can often associate these individuals or the ideas they developed with the actual development of the popular social forms from which they emerged or to which they gave ideological coherence. Thus one can justifiably associate Winstanley’s ideas with the agrarian Anarchism of the yeoman communities in seventeenth-century England, Varlat with the urban neighborhood Anarchism of the revolutionary sections and Enrage movement of Paris in 1793, Proudhon with the artisan Anarchism of craftspeople in pre-industrial France, Bakunin’s anarcho-collectivism with the peasant villages of Russia and Spain, Pelloutier’s anarcho-syndicalism, with the industrial proletariat and emerging factory system and, perhaps most prophetically, Kropotkin’s anarcho-communism with our own era, a body of theory that readily lends itself to the ecological, decentralist, technological, and urban issues that have come to the foreground of social life today.

The anti-statist and anti-political views of these Anarchist thinkers should not obscure the positive content of their views and their roots. The Marxian notion that human “socialization” reaches its most advanced historical form with bourgeois society — a society that strips humanity of its remaining biosocial trappings — would have been emphatically rejected by these Anarchists if only on the intuitive grounds that society can never be totally denatured. As I have argued elsewhere (see my “Beyond Neo-Marxism” in Telos, No. 36), society never frees itself of its natural matrix, even in the internal relations between individuals. The actual issue, if one is to learn from the ecological problems of our time, is the nature of that nature in which society is rooted — organic (as was the case in many precapitalist communities) or inorganic (as is the case in market society). The clan, tribe, polis, medieval commune, even the Parisian sections, the Commune, certainly the village and decentralized towns of the past, were rooted in bio-social relations. Market society with its atomization, competition, total objectification of the individual and her or his labor-power — not to speak of the bureaucratic sinews that hold this lifeless structure together, the concrete, steel, and glass cities and suburbs that provide its environments, and quantification that permeates every aspect of its activity — all of these not only deny life in the biological and organic sense but reduce it to its molecular components in the physical and inorganic sense. Bourgeois society does not achieve society’s domination of nature; rather, it literally desocializes society by making it an object to be appropriated by inorganic nature, by the bourgeois in his inner being and his social being. The bureaucracy colonizes the social institutions of humanity; the concrete city, the organic relations of nature; cybernetics and the mass media, the individual’s personality; in short, market “society” colonizes every aspect of personal and social life.

I cannot emphasize too strongly the umbilical cord that unites organic societies, in the sense and with the qualifications I have described them, with Anarchist theories and movements. Nor can I desist from noting the extent to which Marxism, by contrast, is linked to the most inorganic of all human structures, the state — and at other layers of hierarchy, with that most inorganic of all oppressed classes, the proletariat and such institutionalized forms of centralized power as the factory, the party, and the bureaucracy. That the very “universality” of the proletariat that Marx celebrates in the form of its dehumanization by capital, its association with a technological framework based on centralization, domination, and rationalization which presumably render it into a revolutionary force reveals the extent to which Marx’s own theoretical corpus is rooted in bourgeois ideology in its least self-reflexive form. For this “universality” as we can now see celebrates the “hollowing out” of society itself, its increasing vulnerability to bureaucratic manipulation in industry and politics by capital and trade unions. “Schooled” by the nuclear family, by vocational supervisors, by the hierarchical factory structure, and by the division of labor, the “universality” of the proletariat turns out to be the faceleseness of the proletariat — its expression not of the general interests of humanity in its progress toward socialism but its particular interests, indeed, of interests as such, as the expression of bourgeois egoism. The factory does not unite the proletariat; it defines it — and no tendency more clearly expresses the proletariat’s human desires than its attempt to escape from the factory, to seek what the Berlin Dadaists of 1918 were to demand: “universal unemployment.”

These far-reaching distinctions between Anarchism as a social movement and Marxism as a political one require further emendations. I have no quarrel with the great wealth of Marx’s writings, particularly his work on alienation, his analysis of the commodity relationship and the accumulation of capital. His historical theories require the correction of the best work of Max Weber and Karl Polanyi. But it is not Marx’s writings that must be updated. Their limits are defined by their fundamentally bourgeois origins and their incredible susceptibility to political, that is, state-oriented ideologies. Historically, it is not accidental that Anarchism in Spain, in the Ukraine, and, in its Zapatista form in Mexico, could be crushed only by a genocidal destruction of its social roots, notably the village. Marxian movements, where they suffer defeat, are crushed merely by demolishing the party. The seeming “atavism” of Anarchism — its attempts to retain artisanship, the mutual aid of the community, a closeness to nature and enlightened ethical norms — are its virtues insofar as they seek to retain those richly articulated, cooperative, and self-expressive forms of human consociation scaled to human dimensions. The seeming “effectiveness” of Marxism — its attempt to replicate the state in the form of the party, its emphasis on a political apparatus, its scientific thrust and its denial of a prophetic ethical vision — are its vices insofar as they do not demolish the bourgeois state but incorporate it into the very substance of protest and revolution.

Not accidentally, Marxism has been most sharply alienated from itself. The attempt to “update” Marxian theory, to give it relevance beyond the academy and reformist movements, has added an obfuscating eclectic dimension to its ideological corpus. In response to the Russian general strike of 1905, Rosa Luxemburg was obliged to make the “mass strike” — a typical Anarchist “strategy” — palatable to the Second International — this, not without grossly distorting Engel’s view on the subject and the Anarchist view as well.[2] Lenin was to perform much the same acrobatics in State and Revolution in 1917 when events favored the Paris Commune as a paradigm, again assailing the Anarchists while concealing Marx’s own denigrating judgment of the uprising in the later years of his life. Similar acrobatics were performed by Mandel, Gorz, et al in May-June 1968, when all of France was swept into a near-revolutionary situation.

What is significant, here, is the extent to which the theory follows events which are essentially alien to its analysis. The emergence of the ecology movement in the late 1960s, of feminism in the early 1970s, and more belatedly, of neighborhood movements in recent years has rarely been viewed as a welcome phenomenon by Marxist theorists until, by the sheer force of events, it has been acknowledged, later distorted to meet economistic, Marxist criteria, and attempts are ultimately made to absorb it. At which point, it is not Anarchism, to which these issues are literally indigenous, that has been permitted to claim its relevancy and legitimacy to the problems of our era but rather Marxism, much of which has become the ideology of state capitalism in half of the world. This obfuscating development has impeded the evolution of revolutionary consciousness at its very roots and gravely obstructs the evolution of a truly self-reflexive revolutionary movement.

By the same token, Anarchism has acquired some bad Marxist habits of its own, notably an ahistorical and largely defensive commitment to its own past. The transformation of the sixties counterculture into more institutionalized forms and the decline of the New Left has created among many committed Anarchists a longing for the ideological security and pedigree that currently afflicts many Marxist sects. This yearning to return to a less inglorious past, together with the resurgence of the Spanish CNT after Franco’s death, has fostered an Anarchism that is chillingly similar in its lack of creativity to sectarian forms of proletarian socialism, notably anarcho-syndicalism. What is lacking in both cases is the proletariat and the historical constellation of circumstances that marked the hundred-year-old era of 1848 to 1938. Anarchist commitments to the factory, to the struggle of wage labor versus capital, share all the vulgarities of sectarian Marxism. What redeems the anarcho-syndicalists from outright congruence with authoritarian Marxism is the form their libertarian variant of proletarian socialism acquires. Their emphasis on an ethical socialism, on direct action, on control from below, and their apolitical stance may serve to keep them afloat, but what tends to vitiate their efforts — this quite aside from the historical decline of the workers movement as a revolutionary force — is the authoritarian nature of the factory, the pyramidal structure fostered by syndicalist theory, and the reliance anarcho-syndicalists place on the unique role of the proletariat and the social nature of its conflict with capital.

Viewed broadly, anarcho-syndicalism, Proudhonianism, and Bakuninism belong to an irretrievable past. I say this not because they lack ideological coherence and meaning — indeed, Proudhon’s emphasis on federalism still enjoys its original validity — but simply because they speak to epochs which have faded into history. There is much they can teach us, but they have long been transcended by historically new issues — in my view, more fundamental in their libertarian implications — to which the entire Left must now address itself. This does not mean the “death” or even the “transcendence” of Anarchism as such once we view the term in its generic and historical meaning, for the issues that confront us are more distinctly social than they have ever been at any time in the past. They literally involve the recreation of a new public sphere as distinguished from the state with the forms, institutions, relations, sensibilities, and culture appropriate to a world that is faced with desocialization at every level of life. For Marxism, these issues are fatal and, in fact, render Marxism itself into ideology in a socially destructive sense.

We are no longer living in a world where revolutionary consciousness can be developed primarily or even significantly around the issue of wage labor versus capital. I do not wish to denigrate the significance of this century-old conflict. That a class struggle exists between the proletariat and the bourgeoisie (however broadly we choose to define the term “proletariat”) hardly requires discussion, anymore than the fact that we live in a capitalist society that is ruled by a capitalist class (again, however broadly we choose to define the term “capitalist”). What is really at issue is that a class struggle does not mean a class war in the revolutionary sense of the term. If the past century has taught us anything, I submit it has demonstrated that the conflict between the proletariat and the bourgeoisie has been neither more nor less revolutionary than the conflict between the plebians and patricians in the ancient world or the serfs and the nobility in the feudal world. Both conflicts did not simply end in an impasse; they never contained the authentic possibilities of transcending the social, economic, and cultural forms within which they occurred. Indeed, the view of history as a history of class struggle is a highly convoluted one that is not exhausted by conflicting economic interests, by class consciousness and identity, or by the economically motivated methods that have so easily rooted socialist and syndicalist ideologist in economic reductionism or what is blithely called a “class analysis.”

What lies on the horizon of the remaining portion of this century is not the class struggle as we have known it in the metaphors of proletarian socialism — Socialist or Anarchist. The monumental crisis bourgeois society has created in the form of a disequilibrium between humanity and nature, a crisis that has telescoped an entire geological epoch into a mere century; the expansive notion of human freedom that has given rise of feminism in all its various forms; the hollowing out of the human community and citizenship that threatens the very claims of individuality, subjectivity, and democratic consciousness, perhaps the greatest claim the bourgeois epoch has made for itself as a force for progress; the terrifying sense of powerlessness in the face of ever-greater urban, corporate, and political gigantism; the steady demobilization of the political electorate in a waning era of institutional republicanism — all of these sweeping regressions have rendered an economistic interpretation of social phenomena, a traditional “class analysis,”  and largely conventional political strategies in the forms of electoral politics and party structures grossly inadequate. One must truly torture these issues and grossly warp them into utterly distorted forms to fit them into Marxian categories. Perhaps no less significantly, the far-reaching politicization of the economy itself in the form of state capitalism or its various approximations and the emergence of a highly elaborated bureaucracy have given to the state sweeping historical functions that go far beyond its earlier role as a so-called “executive committee of the ruling class.” Indeed, to an appalling extent, they have turned the state into a substitution for society itself.

One must realize the entirely new conditions this constellation of circumstances has produced for radicalism, the extent to which they redefine the revolutionary project theoretically and practically. The technical progress that Socialism once regarded as decisive to humanity’s domination of nature and as preconditions for human freedom have now become essential in sophisticating the domination of human by human. Technology now savagely reinforces class and hierarchical rule by adding unprecendented instrumentalities of control and destruction to the forces of domination. The wedding of the economy to the state, far from simplifying the revolutionary project as Engels so naively believed in Anti-Duhring, has reinforced the powers of the state with resources that the most despotic regimes of the past never had at their command. The growing recognition that the proletariat has become — and probably has always been — an organ of capitalist society, not a revolutionary agent gestating within its womb, has raised anew the problem of the “revolutionary agent” in an entirely new and non-Marxian form. Finally, the need for the revolutionary project to view itself as a cultural project (or counterculture, if you will) that encompasses the needs of human subjectivity, the empowerment of the individual, the astheticization of the revolutionary ideal has led, in turn, to a need to consider the structural nature, internal relations, and institutional forms of a revolutionary movement that will compensate, if only in part, for the cultural, subjective, and social negation of the public and the private sphere. Indeed, we must redefine the very meaning of the word “Left” today. We must ask if radicalism can be reduced to a crude form of social democracy that operates within the established order to acquire mass, mindless constituencies or if it must advance a far-reaching revolutionary challenge to desocialization and to every aspect of domination, be it in everyday life or in the broader social arena of the coming historic epoch.

Whatever else Anarchism meant in the past — be it the millenarian movements of Christianity, the peasant movements of the Anabaptists, -the Makhnovite and Zapatista militias, the Parisian Enrages and Communards, the Proudhonian artisans, or the early industrial workers who entered the CGT in France and the CNT in Spain — it is clear to me that contemporary Anarchism must address itself in the most sophisticated and radical terms to capitalist, indeed to hierarchical society, in its advanced and, I genuinely believe, its terminal forms. To relegate Anarchism to an ahistorical moral movement based on the virtues of “natural man” and his proclivities for mutual aid, to define it merely in terms of its opposition to the state as the source of all evil, worse, to describe Anarchism merely in terms of one of its variants — the Anarchism of Stirner, Proudhon, Bakunin, or Kropotkin, — is to grossly misread Anarchism as a historical movement, to ignore its existence as a social movement in a specific social context. Anarchism does not have the proprietary character of Marxism with its body of definable texts, commentators, and their offshoots. Conceived as a social movement rather than a political one, it is not only deeply woven into the development of humanity but demands historical treatment.

Do I mean to say, then, that Anarchism dissolves into history and has no theoretical identity? My reply would be an emphatic “No.” What unites all Anarchist theories and movements are not only their defense of society against the state, of direct action against political action; more fundamentally, I believe, Anarchism by definition goes beyond class exploitation (whose significance it never denies) into hierarchical domination, whose historical significance it increasingly analyzes as the source of authority as such. The domination of the young by the old in tribal gerontacracies, of women by men in patriarchal families, the crude objectification of nature — all precede class society and economic exploitation. In fact, they remain the crucial residual sphere of authority that Marxism and Socialism retain all too comfortably in their notions of a classless society. Anarchism, in effect, provides the materials for an analysis of the nature of freedom and the nature of oppression that go beyond the conventional economistic, nexus of capitalist society into the very sensibility, structure, and nature of human consociation as such. The genesis of hierarchy, which for Marx was an inevitable extension of biology into society, is seen as a social phenomenon within the Anarchist framework, one which has its most consolidating source in patriarchy and the supremacy of the male’s civil domain over the woman’s domestic domain. I know of no more brilliant statement of this far-reaching shift than Horkheimer’s and Adorno’s passage on “animals” at the end of the Dialectic of Enlightenment: “For millena men dreamed of acquiring absolute mastery over nature, of converting the cosmos into one immense hunting-ground. “ (p. 248) Inevitably, the genesis of hierarchy and domination yields the objectification of nature as mere natural resources, of human beings as mere human resources, of community as mere urban resources in short, the reduction of the world itself to inorganic technics and a technocratic sensibility that sees humankind as a mere instrument of production.

I have tried to show elsewhere that Marx sophisticates and extends this trend into socialism and, unwittingly, reduces socialism to ideology. (See my “Marxism as Bouregois Sociology,” Our Generation, Vol. 13, No. 3) What concerns me for the present is that Anarchism, often intuitively, assembles the materials for a deeper, richer, and more significantly, a broads insight and grasp into the dialectic of domination and freedom, this by reaching beyond the factory and even the marketplace into hierarchical relations that prevail in the family, the educational system, the community, and in fact, the division of labor, the factory, the relationship of humanity to nature, not to speak of the state, bureaucracy, and the party. Accordingly, the issues of ecology, feminism, and community are indigenous concerns of Anarchism, problems which it often advances even before they acquire social immediacy — not problems which must be tacked on to its theoretical corpus and distorted to meet the criteria of an economistic, class-oriented viewpoint. Hence, Anarchism, by making these issues central to its social analyses and practice has acquired a relevance that, by far, overshadows most trends in present-day socialism. Indeed, Anarchism has become the trough in which Socialism eclectically nourishes itself on an alien diet of “socialist feminism,” the “economics of pollution,” and the “political economy of urbanism.”

Secondly, Anarchism has faced the urgent problem of structuring itself as a revolutionary movement in the form of the very society it seeks to create. It should hardly be necessary to demolish the preposterous notion that hierarchical forms of organization are synonymous with organization as such, anymore than it should be necessary to demolish the notion that the state has always been synonymous with society. What uniquely distinguishes Anarchism from other socialisms is it commitment to a libertarian confederal movement and culture, based on the coordination of human-scaled groups, united by personal affinity as well as ideological agreement, controlled from below rather than from “above,” and committed to spontaneous direct action. Here, it fosters embryonic growth, cell by cell as it were, as distinguished from bureaucratic growth by fiat and inorganic accretion. At a time when consociation is faced with the deadly prospect of dissociation, Anarchism opposes social form to political form, individual empowerment through direct action to political powerlessness through bureaucratic representation. Thus Anarchism is not only the practice of citizenship within a new public sphere, but the self-administration of the revolutionary movement itself. The very process of building an Anarchist movement from below is viewed as the process of consociation, self-activity and self-management that must ultimately yield that revolutionary self that can act upon, change and manage an authentic society.

I have merely scratched the wails of a considerable theoretical corpus and critique that would require volumes to develop in detail. Let me emphasize that the most advanced Anarchist theories, today, do not involve a mystical return to a “natural man,” a crude anti-statism, a denial of the need for organization, a vision of direct action as violence and terrorism, a mindless rejection of sophisticated theory, an opaqueness to what is living in the work of all Socialist theories. Anarchist critique and reconstruction reach far and deep into the Socialist and bourgeois traditions. If Anarchism is the “return of a ghost,” as Adorno once insisted, we may justly ask why this “ghost” continues to haunt us today. This reality can only be answered rationally if one remembers that the “ghost” is nothing less than the attempt to restore society, human consociation at the present level of historical development, in the face of an all-ubiquitious state and bureaucracy with its attendant depersonalization of the individual and its demobilization of the public and the public sphere. By the same token, the bourgeois essence of Socialism, particularly in its Marxian form, lies in its inglorious celebration of the massification of the citizen into the proletarian, of the factory as the public sphere, of cultural impoverishment as “class consciousness,” of the retreat from the social to the economic, of the triumph of technics over nature and of science over ethics. If Anarchism is a “ghost,” it is because human consociation itself threatens to become spectral; if Marxism is a “living presence,” it is because the market threatens to devour social life. Adorno’s metaphors become confused in the name of a false “historicism” where even the past actually enjoys more vitality than the present, a vitality that can never be recovered without giving life to the “ghost” itself. If the state, bureaucracy, and “masses” are to be exorcised, it is not Anarchism that will be removed from the stage of history but Marxism, with its centralized parties, hierarchies, economistic sensibilities, political strategies, and class mythologies.

There is much I have been obliged to omit. My limited time makes it impossible for me to deal with such delectable questions as the nature of the “revolutionary agent” today, the relationship of Anarchist practice to the political sphere (a more complex issue than is generally supposed when one recalls that Anarchists played a significant role in the electoral activities of the Montreal Citizens Movement), the details of Anarchist organizational structures, the relationship of Anarchism to the counterculture, to feminism, to the ecology movement, to neo-Marxist tendencies, and the like.

But allow me to conclude with this very important consideration. At a time when the proletariat is quiescent — historically, I believe — as a revolutionary class and the traditional factory faces technological extinction, Anarchism has raised almost alone those ecological issues, feminist issues, community issues, problems of self-empowerment, forms of decentralization, and concepts of self-administration that are now at the foreground of the famous “social question.” And it has raised these issues from within its very substance as a theory and practice directed against hierarchy and domination, not as exogenous problems that must be “coped” with or warped into an economistic interpretation subject of class analysis and problems of material exploitation.

 
[1] It would be well, at this point, to stress that I am discussing the institutional structure of the social forms cited above. That they all variously may have excluded women, strangers, often non-conformists of various religious and ethnic backgrounds, not to speak of slaves and people lacking property, does not diminish humanity’s capacity to recreate them on more advanced levels. Rather, it indicates that despite their historical limitations, such structures were both possible and functional, often with remarkable success. A free society will have to draw its content from the higher canons of reason and morality, not from — “models” that existed in the past. What the past recovers and validates is the human ability to approximate freedom, not the actualization of freedom in the fullness of its possibilities.
[2] A distortion all the more odious because the Social Democratic rank-and-file had been deeply moved, ideologically as well as emotionally, by the 1905 events. “The anarchists and syndicalists who had previously been driven underground by orthodox Social Democracy now rose to the surface like mushrooms on the periphery of the SPD,” observes Peter Nettl rather disdainfully in his biography of Luxemburg; “when it came to something resembling ‘their’ general strike they felt they were close to legitimacy once more.” And, indeed, with good reason: “For the first time for years anarchist speakers appeared on provincial Socialist platforms by invitation. The orthodox party press led by Vorwarts was much more cautious; but it, too, gave pride of place [albeit if not of doctrine — M. B.] to Russian events and for the first few months abstained from wagging blunt and cautious fingers over the differences between Russian chaos and German order.” (Peter Nettl, Rosa Luxemburg, Oxford University Press, 1969, abridged version, pp. 203–4).




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



Note: The following issue of COMMENT was presented as a lecture to the Critical Theory Seminar of the University of California at Los Angeles on May 29, 1980. My remarks are intended to emphasize the extreme importance today of viewing Anarchism in terms of the changing social contexts of our era — not as an ossified doctrine that belongs to one or another set of European thinkers, valuable as their views may have been in their various times and places. Today, more than ever, the viability of Anarchism in America will depend upon its ability to speak directly — in the language of the American people and to living problems of the American people — rather than to resurrect ideas, expressions, slogans and a weary vernacular that belong to eras past. This is not to deny the internationalist spirit of Anarchism or its historical continuity, but rather to stress the need to solidarize with libertarian traditions and concepts that are clearly relevant to dominated peoples in the areas — conceived in terms of place, time, and forms — in which libertarian movements function.

