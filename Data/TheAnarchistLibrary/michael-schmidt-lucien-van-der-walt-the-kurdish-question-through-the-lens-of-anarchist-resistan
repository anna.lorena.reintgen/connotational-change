      Introduction: Second-Generation Anarchism in Anatolia: The Kurdish National Question      Back to the Beginnings: Alexandre Atabekian and the Rise of Anarchism in the Ottoman Empire      The “Black International” and the Armenian Revolutionary Federation      Anarchist Anti-Imperialist Guerrillas in Thrace: Mikhail Gerdzhikoff & the Czarevo Commune      Anarchist Anti-Imperialist Guerrillas in Macedonia: The VMRO & the Vlach Mountain Communes      The Muja‘is Network, the 1908 “Young Turk” Revolution, and the Workers’ Federation of Salonica      Fragmentation, Repression and Radicalisation (1909-1910s)      The Eclipse of Anarchism, the Demise of the Empire, and the Rise of Communism (1918–1923)
This study of recent anti-imperialist resistance in Kurdistan, looking back to the anarchist resistance in the Ottoman heartland in the period before the formation of the Turkish state, consists of extracts — kindly proof-read in part by Will Firth — from the forthcoming book by Schmidt & van der Walt, Global Fire: 150 Fighting Years of International Anarchism & Syndicalism, Counter-power Vol.2, AK Press, USA, scheduled for release in about 2011.

Anarchism in Turkey [1] — once a significant radical force that contested Ottoman imperialism over its Bulgarian, Macedonian, Greek, Arab, African and Jewish subject peoples — began to re-emerge in the late 1970s. However, this flowering was forced to take root in hostile soil as since the formation of the Turkish state in 1923, Turkish left politics had been dominated by the Communist tradition and by nationalist and socialist groups seeking independence for Kurdistan, which is split between Iran, Iraq, Turkey and Syria (the most notable such group being the Kurdistan Workers’ Party, or PKK, formed in the mid-1970s, and the Turkish Communist Party — Marxist-Leninist, or TKP-ML, [2] both of which are basically Maoist). Kurdish separatists have also been a factor in Iran and Iraq. However, in the 1970s, things began to change; the American anarchist Sam Dolgoff mentioned meeting a Turkish anarchist student in the United States in 1979 in his memoirs, and by the 1980s, accordng to Anarchism in Turkey — produced by the Turkish anarchist group Karambol Publications [3] — anarchist groups and periodicals began to emerge, expanding in the 1990s. The “anarchists first participated in the May Day celebrations with their black flag in 1993 in Istanbul and again in 1994, in Ankara and other centres, creating “big interest in the media,” which gave “special coverage to the anarchists and announced that ‘at last we have our anarchists.’” Among the new generation of Turkish anarchist groups are Firestarter, founded about 1991, an Anarchist Youth Federation (AGF), the Anatolian Anarchists (AA), the Karasin Anarchist Group (KAG), and moving into the 2000s, the “Makhnovist” KaraKizil (BlackRed) group and its affiliated Anarchist Communist Initiative (AKi), the latter being an anarkismo.net founding organisation.

An anarchist current also emerged in the 1980s amongst Kurds from Turkey, such as the 5th of May Group of Kurdish and Turkish exiles in London. These groups posed the question of Kurdish independence in unmistakably libertarian terms, and opposed Islamic fundamentalism as much as nationalism. In We Come to Bury the Turkish Republic, Not to Praise It, [4] the 5th of May group argued that the struggle between modernising nationalists — the “Kemalists” who took power after the end of the Ottoman Empire — and Islamist groups was “fundamentally a power struggle between two forces, which are not principally very different from each other, rather than being a conflict between the two systems.” It condemned the authoritarianism of the Turkish and Kurdish left, such as the PKK’s tendency of using force to “eliminate rival Kurdish and Turkish organisations”. Equally it opposed Turkey’s own imperial ambitions, commenting “we also oppose the colonialist policy of the Turkish State as well as its policy of assimilation, settlement, and forced immigration ... in Northern Cyprus.” The same article added that:

“The concept of nation is an imaginary concept often employed by ruling élites as the basis of their power structure as well as by aspirant cliques to deceive oppressed minorities. For this reason, we believe not in the so-called self-determination of an imaginary “nation,” but in the self-government of voluntary individuals, groups and communities, working and unwaged people, etc.”

Another key text is Do The Kurdish People Lack a State? issued by “Kurdish anarchists” in 1996 [5]. It condemns the PKK and the Kurdish separatist groups who, “in the name of free Kurdistan and supported by landowners, merchants, and a large number of shop owners who control the movement in the market,” have “established themselves as new bosses of Kurdistan, crushing with an iron fist any discontent and challenge to their power and their properties like any other authority in the world.” It rejects a statist solution: “it is a big lie, and is an unforgivable lie, to tell the world through their massive media that a majority of Kurdish people are suffering in life because all they lack is a powerful Kurdish state” because the “truth is that the poor population of Kurdistan are suffering, like the working class population of the rest of the world in many ways, from the brutal forces of the capitalist system and their own authorities.” The solution, the Kurdish Anarchists argued, is “to tell the workers, teachers, students in Kurdistan on farms, in schools, at work places, not to be fooled into struggling for a change of bosses from Turkish to Kurdish, from Persian to Kurdish, from Arabic to Kurdish,” but to “take the lessons from their own history and working class history as a whole.” The “solution is a Communist-Anarchist revolution ... an enormous and bloody task ... on an international scale” that will “Light with the flame of revolt, the hearts and consciousness of Turkish, Persian and Arab workers, students, soldiers to end the power of poverty and the power of money.” Our aim, it concludes, “is to wipe out religion, state, racism and money.”

The late integration of the Middle East and Central Asia into the modern, capitalist, world, which forestalled the emergence of a working class — the primary social base of the broad anarchist tradition — partly explains why anarchist and syndicalist movements were largely absent from these areas in the period under review (with the notable exception of Siberia, the Altai, Lake Baikal and northern Kazakhstan) [6]. By the time that trade unions began to emerge in the 1930s, the anarchist and syndicalist movement was in decline worldwide, and communism and nationalism on the rise. These problems were compounded by the prevalence of autocratic regimes in these areas. Afghanistan was a royal dictatorship from 1919, as was Persia (today Iran) from 1921; although it is possible the Altai and Lake Baikal anarchist movements spilled over into the Mongolian borderlands, particularly as Chekist repression set in and because of the common culture of the Buryat and Mongols, Mongolia became a Soviet-style dictatorship under Bolshevik patronage in 1921; and Saudi Arabia came under the control of the Wahabbi religious fundamantalist Muslim sect in the 1920s.

Nonetheless, there was a sporadic anarchist presence, although it anarchism in the Ottoman Empire [7] was largely an affair of the subject nationalities. Anarchists were involved in the struggles against the Empire in Armenia, Bulgaria and Macedonia. A striking feature of these Eastern European colonial anarchist currents was their attempt to combine national liberation with anti-statist and social revolutionary goals. The Empire initially stretched from Tunisia, through Tripolitania, Fezzan and Cyrenaica (Libya) and Egypt in the western Arab lands of the Maghreb, down to Puntland and Yemen, enclosing the Red Sea, from Budapest through to the Balkans and Anatolia to the Caspian Sea, and the eastern Arab lands of the Mashriq (the Middle East and Arabian Peninsula) as far as the Persian Gulf. It was a multiethnic empire in which Arabs, Persians, Turks and Kurds dominated, but which had significant Slavic, Armenian, Greek, Romanian, Roma (Gypsey), Albanian and Ladino Jewish minorities as outlined by Tunçay and Zürcher [8]. The Empire entered a period of modernising reform called the Islahat from 1856, and in 1876, became a constitutional sultanate under Sultan Abdulhamid II.

In 1876, a year in which an uprising shook eastern Macedonia, the libertarian socialist poet and journalist Christo Botev, viewed as a Bulgarian liberation martyr, was killed in the mountains at the head of a detachment of partisans which was fighting against Ottoman imperialism. Botev had been forced to live in exile in neighbouring Romania where he had contacts during 1869 with the nihilist Sergei Nechaev (on his way back to Russia), at that stage a temporary ally of Bakunin, but although one source claims Botev was under Bakunin’s influence, it appears from samples of his writings in the periodicals Duma (Word) and Zname (Standard), cited in Grancharoff [9] at page 1, that Botev was more an adherent of Proudhon and Fourier than Bakunin. Another martyr of the Bulgarian liberation struggle was Vasil Levski (1837–1873) who Grancharoff cites at page 2 as having said, in reply to the question of who was to be czar after the liberation: “If you fight for a tsar you already have yourself a Sultan.” The Empire began to slowly unravel with its defeat in war against Russia in 1877–1878, which resulted in the loss of Bessarabia to Russia and of Cyprus to Britain, the independence of Serbia, Montenegro and Romania, and the creation of an autonomous Bulgarian province — although Ottoman patronage, though for decades afterwards it remained a pawn in the power-play between Russia, Austro-Hungary and the Ottomans. The Sultanate responded by suspending the constitution and increasing repression at home.

In response to this repression and to the desire for national liberation, the late 19th Century saw Marxist and anarchist tendencies emerge amongst the Bulgarian, Macedonian, Greek, and Jewish minorities within the Empire. Much of this activity centred on the port city of Thessaloniki (Salonica), but there were also some activities in Constantinople (Istanbul) and elsewhere. In 1878, the Armenian anarchist Alphonse Jhéön was executed by czarist agents after the Turks were defeated in Bulgaria. A monument to him, funded jointly by anarchist and nationalist societies, was erected in the central square of the Armenian capital Yerevan [10]. The Armenian-language Hamaink (Commonwealth) was published from 1880 to 1894, first in Resht, Persia, (presumably Rasht, Iran, near the southern shore of the Caspian Sea, safely outside of Ottoman territory), and later in Paris and London by the anarchist militant Alexandre Atabekian, a friend of the leading anarchist-communist theorists Piotr Kropotkin, Élisée Reclus and Jean Grave. This was a daringly radical initiative, given that Persia would only undergo a constitutional reform movement in 1906–1912. It is worth noting that a suspected anarchist attempted to assassinate the profligate Persian Shah Mozzafar-al-Din while on a trip to Paris in 1900.

Atabekian made several attempts to distribute anarchist pamphlets in Constantinople and Izmir. According to Panagiotis Noutsos in Tunçay and Zürcher at page 79, there were subscribers in Constantinople to the Greek-language paper Ardin, which promoted between 1885 and 1887, “a loose set of socialist concepts... in which a discreet preference for the ‘autonomous’ socialism of Kropotkin could be distinguished”. By about 1877, Noutsos suggests, the “Democratic Popular League of Patras” in Greece, which was affiliated to the Switzerland-based anarchist International,

“... was already in touch with the first socialist and syndicalist cells in Istanbul, where the impact made by Italian refugees was noted.”

Subsequently, anarchists from Constantinople were represented at the 1881 founding of the Black International by Errico Malatesta. Atabekian was also the moving spirit behind the 1891 Russian anarchist circle in Geneva that published an anarchist analysis of the Armenian question, linking independence to the social revolution. In 1890, In Tbilisi (Tiflis), Georgia, Atabekian was instrumental in founding the Armenian Revolutionary Federation (HHD, or Dashnaktsutiun), a hybrid organisation of anarchist, nihilist, nationalist and socialist revolutionaries which split from the Armenian nationalist-Marxist Hnchak Revolutionary Party — founded in Geneva in 1887 — and fought against Ottoman imperialism. According to Anahide Ter Minassian in Tunçay and Zürcher at page 129,

“Anarchism never had many followers among the Armenians, although the Dashnhak tradition claims that Christaphor Mikaelian, one of the three founding fathers of the ARF [Armenian Revolutionary Federation], used to be a Bakuninist and remained a partisan with a firm belief in direct action and decentralistion all his life. The only Armenian anarchist to have a memorable career was Alexandre Atabekian...”

The subsequent activities of these anarchists in the national liberation movements via the Dashnaktsutiun, the Macedonian Clandestine Revolutionary Committee (MTPK) and the Internal Macedonian Revolutionary Organisation (VMRO) were directed against Ottoman imperialism, but opposed nationalism, with the paper Otmustenie (Revenge) arguing for an alliance with ordinary Muslims against the Ottoman sultanate.

The Jewish anarchist Abraham Frumkin was active in the heart of the Empire. Born in Jerusalem in 1872, he had worked as a teacher of Arabic, and went to Constantinople to study law in 1891. He lacked funds, left for New York, where he came into contact with anarchism, and returned to Constantinople with a large amount of anarchist materials in 1894. He had some success amongst the Jewish community, bringing in more materials from London and Paris, including the Arbeiter Fraint (Worker’s Friend) from London. Frumkin and Moses Schapiro, who had joined the anarchists, went to London in 1896 and set up a publishing house producing Yiddish anarchist materials [11].

Also in 1896, twenty-six armed women and men of the Dashnaktsutiun seized and held the Ottoman Bank in Constantinople to draw attention to the Armenian cause against the Abdulhamid regime. The action was successful, but pogroms against the Armenian community ensued. Feroz Ahmad, in Tunçay and Zürcher at page 18, argues that

“... groups such as the IMRO, the Dashnak and the Henchak may be seen as much anarchist as socialist, in that they proposed opposing the Hamidian regime by violent and militant means. They also espoused statist tendencies common to the socialist movement, though they stressed mutuality and co-operation as the fundamental principles of the reorganisation and restructuring of society.”

He argues, however, that the nationalist Young Turk movement of the Union and Progress Party (CUP), the Unionists, which arose in the twilight of the 19th Century was influenced by the reformist current around Jean Jaurès in the National Confederation of Labour (CGT) of France, rather than the anarcho-syndicalist current, then dominant. According to Khuri-Makdisi [12] at page 230, while the writers Hamit Borzarslan and Sükrü Hanigolu had in separate works argued for

“... the influence of anarchism on late Ottoman political thought and specifically on the Young Turks, they have framed it rather narrowly, and have mostly focussed on its use of terrorism and political violence, rather than analyze its ideology. For instance, both authors have written about the existence of an association based in Istanbul and called Türk Anarsistler Cemiyeti [Turkish Anarchist Society] in 1901, and have shown that a number of prominent Ottoman political figures and thinkers, such as Abdullah Cevdet, Yahya Kemal, and Prince Sabahaddin, were influenced by anarchist thinkers such as Élisée Reclus.”

But Khuri-Makdisi notes, at page 223, that

“... although many Young Turks had initially been attracted to anarchist ideas — mostly through their adoration of the French Revolution, their desire to dethrone and even kill Abdulhamid, and their embrace of biological materialism — they soon shed this attraction and developed a deep fear of anarchism and what it meant: empowering the masses, eliminating political parties, and destroying the State.”

The Armenian Social Democratic Workers’ Party (ASDWP), founded in Baku in 1903, was Armenian nationalist in orientation and was opposed by the uninfluential Armenian Bolsheviks who were hostile to the project for the creation of an independent Armenia. The Dashnaktsutiun adopted a social-democratic programme in 1907 and joined the Second International in the same year, losing any anarchist content it had earlier possessed.

The Bulgarian anarchist movement grew from the first groups in the 1890s — and the territory became a staging-point for anarchist anti-imperialist activities against the Ottomans, particularly in support of Macedonian independence. In 1893, the Bulgarian Macedonian Edirne Revolutionary Committees (BMERC) — named after the Thracian town of Edirne — were founded in the port city of Thessaloniki and laid the groundwork for radical agitation in the region. From its early years, the BMERC had two main factions: a right wing that favoured Bulgarian annexation of Macedonia on the basis of the cultural and linguistic similarities between these southern Slav nations, and a left wing that favoured Macedonian autonomy. Bulgarian anarchists in the so-called “Geneva Circle” of students such as Mikhail Gerdzhikoff (1877–1947), co-founder in 1898 of the Macedonian Clandestine Revolutionary Committee (MTPK), which had as its mouthpiece Otmustenie (Revenge), played key roles in the anti-imperialist struggle.

In 1897, Ottoman police reprisals against the BMERC radicalised the organisation, turning it towards armed struggle. By 1903, Gerdzhikoff was a guerrilla commander in the MTPK’s armed wing, the Leading Combat Body (LCB) which helped stage a revolt against the Ottomans in Thrace. At least 60 anarchists like Nicholas Deltchev and Jules Cesar-Rosenthal gave their lives in the great Macedonian Revolt of that year, which is also known as the Ilinden-Preobrazhenie uprising according to the dates on the Gregorian calendar. In this revolt anarchists made an attempt to extend the struggle in a revolutionary direction, to move the struggle beyond just “flag independence” but towards the social revolution of working and poor people. Gerdzhikoff’s MTPK/LCB forces, only about 2,000 strong, armed with antique rifles and facing a Turkish garrison of 10,000 well-armed troops, managed to establish a liberated zone in the Strandzha Mountains of Thrace, centered on the Commune of Czarevo (Vassiliko). The Thracian uprising [13] was timed to coincide with another in Macedonia proper by the descendant of the BMERC, an organisation best known as the Internal Revolutionary Organisation of Macedonia (VMRO), in which other anarchists played key roles within its left, pro-independence wing.

With close ties to the Russian populist Social Revolutionaries, VMRO secretly organised a guerrilla force approximately 16,000 strong throughout Macedonia and on July 20, 1903, launched synchronised surprise attacks on imperialist targets. Its theatre of operations included present-day Macedonia, Greece, Bulgaria and Serbia. The rebels established the Kruševo Commune in the village of the same name, under the socialist school-teacher Nicola Karev, as well as similar structures in the villages of Neveska and Klisura, all in the Vlach Mountains. Food, shoes, medical aid and ammunition was distributed to the people who elected a co-ordinating committee with equal representation from the Bulgarian, Aromanian (Vlach) and Greek ethnic communities. A notable feature of the revolt was that Turkish civilian settlers were left in peace. Also of importance was the fact that Russian and Italian anarchists fought alongside the rebels. Although the revolt was brutally crushed (with hundreds of women gang-raped by soldiers and 15,000 killed) in both Macedonia and Thrace at the end of August by 40,000 Turkish troops aided by cavalry and artillery, it not only gave the people of Macedonia a taste of true social revolution, but was one of the final blows which sealed the fate of the Ottoman Empire. Grancharoff is disparaging about the Macedonian endeavour, saying at page 3 that “much energy was wasted in this movement while the issue of anarchist organisation within the country was ignored,” and that “the struggle was undermined and manipulated by the Bulgarian monarchy”. But an anarchist-communist assessment in 1948 put it so:

“... much of their energy [that of the Bulgarian intellectuals and proletariat] went into the national-revolutionary struggle of the Macedonians. Thus the Bulgarian revolutionary movement was deprived of a host of courageous men [sic.], a very grave loss; but for all that, this activity was a precious contribution to the Balkan struggles for liberation. The pioneers of this movement were Anarchists, and the Bulgarian public knows that the Macedonian national-revolutionary movement is primarily the work of Bulgarian Anarchists whose clear understanding of the national-revolutionary movement never allowed them to isolate the struggle for Bulgarian national liberation from the social struggle.”

So it was that, as Grancharoff says, “small [anarchist] groups continued to operate illegally” and sporadic MTPK and VMRO guerrilla activity continued until about 1915, but Macedonia was divided between Serbia, Greece and Bulgaria, only attaining independence in 1991. According to Yalimov in Tunçay and Zürcher at page 95, there appeared in the Bulgarian Workers’ Social Democratic Party (BWSDP)

“... in 1905, an anarcho-liberal group which opposed centralism in the party and stood for the independence of the unions. Similar views were reflected in the organizations in the Ottoman Empire wherever the Bulgarian socialists were influential.”

Quite what Yalimov means by “anarcho-liberal” is unclear, but his description of their decentralist, syndicalist politics appears to conform more to the broad anarchist tradition rather than to some odd hybrid as the term suggests, and also echoes the emergence of anarchists from within social-democratic parties in other countries such as Germany. In 1906, inspired by the Russian Revolt, the first Bulgarian anarchist journals appeared: Anarchists and Svobodna Misl (Free Thought). The revolt, however, hopelessly divided the BWSDP into a Menshevik-styled reformist Shiroki Social Democratic Party and a Bolshevik-styled Tensi Social Democratic Party which both ignored Bulgaria’s extensive peasant majority to focus on its tiny industrial proletariat. Bulgarian delegates were present alongside their Croatian, Czech and Polish comrades at the International Anarchist Congress in Amsterdam in 1907, the result of which was a clear international shift away from insurrectionism — and within three years, the first Bulgarian Anarcho-syndicalist organisations were founded, with an anarchist-communist mass movement established in 1919 [14].

1907 was also the year in which an emergent radical Syrian-Lebanese network centred on Daud Muja‘is — editor of the Arabic-language journals al Nur (The Light) of Alexandria, Egypt (1904–1908), and al Hurriyya (Freedom) of Beirut (1909–1910?) — first celebrated May Day, at the town of Dbayeh near Beirut. The Muja‘is circle also started reading rooms and free night schools in Mount Lebanon, which became key to the spread of radical ideas among the populace. Ibrahim Yalimov in Tunçay and Zürcher at page 91, notes that the Ottoman working class was tiny because of industrial underdevelopment, numbered a mere 100,000 in the entire empire prior to 1914 (compared to a total emperial population of 18,5-million, excluding Arabia, in that year), and was concentrated in the main urban centres of Constantinople, Thessaloniki, Izmir, Kaválla and Beirut. Ahmad, at page 15, argues that because there was “as yet no significant working class — either numerically large or militantly conscious” in the Ottoman Empire, “the strikes and boycotts which followed the restoration of the constitution in 1908 under the Young Turk revolution that overthrew the sultanate were more syndicalist than socialist in nature” — though he means this negatively, that the “emphasis was on action rather than theory”.

In the brief flowering of freedom that followed the Young Turk’s victory, the fact of Bulgarian independence was finally confirmed in 1908 (thus events in Bulgaria itself from then on fall outside of our study), and a Workers’ Federation of Salonica (WFS) was founded by militant Jews, Bulgarians and Macedonians in 1909, the year in which Sultan Abdulhamid, who had launched a counter-coup against the Young Turks, was finally unseated. Together with a Bulgarian socialist group in Thessaloniki, the WFS formed the “Workers’ Party of Turkey” (WPT), that affiliated to the Second International as a sub-section of the empire. It produced a weekly Workers’ Newspaper, initially in four languages: Turkish, Greek, Bulgarian and Ladino. Although the WFS was a politically mixed organisation, Paul Dumont hints at anarchist influence, stating in Tunçay and Zürcher at page 61 that it organised Thessalonika’s first May Day celebration in 1909, and at page 56 that WFS militants such as Abraham Benyaroya and Angel Tomov

“... were convinced that they had at their disposal an irresistible weapon: the federative principle. It was by means of a federation of trade unions and political organisations that they intended to put an end to the dissentions between the various national groups that together constituted the Ottoman proletariat.”

But the WFS fractured in 1909 when the Bulgarians withdrew over a dispute with the Jews over the presence of bourgeois elements in a WFS demonstration against the Spanish state’s execution of the anarchist educator Francisco Ferrer. This reduced it to a primarily Jewish organisation, and its multilingual newspaper became the exclusively Ladino journal Solidaridad Obrera (Workers’ Solidarity). From 1909 onwards, the Young Turks’ CUP regime, over-reacting to the threat posed by the sultan’s failed counter-coup, reintroduced censorship, banned strikes and threatened to rescind the autonomous status of the Ottoman province of Mount Lebanon, centred on the port city of Beirut. According to Khuri-Makdisi at pages 215, the disappointment caused by the collapse of the promise of the Young Turk revolution pushed the Syrian-Lebanese radical network centered on the journal al Hurriyya further leftwards, so that, at page 220,

“... the Syrian radical circle began to express interest, sympathy for and identification with specific anarchist ideas and modes of action.”

In 1909, the Muja‘is network put on an acclaimed play on Ferrer’s martyrdom and al Hurriyya, which began printing that year, published an article on “the philosophy of bombs” by one Stavinsky Polikivich in which, Khuri-Makdisi argues at page 221, his analysis stemmed from “revolutionary anarchist and nihilist practices”. It also published, in 1910, articles by Khairallah Khairallah calling for the establishment of a non-capitalist, classless society. Khuri-Makdisi states at page 222 that, for Syria-Lebanon,

“The international brand of leftist thought which anarchism represented was to have a specific resonance, given local realities. First, members of [the local] radical network and anarchists worldwide shared a common enemy, the Church, which had been identified as a prime target by many European anarchists. In particular, the Spanish brand of anarchism which received attention in the pages of al Hurriyya during the Ferrer affair, had called for and destroyed a significant number of Church property... Besides fitting in well with the growing anti-clerical movement in Beirut and Mount Lebanon, anarchism had yet another local appeal: it was viscerally feared and hated by the Unionists... It is easy to see how radicals opposed to the Young Turks and their policies, in Mount Lebanon and Beirut, would hence be attracted to anarchism.”

In Thessalonika, the WFS limped on, suffering from repression from the Unionist authorities, until suppressed by the outbreak of the First World War in 1914. A separate Ottoman Socialist Party (OSP) was founded in 1910. In 1911, the Ottoman Empire was further eroded when it lost Tripolitania (Libya) to Italian imperialism. In the 1910s, a “Socialist Centre of Istanbul” was founded, calling for the Ottoman working class of all nationalities to unite against capitalist exploitation. It was later renamed the Socialist Studies Group. Panagiotis Noutsos states at page 78 in Tunçay and Zürcher that the Centre’s key figure, the trade unionist and printer Zacharias Vezestenis, played

“... a leading part in the formation of the trade union movement and in the socialist debate among the Greeks of Istanbul (he frequently sent reports on events to [the anarchist newspapers] Bataille syndicaliste and Temps nouveaux in Paris)...”

In 1918, the remnants of the WFS, the core of the Centre and anarcho-syndicalists such as Konstantinos Speras were among the founders of the Socialist Workers’ Party of Greece (SEKE), the libertarian precursor to the authoritarian Greek Communist Party (KKE). In the aftermath of the First World War, the ailing Ottoman Empire, which had fatally sided with the Central Powers, was finally dismembered: Anatolia and the rump of Thrace bordering Constantinople became the new state of Turkey, the Young Turks were overthrown and Sultan Abdulhamid briefly installed as an Entente puppet; Greek gains in Thessalonika were confirmed; Syria-Lebanon fell under French mandate in 1920; Palestine, Transjordan (Jordan and the West Bank) and Mesopotamia (Iraq) fell under British mandate in 1920, 1923 and 1920 respectively; and a short-lived Armenian Republic was established by the Dashnaktsutiun, by then under communist influence, in 1918–1920 (an ephemeral Armenian Communist Party that lasted as long as the republic was the result. The Dashnaktsutiun was revived following the second Armenian independence in 1990 and exists today as a socialist parliamentary party). In this period, Noutsos states at page 88,

“... there was a clear strengthening in the ‘economic organization’ of the working class of the Ottoman urban centres, and its unions (which were initially under the influence of French ‘syndicalism’ and later of the ‘Industrial Workers of the World’) often took precedence over political representation; there was more distinct co-operation among the national groups, including the Turkish groups; relations with other countries expanded (after 1920, Western European [French CGT] and American [IWW] influence declined and was replaced by that of the Soviet Union...”

The year 1920 was a watershed not only because it marked the formation of the Turkish Communist Party (TKP) — although a Turkish Workers’ and Peasants’ Socialist Party also operated in the 1920s — but because Turkish nationalists under army officer Kemal Atatürk launched a successful liberation war against the Entente occupying forces, overthrowing Abdulhamid again and installing a secular republic. Tunçay argues at page 165 that “the communist movement in Turkey before 1925 adopted a particular concept of patriotism, partly under the influence of Soviet support for aid to the Kemalist nationalists. Nevertheless, the TKP was criticised for collaboration with the bourgeoisie in some early Comintern congresses”. The Ottoman Empire was finally dissolved as a state in 1923.

In 1924, Turkey became ruled by an authoritarian secular regime that the following year — in partial response to a Kurdish rebellion — outlawed all political opposition, forming a one-party state with distinct leanings towards Soviet Russia. Atabekian disappeared in Russia in 1929 in an anti-anarchist crackdown by the Bolshevik regime. The Comintern policy from 1936 of creating popular fronts with anti-fascist forces was opposed by the TCP, which Tunçay said, “lead to the exclusion of the TCP from the international communist movement”. According to the obituary of the Makhnovist veteran Leah Feldman (1899–1993), there is a suggestion of an anarchist movement on the eastern Mediterranean island of Cyprus, then a British possession: “Leah was a member of a working group of anarchist women in Holborn [Britain] ever since 1939” that included “Greek Cypriot and Turkish Cypriot” militants, a collaboration across nationalist lines that echoes that of the old Muja‘is network.

 
[1] Online resources on the situation in Turkey and Kurdistan include: An interview by anarkismo’s José Antonio Gutierrez Dantón with Sinan Çiftyürek, the spokesman of the Mesopotamian Socialist Party, a revolutionary Kurdish group, at: www.anarkismo.net and, for a broader perspective, “Crisis in Turkey and the Perspectives for the Left: Modernisation, Authoritarianism and Political Islam” at: www.anarkismo.net . A collection of older anarchist writings and notes on Turkey and Kurdistan can be found at Stiobhard’s collection “Libertarians, the Left and the Middle East”: stiobhard.tripod.com One of the best English-language websites that covered the Kurdish question, the Toronto-based autonomist anti-imperialist Arm The Spirit, sadly appears to be defunct since around 2000, but many of its documents are cached and replicated on other sites.
[2] Participants in the 6,000-strong anarchist contingent in the May Day march in Paris in 2000 will remember the TKP-ML member, one of about 2,000 pro-Kurdistan supporters, who climbed the scaffolding on a building at the gathering-point to plant a party flag at the top, being arrested by the police when he got to the ground — and then promptly “unarrested” by the anarchists and returned safely to his comrades who had stood by and watched. We wonder whether he remains a Maoist today or whether he has defected to us!
[3] Anarchism in Turkey, Karambol Publications, London, UK, 1996.
[4] We Come to Bury the Turkish Republic, Not to Praise It, 5th of May Group, London, UK, 1998, online at: flag.blackened.net
[5] Do The Kurdish People Need a State?, published in Umanita Nova, Italy, 1996, online at: flag.blackened.net
[6] The little-known hey-day of the early Siberian anarchist movement (1907–1928), which spread along the Trans-Siberian Railroad, establishing an IWW presence in the coal-fields and Ural Mountains and armed by a sort of “mini Makhnovschina” on the steppes, is the subject of a forthcoming study by Schmidt and van der Walt.
[7] Founded in 1299 and centred on the city of Constantinople (today Istanbul), the Ottoman Empire at its height at 1683 sprawled across three continents. Over centuries, the increasingly stagnant Empire was gradually whittled away by war losses, provincial secessions such as that of Greece, and foreign purchases, so that by the time our narrative begins in 1880, the Empire had shrunk considerably, and soon lost the last of its North African territories (Tunisia to France in 1881 and Egypt to Britain the following year). On the losing side of World War I, it was finally dissolved in 1923. By the “heart of the Ottoman Empire” we mean the territories comprising current-day Turkey (including Thrace) and its immediate Middle Eastern littoral territories in what are today Armenia, Syria, and Lebanon. In our study, we exclude the further-flung territories of Bulgaria (autonomous, but under nominal Ottoman control from 1878–1908), Macedonia, Palestine (Israel, the Palestinian territories and Jordan), Mesopotamia (Iraq) and the Arabian peninsula territories.
[8] Mete Tunçay and Eric Jan Zürcher, Socialism and Nationalism in the Ottoman Empire, 1876–1923, British Academic Press in association with the International Institute of Social History, Amsterdam (London, New York), 1994.
[9] Jack Grancharoff, The Bulgarian Anarchist Movement, unpublished document drawn up by the Bulgarian anarchist veteran especially for the authors, Quamaa, Australia, 2006.
[10] A collection of older writings on Armenian anarchism can be found at: stiobhard.tripod.com
[11] Frumkin later immigrated to the United States, whilst Schapiro returned to Constantinople, was later involved in the Russian Revolution, and helped found the Anarcho-syndicalist International Workers’ Association (IWA) in 1922.
[12] Ilham Khuri-Makdisi, Levantine Trajectories: The Formulation and Dissemination of Radical Ideas in and between Beirut, Cairo and Alexandria 1860–1914, Harvard University, USA, 2003.
[13] The primary French-language anarchist analysis of the Macedonian national question is Liberation Nationale et Liberation Sociale: l’Example de la Revolution Macedonienne, Georges Balkanski (Georgi Grigoriev), Collection Anarchiste, Federation Anarchiste, Paris, France, undated.
[14] For an account of Bulgarian anarchism in the period 1919–1948, read The Anarchist-Communist Mass Line: Bulgarian Anarchism Armed, Michael Schmidt, Zabalaza Books, South Africa, 2008, online at: www.anarkismo.net This is the first in a planned series on anarchist-communist mass organisations which will include studies on Manchuria, Uruguay, Argentina, and Ukraine.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

