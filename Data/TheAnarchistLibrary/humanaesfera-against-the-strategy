      CRITERION OF KNOWLEDGE AND PRAXIS      CLASS COMPOSITION VERSUS STRATEGY      ADDENDUM: THE FETISHISM OF "PRACTICAL EXAMPLES"
There is nothing more fruitless and misleading than activism, militancy or the craving for "action". To exist is to act. The proletarians are not beasts who do things blindly or by instinct. There are no actions without purposes, objectives, desires ... that is, existence (ie action) presupposes and implies theories that the proletarians create and enhance (or degrade and dogmatize) as their ability to act is objectively increased (or decreased).

Let us explain:

The proletariat's capacity to act is increased when they trust themselves (from an internationalist viewpoint), do not believe in "scapegoats", impose the satisfaction of their needs (which are communists: do not work and that everything be free, "gratuitous") and oppose radically, by this simple act, the ruling class (for whom, of course, this is "oppressive"); consequently, they attack power by dissolving what supports it (the mutual opposition among proletarians in enterprises, nations, race, gender, etc, fighting with each other to defend their own masters) through a material universalism (communism) which ensures free acess for anyone to the means of production and life, the free and autonomous expression of human capabilities and needs. The free individuality is no longer hindered by the reifying, identitarian, massifying comparison that consists of competition, private property, hierarchy, market and state.

On the other hand, the capacity to act is decreased when the proletarians are wary of themselves (to the point of massacring themselves by a simple request of the chiefs and powerful), are willing to attack scapegoats (foreigners, "Jews" immigrants, "vagabonds," "slum dwellers", "bad politicians", "bad businessmen"), and when they repress their desires on behalf of fiction of a "greater good" (nation, enterprise, ethnicity, ideology, religion ...), that is, when they unite with "their" ruling classes (bureaucratic or private, of the left or right) against themselves. The less they are able to act, the more they surrender to the reaction.

In the first case (increased capacity to act), the theory necessarily develops and is enriched, whereas in the second case (reduced capacity to act), the theory can only degrade and dogmatizing.

The criteria in order to distinguish lies or rumors from truth, speculative from probable, things that are true in some contexts and false in others, what is based on mere faith from what is based on evidence etc., these criteria are intrinsic expressions of the degree of autonomy or heteronomy of the proletariat, of their self-determination or their subjection to the ruling classes. In this regard, there are three types of critique of capitalist society:

A) There is a criticism of capitalist society whose truth can be checked materially by anyone in their everyday lives, worldwide, by any proletarian: the critique of capital as a relation that forces us to sell ourselves as useful objects in the labor market, which coerces us, if we want to survive, to sell our capabilities in exchange for money, which coerces us to exercise our capabilities against ourselves, making the world a hostile force that accumulates, depriving us (private property) of our own material conditions of existence, a hostile power that dominates us, use us and discard us: capital and the repressive apparatus that guarantees it (the state). Such is a critique of the essence of capital, it is radical, and it invariably implies the irrevocable need to abolish work, private property, the interprise, the borders and the state, that is, effectively communism [1]. Detail: requires no faith in "special sources of information."

B) There are other criticisms that require some "faith", as are the partial criticism of the capitalism (which are basically social democratic criticism: unequal distribution of income, planned obsolescence, deterioration of living conditions, of the environment, capitalists and bureaucrats circumventing laws, governments plotting the overthrow of others ...) which are nothing more than criticism of accidents of capital, not of its essence. While in the case A, the proletarians are fully autonomous as to be able to verify the truth of his knowledge (which expresses the matter of his own everyday life) and act according to what they know, in case B, they must rely on experts. Yet the truth of this criticism can be weighed in the everyday life (for example, verifying in fact the worsening of living conditions, or not verifying the planned obsolescence). But the less radical and more partial is the criticism, since it is more "inaccessible", the more require that the activity of proletarians submits to "higher spheres", and less expresses an autonomous praxis able to oppose the capital in order order to impose the satisfaction of human needs.

C) And there are criticisms of capitalism that only require faith, a faith entirely based on "special sources of information", a faith that is accepted on the basis of vague "psychological intuitions" or appeal to feelings. For example, speculative criticisms (for example, those who prophesy the "inevitable collapse of capitalism" as the new "critical criticism" - Kurz, Postone, Jappe... -, the acceleracionism, transhumanism etc.), conspiracy theories ("occult forces" that are plotting the suffering and annihilation of the poor, the people or nature) and identitarian criticisms (who claim an identity - gender, race, ethnicity, nationality, culture - against other identities that supposedly "represent capitalism"). In practical terms, these criticisms requires complete submission, the complete annihilation of the ability to think and to act of the proletarians, and the assumption as true of any rumor or lie to confirm the "intuitive" prejudices (eg, the paranoid lies about GMOs , chemicals, vaccines, medicine, science, natural products, technology ... as many environmentalists propagate). The supreme example is the religion itself, in which faith in the revelation of an occult absolute truth requires total obedience to those who claim to have special access to it (hence the word "hierarchy" of hieros, sacred or secret, and arché , source, principle or order).

Some argue the case A, full autonomy, is insufficient because it is abstract and philosophical, and we need the case B, because strategy is needed (eg, "transition period"), which is even seen as more fundamental.

But to talk about strategy only makes sense against a strategy of the opposite side, that is, when there is a presumed counter-strategy. This is not class struggle but a war of fronts, which implies a single chess board, a single language, a single shared logic, implies both sides confront each other as equivalents. In order to war against each other, they need to be in the same plane, be based on the same structure, position themselves in the same level, talk on equal terms. Hence all counter-revolutions in all "victorious" revolutions that have existed, since the same structures (domination, class society, state etc.) of the enemy are reproduced in the name of attacking him.

The great virtue of the proletarians is that they, as an autonomous class, can not attack the structure on the same level of the structure, but as a product, as a resulting molecular production of their own simultaneous everyday activity worldwide. If they attack the structure on the same level of the structure, using a defined strategy, they are condemned to reproduce their own subjection under the same or some new ruling class, because their field of action, the universal simultaneous everyday activity, is doomed to remain unchanged (work, self-sacrifice, subjection ...) to carry out the strategy itself, reproducing automatically, perhaps with new names, the same structures which necessarily result from the alienated everyday activity.

As opposed to the ideology of the strategy, the proletarians can only rely on their own autonomous capacity to act and think, boosted by the rapid spread of their struggle worldwide. In the same act, they communicate with each other worldwide the knowledge of how their simultaneous daily activities interconnects (eg. according the place where each is, the supply chains, the relationship between industry, agriculture and the materials pathways for the free expression of needs, desires, thoughts and capabilities of the residents and travelers of world, etc.) [2], a knowledge that is simultaneous with the active suppression of the material (molecular) conditions of existence of private property, capital and the state and with the creation of a new society in which the means of life and production, inextricably interconnected worldwide in a network of immanent flows, become freely (gratuitous) accessible to anyone who wants to meet his needs, desires, thoughts, projects, passions, and develop freely their skills, abilities and potentials.

An event like this, which disables the basis of the power of the ruling class (businessmen, bureaucrats and the state), has from the very beginning an incomprehensible and non-negotiable language with the ruling class and the state, being in fact a dictatorship against them – the true dictatorship of the proletariat. The ruling class not even have time to begin to understand what is undergoing and can not devise a strategy before the proletariat have self-abolished and thus abolished the ruling class, the class society. Very different from that, activism and militancy is characterized by showing off spectacularly to the ruling class as "opposition". Obviously, the weapons of the ruling class, the state, the death squads, etc. are infinitely more powerful and refined than any "strategic opposition movement" [3], which consequently is merely spectacle - only useful to the ruling class rehearse their watchdogs and control methods, which, staging, legitimizes the status quo itself as "democratic". And when it is not staging, the "strategic opposition movement" is only the reproduction of the structure to which seeks to oppose, as we saw in the preceding paragraphs.

Obviously, the more reduced the capacity to act of the proletariat, the less it can have the luxury to think for yourself, and more it can only be the object of strategies, of bureaucrats, businessmen and politicians who say they think and act for their "well," promising, for example, reforms, improvements etc. So they say we must be realistic, that the proletariat must do everything possible, voting, participating in campaigns, militating, "trying harder", "sacrificing more " etc., in short, participating in strategies. This is a mistake. For if there is no autonomous struggle, it is sheer luck, and extremely unlikely to occur any of the promised improvements; and if there is autonomous struggle, it makes no sense to let us reduce as object of strategies. The immediate side effect of the autonomous struggle is that all bureaucrats, businessmen and politicians, in order to contain the emergence of the proletariat as a class, pass finally to serve such "improvements", but, of course, in the same dish of repression. The point is the autonomy of the proletariat spread so quickly on a global scale that makes it impossible to fall into this trap again.

The revolutions and counter-revolutions we have experienced in the past 300 years have shown us that the most destructive ideology for the world autonomous struggle is that of the "practical examples". As soon as one hears about a "revolution" anywhere in the world, ones abandons all critical faculties and consideration for the truth, which then is considered insignificant when compared the "real practical example of how to transform the world". The reality of the example is considered so complex that all criticism and search for truth is dismissed as reductionist mental masturbation and utopianism. Abandoning the capacity to think, this opens the way for a superstitious “taskism”, destroying the autonomous struggle, whether by the imaginary fight that mimics the appearance of spectacular example, whether by the acceptance of subordination to bureaucrats considered representatives of the example (as when the leninism spread in the world and destroyed the autonomous struggle everywhere thanks to the "unquestionable reality of their example," 1917 in Russia).

As an antidote, there is indeed a accurate minimum criterion to evaluate each and every supposed example (as Kurdistan, Zapatistas, Russian and Spanish Revolution etc): if a supposed revolution does not spread quickly beyond the borders for the whole world (with the proletarians opposing their oppressors in more and more places on earth and constituting themselves as an autonomous class without borders, refusing to kill in wars, turning their weapons against the generals on all sides, communizeing etc.), if the supposed revolution is perpetuated only in one place, this is enough in order to know that there is a state and capital (regardless of the name that use, "self-management", "socialism", "communism", "anarchism" ... ), ie a class society. By the simple fact that, isolated, they are condemned to conform to exchange in the world market, accumulating capital and exploiting the proletariat in order to not to go bankrupt in international competition, and also because they are doomed to be constituted as a state to ally himself, to defend or attack other states.

humanaesfera, July 2016
[1] “It is not a question of what this or that proletarian, or even the whole proletariat, at the moment regards as its aim. It is a question of what the proletariat is, and what, in accordance with this being, it will historically be compelled to do. Its aim and historical action is visibly and irrevocably foreshadowed in its own life situation as well as in the whole organization of bourgeois society today.” (Marx e Engels, The Holy Family or Critique of Critical Criticism.)
[2] It is the class composition. For more details, see:  - Discussion paper on class composition - Kolinko  - The Network of Struggles in Italy - Romano Alquati  - The factory without walls - Brian Ashton  - Reality check: are we living in an immaterial world? - Steve Wrigh
[3] As opposed to the staging of the "strategic opposition," the only way to suppress the repressive force of the status quo is by an emergency so rapid and widespread of the autonomous proletariat (hence of communism) that the ruling class not even find where start repressing, so that their repressive watchdogs will no longer see any point in continuing obedience, ceasing to be watchdogs, turning their weapons against the generals and distributing weapons to the population, for the simple reason they start to be uncontainably and irrepressibly attracted, like the rest of the exploited, to the enthralling emergence of generalized luxurious communism, the worldwide human community.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

