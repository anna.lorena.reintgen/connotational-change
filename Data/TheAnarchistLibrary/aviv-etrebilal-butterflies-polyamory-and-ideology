
Editor’s foreword

This essay is not yet another one on "free love", the “affects“ or "deconstruction". It hopes to be more than that. Written in late July / early August 2013, it served, until October, to lay the foundation for many informal conversations. These discussions were deep and led to a more nuanced and full understanding, as well as raising many questions about the ideological relations that often govern the modes of thought and relationships of the French anti-authoritarian milieu. So if this text is not just another text on the affects, it’s because it is foremost a text on ideology, on scenes and milieus, on inconsistency and leftism. The way it managed to echo many and varied situations that do not necessarily involve emotional relationships, but numerous other issues such as power relations, the conformity of an anti-conformist milieu, how alternatives become the norm, social roles , individual patterns of consumerism, struggles and the tools of struggles etc., make it a text whose primary purpose is to open a debate that will exceed it. If we wanted to publish it today, after these few months of incubation and excited discussions, it is precisely to open this debate, consistent with the content of the text, beyond the limits of sub-cultures and affinity groups. And thus we hope that it will continue its adventure.

October 2013,

Ravage Editions.

***

It is reassuring to see that for some generations of anti-authoritarians, the dogmas which are too often used as our starting points, that consume us and make us go round in circles in a vacuum, are occasionally questioned, that when certain ideological principles end up causing human collateral damage, we are able to question them, abandon them or reformulate them. Companions recently released a text that likely caused excited and important discussions[[“ Amour libre “, vraiment ? Et après ? Published by Le Cri Du Dodo, June 20, 2013.]]. The strength of the writing was that it guided us back in some small way to individuality where it has more or less been replaced with dogma and ideology, and individuals with stereotypes. And when those discussions on free love, coupledom, polyamory, jealousy, non-monogamy etc. did take place between us, most likely in environments where people live together and have occasionally lost their sense of intimacy (squats, communities, etc.) than elsewhere, there was no will to make these discussions public through a text that would not just get passed between one or two groups of friends.

"Free love" is a term in use since the nineteenth century. It originally functioned to describe the anarchist rejection of marriage from the perspective of the individual emancipation of women and men. Its supporters rejected marriage as a form of slavery, primarily for women, but also as an interference by the State and the Church in their privacy, opposing marriage with the notion of "free cohabitation". It consisted in the assertion that two individuals could freely choose each other, love in an irreverent manner, without permission from the mayor and the parish priest and give the finger to all those who wished to interfere in their relationship. Once the concept interacted with educational and communitarian anarchist circles at the end of the Belle Epoque [i], in the form of so-called "loving friendship", it took another sense, though anecdotally, but we shall return to this.

It is really during the 1960s, in contact with the hippie movement, that the term’s meaning totally changed. It suddenly meant having various forms of multiple and joint relationships, as well as opening sexual intimacy to two or more people at once, especially in the form of threesomes and group sex, and most of the time the free-lovers added a dose of mysticism to it all (Tantra, sexual magic etc.).

But "free love" is an expression that is already, in itself, biased, used as it is in a world in which we are not free in any way. It is no wonder that this term prospered in both the educational and communitarian settings of the libertarian movement from the end of the Belle Epoque. Just re-reading that annoying rhetoric of the “en-dehors” [1].

These libertarians, who generally lived in fairly closed communities, where children were "protected" from the outside world (Amish-like) who succumbed to all the ridiculous fashions of the time (oil diet, banning of teas and caffeine, exclusive consumption of nuts, sickly Hygienism, absolute scientism and progressivism etc.), had the feeling of living apart from the world, of living freely. Given the quantity and quality of the revolutionary work necessary to change the world, they used fancy ideological footwork to find a most comfortable position: experience freedom now, among themselves and within their community. These were not the first [2] not the last [3] But we must speak of a total and indivisible freedom because what good is freedom of movement, for example, if there is nowhere to move but in streets filled with shops, surveillance cameras and cops? The same goes for love, how to be free in love when we aren’t free anywhere else?

The typical and historical error of leftism, which is to be satisfied with simply reversing the values of the enemy - to take money from the rich and give to the poor rather than completely abolishing classes, to reclaim the rhetoric of discrimination and turn it into sources of pride (workerism, ethnic, gender and territorial pride of all kinds, …), to do politics better than the official politicians, to invert patriarchy rather than abolishing it etc. - does not spare the arenas of romantic and emotional relationships. It seems therefore that the thing to do would be the opposite of previous generations, of all those parents who have sacrificed their desires and their lives for the institutions of the couple or of the family. It has long been felt that we can invent something new simply by suggesting new prototypes of relationships, modeled on negatives of the old, and then comply with them, as happens with each new norm.

The standard in place today in the way of love and emotional relationships within our milieu is the exhortation to diversity, the moral principle of non-exclusivity, the "creation of an abundance of affection" [4] and having multiple partners. The standard now having been reversed, recalcitrance to the new standard is too. A self-sufficient relationship between two people becomes the new deviance to suppress.

It seems important to reaffirm that two people can feel good together without experiencing the need to multiply their passionate adventures- while also not presenting faithfulness as a moral tenet or wishing to suppress “extramarital" sex because of thoughtless values. But there will always be the loud mouths who believe themselves more liberated than others who will cast down their judgment into the face of others: "they are a couple, shame!"

Basically, why express opinions, as the parish priest or bishop, on things that we do not own and which do not jeopardize our revolutionary project? On things whose issues do not concern us? That one is a believer in monogamy or of polyamory is not the problem of the other. Only one thing is important: everyone should find their fulfillment in their own way without being blinded by any ideology, whether from patriarchal society and its moral imperatives or the milieus of those, who, thinking themselves able to tell who is free and who is not in a world of cages and chains, believe they possess the recipe for freedom. Why refuse to see that the complexity of situations and the complexity of individuals mix together? That if a rule could encompass everyone, it would necessarily be defective and contribute to the negation of individuals? That since it would be a rule, it would once again impede freedom?

How many pamphlets are needed to explain how to fuck, how to love, what relationships one should have with one’s body? [5] How many narrow standards for our desires and perceptions? How many of us, now past the excitement of the misleading freshness of being sixteen or twenty years old, have not managed to find ourselves in these new models of pseudo-freedom? How many have had to suffer being told that they were not made for freedom because they liked only one person and were loved only by one person? How many have whipped themselves for experiencing jealousy, have felt consumed by the other under the pretext of their freedom? How many have felt uncomfortable under the inquisitive eyes of those who believe they are free while living in a social order based on domination? Forgotten in the sectarian and ideological confinement of small cliques, is that there are still billions of people around us.

As in any ideological diversion, even before examining reality, we fit reality to how ideology would like it to be. We do not try to do what we want, we try to want what we should want, and there are plenty of pamphlets, books and texts in the press catalogues of our milieus that explain what we should want, rather than urging us to follow our authentic, individual desires. So in this race for deconstruction and pseudo-freedom, it’s all about being the most open of all, trying anything, because we have to. Or more precisely, we have to in order to feel part of the narrative of deconstruction, better than others, armed, as it were, with a new form of progressivism. So we cannot see past the beam that is in our eye, to invert the biblical metaphor, and no longer see the infinite field of possibilities available to us in the destructive urge- as though the deconstruction of the individual and the destruction of this world could not do well together.

It was good old Kropotkin who said that "structures based on centuries of history cannot be destroyed by a few kilos of dynamite" [6], and he was right, in the sense that physical destruction is not sufficient by itself, that it necessarily must be accessory to a profound renewal of social relations. But nor did he want to express that a few kilos of dynamite could not themselves be helpful in the emergence of splendid possibilities.

Moreover, it is not a few visionaries of deconstruction, modeled on Zarathustra (who retreated into the mountains for ten years, and one day felt the need to share his wisdom with the little-people), that carry the potential to create revolution. Revolution (and to a lesser extent, insurrection) is a social fact, that is to say, like it or not, it will necessitate that at one time or another a large stratum of the population rises. It will be alongside the celebrated "real people" (as we sometimes hear them described) that we might make a revolution, not just with a few anti-authoritarian ultra-deconstructed types who will only be able to participate on their extremely limited scale. Revolution will be the work of these "normal" people, with their qualities and also their many faults, and who are often light years ahead on this issue (and many others …).

But let us return to our butterflies. Armand said that "in love, as in all other areas, it is abundance which destroys jealousy and envy. That is why the formula of unconstrained love should become that of all anarchist milieus." But how can we, then as now, afford to say with such arrogance and satisfaction, what is THE form (" formula "!) of love and sex to be adopted by THE anarchists (or any other social milieu)? The term "free love" already contains in itself this form of exclusion, since it implies that it alone is capable of bringing freedom, but we seriously doubt the possibility of finding freedom through love, whether it is called "free" or not. And is it really freedom that we seek through love?

We must not delude ourselves that in the post-modern era, the concept of freedom is unfortunately too often a pretext for denying individuality as well as the denial of any real will to change the world. "I don’t care and fuck you" seems to be the new freedom, in other words, the notion of a total and indivisible freedom, individual but conditioned by the freedom of the other (which has long been central to anarchist perspectives) was replaced by a sort of already pervasive liberal outlook. Add to this a normalization process which expresses its violence through the marginalization of individuals who are viscerally opposed to these standards, explaining that if this does not work for them it is because they are the problem. But there is nothing surprising in this. After all, this small milieu is the product of the social order, and it reproduces it in return.

But this liberalism has many facets, and goes far beyond the issue of emotional relationships. By habitually thinking in terms of acceptable and sanctioned beliefs and keywords, we ended up being no longer capable of anything other than navel gazing with self-satisfaction in a cozy little bubble where the billions of other humans are forbidden to enter, despite the façade of ultra-social, inclusive speech.

We are told that freedom is about wandering, that it is to flutter, but how then do we embed ourselves in a real revolutionary approach, with continuity, in a neighborhood, a village, a region, a publication, a place, a struggle? Are those who feel free to drift from one struggle to another aware that they can only afford it because someone else is maintaining the continuity? Do they realize that this romantic drifting is really just another form of comfortable consumerism?

We are told that freedom is about wandering, that it is to flutter, but how then do we embed ourselves in a real revolutionary approach, with continuity, in a neighborhood, a village, a region, a publication, a place, a struggle? Are those who feel free to drift from one struggle to another aware that they can only afford it because someone else is maintaining the continuity? Do they realize that this romantic drifting is really just another form of comfortable consumerism?

And when we speak of the revolutionary process as a long process, one which requires substantial efforts and a little "sacrifice" of one’s time, sometimes of one’s freedom and often of one’s comfort, how many are they to be offended, exclaiming: "sacrifice, effort, yuck, dirty capitalist!" Then congratulations dear comrades and companions, you are free, you are not capitalists, you are super deconstructed, but why bother? History will remember that you had fun, but other revolutionaries will remember only that you consumed them, and in the deepest way, this is where capitalism is found: in the consumption of the efforts of the other, but also in the consumption of the body.

To clarify, so that the gossips do not spit their venom through my mouth, this isn’t about opposing revolutionary praxis to enjoyment. I especially want to point out that happiness is not necessarily found in the forms that the spectacle usually gives it. I am not here to advocate any asceticism because what good is it to have fervently critiqued activism only to reproduce it in other ways later. As the product of a certain diversity of experiences, I say that the revolutionary project is found elsewhere than in the false oppositions of leftist militancy and post-modern and subjectivist milieus. Let those who doubt know that we take pleasure and satisfaction in building subversive paths, and that the flutterers and butterflies do not have a monopoly on ecstasy and joy. For as beautiful as it is, the butterfly is an insect that lives only a few days, and whose capacity therefore to develop projects, to consider the future, is severely limited. Butterflies are attractive, and it’s certainly quite romantic to compare oneself to them, but one must choose between becoming revolutionary and merely reveling in the myopia and the immediate gratifications of the inconsequential of liberalism and anarcho-leftism.

We do not necessarily mean by leftism a specific milieu, but trends that are found everywhere in our circles, whether among anarchists, communists, squatters and even among the most ardent supporters of a complete break with the left. As we have said, one of the most important features of leftism is the reversal and inversion of dominant values, which when wedded to a certain form of libertarianism becomes liberalism.

May 68 has probably helped give birth to these new forms of self-absorbed leftism, sometimes in spite of it. In a bourgeois society with an entrenched and stifling morality, many have only sought to free themselves by doing the opposite of what society expected of them, in this way simply creating a mirror image of the same morals. If drug use is a social taboo, why not make a symbol of it and then feel free between two overdoses, one’s head in the gutter? Is the couple a cornerstone of alienation in this society? Then let us be free, orgiastic, fuck as often as we can, collect our passionate conquests and feel free while so many others have only loved people who have used them.

One just needs to open a brochure on "free love", on so-called "liberated" relationships, on non-monogamy, "emotional comfort" and the famous "affects" to realize that the only thing that is being proposed is the total negation of the individual and their use for the sole purpose of egotistical instant gratification, mostly in a ratio of economic accumulation, profit and social cannibalism. So it seems that freedom is having the opportunity to shoot fifty strokes and to "have choices". Reification on every level! Tonight it will be John, he is tall and I’d love to lay a tall one, I am saving Josephine for tomorrow because I like mature women and the day after will be my fetish trip with Billy. Joy unhindered! [7]

But this is a relationship of capital accumulation, of an emotional capital, where the goods are human, considered as social stock, emotional assets accumulated in a romantic bank account. So yes, we are free to exploit and be freely exploited, but then the word ’freedom’ has no meaning: social democracy has won, the economy has won, the time period has won, even our emotional intimacy and our inter-personal relationships have been penetrated to the point of nullifying any form of free association of individuals.

When this world makes us believe that our freedom is found in a supermarket, in the choice between several brands of shit brushes, it operates with exactly the same strategy. Free love or post-modern polyamory as they exist in our milieus are, for the most part, no better than this "freedom to consume". They are actually very similar to that of bourgeois libertinism or the sex friends and other fuck-buddies of urban gilded youth. However, one difference is that bourgeois libertinism gives its practitioners the exciting sensation of breaking or circumventing social norms and prohibitions, providing the thrill of non-conformity and of subverting dominant moral values, even if in a very limited and superficial way. Libertinism in anarchist milieus however is very different in that it enjoys a sort of majority support, which gives the individual participant a sense of complying with the ideological standards of their milieu, despite the unique desires of each person, which of course are perpetually changing, never frozen as with a milieu or any community that sets reductionist rules that must apply to all cases and to all individuals.

Do John, Josephine and Billy really share the same vision of the relationship I have with them, and under the sole pretext that we would have “clearly” discussed? Are we all coming from the same situation when we commit to this type of relationship? Does ideology, combined with the dumbed down language of a world of domination, really clarify everything?

Basically, there is little difference, if we ignore for a moment the differences in posturing, between the free-love consumer and the Emir’s harem from which he chooses every night who he will want to fuck and / or to love while the others prepare him food. There is perhaps one significant difference in our milieus, where an intertwining of leftism and feminism has had an influence: women sometimes have a wider tolerance in the practice of the harem. A bit like men in the rest of society.

The most ideological supporters of free love ultimately make the same mistakes as those who are blinded by ideology generally. They deny the uniqueness and complexity of real-life individuals by replacing them with interchangeable stereotypes. When two people start an ultra-defined relationship, that is to say with the expected discussions intended to “clarify” early on its terms and what each expects from the relationship, we first have to consider the balance between them. Does one of them already have several relationships and not the other? What if one of them is considered "ugly", "beautiful" or "charismatic" and not the other? What if one of them is only looking for affection while the other hopes for love? How is their balance impacted if one of them is happy and the other is unhappy and insecure, or if one is more articulate than the other? Can anyone deny the importance of these things?

How many people, not particularly eager to have a non-exclusive relationship, have accepted one just to match the desires of the other? But is this acceptance really freely chosen? For if John is in love with Jeanne and in a weak position, and Jeanne explains her desire for a non-monogamous relationship, John will accept. And Jeanne will have the impression that everything is simple and easy, without wondering if John would not have equally agreed to the opposite.

Is this weak yes so different from the "yes" that we give to the boss at work?

We affirm that it is the same, and that talk of freedom in such cases perpetuates what Nietzsche called "the sublime lie that interprets weakness as freedom" [8].

Ideas of sexual liberation are beautiful and noble ideas, but each of us, by passing them in the crucible of our own individuality and in the recognition of the uniqueness of the other, give them different forms. As we said earlier, we affirm that there is no single rule that can govern human relationships, for the same reasons that we oppose Law, because it can never take into account the complexity of the individuals it puts under its control [9]. This is also why we counter unlearned and undigested ideas from ideological brochures with an individual and visceral ethics. We also affirm that the only relatively emancipated relationship is the one with the welfare of each other as the center of its attention, free from self-absorption and free from the traps and imperatives of ideology. Why wouldn’t the only valid rule of love be to pay attention to the other, to treat one’s companion properly, as an individual, rather than foolishly applying rules intended to make ourselves free through personal enjoyment, but without any sensitivity to otherness? And why make the analytical error of confining criticism of the economy to the formal economy, rather than to flesh it out in the social relations that govern our alienated relationships?

In order to break the socially expected obligations of coupledom we choose ideological polyamory and manufacture a different norm that will last until new human dramas emerge. And it is no coincidence that the events of May 68, beyond the incredible experiences of occupation and destruction of factories and universities, the clashes and barricades and the generally wonderful experience of having touched the possibility of a real subversion of the existent, it is no coincidence that beyond the Image d’Epinal [10] hide many human tragedies; suicides, overdoses, betrayals and infinite sadness. It is no coincidence that behind every experience of widespread emancipation (or at least experienced as such by its protagonists) hide equally widespread human dramas, from May 68 to Woodstock, from sexual liberation to the Maoists and radical student movements in the United States of 1960/70. No wonder too that so many have bounced back on their feet, now forming the ruling classes of this order, while many others who took the ideas at their word find themselves languishing in jail in oblivion for over forty years, paying for not being inconsequential like the others, for not having merely sought pleasure and immediate gratification.

Those who were there merely to have fun, to flutter and navel gaze, have profited. Those who believed and still believe in revolution have paid the price. Profit for one group always implies the exploitation of another, be it with the arms of capital and labor or with those of ideology, whether autonomous or of the party.

While the butterflies forage, may the flowers revolt.

August 2013,

Aviv Etrebilal.



[i] La Belle Epoque is a period of Western European history. It is conventionally dated from the end of the Franco-Prussian War in 1871 to the outbreak of WW1 around 1914. It was a period characterized by optimism, regional peace, economic prosperity and scientific and cultural innovations. In this climate the arts flourished, especially in Paris. The Belle Époque was named, in retrospect, when it began to be considered a Golden Age in contrast to the horrors of World War I. ; translator’s note.
[1] It literally means outsider. Some individualists who were prone to separation used to refer to themselves in this way. During the Belle Epoque, the individualist movement could be roughly cut in two. One, the ‘educationists’, advocated for communities, pacifism, lifestyle anarchism, social experimentations, etc., and another tendency known as ‘illegalist’, the most famous of which were The Bonnot Gang. Other illegalists are Albert Libertad, Zo d’Axa and Renzo Novatore. Both tendencies referred to themselves in this way, but it had a much different meaning for each ; Tn.
[2] See for instance the followers of Fourrier, the utopians, etc.
[3] From the Kibbutzim, the post ’68 semi-rural communities to the pseudo-commune of Tarnac, etc.
[4] Cf. Contre l’amour (against love), Iosk Editions, August 2003, available at infokiosques.net.
[5] Not unlike the pamphlets circulated by the reforming church during the 1950s in the US.
[6] In an essay published in the journal Le Révolté in 1887. But let’s also remember that 7 years earlier in the same journal, he called for “permanent rebolt by word, by text, by the fist, by the gun, by dynamite”
[7] In French: “Jouir sans entraves” a famous May 68 slogan ; Tn.
[8] Friedrich Nietzsche, The Genealogy of Morals, 1887.
[9] On top of which of course, it will always belong to Power and its maintenance.
[10] The expression Image d’Epinal has become proverbial in French and refers to an emphatically traditionalist and naïve depiction of something, showing only its good aspects. Tn.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

