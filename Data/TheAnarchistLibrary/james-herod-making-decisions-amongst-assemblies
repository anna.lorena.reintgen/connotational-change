      Decision Making amongst Assemblies      Federation and Other Delusions      A Note on the Distinction between a Federation and a Network
Or, Establishing Direct Democracy Across Territories

Also, Clarifying the Distinction between

Networks and Federations

Revised slightly January 2008
And slightly revised again in October 2010

(There are really only two ways that are compatible with direct democracy.)

The key problem in building a horizontal, nonhierarchical social order based on participatory, direct democracy has never been how to do this on the local level in one town meeting, workers council, or village assembly, but how to make decisions across such assemblies. Direct democracy is frequently dismissed precisely because people believe that it cannot work in larger territories. I don't believe this is true, but it is true that it has never yet been done, as far as I know. And this is the challenge we face. Decision-making procedures are thus not secondary matters. Not at all. They are absolutely central, a core issue. Until we solve the problem of directly democratic decision making across territories we cannot establish a free society. It's as stark as that.

What are our options? First, let's review how it is done presently in the capitalist so-called representative democracies. Simple. Individual votes are tallied across the state to elect representatives who then make the decisions. Most everyone realizes now, of course, that this is a sham, and has nothing to do with real democracy. It is a device ruling classes have used to stay in power. The rich are able, by and large, to control who runs for office and what they do after taking office. Besides, the ruling class is making all the big decisions behind the scenes, in order to perpetuate capitalism. Even parliaments controlled for years by a majority of socialists have not been able to unseat capitalist ruling classes.

But what if there were no ruling class, but just our popular assemblies in our neighborhoods and workplaces? What then? Well, we could continue using the same procedure that bourgeois democracies did and elect representatives by tallying individual votes across the land. Marx and Engels believed that if there were no ruling class then there would be no problem with representatives, because they would not be serving the interests of the ruling class but those of the working class. A century and a half later we now know that this is not the case. A decision-making elite very rapidly turns into a bureaucracy which in turn rapidly turns into a new ruling class.

There is another way to select representatives, however (other than picking them through general elections). Each assembly could choose a representative (and the custom among radicals has been to call them delegates, and more recently, spokespersons) and send these delegates to regional assemblies to make decisions. But here is where we start running into confusion, ambiguity, and disagreement.

Traditionally, anarchists have justified relinquishing their decision-making power to delegates with the concepts of "mandate" and "recall." I examined these notions in my book, Getting Free. I think I'll just reprint the relevant paragraphs here.

Anarchists have long deluded themselves with the idea of federation that they have solved the thorny problem of how they can have both direct democracy and large-scale organization at the same time. It's a pat formula that they ritualistically repeat -- "federated at the municipal, regional, national, and international levels." It's a grand illusion. Federation creates a hierarchy by using delegates (i.e., representatives) to form smaller and smaller decision-making units, further and further removed from the neighborhood. But to make it more palatable, this idea is garnished with three other illusions: mandated delegates, instant recall, and the separation of policymaking from administration. I believe all three ideas are flawed and are incompatible with direct democracy, and hence with anarchism, self-government, and autonomy.

The notion of a mandated delegate is a mirage because as soon as a meeting convenes, everything is open. The discussion of the issues redefines those issues. Sometimes, the change of only one word in a proposal can completely alter the proposal's meaning and impact. There is no way delegates can avoid exercising their own judgment on the issues once the discussion gets under way, no matter how detailed their instructions ahead of time. So the idea that mandated delegates preserve the decision-making power of the neighborhood assemblies is an illusion. In short, I do not believe that delegates can be mandated.

The idea of instant recall is also an illusion. For recall to work, the people back home would have to be following the discussion as closely as if they were there themselves. They would have to have detailed, current knowledge of the issues as they were unfolding in debates among delegates. Even if everyone back home were watching the conference live on television (an impossibility), in order to exercise recall they would have to convene themselves in their neighborhood assemblies and debate whether or not a delegate had deviated from the mandate far enough to warrant recall. But if they are going to do this, if they have this kind of intimate knowledge of the issues and this kind of communication system, they might as well be making the decisions themselves directly, without bothering to go through the hassle of setting up a conference of delegates. A moment's reflection shows that the whole idea of recall is fallacious, but it has been repeated uncritically for decades by radicals.

Similarly, the idea of the separation between policy making and administration doesn’t make sense. Anyone who has worked on a project knows that all kinds of decisions have to be made constantly. It can be the most mundane decision and yet have profound policy implications. But unless a decision happens to come under scrutiny, and is discussed and aired, it may not be clear what its policy implications are. In other words, it's next to impossible to separate purely administrative decisions from policy decisions because almost any so-called administrative decision may be shown to have policy implications. The distinction is a false one. It is another illusion, a way of convincing ourselves that we still have a project based on direct democracy, when we do not. (pages 94-95)

These objections are valid, I believe, and confirm our need to stick to direct democracy and never relinquish decision-making power to delegates (representatives, spokespersons). Others continue to object, however, saying that this principle (of not relinquishing decision making) should not be elevated like this. They insist that they have the right to hand over this power to delegates if they want to, and their group does want to. They say that they trust their delegates to make the right decisions, ones that the group itself would make, because the delegate knows the group, and will act in its best interests.

First of all, it is simply not true that delegates can always know what their constituents want on every issue. Opinions change. Disagreements emerge, almost inevitably. But more generally, here is my answer to this line of argument (again taken from Getting Free):

The main reason for wanting to avoid delegating decision-making power to representatives is not that people thereby hand over their power to others and create a decision-making elite, although this is bad enough. It is not that they are thereby no longer "autonomous individuals," for there is no such thing. Rather, it is that they bar themselves by this action from participating in the discussion of the issues. They forfeit their natures as thinking persons and instead hand over this function to others. (page 113)

Others suggest that maybe rotation would solve the problem. We could still relinquish decision making to delegates but then rotate the delegates frequently, so no decision-making elite could emerge. It's true that elite formation could be thus blocked. But it still means we would be giving up our decision making power, handing it over to others. Even though it is held only temporarily by any one person, it is still permanently gone from us. Rotation is a good strategy for many kinds of tasks. Any project is divided into tasks and in many cases it is useful to rotate these tasks so that everyone can learn everything, that is, everyone can more or less become competent at the whole operation. General decision making in assemblies is in a different category, however. It is a task of a different kind.

Perhaps a clarification would be useful at this point. Direct democracy does not mean that every decision about everything is taken by everyone. That's clearly impossible. The very idea is absurd. Decisions over huge areas of life, like most projects, particular jobs, or households, for example, will be made by those directly involved. Only decisions of general significance need be made by general assemblies. But the general assemblies will decide what these matters are, and what should concern them and what not. A better distinction therefore than policy/administrative is whether something is a matter that needs to be decided by the whole community or is a matter that can be left to sub-groups. Any social order will have a division of labor, with specialized tasks, where decision making is best left to those who are actually doing the task. The mistake is to think that decision making itself is a specialized task, for which we elect persons with a specialized skill to carry it out (politicians at present, or more generally, delegates, representatives, or spokespersons). This is a grave error, and leads inevitably to a hierarchical society.

Anyway, this is not the place to fully argue the case for direct democracy. (I should at least mention one salient fact, however; we can only defeat capitalists by taking decision making away from them and relocating it in our own directly democratic assemblies.) But for the time being, let me just assume that we are agreed that we want direct democracy. How do we get it? As mentioned above, the problem is how to make decisions amongst groups (assemblies, councils) in a way that is compatible with direct democracy. That is, how can we make decisions across territories without using delegates or electing representatives? This is a problem that has not yet been solved, historically speaking, that is, in actual social practice.

There are two ways to avoid electing representatives altogether.

(1) We could discuss the issues in our assemblies but then tally individual votes across assemblies. This is a big improvement over parliamentary democracy and the election of representatives (i.e., decision-makers) in that in this practice people are voting on the actual issues, and furthermore, are personally involved in face-to-face discussion and debate, with the votes taken right there in the assembly (but tallied across assemblies). This avoids the practice of polling isolated individuals who have not necessarily benefited from such discussion. This procedure is compatible with direct democracy, and might be used in emergencies provided a proposal can be floated which can be voted up or down (and getting such proposals is not as easy as it might seem). An emergency situation would be one in which there is not time enough to go through this normal consensus process. Some definition of emergency will be needed, obviously; otherwise this procedure might tend to be used in situations where it need not be, thus short-circuiting consensus decision making.

The drawbacks though are that the procedure would require that votes be tallied individually and that some principle of majority rule be used (e.g., simple, two-thirds). This option is closed off, moreover, if our assemblies are using so-called consensus decision-making procedures (which requires the modification of proposals to take account of objections), where votes are not tallied by individuals (which of course will most likely be the normal mode of operation).

(2) The second way to avoid electing representatives involves a back and forth negotiating process and might be formulated as follows:

Proposals that involve two or more assemblies will first be discussed and decided upon in the local assembly, using the "consensus" process. Then negotiators from each assembly involved will confer to work out differences and prepare a general agreement, which will then be returned for ratification by local assemblies. This back and forth process will continue until all assemblies involved are satisfied with the decision.

A key concept here is ratification. Decision making is kept firmly on the local level. Any decisions about matters outside the local area, say on a regional level, must be based on ratification by the local assemblies, and this can only be accomplished through a process of negotiation. There is no other way without reverting to simple majority rule and the tallying of votes by individuals (as in procedure #1 above), thus voiding the consensus process. This may take time, but what's the rush? We must simply recognize that this is how it's done. This is the time we need and the steps we need to go through to make directly democratic decisions.

But there will undoubtedly be those who get impatient with this process and insist that we "get on with the job." What job is it that we are getting on with? There were people like this in the New Left who got frustrated and impatient with all the time we were devoting to getting egalitarian relations within our own projects and groups. They wanted to "get on with the job" (of making the revolution I presume). That is why the New Left gave birth to the idea of prefigurative politics, wherein our current practices, procedures, and behaviors must resemble those that we will want in the future society we are trying to bring into being.

Similarly with decision making. We need to invent now the practices we will need later in a world full of democratic autonomous neighborhoods. If we cannot learn now to make decisions across groups (assemblies, territories) based on direct democracy, we've got nothing, absolutely nothing, and may as well forget about establishing a self-managed society. This is a hurdle we must get over before we can create a free society.

In light of the above discussion it might be useful to consider the recent experiment with popular assemblies in Oaxaca, Mexico. Oaxacans established a Popular Assembly of the Peoples of Oaxaca in Oaxaca City. They also established forty other popular assemblies in smaller towns scattered around the state. I don't know much about the forty local town assemblies (but as far as I do know they made no attempt to establish a network using the direct democratic decision-making procedure outlined above). I do not read Spanish. But I have followed the English-language reports quite closely and so have some knowledge of the Popular Assembly in Oaxaca City. It was composed of delegates (roughly 180 of them, but this number undoubtedly varied from meeting to meeting). The delegates were sent by labor unions, the teacher's union (section 22), various NGOs, civic organizations, the popular assemblies from smaller towns, various neighborhoods, town councils, scattered individuals, and so forth.

This assembly therefore was a sort of modified parliamentary democracy. It was not based on direct democracy. It's true that this way of selecting delegates is far superior to selecting them through general, state-wide elections (which the rich can easily control). It's also true that this was a way of bypassing the existing power structure based on the established political parties. Yet many, perhaps most, of the organizations which sent delegates to the Popular Assembly are not themselves organized internally on the basis of direct democracy, but are rather traditional hierarchical organizations. So basically, they were sending "leaders" to the popular assembly. But at least these delegates were "leaders" of their local groups, and not leaders imposed from above (except perhaps in some of the national unions). This Popular Assembly was definitely set up though to be a state-wide decision-making body. And indeed, the assembly before too long started passing resolutions which were binding on the entire state. So it took on the functions of a legislative body. Also, the Popular Assembly used traditional majority rule, just as a matter of course, with votes being counted individually. In addition, there was only the one Popular Assembly in the whole city of Oaxaca, which has a population of 250,000 (out of 3.3 million state-wide). But it wasn't meant to be an assembly for Oaxaca City itself. Its delegates came from all over the state.

There was at least one element of direct democracy present that I know of. Section 22 of the teacher's union practiced what they called "consulting with the base." Decisions taken by the Popular Assembly had to be ratified by the entire membership of the teacher's union. I don't know if other organizations represented in the assembly used this procedure. My impression is that they didn't. Also, I don't know how often the delegates from Section 22 had to consult, or over what types of decisions. Was it only for the really big decisions, or more or less for everything? My impression is that it was only for the really big decisions, like whether to end the strike or not.

For comparative purposes, we might take a quick look at the neighborhood council system in El Alto, Bolivia. This is one of the most advanced cities on earth in terms of local control and grassroots organization, with elements of direct democracy. This city of 800,000 is organized into 600 neighborhood councils. The councils basically run the city. The average membership in a council is 200, but these two hundred participants usually represent the entire neighborhood. They come from heads of families, unions, trade organizations, and so forth. So these neighborhood councils are not assemblies of the entire neighborhood (which would mean meetings of roughly 1300 people). They are assemblies of delegates from the neighborhood. Nevertheless, this is a powerful grassroots structure. For city-wide decisions El Alto also relies on delegates, on a central council composed of delegates selected from neighborhood councils. So on the city level, they have reverted to federation and hierarchy, to a modified representative government.

In his new book, Consensus (2006, See Sharp Press), Peter Gelderloos has this to say about federations.

"Federations contain two tiers of structure: local and central. The local structure consists of a number of autonomous groups, each working within their immediate communities, that have decided for various reasons to band together. The central structure manifests in periodic meetings attended by people from each local group within the structure. At these central meetings, people decide matters concerning the entire federation, and create strategies of action that each locality can participate in, for a broader impact. The federation should never dictate to a locality, but should recognize the autonomy and self-directed community work of each local group as the source of its strength."

The contradiction here is glaring. If the central meeting decides "matters concerning the entire federation," how is this consistent with the claim that "the federation should never dictate to a locality." It's not. If decisions taken at the central meetings are not binding on local groups there is no point in making them. If the local groups are truly autonomous then why pretend that a regional assembly of delegates has any power over them? In practice, of course, and here is where I think Gelderloos has misrepresented the situation, regional assemblies do assume that they are making decisions that are binding on the entire federation. Moreover, local groups send delegates to these regional meetings also with the understanding that that body can make decisions that are binding on all groups in the federation. That's the whole point of regional assemblies, under federation. And, to my mind, this is the thing that distinguishes a federation from a network. Federationists have agreed to relinquish their decision making powers to delegates who then go to regional assemblies to make decisions for the whole federation. Networkists refuse to relinquish decision making power to delegates, but instead practice direct democratic decision making across groups (assemblies, territories), as outlined above.

Other criteria are sometimes put forward to distinguish networks from federations. Gelderloos mentions one of them, namely, "well-defined principles of unity." So it is often said that federations have much more specific statements of purpose, or platforms, than networks. In federations, it is said, members are more or less on the same page of a more concretely defined politics. They are more homogenous, politically. Networks are thought to be more heterogeneous, with vaguer statements of principles. This may have been true so far historically, but I don't think it need be true. I see no reason why a network couldn't hammer out just as specific a statement of political principles as a federation. Why not? It would take some work. But so do such statements when generated by federations. Federations, by having (starting out with) such a detailed statement of beliefs, simply exclude those who disagree with it. That's why they are more homogenous politically. A network could do the same thing. People who don't agree with the mapped out politics simply won't become a part of the network. Conversely, a federation could be just as loosely defined and vague as most networks are currently thought to be.

A third distinction between networks and federations that is sometimes mentioned is that federations have a more bureaucratic structure, with secretaries, treasurers, various other officers, and the like. Again, there is nothing to stop a network from also setting up such a division of labor.

No, I think the key distinction is whether all decision making is kept on the local level or whether it is sometimes handed over to delegates at regional assemblies. This is the distinction between direct democracy and some form of representative government. If we want to be free, we will need to insist on and fight for direct democracy, and to reject, forever, representative government.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

