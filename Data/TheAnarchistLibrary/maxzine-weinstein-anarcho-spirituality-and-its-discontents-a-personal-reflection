
“What is inflated too much will burst into fragments.”
— Ethiopian proverb

“Spiritual zombies no longer hear their inner guide.” 
— Alice Walker

In 1986, at the Haymarket anniversary anarchist gathering in Chicago, I landed in a “radical ritual.”

We were told that we would start calling in the directions. They get to West and call in the spirits of water. We are just blocks from Lake Michigan. This body of water has nothing to do with the West because it sits to the East! I point this out and am shushed with comments about “tradition” and “how things are done”. That moment helps define me as an anarcho-disillusionist, brought on by the anarcho-superstitionists who wanted me to accept some important tradition. Years later in Tennessee, I attended rituals as part of other events to be supportive of the larger community of anarcho-freaks. At summer solstice 2002, again the directions are invoked. Again, West is called in as the spirit of water. I think about how badly we need rain — and when it comes, it will be drawn from the South. I stay silent in my objections, but cynically think how out of touch with reality the pagan religion is.

I know goddess worshippers call their paganism “spirituality” as distinguished from “religion.” I find it hard to tell the difference. Both are caught up in gender duality: at the anarcho-summer solstice we are invited to paint our bodies blue for female energy and orange for male energy. Also, there is a pagan obsession with the notion of “mother earth” and “father sky”. Wouldn’t a liberating view of gender go beyond the simplistic imposition onto celestial bodies of traditional male/female roles?

Spirituality and religion are both married to order: we are instructed to enter the circle and walk clockwise, and anyone who accidentally or purposefully walks a different direction is coerced (in a sweet, hippie way, of course) into walking in the correct direction.

At the 1989 Anarchist gathering in San Fransisco, I went to a workshop facilitated by Starhawk, author and anarcho-goddess, curious to see how radical rabble rousers interact with this star. Perched in the center, Starhawk invited people to share their thoughts and ideas. Person after person stood and spoke about how they felt spiritually deprived until they read Starhawk and found the goddess. Starhawk would smile and accept the accolades. I could not sit silent. I stood up and said that I felt trapped at a church gathering of born-again testimonials, not an anarchist gathering. Some people looked perplexed while others agreed with my sentiment.

Starhawk essentially said that she agreed this workshop was not about her but about all of us. Then the workshop continued as it had before, with the high priestess basking in the glory. I left at the break before the spiral dance. I had done one of those earlier that year at the Nevada Nuclear Weapons Test Sity with he honor, and it had failed to stop the nuclear threat.

Spirituality is harmless fun, or is it? I believe there are greater forces out there than ourselves. I believe things sometimes happen in amazing, mysterious ways. But what scares me is the pull of religion, with its order, rituals, leaders, and peer pressure, which taken together seduces radical anarchists and creates an environment of repackaged religion under the guise of some supposedly liberating spirituality.

In July 2002, I stood in our vast garden. For weeks, rain came so close, but then just passed by. Thunder and lightening taunted, but failed to soak the soil. So I moved some hoses and turned on the nozzle again. The water is not the same as rain. I have read studies that claim that rainwater is up to four times more effective for plants as water from a hose. The scientists have not been able to explain this, but I feel it and I smell it. Finally, the soaking rains came and we celebrated. And the plants grew at a faster rate than they had from irrigation. Though I have no idea why, I am a true believer. There is a force out there greater than us that we cannot understand. I don’t need a religion to explain it.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“What is inflated too much will burst into fragments.”
— Ethiopian proverb


“Spiritual zombies no longer hear their inner guide.” 
— Alice Walker

