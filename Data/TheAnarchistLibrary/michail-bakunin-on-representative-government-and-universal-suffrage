      Introduction      On Representative Government and Universal Suffrage
Bakunin opposed workers’ participation in bourgeois politics because he feared that participation would corrode the proletariat and perpetuate the establishment. His opposition to parliamentary government was sharpened during his polemics with the Marxist parties, who favored parliamentary action by the workers. Bakunin opposed universal suffrage insofar as it reinforced the bourgeois democratic state, but he never raised abstention from the electoral process to an inflexible article of faith. Under certain exceptional circumstances, he advocated temporary alliance with progressive political parties for specific, limited objectives. In a letter to his friend the Italian anarchist Carlo Gambuzzi, a former lawyer, Bakunin advised him to become a candidate for Deputy from Naples:

You will perhaps be surprised that I, a determined and passionate abstentionist from politics, should now advise my friends [members of the Alliance] to become deputies — this is because circumstances have changed. First, all my friends, and most assuredly yourself, are so inspired by our ideas, our principles, that there is no danger that you will forget, deform, or abandon them, or that you will fall back into the old political habits. Second, times have become so grave, the danger menacing the liberty of all countries so formidable, that all men of goodwill must step into the breach, and especially our friends, who must be in a position to exercise the greatest possible influence on events ...

In a letter to another Italian anarchist, Celso Cerretti, written during the reaction that occurred in all of Europe after the fall of the Paris Commune in 1871, Bakunin noted that Spain was the only country where a revolutionary situation existed and in view of the special circumstances prevailing in that country advised temporary collaboration with the progressive political parties:

Letters that I receive from different parts of Spain indicate that the socialist workers are very effectively organized. And not only the workers but the peasants of Andalusia, among whom socialist ideas [have fortunately] been successfully spread — these peasants too are prepared to take a very active part in the coming revolution. While maintaining our identity, we must, at this time, help the political parties and endeavor later to give this revolution a clearly socialist character... . If the Revolution triumphs in Spain, it will naturally tremendously accelerate and spread the Revolution in all of Europe...

Modern society is so convinced of this truth: every state, whatever its origin or form, must necessarily lead to despotism, that countries which have in our time wrested a measure of freedom from the State have hastened to subject their rulers, even when these rulers emerged from revolution and were elected by all the people, to the strictest possible control. To safeguard their freedom, they depend on the real and effective control exercised by the popular will over those invested with public and repressive authority. In all nations living under representative government freedom can be real only when this control is real and effective. It follows, therefore, that if such control is fictitious, then the freedom of the people becomes likewise a complete fiction.

It would be easy to prove that nowhere in Europe is there real popular control of government, but we shall confine ourselves to Switzerland and see how popular control over the Swiss government is exercised. For what is true in this respect for Switzerland must hold even more for any other country. Around 1830, the most progressive cantons in Switzerland tried to safeguard their liberties by instituting universal suffrage. There were solid grounds for this movement. As long as our legislative councils were chosen by privileged citizens, and unequal voting rights between cities and rural areas, between patricians and plebeians, continued to exist, the officials appointed by these councils as well as the laws enacted by them could not have failed to perpetuate the domination of the ruling aristocracy over the nation. It therefore became necessary to abolish this regime and replace it by one honoring the sovereignty of the people, i.e., universal suffrage.

It was generally expected that once universal suffrage was established, the political liberty of the people would be assured. This turned out to be a great illusion. In practice, universal suffrage led to the collapse, or at least the flagrant demoralization, of the Radical party, which is so glaringly obvious today. The radicals [liberals] did not intend to cheat the people, but they. did cheat themselves. They were quite sincere when they promised to provide popular freedom by means of universal suffrage. Fired by this conviction, they were able to stir up the masses to overthrow the entrenched aristocratic government. Today, demoralized by the exercise of power, they have lost their faith in themselves and in their ideals; this explains the depth of their depression and the profundity of their corruption.

And, indeed, at first glance the idea of universal suffrage seemed so reasonable and so simple; once the legislative and executive powers emanate directly from popular elections, would not these powers faithfully reflect the will of the people? And how could this popular will fail to produce anything other than freedom and general well-being?

The whole system of representative government is .in immense fraud resting on this fiction. that the executive and legislative bodies elected by universal suffrage of the people must or even can possibly represent the will of the people. The people instinctively reach out for two things: the greatest possible prosperity coupled with the greatest possible freedom to live their own lives, to choose, to act. They want the best organization of their economic interests coupled with the complete absence of all political power and all political organization, since every political organization must inescapably nullify the freedom of the people. Such is the dynamic aspiration of all popular movements.

But the ambitions of those who govern those who formulate and enforce the laws. are diametrically opposed to the popular aspirations. Irrespective of their democratic sentiments or intentions, the rulers by virtue of their elevated position look down upon society as a sovereign regarding his subjects. But there can be no equality between the sovereign and the subject. On one side there is the feeling of superiority necessarily induced by a high position; on the other, that of inferiority resulting from the sovereign’s superior position as the wielder of executive and legislative power. Political power means domination And where there is domination. there must be a substantial part of the population who remain subjected to the domination of their rulers: and subjects will naturally hate their rulers. who will then naturally be forced to subdue the people by even more oppressive measures, further curtailing their freedom Such is the nature of political power ever since its origin in human society. This also explains why and how men who were the reddest democrats, the most vociferous radicals, once in power become the most moderate conservatives. Such turnabouts are usually and mistakenly regarded as a kind of treason. Their principal cause is the inevitable change of position and perspective. We should never forget that the institutional positions and their attendant privileges are far more powerful motivating forces than mere individual hatred or ill will. If a government composed exclusively of workers were elected tomorrow by universal suffrage, these same workers, who are today the most dedicated democrats and socialists, would tomorrow become the most determined aristocrats, open or secret worshippers of the principle of authority, exploiters and oppressors.

In Switzerland, as in all other nations, however egalitarian its political institutions may be, it is the bourgeoisie who rule and it is the working masses, including the peasants, who must obey the laws made by the bourgeoisie. The people have neither the time nor the requisite knowledge to participate in governmental functions. The bourgeoisie possess both; hence, not by right, but in fact, they hold the exclusive privilege of governing. Political equality in Switzerland, as in all other countries, is therefore a puerile fiction, an absolute fraud.

Now, since the bourgeoisie by virtue of their economic and political privileges are so far removed from the people, how can their governing and their laws truly express the feelings, ideas, and will of the people? It is impossible, and daily experience demonstrates that in the legislative and all other branches of government, the bourgeoisie is primarily concerned with promoting its own interests and not the legitimate interests of the people. True, all district officials and legislators are directly or indirectly elected by, the people. True, on election day even the proudest bourgeois office seekers are forced to court their majesty, The Sovereign People. They come to the sovereign populace, hat in hand, professing no other wish than to serve them. For the office seeker this is an unpleasant chore, soon over and therefore to be patiently endured. The day after election everybody goes about his business, the people go back to toil anew the bourgeoisie to reaping profits and to political conniving. — They seldom meet and never greet each other till the next election when the farce is repeated... Since popular control in the representative system is the sole guarantee of popular freedom, it is obvious that this freedom too is wholly spurious.

To correct the obvious defects of this system, the radical democrats of the Zurich Canton introduced the referendum, direct legislation by the people. The referendum is also an ineffective remedy; another fraud. In order to vote intelligently on proposals made by legislators or measures advanced by interested groups, the people must have the time and the necessary, knowledge to study these measures thoroughly... . The referendum is meaningful only on those rare occasions when the proposed legislation vitally affects and arouses all the people, and the issues involved are clearly understood by everyone. But almost all the proposed laws are so specialized, so intricate, that only political experts can grasp how they would ultimately affect the people. The people, of course, do not even begin to understand or pay attention to the proposed laws and vote for them blindly when urged to do so by their favorite orators.

Even when the representative system is improved by referendum, there is still no popular control, and real liberty — under representative government masquerading as self-government — is an illusion. Due to their economic hardships, the people are ignorant and indifferent and are aware only of things closely affecting them. They understand and know how to conduct their daily affairs. Away from their familiar concerns they become confused, uncertain, and politically baffled. They have a healthy, practical common sense when it comes to communal affairs. They are fairly well informed and know how to select from their midst the most capable officials. Under such circumstances, effective control is quite possible, because the public business is conducted under the watchful eyes of the citizens and vitally and directly concerns their daily lives. This is why municipal elections always best reflect the real attitude and will of the people. [1] Provincial and county governments, even when the latter are directly elected, are already less representative of the people. Most of the time, the people are not acquainted with the relevant political, juridical, and administrative measures; those are beyond their immediate concern and almost always escape their control. The men in charge of local and regional governments live in a different environment, far removed from the people, who know very little about them. They do not know these leaders’ characters personally. and judge them only by their public speeches, which are packed with lies to trick the people into supporting them... If popular control over regional and local affairs is exceedingly difficult, then popular control over the federal or national government is altogether impossible.

Most of the public affairs and laws, especially those dealing with the well-being and material interests of the local communities and associations are settled in ways beyond the grasp of the people, without their knowledge or concern, and without their intervention. The people are committed to ruinous policies, all without noticing. They have neither the experience nor the time to study all these laws and so they leave everything to their elected representatives. These naturally promote the interests of their class rather than the prosperity of the people, and their greatest talent is to sugarcoat their bitter measures, to render them more palatable to the populace. Representative government is a system of hypocrisy and perpetual falsehood. Its success rests on the stupidity of the people and the corruption of the public mind.

Does this mean that we, the revolutionary socialists, do not want universal suffrage — that we prefer limited suffrage, or a single despot? Not at all. What we maintain is that universal suffrage, considered in itself and applied in a society based on economic and social inequality, will be nothing but a swindle and snare for the people; nothing but an odious lie of the bourgeois-democrats, the surest way to consolidate under the mantle of liberalism and justice the permanent domination of the people by the owning classes, to the detriment of popular liberty. We deny that universal suffrage could be used by the people for the conquest of economic and social equality. It must always and necessarily be an instrument hostile to the people, on which supports the de facto dictatorship of the bourgeoisie.

 
[1] It can be gathered from the context that Bakunin, without explicitly saying so, refers not to great cities with hundreds of thousands or millions of inhabitants but to small or medium-sized communities where face-to-face democracy is practical.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



You will perhaps be surprised that I, a determined and passionate abstentionist from politics, should now advise my friends [members of the Alliance] to become deputies — this is because circumstances have changed. First, all my friends, and most assuredly yourself, are so inspired by our ideas, our principles, that there is no danger that you will forget, deform, or abandon them, or that you will fall back into the old political habits. Second, times have become so grave, the danger menacing the liberty of all countries so formidable, that all men of goodwill must step into the breach, and especially our friends, who must be in a position to exercise the greatest possible influence on events ...



Letters that I receive from different parts of Spain indicate that the socialist workers are very effectively organized. And not only the workers but the peasants of Andalusia, among whom socialist ideas [have fortunately] been successfully spread — these peasants too are prepared to take a very active part in the coming revolution. While maintaining our identity, we must, at this time, help the political parties and endeavor later to give this revolution a clearly socialist character... . If the Revolution triumphs in Spain, it will naturally tremendously accelerate and spread the Revolution in all of Europe...

