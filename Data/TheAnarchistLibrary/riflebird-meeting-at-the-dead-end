      FROM ENSLAVEMENT TO OBLITERATION      COMMON THREADS      KNOW-IT-ALLS AND NO-HOPERS      THE FAILURES OF PRIMITIVISM      MEETING AT THE DEAD END
We are not autonomous, we are everywhere and everyone. We are looking to set an invisible trend that is already here, that abandons the shackles of subculture, identity and ideology, and finds comfort in the revolutionary discomfort we all feel. The suicidal are in control, destroying the land that feeds us, mediating our relationships with each other and all life on this planet, and establishing a global reality that efficiently forces all life to survival as opposed to living. There is unity in our cynicism, skepticism, and common contempt. There is unity in our neglected passions, malnourishment, and feared temptations. While there is also a division set in their very existence, there is a unity in these feelings. There are those who share these feelings, and those who look to silence them, deceive them, or murder and imprison those feeling them. ‘Fire to the Prisons’

The conversation regarding nihilism in anarchist circles has been almost impossible to tune out in recent years. This article has come about from my own recent reading, personal experiences, and talking to those that read nihilist-influenced literature. Not many of these folks would identify as a nihilist of course, because they usually have a strong aversion to labelling themselves and are working toward ‘a negation of political identities’. There are innumerable articles, books, and lengthy theses on nihilism, published around the world. I don’t profess to know about even a fraction of them, I am simply trying to scratch the surface.

The nihilistic literature I have come across can be deep and convoluted, often deliberately contradicting itself. The level of theory makes some articles dense and nearly impenetrable at times, alienating those that don’t appreciate the philosophical tone and the now-generic writing style. Some of the articles I attempted to read just did not hold my attention, even if they were designed as a preliminary reading. Some were overly poetic, contrived or just simply resigned and pessimistic. In other cases, however, I was totally on board and felt like I could relate to the sentiment.

Throughout this piece I will mostly refer to green anarchy and green anarchist theory but also will touch on (anarcho)primitivism which I see as closely related and a necessary inclusion in the topic. As it did with basic anarchist theory and green anarchist ideas, it would take a few years of contemplation to really familiarise myself with the vast array of nihilist-influenced material that’s out there. Oftentimes too, I think it is unclear where nihilist influences end and insurrectionary anarchist ideas begin, or vice versa. I am merely dealing with the material I have read and found relevant to my own exploration. A preoccupation with internalising theory and regurgitating ideas, at the expense of dialogue and experimentation, is not something worth striving for anyway.

Much green anarchist writing resonates with me, and nihilist tracts and journals may speak deeply to somebody else- it’s all personal and subjective. I have felt from my interactions with nihilists a definite sense of kinship and trust, and I wanted to uncover why this is so. Part of my curiosity is that within nihilism there is often an expectation of a much sharper and deeper critique, which I have felt challenged and confronted by. I see this as a positive. Another pattern I have noticed is the willingness to go further in both theory and action.

“The current nihilism amongst the youth is not arising from nothing. It is a reflection of the total failure of both resistance and capitalism. Many see no alternative and want nothing else other than the complete destruction of the beast that feeds them: the city.”

Uncontrollable : Contributions to a Conscious Nihilism.

As aforementioned, I have tried to find commonalities with nihilist thought and green anarchist viewpoints because I do sympathise with both. I came to green anarchist beliefs the long way around, starting from a destructive and nihilistic streak that showed up earlier in my life. I was originally guided almost solely by boredom, depression, and frustration, then inspired by crappy punk and hardcore music, situationist ideas, art, and existential philosophy via Crimethinc, I’m not ashamed to admit. This led to a rejection and abandonment of the values of mass society, far before I had any serious interest in the natural world, environmentalism, or anthropology.

By this point I believed I should question everything, and attempted to start this process, finding many smokescreens and lies that had clouded my vision. During this process I developed a deep distrust of society and authority in general terms, way before extrapolating this out to the entire phenomenon of civilisation. This is contrary to many other green anarchists I have since met; many had a direct experience with some form of remote, wild place early on, which shapes their anti-civ perspective. I realised that I was against civilisation, but at the time was living in an urban environment with almost no connection to my bioregion, no comprehension of the annihilation of the ecosphere, and no understanding of life outside the industrialised bio-dome.

Like many friends I saw little meaning in anything and wanted revenge on society. This manifested in varying small-scale, non-threatening ways, such as petty larceny and vandalism. At the time there was a generalised refusal of what was ‘on offer'; work, careers, shopping, morality and the spectacle. It was not until the literature of Derrick Jensen, Chellis Glendinning, Ward Churchill and Jerry Mander came my way that I specifically critiqued civilisation. These are lesser discussed nowadays by myself not because they say nothing of worth but are not anarchist, and they don’t delve quite as deep as I would like to go.

By interrupting the apparent consensus and social peace, confrontations make injustice visible and legitimize the rage others feel as well. When the fog of apparently universal submission is dispelled, those who wish to fight can finally find each other—and readiness to fight is a better basis for allegiance than merely ideological agreement.

‘Say you want an Insurrection’

The similarities of green anarchist thought and nihilism start where they discuss ‘civilisation’ as a specific enemy and target of attack. This belief is non-existent in workerist and leftist thinking. I also have noticed that domestication is named as an enemy in several (what I would describe as) nihilist influenced publications and communiqués, and the term is discussed extensively within the pages of magazines such as 325 and Baedan. Domestication is not usually referred to or recognised as a part of the problem (these days). It has been ‘off the table’ in most discussions and accepted as inevitable. Alongside green anarchists, nihilists appear to have it in their sights, along with all the other techniques of control and domination that mass society imposes.

A conscious level of self-reflection appears to be key and common to both green anarchy and nihilism, at least in theory if not always in reality. By remaining critical of all social institutions both seek to tear down internalised structures of morality, repression and leaving behind the guilt-driven ineffectual activist mentality that accompanies and characterises so much of broader anarchism. This extends to vehement criticism of politics in general, embracing and referring to a stance of antipolitics, sustaining a critique of the left and traditional ideas of revolution. This is a step in the right direction in my eyes. It should be obvious, but by encouraging critique I am not referring to ripping other peoples’ efforts to shreds, meanwhile contributing nothing useful to the conversation. Nonetheless, this phenomenon seems as widespread as it is infuriating in anarchist ‘communities’ and literature.

The schism seems to begin where green anarchists will outline what they are fighting for and oftentimes nihilists will not. Nihilism deeply opposes any blueprint and seem to favour attack, sabotage, and rupture for its own sake without a specific outcome in mind. This is probably stemming from the failure of leftist ‘programs’; and therefore an understandable reluctance to carry on in this tradition. Instead, nihilists emphasise the sensation of liberation which comes from a direct confrontation with a target. In this way it is similar to the way green anarchists express a desire for immediacy and, in my opinion, possibly comes from a similar place.

Both green anarchists and nihilist reject activism and organisationalism. There is a focus on the subjective experience in both, and a desire expressed for direct sensory experiences, whether in a forest, ocean or cityscape. An overarching premise common to both nihilism and green anarchy is that one should never wait around or ask permission to be liberated or feel free.

“Some contemporary insurrectionism affects a nihilist posture, proposing in an offhand manner that everything that exists must be destroyed. To indigenous or environmentalist ears, this project of universal destruction can sound suspiciously like the program industrial capitalism is already carrying out.”

Say you want an Insurrection

“Does nihilism mean that pretty much everything must go for a decent life to be possible? If so then I’m a nihilist. It’s safe to say that nihil-ism isn’t literally nothing-ism or one couldn’t be both a nihilist and an anarchist. If it means the politics of desperation or hopelessness, no thanks.”

John Zerzan

As John Zerzan, prominent anarchoprimitivist writer, has pointed out, his problem with nihilists is not what they stand for but what they rule out. I have noticed this too, but would say it is generally relegated to the soul-sucking vortex of the internet where ‘know-it-alls’ and contrarians find their miserable home. However, I have come across plenty of articles and personal examples where nihilists have not ruled out everything, and find joy and  celebration within destruction. Indeed some nihilist-influenced writing and themes I find genuinely intriguing and seductive, inciting the desire to act like few others. The concept of ‘passionate friendship’ (as mentioned by the nihilist/egoist writer Wolfi Landstreicher), and a steadfast commitment to solidarity are concepts that are embraced by many nihilists. These are principles that are certainly more meaningful than whether or not you are in political agreeance all the time. On the other hand, some pieces on nihilism and individualist anarchism emphasise the pitfalls of being attached to anything, so commitment or long term alignment with people or groups can be more difficult, or ephemeral.

My own interpretation is that there is an elitist streak present in some nihilist circles that is irritating. Of course, that claim has been levelled at green anarchists and primitivists plenty of times too. It would be wise to remember and focus on the fact that intellectualism, leftism, and the academy are the enemy and have always drained energy away from any struggle or threat to mass society. That said, in terms of practical, tangible direct action and regular attacks on the infrastructure of civilisation, I am inclined to argue that an awful lot is motivated by a purely nihilistic influence, rather than a belief that such a tactic will ‘bring it all down’. It has to be said that if the nihilists are an observable phenomenon (which they would probably argue against) they have been more inclined than most groups to engage in risky and sustained direct action, predominantly fuelled by anger, hatred, and revenge.

By all means, explode with rage. Refuse to reduce your raw anger to demands or suspend your emotional responses to the tragedies around you. Turn your years of pent-up anguish into a fearsome instrument of revenge. Don’t translate your grievances into the language of your oppressors—let them remain burning embers to be hurled from catapults. Attack, negate, destroy.

 But if it’s rage you’re feeling, why quote philosophy professors?

 Say you want an Insurrection

Coming from a green anarchist, anti-civilisation background, and heavily primitivist-leaning myself, I can say there is a significant section of primitivists that are essentially eco-activists that enjoy being outdoors. There is therefore significant crossover with the realms of green activism, student organising, drum circles, and pacifism, and as a result, often, more militant anarchist folks get frustrated. I have witnessed instances whereby folks advocate to ‘drop out’ of civilisation and not give it any ‘energy’, as a primary mode of resistance. Obviously, this does not go deep enough or address the crisis seriously. It is important to recognise how dire the situation is and what level of resistance would be necessary to disrupt the onslaught of techno-industrialism. An acceptance of practical resistance has usually been a major facet of primitivism but I would say this has been dwindling of late, in its place a deluded idea that knowing traditional skills will miraculously heal the entrenched pathology of civilisation. I disagree. A level of philosophical support and solidarity for attacks on civilisation, at the least, should go with the territory.

This is not the case, perhaps due to the co-option/dilution of terms like rewilding and the ongoing campaign of greenwashing by environmental groups, have had the effect of making primitivist concepts palatable to moderate and fluffy hippy activists. I wish it wasn’t so, but I have to concede that it has been an observable phenomenon at gatherings and primitivist encampments I have attended. Conversations around primitivism seem more common but fighting back against the ever-growing tendrils of civilisation is less frequently discussed. Much of this could be self-censorship, attributable to the green scare and the rise of the surveillance state, so the conversations may take place elsewhere. But in many cases it appears some folks just don’t see the point to fighting back and have given up any hope for personal or collective liberation and action. Others pursue change via the mundane, reformist and futile channels of activism and politics.

It is a fine thing to tell stories, foster community, pursue spirituality or magic, and enjoy the fire and stars, and ‘drop out’ of civilisation so that it does not poison one’s psyche. I would argue that all of this can be helpful. Without the flipside of a generalised antipathy towards mass society and decisive strategic self-defence component though, this can be a frustrating waste of time for those genuinely fed up with civilisation. An over-reliance on positivity, hope and magic is absurd. A degree of anger, resentment, bitterness, and a desire for destructive change is a healthy sign and should be encouraged and supported. Without this balance, a paralysing sense of morality tends to take over, and a regression to milder ‘green/eco’ politics. This soon becomes the default setting; and broader, unauthorised actions are condemned as ‘jeopardising all we have worked for’, and careerist eco-activist politicians hijack any struggle for their own purposes.

The nihilism I am advocating would pit itself against all those who wish to manage the potential of the present, not against the people who are managed. Our enemy is not society, our enemies are the people who maintain and create society.

Uncontrollable: Contributions Toward a Conscious Nihilism

‘The dumb or elite try to pass us off as hoodlums. In some ways they’re right. As we mention we are “for nothing” and in this we look to create a trend that desires to destroy “everything”. We are not a political party, but we are a party; one that celebrates tension, conflict, and attack. Not against each other, but to everything that is everything as we know it.’ Fire to the Prisons

The uncompromisingly militant perspective of many nihilist-influenced articles offers a counterpoint to this current failure of primitivism – it primarily advocates and supports property destruction, direct action, attack and sabotage against the mechanisms of society. On the far end of the spectrum are groups like ITS and Wild Reaction, from their communiqués it is clear they have no qualms about killing folks. Other nihilist- influenced texts seem more measured, and offer messages of friendship, community, and favour attack against the machine to facilitate a move toward something better. This aligns well with green anarchist ideas, which encourage the dismantling of the infrastructure of civilisation to slow the assault on our planet, bodies, and psyches and allow us to heal.

In my personal dealings with those who have a more nihilistic outlook they have shown themselves to be quite reliable, solid friends and have shown consideration of my thoughts regarding green anarchy and primitivism. Much more so than other ‘radical’ friends who jump to the defence of civilisation, and lecture me about activist causes I should be supporting more. In general I have found them to have a stronger and deeper critique of mass society, and a willingness to form bonds rather than fight all day about our differences, particularly as many of them are sick and tired of urban existence and what is on offer. This has been a welcome antidote to the waves of anarcholeftist social justice ‘experts’ who revel in the banality of iphones, popular culture, modern ‘life’ and act as apologists for the techno-nightmare engulfing the planet.

‘While many of us feel the specific analysis of institutions, dynamics and origins of civilisation is a necessary project, as well as the investigation of our true desires and their separation from manufactured ones, nihilism may also be an important element to integrate into our deconstructive process. It is actually a liberatory process to be freed from the restrictions of thinking within the confines of conceiving of another world. That responsibility should be left to individuals and their communities of affinity. It cannot be fully dreamed, let alone realised, until all power is destroyed!’

A Morefus – Nihilism as a healthy influence

If, in the fine words of Klee Benally, it is preferable to be ‘accomplices not allies’, I see a possible and potential relationship with some nihilist-leaning individuals. These folks support the sabotage, destruction, and permanent dismantling of civilisation, which would force civilisation to retreat and wildness to flourish. There may still be a rift between nihilists and green anarchists, and sometimes are goals will not be the same, but oftentimes I think the targets and the enemies will be closely related.

Riflebird




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



We are not autonomous, we are everywhere and everyone. We are looking to set an invisible trend that is already here, that abandons the shackles of subculture, identity and ideology, and finds comfort in the revolutionary discomfort we all feel. The suicidal are in control, destroying the land that feeds us, mediating our relationships with each other and all life on this planet, and establishing a global reality that efficiently forces all life to survival as opposed to living. There is unity in our cynicism, skepticism, and common contempt. There is unity in our neglected passions, malnourishment, and feared temptations. While there is also a division set in their very existence, there is a unity in these feelings. There are those who share these feelings, and those who look to silence them, deceive them, or murder and imprison those feeling them. ‘Fire to the Prisons’



“The current nihilism amongst the youth is not arising from nothing. It is a reflection of the total failure of both resistance and capitalism. Many see no alternative and want nothing else other than the complete destruction of the beast that feeds them: the city.”


Uncontrollable : Contributions to a Conscious Nihilism.



By interrupting the apparent consensus and social peace, confrontations make injustice visible and legitimize the rage others feel as well. When the fog of apparently universal submission is dispelled, those who wish to fight can finally find each other—and readiness to fight is a better basis for allegiance than merely ideological agreement.


‘Say you want an Insurrection’



“Some contemporary insurrectionism affects a nihilist posture, proposing in an offhand manner that everything that exists must be destroyed. To indigenous or environmentalist ears, this project of universal destruction can sound suspiciously like the program industrial capitalism is already carrying out.”


Say you want an Insurrection



“Does nihilism mean that pretty much everything must go for a decent life to be possible? If so then I’m a nihilist. It’s safe to say that nihil-ism isn’t literally nothing-ism or one couldn’t be both a nihilist and an anarchist. If it means the politics of desperation or hopelessness, no thanks.”


John Zerzan



By all means, explode with rage. Refuse to reduce your raw anger to demands or suspend your emotional responses to the tragedies around you. Turn your years of pent-up anguish into a fearsome instrument of revenge. Don’t translate your grievances into the language of your oppressors—let them remain burning embers to be hurled from catapults. Attack, negate, destroy.


 But if it’s rage you’re feeling, why quote philosophy professors?


 Say you want an Insurrection



The nihilism I am advocating would pit itself against all those who wish to manage the potential of the present, not against the people who are managed. Our enemy is not society, our enemies are the people who maintain and create society.


Uncontrollable: Contributions Toward a Conscious Nihilism



‘The dumb or elite try to pass us off as hoodlums. In some ways they’re right. As we mention we are “for nothing” and in this we look to create a trend that desires to destroy “everything”. We are not a political party, but we are a party; one that celebrates tension, conflict, and attack. Not against each other, but to everything that is everything as we know it.’ Fire to the Prisons



‘While many of us feel the specific analysis of institutions, dynamics and origins of civilisation is a necessary project, as well as the investigation of our true desires and their separation from manufactured ones, nihilism may also be an important element to integrate into our deconstructive process. It is actually a liberatory process to be freed from the restrictions of thinking within the confines of conceiving of another world. That responsibility should be left to individuals and their communities of affinity. It cannot be fully dreamed, let alone realised, until all power is destroyed!’


A Morefus – Nihilism as a healthy influence

