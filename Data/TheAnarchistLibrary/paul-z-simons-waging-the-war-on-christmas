
By late 1966 Situationist-inspired theory and praxis began to manifest across Western Europe and North America. In the USA the cultural interventions and publishing done by the Up Against the Wall Motherfucker (UAW/MF) affinity group echoed many themes considered Situationist. The artist, minor Bakunin scholar, and major league trouble maker Ben Morea is just one notable example of the type that was attracted to the Motherfuckers. It should be noted the Morea’s journal (presciently titled Black Mask) contains some of the better North American anarchist critical material from the 60s—a living theory instead of the reliving of history. The Detroit-based White Panthers also stand out; though most contemporary younger theorists haven’t had the chance to interact with their ideas or actions. John Sinclair, one of the founding members of the White Panthers was also involved with the Fifth Estate in the late sixties, which traces a direct line from Situationist inspired ideas to the US post-left, and internationally to Black Anarchy. Which leads us to the UK and two brothers trying their best to drink their way through art school in Newcastle-on-Tyne, David and Stuart Wise. Much as the surrealists were drawn into Marxist and Trotskyite circles, the Wise brothers soon they found their cultural interventions leading them inexorably to ultra-left communist/anarchist ideas and Situationist theory. Their investigation was bolstered by a series of meeting with Morea and Vaneigem on the Lower East Side of NYC. By late 1967 the Wise’s had dropped out of art school, moved to London, found a few like-minded maniacs and founded King Mob. An affinity group dedicated to the project of cultural subversion.

King Mob from the very beginning was a project predicated to action, to the manufacture of situations that illuminated in the harshest light the various predations and weaknesses of nation-state and Capital. Here again, the conundrum of organization resolved, where the coordination of activity defined and circumscribed King Mobs existence; not an ideology and certainly not the wholly reactionary “desire to belong to something greater then one’s self.” In terms of the types of actions King Mob perpetrated, an example from April of 1968. By Winter of 67-68 most of the group had moved to West London and immediately began to interact and work with some local groups. One of the biggest grievances then was the lack of playgrounds and green space for the local children. It had become so bad that several kids had been hit by cars while playing, and in one instance a child had been killed. It wasn’t that there was a dearth of space for the kids to gambol; rather the on-going process of gentrification had enclosed large swaths of open grass with seven-foot tall fences. The local Communist/community activist types held a lame demonstration in an attempt to open up the play areas, to no avail. The bourgeoisie were simply not giving up the means of recreation to the marxist rabble.

Enter King Mob, who after a little planning hired a gorilla suit and a circus horse costume (the kind with a front and back piece—so two bipeds can impersonate a quadruped) and settled into a local pub for a few drinks. After achieving an appropriate level of chemical conviviality one member went into the bathroom, downed some speed, put on the gorilla costume and burst back into the pub, roaring and howling. The signal given the remaining conspirators poured into the street, the circus horse costume was donned and adjusted, and they moved as a group towards a fenced off park, Powis Square, announcing loudly to the neighborhood their intention to tear the fences down and then to play. The growing crowd finally arrived at the objective of the action and set about tearing down the fences, followed almost immediately by the arrival of the police. Violent scuffles ensued and the police arrested the circus horse, the raging amphetamine-addled gorilla and four humans. The circus then moved on to the courtroom where the King Mob agitators showed little desire to back down in the face of the law. As an example, the front end of the horse pleaded guilty while the rear end pleaded not guilty, using the completely reasonable argument that he had no idea that the head of the horse was breaking the law. The courtroom confrontation was written up by a columnist in The New Statesmen and King Mob, much to their amusement, found themselves with a smattering of (almost) positive publicity. Finally, as a result of further community intervention, including the successful tearing down of the fence surrounding the park, Powis Square was officially redesignated a children’s playground, which it remains to this day. But this was all prelude to the Selfridges outrage and the temporary breakdown of law, order, private property and the Christmas Spirit that it occasioned.

King Mob actions continued apace and began to settle more and more into attacks on spectacular consumption and the contradictions of Capital. So when Ian Clegg, one of the fringe members of KM proposed a Christmas invasion of Selfridges, it was taken for what it was, an intervention of pure genius. As an aside, Selfridges & Co. is a chain of high-end department stores located primarily in the United Kingdom. Originally founded in 1908 and with a flagship store located in Oxford Street the chain is roughly analogous to Marshall Fields or Saks Fifth Avenue in the United States in terms of merchandise, clientele, and marketing. In other words, King Mob and Selfridges were made for each other. In preparation for the action a leaflet was produced, outlining a general hatred for Christmas, deriding the hypocrisy of the holiday and encouraging the reader to turn the holiday into a true event of giving. The flyer failed to mention when and where the action was to take place but word of mouth carried the message to the ultra-left radical community of London. So sometime during the month of December 1968, no one is quite sure of the date and the action was completely ignored by the main stream media, members of King Mob, including Ben Trueman dressed as St. Nick and a young Malcolm McLaren, came barging into Selfridge’s. On the way to the toy department, Trueman—already in character-- took a detour to the candy section and started handing out Christmas goodies to any one brave enough to approach him and his entourage. No doubt the store security at this time registered a faint blip on the radar screen that something very wrong was happening near the confectionery aisle. From there, King Mob moved like a wave towards the toy section, and as they arrived Santa and his elves began to strip the shelves of their very expensive toys and hand them out to the wide-eyed and no doubt delighted children. This new take on potlatch lasted some minutes before the inevitable arrival of both store security apparatchiks and the Metropolitan Police. Kris Kringle and a few of his more impassioned elves tangled with the forces of authority and were finally placed under arrest. When the dust cleared the real dilemma was brought into view…what to do about all those unpaid for toys? The management of Selfridges had only two options, to let the children keep the toys and hope for some good publicity, a la Miracle on 34th Street, or to demand that store security and the police take the toys from the children and place them back on the shelves. Ultimately, the final decision was a foregone conclusion, and in an extreme turn of good luck there were no press photographers present as Selfridges security staff and the London Police relieved the kids of their Christmas presents from Santa and returned them to their displays.

There are usually two reactions to this intervention; a) Wow, that was mean, or; b) Wow, that was brilliant. Both are true. Misanthropy and scathing critical theory often co-exist as a result of necessity or resonance (witness Oscar Wilde). The action also reminds us that interventions need not always be political, and in fact historically the terrain of culture has proven to be equally important for the anarchist project.

Finally I offer this as a Christmas gift to my Comrades, in recognition of the fact that the best presents are usually stolen, as I have reappropriated this history and now give it to you.

Merry Christmas Conrads!!




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

