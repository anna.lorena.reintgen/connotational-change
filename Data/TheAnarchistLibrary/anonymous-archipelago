      Affinity and affinity group      Informal organization and projectuality      The “others”      In short
Why come back to questions about affinity and informal organization? Certainly not because we are lacking attempts to explore and deepen these aspects of anarchism, not because yesterday’s discussion, like today’s, aren’t being somewhat inspired by them, and also not because there is a lack of texts – true, most of the time in other languages – that approach these questions perhaps in a more dynamic manner. However, without a doubt, certain concepts require a permanent analytical and critical effort, if they don’t want to loose their meaning by being all-too-often used and repeated. Otherwise our ideas risk becoming a common place, some “evidence”, a fertile ground for the idiotic game of identity competition, where critical reflexion becomes impossible. It also happens that the choice of affinity for some becomes quickly dismissed as if it was about a relationship perched on its own ideas, a relationship that would not allow a contact with reality and neither with comrades. While others wave it around like a banner, like some kind of slogan – and like all slogans, usually it is the real meaning, deep and propulsive, to be its first victim.



No human activity is possible without organization, at least if we understand for “organization” the coordination of the mental and physical efforts deemed necessary to achieve a goal. From this definition we can deduct an important aspect, which is often forgotten: organization is functional, it is directed towards the realization of something, towards action in the broadest sense of the word. Those who today urge everyone to just organize, in the absence of clear goals and while awaiting that from this first moment of organization all the rest would automatically develop, they put on a pedistal the fact of organizing as an end in itself. In the best of cases, maybe they hope that from this will spring a perspective, a perspective that they are not able to imagine by themselves or roughly draw up, but which would become possible and palpable only within some kind of collective and organized environment. Nothing less true. An organization is fruitful when it is nurtured, not from a banal quantitative presence, but from individuals that use itto realize a common goal. Said in other words, it is pointless to believe that, just by organizing ourselves, the questions of how, what, where and why to struggle will be resolved by the magic of the collective. In the best of cases – or the worst, depending on the point of view – perhaps someone could find a bandwagon to jump on, a wagon pulled by someone else, and just get comfortable in the quite unpleasant role of follower.

So it is only a matter of time before one would, disgusted and dissatisfied, break with this organization.

Organization is therefore subordinated to what one wants to do. For anarchists, we need to also add the direct ties that need to exist between what one wants to do, the ideal for which one struggles and the way to obtain it. Despite the present disguising and word games, in the more or less marxist meanders, parties are still considered to be an adequate means to fight against political parties. We still see them today put forward the political affirmation of the productive forces (in times when the scale of the industrial disaster is under everyone’s eyes) as a road to end with capitalist relationships. Some want to take measures to render superfluous all other measures. Anarchists have nothing to do with this kind of magic tricks, for them the ends and the means need to coincide. Authority cannot be fought with authoritarian forms of organization. Those who pass their time picking apart the fine points of metaphysics, and find in this affirmation arguments against the use of violence, an alibi or a capitulation by anarchists, demonstrate through this above all their profound desire for order and harmony. Every human relation is conflictual, which does not mean that it is therefore authoritarian. To talk about such questions in absolute terms is certainly difficult, which doesn’t take away the fact the tension towards coherence is a vital need.

If today we think that affinity and affinity groups are the most adequate form for struggle and anarchist intervention in social conflictuality, it is because such a consideration is intimately tied to how we conceive of this struggle and this intervention. In fact, two roads exist to face the question, roads that are not diametrically opposite, but that also do not totally coincide. On one hand, there is the non-negotiable need of coherency. From there comes the question of the measure certain anarchist organizational forms (taking for example the organizations of synthesis with programs, some declarations of principles and some congresses such as anarchist federations or anarcho-sindacalist structures) answer to our idea of anarchism. On the other, there is the matter of adequateness of certain organizational structures. This adequateness puts the question more on the grounds of historical conditions, of goals that want to be reached (and therefore to the organizational form that is considered most apt to this), of analysis of the social and economic situation… To the big federations we would have preferred, also in other eras, small groups who move with autonomy and agility, but on the level of adequateness to the situation, with great difficulty one can exclude a priori that in certain conditions, the choice of an anarchist organization of struggle, specific and federated, of a guerrilla constellation…can (or rather, could have) answer to certain needs.

We think that contributing to insurrectional ruptures and developing them is today the most adequate anarchist intervention to fight against domination. For insurrectional ruptures we mean intentional ruptures, even if temporary, in the time and space of domination; therefore a necessarily violent rupture. Even though such ruptures have also a quantitative aspect (as they are social phenomenons that cannot be reduced to a random action of a fistful of revolutionaries), these are directed towards the quality of the confrontation. They take aim against structures and relations of power, they break with their time and space and allow, through the experiences made and the methods used to self-organize and of direct action, to question again and to attack more aspects of dominion. In short, the insurrectional ruptures seem to us necessary on the road towards the revolutionary transformation of the existent.

Out of all this logically derives the question of knowing how anarchists can organize themselves to contribute to such a rupture. Without giving up on the always important spreading of anarchist ideas, according to us, today, it is not about gathering at all costs the biggest amount of people possible around anarchism. In other words, we don’t think that what is necessary is strong anarchist organizations with a broad shining able to attract the exploited and the excluded, as a quantitative prelude for these organizations that in turn will give (when the time is ripe) the signal of insurrection. Furthermore, we think that it is unthinkable, in our days, that insurrectional ruptures could start from organizations that defend the interest of a particular social group, starting from, for example, more or less anarcho-syndacalist forms. The integration of such organizations within democratic management, in fact perfectly answers to contemporary capitalist economy; it is this integration that made it impossible to potentially cross from a defensive to an offensive position. Finally it seems to us impossible that today a strong “conspiracy” would be able, through different surgical operations, to make domination tremble and to drag the exploited in the insurrectional adventure; beyond the objections that can be made against this way of considering things. In historical contexts where power was very centralized, such as in czarist Russia, one could still somehow imagine the hypothesis of a direct attack against the heart (in this case the assassination of the czar) as a prelude to a generalized revolt. In a context of decentralized power like the one we know, the question can no longer be about striking the heart, hypothesizing a scenario where one, well aimed shot, could make domination shake in its foundations (which obviously doesn’t take anything away of the validity of a well aimed shot). Therefore other paths should be explored.

Many draw back in front of affinity. It is in fact a lot easier and less demanding to sign up to something, be it an organization, a permanent assembly or an scene and to take up and reproduce formal characteristics, rather than undertaking a long and never exhausted research for comrades with whom to share ideas, analysis and eventual projects. Because affinity is exactly this: a reciprocal knowledge between comrades, shared analysis that lead to prospectives of action. Affinity is therefore directed on one hand towards theoretical deepening and on the other towards intervention in social conflictuality.

Affinity is radically placed on the qualitative plane. It aspires to the sharing of ideas and methods, and it does not have as a goal an infinite growth. For some comrades, one of the main preoccupations, even though often well hidden, seems to remain the number. How many are we? What should we do to be more? From the polarization on such a question and from the constatation that today we aren’t many, given by the fact that many others do not share our ideas (no, also not unconsciously), derives the conclusion that we should, to grow numerically, avoid putting too hard of an accent on certain ideas. These days it is rare to still find those who will try to sell you a membership card to some revolutionary organization, destined to quantitatively grow and aspiring to represent always more exploited; but it is many who think that the best way to get to know others consists of organizing “consensual” activities such as for example self-organized bars, workshops, concerts, etc. Surely such activities can have their role, but when we face the topic of affinity we are talking about something else. Affinity is not the same thing as friendship. Of course the two do not exclude each other, but it is not because we share certain analysis that we sleep together, and vice versa. In the same way, just because we listen to the same music it doesn’t mean we want to struggle in the same way against domination.

The search for affinity occurs on an interpersonal level. It is not a collective event, a group affair, where it is always easier to follow than to think for oneself. The deepening of affinity is evidently a matter of thought and action, but in the end affinity is not the result of carrying out an action together, but rather a starting point from which to then pass to action. OK, this is obvious, some might say, but then this would mean that I will not meet many people who could be good comrades, because in some way I would confine myself in affinity. It is true that the search and the deepening of affinity require a lot of time and energy, and that therefore it is not possible to generalize it to all comrades. The anarchist movement of a country, of a city or even of a neighbourhood cannot become one big affinity group. It is not about enlarging different affinity groups with more comrades, but to make possible the multiplication of autonomous affinity groups. The search, the elaboration and the deepening of affinity leads to small groups of comrades that know each other, share analysis and pass together to action.

There’s the word… The aspect “group” of an affinity group has regularly been criticized, in both wrong and right ways. Often there are comrades who share the notion of affinity, but it becomes a lot more complicated when we start talking about “groups” which on one hand goes beyond an inter-individual aspect, while on the other hand seem to limit the “growth”. The objections most of the time consist in underlining the pernicious mechanisms of the “interior/exterior”, of the “inside/outside” that such affinity groups can generate (such as, for example, the fact of renouncing to one’s own path to follow the one of others, the sclerosis and the mechanisms that can surface such as certain forms of competition, hierarchy, feelings of superiority or inferiority, fear…). But these are problems that arise in any kind of organization and are not exclusively tied to affinity. It is about reflecting on how to avoid that the search for affinity brings to a stagnation and to a paralysis rather than to an expansion, a spreading and of a multiplication.

An affinity group is not the same thing as a “cell” of a party or an urban guerilla formation. Since its search is permanent, affinity evolves in permanence. It can “increase” up until the point that a shared project becomes possible, but on the other hand, it can also “decrease” until making it impossible to do anything together. The archipelago of affinity groups therefore constantly changes. This constant change is often pointed out by its critics: one cannot build anything from this, because it is not stable. We are convinced of the opposite: there is nothing to be built around organizational forms that revolve around themselves, away from the individuals that are part of it. Because sooner or later, at the first blows, excuses and tricks will anyways surface. The only fertile ground on which to build is the reciprocal search for affinity.

Finally, we would like to point out that this way of organization has the further advantage of being particularly resistant to the repressive measures of the state, since it does not have representative bastions, structures or names to defend. Where crystallized formations and big organizations can practically be dismantled in one hit, because of the same fact that they are rather static, affinity groups remain agile and dynamic even when repression hits. Since affinity groups are based on reciprocal knowledge and trust, the risks of infiltration, of manipulation and snitching are much more limited than in huge organizational structures to which people can formally join or in vague surroundings where it is only necessary to reproduce certain behaviour to join the club. Affinity is a quite hard base to corrupt, exactly because it starts from ideas and it also evolves according to these ideas.

We believe that anarchists have the most amount of freedom and autonomy of movement to intervene in social conflictivity if they organize themselves in small groups based on affinity, rather than in huge formations or in quantitative organizational forms. Of course, it is desirable and often necessary that these small groups are able to come to an understanding between each other. And not for the purpose of being transformed into a moloch or a phalanx, but to realize specific and shared aims. These aims therefore determine the intensity of the cooperation, of the organization. It is not excluded that one group who shares affinity organizes a demonstration, but in many cases a coordination between different groups could be desirable and necessary to realize this specific goal, anchored in time. Cooperation could be also more intense in the case of a struggle conceived on a medium term, as, for example a specific struggle against a structure of power (the building of a deportation centre, of a prison, of a nuclear base…). In such a case, we could talk about informal organization. Organization, because we are dealing with a coordination of wills, means and capacities between different affinity groups and individuals that share a specific project. Informal because we are not concerned with promoting some name, or quantitatively strengthening an organization, or signing up to a program or a declaration of principles, but of an agile and light coordination to answer the needs of a project of struggle.

In one way, informal organization finds itself also on the ground of affinity, but it goes beyond the inter-individual character. It exists only in the presence of a shared projectuality. An informal organization is therefore directly oriented towards struggle, and cannot exist apart from this. As we previously mentioned, it helps to answer to particular requirements of a project of struggle that cannot be at all, or with great difficulty, sustained by a single affinity group. It can, for example, allow to make available the means that we deem necessary. The informal organization does not therefore have the goal to gather all comrades behind the same flag or to reduce the autonomy of the affinity groups and of individualities, but to allow this autonomy to dialog. This is not a loophole for doing everything together, but it is a tool to materialize the content and the feeling of a common project, through the particular interventions of affinity groups and individualities.

What does it mean to have a project? Anarchist want the destruction of all authority, from this we can deduct that they are on the constant search for ways of doing this. In other words, it is certainly possible to be an anarchist and active in such without a specific project of struggle. In fact this is what happens in general. Whether anarchists are following the directive of the organizations they belong to (something that seems belonging more to the past), or whether they are waiting for the arrival of struggles they can participate to, or whether they attempt to include as many anarchist aspects as possible into their daily life: none of these attitudes presumes the presence of a real projectuality – something that, let’s make it clear, does not make these comrades less anarchists. A project is based on the analysis of the social, political and economic context one finds themselves in, and from which one refines a perspective that allows them to intervene in the short and medium term. A project that therefore holds an analysis, ideas and methods, coordinated to reach a purpose. We can for example publish an anarchist newspaper because we are anarchists and want to spread our ideas. OK, but a more projectual approach would require an analysis of the conditions in which this publication would be suitable to intervene in the conflictuality, which form it should therefore take,… We can decide to struggle against deportations, against the deterioration of the conditions of survival, against prison… because all these things are simply incompatible with our ideas; developing a project would necessitate an analysis to understand from where an anarchist intervention would be the most interesting, which methods to use, how to think of giving an impulse or intensification to the conflictual tension in a given period of time. It goes without saying that similar projects are usually the occasion for organizing informally, in a coordination between different groups and anarchist individualities.

Therefore an informal organization cannot be founded, constituted or abolished. It is born in a completely natural way, fulfilling the needs of a project of struggle and disappears when this project is realized or when it is assessed that it is no longer possible or relevant to realize it. It does not coincide with the entirety of the ongoing struggle: the many organizational forms, the different places of encounter, the assemblies etc. produced by a struggle will exist independently from the informal organization, which does not mean that anarchist cannot also be present there.

Up until now we have mainly talked about organizational forms between anarchists. Without a doubt, many revolts provide precious suggestions that are parallel to what we have just said. Let ́s take as an example the revolts of the last years in certain metropolis. Many rebels organized themselves in small agile groups. Or let ́s think of the riots on the other side of the mediterranean. There was no need of a strong organization or of some kind of representational structure of the exploited to spark the uprisings, their backbone was built of multiple forms of informal self-organization. Of course, in all this we did not express ourselves on the “content” of these revolts, but without rather anti-authoritarian organizational forms, it would be completely unthinkable that they would have taken a liberatory and libertarian direction.

It is time to say goodbye, once and for all, to all political reflexes, even more so in these times when revolts do not answer (not anymore) to political prerogatives. Insurrections and revolts should not be directed, neither by authoritarians nor by anarchists. They don’t ask to be organized in one big formation. This does not take away that our contribution to such events (phenomenons that are really social) cannot remain simply spontaneous if it aspires to be a qualitative contribution – this requires a certain amount of organization and projectuality. However the exploited and the excluded do not need anarchists to revolt or insurge. We can at most be an additional element, welcomed or not, a qualitative presence. But that nonetheless remains important, if we want to make the insurrectional ruptures break through in an anarchist direction.

If the exploited and the excluded are perfectly capable of revolting without anarchists and their presence, not for this are we ready to renounce looking for some points and a terrain where we can struggle with them. These points and this terrain are not “natural” or “automatic” consequences of historical conditions. The encounter among affinity groups, as well as informal organization of anarchists and exploited willing to fight, occurs better in the struggle itself, or at least in a proposal of struggle. The necessity of spreading and deepening anarchist ideas is undeniable and in no moment should we hide them, confine them to the back-alleys, or disguise them in the name of a given strategy. However in a project of insurrectional struggle it is not about converting the most amount of exploited and excluded to one’s own ideas, but rather to make possible experiences of struggle with anarchist and insurrectional methodology (attack, self-organization and permanent conflictuality). Depending on the hypothesis and the projects, it is necessary to effectively reflect on which organizational forms this encounter between anarchists and those who want to struggle on a radical basis can take. These organizational forms can certainly not be exclusively anarchist constellations, since other rebels take part in it. They are therefore not a support to “promote” anarchism, but have the purpose of giving shape and substance to an insurrectional struggle.

In some texts, drawn up from a series of experiences, there is a mention of “base nuclei” formed within the project of a specific struggle, of forms of organization based on the three fundamental characteristics of insurrectional methodology. Anarchists take part, but together with others. In a certain sense, they are mostly points of reference (not of anarchism, but of the ongoing struggle). They somewhat function as the lungs of a insurrectional struggle. When this struggle is intense it involves many people, and it diminishes in number when the temperature drops. The name of such organizational structures has little if no importance. One must discern, within certain projects of struggle, if similar organizational forms are imaginable or necessary. We have to also underline that this is not about collectives, committees, popular assemblies etc. previously formed and that have the purpose of lasting in time, and whose composition is rarely anti-political and autonomous (since there are often institutional elements involved). The “base nuclei” are formed within a project of struggle and only carry a concrete purpose: to attack and destroy an aspect of dominion. Therefore they are not para-unionist organizations that defend the interests of a social group (in the committees of the unemployed, in the assemblies of students…), but occasions of organization geared towards attack. The experiences of self organization and attack do not obviously guarantee that in a future struggle the exploited would not accept or not tolerate institutional elements. But without these experiences, these kind of reactions would be practically unthinkable.

To summarize, according to us it is not about building organizations that would “attract the masses” or to organize them, but to develop and put in practice concrete proposals of struggle. Within these proposals, of an insurrectional character, it is therefore important to reflect on the organizational forms considered necessary and adequate to realize a proposal of attack. We underline once again that these organizational forms do not necessarily implicate structures with meetings, places of encounter etc. but that these can also be born directly on the street, in moments of struggle. In certain places, for example, it can be easier to create some “points of reference” or a “base nucleus” with other exploited by interrupting the routine, putting up a barricade on the street… rather than waiting for everyone to come to an appointment to discuss about putting up a barricade. These aspects cannot be left totally to chance and to spontaneity. A projectuality allows reflexion and an evaluation of different possibilities and their relevancy.

If the question moves away from how to organize people for the struggle, it becomes how to organize the struggle. We think that archipelagos of affinity groups, independent one from the other, that can associate according to their shared prospectives and concrete projects of struggle, constitute the best way to directly pass to the offensive. This conceptions offers the biggest autonomy and the widest field of action possible. In the sphere of insurrectional projects it is necessary and possible to find ways of informally organizing that allow the encounter between anarchists and other rebels, forms of organization not intended to perpetuate themselves, but geared towards a specific and insurrectional purpose.

[Translated from Salto, subversion & anarchy, issue 2, november 2012 (Brussels).]




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

