
Finally I’ve decided to write, four and a half months after my arrest and imprisonment, because I’ve become incredibly sick of this giant, yet suffocating cage that haunts us, outside and inside. How can we not think about the hunts organized against us by police on the outside, like a poison that spreads, determined to choke revolt and suffocate solidarity? How can we forget our friends that are being followed and spied upon, arrested and controlled? How can we ignore a politics of power that seeks the survival of its own rottenness and mediocrity, that builds the legitimacy of its governance on the feeling of fear and separation amongst its subjects?

The fear of macabre crime and hordes of barbarians, sometimes spontaneous sometimes organized, is indispensable for the State to justify a repressive strategy of security and policing that perpetuates its complete power. Citizens can sleep soundly, knowing that the State watches and condemns the criminal pedophiles, the murdering terrorists, the bloodthirsty bandits that nest in our neighborhoods... The threat is everywhere. And words are powerful for creating a danger.

The reality is that the economy of fear is a fertile and blooming market. Surveillance cameras are sprouting around, as are private security companies. New technologies excel when it comes to security and snitching. And the police swarm our streets and our transit stations, justice is mechanical and brisk, the prison of all kinds are flourishing and overpopulated.

It is clear that the world is divided between honest people on the one side, and then on the other side the poor, unemployed, documented and undocumented immigrants, youth, strikers, people without permits, cons of social welfare, frauds, small traders of the black market, petty thieves, agitated people who offend and rebel, unruly people who refuse identification and data profiling, alcoholics, drug users, partisans of fireworks and smokebombs, prostitutes, depressed people, bruisers, casseurs (hooligans), saboteurs, lazy people, people interested in subversive readings, vagabonds... In a world that is governed in favor of honest people, different and guilty behaviour is not acceptable, and the struggle to be part of the honest class is admirable and worthy of merit. Work hard, denounce your neighbors, raise your children to the grandeur of the national identity and be obedient to the rules of the “party”.

Does such a servile and narrow consciousness of the world really exist? Is that the sad order that governs us? We’re not dupes and do not play along with that game. We will not embody these horrors. We will not be scapegoats nor martyrs. In a society where it’s good to remain silent and stay in one’s place, revolt can be fought with beatings, walls and barbwire, and irradiated with the dominant and false discourse, but will not be stopped. Ideas and critical thought know neither masters nor boundaries and free spirits will always remain outraged over having to live under the eternal constraint of the exploiting oppressors and oppressed exploited.

In four months, I’ve explored the female penitentiary of Fleury-Mérogis and the women’s section of Lille-Séquedin. In a month, I’ll enter the prison of Rouen... What to say about this unexpected dislocation that jerks our feet off the ground and our heads out of the clouds to compartmentalize us, to divide us, to reduce us to a thousand times and spaces, to a thousand places and realities, in a jumble of “self”, of self image, of multiple misshaped and amnesic faces?... How to define my uncertain paths between police, justice and prison?... Every step is a step forward in a jungle of cages that interlock like Russian dolls, silent and censored. Every step has to be a return to yourself in order to bring back together the dispersed pieces of your mind, and to destroy the bars that slowly take shape inside your body. It would be ridiculous if my prison situation was getting under my skin! To deny our own consciousness for a paranoia of the State is an act of suicide!

I don’t know exactly how to articulate the power and responsibilities between judicial and penitentiary authorities. The fact is that my transfer to Séquedin was “officially” motivated by the drawings I made of my cell and the yard, with the possibility of damaging the safety of the institution (as a pretext)... So, a dream of escape is probably the most just reason to keep a prisoner awake (but anyway, going from there to actually taking that step is a complicated reality...!) Apparently my immediate transfer to the Parisian region would be disturbed by this kind of complication: the delicate question of specially surveilled prisoners (DPS)... Well, if I’m not free by then, there’s a transfer scheduled to Fresnes in September... In the meanwhile, Rouen is the only temporary solution that is proposed to me on the way to Paris, which I’ve accepted because that penitentiary is more accessible to my relatives. But I won’t forget the precariousness and the illusion of my rights as a defendant presumed innocent, which until now seem to be concordant with imprisonment, an investigation on accusations and then my supposed dangerousness. This implies a reinforced control and justifies my removal and isolation from my relatives and from my defense.

To illustrate the insignificance of this condition I could for example recount how I spent two and a half hours in a cage in a police truck, with my hands cuffed, a few air holes, hardly any daylight, without food or water, until my arrival at the court in Paris. There I spoke for a few minutes with the lawyer whom I had not seen since my last transfer and then with an unbearable headache I was finally interrogated by the judge, knowing I had to look forward to the same thing on the way back again. That is a thorough and precise representation of our rights. And that is well known.

To go back a little, arriving in the prison was an terrible shock. After a nightmare of five days under pressure, under arrest and observation, with heavy accusations and in proportions which I could not have suspected, the never ending waiting has started... Until when? After two months I found a certain balance, linking experiences... But they preferred to break me a bit more, in a logic of punishment and revenge, and I’ve ended up in Lille- Séquedin, a modern prison that arose from the earth five years ago.

Once again, I lost my bearings. I ended up in an environment with more apparent security, smooth, clean, but icy. Long lit-up hallways filled by cameras behind protective covers, a small soulless yard under video surveillance, covered in tar and surrounded by a double row of fences and barbwire, a cell with a shower, a television from the State and five electric power boxes! And to populate this bleak place with ghosts, a rationalization and discipline of movement, meant to repress life to every extent. The spirit of this place has strongly combined comfort and cleanliness in the service of order. The flux and numbers are brought down to a minimum and are strictly regulated (three walks in the yard for 150 prisoners, limited activities in small groups). The time in the yard does not exceed the obligatory maximum per day (one hour and fifteen minutes per half a day). Exchange and solidarity between prisoners (besides the trade in stamps) are particularly complicated to develop in an atmosphere where repression is widespread (even pen and paper are not allowed during yard time)... And not to be confused, when laundry service is offered it is to avoid disorder and confusion with clothes hanging from the windows; to avoid the possibility that prisoners could “re-appropriate” and reshape the place within which they’re trying to survive...

Elsewhere in this new prison, that is partly managed by a private company (SIGES — a branch of SODEXHO) that organizes prison labor, the laundry shift is an essential activity for the women. I think that 1.5 tons of laundry is washed per day, coming from different prisons in the region. The men have to work in the kitchen. According to the same principle, Séquedin delivers meals in trays to all prisons in this zone. The wages are less than 200 Euros for the women (for a full time job) and 100 Euros more for the men. Since the opening of a juvenile prison in Quiévrechain, the juvenile wing of Séquedin has been closed. Now they are working to make it an ultra-secure wing. The security network progresses blindly: a new row of barbwire was recently added to the outside wall, the underground piping for the sewer was equipped with bars, etc... I compare this pacified banality with the penitentiary institution for women in Fleury, which has its history, its struggles, its evolutions, its accomplishments... And what characterizes the old prisons, like “collective” showers or the distribution of warm water in the morning... Sunday afternoon the yard time is extended to three hours with the authorization of a “picnic”. And never will a guard set foot in the yard... In fact, the façade is more apparent in the absolute.

At Séquedin, it’s like separation and erasure work on their own. Seldom do you hear prisoners banging in unison on the doors. But I hope that the women’s wing will be shaken in the future by the refusal of resignation, conquering new “rights” and freedoms, here and elsewhere. And finally, that these institutions of imprisonment everywhere are torn down. At this moment I am still waiting, but with more confidence and a progressive understanding of the mechanisms used to try to control us...

The struggle continues!

Isa, May 2008.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



Isa, May 2008.

