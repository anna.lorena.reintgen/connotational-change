
Now, as to the methods employed by Syndicalism—Direct Action, Sabotage, and the General Strike.

DIRECT ACTION.

Conscious individual or collective effort to protest against, or remedy, social conditions through the systematic assertion of the economic power of the workers.

Sabotage has been decried as criminal, even by so-called revolutionary Socialists. Of course, if you believe that property, which excludes the producer from its use, is justifiable, then sabotage is indeed a crime. But unless a Socialist continues to be under the influence of our bourgeois morality—a morality which enables the few to monopolize the earth at the expense of the many—he cannot consistently maintain that capitalist property is inviolate. Sabotage undermines this form of private possession. Can it therefore be considered criminal? On the contrary, it is ethical in the best sense, since it helps society to get rid of its worst foe, the most detrimental factor of social life.

Sabotage is mainly concerned with obstructing, by every possible method, the regular process of production, thereby demonstrating the determination of the workers to give according to what they receive, and no more. For instance, at the time of the French railroad strike of 1910, perishable goods were sent in slow trains, or in an opposite direction from the one intended. Who but the most ordinary philistine will call that a crime? If the railway men themselves go hungry, and the innocent public has not enough feeling of solidarity to insist that these men should get enough to live on, the public has forfeited the sympathy of the strikers and must take the consequences.

Another form of sabotage consisted, during this strike, in placing heavy boxes on goods marked Handle with care, cut glass and china and precious wines. From the standpoint of the law this may have been a crime, but from the standpoint of common humanity it was a very sensible thing. The same is true of disarranging a loom in a weaving mill, or living up to the letter of the law with all its red tape, as the Italian railway men did, thereby causing confusion in the railway service. In other words, sabotage is merely a weapon of defense in the industrial warfare, which is the more effective, because it touches capitalism in its most vital spot, the pocket.

By the General Strike, Syndicalism means a stoppage of work, the cessation of labor. Nor need such a strike be postponed until all the workers of a particular place or country are ready for it. As has been pointed out by Pelloutier, Pouget, as well as others, and particularly by recent events in England, the General Strike may be started by one industry and exert a tremendous force. It is as if one man suddenly raised the cry Stop the thief! Immediately others will take up the cry, till the air rings with it. The General Strike, initiated by one determined organization, by one industry or by a small, conscious minority among the workers, is the industrial cry of Stop the thief, which is soon taken up by many other industries, spreading like wildfire in a very short time.

One of the objections of politicians to the General Strike is that the workers also would suffer for the necessaries of life. In the first place, the workers are past masters in going hungry; secondly, it is certain that a General Strike is surer of prompt settlement than an ordinary strike. Witness the transport and miner strikes in England: how quickly the lords of State and capital were forced to make peace. Besides, Syndicalism recognizes the right of the producers to the things which they have created; namely, the right of the workers to help themselves if the strike does not meet with speedy settlement.

Sorel maintains that the General Strike is an inspiration necessary for the people to give their life meaning, he is expressing a thought which the Anarchists have never tired of emphasizing. Yet I do not hold with Sorel that the General Strike is a social myth, that may never be realized. I think that the General Strike will become a fact the moment labor understands its full value—its destructive as well as constructive value, as indeed many workers all over the world are beginning to realize.

These ideas and methods of Syndicalism some may consider entirely negative, though they are far from it in their effect upon society to-day. But Syndicalism has also a directly positive aspect. In fact, much more time and effort is being devoted to that phase than to the others. Various forms of Syndicalist activity are designed to prepare the workers, even within present social and industrial conditions, for the life of a new and better society. To that end the masses are trained in the spirit of mutual aid and brotherhood, their initiative and self-reliance developed, and an esprit de corps maintained whose very soul is solidarity of purpose and the community of interests of the international proletariat.

Chief among these activities are the mutualitées, or mutual aid societies, established by the French socialists. Their object is, foremost, to secure work for unemployed members, and to further that spirit of mutual assistance which rests upon the consciousness of labor’s identity of interests throughout the world.

In his The Labor Movement in France, Mr. L. Levine states that during the year 1902 over 74,000 workers, out of a total of 99,000 applicants, were provided with work by these societies, without being compelled to submit to the extortion of the employment bureau sharks.

These latter are a source of the deepest degradation, as well as of most shameless exploitation, of the worker. Especially does it hold true of America, where the employment agencies are in many cases also masked detective agencies, supplying workers in need of employment to strike regions, under false promises of steady, remunerative employment.

The French Confédération had long realized the vicious rôle of employment agencies as leeches upon the jobless worker and nurseries of scabbery. By the threat of a General Strike the French syndicalists forced the government to abolish the employment bureau sharks, and the workers’ own mutualitées have almost entirely superseded them, to the great economic and moral advantage of labor.

Besides the mutualitées, the French Syndicalists have established other activities tending to weld labor in closer bonds of solidarity and mutual aid. Among these are the efforts to assist workingmen journeying from place to place. The practical as well as ethical value of such assistance is inestimable. It serves to instill the spirit of fellowship and gives a sense of security in the feeling of oneness with the large family of labor. This is one of the vital effects of the Syndicalist spirit in France and other Latin countries. What a tremendous need there is for just such efforts in this country! Can anyone doubt the significance of the consciousness of workingmen coming from Chicago, for instance, to New York, sure to find there among their comrades welcome lodging and food until they have secured employment? This form of activity is entirely foreign to the labor bodies of this country, and as a result the traveling workman in search of a job—the blanket stiff—is constantly at the mercy of the constable and policeman, a victim of the vagrancy laws, and the unfortunate material whence is recruited, through stress of necessity, the army of scabdom.

I have repeatedly witnessed, while at the headquarters of the Confédération, the cases of workingmen who came with their union cards from various parts of France, and even from other countries of Europe, and were supplied with meals and lodging, and encouraged by every evidence of brotherly spirit, and made to feel at home by their fellow workers of the Confédération. It is due, to a great extent, to these activities of the Syndicalists that the French government is forced to employ the army for strikebreaking, because few workers are willing to lend themselves for such service, thanks to the efforts and tactics of Syndicalism.

No less in importance than the mutual aid activities of the Syndicalists is the cooperation established by them between the city and the country, the factory worker and the peasant or farmer, the latter providing the workers with food supplies during strikes, or taking care of the strikers’ children. This form of practical solidarity has for the first time been tried in this country during the Lawrence strike, with inspiring results.

And all these Syndicalist activities are permeated with the spirit of educational work, carried on systematically by evening classes on all vital subjects treated from an unbiased, libertarian standpoint—not the adulterated knowledge with which the minds are stuffed in our public schools. The scope of the education is truly phenomenal, including sex hygiene, the care of women during pregnancy and confinement, the care of home and children, sanitation and general hygiene; in fact, every branch of human knowledge—science, history, art—receives thorough attention, together with the practical application in the established workingmen’s libraries, dispensaries, concerts and festivals, in which the greatest artists and literateurs of Paris consider it an honor to participate.

One of the most vital efforts of Syndicalism is to prepare the workers, now, for their rôle in a free society. Thus the Syndicalist organizations supply its members with textbooks on every trade and industry, of a characterthat is calculated to make the worker an adept in his chosen line, a master of his craft, for the purpose of familiarizing him with all the branches of his industry, so that when labor finally takes over production and distribution, the people will be fully prepared to manage successfully their own affairs.

A demonstration of the effectiveness of this educational campaign of Syndicalism is given by the railroad men of Italy, whose mastery of all the details of transportation is so great that they could offer to the Italian government to take over the railroads of the country and guarantee their operation with greater economy and fewer accidents than is at present time done by the government.

Their ability to carry on production has been strikingly proved by the Syndicalists, in connection with the glass blowers’ strike in Italy. There the strikers, instead of remaining idle during the progress of the strike, decided themselves to carry on the production of glass. The wonderful spirit of solidarity resulting from the Syndicalist propaganda enabled them to build a glass factory within an incredibly short time. An old building, rented for the purpose and which would have ordinarily required months to be put into proper condition, was turned into a glass factory within a few weeks, by the solidaric efforts of the strikers aided by their comrades who toiled with them after working hours. Then the strikers began operating the glass-blowing factory, and their cooperative plan of work and distribution during the strike has proved so satisfactory in every way that the experimental factory has been made permanent and a part of the glass-blowing industry in Italy is now in the hands of the cooperative organization of the workers.

This method of applied education not only trains the worker in his daily struggle, but serves also to equip him for the battle royal and the future, when he is to assume his place in society as an intelligent, conscious being and useful producer, once capitalism is abolished.

Nearly all leading Syndicalists agree with the Anarchists that a free society can exist only through voluntary association, and that its ultimate success will depend upon the intellectual and moral development of the workers who will supplant the wage system with a new social arrangement, based on solidarity and economic well-being for all. That is Syndicalism, in theory and practice.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

