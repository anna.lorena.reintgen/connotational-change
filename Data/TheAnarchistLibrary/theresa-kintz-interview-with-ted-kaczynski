
Kaczynski’s story represents a parable:

Once upon a time there was a continent covered with beautiful pristine wilderness, where giant trees towered over lush mountainsides and rivers ran wild and free through deserts, where raptors soared and beavers labored at their pursuits and people lived in harmony with wild nature, accomplishing every task they needed to accomplish on a daily basis using only stones, bones and wood, walking gently on the Earth. Then came the explorers, conquerors, missionaries, soldiers, merchants and immigrants with their advanced technology, guns, and government. The wild life that had existed for millennia started dying, killed by a disease brought by alien versions of progress, arrogant visions of manifest destiny and a runaway utilitarian science.

In just 500 years, almost all the giant trees have been clear-cut and chemicals now poison the rivers; the eagle has faced extinction and the beaver’s work has been supplanted by the Army Corps of Engineers. And how have the people fared? What one concludes is most likely dependent on how well one is faring economically, emotionally and physically in this competitive technological world and the level of privilege one is afforded by the system. But for those who feel a deep connection to, a love and longing for, the wilderness and the wildness that once was, for the millions now crowded in cities, poor and oppressed, unable to find a clear target for their rage because the system is virtually omnipotent, these people are not faring well. All around us, as a result of human greed and a lack of respect for all life, wild nature and Mother Earth’s creatures are suffering. These beings are the victims of industrial society.

Cutting the bloody cord, that’s what we feel, the delirious exhilaration of independence, a rebirth backward in time and into primeval liberty, into freedom in the most simple, literal, primitive meaning of the word, the only meaning that really counts. The freedom, for example, to commit murder and get away with it scot-free, with no other burden than the jaunty halo of conscience.

My God! I’m thinking, what incredible shit we put up with most of our lives — the domestic routine, the stupid and useless and degrading jobs, the insufferable arrogance of elected officials, the crafty cheating and the slimy advertising of the businessmen, the tedious wars in which we kill our buddies instead of our real enemies back home in the capital, the foul, diseased and hideous cities and towns we live in, the constant petty tyranny of the automatic washers, the automobiles and TV machines and telephones-! ah Christ!,... what intolerable garbage and what utterly useless crap we bury ourselves in day by day, while patiently enduring at the same time the creeping strangulation of the clean white collar and the rich but modest four-in-hand garrote!

Such are my thoughts — you wouldn’t call them thoughts would you? — such are my feelings, a mixture of revulsion and delight, as we float away on the river, leaving behind for a while all that we most heartily and joyfully detest. That’s what the first taste of the wild does to a man, after having been penned up for too long in the city. No wonder the Authorities are so anxious to smother the wilderness under asphalt and reservoirs. They know what they are doing. Play safe. Ski only in a clockwise direction. Let’s all have fun together.

— Edward Abbey, Desert Solitaire, 1968

“I read Edward Abbey in mid-eighties and that was one of the things that gave me the idea that, ‘yeah, there are other people out there that have the same attitudes that I do.’ I read The Monkeywrench Gang, I think it was. But what first motivated me wasn’t anything I read. I just got mad seeing the machines ripping up the woods and so forth...”

— Dr. Theodore Kaczynski, in an interview with the Earth First! Journal, Administrative Maximum Facility Prison, Florence, Colorado, USA, June 1999.

Theodore Kaczynski developed a negative attitude toward the techno-industrial system very early in his life. It was in 1962, during his last year at Harvard,  he explained, when he began feeling a sense of disillusionment with the system. And he says he felt quite alone. “Back in the sixties there had been some critiques of technology, but as far as 1 knew there weren’t people who were against the technological system as-such... It wasn’t until 1971 or 72, shortly after I moved to Montana, that I read Jaques Ellul’s book, The Technological Society.” The book is a masterpiece. I was very enthusiastic when I read it. I thought, ‘look, this guy is saying things I have been wanting to say all along.’”

Why, I asked, did he personally come to be against technology? His immediate response was, “Why do you think? It reduces people to gears in a machine, it takes away our autonomy and our freedom.” But there was obviously more to it than that. Along with the rage he felt against the machine, his words revealed an obvious love for a very special place in the wilds of Montana. He became most animated, spoke most passionately, while relating stories about the mountain life he created there and then sought to defend against the encroachment of the system. “The honest truth is that I am not really politically oriented. I would have really rather just be living out in the woods. If nobody had started cutting roads through there and cutting the trees down and come buzzing around in helicopters and snowmobiles I would still just be living there and the rest of the world could just take care of itself. I got involved in political issues because I was driven to it, so to speak. I’m not really inclined in that direction.”

Kaczynski moved in a cabin that he built himself near Lincoln, Montana in 1971. His first decade there he concentrated on acquiring the primitive skills that would allow him to live autonomously in the wild. He explained that the urge to do this had been a part of his psyche since childhood. “Unquestionably there is no doubt that the reason I dropped out of the technological system is because I had read about other ways of life, in particular that of primitive peoples. When I was about eleven I remember going to the little local library in Evergreen Park, Illinois. They had a series of books published by the Smithsonian Institute that addressed various areas of science. Among other things, I read about anthropology in a book on human prehistory. I found it fascinating. After reading a few more books on the subject of Neanderthal man and so forth, I had this itch to read more. I started asking myself why and I came to the realization that what I really wanted was not to read another book, but that I just wanted to live that way.”

Kaczynski says he began an intensive study of how to identify wild edible plants, track animals and replicate primitive technologies, approaching the task like the scholar he was. “Many years ago I used to read books like, for example, Ernest Thompson Seton’s “Lives of Game Animals” to learn about animal behavior. But after a certain point, after living in the woods for a while, I developed an aversion to reading any scientific accounts. In some sense reading what the professional biologists said about wildlife ruined or contaminated it for me. What began to matter to me was the knowledge I acquired about wildlife through personal experience.

Kaczynski spoke at length about the life he led in his small cabin with no electricity and no running water. It was this lifestyle and the actual cabin that his attorneys would use to try to call his sanity into question during his trial. It was a defense strategy that Kaczynski said naturally greatly offended him. We spoke about the particulars of his daily routine. “I have quite a bit of experience identifying wild edible plants,” he said proudly, “it’s certainly one of the most fulfilling activities that I know of, going out in the woods and looking for things that are good to eat. But the trouble with a place like Montana, how it differs from the Eastern forests, is that starchy plant foods are much less available. There are edible roots but they are generally very small ones and the distribution is limited. The best ones usually grow down in the lower areas which are agricultural areas, actually ranches, and the ranchers presumably don’t want you digging up their meadows, so starchy foods were civilized foods. I bought flour, rice, corn meal, rolled oats, powdered milk and cooking oil.”

Kaczynski lamented never being able to accomplish three things to his satisfaction: building a crossbow that he could use for hunting, making a good pair of deerhide moccasins that would withstand the daily hikes he took on the rocky hillsides, and learning how to make fire consistently without using matches. He says he kept very busy and was happy with his solitary life. “One thing I found when living in the woods was that you get so that you don’t worry about the future, you don’t worry about dying, if things are good right now you think, ‘well, if I die next week, so that, things are good right now.’ I think it was Jane Austen who wrote in one of her novels that happiness is always something that you are anticipating in the future, not something that you have right now. This isn’t always true. Perhaps it is true in civilization, but when you get out of the system and become re-adapted to a different way of life, happiness is often something that you have right now.”

He readily admits he committed quite a few acts of monkeywrenching during the seventies, but there came a time when he decided to devote more energy into fighting against the system. He describes the catalyst:

“The best place, to me, was the largest remnant of this plateau that dates from the tertiary age. It’s kind of rolling country, not flat, and when you get to the edge of it you find these ravines that cut very steeply in to cliff-like drop-offs and there was even a waterfall there. It was about a two days hike from my cabin. That was the best spot until the summer of 1983. That summer there were too many people around my cabin so I decided I needed some peace. I went back to the plateau and when I got there I found they had put a road right through the middle of it” His voice trails off; he pauses, then continues, “You just can’t imagine how upset I was. It was from that point on I decided that, rather than trying to acquire further wilderness skills, I would work on getting back at the system. Revenge. That wasn’t the first time I ever did any monkeywrenching, but at that point, that sort of thing became a priority for me... I made a conscious effort to read things that were relevant to social issues, specifically the technological problem. For one thing, my concern was to understand how societies change, and for that purpose I read anthropology, history, a little bit of sociology and psychology, but mostly anthropology and history.”

Kaczynski soon came to the conclusion that reformist strategies that merely called for “fixing” the system were not enough, and he professed little confidence in the idea that a mass change in consciousness might someday be able to undermine the technological system. “I don’t think it can be done. In part because of the human tendency, for most people, there are exceptions, to take the path of least resistance. They’ll take the easy way out, and giving up your car, your television set, your electricity, is not the path of least resistance for most people. As I see it, I don’t think there is any controlled or planned way in which we can dismantle the industrial system. I think that the only way we will get rid of it is if it breaks down and collapses. That’s why I think the consequences will be something like the Russian Revolution, or circumstances like we see in other places in the world today like the Balkans, Afghanistan, Rwanda. This does, I think, pose a dilemma for radicals who take a non-violent point of view. When things break down, there is going to be violence and this does raise a question, I don’t know if I exactly want to call it a moral question, but the point is that for those who realize the need to do away with the techno-industrial system, if you work for its collapse, in effect you are killing a lot of people. If it collapses, there is going to be social disorder, there is going to be starvation, there aren’t going to be any more spare parts or fuel for farm equipment, there won’t be any more pesticide or fertilizer on which modern agriculture is dependent. So there isn’t going to be enough food to go around, so then what happens? This is something that, as far as I’ve read, I haven’t seen any radicals facing up to.

At this point he was asking me, as a radical, to face up to this issue. I responded I didn’t know the answer. He said neither did he, clasped his hands together and looked at me intently. His distinctly Midwestern accent, speech pattern, and the colloquialisms he used were so familiar and I thought about how much he reminded me of the professors I had as a student of anthropology, history and political philosophy in Ohio. I decided to relate to him the story of how one of my graduate advisors, Dr. Resnick, also a Harvard alumni, once posed the following question in a seminar on political legitimacy: Say a group of scientists asks for a meeting with the leading politicians in the country to discuss the introduction of a new invention. The scientists explain that the benefits of the technology are indisputable, that the invention will increase efficiency and make everyone’s life easier. The only down side, they caution, is that for it to work, forty-thousand innocent people will have to be killed each year. Would the politicians decide to adopt the new invention or not? The class was about to argue that such a proposal would be immediately rejected out of hand, then he casually remarked, “We already have it — the automobile.” He had forced us to ponder how much death and innocent suffering our society endures as a result of our commitment to maintaining the technological system — a system we all are born into now and have no choice but to try and adapt to. Everyone can see the existing technological society is violent, oppressive and destructive, but what can we do?

“The big problem is that people don’t believe a revolution is possible, and it is not possible precisely because they do not believe it is possible. To a large extent I think the eco-anarchist movement is accomplishing a great deal, but I think they could do it better... The real revolutionaries should separate themselves from the reformers... And I think that it would be good if a conscious effort was being made to get as many people as possible introduced to the wilderness. In a general way, I think what has to be done is not to try and convince or persuade the majority of people that we are right, as much as try to increase tensions in society to the point where things start to break down. To create a situation where people get uncomfortable enough that they’re going to rebel. So the question is how do you increase those tensions? I don’t know.”

Kaczynski wanted to talk about every aspect of the techno-industrial system in detail, and further, about why and how we should be working towards bringing about its demise. It was a subject we had both given a lot of thought to. We discussed direct action and the limits of political ideologies. But by far, the most interesting discussions revolved around our views about the superiority of wild life and wild nature. Towards the end of the interview, Kaczynski related a poignant story about the close relationship he had developed with snowshoe rabbit.

“This is kind of personal,” he begins by saying, and I ask if he wants me to turn off the tape. He says “no, I can tell you about it. While I was living in the woods I sort of invented some gods for myself” and he laughs. “Not that I believed in these things intellectually, but they were ideas that sort of corresponded with some of the feelings I had. I think the first one I invented was Grandfather Rabbit. You know the snowshoe rabbits were my main source of meat during the winters. I had spent a lot of time learning what they do and following their tracks all around before I could get close enough to shoot them. Sometimes you would track a rabbit around and around and then the tracks disappear. You can’t figure out where that rabbit went and lose the trail. I invented a myth for myself, that this was the Grandfather Rabbit, the grandfather who was responsible for the existence of all other rabbits. He was able to disappear, that is why you couldn’t catch him and why you would never see him... Every time I shot a snowshoe rabbit, I would always say ‘thank you Grandfather Rabbit.’ After a while I acquired an urge to draw snowshoe rabbits. I sort of got involved with them to the extent that they would occupy a great deal of my thought. I actually did have a wooden object that, among other things, I carved a snowshoe rabbit in. I planned to do a better one, just for the snowshoe rabbits, but I never did get it done. There was another one that I sometimes called the Will ‘o the Wisp, or the wings of the morning. That’s when you go out in to the hills in the morning and you just feel drawn to go on and on and on and on, then you are following the wisp. That was another god that I invented for myself.”

So Ted Kaczynski, living out in the wilderness, like generations of prehistoric peoples before him, had innocently rediscovered the forest’s gods. I wondered if he felt that those gods had forsaken him now as he sat facing life in prison with no more freedom, no more connection to the wild, nothing left of that life that was so important to him except for his sincere love of nature, his love of knowledge and his commitment to the revolutionary project of hastening the collapse of the techno-industrial system. I asked if he was afraid of losing his mind, if the circumstances he found himself in now would break his spirit? He answered, “No, what worries me is that I might in a sense adapt to this environment and come to be comfortable here and not resent it anymore. And I am afraid that as the years go by that I may forget, I may begin to lose my memories of the mountains and the woods and that’s what really worries me, that I might lose those memories, and lose that sense of contact with wild nature in general. But I am not afraid they are going to break my spirit.” And he offered the following advice to green anarchists who share his critique of the technological system and want to hasten the collapse of, as Edward Abbey put it, “the destroying juggernaut of industrial civilization”: “Never lose hope, be persistent and stubborn and never give up. There are many instances in history where apparent losers suddenly turn out to be winners unexpectedly, so you should never conclude all hope is lost.”




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



Once upon a time there was a continent covered with beautiful pristine wilderness, where giant trees towered over lush mountainsides and rivers ran wild and free through deserts, where raptors soared and beavers labored at their pursuits and people lived in harmony with wild nature, accomplishing every task they needed to accomplish on a daily basis using only stones, bones and wood, walking gently on the Earth. Then came the explorers, conquerors, missionaries, soldiers, merchants and immigrants with their advanced technology, guns, and government. The wild life that had existed for millennia started dying, killed by a disease brought by alien versions of progress, arrogant visions of manifest destiny and a runaway utilitarian science.


In just 500 years, almost all the giant trees have been clear-cut and chemicals now poison the rivers; the eagle has faced extinction and the beaver’s work has been supplanted by the Army Corps of Engineers. And how have the people fared? What one concludes is most likely dependent on how well one is faring economically, emotionally and physically in this competitive technological world and the level of privilege one is afforded by the system. But for those who feel a deep connection to, a love and longing for, the wilderness and the wildness that once was, for the millions now crowded in cities, poor and oppressed, unable to find a clear target for their rage because the system is virtually omnipotent, these people are not faring well. All around us, as a result of human greed and a lack of respect for all life, wild nature and Mother Earth’s creatures are suffering. These beings are the victims of industrial society.


Cutting the bloody cord, that’s what we feel, the delirious exhilaration of independence, a rebirth backward in time and into primeval liberty, into freedom in the most simple, literal, primitive meaning of the word, the only meaning that really counts. The freedom, for example, to commit murder and get away with it scot-free, with no other burden than the jaunty halo of conscience.


My God! I’m thinking, what incredible shit we put up with most of our lives — the domestic routine, the stupid and useless and degrading jobs, the insufferable arrogance of elected officials, the crafty cheating and the slimy advertising of the businessmen, the tedious wars in which we kill our buddies instead of our real enemies back home in the capital, the foul, diseased and hideous cities and towns we live in, the constant petty tyranny of the automatic washers, the automobiles and TV machines and telephones-! ah Christ!,... what intolerable garbage and what utterly useless crap we bury ourselves in day by day, while patiently enduring at the same time the creeping strangulation of the clean white collar and the rich but modest four-in-hand garrote!


Such are my thoughts — you wouldn’t call them thoughts would you? — such are my feelings, a mixture of revulsion and delight, as we float away on the river, leaving behind for a while all that we most heartily and joyfully detest. That’s what the first taste of the wild does to a man, after having been penned up for too long in the city. No wonder the Authorities are so anxious to smother the wilderness under asphalt and reservoirs. They know what they are doing. Play safe. Ski only in a clockwise direction. Let’s all have fun together.


— Edward Abbey, Desert Solitaire, 1968



“I read Edward Abbey in mid-eighties and that was one of the things that gave me the idea that, ‘yeah, there are other people out there that have the same attitudes that I do.’ I read The Monkeywrench Gang, I think it was. But what first motivated me wasn’t anything I read. I just got mad seeing the machines ripping up the woods and so forth...”


— Dr. Theodore Kaczynski, in an interview with the Earth First! Journal, Administrative Maximum Facility Prison, Florence, Colorado, USA, June 1999.

