      Welcome      Who?      What is the DO@ bloc?      Why?
The O’odham Solidarity Across Borders Collective and the Phoenix Class War Council send you greetings from occupied O’odham land. We also would like to invite you to participate with us in what we are loosely calling the Diné, O’odham, anarchist/anti-authoritarian Bloc. We hope to use this formation on the streets at the January 16th march against deportations in Phoenix to project a vision for a different mode of resistance that breaks with the stilted, uncreative status quo that dominates movement organizing in town. This document is our explanation of the type of force we would like to put out there and why we think its necessary.

We call on everyone tired of holding a sign and marching in endless circles while our lives come under increasing attack; everyone sick of a protest culture of self-sacrifice, defeat and witness; everyone who wants to stand up against the injustices that surround us; everyone interested in creative resistance rather than ritualized demonstrations; everyone tired of seeing our lands divided and destroyed and our movements tracked, tabulated and restricted.

We are an autonomous, anti-capitalist force that demands free movement and an end to forced dislocations for all people. We challenge with equal force both the systems of control that seek to occupy and split our lands in two as well as the organized commodification of every day life that reduces the definition of freedom to what can be produced and sold where and to whom, and compels our social relations to bend to the very same pathetic formula of production and consumption. Capital seeks to desecrate everything sacred. We hold lives over laws and human relations over commodity relations.

We recognize what appears to be an unending historical condition of forced removal here in the Southwestern so-called US. From the murdering of O’odham Peoples and stealing of their lands for the development of what is now known as the metropolitan Phoenix area, to the ongoing forced relocation of more than 14,000 Diné who have been uprooted for the extraction of natural resources just hours north of here, we recognize that this is not a condition that we must accept, it is a system that will continue to attack us unless we act.

Whether we are migrants deported for seeking to organize our own lives (first forced to migrate to a hostile country for work) or working class families foreclosed from our houses, we see the same forces at work. Indeed, in many cases the agents of these injustices are one and the same.

The sheriff’s deputy evicts and that same cop deports. It’s no coincidence that Maricopa County Sheriff Arpaio’s office is in the Wells Fargo building. On Tohono O’odham land, the Border Patrol captures migrants and also harasses traditional elders seeking to exercise their rights to free movement. It turns sovereign land into an armed camp surrounded by checkpoints in the finest Nazi fashion and divided in the most unnatural way. Wackenhut profits from the transportation of migrants held captive by the prison system and at the same time it patrols the city’s light rail stations. The same cameras that watch the border also watch our streets and populate our freeways, tracking our every move. These systems of control and dislocation overlap and affect all of us and, increasingly, they are everywhere. Wherever people organize in libertarian ways to resist the compulsory disarrangements of Capital, we are in solidarity with them.

Further, we categorically reject the government and those who organize with its agents. And we likewise oppose the tendency by some in the immigrant movement to police others within it, turning the young against movement militants and those whose vision of social change goes beyond the limited perspective of movement leaders. Their objectives are substantially less than total liberation, and we necessarily demand more.

Also, we strongly dispute the notion that a movement needs leaders in the form of politicians, whether they be movement personalities, self-appointed police or elected officials. We are accountable to ourselves and to each other, but not to them. Politicians will find no fertile ground for their machinations and manipulations. We have no use for them. We are anti-politics. We will not negotiate with Capital, the State or its agents.

In the last year we have seen signs that there might be openings for a new story to emerge. Almost a year ago we together led the march into the street, much to the chagrin of the leadership of the movement and the excitement of those who joined us, releasing themselves from the humiliation of marching on the sidewalk. Then, in October we challenged and shut down the National Socialist Movement, again leaving egg on the faces of those who in advance had denounced the action. A little more than a month ago what was to be just another boring leftist protest outside an Arpaio speaking engagement got out of control. Anarchists occupied the lobby where a large rally then followed, while other comrades, inside the forum, burst into song, driving the much-hated county cop from the stage. Movement leaders could only look at their hands. They have lost the initiative.

And well that they have, because the movement has failed and to continue on this course is suicide. Tens if not hundreds of thousands of migrants have been deported or self-deported themselves out of fear of attack by the State and vigilantes. The expansion of the attack on migrants, with its ubiquitous border cops and checkpoints, has spilled out onto indigenous communities and even those traveling the highways. Few are unaffected. Unable to conceptualize a framework for building resistance that can both protect those under attack and push forward to the offensive against the racist system as a whole, the movement now cries out for new ideas and creative action.

The movement has a dual problem of respect and identity. Internally, colonial relations often prevail. Age old, far off empires are evoked as justification for the marginalization, abuse and exploitation of peoples indigenous to the area. A general attitude of tokenism and disrespect dominates rather than genuine solidarity. And what was originally an honest desire to interrogate indigenous roots amongst many has morphed into something more like the colonialism of the Mexican and American states. This isn’t a healthy relationship.

That said, we think now could be our time. If we take advantage of this opening we can continue to push the movement towards more interesting and, in the end, successful actions. We can remake the discussion from one of internal colonialism and self-sacrifice into one based on free movement, the resistance to dislocation and anti-colonialism. We can introduce ideas of self-organization, autonomy and direct action, as well as criticisms of Capital and the State. We can shatter the death grip of the movement zombies and make a move towards building a force that can challenge more than just one sheriff in one county in Arizona.

We think the argument for free movement and against dislocation offer opportunities that currently elude the movement because of the inherent limitations of the debate as it now is being presented. The demand for free movement represents a rejection of all controls on travel and necessarily subverts the ever-expanding reach of the State and Capital. Likewise, the opposition to dislocation offers a framework on which to build resistance, something sorely lacking. After all, a foreclosure is a dislocation just like a deportation is. Expanding the argument this way also challenges the prevailing internal colonialism and tokenism, treating the struggles of indigenous people in Arizona with the dignity and respect they deserve.

The argument as it is now framed in the movement is a moral one. And yet many, especially whites, are not persuaded by moralism. Whiteness is a political position, not a moral one. Whites oppose the immigration movement not because they are immoral but because they seek to defend their relatively privileged position. If we remake the argument in a way that brings them into the circle so that they see that they, too, are under attack, then we think all bets are off about what we can do.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

