        Examples and Comments of Contemporary Psychogeography (lecture notes for a conference in Riga Art + Communication Festival, May 2003)
The topic of our panel discussion this afternoon is about “local media, maps and psychogeography”. I think it’s necessary to come back first to a brief history of psychogeography, Unitary Urbanism and the Situationnist International at the turn of the sixties. The end of the fifties and the beginning of the sixties were a period of acceleration in urbanism of European and world cities. In Paris, this period is the explosion of what politicians and urban planners called “new cities”. Paris was exploding outside its “ring road” and cities such as Sarcelles were created with totally new urban models. There was a strong feeling in that time that the cities were losing their human dimensions. I will first try to show how this acceleration of modernization of urban society had an influence on the tactics of the SI as an avant-garde concerned with the uniformization of society through urbanism, mass media, and the dichotomy of work and leisure. I will especially focus on Unitary Urbanism and 4 years of intense activities (‘58-‘61) that finally culminate by totally abandoning these theories. We will then discuss actual initiatives that use tactical medias in the streets and how this is link to the new rise of psychogeography and the necessity of reclaiming the streets.

The Situationist International emerged in 1957 from the Lettrist International, the Imaginary Bauhaus and the London Psychogeographical Committee, with new fields of interests and a revolutionary program that was focusing on the “suppression and realization of art in life” and what they called the “construction of situations”. The preliminary idea for many of their members, as many avant-gardes tried to, was to make creativity appear again in the social sphere.

Constructed Situation, Psychogeography and Unitary Urbanism were the key concepts at the foundation of the Situationist International. In 1958, the first issue of the Situationist International bulletin directed by Debord gives these definitions:

constructed situation

A moment of life concretely and deliberately constructed by the collective organization of a unitary ambiance and a game of events.

psychogeography

The study of the specific effects of the geographical environment (whether consciously organized or not) on the emotions and behavior of individuals.

unitary urbanism

The theory of the combined use of arts and techniques as means contributing to the construction of a unified milieu in dynamic relation with experiments in behavior.

These key concepts were developed for few years by founding members such as Jorn or Debord and relayed in Potlatch, the bulletin of the French section of the Lettrist International. I read you a quote of Asger Jorn’s text “Form and Image” published in 1954 in Potlatch 15.

“Architecture is always the ultimate realization of a mental and artistic evolution; it is the materialization of an economic level/status. Architecture is the pinnacle of realization for all artistic production because architecture signifies the construction of an atmosphere and fixes a way of living.”

Another founding text was The Formulary for a New Urbanism, by Gilles Ivain alias Ivan Chtcheglov, written in 1953 and published in 1958, in the first issue of the Situationist International bulletin.

“The architecture of tomorrow will be a means of modifying present conceptions of time and space. It will be a means of knowledge and a means of action.”

“We have already pointed out the need of constructing situations as being one of the fundamental desires on which the next civilization will be founded. This need for absolute creation has always been intimately associated with the need to play with architecture, time and space....”

Most of the persons who took part in the foundation of the SI were already excluded when the first central bulletin was published. Olmo, Verrone and Simondo were expelled over a row around a text on Experimental Music which Debord had accused of ‘right-wing thought’. Rumney who created the Psychogeographic Committee of London at the launch of the SI was expelled a bit later for failing to complete his psychogeographical report of Venice on time. Ironically he had mailed his essay off two days before being notified of his expulsion from Paris.

In December 1958, Constant and Debord wrote the Amsterdam Declaration to prepare the third conference of the SI and published it in the second issue. This issue also contain the famous “theory of the dérive”. I read you few extracts of the manifesto concerning Unitary Urbanism.

“4. The SI’s minimum programme is the development of complete decors, which must extend to a unitary urbanism, and research into new modes of behavior in relation to these decors.

6. The solution to problems of housing, traffic, and recreation can only be envisaged in relation to social, psychological and artistic perspectives that are combined in one synthetic hypothesis at the level of daily life.

9. All means are usable, on condition that they serve in a unitary action. The coordination of artistic and scientific means must lead to their total fusion.”

The German group Spur, which Jorn met in 1958, joined the SI and became its German section. Together with ex-COBRA Constant, the group developed the concepts of play and pleasure, central to the SI program. Under Constant and mainly with the participation of the Dutch section, the Bureau of Unitary Urbanism was created and based in Amsterdam. It was composed of a team of artists, architects and sociologists, and carried out studies towards the construction of unitary ambiances.

The situationists’ idea of psychogeography (and this was also related to Constant’s experiments since 1953) was that in the city one could create new situations by, for example, linking up parts of the city, neighborhoods that were separated spatially. It was done first in Amsterdam, using walkie-talkies. There was one group that went to one part of the city and could communicate with people in another area.

In the third issue of the bulletin published in December 1959, the text “Unitary Urbanism at the end of the fifties” confirmed that Unitary Urbanism was one of the central concerns of the SI and that Unitary Urbanism was not a doctrine of urbanism but a critique of urbanism.

“Unitary Urbanism is opposed to the temporal fixation of cities. It leads instead to the advocacy of a permanent transformation, an accelerated movement of the abandonment and reconstruction of the city in temporal and at times spatial terms. Unitary Urbanism is opposed to the fixation of people at certain points of a city as well.”

The Issue also report the 3rd Conference of the SI that took place in Munich. Some divergence emerged between Debord and Constant over Unitary Urbanism. Debord insisted on Unitary Urbanism as being a mere instrument, and envisaged a revolutionary creativity separated from existing culture. Constant insisted on the central role of Unitary Urbanism as an alternative means of a liberated creation and did not see the preconditions for a social revolution. Constant went even further in the text “another town for another life” describing utopian cities and architecture. A point that would create separation in the SI objectives.

One year after the third conference, the problem on Unitary Urbanism was not resolved. Constant resigned and the Bureau of Unitary Urbanism moved to Brussels under the direction of Attila Kotányi. In the texts “Gangland and philosophy” published in 1960 in issue 4, Attila Kotányi critiques some aspects of Unitary Urbanism and definitions of Debord as well in his text Construction of Situations He quotes Debord contradiction “Integral art, which has been talked about so much, can be realized only at the level of urbanism” In his opinion, that was exactly where the limit lay in the SI at that time. “Gangland,” in Chicago gangster slang, means the domain of crime, of rackets.

Kotányi suggests to reexamine common language and propaganda:

“We should develop a little glossary of detourned words. I propose that “neighborhood” should often be read gangland. Similarly, social organization = protection. Society = racket. Culture = conditioning. Leisure activity = protected crime. Education = premeditation.

The systematic falsification of basic information (by the idealist conception of space, for example, of which the most glaring expression is conventional cartography) is one of the basic reinforcements of the big lie that the racketeering interests impose on the whole gangland of social space.

If we were allowed to monitor, by means of an exhaustive survey, the entire social life of some specific urban sector during a short period of time, we could obtain a precise cross-sectional representation of the daily bombardment of news and information that is dropped on present-day urban populations.”

After 1960 there was the great movement in urbanization. The Situationists abandoned the theory of Unitary Urbanism, since Unitary Urbanism only had a precise meaning for historic cities, like Amsterdam. But from the moment that the historic city exploded into peripheries, suburbs — like what happened in Paris, and in all sorts of places, Los Angeles, Chicago, wild extensions of the city — the theory of Unitary Urbanism lost any meaning. Guy Debord said that urbanism was becoming an ideology and that people were becoming too much fascinated by it. Between the idea of elaborating an urbanism and the thesis that all urbanism is an ideology is a profound modification. Even the derive, the derive experiments were little by little abandoned around then, too.

The progressive radicalization of the French section was gradually dividing the group. The situation seemed to have got even worse with Jorn’s resignation. He resigned on April 1961, his position in the group having become more and more embarrassing following his huge success in the commercial art world.

In issue 6, the theoretical problem on urbanism became more and more important, even though Spur magazine was publishing a special issue on Unitary Urbanism at the same time. Raoul Vaneigem who had recently entered the SI, was part of the Bureau of Unitary Urbanism with Kotányi. They signed a “basic program of the Bureau of Unitary Urbanism” proclaiming the nullity of Urbanism and the nullity of the Spectacle. The development of the urban milieu was the capitalist domestication of space.

“Modern capitalism, which organizes the reduction of all social life to a spectacle, is incapable of presenting any spectacle other than that of our own alienation. Its urbanistic dream is its masterpiece.”

Vaneigem played two tactical roles — I would say to finish off Unitary Urbanism — one inside the Bureau of Unitary Urbanism and one with the danish Martin for the shift away from the Unitary Urbanist art practice. He writes in the same issue of the bulletin “comments against urbanism” arguing for example that: “Urbanism and information are linked in capitalist and anti-capitalist societies, they organize the silence.”

At the opening of the 5th Conference held in Göteborg (Sweden) at the end of August 1961, Vaneigem made this declaration: “there is no such thing as situationism or a situationist work of art... such a prospective doesn’t mean anything if not directly linked to revolutionary practice, to the will to change the use of life. [...] Our position is that of combatants between two worlds — one that we don’t acknowledge, the other that does not yet exist.”

The position of the German ‘Spur’ group (supported by most of the Scandinavians) on revolution and art were quite different from Vaneigem & friends. As it had already emerged from the previous Conference, the group didn’t believe the workers were dissatisfied enough to hold any revolutionary potential. It also had different opinions on the realization and suppression of art envisaged by Debord. The Conference adopted a resolution by Kotányi proposing to call all the artistic creations by members of the SI as anti-situationist, but ended without any of these controversies being resolved. Six months later the whole ‘Spur’ group was expelled by the Central Committee. In March 1962, the Scandinavian section broke away from the SI and announced the formation of a 2nd Situationist International around Nash (Jorn’s younger brother).

This is in a way the end of Unitary Urbanism and Psychogeography in the SI. With the attack of Vaneigem and Martin against the “nashists” and the rupture with the founding theories on urbanism and psychogeography.

The theory of situations was itself abandoned, little by little. And the journal itself became a political organ. They also began to insult everyone. Jorn was the only ex-situationist who was not attacked or denigrated by Debord.

In 1963 in a show in Denmark, Martin presented a piece called “destruction of the RSG 6”, in parallel to a protest against an A-bomb shelter in UK. In the exhibition, Martin showed thermonuclear maps depicting Europe 4 and a half hours after the beginning of thermonuclear war. This makes me think of Bureau D’Études’ tactics. A few months after this, Kotányi was excluded for asking for a fundamental reorientation of the theory. This reorientation was rejected with accusations of mysticism.

So, to sum things up, this is the time to give my opinion. I think the breaking point in the SI in 1961 and the radicalization of the French group was around the perception that the first aims of psychogeography were shifting from a tool for social movements to a series of psychogeographical “games”. And this is also quite clearly present in the contemporary technological use of GPS, cell phones, wireless networks that are a bit limited to “games” instead of using these technologies for social actions.

To use GPS & cellphones for tracing your friends, as yahoo suggests, for example as a “friendly” use of surveillance technologies has a double side. You don’t really know if these data are monitored. Monitoring so-called “friendly” monitoring. In opposition the idea would be to devellop prototypes of new cell phones for alternative use such as in protests. I would also like to describe the program “Acropol” by the EADS company, a new system of digital radio that replaces the old UHF/VHF radio in the Police of Paris since 2001. This system introduces nodes in the city of Paris and was not really perfect, they didn’t get a signal in tunnels for example, but soon the whole city will be well equipped and this system is supposed to be so well encrypted that radio activists or militants will not be able to listen the police anymore. Of course this is also a thing which they will have to catch up with.

In terms of tactics to organize demonstration, and to chose itinerary of the demo, it’s necessary to know the psychogeographical aspects of your city well. It’s substantially different in Paris if you start a demo in Invalides (that is a symbol with Napoleon grave) to Saint Michel (symbol of 1968), or if you go from Bastille Square (symbol of Revolution) to Nation Square (symbol of resistance to wars with Germany, Nation is on the east of Paris and is where invaders usually came in during wars). You can chose different places you pass through to stimulate imaginations of the protesters that can also provoke riots.

Also I think psychogeography is a tool to learn about the theory of capitalism, history, occult science and politics relations, and urbanism as well. Technology is not necessary. I think psychogeography is also an individual process of drifting to know about your personal relation to the city and history.

Psychogeography was used by the AAA as an exit strategy. It was a good occasion for us to gather. The AAA intergalactic conference in Bologna in 1998 (and after) was the occasion to explore the twelve gates within the historic city center, which are dedicated to the 12 signs of the Zodiac. During the conference, the East London AAA invited the Autonomous  Astronauts to follow the pathway of Giordano Bruno in London under the call “reclaim the stars”. Bruno, the author of the “The Expulsion of the Triumphant Beast” was burnt in Rome in 1600 for crimes of heresy after having proclaimed the plurality of the living world and in consequence fled persecution across Europe. The initiative “Reclaim the Stars” stimulated the psychogeographic events within the larger movement of Reclaim the Streets. The larger movement took place over 18 days of actions against the financial center, the City of London (usually referred to as June 18) and the Space 1999 festival of the AAA as well.

In 1999, the New Zealand group of the Association of Autonomous Astronauts (AAA-Aotearoa) tell the story of a hot air balloon launch in the Garden Place of Hamilton (NZ), as an AAA street theater performance against the tests in the Pacific of the Ballistic Missile Defense (BMD) program of the USA. Then they follow the balloon until it becomes impossible to see.

Jungle AAA is the Dutch group of the AAA and Social Fiction is their global project where they are hosted that also includes works about “Generative Psychogeography”. Another possible definition of psychogeography might be that it’s an activity that tries to figure out how the cognitive image we have of a certain place has been formed by its design & this image conforms reality by exploring it in a way that is different from normal use of that space.

There is a growing number of psychogeographical groups around the world such as the Orlando psychogeographical association, the New York psychogeographical association, or olders like the Nottingham Psychogeographical Unit, London Psychogeographical Association (LPA). In 1992 the LPA resurrected the Psychogeographic Committee of London by publishing a large number of newsletters and psychogeographical explorations within the capital. I can also mention the Psychogeographical Cyclists Association but I think I will stop there.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



constructed situation


A moment of life concretely and deliberately constructed by the collective organization of a unitary ambiance and a game of events.



psychogeography


The study of the specific effects of the geographical environment (whether consciously organized or not) on the emotions and behavior of individuals.



unitary urbanism


The theory of the combined use of arts and techniques as means contributing to the construction of a unified milieu in dynamic relation with experiments in behavior.



“Architecture is always the ultimate realization of a mental and artistic evolution; it is the materialization of an economic level/status. Architecture is the pinnacle of realization for all artistic production because architecture signifies the construction of an atmosphere and fixes a way of living.”



“The architecture of tomorrow will be a means of modifying present conceptions of time and space. It will be a means of knowledge and a means of action.”


“We have already pointed out the need of constructing situations as being one of the fundamental desires on which the next civilization will be founded. This need for absolute creation has always been intimately associated with the need to play with architecture, time and space....”



“4. The SI’s minimum programme is the development of complete decors, which must extend to a unitary urbanism, and research into new modes of behavior in relation to these decors.


6. The solution to problems of housing, traffic, and recreation can only be envisaged in relation to social, psychological and artistic perspectives that are combined in one synthetic hypothesis at the level of daily life.


9. All means are usable, on condition that they serve in a unitary action. The coordination of artistic and scientific means must lead to their total fusion.”



“Unitary Urbanism is opposed to the temporal fixation of cities. It leads instead to the advocacy of a permanent transformation, an accelerated movement of the abandonment and reconstruction of the city in temporal and at times spatial terms. Unitary Urbanism is opposed to the fixation of people at certain points of a city as well.”



“We should develop a little glossary of detourned words. I propose that “neighborhood” should often be read gangland. Similarly, social organization = protection. Society = racket. Culture = conditioning. Leisure activity = protected crime. Education = premeditation.


The systematic falsification of basic information (by the idealist conception of space, for example, of which the most glaring expression is conventional cartography) is one of the basic reinforcements of the big lie that the racketeering interests impose on the whole gangland of social space.


If we were allowed to monitor, by means of an exhaustive survey, the entire social life of some specific urban sector during a short period of time, we could obtain a precise cross-sectional representation of the daily bombardment of news and information that is dropped on present-day urban populations.”



“Modern capitalism, which organizes the reduction of all social life to a spectacle, is incapable of presenting any spectacle other than that of our own alienation. Its urbanistic dream is its masterpiece.”

