
To read Le Guin is to enter a sharply focused world of vivid political drama from individual struggles to cosmic conflict. The following remarks (based on seven of her nine novels and five of her stories) first present the general framework of her political perception, then relate her insights, and her particular use of the future, to contemporary reality, and finally assess the relative effectiveness of her writings as a distinct medium of political communication.

1. Le Guin's perspective seems to have evolved from a more individualized existentialist orientation and anthropological concern in the mid-1960s to an emphatic embrace of Taoism. From there, by the mid-70s, she moved to a much richer social critique and explicit anarchist commitment. To speak of her evolution, however, is to describe only a shifting of emphases. In fact, Le Guin is amazingly consistent in her general preoccupation with the relationship of good and evil, the illusions of superior accomplishment, and the role of the individual in the face of catastrophic change. She shows a corresponding and equally constant disdain for the "ordinary politics" of exploitation, alienation, and egocentrism.

In Le Guin's view, the unity and equilibrium of good and evil in human nature reflects on the individual scale, the larger universal balance and interdependence of opposites in the broader natural world. Her most emphatic early statement of dynamic equilibrium is in A Wizard of Earthsea (WE). The apprentice wizard, Ged, learns through an encounter with his own death-shadow nearly fatal to himself and to the world, the absolute need to acknowledge this balance: one cannot mock or evade death without endangering life itself. Le Guin's works abound with vivid examples of those who fail to comprehend themselves as the unification of opposites: e.g., the Fiia and Clayfolk in Rocannon's World (RW), the nations of Karhide and Orgoreyn on the planet Gethen in The Left Hand of Darkness (LHD), Dr. Haber in The Lathe of Heaven (LoH), the twin planets of Anarres and Urras in The Dispossessed (TD), Captain Davidson in the New Tahiti colony in "The Word for World is Forest" (WWF), and the "happy" citizens of Omelas in "The Ones Who Walk Away from Omelas" (OWO). By contrast, those who see the unity behind their own internal conflicts inevitably become Le Guin's leading protagonists: the wizard Ged in WE, Jakob Agat Alterra in The Planet of Exile (PE), Falk-Agad in City of Illusions (CI), Genly Ai in LHD, George Orr in LoH, Shevek in TD, Selver in WWF.

Self-deceiving illusions of superior accomplishment provide another favorite theme that takes a variety of forms. First and most spectacular is the total inability of one culture to comprehend another, thereby removing any reference points whatsoever. Three astronauts exposed to a mind-shattering total experience in the ruins of a Martian "city" lack any conceptual tools to communicate their findings back to Earth ("Field of Vision" [FV]). A second type of illusion of superiority is that form of cultural imperialism which sees the homeland's way of life as alone deserving recognition; appearing in virtually all of her writings, it is a dominant theme in WWF. Social self-deception also appears when competing societies (such as the Askatever and "farborns"—i.e., Terrans—of PE, and Karhide and Orgoreyn in LHD) complement each other's strengths and weaknesses. Another type of illusion appears when a highly "progressive" society depends for its very success on a fundamental moral failure; the ambiguously utopian Omelas community of OWO is her perfected model, though her most explicit imperialists—such as the Terran New Tahiti colony on Athshe in WWF and the Shing in CI—belong to the same family. A final self-deception derives from a society's inability to define progress in broad enough terms: thus Dr. Haber's attempt to end war on Earth in turn causes war with a non-Earth power (LoH), and even the anarchist utopia of Anarres develops its own brand of political tyranny by failing to protect that individual creativity essential to its own health.

Le Guin consistently concerns herself with individuals striving to preserve their integrity, and their resulting conflicts with society. In her earlier works, she focuses primarily on the individual. Social action, when it appears, comes in the form of defensive measures by key characters in a crisis (as in RW, PE, and CI). In this her tone is existentialist. In the middle transition phase she still emphasizes individual development, yet also reflects on the need for balance in the overall society as well. Here Taoist imagery predominates. Her most recent emphasis shifts to the broad nature and inevitability of constant social change itself and its effects on the individual (as in WWF, TD, and "The Day Before the Revolution" [DBR]). In this there is a much more definite political, specifically anarchist, tone. Each of these phases is closely related to the others. A continuity exists, but the different emphases within it seem to express Le Guin's own political maturation. In this evolution Le Guin represents a significant section of a whole generation of white radical American intellectuals, from the early 1960s to the present. [1]

Typical of her first phase are the chief characters of PE, Rolery and Jakob. These two, strangers from very different though neighboring races, make the existential leap into the absurd through a love affair that risks the very annihilation of both peoples. Despite the impending disaster, Le Guin implies that it is the integrity of the personal relationship, and the willingness to risk all for it, which really counts. Through the persistence of that integrity, the two previously irreconcilable groups eventually ally together in a successful defense and later (as we learn in CI) in a blending of their races.

Le Guin's Taoist tone appears most prominently in WE, LHD, and LoH, though it resonates in practically every one of her works. George Orr, Genly Ai, and Falk-Agat each literally confront the experience of chaos. Orr's world dissolves before his very eyes: "The buildings of downtown Portland, the Capital of the World...were melting. They were getting soggy and shaky, like jello left out in the sun.... It was an area, or perhaps a time-period, of a sort of emptiness" (LoH §10). With each heroic character, however—Le Guin says loud and clear—if the will is strong enough, if one is wholly committed to one's deepest understanding of the truth, and at the same time tolerant of ambiguities, it is possible to pass even through the realm of the void in confronting one's deadliest enemies, and still meet success.

Le Guin's third phase, that of anarchism, asserts that individuals must participate collectively in social change as a necessary precondition to maintaining and developing personal integrity. At the same time, self-development occurs only if social movements themselves are designed for individual growth instead of conformity. Both aspects appear prominently in the Odonian and Athshean revolutionary movements of TD, DBR, and WWF. Selver, for example, realizes the incompleteness and ultimate impossibility of his people relying solely on spiritual, intuitional ("dream-time") fulfillment so long as material ("world-time") conditions were so destructive. Despite his previous pacifism, he tells captured Terran colonists, "We had to kill you, before you drove us mad" (WWF §6). On the other hand, while leading the growing resistance movement, Selver discovers that his political role in turn prevents "dream" consciousness. In addition, however necessary it may be for self-preservation, killing or violence changes the previously non-violent person: "it's himself whom the murderer kills...over and over" (WWF §5).

According to Le Guin, to neglect the need for balance, for moderation, for appreciation of the inherent contradictions in individuals and society, is to cause individual and social egoism and all their disastrous consequences. The imbalance of egocentrism produces every type of human exploitation and disaster. In her view, a conservatism which unabashedly glorifies egoistic fulfillment through existing social structures and a liberalism which protects and encourages the same egoism behind labels of "social interest" are equally pernicious. Her detailed images of conservative logic in the character of Davidson (WWF) and of liberal logic in Lyubov (WWF) and Dr. Haber (LoH) are powerful indeed. So also is her denunciation of authoritarian collectivism, as symbolized by the countries of Thu (TD) and Orgoreyn (LHD). For Le Guin, the only political arrangement sensitive to the need for moderation, for non-egoistic social relations and identity of humans with nature is a classless society. This model she offers consistently—despite its own significant problems—from the primitive tribal groups in her earlier works to the planet-wide anarchist community of TD.

2. Le Guin articulates her political dilemmas in credible and dramatic terms, thus inviting the reader to think politically too. Her writing places believable characters in easily recognizable political settings, and forces them to deal with significant issues. The reader not only feels involved in the political drama, but also receives data for independent agreement or disagreement with the author—a rare quality indeed. Beyond her vividness and internal consistency, her settings, issues, and solutions are similar enough to our own contemporary world that the applicability of her thought and action to our own political problems becomes practically self-evident.

Imperialist relations are clearly one of Le Guin's prime political insights, a theme presented to greater or lesser degree in practically every one of her writings. In WWF an ecologically sensitive, non-aggressive native population is brutally tyrannized by a plundering colonist power all too familiar from the history of European settlers in America, Africa, and Asia, and the recent U.S. war in Vietnam. In the words of colonist Captain Davidson: "This world, New Tahiti, was literally made for men.... Get enough humans here, build machines and robots, make farms and cities, and nobody would need the creechies [the native 'creatures'] any more"; "Cleaned up and cleaned out, it would be a paradise, a real Eden"; "It's just how things happen to be. Primitive races always have to give way to civilized ones. Or be assimilated. But we sure as hell can't assimilate a lot of green monkeys" (§1). Typically, when the "creechie" revolt begins, Davidson can't believe at first that the natives are involved; it has to be a colonist or off-planet force. Once he is forced to believe that there is a revolt, he responds by advocating genocide "to make the world safe for the Terran way of life" (§4).

Beyond the particular issue of imperialism, Le Guin's works also explore the other major contemporary foci of political crisis: racism, sexism, nationalism, militarism, class society, authoritarianism, and ecocide. Sexism, like the other common issues, usually appears as simply a given aspect of the status quo. It becomes a dominant theme in LHD. Genly Ai, the male heterosexual envoy from the Ekumen, is forced to confront at the root of his psyche not only his own biases against women but a planet of complete bisexuals who regard him as sexually degenerate. Even after two years among them, "I was still.... seeing a Gethenian first as a man, then as a woman, forcing him into those categories so irrelevant to his nature and so essential to my own" (§1). By the end he finally sees in his friend Estraven what he "had always been afraid to see, and had pretended not to see in him: that he was a woman as well as a man. Any need to explain the sources of that fear vanished with the fear; what I was left with was, at last, acceptance of him as he was" (§18).

As with her statements of issues, Le Guin's particular political strategies are directly relevant to current American society. Overall, her solution is to develop awareness of exploitation, expose those structures producing it, and create alternative communities as open as possible to the fulfillment of all their members. She clearly favors anarchist and "counter-cultural" directions. But they must be followed consistently, with the open-endedness basic to their definition, to avoid the danger of new walls, new supposedly "liberating" forms of what might turn out to be the old exploitation. Sabul's quasi-politicking and Vea's supposed "sexual liberation" are emphatic examples of the latter in TD. Le Guin prefers Taoist non-action to Western assertiveness. At the same time, she realizes that some action is necessary (as with the Athshean revolt or Shevek's voyage to Urras) to arrive at the desired flexible equilibrium. [2] One must accept no walls, though coming home (to one's roots within the walls) is just as important, just as human, as going forth to adventure. Being and becoming is Le Guin's political stance, as it is the keystone to Shevek's revolutionary physics (TD §7).

Le Guin's alternative worlds in the future (from 1990 in FV to about 4800 in LHD) are primarily either a logical extension of present-day negative trends (such as militarism, ecocide, and egoism in general) or an analogic fantasy-context in which to present more selectively and thus more starkly certain of today's harshest contradictions. In the latter case, it hardly matters whether the society examined is 20 or 2000 years in the future. Her worlds are basically worlds of today. She subtracts or adds a small number of technologies, such as rapid space-flight and instantaneous galactic communication, but these are not essential to the inner dynamics of the particular planets involved.

The two main non-technological social innovations of Le Guin's stories are anarchism and parapsychological communication. Both have recognizable roots in contemporary practice and theory. Even so, Le Guin is ambiguous. Her anarchism seems a clearly superior political form when compared to hierarchical models (as in WWF, DBR, and TD). Yet to date she has placed her anarchist model either in an ecologically sensitive, economically undeveloped tribal-type setting (as in WWF), a limited emigrant enclave (as in PE), a situation of extreme economic scarcity (as in TD), or a vague cosmic setting with great spatial distances between constituent communities (as with the Ekumen in LHD). None of these has a clear direct connection to conditions in contemporary America; perhaps Le Guin will accept that challenge in writings to come. In the meantime, Le Guin clearly invites her US readers to seriously consider the principle of least contradictory, least egoistic politics for the society of which they are now a part.

Le Guin's parapsychology, like her anarchism, is both a critique of existing society and a positive alternative for the future. There is no more devastating critique of existing interpersonal relations, for example, than the deadly subconscious exchanges between shipmates in "Vaster than Empires and More Slow." The highly sensitive empath, Osden, when asked what he perceives with his talents, replies: "Muck. The psychic excreta of the animal kingdom. I wade through your feces." Le Guin's resolution to this story—a highly tuned-in "bliss-out" between sensitive and balanced beings—is perhaps one of her basic long-range social preferences. [3] Beyond the totally exceptional and fanciful experiences in LoH and FV, she does suggest that the development of telepathic talents might promote greater civilization and understanding. On the other hand, as the "mindlying" talents of the Shing in CI demonstrate, telepathy also has serious negative political potentials as well. Perhaps even greater beneficial potentials exist in the integration of waking and sleep. To dream (in the Athshean sense) is to get back in touch with the "springs of reality" or subconscious roots (WWF §§ 2, 5). To dream is also, perhaps, to be more sensitive to parapsychological phenomena. In either case, the collective political effects of significant widespread personal growth in this area could be profound.

3. In conclusion, there is no doubt that Le Guin takes politics extremely seriously, both in her awareness of the destruction it currently produces and in her sense of better alternatives. Her images of contemporary existence are presented clearly and vividly because they are seen in a consistent though evolving political perspective. From this it follows that she herself must be taken seriously as a political writer-activist. Le Guin defends the SF form as a highly important and unique type of political communication:

"At this point, realism is perhaps the least adequate means of understanding or portraying the incredible realities of our existence. The fantasist...may be talking as seriously as any sociologist—and a good deal more directly—about human life as it is lived, and as it might be lived, and as it ought to be lived. For, after all, as great scientists have said and as all children know, it is above all by the imagination that we achieve perception, and compassion, and hope." [4]

At its best SF removes readers from the stale reference points of everyday political discourse and life.

It is true that SF is also inherently susceptible to becoming flippant fantasy, unrelated to the serious world of the present or at best useful merely as escapist relief. The right formula for positively changing political consciousness, especially in a mass audience, is amazingly elusive. Yet there is no doubt that Le Guin's skillful, sensitive, complex, adventurous, and vivid writing about both the micro- and macro-levels of society is one of the clearest proofs to date that SF can carry a general reader into a whole new realm of awareness—an awareness often rejected when presented by other activists with other manners of invitation. "Good artistry doesn't moralize; it seeks to engage one more"; a good SF writer may have "intense and intelligent" moral seriousness, without moralizing and preaching: "He gambles; he tries to engage us. In other words, he works as an artist." [5] It is far easier for the average reader to dismiss a radical tract or a radical speaker than to set down a Le Guin writing, once begun. In her own manner, with her own special skills, Le Guin succeeds in taking us on that spiral journey of growth—adventuring outward, returning back home somewhat wiser—which is so central to her own political thought.
[1] By the 1950s a new generation of rebel intellectuals had emerged that were apparently not linked to the Old Left Marxist radicalism of the 1930s. Based on the existentialist concern with individual integrity, social protest concentrated on immediate issues of individual resistance to social immorality (such as the Chessman, anti-HUAC, Berkeley Free Speech, and original draft-card burning demonstrations). By the late 1960s this tendency had led, as in the case of SDS, to a fundamental alienation from and confrontation with the entire political structure itself. Simultaneously the rise of drug use and alternative life-styles, combined with movement reaction against macho heaviness and Marxian dogmatism, then joined by massive government repression, drove many radicals into an ill-defined, ambiguously "apolitical" new stage. This was characterized by inner-directed "counter-cultural" changes and a more contemplative and sporadically resisting political attitude toward the broader social structures. It was a time when the worth of assertive politics as a whole—radical as well as establishment—was subject to challenge. A new synthesis, the third stage increasingly adopted by the mid-70s, has attempted to integrate counter-cultural insights and radical politics into a consistent whole. Face-to-face and small-group politics are seen as just as essential as, yet also dependent on, the politics of the nation. Non-directive small affinity groups, less publicized local instead of spectacular national organizing, and an increasingly explicit anarchist focus characterize the behavior and orientation of large numbers of radicals in this current stage. Useful statements of this evolution, beyond the works of Le Guin, are found in several books by Michael Rossman and Theodore Roszak, as well as Julian Beck's The Life of the Theatre (City Lights Books, 1972). A 1949 philosophical prefiguration of the same ideological evolution is found in Herbert Read's Anarchy and Order (reprinted 1971 by Beacon Press).
[2] In her recognition of the necessary interdependence of the two traditions, Le Guin's politics are quite similar to those of Gary Snyder and Allen Ginsberg. See Snyder's essays, "Buddhism and the Coming Revolution" and "Why Tribe," in his Earth House Hold (New Directions, 1969), and Ginsberg's essay, "Consciousness and Practical Action," in Counter-Culture, ed. Joseph Berke (London: Owen, 1969). This position, as symbolized also in Le Guin's descriptions of Thu and Orgoreyn, seems clearly to set forth her attitude as well toward Marxist-Leninist models of organization.
[3] Similar political potentials of paranormal behavior are implied in Roszak, in surrealist politics, and in research currently encouraged in the Soviet Union and Eastern Europe. See Theodore Roszak, Where the Wasteland Ends (Doubleday, 1972); "Surrealism in the Service of the Revolution," a special issue of Radical America, January 1970; and Sheila Ostrander and Lynn Schroder, Psychic Discoveries Behind the Iron Curtain (Bantam Books, 1971). I differ here with Ian Watson in his apparent categorizing of paranormal behavior as necessarily "pararational," a romantic mystification unfit for serious political concern (see Watson's articles in SFS #5 and in the present issue).
[4] Ursula K. Le Guin, "National Book Award Acceptance Speech," Algol, Nov 1973, p. 14.
[5] Ursula K. Le Guin, "On Norman Spinrad's The Iron Dream," SFS 1(1973):43.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

