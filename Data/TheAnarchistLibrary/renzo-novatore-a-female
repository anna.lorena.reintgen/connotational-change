
“I love you most of all, when the joy flees from your oppressed brow; when your heart drowns itself in horror, when the horrible cloud of the past extends over your present.”

— Charles Baudelaire

I am a strange, cursed poet.

Everything that is abnormal and perverse has a morbid allure for me.

My spirit — a venomous butterfly with divine features — is attracted to the sinful scents that waft out from the flowers of evil.

Today I sing of the perverse beauty of a “female” — of one of our females that I have never possessed and will never possess...

Now she wanders, nameless, forgotten and ignored, through the twisted paths of life, with such a deep, dark sorrow locked inside her heart that it raises her above Women and makes her divine.

This great flower of evil — contaminated and contaminating — holds so much human purity within itself that it sublimates a life, making it divine.

* * *

Female?

Yes; perhaps!

* * *

A strange tale circulates around her name. It says: Her beautiful and wicked body languished in the arms of vagabonds and thieves, late night revelers and poets, rebels and heroes...

All the monsters of the night knew the voluptuous secrets of her pale flesh...

All those thirsty for love drank from her lips...

But wherever she passed, she left broken hearts and bleeding minds, weeping flesh and spirits in revolt...

For she — this madwoman — was — like Zarathustra’s poem — a dionysian Harp of voluptuousness for everyone and for no one.

While her wicked and trembling body lay wrapped in voluptuous spasms on the bed of love, swept away in the great chasms of devotion, her restless, vagabond, rebel spirit wandered through the endless regions of the infinite to give body to an intangible, ethereal dream. Her mind, sick from solitude and distance, never let itself be swept away by the spasmodic fever of her insatiable flesh...

She loved only herself...

* * *

One of those who held the fragrant, perverse body of this pale “Female” in his arms cast into her — unfortunately fertile — womb the fatal seeds of another most unhappy life. Under the imperious commandment of Nature, the “Female” became Mother. And society, which had been unjust, vindictive and cruel to the Female, was also against the Mother and even the child. Alone and powerless — he was thrown into the overwhelming storm of life, prey to the saddest loneliness that comes from misery and desperation.

The mother, alone, mocked, persecuted, cursed, scorned. He, sad and melancholy, was a premature victim in his turn.

* * *

I focus my eyes on the mysterious dawn of this strange Female mind, so that I can gather its dispersed ruin and reconstruct its secret.

I know that beneath the dionysian playfulness of these perverse and dissolute creatures, a fine thread of mysterious melancholy almost always runs...

Through my reconstructive poetic imagination, I again see the adolescent virgin when the hot, perverse sun of voluptuousness and pleasure first plunged like a golden blade into her flesh that throbbed with desire, making the irresistible cry of exuberant youth thunder in her mind: love, love, love!

It may have been a mild, fair dawn; it may have been a red twilight.

She gave herself to the first loving embrace, and from that day, her body was a Harp of voluptuousness, a poem of pleasure, seized by pagan fire; a hymn of intoxication sung beyond good and evil, where free spirits celebrate the iconoclastic rite to the joy of human life.

But beneath the dionysian playfulness of this perverse and dissolute creature a fine thread of mysterious melancholy ran.

One day — perhaps one of those sad days when the stars, by means of their occult, magnetic forces, forewarn a being of the dark fatality of his destiny — on a path swarming with people in a large, noisy city, three or four pistol shots rang out.

A pale youth reached the horrendous peak of the most tragic desperation, before falling, exhausted and defeated, into the mud on the path. He wanted to make an unfeeling humanity that ignores everything hear the dark thunder of his protest.

A sad and tragic thing.

Together with a member of shameful humanity, a comrade in vengeance falls.

Who was the pale youth who transformed his slender, lily-white hand into and avenger’s claw?

The son of the rebel Female, of the uninhibited one!

* * *

At the tragic announcement, the perverse Female bent over like a melancholy weeping willow under the raging hurricane, and was purified in the great sorrow of the Mother who was mortally wounded in the most intimate and secret of all her emotions! The voluptuous flower of evil cleanses its soul, perhaps impure but beautiful, in the divine and blessed dew of weeping, and becomes a lilac-flower of pure and uncontaminated beauty.

That unfeeling mind of hers, which no one had ever fully possessed, was reserved to gather the great sorrow that the son of her own belly had to bring her in order to avenge her while avenging himself.

* * *

The dissolute and playful “Female” is now the lonely, nameless Mother, locked in the circle of her sorrow, silent and tragic like an impenetrable sphinx who walks the polluted paths of life, maybe pardoning, maybe cursing...

The raging Anarchy of her free instinct has merged with the delicate sensibility of her new maternal emotion, and from the condensation of these two deeply human elements a spirituality must now shine that is so enchanting that it radiates utterly unknown constellations of human sorrow.

I open my mouth wide toward the unknown and loudly call to this Female-mother, greeting her with the name of Sister!

“Woman”?

What does she matter to me?

This Female now lives beyond her: on a higher peak!
I love the dissolute and playful creatures beneath whose dionysian paganism a fine thread of mysterious melancholy runs; and I love them best when the horrible cloud of their past extends itself over their present...




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“I love you most of all, when the joy flees from your oppressed brow; when your heart drowns itself in horror, when the horrible cloud of the past extends over your present.”


— Charles Baudelaire

