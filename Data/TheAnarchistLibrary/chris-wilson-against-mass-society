
Many people desire an existence free of coercive authority, where all are at liberty to shape their own lives as they choose for the sake of their own personal needs, values, and desires. For such freedom to be possible, no individual person can extend his or her sphere of control upon the lives of others without their choosing. Many who challenge oppression in the modern world strive toward their conception of a “free society” by attempting to merely reform the most powerful and coercive institutions of today, or to replace them with “directly democratic” governments, community-controlled municipalities, worker-owned industrial federations, etc. Those who prioritize the values of personal autonomy or wild existence have reason to oppose and reject all large-scale organizations and societies on the grounds that they necessitate imperialism, slavery and hierarchy, regardless of the purposes they may be designed for.

Humans are naturally sociable, but are selective about who they wish to associate with. For companionship and mutual support, people naturally develop relationships with those they share an affinity with. However, only in recent times have people organized themselves in large-scale groupings composed of strangers who share little of relevance in common with each other. For over 99% of human history, humans lived within small and egalitarian extended family arrangements, while drawing their subsistence directly from the land. The foraging bands and shifting horticultural communities of past and present are known to have enjoyed extensive leisure time, and have rarely required more than 2–4 hours daily on average to satisfy subsistence needs. Famine and war are extremely rare in these societies. Additionally, physical health, dental quality and the average lifespan of small-scale communities are markedly higher than that of agricultural and early industrial societies. If leaders exist, they are usually temporary, and hold no power beyond their ability to persuade. While hunting/gathering and slash-and-burn gardening do indeed alter local environments and are sometimes wasteful, they have proven themselves to be ecologically stable adaptations. Foraging served humanity for 3 million years, while horticulture has been relied upon by many societies in the Amazon basin for approximately 9,000 years. The small-scale cultures that remain today generally prefer their traditional way of life, and many are currently waging impressive political resistance against corporations and governments who wish to forcibly assimilate them so that their land and labor may be exploited. People rarely enter mass organizations without being coerced, as they lead to a decline of freedom and health.

The rise of civilization was made possible through compulsory mass production. When certain societies began to prioritize agricultural productivity as their highest value, they began to forcibly subject all life within reach of their cities to that purpose. Communities of people who wished to forage or garden on the land for subsistence would be mercilessly slaughtered or enslaved, and the ecosystems they inhabited would be converted to farmland to feed the cities. Those engaged in the full-time facilitation of crop and animal production would reside in the nearby countryside, while public officials, merchants, engineers, military personnel, servants, and prisoners would inhabit the cities. The task of creating a surplus to feed a growing specialist class caused the duties of the food producers to intensify, while simultaneously creating the need for more land, both for agriculture and for the extraction of materials for construction and fuel. Humans were forced into servitude for the benefit of their culture’s institutions of production as a prerequisite for continued survival, and non-human life was either harnessed or eliminated for the sake of completing human projects. To occupy land, one would be mandated to continuously pay tribute in the form of a tax or tithe (or and more recently, in the form of rent or mortgage), hence requiring one to devote most of one’s time and energy to a politically accepted mode of employment. Upon being required to satisfy the demands of landholders or employers in exchange for personal space and commodities, it becomes impossible for people to make their living through subsistence hunting or gardening. Although small-scale self-sufficient communities would resist or flee the intrusion of military and commercial forces, those that failed would be assimilated. Subsequently, they would quickly forget their cultural practices, causing them to become dependent upon their oppressors for survival.

Capitalism is civilization’s current dominant manifestation. The capitalist economy is controlled mainly by state-chartered corporations; these organizations are owned by stockholders who are free to make business decisions without being held personally accountable for the consequences. Legally, corporations enjoy the status of individuals, and thus an injured party can only target the assets of the company in a court case, not the possessions or property of the individual shareholders. Those employed by corporations are legally required to pursue profit above all other possible concerns (e.g., ecological sustainability, worker safety, community health, etc.), and can be fired, sued, or prosecuted if they do otherwise. As a technologically advanced form of civilization, capitalism encroaches upon and utilizes even greater territory, causing further reduction of the space available for life to freely flourish for its own purposes. Like civilization, capitalism conscripts both human and non-human life into servitude if regarded as useful, and disposes of it if regarded as otherwise. Under capitalism, most people spend the majority of each conscious day (typically 8–12 hours) engaged in meaningless, monotonous, regimented, and often physically and mentally injurious labor to obtain basic necessities. Privileged individuals also tend to work intensively and extensively, but typically to respond to social pressure or to satisfy an addiction to commodified goods and services. Because of the dullness, alienation, and disempowerment that characterizes the average daily experience, our culture exhibits high rates of depression, mental illness, suicide, drug addiction, and dysfunctional and abusive relationships, along with numerous vicarious modes of existence (e.g., through television, movies, pornography, video games, etc).

Civilization, not capitalism per se, was the genesis of systemic authoritarianism, compulsory servitude and social isolation. Hence, an attack upon capitalism that fails to target civilization can never abolish the institutionalized coercion that fuels society. To attempt to collectivize industry for the purpose of democratizing it is to fail to recognize that all large-scale organizations adopt a direction and form that is independent of its members’ intentions. If an association is too large for a face-to-face relationship between members to be possible, it becomes necessary to delegate decision-making responsibilities to representatives and specialists in order to achieve the organization’s goals. Even if delegates are elected by consensus or by majority vote, the group’s members cannot supervise every action of the delegates unless the organization is small enough for everybody to monitor each other on a regular basis. Delegated leaders or specialists cannot be held accountable to mandates, nor can they be recalled for irresponsible or coercive behavior, unless held subject to frequent supervision by a broad cross-section of the group. Such is impossible in an economy based upon a highly stratified division of labor where no given individual can focus upon or even view the actions of the rest. Additionally, elected delegates are allotted more time and resources to prepare and present a case for their objectives, and are thus more likely to gain further power through deception and manipulation. Even if the group at large determines all policies and procedures (which is itself impossible when specialized knowledge is required), and delegates are only assigned the duties of enforcing them, they will still act independently when they disagree with the rules and are confident that they can escape punishment for ignoring them. Democracy is necessarily representative, not direct, when practiced on a large scale — it is incapable of creating organization without hierarchy and control.

Because mass organizations must increase production to maintain their existence and to expand, they tend to imperialistically extend their scope of influence. Because cities and industries rely upon outside inputs, they aim to seize the surrounding areas for agricultural and industrial use, rendering it inhospitable to both non-human ecosystems and self-sufficient human communities. This area will expand in relation to any increase in population or specialization of labor that the city experiences. One could argue that industrial production could be maintained and yet scaled down, leaving ecosystems and non-industrial peoples some room to co-exist. Firstly, this proposal invites the question of why civilization should determine its own boundaries, instead of the victims of its predation. Secondly, there are no historical examples of production economies that do not expand, mainly because they must expand after depleting the resources available to them at any given time.

The structural complexity and hierarchy of civilization must be refused, along with the political and ecological imperialism that it propagates across the globe. Hierarchical institutions, territorial expansion, and the mechanization of life are all required for the administration and process of mass production to occur. Only small communities of self-sufficient individuals can coexist with other beings, human or not, without imposing their authority upon them.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

