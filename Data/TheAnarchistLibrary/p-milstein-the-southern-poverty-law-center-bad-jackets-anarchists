
“Bad-jacketing” is a term used to describe the misrepresentation of a group as something it is not; presumably the false description is bad and discrediting in some way. Bad-jacketing is similar to “red-baiting,” where progressives are sometimes labeled as “communists.”

Are anarchists now the allies and recruits for the latest trend in white neo-fascism? That is the amazing Big Lie being put out by the Southern Poverty Law Center (SPLC), on its popular civil rights website, in the Winter 2000 issue of their “Klanwatch” Intelligence Report. This attack on revolutionary anarchism is more than a simple mistake, more even than just pro-government propaganda that needs to be shot down. It’s a slipping of the mask, exposing the contradiction of pro-capitalist “anti-racism” in America.

This dramatic SPLC report, “Neither Left nor Right,” is nominally about third position neo-fascism (the tendency that advocates both anti-capitalism and racism, first made prominent by the Strasser wing of the 1920-30s German Nazi Party). But the SPLC surprisingly uses the events of the Anti-WTO Battle of Seattle as its starting point and focus — and targets the anarchist Black Bloc. The report says right at the start: “As the streets of Seattle exploded into 1960s-style violence last November, stunned Americans were told that ‘anarchists’ and other left-wingers were leading a huge riot aimed at protesting capitalist globalism ...And these reports were true — but only to a point.

“Right alongside the ‘progressive’ groups that demonstrated in Seattle — mostly peaceful defenders of labor, the environment, animal rights and similar causes — were the hard-edged soldiers of neo-fascism. They carried signs decrying ‘The New World Order Agenda,’ bitterly denounced ‘Jewish media plus big capital’ and in at least one case, fought it out with black youths amidst the tear gas. The ‘Battle of Seattle’ brought erstwhile antagonists together to face a common enemy in the streets.

“What was behind this truly remarkable mix? How was it that members of the far ‘left’ and ‘right’ found themselves facing down police together? ...”

And at the conclusion it comes back to this theme: “Still, it seems clear that the hard right will draw increasingly from the ranks of its former enemies.”

What is so damaging about these lies, of course, is that the Southern Poverty Law Center — recently in the headlines for its winning civil suit against the Aryan Nations in Idaho — is probably the leading mainstream source for information on white racist and far right organizations. Continually quoted in the media, the SPLC is widely used by schools and those concerned about racism.

If the Seattle PD or the f.b.i. had tried such a crude smear — like, “Many of those black-masked anarchists who attacked corporations at Seattle were really from the Ku Klux Klan!” — everyone would have laughed it off. But the Southern Poverty Law Center slipping a “bad-jacket” on anarchists in the name of anti-racism is much more disarming.

The truth here is not in doubt. “Neither Left nor Right” is conveniently vague throughout about numbers, about how large the neo-fascist presence was at Seattle. But its dramatic language would mislead an innocent reader to assume some significant far right presence in the street clashes. Of the hundreds in the anarchist Black Bloc who trashed Starbucks and Niketown, the provable number of neo-fascists was exactly zero. Of the roughly 20,000 Anti-WTO protest marchers, the few neo-fascists were certainly less than the number of various police, f.b.i., and military infiltrators and observers. The white neo-fascists could probably have fit into a van. They did not play any role on the historic events at Seattle (except for buying postcards, getting themselves photographed, and gazing at the action with envy).

It isn’t just the Black Bloc and its supporters who have confirmed this. We can refer to Chip Berlet, a well-known progressive researcher and author on repression issues. Berlet has been a consultant for US government agencies and has defended the SPLC’s contribution to anti-racist education (we mention this to emphasize that Berlet is neither an “extremist” nor someone with a bias against the SPLC). Yet on the accuracy of this report, Berlet wrote in an upcoming book he has co-authored that only “a handful of rightwing activists, including some third position neo-nazis and fans of David Icke, took part in the large and dramatic Seattle protests against globalization...” When asked for clarification on what he meant by “a handful,” Berlet answered: “Several Icke supporters were openly holding signs promoting Icke and handing out pamphlets. There are photographs of this, including one in the SPLC report. The reports about the Buchananites, other Patriot types, and the Third Positionists are from people who were there and know the distinctions. Apparently a few dozen at best.”

Less noticed, the SPLC report has another pattern: it follows the government and police script about the Battle of Seattle. What we find isn’t one error of fact but an entire scenario of lies and pro-law enforcement propaganda.

The anarchist Black Bloc, which startled America with up-front anti-capitalism, is conveniently smeared as haters who join with the “hard-edged soldiers of neo-fascism.”

The considerable street violence at Seattle, with tear gas and “rubber bullet-firing police,” is implied to be between the police and violently attacking anarchists and neo-fascists. This, too, is pure fiction. As we know, the mass street violence was initiated by the police and aimed at the thousands of “peaceful” protesters blocking the downtown streets with CD. The police shot at, chased for blocks, and beat up these nonviolent protesters, filling whole neighborhoods with tear gas.

In the SPLC’s concocted scenario, the real racism in Seattle is covered up. There was no fighting between anarchists (and neo-fascists) and Black youths, as the SPLC report hints. Fighting with Black youths was done by the self-appointed “peace police,” the liberal white protest monitors who tried to physically stop Black youth from liberating capitalist property — and even tried to get the police to arrest them.

The major white racist move at Seattle — the prominent appearance of far right presidential candidate Pat Buchanan as a speaker at the main protest rally, as arranged by the AFL-CIO and the Ralph Nader lobby — isn’t even mentioned by the report. It’s the most “respectable” pro-capitalist elements in the anti-globalization struggle that have been knowingly allying with white racism — not anarchists. So the reader isn’t so much getting important intelligence about racists but rather is being misdirected in an actual cover-up of racism.

Ironically, the far right itself is using this SPLC report. At least one neo-fascist group has put the entire SPLC report up on its website. This is a curious unity, but we can understand it. The far right has been trying to recruit white youth with boasts that it has been on the front lines against corporate Globalization. Now, like a gift dropping into their laps, the Southern Poverty Law Center has endorsed these neo-fascist lies in this report on Seattle.

What kind of “anti-racist” organization would do all this? The Southern Poverty Law Center itself and its founder-director, Morris Dees, while a public relations and fund-raising powerhouse, has for years been viewed by many activists with misgivings. This was especially true after John Egerton’s expose in Progressive magazine took the lid off “Poverty Palace.” It appears that SPLC is the model of capitalist “anti-racism” — that is, it functions like a business with government connections using anti-racism as a commodity. And far from being a critic of the racist system, it is a small gear in the machinery of capitalist hegemony and institutional repression.

Morris Dees was a white segregationist businessman in Alabama during the civil rights battles of the 1960s, which he didn’t support (although, like many millions of other white people, he afterwards claimed that he was secretly sympathetic to Black people all along). He first became a millionaire as a genius at direct mail marketing, although in at least one instance he worked for the Klan as an attorney defending white men who had attacked civil rights demonstrators. According to Dees, in 1967 at an airport he received an epiphany, and decided to dedicate his life to social justice. He starting doing civil rights law. After he profitably cashed out of his business, in the early 1970s he turned to liberal politics and founded SPLC.

The one unique thing is that his role in liberal politics continued to be very profitable direct marketing. Dees ran nationwide mail solicitations for the McGovern, Carter, Kennedy and Hart presidential campaigns (he was national finance director, for example, of the successful Carter campaign). Out of these he amassed a Northern white liberal contributors list of hundreds of thousands, which his Center has used to raise millions of dollars every year. Along the way the SPLC has taken on worthy civil rights lawsuits, of course, but the occasional high profile lawsuits seem to exist mostly to be sold in the multi-million dollar fundraising solicitations that each year result in surpluses that the SPLC invests in its Wall Street accounts.

They have also become a national information resource for many police departments and government agencies — and now a propaganda resource as well.

Randall Williams, who set up the “Klanwatch” project for the SPLC, said after he resigned: “We were sharing information with the FBI, the police, undercover agents. Instead of defending clients and victims, we were more of a super snoop outfit, an arm of law enforcement... but Morris was still writing donors about the Klan menace, and the money was still flowing in.”

The Southern Poverty Law Center has quietly gone about filling a special role in government monitoring. As a private organization wrapped in the banner of Civil Rights, it carries out political surveillance that current u.s. law forbids the f.b.i., a.t.f. and other police agencies from doing. For example, the SPLC records license plate numbers of vehicles near extremist demonstrations, then runs them through state DMV records to compile names and addresses of participants. Extensive computer files with thousands of photographs are kept on those with extremist political views, using not only monitoring of news sources and political journals but paid informers and tips from police. All this political information is then available to government agencies and corporations.

Pro-government propaganda “bad-jacketing” anarchists just fits in with the SPLC’s role as an informal “arm of law enforcement.” The question that immediately comes up is what else has the SPLC been distorting or covering up?

Nor are they the only ones at this dirty work: after Seattle, the Anti Defamation League of B’nai Brith, another major source of mainstream information about racism, added the anarchist circled “A” symbol to their “Hate on Display” internet webpage along with the nazi swastika and other “Extremist Symbols.” After anarchists protested, the ADL changed their webpage to admit (buried in the text) that “the majority of people who identify with this movement consider themselves non-racist or anti-racist,” but still kept the symbol up and the description right underneath it — “General Racist Symbol.”

The ADL itself has a stunning history of covert white supremacy and general sleaziness, but that would be a whole further story in itself.

As the hegemonic culture, capitalism keeps replicating itself over and over. It keeps assaulting, co-opting, counterfeiting, and doing leveraged buyouts of new threatening developments — like today’s anti-racism. Which is why anti-racism can’t just be against the most obvious racist crimes, but has to be part of creating an oppositional culture on the deepest level.

Sidebar: Ottawa Citizen April 23, 2001 — Warren Kinsella (former campaign adviser to Prime Minister Jean Chritien) “... But a more salient question, unaddressed in the avalanche of media coverage surrounding Quebec City, is this: how is it that the extremists found on both sides of the ideological continuum — far-left and far-right — have joined forces to oppose liberalized trading rules? ... Either way, the far right and far left have come together to oppose international co-operation in trade. And, increasingly, they do so violently, or by making use of rhetoric that legitimizes violence ... I suspect that if it has succeeded in unifying so many lunatics, left and right, globalization can’t be such a bad thing after all.”




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



The anarchist Black Bloc, which startled America with up-front anti-capitalism, is conveniently smeared as haters who join with the “hard-edged soldiers of neo-fascism.”



The considerable street violence at Seattle, with tear gas and “rubber bullet-firing police,” is implied to be between the police and violently attacking anarchists and neo-fascists. This, too, is pure fiction. As we know, the mass street violence was initiated by the police and aimed at the thousands of “peaceful” protesters blocking the downtown streets with CD. The police shot at, chased for blocks, and beat up these nonviolent protesters, filling whole neighborhoods with tear gas.



In the SPLC’s concocted scenario, the real racism in Seattle is covered up. There was no fighting between anarchists (and neo-fascists) and Black youths, as the SPLC report hints. Fighting with Black youths was done by the self-appointed “peace police,” the liberal white protest monitors who tried to physically stop Black youth from liberating capitalist property — and even tried to get the police to arrest them.



“Right alongside the ‘progressive’ groups that demonstrated in Seattle — mostly peaceful defenders of labor, the environment, animal rights and similar causes — were the hard-edged soldiers of neo-fascism. They carried signs decrying ‘The New World Order Agenda,’ bitterly denounced ‘Jewish media plus big capital’ and in at least one case, fought it out with black youths amidst the tear gas. The ‘Battle of Seattle’ brought erstwhile antagonists together to face a common enemy in the streets.


“What was behind this truly remarkable mix? How was it that members of the far ‘left’ and ‘right’ found themselves facing down police together? ...”

