      Communitarianism      Institutions and Constitutions      Should Cooperative Work Precede Political Work?      Education for Citizenship      Power and Polity      Dual Power      “The streets will organize you!”      A Vanguard Organization      Vacancy on the Left      Class Society      Confederation and Autonomy      Economics and Technology
Age, chronic illnesses, and the summer heat oblige me to remain at home—hence I am very sorry that I cannot participate in your conference on libertarian municipalism. I would like, however—thanks to Janet Biehl, who will read these remarks—to welcome you to Vermont and to wish you well during the course of your discussions over the next three days.

Some issues have recently arisen in discussions of libertarian municipalism, and I would like to offer my views on them. One of the most important involves the distinction that should be drawn between libertarian municipalism and communitarianism, a distinction that is often lost in discussions of politics.

By communitarianism, I refer to movements and ideologies that seek to transform society by creating so-called alternative economic and living situations such as food cooperatives, health centers, schools, printing workshops, community centers, neighborhood farms, “squats,” unconventional lifestyles, and the like. Allowing for the works of Pierre-Joseph Proudhon, the notable spokespersons of communitarianism have been Martin Buber, Harry Boyte, and Colin Ward, among many others. The word communitarian is often interchangeable with the word cooperative, a form of production and exchange that is attractive because the work is not only amiably collective but worker-controlled or worker-managed.

At most, communitarianism seeks to gently edge social development away from privately owned enterprises—banks, corporations, supermarkets, factories, and industrial systems of agriculture—and the lifeways to which they give rise, into collectively owned enterprises and values. It does not seek to create a power center that will overthrow capitalism; it seeks rather to outbid it, outprice it, or outlast it, often by presenting a moral obstacle to the greed and evil that many find in a bourgeois economy. It is not a politics but a practice, whose constituency is often a relatively small group of people who choose to buy from or work in a particular cooperative enterprise.

Citing Proudhon as one of the fathers of communitarianism dates the inception of this ideology and practice back about 150 years, to an age when most workers were craftspersons and most food cultivators were peasants. During the intervening years, many cooperatives have been formed with the most far-reaching hopes and idealistic intentions—only to fail, stagnate, or turn into profit-oriented enterprises. In order to survive in the capitalist marketplace and withstand the competition of larger, more predatory, profit-oriented enterprises, they have normally been obliged to adapt to it.

Where cooperatives have been able to maintain themselves against capitalist competition, they tend to become introverted, basically centered on their internal problems and collective interests; and to the extent that they link together, they do so in order to focus on ways and means to stay alive or expand as enterprises. Above all, they rarely, if ever, become centers of popular power—partly because they are not concerned with addressing issues of power as such, and partly too because they have no way of mobilizing people around visions of how society should be controlled.

While working and/or living in cooperatives may be desirable in order to imbue individuals with collectivist values and concerns, they do not provide the institutional means for acquiring collective power. Underpinning their social ideas—before these ideas fade into dim memory—is the hope that they can somehow elbow capitalism out, without having to confront capitalist enterprises and the capitalist state. Time tends to increase these parochial tendencies, making cooperatives more introverted, more parochial, more like collective capitalists than social collectivists, and ultimately more capitalistic than socialistic in their practices and interests.

Libertarian municipalism, by contrast, is decidedly a confrontational form of face-to-face democratic, antistatist politics. Looking outward to the entire municipality and beyond, it is decidedly concerned with the all-important question of power, and it poses the questions: Where shall power exist? By what part of society shall it be exercised?

Above all, it asks, what institutions can make the exercise of nonstatist power possible and effective? I once read a Spanish anarchist slogan that declared: “Make war on institutions, not on people.” I find this slogan disturbing because it implies that ideally people can somehow become “autonomous” from institutional obligations, and that institutions as such are straitjackets that prevent them from discovering their “true selves” and engaging in self-determination. No—this is grossly fallacious. Animals, to be sure, can live without institutions (often because their behavior is imprinted in them genetically), but human beings require institutions, however simple or complex, to mold their societies. In a free society, these institutions would be rationally constituted “forms of freedom” (as I called them back in the 1960s) by which people would organize and express their own powers collectively as well as personally.

Moreover, such a free society would have a constitution and laws, formulated and adopted by directly democratic and discursive assemblies. In the mid-19th century, while he was a member of the French Chamber of Deputies, Proudhon refused to vote in favor of a draft Constitution that was oriented toward the protection of property and the construction of a State. While I approve of his negative vote, I thoroughly reject the reasons he gave for it. “No!” he declared, “I did not vote against the Constitution because it was good or bad, but because it was a Constitution.” This frivolous behavior reduced him, intellectually as well as politically, to the world of arbitrary power, against which oppressed Greek peasants such as Hesiod had cried out in the eighth century BCE, denouncing the “barons” who had all but enserfed and exploited the Hellenic peasantry of the ancient world and demanding a society based on laws, not on the whims of men.

Contrary to Proudhon and other anarchist theorists who have rejected laws as such, constitutions and laws have long been demands of oppressed people as instrumentalities for controlling, indeed eliminating, the arbitrary power exercised by kings, tyrants, nobles, and dictators. To ignore this historic fact and fall back on an “instinct for mutual aid” as the basis for social organization, or “an instinct for revolution,” or “an instinct for sharing” is to retreat from a much-desired civilized world into the realm of animality, a social zoology that has no application to humanity as a potentially innovative species that makes and remakes both itself and the world.

Some libertarian municipalists have argued that before we seek political power for our democratic ends, we must first “work over” a community by participating in communitarian activities and establishing cooperatives that will cement mutualistic ways of living throughout the community. Only then, we are told, will a community be “ready” for a libertarian municipalist effort. But do cooperatives really have mutualistic effects on their communities?

Not necessarily—indeed, all too often, for those involved, forming and maintaining a cooperative becomes an end in itself. When cooperatives do manage to survive, their relations with other cooperatives become strained—far from treating each other mutualistically, they turn their faces against each other and even enter into mutual competition. Moreover, a cooperative’s members often become an in-group in the very community they had initially set out to educate—and they abdicate all educational activities, having come to view the people in their community solely as mere customers. Forced by capitalism to adopt methods of capitalist organization, they hire managers and business consultants of one kind or another—presumably in pursuit of efficiency—with the result that, far from giving their community a political education, they deceive it in their own interests, dressing up their capitalist enterprise with the “virtuous” name cooperative instead of openly calling themselves a company or corporation.

Libertarian municipalism tries in every way to avoid losing its identity in the job of building, maintaining, and expanding cooperatives—and thereby sinking into a communitarian morass. Rather, it seeks to recover and to go beyond Aristotle’s definition of “man” as a zoon politikon, a “political being.” In Aristotle’s Politics, “man,” or at least Greeks, are meant to live in a polis (usually mistranslated as “city state”) or a municipality. For Aristotle, this is one form of our actualization and fulfillment as human beings. To use a religious term, human beings, insofar as they realize their humanity, are destined to be polis– and city-dwellers. Our teloi, which include a rationally and democratically constituted system of laws—of duties as well as rights—include as well this ability to be citizens, that is to say, to be educated in order to be competent to assume all the obligations of self-government.

They must be capable intellectually as well as physically of performing all the necessary functions in their community that today are undertaken by the State—that apparatus of soldiers, police, bureaucrats, legislative representatives and the like. The State justifies its existence in great part not only on the indifference of its constituents to public affairs but also—and significantly—on the alleged inability of its constituents to manage public affairs. It claims to have a unique competence, while considering its constituents to be incompetent children who need competent “parents” to manage their affairs. Once citizens are capable of self-management, however, the State can be liquidated both institutionally and subjectively, replaced by free and educated citizens in popular assemblies.

If citizens are to be competent to replace the State, then education for citizenship, or paideia, must be rigorous and involve the building of character and ethical integrity as well as gaining knowledge. This is even more the case when it comes to eliminating hierarchy. Rigorous education and training, in turn, involve a systematic, carefully planned, organized learning process. Citizens cannot be produced if the education and training of the young occur in contexts where the student—usually an inchoate self that has not yet been formed—is called upon to “let everything hang out” in the name of “self-expression.” It is precisely this concern for paideia that made Greek political philosophy so great: it included educational ideas for the making of competent citizens, who would not only think systematically but learn to use weapons in their own defense and in defense of the democracy. The Athenian democracy, let me note, was established when the aristocratic cavalry was replaced by the hoplite footsoldier—the civic guard of the fifth century BCE, which guaranteed the supremacy of the people over the formerly supreme nobility.

In contrast to communitarianism, libertarian municipalism is concerned with the problem of power, especially how ordinary people can acquire it. By power, I do not refer to the psychological feeling of empowerment that one may gain from attending an inspiring meeting or rally. Some fashionable forms of “self-empowerment” are often little more than emotional highs that could more or less be acquired by taking drugs Rather, I mean the tangible power embodied in organized forms of freedom that are rationally conceived and democratically constituted. In contrast to those who would simply use the demand for power as a means to make propaganda and theater, or who would refuse to accept power, even if offered, if they could potentially use it to empower the people in popular assemblies, libertarian municipalism seeks to attain collective, communal power.

A libertarian municipalist polity would thus be a constituted community—one that has rationally and democratically created its own constitution and laws; whose citizens have been fashioned ethically and intellectually by the character-building process of paideia; and which, because of its competence, armed power, democratic institutions, and discursive approach to issues and problems can not only replace the State but perform the socially necessary roles in the community formerly taken over by the State.

This is the political realm, the authentic world of politics, in which we are obliged to form a movement to recover and develop before it is effaced entirely by a Disneyland world. To dissolve this political realm into communitarian institutions and activities is to overlook the very need to reestablish this realm, indeed to play the reactionary role of diffusing it into an night where all is black and indistinguishable.

The issue of dual power should also be clarified, as this phrase has recently been gaining currency in libertarian circles as a “theory.” The Marxists, more specifically Trotsky, had no “theory” of dual power. The notion of a “dual power” was well rooted in Russian socialist politics long before Trotsky devoted a chapter to the concept in his History of the Russian Revolution, a chapter that occupies a mere nine pages, most of which are descriptive. The word dvoevlasty (“dual power”) was used by Russian revolutionaries of all kinds as early as February 1917, simply to describe the dual arrangement in which the Petrograd Soviet and the Provisional Government tried to govern Russia—an arrangement that had come to an end by the October Revolution.

As a “theory,” however, “dual power” was more popular in Germany and Austria immediately after the First World War, in 1918-19, when the Raete or councils were in vogue among theorists such as Rudolf Hilferding, Karl Kautsky, and Victor Adler. These Austro-German Marxists thought of dual power as a permanent condition consisting of permanent councils, through which workers could express their interests, together with parliamentary state, through which the bourgeoisie could express its interests. These Social Democrats divested “dual power” of its revolutionary tension, and the term became a synonym for a two-part government that could conceivably have existed indefinitely.

In libertarian municipalism, dual power is meant to be a strategy for creating precisely those libertarian institutions of directly democratic assemblies that would oppose and replace the State. It intends to create a situation in which the two powers—the municipal confederations and the nation-state—cannot coexist, and one must sooner or later displace the other. Moreover, it is a confluence of the means to achieve a rational society with the structure of that society, once it is achieved. The diremption between means and ends is a problem that has always plagued the revolutionary movement, but the concept of dual power as a means to a revolutionary end and the formation of a rational society overcomes the chasm between the method for gaining a new society and the institutions that would structure it.

A very important problem in libertarian municipalism is the question of what kind of movement can play the educational and, yes, leadership role required to produce these transformations. Those who denounce libertarian municipalism as “statist” often favor instead, not only creating cooperatives, but engaging in episodic actions, especially in the form of demonstrations and street festivals. Even worse, some of them prefer to engage in passing attacks on “authority” by breaking windows or taunting police—and then go home to watch these escapades on television—as if “liberty” and “autonomy” could be so achieved or inspire the people.

We must at the outset dissociate ourselves from a silly cry that was voiced by I. S. Bleikhman, the supreme personality of the Petrograd Anarchist Communists, in July 1917. In those insurgent “July Days,” the Kronstadt sailors together with the Petrograd garrison and most advanced workers decided to “come out” with arms in hand to establish a soviet government. To their appeal for organization, Bleihkman responded: “The streets will organize you!” The streets, of course, “organized” absolutely nothing and no one—and partly for lack of a real leadership, the July insurrection was crushed in only a few days.

In the course of closely studying the history of past revolutions, the most important problem I have encountered has been precisely the issue of organization. The issue is crucial, not least because in a revolutionary upheaval the nature of organization can spell the difference between life and death. What has become very clear in my own mind is that revolutionaries need to create a very proactive organization—a vanguard, to use a term widely used until the New Left poisoned it by associating it with the Bolsheviks—that itself has its own rigorous paideia; that creates a responsible membership of informed and dedicated citizens; that has a structure and a program; and that creates its own institutions, based on a rational constitution.

Such an organization might well be regarded as a polis-in-the-making that, while building a libertarian municipalist movement, can safeguard its basic principles from cooptation (the usual fate of good ideas these days), nourish their development, and apply them in complex and difficult situations. Without a clearly definable organization, a movement is likely to fall into the tyranny of structurelessness.

I would like to point out that if one’s basic principles are not firm and clear, then one has no basic principles at all. One is simply floating in the air with mere opinions and off-the-cuff notions rather than clear ideas, thought-out views, and substantive theories constructed on solid foundations. One may decide to change one’s basic principles, to be sure, which itself presupposes that one had definable principles to begin with. But the prevalence of undefined and unfixed notions reflects the contemporary postmodern invertebrate mentality that regards everything as relative; that rejects the existence of fundamentals; that fosters formless, amoebic ideas; that condemns structure as authoritarian or even totalitarian; and that regards feelings are more important than careful thought.

Ideas are becoming cheap opinions, and principles are becoming ephemeral slogans, which is all the more reason why we should affirm our ideas and theories clearly. Not only for political reasons but also for cultural ones, it is the responsibility of a libertarian municipal movement based on Communalist principles to maintain the highest standards in its writings, discussions, and activities.

Moreover, politics cannot be reduced to theater. Study and experience have taught me that art does not redeem—and certainly does not produce revolutions. Art is sensitizing, emotionally enriching, and creative—but few schools of art, music, and the performing arts have impelled any appreciable number of people to build barricades, let alone fight behind them. Art may be an adjunct of the revolutionary movement, but it is not an impetus. Hence my fear of popular theatrical efforts to “reclaim” the streets—as though we ever had them!—with street festivals. And then what? Nor can elections be reduced to mere theater or even to strategies for engaging in propaganda, important as this may be. Unless we actually run candidates in city council elections, we are not dealing with power. And to live in fear that power might “corrupt” not only ignores the many cases where it did not corrupt; it ignores the need to gain power. Theater, street events, and other photogenic escapades merely play at politics rather than engage in it.

A libertarian municipalist movement that is created by means of distinct steps, with advanced ideas, education, and experience, has every right to regard itself as a vanguard. Obviously, any other kind of movement organization can make the same claim—no libertarian municipalist organization can deny other organizations the right to call themselves vanguards. But no major social change will ever occur without a well-organized vanguard movement that is structured by a constitution and places clear-cut requirements on the right of people to join it. I for one have had enough of the old Clamshell Alliance-type organizational practices that reduce membership to a revolving door in which people enter and leave the organization after a single meeting—but have full voting rights.

And I have had enough of consensus decision-making, in which a minority has the bizarre right to block the majority’s decisions, and that themselves become an obstructive tyranny while claiming absurdly that majority decision-making is “tyrannical.” I oppose the way movement groups often have used consensus decision-making processes to manipulate the membership. I’m sorry, but the streets will not “organize” us. Only a serious, responsible, and structured movement can do that.

Important as it is to create links between libertarian municipalism and oppositional movements, it would be a grave error to dissolve our movement into theirs or surrendered our identity to them. I have no compunction about declaring that we stand on a higher ground than anything else of which I know that calls itself “oppositional.” Like the word revolutionary, the word opposition has been steadily cheapened and will continue to be devalued. The political spectrum has shifted enormously from left to right—a shift that has affected the ecology movement, feminism, self-styled liberation movements, and the labor movement, as well as the bourgeoisie. .

Everywhere the Right is shifting into the darkness of outright reaction, often with dangerous racist overtones. This shift has created a vacancy in that vast space on the political spectrum where the Left should legitimately be. Without a well-anchored Left, indeed, there is no political spectrum at all—and it is my deepest fear that with the widespread ignorance and rejection of history today is dumbing down virtually all social and political standards. Anarchists affirm the importance of the state; Marxists try to fit their theories into a market economy; reformists sound like conservatives; and conservatives, not to speak of reactionaries, find a home in Telos magazine—while bizarre coalitions try to tailor their semifascistic notions to New Left notions.

Some anarcho-syndicalists and anarcho-communists have recently written that I do not “believe” in the existence of classes—an accusation that is almost too ridiculous to answer. I have no doubt that we live in a class society; in fact, conflicts between classes would doubtless exist in citizens’ assemblies as well. For this reason, libertarian municipalism does not forsake the notion of class struggle but carries it out not only in the factories but also into the civic or municipal arena.

It does so, that is, as long as factories continue to exist and as long as proletarians do not imagine that they are “middle class.” But I learned many years ago, while working in a foundry and in an auto plant owned by General Motors, that workers regard themselves as human beings as well as class beings. They are fathers and mothers, sisters and brothers, and sons and daughters who are deeply concerned with the ordinary problems of life, such as the quality of their neighborhoods, dwellings, sanitary facilities, recreation areas, schools, air, water, and food—in short, all the problems that concern city and rural dwellers quite apart from their class status. These general interests, while they do not supplant class interests, can cut across class lines, especially the lines that divide workers from a vast variety of middle-class people.

Even during my years working in heavy industry, I found it easier to reach workers on the basis of environmental and neighborhood issues than on the basis of factory issues. During the 1960s, the transclass appeal of certain issues became obvious to me, such as in my 1963 fight against the Edison Company’s attempt to build a nuclear reactor in New York City. Workers no less than middle-class people simply overwhelmed me with questions and asked me to come to their community groups and address them. This phenomenon continues today: in November 1999, it was not only workers but middle-class people who marched against globalization and the World Trade Organization in Seattle—a march that consciously or unconsciously was aimed against the very core of modern capitalism. Such transclass issues have been emerging for decades now.

Indeed, capitalism is slowly producing these generalized concerns in all strata of society. The much-desired “general interests” that Marx and socialists as well as anarchists hoped would unite most of humanity as a whole against the bourgeoisie are very much on the horizon. If we do not recognize these general interests and formulate them in a revolutionary way, then I shall go to my grave concluding that existing anarchist scenes, in all their silly mutations, are a complete failure, and that the Marxists have done no better. If we are to face the new century with a theory that keeps pace with—or tries to see beyond—new developments, then we will have to draw on the best we can find in anarchism and Marxism and go beyond them by developing a more comprehensive body of ideas to guide us toward a rational future. For the body of ideas that I would recommend, I have given the name Communalism.

Our ideas of confederation should not remain stuck in anarchist writings of the 19th century. In Proudhon’s writings on federalism, for example, we find an extremely naive vision of a “federation of autonomous communes” whose component members could choose, if they so wished, to pull out of that federation and “go it on their own.” But such “autonomy” is no longer possible, if it was even in Proudhon’s day. A unilateral choice to leave the federation, after all, would undermine the entire federation itself. We no longer live in an artisanal and craft world. Imagine if the electrical complex in upstate New York “autonomously” decided to pull out of a confederation with the Vermont electrical complex because it was piqued by Vermont’s behavior.

Equally troubling would be a confederation based on the kind of “voluntary agreements” that Kropotkin found and even celebrated in the railroad lines—no less!—of his day. If the operating principles of 19th-century railroad lines are a good example of “voluntary agreements,” then I would humbly suggest that those formulated by J.P. Morgan and Co. are priceless. The “anarcho”-capitalists would doubtless exult in this view, presented in Kropotkin’s The Conquest of Bread, but allow me to dissent from it.

A confederation should be regarded as a binding agreement, not one that can be canceled for frivolous “voluntaristic” reasons. A municipality should be able to withdraw from a confederation only after every citizen of the confederation has had the opportunity to thoroughly explore the municipality’s grievances and to decide by a majority vote of the entire confederation that it can withdraw without undermining the entire confederation itself.

Does libertarian municipalism have an economic theory? Yes, I should emphasize, one that is very close to Marx’s critique of capitalism in volume 1 of Capital. Too often, knee-jerk rejections of Marx’s brilliant work routinely bring smiles of approval to the faces of his opponents. I refuse to participate in such routines. However much I disagree with many elements of Marxism, no other single analysis of capitalism even remotely, at this late date, approximates that amazing work.

I do not see how a thoughtful libertarian municipalist theorist can avoid studying and absorbing dialectics, or lack a rich philosophical perspective on History, as distinguished from mere Chronicles. No single theory can encompasses social phenomena that have yet to appear on the existing social horizon, but what can provide us with foresight are basic minimal principles—to which we strongly adhere until they are no longer tenable.

Libertarian municipalism is also based on the proposition that we now have the technology for a post-scarcity economy—one that can potentially abolish mindless toil and possibly most of the work that enters into industrial production today. In such a world, the communist ideal of “from each according to ability, to each according to needs” would be historically and technically feasible. Various fears that individual “needs” might be expanded to accommodate greed can be removed by giving the municipal assembly the right to determine whether certain identified “needs” are excessive and whether their fulfillment could damage the well-being of the entire economy.

The world is changing now at a pace that is absolutely stunning. If capitalism does not destroy the biosphere, then in possibly thirty, certainly fifty years the world that survives will be changed beyond our imagination. Not only will the peasant world be gone, but so too will much of the “nature” we often call “wild.” The automation of industry will probably reach incredible proportions, and the earth’s features will be vastly transformed. Whether these changes will produce an ecological crisis, or whether science and technology can mitigate their impact, however unsatisfactorily, I do not know, nor will I ever know, as I am approaching the end of my own life.

This much, however, I do believe: if a libertarian municipalist movement based on Communalist principles cannot establish a system of direct democracy and confederation, then libertarian ideals of all kinds must be significantly revised. But we cannot hope to establish any kind of truly libertarian society without creating a public sphere, beginning with a grassroots electoral politics based on the creation of popular assemblies. In my view, this is the left libertarian movement’s last stand. If you do not agree with me, so be it—but please, use a different label for your ideas, leave the name “libertarian municipalism” alone, and go your own way toward communitarian and cooperative enterprises, if not Taoist monasteries and mystical seances. I would ask my critics not to muddy up ideas that they don’t really like, while at the same time claiming to support them.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

