        1        2        3        4        5        6        7        8
“Police is basically concerned with society.”

-Police Chief Anthony Batts

“Thus as anonymous victims they find themselves unemployed (often for the young this means these is no possibility for them to enter the global productive process), they feel no compulsion to organize against a specific antagonist. The enemy that has victimized them is not any capitalist in particular but capitalist society as a whole.”

-A Sociologist

The wail of a siren in the midst of the city’s white noise, cars assemble and weave together, buildings open and close in accordance to the logic of the unified time of money, sites of consumption normalize bodies in motion, the city’s perpetual movement, its steady yet differentiated cycles of desire, to fuck, to sleep, to eat, to work. This is the city, a site of the regulated circulation of activity, which reproduces itself socially, economically, politically, bodily, on a daily basis. This is the site of a protracted war.

In countering the city’s endless need to circulate bodies, things and desires, the Oakland Commune came into existence with its own consistency, blocking the dictatorship of capitalist time within the city environment. As a material site, the city is a differentiated series of grids — grids upon grids upon grids; in downtown Oakland, traffic is folded over by the three main streets: San Pablo, Telegraph and Broadway, all enclosed by the continuum of the highway. BART shoots straight through the Downtown and Uptown area, excluding much of the outer regions of Oakland. As a social cite, it is an arrangement of differing tonal compositions — the gentrified region of Uptown Oakland starkly contrasts the destitution on the other side of the block deep into West and East Oakland. The composition of bodies within a zone of activity, distinguish themselves according to distinct logics of consumption, work, leisure, idleness, and so forth. In turn, these logics also create a kind of world, an environment that is not simply material, not simply social, and not simply economic.

But what then is the Oakland Commune? As stated by the Operation Plan by the city’s armed forces (OPD), the presence of the Oakland Commune in Oscar Grant Plaza was deemed to be a contaminated site, overrun by “lawless” bodies. It is not an organization, it is not a space, it is not a movement, it is not the People, it is not the 99%. It is a situation, a situation which suspends the unified unfolding of time of the city, a constellation of individuals and acts set within the horizon of social war, brought together through an ensemble of techniques oriented towards attack against the forces of counter-insurrection.

The following are a few observations and notes, at this moment, from a partisan perspective.

The Commune’s foundation is the social strike. At its most basic level, a strike is the refusal of assumed identities based upon social separations (worker, consumer, gender, sexuality, race, etc.). Contrary to the false separations of the social body, the Commune generates a distinction between bodies, a series of alliances and hostilities that are rooted within the immediacy of struggle. It is not the body of the Other as enemy that is of importance, but the drawing of this distinction itself, the politicization of the fictional and neutralized unity of Society, the People, the 99%, etc. In other words, the separations usually hidden within the unifying concepts of Society or the People are exposed and oriented toward the polarization of the silent unanimity of Public Opinion.

The city functions in order to reproduce particular social and economic roles through the circulation of permitted identities, and through this process shape normative forms of social activity. There are sites of work, sites of shelter, sites of consumption, sites of play, sites of imprisonment, and so forth. For the city to exist, they must all remain separate from one another. Yet all these forms of activity, in their mutual separation, reproduce the city as a whole. Much of this activity reproduces it self itself compulsorily due to the synchronization of unified external time (the clocks at work, the timeliness of the bus, the opening of business, etc.) and the infinite multiplication of internal mechanisms (the fear of not having enough to eat, the fear of not having a place to sleep, the fear of not having enough money, etc.). The police form the last material limit that ensures that this activity reproduces itself, and therefore the city.


A police car responds to a call of “criminal activity” occurring near a BART station. Officers exit the vehicle, while groups of youth emerge from the station, pelting the officers with projectiles. The youth cackle in delight, a laughter of jackals — “Fuck the Police!” they shriek.

The intensity of the Commune’s existence depends not solely on numbers (the quantitative reduction of politics), but instead the way in which it intimately binds together differentiated aspects of the social fabric, ways of eating, ways of sleeping, ways of playing, ways of perceiving, ways of thinking, ways of fighting, etc. Any particular element dis-embodied from the unity of the Commune possesses no substantial antagonistic power on its own.

The countless waves of police repression that have trailed the Oakland Commune reveal the current obstacle that the Commune faces. Each attempt by the Commune to counteract repression is responded to by the City of Oakland and the police through an ensemble of haphazard security measures to prevent the extension and intensification of the Commune — silent snatch and grab arrests of comrades, stay-away orders from Oscar Grant Plaza, the public posting of pictures and addresses of those arrested, ambiguous legal standing in place of any legal sanction, mutual aid between police departments, etc.

Yet the livelihood of the Oakland Commune, at this moment has been based on insistent retribution upon these very acts of repression. With each wave of police repression the resolve of the Commune appears to continually polarize social forces, those who seek social inclusion (the People) and those who are excluded.

These is a difference of strategy and tactics in the reaction to police repression in particular to that of a response to the police in general. How could the police be confronted in general, through an initiative outside the circular logic of repression?


Police cruisers ride down the street on their nightly beat patrol, looking for their next big catch. A spontaneously constructed device, made of various metals, punctures the tires of the police cruiser bringing the car to a halt. Youth swarm out of the alleyways, throwing paint all over the windshield of the car, breaking the windows and shouting devilishly.

The movement has been bisected by the media apparatuses of Order into two halves, the one half which are its productive, ‘authentic’ members and the others, those who have hijacked the movement, the ‘outside agitators,’ the black bloc,’ ‘the criminal element,’ etc. We’ve heard this rhetoric time and time again, of how a movement is overtaken by an illegitimate force. “Let us remember that it is a typical bourgeois cliche to oppose the good ‘common sense’ of the masses to the ‘evil’ of a ‘minority of agitators’, and to pretend to be most favorably disposed towards the exploiteds’ interests.”

We should seek to understand the very real lines that do polarize the Occupy movement, in which this splitting of the movement by the spectacular apparatuses of Public Opinion mirrors and expresses the deep-seated social and class stratifications that penetrates all of society. There have been those who are just beginning to see the organized violence of this society, and those who have experiences it for their entire lives.

The current impossibility of material expropriation — not only of goods, property, infrastructure, but life itself — is contained within the act of property destruction. Its appearance within Public Opinion is deemed “an act of violence,” producing a series of circular and endless debates on the nature of violence versus non-violence. In this sense, the limit of actually expropriating the conditions to create our own lives is met by the symbolic expropriation of violence as a contestation of the sacred meaning of private property. The act of property destruction in itself foreshadows the desire for a series of techniques of expropriation. So we must pose the question: how to begin the project of expropriation, of food, of shelter, of language, of living?

At this moment, there are a series of techniques that point toward potential acts of expropriation — the camp, property destruction building occupations, looting, etc. How can we elaborate and multiply these techniques toward insurrectional situations, or in other words, the total suspension of the city’s reproduction? How can we attain the basic infrastructures for living, and do so against the prevailing logic of the city?


It’s a busy Saturday night at the strip mall. Yuppies walk down the street, adoring their new trinkets, a new watch, a new iPhone, a new pair of shoes…Suddenly, a pack of hooded bodies emerge, going from store to store taking what they want, taking what they need, taking without discrimination. The yuppies look on in horror, the hooded ones laugh joyously.

There are varying degrees of paralyzing the “healthy” body of the city. The Commune forms as a slight interruption, a modification of the city’s cycles of desire. Its existence halts the reproduction of the city as city. While the actions of the Commune may have polarized Public Opinion, the camp itself was the primary site of polarization. Despite the different expressions and forms of activity of the camp throughout the nation, it was the existence and maintenance of the camp itself, and not necessarily the various forms of activity, its “public message” nor principle that made it unbearable to the forces of Order.

What would the extension of the model of the Commune to all of Oakland look like? How could this be made possible? We must find ways of conceiving of the city as a differentiated site for extending our vital life activity, in order to orient our energies against the forces and institutions of counter-insurrection. How can we develop these means? What connections must be made?

Through what means could the city be shutdown permanently?

The decline of a social movement can be ascertained by several aspects;

(1) The mechanical repetition of a tactic that was previously used within the movement’s apex (a generalized moment, explicitly antagonistic to the forces of Order).

(2) The continual re-emergence of the apparatus of ‘identity politics,’ a reactionary force that economizes upon the fracturing of the movement under state repression. Identity politics always tries to portray the dominating Subject of revolt as a distinct identity (white, male, anarchist, etc.), the same mechanism used by the State in order to exclude while and uncontrollable bodies from the movement.

(3) Contestation of the meaning of the Movement, falling into debates about semantics, symbolism and representation.

(4) The resurgence of older forms of activism through “consciousness-raising” projects, building “community,” etc.


A group of hooded youth run through the streets in packs, dispersed throughout the city, running over cars, into shops, through bodies frozen within the city’s rhythms of movement. The city has become a playground, and the various factions of youth, each with their own language, each with their own code of ethics, coalesce together sporadically in order to expropriate the conditions of their existence, to live and fight together decisively.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

