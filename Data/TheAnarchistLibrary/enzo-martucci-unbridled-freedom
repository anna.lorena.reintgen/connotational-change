
Stirner and Nietzsche were undoubtedly right. It is not true that my freedom ends where that of others begins. By nature my freedom has its end where my strength stops. If it disgusts me to attack human beings or even if I consider it to be contrary to my interests to do so, I abstain from conflict. But if, pushed by an instinct, a feeling, or a need, I lash out against my likes and meet no resistance or a weak resistance, I naturally become the dominator, the superman. If instead the others resist vigorously and return blow for blow, then I am forced to stop and come to terms. Unless I judge it appropriate to pay for an immediate satisfaction with my life.

It is useless to speak to people of renunciation, of morality, of duty, of honesty. It is stupid to want to constrain them, in the name of Christ or of humanity, not to step on each other’s toes. Instead one tells each of them: “You are strong. Harden your will. Compensate, by any means, for your deficiencies. Conserve your freedom. Defend it against anyone who wants to oppress you”.

And if every human being would follow this advice, tyranny would become impossible. I will even resist the one who is stronger than me. If I can’t do it by myself, I will seek the aid of my friends. If my might is lacking, I will replace it with cunning. And balance will arise spontaneously from the contrast.

In fact, the only cause of social imbalance is precisely the herd mentality that keeps slaves prone and resigned under the master’s whip.

“Human life is sacred. I cannot suppress it either in the other or in myself. And so I must respect the life of the enemy who oppresses me and brings me an atrocious and continuous pain. I cannot take the life of my poor brother, who is afflicted with a terminal disease that causes him terrible suffering, in order to shorten his torment. I cannot even free myself, through suicide, from an existence that I feel as a burden.”

Why?

“Because,” the christians say, “Life is not our own. It is given to us by god and he alone can take it away from us.”

Okay. But when god gives life to us, it becomes ours. As Thomas Aquinas points out, god’s thought confers being in itself, objective reality, to the one who thinks. Thus, when god thinks of giving life to the human being, and by thinking of it, gives it to him, such life effectively becomes human, that is, an exclusive property of ours. Thus, we can take it away from each other, or anyone can destroy it in herself.

Emile Armand frees the individual from the state but subordinates him more strictly to society. For him, in fact, I cannot revoke the social contract when I want, but must receive the consent of my co-associates in order to release myself from the links of the association. If others don’t grant me such consent, I must remain with them even if this harms or offends me. Or yet, by unilaterally breaking the pact, I expose myself to the retaliation and vengeance of my former comrades. More societarian than this and one dies. But this is a societarianism of the Spartan barracks. What! Am I not my own master? Just because yesterday, under the influence of certain feelings and certain needs, I wanted to associate, today, when I have other feelings and needs and want to get out of the association, I can no longer do so. I must thus remain chained to my desire of yesterday. Because yesterday I desired one way, today I cannot desire another way. But then I am a slave, deprived of spontaneity, dependent on the consent of the associates.

According to Armand, I cannot break relationships because I should care about the sorrow and harm that I will cause the others if I deprive them of my person. But the others don’t care about the sorrow and harm that they cause me by forcing me to remain in their company when I feel like going away. Thus, mutuality is lacking. And if I want to leave the association, I will go when I decide, so much the more if, in making the agreement to associate, I have communicated to the comrades that I will maintain my freedom to break with it at any time. In doing this, one does not deny that some societies might have long lives. But in this case, it is a feeling or an interest sensed by all that maintain the union. Not an ethical precept as Armand would like.

From christians to anarchists (?) all moralists insist that we distinguish between freedom, based on responsibility, and license, based on caprice and instinct. Now it is good to explain. A freedom that, in all of its manifestations, is always controlled, reined in, led by reason, is not freedom. Because it lacks spontaneity. Thence, it lacks life.

What is my aim? To destroy authority, to abolish the state, to establish freedom for everyone to live according to her nature as he sees and desires it. Does this aim frighten you, fine sirs? Well then, I have nothing to do. Like Renzo Novatore, I am beyond the arc.

When no one commands me, I do what I want. I abandon myself to spontaneity or I resist it. I follow instincts or I rein them in with reason, at various times, according to which is stronger within me.

In short, my life is varied and intense precisely because I don’t depend upon any rule.

Moralists of all schools instead claim the opposite. They demand that life always be conformed to a single norm of conduct that makes it monotonous and colorless. They want human beings to always carry out certain actions and to always abstain from all the others.

“You must, in every instance, practice love, forgiveness, renunciation of worldly goods and humility. Otherwise you will be damned”, say the Gospels.

“You must, in each moment, defeat egoism and be unselfish. Otherwise you will remain in absurdity and sorrow,” Kant points out.

“You must always resist instinct and appetite, showing yourself to be balanced, thoughtful and wise on every occasion. If you don’t, we will brand you with the mark of archist infamy and treat you as a tyrant,” Armand passes judgment.

In short, they all want to impose the rule that mutilates life and turns human beings into equal puppets that perpetually think and act in the same way. And this occurs because we are surrounded by priests: priests of the church and priests who oppose it, believing and atheistic Tartuffes. And all claim to catechize us, to lead us, to control us, to bridle us, offering us a prospect of earthly or supernatural punishments and rewards. But it is time for the free human being to rise up: the one who knows how to go against all priests and priestliness, beyond laws and religions, rules and morality. And who knows how to go further beyond. Still further beyond.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



In short, my life is varied and intense precisely because I don’t depend upon any rule.

