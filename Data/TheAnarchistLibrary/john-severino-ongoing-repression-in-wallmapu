        The Quino Case        Lof Yeupeko-Katrileo and the Vilcún fire        Wente Winkul Mapu        Pilmaikén        Temucuicui        Freire Airport        Lof Newen Mapu de Chequenco        Puelmapu        The struggle continues
A continuation of “The Intensification of Independence”.

See the former for a glossary of terms in Spanish and Mapudungun.

Awareness of repression should never be turned into a list of cases and prisoners. Those who struggle must understand repression strategically. If the essence of repression is isolation, this means intentionally formulating our responses to overcome that isolation, both by connecting them to the lines of our ongoing struggle, and analyzing and thwarting the particular mechanisms through which the State seeks to isolate us.

In Wallmapu, that ongoing struggle is a struggle for the land, not as an alienated possession, but as a whole relationship outside of and against capitalism. Mapuche in struggle take over their traditional land, fighting with cops and landlords to do it, and sometimes burning them out; they block highways and sabotage the industries that would exploit their lands; and they farm, graze, and common in those lands, build their houses there, hold their rituals there, raise their children, marry, and bury their dead there, making their relationship with that land a solid fact.

Chilean state repression against the Mapuche demonstrates two distinct modes. One mode operates at a lower intensity, and is less likely to be recognized within the format of the anti-repression list that pretends to confront repression by reacting to its most obvious manifestations. This lower intensity mode manifests in constant surveillance, in raids that brutalize community members, traumatize children, and confiscate tools needed for day to day existence (as nearly every farm tool is a potential weapon). This mode levies psychological exhaustion, producing a negative incentive which the NGOs, development funds, and charity projects that offer a positive incentive away from struggle are always waiting to take advantage of. The Chilean state specifically deploys lower intensity repression to isolate Mapuche communities in struggle, dissuading travel between communities and obstructing those from outside who would visit Mapuche communities. Counterinsurgency in Wallmapu also means protecting and promoting the capitalist development that molds the landscape in the furtherance of social control: monoculture deserts of pine plantations that suck up the water, ruin the soil, and supplant the native plants and animals that make the Mapuche way of life—their medicine, rituals, food culture—possible; megaprojects like dams, airports, and highways that displace communities and accelerate military and economic intervention into the territory.

The higher intensity mode of repression seeks a hostage for every outrage against a democratic solution to the “Mapuche conflict” that the weichafe commit. Sabotage and arson have been normalized at this point that the police are unable to arrest a suspect for every illegal action that is carried out, not without abandoning their pretext of legality. But every time a cop or latifundista is killed, or a major infrastructural project is targeted, the Chilean state selects several influential Mapuche to take the fall.

The Chilean state highly values its veneer of legality as a tool for achieving the consent of the governed by symbolically distancing itself from the dictatorship that ended in 1990. This is a difficult task as many Chileans remain suspicious of the government and many more are armed than when the military government took over in 1973. Twice a year, Chileans mark the continuity of their suspicions and in the poblaciones they test out their weaponry, often on police. Many Chileans also sympathize with the Mapuche struggle. (An excellent documentary that explains this background is The Chicago Conspiracy).

The Chilean state faces the same limitations as any state that tries to apply criminal law as a tool to repress a popular struggle. They have to break their own laws if the tool is to have any chance of getting the job done. This would not be a problem in a more sedated democracy, but the Chilean state in particular is sensitive about the effectiveness of its democratic image.

Generally, the only way the Chilean state is able to manufacture evidence adequate for convictions that nominally follow legal rituals is through the dubious figure of the anonymous protected witness. Mapuche communities and their capacity for vengeance are strong enough that the age old tool of the snitch could only be applicable to the Mapuche conflict with heavy modifications: defendants are never allowed to know the identity of the paid informants testifying against them, therefore they can make no specific challenges to the informants’ veracity.

The Chilean state is attempting to stretch an already precarious legal foundation to bring the repression against the Mapuche into the realm of anti-terrorism. Three years ago, a disciplined hungerstrike backed by committed and expansive support defeated the government’s previous attempt to prosecute the Mapuche as terrorists. (See these articles about the 2010 hungerstrike and a statement by some of the hungerstrikers).

Now, the government is trying again, pinning its hopes on the Quino case, in which a dozen peñi face up to twenty years in prison. If they succeed at the social level in applying antiterrorism law, they will have achieved a powerful tool in capping the Mapuche struggle, isolating those elements most committed to full independence and forcing the rest on the path back to a democratic solution that does not challenge the integrity of the Chilean state nor the capitalist ideas of alienated land and alienated freedom on a global level.

What we can induce is the following: the Chilean state, predictably enough, wishes to respond to every single major act of Mapuche illegality by taking hostage people it has identified as valuable to the struggle, utilizing the logic of collective punishment. The particulars of its situation require it to protect the democratic pretext for repression. Therefore, the success or failure of Chilean state repression against the Mapuche is a function of the extent to which that repression can be justified to Chilean society on democratic grounds, grounds on which the ruling class and the ruled can be said to have a unity of interests, because it is only within such a narrative that the argument of public order and safety makes sense.

Domestically, the segment of the Chilean population which the state fears has already demonstrated it will sympathize with the Mapuche struggle even if they burn down logging trucks, construction equipment, warehouses, developments, and mansions (something they frequently do). But in the last year, with the deaths of a couple estate lords, Chilean sympathy has waned. Although incoherence is a universal position under the yoke of capitalism, still we must call out their position as incoherent. These same people all sighed in regret when Pinochet died peacefully in his bed, claimed by old age rather than an act of vengeance. Why, then, do they moan and fret when a Luchsinger, one of the very bedrocks of Pinochet’s power, dies at the hands of those who struggle? If Chileans who are committed in their support of the Mapuche struggle cannot convince their compatriots to resolve this incoherence by shedding their civic qualms, then either the Mapuche struggle has already encountered the outer limit of its available tactics, or the Chilean state will successfully be able to apply antiterrorism law in repressing them.

On the international level, the Chilean state wants to project itself as a stable, developed nation that honors its business contracts and respects the rule of law. The level to which international solidarity can disrupt this projection is added weight to the other side of the scale which prosecutors, governing officials, and businessmen have brought out to see if they can successfully utilize this new tool against the Mapuche.

An additional fact surpasses such calculations: successful international solidarity would also serve as a bridge by which lessons of struggle, and of the nature of capitalism, that are elucidated in the context of the Mapuche struggle can be spread across the world and applied to our own battles.

This is the situation which gives sense to our solidarity, and it is only in this context that we present the following list of major cases of repression. Below: the real people, the specific clashes. Above: the lay of the land and the general motions of the war we are fighting.

On October 10, 2009, a group of Mapuche blockaded the highway at the Quino toll station. On the sole evidence of a highly paid confidential informant, the Chilean state arrested 10 peñi, accusing them under the antiterrorism law of attempted murder, illegal association, robbery, and arson. The accused are José Queipul Huaiquil, Víctor Queipul Millanao, Camilo Tori Quiñinao, Felipe Huenchullán Cayul, Juan Huenchullán Cayul, José Millanao Millape, Juan Patricio Queipul Millanao, Jorge Marimán Loncomilla, Ernesto Cayupán Meliñán and Luis Marileo Cariqueo. The case faced a series of legal setbacks as judges struck down the use of the antiterrorism law, and later acquitted the defendants for lack of evidence. However, the prosecutor, pressured by the government and local business interests, continues to press new charges. One maneuver was to break off the cases of two of the accused, who are minors. Even after the others were acquitted, the two minors from the communities of Temucuicui and Cacique José Guiñón were brought to trial separately, and still under the antiterrorism law, in May 2013 (with results still pending). There are also indications that others previously accused in the case will be brought back to trial on new charges, again under the antiterrorism law. Many of them have already been imprisoned in the past, and some of them participated in the major hungerstrike of 2010 which ultimately caused the government to withdraw its use of the antiterrorism law and release the accused with “time served.” This case constitutes an attempt to lock up some of the most active and well known participants in the Mapuche struggle, on the accusation that they have formed an illegal network spanning multiple communities. Throughout the investigation period, police have also harassed, interrogated, and in some cases even arrested the children of those accused (though in the latter case charges have always been dropped or resulted in absolution).

Shortly after midnight on January 4, 2013, the mansion of the major latifundista and usurper of Mapuche lands Werner Luchsinger was set ablaze at Vilcún, near Temuco. The bodies of Luchsinger and his wife, Vivianne Mckay, were found inside. Werner was the cousin of fellow businessman and latifundista Jorge Luchsinger. On January 3, 2008, Mapuche weichafe Matias Catrileo was shot in the back and killed by police guarding Jorge Luchsinger’s estate against an action to pressure the latifundista with the longterm goal of recovering stolen lands. Police opened fire on the crowd with automatic weapons. Catrileo was killed while running away.

The machi of Lof Yeupeko-Katrileo, Celestino Córdoba, was arrested and accused of the arson and murder under the antiterrorism law. At the end of May, the Chilean prosecutor filed a request for life imprisonment. Supporters have organized many marches and religious ceremonies to aid Córdoba, whose health has deteriorated rapidly since his imprisonment. According to the Mapuche, the machis do not often fare well in prison when their connection with the land is broken. When the Mapuche culture was more heavily repressed, the machis, or those who would have become machis, were often locked up in mental institutions. Córdoba is also accused of the December 2012 arson of another latifundistas home, for which the prosecutor is seeking an additional 36 years of imprisonment.

The leftist Mapuche organization CAM publicly denounced the arson. They attributed what they saw as an irresponsible act to the Temuco prisoners who split with them during the 2010 hungerstrike. Thanks to CAM’s politicking and their attempt to avert the blame, a weichafe had to go into clandestinity.

Lof Yeupeko-Katrileo, renamed in remembrance of Matias Catrileo, is leading the struggle in the county of Vilcún for the recovery of stolen lands. The Luchsingers are the primary usurpers of Mapuche land in the county.

Wente Winkul Mapu, another highly active community on recovered land near Ercilla, was the site of a violent police raid in April 2012. Such raids are extremely common against Mapuche communities in struggle, resulting in the terrorizing of residents, the traumatization of children, brutality against the elderly, destruction of houses, and the stealing of tools and money. However, in April 2012, things turned out a little differently. One cop ended up dead. Apparently, the highly militarized, intensively trained, armed-to-the-teeth GOPE opened fire on themselves, killing one. Of course, the Chilean police are not about to let their stupidity and ineptitude go unpunished. They are claiming that someone inside one of the houses fired the shot, though they do not explain how the shooter got away from the surrounded village.

At the end of April 2013, police arrested the werken of Wente Winkul Mapu, Daniel Melinao, and accused him of complicity in the cop’s murder. Melinao is a highly active, longtime participant in the Mapuche struggle. It is not a coincidence that police arrested him at the bus station in Collipulli as he was on his way to Concepción to participate in a panel discussion about repression against Mapuche communities.

At a court appearance shortly after Melinao’s arrest, police arrested the longko of the nearby community of Temucuicui, Victor Queipul, accusing him of disorder, a charge that could bring a couple years of imprisonment. The longko had come to the appearance in solidarity with Melinao. Melinao was denied provisional release and sent to prison to await trial. The prosecutor revealed that they are searching for Erik Montoya, also of Wente Winkul Mapu. Two anonymous paid witnesses claim to have seen Montoya open fire on the cop during the raid. Montoya is in clandestinity.

In June 2012, police raided Wente Winkul Mapu searching for Montoya, entering houses without a warrant and smashing everything. When the weichafe of the community forced the cops out, they opened fire with tear gas and bullets. They shot one young weichafe, Gabriel Valenzuela Montoya, in the back. Six others were wounded, including Gabriel’s grandfather and three minors. Gabriel evaded arrest for the confrontation by hiding until police left. He later denounced the police. Perhaps in retaliation, in November of the same year he was arrested and accused of a robbery-murder along with Luis Marileo of the community José Guiñón and Leonardo Quijón of the community Chequenco. Gabriel, who is being held in a juvenile detention center at Chol Chol, is currently on hungerstrike to protest the frame-up, which he and supporters say is intended to delegitimize the Mapuche struggle. Quijón carried out a hungerstrike in the prison at Angol shortly after his arrest. Marileo is also one of the accused in the Quino case.

The people of Wente Winkul Mapu and supporters have organized large protest marches to the courthouse to support Melinao, and in late May they began communally cultivating a tract of newly recovered land in protest of the use of the antiterrorism law and as a sign that they would continue their struggle.

Communities along the river Pilmaikén, in the far south close to Osorno, are fighting against the planned installation of a hydroelectric dam that would flood the valley and destroy much land and many villages, as well as the sacred ground of Ngen Mapu Kintuante. The Williche (Mapuche from the far south) have proclaimed their right and responsibility of self-defense and the defense of their territory against any further incursions into the Pilmaikén watershed by the Chilean government, Conadi (the governmental institution for the development of indigenous peoples in Chile), and the company Pilmaiquen, S.A. The ayllu rewe of Ngen Mapu Kintuante currently has four people facing charges for actions against the dams, including the machi Millaray Huichalaf.

On May 19, 2013, the peñi Orlando Benjamín Cayul Colihuinca was remanded to preventive detention pending trial for the arson of construction equipment. Cayul is a member of the community Temucuicui Autonoma. The longko Victor Queipul and werken Jose Queipul, as well as several others of the same community, are also facing charges under different accusations. And on May 23, police raided the community, evicting and burning down several houses that had been constructed on land newly recovered from a latifundista.

Mapuche from several communities in the area of Freire, south of Temuco, are fighting against the construction of a new airport. In 2012, Chilean justice convicted three people involved in the resistance: the werken of the community Mawizache, for illegal possession of a firearm; another member of the same community and the werken of the community Trapilhue, both for public disorder. In March 2013, the communities of Mawizache, Trapilhue, and Wilkilko had to release a public statement, refuting an announcement by AyunMapu, a leftist Mapuche organization based in Santiago that a deal had been made to go ahead with the airport. The three communities asserted themselves as autonomous, contradicting the organization’s claim that they were members. They emphasized that they had participated in a handful of protests alongside but not under the authority of AyunMapu.

In raids against communities in the area on April 30, police arrested three peñi, Jorge Painevilo Loncomil, Miguel Painevilo Licanán y Segundo Braulio Neculmán, and accused them of attempted murder, arson, and illegal possession of firearms. Two weeks later they were released pending further investigation. Their release was secured after a protest of several hundred outside the Temuco prison, and other mobilizations by communities hit with brutal raids in recent months.

On March 9, a large group of Mapuche blocked a major highway in the region with tree trunks and burning tires to protest the airport. The same week, a group of thirty temporarily seized the airport construction site.

In February 2013, Juan Millacheo, longko of Lof Newen Mapu of Chequenco, was arrested by Argentinian police in Nequén, Puelmapu, by Argentinian police, and handed over to their Chilean counterparts. Millacheo had been living in clandestinity for 9 years after being condemned in 2004 to ten years imprisonment for arson under the antiterrorist law. After three weeks, the Chilean courts accepted the defense’s motion to have the sentence commuted to one year of conditional liberty with monthly sign-ins.

Puelmapu, the “Eastern Lands,” are the part of Wallmapu east of the Andes, occupied by the Argentinian state since the 1880s. Although repression and colonization after the invasion were more brutal in Puelmapu, the Argentinian state has not succeeded in stamping out the Mapuche struggle.

In July, 2011, a group of half a dozen armed men, associates of a local latifundista, attacked the community of Lof Loncon in the Rio Negro province, opening fire on community members. They then proceeded to steal the community’s cattle, as police intervened to impede community members trying to stop the theft. In February of 2012 in the province of Nequén, nine families from the community Quintriqueo recovered a parcel of land that had been usurped by area landlords.

In April 2013, Mapuche saboteurs damaged a railroad line, causing the derailing of a logging company train with over 40 cars full of cellulose and leading to the extensive destruction of the line. The next month, masked weichafe blocked several highways with burning tires in and around Temuco. Their communiqué read: “All political prisoners on the street with no conditions! Down with the 28M frame-up! Expel the pigs from the Mapuche communities!”

[The 28M frame-up is the new “Bombs Case of Temuco,” when several anarchists in Temuco were arrested on March 28, as police planted bomb-making material in the social center where they were arrested]




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

