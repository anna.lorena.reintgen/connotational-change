
AAARRRGGGGHHHH, Matey!

I was one of the kids who grew up thinking that pirates were, well, cool as shit. Swashbucklers had evil-looking flags and tattoos, they wore eyepatches, they were fearless bandits, hedonistic drunks, and nationless nomads. No merchant vessel was safe from their ire, and their scorn for laws and the norms of a hierarchical society made it easy to want to emulate them (particularly on Halloweens, when children get to roleplay some of their favorite heroes, villains, freaks, and monstrosities). Given this background, I was excited to pick up a history of piracy since I’ve really never read any — particularly one written with a radical lens.

On this point (that the book is written from a radical perspective), Kuhn (p. 5) is explicit, “While I sincerely hope that this book can arouse the interest of a broad spectrum of readers...it would make little sense to deny that it was written from what has been called a radical perspective.” And from this perspective, he begins his reflections taking issue with two common narratives of pirates, split by ideological readings of pirate histories. That is, more conservative histories would condemn pirates as bandits, common criminals, brutish murderers, and the like. For radical historians, a certain romanticization took place that painted pirates as daring revolutionaries who lived outside the law, abolished the wage system in their communities, and transgressed normative (and hierarchical) assumptions about nation, race, gender, ability, etc.

Kuhn’s position on these two, varied readings seems to be, “Does it really matter?”

After all, what he’s done with this book is try to give us a balanced historical view of piracy (in a brief sense) in terms of what probably happened. But more important to this book is the use of the narrative of piracy to inspire us with stories for contemporary radical political theory and practice. And that’s the hook (sorry, pun intended and I might not be able to stop) — I mean there’s a lot there to be folded into fascinating and entertaining stories, allegories that give us hints at how we might change our world and ourselves.

Take, for example, the idea of the pirate as the “enemy of his own civilization” (taken from Chapter 2’s title). Kuhn borrows from Foucault’s work on a disciplinary society here, implicitly asking the reader, “What will it take for people to disobey in a society such as ours?”:

...the decisive war is the one waged between two categories of people who are defined as ‘civilized’ and ‘savage.’ It is not coincidental that this shift in discourse correlates with the onset of the colonial European enterprise. Certain people needed to be ‘dehumanized.’ This meant non-Europeans as much as Europeans who fell outside the norms of their own society. As Rediker reports, pirates were ‘denounced...as sea monsters, vicious beasts, and a many-headed hydra — all creatures that...lived beyond the bounds of human society.’ Indeed, as late as in the early 20th century, some would call pirates ‘monsters in human form,’ or ‘an odd mixture of human trash’ (p. 97).

There is a similar popular discourse used to describe anarchists as bandits, thieves, declassed workers who refuse to work — and there is a grain of truth in those criticisms. But what might happen if more people embraced that kind of uncivilized, “monstrous” subject position? What might happen if the ideological controls set around notions of “normalcy” were disrupted? How might people react if they were not held back by the obedient domestication that is part and parcel of much of human social existence? And how do we reach a state of generalized revolt against the existing order if people don’t break with those civilized pretensions?

And after his introduction, Kuhn goes through some of the worst pretensions of our civilization and shows counter-narratives based on pirate stories (even while dispelling some of the more egregious myths that romanticize the pirate as a sort of “noble savage”). Kuhn describes nationlessness, and by extension, statelessness in pirate communities. He analyzes their methods of organization, both in leadership and resource distribution (pirate economics!). He discusses the story of women pirates, who transgressed the gender roles of their time, even while balancing this narrative with histories of pirate sexism and, sometimes, contempt for women. He writes about pirates and sexuality (and, honestly, who couldn’t wonder about what all those sweaty folks in tight pants and tattoos stuck aboard those lonely ships together were doing?). And (surprised I’d never really thought of this myself) Kuhn talks a bit about pirates and disability (indeed, rogues with peg legs and eye patches do make for interesting anti-heros). He takes note here to point out these stories, but he balances it with historical guesses on what pirates probably were — often times not the noble savages recounted in many histories. But again, the story’s good, it inspires, and it can serve as a jumping off point to discuss all kinds of political ideas (the best stories do, after all).

And with the deftness and agility of the meanest swashbuckler, Kuhn then turns his attention to radical political theories. Kuhn discusses anarchy, and the implications hidden within varying definitions. He uses the band of pirates roaming the seas as a metaphor for Deleuze and Guattari’s “nomadic war machine.” He discusses the legend of Libertalia — surely a mythic story of a pirate utopia — but presents it as a tale about prefigurative political practice. He reads into the proletarian origins, and politics, of pirate communities; their use of guerrilla techniques; and, of course, their sailing under the black flag.

We all relate differently to various kinds of stories. I typically enjoy reading what many others consider dry and boring political theory. One of my closest friends swears by pop conspiracy novels (I’ll likely never understand this). My brother just recently tried to get me into a sophisticated spy novel he read (not into it).

If we want radical ideas to matter, and to matter to a lot of people, we need many different kinds of stories. We need dry, jargony political theory for people like me (and the rest of the world’s junkies, addicted to Proudhon, Bakunin, Kropotkin, or their modern forebears). We need mystery novels and romances. We need the Le Guin’s, who write science fiction with an eye towards communicating radical values. And we need stories about pirates. What better combo for a kid who grew up on pirates and fell in love with radical politics later in life? Win/win, really.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



AAARRRGGGGHHHH, Matey!



...the decisive war is the one waged between two categories of people who are defined as ‘civilized’ and ‘savage.’ It is not coincidental that this shift in discourse correlates with the onset of the colonial European enterprise. Certain people needed to be ‘dehumanized.’ This meant non-Europeans as much as Europeans who fell outside the norms of their own society. As Rediker reports, pirates were ‘denounced...as sea monsters, vicious beasts, and a many-headed hydra — all creatures that...lived beyond the bounds of human society.’ Indeed, as late as in the early 20th century, some would call pirates ‘monsters in human form,’ or ‘an odd mixture of human trash’ (p. 97).

