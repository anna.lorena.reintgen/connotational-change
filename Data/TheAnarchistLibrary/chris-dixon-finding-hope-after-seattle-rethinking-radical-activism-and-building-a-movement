      Questions of privilege      Rethinking radical activism      Finding hope      Building a movement
Just over a year ago thousands of us were preparing for an uncertain showdown with global capital in Seattle. Yet we barely realized that we were about to initiate some of the most successful protests in recent U.S. history. Indeed, hardly anyone believed that we would have any major impact on corporate globalization. At best, we hoped that we might be a significant blip on the nightly news and perhaps a noticeable inconvenience to trade delegates at the WTO Ministerial.

To our shock, though, what we were able to accomplish spun far beyond our wildest expectations. And it was more than the sheer number of protestors or the final paralysis of the Ministerial. In Seattle we experienced our collective power, learned daily lessons from each other, and realized that we could win. In short, we developed a new capacity for resistance with a vital sense of inspiration.

Now, having recently passed the anniversary of the WTO protests, we sit in a markedly different place. The events of the past year have been a mixed bag, but the momentum of Seattle is still alive. In fact, many are beginning to tentatively refer to ‘the movement’ with seriousness nearly unheard during the last twenty-five years. Now more than ever, then, I think it’s important to take pause to consider where we on the libertarian left stand in relation to what went down in Seattle and what has blossomed from it. As one among a number of anarchists who initiated the Direct Action Network and put out the original call to shut down the Ministerial, I’d like to offer some thoughts to this crucial dialogue.

There are many ways, of course, to critically reflect on this burgeoning movement. For instance, numerous anarchists are rightfully pointing to the lack of clear anti-capitalist analysis, as well as the ludicrous posturing about property damage, coming from many of the organizations involved. Just as important, other anarchists and unaffiliated radicals are thoughtfully warning, in particular, of the dangerous racist dynamics operating in the heart of these activist efforts (see www.tao.ca/~colours for some of the best of this commentary). Here, though, I want to consider this movement from a different perspective, one that has emerged commonly in activist conversations: hope.

Many folks admit that Seattle and subsequent mobilizations have forever changed how they think about the potential for social transformation and mass action. As activist Raj Patel observes about the A16 actions against the International Monetary Fund and World Bank in D.C., “There was a real feeling, as we sat in the rain on Monday, and as our comrades were arrested, that there is a genuine possibility for progressive social change.” In essence, amidst the blockades and police confrontations, scrawled graffiti and tear gas, we’ve found a renewed sense of hope.

There was no secret to it in Seattle. Our determined and imaginative organizing leading up to the WTO protests, combined with our astounding victories in the streets, was a deeply heartening experience for the thousands of us who were there, not to mention the countless others who watched and celebrated from elsewhere. Using brazen anarchist tactics and decision-making systems, we managed to outwit state authorities, endure police assaults, and create substantial disruption. Our ambitious rhetoric (“shutting down the WTO”) for once became a reality. And we left bruised, tired, but profoundly inspired.

Since then, the at times haphazard organizing model from the WTO protests has been redefined and re-created across the globe. International days of action like N30, complete with the requisite abbreviated symbols (A16, S11, S26), are happening more and more. All the while, in many of the communities I visit, I discover that, for every one of us who organized for Seattle, roughly five new folks have been radicalized. Presumably, the hope that we discovered at the WTO Ministerial has grown contagious. What else might explain the dedication of so many to carry Seattle’s success from D.C. to London, Windsor to Melbourne, LA to Prague?

Look closely, though. Each successive mobilization has certainly energized, radicalized, and won on its own terms, but each has also been accompanied by a letdown. None can quite live up to the size, widespread media coverage, and success of Seattle. Plus, the time and energy required to organize and even participate in each mobilization is literally staggering, often leaving activists and organizational resources exhausted. And folks truly are tired. In the words of radical journalist L.A. Kauffman, “If there’s one point on which everyone in the movement seems to agree, it’s that action-hopping is getting old.” Meanwhile, state repression has intensified, which translates into more preempted, jailed, and generally worn activists. Consequently, it’s no surprise that some are feeling disheartened and uncertain.

We definitely haven’t lost our collective sense of hope, but it is waning. We need sources of hope, though, if this really is going to become a sustained movement rather than an historical episode. “Hope,” as revolutionary feminist bell hooks says, “is essential to any political struggle for radical change when the overall social climate promotes disillusionment and despair.” And this isn’t a blithe, feel-good kind of hope. No, this is hope on the ground, hope like we glimpsed in Seattle, real hope generated by the social struggles around us and abroad. Without it, we risk forgetting not only that we can in fact fundamentally transform society as it is — seemingly immutable but not unalterable — but also that we have the capacity to challenge and change it.

Here, however, is the kicker: if we’re going to sustain our hope and this budding movement, we have to seriously rethink what constitutes ‘radical activism.’ We have to stop solely hinging our hopes and focusing exclusively on mass mobilizations, expecting each to be “the next Seattle.” In other words, we have to move beyond the myopic view — often endemic among anarchists — that the most ‘important’ activism only or mainly happens in the streets, enmeshed in police confrontations. This view sidesteps crucial questions of privilege and avoids the reality that social change happens on multiple levels, all while obscuring other avenues for hope that exist around us.

Anarchism is fundamentally about dismantling systems of power. And promisingly, this critical orientation is cropping up throughout the movement. But while successive mass mobilizations since the WTO have directly confronted a number of powerful institutions, they have also managed to replicate patterns of power and exclusion, especially based on race and class. No doubt, many anarchists and other activists realize this. I’m not the first and certainly not the most articulate to level these criticisms. I think, though, that they’re worth briefly repeating, particularly since many young white, middle-class radicals (myself included) often acknowledge them without really reflecting on them.

Broadly, they can be distilled into a single question: Who can afford to action-hop? And we must understand that word afford with its many meanings: Who can afford to travel across the country, or even the world? Who can afford to risk their bodies in potentially dangerous police confrontations? Who can afford to be away from family and/or work responsibilities for uncertain periods of time? Who can afford to risk their legal statuses with the possibility of arrest? Altogether, not many people.

A key problem, then, with the focus on mass mobilizations is the underlying idea that we, as people who seek radical social change, must each take great risks and make huge commitments in very prescribed ways — and that all of us can afford to do that. Yet this just doesn’t face reality. When many folks are working one, two, or even three jobs, taking care of family members, and dealing with immediate crises, they simply can’t devote all of their time to activist efforts. Indeed, many people are concerned about simple survival — feeding their kids, getting some work or getting to work, paying the rent, keeping out of jail, staying healthy with limited or no access to health care.

More to the point, direct action, as many anarchists tend to define it, can be deeply exclusionary. While it undeniably empowers some — mainly white and middle-class — it disempowers others. Used as a central tactic of mass mobilizations, direct action can in fact implicitly assume a certain degree of privilege, with dire consequences. As anti-capitalist organizer Helen Luu explains, “the emphasis on this method alone often works to exclude people of colour because what is not being taken into account is the relationship between the racist (in)justice system and people of colour.” White working-class and poor people, also frequently veterans of police repression, face some similar forms of marginalization.

Further, in the flurry of action-hopping, the focus has so far remained largely global, with occasional forays into the national. And many mobilization organizers continue to frame issues that way. Whether it’s global capitalism, represented by the WTO and the World Bank, or state authority, in the form of the Republican and Democratic National Conventions (all worthy targets, by the way), the connections to everyday lives are frequently lost. What about privatization of city services as neoliberalism on the home front? What about welfare “reform” as domestic structural adjustment? What about daily police brutality and prison growth as massive state repression? The connections are all there, as are the often unacknowledged activists working to challenge these injustices, yet many white, middle-class radicals simply aren’t seeing them. The tragic result is that we lose critical opportunities for linking the workings of global and national systems to local organizing efforts and, in effect, broadening our resistance.

For those of us who can afford to action-hop, it’s easy to overlook how exclusive and often disconnected mass mobilizations are — especially amidst the dangerous thrills of street skirmishes and arrests. For instance, reporting on the Democratic National Convention protests, activist journalist Larry Everest approvingly quotes a young man, thoroughly bruised from rubber bullets, after a major police confrontation: “Damn, I wish we could do this every day.” The reality, however, is that many folks do do that every day — as unwilling victims of police brutality and a racist, classist legal system. And along with many others, they can’t afford to glibly put themselves in that situation any day — a reality with considerable implications for how this movement is shaping up and where our hope lies.

I certainly don’t suggest that we give up on mass mobilizations. In fact, we need to continue engaging in major collective actions that significantly challenge the legitimacy of existing institutions. In order to do this effectively, however, we should be creating a healthy, vibrant, truly diverse movement for fundamental social change. One crucial step for those of us who are relatively privileged (and therefore often insulated) is to push our outlook to encompass many forms of and tactics for effective radical activism — that is, many sources of hope.

Let me illustrate this with some wisdom gleaned from longtime radical Peter Bohmer. His striking emphasis is that everyone can contribute to social change in both.“small and large ways.” Simple yet crucial, this acknowledges the possibility of seemingly small efforts having important — and far-reaching — results. For example, think about Jon Strange and the other folks in Columbus, Ohio, who crashed an internationally televised “town hall meeting” with U.S. Secretary of State Madeline Albright in 1998, and successfully raised critical questions about U.S. foreign policy on Iraq. Consider the demise of the Multilateral Agreement on Investment (MAI) just a couple of years ago, precipitated by small-scale letter-writing, teach-ins, city council resolutions, public education campaigns, and, yes, mass direct action. Or — something with which I’m more intimate — reflect on the widespread success of a handful of us graduating students at the Evergreen State College in 1999 cajoling our administration into hosting political prisoner Mumia Abu-Jamal as a commencement speaker. These are just a few more recent examples, buoyed by a history of small contributions having major consequences.

Recognizing small and large ways of contributing to social change means including much that is frequently excluded. Further, it means blurring the line between what might be considered ‘small’ and ‘large.’ Fighting reactionary ballot measures, establishing community and cultural centers, educating others creatively, demanding authentic public oversight of police, creating local institutions for mutual aid, building art installations, organizing in our workplaces, sustaining nurturing relationships, challenging polluters in our communities, constructing alternative media, opposing prison construction, struggling for control over our schools, painting graffiti, planting community gardens, protesting welfare cuts, confronting oppression in our daily lives — these and many more contributions can be immeasurably powerful and deeply inspiring. Seen as complementary demands and tactics, they harbor valuable lessons about challenging power, fermenting social change, and developing directly democratic control over our lives. Indeed, they are the potential building blocks of a vivacious, diverse movement capable of winning key gains and, ultimately, transforming our society.

These examples may not seem especially ‘radical’ to some, particularly those in purist anarchist and other circles that categorically eschew anything that hints of ‘reformism.’ In fact, acknowledging multiple forms of resistance thankfully does call into question the meaning of the word radical. Too often this concept is defined almost exclusively by white, middle-class men, self-appointed bearers of a radical standard. I think we can do better, though; and I mean we in the sense of all of us who struggle in diverse ways to go to the root — to dismantle power and privilege, and fundamentally transform our society. What’s lost in easy dismissals and clean dichotomies is that we all choose a variety of tools, tactics, strategies, and demands based on our circumstances and objectives. We should be learning from this versatility, not condemning it.

Pointing out that there are many ways that people act in their lives — both small and large — doesn’t mean that we can’t be critical, strategic, or focused in what we do and advocate. Neither can we give up our commitment, our demands on each other and ourselves. There are, however, many ways of working to transform the world. Let’s learn from elders like veteran activist Bernice Johnson Reagon. She reminds us that the particular ‘way’ or ‘issue’ that any one of us chooses is not the way or issue. If we assume that it is, we risk ignoring other possibilities for social change, and we miss out on the inspiring efforts of people all over.

Rethinking radical activism is about understanding social struggles in broad terms and toppling conventional hierarchies of activist ‘worth.’ Equally crucial, it’s also about locating and sustaining hope. Overly fixated on mass mobilizations, we can easily lose sight of what’s happening around us in our workplaces, households, classrooms, religious communities, neighborhoods, and local activist groups. Yet these commonplace venues can be just as subversive as street confrontations at major protests, if not more so.

Certainly there is something awe-inspiring and magical (not to mention critically effective) about large, militant actions. The WTO protests, for instance, were successful not only in disrupting the Ministerial, but also in enthusing and inspiring thousands. As anarchist writer Chris Crass notes, though, Seattle “was made possible because of all of the organizing that we do day-to-day, the often unglamorous work that makes social change happen.” That is, seemingly small contributions — community outreach, creative educational efforts, coalition-building, and much more — laid the groundwork for the victories at the WTO Ministerial. And the same is true for each of the mobilizations since. The hope that many of us have found in the streets from Seattle to Prague, then, isn’t isolated; rather, it stems from the hope that we create every day.

Examples are plentiful. Many of the best, in fact, we will likely never hear about. For one, there all of the dedicated folks who run radical “resource centers” and “infoshops” out of email accounts, small offices, storefronts, campus buildings, and even their own homes. Often unknown beyond their communities, they provide essential resources and circulate crucial information. There are all of the grassroots welfare rights organizations in both small and large cities, organized by some of the most demonized people in our society. Relying on shoestring budgets and all-out volunteer efforts, they organize incredible mutual aid and determined resistance. There are the courageous indigenous peoples, such as the Gwich’in in Northern Alaska and the Dine’h in Big Mountain, Arizona, struggling for the sanctity of their lands and cultures. Steadily, they confront a colonial project that spans hundreds of years. And there are the efforts of those working covertly in the Animal Liberation Front, the Earth Liberation Front, underground anti-biotechnology groups, and others still unknown. My feeling is that we’re going to see more, not less, of these kinds of very direct actions.

Keep in mind that this is only a snapshot. Even these examples don’t adequately represent the real breadth of inspirational resistance. Everywhere there are folks who continue to plug away — largely unknown — steadily transforming the world, each day, each week, each month, each year. At times barely audible, at times deafening, these efforts can supply a sustained sense of hope to last us well beyond our memories of Seattle and other recent mobilizations — and carry us into even bigger, more successful, more innovative ones.

Just over a year after the WTO protests, we are at a crossroads. Actually, we are at many crossroads, with critical questions of strategy, vision, and goals, among others. However, one particularly defining choice concerns how we understand and build this movement. On one hand, we can exclusively pin our hopes on what seasoned radicals Jenn Bowman and Summer Thomas call “activist tourism”: organizing for militant direct actions across the globe, carrying each to a crescendo, moving on after the often anticlimactic outcomes, and calling this ‘the movement.’ On the other hand, we can complement mass mobilizations with a more multidimensional focus on diverse forms of radical activism. And further, we can recognize and ally ourselves with preexisting movements of diverse peoples who are organizing in their communities — and have been for a long time. Obviously, I think the latter holds more promise — more hope — for building a healthy, sustained, inclusive movement (or coalition of movements) struggling for fundamental social change on the edge of the twenty-first century.

We can move in this direction. Indeed, a genuine strength of this burgeoning movement, in my view, is its growing willingness to discuss multiple tactics. Black bloc anarchists in Seattle, for example, managed not only to successfully inflict damage on corporate targets, but also to initiate an ongoing discussion about the many ways that we can and do engage in social struggle. Of course, this discussion isn’t always so civil — at times full of recriminations and self-righteousness — but I think it’s a beginning. If we can steer beyond confining debates about street tactics at mass mobilizations, we can perhaps begin to appreciate and consider diverse tactics and forms of activism in many other areas and circumstances.

I don’t hesitate to say that we are forging a new libertarian politics, an innovative resistance with a vibrant sense of hope. And there is, as radical theorist Michael Albert explains, a “logic” to this dissent: “Raise ever-enlarging threats to agendas that elites hold dear by growing in size and diversifying in focus and tactics until they meet our demands, and then go for more.” Mobilizations are undisputedly crucial flare points for this, moments of gaining ground, captivating public attention, consolidating gains, and solidifying our resolve. Yet they are only one set of tools among a multitude of focuses and tactics.

Writer Naomi Klein, perhaps the most well-respected observer of the recent string of mobilizations, incisively points out that the movement born in Seattle isn’t so much a single movement as a conglomeration of concentrations and campaigns with shifting but often common goals. Together, they’re decentralized, dynamic, innovative, unpredictable, and difficult to control. This essentially anarchist character is their distinct power. Consider the potential power, then, of applying this to an even more extensive movement with a variety of strategies and aims.

Ultimately this is what we must do, for social transformation requires a broad approach encompassing a truly radical movement (or movements) composed by the daily activism of mothers, farmers, people of color, youth, sex workers, immigrants, artists, queers, indigenous peoples, factory laborers, teachers, environmentalists, service employees, poor folks, and all of the other overlapping, diverse sectors of our society.

That is where I find hope after Seattle.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

