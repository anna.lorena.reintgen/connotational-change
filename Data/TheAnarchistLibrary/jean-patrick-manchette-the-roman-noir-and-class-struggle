
It is quite appropriate and more or less pertinent to situate at the end of the Civil War the moment when the terrain which produced the American roman noir was established and critical commentary began to be constructed.

In the years that follow the Civil War, in fact, the mythic frontier fades away, as industrial and financial capital invade the Reconstruction South and occupy the whole of the United States, notably by the multiplication of powerful railroad connections, transcontinental and other. The hardy trappers and the pious caravans of pioneers no longer have a territorial “beyond” where they can dream of establishing an ideal way of life, escaping the constraints (or vices) of civilization. And the internal frontier is in turn abolished in a couple of decades, through Indian and cattle wars.

Henceforth – at the end of the nineteenth and beginning of the twentieth centuries – the immigrants who arrive en masse (some years there are nearly a million) disembark in the closed field of an economy based on wage labor. We must no doubt not forget the farmers, who will not always allow themselves to be forgotten by the history of modern America, and who the roman noir will not forget. But principally it is the violence between capital and labor that shakes up society, starting with the remarkable riots of 1877, which spread across the entire country and against which strong arm men (vigilantes) – hoodlums – are provided by private police agencies (among them, of course, the Pinkertons) and sworn in as deputy sheriffs, along with the state militias, the police, the army.

These forces are overwhelming, they are obviously brutal (they often fired on the strikers), and they commit atrocities. The famous martyrdom of an anarcho-syndicalist is often cited who, taken prisoner, was castrated by a good citizen, then hung from a bridge and finally finished off by rifles. But a multitude of other unionists and agitators will also know a hardly better fate. The judicial apparatus is there for the survivors in order to send them to the penitentiary or deport them.

Against such repression the anarchists sometimes toss bombs, and the agents provocateurs toss others, which only worsens the situation and worsens the pursuit of “criminal syndicalism.” Other militants, doubtless the best of them, attempt to unify the laboring class in the Industrial Workers of the World (IWW). The IWW, very much of a minority, but active throughout the country, has as its slogan One Big Union, and unites workers without distinction as to craft (or ethnicity), leads several large-scale victorious strikes before and during World War I, and suffers many defeats as well. Its program is anarcho-syndicalist and the organization thus refuses any participation in the electoral political game. After the war it is progressively crushed, principally by judicial means. ...The IWW having, in summary, been the only revolutionary organization on the United States it enjoys a special prestige. But it never had much more than 100,000 members at a time (though a million members probably passed through it). Its defeat left intact a tradition of direct action that still flourishes today, where dynamite and rifles periodically resurface in mining and other conflicts. But this defeat showed how impossible it was for the proletarian strata to unify. Which leads us back to the roman noir, which we seem to have strayed from.

In fact, the poor immigrants of the end of the nineteenth beginning of the twentieth centuries, failing to form a community, fall back into their ethnic divisions. Organized crime – Chinese secret societies, Italian Mafia, Irish gangs, Dutchmen, Jews, etc. – is at that time an integral part of the electoral and governmental machines that run the big industrial and commercial cities. In New York, as in Chicago, the mayor, the district attorney, the police commissioner and the gang bosses (which run the electoral districts) form a friendly community of interests. (We should re-read on this subject the relevant chapters of Frank Browning and John Gerassi’s “The American Way of Crime”). Inevitably, the reformist unions and other professional associations quickly become a cog in the machines, and they naturally borrow their methods when it is a question of extending their private interest. The mechanism of the racket, laid out by the criminals of protection associations, becomes the general law when the unions voluntarily welcome gangsters who joyfully provide them with strong arm men and dynamiters. (The essential work on the question is unfortunately impossible to find: “Dynamite; The Story of Class Violence in America” by Louis Adamic, Viking Press, several editions in the 30’s. If one of our dear readers knows of a more recent edition I would be happy if he were to point it out. Thanks).

In the meanwhile, of course, Prohibition, accomplished on a national scale in 1919, placed the commerce of alcohol once again in the hands of organized crime. The latter would draw from this a tremendous increase in its financial and social power. But all this is well known. What we want to stress here – and which is also perhaps well known to our sagacious readers – is the way in which the lower classes of American society were socialized by gangsterism and political corruption.

It is upon this terrain that the roman noir is born and developed. It is this world that the roman noir critiques, either attacking it directly (as in the many stories of rotten cities) or by making it the explicit environment of its intrigues (Burnett is perhaps the author who best alternated between these points of view). And it is even this world which, in a large measure, determines the form, the style of the authors of noirs...

This same world, with its Mafioso system of organization, was prolonged through the 30’s and beyond World War II to today and spread throughout the planet. So that the roman noir, in the form in which it continues to exist and develop, is not a school exercise with rules established in the past that are now mummified. If it is in a certain measure threatened with insignificance it is because the current development of the Mafioso world is becoming perfectly well-known without suffering anyconsequences. But on the other hand, the roman noir never hoped to clean up society. For this it is necessary that men set to themselves in motion.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

