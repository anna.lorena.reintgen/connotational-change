      Introduction      What is an NGO?      Who finances the NGO sector (or: whose interests does it serve)?      Why do capitalists finance the NGO sector?      Why does the State finance the NGO sector?      Which “methods of struggle” are typical for the NGOs?      What is the structure of an NGO?      What are the motives of the NGO activists?      What to do?
There is a good reason why we, anarchists, would pay some attention to the Non-Governmental Organisation (NGO) sector, principally because the general public considers this “third sector” as a tool for democratisation of society, and even worse, as an alternative to the struggle against centralized authority, oppression, poverty and all the other collective misfortunes of the people. To us, this is yet another of the great deceptions of capitalism that serve to keep the illusion of choice and freedom alive. That thing about the elections — that they’d be illegal if they could really change anything — applies to the NGO sector as well, which, beside being tolerated and even encouraged by every government, is still believed to be a mechanism for struggle against the injustice, an antipode of the government and a platform for participation in the political discourse.

In opposition, we believe it has a very different role: to become a mediator between the government and the people, thus suppressing people’s anger; to convert the potential uncontrollable dissent into a calm, peaceful, legal, controlled, institutionalized and completely harmless discontent; to support capitalism, by creating a good image for the corporations and by propagating the ruling ideology; and finally, to create an illusion of struggling for a change.

The need to write a pamphlet like this one stems from our own great problem of constantly having to deal with the NGOs and their continuous efforts to institutionalize every single form of dissent we manage to organise in Macedonia, thus condemning all our efforts to a definite failure. We are aware, though, that the impact of the NGOs on the social struggle elsewhere, especially in the western countries, is different than here, and probably of a lesser scale. But although our conclusions in this text are largely based on our experiences here in Macedonia and wider in the Balkans, we believe that the general statements about the role of the NGOs on a global scale apply in every region.

The pamphlet has been issued by the anarchist group “Crn Blok” based in Skopje, Macedonia. (www.crnblok.blogspot.com)

The ruling classes would describe the NGOs (short for “non-governmental organization”) as voluntary, non-profit private organizations whose diverse activity aims towards change, support or promotion of different social issues. The NGO sector is considered to be the third sector (the first two being the government and the business sector) or the third factor to form the public sector and to impact the political and social policies. We, anarchists, rather think of it as an instrument of government and the capital.

Due to their diversity, NGOs are hard to define. Their only feature that clearly comes out of their name is that they are independent from the government, which is inconsistent with the fact that the majority of NGOs use government funds for at least some of their projects. In the efforts to classify NGOs, different types are specified: charitable organizations, service orientation NGOs, participatory NGOs, empowering NGOs, local, national and international NGOs etc. The foundations that exist to finance NGOs are a type of NGO as well.

USAID describes NGOs as private voluntary organizations, which is a problematic definition, since most NGOs are financed by governments and corporations, but also because it’s hard to say that an organization with professional paid personnel, from managers to field workers, can be called voluntary.

The term NGO first appeared in 1945 with the foundation of the UN, when the UN allowed for certain specialized international non-state agencies to obtain an observer status at its meetings. The oldest organization considered to be an NGO dates long before the NGOs took over both the developed and the developing world, when the international Red Cross was founded in the middle of the 19 century.

Researchers of the NGO sector differ operational from campaigning NGOs – just another of the many classifications, but relevant for this study. Operational NGOs are those who tend to reach short-term goals by various small projects, they waste great amount of energy chasing for grants, donations and other sources to finance each upcoming project, and their activities center around making a project in order to get some cash, rather than the other way around.

The other type, campaigning NGOs, usually emerge around a certain cause for whose promotion they organize various public events and demonstrations. This type of NGOs magically appear every time an organized form of public discontent emerges around certain problem of society, with the NGO (regardless of whether it existed before or was created because of this public discontent) adopting the cause of the discontent masses, and then trying to redirect the anger towards institutional activities, while standing as a mediator between the government and people’s demands, as a replacement for direct and spontaneous action. When all hope directs towards the NGO methods, the NGO will approach the usual institutional and exclusively legal methods of “struggle” (such as petitions, lobbying, seeking support from opposition parties, begging for help from international committees etc), and the moment the struggle moves from the streets into the institutions of the system, the cause may be considered as lost, and all the unprivileged can count on is crumbs or miserable compromise. No one can win on government ground except for the government, which is exactly the role of these NGOs: to calm the masses by taking over the cause and to make each demonstration of discontent harmless and with no chance of making any change.

Regardless of the great amount of categorization of the non-governmental organizations, in this text we will mainly focus on the largest part of the NGO sector: those NGOs who “fight” for change and have social and political goals.

The question about who finances the NGOs is very important, because it also brings us to the answers to other questions, such as: whose interests do they serve, which are their real goals and what is their role in society. The question about the financing of the NGOs also undermines the general opinion about the independence and the non-governmental character of the NGO sector.

The two main financial supporters of the NGO sector on a global scale are the corporations and the governments. A smaller amount of the funds also comes from religious organizations, mainly Christian, Muslim and Jewish, which, much like the state and the capitalists, also do that in order to strengthen their domination.

On a global scale, almost every corporation that you’ve heard of have formed their own foundation to finance the NGO sector: from Microsoft, Monsanto, Nike, Bosch, Western Union, Ford, Toyota, Intel, all the way to Starbucks. If the corporation does not have a grantmaking foundation, then its filthy rich owners certainly do, such as billionaires Bill Gates, George Soros, Rockefeller brothers, or even the queen of England. Being a “philanthropist” has become a part of the image of every ultra-rich capitalist, who we assume probably hopes that the exploited workers all over the world will want to hang him less if they knew that he threw a few dollars for the poor in Africa.

When corporations finance projects for helping the poor, the children, the sick and other vulnerable groups, or rather for protecting the environment, they do that in order to build an image of caring and responsible companies and use that image to encourage people to buy more from them and increase their profits. People, obviously, don’t mind the fact that, Apple, for example, exploits million and a half workers in the sweatshops throughout Asia, making them work up to 14 hour shifts and paying them a yearly salary that barely equals the price of a single iPhone; but that’s OK, because, obviously, they’re a caring company because they’ve once donated a few thousand vaccines for malaria.

What people also don’t seem to grasp is that with this short-term help, the corporations only keep the status quo in the undeveloped regions of the world, making them dependant on western help (which might and might not come this year, and certainly won’t get to all in need), while the imperialist forces continue to rob their resources, leaving them unable for any kind of development. By encouraging mercy in the form of charity and philanthropy, the NGO sector undermines the struggle for radical changes which might put an end to the causes of poverty, hunger, sickness, illiteracy and all the other misfortunes whose victims would not have to wait for the mercy and the occasional help by the western charity organizations.

Corporations don’t finance only NGO projects surrounding the “care for the community”, but they invest even more in spreading the “political” NGO sector, through which they can impose their own agendas for sustaining the status quo of the society as it is. Governments also mostly direct their funds towards this kind of NGOs, and in the case of Macedonia, a great amount of those funds serve the mass propaganda for entering the EU and NATO. These funds, along with the rest, all serve the top goal of Macedonia: to develop into a capitalist state of the western type.

Before answering this question, it’s important to underline that the entire NGO sector is one big machinery for an immense turnover of money. A great amount of the cash flow on global level passes through the channels of the NGO sector, through various grantmaking foundations, charitable programs and corporate money for donations. If we follow the capitalist logics, where in the allocation of recourses, part of the money goes to production costs, other part to investing in new production and a third part goes to capitalists in the form of profits, then financing the NGO sector is contrary to the capitalist logic, because it’s “unrecoverable expenses”, which are most avoided in every company’s policy. Then why would capitalists voluntarily give away part of their profits in order to finance other organizations? Propaganda says: because they’re philanthropists and they care for the community. Of course, every reasonable person knows that there is no such thing as a philanthropist capitalist and that no capitalist, since he lives and gets rich off of the blood and sweat of the miserable wage workers, cannot possibly mean any good to the community.

The answer is, the financing of the NGO sector has nothing to do with donating or philanthropy, but it is simply a way to strengthen the position of the business sector and to help it secure more future profits. In other words, financing the NGOs is an investment, and all financiers expect income from their investments. That income doesn’t come back to capitalists directly as profits, but in long term, it brings bigger and more secure profits for the big capitalists, which is how the NGO sector (promoting the “values” of today’s society) helps keep capitalism alive. When a corporation finances an NGO who fights for democratization and for economic liberties (as so many NGOs do), it will help the NGO to become an influential player who will help push the process of “democratization” of society, meaning deregulation of economy, meaning smaller control over the production, the monopolies, workers’ rights etc, which finally means bigger wealth for the big capitalists and even bigger misery for the working class.

This is just one example; the same applies to financing other types of “political” NGOs, which, at the end of the day, will thank their financier by increasing his already immense wealth, by worsening the position of workers even more and by strengthening capitalism who puts them in that position in the first place.

Besides from the corporations, a large amount of money used by the NGOs comes from government funds (i.e. from our taxes). Governments, much like corporations, expect the same thing in return: a stronger position for capitalism and the rich. Let us not forget that the sole purpose of the existence of the State is to protect the rich and their position in society, with the help of its mechanisms of repression and its monopoly on violence. Thus, it is expected that the State would financially support the work of a whole range of organizations whose activities help maintain the status quo of the capitalist society.

In order to stay strong and dominant so that it can properly serve the rich, the State must continually find new ways to impose its will and to justify its existence. With that whole “citizen’s activity” going on in the NGO sector (completely harmless for the government, of course), the State is given the chance to stage a soothing performance for the people, in which “citizens” can exercise their rights, participate in the policy making, socialize and identify themselves as free individuals. That is the great illusion of democracy, and the NGO sector helps keep it alive. That staged “mechanism of struggle” is not able to bring about any social change (nor was it created for such purpose), but certainly poses as such; so all those who believe that their demands can be reached through NGO activities, will most likely completely exclude all other methods of struggle (which are in fact effective and actually pose a threat to the government, but are unanimously rejected by the NGOs for being illegal). The government wins it all: a false mechanism of struggle, deceived, peaceful and obedient people and security for its dominion.

The NGO sector is in fact one of the many mechanisms the State is using to enforce its dominion and to justify its existence. But unlike the other such mechanisms, like public education or the mass media, which are designed for systematic propaganda and control over the people, the NGO sector has a slightly different role. Namely, every government is aware that, in spite of its endless efforts to keep its people in fear and ignorance, people are still basically rational and libertarian and that the tiniest spark could stir up their anger each time they realize they are in any way oppressed. The riot is the greatest fear of every government, but modern governments know that the violent suppression (‘police state’ style) of massive riots can only further ignite people’s anger and endanger the domination of the State even more. That is why the modern government has come up with a more subtle solution: instead of waiting for a riot to come and then deal with it violently, why not allow the masses to “fight” with a harmless weapon – that weapon being the NGO sector.

Each time an NGO is doing some activity aimed at social change – whether it is some demand to the government, or some criticism for government policies, or anything else – that activity is almost exclusively within the institutional frames. Institutional frames are those set by the State in accordance with the official laws that impose the need for every political demand to pass through the institutions of the State in a firmly specified bureaucratized manner so that they could reach the “authorities”, who will then decide whether they will do anything about the issue. The State ‘provides’ its citizens with a few bureaucratized, confusing, hardly accessible and completely useless legal instruments to defend their rights: complaints, petitions, citizens’ initiatives etc. Whether we say that the ruling elites created the NGO sector, or the NGOs appeared from below to answer the needs of the “citizens”, the epilogue is the same: the government is pleased with the NGOs because in their “struggle” they never step outside of those permitted institutional frames, which makes them no threat to the ruling class.

The NGO struggle is never on the streets, it’s never violent and it never aims at core issues in society. In other words, they’re not a threat to any government, but they manage to recruit all those unpleased and angry: those who might throw Molotov cocktails on the parliament building tomorrow, were already recruited by the NGOs, dressed in a suit, awarded with a wage and tied to a desk to conduct surveys and write reports, legal documents and smart articles for the “progressive” newspapers. Even if it was created with such intentions, the NGO can never go radical; it’s simply too occupied with filing financial reports to its funder and chasing for new funds for next year’s projects. A radical NGO cannot simply last, because he wouldn’t be able to find any funds to rely upon. An NGO can exist as long as there is someone to finance it, and no foundation, corporation or government would ever support radical progressive organizations.

It is in this way that each NGO is being conditioned that its work will be supported only if it doesn’t exceed the legal frames, and so its field of struggle remains very limited. That permitted field of struggle is one that cannot lead to any victory, since no real struggle is involved, but merely dialogue, cooperation and compromise with the authorities. The NGO is thus allowed to oppose certain aspects of the government (a certain law, decision, some specific injustice, a particular institution etc), but it will never struggle against authority in general. (The legal system has a way of preventing the existence of radical NGOs, by imposing that each formal organization must acquire a permit by the state before it’s founded. The law, naturally, forbids any type of organizations whose goal is a total liberation of the people or, as the law would put it “a violent destruction of the system”.) Not only do the NGOs not contribute to any essential change, but by complying with the legal system and by not resisting against authority, they effectively support the ruling of the political and economic elites, as they allow the government to rule in peace.

The system is simply created in such a way to be able to stay intact by any attack that might come from within; that is why no change can be reached by institutional means and that is why this oppressive system cannot be brought down by joining the parliament. What can be achieved are some small reforms, a comforting compromise and the deception of the individual that he/she’s done as much as he/she could. That same person would maybe think of different approaches to the problem, alternative methods of pressuring the authorities to fulfill his/her demands; he/she would maybe use far more effective means that would bring him/her closer to his/her goal. But that is where the NGO sector comes in to pull his/her chains, to remind him/her that we must follow the law, remain non-violent at any cause and avoid provoking the (otherwise benevolent) cops who are only doing their job.

The structure of each NGO is nearly identical to the structure of any enterprise or corporation: highly hierarchical. The NGOs borrowed even the division of labour from the capitalist economy: similar to the division of stockholders, management board and workers in a corporation, each NGO has its president, its council and its field workers (activists). Like any enterprise, each NGO inevitably has its accounting department (since, although “non-profit”, it works with large sums of money), as well as managers of various fields and projects.

Again, the legal system has thought of everything, so in pursuit of ensuring the inevitable hierarchy in the NGO sector, the law determines that one of the basic conditions for registering a formal organization is that it has a listed president or leader (“someone who can be held responsible”). In Macedonian laws, it is impossible to register a horizontal (or egalitarian) organization. Seems like the “freedom fighters” don’t mind taking orders from their superiors in the NGO; anyway, this is how the NGOs provide the state with yet another mechanism for training obedient, subordinated and consenting citizens.

The NGO sector in the world, as well as in Macedonia, is completely professionalized, which is why insisting on calling the NGO activism volunteering, instead of a profession, makes no sense anymore. Furthermore, the NGO sector is the third biggest employer on a national level, right after the business sector and the state. (There are such areas, like Kosovo, where the NGO sector employs far more people than the private business. Macedonia is not far from this scenario.) When the NGO activist spends a number of hours every day working in the organization, receives a salary for his work and doesn’t have a “real” job, it is clear that working in an NGO is a profession, like any other.

It is true, on the other side, that many NGO-affiliated activists are in fact volunteers, i.e. they don’t get paid for their efforts. But in any NGO, those are only the “young and inexperienced beginners” who actually do all the physical work that is relevant for the NGO (such as, conducting surveys in the streets, spreading propaganda materials, doing field work related to some campaign etc). A smaller number of activists (those “most dedicated and most experienced”) do get paychecks for their work, which usually consists of sitting on a soft chair in a private desk, monitoring the work of others. In other words, they get paid for bossing (like capitalists) and for contemplating about common problems in society (and doing nothing about it) – like politicians.

Of course, volunteers do not engage in NGO activities for free out of pure altruism. They too expect many benefits as compensation to their engagement, such as job-promising skills, working experience and valuable contacts to serve their future career. Some work hard hoping that they will ascend to a higher position in their NGO and start getting paid. In fact, a lot of people spend their adult lives as professional activists, but even more among them continue their career at another working position that they’ve acquired thanks to their NGO experience. Participation in NGO projects and volunteering as an activist are “virtues” that are proudly pointed out in any CV, and every company takes those activities into consideration very seriously when deciding to employ somebody. The reason for that is the companies realize that the more the candidate engaged himself into doing something for free, the more he/she will be expected to offer hard labour for low cash; his volunteering merely guarantees to the company that the candidate was successfully trained to become an obedient, hard-working and compliant worker.

Other NGO activists hope that their dedication, along with all the skills they learn in the NGO (speech, organizing, management) might bring them attention in the circles of the political parties (who also search for hard-working and submissive workers to help them seize or maintain power) where they might be recruited as a party staff and then perhaps even seize some profitable position in the public administration. This is especially the case in Macedonia, where political parties monitor very closely all forms of organized dissent, so that they could find fresh young and talented enthusiasts and recruit them in their ranks.

Careerism in the NGO sector is a logical result. The entire NGO sector, being exclusively pro-capitalist, actively praises and promotes all the revolting “values” of capitalism, such as competition, rivaling with coworkers, use of all possible means to acquire financial benefit, taking all kinds of shit from those “above” in the name of career advancement and sacrificing common needs for one’s personal benefit. Egoism, success, competitive spirit, selfishness, rivalry and a complete lack of solidarity are the “values” the NGOs are proudly taking from capitalism. The NGO activists are rarely ideologically oriented, for what drives them is not some desired vision of society, but their personal financial advancement. If they do follow some ideology, it’s the capitalist ideology.

To us who choose to resist the state oppression and the capitalist exploitation of workers is quite clear that the best methods to set ourselves free from our chains are those that are beyond government control. The only means that are truly effective are those who are rid of the mediation of the state institutions: direct actions. Simply said, each autonomous action people undertake for political and social goals without the mediation of a third party (politicians, parties, courts, union leaders, legal experts, NGOs etc) is a direct action.

Massive direct actions have shown to be most successful during the course of history, especially such actions as mass demonstrations, general strikes, occupations of working places and of state institutions, blocking big roads etc. Individual direct actions or actions of smaller groups of people are not as effective in resisting the authorities, but can often cause a strong symbolic effect which might serve as a trigger for future massive organized actions. For example, by attacking properties (banks, parliaments, institutions, corporations, offices of fascist parties etc) not only do we cause material damage to the big bosses, but more importantly, we also publicly express our revolt against those oppressive institutions and thus inspire many others who share the sentiment to organize as well.

Direct action means opposing to the idea that we are powerless to change the conditions that make us miserable. It means acknowledging the fact that no one from above won’t solve our problems and that we must fight for each change by ourselves. The representatives who promise to fight in our name not only do not care about our problems, but also they profit from our miserable conditions of living.

That is why we know that if we want to start struggling for a total liberation from the chains of the state and of our bosses, the last place we will go to is an NGO. We know that those institutional instruments the system offers us as means to protect our rights and make our lives better will only leave us stuck in the institutional labyrinths and empty-handed. Of course, that doesn’t mean that in various specific occasions we shouldn’t use the loopholes and the contradictions of the legal system in order to defend ourselves from any legal attacks. (As anarchists, we should be prepared for the possibility of having many problems with the law and of enduring long mental pressures in the courts and prisons as a way to break our spirit.) But neither the law, nor any other institutional “remedy” the state is offering us can be our main means of struggle against the state oppression and against our exploiters at work. We should remember that the law that (we think) is protecting us today, can be withdrawn over night if the elites find it too menacing to their privileged position. And most importantly, we shouldn’t forget that the laws are written by our rulers and therefore they only serve to protect them from us.

The NGOs, however, will continue to show up in every organized dissent and persuade us to join the institutional struggle. All of us who want to see some real effect from any struggle are obliged to reject their influence and to count only on our own power. The moment when an organized group will become such a threat to the government, that the NGOs – those worshipers of the law – will retreat from that group at their own initiative, that’s when we will know that we’re doing it right. An effective struggle is the one that is capable to shake the foundations of the oppressive system, and thus force the authorities to comply with people’s demands. An effective struggle is the one that makes the government to be afraid of its people, instead of the other way around. This cannot be achieved by petitions or lobbying.

Well then, what should we do? We should organize at work, every time we think the working conditions are no good. If our boss is cutting our wages, we should stop the work of the entire company until the boss, fearing for his profits, complies with our demands. If they want to take away our social benefits, we should organize in our community and show them that it won’t pass. If the state is adopting new and more repressive laws for the workers, we should organize a general strike and block the economy that is slowly killing us. A building built on bad foundations can’t be fixed with a new façade – it must be demolished so that a new and a better one can be built in its place.

In the end, direct action is more than just a method of defending our rights and improvement of our life. Direct action – as was put by the anarcho-syndicalist Rudolf Rocker – is a “school of socialism”, or a way to prepare ourselves for the free society we all strive for. Direct action gives us control over our own struggle, gives us experience and shows us how to learn from our mistakes; it helps us build a culture of resistance and solidarity, and – connecting us with so many others who are in the same shit – gives us back our humanity which was taken from us by the industrial society. And as we control our struggle, we will slowly learn to control our own life, and the more we approach freedom, the more will grow our power to change the world.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

