      Memories      Torment      Flight
To the friends of Nichilismo

“My youth was just a dark hurricane 
passed through here and there by brilliant suns;  
the lightning and the rain wreaked so much havoc, 
that few vermilion fruits were left in my garden.”

— Charles Baudelaire

In a distant spring, gleaming with green and sun, my youthful spirit wandered gently through the divine forests of the sky. One day, a sad day in autumn, it came back to me, disconsolate, weeping. A groups of Angels with large, black wings accompanied it silently. It told me: “God is dead! The great Pan is dead!” The Sun went dark, rivers filled with mud, and plants trembled. Darkness wrapped the Earth in her funeral shroud. Then at my back I heard the satanic thunder of a hellish laugh. It was the laughter of the for whom I had waited, perhaps unaware, for so long: the Demon. He told me: “Come with me!” He brought me into the corrupt city where the true sun has never laid its kiss.

Dance of specters. Darkness. Silence... Beside a temple built for the Goddess of Perversity and Knowledge, a Fountain of Blood gurgled, as if reciting a cursed prayer.

The Demon was somber and black like the tragic Night. From his yellow, phosphorous eyes a ray of sinister light flowed. Suddenly he told me “Goodbye!” and quickly vanished.

I cried out. I was alone in the deepest darkness. The Fountain continued to recite its cursed prayer. Did I tremble? Maybe. I don’t know... I don’t recall... Suddenly the gates of the temple of the Goddess of Perversity and Knowledge flew open and the Fountain of Blood transmuted into the beautiful body of a voluptuous young woman.

“I love you” — she told me — “and I want you. You have to be mine!” I looked into the depth of her eyes. I recognized her. She was the Image perceived through a morbid dream of Matter. A hellish mob blossomed from my mind, inspiring in me a Dionysian quiver.

“Matter is everything!” I forgot the place and time and tried to catch that naked and beautiful body so I could take it in my arms and grasp it tightly to my breast.

“No, not here!...” she told me. And taking my hot and feverish hand in her small, cool one, she led me through the flowery entrance of a cavern in which a group of young witches danced. We abandoned ourselves to a wild embrace, and my large, gaping, passionate mouth enclosed her tiny, beautiful lips in a bite. We closed our eyes. In the midst of so much darkness, I noticed that my mind was not dead, since it had never seen a vaster sea of light.

I don’t know how much time passed. I was suddenly roused by the roar of a funeral march echoing dismally from the bottom of the cavern. Laughing wildly, my companion told me: “The witches are dead. Our embrace killed them. I am avenged!” And saying this, she turned pale, stiffened and became a rock.

A young serpent with eyes of fire and a bloodstained mouth rose up before me: “You have brought death to my lovers and killed Love.” “And what does that matter?” I answered.

“I have known him a long time,” I continued, “this hypocritical, cynical and cruel God of yours. I have seen him insult and mangle so many of my sisters. I have seen him — like all other Gods — shedding blood, devastating brains, feeding on young hearts, always for his own lustful body, in a hot bath of tears.” The serpent bowed his head and said to me, “Look?!” and stuck his neck out at me. There he held the mirror of Life, upside-down. I looked into it and saw myself. In the mirror, I was nothing but a large skull. Thick black clouds lowered over my head. They were funeral hearses for my smothered dreams.

I saw my woman of stone nearly move.

Goblins loaded her onto a cloud that the wind drove into the heights and scattered in the distance.

Then the serpent’s mouth vomited blood and made the ground open under my feet. I fell into a bottomless abyss. Suspended in the void, I again saw the Demon.

“Brother, listen to me...” I said to him. And I whispered a few words in his ear.

Even he was pale, moved, and he answered me: “It is impossible to believe it. Ah, if I could do it!...”

Again my mind quivered. But then he suddenly burst out laughing. “What does it matter to you? Haven’t you seen the mirror of Life?” He brought me by an unknown path and led me back to the magnificent Earth to mock Man, the Overman, the Demon and God.

“I know, how much pain and sweat 
and baking sun it takes on the flaming hill
To engender my life and to give me soul”

— Charles Baudelaire

Man, Demon and God have come together to defile my virgin garden. (I don’t know why the Overman has not gotten there.) They are right in front of me like three perverse allegories. God tells me: “ I am the unattainable good to which you should aspire. Sacrifice yourself, deny yourself, and you will reach me.”

The Demon tells me: “I will give you happiness if you will worship me.”

Man tells me: “I am the Ideal of atheists. Be me.”

I laugh. I laugh, but my laughter is not calm.

I feel that I am not Man, that I do not worship the Demon, that I do not sacrifice myself on the altar of any God; and yet, I still don’t have the mathematical certainty of being my own I, the lord of my fantastic realm. This is my torment. When God tells me: “Killing is bad!”; when the Demon tells me: “Killing is necessary”; when  Man tells me: “Great is the one who dies for the Ideal”; I answer each one of them: “That’s not true!”

Someone knew that I loved conflict and said to me: “I have thousands of men with me, brave and valiant warriors, we will win. Come with us.” I asked him: “Why are you fighting?” “For the greatness of the Fatherland,” he answered.

“I have no fatherland.”

I met other men: “We know that you are a valiant warrior. Come with us. We will pour out our last drop of blood for the redemption of humanity.”

I answered: “I don’t believe in humanity, I don’t believe in its redemption.”

The group’s leader scowled and looked at me with contempt: “You are a coward!”

I laugh. But my laughter still is not calm. I feel something bitter inside me that torments me.

I feel something inside me that is so deeply intimate that I don’t know how to explain, that no one could ever explain. I feel within myself the UNSAYABLE!

It is my unique self, which no one knows. Is this perhaps my torment? Perhaps. Because perhaps it is my Happiness. Because perhaps it is the spring that quenches my thirst, that leads me to the final edge of the I which wants to expand itself and throb in the strong, vast spasm of the Everything, so as to dissolve triumphantly in the Nothing.

“Must one depart? Or stay?... If you can, stay; 
Depart, if you must.”

— Charles Baudelaire

My arrow is ready, my will is rejuvenated, my potency proved. How could I wait any longer?

Yes, I must depart. It is time, it is time!

Nihil, nihil!

Tormented, my mind flies. It flies to with the wings of Reality over the world of dreams, towards broader horizons, towards my eternity.

I can no longer dream, I am the dream of myself. The friend of my possible traveling companions.

* * *

Oh friends, oh friends, where are you?

Don’t you see, over there, the Face of Eternity and Mystery? It is necessary to unravel the final riddle of the eternal. Come on, friends, come, it is time, it is time!

...

Have you arrived?

I have never seen a sky as peaceful as your faces, oh friends.

How beautiful it is to understand each other.

* * *

We are on a frail boat, lost at sea. No more dawns, or dusks, or destinations. We have only sun, light, heat, depth and distance.

Do you hear? Eternity raises her most beautiful song to Life, as she demands of us the bridal rose garland. Oh friends, the roses, where are the roses?

* * *

What a poor, what a miserable thing the land where we lived was!

Do you still remember it, oh friends?

There golden dawns rose, but black nights fell...

There men dreamed of collective aims and measured time...

Ah, friends, friends, I am assailed by an immense pity for that poor land...

* * *

So what is happening to me?...

Let’s forget it! For how many thousands of years have we floated on the endless waves of this vast depth that raises us to the regions of the Sun, above the Sun?

And for how many thousands of years will we yet live?

Ah, jolly Eternity, eternal happy now!

* * *

May no one ever know the secret happiness that fills our solitary hearts, oh friends!

Have we not stoically suffered in forced silence?

No, no, may no one ever know our cruelest sorrows, nor the infinite happiness of this eternal noon.

In the grotesque old world, they now believe that we are dead.

And instead, we have married eternity, we — the loners!

— But the roses, oh friends? Where are the roses? Oh, red roses of Eternal Revolt!




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“My youth was just a dark hurricane 
passed through here and there by brilliant suns;  
the lightning and the rain wreaked so much havoc, 
that few vermilion fruits were left in my garden.”


— Charles Baudelaire



“I know, how much pain and sweat 
and baking sun it takes on the flaming hill
To engender my life and to give me soul”


— Charles Baudelaire



“Must one depart? Or stay?... If you can, stay; 
Depart, if you must.”


— Charles Baudelaire

