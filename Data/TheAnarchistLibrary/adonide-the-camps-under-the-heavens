
The tradition of the oppressed teaches us that the state of exception in which we live is the rule. We must achieve a concept of history that corresponds to this fact.

— Walter Benjamin

The concept of Rights is a huge apparatus that creates exclusion, that is based on exclusion, and yet the chorus of protest against every sort of exclusion merely demands rights, hoping that the heaven of Rights extends itself to newer and newer lands. In fact, democracy is conceived as this progressive conquest of newer and newer spaces. This is why it is not only defended, but also exported. The of legal acknowledgements must cover more of the possibilities and aspirations of individuals every day. An individual who has his rights is a citizen, which is to say a being who has the right of citizenship in the democratic City.

With the most varied intentions, many are waiting for a renewal of democracy.

From the peak of the ruins of the metropolises, one can see what is left of the so-called community of citizens. Political categories have been overturned by the events of the past century. The general will, nationality, the sovereignty of the people, all this is collapsing together with the nation-state that was its basis. The trinity, order-nation-territory, has been broken. The paid theoreticians of democracy have realized that it is necessary to dissociate the concept of citizenship from that of sovereignty. Sovereignty is still a sort of divine ordination, and the individual subjected to sovereign power is always a subject, while democratic ideology requires a secular power and authentic citizens. Since ancient Roman times, the sovereign is the one who can decide on the state of exception, i.e., who can create and suspend norms. He is the one who defines the political space that establishes and rules the norm, as well as the field in which this is temporarily approved (a temporariness over which he himself has the decisions). That this state of exception – of “extraterritoriality” with respect to Rights – is an essential component of sovereign power is shown not only by the fact that every city has its barbarians (its foreigners), but also by the opposition between the people and the population. The political is not the space that welcomes all those who inhabit it (or who are born in it, in accordance with the etymology of the word nation), but rather the zone of the subjects of the sovereign, of those whom the sovereign (and later the state) considers as its political body legally represented. The others, barbarians, foreigners, undesirables, live apart (within other borders or in wandering). They participate in Rights as its reverse side, as the Norm suspended (and yet material in the form of walls and fences). When democracy passes from the polis of adult, free and male citizens – as in classical Athens – to the model of sovereignty as representation of the masses (in the 17th century), only the internal colonies where those excluded from democratic universality, from the heaven of its rights, will change. The “people” will become the subjects of the nation-state (power itself, in the act of self-legitimization, will receive the investiture of “popular sovereignty”), still forming the mass from those who simply submit to power. The concept of the people has always had two distinct accepted meanings: it means both the political body, i.e., the citizens of a state taken as a whole, and the poorer classes (those which someone has called “ the toiling classes, the dangerous classes”). These two meanings can even blend in expressions such as “the Italian people” and “popular justice”, or “man of the people”, “popular quarter”, “popular uprising”. As the class that came to power due to a revolt of the masses, the bourgeoisie has based its entire ideology of popular sovereignty on the identification of the two meanings of the word “people”. It’s no accident that the universal declaration of 1789 is concerned with the rights of Man and of the citizen (in the sense that the first can only exist if he is recognized by the state as an inhabitant of its nation). The poor, excluded from all real decisions, are represented as subjects of rights. The legal fiction of the unity of the political body is opposed to the division of social reality. If in ancient Rome, for example, a clear separation existed between the people and the common people (plebeians), legally quite distinct; if in the Middle Ages as well inhabitants were divided on the basis of profession into the “common people” and the “great people”; with the bourgeoisie, the people – without distinction – became the sole depository of sovereignty. The life of the poor that, in its nakedness deprived of legal tinsel, was once entrusted to god, would later be included, in its basic exclusion, within the political body of the state. All forms of capitalism, in the west as in the east, have tried to make the real poverty of the people (“the people, the unfortunates, applaud me,” Robespierre used to say) disappear behind the mask of the People. Unfortunately, this ambiguity was accepted by the workers’ movement. The ugliest results were first the Leninist theses on oppressed nations and imperialist nations, and later the social nationalist ones of all the stalinisms (the Resistance of the Italian, Chinese, Vietnamese, etc. people, the governments of popular unity). Power has always known that the merging of the people with the People can only point to the end of both, i.e., the end of Rights. Well prepared by the laws that, from the time of the first world war, most European states enacted in order to de-nationalize a part of their citizens, the terrain on which nazism built was the radicalization of the distinction between human and citizen. The Nüremberg laws of 1935, which divided Germans into citizens with full rights and citizens without political rights, were anticipated by those of France in 1915, of Belgium in 1922, of Italy in 1926 and of Austria in 1933. From the naturalized citizens of “enemy origin”, to those responsible for “anti-national” crimes, to those “unworthy of citizenship”, one would reach the citizens who threaten the health of the German people (and to the Jews as a parasite people on the People). The concentration camps originate under the sign of “protective detention” (Schutzhaft), a legal institution already present in Prussian law and applied in a massive way in the first World War. It is neither a question of the extension of ordinary law, nor of that of the prison, but rather a state of exception and of a preventive application of martial law: in short, a police measure. When in March 1933, during the celebration of Hitler’s election to the chancellery of the Reich, Himmler decided to create a “concentration camp for political prisoners” at Dachau, this was immediately entrusted to the SS and, thanks to the Schutzhaft, placed beyond legal rights. The only document that attests that the genocide against the Jews had been determined by a sovereign organ is an official record of a conference in which a group of Gestapo functionaries participated on January 20, 1942. The extermination was so methodical precisely because it was realized as an immense police operation. But “anything was possible” against Jews, gypsies, homosexuals and subversives, since they had previously been deprived of civil rights and, before the extermination, of mere German citizenship as well. They did not belong to the People. As Robert Antelme wrote, they were solely naked members of the human species that the legal order refused to recognize as citizens.

Concentration camps – as the extreme expression of the state of exception and thus of sovereign power – is not a nazi invention. Nazism not only exploited the terrain that Stalinist counter-revolution prepared (social-nationalism that becomes national-socialism), but also expanded an institution of democracy into a technique for the production of death. The first concentration camps (actually described as campos de concentraciones) were constructed by the Spanish state in order to suppress the insurrection of the Cuban population in 1896. Concentration camps created by the English in the war against the Boers at the beginning of the 20th century followed quickly. Moreover, the legal formulation was present (and applied against subversives) in the constitution of the Weimar republic. The camp is a zone of exception that Legal Right creates inside itself. The rule of the camp participates in the Law under the form of absence. Nazism transformed the state of exception into a normal and permanent situation; it pushed the opposition of the concepts of people and population to the extreme in a process of differentiation, selection and extermination that led from citizens to subhumans, from these to inhabitants of the ghettoes, from prisoners to deportees, from internees to “Moslems” (this is how deportees who arrived a step from the end were described in the jargon of Auschwitz) and finally to figures (as the nazi machine, using bureaucratic euphemism, called corpses). Nazism wanted a Europe of peoples, of inhabitants worthy of citizenship.

And yet it never enters the minds of the democratic defenders of the rights of all the excluded that Legal Right itself might be the source of the exclusions, that the citizen will always have his reverse side in the barbarian, in the undesirable. Still distinct from nationality (from the registration of birth in the space of a sovereign power), citizenship can only exist beyond concrete individuals. And this does not change when the people with its sovereign will is replaced by the public with its opinions. The old identities and the old beliefs collapse under the weight of a social atomization produced by media domestication and bureaucratic administration (it is not mere chance that the concept of public defines both consumers and spectators), but the political body has increasingly restrictive and detailed norms. For the poor, citizenship is the uniform of the police or the card of the social worker. Their misery is only the other face of the existence of citizens, i.e., of voters and consumers.

If power comes into play in the relationship between regulation and localization, between coordination and territory; if the camp is the materialization of a state of exception that encloses men and women to whom only the naked membership in the species remains; then the stadiums in which refugees are crowded before being sent back home, or the “centers of temporary residence” (here it is again, the bureaucracy of euphemism at work) for undocumented immigrants; or again the “waiting zones” at French airports in which foreigners who petition for recognition of their refugee status are parked are camps as well. Besides, increasingly certain outskirts of the great metropolises are camps. All these zones (like others in which the wandering of misery is locked up) are non-communities of humans without quality, in which private and public life are undifferentiated under the sign of dispossession. The world of these enclosures without guarantees and without humanity frightens the democrats. They would like to see it under the heaven of Rights, covering that exception with the Norm that only lengthens its shadow.

Now that the wandering of the de facto stateless is again a mass phenomenon, the democrats would like to redefine the rights of citizenship. In the name of humanitarian politics, they would like a new status for refugees, ignoring the fact that all those that have existed up to now (the Nansen Office of 1921, the High Commissariat for Refugees in Germany in 1936, the intergovernmental committee for refugees in the same year, the International Refugee Organization of the UN in 1946, the High Commissariat for Refugees in 1951) have only caused the drama of millions of fugitives to be transferred into the hands of the police and the humanitarian organizations. That these two rackets are increasingly connected is shown by the official propaganda in times of war. If the state is taken literally when it describes the [1991] bombing of the Iraqi population as an “international police operation, in the same way, the havoc wreaked recently on Serbian and Kosovar populations must become “humanitarian operations”. The refugees in whose names the military intervention was justified are still forced into wandering or reconsigned (like the deserters from the Serbian army) to the police. The humanitarian organizations grow rich – one need only make one’s way into Albania to be finally convinced of it – in the shadow of poverty and extermination.

The democratic states now find themselves in need of rebuilding their political body without the parameter of nationality. But being citizens, even if in a redefined territory, will be the condition of a new People that harbors within itself increasingly technological projects of extermination of the poor classes. In the rule of the Economy and the State, entire populations are reduced to their bare membership in the human species, mere raw material for every sort of experimentation (productive, bacteriological, genetic, etc.). The power conflict provoked by the economic, administrative and scientific machine is that of appropriating – even legally – their own survival. The rest are entrusted to the police and the marketplace of humanitarianism.

It is on the scale of the entire world – and in the course of history – that democracy and its citizenship are judged. One will then see that the camps of infamy extend further and further around the Cities. Their exception is already the rule, their enclosures are the authentic face of the present.

Will the only solution indeed be that of raising our eyes toward the heaven of Legal Right again? Maybe opposing to the Europe of commodities and ID cards a “Europe of citizens and peoples”?




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



The tradition of the oppressed teaches us that the state of exception in which we live is the rule. We must achieve a concept of history that corresponds to this fact.



— Walter Benjamin

