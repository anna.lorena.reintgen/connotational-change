
Reversion to 1911 would constitute a perfect first step for a 21st century neo-Luddite movement. Living in 1911 means using technology and culture only up to that point and no further, or as little as possible.

For example, you can have a player-piano and phonograph, but no radio or TV; an ice-box, but not a refrigerator; an ocean liner, but not an aeroplane, electric fans, but no air conditioner.

You dress 1911. You can have a telephone. You can even have a car, ideally an electric. Someday, someone will make replicas of the 1911 “Grandma Duck” Detroit Electric, one of the most beautiful cars ever designed.

1911 was a great year for Modernism, Expressionism, Symbolism, Rosicrucianism, anarcho- syndicalism and Individualism, vegetarian lebensreform, and Nietzschean cosmic consciousness, but it was also the last great Edwardian year, the twilight of British Empire and last decadent gilded moments of Manchu, Austro-Hungarian, German, Russian, French and Ottoman monarchy; last “old days” before the hideous 20th century really got going.

The next step backward would be to join the Amish and other Old Order Anabaptists in 1907 — no telephones, no electricity at all, and no internal combustion. With this move, the battle would virtually be won. The next generation would be able to make the transition to no metal — the neo-neolithic. Arcadian pastoralism.

After that a dizzying sliding spiral back into — illiteracy. Oral/aural culture. Classless tribal anarchy. Democratic shamanism. The Gift. This would be the ultimate Luddite goal. But the first step will be back to 1911.

Those who long to live in 1911 choose that year — really any year from 1890 to 1914 would be equally OK — just because it’s safely in the middle of that long lingering last decade of the long 19th century, which was also the first heroic decade of true modern radicalism, e. g. the Wandervogl, Stirnerite anarchism, the IWW and Jim Larkin, Ascona, Sex Radicals, and Nudism, etc. And, still far removed from the future of total war and totalitarianism to come — a time of utopian revolutionary hope.

Also, it’s the age of decadence; the final year of the Manchu Dynasty; opium ten cents a bottle at any country store; the Paris of J. K. Huysmans. Gaslight. The last gasp of true agrarianism in the USA; the age of Populism, the Grange, Farmers Alliance — the last rural decade.

But there’s another reason we choose 1911 (or thereabouts) for our little Golden Age. It has to do with technology. In 1911, almost all the actual conveniences of modern technology already existed: the car, the electric bulb, the phonograph.

Now, we Luddites do not approve of cars or any of these inventions, which all subtract from the quanta of Imagination available to individuals and to the Social. But, we have to admit — they’re convenient.

In their primitive forms they’re almost likable. The only real convenience invented since then — the electric refrigerator — can be replaced by an Amish-built propane refrigerator, or, we could re-invent the ice-box. We hope some day to learn to sing again, but till then, we can accept a few hand-cranked shellac records (but no radio or TV). Computers are not in any way a part of a revived 1911, however. It’s time to wake up and smell the rot of technopathology.

The telephone easily corrodes social presence and reduces selves to disembodied “voices of the Unseen,” as the Arabs called this invention. But again the primitive version, with its party lines and snoopy local operators, had a social aspect now completely leached out of the medium. If we must be thus haunted let it be via one of these elegant sinister objects — large enough to be a real murder weapon.

Recorded music realizes a dream of pure magic, but at the same time the end and even the death of music itself. As the Muzak company understood, recorded music eventually loses its presence — and in its state of absence or deprivation it becomes a potent subliminal form of anxiety, often alleviated by a shopping spree or food binge — perfect Capitalist behavior.

Thus music becomes background; in expensive restaurants one is expected to listen (but not pay attention) to music appropriate to a honkytonk whorehouse: rock’n’roll, which should be a highly presential dionysiac experience becomes aural vanilla for jaded yuppies. Youth buys its latent rebellion from the world of commercial greed and adult condescension called the Music Industry.

With headphones and computers, everyone composes a soundtrack for their own stupid boring movie, their life as student or wage slave and consumer — music as anodyne for the constant immiseration (as the Situationists used to say) of Too-Late Kapitalismo.

Finally, recording replaces our own voices with dumbness. We let stars sing for us. We let machines come between us and the divine musician within us. Music attains Spectral status. It haunts us with its own non-presence reduced to residual noise pollution.

There is next to no amateur communal music anymore (recording killed it), no “music bees,” so to speak. Music now lacks all sociality except the ersatz of mass consumption at a concert or music festival, but at least it remains possible to hear live music sometimes. Usually, now, when I hear any decent live music, I burst into tears. I give it my attention — a process that produces a kind of high or rush.

If we have to hear a recording, let it be a 1911-style shellac disc or even wax cylinder, cranked up by hand, not electricity; a magic music box to baffle the dog with its master’s voice; a cabinet of aural marvels. If we have to be haunted by music’s non-presence (every recording is the tombstone of a live performance) let it be by one of those graceful ear-shaped or seashell-shaped machines, a Surrealist’s delight or Spirit Trumpet for a charlatanesque medium.

The years between the death of Nietzsche and Queen Victoria in 1900 and 1914, constitute a dawn of Modernism that never happened into day. Instead it was smashed to nihil by the one long war (1914–1989) of the ghastly 20th century. The liberte Libre of trends like Symbolism, Expressionism, anarchism/ socialism, lebensreform, Cosmicism, etc., turned into the cynicism of Dada, the fascism of Futurism, and so on. Hope seemed dead.

Even reading and writing is contaminated with Civilization’s technopathologies. Oral/aural culture would constitute the Luddite ideal. But as an isolated individual and lifelong print addict, I can’t give up books, that necessary poison, like certain drugs. Life in 1911 requires books just as it might ideally include cheap and legal laudanum or tincture of Indian hemp.

Charles Fourier praised the Pigeon Post. It seemed quite modern in 1830, “utterly modern,” as Rimbaud would say. In 1911, we’re allowed telegraph and even telephone, but our hearts still go into writing and receiving letters — handwritten, private, mysteriously brought to your very door by an unseen hand for only pennies per message, the money having been transformed into beautiful stamps.

None of these pleasures are afforded by electromagnetic CommTech, which eliminates everything (including privacy) except text and image.

Imagine perfumed letters sealed with red wax and heraldic imagery; letters like Prince Genji used to write, or Proust, who could send little blue notes by pneumatic post anywhere in Paris. Think of mail-order degrees in Rosicrucianism. Yes, the post — under the sign of Hermes — is sheer magic.

Full play of Imagination becomes possible only without modern technology, because it has become the heartless operation of Capital, which hates all forms of sharing. Let’s work for a secular Anabaptism, bold enough finally to refuse everything back to the steam engine — at least.

Whereupon we may resume human life.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

