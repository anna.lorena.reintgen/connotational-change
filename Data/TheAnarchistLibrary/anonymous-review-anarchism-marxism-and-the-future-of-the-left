
Anarchism, Marxism and the Future of the Left: Interviews and Essays 1993–1998

by Murray Bookchin

AK Press, Edinburgh and San Francisco, 1999

As Murray Bookchin’s latest testament to himself as one of the great thinkers of the 20th Century, this book could be more accurately entitled ‘Anachronism, Marxism and the Suture of what’s Left’. It is also his latest apoplectic rejoinder to the plentiful and vociferous critics who are apparently trying to secure our illustrious author an early grave.

The Great Debate is one of a number of things that make this book incredibly hard going. At every turn, potentially serious theoretical analysis of the role of technology, the apparent demise and possible future of the Left[1] and the current state of anarchism as a revolutionary social force is undermined by petty swipes at and less petty character assassinations of his critics — a vanguard of white male anti-civilisation anarchists based in the US.[2] He (and his critics) end up engaged in such tedious point scoring — focusing on details of ancient tribal life which professional anthropologists cannot agree on, for example — that much of their argument will be meaningless to most readers.

As befits the irritating pomposity of Bookchin’s approach ‘interviews’ appear throughout the book, interspersed with essays some of which have appeared in previous works by the same author and some of which are previously unpublished. Although the interviews do not read as such (they appear to be sentences put into the mouths of named others with a written reply by Bookchin) they are apparently genuine. However, we are left feeling we have been the victims of a crass attempt to inflate the readers’ opinion of Bookchin’s significance to the topics under discussion, by pretending that someone other than Bookchin is actually interested in what he has to say. A little unfair perhaps — I do think some of his opinions on the current state of anarchism and the danger of the reduction of radical activity to asocial “juvenile antics” have some validity, but not when presented as the frustrated railings of an old man against the new.

Bookchin is clearly not an anarchist — except by his own definition. Similarly “lifestyle anarchists” are only lifestyle anarchists by his definition, and no anarcho-primitivists I know aspire to primitivism in anything resembling lifestyle — unless you include not checking your email more than once a week! Amidst the contradictions characteristic of Bookchin’s thought when he’s not talking about the traditional Left, he unwittingly reveals himself to be just that — a Leftist who was renegade enough once to want to enrich the Left with ideas of non-hierarchical anti-statism rather than truly challenging the faith.

Indeed, the book is largely devoted to detailed discussions of the Left. ‘Part One: From Marxism to Anarchism: A Life on the Left’ is a cloyingly rose-tinted description of Bookchin’s youthful political education and activity. A solid grounding in communism as a member of the Young Communist Party gave way to involvement in the movement for racial equality and, from 1962, the Students for a Democratic Society (SDS). There is also a strangely vague and naive allusion to his ‘anarchist’ activity with the odd grandiose claim thrown in, suggesting, for example, that he invented the notion of non-hierarchical society, stating; “I was calling for social changes that were more comprehensive than the abolition of classes and exploitation. I was calling for the abolition of hierarchies as well, of states, not of economic power alone.” (p.55) This is characteristic of Bookchin’s approach throughout. He was everywhere and everything. Despite the best efforts of a century and more of anarchist and libertarian activity, it was not until Murray came along that anyone really got it.

‘Part Two: The New Social Movements’ is dominated by his essay ‘Whither Anarchism? A Reply to Recent Anarchist Critics’, a largely pedantic trashing of his critics and their ideas. The final chapter of the book ‘The Future of the Left’ describes Bookchin’s blueprint for a municipalist utopia: a step-by-step guide to revolution — a programme of city-based, peaceful, bottom-up social reform (this ‘inventor’ of non-hierarchical society doesn’t see that bottom-up is just top-down from another angle). Even if I agree with him that contemporary radical grassroots networks are probably not yet revolutionary in practice, his proposal for a new society does not become revolutionary just because it is practical. The former has the potential to be, and elements of Bookchin’s programme such as education and public self-management outside the State may contribute to revolution. His “democratised communities” sound very much like a misguided reworking of Marx’s misguided notion of encouraging a capitalist state in order to bring about the overthrow of capitalism.

A hindrance to clarity in the structure and content of this volume of essays is that Bookchin’s thinking is clearly encumbered with a sense of his own temporal advance. He is obsessed with history, criticism and the treating of ideas and events within their proper historical context. Is this an underhand way of asking us to treat him within his proper historical context? His notion of historical relativity is extremely dubious — it becomes an apology for anything, and as dubious as his arguments in favour of technology are. Aside from the fact that it is actually keeping him alive (much to the chagrin of Bob Black and Co.) Bookchin proposes that wishing to smash the hegemony of technology is arguing about “our attitude toward a situation that already exists” (p.286) and that to question whether something should exist when it already does is futile and that we should simply accept that it exists and work out how to use it better! Umm... so because capitalist society exists, there is no point wishing we didn’t live in a capitalist society? We just need to learn how to live in a better kind of capitalist society! After all, as he writes of classical Athens, a society cannot rise above itself. Of course, that’s not what Bookchin means — he is simply contradicting himself in that typical Bookchin way, hence when talking about the struggle against capitalist society, he writes that “if our capacity to rationally project ourselves succumbs to “what is”, then we become “realists” in the worst possible sense. We allow our thinking to bog us down in the pragmatics of what exists today.” (p.347)

The issue of technics is a big sticking point with Bookchin’s ideas and this seems to place him most firmly in the Leftist tradition rather than with any contemporary anarchist mores. Bookchin believes that technology is great — indeed, he believes technics is the answer to our problems. According to Bookchin, “we could even use genetic engineering... in such a way as to restore “wild” areas.” (p.286). That is — and this is the subtext — after the geneticists have fiddled with his age-inducing telomeres and have somehow managed to house his brain in a younger, healthier body. (Then perhaps he and his critics could find a dark alley and enjoy the fist-fight they so obviously need.)

The debate over technics is integral to Bookchin’s (tellingly Marxist) attitude to ‘first nature’ — that there is a non-human world which the human has a moral imperative to control and lead in the right evolutionary direction. How nature can be a ‘non-human’ world I don’t really understand but there you have it. I have a similar problem with a fully biocentrist attitude to the ‘human’ world. Bookchin writes that our “capacity to go beyond the animal level, to inquire about the future, to alter the world, to use language — all are fundamentally human attributes.” For Bookchin though, it doesn’t seem to follow that to “inquire about the future” may also include dislocating the concept of ‘future’ from civilisation’s odious progeny ‘progress’ nor that “to alter the world” can mean that further entangling ourselves in advanced technics is something we may decide not to pursue. But then the man is obsessed with ideas of progress and regress to which he attaches very conventional value judgements. The idea of living in a state of “everlasting immediacy” is sharply dismissed as living in a state of “asocial bliss” backed up by some spurious reference to the Lotus Eaters in Homer’s Odyssey! For such a rationalist, Bookchin’s reasoning often eludes me.

Bookchin makes so many patently ridiculous statements in this book that you could be forgiven for thinking he is one of those pathologically insane members of society that he repeatedly bars from any of his theoretical citizens’ assemblies. The notion of mental illness as a largely class-based symptom of a diseased society seems to have escaped his analysis, along with the idea that humans may have worked out that “we share a common humanity” before the emergence of the city state.

Beyond these criticisms I think Bookchin may have a point when he talks about the need for coherence and a politics beyond that of imagination and the liberation of desire. Just as Ward Churchill argued in Pacifism as Pathology[3] that non-violent action in, for example, the American civil rights movement did not achieve change without a simultaneous, grassroots, armed attack on the State, isn’t it likely that the contemporary radical grassroots direct action network challenging life as we know it cannot effect much more than cosmetic social change (and a more liberatory way of life for the few radical but not yet revolutionary networks and individuals in the West), without the collateral existence of a strong popular resistance?

I disagree fundamentally with much of what Murray Bookchin has to say — about technics, the workability and desirability of city society and confederalism, the arrogance of Bookchin’s belief in humanity’s stewardship of and superiority over “first nature”, and his uncritical belief in progress. But whether or not you agree with him, his ideas are much better articulated in his other books. His ideas on coherence, or lack thereof, do strike a chord as we flounder into the 21st Century. As we face the risk of being driven deeper underground, there is a possibility that cynicism and pessimism could come to play a corrupting or paralysing role in informing what Bookchin refers to as “adventurism”, but which I would call “fight”, as we experience a more profound sense of how high the walls are that are closing in. Unfortunately, even Bookchin’s more valid points are overwhelmingly diverted into mutually debasing personal diatribe in this and recent works. This is a collection of memoirs and rejoinders masquerading as historical and theoretical essays of worth. Bookchin is an anachronism — and he is as removed from the movements of today as he is from his glory-days in the earlier part of the twentieth century. This volume is desperately trying to make his position clear (reneging pathologically on previously held opinions) and to have the last laugh at his critics. I think some of Bookchin’s work is worth having a look at, but this is definitely not one of them.

 
[1] To subject yourself to the catfight between Bookchin the Anarchist and Black/Clark/Watson/Zerzan et al the anarchists, the following few books might be useful: Social Anarchism vs. Lifestyle Anarchism — An Unbridgeable Chasm by Murray Bookchin (AK Press, Edinburgh and San Francisco, 1995) ISBN 1-873176-83-X, Beyond Bookchin — Preface for a Future Social Ecology by David Watson (Autonomedia, New York/Black and Red, Detroit, USA, 1996) ISBN 0-934868-32-8 and Anarchy After Leftism by Bob Black (CAL Press, Columbia, USA, 1997) ISBN 1-890532-00-2 — as well, of course, as this book and numerous articles and letters in various US anarchist publications, especially Anarchy — A Journal of Desire Armed.
[2] Throughout the Great Debate the terms ‘the Left’ and ‘Leftist’ are used in a rather vague way as some sort of catch-all insult. I think it is something slightly nebulous, but nonetheless accurate. For me it describes a political perspective that has a faith in progress (production), technology and the city. All these require a citizenry defined by its relationship to a highly structured social process on an institutional scale, and who engage in abstracted activity. Defined almost solely by its relationship to itself, Bookchin’s society is without question of the same ilk as that which currently devastates the world — caught up as it is within its own fucked up and circular logic. Bookchin offers no critique of civilisation and does not challenge our relationship to nature in any meaningful way. Without revolutionising ideas on civilisation and the natural world it is not possible to smash hierarchies and the State — and so depart from our current way of life. His support of progress, highly developed technologies and of the urban environment distinguishes him as someone who speaks more from a traditional leftist perspective than a contemporary anarchist one.
[3] Pacifism as Pathology — Reflections on the Role of Armed Struggle in North America by Ward Churchill with Mike Ryan and an introduction by Ed Mead. (Arbeiter Ring Publishing, Canada, 1998) For a review of this book see Do or Die No.8, pp.305–30.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



Anarchism, Marxism and the Future of the Left: Interviews and Essays 1993–1998


by Murray Bookchin


AK Press, Edinburgh and San Francisco, 1999

