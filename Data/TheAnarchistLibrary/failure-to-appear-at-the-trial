    Kafka as Romantic Anarchist    Kafka on Capitalism and the State    A Neck in the Noose    In the Empty Courtroom
“The Revolution evaporates, and leaves behind only the slime of a new bureaucracy. The chains of tormented mankind are made out of red tape.”

- Franz Kafka

The Literature of Franz Kafka cannot be reduced to a political doctrine of any kind, yet a libertarian sensibility is inscribed into the heart of all his novels, which can be read as darkly comic parables (or even prophecies) on the nightmare of modern bureaucracy wedded to the chilling madness of totalitarianism. When Kafka speaks to us of the STATE, it is in the form of “administration” or “justice” as an impersonal system of domination, which crushes, suffocates, or kills individuals. One of the most important ideas suggested by Kafka’s work, bearing an obvious relationship to anarchy, is the alienated, oppressive and absurd nature of the “normal” legal and constitutional state, the impenetrable and unintelligible system where unfreedom prevails.

It’s no accident that the word and concept “Kafkaesque” has taken hold in this society’s vocabulary, for in his uncanny writing Kafka succeeds in capturing an existential condition unique to modernity and for which “social scientists” did not yet have a term: the dehumanizing effect of a reified bureaucratic apparatus which undoubtedly constitutes one of the most characteristic and defining phenomena of techno-industrial civilization. In the internal landscape of Kafka’s political allegories, a profound insight is applied into the way the bureaucratic machine operates like a blind, self-replicating network of gears in which the relations between individuals become an abstract, independent object. This is one of the most revolutionary, topical and lucid aspects of Kafka’s extraordinary and terrifying prose.

Editors, critics, and biographers have made available (in numerous languages) just about every traceable manuscript and scrap of information related to Kafka. Innumerable interpretations of Kafka’s dense, allegory-laden writing (symbolic, Oedipal, Freudian, existential, post-modern) are easily found by those with an interest in the subject, but the central theme of his novels and short stories – “depersonalization” and an anonymous, administrative Control Network – make decoding the obscure depths of meaning in Kafka’s literature a worthwhile and relevant mental exercise for anarchists (particularly anti-civilization anarchists).

And in fact, recent biographies have unearthed evidence that Kafka’s involvement with the anarchist movement of his time was more direct than ever previously suspected.

“If one is inquiring into Kafka’s political leanings, it is, in fact, misleading to think in terms of the usual antithesis between left and right. The appropriate context would be the ideology which Michael Lowy has labeled “romantic anti-capitalism”, though “anti-industrialism” might be more accurate, since as a general outlook, it transcended the opposition of left and right.”

-From Kafka, Judaism, Politics and Literature, by R. Robertson

Kafka’s interest in the concept of anarchism (also known as “libertarian socialism” in the early 1900’s) began with his friendship with the Czech anarchist, Michal Mares, who invited Kafka to several anarchist meetings and demonstrations. Mares recounts Kafka’s attendance at these gatherings and his interest in the ideas of anarchists such as Peter Kropotkin and Michael Bakunin, in which a critique of modern capitalism is presented as a rejection of institutionalized politics in favor of a society based on a community level without any intervening administrative structures. But Kafka’s “anarchism” manifested itself as an anti-socialist critique of bureaucracy based on his own experiences with the Workers Accident Insurance Institute where he worked as a lawyer. Kafka’s skeptical attitude towards the “organized labor” movement and all political parties and institutions is perhaps best expressed in this description of marching workers from his Diaries: “There are the secretaries, bureaucrats, professional politicians and all the modern sultans for whom they are paving the way to power.”

This statement is typically anarchist because of its emphasis on the authoritarian character of the system and not solely on economic exploitation, as in Marxism. This critique of bureaucracy also links Kafka’s intellectual perspective with that of anarchists such as Gustav Landauer, who had developed a similar critique of socialism. Accounts by three Czech contemporaries document further Kafka’s sympathies for the anarchist struggle and his participation in various libertarian activities. Kafka evidently attended meetings of the “Mlodie Club”, an antimilitarist and anti-clerical group, and in the course of 1910-1912, took part in anarchist conferences on free love and the Paris Commune. Kafka also took part in protests against the death sentence of the Spanish anarchist thinker and educator, Francisco Ferrer, and in 1912, was arrested for protesting the death sentence given to the anarchist Liabetz in Paris.

Kafka’s interest in anarchy is also evident from his readings – he devoured books by Jean Grave, Proudhon, Godwin, Tolstoy, Emma Goldman and Benjamin Tucker and occasionally handed them out as gifts. Max Brod, Kafka’s main biographer, describes him as a “meta-physical anarchist not much given to party politics” – a definition that seems very much on the mark. For while Kafka may have had ties to the Prague libertarian underground, he was never a “joiner” and the anarchist inspiration that runs through Kafka’s novels was not the product of any political doctrine but originated from a state of mind and critical sensibility whose principal weapon is irony and humor, that black humor which surrealist Andre Breton called “a supreme revolt of the spirit.”

“Capitalism is a system of relations of dependence where everything is arranged hierarchically and everything is in chains.”

– Franz Kafka

In the gruesome dreamscape of Kafka’s painfully powerful literary technique, the towering edifices of modernity plaster a horrid uniformity on every facet of the environment – and every facet of the environment becomes an element of the individual’s imprisonment. The sadism and brutality that underpins the system is drained of all justification by Kafka’s armory of radical metaphors, which portray the market hegemony of Capital as a masked institution of rotating deception, a claustrophobic, alien power structure lined with mysterious filing cabinets... where every corridor seems to go on forever. Capitalism is represented as a society trying to merge all its citizens in a bland, numerical hierarchy, it’s massive technocracy dwarfing the lives of the “well-adjusted” workers – who have had their souls ripped out and are hemmed in by only vaguely understandable social roles.

Kafka’s master treatise on Capital’s commodification of life would have to be his magnificent short novel, The Metamorphosis, in which Gregor Samsa, a lowly traveling salesman (and sole bread winner for the Samsa family) wakes up one morning to find himself transformed into a giant insect. Gregor is so concerned about the financial implications his metamorphosis will have that nearly all thoughts of himself are pushed from his mind as he begins to formulate strategies of adaptation to capitalism’s demands. When he is completely unfit to leave his room and reenter the harsh competitive world, Gregor still wants nothing more than to make it to work! He is a slave to time, painstakingly “living” his days in accordance with train and bus schedules.

Gregor’s transformation is viewed more as an economic tragedy than a personal one. Now unable to make money and support the family, Gregor becomes completely insignificant. A drain on his family’s income and resources, an unproductive member of the capitalist system, Gregor is of no monetary use to society and is thus viewed (like many insects) with disgust. In Kafka’s “paranoid” subjective cosmology, the human being is reduced to the condition of a wind-up mannequin, a macabre caricature of a free individual. A system of identification, classification and observation has produced an endless procession of clones – all dressed the same, performing virtually identical tasks, and thinking the same thoughts – linked together without the faintest thread of historical memory. As the proletariat clones beget proletariat Copies of themselves, the original subject’s identity is fragmented even more and lost to a multiplicity of disconnected, irreconcilable social roles.

“Kafka had only one problem, that of organization. What he grasped was our anguish before the ant-hill state, the way that people themselves are alienated by the forms of their common existence.”

– Bertolt Brecht

Like his friends among the Czech anarchists, Kafka seemed to consider every form of state, and the state as such, to be an authoritarian and liberticidal apparatus founded on lies. Kafka’s blackest (and arguably, his most important) novel, The Trial, turns the law into a cryptic metaphor of the world order, where the myriad masks of officialdom take on the (non) appearance of an ever-present supervisor, and mental torture and mind-scrambling are the routine tools of authority. The law is invisible, yet invincible. Charges are never laid; defense is banned; bureaucracy is all-enveloping; the judges unknown and ranked in near infinite hierarchy. With its eerie shadows, it’s unseen tormenter, and its aura of voyeurism, The Trial has the quality of a modern nightmare where the main character is the sinister State machine itself.

In Kafka’s The Trial, the nature, meaning and function of the law all remain mysterious; it is a labyrinth from which there is no escape. Indeed, the law may even be imaginary. There is no certainty. Yet, as a supreme authority, it paradoxically embodies justice, truth, knowledge and power, while its operatives combine the methods of a Total State with almost supernatural forces. The novel’s protagonist, Joseph K., cannot fathom the complexity of this impenetrable chain of command, which scrutinizes and manipulates his every move. The State is everywhere and nowhere and just like the piped muzak in a supermarket, you cannot switch it off!

Despite their hapless, petty and sordid characters, the bureaucrats in The Trial are only cogs in this abstract, paternalistic machine. Inasmuch as this is what the novel explores, a great deal of its significance gets lost in translation. In particular, the title of the book is misleading, for although the English “trial” has more than one meaning, the connotations of the word are different from those of the German one. A more exact translation of Der Prozess, would be “procedure” but which (in German, the language Kafka wrote in) also has undertones of “entanglement” and even “muddle”. Joseph K.’s trial is thus an in-depth study of the grotesque turmoil of getting hopelessly entangled in the System’s “process”.

Kafka’s critique of the State touches upon several issues that are of vital significance to anarchists. One is that the inner workings of the State are controlled by procedures which remain shadowy even to those carrying out its orders. Another is that the autonomous, mechanical bureaucratic system is being transformed into an end-in-itself. A passage from Kafka’s novel The Castle is exceptionally illuminating in this regard:

“One might say that the administrative organism could no longer put up with the strain and irritation it had to endure for years because of dealing with the same trivial business and that it has begun to pass sentence on itself, bypassing the functionaries.”

As the economic and political systems of the world are now locked into one vast coalescing governing system, we can see that Kafka was discussing the pattern of the future in his writing, the swallowing up of humanity by an enigmatic, unyielding bureaucratic process. This monstrous sovereign structure has now attained an almost unqualified supremacy over the lives of its subordinates, reproducing itself even in revolutionary movements and coercing global obedience to it’s own inscrutable logic, rationalism and framework.

As long as this system remains abstruse and incomprehensible to anarchists, its omnipotence is assured. Kafka’s writing is subversive in that it reveals power structures hidden within power structures, implying that these power structures can be targeted and overcome if they are first understood. Anarchists need to demystify the state and its seemingly unbounded control over us, so that we can begin shattering this old order to pieces!




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“The Revolution evaporates, and leaves behind only the slime of a new bureaucracy. The chains of tormented mankind are made out of red tape.”


- Franz Kafka



“If one is inquiring into Kafka’s political leanings, it is, in fact, misleading to think in terms of the usual antithesis between left and right. The appropriate context would be the ideology which Michael Lowy has labeled “romantic anti-capitalism”, though “anti-industrialism” might be more accurate, since as a general outlook, it transcended the opposition of left and right.”


-From Kafka, Judaism, Politics and Literature, by R. Robertson



“Capitalism is a system of relations of dependence where everything is arranged hierarchically and everything is in chains.”


– Franz Kafka



“Kafka had only one problem, that of organization. What he grasped was our anguish before the ant-hill state, the way that people themselves are alienated by the forms of their common existence.”


– Bertolt Brecht



“One might say that the administrative organism could no longer put up with the strain and irritation it had to endure for years because of dealing with the same trivial business and that it has begun to pass sentence on itself, bypassing the functionaries.”

