      The Path I Know      Land, Indigenous People, Anarchy      A Proposal for Understanding Place      Destroying the Third-Position      Individual and Collective Self-Interest
“If non-indigenous anarchists are to develop ways of interacting with indigenous peoples that are different from those of political organizations they must begin from direct communication, solidarity and trust. Anyone who really wants to act in solidarity with others does not stumble around inside their homes, uninvited, stinking of arrogance and ignorance, and taking up space. It should go without saying that cultural differences and the unique experience of colonization should be understood and respected.

The old racist and inaccurate idea of the “noble savage”, which a few petty anarchist philosophers still hold on to, is in need of a complete demolition. As mentioned earlier, there are substantial variations between indigenous nations and communities in terms of their internal social structure. So a generalized model can’t match up with reality.

Real solidarity can be put into practice through direct contact with the indigenous sovereignty movement, and attacks on common enemies — using the principles of direct action, self-organization, and constant struggle.” - Insurgent S, Colonization, Self-Government and Self-Determination in British Columbia, 2003

I have lived nearly my entire life on the traditional lands of the Sḵwx̱wú7mesh, xʷməθkʷəy̓əm and Tsleil-Waututh peoples. For this, it is customary to thank the surviving members of these peoples and their ancestors, but I am unsure how I would accept such gratitude in their shoes. Since the beginning of contact with europeans (diverse groups of people from the same sub-continent as my ancestors), the overbearing trend has been the horror of domination and genocide. Thankfulness, in this context, seems like adding insult to injury more than anything.

Still, the land and mountains are beautiful. The water is clean to drink, and I have thought for a long time of how beautiful, and bountiful the earth, forests, creeks and oceans must have been here, when people were not living in a relationship of domination over the earth, before the British and other europeans came and imposed the terror of capitalism, colonialism and the state. I am thankful to be aware of this, and to understand the task of undoing it all.

As a non-indigenous person living under the weight of capitalism, I have wondered since childhood who I am, and what I am to do. Since a young age, I have clashed with authorities, from schools to Christian ideas and police. As a working class child living in a densely populated area of a suburb, I was bound to form relationships with some of the people from the local Sḵwx̱wú7mesh and Tsleil-Waututh reserves. This has always been a part of how I understood the world. It was always clear that here, people were categorically oppressed by the system, and that the misery I felt under the weight of society was hardly even close to the experiences of many Sḵwx̱wú7mesh and Tsleil-Waututh. I do not, nor have I, nor will I ever identify with the colonizer, the police, the bosses, the bureaucrats, the rich fucks, any of them, and since this has lasted me to my thirties, I see no reason why this will change before my dying days.

At some point before my mid-twenties I became aware of the anarchist movement and I decided that this best represented my tension with this world. Soon after I moved to a part of town, not far away, where I thought I might be able to find other anarchists and engage with the social movements happening in the area. At this time my understanding of anarchy was something more similar to an anarcho-syndicalist point of view with a heavy emphasis on atheism, which saw the workers taking over the means of production and running the economy in their own interests.

In Vancouver, indigenous people are generally at the forefront of movements that represent some level of class conflict. Many of these urban native people are not from the local reserves or peoples, but are from many different places in the geographic area known as “Canada”, pushed off of the land by colonization and industrialization and forced into the Eastside of Vancouver. I am thankful to say that the experience of being in the streets, and witnessing ceremonies with these people caused me to change my archaic view of the world and how I saw anarchy. It became clear to me that this industrial hell is for no-one, and that bringing forth an industrial utopia, worker self-organized or otherwise, would likely end up in the same result of colonial oppression and domination that was causing the misery I already knew so well.

I recently took a trip across this continent and back to the european sub-continent. In some of the places I visited, I intended to see the lands some of my ancestors came from. Having forged close relationships with indigenous people in struggle here, it seemed necessary to think about my own relationships to land and ancestors, a perplexing subject for someone who's ancestors haven't been indigenous in a very, very long time [1]. These are feelings and facts that I am still grappling with, and may be for a while longer.

Another important aspect of this journey was meeting and having conversations with anarchists from the sub-continent about a variety of struggles and ideas. I spent time in London, one of the centres of the hell of domination that covers most of the earth. I also spent time at a ZAD (zone of defense) occupation in western France [2], as well as among comrades in Athens, Berlin and a number of other places having many interesting conversations.

The autonomous zone in France was a land occupation to stop an Airport from being built. People had fought off police attacks and were forging a life without police and state intrusions, while trying to mediate between many different participants in the land struggle with vastly different ideas and motivations. An interesting observation that my travelling partner had was how even here, where you had people casting off the shackles of industrial development, there was a massive disconnect from the land and ecosystems. The best idea offered for reclaiming the land was a pastoral activity, with hay farming and keeping agricultural livestock. They had reclaimed the land and put it in common, yes, but there was no proposal as to whether they would allow the forest to reclaim any area and find ways to live with the ecosystems of the earth. This was a common sight along the “european” anarchist landscape; people there are so far removed from any concept of indigenous life and the wild spaces of the earth, that it is very hard for them to comprehend these possibilities.

In Athens and Berlin, I had some of the conversations that helped motivate me to write this article. The comrades I talked with described to me a general distaste for the idea of stolen or ancestral lands, and were displeased that anarchists would lend their solidarity to concepts and struggles that they saw as inherently authoritarian. The conversations were both extremely frustrating and refreshing. In my own context, for better or for worse, we often do not question such subjects. While it makes sense given our experiences on these lands, it is perhaps not fitting of anarchists, and stops us from pushing further in our goal of the liberation for all people.

“The idea that the state will inevitably reemerge over time is another of these hopelessly eurocentric fantasies in which Western culture indoctrinates people. Dozens of indigenous societies around the world never developed states, they thrived for thousands of years, they have never surrendered, and when they finally triumph against colonialism they will cast off the impositions of white culture, which includes the state and capitalism, and revitalize their traditional cultures, which they still carry with them. Many indigenous groups have experience going back hundreds or even thousands of years of contact with the state, and at no point have they voluntarily surrendered to state authority. Western anarchists have much to learn from this persistence, and all people from Western society should take the hint: the state is not an inevitable adaptation, it is an imposition, and once we learn how to defeat it for good, we will not let it come back.” - Peter Gelderloos, Anarchy Works

Indigenous groups and individuals are as diverse as one can imagine. Some groups are traditionally hierarchical and had created vast, highly structured civilizations prior to contact. Others are hierarchical and created semi-sedentary, semi-feudal societies. Many others are non-hierarchical, or very limited in top-down structure.

Among all these peoples there is also vast difference in the level of bureaucracy used in maintaining social and religious relations between individuals, clans, tribes, and neighbouring peoples. Some were more individualistic, whereas others have a more collective identity.

There is also some difference in how each particular european empire impacted these groups. For example, what time and technological level these european empires were at when contact began has had an effect on how intact traditional cultural structures are within each people. It also has an effect on the level of recuperation versus naked repression and Christianization that can be seen in relation to modern colonial power structures.

I am making these points not with the intention of building a patronizing anthropological thesis of indigenous peoples, but instead to deconstruct grand sweeping declarations of who people are. To make generalizations for the positive or negative of whole groups of people, has the effect of erasing people and furthering the colonial project.

The conquest of the “Americas”, as well as of the entire globe, and its unique groups and individuals has been a very long process. Zig Zag, an indigenous warrior, who has been involved in the anarchist movement on this continent since the 1980's, has described colonization as a “war for territory”. Since what colonial power structures need is access to land, resources, and exploitable populations, indigenous peoples are marked for annihilation and assimilation. At the heart of indigenous struggles, and in fact, their very existence, is the land on which indigenous people live.

When the European powers, and civilizations before them, came to occupy land, they had to first kill-off or subjugate the people who lived there. This is the common thread in how this horrible world came to be. As anarchists, we feel a deep hatred for these circumstances. Since the word anarchy came to be, we have thrown ourselves with an admirable recklessness at the nation-state in our desire to destroy it. We have seldom cared (nor should we ever have) whether the state takes the form of capitalism, socialism, democracy, fascism, mercantilism, nor even if it comes out of a compromised national liberation struggle.

Indigenous people are diverse and have many ways in which they relate to a state. Some may choose a more reformist route, choosing to use a capitalist framework with how they relate to their lands. Others do what most working-class and subjugated peoples do: just try to survive and get along. What I have been most inspired by as an anarchist, is those who oppose the intrusions of the state into their own free ways of life. These people often practice the use of warrior societies in opposing state and capitalist projects on their territories. The people themselves are unique individuals who may have differing views, but one common thread I have noticed is that these people are often heavily linked to the traditional ways of life of their peoples. These people are unfortunately often a minority in their communities, but they have held on to much of what colonial society has tried to rip from them. I have also noticed that what these people usually fight for is not a relationship of domination over vast groups of people in the form of a nation-state, but to freely recreate with others the forms of freedom and control over their own lives that their ancestors enjoyed.

“...and what I've studied about anarchy, is anarchy wishes for social order, but not at everyone else's expense. Not at anyone else's expense. No one else should feel degraded because you're comfortable. Everyone is equal, you organize horizontal... traditional societies are no different. Yes this is a traditional hierarchical system, there is a chief, there is women chiefs, there is children of chiefs. I am born into nobility myself, my mother is a chief, my father is a chief, but that does not mean that I can't be an anarchist. It means that I am looking at that traditional hierarchical system that is also sick. My father is on a decolonization path himself, and I'm not going tell myself that I'm decolonized. I've freed my mind, I've kept a free mind, I'm still impacted, I am not decolonized. Now why I say that is because settler society also must get a sense of what decolonization is, and you're on that path as anarchists. You've taken that step to decolonize.

And how does that relate to traditional societies? In traditional societies you ask permission to be on the land. In our territory, in our camp, you went through a protocol, but it wasn't police standing at the bridge, telling you, you have to ask us for a right to be here, we didn't say that. We stood there very, very openly and welcoming, but stern. Not cold, not really warm, but just... “I'm not going to get erased, I'm not going to get bulldozed, I'm not going to get railroaded”. But at the same time “I'm thankful you're here, this is the protocol we're going to go through first, before you enter the territory”. Not just to say you need permission first, which (traditionally) was actually part of it, you're asking the chiefs permission to be on the territory. But what you were asking was not just to be there, like rights, but how can we share responsibility to be on the land. Sharing responsibilities, sharing the (natural) law, self regulation, to me that totally relates to anarchy.” - Mel Bazil, Gitxsan and Wet'suwet'en, Transcending Rights

“The movement is in our blood, not in your hierarchy” - Callout for Oglala Lakota Territory Liberation Day 2015

Imagine a house.

Imagine that house encompasses a vast ecosystem.

Uninvited, you wake up in that house. Unsure of how you got there. Amnesia makes it hard to remember who you are. You realize that something isn't quite right with what you’re being told about that house's history.

You also come to realize that there are people who are at the bottom of a hierarchy that has been set up in the house, these people have a greater knowledge of the house for what it is, it's ecosystems etc. They also have some hints of a much more communal and egalitarian way of relating to each other in that house. It is clear that in this house, all are forced to rely on structures and resources that maintain that hierarchy in order to survive, while this group of people have a traditional knowledge of how to thrive and live without these structures. This house in undeniably theirs.

The masters of that house threaten you with violence if you don't keep your head down and work. In this position you are allowed more free-passage through some rooms and hallways in the house, but you remain deeply restricted and in many ways suffocated.

Indigenous and Non-indigenous anarchists must destroy the masters of this house and the structures they have set up, uninvited guests though we often are. We also have much to be thankful for, that we have the examples of our indigenous comrades and hosts of this house, in how we can live freely and respectfully in this house and others.

When comrades from the european subcontinent reject the idea of ancestral lands, I don't believe it is because they are desiring the continuation of colonial oppression of indigenous peoples. One position these comrades seem to be arguing is more of a militant multiculturalism, one that places the freedom of individuals in the highest regard, regardless of their place of origin and circumstances of birth. I do have affinity with this position, but I believe it misses some important points in relation to living on lands stolen from indigenous peoples.

When a person or group is placed at a lower level in a hierarchical system, they are then forced to conform to a dominant culture. As an anarchist, I have a problem with the idea that people would need to compromise their diverse ways of being for the benefit of a dominant whole. In the context of a white-supremacist society that intends, through colonialism, to strip people of their diverse ways of being, specifically those that show us an alternative to the hell that we know, it isn't surprising that anarchists lend their solidarity to indigenous rebels, with an aim to break from that dominant culture themselves.

There is of course a major problem here, one identified by at least one of the comrades I talked to on the subcontinent who rejected the concept of stolen lands, and one that anarchists and others would be foolish to ignore. Nazi's, various Nazi spinoffs, conservative nationalists, and many others attempt to argue similar positions to that of indigenous struggles; they argue that their lands are being invaded, by bankers, foreign governments, or immigrants, and they argue that there is a dominant culture that is forcing them towards multiculturalism, accepting immigration, unlearning homophobia, allowing birth control, etc.

Third Positionism is a neo-fascist tendency. It advocates for a break from marxism and capitalism alike, and seeks to create alliances across “racial separatist” lines. Out of this tendency has come the absurd idea of “National Anarchism”. Secessionism is a common theme in this tendency. Secessionism refers to pulling away and declaring independence, which in the eyes of a fascist would mean racial independence. While indigenous sovereigntists want a separation with colonial culture, it would seem clear, though perhaps easy for some to confuse, that they are not arguing for white-supremacist categorizations of separation such as “all people from Europe are white, white people must stick with white people, all people from Africa are black, black people must stick with black people.”

Attackthesystem.com is a neo-fascist website, with the tagline “Pan-anarchism against the state, pan-secessionism against empire.” It appears to have very little of a base in actual social movements, but has contributors from around the world. Deceivingly, they have pictures of a number of classical anarchists on the header to their website. Their writers are not only white fascist rejects and “anarcho-capitalist” wingnuts; for example Vince Rinehart (Raven Warrior) is a Tlingit traditionalist who also contributes to the website. While we are talking about only one known individual, it is not impossible that other indigenous traditionalists hold similar views, and it is possible that if anarchists are not careful with how they interact with indigenous sovereignty movements, they could be creating anything but anarchy.

Although not directly related to indigenous sovereignty, we also know that Nathan Block (aka “exile”) and his partner Sadie, both former Earth Liberation Front prisoners, have become third-positionist fascists. They now live in Olympia, WA, and Nathan himself has posted all kinds of esoteric fascist symbols and quotes on his website. One can read about this by searching through the NYC antifa website. Anarchists and many other revolutionaries are not immune from turning in a completely different direction, even if, and especially when they remain radical.

Fascism will use anything to gain momentum. If socialism is a popular sentiment, they will brand themselves “National Socialists” in order to gain adherents. At present, the ecosystems of the earth are collapsing, and western rational thought derived from Christianity is seen as a fundamental part of the problem, while power and control are decentralizing through social media, and mass surveillance. It is a clever ploy that fascists around the world are latching onto labels like “autonomous-nationalism” and “national-anarchism”, and that adherents to these positions are advocating for a focus on the land and ecosystems. As is standard for fascists they also propose a largely mythical connection to the past and ancestors. Anarchists must be careful that we are always critically minded and not guilted through privilege politics or wooed by hip occultism and environmentalist symbols and scenes, or anything else, into accepting any kind of authoritarianism. A native traditionalist who argues for racial separation and supremacy may not bring us much closer to liberation than a bonehead fascist.

I have been honoured in the last few years with what I have heard from many of my indigenous comrades. Although I can be seen as a person from a population that committed genocide against their people and that continue to occupy their lands, some of them are still willing to see me as a comrade in struggle. They have challenged me to think about anarchy in a way that does not only come from a western worldview. They have challenged me to be more spiritual in how I view my struggle, and have occasionally challenged me to look into my own origins, that I can only vaguely access. I often think it is foolish, and even self-destructive, how much territory anarchists and leftists leave to the fascists when we do not explore these ideas, when we allow pre-christian ceremonies and symbols to become fascist ones, but we have much to be careful of, and much to reject.

My ancient ancestors were not white, or Aryan or any of that nonsense. Whiteness was eventually created as european empires needed to begin exploiting non-european populations, and needed the slaves of their nation to join them in that cause. If we are ever to shake off domination and exploitation, and destroy America, Canada and every nation-state once and for all, I do not intend for white people to exist any longer. I intend to honour my indigenous comrades in their search for their own traditional lives. I intend to create a community with all others, where we can collectively ensure our free individuality and diverse traditions, for however many generations they last and for new ones to begin to flourish organically. This greatly differs from a purely separatist solution, in that we all have a chance to become something greater than the possibilities that have been allowed to us since the various processes of colonization swallowed up our ancestors and took away their great wealth of traditional knowledge and ability to live in co-existence.

One comrade beautifully described to me that they do not see themselves as a “european”, instead as someone who was “born accidentally in a shit-hole of the South Balkans” and that they were unwilling to recognize any ancestors except those who since the dawn of time strived for freedom against all forms of domination, regardless of where they were born. I relate to this, on practical and spiritual levels. I feel strongly for their rejection of a european identity, as it relates to my desire to destroy a concept like whiteness. I too feel that individuals must always have agency. A desire, on individual and collective levels, to break all ties to what is horrible in this world, is of the utmost importance for rebellion. But at the same time I do not expect all oppressed people to simply hear my declaration and accept me as an equal in struggle. I do not expect them to care; I intend to show them, as I move through this life however I can, that I mean it.

On this continent the politics of privilege and the idea of the ally are very popular in social struggles, even among anarchists, and especially around the subject of indigenous solidarity. In the mainstream we see hipster non-profit workers and others, pick up on this disempowering line of thinking. This approach completely rejects the experiences of individuals in emphasizing the experiences of oppressed groups from which individuals are tokenized for legitimacy in struggles. I believe the anarchist relationship to individualism has much to offer in breaking from such a patronizing path.

I do not want to be insensitive towards the people who I believe are the minority within the anti-oppression forum. I know that many of these people are deeply passionate about wanting to end all oppression within public and private spaces, forever. But I cannot in good conscience see my comrades and others go down such a troubled, dead-end road without sending caution to them.

In the last few years a number of texts have circulated that have heavily criticized anti-oppression, allies, privilege politics and the non-profit industrial complex [3]. These critiques have been wonderful to circulate and discuss with comrades younger and older, but they often gloss over the reality that there are a number of people who are from the grassroots, not associated with any non-profit institution, that carry forward the same manner of thinking and sometimes act as their institutional counterparts. The problem here is that these comrades do not fall into the non-profit industrial complex and so they believe the criticisms don't apply to them.

Privilege politics treats people as identifiable categories that can be explained with in a sentence. These categories can then be characterized by one group or individual who visually represent said category. The job of the ally is to take these credible voices and put them on a platform (rightly in a sense) above pompous academics, guilty whiteys, condescending liberals, etc.

The ally is the selfless martyr who is overcoming their privilege and stepping down to help the oppressed. They will deny it, but this is fundamental to their position.

The problem is that no-one acts out of total selflessness. Even our most selfless acts as human beings are often out of a need to be at peace with our conscience, intergenerational self-preservation, or ego. There is nothing wrong with this; the monster, I believe, is created when we deny this fact. If we cannot even be honest with ourselves, then how can we ever be honest with others, especially when we have such a condescending relationship to these “others”?

The consequence in social movements is often a parasitic relationship where one behaves as though they have nothing to gain from their selfless acts and instead is building up an egotistical reputation on the struggles of these others. I believe there is a dire need for everyone to be honest with who and how they are engaging in struggle. If you do not share a common enemy with another, then what actual basis do you have for a relationship of struggle? This can be combated simply by people finding their own individual and collective reasons to struggle against a common enemy, a common enemy that the politics of privilege will not allow someone to acknowledge.

Within the context of a social movement no one would deny that socialization and social hierarchies cloud our vision, leading us towards destructive behavior and complicity with oppression. But there is often (not always) a self-righteous air about those who engage in anti-oppression politics that I find particularly hard to stomach. The general sentiment of these “allies” is that everyone is stupid and wrong, and they need to educate or force others to believe that they and their ideology are right.

It seems clear to me that people throughout history have rarely needed to be sat down and formally educated on why they must struggle against this world. Did the rioters of the black liberation struggle in the states need to go through a bureaucratic process for how to fight against oppression, to sit with gut-wrenching guilt and sorrow, pondering their privileges first? Can we say the same of the gay and trans revolutionaries of the 70's in New York? The maroons in Brazil, Jamaica, and the Dismal Swamp in Virginia? The indigenous societies and their warriors who fought against Babylon, the Celts against the Romans, and those who carry on with this struggle today?

The alienation we experience under capitalism keeps us all too confined for anarchists to not look at our relationships and actions as opportunities for expansion. It would be a bare minimum to provide whatever resources we can to those we have affinity with. Relegating ourselves to the role of supporter or “ally” will do no one any favors when expressing support to indigenous camps or responding to attacks by the state against those who are categorically oppressed. I am only “in the way” if I am disrespectful to those I move forward with, the same as if I am not moving forward myself. The state is already my enemy until death and beyond, when I reach out to those who it attempts to destroy, I am trying to strengthen all our struggles. For us to ever have a chance of unsettling ourselves we must be unrested and unruly, never without initiative.

What has been the largest driving force behind the most powerful, inspiring, and liberatory struggles has been a recognition on individual, and collective levels that we must, ourselves, fight for freedom. We need to be wary of having our struggles compromised and capitalized upon by authoritarians of all kinds, and finding our own reasons and purposes in this struggle will help us towards this goal. To prevent our struggles for liberation from conceding to power and control, before we have a chance of breaking them. To prevent our social war, with its infinite battlefronts, from being told to sit back and introspect.

I do not intend for these observations, gained through years of pain and joy, trial and error, to stay stuck within these pages. I am not writing this from a place of having figured out every detail, but I do see many holes in the way people are engaging with themselves and others. I hope this essay will contribute to a more serious outlook and practice as we move through our struggles and lives which carry heavy consequences for both the positive and negative.
[1] By indigenous, I mean those who have an intimate knowledge of the lands that they inhabit, and that get much of their identity as a person from their experiences and relationships on and with these lands. An ancestral connection to the land is common, and very important.
[2] ZAD refers to a social movement that exists in France today. They are land occupations that halt development in a number of places, and in the midst of these occupations attempt to set up anti-capitalist communitarian relationships in the form of autonomous zones. The one described in this article is at Notre-Dame-Des-Landes, and is often referred to as “La Zad.”
[3] The Bricks we Throw at Police Today Will Build the Liberation Schools of Tomorrow, Three Non-Matriculating Proletarians, 2009/ They Can't Shoot us All, Anonymous, 2010/ Lines in Sand, Peter Gelderloos, 2010/ Who is Oakland, Escalating Identity, 2012/ We Are All Oscar Grant (?), Unfinished Acts, 2012/ Ain't no PC Gonna Fix it, Baby: A Critique of Ally Politics, Crimethinc, 2014/ Accomplices Not Allies: an Indigenous Perspective, Indigenous Action Media, 2014




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“If non-indigenous anarchists are to develop ways of interacting with indigenous peoples that are different from those of political organizations they must begin from direct communication, solidarity and trust. Anyone who really wants to act in solidarity with others does not stumble around inside their homes, uninvited, stinking of arrogance and ignorance, and taking up space. It should go without saying that cultural differences and the unique experience of colonization should be understood and respected.


The old racist and inaccurate idea of the “noble savage”, which a few petty anarchist philosophers still hold on to, is in need of a complete demolition. As mentioned earlier, there are substantial variations between indigenous nations and communities in terms of their internal social structure. So a generalized model can’t match up with reality.


Real solidarity can be put into practice through direct contact with the indigenous sovereignty movement, and attacks on common enemies — using the principles of direct action, self-organization, and constant struggle.” - Insurgent S, Colonization, Self-Government and Self-Determination in British Columbia, 2003



“The idea that the state will inevitably reemerge over time is another of these hopelessly eurocentric fantasies in which Western culture indoctrinates people. Dozens of indigenous societies around the world never developed states, they thrived for thousands of years, they have never surrendered, and when they finally triumph against colonialism they will cast off the impositions of white culture, which includes the state and capitalism, and revitalize their traditional cultures, which they still carry with them. Many indigenous groups have experience going back hundreds or even thousands of years of contact with the state, and at no point have they voluntarily surrendered to state authority. Western anarchists have much to learn from this persistence, and all people from Western society should take the hint: the state is not an inevitable adaptation, it is an imposition, and once we learn how to defeat it for good, we will not let it come back.” - Peter Gelderloos, Anarchy Works



“...and what I've studied about anarchy, is anarchy wishes for social order, but not at everyone else's expense. Not at anyone else's expense. No one else should feel degraded because you're comfortable. Everyone is equal, you organize horizontal... traditional societies are no different. Yes this is a traditional hierarchical system, there is a chief, there is women chiefs, there is children of chiefs. I am born into nobility myself, my mother is a chief, my father is a chief, but that does not mean that I can't be an anarchist. It means that I am looking at that traditional hierarchical system that is also sick. My father is on a decolonization path himself, and I'm not going tell myself that I'm decolonized. I've freed my mind, I've kept a free mind, I'm still impacted, I am not decolonized. Now why I say that is because settler society also must get a sense of what decolonization is, and you're on that path as anarchists. You've taken that step to decolonize.


And how does that relate to traditional societies? In traditional societies you ask permission to be on the land. In our territory, in our camp, you went through a protocol, but it wasn't police standing at the bridge, telling you, you have to ask us for a right to be here, we didn't say that. We stood there very, very openly and welcoming, but stern. Not cold, not really warm, but just... “I'm not going to get erased, I'm not going to get bulldozed, I'm not going to get railroaded”. But at the same time “I'm thankful you're here, this is the protocol we're going to go through first, before you enter the territory”. Not just to say you need permission first, which (traditionally) was actually part of it, you're asking the chiefs permission to be on the territory. But what you were asking was not just to be there, like rights, but how can we share responsibility to be on the land. Sharing responsibilities, sharing the (natural) law, self regulation, to me that totally relates to anarchy.” - Mel Bazil, Gitxsan and Wet'suwet'en, Transcending Rights



“The movement is in our blood, not in your hierarchy” - Callout for Oglala Lakota Territory Liberation Day 2015

