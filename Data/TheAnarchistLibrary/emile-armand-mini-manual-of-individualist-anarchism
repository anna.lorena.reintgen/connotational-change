      I      II      III
To be an anarchist is to deny authority and reject its economic corollary: exploitation — and that in all the domains where human activity is exerted. The anarchist wishes to live without gods or masters; without patrons or directors; a-legal, without laws as without prejudices; amoral, without obligations as without collective morals. He wants to live freely, to live his own idea of life. In his interior conscience, he is always asocial, a refractory, an outsider, marginal, an exception, a misfit. And obliged as he is to live in a society the constitution of which is repugnant to his temperament, it is in a foreign land that he is camped. If he grants to his environment unavoidable concessions — always with the intention of taking them back — in order to avoid risking or sacrificing his life foolishly or uselessly, it is because he considers them as weapons of personal defense in the struggle for existence. The anarchist wishes to live his life, as much as possible, morally, intellectually, economically, without occupying himself with the rest of the world, exploiters or exploited; without wanting to dominate or to exploit others, but ready to respond by all means against whoever would intervene in his life or would prevent him from expressing his thought by the pen or by speech.

The anarchist has for enemy the State and all its institutions which tend to maintain or to perpetuate its stranglehold on the individual. There is no possibility of conciliation between the anarchist and any form whatever of society resting on authority, whether it emanates from an autocrat, from an aristocracy, or from a democracy. No common ground between the anarchist and any environment regulated by the decisions of a majority or the wishes of an elite. The anarchist combats for the same reason the teaching furnished by the State and that dispensed by the Church. He is the adversary of monopolies and of privileges, whether they are of the intellectual, moral or economic order. In a word, he is the irreconcilable antagonist of every regime, of every social system, of every state of things that implies the domination of man or the environment over the individual and the exploitation of the individual by another or by the group.

The work of the anarchist is above all a work of critique. The anarchist goes, sowing revolt against that which oppresses, obstructs, opposes itself to the free expansion of the individual being. He agrees first to rid brains of preconceived ideas, to put at liberty temperaments enchained by fear, to give rise to mindsets free from popular opinion and social conventions; it is thus that the anarchist will push all comers to make route with him to rebel practically against the determinism of the social environment, to affirm themselves individually, to sculpt his internal statue, to render themselves, as much as possible, independent of the moral, intellectual and economic environment. He will urge the ignorant to instruct himself, the nonchalant to react, the feeble to become strong, the bent to straighten. He will push the poorly endowed and less apt to pull from themselves all the resources possible and not to rely on others.

An abyss separates anarchism from socialism in these different regards, including there syndicalism.

The anarchist places at the base of all his conceptions of life: the individual act. And that is why he willingly calls himself anarchist-individualist.

He does not believe that all the evils that men suffer come exclusively from capitalism or from private property. He believes that they are due especially to the defective mentality of men, taken as a bloc. There are not masters because there are slaves and the gods do not subsist because some faithful kneel. The individualist anarchist loses interest in a violent revolution having for aim a transformation of the mode of distribution of products in the collectivist or communist sense, which would hardly bring about a change in the general mentality and which would not provoke at all the emancipation of the individual being. In a communist regime that one would be as subordinated as presently to the good will of the environment: he would find himself as poor, as miserable as now; instead of being under the thumb of the small capitalist minority of the present, he would be dominated by the economic ensemble. Nothing would properly belong to him. He would be a producer, a consumer, put a little or take some from the heap, but he would never be autonomous.

The individualist-anarchist differentiates himself from the anarchist-communist in the sense that he considers (apart from property in some objects of enjoyment extending from the personality) property in the means of production and the free disposition of the product as the essential guarantee of the autonomy of the person. Being understood that that property is limited to the possibility of putting to work (individually, by couples, by familial groups, etc.) the expanse of soil or the engine of production indispensable to the necessities of social unity; under condition, for the possessor, of not renting it to anyone or of not resorting pour its enhancement to someone in his service.

The individualist-anarchist no more intends to live at any price, as individualist, were that as exploiter, than he intends to live under regulation, provided that the bowl of soup is assured, clothing certain and a dwelling guaranteed.

The individualist-anarchist, moreover, does not claim any system which would bind the future. He claims to place himself in a state of legitimate defense with regard to every social atmosphere (State, society, milieu, grouping, etc.) which would allow, accept, perpetuate, sanction or render possible:

a) the subordination to the environment of the individual being, placing that one in a state of obvious inferiority since he cannot treat with the collective ensemble as equal to equal, power to power;

b) the obligation (in whatever domain) of mutual aid, of solidarity, of association;

c) the deprivation of the individual and inalienable possession of the means of production and of the complete and unrestricted disposition of the product;

d) the exploitation of anyone by one of his fellows, who would make him labor on his account and for his profit;

e) monopolization, i.e. the possibility for an individual, a couple, a familial group to possess more than is necessary for its normal upkeep;

f) the monopoly of the State or of every executive form replacing it, that is to say its intervention — in its role as centralizer, administrator, director, organizer — in the relations between individuals, in whatever domain;

g) the loan at interest, usury, agio, money-changing, inheritance, etc., etc.

The individualist-anarchist makes “propaganda” in order to select individualist-anarchist dispositions which he should have, to determine at the very least an intellectual atmosphere favorable to their appearance. Between individualist-anarchists relations are established on the basis of “reciprocity”. “Comradery” is essentially of the individual order, it is never imposed. A “comrade” which pleases him individually to associate with, is one who makes an appreciable effort in order to feel himself to live, who takes part in his propaganda of educational critique and of selection of persons; who respects the mode of existence of each, does not encroach on the development of those who advance with him and of those who touch him the most closely.

The individualist-anarchist is never the slave of a formula-type or of a received text. He admits only opinions. He proposes only theses. He does not impose an end on himself. If he adopts one method of life on one point of detail, it is in order to assure more liberty, more happiness, more well-being, but not at all in order to sacrifice himself. And he modifies it, and transforms it when it appears to him that to continue to remain faithful to it would diminish his autonomy. He does not want to let himself be dominated by principles established a priori; it is a posteriori, on his experiences, that he bases his rule of conduct, nevertheless definitive, always subject to the modifications and to the transformations that the recording of new experiences can register, and the necessity of acquisition of new weapons in his struggle against the environment — without making an absolute of the a priori.

The individualist-anarchist is never accountable to anyone but himself for his acts and gestures.

The individualist-anarchist considers association only as an expedient, a makeshift. Thus, he wants to associate only in cases of urgency but always voluntarily. And he only desires to contract, in general, for the short term, it being always understood that every contract can be voided as soon as it harms one of the contracting parties.

The individualist-anarchist proscribes no determined sexual morality. It is up to each to determine his sexual, affective or sentimental life, as much for one sex as for the other. What is essential is that in intimate relations between anarchists of differing sexes neither violence nor constraint take place. He thinks that economic independence and the possibility of being a mother as she pleases are the initial conditions for the emancipation of woman.

The individualist-anarchist wants to live, wants to be able to appreciate life individually, life considered in all its manifestations. By remaining master meanwhile of his will, by considering as so many servitors put at the disposition of his “self” his knowledge, his faculties, his senses, the multiple organs of perception of his body. He is not a coward, but he does not want to diminish himself. And he knows well he who allows himself to be led by his passions or dominated by his penchants is a slave. He wants to maintain “the mastery of the self” in order to drive towards the adventures to which independent research and free study lead him. He will recommend willingly a simple life, the renunciation of false, enslaving, useless needs; avoidance of the large cities; a rational diet and bodily hygiene.

The individualist-anarchist will interest himself in the associations formed by certain comrades with an eye to tearing themselves from obsession with a milieu which disgusts them. The refusal of military service, or of paying taxes will have all his sympathy; free unions, single or plural, as a protestation against ordinary morals; illegalism as the violent rupture (and with certain reservations) of an economic contract imposed by force; abstention from every action, from every labor, from every function involving the maintenance or consolidation of the imposed intellectual, ethical or economic regime; the exchange of vital products between individualist-anarchist possessors of the necessary engines of production, apart from every capitalist intermediary; etc., are acts of revolt agreeing essentially with the character of individualist-anarchism.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

