
The term "affinity group" is the English translation of the Spanish grupo de afinidad, which was the name of an organizational form devised in pre-Franco days as the basis of the redoubtable Federación Anarquista Ibérica, the Iberian Anarchist Federation. (The FAI consisted of the most idealistic militants in the CNT, the immense anarcho-syndicalist labor union.) A slavish imitation of the FAI's forms of organization and methods would be neither possible nor desirable. The Spanish anarchists of the thirties were faced with entirely different social problems from those which confront American anarchists today. The affinity group form, however, has features that apply to any social situation, and these have often been intuitively adopted by American radicals, who call the resulting organizations "collectives," communes" or "families."

The affinity group could easily be regarded as a new type of extended family, in which kinship ties are replaced by deeply empathetic human relationships—relationships nourished by common revolutionary ideas and practice. Long before the word "tribe" gained popularity in the American counterculture, the Spanish anarchists called their congresses asambleas de las tribus—assemblies of the tribes. Each affinity group is deliberately kept small to allow for the greatest degree of intimacy between those who compose it. Autonomous, communal and directly democratic, the group combines revolutionary theory with revolutionary lifestyle in its everyday behavior. It creates a free space in which revolutionaries can remake themselves individually, and also as social beings.

Affinity groups are intended to function as catalysts within the popular movement, not as "vanguards"; they provide initiative and consciousness, not a "general staff" and a source of "command." The groups proliferate on a molecular level and they have their own "Brownian movement." Whether they link together or separate is determined by living situations, not by bureaucratic fiat from a distant center. Under conditions of political repression, affinity groups are highly resistant to police infiltration. Owing to the intimacy of the relationships between the participants, the groups are often difficult to penetrate and, even if penetration occurs, there is no centralized apparatus to provide the infiltrator with an overview of the movement as a whole. Even under such demanding conditions, affinity groups can still retain contact with each other through their periodicals and literature.

During periods of heightened activity, on the other hand, nothing prevents affinity groups from working together closely on any scale required by a living situation. They can easily federate by means of local, regional or national assemblies to formulate common policies and they can create temporary action committees (like those of the French students and workers in 1968) to coordinate specific tasks. Affinity groups, however, are always rooted in the popular movement. Their loyalties belong to the social forms created by the revolutionary people, not to an impersonal bureaucracy. As a result of their autonomy and localism, the groups can retain a sensitive appreciation of new possibilities. Intensely experimental and variegated in lifestyles, they act as a stimulus on each other as well as on the popular movement. Each group tries to acquire the resources needed to function largely on its own. Each group seeks a rounded body of knowledge and experience in order to overcome the social and psychological limitations imposed by bourgeois society on individual development. Each group, as a nucleus of consciousness and experience, tries to advance the spontaneous revolutionary movement of the people to a point where the group can finally disappear into the organic social forms created by the revolution.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

