      Strategy      Tactics      Weapons      Order of Battle      Snapshots
“Ons Danse le Lachrymo…”

Graffiti, France, July 2016 (transl. “We Dance the Teargas”)

“Comrade, will you watch these while I throw one?” He is tall, masked from head to toe in black, and is known to me. As he speaks he motions to a milk crate stuffed with Molotovs.

“Sure…go ahead,” I say as I light a cigarette and settle in to guard the precious weapons stash while he tosses the thing at the Social Enemy. Ten minutes later he returns and in spite of the dark night, his black clothing, and the shadow we stand in, he glows with happiness—like the Molotov he just launched, he is alight.

The strategy was simple, and for the anarchists new, defend the beating anarchist heart of Athens, of Greece, perhaps the world. Block, stop and turn back any and all attempts by the Athens Police to get to Exarcheia Square. And do so in a coordinated fashion between all the various groups, teams and squats. Each entity taking responsibility for one or two streets—ensuring they are effectively blocked. This in contrast to previous years when the rioting was scattered, unfocussed and usually developed into clashes around the Polytechnic, the University complex set off several blocks from the Square. This year, the Polytechnic and its environs played no role whatsoever, but Exarcheia Square sure as hell did. Finally, in crystalline form, the strategy was to take and keep liberated territory, to free a community—if only for a few hours.

The strategic plan included blocking all the approaches to the Square and by establishing a secondary system of barricades to neutralize the unfortunately offset side streets that link the main avenues. The side streets were one of the real dangers of the plan, because should the Police actually have the ability to turn a barricade they would then have flanking access to at least one, perhaps several, adjacent streets and barricades. The barricade that my team was tasked with defending was located such that the side street would have given the cops the advantage of flanking us effectively from the side and rear. Not good. In order to counter this threat a series of smaller side barricades were set up on these side streets, effectively slowing any belligerent force from going on a free ride from one street to the next, one point of defense to the next. Two small pedestrian streets also lead into the Square and these were barricaded as well. Finally there was a hope that at one or two points the anarchists could push hard enough to move the fighting up the street effectively expanding their territory and maybe even be able to sever a police line of reinforcement, or even better, retreat.

The one huge downside to the system of barricades was simple—if one or several were turned it would have given to the police the ability to flank every remaining barricade from the rear. A rock and a hard place scenario. Everyone seemed aware of this, and as fighting was heard in other streets I saw more than one rioter glance nervously over their shoulder in anticipation of a police charge from the rear. Fortunately this never happened.

The primary tactical component on the anarchist side was the barricade—construction, defense, and use as a weapon. The Exarcheia barricade varied from street to street. Usually low, sometimes waist high, on occasion higher, but never above eye-level so that the fighters could see over and anticipate police charges. Most included tires, wood taken from construction sites, large planters from the sidewalks, anything that could be ripped out of the ground, torn off a wall, or broken was used to raise the barricade just one inch taller. In one case two steel police barricades had been used to block a side street. In many instances barricades caught fire, either deliberately set or by accident. Once alight, the fires were allowed burn unchecked. The actual battle tactic was to taunt, harass and generally disrespect the forces of authority in a vocal and physical fashion. This included standing in front of the barricade throwing stones at the cops in the hopes of pushing them off their adjacent corner. Occasional chants could be heard rising up from various barricades, the only one I recognized being a chant calling cops murderers. The cops would charge and be driven off by Molotov and stone barrages. In one case the barricade I was at was turned by the cops, but only for a moment. A swift counter charge by anarchists pushed them off the barricade and back down the street. It’s fun to watch a cop retreat, especially as the ground around them sputters and roars in flame and smoke.

In terms of cop tactics they are hard to guess. But it seemed a pattern of varied harassment and probing. They seemed to move personnel from one barricade to another over the course of the night. The barricade I was at was very active with three or four charges an hour, usually beginning with a barrage of flash bang grenades followed by teargas, loads of teargas; then a charge, and a retreat. I saw this tactic deployed over and over, on almost every street. Some streets, hotly contested early in the evening, were virtually empty an hour or two later. Other streets, like mine, felt the brunt of the fighting. There was one barricade situated on a downhill street, in other words allowing some tactical advantage to the cops on a charge, which while contested, it was not a main point of fighting. I kept thinking that the reason must be that the retreat was hampered by the sloped street. Finally, the weather helped the insurgents– it was a humid, rainy cool night. The two pedestrian walkways, swathed in tiles that get slick as shit when wet, went effectively uncontested. The cops realizing that short of wearing shoes with soles slathered in krazy glue there was no way to safely run and retreat on a surface that was, in effect, as treacherous as ice. Almost all insurgent forces like inclement weather to fight in especially against regular troops, once Special Forces guys told me that rain and 45-55 degrees Fahrenheit is sufficiently gloomy for regular soldiers to begin to lose heart. In his words, “It tears the morale out of you.” Athens on the evening of December 6, 2016 was misty/slight drizzle, with a temperature hovering at 50 degrees. Perfect.

The Exarcheia Molotov is a brilliant technical innovation of the weapon, and worth taking note of. In general they use 500 ml beer bottles, filled half way or a bit less with a flammable liquid, usually gas. They then take a length of gauze bandage and extend a portion into the gas and tape the remainder at the opening of the bottle for a fuse. The traditional dangling fuse being a relic of the past. This accomplishes two things, first as the bottle is only filled half way, the gauze wicks gas and inundates the remaining air in the bottle with gas fumes. Turning the Exarcheia Molotov from a simple device that delivers fire into—a bomb. The damn things actually explode in massive purplish red flame as the remaining liquid gas erupts and spreads fire to anything it touches. Next the taping of the wick at the opening of the bottle, again inundated with liquid gas makes a perfect “fuse” it can be easily lit and thrown without the danger of self-immolation by a flopping, flaming piece of cloth. I would very much like to meet and congratulate the folks who developed this thing. It’s brilliant, it’s easy, and the Athens Police hate them like the plague—with good reason. The Exarcheia Molotov is a fearsome and effective weapon. Some comrades have further advanced this innovation with an attached canister, which explodes on impact. I have no idea what’s in this canister and asked one of the guys who was throwing these infernal devices what made them work, but as he spoke no English, and as I speak no Greek it remains a mystery. The explosion is loud, like a flash bang, with the attendant dispersal of flaming gasoline to the surrounding area. More information later, perhaps.

Hand thrown chunks of whatever. The most desperate thing I saw in Exarcheia that night was insurgents scrambling to get their hands on stuff to throw. One scene that I’ll always remember was of a bunch of young people kicking a pylon cemented in the sidewalk to loosen it. They eventually succeeded and it had the added benefit of producing further hunks of concrete when it was finally hauled out. Tiles torn out of walls, empty bottles, anything not actually nailed down was loosened, ripped out and thrown at the police.

In addition I saw slingshots, and an actual, honest-to-God, David-slays-Goliath sling being used. The projectiles used included ball-bearings, marbles, stone or concrete chunks. These were clearly weapons of harassment, used during lulls to further infuriate and demoralize the police.

Finally, though linked to a cop weapon, the anarchists have found the use of gas masks absolutely essential. There was little breeze on the night of December 6th and even small amounts of teargas were devastating as it settled into corners, doorways and hung in the damp, unmoving air.

On the cop side little was new, the usual suspects. Flash bang grenades, though in Athens these don’t use launchers, they are hand thrown. What is devastating in their repertoire is teargas, Brazilian teargas. Having been gassed recently in France, I’m beginning to be something of a lachrymator connoisseur, and I can tell you that Greek gas is dense, acrid and acidic—far more so than the Gallic variant. In terms of first aid there is Riopan. A kind of Maalox, but in handy single serving foil packets. The ground around the various barricades was littered with these white foil packs. And the faces of many insurgents looked clown-like as they poured the white liquid liberally into eyes, onto the mucous membranes, and finally taking a swallow to clear the acidic, noxious gas residue out of the throat.

Anarchists: 800-1,000. Organized as teams of between 5 and 10 fighters. Those from Exarcheia were assigned to various barricades and maintained themselves within their area. Those from outside Exarcheia roamed, the sound of flash bang grenades drawing them to specific streets, militants would frantically move from barricade to barricade as cop charges changed location and intensity. In a lull most hung out in Exarcheia, drank beer, talked, and scrounged for more stuff to throw. The number dwindled over the night to perhaps two hundred when the militants finally dumped arms and hostilities ceased, about 11:00 pm.

Cops: 200-300 (a guess). Based on my observations of the number per charge (20 cops maximum) and the number of barricades being simultaneously probed and harassed—upwards of five, and the number of police needed to provide logistics, support, command, reserves, and to steer traffic well out of the area.

As I sit and write this on the Isle of Lesvos a short 24 hours after the battle a number of scenes come to mind. Sitting in a room discussing preparations for the night, many of the militants standing, pacing, nervous with energy to get started. As I guarded the Molotovs having some Italian comrades wander by. They asked for a Molotov, which I provided and we all agreed that the Greeks had done something very right. Helping a young woman overcome by gas, who, when the Riopan got into her eyes and nose immediately recovered. Like a stoned person suddenly sober—she straightened, said, “Thank you Comrade,” turned and headed back to the barricade she was attending to. The sight of burning barricades, great arcs of Molotovs fuses sputtering as they flew and struck home in the ranks of the police. The shouting, chanting, laughing, talking–the feeling of really finally being alive. One’s hair standing on end as the flash bangs explode and teargas projectiles clatter on the ground and cloud the street. Finally on my way back to the apartment I was staying at, I noticed a small store open, with several people playing cards at a table in the back. I knocked on the door—needed smokes and something to drink. They motioned me in, and asked where I was from, a few questions and finally one of the older men asked, “So tonight did you see the riots?”

“Yes,” I answered not wanting to give too much away.

“And who are you with, the young people or the cops?”

Hesitantly I said, “The young people, always.”

He smiled broadly and answered, “So are we.”




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

