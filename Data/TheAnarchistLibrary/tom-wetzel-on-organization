      Delegating Responsibilities      A Chair is Not a Boss      The Right to Dissociate      Talking Until Agreement is Reached      Formal Consensus      Consensus is Anti-democratic      Simple Majority      Small Groups, No Power
“Consensus” has had a certain popularity as a decision-making method among social change groups since the ’60s, especially within the anti-nuclear movement but also in anarchist and radical feminist circles. I think we can understand why if we consider what sorts of organizations exist in this country. Mass organizations in which the membership directly shape the decisions are hard to find. How often have members been ruled “out of order” at union meetings by an entrenched official? Most leftist political groups also have a top-down concept of organization, as befits their preoccupation with “leadership.”

On the other hand, this sort of alienation and lack of control appears absent in activities organized through small circles of acquaintances. Those who engage in an action together typically reached a common agreement after talking it over informally. This leads to the model of the small, informal group — no written constitution, no chair of meetings, no elections for delegated tasks, no careful definition of jobs, no written minutes of meetings. Decisions are made by having an unstructured discussion until consensus is reached.

But informality does not eliminate hierarchy in organizations; it merely masks it. To the insiders, everything appears friendly and egalitarian. But newcomers do not have the same longstanding ties to the group. And having no clear definition of responsibilities, and no elections of individuals who carry out important tasks, makes it more difficult for the membership to control what goes on.

Fortunately, the “small, informal group” is not the only alternative to the dominant hierarchical model of organization. It is possible to build a formal organization that is directly controlled by its membership. Being “formal” merely means that the organization has a written set of rules about how decisions are made, and duties of officers and conditions of membership are clearly defined. An organization does not have to be top-down in order to be “formal” in this sense. A libertarian organization would have a constitution that explicitly lays out a non-hierarchical way of making decisions.

Sometimes people have the idea that setting up elected positions with defined responsibilities is a “hierarchy,” as if any delegation of responsibility creates a boss. Yet, informality does not avoid delegation since some people will inevitably do tasks on behalf of the group, such as answering correspondence or handling a bank account.

It is possible to elect people to perform delegated tasks without creating a top-down organization. Here are a few guidelines:

The scope of authority of an elected position, such as correspondence secretary or treasurer, should be explicitly defined and delimited, so that everyone knows what this person should be doing, and with the requirement of regular reports to keep the membership informed.

The person should be elected for a limited term, such as one year, and should be subject to recall at any time by majority vote of the membership (but with a requirement of adequate notice to ensure that this is not “sprung” all of a sudden by those members least favorable to the person currently doing the job).

If at all feasible, there should be a requirement of mandatory rotation from office. This is especially important for any position of acting as spokesperson or representative of an organization or body of people. If an organization is very small, however, it is sometimes difficult to rotate responsibilities. Even so, the person carrying out responsibilities can report regularly to membership meetings and can be thus directed by decisions of the membership.

Nobody is to be elected to set policy for the organization, but only to carry out those responsibilties that have been assigned by the membership. The general membership meeting of the organization must remain the supreme decision-making body and can over-rule any decisions of elected officers.

The idea is that the main decision-making responsibility of the organization is not to be delegated to some “steering committee” or executive but is conducted directly by the membership through their own discussions and votes; this is the heart of the libertarian concept of organization.

Since many leftists define social change in terms of putting a particular leadership into power — such as the Leninist concept of “the revolutionary party taking state power” — it is no surprise that even organizations formed, or influenced, by leftists may have a hierarchical set-up where the power to make decisions is concentrated in some executive board or steering committee. While libertarians oppose this practice, and pose the alternative of direct decision-making by the members or rank-and-file participants, it is, nonetheless, not necessary to oppose all delegation of tasks or responsibilities.

The real question should be, “What is the relationship between those vested with responsibilities and the rest of the membership?” If the center of decision-making lies in the general meetings, and those with responsibilities must report to these meetings, and are instructed by them, and (where possible) jobs are rotated, then we do not have a top-down structure, but an organization where decision-making is from the bottom up.

Often people who favor the “small, informal group” model of organization also oppose the practice of electing someone to chair a meeting, even if the meeting is a larger gathering. It is easy to understand what they are afraid of. Consider union meetings where the chair is a paid official. He has certain entrenched interests to defend. To serve his ends, he may rule “out of order” motions from the floor on matters of concern to the rank and file, or manipulate the meeting in other ways.

But here the problem is that there is an entrenched bureaucracy; chairing meetings is only one of the ways they control the organization. The situation is different if the chair is elected at the beginning of the meeting by those present, and if the chair can be removed by majority vote at any time. Being chair of a meeting does not convert someone into a bureaucrat.

I’ve sat through chairless meetings where people interrupt each other, voices get louder as people try to express themselves, discussions get side-tracked into numerous tangents, and important decisions are put off or hurriedly decided at the last minute. This experience has made me rather frustrated with the prejudice against having a chair of meetings.

If a meeting only consists of a few people, then obviously it does not need to have a chair. But once meetings achieve a certain size, a chair becomes necessary in order to ensure that the meeting stays on track and moves through the agenda in a reasonable amount of time, while making sure that people have an opportunity to speak.

I’ve heard opponents of chairmanship argue, “It’s the responsibility of each individual to make sure that the meeting stays on track and individuals don’t get out of hand.” But even with the best of intentions, this is difficult to achieve in practice. When you’re thinking about what you want to say next, it’s hard to also be keeping track of whose turn it is to speak and of what the agenda is.

The rationale behind having a chair is that we delegate to one person the responsibility to concentrate on such things as the agenda and the order of speakers while the rest of us are free to concentrate on what is being said. Of course, it can happen that a chair is manipulative, favoring one particular “side” in a matter under dispute. But in such a situation, a motion to replace the chair would be in order.

In working out a libertarian concept of organization, we need to remember that the individual members not only have rights that must be respected by the organization, they also have obligations to the rest of the membership. Since the majority have the right to control their own organization, individuals must conduct themselves so as to respect this right of the majority.

For example, if an individual makes public statements that claim to speak for the organization, but state only the viewpoint of the individual, not a viewpoint actually discussed and agreed to by the majority, then that individual is acting irresponsibly and anti-democratically.

There is, however, no reason why an individual should be required to stay mum publically about disagreements within the organization. As long as the individual makes clear that the stated viewpoint is his or her own, public disagreement with the position of the organization is not irresponsible.

A libertarian concept of organization must allow for diversity of opinions. This means that members must try to maintain a climate of respecting the opinions of others in the organization. But what happens when members do not respect the rights of others? What happens when members are threatening to others, or conduct themselves in ways that are very disruptive to the life of an organization? In such a case the majority may have to consider disassociating themselves from that individual. In other words, the rights of the majority include the right to expel individual members.

To some anarchists, expulsions are always a “purge.” The authoritarian connotation of the latter term are meant to suggest that any expulsion is a violation of freedom, an illegitimate act. But the position of these anarchists is actually self-contradictory. For, it is a very basic libertarian principle that the membership of an organization have the right to directly control it. And this means that no individual has the “right” to act in ways that prevent the majority from accomplishing the purposes for which they got together. If the majority in an organization did not have the right to expel disruptive indivdiduals, this would mean that they couldn’t control the conditions of membership and direction of that organization. Freedom of association implies the freedom to disassociate.

On the other hand, the power to expel members should never be delegated to officials. For, if elected officers can expel members ontheir own, they can expel critics of how they are conducting their responsibilities. Expulsion certainly is used by officials in hierarchical organizations as a means of maintaining their top-down control. What is illegitimate in such cases is not the act of expulsion in itself, but the top-down way it is carried out.

The point here is that individuals have obligations to the other members of an organization. And the majority have the right to ensure that the responsibilities of membership are observed. But expulsion is a last resort, and should not be used lightly. Expulsion is something that the membership should decide on directly, in a general membership meeting or convention. And it should always be required that accused individuals be given advance notice and have the right to defend themselves before the general membership prior to a vote to expel.

The partisans of informality also tend to be averse to voting as a way of making decisions. They prefer the process of talking until agreement is reached (or not reached). In my experience, this process tends to encourage informal hierarchy. That’s because this process tends to heighten the influence of the more articulate and self-confident individuals, and tends to disenfranchise the shy newcomer, and the less articulate. Voting has the advantage that it is an equalizer. The shy and the aggressive, the articulate and the not-so-articulate, all can raise their hands, and each has only one vote.

Advocates of consensus sometimes say that hierarchical organization is the only alternative to consensus. But there is also the alternative of direct democracy where decisions are made by majority vote. Direct voting by the members puts the majority of members in control, and control by the majority of members is the opposite of hierarchy. In a hierarchical organization, it is not the majority of members who are in charge but a few leaders at the top — that is what “hiearchy” means.

The libertarian idea of direct, democratic voting is quite different than the official concept of “democracy” in this society. “Democratic voting” typically means electing officials who then have all the power of making decisions. But that is really elective autocracy, not genuine democracy, which requires direct decision-making by the rank and file.

Though “talking until agreement is reached” is the natural method of decision-making for “small, informal groups,” not all advocates of consensus decision-making are averse to formal organization. However, making the organization formal — a written constitution, definition of membership and so on — does not eliminate the basic problems of the consensus process.

The requirement of unanimity means that disagreements have to be talked out until verbal consensus emerges. This means that even a formal consensus system tends to heighten the influence of the more talkative, self-confident participants. Also, the requirement of consensus often leads to prolonged, marathon sessions, or meetings where nothing is decided.

This aspect of consensus tends to make the movement less conducive to participation by working people, and tends to reduce participation to the hard-core activists. When people have other demands on their time (job, children, spouse), they will tend to be frustrated by meetings that are unnecessarily long, indecisive, or chaotic. Most people will want to have some sense that something will be accomplished, a clear decision made, and in a reasonable amount of time.

In his pamphlet Blocking Progress, Howard Ryan describes a nightmarish example of what can happen with consensus.[1] Many people in the Livermore Action Group — an anti-nuclear action group here in the Bay Area — were uncomfortable with the first point of LAG’s action guidelines which stated: “Our attitude will be one of openness, friendliness and respect toward all people we encounter.” “A common sentiment”, Ryan points out, “was that oppressed people often do not feel these things towards police or authorities and should not be required to feel them in order to join the [Lawrence-Livermore Laboratory] blockade.” In 1982 there was a month-long discussion of this issue, followed by two full days of informal open debate. At the second of these assemblies it was proposed to replace the “friendly and respectful” language with “non-violent.”

Coming towards the end of this long process of discussion, there was a suggestion by one of the participants in the second meeting that a straw poll be taken to determine the general opinion in the room. This was itself considered so controversial that two hours were consumed in debating whether it was even okay to take a straw poll. Finally a poll was taken and the vote was 74 to 2 in favor of changing the non-violence code to remove the “respectful and friendly” language. One of the participants has described what then took place:

One of the two people [a doctrinaire pacifist] blocked it. He was asked repeatedly to stand aside, to leave, to die. People were just so upset. He wouldn’t budge and it was blocked.

This is a good example of the elitist coercion that consensus permits.

The requirement of unanimity is anti-democratic. A small minority does not have the right to prevent the majority of members from doing what they want to do. Organizations are not of value in themselves but only as a vehicle for cooperation and collective activity. Insofar as consensus thwarts the majority from doing what it wants, it makes the organization an ineffective vehicle for them. This can lead to splits and fragmentation — exactly the result that the advocates of consensus say they want to avoid.

The rules of an organization can — and must — protect the rights of individuals and minorities. If one studies the situation in the AFL-CIO-type unions, and major political organizations, it is true that the rights of individuals and political minorities are often in a sorry state. But these are hierarchical organizations. It is the hierarchy, not “majority voting,” that is the problem.

Anarchists of the more individualistic persuasion argue that consensus is necessary to avoid “tyranny of the majority.” But where in the real world does the majority have real power? The real tyrannies that people are fighting around the world are tyrannies of entrenched minorities, of governments and bosses. I don’t want to claim that “majorities are always right” but I do believe that people have the right to make their own mistakes. The issue here is whether people have the right to control their own movements and organizations. To give a single individual or small minority the right of veto on decisions is to have a system of minority rule.

Even when individuals or minorities do not actually threaten or use a block to keep the majority from doing what it wants, everyone is aware that they could, if the organization is run by consensus. The structural requirement of unanimity puts pressure on the majority to placate small minorities in order to accomplish something. Often this leads to decisions that paper over disagreements and leave everyone dissatisfied.

Rudy Perkins has described this problem, based on his experience in the Clamshell Alliance in New England in the late ’70s:

Majority rule is disliked because amongst the two, three or many courses of action proposed, only one is chosen; the rest are “defeated.” Consensus theoretically accommodates everyone’s ideas. In pactice this often led to:

a watered down, least-common-denominator solution, or

the victory of one proposal through intimidation or acquiescence, or

the creation of a vague proposal to placate everyone, while the plan of one side or another was actually implemented through committees or office staff.

In other words, within the anti-nuclear movement ideas are in competition and some do win, but under consensus the act of choosing between alternatives is usually disguised. Because the process is often one of mystification and subterfuge, it takes power of conscious decision away from the organization’s membership.[2]

Consensus puts pressure on minorities not to express misgivings or disagreements because their dissent would prevent the organization from making a decision. Thus it actually becomes harder for minorities to state dissenting opinions because dissent is always a disruptive act. When decisions are made by majority vote, on the other hand, there is not this heavy “cost” to dissent and minorities can freely state their disagreement without thereby disrupting or blocking the organization from reaching a decision.

Consensus also means that it becomes very difficult, if not impossible, to change an organization’s orientation even when it is clear to most members that the current direction is failing. That’s because there will almost always be a minority who will be against change, because the current direction of the organization may have been what attracted them to it, or because they may simply prefer what they are used to.

“Simple majority” is the requirement of one vote more than half the votes cast in order to make a decision. A simple majority is the smallest number of votes needed to guarantee that a decision is made.[3]

Advocates of simple majority sometimes hear the retort: “But do we want to have a major decision made with 51% for 49% against?” Decisions that organizations make in the course of conducting their affairs vary a lot in their relative importance to the participants. For some decisions, a narrow majority won’t matter because those who voted “no” may not have really strong feelings one way or the other. If it is an important issue, though, it is clearly a problem if an organization is closely split.

Sometimes, in organizations that are based on membership participation and democratic voting, close votes will lead the group to stop and reconsider the issue in order to find a proposal that accommodates objections.

More often, this process happens before it reaches a vote. When it becomes clear in the course of the discussion on a proposal that the membership are closely divided and have strong feelings on the issue, there is likely to be an effort to find a proposal that mitigates objections. For one thing, it is to the advantage of the proposal’s partisans to have as much support as possible within the organization. The work of the organization is bound to suffer if it is badly split — dissatisfied members may drag their feet or drop out.

When a union conducts a strike vote, for example, the partisans of a strike will want to get the largest possible majority for a strike. If the vote for a strike isn’t overwhelming, if there is only a narrow majority for striking, the union will be less likely to actually go out because the division among the workforce undermines the chances of winning a strike.

Such considerations have at times led people to propose decision-making based on larger majorities, such as two-thirds or three-fourths. But the problem with this is that most of the decisions that organizations make are not so crucial that large majorities are needed.

Moreover, stipulating a majority larger than 50% plus one means that decisions can be blocked by minorities. Though the minorities required to “block” a majority are larger than under consensus, this still permits minority control. A cohesive minority could exercize undue influence on a group due to its potential for blocking what the majority wants. Thus the arguments against consensus also apply to some extent against a formal requirement of two-thirds or three-fourths majority. The advantage to “simple majority” as a decision-making method is that it is the only way to formally preclude minority rule.

There may be circumstances when it would be desireable to have a larger majority than 50% plus one — as in those cases where the organization is closely split on important issues. But instead of trying to make a formal rule for this, I think this should be dealt with by the membership using good sense in such situations. Not everything that is desireable for an organization can be created by formal rules.

The conditions required for the healthy and democratic functioning of an organization go beyond the formal rules. Whether the rights of members are respected also depends on the climate in the organization. How people treat each other is an informal factor but it is just as important as clauses in constitutions.

There is usually some sort of underlying, informal consensus in almost any organization. To take an obvious example, there needs to be a consensus that disagreements are not settled by punching someone out. So, there does need to be a consensus on some things, on certain basic assumptions that underlie the unity of the organization. The advocates of “consensus decision-making” are correct in perceiving this, but where they go wrong is in trying to elevate this into a general principle of decision-making so that everything requires a consensus. The consensus system puts day-to-day decisions, on the one hand, and the most important decisions, fundamental purposes and ways of treating each other, on the other hand, all on the same level.

However, consensus does often work reasonably well in small groups, especially where the participants have a common background and shared assumptions. Some people might maintain that small, independent groups are all that is needed.

Indeed, some partisans of the small group have argued that “bigness” inevitably brings bureaucracy in movements and that only small, independent groups can be genuinely controlled by their members. This ignores the methods that libertarians have developed for avoiding top-down control in mass organizations (such as the guidelines I mentioned earlier), and the examples of libertarian mass unions that functioned through assemblies, without an entrenched bureaucracy; organizations like the Industrial Workers of the World back in the ’10s or the Spanish National Confederation of Labor (CNT) in the ’30s.

If the “bigness means bureaucracy” dogma were true, a libertarian society would be impossible. To have a society organized along anarchist lines means that there must be a means by which the whole populace can participate in making crucial decisions affecting society as a whole. For this to happen it must be possible to have large organizations, organizations spanning vast areas, such as the North American continent, that are able to function in a non-hierarchical way, directly controlled by their rank and file participants.

If the whole society could be organized to make decisions through direct democracy and mass participation, as anarchists advocate, then surely it must be possible for people to build mass organizations that are run this way today. If not, then how could a libertarian society be brought into existence? Only a mass movement that is itself organized non-hierarchically could create a society free of top-down, bureaucratic, exploitative social relations.

This brings us to the clearest problem with the “small groups” doctrine: Small groups have no power. The power to change society requires a mass movement, and the development of solidarity among working people on a large scale. To unite people from a variety of backgrounds and cultures, to coalesce the various groups into a real movement, to pool resources, mass organizations are needed. In the absence of a larger movement, small groups can be discouraged by their own lack of resources and sense of isolation.

Unless working people can organize their solidarity into mass organizations, they will not be able to develop the power to challenge our very powerful adversaries — the corporations and their government. Without a mass movement, most people will not develop a sense that they have the power to change society. Our ideal of social change in the direction of democratic participation and workers control will appear to most people as merely a “nice idea, but impractical.” Only the strength of a mass movement can convince the majority that our vision of a society run by working people is feasible.
[1] Howard Ryan, Blocking Progress: Consensus Decision Making in the Anti-Nuclear Movement, 1983, published by the Overthrow Cluster of the Livermore Action Group. Ryan’s pamphlet makes a number of the same arguments against consensus that I am making here.
[2] Rudy Perkins, “Breaking with Libertarian Dogma: Lessons from the Anti-Nuclear Struggle,” Black Rose, Fall 1979, p. 15.
[3] If we were to allow a decision to be made when half vote for a proposal, then it might happen that half vote for proposal A and half vote for proposal B. And what if A and B are conflicting proposals? Requiring one vote more than half guarantees that a single solution is decided upon.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



The scope of authority of an elected position, such as correspondence secretary or treasurer, should be explicitly defined and delimited, so that everyone knows what this person should be doing, and with the requirement of regular reports to keep the membership informed.



The person should be elected for a limited term, such as one year, and should be subject to recall at any time by majority vote of the membership (but with a requirement of adequate notice to ensure that this is not “sprung” all of a sudden by those members least favorable to the person currently doing the job).



If at all feasible, there should be a requirement of mandatory rotation from office. This is especially important for any position of acting as spokesperson or representative of an organization or body of people. If an organization is very small, however, it is sometimes difficult to rotate responsibilities. Even so, the person carrying out responsibilities can report regularly to membership meetings and can be thus directed by decisions of the membership.



Nobody is to be elected to set policy for the organization, but only to carry out those responsibilties that have been assigned by the membership. The general membership meeting of the organization must remain the supreme decision-making body and can over-rule any decisions of elected officers.



a watered down, least-common-denominator solution, or



the victory of one proposal through intimidation or acquiescence, or



the creation of a vague proposal to placate everyone, while the plan of one side or another was actually implemented through committees or office staff.

