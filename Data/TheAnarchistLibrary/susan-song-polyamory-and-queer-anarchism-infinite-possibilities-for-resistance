      Queer and Anarchist Intersections      Class Politics and Beyond      Queer Anarchism as a Social Form      Polyamory as a Queer Anarchist Form      A Call to Sexual Freedom
This article discusses queer theory’s relevance to anarchist sexual practice and why anarchists might critique compulsory monogamy as a relationship form. Queer theory resists heteronormativity and recognizes the limits of identity politics. The term “queer” implies resistance to the “normal,” where “normal” is what seems natural and intrinsic. Heteronormativity is a term describing a set of norms based on the assumption that everyone is heterosexual, gendered as male/female and monogamous, along with the assumed and implied permanency and stability of these identities. Queer theory also critiques homonormativity, in which non-heterosexual relationships are expected to resemble heteronormative ones, for instance in being gender-normative, monogamous, and rooted in possession of a partner. In this way, queer theory and practice resists the expectation that everyone should have a monogamous, cis-gendered,[1] heterosexual relationship form.

In “Anarchism, Poststructuralism and the Future of Radical Politics,” Saul Newman distinguishes anarchism from other radical political struggles. Newman conceptualizes emerging anticapitalist and anti-war movements that are “anti-authoritarian and non-institutional...[as]...anarchist struggles.”[2] He describes these movements as those that “resist the centralizing tendencies of many radical struggles that have taken place in the past,...they do not aim at seizing state power as such, or utilizing the mechanisms and institutions of the state.”[3] Anarchism is to be understood here as resisting institutionalization, hierarchy, and complete or partial political assimilation into the state.

Newman also cites anarchist thinkers such as “Bakunin and Kropotkin [who] refused to be deceived by social contract theorists, those apologists for the state like Hobbes and Locke, who saw sovereignty as being founded on rational consent and the desire to escape the state of nature. For Bakunin, this was a fiction, an ‘unworthy hoax’. ...In other words, the social contract is merely a mask for the illegitimacy of the state—the fact that sovereignty was imposed violently on people, rather than emerging through their rational consent.”[4] He describes resistance to the state by recognizing its illegitimacy as a seemingly chosen form. Similarly, queer theory can act to critique biological discourses about gender and sexuality being “natural,” by pointing to its varying forms that are conceptualized in and influenced by historical and social contexts. Queer theory asserts that sexuality as a category and way of identifying, thought to be “biologically natural,” is in fact socially constructed.

This is demonstrated by the ways that “homosexual” and “sex” as biological categories came to be created. In the later nineteenth century, the term “homosexual” emerged as a way to define an identity for those who engage in same-sex sexual acts. Homosexuality as a term arose as a way to define heterosexuality, thus pointing to its socially constructed and unnatural origin. Biological and medical discourses about gender and sexuality shift historically. In “Discovery of the Sexes,” Thomas Laqueur notes how sex was constructed for political and not medical or scientific reasons “sometime in the eighteenth century.”[5] “Organs that had shared a name—ovaries and testicles—were now linguistically distinguished. Organs that had not been distinguished by a name of their own—the vagina, for example—were given one.”[6] Female orgasm and its role, if any, in conception were also debated as a contemporary issue. Sexual difference became a way to articulate a hierarchy of gender where women are viewed as inferior to men. This model of sexual difference is, Laqueur writes, “as much the products of culture as was, and is, the one-sex model.”[7] This transition is demonstrated in instances such as when de Graaf’s observations yielded the claim that “‘female testicles should rather be called ovaries.’”[8] Eighteenth-century anatomists also “produced detailed illustrations of an explicitly female skeleton to document the fact that sexual difference was more than skin deep.”[9] In this one-sex model, the male body is the norm against which other bodies are compared. This model problematically assumes that biological difference creates a “normal” social difference. However, Laqueur destabilizes this idea of sex as a “natural” category that points to significant biological differences, and instead posits that the construction of sex is influenced and shaped by a hierarchy of gender and political impulses.

Queer theory denaturalizes hierarchies of gender, sexuality, and political influence, and is a valuable tool for anarchist practice. Queer theory questions what is “normal” and what creates hierarchical differences between us, opening up new sites of struggle outside of class politics alone. From feminist theory emerged the idea that gender is socially and not biologically constructed, and therefore not innate, natural, stable or “essential” to someone’s identity due to their “biology.” Instead, gender is a product of social norms, individual behaviors, and institutional power. Gay/ lesbian studies added to the discourse around gender and sexuality by introducing homosexuality and LGBT identities as areas to be queried. Following the work of feminist theory and gay/lesbian studies, queer theory understands sexuality and sexual behaviors as similarly socially constructed and historically contingent. Queer theory allows for a multiplicity of sexual practices that challenge heteronormativity, such as non monogamy, BDSM relationships, and sex work.

Queer theory opens up a space to critique how we relate to each other socially in a distinctly different way than typical anarchist practice. Where classical anarchism is mostly focused on analyzing power relations between people, the economy, and the state, queer theory understands people in relation to the normal and the deviant, creating infinite possibilities for resistance. Queer theory seeks to disrupt the “normal” with the same impulse that anarchists do with relations of hierarchy, exploitation, and oppression. We can use queer theory to conceptualize new relationship forms and social relations that resist patriarchy and other oppressions by creating a distinctly “queer-anarchist” form of social relation. By allowing for multiple and fluid forms of identifying and relating sexually that go beyond a gay/straight binary, a queer anarchist practice allows for challenging the state and capitalism, as well as challenging sexual oppressions and norms that are often embedded in the state and other hierarchical social relations.

A queer rejection of the institution of marriage can be based on an anarchist opposition to hierarchical relationship forms and state assimilation. An anarchist who takes care of someone’s children as an alternative way of creating family can be understood as enacting a queer relation. Gustav Landauer in Revolution and Other Writings writes that “The state is a social relationship; a certain way of people relating to one another. It can be destroyed by creating new social relationships; i.e., by people relating to one another differently.”[10] As anarchists interested and working in areas of sexual politics and in fighting all oppressions, we can create a new “queer-anarchist” form of relating that combines anarchist concepts of mutual aid, solidarity, and voluntary association with a queer analysis of normativity and power. We must strive to create and accept new forms of relating in our anarchist movements that smash the state and that fight oppressions in and outside of our bedrooms.

One way that we can relate socially with a queer anarchist analysis is by practicing alternatives to existing state and heteronormative conceptualizations of sexuality. We can embrace a multiplicity of sexual practices, including BDSM, polyamory, and queer heterosexual practices—not setting them as new norms, but as practices among many varieties that are often marginalized under our normative understandings of sexuality. In polyamorous relationships, the practice of having more than one partner challenges compulsory monogamy and state conceptions of what is an appropriate or normal social relation. Polyamory is just one of the practices that arise when we think of relationship forms that can (but do not automatically) embody distinctly queer and anarchist aspects. BDSM allows for the destabilizing of power relations, by performing and deconstructing real-life power relations in a consensual, negotiated setting. Queer heterosexual practices allow for fluidity of gender and sexual practices within heterosexual relationships. Although practicing these relationship forms alone does not make one a revolutionary, we can learn from these practices how to create new conceptualizations of social relations and, importantly, challenge normative indoctrination into our society’s constrictive, limited, and hierarchical sexual culture.

Polyamory refers to the practice of openly and honestly having more than one intimate relationship simultaneously with the awareness and knowledge of all participants. This includes relationships like swinging, friends with benefits, and people in open relationships. The open and honest aspect of polyamory points to anarchist conceptions of voluntary association and mutual aid. Polyamory also allows for free love in a way that monogamous state conceptions of sexuality don’t allow. Emma Goldman in “Marriage and Love” writes, “Man has bought brains, but all the millions in the world have failed to buy love. Man has subdued bodies, but all the power on earth has been unable to subdue love. Man has conquered whole nations, but all his armies could not conquer love. ...Love has the magic power to make of a beggar a king. Yes, love is free; it can dwell in no other atmosphere. In freedom it gives itself unreservedly, abundantly, completely.”[11]

In free love, there reside anarchist notions of mutual aid. Returning to a previous point, polyamory as a form challenges conceptualizing one’s partner as possession or property. Instead of having exclusive ownership over a partner, polyamory allows for partners to share love with as many partners as they agree to have. In contrast to compulsory monogamy, polyamory can allow for more than one partner, which can challenge state conceptions of what is a normal/natural relationship and enacts a queer form of relation. Compulsory monogamy can refer to relationships that are produced in a context where there is pressure to conform to monogamy. Compulsory monogamy is a concept that’s pervasive in our laws and institutions, where the expectation and pressure to conform to monogamy is awarded by material and social gain. This is not to suggest that those who choose monogamous relationships are more restricted than their polyamorous counterparts. A critique of the ways in which monogamy has become compulsory is quite different than judging individual romantic/sexual practices.

Polyamory can also challenge state conceptions of possession and property. Marriage as an institution is invested with notions of heterosexual reproduction and patriarchy. Sara Ahmed’s work can be used to further help conceptualize polyamory. She writes, “In a way, thinking about the politics of ‘lifelines’ helps us to rethink the relationship between inheritance (the lines that we are given as our point of arrival into familial and social space) and reproduction (the demand that we return the gift of that line by extending that line). It is not automatic that we reproduce what we inherit, or that we always convert our inheritance into possessions. We must pay attention to the pressure to make such conversions.”[12] Her analysis demonstrates how polyamory can challenge ideas of inheritance and possession. Polyamory as a form allows for a multiplicity of partners and isn’t necessarily invested in heterosexual reproduction in the same way that marriage as a state institution can be. In this way, polyamory can disrupt practices of reproduction and inheritance by creating new family and relationship forms not invested in sexual ownership and in becoming a part of state-enforced and monitored relations.

One may ask, how is polyamory relevant to me if I’m not interested in practicing it? What is the point of critiquing monogamy if I’m in a satisfying monogamous relationship? By bringing queer theory into our bedrooms and into the streets, we can begin to expand what may not be thought of as in need of liberating. When folks in fulfilling, monogamous relationships consider this history of sexual repression, they have the tools to understand what it means to become sexually liberated in spite of that history, even while choosing to remain in monogamous relationships. We can liberate ourselves from confining and arbitrary gender norms and expectations in not just our romantic relationships but our everyday lives. Queer theory gives us the spaces to transgress and play with gender and question the limits of identity politics to further consider that sexuality and other identities are not stable and don’t have to be. Sexuality can be fluid and come in multiple forms, just as our gender expressions can be.

We want more than class liberation alone. We want to be liberated from the bourgeois expectations that we should be married, that there is only a binary of men and women in rigid normative roles who can date monogamously and express their gender in normative, restrictive ways. We should fight for gender liberation for our gender-transgressive friends and comrades and fight for freedom of consensual sexual expressions and love. This fight isn’t just in the streets. It’s in our bathrooms where transgendered and gender-non normative folks are policed by people who don’t acknowledge trans or other gender non-normative identities, either by reinforcing a gender binary of cisgendered identities and ignoring a fluidity of gender identities or by otherizing transgender folks as an Other gender. It’s in our family structures that create bourgeois order in our lives. It’s in our production of discourses around sexuality, where sexuality is seen as something to be studied under a Western, medical, biological model. It’s in our meetings and movements where critical voices that don’t belong to straight, white, cis-gendered men are marginalized. We should create new, different ways of living and allow for queerer forms of relating and being.

Sexual liberation looks different for each individual. In my experience, being consensually tied up by a friend and consensually flogged in a negotiated setting is liberating. Kissing or hugging someone who you’ve carefully negotiated consent with is explosively satisfying. Being in an open, honest, polyamorous relationship for me created one of the most liberating romantic relationships of my life so far. However, sexual liberation is a deeply subjective experience. A problematic binary is set up in conceptualizing polyamory itself as a queer anarchist form and in potentially creating and reinforcing a new “norm” of polyamory as being superior to monogamy and other heteronormative relationships.

Returning to Ahmed, what is significant in considering new relationship forms is the pressure to make conversions and this should be considered as we form new ways of relating that challenge patriarchy[13] capitalism, and heteronormativity. We must broaden our ideas around what anarchist sexual practice looks like, ensuring that smashing gender norms, accepting that sexuality and gender are fluid, unstable categories, and challenging pressures to be monogamous are as part of our anarchist practice as challenging state forms of relating. We should live, organize and work in a way that consciously builds a culture that embodies these norms of being resistant to patriarchy and heteronormativity. This work is fundamental to our shared liberation from capitalism—but also from patriarchy, heteronormativity, and restrictive and coercive sexual expectations of all kinds.
[1] Cis-gendered is a term referring to individuals who have a gender identity or gender role that matches their sex assigned at birth. For instance, a cis-gendered woman is a woman who was assigned female at birth and identifies with female. This term is sometimes thought of as meaning “not transgender.”
[2] Saul Newman, “Anarchism, Poststructuralism and the Future of Radical Politics.” SubStance (36)(2) (2007): 4.
[3] Ibid., 4.
[4] Ibid., 6.
[5] Thomas Laqueur, Making Sex: Body and Gender from the Greeks to Freud (Cambridge: Harvard University Press, 1990), 27.
[6] Ibid., 27.
[7] Ibid., 29.
[8] Ibid., 44.
[9] Ibid., 31.
[10] Gustav Landauer, Revolution and Other Writings: A Political Reader, ed. and trans. Gabriel Kuhn (Oakland: PM Press. 2010), 214.
[11] Emma Goldman. Anarchism and Other Essays. 3rd ed. (New York: Mother Earth Association, 1917), 93.
[12] Sara Ahmed. Queer Phenomenology: Orientations, Objects, Others. (Durham: Duke UP, 2006), 17.
[13] Patriarchy refers to a system of power embedded in institutions and other ways of social organizing that privileges and grants power to men over women and folks who aren’t cisgendered.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

