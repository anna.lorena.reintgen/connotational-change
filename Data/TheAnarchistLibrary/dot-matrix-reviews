    Passion Fruit    Politics is Not a Banana    Post-Civ!    The Rag #1 Autumn 2006    Killing the Artist    Creating Anarchy    A Problem of Memory
Passion Fruit: Anti-Authoritarian (Con)sensuous Games 
PO Box 63232 
St. Louis, MO 63163 
83 pages; $4

This zine describes physical, sexual and flirtatious games people can play with each other that are creative and as safe as seems reasonable. It discusses issues of consent, communication, and disease. It tries to be fun and responsible.

I have a soft spot in my heart for the thinking behind this zine. I grew up in the time before AIDS, when the shots from the Sexual Revolution were still reverberating (including the ones of penicillin). My early feminist years were spent thinking and reading about how breaking the corporate, button-down affect, getting in touch with our bodies, refusing to abide by suburban morality, were all practices that would liberate us, as well as celebrate our liberation.

There is still something to these arguments — the understanding that we are animals not floating brains, that bodies are to be embodied, that the control implied by suits and high heels may be what we need to attain to destroy this society, but will destroy us as it has the people who believe in it. And Passion Fruit is explicit in its desire to open up people’s options around forms of relationships, which I can only appreciate.

But too much emphasis on sexuality is problematic too. Over-hyped sexuality is the haven of a corporatized world; it is the award and the consolation for our (presumed) powerlessness in the rest of our lives.

Acknowledging the power of sex in human life is one thing; understanding how this culture has forced even more significance into sex is another. Understanding how we are all taught to manipulate (and be manipulated by) sex is part of that.

PF might have worked better for me if it had used different graphics. The images drop you into the middle of a party full of strangers. Of course some people love that jump-in-over-your-head approach, but I prefer to go in one step at a time.

Perhaps the most serious question is about PF’s attempt to marry hedonism with responsibility. Talking to people about how to have fun while they relate in loaded, physical ways with people who they don’t know is a tricky and frequently explosive business. This zine takes the de rigueur approach of cautioning everyone to make sure that everyone consents to everything before anybody does anything, and encouraging people to opt out if they feel uncomfortable. This is the standard approach made popular by sexual harassment suits everywhere: the approach that says that we can have hedonistic fun while dotting i’s and crossing t’s, that trust comes from having signed on the dotted line.

Perhaps this approach is the best we can do. But I would love to see an analysis that challenges that perspective on safety, on consent, and on play.

Politics is Not a Banana 
journal of vulgar discourse 
issuu.com

One of a spate of zines that are being distributed primarily online as PDFs, leaving it up to the reader whether to read it online or download and print it out, PiNaB is a pretty, clean, light-hearted, insurrection-inspired, thoughtful publication with a heavy design element.

It is so designed in fact that the design becomes one of the loudest (if ambiguous) facets of the content. For example, the footnotes are larger than the body text, and are set off in bright red. Message: perhaps that our roots and/ or tangents are important? Or perhaps that we should pay attention to things that are normally considered secondary? Or perhaps, that size and color are not in fact a measure of importance?

Documents that are published online (with the goal of people printing them out themselves) have different (not fewer) challenges than pieces designed for hardcopy. Instead of having to make decisions based on price (of ink, of paper type, of shipping, etc), online documents have to negotiate the differences between reading on a screen (short text sections to suit reading on a monitor, a clunkier page change process, etc) with reading in hardcopy. Pages need to work as well in color (for the screen) as in black and white (per the limitations of most home printers — unless your audience is primarily people who will be scamming color copies).

PiNaB’s design means that it is not especially easy to read, either on screen or in hard copy.

The main problem was one running thread (apparently a subpiece called “politics is not a banananotes”?) that streams along the bottom of the pages. This piece is particularly hard to read — as it’s very broken up — and distracts from the other articles above it. Furthermore, the argument for design is that reading as a sensual experience is worth focusing on, which is contradicted by presenting the document in a way that de-emphasizes touch.

But life is full of compromise.

This document negotiates territory between appealing and funny, and between sincerity and jargon. One of the first images is an apparently appreciative, but perhaps ironic, picture of a masked frat-looking white boy grabbing his crotch in good wigger style.

There’s an argument that this sets a theme for this publication. There is a lot of talk about sex, and some about shit, in what is clearly attempting to reflect a transgressive integration of body and theory. Sometimes this works, but sometimes it just turns the body into another rhetorical device.

A brief philosophical and political introduction to the concept of post-civilization 
tangledwilderness.org

Another of the PDF / online documents that are becoming more common, Post-Civ! comes from one of the people who brought us the excellent magazine, Steampunk. That magazine floated above the conflicts that come from appealing to a broad base of people who frequently don’t get along with each other (science fiction aficionados, DIYers, crafters, anti-civ idealogues, etc). Post-Civ! is a more direct approach to the question of critiquing civilization while not necessarily being anti-tech, and promotes a civilization-critique-without-modifiers. “It’s about the anarchist urban hunter-gatherer squatting the ruins of the city living side-by-side with the micro-hydro engineer who has rigged the water running through the sewers to power her gristmill... It’s about never laboring again. (In this case, we are defining labor as ‘unnecessary, unenjoyable work’.) Frankly, it‘s about destroying civilization and saving the world and living a life of adventure and fulfillment.”

This attempt has honorable precedents. Historically Voltairine de Cleyre is the exemplar of anarchy without adjectives and the utopian novel ‘bolo ‘bolo by p.m. posits a future world in which contradictory lifeways will coexist as long as certain fundamentals (eg community size) remain stable.

While ideologues will be frustrated by the crossing of certain lines, it’s hard to argue with the three basic premises put forth for the definition of post-civilized thought:

Civilization is unsustainable and unsalvageable; it is neither possible nor desirable to return to a pre-civilized state of being; figuring out a good post-civilization is therefore appropriate. There are people who are so attached to the definition of pre-civilization as meaning all things good that they will have a hard time getting past number two, but rhetoric aside, there is nothing to argue with.

The writer(s?) of Post-Civ! takes the route that is more complicated in practice: not rejecting all technology but picking and choosing what works for a specific situation and what doesn’t, not rejecting science but also “not worshiping it.”

This will stick in the craw of those who see science and/or technology as an overarching philosophy, part and parcel of the problems that we face today, but the conflict may be a semantic one, something to determine as (and ii) the project continues. If we agree that we have been irretrievably shaped by our world, then the best we will ever be able to do in overcoming it is to be skeptical of and challenging toward the things that seem to push us to (or keep us in) the status quo.

Since being purist and heady is one way to support the status quo, and since being unreflective and action- or product-oriented is another way to also support the status quo, we will always be in the position of doing the best we can in any given situation.

Post-Civ! seems to err on the side of getting along in a milieu of people who don’t worry much about making friends, and therefore may not satisfy many of their most obvious audience (and will probably be the center of some conflict). But this is a fine little publication. The question is whether the producers are prepared to weather the conflict as bravely as de Cleyre did, and continue to flesh out the bones of their interesting idea.

PO Box 10785
Dublin 1, Ireland
www.ragdublin.org

I visited Ireland for a few weeks many years ago. I don’t have a strong knowledge of the place. I know the basics. It’s strongly Catholic: everything shuts down on Sundays, abortions are even more inaccessible there than they are in the US (unlike the rest of Europe, which in general has no question about whether women should be able to have them). The violence of ongoing warfare; its status as one of the earliest colonies; these realities make Ireland a very different place than the US. So it is hard to position myself relative to the feminist theory that I have read from Ireland, which, to my eyes, seems so reminiscent of the 1970s.

The Rag has articles, like so many dozens of other feminist zines, on herbal medicines, on domestic violence, on why there are so few women in anarchist scenes, on midwives, on the value of anger, and so on. Sadly, the content is no more unusual than the topics. Women don’t want to be part of anarchist scenes because there is a culture of macho posturing, for example. If I had a penny for every time I’ve heard that... I don’t know if it’s true, although some of the best anarchist posturing I know is done by women, myself included — what I do know is that I would love to read something that doesn’t talk about boys making room for girls, bur perhaps talks about girls taking their own room. Power, in any meaningful sense, can not be given. It must be claimed.
The one different thing in this publication is the article on sexworkers. While still couched uncomfortably in “men shouldn’t see women as meat” language, it is at least one indication that feminist arguments have moved incrementally farther than they were when good feminists didn’t talk about decriminalizing sex work.

There is a strong need for people, in groups and individually, to reconsider how women and men, girls, boys, and other, people in general, interact with each other and themselves. There are ongoing, decades-old, centuries-old, problems that relate to how we value ourselves and each other. The urgency of that need is only covered up, concealed, by rhetoric that was tired 20, even 30, years ago.

Like I said, I don’t know. Maybe this stage of dialog is an important part of what needs to happen in Ireland. But it would be great if somehow we could learn more from each other, instead of having to go over the same road again and again, reading the same tired signs that don’t seem to get us to where we want to go.

12 pages, no price listed
stephanemdc@hotmail.com

This reprint of a chapter from a book in French, A mort l’artiste, is a brief and scathing indictment of the artist-as-advanced-individual ideal. Starting with the history of artists under the patronage of the wealthy and powerful (whether noble or church-based), the author(s) move on to point out that currently artists are merely workers with attitude.

“Presenting themselves as the victims of the commodification of culture, they are actually simultaneously its result and one of its principal agents... being the social category recognized notably for its ‘right’ to subversion and transgression, the artist remains the best agent for the neutralisation of critique and its aesthetic recycling.”

While there is not that much that is new here — particularly lacking is an acknowledgment of the impact that aesthetics do have on our lives or the possibilities inherent in something like Oscar Wilde’s determined dandy-ism — it could serve as an wake-up call for any self-righteous liberal arts major you want to smack down.

Available online (in French) at lafeteestfinie.free.fr

Ron Sakolsky
Fifth Estate Books
Liberty, TN
$15, paper, 215 pages

Creating Anarchy works on a few levels — for example Sakolsky’s concise and clear critique of issues like democracy and voting are refreshing and valuable in these days of “anybody but Bush.” The first pieces in this book are bite size, e.g. interviews with Sakolsky and others that don’t go very deeply into any of the things that they talk about, and descriptions of Sakolsky’s experiences teaching or working on free radio projects. These are fine examples of lessons learned, but lessons that are easy to come by in most of our lives, so the audience is apparently young people without a lot of experience in this arena.

The best parts of this book come later, and are on the history and relationship between surrealism and anarchist (or anti-state) thinking.

Sakolsky is a fine historian, intimately connected with his topic(s), knowledgeable and accessible in tone. These pieces are not just his thoughts about the connections between these two fields, but also introductions to various surrealist painters, poets and musicians, for readers who want more information about this tradition.

Some discordant notes

interpreting quotations. Sakolsky quotes people and then explains what the quotation means. Is this because Sakolsky’s history with the authors gives him an understanding of what they mean that is better than what they actually say? Perhaps that is the case, but if so, using quotations is a confusing way to make the given points.

word play. I don’t know why someone who has a background in, and information about, poetry would do some of the goofiness that Sakolsky does here. Phrases like “snivilization” “realpolitricks,” and “evil of two lessers” are neither funny (although of course humor is in the perspective of the beholder) nor interesting commentary. Particularly irritating are simplistic references to animals as in any way relevant to state tendencies, as in “United Snakes of America.” It’s cheesiness like this that gives play a bad name.

Among the complicated philosophical concepts that are bounced around uncritically in this book are  the tropes of building-an-anarchist-movement, and life=good/death=bad.

It is easy and problematic to use the word “movement” as the way to talk about increasing the strength of anarchist ideas. The word has enough baggage, along the lines of democracy and glorification of the masses, to sound alarms. Nor is life is always good, or death always bad, but those associations too are taken for granted in this culture. Someone who has been around as long as he has, and who is conversant in the significance of dreams and storytelling, might be expected to have a more sophisticated understanding. Life and death are part of a whole, made significant by each other. Making one good and the other bad denies both.

The book also includes pictures from various surrealist artists, including Don La Coss, Clifford Harper, Sue Simensky Bietila, Cathy Stoyko, and others.

by Taylor Sparrow
Eberhardt Press

Only a Nod

If you are looking for a current, accessibly written book that talks about the history of US racism against black people, doesn’t demonize all white people, and gives some examples of projects for education reform, this could be the book for you. The author spent some time in a class in Douglas High School (in the 9th Ward of New Orleans), and examples from that class provide descriptions of where some kids are right now in their suspicion and boredom with school (and presumably with their options in general). The history of British colonization of Scotland and Ireland gives context to the history of Black people in the US. And projects like Students At the Center (SAC), Young People’s Project (YPP), and the Algebra Project are given as examples of people making a system that works for students — apparently in hope that such an educational system will encourage students to make a better world.

This book gets some important things right: a) schools are not failing but succeeding at their goal (which is to manage and create people who believe they have no options); b) that this is true regardless of the economic background of the students (although the tactics might be different for different classes); c) that saviors and charity don’t actually create serious change, and d) that memory is a big deal.

But if you’re looking for deep thoughts about memory and history and how we address or experience being cut off from our past, or stories that might actually (as promised) end some nightmares, then this book will disappoint.

The book’s most significant weakness is that it conceives of race as black and white. Asian people are not mentioned once, and native and latin people are thrown in as “and them too.” While of course engaged readers can make some connections, this lack indicates unsophisticated thinking about race and power. Race here and now is about so much more than Manichean “your team vs my team”. And the rhetoric of race, especially in activist circles, has so far to go to coherently address the issues of what is currently called “internalized racism” that it was very disappointing to have this book be so simple on this facet of the topic.

APoM’s most ironic failure is in its nod to a hopeful future. If educational projects like SAC, YPP and the Algebra Project are the best hope for a better future, how are they different from multiple previous education reform projects? History has shown that these kinds of projects are so easily integrated into the status quo as to be swallowed without a ripple. A quote from one of the author’s mentors, Kalamu Ya Salaam: “Unless and until [disenfranchised youth] can honestly recognize and confront their own realities, they will never be able to truly transform themselves and their communities.” Of course, the rub is that what some people mean by transformation is really not what others mean. Educational projects are a fine liberal goal, one that is easy to find support for since it is a deeply-held liberal concept that more information will solve all problems. There have been multiple efforts to empower students through various levels of student participation, from students organizing against wars to members of radical groups becoming teachers to effect change from within. A brief foray into a library reveals that in past decades there have been many high school students who were articulate about the racism and classism of the school system and who had hope that society could be changed. The efforts that are cited in APoM are working at getting students just to that level of analysis (by giving them skills and confidence), and there is no evidence (when and if they get there) that any more change will be effected than was 35 years ago. The question of reform vs. revolution, of what makes change, is only alluded to in this book, and the allusions don’t make a compelling argument. The author’s failure to acknowledge the history that exists here is in direct contradiction to the title of the book.

Finally, there is always a push and pull to memory. Remembering where we came from is crucial to knowing where we are, but we also best remember what best suits us or what we best understand, and what we best remember doesn’t necessarily help us to create different ways of being. Taught to be within structures that despise both us and what we long for, we are not necessarily capable of remembering the things that might be most important to who we want to be. This conservative role of memory is another nuance that is never acknowledged here.

The title of this work gives a nod to significant and powerful topics, a rich menu, but then offers up potato chips and miniature cucumber sandwiches, leaving the reader not starving but ready for something more.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



Passion Fruit: Anti-Authoritarian (Con)sensuous Games 
PO Box 63232 
St. Louis, MO 63163 
83 pages; $4



Politics is Not a Banana 
journal of vulgar discourse 
issuu.com



A brief philosophical and political introduction to the concept of post-civilization 
tangledwilderness.org



PO Box 10785
Dublin 1, Ireland
www.ragdublin.org



12 pages, no price listed
stephanemdc@hotmail.com



Ron Sakolsky
Fifth Estate Books
Liberty, TN
$15, paper, 215 pages



interpreting quotations. Sakolsky quotes people and then explains what the quotation means. Is this because Sakolsky’s history with the authors gives him an understanding of what they mean that is better than what they actually say? Perhaps that is the case, but if so, using quotations is a confusing way to make the given points.


word play. I don’t know why someone who has a background in, and information about, poetry would do some of the goofiness that Sakolsky does here. Phrases like “snivilization” “realpolitricks,” and “evil of two lessers” are neither funny (although of course humor is in the perspective of the beholder) nor interesting commentary. Particularly irritating are simplistic references to animals as in any way relevant to state tendencies, as in “United Snakes of America.” It’s cheesiness like this that gives play a bad name.



by Taylor Sparrow
Eberhardt Press

