      Electoral Campaigns Don’t Take Us Where We Want to Go      History Shows the Failures of the Left in Power      Electoral Campaign Work: Shallow and Superficial      Getting the Goods: Social Movements and Class Power
In the wake of the 2016 presidential election, the gravitational pull of electoral politics has gripped the left with renewed intensity. Fueled by the popularity of Sen. Bernie Sanders, discontent with political elites and the failure of the Democratic Party to defeat Trump, various segments of the left see an opening for breathing new life into building a “party of the 99 percent,” a “party of a new type” or a “mass socialist party.” Others are content running leftist candidates as Democrats under the guise of radical pragmatism. Given the history and structural limitations of such projects, social movements, activists and organizers should regard these calls with caution. If we want meaningful social change, or even basic progressive reforms, the electoral road leads us into a strategic cul-de-sac. Instead of better politicians, we need popular power — independent, self-managed and combative social movements capable of posing a credible threat to capitalism, the state, white supremacy and patriarchy.

The recent push toward electoral politics stems in large part from Senator Sanders’s insurgent primary campaign. For decades, Sanders occupied a relatively obscure position in the political arena. From his first stint in office as mayor of Burlington in the 1980s, to his recent years in the US Senate, Sanders’s lone voice against corporate power had little impact. Yet by 2016, the cumulative weight of deteriorating socioeconomic, political and ecological conditions, along with the growth of mass movements, laid the groundwork for the popularity of the Sanders campaign. Indeed, the political terrain had already shifted before Sanders launched his “political revolution.”

An oft-cited 2011 Pew Poll revealed that 49 percent of Americans under 30 had a positive view of socialism, while just 47 percent had a favorable opinion of capitalism. Disillusionment with President Obama, coupled with a steady stream of post-recession movements from Occupy Wall Street to Black Lives Matter, had significantly altered public discourse, expanded the field of struggle and pulled the broader political spectrum to the left. In other words, the Sanders campaign slipped through the door kicked open by social movements and brought a broad cross-section of the left into the electoral arena.

Following the Sanders campaign, a growing mix of old and new voices have been clamoring for the left to consider electoral struggles. For example, the Democratic Socialists of America (DSA), Jacobin Magazine and strategists like Max Elbaum at Organizing Upgrade have been some of the most vocal proponents of electoral strategies. They justify their calls in terms of fighting back against Trump and the far right, shifting politics to the left, and winning policy change like universal health care. Coupled with the recognition that we also need to build mass movements outside of the voting booth, these same organizations and individuals are promoting variations of an “inside-outside” strategy.

The “inside-outside” approach, which casts itself as hard-nosed, strategic and realistic, claims to hold out a possible middle path between focusing exclusively on movement-building and leaping headlong into the palace intrigue of beltway politics. Its advocates argue that social movements are of vital importance, but they can’t get it done alone: There needs to be a ballot-box strategy to punish bad incumbents, elect movement champions and enact real change by leveraging state power. In other words, as Marxist political economist Leo Panitch often says, echoing civil rights leader Bayard Rustin, we need to move “from protest to politics.”

Their strategy is characterized by the following three points:

• If we want victories, we need strong, militant social movements in communities and workplaces agitating on the outside, but we also need movement champions in elected office changing the system from the inside. Through election campaigns, social movements can expand their base and have the ear of someone in power who can be held accountable to movement demands.

• Political campaigns are an effective way to bring up vital issues, expose more people to left politics and provide easy on-ramps for the newly politicized to get active. After Election Day, no matter how we do, our politics have reached a wider audience and built movement capacity.

• Currently the Democratic Party is the most viable vehicle for our candidates if we want them to win, but ultimately, we need to develop our capacity for building an independent party of the left. Alternatively, some argue that the Democratic Party is beyond repair and we need to build an independent political party of the left now.

But this is wrong; elections are a trap with more costs than benefits. Political change is a question of political power, and the electoral arena is a field of battle that caters to the already rich and powerful. It hands our power to politicians. As a result, when popular candidates win electoral office without the backing of powerful social movements (even candidates of the left), they are powerless to take meaningful action. Instead, electoral campaigns drain movements of vital resources that could be better spent elsewhere. The electoral road is not a shortcut to power; it is a dead end — structurally, historically and strategically.

It’s often said that electoral politics is the graveyard of social movements, but that always seemed unfair to graveyards. After all, graveyards merely house the dead: They don’t actually do the killing.

Those who enter the front door of elective office are quick to find themselves in the house that capital built. Even those with the best intentions will find themselves boxed in on all sides by business interests and institutional constraints. For local and state officials, they must strain under the weight of a larger political and monetary system over which they have zero control, and which can override their decisions and policies at any time. For national officials, not only are constitutional and procedural restraints ever-present, but looming over every choice is the power of business to influence policy and one’s chances of re-election. Ultimately, the ruling class can always use the threat of capital strike and capital flight: A Wall Street crash, a bond rating downgrade, a panic, runaway inflation, currency manipulation and so on. The particular constraints may change based on what position they’re elected to, but the outcome remains the same.

Social movements that dedicate their limited resources to electing politicians end up undermining the very energy and capacity needed to hold those politicians accountable once elected. The resources spent electing someone would be better spent forcing whoever is in office to concede to our demands by developing popular power that cannot be ignored.

To illustrate that movements — not politicians — make change, it’s useful to look at history. In the US, the major periods of political change came when social movements — including labor, Black liberation, feminist and ecological struggles — were at their peak. New Deal reforms of the 1930s came when workers were occupying factories and shutting down cities with general strikes. Civil rights and environmental protection bills came at the end of the 1960s, when social movements were organizing for popular power, and disrupting the ability of business and the government to operate. It is often quipped that Richard Nixon, a Republican, was the last liberal president because he oversaw the creation of the Occupational Safety and Health Administration, the Environmental Protection Agency and other liberal reform measures such as the expansion of affirmative action. He even contemplated a proposal for a universal basic income and mandating employer-provided health insurance. This is not because he was a good-natured liberal at heart, but because social movements had changed the political terrain and forced his hand.

In periods without social movements, politicians fare much worse — even those that authentically believe in creating a better world. In Atlanta, Georgia, in the 1980s, Andy Young, the chief strategist, legal counsel and close friend of Martin Luther King Jr., ran for and won the city’s mayoralty, a position he held for close to a decade. By that time, however, the strength of the civil rights movement had ebbed, leaving Young a crusading reformer in office without the power base to make change. According to scholar Clarence Stone, Young faced widespread opposition from the city’s corporate business elite, preventing him from passing any meaningful reforms for the city’s Black population. Here, lone progressive candidates can do little without the backing of social movements. The phenomenon is true even for far-left candidates like socialist Seattle city council member Kshama Sawant. Her major reform, “$15 Now,” was watered down and transformed by business and business-union interests who created major exemptions in the law, giving Sawant a “victory” she could run a re-election campaign on, but not bringing meaningful change to working people in Seattle. To this day, many workers do not earn $15 an hour in Seattle because of employer exemptions.

In short, movements — not politicians — make social change. No movements, no change — no matter how far left the politician. With movements, social progress and shifting the terrain is possible, no matter how far right the politician.

Elections are designed with the needs of the state and capital in mind. Every step of the way — from the first donation to the final TV ad — is crafted to further stack the deck in favor of entrenched elites and draw people into a system that many have rightfully abandoned. There’s no bypassing the white supremacist, patriarchal, anti-Black and settler-colonial pedigree of the state: The true political power of people is always found and built elsewhere. Elections are at best a reflection, not a cause, of social change — using elections to change society is like trying to turn up the temperature with a thermometer.

The kind of outreach and mobilization efforts undertaken by campaigns is little more than a shadow of actual grassroots organizing, focused first and foremost on the singular transaction of the vote. Forget about a serious one-on-one conversation. When a campaign has 20,000 doors to knock on and it’s crunch time, there isn’t a spare minute to ask about the problems a constituent is having, or what issues they’re interested in. You must find out if they’re planning to vote, and if so, for whom. Give them some literature and a big smile, and be on your way to the next house. Every pancake breakfast, parade appearance and house-party fundraiser is geared toward building the candidate, not the movement. The unique activities of a campaign have very little to offer social movements.

Furthermore, if a left candidate wins, it’s a signal for their supporters to go home and disengage. Getting the candidate in office is the supreme goal of any campaign: the next steps belong in office chambers and committee rooms. “We get you elected, then you do good things for us,” is the rationale of electoral work. Staying active and organizing beyond Election Day goes against the core logic of the campaign itself. We need not look back further than a decade to find concrete examples of this dynamic. After Barack Obama’s historic election in 2008, his administration proved unwilling to mobilize millions of campaign volunteers in support of the Affordable Care Act and other political priorities.

Picking the Wrong Target

Organizing 101 instructs us to pick a primary target that can grant us what we want — be it a corporate board, slumlord or politician. The electoral campaign throws this out completely, focusing on a single elected official and the bad things they’ve done or stand for, while offering an opposing single elected official and all the good things they’ll do and stand for as the alternative. This personalization of politics is harmful to social movement-building because it reinforces the popular notion that our problems are not systemic and structural, but merely a problem of staffing, fixed by swapping in new and improved politicians.

The Media Horserace

Mainstream media coverage is usually trouble for organizers. But elections are a bit easier, and positive media coverage for important issues is one of the main strengths of electoral campaigns of this type. The fundamentals of electoral strategy — people should vote for me and donate, my top issues are x, y and z, and my opponent is bad for these reasons — are familiar to journalists. And they have a set of narratives they choose for their coverage: the outsider, the long-shot, the neck-and-neck race, the third-party spoiler, etc.

But even here there are serious pitfalls. While it can be exciting to have a candidate’s core message spread far and wide through the news, the surrounding narrative makes it often not worth it. Winnability will be the ultimate metric that the media will use to frame a candidate and their agenda. A fringe candidate’s issues can be automatically cast as dangerous and unpopular. A candidate running neck-and-neck with their opponent can have their bold ideas portrayed as politically risky, costing them precious votes.

Election Day: A Timeline Not of Our Choosing

For electoral organizers, dates of campaign climax — the primary and general election — are set in stone. It doesn’t matter if we’d prefer to move it up a few weeks to capitalize on an opponent’s scandal, or delay it until some key community leaders can focus on the campaign. The date is set, and that’s it. Workers know to time union elections and contract fights based on a timeline that offers them the most strategic advantage and greatest ability to harm the owners. Tenant organizers plan their campaigns around the cycles of the housing market to find the best moment to withhold rent from a slumlord. Student organizers ensure their protests and strikes coincide with trustee meetings, alumni days and parent weekends — occasions when the stakes are highest for administrators. With political elections, however, once the votes are cast, you’re done; there is little way to escalate, or for broad-based movement-building to develop.

When political elites agree to adopt progressive reforms, it has never been because of a burst of sympathy for those of us at the bottom. It’s been because they saw a systemic, existential threat to their collective power that made concessions unavoidable. We didn’t get Social Security, the Wagner Act, or the eight-hour work day because of electing the right individual politicians, winning primary fights or clamoring from the sidelines on behalf of a third party. We won them because we had built massive, militant movements that threatened open revolt against our nation’s economic and political rulers.

For those of us who want a world beyond capitalism, we know that we should be spending our limited time, energy and money investing in people-powered movements strong enough to topple our unjust social order. For those who want reform, understand that the only time liberals and progressives in power actually make good on the reforms we want is when we’re capable of posing a fundamental threat to the status quo. Following the “Great Recession,” President Obama said in 2009 to the nation’s bankers that, “I’m the only one standing between you and the pitchforks.” We don’t need more Obamas, or even Sanderses and Sawants. We need more pitchforks.

Despite hopeful spurts of activity, social movements in the United States remain weak, unable to impose their demands beyond a small scale. While most advocates of electoral politics acknowledge that the balance of power is not in our favor, they argue that running candidates — or better yet, winning elected office — will complement or strengthen social struggles. However, the historical record is clear: Electoral campaigns tend to defang, demobilize and drain social movements of limited resources, not strengthen them.

We should resist the calls to organize as an electorate and pick up once again the task of organizing as a class. Only through popular organizations that are democratic and accountable to their members, can we improve our living and working conditions right now while building the power needed to create a better world. These combative popular organizations should be based on our particular location within the economy and society: labor unions at work, student unions at school, tenant unions at home, popular assemblies in our neighborhoods and communities. They’re important not just because they are the sites of struggle most accessible to us as individuals, but because they amplify our power to disrupt and halt the flow of production, distribution and profit. More importantly, they are the necessary basis of a society free from oppression.

This is not a call to disengage from politics, or somehow to operate outside of capitalism and the state. It is exactly the opposite — a call to engage in politics, organizing, and the state in the only meaningful and empowering way available to us. Because we exist as objects, not subjects, of the economic and political system in which we find ourselves, our true power lies in our ability to collectively disrupt, dismantle and replace that system. The state in general, and electoral outcomes in particular, play a critical role in shaping the political terrain in which we all struggle, but we don’t need to “take” the state in order to affect the playing field. You don’t need the excuse of canvassing for a politician to knock on your neighbor’s door; you don’t need to cast a vote to influence an election; and we don’t need a campaign rally to advance our vision for a better world.

Dedicating precious resources to electoral work isn’t just a mistake, it’s malpractice. While many socialists rightfully refuse to try to take back the Democratic Party, the perpetual appeal to independent party politics maintains an instrumentalist approach to the state, fostering the illusion that with the right people in office, along with the right balance of forces, we can wield state power to advance our interests. But even if we want limited social reforms, electoral strategies are dead ends. At the moment, we’re all short on people, resources and — thanks to climate change — we’re short on time. Instead of an “inside-outside” approach, it’s time to commit ourselves to organize where we live, work, study, play and pray — outside, against and beyond the current system.

This piece was originally published at Truthout.org. Addtional hyperlinks for this article can be found in the original link: http://blackrosefed.org/lure-of-elections/

Frank Ascaso is a historian, active with Seattle Solidarity Network and member of Black Rose Anarchist Federation based in Seattle, Washington

Enrique Guerrero-López is an educator and a member of the Industrial Workers of the World and the Black Rose Anarchist Federation based in North Carolina.

Patrick Berkman does graphics design work and is a member of the Black Rose Anarchist Federation. He is based in Burlington, Vermont.

Adam Weaver is a member of Black Rose Anarchist Federation and based in Miami, Florida.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

