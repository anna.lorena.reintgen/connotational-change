
Kropotkin once observed that revolutions are born from the fires of hope, not from the pit of despair.  This observation is as vital now as in the time of the Paris Commune.  Likewise, invitations to despair are no less rampant.  Banks and Wall Street gamblers exert a distant control over the economic conditions of life.  Governments serve the interests of the few while regulating the misery of the many.  An empire built on war and exploitation holds humanity in a state of passive spectatorship and terror.  And now, with the arrival of a new age of “financial crisis,” disciplinary mechanisms are being introduced that will drive the masses further into servitude and desolation.

If hope is revolutionary, false hope is bitter as a dream deferred.  The U.S. political establishment has always maintained its authority by feeding on the optimism of popular struggle – and the pseudo-prophetic presidency of Barack Obama is simply the latest attempt to drain popular energy out of independent social movements and into the channels of State power.  This strategy becomes more expedient in times of institutional crisis, when the masses are forced by circumstance to seek alternative ways of meeting their basic needs.  At such times there is a drive towards self-government, and the ruling classes must act quickly to stabilize the system and reduce collective desire to manageable forms.  But such reactionary measures are purely superficial; all positive potential remains with the spontaneous movements themselves.  Even corporations and G-20 financial advisors now co-opt the discourse of radical ecologists and counter-globalization activists when projecting their public image.  Crisis brings out the anxieties of the population, but it also catalyzes new dreams and intelligences.  People everywhere are ready for a different way of living, if only the mechanisms of normalization and consent could be broken down.

The first step toward breaking down these mechanisms is to realize that they are not mysterious forces outside of us, but the products of our own social activity.  Capitalism would not exist without people who organize their lives around work and invest their desires in a world of commodities.  Militarism would not be possible without the complicity of taxpayers, the aggression of soldiers, or the routine subservience of employees for Bechtel and Halliburton.  Fascism is nothing other than the erotic character-structure of millions of men and women who pledge their obedience to God, family, and country.  Even the regime of technocratic control that defines the current “crisis” could not be enforced without an implicit belief in money, the government, the stock market, and the speculative financial industry.

Considered together, these habits make up a vast, self-destructive machinery that will not be overcome unless the entire complexion of everyday experience is transformed.  In effect, what is needed is a new ethics, a new set of practices that will lead to the creation of new forms of collective existence.

One neighborhood in Chicago – Pilsen, on the lower west side – contains the potential for this kind of utopian ethical practice.  The neighborhood was named during the late nineteenth century, when its principal inhabitants were German and Bohemian factory workers, but since the 1950s it has evolved into a community of mostly first- and second-generation Mexican immigrants.  Español is by far the most common language, and the streets are lined with locally-owned shops, taquerías, pool halls, and hundreds of working-class households.  Most non-Mexican residents of the neighborhood are young, low-income families, some with immigrant backgrounds, along with a mix of artists, outsiders, radicals, and non-conforming layabouts.  A large number of Mexican youths are involved in the local punk and underground music scenes.  Independent bi-lingual media collectives broadcast through the local airwaves.  Guerilla painters enliven the crumbling neighborhood landscape with colorful murals, graffiti, and street art.  Cafés (many with live music and urban Zapatismo spirit) are abundant, as are public parks and schools, and the sidewalk atmosphere is a blend of village solidarity, workaday drudgery, and anarcha-feminist counterculture.

There is no shortage of trouble in the neighborhood, with police brutality and anti-immigrant racism counting among its worst sources.  Internecine street violence – mostly over drug money and territory – persists, but is not as frequent as, say, fifteen or twenty years ago.  Mostly, the neighborhood is under siege from predatory forces seeking to exploit its resources for profit and political gain.  The University of Illinois-Chicago pursues a continual expansion south and westward, and construction projects linked to its hyper-commercial “University Village” have already plowed through several poor communities on the lower west side.  (In fact, it was UIC’s incipient expansion that originally displaced Mexican immigrants into the region known as Pilsen, pushing out the last remaining Germans and Bohemians.)  Real estate developers continue to seize land (often through diabolical, city-sponsored measures) in order to build high-rise (and high-rent) condos, fashionable nightclubs, and upscale restaurants, all of which serve a growing influx of money-making young professionals.  Banks and mortgage companies have foreclosed on hundreds of local properties, many of which now sit vacant, some awaiting demolition.  Multiple coal-fired power plants operate within yards of the neighborhood’s borders, filling the lungs of residents with an incessant stream of toxic pollution.

These forces are a constant encroachment upon life in the neighborhood, but they are not enough to drive the inhabitants of Pilsen into submission.  The agents of gentrification are met with fierce resistance from a broad coalition of community groups and independent social networks.  There is a profound sense of loyalty and protectiveness within the neighborhood – both among Mexican immigrants and within the community at large – and residents are quick to defend one another against the bullying tactics of cops, landlords, corporate developers, and city officials.  It is this spirit of autonomy and self-determination that makes the neighborhood so unique – the sense that Pilsenites have their own way of doing things, their own values and common convictions, and they don’t need Starbucks, Bank of America, or the Democratic Party to show them how to live.  Responsibilities are shared on the basis of know-how and individual association; resources are distributed according to need, and without the mediation of faceless corporations.  Productive work and political self-management are woven into everyday communal life; these and other activities are performed for their immediate social value, not in the service of profit or State authority.  Each act has a concrete cultural meaning; each relationship has a symbolism and ritual power that reveals itself through a constellation of direct experiential encounters.  Pilsenites have not escaped the commodity-system altogether – most of them still exchange goods for money and sell their labor-power in order to survive – but they have planted the seeds of a form of life in which the drive towards commodification is actively and cooperatively resisted.

Within this spectrum of counter-powers, there are groups that wish to go further than monitoring the cops or breaking down the hierarchy of corporate ownership.  Localization and resistance are necessary, but local practices can only become self-sustaining if the necessities of life are removed from the grid of capitalist exploitation and placed in the hands of the face-to-face community.  In Pilsen, de-commodified mutual aid has developed as a natural extension of localized relations of gift and exchange.  Potlatch, Vokü (folk-kitchen), and “Really Really Free Markets” have proliferated as forms of livelihood become integrated with spontaneous arts of friendship and cooperation.  Food not Bombs prepares meals with ingredients salvaged from the wasteful food industry and shares them with community members once or twice a week.  Pilsen Community Market offers ecologically grown herbs, legumes, flowers, fruits, and vegetables – as well as pies, cheeses, beeswax products, and crafts – every Sunday from June to September.  Bike trailers, recycling centers, and guerilla gardening collectives contribute to the growth of a noncapitalist material infrastructure.  Even the control of local housing by banks and real estate agencies has been undermined to some extent by the emergence of squatters’ movements, tenants’ self-defense networks, and creative occupations of privately-owned land.

In some instances, neighborhood initiatives to oppose gentrification and build local economies have been infiltrated by city departments, political sects, and corporations seeking to advance their own agendas.  Community development programs and housing alliances sometimes find themselves collaborating with corporate lenders in order to satisfy the immediate material needs of neighborhood residents.  Powerful Chicago-based companies like Boeing, ComEd, and Sara Lee have insinuated themselves into local projects (art exhibits, music festivals, media events) by offering money in exchange for status as official sponsors.  Many Pilsen organizations are chronically short on funding, so the temptation to rely on wealthy corporate donors is strong.  And yet this reliance solidifies the control that corporations already exert over life in the neighborhood, introducing new barriers to democracy and economic self-management.  Challenges of this sort have at times led to conflict within the neighborhood, exposing divisions between immigrant organizers who are determined to seek every available means of self-empowerment and young countercultural radicals who are more fiercely anti-corporate.  (On the other hand, many non-immigrant projects have also accepted corporate sponsorship – for example, from the University of Illinois, which presents itself as a stalwart benefactor of grassroots institutions.)

Chicago is a city in which the local government likes to have a claim on every citizen project, whether State involvement can be justified on practical grounds or not.  It is difficult to organize a large-scale concert or lecture-series in Chicago without being forced to display a plaque with Richard M. Daley, Mayor inscribed in giant letters at the top.  In Pilsen and other immigrant neighborhoods, the city has installed so-called “empowerment zones,” which are supposed to help new citizens with the development of independent entrepreneurial and non-profit ventures.  In reality, these agencies have the function of initiating immigrants into the city machinery, while ensuring that the self-starters among them are adequately disciplined and reconciled to their subordinate rank.  Meanwhile, and despite the extensive organizing efforts of Pilsen residents, Democratic politicians exercise considerable sway over local decision-making and neighborhood planning.  The Green Party, International Socialist Organization, Revolutionary Communist Party, and a handful of other left-wing sects constitute lesser, but by no means insignificant, threats to local autonomy.

Many Pilsenites are actively involved in (immigrant and non-immigrant) labor organizing, with some counting themselves as members of the anarcho-syndicalist Industrial Workers of the World.  Quite a few local residents participated in the recent occupation of a factory by workers at Republic Windows and Doors, both as allies and as dispossessed employees of the company.  Others in the neighborhood cultivate a more subversive orientation to work, supporting militant labor struggles while pushing for radical alternatives to a work-based existence.  In all of these cases, the tendency is towards spontaneous democratic engagement – as with the Pilsenites who helped to organize the Immigrant Solidarity Network’s national conference in 2009, or the anti-militarization activists who fight tirelessly against military recruitment in Chicago’s public schools.

There is one space in Pilsen, however, that embodies the neighborhood’s spirit of resistance and self-valorization more imaginatively than any other.  This is Lichen Spiritual Archives, located on the inconspicuous 1900 block of South Blue Island Avenue.  Lichen began as the vision of a handful of young anarchists, some of whom had previously been involved in the D.I.Y. renovation of an abandoned warehouse in nearby Little Village.  As the project developed – a kitchen was installed, a bathroom constructed, a loft built, and a band of volunteers assembled – what was once a modest dentist’s office became an important gathering place for artists, anarchists, adventurers, and freedom-loving people from all over the city.  One of the first practices to take root at Lichen was the preparation of free community meals (under the name Comida no Migra), eventually leading to the informal maintenance of a full-time community kitchen.  At the same time, materials were being collected for Lichen’s multi-lingual lending library, which has gradually expanded into its most widely cherished and utilized resource.  The library contains not only books, including many excellent Spanish-language titles, but a vast archive of zines, pamphlets, newsletters, journals, and ephemeral organizing documents.  The resulting wealth of intelligence has been vital not only to members of the Pilsen community, many of whom lend and borrow with great frequency, but also to captives of the Illinois prison system, whose reading habits are encouraged by regular shipments from the anarchist group Midwest Books to Prisoners.

Over the past few months, Lichen has become a major site of experimentation for anarchists and revolutionary dreamers of all sorts.  But its potential as a social laboratory has not been confined to the activities of a single subcultural group.  On an ordinary day, one might expect to meet several Spanish-speaking friends from the neighborhood, a traveling painter or poet, a couple from the nearby homeless shelter, and at least one or two queer militants from the revolutionary group Bash Back!.  One will find nearly as many people over thirty as under thirty, especially on the weekends, and the day’s scheduled event could just as easily be a vegan cooking lesson as a bi-lingual editorial meeting (perhaps for the free Pilsen publication Paloma Negra), an anti-war puppet-making workshop, or a musical benefit for the Chiapas Media Project.  Most of the individuals who host events at Lichen are acting not as experts, but as students of direct experience, and their purpose in gathering together is to share the fruits of their own experience with others, to impart a gift that will provoke new collective experiments in revolt, imagination, and conviviality.

Taken together, these impressions of life in Pilsen reveal a community whose everyday practice disrupts the dominant civilizational patterns of hierarchy, submission, and standardization.  Pilsen is not a perfect example – its residents often act defensively, or out of desperation, and its local economy remains parasitic on capitalist relations of production and consumption – but there is hope to be found in Pilsenites’ collective refusal to surrender, in their daily struggle for dignity, and against dehumanization.  Whether it is by standing up and saying ¡Ya Basta! – no more ICE raids, no more gentrification – or by inventing new styles of creativity outside the work-based economy, members of the Pilsen community are engaged in a constant battle for hope in the midst of organized self-destruction.  By persisting in this battle, and by bringing their energies together to form a localized network of cooperative counter-powers, they show that the corporate-imperial takeover of the planet is not a foregone conclusion.  It can be prevented through the growth of face-to-face community action – autonomous, joyful, and direct – for such action is the ultimate source of revolutionary hope at a time when the general tendency is towards powerlessness and despair.

Chicago, IL

Summer 2009

 




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

