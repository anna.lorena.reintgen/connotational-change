

-Xavier Massot

Spring, 2001, Vermont- It is a strange pill to swallow, the notion that some of your peers, people you don't know, represent your interests. It is an old tradition, it has given most of us the title of civilians, and it has made others our rulers. Although each life, upon a short and close look, obviously contains its own unfathomable trajectory it has been the way of the modern world to pigeon-hole and harness the energy of each life towards historically questionable ends. This practice, abusing the young in order to eternalize the old, has led us to a point in history in which lives are spent for the sake of the machine's continuing momentum. Inane, insane, or otherwise, we all go through the hopper from our birth to our determined, predictable, and sometimes socially necessary, death.

This text is an attempt to communicate the reality of a world in which your standard, modern day politician is a buffoon. We all came into this world kicking and screaming. The supposed facilitators of our homes, towns, and countries have no more insight than anybody else. There are people of great capability who outshine others through their efforts and convictions, but these good people will rarely strive to rule. They are usually too intelligent or humble to seek the cult of power and personality that is the reward and goal of modern politics.

Perhaps, rather than buying into this brokered sacrifice of our brothers and sisters in a quibble over the choicest scraps from the table, the old tradition of survival is one to be revered.

My aim is not to lay blame on those before us, but rather to attack the misuse and propagandisation of instincts which have allowed our clawless and furless breed to survive. Dan Quayle, for example, telling us about family values. Families happen, they are not state property, and the overzealous concern of officials towards the expansion of humanity does nothing but undermine any real attempts at human progress.

This book is aimed at humanity's current trajectory, and in doing so must address the arc of that trajectory, including its root in the past. I personally have no desire to re-write history, and I respect my elders for their time, endurance, and sacrifices. This being true, it is still time for a shift in human affairs, a statement made not to belittle those who came before but rather to bring awareness to those who's turn it is to carry human life forward. Technology has brought much to our lives which is admirable, but at this stage it is weakening people rather than helping them. How many people these days can fix the very appliances which give them status?

The inventions of this century are praised as the inventors are forgotten. Right now, the praise goes to human-vultures who circle around the people really producing and inventing things, looking for facsimile prospects to snatch up and feed to overgrown babies. It is a cannibalistic search for a fading profit that yields the pettiest of fruits.

Here in the U.S.A. We've got most of the shit. We might be spiritually bereft, but we do manage to keep our bellies full and for a great many of us that is enough. Love it or leave it, or even better, hide in it and try to live through it. Those are the options that many of us feel as our only possibilities. The political sphere is painted as our only multi-social option, but the political life is a drab and merciless trap. It's a lot of talking, haranguing, back¬stabbing, and ass kissing. I believe this to be true and that most sane people do right to stay out of it. The problem with this is that political life remains attractive then only to the power hungry mongoloids. The result is a world run by self -serving fools.

There are a great many people on this planet these days and they're not all that good at being on it. We are not the nicest bunch and I'm personally not sure that anything will ever make us so. I can't pretend to know why this is, but I think about it from time to time and the daily news does not alleviate my concerns. I'm fairly adapted to most of the bullshit that gets tossed around, but, being one among many and being a reasonable person, I am forced take certain stances against the killing bastards that sit on executive boards and those who make a living upholding this rotten system.

So yes, shit is fucked up, and the bastards are facilitating more shit. However, the question of who is fucking whom, long the topic from Abbie Hoffman to Sixty Minutes, is not as interesting nowadays as maybe it once was. In a word, we're all getting fucked. Even though we still get to be young, the world is just getting older. This being recognized, we still must face the fact that we have to continue to survive on this aging planet. And in that, we must make survival meaningful, or at least interesting. In this regards the status quo must be judged as a failure and revolution, one way or another, must be understood as the embodiment of hope.

Still, with practical caution, if baby heads are to be crushed by our stampede, I guess we better be serious about the dangers of revolution. Legitimacy and integrity are important things after all and although political people through the ages have turned both towards and away from them to get to power, it is certainly rare that they have actually made use of them in their daily diet of action. Most of us, one way or another, want to live good and happy lives without raping the earth or our neighbors. Most want a life both civic and personal. We want our good dreams to become reality, and our nightmares to be overcome.

So I'm having a cup of tea—it's pretty good and the reason why I'm including it into this sloppy introduction is because it's concrete; it's smarter than my literary aims and better than the whiskey I started this rumination with. Well not better, but for the moment calmer. Any which way, it is real, and in that capacity it is better than pure imagination. It is in just such a manner that we must continue to strive for a real, and better world. In this, revolution becomes our tea, and agitation becomes our tea pot—just now beginning to boil. But, before I get ahead of myself and make a habit of ridiculous metaphors, there are some more things that must be said.

My colleague is doing the dirty but necessary job of writing a chapter which will satisfy the socio-political aims of this book, which means I don't really have to. I have faith that his word is as good if not better than mine, and I get frustrated trying to explain anything in that historical voice, so I won't try to beat him to the punch on that one. After all, we should all know better than we do; there's plenty of manuals out there. Besides, most of the supposedly new thoughts are remedial and/or derivative anyway, just rehashings, repetitions, of no use to anyone.

The question starts to become, in the face of all this human noise, a question of territory, space, area, sphere of influence, property, taxation, and paradigms of life and its purpose. In many ways this closing-in of the options, this modern demarcation from oneself to the rest is healthy if not inevitable. It is the pressure which allows you to exist. Life demands some sort of test from its recipients.

There have been, and are, many people on this earth. The hardships have changed many times, but new lives keep on happening, blank slates who catch up and join in the line. This cannot be helped. Arguably, we live in better times than most of those before us, but there is and maybe always will be a malaise, an itch, a problem. Perfection is rare.

The fight of today may be the backwards cause of tomorrow. How? I'm not sure, but I do know that there have been many struggles. There has been no time without a struggle. Is struggle a problem? No. Struggle is as unavoidable as fertilization. Does that mean we should struggle because it is unavoidable? Are we, humans, aspiring to make struggle another final victory against God? Mussolini wanted punctual trains, Hitler wanted a master race, Uncle Sam wants You for the U.S. Army, Sick people want a cure, Everyone wants something.

Is it gonna stop? Whose gonna stop it? Well...

The truth is no one's gonna stop it, no one can. It's not that it's malicious, it's just not there simply for your sake even though you're a part of it anyway.

This tangent pertains to a book like this because the book is about struggle, it's about a timely choice, about what one should do and what side one should be on. It is a call to arms in most respects and at the same time it does not wish to mislead. The call to arms is against the ways of the world, which are unsatisfactory, and is aimed both at those who created the show and at those who may keep it going. These latter are problematic in that they are a part of yourself. You and I cannot destroy each other and ourselves, even in part, to change ourselves and the world we make. You know people who do wrong by your beliefs but you understand their language, their reasons and, then, the fight really becomes a dirty one.

I mentioned blank slates a little while ago in describing new life. It is the tarnished blank slates which we are that need to be cleaned out. A choice must be made as to what side to be on for the betterment of life. One can choose oneself as the ultimate piece of prime property or one can make arrangements to both survive and enrich the larger environment.

The struggle is not to blame, we are creatures that age and die, but the struggle cannot become a fakery of itself. Pain will always exist, but should not be robbed of its dignity. I question the dignity of this era. I see little encouragement for true ability. I don't question its existence but I don't believe that the positive is encouraged. In fact, I believe that it is being undermined almost totally. Where are the good qualities? I don't mean that I don't see them or know about them when they are here or elsewhere, I simply mean that they are usually missing in a way I see as suspect. The efforts of mankind to create a beautiful world seem, to me, suspect. If we can fly people to the moon why can't we feed the hungry?

There are problems which need solving: pollution, warfare, rape, disease, AIDS, racism, property, taxes, countries, the past, cancer, obesity, drug abuse, drug legalization, famine, housing, electricity, and so on. A bunch of shit needs to be re-done, re-thought, re-applied until humanity gets it through its abused and abusive hide that it does not have to destroy itself through lies and chickenshit proclivities.

Bottom line, this call for a better world is redundant. I don't like it at all. It has an unrealistic side to it. And here, I don't believe in it because it implies a desire to take away accidents. I like accidents. Don't get me wrong, some accidents are better than others, but we've got to take some chances don't we? Aren't we just a bunch of crazed animals who've pushed sentience too far?

It's a horrible mindfuck to the soul of humanity, the advertisements, the lies, and these wingnuts at the top of the shitheap: politicians, religious leaders, and fat-assed money makers telling you how to do it, why you should, when you should and why they know better. The truth of it is that life is not as much of a complication, at least not functionally, as these powerfreaks make it seem, and they're not that important. What is important is the enforcement of their bullshit system by hapless fools who agree with them and feed off their scraps.

In the past when we humans did not have the powers over nature that we have now it made more sense that we fought amongst each other. Life was tough then, it still is now, however the pressures are different. It's not like the hungry can't be fed, it's just that we can't stop starving them.

That last line is probably as close to the truth as I'm going to get, and that's really the crux of the problem we are dealing with, the sickness which makes revolution necessary. 'Pain must be inflicted on the weak because they are not the strong. The wrong must be punished because they are not the right.' The problem with all of this is that the wrong, the right, the weak, the strong, the fed and the starved have become artificial. When I say artificial, I don't mean to say that these things are no longer real, I am saying that they are unnecessary, and solvable. They are only insoluble from the standpoint of the corrupt, bureaucratic, greed-based and inane political bodies which the human mass concedes to.

The physical grievances and the physical bodies which stand in the way of alleviating them are two sides of the same coin. That coin can remain un-flipped, unresolved forever in a vacuum for as long as humanity keeps up its little intrigues and its little worries and its petty hatreds and its petty fears and blah, blah, blah...

So what am I saying? That I want world peace, that all the orphans should find good parents, that all the sick should heal, that all the hungry should be fed, and that all the bad things should disappear forever? Sure, but any hard-bodied beauty pageant gal could tell you the same. In fact, a lot of people have probably told you the same, and I'm sure they all meant it and I know it hasn't happened and so do you. So are we all wasting our time, good-natured people that we are?

Well, often times I think so but I can't know what most people are doing. In fact I don't want to know what most people are doing. A friend of mine is fond of saying that "there is no justification without representation." I agree with him, which is what has led me to get involved in this book about the Black Bloc—these protesters who face off with riot cops in cities to express their desire for an end to the lies and the greed which make all the world peace seekers so sad. Yes, they are correct. I believe that they are right, these brave men and women who aren't afraid to get beaten and tossed in jail to make a point.

A lot of people object to Black Bloc on both sides of the protest fence and in the homes where news is heard, watched, and read. I understand a lot of the grievances and I disagree with all of them. I'm not saying that the Bloc will end the world's problems. I don't know if anything will end the world's problems. I am, however, certain that physically confronting authorities which physically uphold a rotten system and reminding the rest of the populace that such things can be done is healthy.

I don't think it does any harm—it's a pure act. I don't care if the tactics are not always sound, or if you consider violence in all its forms immature and evil, or if you think that they give anarchists a bad name, or if some of them are middle or upper class, or if there aren't enough minorities, women, and gays taking part, or anything else really. Bottom line is that they are here and there trying harder than most of the douche-bags that complain about them.

The landscape of the world will alter, is already changing, that's what it does—some times are better than others and some worse, and nothing will change that. Stick to the good, try to take away the rotten, and have a few laugh while you're at it. Fuck, let's start having more musicians, artists, brewers, cooks, jugglers, bull fighters, dancers, old storytellers, mystics, comedians, boxers, all that good shit.

Okay, it does sound idealistic to demand a society that does not gorge itself on the failures of its absurd program. However, I see the possibility of it all the time. People can help each other, especially in the U.S where the amount of wasted food and property, if re-directed, could make kings and queens out of its homeless population. Are some of us regulating the suffering of others for the sake of a tradition which even they don't want to understand? Yes, that's what's happening!

Getting away from the instinctive fear of not having enough is the next real bridge to cross for humanity. Our ancestors had to find ways to survive. The world today knows how to live, yet refuses to do so in an equitable matter. A work ethic is a great thing, that is undeniable, but to work for the sake of working is nothing but a slow cop-out suicide...

So yes, its time to re-learn what is important and throw away the superfluous bullcrap in the process. Anarchism may not be Plato's idea of perfection, joy, function, or any of it, but then again we've tried it according to shithead elitist philosophers for long enough and all they have ultimately given us is nihilism mixed with a $12 butterball turkey and bad Friday night T.V. I say lets re-do it according to OUR inclinations and in the process come up with a reality which is categorically anti-boredom and genuinely interesting, directly democratic and equitable. Lets work it so that death again becomes genuine; either inflicted upon oneself, or dictated by real nature. Lets eliminate the role of society as murderer and rapist. If were going to fuck up, lets do it ourselves without unnecessary abstractions guiding and excusing our treachery.

Xavier Massot, an original member of the Green Mountain Anarchist Collective, is an artist and musician (co-founder of the rock n' roll band The Putnigs) residing in Montpelier-Vermont. Xavier has labored as a bartender and an archeologist. He is a past member of the Service Employees International Union Local 509, Teamsters Local 1L, the United Electrical Workers Local 255, and co-authored the Black Bloc Papers. Xavier, originally from Brittany-France, works as a butcher.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

