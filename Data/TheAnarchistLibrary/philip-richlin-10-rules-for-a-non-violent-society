
The question "should we have rules?" is not the right question to ask. We need a society with rules, in fact a society with rules is inevitable. "What should be the goal of such rules?" and "What rules should we have to achieve such goals?". Those are some of the questions we need to be asking. Our end goal should be to maximize biopsychosocial wellbeing of all (or as many as possible) which implies concern for our global ecosystem since we are dependent upon our global ecosystem. The rules should be specific enough to allow for correctness, yet broad enough to allow for diversity within that correctness.

The first rule is free association. Free association has nothing to do with non determinist definitions of free will (the idea that we are free from context), nor does it have to do with the freedom to exploit other people. Free association has three components. Freedom of association (freedom to associate), freedom within the association (decision making power proportionate to how the decisions effect you), and freedom from association(freedom to disassociate without harming others).  Freedom from association does not mean the ability to dissent and then violate the consent of others. For example free association does not permit you to get up and say “I’m free from the association" and punch someone in the face. Nor does freedom from association grant you the right to start a violent top down organization (such as a capitalist business or a state). Free association is a psychosocial need.

The second rule is participatory democracy. This term is problematic but the train of thought is essential. The term is problematic because of the common association of democracy with a representative form of democracy, or a majority rules democracy without free association. However participatory democracy advocates none of the above. Participatory democracy is defined by decision making power being proportionate to how decisions effect people. Participatory democracy has a built in defense mechanism against authoritarian relations. Participatory democracy can take many forms. One form is majority rules within a free association. This is not mob rule, nor does it allow the minority to oppress the majority. And the ability for people to dissent from a collective is meaningful in a needs based economy, unlike capitalism where one's options are often "work for a boss or starve/suffer" conditions. Work for a boss or suffer is a threat more than it is a choice. Since one's needs are met in a needs based economy, there is not economic coercion to join collectives one doesn't want to join. Majority rule is only problematic outside of freedom of/from/within associations. The majority of a group expressing a rational preference is not the majority of a group oppressing the minority(similar to how two out of three people in an association wanting to see a concert that the one person does not want to see is not oppressing the one person). Another form of democracy is consensus. Consensus has issues. It can often be the lowest common denominator rather than what people want to do. However consensus can sometimes be by far the most ideal when it is achieved organically rather than procedurally. Then there is participatory  deliberative democracy. Participatory Deliberative democracy proposes deliberation(the process of thoughtfully weighing opinions prior to voting) as a mechanism for arriving at decisions. Even if ALL(or most all) industry was COMPLETELY(or almost completely) automated, we would still need and want participatory democracy within free associations when we associate with each other. (((If participatory democracy  appears foreign to you in regards to lived experience, the chances are that you already utilize participatory democracy amongst friends when arriving at preferences))). The biggest problem with participatory democracy within a free association is not the fact that people are making preferences.  Problems arise when the preferences are irrational. To minimize irrational preferences we ought to have consensus in regards to the scientific method(which can only happen through education).

The third rule is the use of compassion, logic, and the scientific method as mechanisms for arriving at decisions. The intent of compassion is necessary but insufficient in and of itself for maximizing wellbeing of all. If we have the ought statement of we "ought to maximize biopsychoscial wellbeing" but we use incorrect is statements then our ought statements can be flawed and even dangerous. If we want to be serious about compassion then it cant just be an intention, we need to actually arrive at the consequences we are intending for. And the scientific method is a rudder that allows us to aim our compassion.

The fourth rule is decentralization of power. Merriam Webster defines decentralization as “the dispersion or distribution of functions and powers”. Central planning (like irrational planning) is incapable of meeting human needs because it is disconnected from free association which is a psychosocial need. Decentralized planning on the other hand does not suffer from the ignorance of irrational planning nor the violence of central planning. Decentralization of power also creates resiliency. Centralization of power leaves society vulnerable. For the society is forced to be dependent on a centralized power structure. However decentralization of power makes societies less effected by errors. One decentralized component fails and there are others ready to take over the function of the failed system and/or contribute with mutual aid to help the failed system. However even if decentralization of power was less resilient than centralization of power(which it isn’t) we should still advocate for decentralization of power because it is based on egalitarian principles (and more wellbeing). Its important to stress that advocates of decentralization of power are not against legitimate authority(such as a doctor, a shoemaker, a solar panel engineer, or a teacher). Authority is different from authoritarian social relations. Delegation of tasks to certain experts is inevitable and desirable, but we ought to delegate such power in non authoritarian ways.

It is important that decentralized societies associate with one another in order to help each other out in regards to meeting everyone’s needs and teaching one another. This is why the fifth rule is confederation. The term confederation (like democracy) is a tricky term to explain because of various connotations (for example connotations relating to the confederate states of America). However, what a confederation means within anti authoritarian circles is a decentralized federation, or non authoritarian associations between non authoritarian associations.  There is a common theme between free association/participatory democracy/decentralization/confederation.  They all aim towards harmonizing the individual with the community and the ecosystems they are dependent upon. Free association and meeting people’s needs makes sure that democratic institutions allow for meaningful dissent, and decentralization of power makes sure that the federated associations don’t have any political power to be oppressive.

Although preventative medicine via education, meeting people’s needs, minimizing abuse etc is essential, there is going to be symptom suppression that eventually needs to occur. And there are better and worse ways to deal with symptom suppression. The sixth rule is restraint and rehabilitation as opposed to punishment when it comes to symptom suppression. Restraint is about preventing someone from causing harm, whereas punishment is about revenge upon the wrong doer. There are a few levels as to why punishment systems are wrong. On one level, no individual is to blame for their reactions to environmental stimuli (for freedom from causality/context is illusory). To blame someone is to ignore context. On another level punishment doesnt work as symptom suppression, in fact it is mere symptom aggravation in disguise.  Somewhere around 2/3s of prisoners in the US re-offend within three years. punishment systems try to treat abuse/unmet needs with more abuse/unmet needs, adding fuel to the fire of the feedback loop of violence. People who are suffering from abuse and unmet needs to the point where they are harming others need to be restrained from harming others as well as help and rehabilitation. They don’t need punishment. Punishment is all about victim blaming because all oppressors were carved into oppressors by their environment(s). We can’t blame components for reactions to systems regardless of how good it may feel to reduce an oppressor to an existence without a context to the point where they are blame worthy for learning their behavior. Given the goal of wellbeing of all, we ought to use a consequentialist model of responsibility(where we only hold people responsible to the degree that it achieves the consequences we are looking for such as wellbeing of all), rather than a model based on blame.

The seventh rule is the automation of mechanical labor in accord with the needs and rational preferences of communities and individuals. We have the technology to automate the vast majority of mechanical labor to the point where people can be free from it. This would allow people to contribute to science and art rather than be forced to perform avoidable drudgery. The freedom from avoidable mechanical labor through automation is in many ways one of the freedoms that post scarcity economics provides that distinguishes it from traditional anti authoritarian strains of economics. Before the technology exists to automate an undesirable but necessary chore, the chore can be see as “a necessary evil”. However when the technology exists to automate such a chore, the chore becomes an unnecessary evil.

The eighth rule is freedom from systems that generate avoidable unmet needs or freedom from structural violence. Why? because structural violence creates avoidable suffering. And avoidable suffering is by definition something we want to avoid(otherwise it wouldn’t be suffering).  Freedom from structural violence has very liberatory implications including a change in property relations. Private Property is based on one’s position within an economic hierarchy rather than needs/use. It is the idea that some people can have irrational wants met while other people don't have their basic needs met. Private property(not to be confused with personal property) is the idea that some people are more entitled to shelter than others. Private property is the idea that absentee owners and private individuals can own the means of production and profit off of the labor of workers. Private Property is the idea that we can buy and sell land and by extension buy and sell people. And of course private property requires a state to enforce the extreme wealth inequality that private property creates. What we need is a usership system that puts meeting human needs as a priority with library esque access centers rather than an ownership system based on the amount of money one has.  Freedom from avoidable unmet needs also ensures freedom from sexism/racism/ableism/etc. It is essential that we do not just abolish systems of authoritarianism, we also must minimize authoritarian behavior.

The ninth rule is a gift economy rather than a monetary system. Merriam Webster defines gift as “something voluntarily transferred by one person to another without compensation” . However through the nurture/nature of gifting people feel obligated to give back. Gift can occur from community to individual, from the individual to the community, from individual to individual, and from community to community. Economic rewards and punishments only have the ability to motivate work for PURELY mechanical labor. However the vast majority of that labor can and should be automated. Making there no reason for economic rewards/punishments given that economic rewards/punishments inhibit work that isn’t purely mechanical.

the tenth rule is that Our technical efficiency should be checked and balanced by resource efficiency. We need to manage our finite resources in a way that meets human needs(and by extension we need to have concern for the environment we are dependent upon). This means we can’t afford to take cost efficiency and profit into consideration when it comes to production and distribution. We need to take human needs/preferences and resource efficiency into consideration. We don’t need to sacrifice our technology for resource efficiency, but we do need to harmonize our technology with resource efficiency.

These rules in isolation are not sufficient, but when they harmonize with each other they are able to create liberatory conditions. If our goal is to maximize wellbeing, the scientific method can allow us to achieve compassionate results(rather than mere compassionate intentions…). If our goal is maximizing the biopsychosocial wellbeing of all, The scientific method as our epistemology leads us towards meeting human needs(both finding out what these human needs are, and how to best meet them with the current technology available) and the scientific method leads us to an ecological focus by extension(and our ecological problems are social problems in disguise). And if we are trying to meet human needs, we need an access system based on needs/use rather than a system based on private property(and centralization of power).  Consent is by definition a preference we have in regards to how we want to be treated. From consent based social relations we get Participatory democracy within free associations and decentralized yet federated  associations. Our current technology allows us to automate mechanical labor freeing us from avoidable suffering. Due to the potential for the automation of mechanical labor aimed at meeting human needs with concern for the environment, there are no longer any meaningful arguments for a deeds based economy outside of transitioning to a gift and needs based economy. And when it comes to suppressing symptoms, restraint/rehabilitation are less harmful and more effective than punishment models.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

