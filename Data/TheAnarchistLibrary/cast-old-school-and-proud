
SACCO AND VANZETTI (2006) 
Willow Pond Production 
www. willowpondfilms. com 
DVD (not yet for public sale)

Sacco and Vanzetti (2006), is the latest in a long line of documentaries, dramas, poems, paintings, and whispered memories shared amongst friends and family, to record the infamous events that etched these men into history. On April 15, 1920, a shoe factory paymaster and his bodyguard were both fatally shot and robbed of $15,766.51. In an unrelated arrest of an anarchist intended for deportation, Sacco and Vanzetti came under suspicion for owning a car similar to that used in the robbery, in the trunk of which was anarchist propaganda, and for being armed with a gun (ironically, meant for self-defense against increasing police repression). The trial that followed was a corrupt sham which resulted in execution sentences. Millions organized across the world, on every continent, to demand a retrial, and failed; they were executed by electric chair in 1927.

There are many ways to share a memory and this video draws upon each to share with the audience the full breadth of who these men were, from their early lives in Italy, their decisions to come to the United States, their experiences as they arrived and tried to make lives for themselves, and to their emergence as radicals, draft dodgers, and anarchists.

For those familiar with the trial — the false witnesses, the phony forensic tests, the blatant bigotry of the prosecutors and judge — nothing terribly surprising is said concerning it. However this film isn’t called “The Trial of Sacco and Vanzetti” for as important as their persecution was, that was not their life. Rather, we are introduced to both men (given humanity by fine voice actors) by surviving family members and neighbors. We learn about Bartolomeo Vanzetti’s poverty and hardship as a baker in youth, and the agony he suffered after the loss of his mother to cancer, his thoughts churning as he wondered what to do with his life, contemplating first suicide, and then America. We are taken to see the house Nicola Sacco lived in while still in Italy, introduced to his niece, and told his reasons for wanting to leave Italy for North America. We hear the personal stories each man kept to himself, but luckily are shared with us by witnesses. One all but melts upon hearing the story of Vanzetti’s nursing a dying kitten back to health. That’s the kind of feel-good imagery anarchists could stand to get more of. (It also helps that the cat shown is black.)

The film examines the importance of their anarchist status, and celebrates it wholeheartedly. Watching each speaker lament over the injustice of the trial during the introduction, one worries that, in the hands of well meaning, but self-assured leftists, Sacco and Vanzetti’s radicalism would be toned down, sanitized, made impotent, and finally palatable for a larger, left-leaning audience. It’s not an empty fear. That one must wait a full 15 minutes before the dreaded A-word enters the discourse is not of great concern. The ideas these men stood for — their firm beliefs — are emphasized and given all due respect — which is more than most anarchists can ever hope to receive. However, once beyond the immediate goal of reassuring the audience that these men were not mad bombers, there is much stress placed upon liberal-left principles and desires. A great number of hands are wrung as speakers profess the need to protect civil liberties, and cry foul while democracy is put to evil ends in the hands of the corrupt and immoral. The latter half of the film, which covers the attempts made by the Left to save these two “martyrs for the proletariat,” all but drapes itself in Old Glory, as great rivers of protesters march, picket, and speak truth to power. The global impact of these demonstrations is only lightly touched upon, and the more radical or militant aspects of these events are omitted. The bombing of the American Embassy in Paris goes unmentioned, though the protests located on every continent are mentioned. At least the men themselves are never associated with examples of liberal resistance, and their criticism of so- ciety is made rather clear.

The Left is let off easy, as only the cynical attempts of the Communist Party to co-opt the men and their legacy for their ideological ends are revealed. Ultimately, members of the anarchist movement are shown to be the firmest allies the defendants had, raising staggering sums for defense expenses, and even forgoing all pretenses of ideological purity, in their attempts to save two men who easily could, but should not be, made into martyrs for The Cause.

The story ends as we all know it did, but the film continues on well after the executions are carried out. This film goes beyond “The Trial of The Century,” beyond their lifetimes, and reaches out into the collective unconscious of a generation of artists. Poetry, songs, books, and paintings have been created to honor the memories of these men, in more styles and forms than for perhaps any other cause celebre of the 20th century. Upton Sinclair’s novel, Boston, is mentioned, and one might hope that the film makers would address the recent, so-called revelations about Upton Sinclair’s supposed complicity in covering up the guilt of the defendants. Yet at no point is this even alluded to, which is unfortunate, as it is a slanderous rumor which has been challenged elsewhere. This was a missed opportunity to further combat it.

Nearing its end, the film considers how their legacy stretches into our lives: we are reminded of persecution that many face today, as threat levels remain color-coded, and immigrants are hounded, now as much as ever, while democracy still fails to live up to the rhetoric of its promoters.

The film then concludes with contemporary issues, and warnings that society should always be vigilant to prevent such miscarriage of justice in the future, which would be fine, except that as put forth by the film, the concerns over civil liberties and fair judicial practice are not anarchist issues, but rather liberal-left issues. For a film so concerned with the lessons we ought to learn from these men, its failure to note the contemporary persecution of anarchists, like the cases of Jeffrey Luers, Sherman Austin, or the recent victims of the Green Scare is glaring. It exposes the Left of today as being different from the communists of the 20’s in few respects. Anarchists can still expect to languish in jail cells while occasionally given minor acknowledgement from the progressive movement that claims them as martyrs for a cause theyll never be a part of.

By the end of the film, the re- emerging strength of anarchy as a social project has not been touched upon, leaving an outsider to the anarchist milieu to wonder if these two men weren’t among the last of their kind. For that matter, nothing is heard of their anarchist comrades after the executions, as though they all put their coats on and walked towards oblivion. Of current projects to memorialize Sacco and Vanzetti, or carry on in their tradition as they may have wanted, none are mentioned. Although the ideas of early 20th century anarchists can be appreciated, those of the 21st cannot even be given minor reference.

Knowing as we do today that anarchy is in a far stronger position than it was ten, twenty, or thirty years ago, it may not be too far of a stretch to imagine what Sacco and Vanzetti might have made of our situation. Then again, Vanzetti was briefly quoted on the matter in the film:

I am convinced that human history has not yet begun — that we find ourselves in the last period of the prehistoric. I see with the eyes of my soul, how the sky is diffused with the rays of the new millennium.

Yes, perhaps anarchists remain the outcasts of society, only wheeled out for display as an entertaining political novelty and a history lesson, but there is cause to hope in the face of lethal political repression.

Gotta love that old-school optimism. Makes you want to nurse a kitten.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



SACCO AND VANZETTI (2006) 
Willow Pond Production 
www. willowpondfilms. com 
DVD (not yet for public sale)



I am convinced that human history has not yet begun — that we find ourselves in the last period of the prehistoric. I see with the eyes of my soul, how the sky is diffused with the rays of the new millennium.

