      The Dielo Trouda Group and the Platform      The Relevance of the Platform Today
In Quebec, and more generally in North America, anarchism and organization have not been coupled well. Indeed, the last serious attempt to build a political anarchist group in North America date’s back to the adventure of the Love and Rage Revolutionary Anarchist Federation [1].

However, there have been, and there still are, organized anarchists around the world. Generations of activists worked hard on the question of organization, and, for those of us who don’t want to reinvent the wheel, it is useful to look at their analysis and proposals. Even if we find good things in ‘classical’ anarchists like Errico Malatesta and Michael Bakunin, we at NEFAC are mainly influenced by a tradition called, for lack of a better word, ‘platformism’.

The ‘plaformist’ tradition started with the analysis of the anarchist defeat at the hands of the Bolsheviks during the civil war made by a group of Russian anarchists in exile. This group included such important figures as Nestor Makhno, one of the main leaders of the insurrectional army of the Ukrainian peasantry, Peter Arshinov, historian of the same movement and old friend of Makhno, and Ida Mett, passionate partisan and historian of the Kronstadt insurrection [2]. Based in Paris, the group was organized around the publication of an anarcho-communist magazine in Russian, called Dielo Trouda (Worker’s Cause), a project originally conceived of by Arshinov and Makhno while they were rotting in the czarist prison some fifteen years earlier which was finally founded in Paris in 1925.

In addition to the less and less frequent letters from comrades “still in the country” and the analysis of the nature of the Soviet regime — Arshinov was one of the first to rightly call it State Capitalism — the magazine mainly concentrated on finding the cause of the “historical failure of anarchism” in the revolutionary period that just swept Europe. Like most activists that were still anarchists — many defected to Bolshevism — Dielo Trouda thought that the main cause of the failure was “the absence of organizational principles and practices in the anarchist movement,” which had it’s source in “some defects of theory: notably from a false interpretation of the principle of individuality in anarchism: this theory being too often confused with the absence of all responsibility.” It’s in June 1926 that the Dielo Trouda group made public its research on organization in a short pamphlet titled “Platform of the General Union of Anarchists (project)” [3].

The pamphlets open with an introduction that is a devastating critique of the “chronic general disorganization” of revolutionary anarchism, a disorganization compared to nothing less than “yellow fever.” From the first paragraph, the authors are ruthless: “it is very significant that, in spite of the strength and incontestably positive character of libertarian ideas, [...] the anarchist movement remains weak despite everything, and has appeared, very often [...] as a small event, an episode, and not an important factor.” To remedy this state of affairs, the authors think “it is time for anarchism to leave the swamp of disorganization, to put an end to endless vacillations on the most important tactical and theoretical questions, to resolutely move towards a clearly recognized goal, and to operate an organized collective practice.” They proposed the founding of “an organization which [...] establishes in anarchism a general and tactical political line”.

The text of the Platform as such is divided into three parts (general, constructive and organizational). In general, the first and second parts are a rather classical expose of anarcho-communism in which Dielo Trouda is only distancing itself on a few points. One of the main points is the primacy of the class struggle in society and it’s leading role in social change that is affirmed right at the start: “There is no one single humanity, there is a humanity of classes: Slaves and Masters” and “the social and political regime of all states is above all the product of class struggle”. This position, which also refuses the “humanist” positions, draws a clear line of demarcation that runs through the whole document. Dielo Trouda is resolutely basing itself in the social anarchism camp, strongly pro-class struggle. The constructive section has the advantage (and inconvenience) of the benefit of the Russian experience. Advantage because we get out of the abstract optimism so important to followers of Kropotkin, inconvenient however because the Russian situation in 1917 does not have much in common with the one we are currently living. In this sense, even if also dated, the study of the Spanish experience would be more productive.

Another point where the platform is departing from traditional anarchism is on the question of patriarchy and it is, unfortunately, to register a sharp set back. Indeed, none of the questions related to patriarchy — be it the oppression of women, sexual repression or the family and the education of children’s — are addressed. The word ‘women’ doesn’t even appear in the document! Nevertheless, even if it’s not in the same manner that we would address the subject today, the question was far from ignored by other anarchist currents. If we can understand that Dielo Trouda was not really inspired by the example of French individualist anarchists — who go really far in their critique of patriarchy and the bourgeois moral order — other revolutionary anarchist documents, similar to the platform, had nevertheless addressed the issue. The Program of the Alliance of Social Democracy, the first ever anarchist organization, founded by Bakunin in 1864, advocated the equality of men and women and says that children “are neither the property of society, nor the property of their parents but belongs to their future freedom”.

Four years later, Bakunin’s demands at the third conference of the First International include: “firstly, the abolition of the right to hereditary property, secondly, the complete legalization of the political and social rights of women with those of men, thirdly, the abolition of marriage as a civil, political and religious institution”.

Closer to the platform, the Anarchist Program, written by Malatesta and adopted by the Italian Anarchist Union in 1920, also demands the “reconstruction of the family in such a way that it results from the practice of love, freed of any legal chain, any economic or physical oppression, any religious prejudice” and concludes “we want bread, freedom, love and science for all”.

The question was not only theoretical; in the United States anarchists had already moved to practice it. So, when the platform was published, it had already been fifteen years since American anarchists, among them Emma Goldman, were demanding the legalization of abortion and the free circulation of information on contraception.

In Spain, a few years later, they would go much further, thanks to the action of the Mujeres Libres. Even the old enemy of Dielo Trouda, the Bolshevik government, had legally made women the equal of men, legalized abortion and homosexuality during it’s first week in power!

In short, this ‘oversight’ was a truly important theoretical weakness, a weakness that still has repercussions today.

Where the platform really distinguishes itself from classical anarchism is at the level of its organizational proposals and the positions that follow. In order to create a united organization, Dielo Trouda refused the synthesis of these different currents of anarchism as proposed by Sebastien Faure and Voline, because “such an organization [...] would only be a mechanical assembly of individuals each having a different conception of all the questions of the anarchist movement”, and that of anarcho-syndicalism because it “does not resolve the problem of anarchist organization, for it does not give [it] priority”. The platform instead proposed “to rally active anarchist militants to a base of precise positions: theoretical, tactical and organizational (i.e. the more or less perfect base of a homogeneous program).”

The activists of Dielo Trouda considered this double question of the organization and of the revolutionary program to be vital to launch the anarchist movement on a clear path. They indeed thought of the platform as “the outlines, the skeleton of such a program” and wanted it to be the “first step towards rallying libertarian forces into a single, active revolutionary collective”. It was however clear that the platform could not be the definitive program of revolutionary anarchism, and it belonged to the future organization “to enlarge it, to later give it depth, to make of it a definite platform for the whole anarchist movement”.

So the platform was defending the necessity of a theoretical and tactical unity, formulated in a program. This necessity was rejected by the partisans of a “synthesis” model of organization, who either didn’t see it’s utility (Faure), or believed it was premature and thought the proposed method to be “mechanical” (Voline). In the camp of anarcho-syndicalism, they of course agreed with this proposal, the problem was that the platform specifically rejected the program of syndicalism...

Dielo Trouda then introduced a simple principle, collective responsibility that was to draw the fire of critiques. The basic idea of collective responsibility was that “if we collectively agree on political positions and a determined line of action, it is in order that each member apply it in its political work. What’s more, when we agree on work to do and a way to do it, we become responsible to one and other, of its execution. The collective responsibility, finally, is nothing more than the collective method of action” [4]. This idea was however attacked by the Italian anarchist militant Errico Malatesta, who went as far as to compare it to the discipline of the army.

To hold all of this together, the platform proposed the inescapable principle of federalism, which was said to “reconcile the independence and initiative of individuals and the organization with service to the common cause”. Dielo Trouda warned, however, against a usual distortion of libertarian federalism: “the right, above all, to manifest one’s ‘ego’, without obligation to account for duties as regards the organization” and rather advocated that “the federalist type of anarchist organization, while recognizing each member’s rights to independence, free opinion, individual liberty and initiative, requires each member to undertake fixed organization duties, and demands execution of communal decisions.” Of course, in order for all that to work beyond the strictly local level, the stated goal of the platform, we need to give ourselves the necessary structures. The Dielo Trouda document does not develop a lot on the matter, but mentions the relevance of a decisional congress and an “executive committee” to coordinate the activity of the organization. Having mandated members to carry on certain duty seemed to be too much for some who saw there the embryo of a dictatorial authority

Where the platform distances itself the most from classical anarchism is probably regarding the role assigned to anarchists during a revolution.

Indeed, for Dielo Trouda, “the role of the anarchists in the revolutionary period cannot be restricted solely to the propagation of the keynotes of libertarian ideas”. But then, what is this role? For the authors, “anarchism should become the leading concept of revolution”, they specify that “the leading position of anarchist ideas in the revolution suggests an orientation of events after anarchist theory” which should definitely not be confused with “the political leadership of the statist parties which leads finally to State Power”. This idea of the “leading concept” was to get on the nerves of many anarchists and was to be severely critiqued.

The authors had vivid memories of the Russian revolution and reminded us that “although the masses express themselves profoundly in social movement in terms of anarchist tendencies and tenets, these tendencies and tenets do however remain dispersed”, therefore, we need a force that “organizes the driving power of libertarian ideas which is necessary for preserving the anarchist orientation and objectives of the social revolution”. This force will be the anarchist organization according to the platform. The anarchist organization must “manifest its initiative and display total participation in all the domains of the social revolution: in the orientation and general character of the revolution; in the positive tasks of the revolution, in new production, consumption, the agrarian question, etc. On all these questions, and on numbers of others, the masses demand a clear and precise response from the anarchists. And from the moment when anarchists declare a conception of the revolution and the structure of society, they are obliged to give all these questions a clear response, to relate the solution of these problems to the general conception of libertarian communism, and to devote all their forces to the realization of these.”

The members of the Dielo Trouda group have the merit to have reflected on means to get militant anarchism back on its track. Their solutions can, still today, serve as a departing point to build an organized and coherent anarchist practice. Of course, we are far from approaching the platform like a bible (or a little red book!), and we are aware that it has some deficiencies, most notably on the question of patriarchy, like we’ve said, and on the question of the autonomy of social movements.

One of the mistakes of the first ‘platformists’ was, paradoxically, to put too much hope in the existing anarchist movement. Indeed, they were sure to rally the majority of activists to their concepts. Can we really be surprised, given the virulent attacks of the platform, that it didn’t work? Nevertheless, even today, it is still a trap we easily fall into. NEFAC didn’t avoid it. We have spent a lot of time discussing and trying to convince the activists of our region. We are forced to acknowledge our failure...

Is it a bad thing? Not entirely. Indeed, looking at what is actually done — and not only what is said, we are far from sure that the future of revolutionary anarchism lies in anarchist activists. Maybe if anarchists stopped trying to convince one another, they would have more time to give to the rest of the population? As far as we are concerned, we’ve decided to acknowledge the simple fact of the division of our movement and we have decided to “stop talking about it and start doing it”.

What we understand of the platform is the necessity to organize seriously. Which means to give ourselves the means to go forward, and so simple things like a democratic structure with decisional conference, a discussion list, dues, mandated work committees, etc. We also know that anarchism only has limited roots in the region, and that we will need to develop a host of political positions on a variety of subjects in order to remain innovative. To us, the question of tactical and theoretical unity is just common sense and, what’s more, a process of continuous debate.

“It’s goal not being the seizure of power, the anarchist organization can neither be a party nor a self-proclaimed vanguard. It is rather an acting minority within the working class. Its hope is to serve as a libertarian rallying point and take part in the theoretical and practical fight against all authoritarian ideologies. It is first and foremost a force of proposition that tries to rally people, by example and suggestion, to its political points of view. [...] Considering that any revolutionary period must be preceded by organizations able to rally people to the anarchist alternatives and methods, we believe that a strong anarchist organization, rooted in struggles, is necessary. Let’s be clear, however, we don’t think that NEFAC is, right now, such an organization (but we are working on it!)” [5]

 
[1] See Wayne Price’s ’Love & Rage’ piece elsewhere in The Northeastern Anarchist: Magazine of the Northeastern Federation of Anarcho-Communists (NEFAC) #3.
[2] See respectively History Of The Makhnovist Movement, by Arshinov (Freedom Books, London) and The Kronstadt Commune by Mett (Solidarity, available on the web at flag.blackened.net). Both are available through AK Press (www.akpress.org).
[3] Today we usually refer to this text as the “Arshinov’s Platform” or the “Organizational Platform of the Libertarian Communists” (the title used by those who identify with it). All quotations, unless otherwise noted, are from the Platform (the text is available online at flag.blackened.net).
[4] The Question of the Revolutionary Anarchist Organization, position of the Groupe Emile-Henry (NEFAC-Quebec), see www3.sympatico.ca for a French version.
[5] The Question of the Revolutionary Anarchist Organization; Groupe Emile-Henry




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

