
The digital age is pre-eminently the ultimate reign of Number. The time of Big Data, computers (e.g. China’s, world’s fastest) that can process 30 quadrillion transactions per second, algorithms that increasingly predict—and control—what happens in society. Standardized testing is another example of the reductive disease of quantification.

Number surpasses all other ideas for its combination of impact and implication. Counting means imposing a definition and a control, assigning a number value. It is the foundation for a world in which whatever can be domesticated and controlled can also be commodified. Number is the key to mastery: everything must be measured, quantified. It is not what we can do with number, but what it does to us. Like technology, its intimate ally, number is anything but neutral. It tries to make us forget that there is so much that shouldn’t or can’t be measured.

Fifth Estate published my “Number: Its Origin and Evolution” in Summer 1985, just as the digital age was gaining traction following the personal computer explosion at the beginning of the 80s.i The quickening (anti-) pulse of technological change over the past 30 years has been at base a mathematization. Social life in the post-community era is detached, disembodied, drained, statistical. Its core is administration, just as the essence of number is calculation. “Mathematical thinking is coercive,” disclosed British philosopher J.R. Lucas.ii Number totalizes; in mathematics, ambiguity is anathema. The technoculture obeys these norms, and we dance to its tune, its code: number.

But there are some who applaud the new, always more arid reality. And postmodernism wasn’t the nadir of thought, after all. Alain Badiou denies that the Techno Age brings more and more nihilism and mediocrity. Mocking Heidegger’s critique of the ascendancy of technology, he declares that there’s not enough of it!iii

Badiou’s Being and Event (1988), empty and ahistorical, somehow installed him as arguably the biggest star of philosophy in the West. Number and Numbers (1990) is his follow-up hymn to estrangement.iv Mathematics is philosophy, is being, in a formulation as hideous as it is astounding. Fellow Marxist-Leninist and postmodern/speed freak/pop culture clown Slavoj Zizek proclaimed Number and Numbers “breathtaking…[it] announces a new epoch in philosophy.”v Zizek is correct, but only in a thoroughly negative sense. Michel Foucault evidently didn’t see Badiou coming when he held that “theory is by nature opposed to power.”vi

Number implies a relationship and that relationship is precisely that of power, as with capital, but more primary. Communists like Badiou (and Zizek), needless to say, have never taken the trouble to oppose power. A footnote by Andrew Gibson is revealing. Badiou had told him “that he has no liking for James Joyce. One suspects that there is simply too much world there for him.”vii Too much uncontrolled world.

Number is a form of being for Badiou. What’s more, “mathematics is the infinite development of what can be said of being qua being.”viii That is, mathematics is already philosophy; ontology is actually mathematics.

Postmodernism elevated liberal doubt as its response to anyone who could imagine a condition outside alienation and subjection. It worked in a negative vein (e.g. Derrida) to undermine any grounds for hope. Badiou promotes a positivity that works toward the same end. For him, politics is the possibility of a “rupture with what exists.”ix But he grounds this positive hope, his “rupture,” in what couldn’t possibly be more a part of alienation and subjection. Badiou translator Jason Barker notes correctly that “Badiou’s canonical politico-philosophical reference point is Althusser’s Lenin and Philosophy and Other Essays.”x The Stalinist Althusser supported the French Communist Party against the workers and students of the May ’68 uprising. As Badiou freely admits, “there is no theory of the subject in Althusser, nor could there ever be one.”xi Two communists joining hands against the individual, against liberation. What is “seemingly phrased in strictly mathematical language,” as Bruno Bosteels sees it, “is imported from the realm of militant politics.” Specifically the Marxist-Leninist versions of such categories, such as “normality, singularity, and excrescence.”xii Even more specifically, Maoism.

Francois Laruelle finds that Badiou’s “enterprise has no equivalent in the history of philosophy,” a fusion of Platonist mathematicism and Maoism.”xiii “Thought” at its most nakedly authoritarian on every level.

Platonism vis-à-vis math means that numbers are independently existing objects. But numbers are not out there, somewhere, to be discovered; they are invented, as Wittgenstein, for one, grasped quite well. Invented to meet the needs of complex, unequal societies. Counting, accounting, a growing obsession that began with domestication and civilization, has reached the point, according to Ellul, where “everything in human life that does not lend itself to mathematical treatment must be excluded.”xiv

We can count and measure only the lifeless because such processes necessarily exclude what is living. The noted 19th century mathematician Gottlob Frege proclaimed “the miracle of number” but also stated that “the highest degree of [mathematical] rigor…is at the furthest remove from what is natural.”xv As Thoreau put it succinctly, “Nature so abhors a straight line.”xvi

Philosopher of science Keith Devlin is wrong to aver that numbers “arise from the recognition of patterns in the world around us.”xvii They arise because they are necessary for running a certain kind of society; numbers have only an imposed relationship to what is found in the world. Math historian Graham Flegg makes a similar error when he asserts, “Numbers reveal the unity which underlies all of life as we experience it.”xviii The “unity” in question did not exist before it was produced, with the invaluable assistance of number.

In Badiou’s nonsensical formulation, mathematics is “the history of eternity.”xix It is considerably saner to notice that the development of math is intimately involved with the development of the whole of civilization. On the heels of domestication (and its progeny, private property), grain needed weighing for sale, and land needed surveying for ownership—and soon enough, for taxation. Geometry, after all, is literally “land measurement.” Organization and engineering certainly required the services of Egyptian and Babylonian mathematics, to enable the first two civilizations in the West.

It is no coincidence that it was the Babylonian/Sumerian civilization, the first real empire, which first developed the idea of written numbers.xx Number is key to large-scale management and mobilization; numbers and empire have gone hand in hand since earliest times. Babylonian arithmetic was “fully articulated as an abstract computational science by about 2000 B.C.,”xxi about 2000 years before the famed “classical” mathematics of the Greeks.

“All is number,” announced Pythagorus, who thereby founded a religion, it should be added. Plato, a Pythagorean, composed the soul from seven numbers in his Timaeus. And in India as well as in Greece, certain exacting ritual requirements were specified by geometrical exercises intended to avert suffering at the hands of the gods.xxii Nor has this form of idealism died out; the 20th century mathematician-philosopher L.E.J. Brouwer regarded the universe as “a construction of the mathematician.”xxiii

It was the wealthy, aristocratic Plato who famously asserted the ontological primacy of math, which Badiou unreservedly seconds. A corollary is that for Plato, the first upward steps out of the cave towards wisdom begin with mastery of the arts of number. This put thought on the path of representation and mathematical objectification. Mathematics’ more concrete, everyday role—to serve the needs of power—makes this path the history of oppression, rather than Badiou’s “history of eternity”.

Badiou approvingly quotes the German mathematician Richard Dedekind to the effect that “man is always counting.”xxiv Of course it is well-established that in most primal communities people use only “one, two, many” as the limit of their interest in number. In a recent example, Daniel Everett, referring to his years in Amazonian Brazil, concludes that “the Piraha have no number at all and no counting in any form.”xxv

Let us also add a qualification about the use of numbers. Ethnographer W.J. McGee judged that aboriginal people “commonly see in numbers qualities or potencies not customarily recognized by peoples of more advanced culture.”xxvi The association or coloration used with numbers means that they had not yet lost their sense of the uniqueness of everything, every event. This is still present with early terms of measurement. The units––such as the yard, the foot, the pound––were of human size and reference, and local relevance, until mass long-distance civilization took over.

Negative numbers came of age in the latter half of the Middle Ages. They were of inestimable assistance with larger financial transactions in which there might be net losses. At this time international banking greatly expanded, giving math a new value.xxvii Well before Galileo, Copernicus, and Descartes provided the Faustian underpinnings for number’s cardinal role in dominating nature, math had already become essential for merchants, cartographers, imperial navigators, bankers, and others.

The Scientific Revolution, chiefly of the 1600s, largely revolved around the spirit of number. In 1702 Fontenelle observed that the “geometric spirit” is required if order and precision are to be established.xxviii This spirit bloomed with Immanuel Kant (1724–1804). Knowledge for him is mathematical knowledge. Necessary and a priori, already always present, number is central to all the categories of our cognitive process. The new prominence of the mathematical infected society at large. Enlightenment thinkers spoke of a comprehensive “geometry of politics,” a “social mathematics.”xxix

In his Description of New England (1616), Captain John Smith asked native individuals how many fish they caught, in order to more accurately gauge the level of potential plunder. He found that “the Savages compare their store in the sea to the haires of their heads,”xxx most likely an unsatisfactory report. Obsession with a mathematical orientation was present in North America early on but was not pervasive until the 1820s, according to Patricia Cohen. Her A Calculating People focused on “the sudden popularity of numbers and statistics in Jacksonian America.”xxxi

Counting consists of assigning words to things. The first counting symbols were, in fact, the first writing. At this early stage many cultures expressed letters and numbers by the same symbols. Aleph, for example, expressed both the first letter of the Hebrew alphabet and the first of the ordinal numbers.xxxii Spengler pushed the connection much further, wondering whether with number one finds “the birth of grammar.”xxxiii

Measurement, like counting, deals with just one aspect of the object it is measuring and assigns a number to that aspect. This abstracting move is basic to the universal standardization of life inherent in globalizing civilization. Of course, there is and always has been resistance. But in the words of psychologist S.S. Stevens, “Given the deeply human need to quantify, could mathematics really have begun elsewhere than in measurement?”xxxiv In a similar vein, John Henslow found that “measurement is what defines humanity…is what distinguishes the civilized from the uncivilized.”xxxv

Growing social complexity and the all-encompassing integration required by modern domination means more and more measurement. It is as ubiquitous as it is imposed. “A deeply human need”––or the dynamic of ruinous civilization? There is no civilization without measurement, but there is life outside civilization—and ultimately, perhaps only outside civilization.

The prevailing view is that knowledge is limited without measurement, that we can’t really grasp something unless it can be measured. The word “grasp” is telling; it belongs to the language of control. To control, dominate, and hold nature in our grasp, for example: the lexicon of domestication. Is this really a way of understanding? What is lost when we only measure? Does this approach not take us away from a more intimate knowing? Traditional indigenous people do not “grasp” in their knowing.

A small instance from the realm of “fitness”: e-devices with their apps for measuring bodily performance as a function of various rates: breath, pulse, etc. A way of externalizing and objectifying our own bodies, of losing touch with ourselves and our senses.

This is part of the growing technification and concomitant deskilling, hallmarks of the digital age. Ironically, this movement does not produce greater proficiency in numbers. Numeracy, in fact, is in decline. Computers have replaced cash registers; retail clerks have no need to make change, and many don’t know how. A friend, when asked for the time by a teenager, pointed to a nearby clock. The teen couldn’t tell time from a clockface, only a digital readout.

Inevitably asked for a definition of time, that always elusive question, Einstein replied that it’s what a clock measures. The correspondence between measurement and time has been much discussed; but in what does the measuring of time consist?

Plato found an intrinsic connection between time and number, but that only reminds us that we can’t be sure what kind of things time and number are. Aristotle claimed that things are in time the way what is counted is in number, as if that clarifies matters much.

In the 3rd century A.D. Plotinus asked, “Why should the mere presence of a number give us Time?”xxxvi Which is suggestive, in terms of how time stakes its claim, and prompts a closer look at timekeeping itself. Consider 7th century Bedouins in what is now Saudi Arabia. Though pastoral (and therefore domesticators), they had a very minimal sense of time. Along came Mohammad, who unveiled time as part of a new religion. Five compulsory prayer times regulated each day. All our days, said the Prophet, are numbered, just as math-guided industrial processes would regulate and number them a millennium later.

For the Mayans and others in Mesoamerica, a focus on time and number mirrored a preoccupation with order and rule. Bergson’s durée, or lived time, was an attempt to step outside of imposed, identically numbered time. But the bond between time and number has continued and deepened, as domesticating reality commandeers more and more places and lives on the planet.

“There is no way we can escape from numbers,” concluded Graham Flegg.xxxvii Philosopher Michel Serres agreed: “Wherever the road of mathematicity was opened, it was forever.”xxxviii The same unending servitude is consecrated by Badiou, who stakes thought itself on number. But we may imagine what could emerge when the counting and measuring and timing is over, by our own ending of it. Imagine what could emerge only in such a world.

The “elegance” of math? Much more akin to the coldness of advanced civilization. Political theorist Susan Buck-Morss expressed this with great eloquence: “The social body of civilization is impersonal, indifferent to that fellow-feeling that within a face-to-face society causes its members to act with moral concern.”xxxix Face-to-face, where there is little or no need of counting.

Dedekind said that numbers “are a means of apprehending more easily and more sharply the difference of things.”xl What difference could he have been referring to? The written numbering systems of the ancient Egyptians, Hittites, Greeks, and Aztecs were structurally identical,xli and this congruence pointed toward the global homogenization so strongly underway now.

A hollowed-out mathematical order is that of closed-off coldness, indifference, cynicism. The rise in the incidence of autism is one sad aspect among many; it may be worth noting that a disproportionate number of math students and theorists have received a diagnosis of autism.xlii

Number trumps quality and qualities; meanwhile Badiou bases his authoritarianism on the deepest grounding for massification and estrangement. Healthy individuals avoid such brutalist “thinkers.” The 2nd century physician Galen provides a cautionary tale: “It has often happened that people have talked happily with me, because of my work among the sick, but when they discover that I am also an expert mathematician, they avoid me.”xliii




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

