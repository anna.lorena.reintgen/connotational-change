      Identity politics is part of the society we want to destroy.      Identity politics has infected anarchist spaces.
Anarchism in the UK is a joke. Once symbolising hard-fought struggles for freedom, the word has been stripped bare to make way for narrow-minded, separatist and hateful identity politics by middle class activists keen to protect their own privileges. We write this leaflet to reclaim anarchism from these identity politicians.

We write as self-identified anarchists who see our roots in the political struggles of the past. We are anti-fascists, anti-racists, feminists. We want to see an end to all oppressions and we take an active part in those fights. Our starting point though is not the dense language of lefty liberal academics, but anarchism and its principles: freedom, cooperation, mutual aid, solidarity and equality for all regardless. Hierarchies of power, however they manifest, are our enemies.

Identity politics is not liberatory, but reformist. It is nothing but a breeding ground for aspiring middle class identity politicians. Their long-term vision is the full incorporation of traditionally oppressed groups into the hierarchical, competitive social system that is capitalism, rather than the destruction of that system. The end result is Rainbow Capitalism – a more efficient & sophisticated form of social control where everyone gets a chance to play a part! Confined to the ‘safe space’ of people like them, identity politicians become increasingly detached from the real world.

A good example is ‘queer theory’ and how it has sold out to corporate masters. The concept of queer was not long ago something subversive, suggesting indefinable sexuality, a desire to escape society’s attempts to define and study and diagnose everything, from our mental health to our sexuality. However, with little in the way of class critique, the concept was readily appropriated by identity politicians and academics to create yet another exclusive label for a cool clique that is, ironically, anything but liberatory. Increasingly, queer is a nice badge adopted by some to pretend they too are oppressed, and avoid being called out on their shit, bourgeois politics.

We don’t want hear about the next DIY event, queer night or squatter fest that excludes all but those who have the right language, dress code, or social circles.. Come back when you have something genuinely meaningful, subversive and dangerous to the status quo.

Identity politics is narrow-minded, exclusive and divisive. At a time when we need to be reaching outside of our own small circles more than ever, identity politics is all about looking inwards. That’s probably no coincidence. While claiming to be about inclusivity, it is highly exclusionary, dividing the world into two broad groupings: the Unquestionably Oppressed and the Innately Privileged. There are few grey areas allowed in practice and conflict is continually stoked between these two groups.

We do get it, it’s not all about class, but if we can’t rally together to even recognise who really holds the reigns of power then we haven’t a hope in hell of getting anywhere. If their vision were truly one of liberation for all, then theirs wouldn’t be a politics of divisiveness, constantly pitting one group against another in a manner similar to capitalism and nationalism. Things that muddy the simple binary of oppressed vs. privileged, such as personal life experiences or traumas (which cannot be neatly summed up by one’s identity as a member of an oppressed group),, or things that people may not feel comfortable talking about, such as mental health or class, are often wilfully ignored by identity politicians.

As, of course, is the most glaringly obvious point: that the problems we face go well beyond queerphobia or transphobia, but the whole fucking system of planetary enslavement, destruction, exploitation and imprisonment. We don’t want to see anyone in the prison system, whether they are black trans women, or cis white men (which, by the way, make up the vast majority of people imprisoned in the UK). It is unsurprising that politics based on such exclusivity results in constant internal clashes and seeing each other as the enemy, particularly given its vulnerability to exploitation by middle class identity-politician managers.

Identity politics is a tool of the middle classes. It is flagrantly used and abused by articulate, well-educated group representatives to entrench and maintain their own power through politicking, dogma and bullying. The comfortable backgrounds of these activists is betrayed not only through their use of academic language but through their sense of entitlement and confidence in using other activists’ time and energy to switch the focus towards them and their feelings. Indeed, a lack of work ethic, a certain fragility, and a preoccupation with safety and language rather than material conditions and meaningful change are other aspects which reveal the class background of many identity politicians.

We see this in the ease with these individuals ‘call out’ other people at the slightest deviation from the code of practice they have unilaterally imposed, assuming that everyone ought to think as they do or have the time to devote to learning it. Thus ignoring the reality of daily class struggle.

There is a false equivalence between membership of the Unquestionably Oppressed and being working class. On the contrary many in the Unquestionably Oppressed espouse liberal values rooted in capitalist ideology rather than being truly liberatory.

A politic that is based on having the right language and access to the right tone and codes is one that is inherently a tool of oppression. It is certainly not being representative of those who it claims to speak for, those at the bottom of society. An anarchist analysis recognises that though someone may be from an oppressed group, their politics, or the demands made on behalf of the Unquestionably Oppressed, may nevertheless be purely liberal, bourgeois and pro-capitalist.

Identity politics is hierarchical. By entrenching the power and status of middle class petty politicians, identity politics is hierarchical. Beyond the chicanery, imposing certain dogmas also enables this power to go unquestioned. These include: implicit hierarchies of oppression; the creation and use of loaded terms intended to provoke an emotional response (‘triggering’, ‘feeling unsafe’, ‘Terf’, ‘fascist’); those who aren’t members of specific groups being denied an opinion on the wider politics of these groups; the idea that members of the group should under no circumstances have to do any ‘labour’ of explaining their politics to non-members of the group; framing alternative discourses as ‘violence’; and the idea that one cannot question a representative or member of these groups (no matter how bad their politics) by virtue of the fact that they are Unquestionably Oppressed.

These dogmas are used to maintain norms, whether in subcultures or wider society. Anarchists should be suspicious of any tendencies that are based on unquestionable principles, particularly ones which so obviously create hierarchies.

Identity politics often exploits fear, insecurities and guilt. It is important that we recognise this on two fronts. One, it is used to disenfranchise rather than actually empower, as is claimed. It reinforces the idea that people are fragile victims rather than agents of change, and therefore need to accept leaders.

Though safer spaces and language are important, the extent of the obsession with these things is not a sign of strength but of self-perpetuating victimhood. Through social anxiety, it places on everyone else the guilt of being somehow privileged and being utterly accountable for the giant systems of oppression that actually only benefit a few. It also allows those within minority groups who benefit from state and capitalist structures off the hook from any sort of accountability for their oppressive actions or prejudiced behaviour.

An anarchist analysis means we should recognise that members of oppressed groups can hold elite and repressive positions too, and should be equally challenged, not just given a cowardly pass.

Sadly, anarchism is being hollowed out in a rush to virtue-signal, to be ‘good allies’. Allydom is all too often enacted as blind acceptance of the politics of those who are Unquestionably Oppressed, or claim to be, no matter how shit their politics or personal behaviour is. It is willing subservience to the politics of others, the least anarchist position that can be taken and pure spinelessness.

Self-appointed leaders who do not agree with our politics should not be given a platform by us. So, it is ironic that we have allowed groups with little or no radical politics to enter our spaces and shut down debate, and claim that anything that disagrees with their viewpoint must be fascist. It should go without saying that fascism is not something that should be trivialised in this way.

It also amazes us that obvious parallels with right-wing politics are not seen, not least in the way feminists dismissed as ‘feminazis’ is reflected in the current use of the word ‘fascist’ against radical feminists by trans rights activists, as well as slogans calling for ‘terfs’ to be killed regularly cropping up in anarchist spaces both online and real world. It is shocking that the violence of this misogyny is being celebrated, not condemned.

Anarchism is against gods. Is there any phrase that sums up anarchism better than ‘no gods, no masters’? Such hierarchy and exclusivity are antithetical to anarchism. We used to assassinate politicians, and uncountable numbers of comrades gave their lives for the struggle against power. We still reject politicians of all stripes, whether Tories, Labour or those who see themselves as leaders of movements based around identity. It is against the most basic principles of anarchism to accept leadership by others, because we believe all are equal. Likewise we do not accept the notion that we cannot question or query positions held by other activists or those who call themselves anarchists – which unfortunately identity politics all too often insists on.

Anarchism does not support patriarchal religions and anarchists have a long history of conflict with them. It is an embarrassment the way so much of what passes for anarchism in the UK today acts as apologists for those who want to avoid any challenge to their own sexism and patriarchy or even continue their oppressive religions, simply because reactionary conservatives treat them as scapegoats.

The destruction of anarchist projects is carried out and celebrated in the name of identity politics, simply to appease those who have no interest in anarchism itself. And if any do stand up and challenge it, they are met with abuse or even physical attack – behaviour that used to be challenged but is now condoned because it comes from those who are considered to be oppressed. Here more than anywhere the utter failure of anarchist politics by those who supposedly represent it is the most obvious. Lets start by calling out Freedom News for starters, whose uncritical support of groups with little in common with anarchism is shameful.

Anarchism is not identity politics. Anarchism is not just another identity as some like to claim. This is a common crass and lazy kneejerk response from the identity politicians, and a way to avoid answering actual political issues. It also shows no understanding of how identity politics is used to manipulate and subvert anarchist spaces for personal agendas. Sure, ‘anarchist’ can be claimed as an identity too, and anarchists are prone to (often rightly criticised) cliqueish behaviour. But the similarities ends there.

Unlike identity politicians or the SWP, most anarchists do not try to recruit followers, but instead attempt to spread ideas that will support communities in fighting for themselves in a way that cannot be recuperated. Our agenda is radically different and rare in that our core politics are not about furthering our own personal power and status. Anarchism encourages people to question everything, even what we ourselves have to say, in the spirit of freedom.

Unlike the inherent, exclusive characteristics of identity politics with its in-groups and out-groups, anarchism is for us a set of ethics that guide how we understand and react to the world. It is open to any who will look or listen, something anyone can feel, no matter what background they come from. Often the results will be diverse, as people combine it with their individual personalities, life experiences, and other aspects of their identities.

One doesn’t need to know the word anarchy to feel it. It is a simple and consistent set of ideas that can act as anything from guidance in a particular conflict, to the foundation of future societies. To refer to anarchist principles then when there is conflict about identity politics, makes sense when we are supposedly united by these principles.

Being gay or having brown skin does give rise to similar experiences to those who share these characteristics, and obviously means you are likely to have social links, empathy or a sense of belonging to this group. However, lived life is actually much more complex and you might have as much or more in common with a random white queer woman than you would with a fellow brown cis man. Identity politics at times mirrors the chauvinism of nationalism, with different groups seeking to carve out their own domains of power according to categories derived from the capitalist order. We, on the other hand, are internationalists who believe in justice for all. Anarchism seeks to raise up all voices, not just those of minority groups. The notion that oppression only affects minorities rather than the masses is the product of bourgeois politics that never had any interest in revolutionary change.

Identity politics is feeding the far right. On a final note, it is worth stressing how much identity politics plays into the hands of the far right. At best, ‘radical’ politics looks ever more like irrelevant navel-gazing to many. At worst, middle class identity politicians are doing an excellent job of alienating already disenfranchised cis white people, who happen to make up the large majority of people in the UK, and are increasingly gravitating towards the Right.

To ignore this fact and continue to engage in infighting over identity politics would be the height of arrogance. Yet, at a time when we are seeing fascist movements multiply, anarchists are still distracted by politics of divisiveness. For too many, identity politics is simply a game, toleration for leads to constant disruption of activist circles.

Final note. To us anarchism is cooperation, mutual aid, solidarity and fighting the real centres of power. Anarchist spaces should not be for those who merely want to fight those around them. We have a proud history of internationalism and diversity, so lets reclaim our politics for a genuinely inclusive future. This article has been formatted as an A5 pamphlet for printing.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

