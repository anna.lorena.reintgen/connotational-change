      spanish-american war      class struggle      strike breaking      huge losses      fidel’s anti-communism      sceptical      currying favour      shift of focus      anarchists imprisoned
January 1st marked the 50th anniversary of the flight of dictator Fulgencio Batista from the Caribbean island of Cuba and the initiation of what is now commonly referred to as a communist regime under the direction of Fidel Castro. Nowadays, of course, the reins have been passed to Raul, Fidel’s younger brother, but the socialist rhetoric has not weakened in the intervening 50 years. Much has been made of the Castro dynasty’s persecution of political dissidents, homosexuals and other non-conformists and many readers will find themselves irritated by the inaccuracy of the oft quoted Fidelist "socialist paradise" in press coverage of the regime’s jubilee celebrations.

Of course, that the Castro brothers identify themselves as socialist is sufficient for them to be considered thus--equally by the US State Department (who have enforced a trade embargo against the island almost since Fidel’s accession to power) and the international left, for whom Fidel was (and continues to be) a kind of folk hero, a two finger salute to those arrogant gringos who sit only 90 miles to the north. Both sides of the divide claim that to support Fidel (whatever that means in real terms) is to support socialism and oppose US-led capitalism and, for this reason, the logic follows, it is the duty of every revolutionary to "support" an "anti-imperialist" Cuba despite its obvious shortcomings. However, even a cursory glance at Cuban history demonstrates how Fidel was distant from the social upheaval of 1950s Cuba and was massaged and moulded into the post-revolutionary leader by various American lobbies, mostly due to his avowed allegiance to their interests, an allegiance that he maintained long enough to eradicate authentic class struggle in Cuba.

From Columbus’ maiden voyage to the Americas until the momentous events of January 1959, Cuba was an occupied territory, subservient to the economic and political dictums of first Spain and then the USA. In 1898, the two imperial powers actually contrived to fight a war on Cuban soil over possession of the territory, resulting in an American victory and the island’s integration as an American satellite for the ensuing 60 years and creating the context for the Castroist anti-imperialist struggle.

This only tells one part of the story though, since Spanish colonial authority on the island had been steadily undermined by civil unrest, originally instigated by black slaves in revolt and then continued by a labour movement initiated by Spanish anarchist exiles. Indeed, such was the influence of anarchist labour organisers in 1890s Cuba that Jose Marti, a journalist and writer turned pro-independence leader, focused many of his efforts in building links with the Cuban anarchist movement in an attempt to convince them that the removal of the Spanish was integral to social revolution.

In the end, Marti’s overtures were sufficiently flattering that a large majority of the Cuban anarchist movement abandoned labour struggles in order to join Marti in his uprising of 1895, and militated for workers and peasants to do the same. As the uprising floundered, Marti promptly launched an apparently suicidal battle charge in order to ensure his legacy as Cuba’s biggest national hero--even more popular than Fidel. The many ordinary Cubans who died alongside him in his quest for power were not afforded such reverie.

Eventually, the Cuban insurgency garnered US support, leading to the subsequent Spanish-American War of 1898 as previously mentioned, but Cubans were soon to unmask the superficial nature of Marti’s promises of post-colonial freedom as victorious American troops occupied the island until 1902 in order to guarantee the smooth transition of Cuban sugar and cigar factories from Spanish to American hands. Cuban workers found themselves with new masters rather than no masters.

Meanwhile, also in 1902, anarchist organisers returned to the more fruitful task of class struggle, initiating Cuba’s first ever general strike amongst Havana tobacco workers. The strike was crushed by the incipient Cuban government but only served to inspire 20 years of avid organising and striking, especially amongst workers in the tobacco factories, railways and sugar mills. Central to workers’ struggles in this period were the concepts of direct democracy and a rejection of electoralism, while anarchist ideas also permeated the creation of the first Cuban union federation, the CNOC, in 1924.

The election of General Gerardo Machado in 1925 coincided with a period of repression against anarchist groupings in the assertive labour movement and also the creation of the Moscow directed Cuban Communist Party. A purge followed, with the General Secretary of the CNOC being thrown into the sea by persons unknown to be eaten by sharks, and by 1931, the growing Communist Party had managed to seize control of the federation and they proceeded to gradually dismantle its anarchist methods. This marked an era of CNOC negotiation with the Machado government while Machado’s goons attacked and assassinated striking workers. However, workers’ struggles continued and when a series of strikes in 1933 were united by an anarchist led strike committee, it mushroomed out of CNOC control into a national insurrection which eventually toppled the tyrannical Machado.

It is in 1933 that Fulgencio Batista emerges from a military coup as Cuban leader. Batista managed to maintain his hold on power in Cuba right up until 1959, partially via a succession of lackeys he appointed as President who reported to him, and partially via a sort of reformism which relied on mediation between the various factions within Cuban politics. Batista was especially reliant on the support of the Communist Party, granting them control over the new Cuban labour federation, the CTC, and positions within the government in exchange for the Communists’ aid in breaking strikes such as the general strike of 1935.

Meanwhile, the reforms of the Cuban Constitution of 1940 encouraged a variety of political parties to oppose Batista and his cronies with more vigour, and in this decade, a young Fidel Castro graduated from the politically volatile Havana University with a law degree. Fidel soon started to climb through the ranks of the main opposition party, the reformist Orthodox Party, which was gaining popularity through the exposes of governmental corruption by its leader, Eduardo Chibas, on his weekly radio show. By 1951, it was being tipped to win the next election, but Chibas bizarrely shot and killed himself live on air, leaving the party moribund. Fidel, a protege of Chibas, was present in the studio at the moment of the shooting and accompanied him to hospital.

Nevertheless, Fidel had earned a reputation for himself as an eloquent nationalist and critic of US intervention in Cuban affairs and planned to stand for parliament in the 1952 elections, only for Batista to stage another coup against the independent minded President Prio Socarras and cancel them immediately beforehand. Fidel deplored his disrespect for parliamentary democracy and led a small band on an adventurist attack on the Moncada Barracks in Santiago, eastern Cuba. The attack was a massive failure and led to 60 insurgent deaths, yet with international fingers being wagged at Batista, he spared both Castro brothers long imprisonment. Once liberated, the putschists fled to Mexico where, influenced by both his brother Raul and his new acquaintance, Che Guevara, Fidel formed the 26th of July Movement (M26J) with the intention of returning to Cuba and fighting a guerrilla war against Batista.

In 1956, the 82 men of various nationalities in the M26J landed on a deserted beach in the east of the island on the now legendary Granma boat. Almost immediately, the poorly equipped rebels suffered huge losses following a skirmish with the Cuban military. Batista subsequently declared that Fidel had been killed, yet Fidel, Raul and Che were amongst the 20 odd survivors who managed to regroup and retreat deep into the thick forests of the Sierra Maestra.

Meanwhile, the last 10 years had seen a succession of attacks on labour organising. The new anarchist labour federation had been crippled by governmental manipulation, and the anti-Bolshevik sentiments of Prio had led to him banning the new incarnation of the Communist Party, the PSP. This resulted in Communist support for Batista’s 1952 coup, although the Party was not rewarded, as Batista sought to appease Washington instead.

In the aftermath of Fidel’s reported death in 1956, an awkward power struggle ensued between the discredited Batista and his various Havana based opponents, including an increasingly assertive movement based out of Havana University. However, in January 1957, through his contacts and supporters from his days as a Havana politician--people such as the President of Bacardi Rum, the former head of the Cuban national bank and a smattering of Catholic leaders--Fidel was able to organise the visit of New York Times journalist Herbert Matthews to the Sierra Maestra. Matthews was a famous and trusted liberal media personality in the US, and the scoop of reporting Fidel’s continued existence, as well as Matthews’ breathless account of illegally evading Batista’s security forces en route, made front page news three days in a row. Fidel used the opportunity to depict himself as a romantic idealist in the vein of Robin Hood, as well as dispelling Batista’s slurs against him as a communist. He told Matthews that he was fighting for "an end to the dictatorship" and a return to the 1940 Constitution, and Matthews surmised that he was "democratic and therefore anti-communist". His obvious charisma was also emphasised in photographs of him in military fatigues, with cigar and beard, an image that has come to represent the uniform of the left wing revolutionary.

The interview was a huge PR success for both parties. Matthews, now convinced of Fidel’s cause, became an unofficial advisor to the US State Department on Cuban issues. Moreover, the interview placed Fidel as the leading figure in the Cuban opposition in American eyes, despite his relative irrelevance as the leader of a band of 30 men in the sierra, lacking equipment, training and support. The influence of American media over Cuba was starkly demonstrated when smuggled copies of the New York Times arrived in Havana’s cafes, causing the meteoric rise of Fidel’s public profile, and positing him as a viable and safe alternative to Batista that the fractured oppositional forces in Cuba’s ruling class could unite behind. For the many malcontents with Batista’s corruption and recklessness, Fidel’s anti-communist pronouncements amounted to a guarantee of private property and a promise to curb class struggle.

However, while many figures in the urban bourgeoisie started to cosy up to the M26J, Cuba as a whole started to experience an escalation in working class resistance to Batista. In 1957 university students launched an unsuccessful attempt to assassinate the President in his offices, resulting in their leader, Jose Antonio Echeverria, being shot dead by palace guards. Echeverria had been sitting on top of a militant student body but his ill-conceived methods killed 35 students and effectively culled any moves towards organising the students against Batista. From thence onwards, students resorted to isolated bombings and other such terroristic acts intended to catalyse a more general anti-Batista struggle. Indeed, the entire country found itself subject to a succession of bomb attacks which were later accredited to anti-Batista sentiment, most of which had little connection to Fidel and his resurgent guerrilla movement.

Fidel, spurred on by his popularity in the US and, by proxy, in Cuba, had seized a section of the Sierra Maestra as his own "autonomous zone" and was rumoured to be planning to seize Santiago, the nearest major city in the Oriente province. Outside of Oriente, however, Cubans were largely sceptical about his intentions. In Escambray province, a separate rebel militia seized control of the key city of Cienfuegos (site of a failed insurrection the previous year), while the continued bomb attacks, which had now become nationwide, flew in the face of Batista’s emphasis on maintaining law and order.

Eventually, Batista fled a very volatile Havana practically on the stroke of midnight as 1958 became 1959. His probable motivations were a loss of his most loyal advocates in the armed forces due to the destabilising effect of civil unrest, against which he had emerged as a caudillo leader a quarter of a century earlier. His last act was to appoint his successor as commander of the armed forces, who promptly surrendered the entire nation to Fidel, who had already claimed the province of Oriente as his own jurisdiction, and told the guerrilla leader that the entire Cuban armed forces were now at his disposal. Subsequently, as the Batista regime’s chosen recipient of their surrender Fidel, the eloquent, romantic Robin Hood that Matthews had sold to the West--the image that the left still paints of him--became the apparent victor, the flipside to the vanquished Batista that the international media would need. Only seven days after Batista’s military planes left Havana International Airport, Capitol Hill recognised Fidel as the new Cuban head of state.

Of course, in reality, even after the removal of Batista, Fidel’s passage to power was more drawn out. Having nominated the liberal minded bourgeois dissident Manuel Urrutia as President of a Provisional Government Fidel, himself Prime Minister, embarked on a tour to New York City, where he further curried favour with the American media--playing basketball in Harlem, eating a hotdog and impressing on then Vice-President Richard Nixon that he was not a communist.

However, Fidel, like Batista before him, was also aware of the power of the Cuban Communist Party, as one by one weak, liberal minded ministers in the Provisional Government resigned over disagreements concerning the implementation of the 1940 Constitution (only imposed in part), multi-party elections (which never happened) and the proliferation of Moscow-backed Communists from the PSP within the Provisional Government, who within months forced Urrutia’s resignation. His replacement was Osvaldo Dorticos Torrado, a wealthy member of the PSP. In 1961, the M26J merged with the PSP and renamed itself the Cuban Communist Party, with Fidel as its General Secretary.

With all its internal wrangling and jockeying for position, the Provisional Government risked losing control of a rapidly advancing social revolution that was occurring in the near power vacuum that was Cuba in the period immediately following the 1959 Revolution, and therefore enacted some reforms in order to head off more serious social changes. Rent and electricity prices were slashed by the nascent ruling caste, who also finally signed into law land reforms promised by Batista 20 years previously. Fidel personally oversaw a succession of modernisation projects, many of which had been started or at least drawn up by Batista. The much vaunted nationalisations of foreign businesses that took place in 1960 and 1961 were motivated originally by a desire to claim profits for the Cuban state which historically had left the island, but ultimately Fidel et al became embroiled in a game of tit for tat with the offended Eisenhower administration, resulting in Fidel’s infamous declaration of socialism in 1961 and the subsequent alliance with the USSR.

In the areas in which Fidel’s regime did manage to materially benefit the Cuban people, it was mainly due to the replacement of corrupt batistiano managers with non-skimming fidelista apparatchiks (although that’s not to indicate that Fidel’s regime has been without corruption), and the piecemeal reforms thrown to the Cuban people in order to gain their favour and stem the tidal wave of families fleeing the country. The revolution did reverberate in Havana’s corridors of power in the sense that it instigated a shift in focus in the ruling class’ politics. Hegemony took on a more human face as capital came to be administered by the state. Social programmes were prioritised over free market economics, but the class structure and the relationship of power were not removed, and neither were many batistiano stalwarts (at least two Communist Party ministers in Fidel’s Provisional Government were actually veterans of the 1952 coup).

Simultaneous to what has been referred to as the second Cuban revolution of 1959-1961, Fidel et al also instigated the hounding of Cuba’s notoriously militant and independent workforce as it attempted to assert itself once more in the new political climate. Workers’ and peasants’ cooperatives had sprung up through the length and breadth of the island--in shoe workshops, on newly created rice fields and in the tobacco factories of Pinar del Rio. Yet they all found themselves subsumed under the centralised control of the state run National Institute of Agrarian Reform and its communist bureaucrats, who concerned themselves with enacting further Batista designed reforms. The Cuban union movement, barely recovered from Batista’s repression, was "provisionally" hijacked by unelected Communist Party leaders, who promptly purged all independent voices and eventually banned all non-state affiliated labour organisations and even the right to strike, leading to many voices drawing the comparison with the corporativist devouring of the Italian workers’ movement by Mussolini’s fascist state almost 40 years earlier.

Resistance to this counter-revolution led to imprisonment, and many Cuban anarchists did find themselves alongside homosexuals and right wing dissidents in the infamous Cuban labour camps. Fortunately, most militants were able to flee to the United States, where the Cuban Libertarian Movement in Exile was formed, and continues to this day.

Meanwhile, the Castro dynasty appears to have withstood its post-Fidel cosmetic reforms and continues almost unchecked in Havana. Cubans are forced to resort to the black market for fundamental foodstuffs while the state relies on tourism to resuscitate its starved economy. The Cuban working class, denied even the right of immigration, as well as the legal means with which to defend itself against state capital’s attacks, has developed something of a stoic survival instinct and defiant humour, while international handwringers bemoan the state’s inhumane treatment of its discontents.

The Cuban experience stands as empirical evidence of the folly of a popular revolution being accredited to one man and his clique, as well as providing a means of distinguishing between the empty promises of anti-imperialism as opposed to genuine class struggle activity.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

