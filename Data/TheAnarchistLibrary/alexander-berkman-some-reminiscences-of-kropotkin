
It was about 1890, when the anarchist movement was still in its infancy in America. We were just a handful then, young men and women fired by the enthusiams of a sublime ideal, and passionately spreading the new faith among the population of the New York Ghetto. We held our gatherings in an obscure hall in Orchard Street, but we regarded our efforts as highly successful. Every week greater numbers attended our meetings, much interest was manifested in the revolutionary teachings, and vital questions were discussed late into the night, with deep conviction and youthful vision. To most of us it seemed that capitalism had almost reached the limits of its fiendish possibilities, and that the Social Revolution was not far off. But there were many difficult questions and knotty problems involved in the growing movement, which we ourselves could not solve satisfactorily. We longed to have our great teacher Kropotkin among us, if only for a short visit, to have him clear up many complex points and to give us the benefit of his intellectual aid and inspiration. And then, what a stimulus his presence would be for the movement!

We decided to reduce our living expenses to the minimum and devote our earnings to defray the expense involved in our invitation to Kropotkin to visit America. Enthusiastically the matter was discussed in group meetings of our most active and devoted comrades; all were unanimous in the great plan. A long letter was sent to our teacher, asking him to come for a lecture tour to America and emphasizing our need of him.

His negative reply gave us a shock: we were so sure of his acceptance, so convinced of the necessity of his coming. But the admiration we felt for him was even increased when we learned the motives for his refusal. He would very much like to come — Kropotkin wrote — and he deeply appreciated the spirit of our invitation. He hoped to visit the United States sometime in the future, and it would give him great joy to be among such good comrades. But just now he could not afford to come at his own expense, and he would not use the money of the movement even for such a purpose.

I pondered over his words. His viewpoint was just, I thought, but it could apply only under ordinary circumstances. His case, however, I considered exceptional, and I deeply regretted his decision not to come. But his motives epitomized to me the man and the grandeur of his nature. I visioned him as my ideal of revolutionist and Anarchist.

Years later, while I was in the Western Penitentiary of Pensylvania, the hope of seeing our Grand Old man Kropotkin for a moment illumined the darkness of my cell. Friends had notified me that Peter had come to the States on his way to Canada, where he was to participate in some Congress of scientists. Peter intended to visit me, I was informed, and I counted the days and the hours waiting for the longed-for visit. Alas, the fates were against my meeting my teacher and comrade. Instead of being called to see my dear visitor, I was ordered into the Warden’s office.* He held in his had a letter, and I recognised Peter’s small and neat handwriting. On the envelope, after my name, Kropotkin had written, “Political Prisoner”.

The Warden was in a rage. “We have no political prisoners in our free country!” he roared. And then he tore the envelope into pieces. I became enraged at such desecration. There followed a hot argument on American freedom in the course of which I called the Warden a liar. That was considered lese majesté and he demanded an apology. I refused. The result was that instead of meeting Peter I was sentenced to 7 days in the dungeon, which was a cell 2 feet by four, absolutely dark and 15 feet underground, one small slice of bread as my daily ration.

That was about the year 1895. In the years following Peter Kropotkin had repeatedly visited America, but I never got a chance to see him, because I was mostly in punishment in prison and for ten years I was deprived of visits and not allowed to see any one. A quarter of a century passed before I could at last take the hand of my old comrade in mine. It was in Russia, in March 1920, that I first met Peter. He lived in Dmitrov, a small town 60 verats from Moscow. I was in Petrograd (Leningrad) then, and the railroad conditions were such that traveling from the North to Dmitrov was out of the question. Later on I had a chance to go to Moscow and there I learned that the Government had made special arrangements to enable George Lansbury, the editor of the London Dail Herald, and one of his contributors, to visit Kropotkin in Dmitrov. I took advantage of the opportunity, together with our comrades Emma Goldman and A. Schapiro.

Meeting “celebrities” is generally disappointing: rarely does reality tally with the picture of our imagination. But it wasw not so in the case of Kropotkin; both physically and spiritually he corresponded almost exactly to the mental portrait I had made of him. He looked remarkably like his photographs, with his kindly eyes, sweet smile and generous beard. Every time Kropotkin entered the room it seemed to light up by his presence. The stamp of the idealist was so strikingly upon him, the spiriturality of his personality could almost be sensed. But I was schocked at the sight of his emaciation and feebleness.

Kropotkin received the academic pyock which was considerably better than the ration issued to the ordinary citizen. But it was far from sufficient to support life and it was a struggle to keep the wolf from the door. The question of fuel and lighting was also a matter of constant worry. The winters were severe and wood very scarce; kerosene difficult to procure, and it was considered a luxury to burn more than one lamp in the house. This lack was particularly felt by Kropotkin; it greatly handicapped his literary labors.

Several times the Kropotkin family had been dispossessed of their home in Moscow, their quarters being requistioned [sic] for government purposes. They they decided to move to Dmitrov. It is only about half a hundred verats from the capital, but it might as well be a thousand miles away, so completely was Kropotkin isolated. His friends could rarely visit him; news from the Western world, scientific works, or foreign publications were unattainable. Naturally Kropotkin felt deeply the lack of intellectual companionship and mental relaxation.

I was eager to learn his views on the situation in Russia, but I soon realised that Peter did not feel free to express himself in the presence of the English visitors. The conversation was therefore of a general character. But one of his remarks was very significant and gave me the key to his attitude. “They have shown,” he said, referring to the Bolsheviki, “how the Revolution is not to be made.” I knew, of course, that as an Anarchist Kropotkin would not acept any Government position, but I wanted to learn why he was not participating in the economic up-building of Russia. Though old and physically weak, his advice and suggestions would be most valuable to the Revolution, and his influence of great advantage and encouragement to the Anarchist movement. Above all, I was interested to hear his positive ideas on the conduct of the Revolution. What I had heard so far from the revolutionary opposition was mostly critical, lacking helpful constructiveness.

The evening passed in desultory talk about the activities on the front, the crime of the Allied blokade in refusing even medicine to the sick, and the spread of disease resulting from lack of food and unhygenic conditions. Kropotkin looked tired, apparently exhausted by the mere presence of visitors. He was old and weak; and I feared he would not live much longer under those conditions. He was evidently undernourished, though he said that the Anarchists of the Ukraina had been trying to make his life easier by supplying him with flour and other products. Makhno, also, when still friendly with the Bolsheviki, had been able to send him provisions. Not to tire Peter too much, we left early.

Some months later I had another opportunity to visit our old comrade. It was summer-time and Peter seemed to have revived with the resurrection of Nature. He looked younger, in good health and full of youthful spirit. Without the presence of outsiders, like the former English visitors, he felt more at home with us and we talked freely about Russian conditions, his attitude and the outlook for the future. He was the genial Old Peter again, with a fine sense of humor, keen observation and most generous humanity. At first he chided me solemly on my stand against the War, but he quickly changed the subject into less dangerous channels. Russia was our main point of discussion. The conditions were terrible, as everyone agreed, and the Dictatorship the greatest crime of the Bolsheviki. But there was no reason to lose faith, he assured me. The Revolution and the masses were greater than any political Party and its machinations. The latter might triumph temporarily, but the heart of the Russian masses was uncorrupted and they would rally themselves to a clear understanding of the evil of the Dictatorship and of Bolshevik tyranny. Present Russian life, he said, was an artificial condition forced by the governing class. The rule of a small political Party was based on false theories, violent methods, fearful blunders and general inefficiency. They were suppressing the very expression of the people’s will and initiative which alone could rebuild the ruined economic life of the country. The stupid attitude of the Allied Powers, the blockade and the attacks on the Revolution by the interventionists were helping to strengthen the power of the Communist regime. But things will change and the masses will awaken to the realisation that no one, no political Party or governmental clique must be permitted in the future to monopolise the Revolution, to control or direct it, for such attempts inevitably result in the death of the Revolution itself.

Various other phases of the Revolution we discussed on that occasion. Kropotkin particularly emphasised the constructive side of revolutions, and especially that the organisation of the economic life must be dealt with as the first and greatest necessity of a revolution, as the foundation of its existence and development. This thought he wanted to impress most forcibly upon our own comrades for our guidance in the coming great struggles of the international proletariat.

My visits to our dear Peter were a treat, intellectually and spiritually. I was leaving for the Ukraina for a long tour in behalf of the Petrograd Museum of the Revolution, but I hoped for many more visits to our old, brave teacher of the wonderful brain and heart. It was not to be. He died some months later, on February 8, 1921. I could reach his bedside in time only to say my last farewell to the dead. A great Man, a great Anarchist had departed.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

