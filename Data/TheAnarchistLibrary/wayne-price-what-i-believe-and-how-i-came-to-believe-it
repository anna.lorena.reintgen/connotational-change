      References
The following essays cover different topics, written at different times, but all reflect a particular viewpoint. I am not a spokesperson for anyone and make no claims to be an “orthodox anarchist,” whatever that would be. But my views are more-or-less consistent with the main tendency of certain traditional and current anarchist thought. This tendency is revolutionary, believing that eventually the working class will have to directly confront and dismantle the state; it is anti-capitalist, in the tradition of libertarian socialism, social anarchism, and anarchist-communism; it is decentralist, believing that society should be in human scale, rooted in direct democracy; it is federalist, believing that local assemblies and workplace councils should replace the state with associations and networks; it is internationalist, believing that a world-wide revolution is necessary; it is for ecological and environmental rebuilding of all industry and technology, in the tradition of social ecology and Green anarchism; as class struggle anarchism, it sees the working class as central to the revolutionary struggle, an analysis which overlaps with the libertarian and humanistic aspects of Marxism; yet it also supports every struggle against oppression by every group and on every issue, including that of women, People of Color, nations oppressed by imperialism, Gay Lesbian Bi and Transexual people, physically disabled people, those opposed to war or to ecological catastrophe; etc. In order to achieve these goals, it believes that revolutionaries should organize themselves to fight for them by word and example; this is seen as part of the self-organization of the working class and oppressed, a program called Platformism or especifisimo.

If you are not at least curious about these ideas, do not bother to read this book.

I came to this set of ideas by a zig-zag process. I grew up in the suburbs, the child of white collar workers (at the upper end of the working class or the lower end of the middle class). I suffered no material deprivation or personal abuse, but I was intensely, neurotically, unhappy. Shortly before entering high school, I spent a summer in a camp program for teenagers. By happenstance, I came across the writings of Paul Goodman and Dwight Macdonald which converted me to anarchist-pacifism. Over time, I was also influenced by the bioregionalist Lewis Mumford and by the humanistic Marxist, Erich Fromm (Fromm and Goodman were also both radical psychologists). These and other writers convinced me that there was another way for human beings to relate to each other, more human, kinder, and more rational, than what I was used to. People, they said, needed decentralized, human-scale, face-to-face, radically democratic, communities, and this was technologically possible. They answered my intense need to rebel against authority while still keeping most of the humanistic and democratic values I had internalized from my liberal parents. I regarded myself as a decentralist socialist — and still do. (Paul Goodman is discussed further in one of the following essays.)

When I went to college, I joined the Students for a Democratic Society and participated in the movement against the Vietnamese war. In the course of this, I ran across a Trotskyist who talked me out of anarchist-pacifism. He persuaded me that a revolution was needed and that anarchist-pacifism was not a sufficient program for revolution. He argued that nonviolence would not work against a committed evil force, such as the Nazis. He gave me works on the Hungarian revolution and the Spanish revolution of the thirties. These argued that the Leninist concept of a “workers’ state” or “dictatorship of the proletariat” meant that workers, peasants, and soldiers should form assemblies and councils and should associate these together as an alternate power to either the fascists or to the liberal capitalist state. Why, I thought, I am for that! I still am, although I would not call this a workers state. So I became a Trotskyist.

But I never could agree with — or even understand — his orthodox Trotskyist belief that the Soviet Union was a workers’ state, as were Eastern Europe and China, and especially Cuba. He admitted that the workers did not control any of these states, that the workers and peasants were extremely oppressed in all of these states (except, he claimed, Cuba), and that the regimes, outside of the USSR, had all come to power without workers’ revolutions. Nevertheless, he insisted, the workers were the ruling class in these states because industry was nationalized and the economy was planned (he thought). I thought this was ridiculous and in complete contradiction to the democratic and proletarian view of Marxism I had been learning.

So, much to this Trotskyist’s disgust, I joined the unorthodox, soft, semi-social-democratic, wing of Trotskyism. This rejects Trotsky’s view that Stalin’s Soviet Union was a workers’ state, in favor of theories that the Stalinist bureaucracy was a new ruling class, maintaining either state capitalism or a new type of class society (“bureaucratic collectivism,” similar to the “coordinatorist” theory of today’s Pareconists). In 1969 I was a founding member (that is, I was at the founding conference) of the International Socialists. This was in the the tradition of the Independent Socialist League of Max Shachtman but was also influenced by the British International Socialists (now the Socialist Workers Party).

Later I was to work together with my Trotskyist friend from college when I began to do opposition work in the New York City teachers union. The last I heard he has become a leader of Socialist Action, a split-off from the U.S. Socialist Workers Party (no relation to the British group) after the latter abandoned Trotskyism altogether for Castroism.

What most attracted me to the I.S. was its concept of “socialism-from-below” as opposed to “socialism-from-above,” expounded by Hal Draper in his pamphlet, The Two Souls of Socialism (reprinted in Draper, 1992, pp. 2 — 33). Real socialism, he argued, could only come about through the upheavals of ordinary people, workers and others, against the elites who ruled us, and this had to be done against those who only wanted to use the people as a battering ram to put themselves in power. He claimed that this was the essential meaning of the Marxism of Marx and Engels, and eventually wrote a series of fat books to argue his case (e.g., Draper, 1977). These books are worth reading, in my opinion, despite his anti-anarchist bias, which I accepted at the time (as I put my decentralism on hold). Draper’s contributions are discussed further in one of my essays below; I am still for socialism-from-below.

This position was not easy to hold in the 60s and 70s. People who regarded themselves as revolutionaries were mostly attracted to the politicians which seemed to be leading revolutions against U.S. imperialism: Castro, Ho Chi Minh, and Mao Tse Tung. All three were dictators in the tradition of Stalin, who made revolutions based on control of the peasantry and not on the workers’ and peasants’ self-organization. U.S. Maoists became influential among radical workers and People of Color. The main Trotskyists were the orthodox sort who also regarded these regimes as workers’ states. Meanwhile the low level of working class struggles in that period made it difficult to argue for a working class orientation, as we did. The I.S. were marginalized.

A number of us came to conclude that the I.S., was not really revolutionary in action, being organizationally sloppy and politically muddled. Draper himself did a lot to push the I.S. toward building a middle class liberal party, the Peace and Freedom Party, whose only virtue was that it was not the Democrats or Republicans (similar to today’s Green Party, Labor Party Advocates, or Nader’s electoral runs, which have been supported by the decendents of the I.S.). A fierce faction fight broke out and we split off (were expelled), forming the Revolutionary Socialist League in 1973. The I.S. continued; today its main survivors in the U.S. are the International Socialist Organization (probably the largest Left group) and Solidarity.

Our goal was to be really Trotskyist, unlike the I.S., except for the orthodox Trotskyist position on the Soviet Union. From the start the R.S.L. rejected Trotsky’s belief that Stalin ruled a workers’ state, in favor of a state capitalist analysis. Otherwise we studied Trotsky’s writings and sought to be as Trotskyist as could be.

Fervent Trotskyism may seem like an odd detour from socialism-from-below to revolutionary anarchism, but there was a logic to it. What we saw in Trotsky’s Trotskyism was a serious approach to revolution. It offered the intellectual resources of Marxist theory (I studied and taught the three volumes of Capital). It was based on an analysis that capitalism was in an overall epoch of decay, despite the extended periods of apparent prosperity after World War II, and that therefore reforms could not be won on a consistent and lasting basis. It believed that the revolution could be made by the working class, in particular by the most oppressed sections of the working class: women, African-Americans, workers of the oppressed nations, youth, etc. (in this it was consistent with socialism-from-below). It sought to replace the states of capitalism and of the Stalinist bureaucracy with associations of councils (soviets), with democracy for opposing tendencies. It called for world revolution.

Especially, Trotsky’s Trotskyism opposed both holding ourselves aloof from popular reform struggles, as the sectarians do, or burying ourselves in reform efforts, as the opportunists do. It looked for ways for revolutionaries to combine active participation in the struggles of the exploited with an open expression of the need for revolution. Trotsky demanded of his followers that they “say what is,” tell the truth to the workers about the need for revolution, even while participating in more limited reform efforts. He taught methods for this, such as the United Front, critical support, the Permanent Revolution, and transitional demands, which we studied as they had been applied in various revolutions in the past. We tried to apply the lessons of revolutionary history in our own situtation, but this became more difficult as time went on and the period became more conservative. I won’t say we were perfect in combining revolutionary propaganda with popular participation — far from it — but we tried.

We were also deeply influenced by the movements for Women’s Liberation and for Gay Liberation. It was not so much their overt programs, but their implicit libertarianism more and more came into conflict with the authoritarianism of Trotskyism. In general we found it increasingly difficult to reconcile the democratic-libertarian side of Marxism with its authoritarian side. Why had Trotsky insisted that Stalin’s state (which he had said was similar to Hitler’s) was nevertheless workers’ rule, so long as the economy remained nationalized? This made the actual power of the workers to be secondary to the importance of the statified economy in his conception of socialism. Why had Lenin and Trotsky set up a one-party dictatorship? If Marx and Engels were so democratic, as Draper claimed, how come their followers were almost all authoritarians (as Draper admitted)? Were we so right in saying that we were almost the only ones who really understood Marx, while 99.99% of self-proclaimed Marxists had an entirely different interpretation? Perhaps their authoritarian interpretation of Marxism also had a legitimate basis in the work of Marx, Lenin, and Trotsky?

We held a discussion of the failures of Trotskyism (summarized in Hobson & Tabor, 1988, which included an analysis of the Soviet Union’s state capitalism). This was followed by Ron Tabor’s (1988) devastating critique of Leninism. I contributed a few papers on decentralism, workers’ control of industry, and anarchism. Meanwhile a minority had split off (been expelled) because they wanted to continue to develop their own orthodox Trotskyism (see Daum, 1990).

We were attracted by the growth of an anarchist movement in the 80s. In 1989 we dissolved the R.S.L. after about 16 years. Some of us then joined with a variety of younger anarchists to form the Love and Rage Revolutionary Anarchist Federation (most former R.S.L. members dropping out of politics altogether). This was the wing of anarchism which saw itself as leftist and anti-capitalist; they supported the struggles of People of Color, of women, and of oppressed nations. Unfortunately, they were ambivalent about supporting the working class. They were for a distinct anarchist organization, unlike the anti-organizationalist anarchists. They were serious about joining in popular struggles in a militant way, working together with others while raising the perspective of anarchist revolution.

Love and Rage lasted for nine years. As my friends and I had been moving from Marxism to anarchism, others had been moving from anarchism to Marxism — of a Maoist variety, no less. As our paths crossed, we thought for a while that we agreed with each other, but actually we were moving in opposite directions. The left as a whole was declining in the 90s, including its anarchist wing. In reaction, there was an attraction for some to the “successes” of Marxism and its body of work. Former R.S.L. members and a few others opposed this tendency, out of our many years of hating Stalinism. The resulting faction fight ended with the dissolution of Love and Rage in 1998.

I remain an anarchist, a decentralist socialist, and a believer is socialism-from-below. As a class struggle, Platformist, revolutionary anarchist, I can have all the benefits I sought as a Trotskyist, while maintaining the libertarian vision of anarchism. I no longer advocate a “workers’ state” (whatever that means), but I do advocate a federation of workers’ and popular councils (in the tradition of the Friends of Durruti Group of the Spanish revolution). I no longer advocate a vanguard (Leninist) party, which aims to rule over the workers, but I do advocate a revolutionary organization of anachist workers: Platformism or especificismo. (These topics are discussed in essays in this book as well as in my book, The Abolition of the State: Anarchist and Marxist Perspectives.) While I no longer call myself a Marxist, I accept many ideas from the Marxist tradition (as can be seen from my essays) This is especially true from the libertarian Marxists (such as C.L.R. James, the council communists, etc.). I now regard myself as a Marxist-informed anarchist. I have joined the Northeastern Federation of Anarchist-Communists (or NEFAC) and write for the www.Anarkismo.net site, which is the web site for our international tendency.

It cannot be said that I have sacrified much by being a revolutionary, compared to others, especially to those in other countries who they have risked years of imprisonment or even their lives. All I have lost has been some time and some money. I have undergone some emotional stress, went to many boring meetings, and had a few profoundly moving experiences. I met a few stinkers and some wonderful human beings. I still believe in the ideal, as something both necessary to save the world from destruction and as morally right. There are better ways for humans to live and work together.

Daum, Walter (1990). The life and death of Stalinism; A resurrection of Marxist theory. NY: Socialist Voice Publishing Co.

Draper, Hal (1992). Socialism from below (E. Haberkern, ed.). New Jersey/London: Humanities Press.

Draper, Hal (1977). Karl Marx’s theory of revolution; Vol. 1: State and bureaucracy. NY/London: Monthly Review Press.

Hobson, Christopher, & Tabor, Ronald D. (1988). Trotskyism and the dilemma of socialism. NY/Westport CT/London: Greenwood Press.

Taber, Ron (1988). A look at Leninism. NY: Aspect Foundation.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



Daum, Walter (1990). The life and death of Stalinism; A resurrection of Marxist theory. NY: Socialist Voice Publishing Co.



Draper, Hal (1992). Socialism from below (E. Haberkern, ed.). New Jersey/London: Humanities Press.



Draper, Hal (1977). Karl Marx’s theory of revolution; Vol. 1: State and bureaucracy. NY/London: Monthly Review Press.



Hobson, Christopher, & Tabor, Ronald D. (1988). Trotskyism and the dilemma of socialism. NY/Westport CT/London: Greenwood Press.



Taber, Ron (1988). A look at Leninism. NY: Aspect Foundation.

