      Culture, Communication, and Organization in an Anarchist Soup Kitchen      Background      Pass The Salt, Share The Work      The Four Hardest Words: “Hello, My Name Is...”      An Unorthodox Chapter Of An Ostensibly Unorthodox Organization      References:
Every Friday at 4 PM, a motley group of people come together at a Mennonite Church in Harrisonburg, Virginia, and eat a free meal cooked from dumpstered and donated food. They are young and old, black, white, and latino; workers, students, and unemployed. The event is called Harrisonburg Food Not Bombs. Some people think of it primarily as a free meal; others think of it as social time; some experience it as a duty, and others consider it a radical act. Amid all these varying perceptions lies an experiment in anarchist organizing that has succeeded roughly as much as it has failed, and if we dig deeper, a veritable dumpster-full of lessons for anarchists trying to organize with one another and with their communities.

This critical history will paint a background portrait, for those not familiar with Food Not Bombs or the Harrisonburg chapter thereof, and then examine three episodes that shed light on difficulties and developments experienced by the Harrisonburg activists, which will hopefully be instructive for anarchists elsewhere.

Food Not Bombs started in Cambridge, Massachusetts, in 1980 and has since grown “hundreds” of autonomous chapters across the country and across “the Americas, Europe, Asia, and Australia” (www.foodnotbombs.net, 2005). Essentially, the group is a sort of soup kitchen with four major modifications. Meals are vegan, to draw attention to the violence of industrial meat production and its role in exacerbating global poverty and hunger, and also though not admittedly, to cater to the white punk/anarchist subcultures from which most chapters draw their activists. Meals are served in the open, to resist the shame and obscurity with which poverty is made invisible, to make a visible political act out of serving free food, and oftentimes to meet homeless people on their own turf, in the urban parks where they congregate. Thirdly, Food Not Bombs sets itself in opposition to charity, ideally avoiding the paternalism of traditional soup kitchens and striving for the ideal of cooking and eating meals together, to blur the distinction between the giver and receiver of charity. Finally Food Not Bombs is anti-militarist. This orientation manifests in the name, in the distribution of literature by many chapters portraying militarism as a drain on social resources and a cause of poverty, and in the location of Food Not Bombs within anti-war, anti-globalization, and other leftwing opposition movements (either through the other affiliations of Food Not Bombs activists or the collaboration between Food Not Bombs and other protest groups, whereby a Food Not Bombs chapter might cook meals for a protest or conference).

Harrisonburg Food Not Bombs began in Fall 2001 in conjunction with a new anarchist community center, which city authorities later shut down. A group of fewer than a dozen activists, predominantly young, white students from James Madison University, assembled the bare minimum of cooking supplies and began serving meals. The group initially served meals downtown, on the sidewalk in front of the community center once that opened. After the anarchist space got shut down, Food Not Bombs moved its weekly meals to Court Square, and eventually shut down for about a year due to lack of help. It restarted in 2003, serving out of a park on the northeast end of town, but by the end of the year moved into a small, progressive Mennonite church in an old working class neighborhood, and began serving its meals indoors. Harrisonburg Food Not Bombs has served meals at this location every Friday for over two years now.

The majority of materials used by Harrisonburg Food Not Bombs are taken from grocery store dumpsters, and the rest is acquired through donations. Seldomly, staples, spices, or cooking oils are purchased out-of-pocket.

There is a high degree of regularity to who eats and prepares meals every week at Food Not Bombs, though the group is dynamic and fluctuating. If a particular Food Not Bombs chapter successfully achieved its ideal, everyone involved would be considered a Food Not Bombs member, as membership is open and informal, and everyone is intended to cook, eat, and clean up together, regardless of whether they are a homeless person, a full-time activist, or a student. Harrisonburg Food Not Bombs does not meet this ideal, and neither does any chapter I know of.

Those who do nearly all the planning and preparing for the meal are far more homogenous than the total pool of people who come out for any given meal. The people seated around the plastic tables every Friday are black, white and latino; they include children and septuagenarians; students, social workers, taxi drivers, the homeless, disabled, and unemployed. But the Food Not Bombs members, those who hold power and responsibility in the outcome of the weekly meal, are on average younger, whiter, wealthier, better educated, and more politically active. Harrisonburg members struggle to make the appropriate distinctions to describe the divisions within the group. Generalizations regarding age or socioeconomic background are accurate, but imperfect. Not all older people and working-class/homeless people remain external to the group, and many younger people and middle-class people do not become members. Though it is not defined, membership in Food Not Bombs is observable, and the barrier between membership and non-membership is most certainly cultural. But it is not an impassable barrier, and membership exists in degrees, or shades of grey, in Harrisonburg Food Not Bombs. One old, disabled curmudgeonly man with little sympathy for anarchist activism has won a firm social niche over the years, and he regularly carries out multiple responsibilities, though he declines to partake in meetings. To avoid generalizing either group I will make my distinction functional, between “activists” and “participants”. The latter term is not simply a euphemism. Anyone who comes to Food Not Bombs often enough will eventually participate in some way other than simply eating. After all, the meal is not treated like charity, end everyone is encouraged to help out. However, most people will never become involved in decision-making, planning, or other regular responsibilities fulfilled by the “activist” core. It should be understood that participants are a diverse group, while activists are predominantly young and white, and mostly students, though with limited variation.

For most of its history Harrisonburg Food Not Bombs distributed work in such a way as to encourage burn-out among activists. For several months at a time, two or three activists would be responsible for nearly all the work each and every meal, including dumpstering the night before (I’ll refer to these people as “leaders”, though the only authority they had stemmed from the fact they did all the work). They would carry out these duties as best they could, and just when they were ready to give up, another two or three people stepped forward, and the former leaders faded into the background, sometimes never returning to the group, sometimes filling the thankless leadership role again down the road. I personally have played this role three or four times throughout the history of Harrisonburg Food Not Bombs. There were a few exceptional periods in the groups history when large numbers of people shared and rotated work, but these were rare. In Fall 2005, the current leaders were suffering from burn-out, and other activists who had already had experience as the leaders did not want to step forward again. There were few people coming to the meal who would actually go hungry without free food, and morale was low. Food Not Bombs activists discussed ending the weekly meal and devoting their energies to other projects. We realized after all, that Harrisionburg’s Food Not Bombs had originally been founded, and restarted, not based on any strategic decisions regarding local organizing or the needs of the community (which has a small homeless population relative to other cities), but because Food Not Bombs was something anarchists in other cities did, so...

In response to our impending end, one community member who did rely in part on our meals began searching for restaurant donations for us, to spare us some of the work of dumpstering, and several younger student activists began recruiting their friends to come help out. Morale boosted, we began to figure out how we could share the work and make Food Not Bombs sustainable. In the past we had tried dividing tasks between volunteer coordinators (e.g., dumpstering, prep, cleanup, outreach). Each coordinator was supposed to coordinate their particular field of work and recruit volunteers to help them cook, clean up, or dumpster each week. But every time, the system fell apart after a couple of months. Coordinators would pass on their responsibilities to other activists, or would take on all the work themselves and not look for other volunteers to help out and learn the skills that would be necessary for them to take on more responsibilities. No one held themselves or other activists accountable to their obligations.

This time around, we met and came up with a new structure for sharing work. We set out coordinator positions again, but stipulated that they be rotated every month. We also emphasized and agreed to make criticisms and compliments of one another, and actively hold other activists accountable. So far, creating a visible power structure has allowed more people to take on responsibilities within the group, though time has yet to tell whether we will keep ourselves accountable, or lapse again in fulfilling and sharing the duties involved in making the weekly meal.

A recent academic article on the anarchist movement, co-authored by a retired police chief, speculates on how the movement funds itself. One hypothesis is that networks of Food Not Bombs chapters across the country provide free food for anarchists (Borum and Tilby, 2005). Though the article is laughably haphazard, the observation that Food Not Bombs exists to cater to a subculture is worth taking seriously. Numerous outside observers, and even most of us who are integrally involved with Harrisonburg Food Not Bombs, have remarked that the group is welcoming to activists who fit in with any of the white anarchist subcultures (politically radical punks, hippies, and indy kids) and varyingly lukewarm or uninviting to all other people, whether they be visiting college students who dress conventionally, or older homeless people with few cultural reference points in common. Most of the Food Not Bombs activists are very nice to newcomers, but if a group consists of a core group of friends, and then various other people, no amount of politeness will prevent divisions from arising.

An ongoing challenge for the Harrisonburg chapter and, from what I have seen, other Food Not Bombs chapters, is to be truly welcoming to people outside the dominant clique, and in fact to destroy that clique. Activists have to constantly remind themselves and one another to make an effort to talk to and get to know other participants, especially the people who rely on the free food that we serve. Otherwise, during a meal all the young activists sit, eat, and talk loudly at one table, and the poor people eat quietly at another table. People from privileged backgrounds especially fail to grasp the importance of building real relationships as a basis for their activism. We need to prioritize getting to know people from different social circles and cultural backgrounds, beyond politeness, and to the point of establishing a real rapport. Once we know someone well enough to socialize and joke with them, we have a basis for working with them, asking them to take on responsibilities, and respecting and listening to their input. This is a necessary goal if Food Not Bombs or similar projects are to be anything but charity by another name.

How does a decentralized organization evolve? Is a chapter that deviates from the original principles of the organization still a part of the group, if there is no central committee to kick it out? Perhaps more so than most chapters, Harrisonburg Food Not Bombs stretches the common traits that foster unity in the absence of centralization, hierarchy, or even regular communication between the chapters.

The Harrisonburg chapter serves its meals indoors, and we are not vegan — though there are always vegan options, we usually have food with eggs or dairy (usually dumpstered), and we often serve donated meat dishes. Many of the people involved in the Harrisonburg chapter have been Food Not Bombs activists for years, several of us having been active with chapters in other cities, and we only broke with these two norms (outdoor meals and veganism) after years of doing it by the book. I would argue to any orthodox Food Not Bombs die-hards that our two acts of deviance violate the form of Food Not Bombs, but uphold the underlying principles more effectively, given the particular local conditions we in Harrisonburg have to face.

Harrisonburg’s homeless population is small enough that they do not congregate in parks; so by serving meals outside we were bringing them out into the elements, often in bad weather, when they would otherwise be indoors or in cars. Especially through the winter, we were faced with the choice of getting food to those who needed it by moving to an indoor location, or making a point about poverty by giving free food to college students. By moving indoors we prioritized the needs of economically disadvantaged participants, and looked for other ways of making a point about poverty. Serving food in a comfortable location in a working-class neighborhood, we are working to destigmatize poverty and portray free food in a good light — accessible and enjoyable to the homeless, the working poor, and the middle-class alike.

The shift away from veganism was much more painful. The majority of lower-class participants strongly preferred us to provide non-vegan food, and several even used their food stamps to buy us meat to cook — a couple of these people stopped coming after vegan members of the group responded in an icy, exclusionary, or condescending way. Many participants appreciated our healthy food, but without any protein or fat it wasn’t much of a meal. The lack of appetizing food and the major cultural barriers created by enforcing a vegan space on a mostly omnivorous population caused consistently low turnout from the very populations we were trying to provide with free food. Some activists insisted we couldn’t be a Food Not Bombs if we weren’t vegan (though they had not brought up the same orthodox objection to our move indoors), and they also refused to recognize that vegan food is not culturally neutral, or even constitutive of a meal to people from some cultural backgrounds. No one was suggesting that we stop serving vegan food. And by adding non-vegan food to our menu, we would not be supporting the meat industry, as any meat we served was not purchased, and would have ended up in the trash. In the end, we decided to allow for a menu catering to non-vegans as well as vegans, and a couple of activists who wanted to preserve the vegan comfort zone stopped coming. On several occasions, additional people from the neighborhood have come to eat because of the availability of meat dishes, though our reputation for unsatisfying food will take time to overcome, especially given lingering difficulties making nourishing, appetizing meals with consistency (though this is more often the fault of limited resources than of the efforts of our group).

Because Food Not Bombs has no hierarchy or centralized administrative body, we changed our group in opposition to these founding principles without getting permission from any other chapter. I have a good deal of apprehension about how the often clique-ish Food Not Bombs milieu would respond to the unorthodoxy of the Harrisonburg chapter. At regional Food Not Bombs gatherings in the past, I have brought up the deviance of the Harrisonburg chapter, and the reasons for it, but nobody responded; I am not sure if this is because they were fine with it, or they wanted to stifle discussion. In any case, the autonomy of Food Not Bombs chapters is meaningless if they are ostracized for adapting to local conditions. Flexibility and autonomy are necessary, but so is communication, to foster growth and coordination between far-flung autonomous chapters. Decentralized networks such as Food Not Bombs need to develop means for communicating and discussing innovations, such as those developed and employed by the Harrisonburg chapter. Regional conferences that give greater focus to group conversations structured to trade and evaluate skills and lessons, or help particular groups brainstorm solutions to their particular problems, would be invaluable. Magazines, newspapers, and other periodicals would also help encourage discourse, as well as recruit or educate those outside the movement. Recently, e-mail listserves have come into limited use, though the medium does not encourage well thought-out analysis or the participation of the many people with limited computer and internet access and literacy.

The ideals that anarchists assert to be possible — horizontal organizing, mutual aid, and decentralization — are, indeed. But these possibilities do not become realities without a great deal of work. In a capitalist system, it makes perfect sense that people are either used to slacking off and avoiding responsibility, or taking individualistic leadership roles and toiling like martyrs. To share power and responsibilities, consciously crafting horizontal organizing structures is as important as the informal activities that no structure will automatically guarantee — like holding ourselves and each other accountable with a healthy mix of compliments and criticisms. We also need to be conscious and deliberate in overcoming the barriers of race, class, and culture if we are to create meaningful networks of mutual aid, not just self-serving cliques. Finally, our necessary emphasis on decentralization and autonomy must be complemented by developing and perfecting anti-authoritarian methods for communicating and coordinating over large areas and large populations. Without this, local groups will stagnate, or remain at the mercy of the bureaucratic, elitist coalitions that currently exploit the grassroots and dominate large-scale organizing within the anti-war and anti-globalization movements, and other movements in which decentralized networks like Food Not Bombs could play an important role if they overcame certain habitual deficiencies.

“The Food Not Bombs Story”, [internet] www.foodnotbombs.net/firstindex.html. Viewed December 13, 2005.

Randy Borum, Chuck Tilby, “Anarchist Direct Actions: A Challenge for Law Enforcement”, Studies in Conflict and Terrorism, No.28, 2005.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“The Food Not Bombs Story”, [internet] www.foodnotbombs.net/firstindex.html. Viewed December 13, 2005.



Randy Borum, Chuck Tilby, “Anarchist Direct Actions: A Challenge for Law Enforcement”, Studies in Conflict and Terrorism, No.28, 2005.

