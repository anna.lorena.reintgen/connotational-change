
They’re old things, from another century. Two anarchists get arrested after a bank job. The first robbed it, gun in hand. They say the second helped him, holding the money. It happened in a small Greek village, this past October 1. And so? There are things that happen. And then that is a far away country, with an incomprehensible and untranslatable language. Who do you want to be interested in it?

The robber is Christos Stratigopulos, already arrested and convicted here in Italy fifteen years ago on a similar charge. The penalty served, he returned to Greece. Remembered by a few, unknown to most. But the other one arrested is Italian; it is Alfredo Bonanno. Yes, precisely him; who hasn’t heard his name? In its own small way, the news has gone quickly around the world, revived by many press agencies: “one of the major theorists of insurrectionalist anarchism”, “among the major ideologues of anarchy”, “anarchist activist and writer”, “international fugitive anarchist robber”, “theorist of revolutionary violence”, has ended up behind bars again. The promoters of antiterrorism, both Greek and Italian, have rushed in, ready to exploit the juicy occasion. The elements for concocting a fine theorem are all there: a country in which there are still fires blazing after the great insurrectionary conflagration that flared up last December, a Greek anarchist active in the movement, a foreign anarchist known for his subversive theories who travelled around the country holding meetings, a bank robbed.

Christos has taken full responsibility for the act, caused by economic problems, denying Alfredo’s involvement. But, clearly, the judge didn’t believe him. So both are still in jail. The first, because he dared to reach out a hand toward wealth rather than resign himself to dying in misery. What’s more, he is an anarchist. The second, because... because... because maybe he helped his comrade. And, for sure, he is an anarchist. And that’s enough.

They are old things, from another century. Two anarchists get arrested after a bank job. Outside, solidarity is organized. Funds start to be collected; initiatives are prepared. But that’s not all. In Athens, the two prisoners get explosive greetings from the group Conspiracy of the Cells of Fire, which had just disturbed the crowning of the new Greek premier. In Villejuif, France, someone renders their homage by smashing the windows of the local offices of the Socialist Party. One of the beauties of anarchy is that it doesn’t recognize borders. And in Italy? Bah, here it has been limited to communicating the news, faithfully and coldly reporting the journalists’ poisons. No comment. The drafters of daily virtual communiqués say nothing. The tenders of militant gardens fall silent. The little strategists of the new alliances hush up. The movement has now become a community, and anyone who doesn’t share its rules and language doesn’t exist. He is nameless. In the rush to follow the masses, have individuals been forgotten? Perhaps it’s better this way. Better a sincere silence, if in the face of such an act, one no longer knows what to say, than hypocritical chatter about solidarity. Let’s leave that to the Stalinist annoyances and other ruins. Or to a few third millennium fascists, who on one of their forums rendered “honor” to the two arrested anarchists.

They are old things, from another century. Two anarchists get arrested after a bank job. The first is 46-years-old, the second 72. Whether guilty or innocent, for them being anarchists doesn’t even have the excuse of being an infantile disorder of extremism. Stubborn as they are, they haven’t understood that now is the time to ride the wave of social movements, to defend who knows what in front of places of power, to act as social workers for the damned of the earth. No, they haven’t understood this. The dream that they have in their hearts is much too big to adapt itself to the tick-tock of modern times.

No pardon, no pity.

Good-bye, beautiful Lugano.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



No pardon, no pity.


Good-bye, beautiful Lugano.

