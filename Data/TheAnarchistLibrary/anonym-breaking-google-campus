      Why no Campus in Berlin Kreuzberg?      Anti-Google-Café face2face      Noise against Google      Neighborhood Presence      The Deed      Whitewashing      About Depth      What Remains
There will be no Google-Campus in Berlin Kreuzberg, for now. Although Google continues to work worldwide on infiltrating every life and on technologizing all social matters, preventing the campus is still a small success, a pinprick against one of the most powerful structures in the world. This may encourage people to defend themselves, not just put up with everything - even if Google seems all-powerful. But a pinprick is not a stab in the back and even a stab in the back does not reject all power relations.

This text will go into more detail on those initiatives that were based on the idea of informal self-determination. It is a search for moments of quality within the struggle from a perspective hostile to domination.

In order to find out why Google called off the project and what critical moments this struggle had, it is important to see what happened in two and a half years of fighting against the Google-Campus in Berlin.

After Google announced in the press, that they were planning to open a Start-Up-Campus in Kreuzberg, posters, fliers and graffiti soon appeared with an anarchist perspective. They called for an informal fight based on self-organization, individual initiative and without representation. The Anti-Google Café face2face at the anarchist library Kalabal!k soon became an open space for anyone aiming for non-reformist resistance. More about this later. Events and discussions took place, bringing together many different people came together. At the first public discussion, little notes were passed around, calling for an unregistered demo to the Umspannwerk. This was put into action right away. Texts and posters that were available so far were very uncompromising; they dealt with general relations of power, the criticism of technology, and the idea of self-organization. They did not carry a group name or fixed identity, and focused on a critique of technological domination. Soon there emerged a radical left-wing alliance, a more reformist (neighborhood) alliance, there was a lot of activity on the Internet and there were informal groups doing their thing. It was not necessary to agree on a common denominator and force everyone under this constraint. Everyone was able to act in their own way, which was also clearly noticeable in terms of both content and methodology. After all, the objectives were also different. While some wanted to prevent the Campus and displacement, others demanded the abolition of capitalism, while others defined revolt, revolution and insurrection and their corresponding methods as the ultimate goal.

This diversity, but also a certain diffusion, may have contributed to preventing the Campus. Since many texts, meetings, noise hours, posters, etc. could not be assigned to one group. It was often quite unclear who was doing what. Various small projects of those interested made things hard to oversee.

Two and a half years of defiling the company's image, worked. The issue of rising rents and displacement, as well as a critique of technology, of power and domination were present in the neighborhood and beyond. Security guards in front of the Umspannwerk, a hostile environment with the danger of attacks, do not match the company's open-minded social image.

To understand the power of this hostility, it is necessary to look at Google and its products. The products of Facebook, Google, Amazon & Co are all in all good for you, they make your life easier and they are your best friends or they enable you to have friends. They let you find anything, help you get by, pursue your interests and supposedly enable you to lead a social life. At the same time, they must always be positive, enhancing, practical, soothing, new and efficient. This alleged moral correctness, the good, the personal, stands in opposition to attack, unforgivingness, security guards and open hostility. I think this is what most affected Google: the relentless, essentially unforgiving chiseling away at their BFF image (More about Google in: "And the world shall become Google" at Kalabal!k or at theanarchistlibrary.org).

An important reference and meeting point was the Anti-Google-Café face2face. Posters, flyers, newspapers and event calendars were used to openly invite people to the Anti-Google-Café. There was no web presence, e-mail list or the like. The café was open to people who wanted to fight against the Campus in a non-reformist, self-determined way and without appealing to or negotiation with the state and those responsible. It was understood as a space of encounter and coordination, not as a political group or the like. This meant that no one could speak on its behalf and no one had to ask others for permission, or reach a consensus to put an idea into practice. There were some proposals that were discussed and sometimes implemented together. Others were set up by people who wanted to share certain projects. This made it possible to experiment and develop both individual and collective ideas, many of which were then continued elsewhere.

Since the café provided the only continuous open invitation to come together against the Google Campus, journalists often appeared as well. They came to consume the meeting, thus contradicting the idea of individual initiative. Following an explanation, they were always thrown out immediately. Some arranged to meet with them individually, while others refused to cooperate with the press and requested to think about how we can communicate with our environment. But the café was always a place for people who want to fight.

Naturally there were sometimes more, sometimes fewer people and there were some very good, in-depth discussions and coordination and occasionally (almost) nothing happened, or it was just horrible. Self-organization worked out better or worse at times. In any case, it was important that some people were there continuously, as many discussions kept coming up. It was sometimes very impressive the way some were inspired by the ideas, but sometimes also very exhausting to keep having similar discussions over and over again. It is a dilemma of a continuous open invitation: Some continue discussions over a longer period of time, while some "new" people may be offended and want to discuss basic issues or simply have a lot of questions. This is challenging, not always efficient, but also enriching and constantly bringing new insights for all involved.

In any case, relationships have developed at the café and many people have got to know each other and their ideas. Many have discovered something new. This open space was extremely important in order to overcome isolation and alienation from our environment, as well as to act informally, that is, on the basis of affinity, without formal structures. In addition, it was possible to develop diffuse practices where individual ideas expressed themselves. These were not necessarily based on the café, but on the relationships that had developed there.

After the campus has ended, the Post-Google-Café continues to meet. This meeting place against technological domination remains active.

Posters appeared in the streets around Umspannwerk in early 2018, calling for "Noise against Google" on every 1st Friday of the month. Contrary to usual Berlin habits, nothing was registeredofficially, i.e. there was no asking for permission. In the beginning, the cops didn't really know how to proceed. There wasn't any group or organization associated with the invitation, no official registrants, no speakers, and many of those present didn't belong to a specific scene. After a couple of rallies, the cops always tried to confine everyone to a certain spot on the opposite side of the street while filming everyone continuously. Subsequently, posters called for decentralization.

Although the reproducibility of the noise action is very simple, and most neighbors knew about it and many were clearly opposed to the Campus, only few participated. This could be due to the fact that it was unclear who was issuing the invitation. And instead of making the action their own, some people paid lip service. It is also possible, that not officially registering or repression by the cops might also have been reasons. But even the basic idea that this form of expression requires one's own action, might discourage some, and in turn encourage, others. It was not the aim to gather as many people as possible, even if it is important that many express themselves. The quality of not-asking-for-permission, and of self-initiative, was always very important. Self-determination and self-empowerment as a goal and means, as a contrast to domination and heteronomy.

Nonetheless, more noise was made, banners were held up and a lot of flyers were distributed to people passing by. There were also accompanying experiments, like attempting to walk on the street, fireworks in the area and on the building, large banners, and notes thrown from the roof all over the street. There was noise from boats on the adjoining canal, and lots of banners all over the neighborhood the day before. In an attempt to take the street, the cops chased one person through the area. A few days later, unforgiving posters appeared addressing the issue.

The noise could have had more impact, had it been more decentralized, more spread out and more in motion. The question also arises here: do I want to make noise at the building or communicate with the environment? While some of the "we-feeling" may be lost, scattered noise and more experiments using other methods and means, during the noise hour, could have gone further, and might have disrupted the framework that was later clearly controlled by the cops.

The noise hours continued even after the Campus plans expired, for example walking through the neighborhood to the old post office, where the Samwer brothers have set up companies to promote Blockchain technology, or at the opening of the Google office in Berlin-Mitte.

There were posters, newspapers, graffiti and leaflets all over the neighborhood and the surrounding areas. The Campus constantly being the subject of this massive presence. Thematically, this communication mostly referred to a critique of technological domination, while the formal alliances and groups mostly referred to displacement and sometimes to a critique of capitalism.

Many posters had no group name, often no identifying symbols and focused on the actual message. It cannot be verified, but during conversations on the street while distributing flyers, it became clear that the criticism of control and technology was heard and not just the fear of rising rents. A broad rejection of the Campus was also noticeable in the neighborhood. While handing out flyers, it often happened that people thanked you for fighting against the Campus or expressed how important this was. However, given the thousands of local residents, the conversion of their own discontent into action was relatively little, but sometimes intense as well.

The communication was focused on the direct surroundings and not on "scene locations" and many flyers and posters were written uncompromisingly but as clearly as possible. All initiatives had their own brochures on the topic, which were distributed by the thousands.

In addition, three editions of the anarchist newspaper "Shitstorm" with a circulation of 8000 copies were distributed in neighborhood mailboxes and in shops and pubs. The newspaper tried to deepen a criticism of domination and technologization by focusing on the specific project of the Google Campus and suggesting ideas for self-determined action (available at Kalabal!k or some articles at theanarchistlibrary.org).

In addition, there were registered protests and demonstrations by alliances against the Campus. Participation was rather low for Berlin and never deviated from legality. Again, it is surprising how few activists from the left and left-wing radicals actively participated.

At a panel discussion against the Google Campus, among others, a left-wing politician was also sitting on the stage. Not to mention the fact that panel discussions are usually focused on consumption instead of individual action, politics itself stands in contrast to any freedom, insofar as it decides in favor of others. Some people used this opportunity, stormed the stage with a banner ("Poltical solutions are never smart"), and left a flyer hostile to any politics. Many applauded. This action clarified differences between actors in the Anti-Google struggle, it linked different actors of domination, it aimed at self-determination instead of political representation. A few weeks later, the formal initiatives jointly published a letter in which they rejected cooperation with Google and also politics.

In addition to international press, there were also attempts by some to publicize the topic through their own channels on the Internet. For example, there was a wiki "fuckoffgoogle.de", an alternative search engine "search.fuckoffgoogle.net" and a lot of Twitter tweets. The Hashtag #GoogleCampus is since dominated by disputes around Google. Even if this was not the choice of the means of all participants and it was disputed which quality short messages or the use of digital media have at all, a constant online presence of the conflict was created. This has certainly increased pressure on Google. On fuckoffgoogle.de it was possible to publish own content, announcements and dates.

It is hard to say what this has achieved. A stronger international awareness was certainly reached and maybe also some who came to the face2face-Café to meet offline.

Time and again there were direct actions related to the struggle against the Campus.

The Umspannwerk was repeatedly spray-painted, once a garbage can was burning in the yard. Paint and stones were used with reference to the Campus Start-Up locations, Zalando and the Factory Campus in Mitte. Written statements regarding the torching of Telekom, Amazon and Deutsche Bahn vehicles as well as a Vodafone radio pylon and the destruction of an important Internet hub are refering to the technological attack and the fight against the Google Campus. After Google's refusal to set up a Campus, windows were destroyed at its new headquarters in Berlin-Mitte.

These are actions that were clearly visible or for which statements exist. Who knows what else people have decided and implemented for themselves?

In September 2018 the Campus construction site was occupied. Flyers were distributed in the surrounding area expressing only two demands: that there should be no Campus and that the space should be made available for a neighborhood meeting. Before the cops were able to vacate, people inside launched an outbreak and almost everyone was able to escape. The workers had finished work early.

All in all, however, it can be said that the fight fell short of its potential. Only the occupation had paralyzed the construction site for a short time. Apparently there were no concrete attacks on the infrastructure of the companies involved or on the construction site itself. Admittedly, the construction site was guarded around the clock, plainclothed cops in their vehicles were often seen in the area, but still there were opportunities. The focus of the struggle was therefore mainly on communication, the attack on ideas of governance and the circulation of methods of self-organization. Ideas needs practice and vice versa. This does not mean that everyone was just sitting around, but that the prevention of such a project will not always be as "easy" and that actions can expand the scope of action and show that more is possible than putting up posters. A conversation with a neighbor is no less valuable than an attack on a construction vehicle, but both are possible and sometimes necessary.

At a press conference, Google announced that the Campus would not be built, but they would still rent the rooms at Umspannwerk in Kreuzberg and instead make them available to social initiatives such as Karuna or Betterplace for five years. This is to be financed totally selflessly with about 15 million Euros. Considering last year's revenue of over 120 billion US dollars - peanuts. Google is now trying to clear its record. While Google acquires entire blocks of houses in the USA, causing displacement and homelessness, they support Karuna, an association that cares for the homeless in Berlin. As so often in capitalism, problems created by these companies themselves are smoothed over and pacified. The social initiatives that have entered into the pact with Google pay a damn high price. The price of lobbying, whitewashing, of getting bought. They are making Google reputable and legitimise their despicable actions. First of all, it doesn't matter what good or bad work those initiatives do. In any case, they promote a company and its ideas of total control, total capitalization of all areas of life. They are participating in the lie of Big Brother, who is supposedly just your best friend. So they're part of what Google is doing.

Can the ideas be found in the methodologies? Was it possible to communicate a critique of power? Was this struggle a step towards a general overthrow? These questions are essential if the goal is to create a completely different world, if the goal is to abolish the rule of human over human.

Attempts were made not only to fight Google, but also to fight the technological attack and, above all, to fight domination itself. Since technology is also only a tool of domination. This includes the communication of certain ideas and the use of methods consistent with those ideas. Many banners, posters, flyers, etc. identified systems of domination and exploitation as the problem, instead of just a campaigning for its own sake and at best being critical of capitalism. There was a continuous communication with the environment in various ways, always through reproducible means. This was done mainly with interested people, angry people and the neighbourhood, not so much with those who hang out at the scene pub. In the spirit of self-organization, labels and identity symbols were avoided. Anyone could make the statements their own. Meetings and actions should always be self-organized and should encourage individual initiative. There was also an attempt to be openly approachable via the Café face2face and to share information and knowledge as much as possible. We are always responsible for conveying our ideas ourselves. Thus many people rejected any cooperation with the press and instead used their own means of communication such as graffiti, a newspaper or a blog. Also, ideas should be reflected in deeds, and vice versa. There should be no hierarchization of means. Acts and ideas should inspire conflict. The rejection of politics as such usually led to a consciously non-legalistic approach, like not registering demos. These questions were addressed in what I think is a very important Shitstorm article ("How to fight the Google-Campus?" in Shitstorm #2 or at theanarchistlibrary.org).

All these ideas have found their way into this conflict and the actions of many individuals. Self-organization and individual initiative as the cornerstone of a free world were determining factors, they were discovered, acquired and shared.

It was not really possible to go beyond the neighborhood and a smaller scope, let alone to incite international action against Google. There were press releases worldwide and also events and discussions in German-speaking countries. Face2face-like meetings against Google&Co were also held in other places. Many knew about the conflict and were also interested in it, but a real expansion of it would probably have needed more exchange, travel, translation, direct action and reference to other struggles. However, we have heard from many countries that this fight against a giant, against the new face of domination, has been noticed and inspired similar projects.

To get used to not registering anything with the authorities, to do without moderation if possible, or to avoid identity group names and to organize according to affinity. Good experiences have been made with these methods and they have become understandable through practice.

Many relationships, experiences and ideas have survived the Campus. I think this is one of the greatest achievements of this fight, along with the experience that you can act for yourself. This is what remains and hopefully will stay with us in future struggles.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

