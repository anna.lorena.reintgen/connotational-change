
The following text is the translation of what Nikos Maziotis said to the court during his trial which took place on the 5th to 7th of July 1999 in Athens, Greece. He was convicted and given a 15-year prison sentence for ‘attempted explosion with danger for human lives’ and ‘possession of guns and explosives’ for his action of placing a bomb in the Ministry of Industry and Development on December 12, 97, in solidarity with the revolt of the villages in Strymonikos against the installation of a gold metallurgy by multinational company TVX GOLD. During the trial he again supported his choices politically, as he did from the beginning when he had sent a letter from prison with which he took responsibility of the action against the Ministry. Though he never accepted the charges the state was accusing him of, as revolutionary acts cannot be described in terms of the penal code. In that sense, this trial was not a typical procedure of convicting someone who pleads ‘guilty’ but it turned into a political confrontation as much between Nikos and his prosecutors, as between his comrades, anarchists and revolutionaries and the state and its mechanisms. This confrontation was strongly supported by the presence of comrades from Sardinia (Costantino Cavalleri), Italy (Alfredo Bonanno) and France (Hellyette Bess) who testified in the court in solidarity with Nikos and by the letters sent in support by the imprisoned militants of Action Directe, France, by the ABC of Barcelona and by other anarchist groups from Spain. All these together, along with the presence inside and outside the court of anarchist comrades and of course the speech of Nikos Maziotis against his prosecutors, gave a sense of the international struggle for freedom and of solidarity with all the people in revolt, with all prisoners captured in moments of the social and class war against the state and the capital.

First, I do not intend to pretend to be the ‘good guy’ here when I was forced to come. I will not apologize for anything, because I do not consider myself a criminal. I am a revolutionary. I have nothing to repent. I am proud of what I have done. The only thing I regret is the technical error that was made so the bomb didn’t explode, so that my fingerprint was found on it later and I ended up here. This is the only thing I repent.

You must keep in mind that although you are judges and sitting higher than me, many times the revolutionaries, and myself specifically, have judged you long before you judge me. We are in opposite camps, hostile camps.

The revolutionaries and revolutionary justice because I don’t believe that this court is justice, it’s the word justice in quotation marks- many times judge their enemies more mercilessly, when they get the chance to impose justice. I will begin from many years ago. We don’t have any crime of mine to judge here. On the contrary, we will talk about crimes, but not mine. We will talk about the crimes of the State, of its mechanisms, of justice and police crimes...

The biggest lie of all time is that the State is society. I think Nietzsche has also said that the State lies. We are opposed to the division of society into classes, we are against a separation between those who give orders and others who obey orders. This authoritarian structure penetrates the whole of society and it is this structure that we want to destroy. Either with peaceful or with violent means, even with guns. I have no problem with that.

I will contradict my brother who said before, that he didn’t want the guns in order to make war. They were for war. Maybe they were just kept there. But guns are for war, you don’t just have them to keep them at home. I might have kept them as they were, but they are to make war and I make war... The bomb in the ministry was an act of war.

Our purpose, within the anti-State and anti-capitalist struggle, is to connect ourselves with different social struggles. Our purpose when interfering in these struggles is also to attempt to make things reach the edge, which means to culminate with the conflict of these social parts with the State and the police. To urge the people fighting to surpass the institutional frames, the trade-unions, the local administrations and all these manipulators who are enemies of human freedom. Many comrades of mine, with their small forces, were engaged in such struggles. I will tell you about them more specifically. In 1989, in a struggle of environmental interest in the village of Aravissos, the residents of the area didn’t want their water sources to be exploited by the Water Company of Thessaloniki. They clashed with the police and the riot police, they burnt water pumps, they set fires and put up barricades. And some of our comrades from Thessaloniki took part in this struggle and they were even arrested....

Generally, wherever there are disturbances, there are conflicts we want to be in. To subvert things. For us, this is not a crime. In a real sense, these disturbances are the ‘popular sovereignty’ that professional politicians keep talking about. That’s where freedom is expressed...

Now let’s talk about the struggle of the people in Strymonikos. Long before I placed the bomb, other comrades had been to the villages, they had been talking with the people there, they had published a brochure about this revolt, about the clashes in October of 1996. But I will talk more specifically about the struggle in Strymonikos in a little while. First, I want to talk exclusively about the action.

To tell the truth, I was inspired to place this bomb for a specific reason: The people of the villages had surpassed the limits, by themselves. If it was a struggle inside institutional frames, in the way that trade unions and local administrations try to keep these struggles restricted, if it was confined in a mild, harmless and not dangerous protest, maybe I wouldn’t have done anything.

But the comrades up there in the villages -who are not anarchists of course, but I don’t care about that, they are citizens who also want their freedom- had surpassed every limit. They had conflicts with the police three times -in the 17th of October 1996, in the 25th of July ’97 and in November 9 ’98-, they had set fire to police cars and riot police vans, they had burnt machinery belonging to TVX, they had invaded in the mines of Olympiada and destroyed part of the installations. Some of them also became a sort of guerrilla. In the nights, they were going out with guns, shooting in the air to frighten the policemen. And I thought, these people are cool, they’ve gone even further than us. And then repression followed, especially in ’97 when marshal law was imposed in the area.

The Chief of Police in Halkidiki gave an order according to which all gatherings and demonstrations were forbidden. They also sent special police units and police tanks, which came in the streets for the first time since 1980. And now they were sending them out again there, in the villages of Halkidiki. So, I thought, we must do something here, in Athens. It is not possible that the others are under repression and we here staying passive.

The ministry of Industry and Development, in Papadiamadopoulou and Michalakopoulou streets, was one of the centers of this case. The struggle in Strymonikos was a struggle against ‘development’, against ‘modernization’ and all this crap they keep saying. What is hidden behind all these expressions is the profits of multinationals, the profits of ‘our own’ capitalists, Greek capitalists, the profits of states’ officials, of the Greek state, of the bureaucrats, of all those who take the money, of technical companies... There is no relation between this ‘development’ and ‘modernization’ they are talking about and the covering of popular needs. No relation at all. So, I placed a bomb.

The purpose was the one I said in the letter with which I took responsibility for the action. In the passage of February ’98 I say: in placing the explosive device my purpose was to send a double political message. Everything is political. Even if you use such means, the messages are political. War itself is a means of political pressure. In this case, this was also a political means, a political practice.

First of all, it was a message to the people of Strymonikos that ‘you are not alone, there are also others who may live 600 km away from you but they care’. Not for personal reasons... I don’t know anyone from there personally. Other comrades know people from there. I haven’t even been there. It was not my house that was threatened, but this is not the point.

Simply, my principle, and generally the principle of the anarchists and of other non-anarchist revolutionaries is that social freedom is one and inseparable. So, if freedom is partially offended, in essence it is offended as a whole. If their freedom is offended, mine is offended too. Their war will be my war, especially in an area where the ‘sovereign people’ -again an expression used by professional politicians- does not want what the state and the capital want: the gold metallurgy of TVX.

On the other hand, I have said that, OK, there would be some damage — I knew that. Yes, I had the intention to cause material damage. So, what damage would that be? On the windows, in that specific place, what kind of damage? Or outside the storehouse where I placed the bomb? According to me, the damages would be minimal. But even if they were more than minimal, for me it is not important at all. Because freedom can’t be compared with the material damages of some windows, of a state car or state-property. For me, the ministry is not an institution of common benefit as the charges say. Of state benefit yes, but not of any social benefit. However, even if the device did not explode, I sent my message...

I will refer a little to the technical aspects. Exactly because I am a social revolutionary, and when you say that it is like talking for the benefit of society. Not like. It is for the social benefit. As I have this principle I couldn’t harm any citizen. I could harm a policeman. I consider them my enemies. And you are my enemies too. I separate you. I make a clear class separation. On one hand we have those, on the other hand, we have the others. In this occasion though I didn’t intend to harm either the policeman who guarded the ministry or anybody else; and of course not a citizen.

The procedure that is used by groups or individuals, in general, is exactly this: you first place the bomb in your target and then you call to a newspaper. In that case, I called to ’Eleftherotypia’ and said: In half an hour a bomb will explode there. Exactly what is written in the evidence: In 30 minutes there will be an explosion in the Ministry of Industry and Development, for the case of TVX in Strymonikos. Whether the bomb exploded or not there was absolutely no danger for human lives. In case that it exploded, there would be only material damages. So, it would happen exactly as I intended. Objectively, if the device had exploded there was no chance of an accident, like exploding before or after the time given...

I want to refer more to what I call solidarity, to the motives that I had. What is this solidarity. I believe ...that human society was created, based on three components: solidarity, mutuality and helping each other. So, that’s what human freedom is based on. Any social group in struggle, in a different place and time, whether they are pupils or farmers or citizens of local societies, for me and for the anarchists these struggles are very important. It doesn’t have to do with whether I am a worker and identifying my interests with the interests of that class. If someone asks for a higher salary or has a trade-unionist demand for me it is not important. For me, solidarity means the unreserved acceptance and support with every means of the right that the people must have to determine their lives as they wish, not letting others decide in their place, like the State and the Capital do.

That means that in this specific case, of the struggle of Strymonikos but also in every social struggle, for me what counts mostly is that they are struggles through which the people want to determine their fate alone. And not having any police chief or state official or capitalist deciding what they should do. It is of secondary importance if they want or don’t want the factory, if the focal point of the struggle is environmental. The important thing is that they don’t want the factory because they don’t like something imposed to them with violence.

Concerning the matter of political violence now... From the very beginning they tried to present a case of ‘repulsive criminals’ and ‘terrorists’ who ‘placed ’blind’ bombs’.: something that doesn’t exist. If theoretically terrorism is exercising violence against citizens and an unarmed population, that definition applies exclusively for the State. Only the State attacks civilians, that’s what the repression mechanisms are for: the riot police, special repression police units, the army, special forces... Mechanisms that also rob the people. They finance armed professionals, policemen. Aren’t they trained to shoot real targets? Aren’t the riot police armed with chemical gas? To use them where? On citizens, in the demonstrations. So, only the State exercises violence against the citizens. I didn’t use any violence against any citizen. I will say exactly what terrorism is.

Terrorism is when occupations, demonstrations and strikes are being attacked. When the riot police attacked the pensioners who demonstrated outside Maximou four years ago...

Terrorism is when special police forces invade the Chemistry School and beat up anarchists and youth...

Terrorism is when citizens are murdered by the police in simple ’identification controls’...

Terrorism is when Ali Yumfraz, a Pomak from Vrilisia suburb of Athens, was arrested for being drunk and later was found dead in his cell in the police station...

Terrorism is this court, here. Every trial of a militant, every trial of a revolutionary is terrorism, a message of intimidation for society. I said it again in my statements yesterday, when you called me to say if I accept the charges, and I will repeat it. Because of my persecution being political, the message is clear: whoever fights against the State and the Capital will be penalized, criminalized and given the characterization of terrorist. The same for any solidarity to any social struggle: it will be penalized and crushed down. This is the message of this trial and by this sense it is terrorism. Terrorism against me, terrorism against the anarchists, terrorism against the people of Strymonikos, who are also receiving similar messages this period, as they have similar trials for their mobilizations. This is terrorism. The fact that I put a bomb as an action of solidarity is not terrorism. Because no citizen was harmed by this action.

What the state wants is to deal with everyone alone. You must have heard an expression that the prime minister Simitis is using a lot, talking about ‘social automatism’ whenever social reactions burst out.. He uses this expression in order to present these social reactions -the blockades in the streets, the squatting of public buildings and all the actions of this kind- as being in contrast with the interests of the rest of society. Something that is a total lie. It is just the tactics of ‘divide and rule’, which means ‘spread the discord to break solidarity’. Because solidarity is very important as anyone who is alone becomes an easy target. When a workers’ strike takes place and there is no solidarity it is easier for it to be attacked. They talk about a ‘minority’. This is the argument of the state, that it is ´a trade-unionist minority having retrogressive interests which turn against modernization, against development, against all the reforms’ and all that nonsense. Well, there hasn’t been one social part or social group that didn’t come up in conflict with the state, especially during the 90’s, and that hasn’t been faced with the argument that ‘you are just a minority’, that ‘your struggle is in contrast with the rest of society’s interests’. That is exactly what happened in all cases.... The same thing happened of course with the people of Strymonikos.

What is really being attacked is solidarity. And that’s what is also attacked, without any disguise, through my trial. The state wants to attack everyone alone. Because when it finds them together things are much more difficult.

Finally, I am not on trial because I placed a bomb, nor because I possessed three guns and ten kilograms of dynamite. After all, the army and the police have a lot more guns than me and they use them. The one can’t be compared with the other.

I have nothing else to say. The only thing I’ll say more is that no matter what sentence I am convicted with, because it is certain that I will be convicted, I am not going to repent for anything. I will remain who I am. I can also say that prison is always a school for a revolutionary. His ideas and the endurance of his soul are experienced. And if he surpasses this test he becomes stronger and believes more in those things for which he was brought to prison. I have nothing more to say.

I want to complete what I was telling the public prosecutor before, about terrorism on an international level. In reality, at this moment, the US is the global gendarme and terrorist, as the only great world power left. Which means it is the worst thing on earth. And according to our perception -as anarchists- the State, all the states and all the governments are antisocial, terrorist mechanisms, since they have organized armies, police, hired torturers. I also want to complete what I was saying about having two weights and two measures. For example, the US provides with weapons, finances and instigates every dictatorial regime all over the world. And in Greece as well. In Latin America, Chile, Argentina, Bolivia, Peru.... This is Terrorism. Terrorism is to arm dictators, to arm death squads in Argentina or in Bolivia in order to kill people of the Left, citizens, revolutionaries. Those who equip the death squads to torture, those are the terrorists. Terrorism is when they bombard Yugoslavia for ten days, killing civilians.

Excuse me, Mr. prosecutor, but the US are the ones who say who is a terrorist and who isn’t. Their State Department issues official directions, advising Greece about who is a terrorist. At this time, they place pressure on the Greek state to make an anti-terrorist law, a model of law which will criminalize those who fight, to make laws which are more draconian than those already existing....These are Terrorism.

The revolutionaries and the militants are not terrorists. Terrorists are the states themselves. And with this accusation, with this stigmatizing (of terrorism) all the states and governments try to criminalize the social revolutionaries and the militants inside their countries. The internal social enemy... In fact, the State, justice and the police face me also as this kind of enemy. As an internal social enemy. On the basis of the division I described before. That’s the way the state sees it. This is what is ventured in this trial. Public prosecutor: What do you have to oppose to the existent?

Social revolution. By any means necessary. It is generally proven, because I am well versed in Greek as well as in international social and political history, that no changes have ever come about, never did humanity achieve any progress -progress as I conceive it- through begging, praying or with words alone.

In the text I sent to claim the action, when I said that I placed the bomb, which was published in ‘Eleftherotypia’ newspaper, I said that the social elite, the mandarins of the capital, the bureaucrats, all these useless and parasitic people -that should disappear from the proscenium of history- they will never give up their privileges through a civilized discussion, through persuasion. I don’t want to have a discussion because you can’t have a discussion with that kind of people...

I would like to add something. Exactly because I have studied a lot, (I know that) during the events of July of ’65, a conservative congressman of the National Radical Union came out and said about those who went down to the streets and caused disturbances, when Petroulas was killed, that ‘democracy is not the red tramps but we, the participants in the parliament’, which means the congressmen who are well paid.

I will reverse that. Popular sovereignty, sir judges, is when molotov cocktails and stones are thrown at the police, when state cars, banks, shopping centers and luxury stores are burnt down.... This is how the people react. History itself has proven that this is the way people react. This is popular sovereignty. When Maziotis goes and places a bomb in the ministry of Industry and Development, in solidarity with the struggle of the people in Strymonikos. This is the real popular sovereignty and not what the Constitution says...

July 7, 1999.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



The following text is the translation of what Nikos Maziotis said to the court during his trial which took place on the 5th to 7th of July 1999 in Athens, Greece. He was convicted and given a 15-year prison sentence for ‘attempted explosion with danger for human lives’ and ‘possession of guns and explosives’ for his action of placing a bomb in the Ministry of Industry and Development on December 12, 97, in solidarity with the revolt of the villages in Strymonikos against the installation of a gold metallurgy by multinational company TVX GOLD. During the trial he again supported his choices politically, as he did from the beginning when he had sent a letter from prison with which he took responsibility of the action against the Ministry. Though he never accepted the charges the state was accusing him of, as revolutionary acts cannot be described in terms of the penal code. In that sense, this trial was not a typical procedure of convicting someone who pleads ‘guilty’ but it turned into a political confrontation as much between Nikos and his prosecutors, as between his comrades, anarchists and revolutionaries and the state and its mechanisms. This confrontation was strongly supported by the presence of comrades from Sardinia (Costantino Cavalleri), Italy (Alfredo Bonanno) and France (Hellyette Bess) who testified in the court in solidarity with Nikos and by the letters sent in support by the imprisoned militants of Action Directe, France, by the ABC of Barcelona and by other anarchist groups from Spain. All these together, along with the presence inside and outside the court of anarchist comrades and of course the speech of Nikos Maziotis against his prosecutors, gave a sense of the international struggle for freedom and of solidarity with all the people in revolt, with all prisoners captured in moments of the social and class war against the state and the capital.

