      A new world?      Which road to take?      Perspectives
Without precedents. This is the characteristic of the times we are living through full of wonder, anxiety, dismay, hope. Not to say that in the past history has not known wars, insurrections or plunging economies. However, with the sense of the later and with the proper amount of security distance, it has always been easy to pick out the different sides in play, their reasons and the influence of the protagonists on the unfolding of a chain of events. The last two centuries have provided us with the knowledge from which to draw, have engraved our certainties and our doubts, have laid out the guide that we use in our daily acts. But the third millenium opened immediately on a very unpredictable note.

On the morning of September 11, upon waking up, who would have thought that a few hours later the world would never be the same again? Ten years have passed since then which have repeatedly destroyed all our consolidated benchmarks one after the other. Until we come to today with one European country teetering between reaction and revolution (Greece), another one famous for its stolidness put to the sword (England), others on the verge of economic collapse (Italy, Spain, Portugal and Ireland), distant regimes that seemed eternal crumbling in a few weeks (Tunisia, Egypt, Lybia), others forced to survive a vicious repression against its own people (Syria); the worldwide super-power itself, the United States, master of this planet, finds itself dealing with a failing economy.

Not to even mention those wars that should have been brief, but that are still ongoing (Iraq and Afghanistan), of the conflicts that seemed to have died down, but that have revived (Israel and Palestine), mass migrations that wreak havoc (on one side and the other) on the way of life of millions of people, of the (un)natural disasters that determine not only important environmental shifts, but also political and social changes. Up until the present daily life, the one that we drag behind us day after day, dealing with lack of alienating work that is necessary for getting money that is not enough, in any case, to buy things that are not worth anything… everything contributes to spreading the consciousness that this present does not have a future.

The world as we know it, the only one of which we have had direct experience, is crumbling before our eyes. It is not important here to establish whether its downfall is the result of a poor administration of power or of social movements, whether its an old self-fulfilling prophecy or a surprising novelty. It even has little relevance to know whether it is real and material or just the latest virtual trick. It is certain that it is perceived, felt. And this, for those who want to turn this world upside down is nothing but good news. It is not necessary anymore to try to open the cracks in the wall of the consensus that structures social order: that wall is already falling to pieces. Nothing is the same as before. However the situation that has emerged, and that theoretically should only evoke enthusiasm on our side, is practically mostly bringing bewilderment. Born and raised in the last century, how can we becontemporary and topical? The language, the formulas of interpretation that we are used to, seem to be more and more useless and become obsolete. We are running the risk of becoming historical artifacts that will end up collecting dust in museums.

This is why a broadened confrontation is now more than ever necessary and urgent. Unimmaginable possibilities are opening up right in front of us. To be able to seize them we don’t need to learn the lesson of the day by heart, but nor do we need to just throw ourselves into pure chance, let alone make use of some vague ideological fashion. Meeting, discussing, exchanging your own ideas in view of… (yeah, in view of what?), becomes all the more vital.

We start thinking of some famous words of Buenaventura Durruti. We are not afraid of the ruins, because a new world is already being born in our hearts. So let’s start from there. In the old continent the collapse of this world tends to provoke reactions with nihilist or citizenist tones, this is because there is no new world in the heart of the human beings that are inhabiting it. In North Africa the rebels fought with courage and determinations, also because they still have a hope that animates them. We know that the myth of democracy is a lie and we repeat (ourselves) that in their mouths it is only an excuse to cause a ruckus.

Whether it’s a reason or an excuse, it’s pointless to deny the fact that they need that myth, that dream that pushes them to destroy what stands in the way of its realization. All revolutions have needed a dream powerful and intoxicating enough to excite the people and push them to action. And this dream has always been something other than the miserable concessions of the existent. The direct democracy invoked by the Enragés was unfathomable before 1789, as was the Commune before 1871, or the Soviet before 1917, or Collectivity before 1936…

But today, here in the west, what is the dream? The only utopia that stays untouched (even in a certain sense, as bad as it is to say out loud, also thanks to the defeat of the Spanish revolution) is anarchy, a world without power relations. Even so, even among anarchist we notice a certain reluctance to support it, an embarrassment of those who do not want to appear too impractical, too unrealistic. And furthermore to whom do we address ourselves? Carried by the irresistible push of technological development, the last decades have seen the erosion of all meaning, the distorision of all words, the generalization of aphasia. The Babylon of the free market is also the tower of Babel of the inability to communicate.

This has provoked the disapearence, not of the so-called social aspect, but more of its awareness. Today’s social struggles are not carried out by exploited that want to put an end to their exploitation (and unfortunately they still trust politicians ready to betray them) but of integrated citizens that only want a more authentic democracy. Meanwhile the revolts that suddenly explode in our corner of the world are usually empty of content, don’t formulate demands, don’t indicate prospects, are only explosions of rage. This tendency, very visible in Europe has pushed the biggest part of the anarchist movement to divide, and to take two apparently opposing roads, that in reality mirroring each other.

Once all the hope in our hearts has been subdued, the eyes of many comrades who don’t intend to resign themselves, a dry, brutal, inevitable alternative is being outlined. Either to give up any attempt to involve masses that show themselves to be more and more alienated and transform social war into a private war between anarchists and the State (armed struggleism). Or to pursue this involvement to the point that one adapts to the “dynamics” of the masses, taking over its demands and transforming social war into a contest between civil society and the state (citizenism). We can’t help making the observation that the starting point of these two roads is the same: the realization that the reality around us does not allow for a revolutionary intervention like the one practiced or even hoped for in the last century.

Let’s be clear, both of these hypothesis put forward answers to real, concrete needs, which were never called into question. It is just that the attempt to carve into the surrounding reality has been separated from the methods, so that the different ways of struggle are no longer complementary, but have polarized into two equally political alternatives: on the one hand an intentionally acritical participation in “popular struggles”, on the other hand the formation of a specific organization that claims various attacks against power. Now, it’s precicely the penetration of politics and its calculations into a movement that was hostile to them that is one of the main causes of the present-day “depression” of many comrades. And the more politics is revealed to be “winning”, thanks to an unscrupulous use of various self promotional tactics, the more one cannot do without it.

The anarcho-citizenism has managed to lure some comrades into certain mass situations, allowing them to obtain some visibility and approval… but at what price? As long as you give up being an anarchist, learn to disguise or silence your thoughts, to bear the unbearable. This is a “victory” which is unable to hide the dismal opportunism that made it possible in the first place, which succeeded in an achievement once unthinkable: making many comrades actually disdain the very idea of intervening in social struggle, intervention that is now considered synonymous with compromise. How surprising is this, after we have seen comrades organizing conferences with reformists and presenting lists of signatures to the authorities? Why should this be shocking, after we have seen them giving support to a heavier circulation of goods while scolding the self-professed pacifists for not properly doing their institutional duty? Why complain, after we have seen them working hand in hand with priests and stalinists? Not only that, but this strictly political interpretation of social struggle is passed off as a truth acquired through un indisputable historical experience. “sharing or State”- is the pathetic decree that is imposed these days to avoid facing problems.

Anyway, faced with the spread of rage, with the increasing outbursts of protests, with the opening of new prospects, it is absurd to deprive ourselves of the possibility to intervening in wider contexts only because we are deafened by the noisy marketing of some petty movement leaders. Therefore, instead of shuddering in the face of the inevitable limitations of social struggles, we should attempt to fight within them as well, being certain and making it clear that the social aspect of a struggle is enriched by its qualitative dimension, not its quanititative one. A few comrades who sabotage the building sites for the TAV, for example, are conducting a social struggle on their own terms, since the High Speed Trains are a problem that affects eveyone without distinction. Many comrades that demonstrated for the abolition of life sentences, to give another example, carry out a political struggle on someone else’s terms, since life inprison without the possibility of parole is a problem that concerns very few and that can only find a abolitionist solution on the legislative level.

Therefore, it’s not that we want to stay away from social struggles. We intend to stay away from the politicians that are infesting them, including anarchists.

Anarcho-armed-struggleism, on their hand, although it has been able to directly strike the enemy more often and with better results (like in Greece or in Latin America), tends to reduce social subversion to a purely military practice, a conflict between us and them. Look at the fact that most of these actions are a direct answer to a repressive operation. Instead of continuing and expanding the struggle against domination in all of its forms, this form of solidarity is reduced to the defense of your own little garden: anarchists attack the State that just arrested some comrades, the State reacts by arresting other anarchists, which then react by attacking the state, which then reacts by attacking other anarchists, who then… This creates a vicious circle which becomes even less enticing, especially when embellished by that sad retoric that praises martyrs and sacrifice.

For the majority of people it is not a struggle that aims at subverting an unbearable existence, but a duel between a few individual rebels and the State. The fact that this conflict sometimes ends up on the front page of newspapers does not make it interesting, but in any case it is perceived as a private affair and as such can only attract spectators. Also because, and this is the worst part, armed-struggleism turns the attack on structures and on those responsible for domination into a characteristic of specific organizations rather than of an entire movement. And in no way is this a natural choice. It is an arbitrary choice. As most of the history of the anarchist movement can prove, “propaganda by the deed” can very well be the work of an entire movement. This happens when the action stays anonymous, without anyone claiming its ownership. When an action does not belong to anyone specific, it can belong to everyone. But when you make the effort to claim it, to brand it with your mark, it is because you want to make it clear to the world that that action belongs to someone.

Despite appearences, citizenism and armed-struggleism look like and feed each other. The openness to compromises of the first and the closure of identity of the second, and vice versa. The citizenist who swears on his own radicality while holding hands with a politician is not that different from the armed-struggleist who swears on his own informality while building an organization with acronym and program. The first seeks consensus of the masses, and therefore does not disdain the microphones of journalists. The second disdains the masses, but looks for the flashes from the media. Both in their own way seek visibility.

We consider immensly more attractive a movement that is anonymous and informal- an autonomous anarchist movement, as it was once called before journalists and magistrates distorted it- which does not renounce its difference from the world that surrounds it. But which also does not renounce the possibility of subverting it, which does not accept the extinguishing of the flame in our hearts for the new world that is not afraid of the ruins. Utopia is the only antidote against citizenism and against nihilism. We live like guests, undesirable and undesired, in this old decrepit world. Its agony does not move us, we are inclined more than ever on speeding up its disappearence.

How many times do we need to see our dreams shattered before we stop dreaming? How many times do we need to feel our own trust shattered before we start distrusting everyone? How many times do we need to see our ideas renounced before we just settle for some ever-changing opinions? How many times do we need to have our thoughts banalized before we renounce to any form of communication? There are those who continue to ask themselves these questions, hoping in their own hearts to never find an answer. We do. Stubborn or just plain stupid, untimely or just late, we find it intolerable to sink into melancholy at the exact moment when new and fascinating possibilities are opening up.

But- we need to aknowledge this- it is not subversive propaganda, it is not the formation of a revolutionary organization that gets rebels to take to the streets. It is the misery, material and emotional, of this existence that we drag on in our daily lives. If that was true in the past, it is even moreso today, when over the hills we cannot even catch a glimpse of the sun of the new days, but rather the deep night of primal chaos. In the face of this darkness militants will continue to stay secluded in their own cloisters for fear of being taken for trivial scoundrels, while intellectuals will continue to question themselves on the crisis of representationalism. However there is nothing to condemn or praise about modern struggles, the ones which send our own habitual compasses out of whack.

Everything needs to be taken on.

For decades we have remained practically immobile in the stagnating waters of social pacification, waiting for the winds that might to carry us towards our respective destinatons. Our hopes and expectations have been disappointed, it is not just a breeze that is rising. On the horizon we can make out a black sky that promises only a storm. And now? What do we want to do? Do we lower the sails and throw down the anchor, determined to stay still because the risk of sinking is too high, or do we reinforce the ship and let loose the moorings?

The fact that the riots that spontaniously break out are limited by time and substance is a false problem. When they are, this is because of the absence of those who could contribute to prolonging them and raising them. Even when they are the discharge of the fever of a sick social body, the fact remains that they include the lowering of the immune defences able to facilitate the insurgence of the fatal infection that we hope for. Even if they are the short recess before a test, the fact remains that it is up to us to sabotage the school bell. And if those who take part in this without any revolutionary aspirations, but more out of rancor due to their social marginalization than out of the refusal of institutional integration, this has also very little importance.

What makes these uprisings desirable is the suspension of normality which they manage to impose, an indispensable premise for any attempt to transform reality. It is not about sharing the taste of those who fight against the police, nor of trying to anthropologize it, chasing it with sacred subversive texts in hand while going to the assault of vile merchandise. It’s about throwing oneself into the chaos that is being created- even if for banal reasons, even in a guided way- and attempting to shake up, stop, slow down and prevent any return to the predefined order. This means snatching precious time to experiment, propagate and consolidate the disorder of desires.

This is why, in light of the new hotbeds that are igniting and with the atmosphere that is breathing in Europe, it becomes more and more important for us not not let ourselves be found unprepared. Not planning our actions so as to protect ourselves against the unknown, nor searching for complicity where it cannot be found so that we end up becoming the unknowing social workers of our own destiny. Without guarantees, without certainty, without fear of what is undecipherable. However, in the eventuality, which is not even so far out, that a fire might break out under our house, it is best to have a more or less clear idea of where to go and what to do, while we keep examining how to do it and why.

«There is no organization that is above my individual freedom…

and in any case it is not my revolution when i can’t dance».




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

