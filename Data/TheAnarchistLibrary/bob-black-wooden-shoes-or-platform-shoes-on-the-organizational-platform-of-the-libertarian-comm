
Organisational Platform of the Libertarian Communists. By Nestor Makhno, Ida Mett, Pyotr Arshinov, Valevsky & Linsky. Dublin, Ireland: Workers’ Solidarity Movement, 1989.

It attests to the ideological bankruptcy of the organizational anarchists today that they should exhume (not resurrect) a manifesto which was already obsolete when promulgated in 1926. The Organizational Platform enjoys an imperishable permanence: untimely then, untimely now, untimely forever. Intended to persuade, it elicited attacks from almost every prominent anarchist of its time. Intended to organize, it provoked splits. Intended to restate the anarchist alternative to Marxism, it restated the Leninist alternative to anarchism. Intended to make history, it barely made it into the history books. Why read it today?

Precisely because, poor as it is, it has never been surpassed as a programmatic statement of organizationalist, workerist anarchism. Not that latter-day workies deserve to be saddled with archaism like the Platformist policy toward the peasantry, to which many words are devoted. But much of the rhetoric is familiar — so much so that the formulations in circulation apparently cannot be improved upon. The Platform may have had great influence on those who have not had great influence.

In language redolent of recent rantings against “lifestyle anarchism” — right down to the disparaging quotation marks — the Platform attributes the “chronic general disorganization” of anarchists to “the lovers of assertion of ‘self,’ [who,] solely with a view to personal pleasure, obstinately cling to the chaotic state of the anarchist movement.” The absence of organizational principles and practices is the “most important” reason why anarchism is weak (11). Most deplorable is the claim of a right “to manifest one’s ‘ego,’ without obligation to account for duties as regards the organization” (33). It is remarkable that, in 1926, these anarchists did not consider more important than any internal cause of weakness the kind of state repression they had all experienced, or the influence of the Communists who had defeated and exiled them, or even tendencies in capitalist development which eroded anarchism’s social bases. The Plaform is a triumph of ideology over experience.

No document of this type is complete — the Communist Manifesto is another specimen — unless it opens with some sweeping, categorical falsifications of history. Everybody knows it is not true that “all human history represents an uninterrupted chain of struggles waged by the working masses for their rights, liberty, and a better life” (14). During long stretches the “working masses” have been quiescent. At other times — including ours, in many places — the struggles have been confined to small numbers of militants. “In the history of human society this class struggle has always been the primary factor which determined the form and structure of these societies” (14). Maybe long, long ago in a galaxy far, far away ... Space does not permit listing all the societies of which this is not even colorably true (such as colonial America, or ancient Greece, or Anglo-Saxon England, or Tokugawa Japan, or ... )

What’s the point of these historical howlers, these proletarian pieties? To give the reader the feeling that if he should mix it up with class society, he is part of the primary determinant of history, even if, as usually happens, his efforts determine nothing.

Next, Makhno & Co. discuss how “the principle of enslavement and exploitation of the masses by violence constitutes the basis of modern society” (14) (only modern society?); they iterate many forms of institutional and ideological domination. So far, so good. The conclusion: “Analysis of modern society [“description” is more like it] leads to the conclusion that the only way to transform capitalist society into a society of free workers is the way of violent social revolution” (15). Huh? There’s a middle term missing, perhaps something like “if capitalist society is very strong, then it can only be overthrown by violent social revolution.” But other consequents are conceivable, e.g., “if capitalist society is very strong, resistance is futile, you will be assimilated,” or “if capitalist society is very strong, the only way to overthrow it is not to resist it on its own violent terrain.” Each is as dogmatic and unverifiable as the others.

Class struggle gave birth to the idea of anarchism, which came not — the comrades are very insistent — “from the abstract reflections of an intellectual or a philosopher” (15). This is of course untrue. Modern anarchism as something with a continuing history is the idea of Proudhon, who was as much an intellectual as he was a worker, and who was not engaged in class struggle or even thinking about it in 1840. “The outstanding anarchist thinkers, Bakunin, Kropotkin and others,” discovered the idea of anarchism in the masses (15–16) — an extraordinary feat of clairvoyance, since the masses had no idea the idea was theirs. If Bakunin got the idea of anarchism from the struggling masses, it took him long enough. Kropotkin got the idea from the Swiss workers in the Jura Federation, who got their anarchism from Bakunin. As he writes in his Memoirs, the egalitarianism — he doesn’t mention class struggle — more than anything else, won him over to anarchism.

A platform, like a catechism, cannot accommodate complexity, plurality or uncertainty. An idea must have a single origin and a single outcome. If the masses originate an idea then no individual does. If anarchism cannot be reduced to humanitarianism, then it is not a product of humanitarianism at all (16), and never mind if there have been real individuals (William Godwin, for instance) who arrived at anarchism by carrying their version of humanitarianism (in Godwin’s case, utilitarianism) to its logical conclusion.

After some acceptable if simplistic strictures upon democracy, the social democrats, and the Bolsheviks, the Platformists aver that, contrary to the Bolsheviks, “the labouring masses have inherent creative and constructive possibilities which are enormous” (19). But rather than let nature take its course, before the revolution the General Union of Anarchists (not to be confused with the Union of Egoists) are to prepare the masses for social revolution through “libertarian education” — but that is not sufficient (20). After all, if it were sufficient, there would be no need for the General Union of Anarchists.

The GUA is to organize the worker and peasant class “on the basis of production and consumption, penetrated by revolutionary anarchist positions” (20–21). This choice of words is either revealing or unfortunate. Organized “consumption” means cooperatives (20), but what organization around production means is surprisingly unclear for a workerist platform. The comrades are anti-syndicalist, although, with obvious insincerity, they profess to be agnostic about choosing between factory committees or workers’ soviets (their preference) and revolutionary trade-unions to organize production (24–25).

However, syndicalist unions are to be used as a means, “as one of the forms of the revolutionary workers movement” (25). Anarchists from GUA are supposed to turn the unions in a libertarian direction, something which even revolutionary syndicalists, having no “determining theory,” and dealing with ideologically diverse union members, cannot be counted on to accomplish. But isn’t that just more “libertarian education”? This much is clear, anarchists “must enter into revolutionary trade unions as an organized force, responsible to accomplish work in the union before [?] the general anarchist organization and orientated by the latter” (25). In other words, take over the organizations of others for your purposes, not theirs. Of course, it’s for their own good. This part of the Platform is not much use to contemporary organizers, since the revolutionary unions they are supposed to infiltrate nowhere exist, and even they must know better than to try to start some, since they never do.

Current interest in the Platform presumably focuses on the climactic “Organizational Section.” Having denounced at some length “all the minimum programmes of the socialist political parties” (22–24), in this section the authors state that their scheme “appears to be the minimum to which it is necessary and urgent to rally all the militants of the organized anarchist movement”! (32). Repeatedly the Platform requires that all the militants work toward creation of the General Union of Anarchists and undertake no revolutionary action not authorized by the organization. “The practice of acting on one’s personal responsibility should be decisively condemned and rejected” because revolution “is profoundly collective by nature” (32). Maybe in the endgame, but there has never been a revolution which was not prepared by various activities of individuals and groups (usually small). And, unless you count the Bolshevik coup d’etat, there has never been a revolution ordered and carried out by a vanguard organization. The Platform is unfathomable as an anarchist program except as a reaction to the anarchist defeat in Russia. The losers, brooding in exile (and in Makhno’s case, in his cups), fetishize unity precisely because it is always unattainable in their circumstances. Their hatred adulterated with envy, they long to turn the tables on the winners. They have to believe that they could have won — and maybe they could have, as their critic Voline believed — otherwise their sacrifices were meaningless. Significantly, their very first sentence invokes, in the religious sense of the word, “the heroism and innumerable sacrifices borne by the anarchists in the struggle for libertarian communism” (11).

“Theory represents the force which directs the activity of persons and organizations along a defined path towards a determined goal. Naturally it should be common to all the persons and organizations adhering to the General Union” (32). Naturally. The criticism of weapons having failed them, the Platformists take up the weapons of criticism. The organization dictates the ends and the means to “all the militants.” But theory is not to guide activity directly, as in the current “chaotic state of the anarchist movement” (11). Theoretician-leaders translate theory into commands. Am I exaggerating? The Union “requires each member to undertake fixed organization duties, and demands execution of communal decisions” (34). The Union prescribes common “tactical methods” for all (32). By rendering themselves uniform and predictable, the revolutionaries confer an immense advantage on their enemies. Taking “a firm line against irresponsible individualism” (30), the Union forfeits the benefits of responsible individualism.

The division between leaders and led is not confined to the “executive committee” at the top of the hierarchy (which the Platform calls “federalism”). “Every organization adhering to the Union represents a vital cell of the common organism. Every cell should have its secretariat, executing and guiding theoretically the political and technical work of the organization” (34). I am reminded of nothing so much as the famous frontispiece to Hobbes’ Leviathan, depicting a giant with the had of a king and a body consisting of swarms of little people. At exactly this point in history, the Fascists were expressing similar ideas in similar organismic metaphors. Notice that the secretariat both proposes and disposes. In its capacity as theoretical guide, it takes the initiative in transmitting and interpreting Union directives, and in its capacity as executive, it orders and supervises their implementation. The rank and file militants are only conduits.

The Workers’ Solidarity Movement edition, without so indicating, omits several interesting passes from the Platform which are quoted in Concerning the Platform for an Organization of Anarchists, a rebuttal by Voline and other Russian anarchists. For example, “We believe that decisions of the soviets will be carried out in society without decrees of coercion. But such decisions must be obligatory for everyone who has accepted them [how? how long?], and sanctions must be applied against those who reject them.” This is the state. Also, “there can be specific moments when the press, however well intentioned, will be controlled to an extent for the good of the revolution.” The critics ask: controlled by whom? They voice other objections, including objections to the defense of the revolution by a centralized regular army. Ten years later, the issue was posed in Spain between the revolutionary militias and the counter-revolutionary People’s Army.

Anticipating criticism, the Platformists sought to discount it in advance by attributing it to rabid individualists. “We foresee that several representatives of self-styled individualism and chaotic anarchism will attack us, foaming at the mouth, and accuse us of breaking anarchist principles” (13). Instead, they were attacked by the most prominent collectivist anarchists: Voline, Malatesta, Fabbri, Nettlau and Berkman. (With a similar if even cruder ploy, a recent convert to organizationalism, Bookchin, denounces his self-appointed enemies as individualists, although David Watson, John Zerzan, L. Susan Brown and the rest are, without exception, collectivists). The Platformists are testy about accusations that the Platform is “only one step away from bolshevism, a step that the authors of the Platform do not dare to take” (“Some Russian Anarchists”) — but the principal author, Arshinov, took that step, returning to Stalinist Russia in 1933, only to be liquidated in 1937 (9).

That the Organizational Platform is on its face a betrayal of anarchism is almost the least of its vices. It is fundamentally false in its historical method, positing an imaginal, vaguely defined revolutionary class as an eternal, immutable historical presence — not as something with real spatial or temporal coordinates, something repeatedly self-created but never in quite the same form or with exactly the same meaning. It calls for an organization so strongly predisposed to oligarchy that it might have been designed for that purpose. It offers a formula for victory conceived by losers. Above all, it contradictorily demands an organization at once inclusive and orthodox. It cannot command inclusion, but it can impose orthodoxy, and it clearly states that it will do so. The result is yet another sect. A project with the announced purpose of eliminating the confusing multiplicity of anarchist organizations only increases the multiplicity by adding one more.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



Organisational Platform of the Libertarian Communists. By Nestor Makhno, Ida Mett, Pyotr Arshinov, Valevsky & Linsky. Dublin, Ireland: Workers’ Solidarity Movement, 1989.

