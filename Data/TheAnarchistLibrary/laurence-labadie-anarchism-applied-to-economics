
Value is exchange equivalency of something measured in terms of another thing. The fundamental quality upon which value depends is utility in satisfying desire. In economics, utility doesn't mean the 'real' or 'actual' ability of a thing to accomplish or assist in accomplishing a result, but means the human estimate of the ability of a thing to satisfy desire. This estimate may be erroneous but it is in effect the measure of the desire for it. In economics, therefore, desire and utility may be considered convertible terms.

Now in procuring anything, there is a hardship to be overcome. Without this hardship nothing would possess value for no one would exchange one thing for another thing which could be had without effort. So two factors are necessary in order for a thing to have value, desire and effort to be overcome, - utility and labor. Value may be enhanced by stimulating desire or by creating an artificial hindrance to production thereby affecting the equalizing forces of the law of supply and demand under competition.

Now presupposing effort to be necessary for the acquirement of two things of exchange, will they not be exchanged of the basis of equal effort? Not necessarily, for if A can produce one thing with effort of 10 and another thing for effort of 20, and the measure of effort for B to produce the things is in inverse ration, it will be to the advantage of both to produce and exchange in any ration between the limit of which means a decreased effort for both parties. If A gains 10 times as much as B it is still to B's advantage to exchange as long as he gains, because of reduced effort, in acquiring what he ultimately wants. The actual ratio of exchange would be determined by psychological and material conditions.

But when producers increase in numbers there arises competition in offering articles in exchange to benefit by the decrease of effort due to the division of labor. And presupposing enough producers of each commodity to satisfy the respective demands for them, competition will tend to make them exchange on a basis of labor time or effort necessary to overcome the obstacle of production.

For, should the demand for any article be more than the supply of this article offered for exchange, the probability is that a rise in the price or value will ensue. And presupposing a number of marginal workers, that is, producers whose aptitude is producing different articles is approximately equal, there will be an influx of capital and labor into production of the article which as increased in exchange value.

So it may be said that, granting free competition, that is, free and equal access to the means of production, to the raw materials, and to an unrestricted market, the price of all articles will always tend to be measured by the effort necessary for their production. In other words, labor as factor in measuring value will become predominant.

Should there be any restrictions, however, to these phases necessary to free competition, the desire or utility factor will tend to become more prominent as a factor in the exchange value of those things to which artificial hindrances to production have been applied.

From the Anarchist standpoint, these artificial hindrances which are the cause of three main forms of usury - interest, profit, and rent, are, in the order of their importance, monopoly in the control of the circulating medium - money and credit private property in land not based on occupancy and use, patent rights and copyrights, and tariffs.

It is also the claim of anarchists that government and States are involuntary and invasive institutions originated and maintained for the purpose of protecting and enforcing antisocial rights. They claim that the very first act of governments, the compulsory payment of taxes, is not only a denial of the right of the individual to determine what he shall buy and how much he shall choose to offer, but is nothing more than adding insult to injury when the very money extorted from him should be used to his disadvantage. They therefore attempt to instruct people in the belief that government, whether it be the rule of the mass by a few or of the minority by the majority, is both tyrannical and unjust, that any form of ruler-ship is bound to redound to the detriment of the ruled.

How the government protects the privileges by which usurious exploitation is made possible is easily seen upon investigation. Money interest is due to the privilege attributed to a certain kind of wealth, gold to be used as a basis for the reissuance of money, thereby putting the control of the monetizing of other kinds of credit indirectly into the hands of those holding this kind of wealth. Interest, therefore, is simply a royalty paid to the privileged class for the right to monetize one's credit. And the rate of interest on money fixes the rate of interest on all other capital the production of which is subject to competition. The rate of interest is an index to the 'use value' of money and bears no relation to the labor cost of furnishing money because competition in the right to monetize wealth has been restricted to the holders of a certain kind of wealth.

Interest is nothing more than a tax and like all taxes is prohibitory in nature. In all productive enterprises as in all individuals there are grades of efficiency. Because of this slight inequality of natural abilities and on account of previous exploitation there have developed individuals and combinations possessing different aggregations of wealth. Now let us see how it is that the rate of interest on money determines rate of interest (i.e. capital returns or that portion of profit not due to increased efficiency) on all other capital the production of which is subject to a competitive supply. By the latter is meant buildings, machinery, and products such as groceries, clothing, hardware, amusements, etc. The larger producer of these things is fortunate enough to own the capital he employs while the smaller producer finds it necessary to monetize some of his wealth, that is to use his credit, in order to produce on a scale commensurable to reap some of the benefits of a larger scale of production. Now he has, in addition to the unhampered natural cost of production, an additional cost which is payment for the allowance of monetizing his wealth. As the price which both producers get for their goods is the same, it is evident that the producer who is not indebted for any of his capital raps a profit equal to rate of interest plus that which is due to increased efficiency or to the decreasing cost due to large scale production. A similar occurrence obtains that in all things subject to competitive supply. Interest, by far the most potent force for the acquisition of unearned income, continually squeezes out the little fellow and causes vast amounts of wealth to accumulate into fewer hands. Without it, all great enterprises could not be accomplished except by the joint subscription and cooperation of a large group of persons. The Anarchist position for the abolition of interest is the repudiation of all laws prohibiting mutual banks and the abolition of all restrictions to free trade.

Rent is the tribute paid by the non-owning users of land to the non-using owner. It is quite evident that ownership in and by itself cannot and does not produce anything. It is only the use of land and things, only by labor, that anything can be produced. Therefore he anarchist denies the right of ownership of land if that ownership is not based on occupancy and use of land. No one should be allowed to hold land out of use because it is a denial of the first requisite of Anarchism, the equality of opportunity.

The other restrictions to free production and distribution are patents, copyrights, and tariffs. Anarchists deny the right of property in ideas or processes, and deny that any individual or combinations of individuals shall be restricted in exchanging their products when and where they please. They claim that all restrictions are in form of a tax and that all taxes are ultimately paid by the consumer ans insofar as the consumer is at the same time a producer, if the producer is not at the same time an owner, exploitation naturally ensues,

This concise statement of the position of the anarchist should be evident and even trite to any reflective person. While Anarchism is, in one sense, not a constructed philosophy, that is, not a "system", anarchists stand firm "constructively" in the position above stated. What form voluntary associations which anarchists contemplate will take, remains for the future to evince. Anarchism primarily, is not an economic arrangement but a social philosophy based upon the conclusion that man is happy and independent in proportion to the freedom he experiences and can maintain.

In a world where inequality of ability is inevitable, anarchists do not sanction any attempt to produce equality by artificial or authoritarian means. The only equality they posit and will strive their utmost to defend is the equality of opportunity. This necessitates the maximum amount of freedom for each individual. This will not necessarily result in equality of incomes or of wealth but will result in returns proportionate to service rendered. Free competition will see to that. To base society on the supposition "that the laborer of great capacity will content himself, in favor of the weak with half his wages, furnish his services gratuitously, and produce for that abstraction called society," int the words of Proudhon," is to base society on a sentiment, I do not say beyond the reach of man, but one which erected systematically into principle, is only a false virtue, a dangerous hypocrisy." A hypocrisy, unfortunately, eagerly subscribed to by a weak, downtrodden, and misguided proportion of the populace.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

