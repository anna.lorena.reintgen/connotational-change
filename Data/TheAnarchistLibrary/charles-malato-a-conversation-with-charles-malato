      London, February 27.  From our Special Envoy.      The Clubs      At Malato’s      1871–1894      The Movement      The Finer Points      A Legal Crime
After Mecca, Jerusalem and Rome—London!  Today London has become an anarchist holy city.  Here the militant “companions” have come to fraternize in their miserable exile, thrown out or chased off the continent.  Today there are around a thousand of them on the shores of the Thames, mostly French, Italian and Czech.  Next come the Swiss and Belgians.  As for the Spaniards, they are represented by only two or three revolutionaries, which might seem surprising given the hunt for anarchists that has taken place on the Iberian Peninsula over the past four months.  Finally there are many Russian Jews.

The most remarkable of the Slavs is indisputably Prince Kropotkine, a first rate philosopher and scholar but who lives very secluded.  He lives in Acton, a London suburb, with his wife and daughter Sacha, in a modest little house that he hardly ever leaves, really only to go work in the library of the British Museum.

Stepniak, once a famous terrorist, devotes himself now to propaganda by the pen for Free Russia, a paper sent clandestinely into Russia.  However, he is not an anarchist but rather a social democrat.

Finally it would not be right to forget the… English anarchists, even though they are, as their counterparts on the continent admit, more like theologians than dynamiters.  They form two groups that each takes their name from the newspaper they publish:  Freedom and Commonwealth.  Their main orators are Mowbray, Samuels and Nicholl.

The Autonomy club, which now has the honor of being famous, was founded by the German companions a dozen years ago.  It consists of a long, narrow hall used for conferences and balls, a bar whose walls are plastered with revolutionary posters, and a few other rather cramped rooms.  Originally created on Whitfield Street, it has since been transferred close by to Windmill Street, right near the main thoroughfare of Tottenham Court Road.  It is smack in the middle of the French quarter and has as many French regulars as German.  It is entirely anarchist.

The Graiton Hall club, which is just as famous of late, is independent socialist.  More fashionable and especially bigger, it counts among its members not only revolutionaries of different stripes, but also the dandies that come on Saturdays, Sundays and Monday evenings to enjoy the two-step waltz.  The grand ballroom that is used for both the choreographic entertainment and for public meetings can hold 1,200 people.  The walls are decorated with that call that ends Karl Marx’ famous manifesto, written in every European language:  “Workers of all countries, Unite!”

It is, however, in another club, the one on Tottenham Street, where the real gospel of Marx is preached.  The old friend of the German sociologist, Frederic Engels, who has lived in London for a number of years, graces it with his presence.  On Tottenham Street the club is completely authoritarian communist.

After a little research, I went to see an anarchist considered by his friends to be as far from the staunch individualists as he is from the pompous theoreticians.

Monsieur Charles Malato, whom we met once in the Pavillon of the Princes in Sainte-Pélagie [a Parisian prison], is a writer for the cause with virulent ideas but not a very savage appearance.  He did not, however, welcome me with open arms.

“Are you annoyed that I asked you to talk to me, to tell me something that a militant can honestly say, about the anarchist movement in London?”

“But,” he answered, “you have here a Mr. Melville, inspector at Scotland Yard (the police department), who’ll be able to accommodate you better than I.  You’re wonderful!  Your newspapers treat me everyday like a wild madman, a coward and bandit, demanding that they deport me or exterminate me and then you come here, sweet as can be, to ask for an interview.”

“Come now, my dear colleague,” (Monsieur Malato publishes books and articles), “don’t be unfair.  Not all the journalists, even the bourgeois, deserve to be bombed.  When you came to Paris five weeks ago, didn’t one of us recognize you and keep it secret in good faith?”

This ad hominem argument seemed to affect the revolutionary publicist.  After a moment of reflection he responded, “Well, so be it!  Even though we’re at war, speak first, Monsieur Bourgeois.  If I can, in good conscience, answer you, I will, but solely my own ideas, as is the custom among us other anarchists.  After all, it doesn’t bother me to tell you what I think, which is the same as many friends, about [Auguste] Vaillant and [Emile] Henry.”

“They say you went to Italy to take part in the revolutionary movement.”

“Well now, I’ve come back so I have no reason to hide it or, really, to get any misplaced glory out of it, because despite the courage and self-sacrifice of the rebels, the movement was crushed.”

“What kind of anarchist movement was it, republican, authoritarian?  Were there leaders?”

“A revolution is made with all kinds of elements.  For example, the movement in Sicily was due to economic causes, to poverty.  In Lunigiana, it was purely anarchist.  The staunch revolutionary accepts the battle under all circumstances, be it alone or with ten thousand men.  For me, I went down there alone, on the strength of my will alone, being a soldier not for a man or for a committee but for the Revolution.  I fully admit that the on-the-spot organization was a little like the Garibaldian bands under enemy fire, but I don’t much believe in massive organizations prepared over a long period that go rusty and derail when the day comes.”

“Did you believe in success when you went Italy?”

“I believe that the triumph will not come by itself, that it has to be won.  Anyway, I consider it a duty for those who urge others into combat to go and risk their own hide sometimes.  The triumph of the Italian Revolution, which would have given them, at the very least, a largely social republic, would have been an invaluable benefit from the economic standpoint: the emancipation of twenty five million alpine proletariats, the end of their economic rivalries with the French proletariats.  From the political standpoint: breaking up the Triple Alliance [Italy, German, Austria-Hungary] and a guaranteed European peace—the working masses of France and Italy don’t really want war.  So, I stopped correcting the proofs of my book De la Commune à l’Anarchie [From the Commune to Anarchy], which is why, sorry, the reader will find some mistakes in it—which irks me a little.”

I noticed a pile of books with oxblood covers.  I thumbed through one and was surprised to see the following note printed:

“The author and editor reserve all rights of translation and reproduction in all countries including Sweden and Norway.  This book has been registered with the Ministry of Interior (publishing section) in February 1894.”

“Well, well, is this really anarchist?”

“For the reproduction I’m bound by my contract with the publisher, Monsieur Stock.  As for sending the book to the Ministry of Interior, he did it himself while I was wandering around in the mountains of Italy and, from the point of view of the publisher, he had the very ingenious idea that I, being an anarchist, would never have come up with.  I think that now the government won’t be able to seize my book like it did with [Jean] Grave’s Societé mourante et l’Anarchie [Moribund Society and Anarchy].

We talked again about the current anarchist movement and I asked Malato if he considered it collective or individualist.

“Both,” he answered.  “It’s wrong for some rather intolerant friends to want to force people to do things they don’t want to do.  There have always been temperaments made for isolated actions and others for methodic, collective actions.  The Revolution needs both and I figure that the plasticity of the anarchists is what’s allowed them so far to resist the shocks that have crushed every compact, centralized organization.  Nevertheless, there are times when we must act together.  Individual action can’t be the answer to everything.  I think it makes more of an impression than some collective movements, but it’s only by collective revolt that the bourgeois society will collapse.  Both modes of action are compatible with the anarchist idea.  You see now, at the same time, Vaillant’s isolated action and the revolt of the whole Carrara population in Italy.

“What’s your opinion of Vaillant’s act?”

“For this noble-hearted man whom I once had the honor of knowing, I have nothing but admiration and, even if it might seem excessive coming from the mouth of an anarchist, respect.”

“Hell, you’re going too far.”

“How’s that?  I thought that the bourgeois republicans admired regicide like, for example, the one in 1793 from which they benefited greatly.  Well, the proletariat Vaillant, by throwing a bomb at the ‘kings of the Republic’ committed an act of regicide.”

“And you also admire Emile Henry’s action?”

“Not at all.”  His answer was curt.  He continued, “I say what I think and not being a moderate, a simple theoretician, like they’ve sometimes said, not having thrown a stone at Ravachol, unreservedly admiring [Paulino] Pallas and Vaillant, I believe, without being guilty of spinelessness, that I can confess my absolute lack of enthusiasm for an action of this sort.  I complete agree with Octave Mirbeau:  The act of Emile Henry, even though he is a highly intelligent and highly courageous anarchist, has, more than anything, been a blow to anarchy.  It was savage to attack a collaborator who, even if it was a mistake, almost killed him.  But I can’t help thinking that he could have done better.  I approve all violence that targets the obstacle, that strikes at the enemy, not that strikes blindly.  A crowd is unconscious, often even brutal and hateful, I know that very well, but whose fault is that?  The masters who keep it in ignorance and secular submission.  If we are really what we have always claimed to be, friends of the masses, and not neurotic decadents, we ought to be throwing at the crowd, which has the right to go a café, not dynamite but ideas.”

“This said, I will add that to guillotine Emile Henry would be a crime and my reasoning is not sentimental but purely scientific.  Emile Henry, whom I knew rather closely, who had a remarkable education and has a great deal of intelligence, suffered more than others from the influence of his background and his surroundings.  He was born in that Catalonia that is both fervent and tenacious, of a mother with a passionate imagination, almost extravagant.  His father, a member of the Commune, condemned to death, nursed him with stories of the Semaine Sanglante [The Bloody Week].  When he came to France, being cultivated and proud, he suffered from the scorn of his aristocratic relatives.  He buried himself in his studies with feverish enthusiasm and soon slipped into occultism.  The spiritualists got hold of his young imagination and made him participate in their séances until they finally subdued him, as a medium of incarnations.  I know this from him, from his experiences that disturbed his sensitive organism so much that they always ended in him fainting.   Seeing their hoaxes he left them, but the fatal blow had already been struck.

Emile Henry is proud.  He will march toward death with his head held high, forbidding his lawyer to call up any extenuating circumstances.  But if, instead of dealing with judges, whose job is to condemn, he were dealing with men of science, I wonder if they would dare deliver the young man to Deibler [the executioner].”

And with that it was over.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

