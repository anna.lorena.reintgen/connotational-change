
“I myself have never been able to find out what feminism is: I only know that people call me a feminist whenever I express sentiments that differentiate me from a doormat”

— Rebecca West, The Clarion 1913

Most people in the current anarchist milieu — female or male — would disagree, at least in principle, with most of the following statements: there are two immutable and natural categories under which all humans are classified: male and female. A male human being is a man, and a female human being is a woman. Women are inherently inferior to men. Men are smarter and stronger than women; women are more emotional and delicate. Women exist for the benefit of men. If a man demands sex from his wife, it is her duty to oblige him, whether she wants to or not. A man may force a woman to have sex with him, as long as he has a very good reason for making this demand. Humans are to be conceived of, in the universal sense, as male (“man”), and only referred to as female when one is speaking of particular individuals. Women are a form of property. To demand rights for women is tantamount to demanding rights for animals and just as absurd.

As ridiculous as most of these statements may seem, every one of them has been considered obvious and natural by most of the West at one point or another, and many are still more the rule than the exception to this day. If most of them seem a little strange, jarring, or just plain wrong, that is not because they contradict some vague notion of justice or common sense that we have all been born with. To the contrary, the change in attitude that allows most of us to claim a more enlightened, seemingly natural viewpoint, is actually the concrete result of an ongoing struggle which has claimed many reputations, relationships, and lives over the last 200 years and which, like all struggles for liberation, has been discredited, slandered, and marginalized since its inception. Although this struggle has been, and still is, strategically diverse and conceptually multifarious and hence hard to define, it is not hard to name: I am, of course, referring to feminism.

Feminism has changed our culture to the point where it is at least a common idea that women are fully human. If most people today claim to agree with this idea, this is not because society is becoming more benevolent, or evolving naturally into a more egalitarian state of affairs. Those who hold power do not simply decide to grant equal status to those who do not; rather, they only yield power when they are forced to. Women, like every other oppressed group, have had to take everything they have gotten, through an arduous process of struggle. To deny this struggle is to perpetuate a myth similar to that of the happy slave. Yet this is precisely what we do when we speak of feminism as somehow perpetuating a gender divide, or hindering our progress away from identity politics. Feminism did not create the conflict between genders: patriarchal society did. It is important not to forget that the aforementioned idea that women are fully human is not common sense but absolutely, emphatically, a feminist notion. To pay lip-service to women’s liberation while denying the historical struggle of women to achieve this for themselves is paternalistic and insulting.

Not only has Western society overtly relegated women to a subhuman role throughout its history, but, until recently, most liberatory movements have as well. This has often been partially unconscious, as a reflection of the mores of the dominant culture. Just as often, however, this has been fully conscious and intentional (cf. Stokely Charmichael’s famous quote that the “only position” for women in the Student Nonviolent Coordinating Commitee [SNCC] was “prone”). Either way, people who purported to be working for the emancipation of all humans were really just working for the emancipation of “man,” which until quite recently, is exactly how it was usually phrased.  Women who complained about this state of affairs were (and are) condescendingly told to wait until the more important struggle was won before they demanded their own liberation. This has been true of abolition, civil rights, the anti-war movement, the New Left, the anti-nuke movement, radical environmentalism and, obviously, anarchism. Women have been criticized for pursuing feminist aims as if these were wrong-headed, counterrevolutionary, or unimportant. Anarchists did not simply wake up one morning with more enlightened views of women, nor did patriarchy suddenly reveal itself as “just another form of domination.” Feminist theory and practice brought to light the oppression of women that often manifested itself in otherwise revolutionary milieus.

This is not to say that all feminists were/are not anarchists, or all anarchists were/are not feminists. But feminism is often criticized within the anarchist milieu, from several different angles. I will try to discuss the most common criticisms I have heard voiced, both publicly and privately, in anarchist circles. It has been suggested that feminism is essentialist. It has also been suggested that feminism, in keeping with its essentialist views, is a philosophy that asserts the superiority, in one way or another, of women to men. Finally, the charge has been made that feminism perpetuates gender categories, whereas the revolutionary task is to move beyond gender altogether. In other words, feminism is accused of being a kind of identity politics that perpetuates harmful and divisive societal roles that ultimately oppress everyone.

The one thing that all of these allegations have in common is that they posit a single, more or less univocal entity named “feminism.” However, anyone who studies feminism soon learns that there has always been a fair amount of diversity within feminist theory, and this has never been more true than it is now. No single set of ideas about sex and gender represents feminism; rather, feminism is a loose category that encompasses just about all forms of thought and action which are explicitly concerned with the liberation of women.

Although feminism has often been accused of essentialism, the critique of essentialism is particularly strong within feminism, and has been for quite some time. Essentialism is the idea that there is an unchanging substance or essence that constitutes the true identity of people and things. In this view, a woman is somehow truly, deep in her core, identifiable as a woman; being a woman is not simply the result of different attributes and behaviors.  This is seen as a politically backward stance by many, because it implies that people are limited to certain capabilities and behaviors that are somehow dictated by their nature.

When we examine the range of ideas that has emerged from second wave (post-1963 or so) feminism, however, a different picture comes into focus. Probably the most famous quote from The Second Sex, Simone de Beauvoir’s seminal 1940s work, is the following: “One is not born, but rather becomes, a woman.” The book goes on to argue that gender is a social category, which individuals can reject. The influence of The Second Sex was enormous, and Beauvoir wasn’t the only feminist to question the naturalness of the category of gender. Many feminist writers began to draw a distinction between sex and gender, asserting that the former describes the physical body, while the latter is a cultural category. For instance, having a penis pertains to sex, whereas how one dresses, and the social role one fills, pertains to gender.

This is a distinction that some feminists still make, but others have questioned the use of supposedly pre-cultural categories like sex altogether.  Colette Guillamin has suggested that sex (as well as race) is an arbitrary system of “marks” that has no natural status at all, but simply serves the interests of those who hold power. Although various physical differences exist between people, it is politically determined which ones are chosen as important or definitive. Although people are divided into supposedly natural categories on the basis of these marks, there is nothing natural about any category; categories are purely conceptual.

Building on the work of Beauvoir and Guillamin, among others, Monique Wittig has argued that the feminist goal is to eliminate sex and/or gender as a category entirely. Like the proletariat in Marx’s philosophy, women are to constitute themselves as a class for the sake of overthrowing the system that allows classes to exist. One is not born a woman, except in the same sense that one is born a proletarian: being a woman denotes a social position, and certain social practices, rather than an essence or true identity. The ultimate political goal of a woman, for Wittig, is to not be one. More recently, Judith Butler has predicated an entire theory of gender based on the radical rejection of essence.

Of course, there have been a number of feminists who, disturbed by what they saw as an assimilationist tendency in feminism, asserted a more positive notion of femininity that was, at times, undoubtedly essentialist. Susan Brownmiller, in her important book Against Our Wills, suggested that men may be genetically predisposed to rape, a notion that has been echoed by Andrea Dworkin. Marxist feminists like Shulamite Firestone sought the material basis of gender oppression in the female reproductive role, and several feminist theorists — Nancy Chodorow, Sherry Ortner, and Juliet Mitchell among others — have examined the role of motherhood in creating oppressive gender roles. “Woman-identified” feminists like Mary Daly embraced certain traditional notions of femininity and sought to give them a positive spin.  Although woman-identified feminists have, at times, taken essentialist positions, this brand of feminism has redressed some of the imbalances of that strain of feminist thought that rejects femininity altogether as a slave-identity. This has always been the dichotomy that has troubled feminist thinkers: either to assert a strong feminine identity and risk legitimizing traditional roles and providing fodder to those who employ the idea of a natural difference in order to oppress women, or to reject the role and the identity women have been given, and risk eliminating the very ground of a feminist critique. The task of contemporary feminism is to find a balance between viewpoints that risk, on the one hand, essentialism, and on the other the elimination of women as the subject of political struggle altogether.

The goal of feminism, then, is the liberation of women, but what that exactly means is open to dispute. For some feminists, this means that women and men will coexist equally; for others, that we will no longer see people as women and men. Feminism provides a rich panorama of views on gender problems. One thing all feminists can agree on, though, is that gender problems exist. Whether as a result of natural differences or cultural construction, people are oppressed on the basis of gender. To go beyond gender, this situation needs to be redressed; gender cannot simply be declared defunct. Feminism can perhaps be best defined as the attempt to get beyond the state of affairs where people are oppressed because of gender.  Thus, it is not possible to go beyond gender without feminism; the charge that feminism itself perpetuates gender categories is patently absurd.

Since anarchy is opposed to all forms of domination, anarchy without feminism is not anarchy at all. Since anarchy declares itself opposed to all archy, all rulership, true anarchy is by definition opposed to patriarchy, i.e. it is, by definition, feminist. But it is not enough to declare oneself opposed to all domination; one needs to try to understand domination in order to oppose it. Feminist authors should be read by all anarchists who consider themselves opposed to patriarchy. Feminist critiques are certainly just as relevant as books about government oppression. Ward Churchill’s excellent Agents of Repression is considered essential reading by many anarchists, even though Churchill is not an anarchist. Many feminist works, on the other hand, are neglected, even by those who pay lip service to feminism. Yet, while FBI repression is a real threat to anarchists, the way we inhabit our gender-roles must be dealt with every day of our lives. Thus, feminist literature is more relevant to the daily fight against oppression than much of the literature that anarchists read regularly.

If anarchism needs feminism, feminism certainly needs anarchism as well.  The failure of some radical feminist theorists to address domination beyond the narrow framework of women being victimized by men has prevented them from developing an adequate critique of oppression. As a prominent anarchist writer has correctly pointed out, a political agenda based on asking men to give up their privilege (as if that were even possible) is absurd. Feminists like Irigaray, MacKinnon and Dworkin advocate legislative reforms, without criticizing the oppressive nature of the state. Female separatism (particularly as enunciated by Marilyn Frye) is a practical, and perhaps necessary, strategy, but only within the framework of a larger society that is assumed to be stratified on the basis of gender. Feminism is truly radical when it seeks to eliminate the conditions that make gender oppression inevitable.

Anarchism and feminism clearly need one another. It is all well and good to say that once the primary source of oppression (whatever that is) is removed, all other oppressions will wither away, but what evidence is there for that? And how does that keep us from oppressing one another now, while we’re waiting for this great revolution? Conversely, it is important to recognize that the oppression of women is not the only oppression. Arguments about which forms of oppression are more important, or more primary, are unresolvable and silly. The value, and the danger, of anarchism is this; it seeks to eliminate all forms of domination. This goal is valuable because it does not lose sight of the forest for the trees, getting caught up in distracting reformist battles and forgetting its trajectory toward total liberation. But it is also dangerous because anarchism continually runs the risk of ignoring real-life situations in favor of abstractions, and underemphasizing or dismissing movements that seek to address specific issues. Let’s have an anarchist feminism and a feminist anarchism!




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



“I myself have never been able to find out what feminism is: I only know that people call me a feminist whenever I express sentiments that differentiate me from a doormat”


— Rebecca West, The Clarion 1913

