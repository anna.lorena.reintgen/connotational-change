
Godfrey Reggio could be described, simply, as a documentarian. However, his experimental, non-narrated films go far beyond the simplistic mode of information-based moving pictures. Instead of numbers, charts and equations we are presented with inscrutable human faces, immersed in the technological world through which they travel. Stunning natural oases of water and land barricade the ominous enormity of industrialism, which crashes and storms with the surges of Phillip Glass’ minimalist orchestral score. Challenging, but never high-minded, encompassing but never elitist, Reggio has finally concluded the Qatsi trilogy (Koyaanisqatsi, Powaqqatsi and Naqoyqatsi) with the theatrical release of Naqoyqatsi. Each film deals with, respectively, the perspectives as regards technology within the first world, the third world and the digital world, to be very brief.

Reggio has worked in a “non-ideological, mutual aid collective”, founded 33 years ago, that operated without wage labor and focused on living life creatively. Its members have managed to retain creative control over their films despite substantial contracts with MGM, which has released the Qatsi DVDs. He and his teams’ creative approach to cataloguing and debunking the industrial division of labor is unprecedented in the documentary tradition. Reggio’s work, in particular Koyaanisqatsi, is notable to Green Anarchists as one of the first films to question technology as a totality. In his own words, “The idea was to mainline in the vascular structure of the beast this form, which was created by technology, to question technology. In other words, these are not environmental films, these are films more about the presence of technology as a new and comprehensive host of life and three different points of view about it.” The current film, Naqoyqatsi, will finish its theatrical run on January 24 and arrive in a three-DVD set with the rest of the films in 2004. Reggio has no current plans to create films after the end of the Qatsi trilogy.

Sk!: Could you give us some brief background on your life in the context of what brought you to critiquing technological processes through film? What experiences, thoughts or words influenced your path?

Godfrey Reggio: Well, I think for all of us there’s a line, even though it’s quite crooked, that gives, as it were, some testament to who we are and what we do. In my case, I grew up in a very stratified society of New Orleans. At the age of 13/14, I decided to throw in the towel, that it was all too crazy, not so interesting. I was getting burnt out. At a young age, living in the fat as it were, I decided to go away and become a monk. So I left home. My parents were not too excited about that, and I stayed out for 14 years, having taken final vows as a Christian brother. In effect, got to live in the middle ages during the 1950s and learned crazy things, like the meaning of life is to give, not to receive, that we should be in the world but not of it. All these things I think, certainly influenced me. I’m very grateful for that highly disciplined, very rugged way of life, that would make the marine corp look like the boy scouts. So I think that had a big influence on me. During the course of that time, I saw a film called Los Olvidados (The Forgotten One) by Luis Bunuel, so The Young and the Damned, the first film he made in Mexico after being kicked out of Franco Spain. It was so moving to me that it was the equivalent of a spiritual experience. I was at that time working with street gangs. This film was about the street gangs in Mexico City, I was working with street gangs in Northern New Mexico. It moved me to the quick: it wasn’t entertainment, it was something that was an event that touched me and hundreds and hundreds of gang members that saw it. We bought a 16mm copy and I guess I’ve seen the film a couple hundred times. So that motivated me to look towards film as a medium of direct action. Now, film is usually not seen as that. I don’t see it as entertainment in my case, I hope it can be a vehicle for direct action. That’s how I became involved, it was also during that time that I had the good fortune to meet Ivan Illich, Illich was a priest at that time, I don’t know if you know who Illich is.

Sk!: I do.

GR: Ok, he’s just passed away by the way, December the 2nd. So I had the good fortune to become a confidant of his, at a young age I used to do my religious retreats in Mexico at his think tank. Got a great appreciation for, I guess, being sensitive to different points of view about what could be done for social change. His point of view was much more radical than, say, the radical left of the country, which was anti-war, pro-social justice, and included a good dose of socialism or communism. His radicalism was way beyond that; it was much more fundamental. It had to do with the very nature of society and institutions (not just who controls them, which is kind of the communist mantra). So I had the opportunity to be in the presence of a great teacher who was also a great activist. So I think those things impelled me to the position I’m in now.

Sk!: One of the influences you’ve noted at the end of Naqoyqatsi is Jacques Ellul, whose critique of technology is closely intertwined with a Christian theology. You, yourself, were once a Christian monk. Do you feel that a critique of the dominant technological order is effective in a religious context?

GR: Yes I do, now let’s talk a little bit about his critique. This was a man who was not accepted by either the organized religions of his day or the Left of France. He was persona non grata from the Left and the Right, much like Wilhelm Reich was persona non grata of the Left and Right of Germany. Here was a man who, more than any single individual, has contributed to our understanding of the nature of technology not as something we use but as something we live. For Jacques Ellul, technology is the new and comprehensive host of life, the new environment of life. The problem with that statement is that our language hasn’t caught up to the profundity of the thought, our language has become assumptive and no longer, in my opinion, describes the world in which we live. Ellul bore great criticism, if not persecution, for his ideas, from the Left as well as the Right, because like Ivan Illich, who made statements like “Freedom is the ability to say ‘no’ to technological necessity”, Jacques Ellul described our greatest act of freedom as to know that which controls our behavior. So both of these men were on very similar tracts, both of them were way outside the sphere of organized Right and Left, both of them were way to the Left of the Left. His ideas on the environment, you could call them Christian, but I wouldn’t. Certainly he was a theologian and he wrote many books on the word of god from his own point of view, but his stuff can certainly stand. His book for example, The Technological Society, his first book, 1949 I think it was really written and released here sometime in the mid 50s, that book is a solid philosophical, sociological text about the nature of technique. It’s light years beyond anything being written now. I think, if I’m not mistaken, the University of California at Berkeley has acquired the rights to his full library, all of his notes, his books, and they have in there a great gem.

Sk!: What was the impetus to initiate the Qatsi trilogy? What motivations brought you, a person not associated with film into the director’s chair?

GR: Street gangs for many years, as a brother. I became convinced that, while there are a few loonies that probably would hurt anybody under any condition, most people are good. I believe that; it’s my experience that most people are good, it’s not something I believe, it’s something I know. If you tell somebody they’re a shit, they’ll probably behave like a shit. If you tell somebody they’re great, they might achieve greatness. I think that’s the fragility of who we are. We live in a world not of this or that but this and that. So after working with street gangs for quite a long time, I realized that the context in which people of poverty have to try to work out how to live in this society is very cruel. I didn’t start this project to set up an institution that would live forever. It was a response to an immediate situation, and I left to pursue film as a form of direct action. Now by that I mean the following; since people are at the public trough of cinema, either through television or in the theater itself, I felt, what better place to put another idea out? Not in the form of language, but in the form of image and music. Let me explain that it’s not for lack of love for the language that my films have no words. It’s because of my, I guess, tragic thought that our language no longer describes the world in which we live. Through Ivan Illich, I had the good fortune to meet Paulo Freire, in Brazil, in São Paulo, before he passed on. I had a good time talking with him about this enormous book that he wrote, The Pedagogy of the Oppressed. In that, he says that the single most important thing a person can do is to begin to rename the world in which they live. This was his form of literacy, not teaching one how to read a book in the traditional sense, but to rename the world, because when you name something, you in effect create it. My own thought is that our language is bound with antique ideas, old formulas that no longer describe the moment in which we are. Therefore, that statement, A picture is worth a thousand words, I tried to take it and turn it on its head, and tried to give you a thousand pictures that can offer the power of one word. In the case of each of the three films, Koyaanisqatsi, Powaqqatsi, Naqoyqatsi, words that come from an illiterate source, a primal source, a wisdom that is beyond our ability to describe the world. A wisdom that says that all things we call normal are abnormal, all things that we call sane are insane. Now I realize that this is a pretty intense point of view, but that’s the point of view I ended up with from my own experience, not from academia but from being on the line in the ‘60s, trying to see the world from another point of view.

Sk!: The films were independently funded, avoiding governmental grant processes and industrialist handouts. You seem conscious of that old Marxist adage that the ideology closest to the means of production becomes the dominant ideology. Do you feel that you were able to avoid the constraints of capital influence in the Qatsi trilogy?

GR: Well, you know, it’s hard to say that. I wouldn’t want to exempt myself from anything, all of money is dirty money. Whether I got my money from an angel, and I don’t know how you get your money but it’s as dirty as the money I got. The events that I’m talking about are way beyond capitalism and communism (which is its flipside). Both of those ‘isms are much closer together than most people believe. They both share the same point of view about the instrumentality of life, the mass society, the industrialization of society, their only difference is who controls it. In the case of capitalists, it’s individuals who have accumulated wealth on the backs and the injustice of millions of people, literally. In the case of the soviets, it’s a new class of administrators, bureaucrats, who created a class, in my opinion, just as ironclad and unjust as the capitalist class. Both really want the same thing; they are just concerned about who controls the means of production. My question is not who controls the means of production, but the nature of production, as such. The question is not whether or not workers have an equitable pay and a healthy work environment, which is the interest of organized labor, or the Left that works with organized labor. The question, more profoundly, is, what is the effect of the automobile on society and should we have that in the first place? So, we’re dealing more with fundamental questions. It has become my experience, sadly, that human beings become their environment. We become what we see, what we hear, what we taste, what we touch. Anything that we do without question, in an altered state, we become that environment. If the environment that we live in today, as Ellul says, is a technological milieu or environment, if we no longer live with nature, and I’m not parenthetically talking about going back to teepees and caves etc, if our environment itself is technological, if we don’t use technology, if we live it, breathe it like the air that is ubiquitous around us, then we become that environment. In that sense, whether you’re communist, capitalist, socialist, primitive, an outsider, an artist, a revolutionary, if you live in this world, all of us doing that, we become this world. In that sense, all of us now are cyborged. Cyborg is not something for the future, it is already here. We live now in both worlds. The old world, the world that “nature” replaced, old nature, held its unity through the mystery of diversity. So there are many languages, many different environments to live in, there’s tropical, there’s semi-tropical, there’s mountain, there’s desert, there’s savannah, there’s salva, etc There’s not one flower, there’s uncountable flowers. Not one animal, a zillion of them, not one human being, many. The mantra of the old world was, Divided we stand. The new world, the technological order, holds its unity through a technological imperative. It creates unity through technological homogenization. Its mantra is United we stand. To me, this is the moment we’re in. We’re at that crossroads and the world is becoming homogenized; what we’re seeing is the Los Angelization of the planet through technology. My work has been, in effect, to try to shield my eyes from the blinding light of the new sun, technology, seeking the darkness, walking towards the positive value of negation. Trying to question the very structures, the very contexts in which we live, not who controls them.

We become what we see, what we hear, what we taste, what we smell, it’s so easily said but it’s a profound concept beyond the simplicity of the words that bear it. We live in an environment, as Ellul said, that is, in terms of a social event, the most enormous event of the last 5,000 years has gone unnoticed, the transitioning of old nature to new nature. Environmentalists don’t get it, most of environmentalism is how to make this madness safe. How to make cars safe, how to make industry safe, how to make electricity and war safe for the environment. We live in a time where we are like blind people, we don’t see the moment in which we are. We no longer use metaphor as our means of communion or communication (ie language). Metamorphosis is the form now, where the transformation, where the substance of something is changed, the transubstantiation of something is a metamorphic approach to communion rather than the metaphoric, which is the power of language. But language is disappearing. At the beginning of the 20th century, there were over 30,000 languages and principal dialects in the world. Today, with many more people, over double the number of people that were present then, we’re approaching 4,000 languages and principal dialects. In other words, as the earth is being eaten up by the voracious appetite of technology, everything that is local is disappearing. In that disappearance, language disappears and when language disappears, we are left with a more homogenized language to describe the world which, again, does not give us access to understanding. It produces more conformity.

Sk!: With Koyaanisqatsi you examined the first world in great detail, starting off from stunning wild lakes, through constricting cities, the faces of people, culminating in the destruction of the space shuttle Challenger. Throughout this film, technology is portrayed as an acceleratory, agglomerating, isolating and destructive force. Many critics would charge that it is merely the arrangement of technology or the puppeteer behind the scenes controlling technology that must be changed. Do you see hierarchy as endemic to these systems of control? Can we separate technology from domination?

GR: I don’t believe, I think it’s a pure myth, right, left, upsidedown, backward, to think that we control technology. I think that’s a joke. Technology is in the driver’s seat. I would go to the very radical writing of Mary Shelley, not the Hollywood version, but her original book Frankenstein, where we’ve empowered something that’s not in the organic realm, we’ve organized and allowed it to exist, and now it has its own life form. Now, that’s very hard for us to get our mind around, because we give ourselves more credit than we’re due. We think that our greatest attribute is our mind, actually our greatest attribute is what is our action, our act, what we do everyday. It’s what we’ve become. Marx has this great adage, I think Marx says, “Is it the behavior we have that determines our consciousness or is it the consciousness that we have that determines our behavior?” And of course the answer for 8 out of 7 people is that it’s the behavior that we involve ourselves in that determines our consciousness. The only way to avoid that is to do what Joseph Brodsky did, to become an outsider to society, all of us have to live in this world but we don’t have to be of it. Brodsky decided not to be of it. He became, for me, a revolutionary poet, though he’s not seen that way in the communist world. Stood outside, answered Marx’s questions. He said consciousness, or removing oneself, being in the world but not of it, would be a way of having your mind determine your behavior. So, the thing that I’m railing against, technology, is something I use. Some would say this is hypocritical or contradictory, let me agree with them, that it is contradictory. In the sense I’m trying to communicate, and wishing to do so in the contradiction of a mass culture, then I have to consciously adopt the tools of that culture or the language of that culture in order to communicate. So it’s the equivalent of fighting fire with fire. In that sense, I see the work that I do as direct action. Though I certainly use a very high-tech base, using that in order to make it available to raise questions about the very thing I’m using.

Sk!: The camerawork in city scenes throughout the trilogy often creates an industrial claustrophobia, giant buildings crowd the viewer into a confrontation with urban space as alienation. Living in the desert as long as you have, what are your impressions of urban civilization?

GR: Well I grew up in urban civilization, in New Orleans, then I came out to New Mexico which is one of the highest deserts in the world. Here, the sky... you don’t look at, you breathe it. I’ve lived here now 44 years, I consider myself fortunate to be out here, it’s like the Siberia of America. In this magnificent beauty is this enormous enigma, and the evil demon of nuclear technology that sits, as the crow flies, about fourteen miles from my window. So it’s a place of inscrutable beauty and unbelievable demonic energy. I’m sure that’s had an influence on me, being here, breathing the sky and having the presence of this monster. It allows me to have another point of view of the world in which I lived. When I shot Koyaanisqatsi with my collaborators, the way we did this film was eliminate all the foreground of what is a normal theatrical film, the plot, the characterization, the acting etc When you don’t have the foreground, what’s left is the second unit or background to the story. Stripping the film of all that foreground material, we take the background or second unit, and make that the foreground. So, in this case, the building becomes like an entity, the traffic becomes like an entity, something that has a life of itself. The whole purpose of this film was to try to see the ordinary, that which, let’s say, we are basted in. Being marinated in the environment that we live in, it all seems very familiar. And I was trying to show that that very thing that we call familiar is itself a techno-fascistic way of living. So I tried to see it from another point of view, I tried to see it as a life-form, albeit a non-organic life-form, that has a life absolutely independent of our own. Right now, the cities are made for the automobile, not for the people. When the automobile was brought in as a technology, they said it would just be a “faster horse,” it wouldn’t have any more effect than that. But we all know that’s ridiculous, we all know that we pay a hidden price for our pursuit of technological happiness and we call it, instead of war, we call it accident. But more people die in vehicular crashes than they do in war, if that’s even believable. So, it’s just the price we’re willing to pay for the pursuit of our technological happiness, and these films are about questioning that point of view.

Sk!: Powaqqatsi is defined at the end of the second film as “a way of life that consumes the life forces of other beings in order to further its own life.” Later you are quoted as saying that between the third world and the first world, Powaqqatsi captures “our unanimity as a global culture.” Now, the film portrays the third world from agriculture to commodity trading, bartering to industry—a narrative is constructed that seems to point the third world in the direction of increasingly intensified civilization. To what extent are the narratives of “development” (in the case of the WTO and IMF) and “history” (in the case of Marxism) negative factors in the lives of people in the third world? Since the definition of Powaqqatsi refers to a parasitic sorcerer, is it reasonable to characterize the first world as a parasite?

GR: My answer would be simply, yes. The whole point of view of Powaqqatsi is that through the dogma/religion of progress and development, which again, parenthetically, is not only a capitalist agenda but also a Marxist agenda that very paradigm consumes, and eats, and pulls out of the sockets people who live a handmade life. I was criticized when I made that film by Leftists in Germany, for romanticizing poverty, for trying to eliminate industrialization and, therefore, a better way of living. Well that’s in a point of view, if that’s how they see it, so be it, but that’s certainly not my intention. My intention was to say that standards of living are ephemeral. The standard of living of the world is based on first world norms, of consumption, of the institutionalization of life, of giving up your own control to the control of others. The very opposite is true in the so-called third world or Southern hemisphere, where really, the heritage of the earth exists not only in nature but in human development. Small, convivial, decentralized societies of handmade living, where things can be uniquely different, valley to valley, plain to plain. The world that we’re trying to force, through the IMF, etc, on the southern hemisphere, is a world of homogenized value. A world where Los Angeles, Jakarta, Hong Kong, the Philippines, etc, all look the same. This is in diametric opposition to the nature of the development of the South, which is disappearing right now because of the norms of development. The very founding, for example, of the United Nations, was on the dogma, on the theology, on the philosophy of promoting progress and development around the world as our guarantee for world peace. Now what crazier thought could you have? All of us buy in, in some way. Many people buy into the United Nations, but their very purpose is to produce this homogenizing event all over the world. For me this is the essence of techno-fascism, and it’s another example of how the Northern hemisphere is consuming, without question, the Southern hemisphere. The Northern hemisphere has consumed most of its own resources already, the Southern hemisphere is where the nature bank of our world still exists. If the north has its way, that will be consumed to create and further develop the technological order, which for me, is a fascistic venture.

Sk!: The latest film, Naqoyqatsi, has shifted the focus directly to digital technology and its violent consequences. What societal changes, observed in the bridge between Powaqqatsi and Naqoyqatsi, did you want to integrate into the new film?

GR: Here’s the thing, these films, early on, were conceived. It took years to realize them, but the idea was that Koyaanisqatsi would deal with the northern hemisphere (or in your terms, the first world). The hyper-industrial grids that we call societies. The second film deals with the southern hemisphere or what you might call the third world. Societies of simplicity, where unity is held through the mystery of diversity and how those societies are being consumed by the myth of progress and development. The third film, conceived early on as well, dealt with the globalized moment in which we live. How the world is being homogenized, how unity is being held together by the new divine, the computer. The new divine is the manufactured image, which is the subject of Naqoyqatsi and hence, the necessity of using digital technology to create it. In the case of Koyaanisqatsi and Powaqqatsi, we went to real locations to film them. In the case of Naqoyqatsi, we went to virtual locations to film them. We took stock and archival images that venerated familiar those things we have all grown up with through the myth of history, and we’ve taken and revivified them, or tortured them with a computer to create a manufactured image which is, as Baudrillard would call it, the evil demon of image. The purpose of image is to produce this monstrous, demonic conformity. Right now, image is more important than truth or reality. Look at the political spectrum, it’s all about the image of something. So this third film deals squarely with the image as its principal subject matter, the manufactured image in the globalization of the world. We spoke a bit about the computer, because it plays a central role as an entity in Naqoyqatsi. From my point of view, the computer is the new divine. When I say that, it portends supernatural powers. The computer is not just something we use again, it’s the very vehicle that’s remaking the world to its own image or likeness. If one were a Christian theologian or a Catholic theologian, the highest form of magic in the Catholic universe is the sacrament. The sacrament is different from a sign in that it produces what it signifies. Unlike a sign, like if one is married and wears a ring, that ring is a sign of your fidelity, of your union with your spouse. But it doesn’t produce it, it only reminds you or others that you’re married. In the case of a sacrament, the sacrament produces what it signifies. So if there was a sacrament of unity, it produces that unity, it’s the very highest form of magic. So I’m saying that the computer is the new sacramental magic, it produces what it signifies, it remakes the world to its own image and likeness. In that sense it is the very driving force of what I would call the techno-fascistic world. As the swastika was the image of fascism in the 20th century, and there were many other images as well but that one prevailed, the new image of techno-fascism is the blue planet. Not the reality of the earth, but the image of the blue planet. That, to me, is the ubiquitous image of techno-fascism.

Sk!: Notably, Naqoyqatsi’s framing definition is “civilized violence.” Never before in the series has the polemic been so searingly presented. Yet, throughout Naqoyqatsi, while high technology and digital life are critically examined, the film is ambiguous as to the fundamental disjunct that enables civilized violence. From a primitivist perspective, which views the rise of technology parallel to the rise of the division of labor, agriculture and symbolic culture, it seems like an incomplete critique. How do we undo technology, a force we breathe like oxygen, if we have no constructive alternative? Is it enough to present the case without suggesting a course of action?

GR: Well, first of all, let me say that if there’s a course of action that someone would recommend that would be right for anyone, that very rightness for everyone would make it fascistic. So anything universal for me is fascistic. I don’t pretend to have the answers, but I know that the question is the mother of the answer. Rather than presenting answers to people which I think is a fascist modus operandi, it’s much more important to present questions. The question becomes the mother of the answer. That which can change things more fundamentally than anything is the power of a community example. The power of a community in direct action or living an alternative. I’m not talking about utopias, I’m talking about a community in struggle, that wishes to present an alternative to the slavery to which we’ve all subjected ourselves through mass society. That would be a way out. If you look at it from a more comprehensive point of view, perhaps there is no exit from technology. This is, itself, a tragedy. On the other hand, I believe that there is no destiny that human beings cannot overcome. How that is done is up to the individual, it’s not up for any of us to give answers to others as to how to remake their world.

Sk!: Many civilized radicals find themselves weighed by guilt and alienated from cultures that civilization has domesticated. How did you, as a person born into American civilization, guide your participation in the lives of the Hopi? Why did you frame the discourse of all three movies in the context of Hopi prophecy?

GR: Well first of all let me say that I’m not a Hopi devotee, I don’t spend time over there. All of my contacts have died there. This film is not about Hopi, I am not trying to go back to a Hopi way of life, nor am I espousing that. We can’t go back to the teepee, we can’t go back to the cave. What I tried to do is simply take their point of view, because I found it laden with wisdom, I found that they understood our world better than we did. That doesn’t have to be the result of guilt, it has to be the result of coming in contact with someone that blows your mind with their perspicacity of thought. That’s what happened to me. It was music to my ears to hear David Menongue, an elder who was in his late 90s when I met him, say that everything that white people call normal we look at as abnormal. Everything white people call sane we look at as insane. Well that was music to my ears because that was exactly how I felt, they didn’t give me this idea, it was like confirmation. If you have a way-out idea and it’s so way-out that you think you might be nuts, which I thought for years, if you find some other people that actually have that same idea in another form, it confirms you. So I used it as a confirming. I also felt that their language has no cultural baggage, when you say Koyaanisqatsi, no one knows what that means, it sounds like, perhaps, a Japanese word. I’m taking that language, that doesn’t come from a literate form, it actually comes from an illiterate form, it’s a culture of morality. I’m taking the wisdom of that point of view to describe our world. Much like academics do in universities, they take their own subjective categories of intellectual pursuit and apply them to Indians through ethnographic studies, anthropology, etc. This is turning the tables, it’s taking the subjective content, or ideas, of Hopi, and applying it to white civilization. And that’s something that makes some people uncomfortable. That’s an easy way of getting out of seeing the value of other people’s cultures and contributions beyond your own.

Sk!: One thing that I noticed, after viewing all three movies, was the persistent image of the atom bomb mushroom cloud. Culturally we’ve seen that everywhere, you could almost say that’s a burnt-out image for a lot of people. And yet, in Naqoyqatsi, which just came out, you put it in again. Is that something you see as an endpoint?

GR: No, if it’s burnt out, it’s only because it’s been used so often. My whole thing in Naqoyqatsi was to take all of these burnt out images, images that we’re surrounded with, like the wallpaper of life which we call history, that great lie as it were, and re-examine those, put them in another context. So this film was a little more difficult than the other two, it’s taking our familiar, that which we’ve seen ad nauseam, and trying to put it in another context. Nuclear is something that, while we think we know something about, we have no idea of what it’s done to us. Much like television, something as ubiquitous as television, we have no idea of what it’s doing to us. Because we keep looking at it from the point of view of the subject matter that’s on the tube, rather than the technology, which is a cathode ray gun aimed directly at the viewer that probably changes our genetic structure and certainly puts us into a deep comatose state. I made a film called Evidence of Children Watching Television (and they were watching Dumbo actually, or they could have been watching anything, it didn’t really matter). Their eyes become fixated, their breathing slows down, automaticities take place on the face, slobbering comes out of the mouth; these kids are on drugs heavier than Prozac just by having the television on. It’s the same thing with nuclear technology, we think it’s just something that we control, that if we had a “Nuclear Test-ban” treaty, everything would be fine. The nuclear war has already occurred, all during the 50s. We doubled the background radiation of the planet, it’s affected all of our genetic structures. So, while these things have the familiarity of the surface image, the profundity of their depth is something that we know very little about. I think it’s Einstein that said that the fish would be the last to know water, I would say, taking off on that context, that human beings will be the last to know technology, because it’s the very water we live in.

Sk!: What advice would you give to young people all around the world gradually awaking to the nightmare of a world out of control with the proliferation of mass techniques?

GR: I don’t like to give advice, but I’ll say what I think as to what we can do. I think our greatest opportunity is to live a creative life. Often that means to reject schooling, rejecting organized education. For many of us, our diploma from college becomes our death certificate, because it ingratiates us into a way of life that’s unquestioned where the principal modus operandi is finance, or money. The real meaning of life, I think for all of us, in our different ways, is the opportunity to live a creative life, to create things, to name things. I would say for all of us, the most radical thing we can do, and the most practical thing we can do, is to be idealistic, to rename the world in which we live. I think we do that best through example, not just through using words, but using words that we can stand on, the acts that we do. Living in the world but not being of the world, being an outsider, yet knowing that all of us are insiders. Living with the conundrum that life is not this or that, life is this and that. It’s not black or white, it’s black and white. So I’ll add to that whole recipe humor, and one has the possibility of living a meaningful life.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

