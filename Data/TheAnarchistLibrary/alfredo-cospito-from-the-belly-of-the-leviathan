
… dreams are to be realized here and now, not in a hypothetical future, because the future has always been sold by priests of whatever religion or ideology in order to steal from us with impunity. We want a present worth living and not simply sacrificed to the messianic expectation of a future earthly paradise. For this reason we wanted to talk of an anarchy to be realized now and not in the future. The “everything now” is a bet, a game we play where the stakes are our lives, everybody’s life, and our death, everybody’s death…’ — Pierleone Mario Porcu

‘Science is the eternal sacrifice of life, fleeting, ephemeral but real, on the altar of eternal abstractions. What I predict is therefore the revolt of life against the government of science.’ — Mikhail Bakunin

‘The empire that reigns sovereign founded on nothing is collapsing.

It cannot bear the weight of truth.

I recommend a massive dose of life!

I recommend a massive dose of life!

At least that way you will be able to say you have lived it.’ — Congegno

‘Bastards… I know who sent you!!’ — Roberto Adinolfi

In a wonderful morning in May I acted, and in the space of a few hours I fully enjoyed my life. For once I left fear and self-justification behind and defied the unknown. In a Europe dotted with nuclear power stations, one of those mainly responsible for the nuclear disaster to come fell at my feet. I want to be absolutely clear: the Olga FAI/FRI nucleus is only Nicola and I. No one else took part in this action or helped or planned it. Nobody knew about our project.

I won’t allow my action to be placed within an obscene and absurd media and judicial cauldron in order to divert attention from its real goal, a cauldron made of ‘subversion of the democratic order’, ‘conspiracy’, ‘armed gang’, ‘terrorism’: empty words that fill the mouths of judges and journalists.

I am an anti-organization anarchist because I oppose all forms of authority and organizational constraints. I am nihilist because I live my anarchy today and not in waiting for a revolution, which – if it ever came about – would only produce more authority, technology, civilization. I live my anarchy with ease, joy, pleasure, without any spirit of martyrdom, by opposing this civilized existent with all my strength, an existent I cannot bear. I am antisocial because I am convinced that society can only exist in the differentiation between the dominant and the dominated. I do not strive for any future blissful socialist alchemy, I do not trust any social class; my revolt without revolution is individual, existential, overpowering, absolute, armed.

There’s no feeling of omnipotence in me, no disdain for the oppressed, for the ‘people’. As an eastern saying goes: ‘don’t scorn the snake because it doesn’t have horns; one day it might turn into a dragon!’. Similarly a slave can turn into a rebel, one man or one woman can become devastating fire. I scorn the powerful of the earth with all my strength, be they politicians, scientists, technocrats, leaders of all sorts, bureaucrats, army and religious chiefs.

The order I want to knock down is that of civilization, which destroys everything that makes life worth living day by day. State, democracy, social classes, ideologies, religions, police, armies, your very court, are shadows, ghosts, clogs of a all-embracing mega-machine that can be replaced. One day technology will do without us and will transform us all into atoms lost in a landscape of death and desolation.

On that 7th May 2012 I threw sand in the clogs of this mega-machine in the space of a second, and during that second I fully lived and made a difference. On that day my weapon was not an old Tokaref but the deep and ferocious hatred I feel towards techno-industrial society. I claimed the action as FAI/FRI because I fell in love with this lucid ‘madness’ that has become true poetry, at times a breeze, at others a storm, blowing halfway around the world, undaunted, improbable, against all laws, ‘commonsense’, ideologies, politics, science and civilization, against all authorities, organizations and hierarchies.

A concrete view of anarchy that doesn’t contemplate theoreticians, leaders, cadres, soldiers, heroes, martyrs, organization charts, militants or spectators. For years I had been witnessing the development of this new anarchy as a spectator. For too long I’d been looking on. If anarchy doesn’t turn into action it rejects life and becomes ideology, shit or a little more, in the best of cases a powerless outburst of frustrated men and women.

I decided to go for action after the nuclear disaster in Fukushima. Far too often we feel impotent in the face of such big events. Primitive men faced danger, they knew how to defend themselves. Civilized and modern men are helpless in the face of the constructions-constraints of technology. Just as sheep look at the shepherd for protection, the very shepherd that will slaughter them, so we civilized men confide in the secular priests of science, the very priests that are slowly digging our grave.

We saw Adinolfi smiling slyly and playing the victim from television screens. We saw him lecturing against ‘terrorism’ in schools. But I wonder: what is terrorism? A gunshot, a searing pain, an open wound or the incessant, continuous threat of a slow death devouring you from inside? The continuous incessant terror that one of their nuclear plants can vomit death and desolation upon us all of a sudden?

Ansaldo Nucleare and Finmeccanica bear huge responsibilities. Their projects continue to sow death everywhere. Recently the rumour has spread of probable investments in the enlargement of the nuclear plant of Kryko, Slovenia, a high seismic risk area very close to Italy. In Cernadova, Romania, several incidents have occurred since 2000, caused by Ansaldo’s stupidity during the construction of one of their plants. How many lives have been lost? How much blood shed? Technocrats of Anslado and Finmeccanica, all facile smiles and a ‘clean’ conscience: your ‘progress’ stinks of death, and the death you sow all over the world is shouting for revenge.

There are many ways to effectively oppose nuclear power: blocks of trains carrying nuclear waste, sabotage of the pylons carrying electricity produced by nuclear power. I had the idea of striking the one most responsible for this mess in Italy: Roberto Adinolfi, managing director of Ansaldo Nucleare. It didn’t take much to find out where he lived, five sessions of laying in wait were sufficient. There’s no need for a military structure, a subversive association or an armed gang in order to strike. Anyone armed with a strong will can think the unthinkable and act consequently.

I’d have liked to have done it all by myself but unfortunately I needed help with the bike. I asked Nicola and appealed to his friendship. He didn’t back down. I bought the gun for three hundred euro on the black market. There’s no need for clandestine infrastructures or huge amounts of money to arm oneself. We left by car from Turin the night before. Everything went smoothly, or kind of. Nicola was driving. I struck right where we had decided to strike. An accurate shot, I ran towards the bike and then the unexpected, the angry cry of Adinolfi, the shouted sentence that froze me: ‘Bastards… I know who sent you!’

At that very moment I had the absolute certainty that I had hit the target, and was fully aware that I had put my hands into a cesspit: money interests, international finance, politics and power, mud and cesspit. Those ‘stolen’ seconds allowed Adinolfi to read a part of the number plate, which we hadn’t covered due to inexperience. Thanks to the numbers they traced the bike and then the camera.

It won’t be the sentence of this court to turn us into bad terrorists and Adinolfi and Finmeccanica benefactors of humanity. The time has come for the great refusal, a refusal made of a plurality of resistance, each of them special. Some are possible, necessary, improbable; others are spontaneous, wild, solitary, arranged, overflowing or violent. Ours was solitary and violent. Was it worthwhile? Yes! If only for the joy we felt when we heard of the defiant smile that Olga Ikonomidou, brave sister of the Conspiracy of the Cells of Fire, threw in the face of her jailers from a solitary confinement cell of a Greek prison.

I’m happy to be what I am, a free man even if I’m ‘temporarily’ in chains. I can’t complain much, given that the vast majority of ‘people’ have chains well placed in their brains. I’ve always tried to do what I thought right and never what was convenient. Half measures never convinced me. I’ve loved a lot. Hated a lot. And for that reason I won’t surrender to your bars, uniforms, weapons. You’ll always find me an irreducible, proud enemy. Not only. Anarchists are never alone, sometimes they are solitary but never alone. A thousand projects in our minds, a hope in our hearts that stays alive, stronger and stronger, determined and shared more and more. A concrete perspective that ‘risks’ changing the face of anarchy in the world. Small, great earthquakes that will stir a cataclysm one day. It will take time, never mind, for the time being I am enjoying the earthquake that broke out inside me from all this desire for joy and struggle.

I conclude with a quotation from Martino (Marco Camenish), unconquered warrior, prisoner for over twenty years because of his profound love of life, today locked up in an aseptic Swiss prison. I make his words my own:

‘… the courage to think things through, to break the technological police bans of the “impossible” and the “unconceivable”, the courage to thinking other and in another way act consequently. Only this can take us beyond the tepid toxic dishwater of modernity into places where nothing and nobody will lead us, to a place without security, the place of responsibility in first person, for non-submission with all its consequences. Freedom is hard and dangerous and there’s no life without death. For fear of losing our lives we often surrender to slavery and annihilation.’

Death to civilization
Death to technological society 
Long live the CCF
Long live the FAI/FRI
Long live the black international!
Long live anarchy!

Alfredo Cospito




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



… dreams are to be realized here and now, not in a hypothetical future, because the future has always been sold by priests of whatever religion or ideology in order to steal from us with impunity. We want a present worth living and not simply sacrificed to the messianic expectation of a future earthly paradise. For this reason we wanted to talk of an anarchy to be realized now and not in the future. The “everything now” is a bet, a game we play where the stakes are our lives, everybody’s life, and our death, everybody’s death…’ — Pierleone Mario Porcu



‘Science is the eternal sacrifice of life, fleeting, ephemeral but real, on the altar of eternal abstractions. What I predict is therefore the revolt of life against the government of science.’ — Mikhail Bakunin



‘The empire that reigns sovereign founded on nothing is collapsing.


It cannot bear the weight of truth.


I recommend a massive dose of life!


I recommend a massive dose of life!


At least that way you will be able to say you have lived it.’ — Congegno



‘Bastards… I know who sent you!!’ — Roberto Adinolfi



‘… the courage to think things through, to break the technological police bans of the “impossible” and the “unconceivable”, the courage to thinking other and in another way act consequently. Only this can take us beyond the tepid toxic dishwater of modernity into places where nothing and nobody will lead us, to a place without security, the place of responsibility in first person, for non-submission with all its consequences. Freedom is hard and dangerous and there’s no life without death. For fear of losing our lives we often surrender to slavery and annihilation.’

