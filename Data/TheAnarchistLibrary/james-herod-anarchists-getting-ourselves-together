      Name      Points of Unity      Purpose of the Network      Structure of Decision Making      Projects      Membership in Groups      Affiliation with the Network      Expulsion from the Network      Discussion
Northeast Network of Anarchist Groups

(1) We assert that human beings are inter-subjective creatures, and that therefore the words "individual" and "society" misconstrue and inaccurately describe the reality of our situation and lives. This means, among other things, that there is no such thing as an "autonomous individual." Consequently, anarchy cannot be conceived as an aggregate of such individuals. It also means that "freedom" cannot be an attribute of an individual but only of a social order (that is, a pattern of social relations). Freedom (and even individuality) can only be a social achievement.

(2) In general, we picture anarchy as a world full of autonomous communities, in which people control all aspects of their lives at all levels. This is sometimes called (rather awkwardly and even confusedly) a self-managed society. The core social forms through which our autonomy will find expression are directly democratic assemblies in our neighborhoods, in our projects (workplaces), and in our extended households. These will be the basic decision-making bodies under anarchy. This means that our new society (anarchy) will be horizontally organized, not hierarchically.

(3) We fight for the abolition of capitalism, the state, and god, and for the establishment of a social order without wage-slavery, markets, money, commodities, classes, or war.

(4) We are committed to the principle of direct democracy, and reject and will campaign vigorously against representative government. By direct democracy we mean decision-making in assemblies through face-to-face discussion, deliberation, and voting, and an association of such assemblies built up through negotiated agreements (pacts, treaties), not through federation (or confederation) using delegates. The term has also been widely used in recent years to refer to referendums and recalls, which is an unfortunate restriction and weakening of the concept, which originally referred to direct participatory democracy, as in a town meeting. We stick to the original meaning of the term.

(5) We reject the principle of simple majority rule (half plus one), in favor of decision-making procedures which achieve the greatest possible majority on any issue (usually called, inaccurately, consensus decision making).

(6) We stand opposed to all forms of domination and discrimination based on race, gender, sexual orientation, age (and any other personal attribute -- personal beauty, ethnicity, intelligence -- if irrelevant to the issue at hand). All persons are equal as to rights.

(7) We are opposed to the destruction of the environment and the abuse of animals.

(8) We make a distinction between us anarchists organizing amongst ourselves as anarchists, for the purposes of agitation, and the social forms that will be needed to constitute anarchy. Organizations (or networks) of anarchists are still one step removed from anarchy itself. For anarchy itself, we will need social forms to enable us to self-management our social relations generally across society.

To agitate for anarchy, as defined above.

Within Our Groups

Decision making within our groups is based on face-to-face discussion and voting. This is generally known as direct democracy or participatory democracy. Since these groups have no power (nor desire) to impose decisions on those in the group who do not agree with them, procedures must be adopted which ensure that the largest possible majority will be reached on any issue, and on securing the willingness to go along by those who disagree. Such procedures are generally known as 'consensus decision making' (a misnomer actually). Decisions will never be delegated to a decision-making elite (elected officers, for example). (See note below for further discussion.)

Amongst Our Groups

Decisions amongst our groups will be made in either of two ways.

(a) A proposal can be discussed and voted on within each local group, but the votes will be tallied across groups. In this case, the votes would be simple yeas or nays, that is, a straight up or down vote on the proposal, with the individual votes tallied across groups. Consensus decision making would not work here, since that usually involves changing the proposal to take account of objections, until most agree with the proposal. However, a similar process could be achieved by successive votes on slightly modified versions of the proposal until most agree with it. Cumbersome but necessary if direct democracy is to be preserved.

(b) A negotiator from each group will be sent to a negotiating conference to hammer out an agreement, which will then be returned to local groups for ratification. This back and forth process will continue until most are satisfied with the decision. This technique is already in use all over the world on a regular basis in the process of negotiating treaties amongst various autonomous entities. Cumbersome but necessary if direct democracy is to be preserved.

(a) Origin of Projects

Any individual in any group can float a proposal for a project, either within their own group, amongst several groups, or to the network at large. Those who are interested in the project, and feel they have time and resources to devote to it, will volunteer. All these volunteers will then meet to hammer out an agreement about how to do the project.

(b) Control of Projects

Projects, both local and regional, are controlled (except for the veto) by those who are doing them and not by the network as a whole.

(c) The Nature of Projects

A project can be anything that helps establish anarchy.

(d) The Vetoing of Projects

Any project can be vetoed by a two-thirds vote of all groups (with each group having one vote).

Membership in the groups is left up to the individual groups themselves.

Initially, the groups present at the founding of the network are automatically affiliated. Subsequently, new groups can apply for affiliation and be admitted by a unanimous vote of existing groups (with each group having one vote).

Any group can be expelled from the network by a unanimous vote of other groups (with each group having one vote).

Definition of Network

The term network is vague. Nevertheless, we use it because it carries with it the connotation of horizontalism, rather than hierarchy. It is usually thought that a network consists of equal nodes, resting on a flat plane, which are connected. And so it is with this anarchist network, which consists of equal groups which are connected (by communication and decision-making procedures) in the common purpose of agitating for anarchy.

Network not Federation

Most of the anarchist federations established recently consist of individuals or groups who agree to abide by the decisions of an annual or bi-annual regional assembly of members. This feature will not be characteristic of this network, the absence of which is what distinguishes it from a federation. This network does not use delegates or representatives for decision making. Nor would a regional assembly, even if it were attended by every person in the network, have the power to make decisions that were binding on local groups (such decisions are made following different procedures, as explained above). The existence of such a regional decision-making body would rapidly lead to the idea that there is a superior, or higher, level of decision making than the local group. This is what we want to avoid. Besides, in practice, one hundred percent attendance is next to impossible to achieve, which is an additional reason for not engaging in the practice.

Terminology. Group as Opposed to Collective

The word 'collective' is ugly and cumbersome. Moreover, it is unnecessary since it carries no meanings that are superior to the word 'group,' so we're just using a bigger clumsier word when a shorter nicer word would work even better. In fact, for those with a bit of history, the term collective carries bad connotations, connected as it was for seventy years with the authoritarian "collectives" of the Soviet Union. Even without that connection, however, the word seems to imply the superiority of the collective over the individual. For these reasons, we prefer the much simpler word 'group.'

Terminology. Negotiator, not Delegate or Representative

We use the term negotiator because this word describes more accurately what is going on. Both the terms representative and delegate are unsatisfactory because they imply the relinquishing of decision making power to these individuals. Many radicals are in the habit of claiming that delegates hold no such power, because they are mandated and recallable. We deny this, seeing these concepts as mere illusions, in practice certainly. Negotiators are what we need, not delegates.

Definition of a Group

A group must consist of at least three members. They must hold at least occasional (but preferably more frequent) face-to-face meetings. That is, three people who communicate solely by electronic means or via regular mail will not be considered a group for the purposes of this network. Non-in-person communication of course can be used substantially by any group.

Groups not Individuals as Members

We insist that groups and not individuals make up the network because of our strong belief in participatory democracy. It is essential that issues be aired in face-to-face discussions involving real people, unmediated by remote communications technology (microphones and speakers are okay). Votes could be tallied for isolated individuals, of course, but this would be polling, not direct, participatory democracy.

On the Provision for Vetoing Projects

This is a precautionary procedure only. It is unlikely that it will have to be used. It is conceivable, however, that scattered individuals amongst several groups, or even one or two groups themselves, will launch a very ill-advised project, which violates the Points of Unity, and reflects badly on the network as a whole. There must be a mechanism for blocking such projects.

A Brief Note on So-called Consensus Decision Making

There is an extensive literature on so-called consensus decision making. There is general agreement in this literature as to what it means, but there are nevertheless some variations. Plus there are some ambiguities remaining (e.g., are the stand-asides obligated to help carry out the decision even though they disagreed with it; and under what conditions are blocks allowed?) No matter what version is used the process works only if everyone is on board and have acquired some skill in it. Skilled people can made some beautiful, effective, and satisfying meetings, but half-assed consensus decision making usually results in a really horrible meeting. You'd almost be better off using simple majority rule, or Robert's Rules of Order. Moreover, already many misconceptions of the technique are widespread in the movement, like the belief that it overcomes majority rule, that there is no voting, or that a block can be used by anyone under any circumstances. Nevertheless, this process (relabeled with a more accurate name) is superior is all respects to simple majority rule: it results in better decisions, achieves greater compliance with the decisions, builds solidarity, results in more effective actions and campaigns, and is consistent with direct democracy and anarchism. A first task of the network will be to hammer out an agreement amongst groups about the decision making procedures to be followed (or at least recommended), that is, the version of so-called consensus decision making that will be used. This shouldn't be all that difficult. Four decades of work have gone into honing these procedures, the result of the New Left's disgust with Robert's Rules of Order.

Alternative Names

North Atlantic Anarchist Association

North Atlantic Anarchist Network

Northeast Anarchist Network

New England Network of Anarchist Groups

North Atlantic Association of Anarchist Groups

Mid and North Atlantic Association of Anarchists

And so forth.

PS.

For background and a more extended discussion of many of the issues involved here you might want to examine three previous papers of mine, namely:

(1) "Remarks on the Efforts Underway to Organize a Northeast Anarchist Network," March 2007, at:

http://www.jamesherod.info/testing/index.php?sec=paper&id=15

(2) "A Great Plains Association for Anarchy?" November 2002, at:

http://www.jamesherod.info/testing/index.php?sec=paper&id=16

(3) "Seeing the Inadequacies of ACF's Strategy Statement," February 1999, at:

http://www.jamesherod.info/testing/index.php?sec=paper&id=27

This present paper can also be accessed on the web at:

http://www.jamesherod.info/testing/index.php?sec=paper&id=31

Please note: These links take you to my new website. It is not loaded up yet, except for a few blogs and the 23 papers under Selected Papers: 1998-Present. Clicking on anything else will just get you a blank page. Sorry. I hope to have everything uploaded by the end of April (but don't hold your breath).




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



(a) A proposal can be discussed and voted on within each local group, but the votes will be tallied across groups. In this case, the votes would be simple yeas or nays, that is, a straight up or down vote on the proposal, with the individual votes tallied across groups. Consensus decision making would not work here, since that usually involves changing the proposal to take account of objections, until most agree with the proposal. However, a similar process could be achieved by successive votes on slightly modified versions of the proposal until most agree with it. Cumbersome but necessary if direct democracy is to be preserved.


(b) A negotiator from each group will be sent to a negotiating conference to hammer out an agreement, which will then be returned to local groups for ratification. This back and forth process will continue until most are satisfied with the decision. This technique is already in use all over the world on a regular basis in the process of negotiating treaties amongst various autonomous entities. Cumbersome but necessary if direct democracy is to be preserved.



(a) Origin of Projects


Any individual in any group can float a proposal for a project, either within their own group, amongst several groups, or to the network at large. Those who are interested in the project, and feel they have time and resources to devote to it, will volunteer. All these volunteers will then meet to hammer out an agreement about how to do the project.


(b) Control of Projects


Projects, both local and regional, are controlled (except for the veto) by those who are doing them and not by the network as a whole.


(c) The Nature of Projects


A project can be anything that helps establish anarchy.


(d) The Vetoing of Projects


Any project can be vetoed by a two-thirds vote of all groups (with each group having one vote).

