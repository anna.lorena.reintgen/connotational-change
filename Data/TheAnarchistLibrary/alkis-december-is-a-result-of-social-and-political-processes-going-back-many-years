
First, I want to say that I am not a historian. I’m an activist, a fighter on the frontlines in the anarchist struggle, since the end of the ’70s. I don’t know how precise my knowledge of anarchist history is, as it is a product of my memory and the things I heard and learned from other comrades during the years of my participation in this struggle.

As far as I know, concerning the post-war period, the first anarchists appeared early in the ’70s and the last years of the dictatorship, as a result of the influence of the revolt of May ’68 which mainly had an impact on the Greeks living abroad, but also on those living here. By saying the influence of May ’68 I also mean what came before that, the Situationists and other radical positions. In that sense the birth of anarchy in Greece, as a movement, does not refer so much to traditional anarchism—with its most significant moment being the Spanish Revolution and its main expressions the anarchist federations and the anarcho-syndicalist organizations—but mainly to the antiauthoritarian, radical political waves of the ‘60s.

As I said before, in Greece anarchists appeared in the beginning of the ’70s and that is when they made their first publications and analysis about the Greek reality from an antiauthoritarian point of view.

The presence and participation of anarchist comrades in the events of the revolt of November 1973 was very significant, not in terms of numbers but rather in terms of their particular, remarkable political contribution, as they did not limit themselves to slogans against the dictatorship, but instead adopted broader political characteristics, which were anticapitalist and antistate. They were also among the few who started this revolt together with militants from the extreme Left. And they were so visible that representatives of the formal Left condemned their presence in the events, claiming that the anarchists were provocateurs hired by the dictatorship, while they also condemned their slogans, characterizing them as foreign and unrelated with the popular demands. In reality, the formal Left was hostile to the revolt itself because they were supporting the so-called democratization, a peaceful transition from dictatorship to democracy. And since they could not stop the spontaneous revolt of ‘73 in which youth and workers participated, they came with the intent to manipulate it, and then, after the fall of the dictatorship, to exploit it politically.

During the revolt of ’73 there were two tendencies: those who wanted it to be controlled and manipulated, in the context of fighting against the dictatorship, in favour of democracy, and against American influence; and those, of whom anarchists formed an important part, who saw the revolt in a broader way, against Authority and capitalism. These two tendencies continued to clash, also after the dictatorship, in the era we call metapolitefsi, which means after the colonels gave the power to the politicians. It was a conflict between those who supported civil democracy and those who were against it. The first tendency considered the events of the Polytechnic as a revolt for democracy, while those who were against the regime of civil democracy saw the events of the Polytechnic as a revolt for social liberation. The echo of this conflict lasts until today, in a way.

So, this is how anarchists appeared, and this was their contribution...

After the colonels handed over power to the politicians, two major forces appeared in the Greek reality. From the one side, there were radical political and social forces disputing the existing political, social, and economic order, and this was expressed by parts of the youth and workers as well. And on the other side there were the political forces of domination, from the conservative rightwing which was in government to their allies on the formal Left which became incorporated in the political system after the fall of the dictatorship. The rightwing government was trying to repress and terrorize the radical political and social forces we mentioned before, and so did the institutional Left, with its own means, when it couldn’t control and manipulate them. Among these radical political and social forces were the anarchists, who were in conflict with even the most radical traditional concepts of the Left, such as the central role of the working class, the hierarchical organization in political parties, the idea of the vanguard, the vision of taking power, and the socialist transformation of society from above.

An important moment of the social struggle during the first years of metapolitefsi, at the end of the ’70s, was the struggle in the universities, sparked by the efforts of the rightwing government to institute an educational reform. In this struggle anarchists also had a significant presence, as well as other groups and individuals with an anti-authoritarian and libertarian perspective. To a large degree, this struggle surpassed the boundaries of the university, and also surpassed university students as a subject, assuming wider radical characteristics and attracting the presence and participation of many more people, not strictly students, but generally youth, like highschoolers, and workers as well. It was an important moment in which the anarchists spread their influence among wide social sectors that were fighting.

In almost the same period of time, a little while after this struggle against the educational reform, anarchists, almost alone, carried out another struggle—solidarity with the prisoners’ struggles. There, they demonstrated another characteristic of their radicalism: they didn’t hesitate to engage in questions that were seen as taboo for society, like the question of prisons and prisoners, and they expressed their solidarity with them, fighting together with them for their demands—the abolition of disciplinary penalties, denunciation of tortures, and granting prisoners with life sentences the right to have their cases examined by appeal courts—while always maintaining their vision of a society without any prisons at all.

A very important event of that period which shows the political and social dynamics of the subjects of resistance and, at the same time, the ferocity of political power, an event which actually defined the political developments of those times, was a demonstration that took place on the 17th of November, 1980, on the seventh anniversary of the Polytecnic revolt. (Every year there was and still is a demonstration on the anniversary). That particular year the government had forbidden the demonstration from going to the US Embassy. The youth organizations, as well as the student organizations, controlled by the Communist and the Socialist Parties, obeyed the prohibition; however, political organizations of the extreme Left, which were strong in that period of time, decided to attempt to continue the demonstration to the American Embassy, defying the prohibition laid down by the government and the police.

So, on the night of the 17th of November, 1980, next to the building of the Parliament, in the street leading to the embassy, thousands of demonstrators were confronted by a very strong force of police. The effort of the first lines of demonstrators, who were members of the extreme Left, to push forward to the American Embassy, was followed by a mass attack by the police forces in order to disperse the crowd of thousands. But despite the police attacks there was a strong and lasting resistance by several thousand people, youth and workers, members of the extreme Left, anarchists and autonomists, who set up barricades in central Athens—barricades which the police used armored vehicles to dismantle. During these clashes two demonstrators were murdered by the police, Iakovos Koumis and Stamatina Kanelopoulou, both members of extreme Left organizations, and hundreds were injured, some seriously. Among the ones injured, two were wounded by live ammunition, one of them in the chest, shot by police outside the Polytechnic.

During these clashes many capitalist targets were attacked and looted, like department stores, jewellery shops, and the like. This type of attack, which was one of the first expressions of metropolitan violence not strictly limited to targeting the police but also expressions and symbols of wealth, was condemned even by the extreme Left, whose political culture recognized only the police as a legitimate target. But a new phenomenon was emerging then, metropolitan violence, where besides engaging in confrontations with the police demonstrators were also destroying and looting capitalist targets, and that is exactly what was condemned by the Left.

Those events of November 1980 were, as we mentioned, an expression of the political and social dynamics of the first years of metapolitefsi, but also the culmination and the end of the hegemony of the extreme Left on these dynamics, since they didn’t manage to explain, in their own terms, the extent and the form of the events neither socially nor even to their followers. However, these same events were a catalyst for the fall of the rightwing government, one year later.

In the beginning of the ‘80s, as a result of a major effort by a part of the political system to control and manipulate the social, political, and class resistances and demands, a new political change occurred and the Socialist Party, PASOK, came to power (October ’81). This was something that in that period seemed to be a huge, historical change. It created a lot of illusions, incorporated and neutralized old militants in the institutions and marked the end of these first years of metapolitefsi, the end of a variety of spontaneous social and class struggles which had appeared in the first years after the fall of the dictatorship. So, after this political change, anarchists who were hostile to any kind of mediation and incorporation into the institutions were in a sense alone against this new authority that had many controlled and manipulated supporters, many adherents full of illusions.

PASOK came to power in order to modernize Greek society, repealing laws that were products of the civil war era—when the Right had crushed the Left in an armed conflict—and the post-civil war era, and satisfying a series of demands coming from the people of the Left; demands that did not at all undermine the authoritarian and class organization of society, but, on the contrary, that modernized and strengthened it by making it come closer to the model of the Western European societies.

This political change meant that a large part of the Left was weakened and absorbed into the system, so it also meant that the anarchists together with autonomists and antiauthoritarians in general manifested a single effort to intervene socially, referring mainly to youth, and making the first squats in Greece, influenced by similar projects in Western Europe.

The project of the first squat that happened in Exarchia became for some time the epicentre of anarchist and antiauthoritarian mobilizations, and led to other occupations in Athens and Thessaloniki, but after a while it was attacked by repression and was evicted, in the beginning of 1982. The same happened with the other squats as well. (On that point, we could also mention that from the end of the ‘70s and especially in the beginning of the ‘80s a repressive operation by the State was conducted in order to corrupt and destroy the resistance movement by spreading heroin in the social spaces of the youth. This operation was very new then, unprecedented in the Greek reality, and anarchists came in face-to-face conflict with that, fighting against it in the social spaces, in the places of the youth, and also inside the squats.)

The first years of government by PASOK were full of artificially cultivated aspirations for changes, changes that were of course neither essential nor subversive. They were years of a broad social consent to political power, where anarchists stood against it alone to a large degree. But very soon this political authority showed its cruel true face and its profound class character against the lower social classes, as well as its repressive ambitions with regards to those resisting—anarchists, leftists, and insubordinate youth. The turning point, the end of the illusions, was in 1985, a year scarred by the police murder of 15-year-old Michalis Kaltezas who was shot in the back of the head outside the Polytechnic during riots between anarchists and insubordinate youth on one side and the police on the other, after the end of the 17th of November demonstration that year.

This murder triggered a series of insurrectionary events of resistance whose major moments were the occupation of the Chemistry University and the Polytechnic. Moreover, it caused a deeper uprising of consciousness and hostile dispositions against the police and Authority which gave birth to numerous events of resistance in the following years, since it was not something that was expressed and exhausted in one moment, but became a precedent of many violent and combative moments of resistance in the following years. It formed a “tradition” of similar events; events that burst forth either as reactions to State murders, or as expressions of solidarity with the struggles of oppressed people, such as the prisoners. It is also within these conditions that a new wave of squats, mainly by anarchists and antiauthoritarian groups, appeared and rooted socially, thus broadening the fronts as much as the influence of the struggle.

For example we can mention the clashes with the police and the occupation of the Polytechnic for 17 days in 1990, after the acquittal of the cop who murdered Kaltezas...

...The extensive social clashes in the streets of Athens in 1991, lasting a full two days, after the murder of the teacher and fighter of the Left Nikos Temponeras by para-state thugs in a student-occupied school in the city of Patras...

...The uprising of anarchists and youth in November, 1995, during the anniversary of the ‘73 revolt, in which they occupied the Polytechnic in solidarity with the revolt of the prisoners which was going on at the same time. This revolt in the prisons was under fire from the whole propaganda mechanism of the State, by the media, and it was facing the immediate threat of a police invasion in the prison facilities.

In an effort to suppress the ‘95 Polytechnic revolt and attack the anarchists and the youth—not only for the resistance they were engaged in at that specific moment but also for all the events that they had created during the previous years, and the events which they were threatening to continue—the State made use of the major propaganda assault by the media, which had been waged to extract social consent for the plans of repression. The police invaded the occupied Polytechnic on the morning of the 17th of November, 1995, and arrested more than 500 occupants, but the entire repressive operation was a failure: they wanted to present the anarchists as very few and isolated, as small gangs of rioters—the stereotype presented by the State is of “50 known unknowns”—but they turned out to have great influence on youths. They also failed to terrorize anarchists with the arrests and the prosecutions in the courts, because the majority of defendants remained insubordinate, turning the trials that followed into another point of strong conflict with the State.

In the following years, this phenomenon of refusal and resistance by anarchists, antiauthoritarians, and insubordinate youth spread socially, leading to a variety of political initiatives, social interventions, counter-information projects, events of resistance, and the creation of new self-organized spaces. No strategy of domination was left unchallenged, neither the policies against the immigrants, nor the 2004 Olympics, the international political and economic summits, the participation of Greece in military plans and operations of the West against the countries of the East.

Based on the political and simultaneously organizational values of social solidarity, direct action, equality, anti-hierarchy, and self-organization, anarchists didn’t hesitate and didn’t fail to answer, at least to the extent they could, any attack by the State against society, and its most marginalized parts. They always stood side by side with the oppressed people and with those of them who fought back, refusing the dilemmas and defying the blackmails that the State utilizes in order to extract consent. And they did that clearly and regardless of the cost they would have to pay. They consistently stayed outside and against all institutions, outside and against the political system. At a time when others, no matter how radical they appeared, were adopting the mentality of the State, the anarchists stood alone against such proposals. The result was that the Left lost its influence among the most radical parts of society, while for the anarchists, the same thing that was said to be a weakness that would lead to their social isolation, was and still is exactly their strength: the fact that they stayed outside the political system and all institutions. Because when the people revolt they surpass the institutions and their restrictions, and communicate very well with the anarchists.

We hardly have any money, we work unselfishly in small, fluid affinity groups, but this is our strength.

As the events of December showed, those who lost contact with society’s most radical and militant expressions were not the anarchists, but, on the contrary, those who were flirting with the ideas and structures of authority, claiming a role for themselves as representatives of the social subjects and mediators of social contrasts.

Through a long-lasting process of struggle, which I briefly described before, anarchists and anti-authoritarians in general gained a lot of ground in the consciousness of the people, something that was not evident to everybody until December. Because beyond the idea that the State lost a lot of social ground during the days of December, the more profound truth is that it had already lost a lot of this ground before the events of December, over a long period of time. And that is something that was expressed in a very revealing way from the first moment of the explosion of the revolt, with the participation of crowds of people in actions that were considered up to that moment exclusively as actions of small groups of anarchists.

In reality, December of 2008 has a profound historical, political, and social background that is connected to the entire history of the struggles of the last 30 years, and to the presence and participation of anarchists inside those struggles; a participation that is characterized by the praxis of social revolt without mediators and without illusions for a change inside the existing system, proposing self-organization against any kind of hierarchical organization, proposing counter-violence against State violence, and solidarity against individualization and the artificial divisions created by the Power.

Here we could talk about dynamic practices of struggle, such as the clashes with the police, that were appropriated by crowds of people in December, same as the occupations of buildings (universities, schools, town halls and many others). The same happened with self-organization through open anti-hierarchical assemblies which were created during the days of December and afterwards. Those practices were avoided and downrated by the Left and the result is that the events surpassed them.

However, even though December is a result of social and political processes going back many years, and it does have similarities and analogies with previous events, at the same time it surpasses them and expresses new situations, needs and desires, creating new potentials. To talk about the differences from past events, we should say that this time the events were not limited or focalized in a specific time and space. They were diffused to numerous cities all over the country and took many different forms, more or less violent but always antagonistic to the State, based each time on the inspiration and imagination, the inventiveness of the people who participated.

Furthermore, it is a process which, because of its diffusion and its multiform character, doesn’t seem to have an endpoint; rather it seems to continue and renew itself taking new forms and bearing the promise of new eruptions of social explosions in spite of the current decline of violent events. Previously also the events concerned mainly Greek youth but in December what spread all across the country included people of many other nationalities, including migrants and refugees.

Dynamic methods of struggle and processes of self-organization were adopted by many people, without representatives and without putting forward any demands. December not only continues a culture of political violence, it is also laying down a new tradition of self-organization as an important social urge, to organize from below. Now these processes of self-organization which constitute a form of continuation of the revolt don’t have as their only objective to respond to murderous police violence but to respond to all the expressions of Authority, from the way we live, the way we work, produce, consume, to the issues of health, the environment, everything. Every aspect of authority is a front of struggle for the people who self-organize and fight from below, not always violently but almost always antagonistically to the State.

Another point is that the revolt justified certain positions inside the antiauthoritarian movement and disproved certain others. For example, the notion that claimed that everything is under control, that manipulation and control of people is so strong today that revolts are not possible, or that society is dead, that it cannot produce anything healthy and that we anarchists are alone against the State; this is a notion that was disproved. December proved that revolt is possible, and, much more, that social revolt is possible.

One more aspect has to do with the subjects of the revolt. There has been a lot of talk about who were those who rebelled and there has been a major effort by the media and representatives of the political system to determine the subjects of the revolt in order to write the history themselves; to control, even afterwards, whatever they can. They allege that it was a revolt of youth, and most specifically the Greek youth, and especially high school students, based on the fact that really part of the revolt was mobilizations of high school students, who, on many occasions, went as far as to demonstrate at police stations and assault them. But this is a very limited and falsified presentation of the revolt. The political system and the media want to conceal the wider social, multinational, and class character of the revolt. It was not only the students who were in the streets! And, in any case, most of the youth who came into the streets did not come down as students, but as insurgents against the world of domination, state violence, authority, and exploitation. They want to hide what was evident to everybody who was in the streets: that in those streets there were the poor, the salaried workers, the unemployed, those we call excluded. And a large number of them were immigrants, those who are the cheapest labour force and main victims not only of labour exploitation but also of police violence and state repression.

Consequently, the subject that each analyst presents as having a central role in the revolt indicates his or her own political purposes and reflects their subjective perception of the revolt and their future objectives. For example, when they talk about Greek youth and especially about high school students, it is in order to separate them as “good” rebels, considering them easier to manipulate, from the “bad,” uncontrollable rebels. However the majority of the people who were in the streets basically belonged to the latter category, they were uncontrollable, oppressed people.

Today we are facing two things. One is the repressive moves by the State through the judicial system and the police, such as arrests, imprisonments, people being held hostage through prosecutions, decisions about installing surveillance cameras everywhere, the penalization of wearing masks and of insulting the police verbally, the targeting of squats, of self-managed spaces and generally of the self-organized structures of the movement. On the other hand we have the ideological attack launched by the State in order to divide the rebels of December into “good” students, aiming to incorporate them into the system, and the “bad ones,” who cannot or do not want to be incorporated and thus must be isolated and attacked by repression.

We should say at this point that while repression is basically expressed directly by the state mechanisms, the ideological war on the other hand is not being expressed only by them but also by other auxiliary mechanisms such as the parties of the institutional Left. While the judiciary and the police repression are immediately visible and understood as something that comes from outside, the ideological war is more insidious and it is also generated within the movement itself, since it is expressed not only by those who are hostile to the movement but also by people who appear as friends of the movement and who are selectively projecting those characteristics of the revolt which they like, which means those characteristics they think they can absorb and utilize. And at the same time they slander those characteristics and subjects of the revolt that they don’t consider agreeable, naming them non-political, anti-social, or even criminal.

This ideological war aims to incorporate, to terrorize those who are not incorporated, and to isolate those who hold the perspective of revolt. The crisis of the system, though, which at its base is a crisis of its social legitimation, radically limits the possibilities of incorporation for a large portion of the people who react and resist. To clarify, this means that more and more people lose their trust in the institutions or the proponents of the system. This is why, even if they manage to incorporate some, they can’t really confine and intercept the influence of the radical ideas.

The ones that we have to be wary of, because of their erosive and undermining presence, are exactly the ones who have one foot in the old world and the other foot with us, talking about a new world. These double-faced enemies of the revolt are the worse. They can be even worse than police and judges.

We have to make clear that here we refer specifically to those who play a certain role, even not that important, inside the institutions, and not generally to people—workers, neighbours, youth—with whom we meet. As for the latter, people who are being acculturated and educated by the system to have faith in the institutions, it was much easier to communicate with them especially in the first days of the revolt, because the material conditions and the tension of the events was such that everyone was moving from their old positions to new ones.

Today, as time goes by, our political and personal ability to keep these contacts is being tested. And so does our patience when acting together with people different from us, recognizing that we have a lot more to learn about how to keep contact with all these people whom we met in the streets in December. And the most important way that we meet face-to-face, beyond the usual propaganda material, the texts and flyers, is in the self-organized assemblies. From our side, we encourage the creation of such assemblies, we participate and intervene in them. And it is there also that we’re faced with the ideological war I talked about before. But apart from that, there are the prejudices; both the prejudice of other people regarding us, and our prejudice towards people who do not have a clear rejection of the existing system, either out of naivete, out of fear or just because they are accustomed to it.

But we are on the right path. The relations that have been developed between anarchists, antiauthoritarians, and other parts of society constitute a whirlwind and the outcome is unpredictable. For sure it is something positive, as we don’t allow normality and alienation to re-establish themselves. Because in contradiction to the swirl of the revolt where everything is possible and we can hope for the best, normality is a situation where almost everything is predictable and most of the time the result is negative.

Things are unpredictable, not only concerning the relation between anarchists and antiauthoritarians with other people, but within the movement as well. And, mostly, things are unpredictable in terms of the relation between the anarchists, society, and the State. The anarchist/antiauthoritarian social movement produces many initiatives and acts of resistance against the State, some more dynamic and others less so, some more social and others less so. That is to say that there is not any central organ or single nucleus, but a variety of larger and smaller initiatives of struggle from below, some of which coordinate among each other while others do not. In every case, what should be avoided, in my opinion, is to be socially isolated, to be isolated among us, in the movement, and to be left alone to carry out a confrontation with the State.

We understand that if a number of things that are done here were done in the US or in Italy for example, some of us would be dead and many more would be in prison for a lot of years. This balance of power that exists today—the fact that there is such activity and that we can talk about these things—has been 30 years in the making. But our lives and our freedom are always imperilled and targeted by the state mechanisms. After December the State wants to change this balance of power, and it could reverse it. Just as in one moment, when Alexis Grigoropoulos was murdered, many desires for revolt were liberated from within the people, there could be another moment where, based on a different event, an explosion of state repression could occur; and anarchists, as well as other fighters, could be exposed to tremendous dangers.

The history of the movement in the US, in Europe, and in the world teaches us both what we can do and what we can be faced with. Having a deeper knowledge of what we are and what we want to do, but also of what the State is and what it wants to do with us—to make us disappear—what we should make sure of is not to isolate ourselves from society, but also not to be divided within the movement, so that as a whole we won’t be left alone against the State, nor that every individual comrade will be left alone against the State. But it is also important not to restrain our impetus or compromise our inner desires, to act and make things happen, to use our courage and even our craziness.

We haven’t said anything so far about the role of spontaneity in the events of December. Spontaneity has always played a role in the anarchist initiatives and did again in December. But there was also the spontaneity of the social groups that participated in the revolt, the spontaneity of the masses. According to Castoriadis, spontaneity is the excess of the “result” over the “causes.” There were spontaneous forces that were expressed in December, forces that were hidden inside the masses of the people and which were not predictable before. And these forces still inhere in society, much more in a society that is on its knees, much more in a society divided into classes, suffocating by the violence of the system, by poverty, despair, fear. For people living in such a society, two possibilities remain: either the passive acceptance of the existing reality, which the State wants to present as the only option; or insurrection, which even when it is not visible as a possibility or choice doesn’t mean that it doesn’t exist and that it won’t burst forth.

And there is one more point: in today’s conditions of domination by the State and capitalism in the West, the explosion of revolts is not so rare, including metropolitan riots, mostly by groups of youth and usually triggered by incidents of police violence, such as the events in the French suburbs, or the black revolt in L.A. in ’92. And as a different case, we could also mention the Albanian revolt in ‘97, even though it has many distinct characteristics. But what happened here in December, in comparison with other big insurrectionary events, was that political and social subjects met and interacted. Anarchists met with social subjects ready to revolt.

In this context, revolt becomes much more dangerous for Authority; when it is not just an outburst of social rage by a specific oppressed social group, but the fertile meeting of the dynamics of various social groups who direct together their violence against the source of all the exploitation and oppression.

Revolts happen and cannot be avoided. Authority knows that, so, it prefers to suppress each one social group alone and not let revolts take clear political characteristics, not let them have a total criticism against the existing order. The presence and participation of the anarchists in December gave such wider political characteristics; and to a large extent a subversive criticism of the system as a whole was developed.

And that was right, and it is right for every comrade or group of comrades, wherever they are in the world, to attempt and to realize the meeting with social groups that suffer from the tyranny of the State and capitalism and have the desire to fight back, so that the unavoidable revolts become more widespread and not restricted.

If only we imagine what could happen with the meeting between political subjects who are consciously intending the subversion of the existing order, with all those social subjects who suffocate from the State and capitalism and have reasons to revolt. Only imagining this is enough to understand. And this is what happened to a large degree in Greece in December.

Alkis,

April 2009




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

