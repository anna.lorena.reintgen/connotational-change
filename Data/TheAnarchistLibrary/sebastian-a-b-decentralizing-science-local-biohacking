      Stigmergic Science      Aided by Randomness      Radical Biology      Paternalism Comes Knocking
Do-It-Yourself scientists working in hackerspaces are positioned to make significant contributions with low overhead and little formal training (becoming necessary and valuable apprenticeship sites as the current higher education system deteriorates). The state has yet to heavily clamp down, but, because such freedom threatens the status quo, we can expect intervention to intensify.

A hacker is someone who enjoys playful cleverness — not necessarily with computers. The programmers in the old MIT free software community of the 60s and 70s referred to themselves as hackers. Around 1980, journalists who discovered the hacker community mistakenly took the term to mean “security breaker.”

— Richard Stallman

Science and innovation are chaotic, stochastic processes that cannot be governed and controlled by desk-bound planners and politicians, whatever their intentions. Good scientists are by definition anarchists.

— Theo Wallimann, biologist at ETH Zurich

The individual is the basic functional unit of innovation. Institutions provide resources — capital, human and fixed. But free people can achieve a lot with very little.

Steve Wozniak built Apple from a garage (with the help of frontman Jobs), and now it reigns among the largest companies in the world (not to glorify the crooks at Apple — they are patent trolls and sweatshop labor exploiters).

Do-It-Yourself scientists working in hackerspaces are positioned to make significant contributions with low overhead and little formal training (becoming necessary and valuable apprenticeship sites as the current higher education system deteriorates). The state has yet to heavily clamp down, but, because such freedom threatens the status quo, we can expect intervention to intensify.

The magnitude of creative productivity is most strongly correlated with the number of researchers, and less with the talent of the individuals involved, and fortunately the positive feedback loop (or virtuous cycle) of technology continues to lower the cost of instrumentation. That is, happy accident probability is proportional to time invested rather than just skill.

Establishment science institutions are somewhat impeded from developing groundbreaking, disruptive or revolutionary technologies, for three reasons:

First, they need to be able to monopolize them. Anything that lends itself to decentralization (solar power, self-replicating 3D printers) threatens the established order and will be resisted to the end. If a modern-day Nikola Tesla were to invent a disruptive energy technology, s/he would likely be suppressed, just as J.P. Morgan and Edison suppressed Tesla.

“Science is but a perversion of itself unless it has, as its ultimate goal, the betterment of humanity.”

— Nikola Tesla

Second, visible and legally liable institutions must abide the patent monopoly structure. They must pay for the use of ideas. Garage developers fly below the radar. Thus, R&D is cheaper, but patents make marketing a product prohibitively expensive and retard deliverability.

Finally, far-out ideas make established scientists uncomfortable. If your entire career was built around the fax machine, phrenology, the geocentric model or the beeper, you’re not too excited about these crazy kids and their ideas. There is a lot of untapped brainpower out there. The state education mill is a barrier to entry, a great divider — a credential firewall. MOOCs and badges may displace the academic cartel, but not without vested interests fighting to halt creative destruction along the way.

“Academic Libertarian,” statistician and philosopher Nassim Nicholas Taleb recognizes that “stochastic tinkering” rather than systematic, institutional agendas yield the greatest discoveries. Taleb is best known for coining the term “Black Swan,” to describe hard-to-predict and disproportionately momentous events.

Stochastic tinkering is a process of trial and error, present in all creative endeavors, where randomness plays a great role. Taleb writes, in his essay The Birth of Stochastic Science:

The world is giving us more “cheap options”, and options benefit principally from uncertainty. So I am particularly optimistic about medical cures. To the dismay of many planners, there is an acceleration of the random element in medicine putting the impact of discoveries in a class of Mandelbrotian power-law style payoffs.

It is compounded by another effect: exposure to serendipity. People are starting to realize that a considerable component of the gravy in medical discoveries is coming from the “fringes”, people finding what they are not exactly looking for. It is not just that hypertension drugs lead to Viagra, angiogenesis drugs lead to the treatment of macular degeneration, tuberculosis drugs treat depression and Parkinson’s disease, etc., but that even discoveries that we claim to come from research are themselves highly accidental, the result of tinkering narrated ex post and dressed up as design. The high rate of failure should be sufficiently convincing of the lack of effectiveness of design.
[...]

All the while institutional science is largely driven by causal certainties, or the illusion of the ability to grasp these certainties; stochastic tinkering does not have easy acceptance. Yet we are increasingly learning to practice it without knowing — thanks to overconfident entrepreneurs, naive investors, greedy investment bankers, and aggressive venture capitalists brought together by the free-market system [sic].

I am also optimistic that the academy is losing its power and ability to put knowledge in straightjackets and more out-of-the-box knowledge will be generated Wiki-style. But what I am saying is not totally new.

Accepting that technological improvement is an undirected (and unpredictable) stochastic process was the agenda of an almost unknown branch of Hellenic medicine in the second century Mediterranean Near East called the “empirics”. Its best known practitioners were Menodotus of Nicomedia and my hero of heroes Sextus Empiricus. They advocated theory free opinion-free trial-and-error, literally stochastic medicine. Their voices were drowned by the theoretically driven Galenic, and later Arab-Aristotelian medicine that prevailed until recently.

As a biologist, I can strongly affirm the accuracy of Taleb’s notion of “accidental” discovery in this field. Biology is extremely complex and experimental outcomes are unpredictable.

Living organisms and cells require time to grow and change. There are too many moving parts and holding them constant is difficult because we don’t even know how many parts there are and how they interact. Metabolic pathways are considered both discrete and continuous, but no one doubts that they are dynamically equilibrating systems that cannot be easily modeled, not as amenable to tinkering like mechanical or chemical engineering — even basic biology is expensive and has historically been the purview of big-budget institutions.

That is changing.

Theory, as usual, has a weak barrier to entry. Projects like TinkerCell allow cellular biologists to design their own metabolic pathways and share them open-source with a community, dramatically boosting stigmergic idea development and cross-pollination. What one wrote on a pad and paper and filed away in a dusty file cabinet for posterity will soon be indexed and searchable.

“Wetlabs,” however, are the big story. These labs are now becoming available to those not associated with universities or corporations. Anyone in the community can pitch in and do biology. Science enthusiasts are organizing IRL to poke and prod at the mystery of life (Making the Modern Do-It-Yourself Biology Laboratory, Singularity Hub).

Molecular biologist Ellen Jorgensen established Genspace, a major DIY lab in Brooklyn. Some highlights from her TED talk:

“You might be asking yourself, ‘What would I do in a biolab?’ Well, it wasn’t that long ago we were asking, ‘What would anyone do with a personal computer?’”

“The press had a tendency to consistently overestimate [biohackers'] capabilities and underestimate our ethics.”

“DIY [biotech] people from all over the world ... got together last year, and we hammered out a common code of ethics. That’s a lot more than conventional science has done.”

“[In a DIY bio lab,] you can work on a project and you don’t have to justify to anyone that it’s going to make a lot of money, that it’s going to save mankind, or even that it’s feasible.”

If you want to get involved, check out this listing of DIY wetlabs, or start your own.

If the success of young scientists like Jack Andraka (who surfed the internet and developed a promising and cheap pancreatic cancer screening test — with the help of professionals) are any indication, it’s better to have a lot of moderately-trained people doing science than just a vanguard of highly trained experimenters.

Regarding experiments, something can be said for quantity over quality, perhaps for two reasons: First, experiments take time. The more the merrier. Second, accidental, wild, speculative results are born from intractable randomness — and positive Black Swans may be more likely to come out of science than, say, finance or statecraft.

In this era of big science, the most important lesson to be learned from [...] the achievements of countless amateurs is that scientific observations and discoveries don’t necessarily require giant government grants and huge teams of researchers with specialized degrees. Small science still works, and it often works during off hours, weekends, and holidays when professionals are generally at home or on vacation.

— Forrest M. Mims III (1999), writing in Science.

As is evident from several millennia of prudent governance by states, the right balance between free-form innovation and legal restriction will be struck. Statists are already calling for regulation, but restrictions are quite unenforceable (the tools and knowledge of garage science are becoming ever more accessible).

A number of regulatory approaches have been put forward: requiring biosafety training for all practitioners through programs designed by the Centers for Disease Control and Prevention, registering community labs with government agencies, requiring some type of personal liability insurance, excluding felons from DIY activities, and instituting screenings for loyalty and integrity.

The feds have already goofed up an investigation, branding an artist as a bioterrorist.

Casting a long shadow over the DIY bio movement is the case of Steven J. Kurtz, an art professor at the State University of New York at Buffalo. The case has been held up as a warning about overly aggressive law enforcement in cases involving home laboratories. While not a scientist—professional or amateur—Kurtz uses DNA and other biological materials in his artwork.

In May 2004, Kurtz and his wife, Hope, had been preparing commissioned works when Hope Kurtz died at their home. Her husband called 911. Her death was later determined to be of natural causes. In attending to Hope Kurtz, emergency personnel observed Petri dishes containing bacteria cultures and food–testing equipment that was considered suspicious. They contacted the Federal Bureau of Investigation (FBI).

Authorities later told Kurtz he was being investigated for bioterrorism, and, eventually, Kurtz and Robert E. Ferrell, former chair of the University of Pittsburgh’s Department of Human Genetics, were indicted on mail and wire fraud for their alleged efforts to obtain biological organisms from a lab that was not allowed to sell to individuals.

In April 2008, a federal judge dismissed the mail and wire fraud charges against Kurtz, noting that there was insufficient proof to go forward. Ferrell was fined $500 after pleading guilty to a count of mailing an injurious article, a misdemeanor.

Scientists had feared the case would be precedent–setting, but instead it has turned out to be a cautionary lesson about the dangers of under–educated law enforcement personnel who cannot tell the difference between a bioterrorist lab and an artist’s studio using common bacteria.

The FBI says it has done much to make sure the Kurtz incident stays as the exception rather than the rule. Many safeguards and precautions have been put into place since the announcement, most important, the education of local law enforcement about DIY scientists and labs.

State enforcement will prove incapable of preventing anyone from, say, dumping noxious chemicals or developing the next superbug (indeed, states are already doing that — they just try to keep them in the lab) — but bio-hackerspace communities might. Everyone else’s experiments tend to be known to others, and getting away with anything sinister is much harder.

The greatest risk in state control of scientific inquiry is this: the government is likely to suppress inventions that threaten profit and mass control. Energy and medicine are particularly sensitive areas for the corporation-state. Scientists of the future must be skeptical of idea management by means of centralized systematic violence.

“It is the responsibility of scientists never to suppress knowledge, no matter how awkward that knowledge is, no matter how it may bother those in power. We are not smart enough to decide which pieces of knowledge are permissible and which are not.”

- Carl Sagan, UCLA commencement speech on June 14, 1991

What they call the “social order,” we call predation.

What they call “unregulated” chaos, we recognize as a driver of innovation.

The state cannot be overcome by force, because another state would rise from the ashes of the human mind.

The scarcity and dependence on centralized expertise that appears to justify states can be abolished with the spread of disruptive technology.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                



A hacker is someone who enjoys playful cleverness — not necessarily with computers. The programmers in the old MIT free software community of the 60s and 70s referred to themselves as hackers. Around 1980, journalists who discovered the hacker community mistakenly took the term to mean “security breaker.”

— Richard Stallman


Science and innovation are chaotic, stochastic processes that cannot be governed and controlled by desk-bound planners and politicians, whatever their intentions. Good scientists are by definition anarchists.

— Theo Wallimann, biologist at ETH Zurich



“Science is but a perversion of itself unless it has, as its ultimate goal, the betterment of humanity.”

— Nikola Tesla



The world is giving us more “cheap options”, and options benefit principally from uncertainty. So I am particularly optimistic about medical cures. To the dismay of many planners, there is an acceleration of the random element in medicine putting the impact of discoveries in a class of Mandelbrotian power-law style payoffs.


It is compounded by another effect: exposure to serendipity. People are starting to realize that a considerable component of the gravy in medical discoveries is coming from the “fringes”, people finding what they are not exactly looking for. It is not just that hypertension drugs lead to Viagra, angiogenesis drugs lead to the treatment of macular degeneration, tuberculosis drugs treat depression and Parkinson’s disease, etc., but that even discoveries that we claim to come from research are themselves highly accidental, the result of tinkering narrated ex post and dressed up as design. The high rate of failure should be sufficiently convincing of the lack of effectiveness of design.
[...]


All the while institutional science is largely driven by causal certainties, or the illusion of the ability to grasp these certainties; stochastic tinkering does not have easy acceptance. Yet we are increasingly learning to practice it without knowing — thanks to overconfident entrepreneurs, naive investors, greedy investment bankers, and aggressive venture capitalists brought together by the free-market system [sic].



“You might be asking yourself, ‘What would I do in a biolab?’ Well, it wasn’t that long ago we were asking, ‘What would anyone do with a personal computer?’”


“The press had a tendency to consistently overestimate [biohackers'] capabilities and underestimate our ethics.”


“DIY [biotech] people from all over the world ... got together last year, and we hammered out a common code of ethics. That’s a lot more than conventional science has done.”


“[In a DIY bio lab,] you can work on a project and you don’t have to justify to anyone that it’s going to make a lot of money, that it’s going to save mankind, or even that it’s feasible.”



In this era of big science, the most important lesson to be learned from [...] the achievements of countless amateurs is that scientific observations and discoveries don’t necessarily require giant government grants and huge teams of researchers with specialized degrees. Small science still works, and it often works during off hours, weekends, and holidays when professionals are generally at home or on vacation.

— Forrest M. Mims III (1999), writing in Science.



“It is the responsibility of scientists never to suppress knowledge, no matter how awkward that knowledge is, no matter how it may bother those in power. We are not smart enough to decide which pieces of knowledge are permissible and which are not.”

- Carl Sagan, UCLA commencement speech on June 14, 1991

