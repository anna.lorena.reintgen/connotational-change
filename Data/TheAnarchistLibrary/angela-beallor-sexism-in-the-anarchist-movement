      Challenging Ideas and Behaviors      Political Study      Encouraging Women      Anarchist Organizational Structures      Taking up Womens’Struggle      Forward Always, Backwards Never
This article is an attempt to add to the discourse that is (or should be) occurring around sexism within the very movements that purport to be fighting it. It was a hard process to distinguish between sexism within the anarchist movement and the general sexism within society because so many of the criticisms that can be leveled against the anarchist movement are criticisms of the greater society. There is a void where critical anarchist feminist/anti-sexist critiques should be which has lead to a lack of dialogue and concrete action around sexism. This critique will be based upon many of the weaknesses within the Anarchist movement, which are often compounded around issues of sexism (and other forms of oppression). There is a continuum of thought and concrete action which anarchists must address or take up in order to combat our own sexism and sexism in the greater society.

The continuum begins with our personal thoughts and behavior. Growing up in a sexist society imbues within us the idea that women are inferior to men. Unless these ideas are thoroughly challenged, in every aspect of our lives, every waking minute, then these ideas are allowed to flourish in our behavior. Many may feel this is an obvious point, but as Kevin Powell wrote in a recent Ms. article, “Everyday I struggle within myself not to use the language of gender oppression, to see the sexism inherent in every aspect of America, to challenge all injustices, not just those that are convenient for me.”

Anti-sexism is not just about fighting overt forms of sexism — violent rape, domestic violence, overtly sexist words — it is also about challenging our relationships, the ideas that create a rape culture, the way people are socialized, etc. These are not convenient issues to struggle around for they involve digging deep within ourselves, traveling back in our development, and dedicating time to the difficult process of self-change. We must challenge the ideas and behaviors that promote sexism to other men and alienate women-both in personal relationships and in organizations.

Recognizing that anti-sexist work is a deep, hard process is very important but a point many miss. All too often men who are genuinely against sexism fail to acknowledge and challenge the sexism that lies within themselves. “I AM an anti-sexist,” they proclaim. But it is said so loudly that they fail to hear the voices of women. It becomes a label to proudly sport instead of a serious and difficult process. Don’t get me wrong, if a man is indeed anti-sexist, he needs to display it, but this is accomplished through his actions and in his explanations of our current reality- especially to other men. Men must become examples to challenge the mainstream notions of masculinity and that takes more than a simple label.

Often complexities arise, however, when women challenge “anti-sexist” men. Men get defensive when women critique their oppressive and sexist behaviors. Rather than listening and benefiting from criticism, a defensive stance is taken and women’s voices are ignored once again. No one is above being questioned, as there should be no unnecessary hierarchy. The lack of principled criticism and self-criticism within the anarchist movement is the first problem that is then compounded when applied to issues of sexism and other forms of oppression. Women must be genuinely listened to and, if the criticisms are valid, men should seek to change their thoughts and their actions.

Understanding sexism is important to all within the anarchist movement. However, as a woman, it is not my duty to always answer questions and educate men on how sexism affects my life. Many anarchist groups already have a program or project in place that could be utilized to gain a better understanding of sexism without burdening women with the task of explaining our lives: the political study group. When was the last time you or your group read something on women, sexism, feminism, or women’s liberation?

Many times, and I have been guilty of this, we feel that readings on women’s issues are not as important as readings on capitalism or anarchism or anti-colonial struggle, etc. We have to stop considering women’s liberation as a side project or issue and view it as an integral part of the liberation struggle. These writings do not have to be specifically Anarchist or even revolutionary to give us good insights. When was the last time you read something by Audre Lorde, bell hooks, Barbara Smith, Angela Davis, Patricia Hill Collins, or Emma Goldman? We must take the initiative to read that which women have placed before us.

Since I was a little girl, I was socialized to feel inferior to men. I was socialized to recognize where my “place” was in society and it was not participating in an equal dialogue with men, certainly not in any type of politics, and it was definitely not on any kind of front line of revolutionary struggle. I often look around at meetings and events (that are not women-centric) and see that I am one of a handful of women in attendance or worse yet, the only woman there. Alternately, even when there are a lot of women in the room, I find that I am the only woman contributing to the dialogue.

When examining women’s involvement in political struggle, we have to examine the root causes. Women are socialized to look at politics as outside of our realm. When the politics are radical or revolutionary, the level of intimidation increases. Because of this reality, we have to exert a lot of time and energy into creating a more anti-sexist/pro-woman movement. We have to start by involving more women within our organizations and movements. This first involves putting sexism as one of the main points of organization alongside the other issues affecting women (and all humans): racism, heterosexism, ableism, colonialism, and class oppression. While we cannot place all of our energy into all of these problems at once, we must ensure we are dealing holistically with all of these issues within our focus. Second, we must actively recruit women into our organizations. This takes various forms such as tabling at women’s events, consistent outreach to women and participating in women-centric struggles.

Once women are in our organizations, we must look at the level of participation of women within the organization. I have been involved with politics for 7+ years. It has only been within the past year and a half that I have fully participated in politics. This is because I have had to learn that I could speak in meetings, that I could contribute in meaningful and positive ways, and that it is my place to contribute and participate. I have had to overcome the intimidation I felt when I was working with men who I looked up to and respected. I had to overcome the mental chains that were holding me back.

A couple factors contributed to this change. A dear comrade helped me realize that I am fully capable of participating and that no one can say different. For him, it was crucial that I participate on an equal level and he put a great deal of time and energy in encouraging me. I would love to see more men take up this task. Then, my level of commitment, seriousness, and sense of responsibility to liberatory politics forced me to put my level of involvement above my sense of comfort. This was not an easy task at all and one that I still struggle with to this day. This is something that we all have to battle within ourselves; men can help women get to this point by treating women equally and respectfully. We also must analyze our organizational behaviors. Are we consistently encouraging women to take up leadership positions? Is it mostly men or women who are taking up speaking engagements? Who talks at meetings? Who facilitates meetings? Who does the work of the organization, and then, who gets credit for it? We have to be very perceptive of men talking over women, invalidating and/or ignoring a woman’s words and contributions.

We all must make an extra effort to look at the gender dynamics of our functions and meetings. Without the direct leadership of women in any movement, our important voices are left out of the dialogue and the fight against sexism.

One of the biggest challenges to the anarchist movement is creating viable anti-authoritarian structures for our organizations. We are struggling to create new ideas of organization from the examples we have had and through new ideas and innovations. Not only are we trying to organize our movement in an anarchist fashion but it is also a testing ground for a future society.

Anarchism seeks to create a society based on a great sense of personal responsibility and accountability to ourselves and each other. We want a society based on mutual aid and communalism. This cannot happen out of spontaneous activity; it must result out of a highly organized society based on democratic, decentralized structures. I hope the anarchist movement realizes the need to work out new structural ideas for our organizations and a new society. I know many feel creating structure inherently runs counter to the ideas and principles of Anarchism. I would argue that not sitting down and forming democratic structures is counter to the ideas and principles of anarchism.

Jo Freeman wrote in The Tyranny of Structurelessness that “The idea of structurelessness does not prevent the formation of informal structures, only formal ones. A ‘laissez-faire’ ideal for group structure becomes a smoke screen for the strong or the lucky to establish unquestioned hegemony over others. Thus structurelessness becomes a way of masking power. As long as the structure of the group is informal, the rules of how decisions are made are known only to the few, and awareness of power is limited to those who know the rules.”

Structurelessness is often a means to perpetuate sexism, racism and class stratification. If men are socialized to be leaders and women are not, then it is not hard to imagine who would develop into leaders in a non-structured organization. A lack of structure provides no means of balancing those with certain privileges with those who are oppressed. We must create organizational structures that inherently guard against these forms of power imbalance.

In forming Anarchist organizational structures, we must also form structures to specifically deal with sexism in our organizations. One very sensitive issue that we have to address is sexual assault (and domestic violence). I have heard of many situations where a politically active male has sexually assaulted a fellow activist. It would be impossible to plan out all of the steps of dealing with this type of situation-especially since the survivor of sexual assault should largely control what happens-but we need a skeleton of steps to help handle this type of situation. Members of any organization should all have political education on both rape and sexual assault and how to deal when you or someone you know has been raped. Organizations should have a framework so that they are not fumbling around when sexual assault happens. Not having a framework could leave a survivor with little to no support from those whom should be providing as much support as she or he needs.

What can anarchist organizations do in these situations? What do we do if one amongst us is sexually assaulted? What do we do if one amongst us has sexually assaulted someone else? What do we do when both parties are in our organization? I challenge all organizations to consider how to prevent sexual assault from occurring in the first place, how to deal with it if it does, and how to support survivors of sexual assault to the fullest extent possible.

The struggle against sexism is everyone’s struggle. It affects everyone: men, women and transgendered peoples. It is especially important that anti-sexist men, who benefit from sexism, take up the struggle for womens’ liberation. Just as it is especially important for white people to dedicate themselves to anti-racist struggle and straight people to dedicate themselves to anti-homophobia/heterosexism work, men must dedicate an intense amount of time to anti-sexist work.

For anarchist men, the question is, are you involved with struggles spontaneously taken up by women, led and organized by women, and primarily aimed at other women? If not, why? I have heard the claim that many of the struggles are “too reformist.” In some cases this is my critique as well but I do not see a revolutionary struggle in the United States that is able to aid women in the ways these movements do. The answer is not to ignore these movements but to build new movements within or without that which already exists. Are anarchists creating alternate structures for survivors of sexual assault? Are we able to aid abused women in a revolutionary fashion at this point in time?

Others brush anti-sexist struggle off as “women’s work.” Others do not see anti-sexist struggle as central to the struggle for liberation. Others believe we can wait to challenge sexism when revolutionary change occurs.These analyses must change. If we truly want an egalitarian society then we must begin creating a more equitable movement-along lines of race, class, gender, and sexuality. We must make the anarchist movement a women’s movement. If we want an end to sexism, our work should have began yesterday.

Anarchists often have a good analysis of the way sexism is “a mesh of practices, institutions, and ideas which have an overall effect of giving more power to men than to women.” Beginning with an institutional analysis is correct, however, we must also translate this into our own thoughts and actions. Only then can all anarchists work together most effectively (at least along gender lines but we must also deal with homophobia, racism and class issues). To create an egalitarian society, our movement must be egalitarian and presently it is not. Working to create revolutionary change must begin today by challenging our sexist, racist, and heterosexist capitalist society. It means challenging that which is in ourselves, our families, our neighborhoods, our communities and our movements. As Kevin Powell said, “Just as I feel it is whites who need to be more vociferous about racism in their communities, I feel it is men who need to speak long and loud about sexism among each other.”

The Anarchist movement needs to be more vocal and active in the struggle against sexism. All our lives depend on it.




                  Table of Contents
                





                  Archive
                





                    Titles
                  



                    Authors
                  



                    Topics
                  




                    Latest entries
                  




                    Add a new text
                  



                    Real time help
                  




                    Titles
                  


                    Authors
                  


                    Topics
                  


                    Latest entries
                  


                    Add a new text
                  


                    Real time help
                  




                  More
                




About the project


Announcements archive


Distribution


Live Chat (IRC)


Recompiling the formats


The Torrent: 2019, winter


Bookshelf (wiki)



About the project

Announcements archive

Distribution

Live Chat (IRC)

Recompiling the formats

The Torrent: 2019, winter

Bookshelf (wiki)




                  Other languages
                




Anarchistische Bibliothek


Bibliothèque Anarchiste


Анархистичка библиотека


Det Anarkistiska Biblioteket


Anarhistička biblioteka


Biblioteca anarchica


Biblioteca anarquista


Anarkistinen kirjasto


Библиотека Анархизма


Det Anarkistiske Bibliotek


De Anarchistische Bibliotheek



Anarchistische Bibliothek

Bibliothèque Anarchiste

Анархистичка библиотека

Det Anarkistiska Biblioteket

Anarhistička biblioteka

Biblioteca anarchica

Biblioteca anarquista

Anarkistinen kirjasto

Библиотека Анархизма

Det Anarkistiske Bibliotek

De Anarchistische Bibliotheek




                  Bookbuilder
                





                  Mobile applications
                





                  RSS feed
                





                  Random
                





                  Add a new text
                

