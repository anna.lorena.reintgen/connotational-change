"""
################ Functionality ################
Wraps semaxis.py from the paper
1. Takes the semantic axes defined by the SemAxis paper
    Filters out those pairs that are not conatined in vocab
2. Applies the projection using filtered axes

Embedding used to filter the axes can be set individually
call filter_axes with complete path to desired embedding
"""

import csv
import gensim.models as mod
import numpy as np
import SemAxis.code.semaxis as semax
import preprocess as pre

####################################
# full path to folder containing all data
ROOT_DIR = " "
####################################

AXES = []


def filter_axes(_path):
    """
    filter the 732 semantic axes for those appearing in Wikipedia Featured
    _path gives the complete path to the embedding
    """
    source = mod.KeyedVectors.load(_path, mmap='r')

    all_axes = []
    final = []
    # read axes
    with open(ROOT_DIR+"SemAxis/axes/732_semaxis_axes.tsv", newline='') as semaxes:
        semaxes = csv.reader(semaxes, delimiter='\t')
        for axis in semaxes:
            all_axes.append((axis[0], axis[1]))

    # go through axes and remove all that dont fit
    for pos, neg in all_axes:
        try:
            source.vocab[pre.ST.stem(pos)]
        except KeyError:
            continue  # jump to next iteration
        try:
            source.vocab[pre.ST.stem(neg)]
        except KeyError:
            continue
        final.append((pos, neg))

    print('Number of axes that are usable: %i' % len(final))

    with open(ROOT_DIR+'axes', 'w', encoding='utf-8') as ax_file:
        for pos, neg in final:
            ax_file.write('{} {}\n'.format(pos, neg))
    AXES.extend(final)


def project_word(_word, _vec_space):
    """
    Wraps the semaxis projection function
    Only the dimensions that exist in our embedding
    """
    # catch eventuality that axes were not defined yet
    if not AXES:
        try:
            with open(ROOT_DIR+'axes', 'r', encoding='utf-8') as f:
                for line in f:
                    AXES.append([pre.ST.stem(i) for i in line.split()])
        except FileNotFoundError:
            print("Found no axes file. Running filter_axes first")
            # use default settings for loading the embedding file
            filter_axes(ROOT_DIR+'Embeddings/WFDocs300.kv')

    vec = []
    for dim in AXES:
        vec.append(semax.project_word_on_axis(_vec_space, _word, dim, k=3))
    # use np array instead of list
    return np.array(vec)
