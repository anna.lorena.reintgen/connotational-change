"""
################ Functionality ################
    read the reference model
    for every target model
    compare the frequencies of the top 1000 words
    find deviations with chi square metric
Is called from within main, but also runs on its own
"""

import gensim.models as mod
import numpy as np

ROOT_DIR = "" # path to folder containing all data
TARGETS = 'AnDocs CDocs MDocs MxDocs noTalkLW'.split()


def statistics(_source, _targets, _vec_size):
    """
    Compute the statistical significant deviations
    Based on Chi Square metric
    write results into result_stat

    Arguments:
        _targets {[string]} -- list of target embeddings
        _vec_size {int} -- vector size of embedding
    """
    # Load the source embedding  and get the top 1000 most frequent words
    source = mod.KeyedVectors.load(
        "{}Embeddings/{}{}.kv".format(ROOT_DIR, _source, _vec_size), mmap='r')

    source_count = {}
    for word in source.vocab:
        source_count[word] = source.vocab[word].count
    source_top = sorted(source_count.items(),
                        key=lambda x: x[1], reverse=True)[:1000]

    for trgt in _targets:
        print("Computing statistics for {}".format(trgt))
        # Load target and get top 1000 most frequent words
        target = mod.KeyedVectors.load(
            "{}Embeddings/{}{}.kv".format(ROOT_DIR, trgt, _vec_size), mmap='r')
        # Get top 1000 words for the target
        target_count = {}
        for word in target.vocab:
            target_count[word] = target.vocab[word].count
        target_top = sorted(target_count.items(),
                            key=lambda x: x[1], reverse=True)[:1000]

        # working set is the union of both top 1000 lists
        words = list(set([x for x, y in source_top] +
                         [x for x, y in target_top]))
        print("We have a list of {} most frequent words that are going to be compared".format(
            len(words)))

        # compute chi square  score for every word in the set
        # formula taken from subgroup paper by atzmueller
        chi = {}
        fac = len(target.vocab) / \
            (np.absolute(len(source.vocab) - len(target.vocab)))
        for word in words:
            try:
                chi[word] = fac * (target.vocab[word].count -
                                   source.vocab[word].count)**2
            except KeyError:
                try:  # with bigrams source vocab not always included in target
                    chi[word] = fac * (target.vocab[word].count - 0)**2
                except KeyError:
                    chi[word] = fac * (0 - source.vocab[word].count)**2
        res = sorted(chi.items(), key=lambda x: x[1], reverse=True)

        # file doesnt have to exist, is created if needed
        with open('{}result_stat'.format(ROOT_DIR), 'a', encoding='utf-8') as res_file:
            res_file.write(
                'Statistical Result for the embedding {}:\n'.format(trgt))
            for word, dist in res[:25]:
                try:
                    s_count = source.vocab[word].count
                except KeyError:
                    s_count = 0
                res_file.write('%s - %.2f, ' % (word, dist))
                res_file.write('count source: %i, ' % s_count)
                try:
                    res_file.write('count target: %i \n' %
                                   target.vocab[word].count)
                except KeyError:
                    res_file.write('count target: %i \n' % 0)
