"""
################ Functionality ################

 0. Construct a synthetic dataset for evaluation (added to pipeline afterwards)
    Uses Wikipedia Featured corpus
    Prerequesites:
        reference embedding
        base model
    directly processes the created dataset
    computes statistical and linear deviations
 1. Preprocess all texts in the targets
    Replace all punctuation by spaces
    Remove stopwords
    Stemming and Lemmatization
    Remove custom defined expressions
 2. Compute base model from wikipedia top articles
 3. Train the Models for each resp community on top of pretrained vectors
    Get the model for each community
    Create skip-gram vectors
    Store the vectors, delete model
 4. Compute the linear map for each target and the set of deviating words.
    (only step that is not optional)
    result can additionally be refined with semaxis projection
 5. Compute the statistical deviation for each target


    should be called like this:
    build_we.py <optional parameters>
     -u unigrams, default is bigrams
     --targets= <list of targets as one string>
     -v = 300 vector size for the embedding
     -p apply preprocessing to the targets
     --base_model (re)train base model
     --community_model (re)train community models
     --statistics compute statistical results
     --semax project result into semantic space
     --evaluation compute synthetic dataset for evaluation

    if arguments are left empty, default is used
"""

import argparse
import os
from gensim.test.utils import datapath
from gensim.models.phrases import Phrases, Phraser
from gensim import utils
import gensim.models as mod
from nltk.tokenize import word_tokenize
import preprocess as pre
import postprocess as post
import compute_projection as proj
import statistical_comp as stat
import evaluation as evl

############################################
# path to main folder containing all data
ROOT_DIR = ''
############################################

class MyCorpusIterator:
    """
    An Iterator that yields sentences (lists of str) from all the files found in path.
    Requires package os
    """

    def __init__(self, corpus):
        self.dir_path = ROOT_DIR + corpus.path
        self.index = [0, 0]  # file|line index
        self.list = os.listdir(self.dir_path)

    def __next__(self):
        ''''Returns the next value from the current files sentences '''
        if self.index[0] < len(self.list):  # number of files
            reader = open(self.dir_path+'/' +
                          self.list[self.index[0]], 'r', encoding='utf-8')
            content = reader.read().split('\n')  # read file line-wise into content-array
            reader.close()
            # Check if file is fully iterated or not
            if self.index[1] < len(content):
                result = word_tokenize(content[self.index[1]])
            elif not self.index[0] == len(self.list)-1:
                self.index[0] += 1
                self.index[1] = 0
                reader = open(self.dir_path+'/' +
                              self.list[self.index[0]], 'r', encoding='utf-8')
                content = reader.read().split('\n')  # read file line-wise into content-array
                reader.close()
                result = word_tokenize(content[self.index[1]])
            else:
                raise StopIteration
            self.index[1] += 1
            return result

       # End of Iteration
        raise StopIteration


class MyCorpus():
    """
    Only stores the path and overrides the __iter__ function
    word2vec needs iterator for training
 """

    def __init__(self, p):
        self._path = p

    def __iter__(self):
        return MyCorpusIterator(self)

    @property
    def path(self):
        """
        give the path stored in MyCorpus object

        Returns:
            self._path -- string of the path relative to ROOT_DIR
        """
        return self._path

    @path.setter
    def path(self, new_path):
        self._path = new_path


def trim(_word, _count, _min_count=10):
    """trim rule to be passed to build_vocab during training of the community models
    hard coded min count, because otherwise min_count = 5 is used from training of the base_model
    """
    if _count < 10 or _count < _min_count:
        return utils.RULE_DISCARD

    return utils.RULE_KEEP


if __name__ == '__main__':

    # define params
    TARGETS_ALL = 'AnDocs CDocs MDocs MxDocs LWDocs WFDocs'

    parser = argparse.ArgumentParser(description='framework')
    parser.add_argument('-u', action="store_true",
                        dest='unigrams', default=False)
    parser.add_argument('--semax', action="store_true",
                        dest='semaxis', default=False)
    parser.add_argument('--targets', action="store",
                        dest='targets', default=TARGETS_ALL)
    parser.add_argument('-v', action="store", dest='vec_size', default=300)
    parser.add_argument('-p', action="store_true",
                        dest='preprocess', default=False)
    parser.add_argument('--base_model', action="store_true",
                        dest='train_base', default=False)
    parser.add_argument('--community_model',
                        action="store_true", dest='train_com', default=False)
    parser.add_argument('--statistics', action='store_true',
                        dest='stat', default=False)
    parser.add_argument('--evaluation', action='store_true',
                        dest='eval', default=False)

   # load the parsed arguments into their variables
    args = parser.parse_args()
    UNIGRAMS = args.unigrams  # use unigrams or bigrams during training
    SEMAX = args.semaxis  # apply semAxis transformation before computing  the result set
    TARGETS = args.targets.split()  # list of target folders for processing
    VEC_SIZE = args.vec_size  # vector size for the embedding
    PREPROCESS = args.preprocess  # apply textual preprocessing to targets
    TRAIN_BASE = args.train_base  # (re)train the base model
    TRAIN_COMMUNITY = args.train_com  # (re)train community models
    STAT = args.stat  # compute the result set for statistical method
    # apply framework to synthetically modified reference corpus
    EVALUATION = args.eval

 # Step 0 - Generate synthetic dataset
 # automatically set train_community and statistics to true
 # use wikipedia featured corpus
    if EVALUATION:
        if UNIGRAMS:
            embedding = mod.KeyedVectors.load(
                '{}Embeddings/WFDocs{}.kv'.format(ROOT_DIR, VEC_SIZE))
        else:
            embedding = mod.KeyedVectors.load(
                '{}Embeddings/WFDocs_bi_{}.kv'.format(ROOT_DIR, VEC_SIZE))
        candidates = evl.compute_candidates(embedding)
        # use processed corpus to replace word pairs
        evl.replace_candidates(candidates, ROOT_DIR+'WFDocs')
        TARGETS = ['WFDocs_synthetic']
        TRAIN_COMMUNITY = True
        STAT = True

 # Step 1: Preprocessing
    if PREPROCESS:
        print("Started preprocessing of the raw document folders")
        for _dir in TARGETS:
            print("Currently handling {}".format(_dir))
            pre.process_dir(ROOT_DIR+_dir)
            post.process_dir(ROOT_DIR+_dir)

 # Step 2: train the base model
 # Use skip-gram for training, works well for small corpora, predicts context from word
 # base_model is not the same for bigrams and unigrams
    if not TRAIN_BASE and not UNIGRAMS:  # default case
        try:  # to load the base model from file
            model = mod.Word2Vec.load(
                "{}Embeddings/base_model_bi_{}.model".format(ROOT_DIR, VEC_SIZE))
            print("Loaded the bigram base model from file")
        except FileNotFoundError:
            print("Did not find base model, training on featured articles now")
            TRAIN_BASE = True
    elif not TRAIN_BASE and UNIGRAMS:
        try:  # to load the unigram base model from file
            model = mod.Word2Vec.load(
                "{}Embeddings/base_model{}.model".format(ROOT_DIR, VEC_SIZE))
            print("Loaded the unigram base model from file")
        except FileNotFoundError:
            print("Did not find base model")
            TRAIN_BASE = True
    if TRAIN_BASE:
        print('Initialized training of the base model.')
        sentences = MyCorpus('WFDocs')
        if not UNIGRAMS:
            phrases = Phrases(sentences, min_count=50, threshold=50.0)
         # bigram expressions
            bigrams = Phraser(phrases)
            del phrases
            # full path to <param> in current folder
            PATH = "{}Embeddings/base_model_bi_{}.model".format(
                ROOT_DIR, VEC_SIZE)
            # apply bigram detection before streaming the sentences into the training method
            base_model = mod.Word2Vec(
                [bigrams[item] for item in sentences], min_count=5, size=VEC_SIZE, workers=15, sg=1)
        else:
            PATH = "{}Embeddings/base_model{}.model".format(ROOT_DIR, VEC_SIZE)
            base_model = mod.Word2Vec(
                sentences, min_count=5, size=VEC_SIZE, workers=15, sg=1)
        base_model.save(PATH)
        print("Saved base model sucessfully to {}".format(PATH))
        pearson, spear, oov = base_model.wv.evaluate_word_pairs(
            datapath('wordsim353.tsv'))
        analogy_scores = base_model.wv.evaluate_word_analogies(
            datapath('questions-words.txt'))
        cor = 0
        icor = 0
        for item in analogy_scores[1]:
            cor += len(item['correct'])
            icor += len(item['incorrect'])
        with open('{}resumee'.format(ROOT_DIR), 'a', encoding='utf8') as f:
            f.write('Result for the base model:\n')
            f.write("pearson - {}, spear - {}, oov - {}, analogy test - ({},{})".format(
                pearson, spear, oov, cor, icor))

 # Step 3: Get models & vectors
 # Build corpora from the text files
 # start off with 3 additional training cycles, may need additional testing
    if TRAIN_COMMUNITY:
        print("Initializing Step 3: Training of the community models")
        if not os.path.exists('{}Embeddings'.format(ROOT_DIR)):
            os.makedirs('{}Embeddings'.format(ROOT_DIR))
        for _dir in TARGETS:
            # load sentences as generator stream from files in folder
            sentences = MyCorpus(_dir)
        # copy the base model and train on top of it
            if not UNIGRAMS:
                print("Training bigram model for the community %s" %_dir)
                model = mod.Word2Vec.load(
                    "{}Embeddings/base_model_bi_{}.model".format(ROOT_DIR, VEC_SIZE))
                phrases = Phrases(sentences, min_count=30,
                                  threshold=30.0)  # train a new phraser
                bigrams = Phraser(phrases)
                del phrases
                model.build_vocab(
                    [bigrams[item] for item in sentences], update=True, trim_rule=trim)
                count = 0
                for item in model.wv.vocab:
                    count += model.wv.vocab[item].count
                model.train([bigrams[item] for item in sentences],
                            total_words=count, epochs=3)
            # save the word vectors
                # in the end its both KeyedVectors
                model.wv.save(
                    '{}Embeddings/{}_bi_{}.kv'.format(ROOT_DIR, _dir, VEC_SIZE))
            else:
                model = mod.Word2Vec.load(
                    "{}Embeddings/base_model{}.model".format(ROOT_DIR, VEC_SIZE))
                model.build_vocab(sentences, update=True, trim_rule=trim)
                count = 0
                for item in model.wv.vocab:
                    count += model.wv.vocab[item].count
            # total words is necessary for linear alpha value decay
                model.train(sentences, total_words=count, epochs=3)
            # save the word vectors
                # in the end its both KeyedVectors
                model.wv.save(
                    '{}Embeddings/{}{}.kv'.format(ROOT_DIR, _dir, VEC_SIZE))
            print("Saved the vectors for {}.".format(_dir))
            pearson, spear, oov = model.wv.evaluate_word_pairs(
                datapath('wordsim353.tsv'))
            analogy_scores = model.wv.evaluate_word_analogies(
                datapath('questions-words.txt'))
            cor = 0
            icor = 0
            for item in analogy_scores[1]:
                cor += len(item['correct'])
                icor += len(item['incorrect'])
            with open('{}resumee'.format(ROOT_DIR), 'a', encoding='utf8') as f:
                f.write('Result for the {} model:\n'.format(_dir))
                f.write("pearson - {}, spear - {}, oov - {}, analogy test - ({},{})".format(
                    pearson, spear, oov, cor, icor))

            del model  # free memory space

 # Step 4 - Compute the projection
    SOURCE = 'WFDocs'
    if not UNIGRAMS:
        TARGETS = ['{}_bi_'.format(i) for i in TARGETS]
        SOURCE += '_bi_'
    proj.compute_projection(
        SOURCE, TARGETS, semaxis=SEMAX, _vec_size=VEC_SIZE)

 # Step 5 - Compute the results for the statistics method
    if STAT:
        stat.statistics(SOURCE, TARGETS, VEC_SIZE)


######################################################
