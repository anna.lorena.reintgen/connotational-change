"""
Perform syntactic preprocessing steps
Stemming, stopword filtering, punctuation

main function is process_dir
called on entire directory
writes cleaned text to new directory, filewise

"""
import os
import re
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import words as w
from tqdm import tqdm
import unidecode
from customlist import CLIST

STOPLIST = set(stopwords.words('english'))
STOPLIST.add('whenever')
STOPLIST.add('utc')
# add some additional stopwords
for word in ['january', 'jan', 'february', 'feb', 'march',
             'april', 'apr', 'may', 'june', 'jun', 'july',
             'jul', 'august', 'aug', 'september', 'sep',
             'october', 'oct', 'november', 'nov', 'december',
             'dec', 'since', 'edt', 'jump', 'even', 'however',
             'well', 'although', 'though']:
    STOPLIST.add(word)
# st = PorterStemmer() #alternative stemmer
ST = SnowballStemmer('english', ignore_stopwords=True)
DIC = set(w.words())


def remove_footers(txt):
    """
    Iterate through the customlist and remove expressions
    semantical filtering
    clist was constructed after inspection of dataset
    Introduce new line after every sentence
    Makes handling of text easier in later steps

    Arguments:
        txt {string} -- the string to be processed

    Returns:
        string -- txt cleared of all predefined expressions
    """
    for exp in CLIST:
        txt = txt.replace(exp, ' ')
    txt = txt.replace('.', '\n')  # make it easier to iterate over sentences

    return txt


def sanity_check(file, txt):
    """
    Check how many words are not contained in the dictionary

    Arguments:
        file {string} -- name of text file that is to be checked
        txt {string} -- content of file
    """
    nonwords = [i for i in txt.split() if i not in DIC and ST.stem(i)
                not in DIC]

    with open('Report_firstPass', "a", encoding="utf-8") as f:
        f.write("{} has {} fails:\n".format(file, len(nonwords)))
        f.write(' '.join(nonwords))
        f.write('\n')


def text_cleaning(txt_array):
    """
    Perform some basic text cleaning
    Remove punctuation, single 's' and 'th'
    Filter out stopwords

    Arguments:
        txt_array {[string]} -- list of strings

    Returns:
        [string] -- list of processed strings
    """
    res = []
    for txt in txt_array:
        # remove all punctuation
        sentence = re.sub(r'([^\w\s]|\d)+', ' ', txt.lower())
        sentence = unidecode.unidecode(sentence)  # remove accents

        # remove single 's'
        sentence = sentence.replace(' s ', ' ')
        sentence = sentence.replace(' th ', ' ')
        sentence = sentence.replace('  ', ' ')  # remove double spaces
        res.append(
            ' '.join([i for i in sentence.split() if i not in STOPLIST]))
    return res


def process_dir(_dir, check=False, generate_ouput=True):
    """ Perform textual preprocessing on a folder
    Expects raw files to be in <_dir>_raw 
    

    Arguments:
        _dir {string} -- target directory for processed files

    Keyword Arguments:
        check {bool} -- sanity check (stub) (default: {False})
        generate_ouput {bool} -- write to file (default: {True})
    """
    # unprocessed files in *_raw folder
    if generate_ouput:
        targetdir = _dir
        sourcedir = _dir+'_raw'
        if not os.path.exists(targetdir):
            os.makedirs(targetdir)
            print("Created target folder {}".format(targetdir))
# get the index of the folder
    filelist = os.listdir(sourcedir)
    print("Found {} files in the source folder".format(len(filelist)))

    for i in tqdm(range(len(filelist))):
        f = filelist[i]
        # print("Processing file {}".format(f))
        with open('{}/{}'.format(sourcedir, f), "r", encoding="utf-8") as readfile:
            raw_txt = re.sub(r'\^.*\n', '', readfile.read()
                             )  # remove the footnotes
            # less wrong has different ones
            raw_txt = re.sub(r'↑.*\n', '', raw_txt)
            filtered_txt = remove_footers(raw_txt).split(
                '\n')  # now a sentencewise array
            clean_txt = text_cleaning(filtered_txt)
            if check:
                sanity_check(f, clean_txt)
            if generate_ouput:
                with open('{}/{}'.format(targetdir, f), "w", encoding="utf-8") as writefile:
                    for j in enumerate(clean_txt):
                        tmp_txt = j[1]
                        if not tmp_txt:
                            continue
                        # try to filter out single letters
                        writefile.write(' '.join(ST.stem(i)
                                                 for i in tmp_txt.split() if len(i) > 1))
                        if len(tmp_txt.split()) > 2:
                            writefile.write('\n')
                        else:
                            writefile.write(' ')
