# Connotational Change in Word Embeddings

A framework to compare connotations across different word embeddings.

This is the complete code used for my master's thesis. It contains the scripts used for crawling five different communities:
The Anarchist Library (ancrawler.py), Conservapedia (ccrawler.py), Metapedia (mcrawler.py), Marxists Internet Archive (mxcrawler.py), Rationalwiki + LessWrong Blog (lwcrawler.py).

The framework pipeline is called by main.py and consists in the following steps:
1. Textual preprocessing of the raw text documents, stemming, filtering
2. Computing the word embedding model for the reference corpus (base model)
3. Retraining the community model on top of the base model
4. Find the set of words with most deviating connotation by linear transformation.
5. Optionally: qualitative analysis of results with semaxis projection by Yisun An (see [here](https://arxiv.org/pdf/1806.05521.pdf))
6. Optionally: frequency based analysis of word usage differences.

Requires path to project folder within main.py, compute_projection.py, sem_ax_proj.py

see main.py for detailed instructions.